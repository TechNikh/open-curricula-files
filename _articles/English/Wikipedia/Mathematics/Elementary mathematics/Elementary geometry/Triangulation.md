---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Triangulation
offline_file: ""
offline_thumbnail: ""
uuid: 55628875-229c-4c3e-85ea-e39441ad1542
updated: 1484308948
title: Triangulation
tags:
    - Applications
    - History
categories:
    - Elementary geometry
---
Specifically in surveying, triangulation per se involves only angle measurements, rather than measuring distances to the point directly as in trilateration; the use of both angles and distance measurements is referred to as triangulateration.
