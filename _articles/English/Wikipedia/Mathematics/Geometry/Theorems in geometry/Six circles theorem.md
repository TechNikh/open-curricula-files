---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Six_circles_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 3e331706-b02a-4442-81be-785dba234ddb
updated: 1484309126
title: Six circles theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Six_circles_theorem.svg.png
categories:
    - Theorems in geometry
---
In geometry, the six circles theorem relates to a chain of six circles together with a triangle, such that each circle is tangent to two sides of the triangle and also to the preceding circle in the chain. The chain closes, in the sense that the sixth circle is always tangent to the first circle.[1]
