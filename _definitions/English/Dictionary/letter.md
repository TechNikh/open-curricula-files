---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/letter
offline_file: ""
offline_thumbnail: ""
uuid: 9997df46-1162-410e-ac77-0735772c12f0
updated: 1484310284
title: letter
categories:
    - Dictionary
---
letter
     n 1: a written message addressed to a person or organization;
          "mailed an indignant letter to the editor" [syn: {missive}]
     2: the conventional characters of the alphabet used to
        represent speech; "his grandmother taught him his letters"
        [syn: {letter of the alphabet}, {alphabetic character}]
     3: a strictly literal interpretation (as distinct from the
        intention); "he followed instructions to the letter"; "he
        obeyed the letter of the law"
     4: an award earned by participation in a school sport; "he won
        letters in three sports" [syn: {varsity letter}]
     5: owner who lets another person use something (housing
        ...
