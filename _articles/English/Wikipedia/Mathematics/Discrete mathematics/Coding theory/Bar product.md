---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bar_product
offline_file: ""
offline_thumbnail: ""
uuid: 076d2a0b-e067-4fba-b204-8825d879aefd
updated: 1484308545
title: Bar product
tags:
    - Properties
    - Rank
    - Proof
    - Hamming weight
    - Proof
categories:
    - Coding theory
---
where (a | b) denotes the concatenation of a and b. If the code words in C1 are of length n, then the code words in C1 | C2 are of length 2n.
