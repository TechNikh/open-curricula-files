---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multivector
offline_file: ""
offline_thumbnail: ""
uuid: aa5f0b00-6ba5-4bf2-b95c-44ca9e5f2742
updated: 1484309231
title: Multivector
tags:
    - Wedge product
    - Area and volume
    - Multivectors in R2
    - Multivectors in R3
    - Grassmann coordinates
    - Multivectors on P2
    - Multivectors on P3
    - Clifford product
    - Geometric algebra
    - Examples
    - Bivectors
    - Applications
categories:
    - Differential geometry
---
A multivector is the result of a product defined for elements in a vector space V. A vector space with a linear product operation between elements of the space is called an algebra; examples are matrix algebra and vector algebra.[1][2][3] The algebra of multivectors is constructed using the wedge product ∧ and is related to the exterior algebra of differential forms.[4]
