---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rarely
offline_file: ""
offline_thumbnail: ""
uuid: e3ccc92c-e256-4b6f-9029-47a4601025d3
updated: 1484310275
title: rarely
categories:
    - Dictionary
---
rarely
     adv : not often; "we rarely met" [syn: {seldom}] [ant: {frequently}]
