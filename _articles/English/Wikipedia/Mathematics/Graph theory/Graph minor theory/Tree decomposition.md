---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tree_decomposition
offline_file: ""
offline_thumbnail: ""
uuid: f4ff66ea-6aa1-4284-8cf8-d361ab66db16
updated: 1484309417
title: Tree decomposition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Tree_decomposition.svg.png
tags:
    - Definition
    - Treewidth
    - Dynamic programming
    - Notes
categories:
    - Graph minor theory
---
In graph theory, a tree decomposition is a mapping of a graph into a tree that can be used to define the treewidth of the graph and speed up solving certain computational problems on the graph.
