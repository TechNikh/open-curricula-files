---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/labored
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484596501
title: labored
categories:
    - Dictionary
---
labored
     adj 1: lacking natural ease; "a labored style of debating" [syn: {laboured},
             {strained}]
     2: requiring or showing effort; "heavy breathing"; "the subject
        made for labored reading" [syn: {heavy}, {laboured}]
