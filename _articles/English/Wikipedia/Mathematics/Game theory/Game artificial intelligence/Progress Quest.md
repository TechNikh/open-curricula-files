---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Progress_Quest
offline_file: ""
offline_thumbnail: ""
uuid: e47b4362-6401-4550-8ab0-7e31b428ce19
updated: 1484309006
title: Progress Quest
tags:
    - Plot
    - Gameplay
    - Equipment
    - History
    - Critical reception and legacy
categories:
    - Game artificial intelligence
---
Progress Quest is a video game developed by Eric Fredricksen as a parody of EverQuest and other massively multiplayer online role-playing games. It is loosely considered a zero-player game, in the sense that once the player has set up their artificial character, there is no user interaction at all; the game "plays" itself, with the human player as spectator.
