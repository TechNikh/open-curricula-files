---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blindly
offline_file: ""
offline_thumbnail: ""
uuid: 589a1507-e076-4a12-b9b0-a12831a679d5
updated: 1484310573
title: blindly
categories:
    - Dictionary
---
blindly
     adv 1: without seeing or looking; "he felt around his desk blindly"
     2: without preparation or reflection; without a rational basis;
        "they bought the car blindly"; "he picked a wife blindly"
