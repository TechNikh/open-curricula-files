---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infectious
offline_file: ""
offline_thumbnail: ""
uuid: 0b3d79df-1591-4605-8e09-3dd5d5ca6a94
updated: 1484310445
title: infectious
categories:
    - Dictionary
---
infectious
     adj 1: caused by infection or capable of causing infection;
            "viruses and other infective agents"; "a carrier
            remains infective without himself showing signs of the
            disease" [syn: {infective}]
     2: easily spread; "fear is exceedlingly infectious; children
        catch it from their elders"- Bertrand Russell [ant: {noninfectious}]
     3: of or relating to infection; "infectious hospital";
        "infectious disease"
