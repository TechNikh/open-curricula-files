---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lincoln
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484421001
title: lincoln
categories:
    - Dictionary
---
Lincoln
     n 1: 16th President of the United States; saved the Union during
          the Civil War and emancipated the slaves; was
          assassinated by Booth (1809-1865) [syn: {Abraham Lincoln},
           {President Lincoln}, {President Abraham Lincoln}]
     2: capital of the state of Nebraska; located in southeastern
        Nebraska; site of the University of Nebraska [syn: {capital
        of Nebraska}]
     3: long-wooled mutton sheep originally from Lincolnshire
