---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unemployment
offline_file: ""
offline_thumbnail: ""
uuid: 39259ed8-d2af-4510-8680-676c44de9814
updated: 1484310453
title: unemployment
categories:
    - Dictionary
---
unemployment
     n : the state of being unemployed or not having a job;
         "unemployment is a serious social evil"; "the rate of
         unemployment is an indicator of the health of an economy"
         [ant: {employment}]
