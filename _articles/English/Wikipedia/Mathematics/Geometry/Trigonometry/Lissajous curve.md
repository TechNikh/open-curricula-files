---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lissajous_curve
offline_file: ""
offline_thumbnail: ""
uuid: 2cd03ebc-3f85-4293-bc9a-d7e9813e8372
updated: 1484309345
title: Lissajous curve
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Lissajous-Figur_1_zu_3_%2528Oszilloskop%2529.jpg'
tags:
    - Examples
    - Generation
    - Practical application
    - Application for the case of a = b
    - In engineering
    - In culture
    - In film
    - Company logos
    - In modern art
    - Notes
    - Interactive demos
categories:
    - Trigonometry
---
In mathematics, a Lissajous curve /ˈlɪsəʒuː/, also known as Lissajous figure or Bowditch curve /ˈbaʊdɪtʃ/, is the graph of a system of parametric equations
