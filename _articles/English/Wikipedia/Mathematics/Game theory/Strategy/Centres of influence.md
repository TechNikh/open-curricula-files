---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Centres_of_influence
offline_file: ""
offline_thumbnail: ""
uuid: 564d5c51-2829-406e-8421-718f1bd94869
updated: 1484309063
title: Centres of influence
categories:
    - Strategy
---
Centres of Influence (COI’s), sometimes referred to as Circles of Influence, is a common marketing term that describes the key people within a businesses target client segment they wish to penetrate.[citation needed]
