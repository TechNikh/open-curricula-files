---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reference_books
offline_file: ""
offline_thumbnail: ""
uuid: 65c40f84-a1c2-4191-b77b-3d15a8d671d4
updated: 1484308366
title: Reference work
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-2010-09_CPOV_IMG_6251.JPG
tags:
    - Reference book
    - Electronic resources
categories:
    - Uncategorized
---
A reference work is a book or periodical (or its electronic equivalent) to which one can refer for information. The information is intended to be found quickly when needed. Reference works are usually referred to for particular pieces of information, rather than read beginning to end. The writing style used in these works is informative; the authors avoid use of the first person, and emphasize facts. Many reference works are compiled by a team of contributors whose work is coordinated by one or more editors rather than by an individual author. Indices are commonly provided in many types of reference work. Updated editions are usually published as needed, in some cases annually (e.g. ...
