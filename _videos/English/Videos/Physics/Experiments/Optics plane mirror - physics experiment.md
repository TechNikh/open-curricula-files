---
version: 1
type: video
provider: YouTube
id: LMmnuUOZ6ho
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0c111563-b390-486e-9650-29b42a2a050f
updated: 1486069634
title: 'Optics plane mirror - physics experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/LMmnuUOZ6ho/default.jpg
    - https://i3.ytimg.com/vi/LMmnuUOZ6ho/1.jpg
    - https://i3.ytimg.com/vi/LMmnuUOZ6ho/2.jpg
    - https://i3.ytimg.com/vi/LMmnuUOZ6ho/3.jpg
---
