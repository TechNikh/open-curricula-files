---
version: 1
type: video
provider: YouTube
id: f1gG3e_WNaw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/what%20is%20Capacitance%20Animation%20Lesson%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3451670d-5785-42ec-b223-542cf779956b
updated: 1486069653
title: what is Capacitance Animation Lesson 3
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/f1gG3e_WNaw/default.jpg
    - https://i3.ytimg.com/vi/f1gG3e_WNaw/1.jpg
    - https://i3.ytimg.com/vi/f1gG3e_WNaw/2.jpg
    - https://i3.ytimg.com/vi/f1gG3e_WNaw/3.jpg
---
