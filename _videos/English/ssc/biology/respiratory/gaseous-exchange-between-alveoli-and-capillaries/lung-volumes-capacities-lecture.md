---
title: Lung Volumes & Capacities-lecture
youtube_id: AC5gwzmrXOM
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
