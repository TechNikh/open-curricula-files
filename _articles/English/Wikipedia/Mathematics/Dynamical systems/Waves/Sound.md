---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sound
offline_file: ""
offline_thumbnail: ""
uuid: 8e5f0c1f-dff5-46fe-aafe-9bc7c34cbfcf
updated: 1484308903
title: Sound
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Thoth08BigasDrumEvansChalmette.jpg
tags:
    - Acoustics
    - Definition
    - Physics of sound
    - Longitudinal and transverse waves
    - Sound wave properties and characteristics
    - Speed of sound
    - Perception of sound
    - Elements of sound perception
    - Pitch
    - Duration
    - Loudness
    - Timbre
    - Sonic texture
    - Spatial location
    - Noise
    - Soundscape
    - Sound pressure level
categories:
    - Waves
---
In physics, sound is a vibration that propagates as a typically audible mechanical wave of pressure and displacement, through a medium such as air or water. In physiology and psychology, sound is the reception of such waves and their perception by the brain.[1]
