---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swing
offline_file: ""
offline_thumbnail: ""
uuid: a42a790b-6dc5-4e52-9d62-5af2e1042182
updated: 1484310366
title: swing
categories:
    - Dictionary
---
swing
     n 1: a state of steady vigorous action that is characteristic of
          an activity; "the party went with a swing"; "it took
          time to get into the swing of things"
     2: mechanical device used as a plaything to support someone
        swinging back and forth
     3: a sweeping blow or stroke; "he took a wild swing at my head"
     4: changing location by moving back and forth [syn: {swinging},
         {vacillation}]
     5: a style of jazz played by big bands popular in the 1930s;
        flowing rhythms but less complex than later styles of jazz
        [syn: {swing music}, {jive}]
     6: a jaunty rhythm in music [syn: {lilt}]
     7: the act of swinging a golf ...
