---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/threaten
offline_file: ""
offline_thumbnail: ""
uuid: d59c4aaa-7ba1-4917-bf26-19a9d2a0119f
updated: 1484310174
title: threaten
categories:
    - Dictionary
---
threaten
     v 1: pose a threat to; present a danger to; "The pollution is
          endangering the crops" [syn: {endanger}, {jeopardize}, {jeopardise},
           {menace}, {imperil}, {peril}]
     2: to utter intentions of injury or punishment against:"He
        threatened me when I tried to call the police"
     3: to be a menacing indication of something:"The clouds
        threaten rain"; "Danger threatens"
