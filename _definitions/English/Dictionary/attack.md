---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attack
offline_file: ""
offline_thumbnail: ""
uuid: 9c1dab4c-aa10-4961-81b9-789f7601a0cf
updated: 1484310551
title: attack
categories:
    - Dictionary
---
attack
     n 1: (military) an offensive against an enemy (using weapons);
          "the attack began at dawn" [syn: {onslaught}, {onset}, {onrush}]
     2: a sudden occurrence of an uncontrollable condition; "an
        attack of diarrhea"
     3: intense adverse criticism; "Clinton directed his fire at the
        Republican Party"; "the government has come under attack";
        "don't give me any flak" [syn: {fire}, {flak}, {flack}, {blast}]
     4: the act of attacking; "attacks on women increased last
        year"; "they made an attempt on his life" [syn: {attempt}]
     5: an offensive move in a sport or game; "they won the game
        with a 10-hit attack in the 9th inning"
     ...
