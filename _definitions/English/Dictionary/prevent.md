---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prevent
offline_file: ""
offline_thumbnail: ""
uuid: 3ee83960-618f-45d3-8e89-43fe4f4ac9e7
updated: 1484310242
title: prevent
categories:
    - Dictionary
---
prevent
     v 1: keep from happening or arising; have the effect of
          preventing; "My sense of tact forbids an honest answer"
          [syn: {forestall}, {foreclose}, {preclude}, {forbid}]
     2: prevent from doing something or being in a certain state;
        "We must prevent the cancer from spreading"; "His snoring
        kept me from falling asleep"; "Keep the child from eating
        the marbles" [syn: {keep}] [ant: {let}]
