---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nervousness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484491261
title: nervousness
categories:
    - Dictionary
---
nervousness
     n 1: the anxious feeling you have when you have the jitters [syn:
           {jitteriness}, {jumpiness}, {restiveness}]
     2: an uneasy psychological state; "he suffered an attack of
        nerves" [syn: {nerves}]
     3: a sensitive or highly strung temperament
