---
version: 1
type: video
provider: YouTube
id: WWe7pZjc4s8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Cosine%20Taylor%20Series%20at%200%20%28Maclaurin%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 78ebb647-ca3d-425c-9448-cdb54a9bf823
updated: 1486069689
title: Cosine Taylor Series at 0 (Maclaurin)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/WWe7pZjc4s8/default.jpg
    - https://i3.ytimg.com/vi/WWe7pZjc4s8/1.jpg
    - https://i3.ytimg.com/vi/WWe7pZjc4s8/2.jpg
    - https://i3.ytimg.com/vi/WWe7pZjc4s8/3.jpg
---
