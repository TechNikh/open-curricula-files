---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/require
offline_file: ""
offline_thumbnail: ""
uuid: 18aeea95-0f2e-40db-b4b5-2a80e5753af8
updated: 1484310286
title: require
categories:
    - Dictionary
---
require
     v 1: require as useful, just, or proper; "It takes nerve to do
          what she did"; "success usually requires hard work";
          "This job asks a lot of patience and skill"; "This
          position demands a lot of personal sacrifice"; "This
          dinner calls for a spectacular dessert"; "This
          intervention does not postulates a patient's consent"
          [syn: {necessitate}, {ask}, {postulate}, {need}, {take},
           {involve}, {call for}, {demand}] [ant: {obviate}]
     2: consider obligatory; request and expect; "We require our
        secretary to be on time"; "Aren't we asking too much of
        these children?"; "I expect my students to arrive ...
