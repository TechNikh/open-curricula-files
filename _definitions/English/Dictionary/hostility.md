---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hostility
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484565841
title: hostility
categories:
    - Dictionary
---
hostility
     n 1: a hostile (very unfriendly) disposition; "he could not
          conceal his hostility" [syn: {ill will}]
     2: a state of deep-seated ill-will [syn: {enmity}, {antagonism}]
     3: the feeling of a hostile person; "he could no longer contain
        his hostility" [syn: {enmity}, {ill will}]
     4: violent action that is hostile and usually unprovoked [syn:
        {aggression}]
     5: acts of overt warfare; "the outbreak of hostilities" [syn: {belligerency}]
