---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Glycogen
offline_file: ""
offline_thumbnail: ""
uuid: a6206beb-529e-4cab-9fad-b90753e2a305
updated: 1484308337
title: Glycogen
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Glycogen_structure.svg.png
tags:
    - Structure
    - Functions
    - Liver
    - Muscle
    - History
    - Metabolism
    - Synthesis
    - Breakdown
    - Clinical relevance
    - Disorders of glycogen metabolism
    - Glycogen depletion and endurance exercise
categories:
    - Uncategorized
---
Glycogen is a multibranched polysaccharide of glucose that serves as a form of energy storage in animals[2] and fungi. The polysaccharide structure represents the main storage form of glucose in the body.
