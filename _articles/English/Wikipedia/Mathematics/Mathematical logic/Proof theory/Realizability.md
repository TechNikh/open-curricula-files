---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Realizability
offline_file: ""
offline_thumbnail: ""
uuid: 638a7c1b-04a3-48e2-890b-64365928b8db
updated: 1484309495
title: Realizability
tags:
    - 'Example: realizability by numbers'
    - Later developments
    - Applications
    - Notes
categories:
    - Proof theory
---
In mathematical logic, realizability is a collection of methods in proof theory used to study constructive proofs and extract additional information from them.[1] Formulas from a formal theory are "realized" by objects, known as "realizers", in a way that knowledge of the realizer gives knowledge about the truth of the formula. There are many variations of realizability; exactly which class of formulas is studied and which objects are realizers differ from one variation to another.
