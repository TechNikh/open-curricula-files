---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Calcination
offline_file: ""
offline_thumbnail: ""
uuid: bf60b719-2c25-4954-a49f-b3ffbae5987c
updated: 1484308389
title: Calcination
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-LDLimeShaftKilnBasic.jpg
tags:
    - Industrial processes
    - Calcination reactions
    - Oxidation
    - Alchemy
categories:
    - Uncategorized
---
Authorities differ on the meaning of calcination (also referred to as calcining). The IUPAC defines it as 'heating to high temperatures in air or oxygen'.[1] However, calcination is also used to mean a thermal treatment process in the absence or limited supply of air or oxygen applied to ores and other solid materials to bring about a thermal decomposition. A calciner is a steel cylinder that rotates inside a heated furnace and performs indirect high-temperature processing (550–1150 °C, or 1000–2100 °F) within a controlled atmosphere.[2] Some chemical changes occur during calcination:
