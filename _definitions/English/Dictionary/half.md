---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/half
offline_file: ""
offline_thumbnail: ""
uuid: ff33c304-bd5f-4ea5-b8a8-b0698d445014
updated: 1484310343
title: half
categories:
    - Dictionary
---
half
     adj 1: consisting of one of two equivalent parts in value or
            quantity; "a half chicken"; "lasted a half hour" [syn:
             {half(a)}]
     2: partial; "gave me a half smile"; "he did only a half job"
        [syn: {half(a)}]
     3: (of siblings) related through one parent only; "a half
        brother"; "half sister" [ant: {whole}]
     n 1: one of two equal parts of a divisible whole; "half a loaf";
          "half an hour"; "a century and one half" [syn: {one-half}]
     2: in various games or performances: either of two periods of
        play separated by an interval
     adv : partially or to the extent of a half; "he was half hidden by
           the ...
