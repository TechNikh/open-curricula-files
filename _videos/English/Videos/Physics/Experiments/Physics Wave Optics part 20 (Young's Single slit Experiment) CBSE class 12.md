---
version: 1
type: video
provider: YouTube
id: ERx74CXtFj0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e1878e04-9ec8-4bd1-bc27-1bd0858c6bce
updated: 1486069635
title: "Physics Wave Optics part 20 (Young's Single slit Experiment) CBSE class 12"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/ERx74CXtFj0/default.jpg
    - https://i3.ytimg.com/vi/ERx74CXtFj0/1.jpg
    - https://i3.ytimg.com/vi/ERx74CXtFj0/2.jpg
    - https://i3.ytimg.com/vi/ERx74CXtFj0/3.jpg
---
