---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upbringing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484620201
title: upbringing
categories:
    - Dictionary
---
upbringing
     n 1: properties acquired during a person's formative years
     2: raising someone to be an accepted member of the community;
        "they debated whether nature or nurture was more
        important" [syn: {breeding}, {bringing up}, {fostering}, {fosterage},
         {nurture}, {raising}, {rearing}]
