---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interlinking
offline_file: ""
offline_thumbnail: ""
uuid: 46b36c0e-3dca-4956-9e71-2191a9eaac98
updated: 1484310525
title: interlinking
categories:
    - Dictionary
---
interlinking
     adj : linked or locked closely together as by dovetailing [syn: {interlacing},
            {interlocking}, {interwoven}]
