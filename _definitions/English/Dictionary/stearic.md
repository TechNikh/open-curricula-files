---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stearic
offline_file: ""
offline_thumbnail: ""
uuid: bc8f408a-3e47-4bfd-adf5-d8ab1dfe3568
updated: 1484310426
title: stearic
categories:
    - Dictionary
---
stearic
     adj : of or relating to or composed of fat
