---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sufficiently
offline_file: ""
offline_thumbnail: ""
uuid: c7029886-fbf3-4c9d-b1c9-302b30bbb612
updated: 1484310200
title: sufficiently
categories:
    - Dictionary
---
sufficiently
     adv : to a sufficient degree; "she was sufficiently fluent in
           Mandarin" [ant: {insufficiently}]
