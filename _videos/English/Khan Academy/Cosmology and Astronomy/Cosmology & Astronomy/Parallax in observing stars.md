---
version: 1
type: video
provider: YouTube
id: ETzUpoqZIHY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Parallax%20in%20observing%20stars.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 64df0b75-1b37-43ca-910a-6bf3993ec3c7
updated: 1486069686
title: Parallax in observing stars
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/ETzUpoqZIHY/default.jpg
    - https://i3.ytimg.com/vi/ETzUpoqZIHY/1.jpg
    - https://i3.ytimg.com/vi/ETzUpoqZIHY/2.jpg
    - https://i3.ytimg.com/vi/ETzUpoqZIHY/3.jpg
---
