---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Optoelectrofluidics
offline_file: ""
offline_thumbnail: ""
uuid: 521c3bd5-558e-4a44-9a1b-8e59dccf5282
updated: 1484308762
title: Optoelectrofluidics
tags:
    - Display Devices
    - System Configuration
    - Target Materials
    - Operating Principles
    - Other Phenomena
    - Applications
    - Research Groups
categories:
    - Electrodynamics
---
Optoelectrofluidics, also known as optically induced electrohydrodynamics, refers to the study of the motions of particles or molecules and their interactions with optically-induced electric field and the surrounding fluid.
