---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Church%E2%80%93Turing%E2%80%93Deutsch_principle'
offline_file: ""
offline_thumbnail: ""
uuid: 16921b5c-a3d6-4dfd-a723-4c0cf761b705
updated: 1484309485
title: Church–Turing–Deutsch principle
tags:
    - Statement
    - History
    - Notes
    - General references
categories:
    - Computability theory
---
In computer science and quantum physics, the Church–Turing–Deutsch principle (CTD principle[1]) is a stronger, physical form of the Church–Turing thesis formulated by David Deutsch in 1985.
