---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polar_vortex
offline_file: ""
offline_thumbnail: ""
uuid: 96bb3219-abf9-4fc0-a7bf-bae143cd5aad
updated: 1484308874
title: Polar vortex
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Polarvortexjan211985.jpg
tags:
    - History
    - Identification
    - Duration and power
    - Climate change
    - Ozone depletion
    - Outside Earth
    - Hot polar vortex
categories:
    - Vortices
---
A polar vortex is an upper level low-pressure area, that lies near the Earth's pole. There are two polar vortices in the Earth's atmosphere, which overlie the North, and South Poles. Each polar vortex is a persistent, large-scale, low pressure zone that rotates counter-clockwise at the North Pole (called a cyclone), and clockwise at the South Pole. The bases of the two polar vortices are located in the middle and upper troposphere and extend into the stratosphere. Beneath that lies a large mass of cold, dense arctic air. The vortices weaken and strengthen from year to year. When the vortex of the arctic is strong it is well defined, there is a single vortex and the arctic air is well ...
