---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Marching_line
offline_file: ""
offline_thumbnail: ""
uuid: 0d7da33f-be17-4a85-9f56-c4053a6f5457
updated: 1484309297
title: Marching line
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-2016_Busola.JPG
categories:
    - Orientation (geometry)
---
Marching lines are a pair of lines drawn on the glass of a compass, and arranged at 45 degrees to each other. These are an essential component in hiking through the wilderness. Most modern compasses have adjustable luminous marching lines.
