---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fifty
offline_file: ""
offline_thumbnail: ""
uuid: a437530d-1d9f-4ec6-b368-4bc27bb83028
updated: 1484310518
title: fifty
categories:
    - Dictionary
---
fifty
     adj : being ten more than forty [syn: {50}, {l}]
     n 1: the cardinal number that is the product of ten and five
          [syn: {50}, {L}]
     2: a United States bill worth 50 dollars [syn: {fifty dollar
        bill}]
