---
version: 1
type: video
provider: YouTube
id: ibnIs-0z_dY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fe60ed16-4a0a-40ea-9f2c-690f988655c0
updated: 1486069641
title: 'SOLAR QUEST - Celestial Mass'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/ibnIs-0z_dY/default.jpg
    - https://i3.ytimg.com/vi/ibnIs-0z_dY/1.jpg
    - https://i3.ytimg.com/vi/ibnIs-0z_dY/2.jpg
    - https://i3.ytimg.com/vi/ibnIs-0z_dY/3.jpg
---
