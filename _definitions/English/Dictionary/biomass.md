---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biomass
offline_file: ""
offline_thumbnail: ""
uuid: 6696d068-f42f-4951-847e-e57ac1435c76
updated: 1484310268
title: biomass
categories:
    - Dictionary
---
biomass
     n 1: plant materials and animal waste used as fuel
     2: the total mass of living matter in a given unit area
