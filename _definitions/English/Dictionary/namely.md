---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/namely
offline_file: ""
offline_thumbnail: ""
uuid: 4aa69f6e-23a5-4884-b7c7-5e50c4a325e8
updated: 1484310216
title: namely
categories:
    - Dictionary
---
namely
     adv : as follows [syn: {viz.}, {that is to say}, {videlicet}]
