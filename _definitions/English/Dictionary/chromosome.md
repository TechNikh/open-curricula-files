---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chromosome
offline_file: ""
offline_thumbnail: ""
uuid: 45fbf1f8-a4aa-484e-a537-af94be3c8c5b
updated: 1484310295
title: chromosome
categories:
    - Dictionary
---
chromosome
     n : a threadlike body in the cell nucleus that carries the genes
         in a linear order
