---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Curve_orientation
offline_file: ""
offline_thumbnail: ""
uuid: 1479ec33-d967-4518-abb3-5252a46fee91
updated: 1484309292
title: Curve orientation
tags:
    - Orientation of a simple polygon
    - Practical considerations
    - Local concavity
categories:
    - Orientation (geometry)
---
In mathematics, a positively oriented curve is a planar simple closed curve (that is, a curve in the plane whose starting point is also the end point and which has no other self-intersections) such that when traveling on it one always has the curve interior to the left (and consequently, the curve exterior to the right). If in the above definition one interchanges left and right, one obtains a negatively oriented curve.
