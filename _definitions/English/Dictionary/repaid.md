---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repaid
offline_file: ""
offline_thumbnail: ""
uuid: 9a265a45-cb12-4c3b-87d8-2c44726aa8ce
updated: 1484310183
title: repaid
categories:
    - Dictionary
---
repay
     v 1: pay back; "Please refund me my money" [syn: {refund}, {return},
           {give back}]
     2: make repayment for or return something [syn: {requite}]
     3: act or give recompensation in recognition of someone's
        behavior or actions [syn: {reward}, {pay back}]
     4: answer back [syn: {retort}, {come back}, {return}, {riposte},
         {rejoin}]
     [also: {repaid}]
