---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greek
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484309941
title: greek
categories:
    - Dictionary
---
Greek
     adj : of or relating to or characteristic of Greece or the Greeks;
           "Greek mythology"; "a grecian robe" [syn: {Grecian}, {Hellenic}]
     n 1: the Hellenic branch of the Indo-European family of languages
          [syn: {Hellenic}, {Hellenic language}]
     2: a native or inhabitant of Greece [syn: {Hellene}]
