---
version: 1
type: video
provider: YouTube
id: 7hugaxQ5a3U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5246c3de-a48f-4d8a-a9ad-5ee79f03c8cf
updated: 1486069635
title: >
    Determination of focal length of convex lens experiment
    (CBSE) demo video by LabInApp
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/7hugaxQ5a3U/default.jpg
    - https://i3.ytimg.com/vi/7hugaxQ5a3U/1.jpg
    - https://i3.ytimg.com/vi/7hugaxQ5a3U/2.jpg
    - https://i3.ytimg.com/vi/7hugaxQ5a3U/3.jpg
---
