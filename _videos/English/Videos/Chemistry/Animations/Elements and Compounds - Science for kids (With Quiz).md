---
version: 1
type: video
provider: YouTube
id: avgFqlNML5o
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Elements%20and%20Compounds%20-%20Science%20for%20kids%20%28With%20Quiz%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c01d36cd-3fa7-4873-8cd0-0e0f24ceceb1
updated: 1486069664
title: 'Elements and Compounds - Science for kids (With Quiz)'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/avgFqlNML5o/default.jpg
    - https://i3.ytimg.com/vi/avgFqlNML5o/1.jpg
    - https://i3.ytimg.com/vi/avgFqlNML5o/2.jpg
    - https://i3.ytimg.com/vi/avgFqlNML5o/3.jpg
---
