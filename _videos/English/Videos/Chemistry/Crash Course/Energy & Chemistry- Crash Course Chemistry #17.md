---
version: 1
type: video
provider: YouTube
id: GqtUWyDR1fg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Energy%20%26%20Chemistry-%20Crash%20Course%20Chemistry%20%2317.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c9391a66-0072-4cd1-8522-ab85c912e96f
updated: 1486069680
title: 'Energy & Chemistry- Crash Course Chemistry #17'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/GqtUWyDR1fg/default.jpg
    - https://i3.ytimg.com/vi/GqtUWyDR1fg/1.jpg
    - https://i3.ytimg.com/vi/GqtUWyDR1fg/2.jpg
    - https://i3.ytimg.com/vi/GqtUWyDR1fg/3.jpg
---
