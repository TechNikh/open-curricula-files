---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vertex_separator
offline_file: ""
offline_thumbnail: ""
uuid: 2f302453-a153-4dc5-8d52-5954189bd28d
updated: 1484309405
title: Vertex separator
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Grid_separator.svg.png
tags:
    - Examples
    - Minimal separators
    - Notes
categories:
    - Graph connectivity
---
In graph theory, a vertex subset 
  
    
      
        S
        ⊂
        V
      
    
    {\displaystyle S\subset V}
  
 is a vertex separator (or vertex cut, separating set) for nonadjacent vertices 
  
    
      
        a
      
    
    {\displaystyle a}
  
 and 
  
    
      
        b
      
    
    {\displaystyle b}
  
 if the removal of 
  
    
      
        S
      
    
    {\displaystyle S}
  
 from the graph separates 
  
    
      
        a
      
    
    {\displaystyle a}
  
 and 
  
    
      
        b
      
    
    {\displaystyle b}
  
 into distinct connected components.
