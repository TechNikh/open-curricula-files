---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reporting
offline_file: ""
offline_thumbnail: ""
uuid: 53a1b17e-0d88-4abd-917a-0a0e3f243d0e
updated: 1484310517
title: reporting
categories:
    - Dictionary
---
reporting
     n : the news as presented by reporters for newspapers or radio
         or television; "they accused the paper of biased coverage
         of race relations" [syn: {coverage}, {reportage}]
