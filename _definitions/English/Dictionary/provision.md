---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/provision
offline_file: ""
offline_thumbnail: ""
uuid: 0f1d50ab-eb4b-4ceb-9ebc-f2319c9e8eae
updated: 1484310447
title: provision
categories:
    - Dictionary
---
provision
     n 1: a stipulated condition; "he accepted subject to one
          provision" [syn: {proviso}]
     2: the activity of supplying or providing something [syn: {supply},
         {supplying}]
     3: the cognitive process of thinking about what you will do in
        the event of something happening; "his planning for
        retirement was hindered by several uncertainties" [syn: {planning},
         {preparation}]
     4: a store or supply of something (especially of food or
        clothing or arms)
     v : supply with provisions [syn: {purvey}]
