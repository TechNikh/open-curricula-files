---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Time_reversibility
offline_file: ""
offline_thumbnail: ""
uuid: f28c7ba0-5210-4245-bcd8-c3ed2f518a6c
updated: 1484309321
title: Time reversibility
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/688px-MontreGousset001_1.jpg
tags:
    - Mathematics
    - Physics
    - Stochastic processes
    - Waves and optics
    - Notes
categories:
    - Symmetry
---
A deterministic process is time-reversible if the time-reversed process satisfies the same dynamic equations as the original process; in other words, the equations are invariant or symmetrical under a change in the sign of time. A stochastic process is reversible if the statistical properties of the process are the same as the statistical properties for time-reversed data from the same process.
