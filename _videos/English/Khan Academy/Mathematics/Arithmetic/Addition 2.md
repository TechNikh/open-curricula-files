---
version: 1
type: video
provider: YouTube
id: t2L3JFOqTEk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Addition%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: defa12e7-2cfa-478b-801d-5dfddb27dea4
updated: 1486069567
title: Addition 2
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/t2L3JFOqTEk/default.jpg
    - https://i3.ytimg.com/vi/t2L3JFOqTEk/1.jpg
    - https://i3.ytimg.com/vi/t2L3JFOqTEk/2.jpg
    - https://i3.ytimg.com/vi/t2L3JFOqTEk/3.jpg
---
Adding 2-digit numbers
