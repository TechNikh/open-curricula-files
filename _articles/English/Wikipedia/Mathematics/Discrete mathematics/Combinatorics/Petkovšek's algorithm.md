---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Petkov%C5%A1ek%27s_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: fdf16372-aee0-4fbc-a0f3-1593a0a49132
updated: 1484308577
title: "Petkovšek's algorithm"
categories:
    - Combinatorics
---
Petkovšek's algorithm is a computer algebra algorithm that computes a basis of hypergeometric terms solution of its input linear recurrence equation with polynomial coefficients. Equivalently, it computes a first order right factor of linear difference operators with polynomial coefficients. This algorithm is implemented in all the major computer algebra systems.
