---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/petroleum
offline_file: ""
offline_thumbnail: ""
uuid: 97eb13f2-7e3a-4851-8ffc-79e2a3ae4e41
updated: 1484310327
title: petroleum
categories:
    - Dictionary
---
petroleum
     n : a dark oil consisting mainly of hydrocarbons [syn: {crude
         oil}, {crude}, {rock oil}, {fossil oil}]
