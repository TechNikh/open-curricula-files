---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loan
offline_file: ""
offline_thumbnail: ""
uuid: 987a4d87-0f7a-4a14-8945-fd586c4bdb24
updated: 1484310459
title: loan
categories:
    - Dictionary
---
loan
     n 1: the temporary provision of money (usually at interest)
     2: a word borrowed from another language; e.g. `blitz' is a
        German word borrowed into modern English [syn: {loanword}]
     v : give temporarily; let have for a limited time; "I will lend
         you my car"; "loan me some money" [syn: {lend}] [ant: {borrow}]
