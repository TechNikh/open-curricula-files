---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bullying
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484409301
title: bullying
categories:
    - Dictionary
---
bullying
     adj : noisily domineering; tending to browbeat others [syn: {blustery}]
     n : the act of intimidating a weaker person to make them do
         something [syn: {intimidation}]
