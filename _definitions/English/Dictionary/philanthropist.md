---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/philanthropist
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484428801
title: philanthropist
categories:
    - Dictionary
---
philanthropist
     n : someone who makes charitable donations intended to increase
         human well-being [syn: {altruist}]
