---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biogeochemical
offline_file: ""
offline_thumbnail: ""
uuid: 783f6850-d959-47ba-ab20-f90054073e89
updated: 1484308340
title: Biogeochemistry
tags:
    - History
    - Early development
    - Research
    - Representative books and publications
categories:
    - Uncategorized
---
Biogeochemistry is the scientific discipline that involves the study of the chemical, physical, geological, and biological processes and reactions that govern the composition of the natural environment (including the biosphere, the cryosphere, the hydrosphere, the pedosphere, the atmosphere, and the lithosphere). In particular, biogeochemistry is the study of the cycles of chemical elements, such as carbon and nitrogen, and their interactions with and incorporation into living things transported through earth scale biological systems in space through time. The field focuses on chemical cycles which are either driven by or influence biological activity. Particular emphasis is placed on the ...
