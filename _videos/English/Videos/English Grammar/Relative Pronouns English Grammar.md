---
version: 1
type: video
provider: YouTube
id: As0h2-fzVME
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1c83d864-1524-4d3b-b2e1-a737a2d7f457
updated: 1486069631
title: Relative Pronouns English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/As0h2-fzVME/default.jpg
    - https://i3.ytimg.com/vi/As0h2-fzVME/1.jpg
    - https://i3.ytimg.com/vi/As0h2-fzVME/2.jpg
    - https://i3.ytimg.com/vi/As0h2-fzVME/3.jpg
---
