---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pieter_Hendrik_Schoute
offline_file: ""
offline_thumbnail: ""
uuid: d591fce0-c306-4627-b93e-a9ef9a3698a5
updated: 1484309098
title: Pieter Hendrik Schoute
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pieter_Hendrik_Schoute%252C_1911.jpg'
tags:
    - External links and references
    - Sources
categories:
    - Geometers
---
Pieter Hendrik Schoute (21 January 1846, Wormerveer – 18 April 1923, Groningen) was a Dutch mathematician known for his work on regular polytopes and Euclidean geometry.
