---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/striking
offline_file: ""
offline_thumbnail: ""
uuid: 41b0989d-a7bd-43d1-bec6-de39b41021c9
updated: 1484310553
title: striking
categories:
    - Dictionary
---
striking
     adj 1: sensational in appearance or thrilling in effect; "a
            dramatic sunset"; "a dramatic pause"; "a spectacular
            display of northern lights"; "it was a spectacular
            play"; "his striking good looks always created a
            sensation" [syn: {dramatic}, {spectacular}]
     2: having a quality that thrusts itself into attention; "an
        outstanding fact of our time is that nations poisoned by
        anti semitism proved less fortunate in regard to their own
        freedom"; "a new theory is the most prominent feature of
        the book"; "salient traits"; "a spectacular rise in
        prices"; "a striking thing about Picadilly Circus ...
