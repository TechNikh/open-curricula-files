---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decorate
offline_file: ""
offline_thumbnail: ""
uuid: 4aa1b44a-4d6f-4a76-9889-5305bb927eed
updated: 1484310439
title: decorate
categories:
    - Dictionary
---
decorate
     v 1: make more attractive by adding ornament, colour, etc.;
          "Decorate the room for the party"; "beautify yourself
          for the special day" [syn: {adorn}, {grace}, {ornament},
           {embellish}, {beautify}]
     2: be beautiful to look at; "Flowers adorned the tables
        everywhere" [syn: {deck}, {adorn}, {grace}, {embellish}, {beautify}]
     3: award a mark of honor, as a medal, to
     4: provide with decoration; "dress the windows" [syn: {dress}]
