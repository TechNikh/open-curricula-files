---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/teach
offline_file: ""
offline_thumbnail: ""
uuid: 9710b346-2cb0-43f5-97b1-a7521c2ffbbc
updated: 1484310518
title: teach
categories:
    - Dictionary
---
Teach
     n : an English pirate who operated in the Caribbean and off the
         Atlantic coast of North America (died in 1718) [syn: {Edward
         Teach}, {Thatch}, {Edward Thatch}, {Blackbeard}]
     v 1: impart skills or knowledge to; "I taught them French"; "He
          instructed me in building a boat" [syn: {learn}, {instruct}]
     2: accustom gradually to some action or attitude; "The child is
        taught to obey her parents"
