---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Localization_formula_for_equivariant_cohomology
offline_file: ""
offline_thumbnail: ""
uuid: 3ba8d69e-8fc2-41d9-be0b-97f3a8a2c26f
updated: 1484309228
title: Localization formula for equivariant cohomology
categories:
    - Differential geometry
---
In differential geometry, the localization formula states: for an equivariantly closed equivariant differential form 
  
    
      
        α
      
    
    {\displaystyle \alpha }
  
 on an orbifold M with a torus action and for a sufficient small 
  
    
      
        ξ
      
    
    {\displaystyle \xi }
  
 in the Lie algebra of the torus T,
