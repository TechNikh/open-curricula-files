---
version: 1
type: video
provider: YouTube
id: TfXsr1AAaKY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fb7bae19-c311-4e6a-8dd2-ead6e3df8565
updated: 1486069640
title: 'Physics Lab - 2. Linear Motion with Constant Acceleration and Motion in a Plane'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/TfXsr1AAaKY/default.jpg
    - https://i3.ytimg.com/vi/TfXsr1AAaKY/1.jpg
    - https://i3.ytimg.com/vi/TfXsr1AAaKY/2.jpg
    - https://i3.ytimg.com/vi/TfXsr1AAaKY/3.jpg
---
