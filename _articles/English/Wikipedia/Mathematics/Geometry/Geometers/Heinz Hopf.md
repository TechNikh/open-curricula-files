---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heinz_Hopf
offline_file: ""
offline_thumbnail: ""
uuid: 2632bbe3-8698-4e5e-a4a0-48ffa8a2efa5
updated: 1484309085
title: Heinz Hopf
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Hopf_Link.png
tags:
    - Early life and education
    - Career
    - Personal life
    - Honors and awards
    - Publications
categories:
    - Geometers
---
Heinz Hopf (19 November 1894 – 3 June 1971) was a German mathematician who worked on the fields of topology and geometry. He was one of the greatest mathematicians of the 20th century.[1]
