---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/constitutional
offline_file: ""
offline_thumbnail: ""
uuid: 410d18e1-a6a6-40e4-aed8-59d6046423d5
updated: 1484310569
title: constitutional
categories:
    - Dictionary
---
constitutional
     adj 1: of or relating to a constitution; "constitutional
            amendments"
     2: of benefit to or intended to benefit your physical makeup;
        "constitutional walk"
     3: sanctioned by or consistent with or operating under a
        constitution; "the constitutional right of free speech";
        "constitutional government"; "constitutional guarantees"
        [ant: {unconstitutional}]
     4: existing as an essential constituent or characteristic; "the
        Ptolemaic system with its built-in concept of
        periodicity"; "a constitutional inability to tell the
        truth" [syn: {built-in}, {inbuilt}, {inherent}, {integral}]
     5: constitutional ...
