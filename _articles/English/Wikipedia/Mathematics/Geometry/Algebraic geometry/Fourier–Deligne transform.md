---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fourier%E2%80%93Deligne_transform'
offline_file: ""
offline_thumbnail: ""
uuid: ba25ff8b-033a-4ce8-b417-d25a46ec23e4
updated: 1484309147
title: Fourier–Deligne transform
categories:
    - Algebraic geometry
---
In algebraic geometry, the Fourier–Deligne transform, or ℓ-adic Fourier transform, or geometric Fourier transform, is an operation on objects of the derived category of ℓ-adic sheaves over the affine line. It was introduced by Pierre Deligne on November 29, 1976 in a letter to David Kazhdan as an analogue of the usual Fourier transform. It was used by Laumon (1987) to simplify Deligne's proof of the Weil conjectures.
