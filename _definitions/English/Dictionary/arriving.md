---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arriving
offline_file: ""
offline_thumbnail: ""
uuid: 0041e158-deb9-4a99-82c4-dd8943b66bb9
updated: 1484310174
title: arriving
categories:
    - Dictionary
---
arriving
     adj : directed or moving inward or toward a center; "the inbound
           train"; "inward flood of capital" [syn: {arriving(a)},
           {inbound}, {inward}]
