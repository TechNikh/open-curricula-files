---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sikkim
offline_file: ""
offline_thumbnail: ""
uuid: 2e2faa6c-ddcc-4807-924b-eac244da66a4
updated: 1484308415
title: Sikkim
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Historical_Map_of_Sikkim_in_northeastern_India.jpg
tags:
    - Toponymy
    - History
    - Foundation of the monarchy
    - Sikkim during the British Raj
    - After Indian independence
    - Recent history
    - geography
    - Geology
    - Climate
    - Government and politics
    - Subdivisions
    - Flora and Fauna
    - Economy
    - Transport
    - air
    - Roads
    - Rail
    - Infrastructure
    - Demographics
    - Languages
    - Ethnicity
    - religion
    - culture
    - Cuisine
    - Media
    - Education
    - Bibliography
categories:
    - Uncategorized
---
Sikkim (/ˈsɪkᵻm/) is a landlocked state of India, and the last to give up its monarchy and fully integrate into India in 1975. Located in the Himalayan mountains, the state is bordered by Nepal to the west, China's Tibet Autonomous Region to the north and northeast, and Bhutan to the east and the Indian state of West Bengal to the south.[5]
