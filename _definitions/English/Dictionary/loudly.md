---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loudly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484436661
title: loudly
categories:
    - Dictionary
---
loudly
     adv 1: with relatively high volume; "the band played loudly"; "she
            spoke loudly and angrily"; "he spoke loud enough for
            those at the back of the room to hear him"; "cried
            aloud for help" [syn: {loud}, {aloud}] [ant: {softly}]
     2: in manner that attracts attention; "obstreperously, he
        demanded to get service" [syn: {obstreperously}, {clamorously}]
     3: used as a direction in music; to be played relatively loudly
        [syn: {forte}] [ant: {piano}]
