---
version: 1
type: article
id: https://en.wikipedia.org/wiki/IP_set
offline_file: ""
offline_thumbnail: ""
uuid: 63f1f610-c50d-4f3a-b069-5a61d53c7cc6
updated: 1484308780
title: IP set
tags:
    - "Hindman's theorem"
    - Semigroups
categories:
    - Ergodic theory
---
The finite sums of a set D of natural numbers are all those numbers that can be obtained by adding up the elements of some finite nonempty subset of D. The set of all finite sums over D is often denoted as FS(D).
