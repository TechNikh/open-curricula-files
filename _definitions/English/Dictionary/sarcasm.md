---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sarcasm
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484545081
title: sarcasm
categories:
    - Dictionary
---
sarcasm
     n : witty language used to convey insults or scorn; "he used
         sarcasm to upset his opponent"; "irony is wasted on the
         stupid"; "Satire is a sort of glass, wherein beholders do
         generally discover everybody's face but their
         own"--Johathan Swift [syn: {irony}, {satire}, {caustic
         remark}]
