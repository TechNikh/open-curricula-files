---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/search
offline_file: ""
offline_thumbnail: ""
uuid: 2b5b3ff6-6572-40de-be55-e75e85e2378e
updated: 1484310455
title: search
categories:
    - Dictionary
---
search
     n 1: the activity of looking thoroughly in order to find
          something or someone [syn: {hunt}, {hunting}]
     2: an investigation seeking answers; "a thorough search of the
        ledgers revealed nothing"; "the outcome justified the
        search"
     3: an operation that determines whether one or more of a set of
        items has a specified property; "they wrote a program to
        do a table lookup" [syn: {lookup}]
     4: the examination of alternative hypotheses; "his search for a
        move that would avoid checkmate was unsuccessful"
     5: boarding and inspecting a ship on the high seas; "right of
        search"
     v 1: try to locate or discover, or ...
