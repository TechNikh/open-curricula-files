---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drown
offline_file: ""
offline_thumbnail: ""
uuid: 944765a3-556a-452d-82cd-7eec4adda588
updated: 1484310543
title: drown
categories:
    - Dictionary
---
drown
     v 1: cover completely or make imperceptible; "I was drowned in
          work"; "The noise drowned out her speech" [syn: {submerge},
           {overwhelm}]
     2: get rid of as if by submerging; "She drowned her trouble in
        alcohol"
     3: die from being submerged in water, getting water into the
        lungs, and asphyxiating; "The child drowned in the lake"
     4: kill by submerging in water; "He drowned the kittens"
