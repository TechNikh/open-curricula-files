---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chlorophyll
offline_file: ""
offline_thumbnail: ""
uuid: 581ee2e3-619a-4eb0-aedf-7d588ba322a2
updated: 1484310373
title: chlorophyll
categories:
    - Dictionary
---
chlorophyll
     n : any of a group of green pigments found in photosynthetic
         organisms [syn: {chlorophyl}]
