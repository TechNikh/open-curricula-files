---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/output
offline_file: ""
offline_thumbnail: ""
uuid: 76e8bb99-ab0b-48ff-8bfb-ee68c77d4660
updated: 1484310455
title: output
categories:
    - Dictionary
---
output
     n 1: final product; the things produced [syn: {end product}]
     2: production of a certain amount [syn: {yield}]
     3: signal that comes out of an electronic system [syn: {output
        signal}]
     4: the quantity of something (as a commodity) that is created
        (usually within a given period of time); "production was
        up in the second quarter" [syn: {yield}, {production}]
     5: what is produced in a given time period [syn: {outturn}, {turnout}]
     v : to create or manufacture a specific amount; "the computer is
         outputting the data from the job I'm running"
     [also: {outputting}, {outputted}]
