---
version: 1
type: video
provider: YouTube
id: T-aCweuimis
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Parallel%20Lines.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cbcf2764-b9c8-4abb-b400-8f0960f96d57
updated: 1486069563
title: Parallel Lines
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/T-aCweuimis/default.jpg
    - https://i3.ytimg.com/vi/T-aCweuimis/1.jpg
    - https://i3.ytimg.com/vi/T-aCweuimis/2.jpg
    - https://i3.ytimg.com/vi/T-aCweuimis/3.jpg
---
