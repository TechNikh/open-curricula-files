---
version: 1
type: video
provider: YouTube
id: u1DpLXlLDO8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d435b01a-d869-4103-8925-2cd62e4f5cba
updated: 1486069641
title: 'VJ demo 006 - ambient visualization'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/u1DpLXlLDO8/default.jpg
    - https://i3.ytimg.com/vi/u1DpLXlLDO8/1.jpg
    - https://i3.ytimg.com/vi/u1DpLXlLDO8/2.jpg
    - https://i3.ytimg.com/vi/u1DpLXlLDO8/3.jpg
---
