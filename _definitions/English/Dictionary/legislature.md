---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/legislature
offline_file: ""
offline_thumbnail: ""
uuid: 4934cd0d-f990-4646-9c79-ab3b9ffaa043
updated: 1484310593
title: legislature
categories:
    - Dictionary
---
legislature
     n : persons who make or amend or repeal laws [syn: {legislative
         assembly}, {general assembly}, {law-makers}]
