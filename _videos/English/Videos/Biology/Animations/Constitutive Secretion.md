---
version: 1
type: video
provider: YouTube
id: MrHULUxAsGg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Constitutive%20Secretion.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 04c37e71-eacf-40ba-90e1-969d34594be4
updated: 1486069669
title: Constitutive Secretion
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/MrHULUxAsGg/default.jpg
    - https://i3.ytimg.com/vi/MrHULUxAsGg/1.jpg
    - https://i3.ytimg.com/vi/MrHULUxAsGg/2.jpg
    - https://i3.ytimg.com/vi/MrHULUxAsGg/3.jpg
---
