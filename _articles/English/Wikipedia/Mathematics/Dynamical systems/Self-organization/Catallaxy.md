---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Catallaxy
offline_file: ""
offline_thumbnail: ""
uuid: a147ecd3-9ca8-43aa-a12d-34cbb3129ae6
updated: 1484308822
title: Catallaxy
categories:
    - Self-organization
---
Catallaxy or catallactics is an alternative expression for the word "economy". Whereas the word economy suggests that people in a community possess a common and congruent set of values and goals, catallaxy suggests that the emergent properties of a market (prices, division of labor, growth, etc.) are the outgrowths of the diverse and disparate goals of the individuals in a community.
