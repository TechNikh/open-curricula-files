---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discriminate
offline_file: ""
offline_thumbnail: ""
uuid: 0662a3b4-4777-42f2-81b6-dfebfd09e351
updated: 1484310307
title: discriminate
categories:
    - Dictionary
---
discriminate
     adj 1: marked by the ability to see or make fine distinctions;
            "discriminate judgments"; "discriminate people" [syn:
            {discriminating}] [ant: {indiscriminate}]
     2: noting distinctions with nicety; "a discriminating interior
        designer"; "a nice sense of color"; "a nice point in the
        argument" [syn: {nice}]
     v 1: recognize or perceive the difference [syn: {know apart}]
     2: treat differently on the basis of sex or race [syn: {separate},
         {single out}]
     3: distinguish; "I could not discriminate the different tastes
        in this complicated dish"
