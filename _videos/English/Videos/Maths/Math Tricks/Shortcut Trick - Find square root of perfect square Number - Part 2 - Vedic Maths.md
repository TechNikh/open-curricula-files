---
version: 1
type: video
provider: YouTube
id: uJ6vrnTEcy0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d33c03b6-0589-4324-bbdb-5064490e5991
updated: 1486069621
title: 'Shortcut Trick - Find square root of perfect square Number - Part 2 - Vedic Maths'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/uJ6vrnTEcy0/default.jpg
    - https://i3.ytimg.com/vi/uJ6vrnTEcy0/1.jpg
    - https://i3.ytimg.com/vi/uJ6vrnTEcy0/2.jpg
    - https://i3.ytimg.com/vi/uJ6vrnTEcy0/3.jpg
---
