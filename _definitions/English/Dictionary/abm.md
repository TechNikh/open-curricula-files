---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abm
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484348881
title: abm
categories:
    - Dictionary
---
ABM
     n : a defensive missile designed to shoot down incoming
         intercontinental ballistic missiles; "the Strategic Arms
         Limitation Talks placed limits on the deployment of ABMs"
         [syn: {antiballistic missile}]
