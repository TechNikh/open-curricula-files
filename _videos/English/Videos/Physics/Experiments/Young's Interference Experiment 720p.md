---
version: 1
type: video
provider: YouTube
id: acMo3JaR2Ao
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 09f69142-bd85-41e3-a9e8-0ed49685f4c7
updated: 1486069635
title: "Young's Interference Experiment 720p"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/acMo3JaR2Ao/default.jpg
    - https://i3.ytimg.com/vi/acMo3JaR2Ao/1.jpg
    - https://i3.ytimg.com/vi/acMo3JaR2Ao/2.jpg
    - https://i3.ytimg.com/vi/acMo3JaR2Ao/3.jpg
---
