---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remember
offline_file: ""
offline_thumbnail: ""
uuid: 5502387f-c984-43a7-8b80-56c1987afa2f
updated: 1484310351
title: remember
categories:
    - Dictionary
---
remember
     v 1: recall knowledge from memory; have a recollection; "I can't
          remember saying any such thing"; "I can't think what her
          last name was"; "can you remember her phone number?";
          "Do you remember that he once loved you?"; "call up
          memories" [syn: {retrieve}, {recall}, {call back}, {call
          up}, {recollect}, {think}] [ant: {forget}]
     2: keep in mind for attention or consideration; "Remember the
        Alamo"; "Remember to call your mother every day!"; "Think
        of the starving children in India!" [syn: {think of}]
        [ant: {forget}]
     3: recapture the past; indulge in memories; "he remembered how
        he used to ...
