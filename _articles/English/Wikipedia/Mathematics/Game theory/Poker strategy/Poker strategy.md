---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Poker_strategy
offline_file: ""
offline_thumbnail: ""
uuid: 78f3aa43-2fd2-46d6-9cfc-4862629de085
updated: 1484309059
title: Poker strategy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-DavidSklansky.jpg
tags:
    - The fundamental theorem of poker
    - Pot odds, implied odds and poker probabilities
    - Deception
    - Position
    - Reasons to raise
    - Reasons to call
    - Gap concept
    - Sandwich effect
    - Loose/tight play
    - Aggressive/passive play
    - Hand reading, tells and leveling
    - Table image and opponent profiling
    - Equity
    - Short-handed considerations
    - Structure considerations
    - Mindset considerations
    - Poker plays
    - Specific games
    - Notes
categories:
    - Poker strategy
---
Poker is a popular card game that combines elements of chance and strategy. There are various styles of poker, all of which share an objective of presenting the least probable or highest-scoring hand. A poker hand is a configuration of five cards, either held entirely by a player or drawn partly from a number of shared, community cards. Players bet on their hands in a number of rounds as cards are drawn, employing various mathematical and intuitive strategies in an attempt to better opponents.
