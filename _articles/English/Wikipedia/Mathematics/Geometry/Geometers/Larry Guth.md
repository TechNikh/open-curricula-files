---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Larry_Guth
offline_file: ""
offline_thumbnail: ""
uuid: 7e0f7370-74e0-4ef9-91b9-9b33b2dcd7d4
updated: 1484309083
title: Larry Guth
categories:
    - Geometers
---
Lawrence David Guth is a professor of mathematics at the Massachusetts Institute of Technology.[1] He has previously worked at [2] the New York University's Courant Institute of Mathematical Sciences and at the University of Toronto.
