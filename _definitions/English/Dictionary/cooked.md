---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cooked
offline_file: ""
offline_thumbnail: ""
uuid: e39b6a33-865a-40ea-902e-1c0635f33730
updated: 1484310535
title: cooked
categories:
    - Dictionary
---
cooked
     adj : having been prepared for eating by the application of heat
           [ant: {raw}]
