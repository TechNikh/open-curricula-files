---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accession
offline_file: ""
offline_thumbnail: ""
uuid: 4f7bb08e-6241-4595-97b2-8c6fd75474ee
updated: 1484310591
title: accession
categories:
    - Dictionary
---
accession
     n 1: a process of increasing by addition (as to a collection or
          group); "the art collectin grew through accession"
     2: (civil law) the right to all of that which your property
        produces whether by growth or improvement
     3: something added to what you already have; "the librarian
        shelved the new accessions"; "he was a new addition to the
        staff" [syn: {addition}]
     4: agreeing with or consenting to (often unwillingly);
        "accession to such demands would set a dangerous
        precedent"; "assenting to the Congressional determination"
        [syn: {assenting}]
     5: the right to enter [syn: {entree}, {access}, {admittance}]
  ...
