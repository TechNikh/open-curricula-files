---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Foot_print
offline_file: ""
offline_thumbnail: ""
uuid: 4448c0f8-2fad-4c34-ba04-997375cf917e
updated: 1484308377
title: Footprint
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Apollo_11_bootprint.jpg
tags:
    - Footprints in detective work
    - Ridge patterns
    - Ancient footprints
    - Other footprint findings
    - Footprints in myth and legend
    - Footprints in popular culture
categories:
    - Uncategorized
---
Footprints (or footmarks) are the impressions or images left behind by a person walking or running. Hoofprints and pawprints are those left by animals with hooves or paws rather than feet, while "shoeprints" is the specific term for prints made by shoes. They may either be indentations in the ground or something placed onto the surface that was stuck to the bottom of the foot. A "trackway" is set of footprints in soft earth left by a life-form; animal tracks are the footprints, hoofprints, or pawprints of an animal.
