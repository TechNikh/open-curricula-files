---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Market_game
offline_file: ""
offline_thumbnail: ""
uuid: e95d8efd-d4fb-4cc4-9a88-6c8e7ef847a3
updated: 1484309038
title: Market game
categories:
    - Game theory game classes
---
In economic theory, a market game is a game explaining price formation through game theory, typically implementing a general equilibrium outcome as a Nash equilibrium.
