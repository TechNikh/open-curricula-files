---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Confronted_animals
offline_file: ""
offline_thumbnail: ""
uuid: 304a6903-1032-4ea8-b38e-8e6dbe5d866d
updated: 1484309309
title: Confronted animals
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Gebel_el-Arak_knife_mp3h8791.jpg
tags:
    - Gebel el-Arak Knife
    - Examples from archaeology
    - Cylinder seals
    - Confronted snakes
    - Narmer Palette
    - Mycenae Lion Gate
    - "Ancient Greek herald's staffs (kerykeia)"
    - Etruscan tomb mural
    - European art
    - Asian art
    - Luristan bronzes, Anatolian "animal" carpets
    - Native North American Art
categories:
    - Symmetry
---
Confronted animals, or confronted-animal as an adjective, where two animals face each other in a symmetrical pose, is an ancient bilateral motif in art and artifacts studied in archaeology and art history. The "anti-confronted animals" is the opposing motif, with the animals back to back.
