---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homomorphic_equivalence
offline_file: ""
offline_thumbnail: ""
uuid: a00ef069-308e-471a-a3a7-781115696ce1
updated: 1484308619
title: Homomorphic equivalence
categories:
    - Graph theory
---
In the mathematics of graph theory, two graphs, G and H, are called homomorphically equivalent if there exists a graph homomorphism 
  
    
      
        f
        :
        G
        →
        H
      
    
    {\displaystyle f\colon G\to H}
  
 and a graph homomorphism 
  
    
      
        g
        :
        H
        →
        G
      
    
    {\displaystyle g\colon H\to G}
  
. An example usage of this notion is that any two cores of a graph are homomorphically equivalent.
