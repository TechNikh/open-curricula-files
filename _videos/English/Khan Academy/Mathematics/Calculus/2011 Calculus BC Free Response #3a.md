---
version: 1
type: video
provider: YouTube
id: _G9JwTwjulA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20BC%20Free%20Response%20%233a.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 31e88e00-b7b7-495d-9860-40dff1b6bcd8
updated: 1486069690
title: '2011 Calculus BC Free Response #3a'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/_G9JwTwjulA/default.jpg
    - https://i3.ytimg.com/vi/_G9JwTwjulA/1.jpg
    - https://i3.ytimg.com/vi/_G9JwTwjulA/2.jpg
    - https://i3.ytimg.com/vi/_G9JwTwjulA/3.jpg
---
