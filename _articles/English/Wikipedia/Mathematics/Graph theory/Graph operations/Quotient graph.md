---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quotient_graph
offline_file: ""
offline_thumbnail: ""
uuid: b2553170-202f-473f-b781-9e1628f1cbc3
updated: 1484309448
title: Quotient graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Graph_Condensation.svg.png
categories:
    - Graph operations
---
In graph theory, a quotient graph Q of a graph G is a graph whose vertices are blocks of a partition of the vertices of G and where block B is adjacent to block C if some vertex in B is adjacent to some vertex in C with respect to the edge set of G.[1] In other words, if G has edge set E and vertex set V and R is the equivalence relation induced by the partition, then the quotient graph has vertex set V/R and edge set {([u]R, [v]R) | (u, v) ∈ E(G)}.
