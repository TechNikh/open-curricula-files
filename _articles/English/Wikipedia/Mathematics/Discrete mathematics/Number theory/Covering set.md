---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Covering_set
offline_file: ""
offline_thumbnail: ""
uuid: 081e8851-37b3-46d8-ac2c-5160c8590818
updated: 1484308636
title: Covering set
tags:
    - Sierpinski and Riesel numbers
    - Other covering sets
    - Notes
categories:
    - Number theory
---
In mathematics, a covering set for a sequence of integers refers to a set of prime numbers such that every term in the sequence is divisible by at least one member of the set.[1] The term "covering set" is used only in conjunction with sequences possessing exponential growth.
