---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kodaira%E2%80%93Spencer_map'
offline_file: ""
offline_thumbnail: ""
uuid: f076c589-da63-4e5a-a99d-4479d59287f7
updated: 1484309156
title: Kodaira–Spencer map
categories:
    - Algebraic geometry
---
In mathematics, the Kodaira–Spencer map, introduced by Kunihiko Kodaira and Donald C. Spencer, is a map associated to a deformation of a scheme or complex manifold X, taking a tangent space of a point of the deformation space to the first cohomology group of the sheaf of vector fields on X.
