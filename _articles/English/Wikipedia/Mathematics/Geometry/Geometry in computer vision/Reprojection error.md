---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reprojection_error
offline_file: ""
offline_thumbnail: ""
uuid: 520a2f2e-4f33-4a3a-8398-6746093a5e15
updated: 1484309181
title: Reprojection error
categories:
    - Geometry in computer vision
---
The reprojection error is a geometric error corresponding to the image distance between a projected point and a measured one. It is used to quantify how closely an estimate of a 3D point 
  
    
      
        
          
            
              
                X
              
              ^
            
          
        
      
    
    {\displaystyle {\hat {\mathbf {X} }}}
  
 recreates the point's true projection 
  
    
      
        
          x
        
      
    
    {\displaystyle \mathbf {x} }
  
. More precisely, let 
  
    
      
        
          P
        
      
    
    {\displaystyle \mathbf {P} }
  
 be the projection matrix of a camera and 
  
    
      
   ...
