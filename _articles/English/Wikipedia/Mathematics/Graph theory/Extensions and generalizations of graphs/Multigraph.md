---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multigraph
offline_file: ""
offline_thumbnail: ""
uuid: bd0d5cca-0256-4894-8583-0f6ad23db1ce
updated: 1484309406
title: Multigraph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Multi-pseudograph.svg.png
tags:
    - Undirected multigraph (edges without own identity)
    - Undirected multigraph (edges with own identity)
    - Directed multigraph (edges without own identity)
    - Directed multigraph (edges with own identity)
    - Labeling
    - Notes
categories:
    - Extensions and generalizations of graphs
---
In mathematics, and more specifically in graph theory, a multigraph is a graph which is permitted to have multiple edges (also called parallel edges[1]), that is, edges that have the same end nodes. Thus two vertices may be connected by more than one edge.
