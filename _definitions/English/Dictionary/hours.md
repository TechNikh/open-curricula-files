---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hours
offline_file: ""
offline_thumbnail: ""
uuid: 92cbf6fd-e392-42c6-9388-e44ab3b1d6b2
updated: 1484310325
title: hours
categories:
    - Dictionary
---
hours
     n 1: a period of time assigned for work; "they work long hours"
     2: an indefinite period of time; "they talked for hours"
