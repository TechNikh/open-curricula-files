---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/signal
offline_file: ""
offline_thumbnail: ""
uuid: d857abbc-7c8e-45ee-8915-2be45eb3acfe
updated: 1484310426
title: signal
categories:
    - Dictionary
---
signal
     adj : notably out of the ordinary; "the year saw one signal
           triumph for the Labour party"
     n 1: any communication that encodes a message; "signals from the
          boat suddenly stopped" [syn: {signaling}, {sign}]
     2: any incitement to action; "he awaited the signal to start";
        "the victory was a signal for wild celebration"
     3: an electric quantity (voltage or current or field strength)
        whose modulation represents coded information about the
        source from which it comes
     v 1: communicate silently and non-verbally by signals or signs;
          "He signed his disapproval with a dismissive hand
          gesture"; "The diner ...
