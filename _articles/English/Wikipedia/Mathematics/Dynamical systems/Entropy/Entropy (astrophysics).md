---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entropy_(astrophysics)
offline_file: ""
offline_thumbnail: ""
uuid: 902216cd-d04a-42b8-be85-a11eb7a12cf4
updated: 1484308762
title: Entropy (astrophysics)
categories:
    - Entropy
---
Using the first law of thermodynamics for a quasi-static, infinitesimal process for a hydrostatic system
