---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/culture
offline_file: ""
offline_thumbnail: ""
uuid: bfe89065-3dca-4961-aba2-c7929a8e7860
updated: 1484310484
title: culture
categories:
    - Dictionary
---
culture
     n 1: a particular society at a particular time and place; "early
          Mayan civilization" [syn: {civilization}, {civilisation}]
     2: the tastes in art and manners that are favored by a social
        group
     3: all the knowledge and values shared by a society [syn: {acculturation}]
     4: (biology) the growing of microorganisms in a nutrient medium
        (such as gelatin or agar); "the culture of cells in a
        Petri dish"
     5: (bacteriology) the product of cultivating micro-organisms in
        a nutrient medium
     6: a highly developed state of perfection; having a flawless or
        impeccable quality; "they performed with great polish"; "I
        ...
