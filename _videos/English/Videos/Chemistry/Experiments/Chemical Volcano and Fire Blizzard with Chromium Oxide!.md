---
version: 1
type: video
provider: YouTube
id: 12Z_VeDtnuM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3362715e-adb9-4fbf-9149-e9aeb95ca99e
updated: 1486069661
title: Chemical Volcano and Fire Blizzard with Chromium Oxide!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/12Z_VeDtnuM/default.jpg
    - https://i3.ytimg.com/vi/12Z_VeDtnuM/1.jpg
    - https://i3.ytimg.com/vi/12Z_VeDtnuM/2.jpg
    - https://i3.ytimg.com/vi/12Z_VeDtnuM/3.jpg
---
