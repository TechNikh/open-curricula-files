---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wooden
offline_file: ""
offline_thumbnail: ""
uuid: f7347ad6-0698-4513-98e7-3cff517c5864
updated: 1484310234
title: wooden
categories:
    - Dictionary
---
wooden
     adj 1: made or consisting of (entirely or in part) or employing
            wood; "a wooden box"; "an ancient cart with wooden
            wheels"; "wood houses"; "a wood fire"
     2: lacking ease or grace; "the actor's performance was wooden";
        "a wooden smile"
