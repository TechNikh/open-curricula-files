---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/several
offline_file: ""
offline_thumbnail: ""
uuid: 336761c5-3a6f-488f-98c2-6d42636d3958
updated: 1484310344
title: several
categories:
    - Dictionary
---
several
     adj 1: considered individually; "the respective club members";
            "specialists in their several fields"; "the various
            reports all agreed" [syn: {respective(a)}, {several(a)},
             {various(a)}]
     2: distinct and individual; "three several times" [syn: {several(p)}]
     3: (used with count nouns) of an indefinite number more than 2
        or 3 but not many; "several letters came in the mail";
        "several people were injured in the accident" [syn: {several(a)}]
