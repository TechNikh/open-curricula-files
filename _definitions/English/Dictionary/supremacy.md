---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supremacy
offline_file: ""
offline_thumbnail: ""
uuid: c90fd305-28fa-49af-acec-52f1aa3816ee
updated: 1484310565
title: supremacy
categories:
    - Dictionary
---
supremacy
     n : power to dominate or defeat; "mastery of the seas" [syn: {domination},
          {mastery}]
