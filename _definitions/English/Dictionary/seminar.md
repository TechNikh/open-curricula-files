---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seminar
offline_file: ""
offline_thumbnail: ""
uuid: b0cb8650-cf1d-4baf-89d5-b79dd04ee6ba
updated: 1484310242
title: seminar
categories:
    - Dictionary
---
seminar
     n 1: any meeting for an exchange of ideas
     2: a course offered for a small group of advanced students
