---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Russians
offline_file: ""
offline_thumbnail: ""
uuid: 0223adac-612d-4fed-9a79-35307c378ed6
updated: 1484308476
title: Russians
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-EthnicRussiansInTheFormerUSSR.png
tags:
    - Ethnonym
    - History
    - Origin
    - "Kievan Rus'"
    - population
    - Russia
    - Former Soviet states
    - Diaspora
    - culture
    - language
categories:
    - Uncategorized
---
Russians (Russian: русские, russkiye) are an East Slavic ethnic group native to Eastern Europe. The majority of Russians inhabit the nation state of Russia, while notable minorities exist in Ukraine, Kazakhstan, and other former Soviet states. A large Russian diaspora exists all over the world, with notable numbers in the United States, Germany, Israel, and Canada. Russians are the most numerous ethnic group in Europe.
