---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ghee
offline_file: ""
offline_thumbnail: ""
uuid: ff5927a5-5970-4e01-8fd5-6ef279943523
updated: 1484308327
title: Ghee
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Butterschmalz-3.jpg
tags:
    - Description
    - In Hinduism
    - Culinary uses
    - Clarified butter vs. ghee
    - Traditional medicine
    - Nutrition
    - Outside the Indian subcontinent
    - Etymology and other names
    - Market
categories:
    - Uncategorized
---
Ghee is a class of clarified butter that originated in ancient India and is commonly used in South Asian, Iranian and Arabic cuisines, traditional medicine, and religious rituals.
