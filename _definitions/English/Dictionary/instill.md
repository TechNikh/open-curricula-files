---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instill
offline_file: ""
offline_thumbnail: ""
uuid: 2aff9958-4bf2-49e9-84ea-f4f3f0606e45
updated: 1484310565
title: instill
categories:
    - Dictionary
---
instill
     v 1: impart gradually; "Her presence instilled faith into the
          children"; "transfuse love of music into the students"
          [syn: {transfuse}]
     2: enter drop by drop; "instill medication into my eye" [syn: {instil}]
     3: produce or try to produce a vivid impression of; "Mother
        tried to ingrain respect for our elders in us" [syn: {impress},
         {ingrain}]
     4: teach and impress by frequent repetitions or admonitions;
        "inculcate values into the young generation" [syn: {inculcate},
         {infuse}]
     5: fill, as with a certain quality; "The heavy traffic
        tinctures the air with carbon monoxide" [syn: {impregnate},
         ...
