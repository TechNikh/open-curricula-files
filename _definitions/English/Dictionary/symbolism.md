---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/symbolism
offline_file: ""
offline_thumbnail: ""
uuid: d95d6c10-cc08-436c-88cc-5c698b45281b
updated: 1484310174
title: symbolism
categories:
    - Dictionary
---
symbolism
     n 1: a system of symbols and symbolic representations
     2: the practice of investing things with symbolic meaning [syn:
         {symbolization}, {symbolisation}]
     3: an artistic movement in the late 19th century that tried to
        express abstract or mystical ideas through the symbolic
        use of images
