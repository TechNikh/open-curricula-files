---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chore_division
offline_file: ""
offline_thumbnail: ""
uuid: 9f4f9faf-3efc-4271-aa56-26fe582ace9b
updated: 1484308524
title: Chore division
tags:
    - Proportional chore-cutting
    - Equitable and exact chore-cutting
    - Envy-free chore-cutting
    - "Oskui's discrete procedure for three partners"
    - "Patterson and Su's continuous procedures for three and four partners"
    - Price-of-fairness
    - Applications
categories:
    - Fair division
---
Chore division is a fair division problem in which the divided resource is undesirable, so that each participant wants to get as little as possible. It is the mirror-image of the fair cake-cutting problem, in which the divided resource is desirable so that each participant wants to get as much as possible. Both problems have heterogeneous resources, meaning that the resources are nonuniform. In cake division, cakes can have edge, corner, and middle pieces along with different amounts of frosting. Whereas in chore division, there are different chore types and different amounts of time needed to finish each chore. Similarly, both problems assume that the resources are divisible. Chores can be ...
