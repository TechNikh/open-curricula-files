---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enlarged
offline_file: ""
offline_thumbnail: ""
uuid: c02c19cb-8226-4f7f-85ff-809760b9ba6b
updated: 1484310218
title: enlarged
categories:
    - Dictionary
---
enlarged
     adj 1: expanded in scope; "the enlarged authority of the committee"
     2: (of an organ or body part) excessively enlarged as a result
        of increased size in the constituent cells; "hypertrophied
        myocardial fibers" [syn: {hypertrophied}] [ant: {atrophied}]
     3: as of a photograph; made larger; "the enlarged photograph
        revealed many details" [syn: {blown-up}]
     4: larger than normal; "enlarged joints"
     5: enlarged to an abnormal degree; "thick lenses exaggerated
        the size of her eyes" [syn: {exaggerated}, {magnified}]
