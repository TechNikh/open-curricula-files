---
version: 1
type: video
provider: YouTube
id: 7NZhmOIyYQM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Lecture%20-%209%20Electromagnetic%20waves%20-%20I.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6471b512-b84a-4da0-94ae-5cd2e159f8a8
updated: 1486069652
title: 'Lecture - 9 Electromagnetic waves - I'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/7NZhmOIyYQM/default.jpg
    - https://i3.ytimg.com/vi/7NZhmOIyYQM/1.jpg
    - https://i3.ytimg.com/vi/7NZhmOIyYQM/2.jpg
    - https://i3.ytimg.com/vi/7NZhmOIyYQM/3.jpg
---
