---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transpiration
offline_file: ""
offline_thumbnail: ""
uuid: 74e4d9ef-b3a6-41f6-bda7-574d77174b45
updated: 1484310462
title: transpiration
categories:
    - Dictionary
---
transpiration
     n 1: the passage of gases through fine tubes because of
          differences in pressure or temperature
     2: the process of givng off or exhaling water vapor through the
        skin or mucous membranes
     3: the emission of water vapor from the leaves of plants
