---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_coordinate_system
offline_file: ""
offline_thumbnail: ""
uuid: 56eb979d-0314-4640-931f-be376ebaa043
updated: 1484309352
title: Affine coordinate system
categories:
    - Geometry stubs
---
In mathematics, an affine coordinate system is a coordinate system on an affine space where each coordinate is an affine map to the number line. In other words, it is an injective affine map from an affine space A to the coordinate space Kn, where K is the field of scalars, for example, the real numbers R.
