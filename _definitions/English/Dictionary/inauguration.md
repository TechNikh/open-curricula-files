---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inauguration
offline_file: ""
offline_thumbnail: ""
uuid: 79cf8141-5ed2-4208-b654-ae29f14d8697
updated: 1484310603
title: inauguration
categories:
    - Dictionary
---
inauguration
     n 1: the act of starting a new operation or practice; "he opposed
          the inauguration of fluoridation"; "the startup of the
          new factory was delayed by strikes" [syn: {startup}]
     2: the ceremonial induction into a position; "the new president
        obviously enjoyed his inauguration" [syn: {inaugural}]
