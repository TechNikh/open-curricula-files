---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/begin
offline_file: ""
offline_thumbnail: ""
uuid: e98e91b5-75a4-4e9c-887d-bb03fb5936cc
updated: 1484310333
title: begin
categories:
    - Dictionary
---
Begin
     n : Israeli statesman (born in Russia) who (as prime minister of
         Israel) negotiated a peace treaty with Anwar Sadat (then
         the president of Egypt) (1913-1992) [syn: {Menachem Begin}]
     v 1: take the first step or steps in carrying out an action; "We
          began working at dawn"; "Who will start?"; "Get working
          as soon as the sun rises!"; "The first tourists began to
          arrive in Cambodia"; "He began early in the day"; "Let's
          get down to work now" [syn: {get down}, {get}, {start
          out}, {start}, {set about}, {set out}, {commence}] [ant:
           {end}]
     2: have a beginning, in a temporal, spatial, or evaluative
      ...
