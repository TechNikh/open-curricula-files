---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/protected
offline_file: ""
offline_thumbnail: ""
uuid: de4b5f8e-7980-40ed-b961-9858d6bd9645
updated: 1484310330
title: protected
categories:
    - Dictionary
---
protected
     adj 1: kept safe or defended from danger or injury or loss; "the
            most protected spot I could find" [syn: {secure}]
            [ant: {unprotected}]
     2: guarded from injury or destruction [syn: {saved}]
