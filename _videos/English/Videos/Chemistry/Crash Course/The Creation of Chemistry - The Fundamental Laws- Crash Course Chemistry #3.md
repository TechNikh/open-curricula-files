---
version: 1
type: video
provider: YouTube
id: QiiyvzZBKT8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/The%20Creation%20of%20Chemistry%20-%20The%20Fundamental%20Laws-%20Crash%20Course%20Chemistry%20%233.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4c5eb77b-8985-4294-9928-1132f571c345
updated: 1486069678
title: 'The Creation of Chemistry - The Fundamental Laws- Crash Course Chemistry #3'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/QiiyvzZBKT8/default.jpg
    - https://i3.ytimg.com/vi/QiiyvzZBKT8/1.jpg
    - https://i3.ytimg.com/vi/QiiyvzZBKT8/2.jpg
    - https://i3.ytimg.com/vi/QiiyvzZBKT8/3.jpg
---
