---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Levi-Civita_parallelogramoid
offline_file: ""
offline_thumbnail: ""
uuid: 3e298739-fa5b-4acd-98c3-ded6e30596a5
updated: 1484309226
title: Levi-Civita parallelogramoid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Parallelogramoid.svg.png
tags:
    - Construction
    - Quantifying the difference from a parallelogram
    - Discrete approximation
categories:
    - Differential geometry
---
In the mathematical field of differential geometry, the Levi-Civita parallelogramoid is a quadrilateral in a curved space whose construction generalizes that of a parallelogram in the Euclidean plane. It is named for its discoverer, Tullio Levi-Civita. Like a parallelogram, two opposite sides AA′ and BB′ of a parallelogramoid are parallel (via parallel transport along side AB) and the same length as each other, but the fourth side A′B′ will not in general be parallel to or the same length as the side AB, although it will be straight (a geodesic).
