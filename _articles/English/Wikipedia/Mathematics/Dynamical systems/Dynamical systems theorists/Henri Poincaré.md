---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Henri_Poincar%C3%A9'
offline_file: ""
offline_thumbnail: ""
uuid: 14248a0f-b74a-438d-beb1-2753253ee310
updated: 1484308690
title: Henri Poincaré
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Henri_Poincar%25C3%25A9_maison_natale_Nancy_plaque.jpg'
tags:
    - life
    - Education
    - First scientific achievements
    - Career
    - Students
    - Work
    - Summary
    - Three-body problem
    - Work on relativity
    - Local time
    - Principle of relativity and Lorentz transformations
    - Mass–energy relation
    - Poincaré and Einstein
    - Algebra and number theory
    - Topology
    - Astronomy and celestial mechanics
    - Differential equations and mathematical physics
    - Assessments
    - Character
    - "Toulouse's characterisation"
    - Attitude towards transfinite numbers
    - Honours
    - Philosophy
    - Free will
    - concepts
    - Theorems
    - Other
    - Footnotes and primary sources
    - "Poincaré's writings in English translation"
    - General references
    - Secondary sources to work on relativity
categories:
    - Dynamical systems theorists
---
Jules Henri Poincaré (French: [ʒyl ɑ̃ʁi pwɛ̃kaʁe];[2][3] 29 April 1854 – 17 July 1912) was a French mathematician, theoretical physicist, engineer, and philosopher of science. He is often described as a polymath, and in mathematics as The Last Universalist by Eric Temple Bell,[4] since he excelled in all fields of the discipline as it existed during his lifetime.
