---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cost-efficient
offline_file: ""
offline_thumbnail: ""
uuid: d667fc18-ce8a-4991-ad7f-72a285964bfe
updated: 1484308335
title: Cost efficiency
categories:
    - Uncategorized
---
Cost efficiency (or cost optimality), in the context of parallel computer algorithms, refers to a measure of how effectively parallel computing can be used to solve a particular problem. A parallel algorithm is considered cost efficient if its asymptotic running time multiplied by the number of processing units involved in the computation is comparable to the running time of the best sequential algorithm.
