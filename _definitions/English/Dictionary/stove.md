---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stove
offline_file: ""
offline_thumbnail: ""
uuid: 042a4103-9f20-420a-b53c-9d87eb0c678d
updated: 1484310221
title: stove
categories:
    - Dictionary
---
stave
     n 1: (music) the system of five horizontal lines on which the
          musical notes are written [syn: {staff}]
     2: one of several thin slats of wood forming the sides of a
        barrel or bucket [syn: {lag}]
     3: a crosspiece between the legs of a chair [syn: {rung}, {round}]
     v 1: furnich with staves; "stave a ladder"
     2: burst or force (a hole) into something [syn: {stave in}]
     [also: {stove}]
