---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phase_space_formulation
offline_file: ""
offline_thumbnail: ""
uuid: 9131bb4e-8dc0-4af6-8605-3abe72c8dfbb
updated: 1484308791
title: Phase space formulation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Wigner_functions.jpg
tags:
    - Phase space distribution
    - Star product
    - Time evolution
    - Examples
    - Simple harmonic oscillator
    - Free particle angular momentum
    - Morse potential
    - Quantum tunneling
    - Quartic potential
    - Schrödinger cat state
categories:
    - Hamiltonian mechanics
---
The phase space formulation of quantum mechanics places the position and momentum variables on equal footing, in phase space. In contrast, the Schrödinger picture uses the position or momentum representations (see also position and momentum space). The two key features of the phase space formulation are that the quantum state is described by a quasiprobability distribution (instead of a wave function, state vector, or density matrix) and operator multiplication is replaced by a star product.
