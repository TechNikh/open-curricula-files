---
version: 1
type: video
provider: YouTube
id: ERKx3Oa2omo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Scale%20of%20the%20small.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a1570a32-ab1c-4719-8442-592b39291531
updated: 1486069686
title: Scale of the small
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/ERKx3Oa2omo/default.jpg
    - https://i3.ytimg.com/vi/ERKx3Oa2omo/1.jpg
    - https://i3.ytimg.com/vi/ERKx3Oa2omo/2.jpg
    - https://i3.ytimg.com/vi/ERKx3Oa2omo/3.jpg
---
