---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flotation
offline_file: ""
offline_thumbnail: ""
uuid: 8fd719a9-9835-4b38-9ce9-9bb4f5c13d81
updated: 1484310409
title: flotation
categories:
    - Dictionary
---
flotation
     n 1: the phenomenon of floating (remaining on the surface of a
          liquid without sinking) [syn: {floatation}]
     2: financing a commercial enterprise by bond or stock shares
        [syn: {floatation}]
