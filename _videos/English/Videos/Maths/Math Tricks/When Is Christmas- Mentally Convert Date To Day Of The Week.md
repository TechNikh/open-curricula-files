---
version: 1
type: video
provider: YouTube
id: 5NxrGK9GdAQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 13d3b0a9-2379-4b16-aa3b-080e79c91dcb
updated: 1486069623
title: When Is Christmas? Mentally Convert Date To Day Of The Week
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/5NxrGK9GdAQ/default.jpg
    - https://i3.ytimg.com/vi/5NxrGK9GdAQ/1.jpg
    - https://i3.ytimg.com/vi/5NxrGK9GdAQ/2.jpg
    - https://i3.ytimg.com/vi/5NxrGK9GdAQ/3.jpg
---
