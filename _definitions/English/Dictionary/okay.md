---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/okay
offline_file: ""
offline_thumbnail: ""
uuid: 9c868318-a117-4e90-b4f7-10dd177c9cb3
updated: 1484310166
title: okay
categories:
    - Dictionary
---
okay
     adj : being satisfactory or in satisfactory condition; "an
           all-right movie"; "the passengers were shaken up but
           are all right"; "is everything all right?";
           "everything's fine"; "things are okay"; "dinner and the
           movies had been fine"; "another minute I'd have been
           fine" [syn: {all right}, {fine}, {ok}, {o.k.}, {hunky-dory}]
     n : an endorsement; "they gave us the O.K. to go ahead" [syn: {O.K.},
          {OK}, {okey}, {okeh}]
     adv : in a satisfactory or adequate manner; "she'll do okay on her
           own"; "held up all right under pressure"; (`alright' is
           a nonstandard variant of `all right') [syn: {O.K.}, ...
