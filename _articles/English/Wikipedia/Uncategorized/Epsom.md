---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Epsom
offline_file: ""
offline_thumbnail: ""
uuid: f76bb706-2fd9-4605-9ec7-2856802fd5e2
updated: 1484308389
title: Epsom
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jean_Louis_Th%25C3%25A9odore_G%25C3%25A9ricault_001.jpg'
tags:
    - History
    - Economy and amenities
    - Sport and leisure
    - geography
    - Soil and elevation
    - Nearest settlements
    - Demography and housing
    - Hospitals
    - Localities
    - Horton
    - Langley Vale
    - Transport
    - Rail
    - Bus
    - Road
    - Education
    - Emergency services
    - Notable people
categories:
    - Uncategorized
---
Epsom (/ˈɛpsəm/) is a market town in Surrey, England, 13.6 miles (21.9 km) south south-west of London, located between Ashtead and Ewell. The town straddles chalk downland (Epsom Downs) and the upper Thanet Formation. Epsom Downs Racecourse holds The Derby, now a generic name for sports competitions in English-speaking countries. The town also gives its name to Epsom salts, extracted from mineral waters there.
