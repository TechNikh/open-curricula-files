---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arbitration
offline_file: ""
offline_thumbnail: ""
uuid: f3c6974a-f8f4-4f6b-a28e-2f985c769803
updated: 1484310555
title: arbitration
categories:
    - Dictionary
---
arbitration
     n 1: (law) the hearing and determination of a dispute by an
          impartial referee agreed to by both parties (often used
          to settle disputes between labor and management)
     2: the act of deciding as an arbiter; giving authoritative
        judgment; "they submitted their disagreement to
        arbitration" [syn: {arbitrament}, {arbitrement}]
