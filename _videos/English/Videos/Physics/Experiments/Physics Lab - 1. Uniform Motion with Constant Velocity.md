---
version: 1
type: video
provider: YouTube
id: rYRAgldrYzY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6579a388-fd78-4b82-9924-bd5a8573748c
updated: 1486069640
title: 'Physics Lab - 1. Uniform Motion with Constant Velocity'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/rYRAgldrYzY/default.jpg
    - https://i3.ytimg.com/vi/rYRAgldrYzY/1.jpg
    - https://i3.ytimg.com/vi/rYRAgldrYzY/2.jpg
    - https://i3.ytimg.com/vi/rYRAgldrYzY/3.jpg
---
