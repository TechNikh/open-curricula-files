---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oiled
offline_file: ""
offline_thumbnail: ""
uuid: 0db6dddc-8124-4d4d-bca9-54674f60dccb
updated: 1484310335
title: oiled
categories:
    - Dictionary
---
oiled
     adj : treated with oil; "oiled country roads"; "an oiled walnut
           table" [ant: {unoiled}]
