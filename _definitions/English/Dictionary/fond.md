---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fond
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484451121
title: fond
categories:
    - Dictionary
---
fond
     adj 1: having or displaying warmth or affection; "affectionate
            children"; "caring parents"; "a fond embrace"; "fond
            of his nephew"; "a tender glance"; "a warm embrace"
            [syn: {affectionate}, {caring}, {lovesome}, {tender},
            {warm}]
     2: extravagantly or foolishly loving and indulgent; "adoring
        grandparents"; "deceiving her preoccupied and doting
        husband with a young captain"; "hopelessly spoiled by a
        fond mother" [syn: {adoring}, {doting}]
     3: absurd or silly because unlikely; "fond hopes of becoming
        President"; "fond fancies"
     4: (followed by `of' or `to') having a strong preference or
       ...
