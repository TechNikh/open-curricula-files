---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Voltmeters
offline_file: ""
offline_thumbnail: ""
uuid: 3ca11b0c-9786-4442-bb85-2c530db36a6a
updated: 1484308319
title: Voltmeter
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Voltmeter_hg.jpg
tags:
    - Analog voltmeter
    - VTVMs and FET-VMs
    - Digital voltmeter
categories:
    - Uncategorized
---
A voltmeter is an instrument used for measuring electrical potential difference between two points in an electric circuit. Analog voltmeters move a pointer across a scale in proportion to the voltage of the circuit; digital voltmeters give a numerical display of voltage by use of an analog to digital converter.
