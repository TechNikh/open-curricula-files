---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unfairly
offline_file: ""
offline_thumbnail: ""
uuid: 4382a951-7bc5-4266-8cc7-95738868d8c5
updated: 1484310529
title: unfairly
categories:
    - Dictionary
---
unfairly
     adv : in an unfair manner; "they dealt with him unfairly"; "their
           accusations hit below the belt" [syn: {below the belt}]
           [ant: {fairly}]
