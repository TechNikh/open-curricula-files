---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coat
offline_file: ""
offline_thumbnail: ""
uuid: c32dfdbe-25e7-4373-b67c-eb5b09fbab78
updated: 1484310307
title: coat
categories:
    - Dictionary
---
coat
     n 1: an outer garment that has sleeves and covers the body from
          shoulder down; worn outdoors
     2: a thin layer covering something; "a second coat of paint"
        [syn: {coating}]
     3: growth of hair or wool or fur covering the body of an animal
        [syn: {pelage}]
     v 1: put a coat on; cover the surface of; furnish with a surface;
          "coat the cake with chocolate" [syn: {surface}]
     2: cover or provide with a coat
     3: form a coat over; "Dirt had coated her face" [syn: {cake}]
