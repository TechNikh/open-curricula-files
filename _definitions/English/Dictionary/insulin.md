---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insulin
offline_file: ""
offline_thumbnail: ""
uuid: 0d8b6d2b-12ad-4f2b-81a6-759cfc3893ab
updated: 1484310160
title: insulin
categories:
    - Dictionary
---
insulin
     n : hormone secreted by the isles of Langerhans in the pancreas;
         regulates storage of glycogen in the liver and
         accelerates oxidation of sugar in cells
