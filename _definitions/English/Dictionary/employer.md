---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/employer
offline_file: ""
offline_thumbnail: ""
uuid: 3a8884b9-dc13-4de6-878e-b803f9f96002
updated: 1484310455
title: employer
categories:
    - Dictionary
---
employer
     n : a person or firm that employs workers [ant: {employee}]
