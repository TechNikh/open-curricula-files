---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clark_Kimberling
offline_file: ""
offline_thumbnail: ""
uuid: 9fd311d9-6501-4983-bc5e-91c3d25a2b74
updated: 1484309085
title: Clark Kimberling
categories:
    - Geometers
---
Clark Kimberling (born November 7, 1942, in Hinsdale, Illinois) is a mathematician, musician, and composer. He has been a mathematics professor since 1970 at the University of Evansville. His research interests include triangle centers, integer sequences, and hymnology.
