---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Formally_smooth_map
offline_file: ""
offline_thumbnail: ""
uuid: dc0dab34-e2a7-412d-85d6-846b9119d9af
updated: 1484309147
title: Formally smooth map
categories:
    - Algebraic geometry
---
In algebraic geometry and commutative algebra, a ring homomorphism 
  
    
      
        f
        :
        A
        →
        B
      
    
    {\displaystyle f:A\to B}
  
 is called formally smooth (from French: Formellement lisse) if it satisfies the following infinitesimal lifting property:
