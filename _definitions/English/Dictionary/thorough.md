---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thorough
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484416801
title: thorough
categories:
    - Dictionary
---
thorough
     adj 1: painstakingly careful and accurate; "our accountant is
            thorough"; "thorough research"
     2: very thorough; exhaustively complete; "an exhaustive study";
        "made a thorough search"; "thoroughgoing research" [syn: {exhaustive},
         {thoroughgoing}]
