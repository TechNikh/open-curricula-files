---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/team
offline_file: ""
offline_thumbnail: ""
uuid: 843232ad-e1e1-45d2-80d9-c6c5daa7590a
updated: 1484310414
title: team
categories:
    - Dictionary
---
team
     n 1: a cooperative unit [syn: {squad}]
     2: two or more draft animals that work together to pull
        something
     v : form a team; "We teamed up for this new project" [syn: {team
         up}]
