---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/correctly
offline_file: ""
offline_thumbnail: ""
uuid: 376d02aa-4aed-440f-868f-5d2abb0663ce
updated: 1484310344
title: correctly
categories:
    - Dictionary
---
correctly
     adv : in a correct manner; "he guessed right" [syn: {right}, {aright}]
           [ant: {incorrectly}, {incorrectly}]
