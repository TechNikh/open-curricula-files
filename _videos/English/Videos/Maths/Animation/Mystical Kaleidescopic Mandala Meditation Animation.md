---
version: 1
type: video
provider: YouTube
id: 9GMtj_-3BOc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: af060806-a530-4a98-a776-9de176c4e8a9
updated: 1486069643
title: Mystical Kaleidescopic Mandala Meditation Animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/9GMtj_-3BOc/default.jpg
    - https://i3.ytimg.com/vi/9GMtj_-3BOc/1.jpg
    - https://i3.ytimg.com/vi/9GMtj_-3BOc/2.jpg
    - https://i3.ytimg.com/vi/9GMtj_-3BOc/3.jpg
---
