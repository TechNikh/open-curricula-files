---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transmitting
offline_file: ""
offline_thumbnail: ""
uuid: 46ad1326-bd90-4a7a-a0ce-cf3056b98da5
updated: 1484310387
title: transmitting
categories:
    - Dictionary
---
transmitting
     n : the act of sending a message; causing a message to be
         transmitted [syn: {transmission}, {transmittal}]
