---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/autocrat
offline_file: ""
offline_thumbnail: ""
uuid: ebfec352-9de1-49ef-8f34-43c2ba2f76c9
updated: 1484310557
title: autocrat
categories:
    - Dictionary
---
autocrat
     n : a cruel and oppressive dictator [syn: {tyrant}, {despot}]
