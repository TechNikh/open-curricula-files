---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Local_analysis
offline_file: ""
offline_thumbnail: ""
uuid: d21fcad4-d0bc-4d64-852f-f27cf0c09d0e
updated: 1484308645
title: Local analysis
categories:
    - Number theory
---
In mathematics, the term local analysis has at least two meanings - both derived from the idea of looking at a problem relative to each prime number p first, and then later trying to integrate the information gained at each prime into a 'global' picture. These are forms of the localization approach.
