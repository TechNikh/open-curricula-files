---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fiber
offline_file: ""
offline_thumbnail: ""
uuid: 84f79073-c1f6-4a1b-9092-30c339224835
updated: 1484310210
title: fiber
categories:
    - Dictionary
---
fiber
     n 1: a slender and greatly elongated solid substance [syn: {fibre}]
     2: the inherent complex of attributes that determine a persons
        moral and ethical actions and reactions; "education has
        for its object the formation of character"- Herbert
        Spencer [syn: {character}, {fibre}]
     3: a leatherlike material made by compressing layers of paper
        or cloth [syn: {fibre}, {vulcanized fiber}]
