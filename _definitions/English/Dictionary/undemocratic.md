---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undemocratic
offline_file: ""
offline_thumbnail: ""
uuid: f8db8c2f-eb2e-462e-addf-b01e6db7694f
updated: 1484310559
title: undemocratic
categories:
    - Dictionary
---
undemocratic
     adj : not in agreement with or according to democratic doctrine or
           practice or ideals; "the union broke with its past
           undemocratic procedures" [ant: {democratic}]
