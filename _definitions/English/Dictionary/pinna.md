---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pinna
offline_file: ""
offline_thumbnail: ""
uuid: e3ca645a-5da7-425c-8bca-2ee8ce8f8f9c
updated: 1484310275
title: pinna
categories:
    - Dictionary
---
pinna
     n 1: division of a usually pinnately divided leaf [syn: {pinnule}]
     2: the externally visible cartilaginous structure of the
        external ear [syn: {auricle}, {ear}]
     [also: {pinnae} (pl)]
