---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accident
offline_file: ""
offline_thumbnail: ""
uuid: 42339508-dde2-49b9-a80f-af86b082fc48
updated: 1484310279
title: accident
categories:
    - Dictionary
---
accident
     n 1: a mishap; especially one causing injury or death
     2: anything that happens by chance without an apparent cause
        [syn: {fortuity}, {chance event}]
