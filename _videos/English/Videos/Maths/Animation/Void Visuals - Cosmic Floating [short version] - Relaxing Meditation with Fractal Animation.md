---
version: 1
type: video
provider: YouTube
id: S__x2k64l1I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7f9a2a93-f515-49cd-9ebc-791464788bcf
updated: 1486069643
title: 'Void Visuals - Cosmic Floating [short version] | Relaxing Meditation with Fractal Animation'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/S__x2k64l1I/default.jpg
    - https://i3.ytimg.com/vi/S__x2k64l1I/1.jpg
    - https://i3.ytimg.com/vi/S__x2k64l1I/2.jpg
    - https://i3.ytimg.com/vi/S__x2k64l1I/3.jpg
---
