---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pythagorean_theorem
offline_file: ""
offline_thumbnail: ""
uuid: b8f5b0a5-3494-4296-9aff-57806931ab11
updated: 1484309247
title: Pythagorean theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Pythagorean.svg.png
tags:
    - Pythagorean proof
    - Other forms of the theorem
    - Other proofs of the theorem
    - Proof using similar triangles
    - "Euclid's proof"
    - Proofs by dissection and rearrangement
    - "Einstein's proof by dissection without rearrangement"
    - Algebraic proofs
    - Proof using differentials
    - Converse
    - Consequences and uses of the theorem
    - Pythagorean triples
    - Incommensurable lengths
    - Complex numbers
    - Euclidean distance in various coordinate systems
    - Pythagorean trigonometric identity
    - Relation to the cross product
    - Generalizations
    - Similar figures on the three sides
    - Law of cosines
    - Arbitrary triangle
    - General triangles using parallelograms
    - Solid geometry
    - Inner product spaces
    - Sets of m-dimensional objects in n-dimensional space
    - Applied to sets containing a single object
    - Applied to sets containing multiple objects
    - Applied in any number of dimensions
    - Non-Euclidean geometry
    - Spherical geometry
    - Hyperbolic geometry
    - Very small triangles
    - Differential geometry
    - History
    - In popular culture
    - Notes
categories:
    - History of geometry
---
In mathematics, the Pythagorean theorem, also known as Pythagoras's theorem, is a fundamental relation in Euclidean geometry among the three sides of a right triangle. It states that the square of the hypotenuse (the side opposite the right angle) is equal to the sum of the squares of the other two sides. The theorem can be written as an equation relating the lengths of the sides a, b and c, often called the "Pythagorean equation":[1]
