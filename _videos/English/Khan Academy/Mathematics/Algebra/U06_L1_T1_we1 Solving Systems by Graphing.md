---
version: 1
type: video
provider: YouTube
id: Li5XGPiLLAY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/U06_L1_T1_we1%20Solving%20Systems%20by%20Graphing.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e6e17b2d-b589-4212-b96d-be6719944cc7
updated: 1486069557
title: U06_L1_T1_we1 Solving Systems by Graphing
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/Li5XGPiLLAY/default.jpg
    - https://i3.ytimg.com/vi/Li5XGPiLLAY/1.jpg
    - https://i3.ytimg.com/vi/Li5XGPiLLAY/2.jpg
    - https://i3.ytimg.com/vi/Li5XGPiLLAY/3.jpg
---
