---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/theoretical
offline_file: ""
offline_thumbnail: ""
uuid: 755e0bbd-0caa-4c43-a656-7c0a2665c745
updated: 1484310393
title: theoretical
categories:
    - Dictionary
---
theoretical
     adj 1: concerned primarily with theories or hypotheses rather than
            practical considerations; "theoretical science" [syn:
            {theoretic}] [ant: {empirical}]
     2: concerned with theories rather than their practical
        applications; "theoretical physics" [ant: {applied}]
     3: based on specialized theory; "a theoretical analysis" [syn:
        {abstract}]
