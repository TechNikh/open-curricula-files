---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Propionic
offline_file: ""
offline_thumbnail: ""
uuid: 9687a27d-d967-4118-8df9-6d133b5ec6e7
updated: 1484308394
title: Propionic acid
tags:
    - History
    - Properties
    - Production
    - Industrial uses
    - Biological uses
    - Human occurrence
categories:
    - Uncategorized
---
Propionic acid (from the Greek words protos, meaning "first", and pion, meaning "fat"; also known as propanoic acid) is a naturally occurring carboxylic acid with chemical formula CH3CH2COOH. It is a clear liquid with a pungent and unpleasant smell somewhat resembling body odor. The anion CH3CH2COO− as well as the salts and esters of propionic acid are known as propionates (or propanoates).
