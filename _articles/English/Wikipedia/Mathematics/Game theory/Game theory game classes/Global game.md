---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Global_game
offline_file: ""
offline_thumbnail: ""
uuid: 1360c9ac-6a67-4869-96bf-3d55829046d2
updated: 1484309035
title: Global game
categories:
    - Game theory game classes
---
In economics and game theory, global games are games of incomplete information where players receive possibly-correlated signals of the underlying state of the world. Global games were originally defined by Carlsson and van Damme (1993).[1]
