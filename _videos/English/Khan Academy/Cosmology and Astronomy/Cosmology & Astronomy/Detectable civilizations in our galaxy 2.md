---
version: 1
type: video
provider: YouTube
id: jN03g05TSWM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Detectable%20civilizations%20in%20our%20galaxy%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b773fff0-9a6f-43cb-b0b9-9ef0377dddd5
updated: 1486069685
title: Detectable civilizations in our galaxy 2
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/jN03g05TSWM/default.jpg
    - https://i3.ytimg.com/vi/jN03g05TSWM/1.jpg
    - https://i3.ytimg.com/vi/jN03g05TSWM/2.jpg
    - https://i3.ytimg.com/vi/jN03g05TSWM/3.jpg
---
