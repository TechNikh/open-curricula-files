---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Structural_stability
offline_file: ""
offline_thumbnail: ""
uuid: 9b576b6f-d071-4244-bff2-b0c53a4eac44
updated: 1484308832
title: Structural stability
tags:
    - Definition
    - Examples
    - History and significance
categories:
    - Stability theory
---
In mathematics, structural stability is a fundamental property of a dynamical system which means that the qualitative behavior of the trajectories is unaffected by small perturbations (to be exact C1-small perturbations).
