---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/selfconsciousness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484487301
title: selfconsciousness
categories:
    - Dictionary
---
self-consciousness
     n 1: embarrassment deriving from the feeling that others are
          critically aware of you [syn: {uneasiness}, {uncomfortableness}]
     2: self-awareness plus the additional realization that others
        are similarly aware of you [ant: {unselfconsciousness}]
