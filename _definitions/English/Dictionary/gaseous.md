---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gaseous
offline_file: ""
offline_thumbnail: ""
uuid: 4b5021e6-cbf1-4855-9c6a-7805c07dbfd4
updated: 1484310228
title: gaseous
categories:
    - Dictionary
---
gaseous
     adj : existing as or having characteristics of a gas; "steam is
           water is the gaseous state" [ant: {solid}, {liquid}]
