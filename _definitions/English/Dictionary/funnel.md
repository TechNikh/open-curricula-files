---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/funnel
offline_file: ""
offline_thumbnail: ""
uuid: 4add1ec3-b1a3-48a2-861f-63da917d5be7
updated: 1484310226
title: funnel
categories:
    - Dictionary
---
funnel
     n 1: a conical shape with a wider and a narrower opening at the
          two ends [syn: {funnel shape}]
     2: a conically shaped utensil having a narrow tube at the small
        end; used to channel the flow of substances into a
        container with a small mouth
     3: (nautical) smokestack consisting of a shaft for ventilation
        or the passage of smoke (especially the smokestack of a
        ship)
     v : move or pour through a funnel; "funnel the liquid into the
         small bottle"
     [also: {funnelling}, {funnelled}]
