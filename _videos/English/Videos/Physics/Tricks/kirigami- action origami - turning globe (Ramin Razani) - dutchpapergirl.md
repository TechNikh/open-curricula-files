---
version: 1
type: video
provider: YouTube
id: OHhDFwSRZoE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c7ec9340-c875-46de-abb6-4a6f4661abc6
updated: 1486069660
title: 'kirigami- action origami - turning globe (Ramin Razani) - dutchpapergirl'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/OHhDFwSRZoE/default.jpg
    - https://i3.ytimg.com/vi/OHhDFwSRZoE/1.jpg
    - https://i3.ytimg.com/vi/OHhDFwSRZoE/2.jpg
    - https://i3.ytimg.com/vi/OHhDFwSRZoE/3.jpg
---
