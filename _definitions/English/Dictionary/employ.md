---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/employ
offline_file: ""
offline_thumbnail: ""
uuid: 3a915093-42c6-48ea-b58d-19bf3b290de5
updated: 1484310453
title: employ
categories:
    - Dictionary
---
employ
     n : the state of being employed or having a job; "they are
         looking for employment"; "he was in the employ of the
         city" [syn: {employment}] [ant: {unemployment}]
     v 1: put into service; make work or employ (something) for a
          particular purpose or for its inherent or natural
          purpose; "use your head!"; "we only use Spanish at
          home"; "I can't make use of this tool"; "Apply a
          magnetic field here"; "This thinking was applied to many
          projects"; "How do you utilize this tool?"; "I apply
          this rule to get good results"; "use the plastic bags to
          store the food"; "He doesn't know how to use a ...
