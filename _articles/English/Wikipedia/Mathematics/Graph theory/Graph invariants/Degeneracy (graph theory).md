---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Degeneracy_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 3a30b5da-aa08-4627-9329-ad645c77ede4
updated: 1484309420
title: Degeneracy (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-2-degenerate_graph_2-core.svg.png
tags:
    - Examples
    - Definitions and equivalences
    - k-Cores
    - Algorithms
    - Relation to other graph parameters
    - Infinite graphs
    - Notes
categories:
    - Graph invariants
---
In graph theory, a k-degenerate graph is an undirected graph in which every subgraph has a vertex of degree at most k: that is, some vertex in the subgraph touches k or fewer of the subgraph's edges. The degeneracy of a graph is the smallest value of k for which it is k-degenerate. The degeneracy of a graph is a measure of how sparse it is, and is within a constant factor of other sparsity measures such as the arboricity of a graph.
