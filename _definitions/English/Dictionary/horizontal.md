---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/horizontal
offline_file: ""
offline_thumbnail: ""
uuid: 9d76e7e9-c97c-47a7-af40-628cd6e4d9bd
updated: 1484310218
title: horizontal
categories:
    - Dictionary
---
horizontal
     adj : parallel to or in the plane of the horizon or a base line;
           "a horizontal surface" [ant: {vertical}, {inclined}]
     n : something that is oriented horizontally
