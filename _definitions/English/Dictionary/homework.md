---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/homework
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484588521
title: homework
categories:
    - Dictionary
---
homework
     n : preparatory school work done outside school (especially at
         home) [syn: {prep}, {preparation}]
