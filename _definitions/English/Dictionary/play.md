---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/play
offline_file: ""
offline_thumbnail: ""
uuid: 3d033280-718f-432f-bbe8-067a106771f4
updated: 1484310339
title: play
categories:
    - Dictionary
---
play
     n 1: a dramatic work intended for performance by actors on a
          stage; "he wrote several plays but only one was produced
          on Broadway" [syn: {drama}, {dramatic play}]
     2: a theatrical performance of a drama; "the play lasted two
        hours"
     3: a preset plan of action in team sports; "the coach drew up
        the plays for her team"
     4: a deliberate coordinated movement requiring dexterity and
        skill; "he made a great maneuver"; "the runner was out on
        a play by the shortstop" [syn: {maneuver}, {manoeuvre}]
     5: a state in which action is feasible; "the ball was still in
        play"; "insiders said the company's stock was in ...
