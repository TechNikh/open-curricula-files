---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paltry
offline_file: ""
offline_thumbnail: ""
uuid: 4c916af4-869d-4c54-872a-01233a40b27b
updated: 1484310170
title: paltry
categories:
    - Dictionary
---
paltry
     adj 1: not worth considering; "he considered the prize too paltry
            for the lives it must cost"; "piffling efforts"; "a
            trifling matter" [syn: {negligible}, {trifling}]
     2: contemptibly small in amount; "a measly tip"; "the company
        donated a miserable $100 for flood relief"; "a paltry
        wage"; "almost depleted his miserable store of dried
        beans" [syn: {measly}, {miserable}]
     [also: {paltriest}, {paltrier}]
