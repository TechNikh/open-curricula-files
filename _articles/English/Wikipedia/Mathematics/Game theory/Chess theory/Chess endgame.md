---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chess_endgame
offline_file: ""
offline_thumbnail: ""
uuid: 97c7d2e1-78d4-4eee-aeba-f07ce09f2594
updated: 1484309020
title: Chess endgame
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Nuvola_apps_package_games_strategy_1.png
tags:
    - Categories
    - The start of the endgame
    - General considerations
    - Common types of endgames
    - Basic checkmates
    - King and pawn endings
    - King and pawn versus king
    - Knight and pawn endings
    - Knight and pawn versus knight
    - Bishop and pawn endings
    - Bishop and pawn versus bishop on the same color
    - Bishops on opposite colors
    - Bishop versus knight endings (with pawns)
    - Bishop and pawn versus knight
    - Knight and pawn versus bishop
    - Rook and pawn endings
    - Rook and pawn versus rook
    - Quotation
    - Queen and pawn endings
    - Queen and pawn versus queen
    - Rook versus a minor piece
    - Two minor pieces versus a rook
    - Queen versus two rooks
    - Queen versus rook and minor piece
    - Queen versus rook
    - Piece versus pawns
    - Endings with no pawns
    - Positions with a material imbalance
    - Effect of tablebases on endgame theory
    - Longest forced win
    - Endgame classification
    - Frequency table
    - Quotations
    - Literature
categories:
    - Chess theory
---
The line between middlegame and endgame is often not clear, and may occur gradually or with the quick exchange of a few pairs of pieces. The endgame, however, tends to have different characteristics from the middlegame, and the players have correspondingly different strategic concerns. In particular, pawns become more important as endgames often revolve around attempting to promote a pawn by advancing it to the eighth rank. The king, which has to be protected in the middlegame owing to the threat of checkmate, becomes a strong piece in the endgame. It can be brought to the center of the board and act as a useful attacking piece.
