---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aren
offline_file: ""
offline_thumbnail: ""
uuid: 88020ad8-f98a-49f3-9320-8f484275c837
updated: 1484308455
title: Aren
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Aren_Wayside_Cross.JPG
tags:
    - geography
    - Places and Hamlets
    - Neighbouring communes and villages
    - Toponymy
    - History
    - Administration
    - Inter-communality
    - Demography
    - Economy
    - Culture and Heritage
    - Civil heritage
    - Religious heritage
    - Environmental heritage
    - Notable people linked to the commune
    - Notes
categories:
    - Uncategorized
---
The inhabitants of the commune are known as Arenais or Arenaises.[1]
