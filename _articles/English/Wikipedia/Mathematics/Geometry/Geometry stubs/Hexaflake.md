---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hexaflake
offline_file: ""
offline_thumbnail: ""
uuid: 42163e54-3886-4adc-9f0f-24664e80e8e4
updated: 1484309362
title: Hexaflake
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hexaflake.gif
categories:
    - Geometry stubs
---
A hexaflake has 7n-1 hexagons in its nth iteration, each smaller by 1/3 than the hexagons in the previous iteration. Its exterior boundary is the von Koch flake, and the full boundary contains an infinite number of Koch snowflakes. The Hausdorff dimension of the hexaflake is equal to ln(7)/ln(3), approximately 1.7712. It may also be constructed by projecting the Cantor cube onto the plane orthogonal to its main diagonal.
