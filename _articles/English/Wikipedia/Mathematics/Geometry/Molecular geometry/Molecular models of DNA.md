---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Molecular_models_of_DNA
offline_file: ""
offline_thumbnail: ""
uuid: a2bf25df-50af-498b-84f2-35accc8cc40e
updated: 1484309277
title: Molecular models of DNA
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-DNA_nanostructures.png
tags:
    - History
    - Importance
    - Fundamental concepts
    - DNA structure
    - >
        DNA structure determination using molecular modeling and DNA
        X-ray patterns
    - Paracrystalline lattice models of B-DNA structures
    - >
        Genomic and biotechnology applications of DNA molecular
        modeling
    - Gallery of DNA models
    - Databases for DNA molecular models and sequences
categories:
    - Molecular geometry
---
Molecular models of DNA structures are representations of the molecular geometry and topology of deoxyribonucleic acid (DNA) molecules using one of several means, with the aim of simplifying and presenting the essential, physical and chemical, properties of DNA molecular structures either in vivo or in vitro. These representations include closely packed spheres (CPK models) made of plastic, metal wires for skeletal models, graphic computations and animations by computers, artistic rendering. Computer molecular models also allow animations and molecular dynamics simulations that are very important for understanding how DNA functions in vivo.
