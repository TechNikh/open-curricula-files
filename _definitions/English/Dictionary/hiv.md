---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hiv
offline_file: ""
offline_thumbnail: ""
uuid: a07bb898-9e53-4c49-9ce6-96c7b462a676
updated: 1484310163
title: hiv
categories:
    - Dictionary
---
HIV
     n 1: infection by the human immunideficiency virus
     2: the virus that causes acquired immune deficiency syndrome
        (AIDS); it replicates in and kills the helper T cells
        [syn: {human immunodeficiency virus}]
