---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalized_quadrangle
offline_file: ""
offline_thumbnail: ""
uuid: 85b45a35-b209-41d5-9e05-0578603200ef
updated: 1484309253
title: Generalized quadrangle
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-GQ%25282%252C2%2529%252C_the_Doily.svg.png'
tags:
    - Definition
    - Properties
    - Graphs
    - Duality
    - Generalized quadrangles with lines of size 3
    - Classical generalized quadrangles
    - Non-classical examples
    - Restrictions on parameters
categories:
    - Incidence geometry
---
In geometry, a generalized quadrangle is an incidence structure whose main feature is the lack of any triangles (yet containing many quadrangles). A generalized quadrangle is by definition a polar space of rank two. They are the generalized n-gons with n = 4 and near 2n-gons with n = 2. They are also precisely the partial geometries pg(s,t,α) with α = 1.
