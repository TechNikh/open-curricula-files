---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proposed
offline_file: ""
offline_thumbnail: ""
uuid: 63a9c78d-e41f-4f79-a349-0dfa28d84ff1
updated: 1484310290
title: proposed
categories:
    - Dictionary
---
proposed
     adj : planned for the future; "the first volume of a proposed
           series" [syn: {projected}]
