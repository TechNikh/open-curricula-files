---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Curtis_T._McMullen
offline_file: ""
offline_thumbnail: ""
uuid: aa50b965-8914-4e62-a97b-a1c7b16c14e7
updated: 1484308696
title: Curtis T. McMullen
tags:
    - Biography
    - Honors and awards
    - trivia
    - Works
categories:
    - Dynamical systems theorists
---
Curtis Tracy McMullen (born 21 May 1958) is Professor of Mathematics at Harvard University. He was awarded the Fields Medal in 1998 for his work in complex dynamics, hyperbolic geometry and Teichmüller theory.
