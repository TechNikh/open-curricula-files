---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/type
offline_file: ""
offline_thumbnail: ""
uuid: a20b76b8-3533-47e7-b78f-1531717a30f2
updated: 1484310359
title: type
categories:
    - Dictionary
---
type
     n 1: a subdivision of a particular kind of thing; "what type of
          sculpture do you prefer?" [ant: {antitype}]
     2: a person of a specified kind (usually with many
        eccentricities); "a real character"; "a strange
        character"; "a friendly eccentric"; "the capable type"; "a
        mental case" [syn: {character}, {eccentric}, {case}]
     3: (biology) the taxonomic group whose characteristics are used
        to define the next higher taxon
     4: printed characters; "small type is hard to read"
     5: a small metal block bearing a raised character on one end;
        produces a printed character when inked and pressed on
        paper; "he dropped a case ...
