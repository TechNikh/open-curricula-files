---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/negatively
offline_file: ""
offline_thumbnail: ""
uuid: 6b01d1c4-0f17-4221-9697-48e602b370d0
updated: 1484310202
title: negatively
categories:
    - Dictionary
---
negatively
     adv 1: in a harmful manner; "he was negatively affected"
     2: in a negative way; "he was negatively inclined"
