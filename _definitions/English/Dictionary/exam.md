---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exam
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484316841
title: exam
categories:
    - Dictionary
---
exam
     n : a set of questions or exercises evaluating skill or
         knowledge; "when the test was stolen the professor had to
         make a new set of questions" [syn: {examination}, {test}]
