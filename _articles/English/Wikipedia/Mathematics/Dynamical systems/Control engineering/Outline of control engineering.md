---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Outline_of_control_engineering
offline_file: ""
offline_thumbnail: ""
uuid: dee28aa2-9600-4d63-81e0-3055d682df47
updated: 1484308738
title: Outline of control engineering
tags:
    - Branches
    - Mathematical Concepts
    - System Properties
    - Digital Control
    - Advanced Techniques
    - Tools
    - Controllers
    - Control Applications
    - Control engineering organizations
    - Publications about control engineering
    - Persons influential in control engineering
categories:
    - Control engineering
---
Control engineering – engineering discipline that applies control theory to design systems with desired behaviors. The practice uses sensors to measure the output performance of the device being controlled and those measurements can be used to give feedback to the input actuators that can make corrections toward desired performance. When a device is designed to perform without the need of human inputs for correction it is called automatic control (such as cruise control for regulating a car's speed).
