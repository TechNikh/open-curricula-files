---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bright
offline_file: ""
offline_thumbnail: ""
uuid: b2adb7cb-b8be-46e0-8e1b-e106d48faea2
updated: 1484310305
title: bright
categories:
    - Dictionary
---
bright
     adj 1: emitting or reflecting light readily or in large amounts;
            "the sun was bright and hot"; "a bright sunlit room"
            [ant: {dull}]
     2: having striking color; "bright greens"; "brilliant
        tapestries"; "a bird with vivid plumage" [syn: {brilliant},
         {vivid}]
     3: characterized by quickness and ease in learning; "some
        children are brighter in one subject than another"; "smart
        children talk earlier than the average" [syn: {smart}]
     4: having lots of light either natural or artificial; "the room
        was bright and airy"; "a stage bright with spotlights"
     5: made smooth and bright by or as if by rubbing; ...
