---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyclohedron
offline_file: ""
offline_thumbnail: ""
uuid: ac3d0e5d-7d19-4df5-924d-71c0f50cfd48
updated: 1484309359
title: Cyclohedron
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Cyclohedron_W3.svg.png
tags:
    - Notes
categories:
    - Geometry stubs
---
The configuration space of n distinct points on the circle S1 is an n-dimensional manifold, which can be compactified into a manifold with corners by allowing the points to approach each other. This compactification can be factored as 
  
    
      
        
          S
          
            1
          
        
        ×
        
          W
          
            n
          
        
      
    
    {\displaystyle S^{1}\times W_{n}}
  
, where Wn is the cyclohedron.
