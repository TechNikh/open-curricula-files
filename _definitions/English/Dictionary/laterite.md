---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laterite
offline_file: ""
offline_thumbnail: ""
uuid: d582b5b3-2ac2-4cf3-90f7-080e61b6e2c6
updated: 1484310533
title: laterite
categories:
    - Dictionary
---
laterite
     n : a red soil produced by rock decay; contains insoluble
         deposits of ferric and aluminum oxides
