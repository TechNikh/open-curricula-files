---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/triangular
offline_file: ""
offline_thumbnail: ""
uuid: 9f0d567f-88db-4dfd-b697-2b5bb06ed02b
updated: 1484310268
title: triangular
categories:
    - Dictionary
---
triangular
     adj 1: having three angles; forming or shaped like a triangle; "a
            triangular figure"; "a triangular pyrimid has a
            triangle for a base"
     2: involving three parties or elements; "the triangular
        mother-father-child relationship"; "a trilateral
        agreement"; "a tripartite treaty"; "a tripartite
        division"; "a three-way playoff" [syn: {trilateral}, {tripartite},
         {three-party}, {three-way}]
     3: having three sides; "a trilateral figure" [syn: {trilateral},
         {three-sided}]
