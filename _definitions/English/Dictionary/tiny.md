---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tiny
offline_file: ""
offline_thumbnail: ""
uuid: f158cbb9-7a7f-449c-aec6-139c01e2b520
updated: 1484310349
title: tiny
categories:
    - Dictionary
---
tiny
     adj : very small; "diminutive in stature"; "a lilliputian chest of
           drawers"; "her petite figure"; "tiny feet"; "the
           flyspeck nation of Bahrain moved toward democracy"
           [syn: {bantam}, {diminutive}, {lilliputian}, {midget},
           {petite}, {flyspeck}]
     [also: {tiniest}, {tinier}]
