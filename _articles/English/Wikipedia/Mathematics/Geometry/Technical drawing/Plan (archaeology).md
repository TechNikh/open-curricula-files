---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plan_(archaeology)
offline_file: ""
offline_thumbnail: ""
uuid: 2f9ae3a8-b21c-4e00-b660-c94a6eb1727c
updated: 1484309332
title: Plan (archaeology)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Plan_archaeological.jpg
tags:
    - Overview
    - Archaeological plan topics
    - The grid
    - Planning drawing conventions
    - Pre-excavation and base plans
    - Critics of pre-excavation planning
categories:
    - Technical drawing
---
