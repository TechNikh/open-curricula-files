---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/daddy
offline_file: ""
offline_thumbnail: ""
uuid: 501e308d-506d-433c-af1c-7a173637a25b
updated: 1484310150
title: daddy
categories:
    - Dictionary
---
daddy
     n : an informal term for a father; probably derived from baby
         talk [syn: {dad}, {dada}, {pa}, {papa}, {pappa}, {pater},
          {pop}]
