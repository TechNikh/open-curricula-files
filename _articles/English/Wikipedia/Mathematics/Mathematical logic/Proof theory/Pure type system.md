---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pure_type_system
offline_file: ""
offline_thumbnail: ""
uuid: b0a20d52-df75-4455-b30c-1170d10a6a35
updated: 1484309497
title: Pure type system
categories:
    - Proof theory
---
In the branches of mathematical logic known as proof theory and type theory, a pure type system (PTS), previously known as a generalized type system (GTS), is a form of typed lambda calculus that allows an arbitrary number of sorts and dependencies between any of these. The framework can be seen as a generalisation of Barendregt's lambda cube, in the sense that all corners of the cube can be represented as instances of a PTS with just two sorts.[1][2] In fact Barendregt (1991) framed his cube in this setting.[3] Pure type systems may obscure the distinction between types and terms and collapse the type hierarchy, as is the case with the calculus of constructions, but this is not generally ...
