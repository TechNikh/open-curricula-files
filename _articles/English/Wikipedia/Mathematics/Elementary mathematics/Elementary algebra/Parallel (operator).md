---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parallel_(operator)
offline_file: ""
offline_thumbnail: ""
uuid: e24197fa-c6c0-4454-9a29-1392085a0323
updated: 1484308909
title: Parallel (operator)
categories:
    - Elementary algebra
---
The parallel operator 
  
    
      
        ∥
      
    
    {\displaystyle \|}
  
 (pronounced "parallel") is a mathematical function which is used especially as shorthand in electrical engineering. It represents the reciprocal value of a sum of reciprocal values and is defined by:
