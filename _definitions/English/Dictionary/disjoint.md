---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disjoint
offline_file: ""
offline_thumbnail: ""
uuid: a50c3963-f9d4-45ff-873b-71aabc7ceca5
updated: 1484310156
title: disjoint
categories:
    - Dictionary
---
disjoint
     adj : having no elements in common
     v 1: part; cease or break association with; "She disassociated
          herself from the organization when she found out the
          identity of the president" [syn: {disassociate}, {dissociate},
           {divorce}, {disunite}]
     2: separate at the joints; "disjoint the chicken before cooking
        it" [syn: {disarticulate}]
     3: make disjoint, separated, or disconnected; undo the joining
        of [syn: {disjoin}] [ant: {join}]
     4: become separated, disconnected or disjoint [syn: {disjoin}]
        [ant: {join}]
