---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tutte_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: cb6f3637-a6af-40f3-b93c-93f6546ff644
updated: 1484309239
title: Tutte polynomial
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Tutte_polynomial_and_chromatic_polynomial_of_the_Bull_graph.jpg
tags:
    - Definitions
    - Properties
    - Examples
    - History
    - Specialisations
    - Chromatic polynomial
    - Jones polynomial
    - Individual points
    - (2,1)
    - (1,1)
    - (1,2)
    - (2,0)
    - (0,2)
    - (0,−2)
    - (3,3)
    - Potts and Ising models
    - Flow polynomial
    - Reliability polynomial
    - Dichromatic polynomial
    - Related polynomials
    - Martin polynomial
    - Algorithms
    - Deletion–contraction
    - Gaussian elimination
    - Markov chain Monte Carlo
    - Computational complexity
    - Exact computation
    - Approximation
    - Notes
categories:
    - Duality theories
---
The Tutte polynomial, also called the dichromate or the Tutte–Whitney polynomial, is a polynomial in two variables which plays an important role in graph theory. It is defined for every undirected graph and contains information about how the graph is connected. It is denoted by 
  
    
      
        
          T
          
            G
          
        
      
    
    {\displaystyle T_{G}}
  
.
