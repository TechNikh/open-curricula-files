---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lakshmi
offline_file: ""
offline_thumbnail: ""
uuid: 975962b9-215f-4921-aefb-f4e5ec327e94
updated: 1484308333
title: Lakshmi
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Ravi_Varma-Lakshmi.jpg
tags:
    - Etymology
    - Symbolism and iconography
    - Names
    - Ancient literature on Lakshmi
    - Upanishads
    - Stotrams and sutras
    - Puranas
    - Subhasita, gnomic and didactic literature
    - Manifestations and aspects
    - Secondary manifestations
    - Jain temples
    - Creation and legends
    - Celebration in Hindu society
    - Hymns
    - Archaeology
    - Related goddesses
    - Japan
    - Tibet and Nepal
categories:
    - Uncategorized
---
Lakshmi (Sanskrit: लक्ष्मी, lakṣmī, ˈləkʂmiː) is the Hindu goddess of wealth, fortune and prosperity. She is the wife and shakti (energy) of Vishnu, a major god in Hinduism.[2] Lakshmi is also an important deity in Jainism and found in Jain temples.[3] Lakshmi was also a Goddess of abundance and fortune for Buddhists, and was represented on the oldest surviving stupas and cave temples of Buddhism.[4][5] In Buddhist sects of Tibet, Nepal and southeast Asia, goddess Vasudhara mirrors the characteristics and attributes of the Hindu goddess Lakshmi with minor iconographic differences.[6]
