---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rusty
offline_file: ""
offline_thumbnail: ""
uuid: 57d839a6-033c-45ba-b8fc-7cb83bc1c88b
updated: 1484310411
title: rusty
categories:
    - Dictionary
---
rusty
     adj 1: covered with or consisting of rust; "a rusty machine";
            "rusty deposits"
     2: of the color of rust [syn: {rust}]
     3: impaired in skill by neglect [syn: {out of practice(p)}]
     4: ancient; "hoary jokes" [syn: {hoary}]
     [also: {rustiest}, {rustier}]
