---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/betray
offline_file: ""
offline_thumbnail: ""
uuid: 64509eb7-5d2b-4607-80fc-0e75d8da0e54
updated: 1484310549
title: betray
categories:
    - Dictionary
---
betray
     v 1: reveal unintentionally; "Her smile betrayed her true
          feelings" [syn: {bewray}]
     2: deliver to an enemy by treachery; "Judas sold Jesus"; "The
        spy betrayed his country" [syn: {sell}]
     3: disappoint, prove undependable to; abandon, forsake; "His
        sense of smell failed him this time"; "His strength
        finally failed him"; "His children failed him in the
        crisis" [syn: {fail}]
     4: be sexually unfaithful to one's partner in marriage; "She
        cheats on her husband"; "Might her husband be wandering?"
        [syn: {cheat on}, {cheat}, {cuckold}, {wander}]
     5: give away information about somebody; "He told on his
        ...
