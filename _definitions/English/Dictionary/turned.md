---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/turned
offline_file: ""
offline_thumbnail: ""
uuid: 471cbf8e-962b-4333-820f-42311bb77276
updated: 1484310313
title: turned
categories:
    - Dictionary
---
turned
     adj 1: moved around an axis or center [ant: {unturned}]
     2: in an unpalatable state; "sour milk" [syn: {off}, {sour}]
