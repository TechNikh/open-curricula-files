---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shrub
offline_file: ""
offline_thumbnail: ""
uuid: c12a6d74-63db-45d6-a2b7-7ba00dadf508
updated: 1484310543
title: shrub
categories:
    - Dictionary
---
shrub
     n : a low woody perennial plant usually having several major
         branches [syn: {bush}]
