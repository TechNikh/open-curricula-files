---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/offshore
offline_file: ""
offline_thumbnail: ""
uuid: 3c53dc2a-a27d-4b48-a7f2-9321583b943d
updated: 1484310581
title: offshore
categories:
    - Dictionary
---
offshore
     adj 1: (of winds) coming from the land; "offshore winds" [ant: {inshore}]
     2: at some distance from the shore; "offshore oil reserves";
        "an offshore island"
     adv : away from shore; away from land; "cruising three miles
           offshore" [ant: {onshore}]
