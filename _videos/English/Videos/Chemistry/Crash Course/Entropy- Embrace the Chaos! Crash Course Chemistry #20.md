---
version: 1
type: video
provider: YouTube
id: ZsY4WcQOrfk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Entropy-%20Embrace%20the%20Chaos%21%20Crash%20Course%20Chemistry%20%2320.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5a46b076-6185-4f96-a98b-b19e09b1698a
updated: 1486069678
title: 'Entropy- Embrace the Chaos! Crash Course Chemistry #20'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZsY4WcQOrfk/default.jpg
    - https://i3.ytimg.com/vi/ZsY4WcQOrfk/1.jpg
    - https://i3.ytimg.com/vi/ZsY4WcQOrfk/2.jpg
    - https://i3.ytimg.com/vi/ZsY4WcQOrfk/3.jpg
---
