---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Squared_triangular_number
offline_file: ""
offline_thumbnail: ""
uuid: 6dad5f1f-0a58-459d-a64e-1eb08ee7a778
updated: 1484308659
title: Squared triangular number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Nicomachus_theorem_3D.svg.png
tags:
    - History
    - Numeric values; geometric and probabilistic interpretation
    - Proofs
    - Generalizations
categories:
    - Number theory
---
The same equation may be written more compactly using the mathematical notation for summation:
