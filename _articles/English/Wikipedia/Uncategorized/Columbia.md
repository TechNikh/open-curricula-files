---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Columbia
offline_file: ""
offline_thumbnail: ""
uuid: f21ea206-91d2-41cd-961a-fb526978815b
updated: 1484308345
title: Columbia
tags:
    - Places
    - North America
    - Elsewhere
    - Companies
    - Music and entertainment
    - Other companies
    - Music
    - schools
    - School districts
    - Ships
    - Naval vessels
    - "America's Cup yachts"
    - Other ships
    - Aircraft and spacecraft
    - In fiction
    - Publications
    - people
    - Other uses
categories:
    - Uncategorized
---
Columbia may refer to:
