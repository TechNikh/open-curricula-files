---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/justification
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484357401
title: justification
categories:
    - Dictionary
---
justification
     n 1: something (such as a fact or circumstance) that shows an
          action to be reasonable or necessary; "he considered
          misrule a justification for revolution"
     2: a statement in explanation of some action or belief
     3: the act of defending or explaining or making excuses for by
        reasoning; "the justification of barbarous means by holy
        ends"- H.J.Muller
