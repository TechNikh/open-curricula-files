---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exception
offline_file: ""
offline_thumbnail: ""
uuid: efa50e13-db81-4e11-867a-1194abd298c7
updated: 1484310405
title: exception
categories:
    - Dictionary
---
exception
     n 1: a deliberate act of omission; "with the exception of the
          children, everyone was told the news" [syn: {exclusion},
           {elision}]
     2: an instance that does not conform to a rule or
        generalization; "all her children were brilliant; the only
        exception was her last child"; "an exception tests the
        rule"
     3: grounds for adverse criticism; "his authority is beyond
        exception"
