---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Prior-independent_mechanism
offline_file: ""
offline_thumbnail: ""
uuid: ea028bf7-1788-4923-936c-4b5d322e146d
updated: 1484309046
title: Prior-independent mechanism
tags:
    - Single-item auctions
    - Single-parametric agents
    - Multi-parametric agents
    - Alternatives
categories:
    - Mechanism design
---
A Prior-independent mechanism (PIM) is a mechanism in which the designer knows that the agents' valuations are drawn from some probability distribution, but does not know the distribution.
