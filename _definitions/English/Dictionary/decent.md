---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decent
offline_file: ""
offline_thumbnail: ""
uuid: 8d5bffc4-23ee-41d3-a711-934ab73472c5
updated: 1484310443
title: decent
categories:
    - Dictionary
---
decent
     adj 1: socially or conventionally correct; refined or virtuous;
            "from a decent family"; "a nice girl" [syn: {nice}]
     2: according with custom or propriety; "her becoming modesty";
        "comely behavior"; "it is not comme il faut for a
        gentleman to be constantly asking for money"; "a decent
        burial"; "seemly behavior" [syn: {becoming}, {comely}, {comme
        il faut}, {decorous}, {seemly}]
     3: conforming to conventions of sexual behavior; "speech in
        this circle, if not always decent, never became lewd"-
        George Santayana [ant: {indecent}]
     4: enough to meet a purpose; "an adequate income"; "the food
        was adequate"; ...
