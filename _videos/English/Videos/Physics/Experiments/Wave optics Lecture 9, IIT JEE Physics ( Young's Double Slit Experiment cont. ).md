---
version: 1
type: video
provider: YouTube
id: M0e2-IKygzU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c516d610-dab2-4fc2-93a0-0b23876312c5
updated: 1486069637
title: "Wave optics Lecture 9, IIT JEE Physics ( Young's Double Slit Experiment cont. )"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/M0e2-IKygzU/default.jpg
    - https://i3.ytimg.com/vi/M0e2-IKygzU/1.jpg
    - https://i3.ytimg.com/vi/M0e2-IKygzU/2.jpg
    - https://i3.ytimg.com/vi/M0e2-IKygzU/3.jpg
---
