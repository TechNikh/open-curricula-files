---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_Forbes_Nash_Jr.
offline_file: ""
offline_thumbnail: ""
uuid: ebf38428-dba5-419c-924b-43e65ca15a0d
updated: 1484308506
title: John Forbes Nash Jr.
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-John_f_nash_20061102_3.jpg
tags:
    - Early life and education
    - Major contributions
    - Game theory
    - Other mathematics
    - Mental illness
    - Recognition and later career
    - Personal life
    - Death
    - Representation in culture
    - Awards
    - Bibliography
    - Documentaries and video interviews
categories:
    - Game theorists
---
John Forbes Nash Jr. (June 13, 1928 – May 23, 2015) was an American mathematician who made fundamental contributions to game theory, differential geometry, and the study of partial differential equations.[2][3] Nash's work has provided insight into the factors that govern chance and decision making inside complex systems found in daily life.
