---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carbon_dioxide
offline_file: ""
offline_thumbnail: ""
uuid: 41ab4a8e-777f-44ba-b895-9fea4f7482a7
updated: 1484308380
title: Carbon dioxide
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Auto-and_heterotrophs.png
tags:
    - Background
    - Chemical and physical properties
    - Structure and bonding
    - In aqueous solution
    - Chemical reactions of CO2
    - Physical properties
    - Isolation and production
    - Uses
    - Precursor to chemicals
    - Foods
    - Beverages
    - Wine making
    - Inert gas
    - Fire extinguisher
    - Supercritical CO2 as solvent
    - Agricultural and biological applications
    - Oil recovery
    - Bio transformation into fuel
    - Refrigerant
    - Coal bed methane recovery
    - Niche uses
    - "In Earth's atmosphere"
    - In the oceans
    - Biological role
    - Photosynthesis and carbon fixation
    - Toxicity
    - 'Below 1%'
    - Ventilation
    - Human physiology
    - Content
    - Transport in the blood
    - Regulation of respiration
categories:
    - Uncategorized
---
Carbon dioxide (chemical formula CO2) is a colorless and odorless gas vital to life on Earth. This naturally occurring chemical compound is composed of a carbon atom covalently double bonded to two oxygen atoms. Carbon dioxide exists in Earth's atmosphere as a trace gas at a concentration of about 0.04 percent (400 ppm) by volume.[3] Natural sources include volcanoes, hot springs and geysers, and it is freed from carbonate rocks by dissolution in water and acids. Because carbon dioxide is soluble in water, it occurs naturally in groundwater, rivers and lakes, in ice caps and glaciers and also in seawater. It is present in deposits of petroleum and natural gas.[4]
