---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uncultivated
offline_file: ""
offline_thumbnail: ""
uuid: bb893e4f-184b-41aa-bfdd-dd5f7effafd4
updated: 1484310547
title: uncultivated
categories:
    - Dictionary
---
uncultivated
     adj 1: (of land or fields) not prepared for raising crops;
            "uncultivated land" [ant: {cultivated}]
     2: (of persons) lacking art or knowledge [syn: {artless}, {uncultured}]
     3: characteristic of a person who is not cultivated or does not
        have intellectual tastes; "lowbrow tastes" [syn: {lowbrow},
         {lowbrowed}]
