---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/political
offline_file: ""
offline_thumbnail: ""
uuid: b0cfd014-c0e1-4ad3-a1f5-bfa88f5306f3
updated: 1484310464
title: political
categories:
    - Dictionary
---
political
     adj 1: involving or characteristic of politics or parties or
            politicians; "calling a meeting is a political act in
            itself"- Daniel Goleman; "political pressure"; "a
            political machine"; "political office"; "political
            policy" [ant: {nonpolitical}]
     2: of or relating to your views about social relationships
        involving authority or power; "political opinions"
     3: of or relating to the profession of governing; "political
        career"
