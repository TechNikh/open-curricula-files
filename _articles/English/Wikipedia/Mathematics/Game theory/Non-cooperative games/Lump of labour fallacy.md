---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lump_of_labour_fallacy
offline_file: ""
offline_thumbnail: ""
uuid: 520a7416-23b5-4916-9d4a-93c0a1b2786b
updated: 1484309059
title: Lump of labour fallacy
tags:
    - Origins
    - Application to employment regulations
    - Early retirement
    - Economic analysis
    - Immigration
categories:
    - Non-cooperative games
---
In economics, the lump of labour fallacy (or lump of jobs fallacy, fallacy of labour scarcity, or the zero-sum fallacy, from its ties to zero-sum games) is the contention that the amount of work available to labourers is fixed. It is considered a fallacy by most economists, who hold that the amount of work is not static, although the history of the claim contains inconsistencies and anomalies.[1] Another way to describe the fallacy is that it treats the demand for labour as an exogenous variable, when it is not.
