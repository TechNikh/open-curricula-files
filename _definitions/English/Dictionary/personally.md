---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/personally
offline_file: ""
offline_thumbnail: ""
uuid: 99b08556-90a6-4163-bfee-239ab157ea6b
updated: 1484310605
title: personally
categories:
    - Dictionary
---
personally
     adv 1: as yourself; "speaking personally, I would not want to go"
     2: as a person; "he is personally repulsive"
     3: in a personal way; "he took her comments personally" [ant: {impersonally}]
     4: in the flesh; without involving anyone else; "I went there
        personally"; "he appeared in person" [syn: {in person}]
     5: concerning the speaker; "personally, I find him stupid"
