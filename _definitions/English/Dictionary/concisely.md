---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concisely
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484498641
title: concisely
categories:
    - Dictionary
---
concisely
     adv : in a concise manner; in a few words; "the history is summed
           up concisely in this book"; "she replied briefly";
           "briefly, we have a problem"; "to put it shortly" [syn:
            {briefly}, {shortly}, {in brief}, {in short}]
