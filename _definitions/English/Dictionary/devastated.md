---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/devastated
offline_file: ""
offline_thumbnail: ""
uuid: 1fefef23-19a2-4f14-81ed-ff117a3cb61d
updated: 1484310541
title: devastated
categories:
    - Dictionary
---
devastated
     adj : made uninhabitable; "upon this blasted heath"- Shakespeare;
           "a wasted landscape" [syn: {blasted}, {desolate}, {desolated},
            {ravaged}, {ruined}, {wasted}]
