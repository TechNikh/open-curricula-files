---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glory
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484433721
title: glory
categories:
    - Dictionary
---
glory
     n 1: a state of high honor; "he valued glory above life itself"
          [syn: {glorification}]
     2: brilliant radiant beauty; "the glory of the sunrise" [syn: {resplendence},
         {resplendency}]
     3: an indication of radiant light drawn around the head of a
        saint [syn: {aura}, {aureole}, {halo}, {nimbus}, {gloriole}]
     v : rejoice proudly
     [also: {gloried}]
