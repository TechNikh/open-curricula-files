---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Extended_side
offline_file: ""
offline_thumbnail: ""
uuid: be9b4f32-ea26-4d24-8762-4b566667f0ba
updated: 1484308930
title: Extended side
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Incircle_and_Excircles.svg.png
tags:
    - Triangle
    - Ex-tangential quadrilateral
    - Hexagon
categories:
    - Elementary geometry
---
In plane geometry, an extended side or sideline of a polygon is the line that contains one side of the polygon. The extension of a side arises in various contexts.
