---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Physiochemical
offline_file: ""
offline_thumbnail: ""
uuid: f4d260fa-0857-4753-9080-74c9b5de4657
updated: 1484308377
title: Biochemistry
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Gerty_Theresa_Radnitz_Cori_%25281896-1957%2529_and_Carl_Ferdinand_Cori.jpg'
tags:
    - History
    - 'Starting materials: the chemical elements of life'
    - Biomolecules
    - Carbohydrates
    - Lipids
    - Proteins
    - Nucleic acids
    - Metabolism
    - Carbohydrates as energy source
    - Glycolysis (anaerobic)
    - Aerobic
    - Gluconeogenesis
    - Relationship to other "molecular-scale" biological sciences
    - Lists
    - Notes
    - Cited literature
categories:
    - Uncategorized
---
Biochemistry, sometimes called biological chemistry, is the study of chemical processes within and relating to living organisms.[1] By controlling information flow through biochemical signaling and the flow of chemical energy through metabolism, biochemical processes give rise to the complexity of life. Over the last decades of the 20th century, biochemistry has become so successful at explaining living processes that now almost all areas of the life sciences from botany to medicine to genetics are engaged in biochemical research.[2] Today, the main focus of pure biochemistry is on understanding how biological molecules give rise to the processes that occur within living cells,[3] which in ...
