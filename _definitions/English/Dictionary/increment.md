---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/increment
offline_file: ""
offline_thumbnail: ""
uuid: 88af08b3-76b2-4894-be26-019ce51852fe
updated: 1484310469
title: increment
categories:
    - Dictionary
---
increment
     n 1: a process of becoming larger or longer or more numerous or
          more important; "the increase in unemployment"; "the
          growth of population" [syn: {increase}, {growth}] [ant:
          {decrease}, {decrease}]
     2: the amount by which something increases; "they proposed an
        increase of 15 percent in the fare" [syn: {increase}]
        [ant: {decrease}]
