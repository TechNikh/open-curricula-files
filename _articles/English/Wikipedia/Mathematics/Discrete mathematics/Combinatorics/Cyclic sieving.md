---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyclic_sieving
offline_file: ""
offline_thumbnail: ""
uuid: fd073f34-fed9-4ab4-a0e2-6d04d611945d
updated: 1484308580
title: Cyclic sieving
categories:
    - Combinatorics
---
In combinatorial mathematics, cyclic sieving is a phenomenon by which evaluating a generating function for a finite set at roots of unity counts symmetry classes of objects acted on by a cyclic group.[1]
