---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unerring
offline_file: ""
offline_thumbnail: ""
uuid: e1ea6321-9447-47ea-bbe8-edc33285427b
updated: 1484310563
title: unerring
categories:
    - Dictionary
---
unerring
     adj : not liable to error; "the Church was...theoretically
           inerrant and omnicompetent"-G.G.Coulton; "lack an
           inerrant literary sense"; "an unerring marksman" [syn:
           {inerrable}, {inerrant}]
