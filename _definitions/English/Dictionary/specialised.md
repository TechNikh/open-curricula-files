---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/specialised
offline_file: ""
offline_thumbnail: ""
uuid: 2f18d808-071a-43ab-93fc-7c31e09d1cec
updated: 1484310290
title: specialised
categories:
    - Dictionary
---
specialised
     adj 1: developed or designed for a special activity or function; "a
            specialized tool" [syn: {specialized}] [ant: {unspecialized}]
     2: marked by or characteristic of specialization in a
        mechanical or scientific subject; "his specialized
        qualifications for the job"; "all kinds of specialized
        technical books" [syn: {specialized}]
