---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gleaner
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484650021
title: gleaner
categories:
    - Dictionary
---
gleaner
     n 1: someone who picks up grain left in the field by the
          harvesters
     2: someone who gathers something in small pieces (e.g.
        information) slowly and carefully
