---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alpha_shape
offline_file: ""
offline_thumbnail: ""
uuid: 0c7581be-5760-40d2-8865-c7d4ddc8b25f
updated: 1484309167
title: Alpha shape
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-ScagnosticsBase.svg.png
tags:
    - Characterization
    - Alpha complex
    - Examples
categories:
    - Computational geometry
---
In computational geometry, an alpha shape, or α-shape, is a family of piecewise linear simple curves in the Euclidean plane associated with the shape of a finite set of points. They were first defined by Edelsbrunner, Kirkpatrick & Seidel (1983). The alpha-shape associated with a set of points is a generalization of the concept of the convex hull, i.e. every convex hull is an alpha-shape but not every alpha shape is a convex hull.
