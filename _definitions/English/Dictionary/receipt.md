---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/receipt
offline_file: ""
offline_thumbnail: ""
uuid: e97f61f5-3d76-48c8-953a-4b5b719f80cf
updated: 1484310517
title: receipt
categories:
    - Dictionary
---
receipt
     n 1: the act of receiving [syn: {reception}]
     2: an acknowledgment (usually tangible) that payment has been
        made
     v 1: report the receipt of; "The program committee acknowledged
          the submission of the authors of the paper" [syn: {acknowledge}]
     2: mark or stamp as paid
