---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Enthalpy%E2%80%93entropy_compensation'
offline_file: ""
offline_thumbnail: ""
uuid: 82dede8c-63a3-4df8-8649-9c5ae9f9c7fc
updated: 1484308842
title: Enthalpy–entropy compensation
tags:
    - Related terms
    - Mathematics
    - Enthalpy–entropy compensation as a requirement for LFERs
    - Isokinetic and isoequilibrium temperature
    - History
    - Phenomenon explained
    - Criticism
categories:
    - Thermodynamics
---
Enthalpy–entropy compensation is a specific example of the compensation effect. The compensation effect refers to the behavior of a series of closely related chemical reactions (e.g., reactants in different solvents or reactants differing only in a single substituent), which exhibit a linear relationship between one of the following kinetic or thermodynamic parameters for describing the reactions:[1]
