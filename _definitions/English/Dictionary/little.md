---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/little
offline_file: ""
offline_thumbnail: ""
uuid: 7b998060-5353-406c-ab54-9aab0980d4aa
updated: 1484310321
title: little
categories:
    - Dictionary
---
little
     adj 1: limited or below average in number or quantity or magnitude
            or extent; "a little dining room"; "a little house";
            "a small car"; "a little (or small) group"; "a small
            voice" [syn: {small}] [ant: {large}, {large}]
     2: (quantifier used with mass nouns) small in quantity or
        degree; not much or almost none or (with `a') at least
        some; "little rain fell in May"; "gave it little thought";
        "little hope remained"; "little time is left"; "we still
        have little money"; "a little hope remained"; "a little
        time is left" [syn: {little(a)}] [ant: {much(a)}]
     3: of short duration or distance; "a brief stay ...
