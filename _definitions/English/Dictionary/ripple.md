---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ripple
offline_file: ""
offline_thumbnail: ""
uuid: 5285e06d-a96b-4d70-9823-0a2e8751e6ef
updated: 1484310281
title: ripple
categories:
    - Dictionary
---
ripple
     n 1: a small wave on the surface of a liquid [syn: {rippling}, {riffle},
           {wavelet}]
     2: (electronics) an oscillation of small amplitude imposed on
        top of a steady value
     v 1: stir up (water) so as to form ripples [syn: {ruffle}, {riffle},
           {cockle}, {undulate}]
     2: flow in an irregular current with a bubbling noise;
        "babbling brooks" [syn: {babble}, {guggle}, {burble}, {bubble},
         {gurgle}]
