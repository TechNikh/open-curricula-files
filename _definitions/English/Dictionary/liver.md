---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liver
offline_file: ""
offline_thumbnail: ""
uuid: cb749415-ab5c-48bb-a3da-ef827c86fbce
updated: 1484310327
title: liver
categories:
    - Dictionary
---
liver
     n 1: large and complicated reddish-brown glandular organ located
          in the upper right portion of the abdominal cavity;
          secretes bile and functions in metabolism of protein and
          carbohydrate and fat; synthesizes substances involved in
          the clotting of the blood; synthesizes vitamin A;
          detoxifies poisonous substances and breaks down worn-out
          erythrocytes
     2: liver of an animal used as meat
     3: a person who has a special life style; "a high liver"
     4: someone who lives in a place; "a liver in cities"
