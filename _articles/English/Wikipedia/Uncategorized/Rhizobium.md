---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rhizobium
offline_file: ""
offline_thumbnail: ""
uuid: d9bc9ebf-9af5-4869-8d1c-8f6af935f168
updated: 1484308473
title: Rhizobium
tags:
    - History
    - Research
    - species
    - Phylogeny
categories:
    - Uncategorized
---
Rhizobium is a genus of Gram-negative soil bacteria that fix nitrogen. Rhizobium species form an endosymbiotic nitrogen-fixing association with roots of legumes and Parasponia.
