---
version: 1
type: video
provider: YouTube
id: 1kvFRFhdERo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c1ce6316-c307-4735-9679-75b14fe4bc26
updated: 1486069645
title: Mandelbrot zoom dans les spirale 10^48
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/1kvFRFhdERo/default.jpg
    - https://i3.ytimg.com/vi/1kvFRFhdERo/1.jpg
    - https://i3.ytimg.com/vi/1kvFRFhdERo/2.jpg
    - https://i3.ytimg.com/vi/1kvFRFhdERo/3.jpg
---
