---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/starkly
offline_file: ""
offline_thumbnail: ""
uuid: 396f0502-ae7d-47a0-a87d-3f6345c112fe
updated: 1484310455
title: starkly
categories:
    - Dictionary
---
starkly
     adv 1: in a stark manner; "He was starkly unable to achieve
            coherence"
     2: in sharp outline or contrast; "the black walls rose starkly
        from the snow"
     3: in a blunt manner; "in starkly realistic terms"
