---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bats
offline_file: ""
offline_thumbnail: ""
uuid: c3e9b2cf-d60f-4d78-86a3-d31649c4e3ba
updated: 1484310286
title: bats
categories:
    - Dictionary
---
bats
     adj : informal or slang terms for mentally irregular; "it used to
           drive my husband balmy" [syn: {balmy}, {barmy}, {batty},
            {bonkers}, {buggy}, {cracked}, {crackers}, {daft}, {dotty},
            {fruity}, {haywire}, {kooky}, {kookie}, {loco}, {loony},
            {loopy}, {nuts}, {nutty}, {round the bend}, {around
           the bend}, {wacky}, {whacky}]
