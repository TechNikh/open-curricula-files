---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anomalous
offline_file: ""
offline_thumbnail: ""
uuid: 061c49b3-0e1d-4194-ab91-d8ff08937d0f
updated: 1484310399
title: anomalous
categories:
    - Dictionary
---
anomalous
     adj : deviating from the general or common order or type;
           "advanced forms of life may be anomalous in the
           universe"
