---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/packed
offline_file: ""
offline_thumbnail: ""
uuid: c710f473-c483-4956-8437-4e2867130807
updated: 1484310365
title: packed
categories:
    - Dictionary
---
packed
     adj 1: filled to capacity; "a suitcase jammed with dirty clothes";
            "stands jam-packed with fans"; "a packed theater"
            [syn: {jammed}, {jam-pawncked}]
     2: pressed together or compressed; "packed snow"
