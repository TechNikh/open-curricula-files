---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Control_reconfiguration
offline_file: ""
offline_thumbnail: ""
uuid: c319776e-4336-427c-a824-4be338b89ea1
updated: 1484308727
title: Control reconfiguration
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/ReconfStructure.png
tags:
    - Reconfiguration problem
    - Fault modelling
    - Reconfiguration goals
    - Reconfiguration approaches
    - Fault hiding
    - Linear model following
    - Optimisation-based control schemes
    - Probabilistic approaches
    - Learning control
    - Mathematical tools and frameworks
categories:
    - Control engineering
---
Control reconfiguration is an active approach in control theory to achieve fault-tolerant control for dynamic systems.[1] It is used when severe faults, such as actuator or sensor outages, cause a break-up of the control loop, which must be restructured to prevent failure at the system level. In addition to loop restructuring, the controller parameters must be adjusted to accommodate changed plant dynamics. Control reconfiguration is a building block toward increasing the dependability of systems under feedback control.[2]
