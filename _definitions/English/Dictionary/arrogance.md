---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arrogance
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484518141
title: arrogance
categories:
    - Dictionary
---
arrogance
     n : overbearing pride evidenced by a superior manner toward
         inferiors [syn: {haughtiness}, {hauteur}, {highhandedness},
          {lordliness}]
