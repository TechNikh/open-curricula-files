---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sangli
offline_file: ""
offline_thumbnail: ""
uuid: 7d9b6389-ea04-4ecc-9fd5-73febf47df92
updated: 1484308455
title: Sangli
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sangli-ganpati-02.jpg
tags:
    - geography
    - History
    - Timeline of Sangli
    - Etymology
    - Sangli at present
    - Princely State of Sangli
    - Current governance
    - Civic officials and government body
    - Turmeric Production and Trade
    - Educational Institutes in Sangli
    - High schools
    - Tertiary education
    - Engineering colleges
    - Medical colleges
    - Management institutes
    - Pharmacy colleges
    - Other Colleges
    - Notable people
    - Sagareshwar Wildlife Sanctuary
    - Ganapati Temple
    - Nearby Religious Places
    - Food
    - Architecture and Places of Interest
    - Climate
    - Demographics
categories:
    - Uncategorized
---
Sangli ( pronunciation (help·info)) is a city and the district headquarters of Sangli District in the state of Maharashtra, in western India. It is known as the Turmeric City of Maharashtra due to its production and trade of the spice.[3] Sangli is situated on the banks of river Krishna and houses many sugar factories. The Ganesha Temple of Sangli is a historical landmark of the city and is visited by thousands of pilgrims.
