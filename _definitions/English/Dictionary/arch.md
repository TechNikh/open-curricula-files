---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arch
offline_file: ""
offline_thumbnail: ""
uuid: a83330fe-3f86-4481-9056-c5c72379c58e
updated: 1484310435
title: arch
categories:
    - Dictionary
---
arch
     adj 1: (of persons) highest in rank or authority or office; "his
            arch rival" [syn: {arch(a)}]
     2: (used of behavior or attitude) characteristic of those who
        treat others with condescension [syn: {condescending}, {patronizing},
         {patronising}]
     3: expert in skulduggery; "an arch criminal" [syn: {arch(a)}]
     n 1: a curved shape in the vertical plane that spans an opening
     2: a curved bony structure supporting or enclosing organs
        (especially arches of the feet)
     3: a passageway under an arch [syn: {archway}]
     4: (architecture) a masonry construction (usually curved) for
        spanning an opening and supporting the weight ...
