---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sympathy
offline_file: ""
offline_thumbnail: ""
uuid: 5158ce6e-fdaf-4739-9d7b-10a62cbe0792
updated: 1484310527
title: sympathy
categories:
    - Dictionary
---
sympathy
     n 1: an inclination to support or be loyal to or to agree with an
          opinion; "his sympathies were always with the underdog";
          "I knew I could count on his understanding" [syn: {understanding}]
     2: sharing the feelings of others (especially feelings of
        sorrow or anguish) [syn: {fellow feeling}]
     3: a relation of affinity or harmony between people; whatever
        affects one correspondingly affects the other; "the two of
        them were in close sympathy"
