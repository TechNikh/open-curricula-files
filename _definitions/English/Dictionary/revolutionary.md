---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revolutionary
offline_file: ""
offline_thumbnail: ""
uuid: c9fa6fa6-c44d-405e-93f7-d4518ccbff9f
updated: 1484310559
title: revolutionary
categories:
    - Dictionary
---
revolutionary
     adj 1: markedly new or introducing radical change; "a revolutionary
            discovery"; "radical political views" [syn: {radical}]
     2: relating to or having the nature of a revolution;
        "revolutionary wars"; "the Revolutionary era"
     3: advocating or engaged in revolution; "revolutionary
        pamphlets"; "a revolutionary junta" [ant: {counterrevolutionary}]
     n : a radical supporter of political or social revolution [syn:
         {revolutionist}, {subversive}, {subverter}]
