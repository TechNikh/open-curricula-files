---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spoke
offline_file: ""
offline_thumbnail: ""
uuid: 0cd817f4-7f38-4d85-89d9-e2400fbc2642
updated: 1484310202
title: spoke
categories:
    - Dictionary
---
speak
     v 1: express in speech; "She talks a lot of nonsense"; "This
          depressed patient does not verbalize" [syn: {talk}, {utter},
           {mouth}, {verbalize}, {verbalise}]
     2: exchange thoughts; talk with; "We often talk business";
        "Actions talk louder than words" [syn: {talk}]
     3: use language; "the baby talks already"; "the prisoner won't
        speak"; "they speak a strange dialect" [syn: {talk}]
     4: give a speech to; "The chairman addressed the board of
        trustees" [syn: {address}]
     5: make a characteristic or natural sound; "The drums spoke"
     [also: {spoken}, {spoke}]
