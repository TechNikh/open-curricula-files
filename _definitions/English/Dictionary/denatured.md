---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/denatured
offline_file: ""
offline_thumbnail: ""
uuid: 0d3d1256-894d-42cd-ab31-8600823f6030
updated: 1484310424
title: denatured
categories:
    - Dictionary
---
denatured
     adj : changed in nature or natural quality; "denatured alcohol"
           [syn: {denaturized}, {denaturised}]
