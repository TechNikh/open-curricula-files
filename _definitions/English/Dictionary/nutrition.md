---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nutrition
offline_file: ""
offline_thumbnail: ""
uuid: 17a18a0d-46c7-4a74-ab95-aef037cecb5b
updated: 1484310333
title: nutrition
categories:
    - Dictionary
---
nutrition
     n 1: (physiology) the organic process of nourishing or being
          nourished; the processes by which an organism
          assimilates food and uses it for growth and maintenance
     2: a source of materials to nourish the body [syn: {nutriment},
         {nourishment}, {sustenance}, {aliment}, {alimentation}, {victuals}]
     3: the scientific study of food and drink (especially in
        humans)
