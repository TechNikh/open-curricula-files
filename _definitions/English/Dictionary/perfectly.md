---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perfectly
offline_file: ""
offline_thumbnail: ""
uuid: dacff011-4cc8-4cb5-b350-7f7b2bd182d2
updated: 1484310416
title: perfectly
categories:
    - Dictionary
---
perfectly
     adv 1: completely and without qualification; used informally as
            intensifiers; "an absolutely magnificent painting"; "a
            perfectly idiotic idea"; "you're perfectly right";
            "utterly miserable"; "you can be dead sure of my
            innocence"; "was dead tired"; "dead right" [syn: {absolutely},
             {utterly}, {dead}]
     2: in a perfect or faultless way; "She performed perfectly on
        the balance beam"; "spoke English perfectly"; "solved the
        problem perfectly" [ant: {imperfectly}]
