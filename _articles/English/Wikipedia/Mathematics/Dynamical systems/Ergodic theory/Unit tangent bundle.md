---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unit_tangent_bundle
offline_file: ""
offline_thumbnail: ""
uuid: be818d26-0161-4196-8ff2-4da9ddfb72d5
updated: 1484308777
title: Unit tangent bundle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_7.png
categories:
    - Ergodic theory
---
In Riemannian geometry, a branch of mathematics, the unit tangent bundle of a Riemannian manifold (M, g), denoted by UT(M) or simply UTM, is the unit sphere bundle for the tangent bundle T(M). It is a fiber bundle over M whose fiber at each point is the unit sphere in the tangent bundle:
