---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/small-scale
offline_file: ""
offline_thumbnail: ""
uuid: b7d5ac51-7132-4926-bf36-81465127a10f
updated: 1484310459
title: small-scale
categories:
    - Dictionary
---
small-scale
     adj 1: created or drawn on a small scale; "small-scale maps"; "a
            small-scale model"
     2: limited in size or scope; "a small business"; "a newspaper
        with a modest circulation"; "small-scale plans"; "a
        pocket-size country" [syn: {minor}, {modest}, {small}, {pocket-size},
         {pocket-sized}]
