---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unadulterated
offline_file: ""
offline_thumbnail: ""
uuid: 9c7a76da-4548-43f8-acb2-e3c97c4a0831
updated: 1484310445
title: unadulterated
categories:
    - Dictionary
---
unadulterated
     adj : not mixed with impurities; "unadulterated maple syrup"; "the
           unadulterated truth"; "here is genius unadulterated"-
           Amy Loveman
