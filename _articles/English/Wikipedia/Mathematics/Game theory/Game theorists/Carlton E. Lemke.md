---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carlton_E._Lemke
offline_file: ""
offline_thumbnail: ""
uuid: 7151abf0-941e-442f-9524-10371bb7c89a
updated: 1484308512
title: Carlton E. Lemke
categories:
    - Game theorists
---
Lemke received his bachelor's degree in 1949 at the University of Buffalo and his PhD (Extremal problem in Linear Inequalities) in 1953 at Carnegie Mellon University (then Carnegie Institute of Technology). In 1952-1954 he was instructor at the Carnegie Institute of Technology and in 1954-55 at the Knolls Atomic Power Laboratory of General Electric. In 1955-56 he was an engineer at the Radio Corporation of America in New Jersey. From 1956 he was assistant professor and later professor at the Rensselaer Polytechnic Institute. Since 1967, he was there Ford Foundation Professor of Mathematics.
