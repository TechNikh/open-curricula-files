---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/underwater
offline_file: ""
offline_thumbnail: ""
uuid: db1e2537-f79a-4fff-a656-0e07728ab6e5
updated: 1484310170
title: underwater
categories:
    - Dictionary
---
underwater
     adj 1: beneath the surface of the water; "submerged rocks" [syn: {submerged},
             {submersed}]
     2: growing or remaining under water; "viewing subaqueous fauna
        from a glass-bottomed boat"; "submerged leaves" [syn: {subaqueous},
         {subaquatic}, {submerged}, {submersed}]
