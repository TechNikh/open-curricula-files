---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assigning
offline_file: ""
offline_thumbnail: ""
uuid: c98ed559-947c-432e-9d8e-6ee0d4f8248c
updated: 1484310401
title: assigning
categories:
    - Dictionary
---
assigning
     n : the act of distributing something to designated places or
         persons; "the first task is the assignment of an address
         to each datum" [syn: {assignment}]
