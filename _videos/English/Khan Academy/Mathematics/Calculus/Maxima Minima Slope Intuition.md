---
version: 1
type: video
provider: YouTube
id: tpHz0gZfVss
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Maxima%20Minima%20Slope%20Intuition.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 80e3b3bf-8764-4fb7-bc0b-409dc39d836f
updated: 1486069693
title: Maxima Minima Slope Intuition
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/tpHz0gZfVss/default.jpg
    - https://i3.ytimg.com/vi/tpHz0gZfVss/1.jpg
    - https://i3.ytimg.com/vi/tpHz0gZfVss/2.jpg
    - https://i3.ytimg.com/vi/tpHz0gZfVss/3.jpg
---
