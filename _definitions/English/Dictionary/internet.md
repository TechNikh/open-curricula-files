---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/internet
offline_file: ""
offline_thumbnail: ""
uuid: 951c3e83-e3b5-4a79-8d00-6398102140ac
updated: 1484310369
title: internet
categories:
    - Dictionary
---
Internet
     n : a computer network consisting of a worldwide network of
         computer networks that use the TCP/IP network protocols
         to facilitate data transmission and exchange [syn: {Net},
          {cyberspace}]
