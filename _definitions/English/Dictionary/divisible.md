---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/divisible
offline_file: ""
offline_thumbnail: ""
uuid: 0a3bc925-c3b2-443d-8bbf-d40fbd0b50b3
updated: 1484310154
title: divisible
categories:
    - Dictionary
---
divisible
     adj : capable of being or liable to be divided or separated; "even
           numbers are divisible by two"; "the Americans fought a
           bloody war to prove that their nation is not divisible"
           [ant: {indivisible}]
