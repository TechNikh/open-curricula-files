---
version: 1
type: video
provider: YouTube
id: ZFUgy3crCYY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c1abcc85-e0a6-4d9f-9e50-b54da839b4d2
updated: 1486069685
title: 'Venus- Crash Course Astronomy #14'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZFUgy3crCYY/default.jpg
    - https://i3.ytimg.com/vi/ZFUgy3crCYY/1.jpg
    - https://i3.ytimg.com/vi/ZFUgy3crCYY/2.jpg
    - https://i3.ytimg.com/vi/ZFUgy3crCYY/3.jpg
---
