---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saturated
offline_file: ""
offline_thumbnail: ""
uuid: 90fa038f-8fb0-4664-952e-bb5f2c1fbe96
updated: 1484310226
title: saturated
categories:
    - Dictionary
---
saturated
     adj 1: being the most concentrated solution possible at a given
            temperature; unable to dissolve still more of a
            substance; "a saturated solution" [syn: {concentrated}]
            [ant: {unsaturated}]
     2: wet through and through; thoroughly wet; "stood at the door
        drenched (or soaked) by the rain"; "a shirt saturated with
        perspiration"; "his shoes were sopping (or soaking)"; "the
        speaker's sodden collar"; "soppy clothes" [syn: {drenched},
         {soaked}, {soaking}, {sodden}, {sopping}, {soppy}]
     3: used especially of organic compounds; having all available
        valence bonds filled; "saturated fats" [ant: ...
