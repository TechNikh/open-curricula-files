---
version: 1
type: video
provider: YouTube
id: 7akhBG9kvh4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ea73d22f-1d2e-459a-9ef4-9beeb422b0c3
updated: 1486069643
title: 'DMT RMX - Trippy Psychedelic Fractal Simulation (Terence McKenna voices) - HD'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/7akhBG9kvh4/default.jpg
    - https://i3.ytimg.com/vi/7akhBG9kvh4/1.jpg
    - https://i3.ytimg.com/vi/7akhBG9kvh4/2.jpg
    - https://i3.ytimg.com/vi/7akhBG9kvh4/3.jpg
---
