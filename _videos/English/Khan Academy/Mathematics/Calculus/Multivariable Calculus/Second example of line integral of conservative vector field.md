---
version: 1
type: video
provider: YouTube
id: LpY8Qa3IP1w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Second%20example%20of%20line%20integral%20of%20conservative%20vector%20field.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0797b1a7-4e84-4359-b7eb-736128003eeb
updated: 1486069696
title: Second example of line integral of conservative vector field
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/LpY8Qa3IP1w/default.jpg
    - https://i3.ytimg.com/vi/LpY8Qa3IP1w/1.jpg
    - https://i3.ytimg.com/vi/LpY8Qa3IP1w/2.jpg
    - https://i3.ytimg.com/vi/LpY8Qa3IP1w/3.jpg
---
