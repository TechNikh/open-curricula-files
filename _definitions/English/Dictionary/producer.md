---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/producer
offline_file: ""
offline_thumbnail: ""
uuid: 6f6ed274-c401-429e-9c34-942d3a7f28a6
updated: 1484310273
title: producer
categories:
    - Dictionary
---
producer
     n 1: someone who manufactures something [syn: {manufacturer}]
     2: someone who finds financing for and supervises the making
        and presentation of a show (play or film or program or
        similar work)
     3: something that produces; "Maine is a leading producer of
        potatoes"; "this microorganism is a producer of disease"
