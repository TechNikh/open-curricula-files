---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hydrostatic_stress
offline_file: ""
offline_thumbnail: ""
uuid: 18c98904-d3d6-43c4-828e-58d2bcbea58e
updated: 1484309293
title: Hydrostatic stress
categories:
    - Orientation (geometry)
---
In continuum mechanics, a hydrostatic stress is an isotropic stress that is given by the weight of water above a certain point. It is often used interchangeably with "pressure" and is also known as confining stress, particularly in the field of geomechanics. Its magnitude 
  
    
      
        
          σ
          
            h
          
        
      
    
    {\displaystyle \sigma _{h}}
  
 can be given by:
