---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diverge
offline_file: ""
offline_thumbnail: ""
uuid: 68281346-26c2-4474-bda1-6f78b1a4f4df
updated: 1484310220
title: diverge
categories:
    - Dictionary
---
diverge
     v 1: move or draw apart; "The two paths diverge here" [ant: {converge}]
     2: have no limits as a mathematical series [ant: {converge}, {converge}]
     3: extend in a different direction; "The lines start to diverge
        here"; "Their interests diverged" [ant: {converge}]
     4: be at variance with; be out of line with [syn: {deviate}, {vary},
         {depart}] [ant: {conform}]
