---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_Pardon
offline_file: ""
offline_thumbnail: ""
uuid: 5189c7c8-7292-4166-b82e-22d6588d3acd
updated: 1484309096
title: John Pardon
tags:
    - Education and accomplishments
    - Selected publications
categories:
    - Geometers
---
John Vincent Pardon (born June 1989, in Chapel Hill, North Carolina) is an American mathematician who works on geometry and topology.[1] He is primarily known for having solved Gromov's problem on distortion of knots, for which he was awarded the 2012 Morgan Prize. He is a professor at Princeton University.
