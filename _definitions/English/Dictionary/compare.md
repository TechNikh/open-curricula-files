---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compare
offline_file: ""
offline_thumbnail: ""
uuid: a4098340-bbfc-454b-9bb2-9122d5bfac44
updated: 1484310327
title: compare
categories:
    - Dictionary
---
compare
     n : qualities that are comparable; "no comparison between the
         two books"; "beyond compare" [syn: {comparison}, {equivalence},
          {comparability}]
     v 1: examine and note the similarities or differences of; "John
          compared his haircut to his friend's"; "We compared
          notes after we had both seen the movie"
     2: be comparable; "This car does not compare with our line of
        Mercedes"
     3: consider or describe as similar, equal, or analogous; "We
        can compare the Han dynasty to the Romans"; "You cannot
        equate success in financial matters with greed" [syn: {liken},
         {equate}]
     4: to form the comparative or ...
