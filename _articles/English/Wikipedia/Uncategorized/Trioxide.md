---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trioxide
offline_file: ""
offline_thumbnail: ""
uuid: 1be6f47c-9272-4367-a337-1f6f233ff7ce
updated: 1484308317
title: Trioxide
tags:
    - List of trioxides
    - MO3
    - M2O3
    - Other trioxides
categories:
    - Uncategorized
---
A trioxide is a compound with three oxygen atoms. For metals with the M2O3 formula there are several common structures. Al2O3, Cr2O3, Fe2O3, and V2O3 adopt the corundum structure. Many rare earth oxides adopt the "A-type rare earth structure" which is hexagonal. Several others plus indium oxide adopt the "C-type rare earth structure", also called "bixbyite", which is cubic and related to the fluorite structure.[1]
