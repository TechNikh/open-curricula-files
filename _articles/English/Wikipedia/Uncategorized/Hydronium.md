---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hydronium
offline_file: ""
offline_thumbnail: ""
uuid: 569b54cf-a197-4c44-97ac-15f519e963b8
updated: 1484308312
title: Hydronium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Zundel-cation.JPG
tags:
    - Determination of pH
    - Nomenclature
    - Structure
    - Acids and acidity
    - Solvation
    - Solid hydronium salts
    - Interstellar H3O+
    - Motivation for study
    - Interstellar chemistry
    - Astronomical detections
categories:
    - Uncategorized
---
In chemistry, hydronium is the common name for the aqueous cation H
3O+, the type of oxonium ion produced by protonation of water. It is the positive ion present when an Arrhenius acid is dissolved in water, as Arrhenius acid molecules in solution give up a proton (a positive hydrogen ion, H+) to the surrounding water molecules (H2O).
