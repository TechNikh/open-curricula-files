---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Siltation
offline_file: ""
offline_thumbnail: ""
uuid: d16d574d-8db1-4224-a7e0-878659016f84
updated: 1484308340
title: Siltation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Dumping_of_faecal_sludge_into_the_river.jpg
tags:
    - Causes
    - Vulnerabilities
    - Measurement and monitoring
    - mitigation
categories:
    - Uncategorized
---
Siltation or siltification is the pollution of water by particulate terrestrial clastic material, with a particle size dominated by silt or clay. It refers both to the increased concentration of suspended sediments, and to the increased accumulation (temporary or permanent) of fine sediments on bottoms where they are undesirable. Siltation is most often caused by soil erosion or sediment spill.
