---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Fair_division_of_a_single_homogeneous_resource
offline_file: ""
offline_thumbnail: ""
uuid: bdfd7a19-c14f-4d4f-b9e9-2de0e7ee9384
updated: 1484308524
title: Fair division of a single homogeneous resource
tags:
    - Setting
    - Allocation rules
    - Envy-free
    - Utilitarian
    - Egalitarian
    - Examples
    - Common utility and unequal endowments
    - Constant utility ratios
    - Properties of allocation rules
categories:
    - Fair division
---
Fair division of a single homogeneous resource is one of the simplest settings in fair division problems. There is a single resource that should be divided between several people. The challenge is that each person derives a different utility from each amount of the resource. Hence, there are several conflicting principles for deciding how the resource should be divided. A primary conflict is between efficiency and equality. Efficiency is represented by the utilitarian rule, which maximizes the sum of utilities; equality is represented by the egalitarian rule, which maximizes the minimum utility. [1]:sub.2.5
