---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/The_Chemical_Basis_of_Morphogenesis
offline_file: ""
offline_thumbnail: ""
uuid: ee51b10a-f7b1-4644-8d47-03a0b9b4e07f
updated: 1484308712
title: The Chemical Basis of Morphogenesis
tags:
    - Reaction–diffusion systems
categories:
    - Chaos theory
---
"The Chemical Basis of Morphogenesis" is an article written by the English mathematician Alan Turing in 1952 describing the way in which non-uniformity (natural patterns such as stripes, spots and spirals) may arise naturally out of a homogeneous, uniform state.[1] The theory (which can be called a reaction–diffusion theory of morphogenesis) has served as a basic model in theoretical biology.[2]
