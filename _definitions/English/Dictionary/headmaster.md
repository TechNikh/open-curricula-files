---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/headmaster
offline_file: ""
offline_thumbnail: ""
uuid: bd871be7-c152-41f0-bf3f-97d71702b181
updated: 1484310150
title: headmaster
categories:
    - Dictionary
---
headmaster
     n : presiding officer of a school [syn: {schoolmaster}, {master}]
