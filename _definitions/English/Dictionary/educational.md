---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/educational
offline_file: ""
offline_thumbnail: ""
uuid: 8402e7f5-4fe4-4ac1-a190-7fe51a83ed06
updated: 1484310447
title: educational
categories:
    - Dictionary
---
educational
     adj 1: relating to the process of education; "educational
            psychology"
     2: providing knowledge; "an educational film"
