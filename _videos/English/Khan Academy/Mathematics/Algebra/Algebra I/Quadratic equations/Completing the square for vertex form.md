---
version: 1
type: video
provider: YouTube
id: 02h9yhc7ruc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Quadratic%20equations/Completing%20the%20square%20for%20vertex%20form.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f3fef60b-6494-4159-be9b-05c7c0637634
updated: 1486069557
title: Completing the square for vertex form
categories:
    - Quadratic equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/02h9yhc7ruc/default.jpg
    - https://i3.ytimg.com/vi/02h9yhc7ruc/1.jpg
    - https://i3.ytimg.com/vi/02h9yhc7ruc/2.jpg
    - https://i3.ytimg.com/vi/02h9yhc7ruc/3.jpg
---
