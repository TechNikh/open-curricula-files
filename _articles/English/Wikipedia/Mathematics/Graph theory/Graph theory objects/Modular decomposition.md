---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Modular_decomposition
offline_file: ""
offline_thumbnail: ""
uuid: 6b50f1d1-52ac-4070-b050-4f63a0e76b51
updated: 1484309445
title: Modular decomposition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-ModularDecomposition.png
tags:
    - Modules
    - Modular quotients and factors
    - The modular decomposition
    - Algorithmic issues
    - Generalizations
categories:
    - Graph theory objects
---
In graph theory, the modular decomposition is a decomposition of a graph into subsets of vertices called modules. A module is a generalization of a connected component of a graph. Unlike connected components, however, one module can be a proper subset of another. Modules therefore lead to a recursive (hierarchical) decomposition of the graph, instead of just a partition.
