---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/destroyed
offline_file: ""
offline_thumbnail: ""
uuid: 050256e7-3d14-45ad-a13f-8a02102b02b6
updated: 1484310328
title: destroyed
categories:
    - Dictionary
---
destroyed
     adj 1: spoiled or ruined or demolished; "war left many cities
            destroyed"; "Alzheimer's is responsible for her
            destroyed mind" [ant: {preserved}]
     2: destroyed physically or morally [syn: {ruined}]
