---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/K%C3%A4hler_differential'
offline_file: ""
offline_thumbnail: ""
uuid: 6f92b625-e520-470a-a85e-ea68507bd63a
updated: 1484309156
title: Kähler differential
tags:
    - Presentation
    - Construction
    - Use in algebraic geometry
    - Use in algebraic number theory
categories:
    - Algebraic geometry
---
