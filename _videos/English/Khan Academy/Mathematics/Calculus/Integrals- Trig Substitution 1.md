---
version: 1
type: video
provider: YouTube
id: n4EK92CSuBE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Integrals-%20Trig%20Substitution%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d01f116b-d5e2-42c7-a56d-49c3330f4c73
updated: 1486069694
title: 'Integrals- Trig Substitution 1'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/n4EK92CSuBE/default.jpg
    - https://i3.ytimg.com/vi/n4EK92CSuBE/1.jpg
    - https://i3.ytimg.com/vi/n4EK92CSuBE/2.jpg
    - https://i3.ytimg.com/vi/n4EK92CSuBE/3.jpg
---
