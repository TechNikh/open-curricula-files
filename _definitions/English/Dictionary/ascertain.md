---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ascertain
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484462641
title: ascertain
categories:
    - Dictionary
---
ascertain
     v 1: after a calculation, investigation, experiment, survey, or
          study; "find the product of two numbers"; "The physicist
          who found the elusive particle won the Nobel Prize"
          [syn: {determine}, {find}, {find out}]
     2: be careful or certain to do something; make certain of
        something; "He verified that the valves were closed"; "See
        that the curtains are closed"; "control the quality of the
        product" [syn: {see}, {check}, {insure}, {see to it}, {ensure},
         {control}, {assure}]
     3: find out, learn, or determine with certainty, usually by
        making an inquiry or other effort; "I want to see whether
        she ...
