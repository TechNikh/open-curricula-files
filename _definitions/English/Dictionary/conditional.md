---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conditional
offline_file: ""
offline_thumbnail: ""
uuid: 8c1702d5-88c0-4d24-9f68-f4a1fa53a165
updated: 1484310163
title: conditional
categories:
    - Dictionary
---
conditional
     adj 1: qualified by reservations
     2: imposing or depending on or containing a condition;
        "conditional acceptance of the terms"; "lent conditional
        support"; "the conditional sale will not be complete until
        the full purchase price is paid" [ant: {unconditional}]
