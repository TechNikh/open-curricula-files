---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mountain
offline_file: ""
offline_thumbnail: ""
uuid: f2ba9a5e-d7d8-40f1-825a-4b5f3c14142d
updated: 1484310273
title: mountain
categories:
    - Dictionary
---
mountain
     adj : relating to or located in mountains; "mountain people" [syn:
            {mountain(a)}]
     n 1: a land mass that projects well above its surroundings;
          higher than a hill [syn: {mount}]
     2: a large number or amount; "made lots of new friends"; "she
        amassed a mountain of newspapers" [syn: {tons}, {dozens},
        {heaps}, {lots}, {piles}, {scores}, {stacks}, {loads}, {rafts},
         {slews}, {wads}, {oodles}, {gobs}, {scads}, {lashings}]
