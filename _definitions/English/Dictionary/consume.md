---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consume
offline_file: ""
offline_thumbnail: ""
uuid: 59c4e659-e0da-4edd-b8b2-2648030fd449
updated: 1484310351
title: consume
categories:
    - Dictionary
---
consume
     v 1: eat immoderately; "Some people can down a pound of meat in
          the course of one meal" [syn: {devour}, {down}, {go
          through}]
     2: serve oneself to, or consume regularly; "Have another bowl
        of chicken soup!"; "I don't take sugar in my coffee" [syn:
         {ingest}, {take in}, {take}, {have}] [ant: {abstain}]
     3: spend extravagantly; "waste not, want not" [syn: {squander},
         {waste}, {ware}]
     4: destroy completely; "The fire consumed the building"
     5: use up (resources or materials); "this car consumes a lot of
        gas"; "We exhausted our savings"; "They run through 20
        bottles of wine a week" [syn: {eat up}, {use ...
