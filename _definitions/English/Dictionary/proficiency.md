---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proficiency
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484641081
title: proficiency
categories:
    - Dictionary
---
proficiency
     n 1: the quality of having great facility and competence
     2: skillfulness in the command of fundamentals deriving from
        practice and familiarity; "practice greatly improves
        proficiency" [syn: {technique}]
