---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Porous_set
offline_file: ""
offline_thumbnail: ""
uuid: 0aedd0d1-cd7f-4840-8303-df8de7fee248
updated: 1484309272
title: Porous set
categories:
    - Metric geometry
---
In mathematics, a porous set is a concept in the study of metric spaces. Like the concepts of meagre and measure zero sets, a porous set can be considered "sparse" or "lacking bulk"; however, porous sets are not equivalent to either meagre sets or measure zero sets, as shown below.
