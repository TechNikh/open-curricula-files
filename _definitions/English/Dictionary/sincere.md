---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sincere
offline_file: ""
offline_thumbnail: ""
uuid: ad1f94b9-3301-4247-9a6e-da172c01cc84
updated: 1484310583
title: sincere
categories:
    - Dictionary
---
sincere
     adj 1: open and genuine; not deceitful; "he was a good man, decent
            and sincere"; "felt sincere regret that they were
            leaving"; "sincere friendship" [ant: {insincere}]
     2: characterized by a firm and humorless belief in the validity
        of your opinions; "both sides were deeply in earnest, even
        passionate"; "an entirely sincere and cruel tyrant"; "a
        film with a solemn social message" [syn: {earnest}, {in
        earnest(p)}, {solemn}]
