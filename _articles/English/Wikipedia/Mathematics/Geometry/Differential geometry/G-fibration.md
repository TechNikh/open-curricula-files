---
version: 1
type: article
id: https://en.wikipedia.org/wiki/G-fibration
offline_file: ""
offline_thumbnail: ""
uuid: 5252afe5-1674-415a-9e91-897b6d5b602d
updated: 1484309221
title: G-fibration
categories:
    - Differential geometry
---
In algebraic topology, a G-fibration or principal fibration is a generalization of a principal G-bundle, just as a fibration is a generalization of a fiber bundle. By definition,[1] given a topological monoid G, a G-fibration is a fibration p: P→B together with a continuous right monoid action P × G → P such that
