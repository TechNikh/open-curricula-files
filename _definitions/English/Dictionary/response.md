---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/response
offline_file: ""
offline_thumbnail: ""
uuid: ee7d8e37-5b49-45d4-88e6-a6fb585ef269
updated: 1484310321
title: response
categories:
    - Dictionary
---
response
     n 1: a result; "this situation developed in response to events in
          Africa"
     2: a bodily process occurring due to the effect of some
        foregoing stimulus or agent; "a bad reaction to the
        medicine"; "his responses have slowed with age" [syn: {reaction}]
     3: a statement (either spoken or written) that is made in reply
        to a question or request or criticism or accusation; "I
        waited several days for his answer"; "he wrote replies to
        several of his critics" [syn: {answer}, {reply}]
     4: the manner in which something is greeted; "she did not
        expect the cold reception she received from her superiors"
        [syn: ...
