---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/percolation
offline_file: ""
offline_thumbnail: ""
uuid: b6b9b8d7-2204-4605-9101-a8ba57dff505
updated: 1484310264
title: percolation
categories:
    - Dictionary
---
percolation
     n 1: the slow passage of a liquid through a filtering medium;
          "the percolation of rainwater through the soil"; "the
          infiltration of seawater through the lava" [syn: {infiltration}]
     2: the act of making coffee in a percolator
     3: the filtration of a liquid for extraction or purification
