---
version: 1
type: article
id: https://en.wikipedia.org/wiki/2-factor_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 31206792-efad-4054-86ca-12f471fdfa7c
updated: 1484309456
title: 2-factor theorem
categories:
    - Theorems in graph theory
---
In the mathematical discipline of graph theory, 2-factor theorem discovered by Julius Petersen, is one of the earliest works in graph theory and can be stated as follows:
