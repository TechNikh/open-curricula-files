---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Port_and_starboard
offline_file: ""
offline_thumbnail: ""
uuid: 18b3e43c-c119-4b68-919d-e1ce3a0e2bad
updated: 1484309299
title: Port and starboard
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Tapisserie_bato1.jpg
categories:
    - Orientation (geometry)
---
Port and starboard are nautical terms for left and right, respectively. Port is the left-hand side of or direction from a vessel, facing forward. Starboard is the right-hand side, facing forward. Since port and starboard never change, they are unambiguous references that are not relative to the observer.[2][3]
