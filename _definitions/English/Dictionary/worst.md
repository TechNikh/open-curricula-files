---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worst
offline_file: ""
offline_thumbnail: ""
uuid: 9212412c-f662-4e4e-8f57-ba62a4dfa331
updated: 1484310471
title: worst
categories:
    - Dictionary
---
worst
     adj : (superlative of `bad') most wanting in quality or value or
           condition; "the worst player on the team"; "the worst
           weather of the year" [ant: {best}]
     n 1: the least favorable outcome; "the worst that could happen"
     2: the greatest damage or wickedness of which one is capable;
        "the invaders did their worst"; "so pure of heart that his
        worst is another man's best"
     3: the weakest effort or poorest achievement one is capable of;
        "it was the worst he had ever done on a test" [ant: {best}]
     adv : to the highest degree of inferiority or badness; "She
           suffered worst of all"; "schools were the worst hit by
     ...
