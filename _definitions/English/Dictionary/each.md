---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/each
offline_file: ""
offline_thumbnail: ""
uuid: 79bacc1f-98bc-4e8e-b4d7-1111b953426b
updated: 1484310359
title: each
categories:
    - Dictionary
---
each
     adj : (used of count nouns) every one considered individually;
           "each person is mortal"; "each party is welcome" [syn:
           {each(a)}]
     adv : to or from every one of two or more (considered
           individually); "they received $10 each" [syn: {to each
           one}, {for each one}, {from each one}, {apiece}]
