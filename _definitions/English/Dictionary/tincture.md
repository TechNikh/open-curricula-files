---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tincture
offline_file: ""
offline_thumbnail: ""
uuid: dddfcb9e-cf3e-407e-9409-b75befab860f
updated: 1484310341
title: tincture
categories:
    - Dictionary
---
tincture
     n 1: a substances that colors metals
     2: an indication that something has been present; "there wasn't
        a trace of evidence for the claim"; "a tincture of
        condescension" [syn: {trace}, {vestige}, {shadow}]
     3: a quality of a given color that differs slightly from a
        primary color; "after several trials he mixed the shade of
        pink that she wanted" [syn: {shade}, {tint}, {tone}]
     4: (pharmacology) a medicine consisting of an extract in an
        alcohol solution
     v 1: fill, as with a certain quality; "The heavy traffic
          tinctures the air with carbon monoxide" [syn: {impregnate},
           {infuse}, {instill}]
     2: stain ...
