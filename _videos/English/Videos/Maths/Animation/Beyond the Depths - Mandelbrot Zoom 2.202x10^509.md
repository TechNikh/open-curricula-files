---
version: 1
type: video
provider: YouTube
id: b583L3TfzZ0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 398f0c50-213a-4c78-a3f6-36496180de0d
updated: 1486069647
title: 'Beyond the Depths - Mandelbrot Zoom 2.202x10^509'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/b583L3TfzZ0/default.jpg
    - https://i3.ytimg.com/vi/b583L3TfzZ0/1.jpg
    - https://i3.ytimg.com/vi/b583L3TfzZ0/2.jpg
    - https://i3.ytimg.com/vi/b583L3TfzZ0/3.jpg
---
