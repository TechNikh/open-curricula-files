---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bilingual
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441221
title: bilingual
categories:
    - Dictionary
---
bilingual
     adj : using or knowing two languages; "bilingual education"
     n : a person who speaks two languages fluently
