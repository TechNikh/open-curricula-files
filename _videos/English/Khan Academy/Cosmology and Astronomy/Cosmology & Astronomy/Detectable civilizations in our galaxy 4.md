---
version: 1
type: video
provider: YouTube
id: T5DGZIsfK-0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Detectable%20civilizations%20in%20our%20galaxy%204.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bdbb5e38-fa37-44df-9666-78da84d49d6e
updated: 1486069685
title: Detectable civilizations in our galaxy 4
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/T5DGZIsfK-0/default.jpg
    - https://i3.ytimg.com/vi/T5DGZIsfK-0/1.jpg
    - https://i3.ytimg.com/vi/T5DGZIsfK-0/2.jpg
    - https://i3.ytimg.com/vi/T5DGZIsfK-0/3.jpg
---
