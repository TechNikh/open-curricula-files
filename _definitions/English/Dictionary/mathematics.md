---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mathematics
offline_file: ""
offline_thumbnail: ""
uuid: 6194ad34-8b06-494e-8008-f6ce7890993b
updated: 1484310156
title: mathematics
categories:
    - Dictionary
---
mathematics
     n : a science (or group of related sciences) dealing with the
         logic of quantity and shape and arrangement [syn: {math},
          {maths}]
