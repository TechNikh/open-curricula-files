---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/balancing
offline_file: ""
offline_thumbnail: ""
uuid: 4ae7251d-7a46-49b3-a726-962f32261160
updated: 1484310371
title: balancing
categories:
    - Dictionary
---
balancing
     n : getting two things to correspond; "the reconciliation of his
         checkbook and the bank statement" [syn: {reconciliation}]
