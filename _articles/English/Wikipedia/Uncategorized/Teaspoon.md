---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tea_spoon
offline_file: ""
offline_thumbnail: ""
uuid: 2be4917d-19a9-4856-9f06-10e24ccb2cef
updated: 1484308366
title: Teaspoon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Teaspoon.jpg
tags:
    - Cutlery
    - Measure of volume
    - History
    - United States customary teaspoon
    - Metric teaspoon
    - Alternative definitions
categories:
    - Uncategorized
---
