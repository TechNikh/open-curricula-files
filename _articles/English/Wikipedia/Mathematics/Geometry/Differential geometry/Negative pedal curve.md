---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Negative_pedal_curve
offline_file: ""
offline_thumbnail: ""
uuid: 9f9a0af6-679f-4fc2-8ec3-b36f6ef31db2
updated: 1484309230
title: Negative pedal curve
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/NegativePedal.gif
tags:
    - Definition
    - Parameterization
    - Properties
categories:
    - Differential geometry
---
In geometry, a negative pedal curve is a plane curve that can be constructed from another plane curve C and a fixed point P on that curve. For each point X ≠ P on the curve C, the negative pedal curve has a tangent that passes through X and is perpendicular to line XP. Constructing the negative pedal curve is the inverse operation to constructing a pedal curve.
