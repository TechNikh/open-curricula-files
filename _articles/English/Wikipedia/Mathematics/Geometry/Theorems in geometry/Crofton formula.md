---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crofton_formula
offline_file: ""
offline_thumbnail: ""
uuid: 57a4af00-c71a-4bc0-85e3-683dbdd25bec
updated: 1484309114
title: Crofton formula
tags:
    - Statement
    - Proof Sketch
    - Other forms
    - Applications
categories:
    - Theorems in geometry
---
In mathematics, the Crofton formula, named after Morgan Crofton (1826–1915), is a classic result of integral geometry relating the length of a curve to the expected number of times a "random" line intersects it.
