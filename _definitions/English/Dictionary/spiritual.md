---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spiritual
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484518021
title: spiritual
categories:
    - Dictionary
---
spiritual
     adj 1: concerned with sacred matters or religion or the church;
            "religious texts"; "a nenber if a religious order";
            "lords temporal and spiritual"; "spiritual leaders";
            "spiritual songs" [syn: {religious}]
     2: concerned with or affecting the spirit or soul; "a spiritual
        approach to life"; "spiritual fulfillment"; "spiritual
        values"; "unearthly love" [syn: {unearthly}]
     3: lacking material body or form or substance; "spiritual
        beings"; "the vital transcendental soul belonging to the
        spiritual realm"-Lewis Mumford
     4: like or being a phantom; "a ghostly face at the window"; "a
        phantasmal ...
