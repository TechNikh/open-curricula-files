---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poincar%C3%A9_plot'
offline_file: ""
offline_thumbnail: ""
uuid: 33d407eb-9ad3-47ea-8cdd-c76d02da37fa
updated: 1484308709
title: Poincaré plot
categories:
    - Chaos theory
---
A Poincaré plot, named after Henri Poincaré, is used to quantify self-similarity in processes, usually periodic functions. It is also known as a return map.[1][2] Poincaré plots can be used to distinguish chaos from randomness by embedding a data set into a higher-dimensional state space.
