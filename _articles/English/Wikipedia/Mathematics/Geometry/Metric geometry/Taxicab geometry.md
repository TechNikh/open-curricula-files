---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Taxicab_geometry
offline_file: ""
offline_thumbnail: ""
uuid: a5a26f45-d4d3-432b-af95-029417c26853
updated: 1484309273
title: Taxicab geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Manhattan_distance.svg_0.png
tags:
    - Formal definition
    - Properties
    - Circles
    - Applications
    - Measures of distances in chess
    - Compressed sensing
    - Differences of frequency distributions
    - Notes
categories:
    - Metric geometry
---
Taxicab geometry, considered by Hermann Minkowski in 19th-century Germany, is a form of geometry in which the usual distance function of metric or Euclidean geometry is replaced by a new metric in which the distance between two points is the sum of the absolute differences of their Cartesian coordinates.
