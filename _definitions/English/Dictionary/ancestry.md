---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ancestry
offline_file: ""
offline_thumbnail: ""
uuid: 4f7a55d4-d686-4f65-a0f6-ceba399e32de
updated: 1484310283
title: ancestry
categories:
    - Dictionary
---
ancestry
     n 1: the descendants of one individual; "his entire lineage has
          been warriors" [syn: {lineage}, {line}, {line of descent},
           {descent}, {bloodline}, {blood line}, {blood}, {pedigree},
           {origin}, {parentage}, {stemma}, {stock}]
     2: inherited properties shared with others of your bloodline
        [syn: {lineage}, {derivation}, {filiation}]
