---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maximal_independent_set
offline_file: ""
offline_thumbnail: ""
uuid: 26f823a3-1862-4500-a592-dda377050278
updated: 1484309392
title: Maximal independent set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Cube-maximal-independence.svg.png
tags:
    - Definition
    - Related vertex sets
    - Graph family characterizations
    - Bounding the number of sets
    - Finding a single maximal independent set
    - Sequential algorithm
    - Random-selection parallel algorithm
    - Random-priority parallel algorithm
    - Random-permutation parallel algorithm
    - Listing all maximal independent sets
    - Parallelization of finding maximum independent sets
    - History
    - Complexity class
    - Communication and data exchange
    - Notes
categories:
    - Computational problems in graph theory
---
In graph theory, a maximal independent set (MIS) or maximal stable set is an independent set that is not a subset of any other independent set. In other words, there is no vertex outside the independent set that may join it because it is maximal with respect to the independent set property.
