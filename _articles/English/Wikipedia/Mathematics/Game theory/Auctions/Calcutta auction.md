---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Calcutta_auction
offline_file: ""
offline_thumbnail: ""
uuid: e5fecb30-f0bf-4c8a-9dff-a921163e3b79
updated: 1484309011
title: Calcutta auction
categories:
    - Auctions
---
A Calcutta auction is an open auction held in conjunction with a golf tournament,[1] horse race or similar contest with multiple entrants. It is popular in backgammon, the Melbourne Cup, and college basketball pools during March Madness[2] (originally in Calcutta, India, from where this technique was first recorded by the Colonial British) bid among themselves to "buy" each of the contestants, with each contestant being assigned to the highest bidder. The contestant will then pay out to the owner a predetermined proportion of the pool depending on how it performs in the tournament. While variations in payoff schedules exist, in an NCAA Basketball tournament (64 teams, single elimination) ...
