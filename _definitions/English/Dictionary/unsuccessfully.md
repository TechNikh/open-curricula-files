---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unsuccessfully
offline_file: ""
offline_thumbnail: ""
uuid: 4937c407-f525-4bfe-82b9-e2ebd7258b23
updated: 1484310180
title: unsuccessfully
categories:
    - Dictionary
---
unsuccessfully
     adv : without success; "she tried unsuccessfully to persuade him
           to buy a new car" [ant: {successfully}]
