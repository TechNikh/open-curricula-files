---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Renu_C._Laskar
offline_file: ""
offline_thumbnail: ""
uuid: 036ad7af-5b16-44c1-a139-ca4c5f6817bd
updated: 1484309472
title: Renu C. Laskar
tags:
    - Early life, education
    - Academic life
categories:
    - Graph theorists
---
Renu Chakravarti Laskar is an Indian-born American mathematician, specializing in Graph theory. She is Professor Emerita of Mathematical sciences at Clemson University. She received her Ph.D. in Mathematics from the University of Illinois at Urbana-Champaign in 1962.[1]
