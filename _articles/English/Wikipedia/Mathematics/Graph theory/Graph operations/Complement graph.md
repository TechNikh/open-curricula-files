---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complement_graph
offline_file: ""
offline_thumbnail: ""
uuid: abef32cc-3a99-49d4-9c18-4ed50549407f
updated: 1484309450
title: Complement graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Petersen_graph_complement.svg.png
tags:
    - Definition
    - Applications and examples
    - Self-complementary graphs and graph classes
    - Algorithmic aspects
categories:
    - Graph operations
---
In graph theory, the complement or inverse of a graph G is a graph H on the same vertices such that two distinct vertices of H are adjacent if and only if they are not adjacent in G. That is, to generate the complement of a graph, one fills in all the missing edges required to form a complete graph, and removes all the edges that were previously there.[1] It is not, however, the set complement of the graph; only the edges are complemented.
