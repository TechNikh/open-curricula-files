---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/butter
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484546641
title: butter
categories:
    - Dictionary
---
butter
     n 1: an edible emulsion of fat globules made by churning milk or
          cream; for cooking and table use
     2: a fighter who strikes the opponent with his head
     v : spread butter on; "butter bread"
