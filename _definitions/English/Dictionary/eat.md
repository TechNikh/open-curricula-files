---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eat
offline_file: ""
offline_thumbnail: ""
uuid: ac1c92a0-a3e0-4044-a320-6dfce66412e4
updated: 1484310353
title: eat
categories:
    - Dictionary
---
eat
     v 1: take in solid food; "She was eating a banana"; "What did you
          eat for dinner last night?"
     2: eat a meal; take a meal; "We did not eat until 10 P.M.
        because there were so many phone calls"; "I didn't eat
        yet, so I gladly accept your invitation"
     3: take in food; used of animals only; "This dog doesn't eat
        certain kinds of meat"; "What do whales eat?" [syn: {feed}]
     4: use up (resources or materials); "this car consumes a lot of
        gas"; "We exhausted our savings"; "They run through 20
        bottles of wine a week" [syn: {consume}, {eat up}, {use up},
         {deplete}, {exhaust}, {run through}, {wipe out}]
     5: worry or ...
