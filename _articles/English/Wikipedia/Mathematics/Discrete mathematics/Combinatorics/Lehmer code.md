---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lehmer_code
offline_file: ""
offline_thumbnail: ""
uuid: 5e548b23-f5da-4e98-96ac-6ed40480480a
updated: 1484308573
title: Lehmer code
tags:
    - The code
    - Encoding and decoding
    - Applications to combinatorics and probabilities
    - Independence of relative ranks
    - Number of right-to-left minima and maxima
    - The secretary problem
    - Bibliography
categories:
    - Combinatorics
---
In mathematics and in particular in combinatorics, the Lehmer code is a particular way to encode each possible permutation of a sequence of n numbers. It is an instance of a scheme for numbering permutations and is an example of an inversion table.
