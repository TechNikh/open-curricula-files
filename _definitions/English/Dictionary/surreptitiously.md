---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surreptitiously
offline_file: ""
offline_thumbnail: ""
uuid: 50d3d4a1-ba1c-4142-b510-7a37fdd7e8d3
updated: 1484310192
title: surreptitiously
categories:
    - Dictionary
---
surreptitiously
     adv : in a surreptitious manner; "he was watching her
           surreptitiously as she waited in the hotel lobby" [syn:
            {sneakily}]
