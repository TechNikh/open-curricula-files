---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/favourable
offline_file: ""
offline_thumbnail: ""
uuid: c4dea745-99d7-4dbf-a84a-9d8db81d1c3a
updated: 1484310477
title: favourable
categories:
    - Dictionary
---
favourable
     adj 1: encouraging or approving or pleasing; "a favorable reply";
            "he received a favorable rating"; "listened with a
            favorable ear"; "made a favorable impression" [syn: {favorable}]
            [ant: {unfavorable}]
     2: (of winds or weather) tending to promote or facilitate; "the
        days were fair and the winds were favorable" [syn: {favorable}]
        [ant: {unfavorable}]
     3: at a convenient or suitable time; "an opportune time to
        receive guests" [syn: {favorable}, {opportune}]
     4: tending to favor or bring good luck; "miracles are
        auspicious accidents"; "encouraging omens"; "a favorable
        time to ask for a ...
