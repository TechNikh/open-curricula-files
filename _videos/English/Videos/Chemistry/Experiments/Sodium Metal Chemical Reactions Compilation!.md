---
version: 1
type: video
provider: YouTube
id: WsrdE5g4z1I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Sodium%20Metal%20Chemical%20Reactions%20Compilation%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d60f7e66-03a8-4549-8cc4-59841a56e745
updated: 1486069661
title: Sodium Metal Chemical Reactions Compilation!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/WsrdE5g4z1I/default.jpg
    - https://i3.ytimg.com/vi/WsrdE5g4z1I/1.jpg
    - https://i3.ytimg.com/vi/WsrdE5g4z1I/2.jpg
    - https://i3.ytimg.com/vi/WsrdE5g4z1I/3.jpg
---
