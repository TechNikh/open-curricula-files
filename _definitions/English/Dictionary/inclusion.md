---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inclusion
offline_file: ""
offline_thumbnail: ""
uuid: 2e2bed3c-9e98-4538-bfb8-e38160aff49c
updated: 1484310397
title: inclusion
categories:
    - Dictionary
---
inclusion
     n 1: the state of being included [ant: {exclusion}]
     2: the relation of comprising something; "he admired the
        inclusion of so many ideas in such a short work" [syn: {comprehension}]
     3: any small intracellular body found within another
        (characteristic of certain diseases); "an inclusion in the
        cytoplasm of the cell" [syn: {inclusion body}, {cellular
        inclusion}]
     4: the act of including
