---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deccan
offline_file: ""
offline_thumbnail: ""
uuid: 555f373e-9b67-4bc2-b9e7-d63ee6036907
updated: 1484308415
title: Deccan Plateau
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Map_of_the_Cities_and_Towns_in_the_Deccan_Plateau%252C_India.png'
tags:
    - Etymology
    - Extent
    - geography
    - Climate
    - The Deccan Traps
    - Geology
    - people
    - History
    - Economy
    - Gallery
    - Notes
categories:
    - Uncategorized
---
The Deccan Plateau[1] is a large plateau in southern India. It rises to 100 metres (330 ft) in the north, and to more than 1 kilometre (0.62 mi) in the south, forming a raised triangle within the downward-pointing triangle of the Indian subcontinent's coastline.[2]
