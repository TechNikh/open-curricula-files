---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/try
offline_file: ""
offline_thumbnail: ""
uuid: be6ecb83-98b4-41ec-8120-734f6d66ef4b
updated: 1484310351
title: try
categories:
    - Dictionary
---
try
     n : earnest and conscientious activity intended to do or
         accomplish something; "made an effort to cover all the
         reading material"; "wished him luck in his endeavor";
         "she gave it a good try" [syn: {attempt}, {effort}, {endeavor},
          {endeavour}]
     v 1: make an effort or attempt; "He tried to shake off his
          fears"; "The infant had essayed a few wobbly steps";
          "The police attempted to stop the thief"; "He sought to
          improve himself"; "She always seeks to do good in the
          world" [syn: {seek}, {attempt}, {essay}, {assay}]
     2: put to the test, as for its quality, or give experimental
        use to; "This ...
