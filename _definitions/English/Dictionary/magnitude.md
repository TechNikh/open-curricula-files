---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnitude
offline_file: ""
offline_thumbnail: ""
uuid: 027381c3-477f-4861-ad4f-64f4a32edee1
updated: 1484310204
title: magnitude
categories:
    - Dictionary
---
magnitude
     n 1: the property of relative size or extent; "they tried to
          predict the magnitude of the explosion"
     2: a number assigned to the ratio of two quantities; two
        quantities are of the same order of magnitude if one is
        less than 10 times as large as the other; the number of
        magnitudes that the quantities differ is specified to
        within a power of 10 [syn: {order of magnitude}]
     3: relative importance; "a problem of the first magnitude"
