---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ernest_de_Jonqui%C3%A8res'
offline_file: ""
offline_thumbnail: ""
uuid: 0e181f68-a95c-4b3a-a6c3-d9e0ac05b650
updated: 1484309085
title: Ernest de Jonquières
categories:
    - Geometers
---
Ernest Jean Philippe Fauque de Jonquières (3 July 1820 – 12 August 1901) was a French mathematician and naval officer who made several contributions in geometry. He was born in Carpentras, and died, aged 81, in Mousans-Sartoux.
