---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weaken
offline_file: ""
offline_thumbnail: ""
uuid: df1bbc37-d620-4ff9-a5c0-7850903a0f3b
updated: 1484310555
title: weaken
categories:
    - Dictionary
---
weaken
     v 1: lessen the strength of; "The fever weakened his body" [ant:
          {strengthen}]
     2: become weaker; "The prisoner's resistance weakened after
        seven days" [ant: {strengthen}]
     3: destroy property or hinder normal operations; "The
        Resistance sabotaged railroad operations during the war"
        [syn: {sabotage}, {undermine}, {countermine}, {counteract},
         {subvert}]
     4: reduce the level or intensity or size or scope of;
        "de-escalate a crisis" [syn: {de-escalate}, {step down}]
        [ant: {escalate}]
     5: lessen in force or effect; "soften a shock"; "break a fall"
        [syn: {dampen}, {damp}, {soften}, {break}]
