---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/logically
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484361121
title: logically
categories:
    - Dictionary
---
logically
     adv 1: according to logical reasoning; "logically, you should now
            do the same to him"
     2: in a logical manner; "he acted logically under the
        circumstances" [ant: {illogically}]
