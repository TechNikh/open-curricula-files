---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Enumerative_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 58332453-cc7d-44af-9c40-42ec5a0ced86
updated: 1484309142
title: Enumerative geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Apollonius8ColorMultiplyV2.svg.png
tags:
    - History
    - Key tools
    - Schubert calculus
    - "Fudge factors and Hilbert's fifteenth problem"
    - Clemens conjecture
    - Examples
categories:
    - Algebraic geometry
---
In mathematics, enumerative geometry is the branch of algebraic geometry concerned with counting numbers of solutions to geometric questions, mainly by means of intersection theory.
