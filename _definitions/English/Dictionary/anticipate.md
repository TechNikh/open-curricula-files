---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anticipate
offline_file: ""
offline_thumbnail: ""
uuid: 96ea73d4-04a3-46b7-a06a-5f1861b73e77
updated: 1484310545
title: anticipate
categories:
    - Dictionary
---
anticipate
     v 1: regard something as probable or likely; "The meteorologists
          are expecting rain for tomorrow" [syn: {expect}]
     2: act in advance of; deal with ahead of time [syn: {foresee},
        {forestall}, {counter}]
     3: realize beforehand [syn: {previse}, {foreknow}, {foresee}]
     4: make a prediction about; tell in advance; "Call the outcome
        of an election" [syn: {predict}, {foretell}, {prognosticate},
         {call}, {forebode}, {promise}]
     5: be excited or anxious about [syn: {look for}, {look to}]
     6: be a forerunner of or occur earlier than; "This composition
        anticipates Impressionism"
