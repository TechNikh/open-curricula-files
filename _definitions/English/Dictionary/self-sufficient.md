---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-sufficient
offline_file: ""
offline_thumbnail: ""
uuid: 3ea54888-c249-4b06-88b2-ce3a3fc25a31
updated: 1484310611
title: self-sufficient
categories:
    - Dictionary
---
self-sufficient
     adj : able to provide for your own needs without help from others;
           "a self-sufficing economic unit" [syn: {self-sufficing},
            {self-sustaining}]
