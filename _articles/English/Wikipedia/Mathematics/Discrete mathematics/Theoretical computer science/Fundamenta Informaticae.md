---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fundamenta_Informaticae
offline_file: ""
offline_thumbnail: ""
uuid: 595e9e6f-4106-449f-8e8c-1b34c4b9d8dd
updated: 1484308683
title: Fundamenta Informaticae
categories:
    - Theoretical computer science
---
Fundamenta Informaticae is a peer-reviewed scientific journal covering computer science. The editor-in-chief is Damian Niwiński. It was established in 1977 by the Polish Mathematical Society as Series IV of the Annales Societatis Mathematicae Polonae, with its main focus on theoretical foundations of computer science. The journal is currently published by IOS Press under the auspices of the European Association for Theoretical Computer Science.
