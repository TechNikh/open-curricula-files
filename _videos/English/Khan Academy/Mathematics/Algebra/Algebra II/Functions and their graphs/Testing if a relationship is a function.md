---
version: 1
type: video
provider: YouTube
id: 3SO1BQQ9_1E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Functions%20and%20their%20graphs/Testing%20if%20a%20relationship%20is%20a%20function.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 11caf201-7bc6-45d7-8f0c-800ffa1be14c
updated: 1486069559
title: Testing if a relationship is a function
categories:
    - Functions and their graphs
thumbnail_urls:
    - https://i3.ytimg.com/vi/3SO1BQQ9_1E/default.jpg
    - https://i3.ytimg.com/vi/3SO1BQQ9_1E/1.jpg
    - https://i3.ytimg.com/vi/3SO1BQQ9_1E/2.jpg
    - https://i3.ytimg.com/vi/3SO1BQQ9_1E/3.jpg
---
