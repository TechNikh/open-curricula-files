---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rokhlin_lemma
offline_file: ""
offline_thumbnail: ""
uuid: e083c94d-735e-4f8c-813b-faace1f700fd
updated: 1484308777
title: Rokhlin lemma
tags:
    - Terminology
    - Statement of the lemma
    - A topological version of the lemma
    - Further generalizations
    - Notes
categories:
    - Ergodic theory
---
In mathematics, the Rokhlin lemma, or Kakutani–Rokhlin lemma is an important result in ergodic theory. It states that an aperiodic measure preserving dynamical system can be decomposed to an arbitrary high tower of measurable sets and a remainder of arbitrarily small measure. It was proven by Vladimir Abramovich Rokhlin and independently by Shizuo Kakutani. The lemma is used extensively in ergodic theory, for example in Ornstein theory and has many generalizations.
