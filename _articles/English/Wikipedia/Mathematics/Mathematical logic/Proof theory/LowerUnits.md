---
version: 1
type: article
id: https://en.wikipedia.org/wiki/LowerUnits
offline_file: ""
offline_thumbnail: ""
uuid: 0df94bf1-8f50-4b38-9875-69140ad462c6
updated: 1484309492
title: LowerUnits
categories:
    - Proof theory
---
In proof compression LowerUnits (LU) is an algorithm used to compress propositional logic resolution proofs. The main idea of LowerUnits is to exploit the following fact:[1]
