---
version: 1
type: video
provider: YouTube
id: gcJeg4SdIpU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Polynomial%20approximation%20of%20functions%20%28part%204%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0e397ed2-e2dc-4ceb-b201-95db93c4e75d
updated: 1486069694
title: Polynomial approximation of functions (part 4)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/gcJeg4SdIpU/default.jpg
    - https://i3.ytimg.com/vi/gcJeg4SdIpU/1.jpg
    - https://i3.ytimg.com/vi/gcJeg4SdIpU/2.jpg
    - https://i3.ytimg.com/vi/gcJeg4SdIpU/3.jpg
---
