---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/progress
offline_file: ""
offline_thumbnail: ""
uuid: 13ed1c57-eab8-410a-af63-f8f5a0cdf9a2
updated: 1484310439
title: progress
categories:
    - Dictionary
---
progress
     n 1: gradual improvement or growth or development; "advancement
          of knowledge"; "great progress in the arts" [syn: {advancement}]
     2: the act of moving forward toward a goal [syn: {progression},
         {procession}, {advance}, {advancement}, {forward motion},
         {onward motion}]
     3: a movement forward; "he listened for the progress of the
        troops" [syn: {progression}, {advance}]
     v 1: develop in a positive way; "He progressed well in school";
          "My plants are coming along"; "Plans are shaping up"
          [syn: {come on}, {come along}, {advance}, {get on}, {get
          along}, {shape up}] [ant: {regress}]
     2: move forward, ...
