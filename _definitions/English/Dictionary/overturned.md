---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overturned
offline_file: ""
offline_thumbnail: ""
uuid: 5055f4c6-8eb6-41d9-8962-8e768fdb76b3
updated: 1484310593
title: overturned
categories:
    - Dictionary
---
overturned
     adj : having been turned so that the bottom is no longer the
           bottom; "an overturned car"; "the upset pitcher of
           milk"; "sat on an upturned bucket" [syn: {upset}, {upturned}]
