---
version: 1
type: video
provider: YouTube
id: vf0BWljVndg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1775a6f4-07ba-4572-af30-328fcc2e8cc2
updated: 1486069641
title: Geometry of Numbers (final version)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/vf0BWljVndg/default.jpg
    - https://i3.ytimg.com/vi/vf0BWljVndg/1.jpg
    - https://i3.ytimg.com/vi/vf0BWljVndg/2.jpg
    - https://i3.ytimg.com/vi/vf0BWljVndg/3.jpg
---
