---
version: 1
type: video
provider: YouTube
id: WOEvvHbc240
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a4f9eba5-3ab6-40ac-9eb4-d262986ec0d6
updated: 1486069682
title: 'Kinetic Theory and Phase Changes- Crash Course Physics #21'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/WOEvvHbc240/default.jpg
    - https://i3.ytimg.com/vi/WOEvvHbc240/1.jpg
    - https://i3.ytimg.com/vi/WOEvvHbc240/2.jpg
    - https://i3.ytimg.com/vi/WOEvvHbc240/3.jpg
---
