---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Harold_Scott_MacDonald_Coxeter
offline_file: ""
offline_thumbnail: ""
uuid: c6835153-3763-42e5-a793-03241445a374
updated: 1484309080
title: Harold Scott MacDonald Coxeter
tags:
    - Biography
    - Works
    - Bibliography
categories:
    - Geometers
---
Harold Scott MacDonald "Donald" Coxeter, FRS, FRSC, CC (February 9, 1907 – March 31, 2003)[2] was a British-born Canadian geometer. Coxeter is regarded as one of the greatest geometers of the 20th century. He was born in London but spent most of his adult life in Canada.
