---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paper_size
offline_file: ""
offline_thumbnail: ""
uuid: 3ff54290-7729-4a18-a12a-c09462eab10d
updated: 1484309327
title: Paper size
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-A_size_illustration2_with_letter_and_legal.svg.png
tags:
    - Grain
    - International paper sizes
    - A series
    - B series
    - C series
    - 'Overview: ISO paper Sizes'
    - German extensions
    - Swedish extensions
    - Japanese B-series variant
    - Colombian common sizes naming
    - North American paper sizes
    - Loose sizes
    - Common loose sizes
    - Variant loose sizes
    - Standardized American paper sizes
    - Architectural sizes
    - Other sizes
    - Tablet sizes
    - Office sizes
    - Photography sizes
    - Postage sizes
    - Traditional inch-based paper sizes
    - Traditional sizes for paper in the United Kingdom
    - Demitab
    - Transitional paper sizes
    - PA series
    - Antiquarian
    - Other metric sizes
    - Newspaper sizes
categories:
    - Technical drawing
---
Many paper size standards conventions have existed at different times and in different countries. Today, the A and B series of ISO 216, which includes the popular A4 size, are the international standard used by almost every country. The only exceptions are United States and Canada, where the "Letter" is more prevalent. Paper sizes affect writing paper, stationery, cards, and some printed documents. The international standard for envelopes is the C series of ISO 269.
