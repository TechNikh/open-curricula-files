---
version: 1
type: video
provider: YouTube
id: R4hfUAwLzTA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 07686b4e-269d-418e-a382-ae468f66d60b
updated: 1486069605
title: More comparison word problems
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/R4hfUAwLzTA/default.jpg
    - https://i3.ytimg.com/vi/R4hfUAwLzTA/1.jpg
    - https://i3.ytimg.com/vi/R4hfUAwLzTA/2.jpg
    - https://i3.ytimg.com/vi/R4hfUAwLzTA/3.jpg
---
