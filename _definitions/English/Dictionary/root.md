---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/root
offline_file: ""
offline_thumbnail: ""
uuid: 28c1bf01-82e2-4f5f-bca0-c7361cb6c0e3
updated: 1484310246
title: root
categories:
    - Dictionary
---
root
     n 1: (botany) the usually underground organ that lacks buds or
          leaves or nodes; absorbs water and mineral salts;
          usually it anchors the plant to the ground
     2: (linguistics) the form of a word after all affixes are
        removed; "thematic vowels are part of the stem" [syn: {root
        word}, {base}, {stem}, {theme}, {radical}]
     3: the place where something begins, where it springs into
        being; "the Italian beginning of the Renaissance";
        "Jupiter was the origin of the radiation"; "Pittsburgh is
        the source of the Ohio River"; "communism's Russian root"
        [syn: {beginning}, {origin}, {rootage}, {source}]
     4: a number ...
