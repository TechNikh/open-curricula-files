---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Instrument_mechanic
offline_file: ""
offline_thumbnail: ""
uuid: fa77dac1-8d1a-422f-b1d1-ca83b94c5bdb
updated: 1484308727
title: Instrument mechanic
tags:
    - History
    - Terminology
    - Training and regulation of trade
    - Canada
    - Australia
    - Other names
    - Fields of study
categories:
    - Control engineering
---
Instrument mechanics in engineering are tradesmen who specialize in installing, troubleshooting, and repairing instrumentation, automation and control systems.
