---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deposit
offline_file: ""
offline_thumbnail: ""
uuid: 87d3b32e-2a83-434b-8d5e-5906034fbfca
updated: 1484310437
title: deposit
categories:
    - Dictionary
---
deposit
     n 1: the phenomenon of sediment or gravel accumulating [syn: {sedimentation},
           {alluviation}]
     2: matter deposited by some natural process [syn: {sediment}]
     3: the natural process of laying down a deposit of something
        [syn: {deposition}]
     4: money deposited in a bank [syn: {bank deposit}]
     5: a partial payment made at the time of purchase; the balance
        to be paid later [syn: {down payment}]
     6: money given as security for an article acquired for
        temporary use; "his deposit was refunded when he returned
        the car"
     7: a payment given as a guarantee that an obligation will be
        met
     8: a facility where ...
