---
version: 1
type: video
provider: YouTube
id: xL9ejqb53ms
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Detectable%20civilizations%20in%20our%20galaxy%205.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 54bf7c2b-9261-442f-885c-4b82a5d49388
updated: 1486069686
title: Detectable civilizations in our galaxy 5
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/xL9ejqb53ms/default.jpg
    - https://i3.ytimg.com/vi/xL9ejqb53ms/1.jpg
    - https://i3.ytimg.com/vi/xL9ejqb53ms/2.jpg
    - https://i3.ytimg.com/vi/xL9ejqb53ms/3.jpg
---
