---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fare
offline_file: ""
offline_thumbnail: ""
uuid: e4f3ec3c-a951-4493-a932-7bb36a11c67d
updated: 1484310384
title: fare
categories:
    - Dictionary
---
fare
     n 1: an agenda of things to do; "they worked rapidly down the
          menu of reports" [syn: {menu}]
     2: the sum charged for riding in a public conveyance [syn: {transportation}]
     3: a paying (taxi) passenger
     4: the food and drink that are regularly consumed
     v 1: proceed or get along; "How is she doing in her new job?";
          "How are you making out in graduate school?"; "He's come
          a long way" [syn: {do}, {make out}, {come}, {get along}]
     2: eat well
