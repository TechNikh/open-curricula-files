---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cook
offline_file: ""
offline_thumbnail: ""
uuid: 1bb2854c-70ec-456e-8f39-75d6f869f84d
updated: 1484310313
title: cook
categories:
    - Dictionary
---
cook
     n 1: someone who cooks food
     2: English navigator who claimed the east coast of Australia
        for Britain and discovered several Pacific islands
        (1728-1779) [syn: {James Cook}, {Captain Cook}, {Captain
        James Cook}]
     v 1: prepare a hot meal; "My husband doesn't cook"
     2: prepare for eating by applying heat; "Cook me dinner,
        please"; "can you make me an omelette?"; "fix breakfast
        for the guests, please" [syn: {fix}, {ready}, {make}, {prepare}]
     3: transform and make suitable for consumption by heating;
        "These potatoes have to cook for 20 minutes"
     4: transform by heating; "The apothecary cooked the medicinal
        ...
