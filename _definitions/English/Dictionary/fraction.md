---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fraction
offline_file: ""
offline_thumbnail: ""
uuid: f6d857c8-5eff-47c4-ab79-2d15c94028c7
updated: 1484310575
title: fraction
categories:
    - Dictionary
---
fraction
     n 1: a component of a mixture that has been separated by a
          fractional process
     2: a small part or item forming a piece of a whole
     3: the quotient of two rational numbers
     v : perform a division; "Can you divide 49 by seven?" [syn: {divide}]
         [ant: {multiply}]
