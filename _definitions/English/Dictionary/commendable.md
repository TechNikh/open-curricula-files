---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commendable
offline_file: ""
offline_thumbnail: ""
uuid: 57940964-d192-41c8-b934-51dd8d7b2083
updated: 1484310181
title: commendable
categories:
    - Dictionary
---
commendable
     adj : worthy of high praise; "applaudable efforts to save the
           environment"; "a commendable sense of purpose";
           "laudable motives of improving housing conditions"; "a
           significant and praiseworthy increase in computer
           intelligence" [syn: {applaudable}, {laudable}, {praiseworthy}]
     adv : in an admirable manner; "the children's responses were
           admirably normal" [syn: {admirably}, {laudably}, {praiseworthily}]
