---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kawasaki%27s_Riemann%E2%80%93Roch_formula'
offline_file: ""
offline_thumbnail: ""
uuid: 489523b5-5bf7-4a69-9db7-f0b4c7ea9fd3
updated: 1484309363
title: "Kawasaki's Riemann–Roch formula"
categories:
    - Geometry stubs
---
Kawasaki's original proof made a use of the equivariant index theorem. Today, the formula is known to follow from the Riemann–Roch formula for quotient stacks.
