---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Satyagraha
offline_file: ""
offline_thumbnail: ""
uuid: cc5b34f8-bb05-4e7d-8aea-1a56ea2dc33e
updated: 1484308312
title: Satyagraha
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Marche_sel.jpg
tags:
    - Origin and meaning of name
    - Contrast to "passive resistance"
    - Ahimsa and satyagraha
    - Defining success
    - Means and ends
    - Satyagraha versus Duragraha
    - In large-scale conflict
    - Principles
    - Rules for satyagraha campaigns
    - Use by the United States Civil Rights Movement
    - Jewish Holocaust
categories:
    - Uncategorized
---
Satyagraha (/ˌsætɪəˈɡrɑːhɑː/; Sanskrit: सत्याग्रह satyāgraha) — loosely translated as "insistence on truth" (satya "truth"; agraha "insistence" or "holding firmly to") or holding onto truth[1] or truth force — is a particular form of nonviolent resistance or civil resistance. The term satyagraha was coined and developed by Mahatma Gandhi (1869–1948).[2] He deployed satyagraha in the Indian independence movement and also during his earlier struggles in South Africa for Indian rights. Satyagraha theory influenced Martin Luther King, Jr.'s and James Bevel's campaigns during the Civil Rights Movement in the United States (1954–1968), and many other social ...
