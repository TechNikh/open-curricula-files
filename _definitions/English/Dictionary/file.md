---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/file
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484344981
title: file
categories:
    - Dictionary
---
file
     n 1: a set of related records (either written or electronic) kept
          together [syn: {data file}]
     2: a line of persons or things ranged one behind the other
        [syn: {single file}, {Indian file}]
     3: office furniture consisting of a container for keeping
        papers in order [syn: {file cabinet}, {filing cabinet}]
     4: a steel hand tool with small sharp teeth on some or all of
        its surfaces; used for smoothing wood or metal
     v 1: record in a public office or in a court of law; "file for
          divorce"; "file a complaint" [syn: {register}]
     2: smooth with a file; "file one's fingernails"
     3: proceed in line; "The students filed into ...
