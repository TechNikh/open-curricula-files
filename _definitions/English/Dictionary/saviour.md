---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saviour
offline_file: ""
offline_thumbnail: ""
uuid: 4583800a-4221-4edb-8413-39f7fb8bebbe
updated: 1484310563
title: saviour
categories:
    - Dictionary
---
Saviour
     n 1: a teacher and prophet born in Bethlehem and active in
          Nazareth; his life and sermons form the basis for
          Christianity (circa 4 BC - AD 29) [syn: {Jesus}, {Jesus
          of Nazareth}, {the Nazarene}, {Jesus Christ}, {Christ},
          {Savior}, {Good Shepherd}, {Redeemer}, {Deliverer}]
     2: a person who rescues you from harm or danger [syn: {savior},
         {rescuer}, {deliverer}]
