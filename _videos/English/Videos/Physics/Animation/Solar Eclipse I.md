---
version: 1
type: video
provider: YouTube
id: mobIY2vIS6s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3d289b86-a94e-4425-a749-0fd905adf05c
updated: 1486069651
title: Solar Eclipse I
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/mobIY2vIS6s/default.jpg
    - https://i3.ytimg.com/vi/mobIY2vIS6s/1.jpg
    - https://i3.ytimg.com/vi/mobIY2vIS6s/2.jpg
    - https://i3.ytimg.com/vi/mobIY2vIS6s/3.jpg
---
