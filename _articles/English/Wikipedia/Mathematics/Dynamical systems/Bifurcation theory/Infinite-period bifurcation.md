---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Infinite-period_bifurcation
offline_file: ""
offline_thumbnail: ""
uuid: bce40709-13e0-4025-a785-3e96e05a4172
updated: 1484308696
title: Infinite-period bifurcation
categories:
    - Bifurcation theory
---
In mathematics, an infinite-period bifurcation is a global bifurcation that can occur when two fixed points emerge on a limit cycle. As the limit of a parameter approaches a certain critical value, the speed of the oscillation slows down and the period approaches infinity. The infinite-period bifurcation occurs at this critical value. Beyond the critical value, the two fixed points emerge continuously from each other on the limit cycle to disrupt the oscillation and form two saddle points.
