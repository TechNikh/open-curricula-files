---
version: 1
type: video
provider: YouTube
id: C_H-ONQFjpQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/MCAT/Cells/Sodium-potassium%20pump.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8a861e84-405b-4dec-bf5f-634742bfd651
updated: 1486069583
title: Sodium-potassium pump
categories:
    - Cells
thumbnail_urls:
    - https://i3.ytimg.com/vi/C_H-ONQFjpQ/default.jpg
    - https://i3.ytimg.com/vi/C_H-ONQFjpQ/1.jpg
    - https://i3.ytimg.com/vi/C_H-ONQFjpQ/2.jpg
    - https://i3.ytimg.com/vi/C_H-ONQFjpQ/3.jpg
---
How a sodium potassium pump can maintain a voltage gradient across a cell or neuron's membrane. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/test-prep/mcat/cells/transport-across-a-cell-membrane/v/secondary-active-transport-in-the-nephron?utm_source=YT&utm_medium=Desc&utm_campaign=mcat

Missed the previous lesson? https://www.khanacademy.org/test-prep/mcat/cells/transport-across-a-cell-membrane/v/glomerular-filtration-in-the-nephron?utm_source=YT&utm_medium=Desc&utm_campaign=mcat

MCAT on Khan Academy: Go ahead and practice some passage-based questions!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s MCAT channel: https://www.youtube.com/channel/UCDkK5wqSuwDlJ3_nl3rgdiQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
