---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/railway
offline_file: ""
offline_thumbnail: ""
uuid: 98ddc1e8-02cc-4735-90c3-4642e722a0a7
updated: 1484310411
title: railway
categories:
    - Dictionary
---
railway
     n 1: line that is the commercial organization responsible for
          operating a railway system [syn: {railroad}, {railroad
          line}, {railway line}, {railway system}]
     2: a line of track providing a runway for wheels; "he walked
        along the railroad track" [syn: {railroad track}, {railroad}]
