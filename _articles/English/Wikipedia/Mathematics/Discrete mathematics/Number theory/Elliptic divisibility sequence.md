---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Elliptic_divisibility_sequence
offline_file: ""
offline_thumbnail: ""
uuid: 1ff2b1b2-cdde-4030-863f-e9f0c18b3ac4
updated: 1484308644
title: Elliptic divisibility sequence
tags:
    - Definition
    - Divisibility property
    - General recursion
    - Nonsingular EDS
    - Examples
    - Periodicity of EDS
    - Elliptic curves and points associated to EDS
    - Growth of EDS
    - Primes and primitive divisors in EDS
    - EDS over finite fields
    - Applications of EDS
    - Further material
categories:
    - Number theory
---
In mathematics, an elliptic divisibility sequence (EDS) is a sequence of integers satisfying a nonlinear recursion relation arising from division polynomials on elliptic curves. EDS were first defined, and their arithmetic properties studied, by Morgan Ward[1] in the 1940s. They attracted only sporadic attention until around 2000, when EDS were taken up as a class of nonlinear recurrences that are more amenable to analysis than most such sequences. This tractability is due primarily to the close connection between EDS and elliptic curves. In addition to the intrinsic interest that EDS have within number theory, EDS have applications to other areas of mathematics including logic and ...
