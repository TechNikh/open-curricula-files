---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reverse_mathematics
offline_file: ""
offline_thumbnail: ""
uuid: 92939028-e449-487f-aa82-8c0d5de5e74a
updated: 1484309490
title: Reverse mathematics
tags:
    - General principles
    - Use of second-order arithmetic
    - The big five subsystems of second order arithmetic
    - The base system RCA0
    - "Weak König's lemma WKL0"
    - Arithmetical comprehension ACA0
    - Arithmetical transfinite recursion ATR0
    - Π11 comprehension Π11-CA0
    - Additional systems
    - ω-models and β-models
categories:
    - Computability theory
---
Reverse mathematics is a program in mathematical logic that seeks to determine which axioms are required to prove theorems of mathematics. Its defining method can briefly be described as "going backwards from the theorems to the axioms", in contrast to the ordinary mathematical practice of deriving theorems from axioms. It can be conceptualized as sculpting out necessary conditions from sufficient ones.
