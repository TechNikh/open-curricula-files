---
version: 1
type: video
provider: YouTube
id: VgDe_D8ojxw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra-%20Solving%20Inequalities.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0a3021dd-056d-4769-aa67-f1794e939839
updated: 1486069573
title: 'Algebra- Solving Inequalities'
tags:
    - Algebra
    - Linear
    - inequality
    - Equations
    - less
    - greater
    - than
    - CC_6_EE_5
    - CC_6_EE_8
    - CC_7_EE_4_b
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/VgDe_D8ojxw/default.jpg
    - https://i3.ytimg.com/vi/VgDe_D8ojxw/1.jpg
    - https://i3.ytimg.com/vi/VgDe_D8ojxw/2.jpg
    - https://i3.ytimg.com/vi/VgDe_D8ojxw/3.jpg
---
Solving linear inequalities.
