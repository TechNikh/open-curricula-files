---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cyanide
offline_file: ""
offline_thumbnail: ""
uuid: 3ffedc82-bc9d-4834-809f-a65436886d17
updated: 1484310416
title: cyanide
categories:
    - Dictionary
---
cyanide
     n 1: any of a class of organic compounds containing the cyano
          radical -CN [syn: {nitrile}, {nitril}]
     2: an extremely poisonous salt of hydrocyanic acid
