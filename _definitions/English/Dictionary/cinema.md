---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cinema
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484516221
title: cinema
categories:
    - Dictionary
---
cinema
     n 1: a medium that disseminates moving pictures; "theater pieces
          transferred to celluloid"; "this story would be good
          cinema"; "film coverage of sporting events" [syn: {film},
           {celluloid}]
     2: a theater where films are shown [syn: {movie theater}, {movie
        theatre}, {movie house}, {picture palace}]
