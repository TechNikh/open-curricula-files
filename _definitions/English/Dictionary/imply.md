---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imply
offline_file: ""
offline_thumbnail: ""
uuid: 9f180396-fd10-4acd-861b-c37a6d3812f2
updated: 1484310473
title: imply
categories:
    - Dictionary
---
imply
     v 1: express or state indirectly [syn: {connote}]
     2: suggest as a logically necessary consequence; in logic
     3: have as a logical consequence; "The water shortage means
        that we have to stop taking long showers" [syn: {entail},
        {mean}]
     4: suggest that someone is guilty [syn: {incriminate}, {inculpate}]
     5: have as a necessary feature or consequence; entail; "This
        decision involves many changes" [syn: {involve}]
     [also: {implied}]
