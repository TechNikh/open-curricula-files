---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/looted
offline_file: ""
offline_thumbnail: ""
uuid: feb0b6dd-8fdf-49d1-9142-23173966ba92
updated: 1484310559
title: looted
categories:
    - Dictionary
---
looted
     adj : wrongfully emptied or stripped of anything of value; "the
           robbers left the looted train"; "people returned to the
           plundered village" [syn: {pillaged}, {plundered}, {ransacked}]
