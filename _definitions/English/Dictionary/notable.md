---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/notable
offline_file: ""
offline_thumbnail: ""
uuid: 4c7359d8-d5aa-445f-8be0-8c9445aa557b
updated: 1484310467
title: notable
categories:
    - Dictionary
---
not able
     adj : (usually followed by `to') not having the necessary means or
           skill or know-how; "unable to get to town without a
           car"; "unable to obtain funds" [syn: {unable}] [ant: {able}]
