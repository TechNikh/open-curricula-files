---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_Roe_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: accd84a2-4cd4-4285-bc59-3bd11094672c
updated: 1484309096
title: John Roe (mathematician)
categories:
    - Geometers
---
Roe grew up in the countryside in Shropshire. He went to Rugby School, was an undergraduate at Cambridge University, and received his D.Phil. in 1985 from the University of Oxford under the supervision of Michael Atiyah.[1] As a post-doctoral student, he was at the Mathematical Sciences Research Institute (MSRI) in Berkeley, and then a tutor at Jesus College, Oxford. Since 1998 he has been a professor at the Pennsylvania State University.
