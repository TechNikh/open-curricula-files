---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/induce
offline_file: ""
offline_thumbnail: ""
uuid: 6b265b18-416b-4ef4-9aec-fed9c8b8cf64
updated: 1484310539
title: induce
categories:
    - Dictionary
---
induce
     v 1: cause to arise; "induce a crisis" [syn: {bring on}]
     2: cause to do; cause to act in a specified manner; "The ads
        induced me to buy a VCR"; "My children finally got me to
        buy a computer"; "My wife made me buy a new sofa" [syn: {stimulate},
         {cause}, {have}, {get}, {make}]
     3: cause to occur rapidly; "the infection precipitated a high
        fever and allergic reactions" [syn: {stimulate}, {rush}, {hasten}]
     4: reason or establish by induction
     5:  produce electric current by electrostatic or magnetic
        processes [syn: {induct}]
