---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Airport_problem
offline_file: ""
offline_thumbnail: ""
uuid: 7ed81b35-3f76-49e8-b666-5a074533febe
updated: 1484308533
title: Airport problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/ArtificialFictionBrain_1.png
categories:
    - Cooperative games
---
In mathematics and especially game theory, the airport problem is a type of fair division problem in which it is decided how to distribute the cost of an airport runway among different players who need runways of different lengths. The problem was introduced by S. C. Littlechild and G. Owen in 1973.[1] Their proposed solution is:
