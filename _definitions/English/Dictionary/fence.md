---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fence
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484347501
title: fence
categories:
    - Dictionary
---
fence
     n 1: a barrier that serves to enclose an area [syn: {fencing}]
     2: a dealer in stolen property
     v 1: enclose with a fence; "we fenced in our yard" [syn: {fence
          in}]
     2: receive stolen goods
     3: fight with fencing swords
     4: surround with a wall in order to fortify [syn: {wall}, {palisade},
         {fence in}, {surround}]
     5: have an argument about something [syn: {argue}, {contend}, {debate}]
