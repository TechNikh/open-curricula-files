---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Placental_mammal
offline_file: ""
offline_thumbnail: ""
uuid: b706a358-1105-4f83-9dba-4d126b73879c
updated: 1484308365
title: Placentalia
tags:
    - Anatomical features
    - Subdivisions
    - Evolution
categories:
    - Uncategorized
---
Placentalia ("Placentals") is one of the three extant[note 1] subdivisions of the class of animals Mammalia; the other two are Monotremata and Marsupialia. The placentals are primarily distinguished from other mammals in that the fetus is carried in the uterus of its mother where it is nourished via a placenta, until the live birth of a fully developed offspring occurs.
