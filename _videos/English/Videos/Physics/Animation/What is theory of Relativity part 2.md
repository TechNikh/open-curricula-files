---
version: 1
type: video
provider: YouTube
id: _uCqZ-72vE8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20theory%20of%20Relativity%20part%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2eb6a7de-185a-4d86-86fc-0312c99e5987
updated: 1486069654
title: What is theory of Relativity part 2
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/_uCqZ-72vE8/default.jpg
    - https://i3.ytimg.com/vi/_uCqZ-72vE8/1.jpg
    - https://i3.ytimg.com/vi/_uCqZ-72vE8/2.jpg
    - https://i3.ytimg.com/vi/_uCqZ-72vE8/3.jpg
---
