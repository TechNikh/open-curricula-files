---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rest
offline_file: ""
offline_thumbnail: ""
uuid: 4f063241-d54c-4dde-bb7b-6917a750c25c
updated: 1484310303
title: rest
categories:
    - Dictionary
---
rest
     n 1: something left after other parts have been taken away;
          "there was no remainder"; "he threw away the rest"; "he
          took what he wanted and I got the balance" [syn: {remainder},
           {balance}, {residual}, {residue}, {residuum}]
     2: freedom from activity (work or strain or responsibility);
        "took his repose by the swimming pool" [syn: {ease}, {repose},
         {relaxation}]
     3: a pause for relaxation; "people actually accomplish more
        when they take time for short rests" [syn: {respite}, {relief},
         {rest period}]
     4: a state of inaction; "a body will continue in a state of
        rest until acted upon"
     5: ...
