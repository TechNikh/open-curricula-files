---
version: 1
type: video
provider: YouTube
id: BTWLwoaNeBA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Why%20S-waves%20only%20travel%20in%20solids.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9bc2272d-d189-4172-bbd5-71972455e9f1
updated: 1486069686
title: Why S-waves only travel in solids
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/BTWLwoaNeBA/default.jpg
    - https://i3.ytimg.com/vi/BTWLwoaNeBA/1.jpg
    - https://i3.ytimg.com/vi/BTWLwoaNeBA/2.jpg
    - https://i3.ytimg.com/vi/BTWLwoaNeBA/3.jpg
---
