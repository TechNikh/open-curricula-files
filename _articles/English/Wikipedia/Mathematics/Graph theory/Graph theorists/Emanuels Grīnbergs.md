---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Emanuels_Gr%C4%ABnbergs'
offline_file: ""
offline_thumbnail: ""
uuid: 13b5d17e-df06-4030-ab44-356ec7854e68
updated: 1484309466
title: Emanuels Grīnbergs
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Grinberg_5CEC_Nonhamiltonian_graph.svg_0.png
categories:
    - Graph theorists
---
Emanuels Donats Frīdrihs Jānis Grinbergs (1911–1982, westernized as Emanuel Grinberg) was a Latvian mathematician, known for Grinberg's theorem on the Hamiltonicity of planar graphs.[1][2][3][4][5][6]
