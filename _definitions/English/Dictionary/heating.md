---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heating
offline_file: ""
offline_thumbnail: ""
uuid: 31749505-a204-4435-8ce3-55899446f445
updated: 1484310230
title: heating
categories:
    - Dictionary
---
heating
     adj : serving to heat; "a heating pad is calefactory" [syn: {calefactory},
            {calefactive}]
     n 1: the process of becoming warmer; a rising temperature [syn: {warming}]
     2: utility to warm a building; "the heating system wasn't
        working"; "they have radiant heating" [syn: {heating
        system}, {heating plant}, {heat}]
