---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sex
offline_file: ""
offline_thumbnail: ""
uuid: 258f6197-d0a3-45af-9cde-e29dd9ba3469
updated: 1484310299
title: sex
categories:
    - Dictionary
---
sex
     n 1: activities associated with sexual intercourse; "they had sex
          in the back seat" [syn: {sexual activity}, {sexual
          practice}, {sex activity}]
     2: either of the two categories (male or female) into which
        most organisms are divided; "the war between the sexes"
     3: all of the feelings resulting from the urge to gratify
        sexual impulses; "he wanted a better sex life"; "the film
        contained no sex or violence" [syn: {sexual urge}]
     4: the properties that distinguish organisms on the basis of
        their reproductive roles; "she didn't want to know the sex
        of the foetus" [syn: {gender}, {sexuality}]
     v 1: stimulate ...
