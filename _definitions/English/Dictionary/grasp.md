---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grasp
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484434141
title: grasp
categories:
    - Dictionary
---
grasp
     n 1: understanding of the nature or meaning or quality or
          magnitude of something; "he has a good grasp of
          accounting practices" [syn: {appreciation}, {hold}]
     2: the limit of capability; "within the compass of education"
        [syn: {compass}, {range}, {reach}]
     3: a firm controlling influence; "they kept a firm grip on the
        two top priorities"; "he was in the grip of a powerful
        emotion"; "a terrible power had her in its grasp" [syn: {grip}]
     4: the act of grasping; "he released his clasp on my arm"; "he
        has a strong grip for an old man"; "she kept a firm hold
        on the railing" [syn: {clasp}, {clench}, {clutch}, ...
