---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/function
offline_file: ""
offline_thumbnail: ""
uuid: 1b07faf0-787a-479b-83ce-465ba948f6aa
updated: 1484310353
title: function
categories:
    - Dictionary
---
function
     n 1: a mathematical relation such that each element of one set is
          associated with at least one element of another set
          [syn: {mathematical function}]
     2: what something is used for; "the function of an auger is to
        bore holes"; "ballet is beautiful but what use is it?"
        [syn: {purpose}, {role}, {use}]
     3: the actions and activities assigned to or required or
        expected of a person or group; "the function of a
        teacher"; "the government must do its part"; "play its
        role" [syn: {office}, {part}, {role}]
     4: a relation such that one thing is dependent on another;
        "height is a function of age"; "price is a ...
