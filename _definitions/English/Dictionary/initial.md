---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/initial
offline_file: ""
offline_thumbnail: ""
uuid: 912e3645-0b85-40ce-a73f-a054ade14073
updated: 1484310234
title: initial
categories:
    - Dictionary
---
initial
     adj : occurring at the beginning; "took the initial step toward
           reconciliation"
     n : the first letter of a word (especially a person's name); "he
         refused to put the initials FRS after his name"
     v : mark with one's initials
     [also: {initialling}, {initialled}]
