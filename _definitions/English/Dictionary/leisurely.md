---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leisurely
offline_file: ""
offline_thumbnail: ""
uuid: 96a420b8-5055-426f-9db4-692b7c811b5a
updated: 1484310152
title: leisurely
categories:
    - Dictionary
---
leisurely
     adj : not hurried or forced; "an easy walk around the block"; "at
           a leisurely (or easygoing) pace" [syn: {easy}, {easygoing}]
     adv : in an unhurried way or at one's convenience; "read the
           manual at your leisure"; "he traveled leisurely" [syn:
           {at leisure}]
