---
version: 1
type: video
provider: YouTube
id: DxkkAHnqlpY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Supermassive%20black%20holes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 26c5ffb4-dbb9-4fc3-902b-03681da43604
updated: 1486069685
title: Supermassive black holes
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/DxkkAHnqlpY/default.jpg
    - https://i3.ytimg.com/vi/DxkkAHnqlpY/1.jpg
    - https://i3.ytimg.com/vi/DxkkAHnqlpY/2.jpg
    - https://i3.ytimg.com/vi/DxkkAHnqlpY/3.jpg
---
