---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rank
offline_file: ""
offline_thumbnail: ""
uuid: 12c2a5c4-94ae-4ca0-bd7b-a9a785c6bbdc
updated: 1484310447
title: rank
categories:
    - Dictionary
---
rank
     adj 1: very fertile; producing profuse growth; "rank earth"
     2: very offensive in smell or taste; "a rank cigar"
     3: conspicuously and outrageously bad or reprehensible; "a
        crying shame"; "an egregious lie"; "flagrant violation of
        human rights"; "a glaring error"; "gross ineptitude";
        "gross injustice"; "rank treachery" [syn: {crying(a)}, {egregious},
         {flagrant}, {glaring}, {gross}]
     4: complete and without restriction or qualification; sometimes
        used informally as intensifiers; "absolute freedom"; "an
        absolute dimwit"; "a downright lie"; "out-and-out mayhem";
        "an out-and-out lie"; "a rank outsider"; "many ...
