---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frog
offline_file: ""
offline_thumbnail: ""
uuid: 924e2a24-17f3-4cf5-abde-ceb769366b24
updated: 1484310283
title: frog
categories:
    - Dictionary
---
frog
     n 1: any of various tailless stout-bodied amphibians with long
          hind limbs for leaping; semiaquatic and terrestrial
          species [syn: {toad}, {toad frog}, {anuran}, {batrachian},
           {salientian}]
     2: a person of French descent [syn: {Gaul}]
     3: a decorative loop of braid or cord [syn: {frogs}]
     [also: {frogging}, {frogged}]
