---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Two-sided
offline_file: ""
offline_thumbnail: ""
uuid: d0facaed-2c34-4a11-aa05-bcc9cc21070e
updated: 1484308463
title: 2-sided
categories:
    - Uncategorized
---
In topology, a compact codimension one submanifold 
  
    
      
        F
      
    
    {\displaystyle F}
  
 of a manifold 
  
    
      
        M
      
    
    {\displaystyle M}
  
 is said to be 2-sided in 
  
    
      
        M
      
    
    {\displaystyle M}
  
 when there is an embedding
