---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pole
offline_file: ""
offline_thumbnail: ""
uuid: 7053c645-bfe7-4d3f-b160-8189e828aa6a
updated: 1484310218
title: pole
categories:
    - Dictionary
---
pole
     n 1: a long (usually round) rod of wood or metal or plastic
     2: a native or inhabitant of Poland
     3: one of two divergent or mutually exclusive opinions; "they
        are at opposite poles"; "they are poles apart"
     4: a linear measure of 16.5 feet [syn: {perch}, {rod}]
     5: a square rod of land [syn: {perch}, {rod}]
     6: one of two points of intersection of the Earth's axis and
        the celestial sphere [syn: {celestial pole}]
     7: one of two antipodal points where the Earth's axis of
        rotation intersects the Earth's surface
     8: a contact on an electrical device (such as a battery) at
        which electric current enters or leaves [syn: ...
