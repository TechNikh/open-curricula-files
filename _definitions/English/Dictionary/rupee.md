---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rupee
offline_file: ""
offline_thumbnail: ""
uuid: 5b65ea6e-02e8-4f2a-9bdd-58d357f11917
updated: 1484310517
title: rupee
categories:
    - Dictionary
---
rupee
     n 1: the basic unit of money in Sri Lanka; equal to 100 cents
          [syn: {Sri Lanka rupee}]
     2: the basic unit of money in Seychelles; equal to 100 cents
        [syn: {Seychelles rupee}]
     3: the basic unit of money in Nepal; equal to 100 paisas [syn:
        {Nepalese rupee}]
     4: the basic unit of money in Mauritius; equal to 100 cents
        [syn: {Mauritian rupee}]
     5: the basic unit of money in Pakistan; equal to 100 paisas
        [syn: {Pakistani rupee}]
     6: the basic unit of money in India; equal to 100 paise [syn: {Indian
        rupee}]
