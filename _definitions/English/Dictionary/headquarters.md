---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/headquarters
offline_file: ""
offline_thumbnail: ""
uuid: c59c193f-29c9-4c85-846a-414b85fa9c46
updated: 1484310170
title: headquarters
categories:
    - Dictionary
---
headquarters
     n 1: (usually plural) the office that serves as the
          administrative center of an enterprise; "many companies
          have their headquarters in New York" [syn: {central
          office}, {main office}, {home office}, {home base}]
     2: (usually plural) the military installation from which a
        commander performs the functions of command; "the
        general's headquarters were a couple of large tents" [syn:
         {HQ}, {military headquarters}]
     3: (plural) a military unit consisting of a commander and the
        headquarters staff
