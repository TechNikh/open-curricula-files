---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dignified
offline_file: ""
offline_thumbnail: ""
uuid: fd4e7f64-d526-4179-a1dd-e945d87e5507
updated: 1484310561
title: dignified
categories:
    - Dictionary
---
dignified
     adj 1: having or expressing dignity; especially formality or
            stateliness in bearing or appearance; "her dignified
            demeanor"; "the director of the school was a dignified
            white-haired gentleman" [ant: {undignified}]
     2: having or showing self-esteem [syn: {self-respecting}, {self-respectful}]
