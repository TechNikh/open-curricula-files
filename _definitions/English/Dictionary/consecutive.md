---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consecutive
offline_file: ""
offline_thumbnail: ""
uuid: 736e82ff-807b-45c2-b7ba-1cbe8ebb5ccf
updated: 1484310156
title: consecutive
categories:
    - Dictionary
---
consecutive
     adj 1: in regular succession without gaps; "serial concerts" [syn:
            {sequent}, {sequential}, {serial}, {successive}]
     2: successive (without a break); "sick for five straight days"
        [syn: {straight}]
     3: one after the other; "back-to-back home runs" [syn: {back-to-back}]
     adv : in a consecutive manner; "we numbered the papers
           consecutively" [syn: {sequentially}]
