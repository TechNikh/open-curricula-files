---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Millets
offline_file: ""
offline_thumbnail: ""
uuid: 5fe615bb-1e03-433d-bf80-28f8afbe7931
updated: 1484308432
title: Millet
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Grain_millet%252C_early_grain_fill%252C_Tifton%252C_7-3-02.jpg'
tags:
    - Description
    - Millet varieties
    - Major millets
    - Minor millets
    - History
    - Production
    - Alcoholic beverages
    - As a food source
    - Grazing millet
    - Nutrition
    - Comparison with other major staple foods
    - Notes
categories:
    - Uncategorized
---
Millets are a group of highly variable small-seeded grasses, widely grown around the world as cereal crops or grains for fodder and human food. Millets are important crops in the semiarid tropics of Asia and Africa (especially in India, Mali, Nigeria, and Niger), with 97% of millet production in developing countries.[1] The crop is favored due to its productivity and short growing season under dry, high-temperature conditions.
