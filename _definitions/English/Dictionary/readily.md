---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/readily
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484523181
title: readily
categories:
    - Dictionary
---
readily
     adv 1: without much difficulty; "these snakes can be identified
            readily"
     2: in a punctual manner; "he did his homework promptly" [syn: {promptly},
         {pronto}, {without delay}]
