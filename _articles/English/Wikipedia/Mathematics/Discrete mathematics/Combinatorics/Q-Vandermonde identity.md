---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Q-Vandermonde_identity
offline_file: ""
offline_thumbnail: ""
uuid: 5c3bbcc8-6044-4395-a0e2-99b4598670a7
updated: 1484308592
title: Q-Vandermonde identity
tags:
    - Other conventions
    - Proof
    - Notes
categories:
    - Combinatorics
---
In mathematics, in the field of combinatorics, the q-Vandermonde identity is a q-analogue of the Chu–Vandermonde identity. Using standard notation for q-binomial coefficients, the identity states that
