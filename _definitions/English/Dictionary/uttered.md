---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uttered
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484487121
title: uttered
categories:
    - Dictionary
---
uttered
     adj : communicated in words; "frequently uttered sentiments" [syn:
            {expressed}, {verbalized}, {verbalised}]
