---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dual_pair
offline_file: ""
offline_thumbnail: ""
uuid: f251ec49-d6cd-459f-93f2-af7cf50e3366
updated: 1484309236
title: Dual pair
tags:
    - Definition
    - Example
    - Comment
categories:
    - Duality theories
---
In functional analysis and related areas of mathematics a dual pair or dual system is a pair of vector spaces with an associated bilinear map to the base field.
