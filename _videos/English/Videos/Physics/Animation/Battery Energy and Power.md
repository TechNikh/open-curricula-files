---
version: 1
type: video
provider: YouTube
id: u4FpbaMW5sk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Battery%20Energy%20and%20Power.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 668829cd-0f81-461d-8cbd-3afd8a724a3e
updated: 1486069657
title: Battery Energy and Power
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/u4FpbaMW5sk/default.jpg
    - https://i3.ytimg.com/vi/u4FpbaMW5sk/1.jpg
    - https://i3.ytimg.com/vi/u4FpbaMW5sk/2.jpg
    - https://i3.ytimg.com/vi/u4FpbaMW5sk/3.jpg
---
