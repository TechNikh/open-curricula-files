---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kilogram
offline_file: ""
offline_thumbnail: ""
uuid: 8b4086a2-4e64-4660-aaed-1dab6c6ae343
updated: 1484310533
title: kilogram
categories:
    - Dictionary
---
kilogram
     n : one thousand grams; the basic unit of mass adopted under the
         Systeme International d'Unites; "a kilogram is
         approximately 2.2 pounds" [syn: {kg}, {kilo}]
