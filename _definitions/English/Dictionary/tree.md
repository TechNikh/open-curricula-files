---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tree
offline_file: ""
offline_thumbnail: ""
uuid: 69b6302c-4ff5-4d95-8a9e-f48824ac5add
updated: 1484310330
title: tree
categories:
    - Dictionary
---
tree
     n 1: a tall perennial woody plant having a main trunk and
          branches forming a distinct elevated crown; includes
          both gymnosperms and angiosperms
     2: a figure that branches from a single root; "genealogical
        tree" [syn: {tree diagram}]
     3: English actor and theatrical producer noted for his lavish
        productions of Shakespeare (1853-1917) [syn: {Sir Herbert
        Beerbohm Tree}]
     v : chase a bear up a tree with dogs and kill it
