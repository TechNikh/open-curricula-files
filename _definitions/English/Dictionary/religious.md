---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/religious
offline_file: ""
offline_thumbnail: ""
uuid: 5b79215e-8f53-4f77-b72c-ef69370a93ba
updated: 1484310475
title: religious
categories:
    - Dictionary
---
religious
     adj 1: concerned with sacred matters or religion or the church;
            "religious texts"; "a nenber if a religious order";
            "lords temporal and spiritual"; "spiritual leaders";
            "spiritual songs" [syn: {spiritual}]
     2: having or showing belief in and reverence for a deity; "a
        religious man"; "religious attitude" [ant: {irreligious}]
     3: extremely scrupulous and conscientious; "religious in
        observing the rules of health"
     n : a member of a religious order who is bound by vows of
         poverty and chastity and obedience
