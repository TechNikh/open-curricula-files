---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elderly
offline_file: ""
offline_thumbnail: ""
uuid: 4ac411b1-d0d2-46f7-8175-13d5b951c191
updated: 1484310515
title: elderly
categories:
    - Dictionary
---
elderly
     adj : advanced in years; (`aged' is pronounced as two syllables);
           "aged members of the society"; "elderly residents could
           remember the construction of the first skyscraper";
           "senior citizen" [syn: {aged}, {older}, {senior}]
