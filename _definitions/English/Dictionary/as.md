---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/as
offline_file: ""
offline_thumbnail: ""
uuid: 204b5e5b-904e-4522-b056-f673623cca72
updated: 1484310361
title: as
categories:
    - Dictionary
---
As
     n 1: a very poisonous metallic element that has three allotropic
          forms; arsenic and arsenic compounds are used as
          herbicides and insecticides and various alloys; found in
          arsenopyrite and orpiment and realgar [syn: {arsenic}, {atomic
          number 33}]
     2: a United States territory on the eastern part of the island
        of Samoa [syn: {American Samoa}, {Eastern Samoa}]
     adv : to the same degree (often followed by `as'); "they were
           equally beautiful"; "birds were singing and the child
           sang as sweetly"; "sang as sweetly as a nightingale";
           "he is every bit as mean as she is" [syn: {equally}, {every
           ...
