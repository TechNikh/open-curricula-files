---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aba
offline_file: ""
offline_thumbnail: ""
uuid: 39a1203e-e058-40e7-bfba-c2086f1d4cc4
updated: 1484310162
title: aba
categories:
    - Dictionary
---
aba
     n 1: a loose sleeveless outer garment made from aba cloth; worn
          by Arabs
     2: a fabric woven from goat and camel hair [syn: {aba cloth}]
