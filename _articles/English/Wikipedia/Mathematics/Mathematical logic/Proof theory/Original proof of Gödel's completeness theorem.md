---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Original_proof_of_G%C3%B6del%27s_completeness_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: b84f177c-1fd6-4303-8c99-d2b761eb88a4
updated: 1484309495
title: "Original proof of Gödel's completeness theorem"
tags:
    - Definitions and assumptions
    - Statement of the theorem and its proof
    - >
        Theorem 1. Every valid formula (true in all structures) is
        provable.
    - >
        Theorem 2. Every formula φ is either refutable or
        satisfiable in some structure.
    - Equivalence of both theorems
    - 'Proof of theorem 2: first step'
    - Reducing the theorem to formulas of degree 1
    - Proving the theorem for formulas of degree 1
    - Intuitive explanation
    - Extensions
    - Extension to first-order predicate calculus with equality
    - Extension to countable sets of formulas
    - Extension to arbitrary sets of formulas
categories:
    - Proof theory
---
The proof of Gödel's completeness theorem given by Kurt Gödel in his doctoral dissertation of 1929 (and a rewritten version of the dissertation, published as an article in 1930) is not easy to read today; it uses concepts and formalism that are no longer used and terminology that is often obscure. The version given below attempts to represent all the steps in the proof and all the important ideas faithfully, while restating the proof in the modern language of mathematical logic. This outline should not be considered a rigorous proof of the theorem.
