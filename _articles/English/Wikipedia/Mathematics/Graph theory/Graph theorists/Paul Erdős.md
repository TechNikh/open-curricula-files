---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Paul_Erd%C5%91s'
offline_file: ""
offline_thumbnail: ""
uuid: eca863bc-a09a-4ada-9b0b-986c25134cad
updated: 1484309463
title: Paul Erdős
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ronald_graham_couple_with_erdos_1986_0.jpg
tags:
    - life
    - Personality
    - Career
    - Mathematical work
    - "Erdős' problems"
    - Collaborators
    - Erdős number
    - Signature
    - Books about Erdős
    - Notes
categories:
    - Graph theorists
---
Paul Erdős (Hungarian: Erdős Pál [ˈɛrdøːʃ ˈpaːl]; 26 March 1913 – 20 September 1996) was a Hungarian mathematician. He was one of the most prolific mathematicians of the 20th century.[2] He was known both for his social practice of mathematics (he engaged more than 500 collaborators) and for his eccentric lifestyle (Time magazine called him The Oddball's Oddball).[3] He devoted his waking hours to mathematics, even into his later years—indeed, his death came only hours after solving a geometry problem in a conference in Warsaw.
