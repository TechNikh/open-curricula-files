---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dynamic_fluid_film_equations
offline_file: ""
offline_thumbnail: ""
uuid: 14cecb13-4991-4bf9-8e2a-14a368f01c50
updated: 1484308813
title: Dynamic fluid film equations
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Making_soapbubbles-SteveEF.jpg
categories:
    - Nonlinear systems
---
Fluid films, such as soap films, are commonly encountered in everyday experience. A soap film can be formed by dipping a closed contour wire into a soapy solution as in the figure on the right. Alternatively, a catenoid can be formed by dipping two rings in the soapy solution and subsequently separating them while maintaining the coaxial configuration.
