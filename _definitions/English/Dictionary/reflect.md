---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reflect
offline_file: ""
offline_thumbnail: ""
uuid: 162a1679-bf86-4020-9a93-037b4d2aa097
updated: 1484310234
title: reflect
categories:
    - Dictionary
---
reflect
     v 1: manifest or bring back; "This action reflects his true
          beliefs"
     2: to throw or bend back or reflect (from a surface); "A mirror
        in the sun can reflect light into a person's eyes"; "Sound
        is reflected well in this auditorium" [syn: {reverberate}]
     3: reflect deeply on a subject; "I mulled over the events of
        the afternoon"; "philosophers have speculated on the
        question of God for thousands of years"; "The scientist
        must stop to observe and start to excogitate" [syn: {chew
        over}, {think over}, {meditate}, {ponder}, {excogitate}, {contemplate},
         {muse}, {mull}, {mull over}, {ruminate}, {speculate}]
     ...
