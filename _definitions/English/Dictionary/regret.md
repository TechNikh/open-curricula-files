---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regret
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464921
title: regret
categories:
    - Dictionary
---
regret
     n : sadness associated with some wrong done or some
         disappointment; "he drank to drown his sorrows"; "he
         wrote a note expressing his regret"; "to his rue, the
         error cost him the game" [syn: {sorrow}, {rue}, {ruefulness}]
     v 1: feel remorse for; feel sorry for; be contrite about [syn: {repent},
           {rue}]
     2: feel sad about the loss or absence of
     3: decline formally or politely; "I regret I can't come to the
        party"
     4: be sorry; "I regret to say that you did not gain admission
        to Harvard"
     [also: {regretting}, {regretted}]
