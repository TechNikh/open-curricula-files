---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/welcomed
offline_file: ""
offline_thumbnail: ""
uuid: a91a727d-8b2f-4ac5-87b8-4cd99d366dbb
updated: 1484310565
title: welcomed
categories:
    - Dictionary
---
welcomed
     adj : gladly and cordially received or admitted [syn: {welcomed(a)}]
