---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/double-faced
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484458501
title: double-faced
categories:
    - Dictionary
---
double-faced
     adj 1: (of fabrics) having faces on both sides; "damask is a
            double-faced fabric"
     2: marked by deliberate deceptiveness especially by pretending
        one set of feelings and acting under the influence of
        another; "she was a deceitful scheming little thing"-
        Israel Zangwill; "a double-dealing double agent"; "a
        double-faced infernal traitor and schemer"- W.M.Thackeray
        [syn: {ambidextrous}, {deceitful}, {double-dealing}, {duplicitous},
         {Janus-faced}, {two-faced}, {double-tongued}]
