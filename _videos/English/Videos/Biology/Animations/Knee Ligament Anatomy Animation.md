---
version: 1
type: video
provider: YouTube
id: RTV5Yo3E7VQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Knee%20Ligament%20Anatomy%20Animation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d57c9b30-f064-4e88-8387-4f5136f3db15
updated: 1486069669
title: Knee Ligament Anatomy Animation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/RTV5Yo3E7VQ/default.jpg
    - https://i3.ytimg.com/vi/RTV5Yo3E7VQ/1.jpg
    - https://i3.ytimg.com/vi/RTV5Yo3E7VQ/2.jpg
    - https://i3.ytimg.com/vi/RTV5Yo3E7VQ/3.jpg
---
