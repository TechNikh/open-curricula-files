---
version: 1
type: video
provider: YouTube
id: EQvTEFCANTM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Photorespiration.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 63c6d280-cf52-4e01-8f70-15eeb0969380
updated: 1486069581
title: Photorespiration
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/EQvTEFCANTM/default.jpg
    - https://i3.ytimg.com/vi/EQvTEFCANTM/1.jpg
    - https://i3.ytimg.com/vi/EQvTEFCANTM/2.jpg
    - https://i3.ytimg.com/vi/EQvTEFCANTM/3.jpg
---
More detail on the Calvin Cycle and Photorespiration
More free lessons at: http://www.khanacademy.org/video?v=EQvTEFCANTM
