---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imprint
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484554081
title: imprint
categories:
    - Dictionary
---
imprint
     n 1: a distinctive influence; "English stills bears the imprint
          of the Norman invasion"
     2: a concavity in a surface produced by pressing; "he left the
        impression of his fingers in the soft mud" [syn: {depression},
         {impression}]
     3: an identification of a publisher; a publisher's name along
        with the date and address and edition that is printed at
        the bottom of the title page; "the book was publsihed
        under a distinguished imprint"
     4: an impression produced by pressure or printing [syn: {embossment}]
     5: a device produced by pressure on a surface
     v 1: establish or impress firmly in the mind; "We imprint our
 ...
