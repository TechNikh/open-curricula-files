---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grid_north
offline_file: ""
offline_thumbnail: ""
uuid: 9be05b31-3c0d-4158-8634-f71e0c67e48d
updated: 1484309295
title: Grid north
categories:
    - Orientation (geometry)
---
Grid north is a navigational term referring to the direction northwards along the grid lines of a map projection. It is contrasted with true north (the direction of the North Pole) and magnetic north (the direction in which a compass needle points). Many topographic maps, including those of the United States Geological Survey and Great Britain's Ordnance Survey, indicate the difference between grid north, true north, and magnetic north.
