---
version: 1
type: video
provider: YouTube
id: 3do1mf2cCug
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2a66524f-1546-42d4-903c-919db22535ea
updated: 1486069634
title: Working model for earthquake alert
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/3do1mf2cCug/default.jpg
    - https://i3.ytimg.com/vi/3do1mf2cCug/1.jpg
    - https://i3.ytimg.com/vi/3do1mf2cCug/2.jpg
    - https://i3.ytimg.com/vi/3do1mf2cCug/3.jpg
---
