---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/si
offline_file: ""
offline_thumbnail: ""
uuid: 3ef6c398-2a08-420b-bba8-6b9fb2291b7d
updated: 1484310232
title: si
categories:
    - Dictionary
---
SI
     n 1: a complete metric system of units of measurement for
          scientists; fundamental quantities are length (meter)
          and mass (kilogram) and time (second) and electric
          current (ampere) and temperature (kelvin) and amount of
          matter (mole) and luminous intensity (candela); "Today
          the United States is the only country in the world not
          totally committed to the Systeme International d'Unites"
          [syn: {Systeme International d'Unites}, {Systeme
          International}, {SI system}, {International System of
          Units}, {International System}]
     2: a tetravalent nonmetallic element; next to oxygen it is the
        most ...
