---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crowded
offline_file: ""
offline_thumbnail: ""
uuid: b4a2209a-d16d-4e7d-b32b-076131440653
updated: 1484310194
title: crowded
categories:
    - Dictionary
---
crowded
     adj : overfilled or compacted or concentrated; "a crowded
           theater"; "a crowded program"; "crowded trains"; "a
           young mother's crowded days" [ant: {uncrowded}]
