---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/N%C3%A9ron_model'
offline_file: ""
offline_thumbnail: ""
uuid: 282a961d-d1cd-4045-9e8f-a2b93f251dd7
updated: 1484308652
title: Néron model
tags:
    - Definition
    - Properties
    - The Néron model of an elliptic curve
categories:
    - Number theory
---
In algebraic geometry, the Néron model (or Néron minimal model, or minimal model) for an abelian variety AK defined over the field of fractions K of a Dedekind domain R is the "push-forward" of AK from Spec(K) to Spec(R), in other words the "best possible" group scheme AR defined over R corresponding to AK.
