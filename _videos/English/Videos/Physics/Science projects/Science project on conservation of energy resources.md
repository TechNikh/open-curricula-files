---
version: 1
type: video
provider: YouTube
id: ZpxFwthilmk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fd5c1dcd-2881-477d-8bfd-cf7114fd50ae
updated: 1486069634
title: Science project on conservation of energy resources
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZpxFwthilmk/default.jpg
    - https://i3.ytimg.com/vi/ZpxFwthilmk/1.jpg
    - https://i3.ytimg.com/vi/ZpxFwthilmk/2.jpg
    - https://i3.ytimg.com/vi/ZpxFwthilmk/3.jpg
---
