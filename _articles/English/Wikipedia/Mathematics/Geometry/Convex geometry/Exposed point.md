---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exposed_point
offline_file: ""
offline_thumbnail: ""
uuid: 6644273c-b89b-4fcb-b2a4-b539749397c1
updated: 1484309197
title: Exposed point
categories:
    - Convex geometry
---
In mathematics, an exposed point of a convex set 
  
    
      
        C
      
    
    {\displaystyle C}
  
 is a point 
  
    
      
        x
        ∈
        C
      
    
    {\displaystyle x\in C}
  
 at which some continuous linear functional attains its strict maximum over 
  
    
      
        C
      
    
    {\displaystyle C}
  
. Such a functional is then said to expose 
  
    
      
        x
      
    
    {\displaystyle x}
  
. Note that there can be many exposing functionals for 
  
    
      
        x
      
    
    {\displaystyle x}
  
. The set of exposed points of 
  
    
      
        C
      
    
    {\displaystyle C}
  
 is usually denoted 
  
    ...
