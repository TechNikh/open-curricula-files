---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alluvium
offline_file: ""
offline_thumbnail: ""
uuid: 852137a8-d667-4443-a368-e221a2a00d99
updated: 1484310435
title: alluvium
categories:
    - Dictionary
---
alluvium
     n : clay or silt or gravel carried by rushing streams and
         deposited where the stream slows down [syn: {alluvial
         sediment}, {alluvial deposit}, {alluvion}]
     [also: {alluvia} (pl)]
