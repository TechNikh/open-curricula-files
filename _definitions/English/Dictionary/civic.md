---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/civic
offline_file: ""
offline_thumbnail: ""
uuid: ac0d72d4-e208-4658-8775-c895d57f239f
updated: 1484310597
title: civic
categories:
    - Dictionary
---
civic
     adj 1: of or relating or belonging to a city; "civic center";
            "civic problems"
     2: of or relating to or befitting citizens as individuals;
        "civil rights"; "civil liberty"; "civic duties"; "civic
        pride" [syn: {civil}]
