---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/B%C3%A9zout%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 24f0246f-02ee-469f-8e11-950762dd88ee
updated: 1484309251
title: "Bézout's theorem"
tags:
    - Rigorous statement
    - History
    - Intersection multiplicity
    - Examples
    - Sketch of proof
    - Notes
categories:
    - Incidence geometry
---
Bézout's theorem is a statement in algebraic geometry concerning the number of common points, or intersection points, of two plane algebraic curves, which do not share a common component (that is, which do not have infinitely many common points). The theorem claims that the number of common points of two such curves is at most equal to the product of their degrees, and equality holds if one counts points at infinity, points with complex coordinates (or more generally, coordinates from the algebraic closure of the ground field), and if each point is counted with its intersection multiplicity.
