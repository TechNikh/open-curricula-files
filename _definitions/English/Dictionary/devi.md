---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/devi
offline_file: ""
offline_thumbnail: ""
uuid: 345f7c86-5bfd-43e8-b7fd-f1d49c14c267
updated: 1484310246
title: devi
categories:
    - Dictionary
---
Devi
     n : mother goddess; supreme power in the universe; wife or
         embodiment of the female energy of Siva having both
         beneficent and malevolent forms or aspects
