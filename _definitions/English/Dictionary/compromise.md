---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compromise
offline_file: ""
offline_thumbnail: ""
uuid: 025bdb9c-28e5-4efe-a0a1-261cf2a6ac20
updated: 1484310585
title: compromise
categories:
    - Dictionary
---
compromise
     n 1: a middle way between two extremes [syn: {via media}]
     2: an accommodation in which both sides make concessions; "the
        newly elected congressmen rejected a compromise because
        they considered it `business as usual'"
     v 1: make a compromise; arrive at a compromise; "nobody will get
          everything he wants; we all must compromise"
     2: settle by concession
     3: expose or make liable to danger, suspicion, or disrepute;
        "The nuclear secrets of the state were compromised by the
        spy"
