---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Dynamic_Data_Driven_Applications_Systems
offline_file: ""
offline_thumbnail: ""
uuid: 1cfd8f30-d678-4738-b182-6cf4cec38e5a
updated: 1484308671
title: Dynamic Data Driven Applications Systems
categories:
    - Theoretical computer science
---
Dynamic Data Driven Applications Systems (DDDAS) is a new paradigm whereby the computation and instrumentation aspects of an application system are dynamically integrated in a feed-back control loop, in the sense that instrumentation data can be dynamically incorporated in to the executing model of the application, and in reverse the executing model can control the instrumentation. Such approaches have been shown that can enable more accurate and faster modeling and analysis of the characteristics and behaviors of a system and can exploit data in intelligent ways to convert them to new capabilities, including decision support systems with the accuracy of full scale modeling, efficient data ...
