---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/casualty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484618941
title: casualty
categories:
    - Dictionary
---
casualty
     n 1: someone injured or killed or captured or missing in a
          military engagement
     2: someone injured or killed in an accident [syn: {injured
        party}]
     3: an accident that causes someone to die [syn: {fatal accident}]
     4: a decrease of military personnel or equipment
