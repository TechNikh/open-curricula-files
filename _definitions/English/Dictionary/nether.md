---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nether
offline_file: ""
offline_thumbnail: ""
uuid: fda97c02-d0ce-4583-8249-1938983d3f32
updated: 1484310180
title: nether
categories:
    - Dictionary
---
nether
     adj 1: lower; "gnawed his nether lip"
     2: of the underworld; "nether regions" [syn: {chthonian}, {chthonic},
         {lower}]
     3: located below or beneath something else; "nether garments";
        "the under parts of a machine" [syn: {under}]
