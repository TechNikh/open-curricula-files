---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/woo
offline_file: ""
offline_thumbnail: ""
uuid: eb8d3f40-96c7-40da-8d9b-c1ec388a52ee
updated: 1484310522
title: woo
categories:
    - Dictionary
---
woo
     v 1: seek someone's favor; "China is wooing Russia" [syn: {court}]
     2: make amorous advances towards; "John is courting Mary" [syn:
         {court}, {romance}, {solicit}]
