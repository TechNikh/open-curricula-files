---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lottery_mathematics
offline_file: ""
offline_thumbnail: ""
uuid: 1b17538b-f9b7-49d6-9bce-24734c9d1065
updated: 1484308590
title: Lottery mathematics
tags:
    - Calculation explained in choosing 6 from 49
    - Odds of getting other possibilities in choosing 6 from 49
    - Pick8-32 Odds and Calculations
    - Powerballs And Bonus Balls
    - Minimum number of tickets for a match
categories:
    - Combinatorics
---
As an example, one of the world's most popular lottery games is the United Kingdom's National Lottery, which, since October 2015, requires players to select 6 numbers from 59 and guess all six correctly. The chance of winning has thereby reduced (since it had been a 6-of-49 game) from 1 in 13.98 million to the current 1 in 45 million. The "45 million" number can be approximated by multiplying 59 × 58 × 57 × 56 × 55 × 54, and then dividing by (6 × 5 × 4 × 3 × 2 × 1); the exact figure is 45,057,474. The factorial notation, used below and shown by an exclamation mark (!), is merely a mathematical shorthand for writing out long strings of multiplication. Example: 5 factorial, written ...
