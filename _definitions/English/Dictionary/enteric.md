---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enteric
offline_file: ""
offline_thumbnail: ""
uuid: d7806a94-67c6-41d3-9550-ba748972fc8d
updated: 1484310323
title: enteric
categories:
    - Dictionary
---
enteric
     adj 1: of or relating to the enteron [syn: {enteral}]
     2: of or relating to or inside the intestines; "intestinal
        disease" [syn: {intestinal}, {enteral}]
