---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/justified
offline_file: ""
offline_thumbnail: ""
uuid: b6be88be-658b-44cd-ba3a-108bf7b17dc4
updated: 1484310403
title: justified
categories:
    - Dictionary
---
justified
     adj : having words so spaced that lines have straight even margins
