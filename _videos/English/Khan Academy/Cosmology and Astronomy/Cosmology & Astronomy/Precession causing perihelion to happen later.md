---
version: 1
type: video
provider: YouTube
id: 2o-Sef6wllg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Precession%20causing%20perihelion%20to%20happen%20later.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 03c2d72e-d673-4871-8166-f423d60fc10b
updated: 1486069686
title: Precession causing perihelion to happen later
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/2o-Sef6wllg/default.jpg
    - https://i3.ytimg.com/vi/2o-Sef6wllg/1.jpg
    - https://i3.ytimg.com/vi/2o-Sef6wllg/2.jpg
    - https://i3.ytimg.com/vi/2o-Sef6wllg/3.jpg
---
