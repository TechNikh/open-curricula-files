---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Square_antiprismatic_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 360270d1-af07-4514-a4fb-b22308dcd84a
updated: 1484309280
title: Square antiprismatic molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Square-antiprismatic-3D-balls.png
categories:
    - Molecular geometry
---
In chemistry, the square antiprismatic molecular geometry describes the shape of compounds where eight atoms, groups of atoms, or ligands are arranged around a central atom, defining the vertices of a square antiprism.
