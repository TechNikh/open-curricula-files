---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Burst_error-correcting_code
offline_file: ""
offline_thumbnail: ""
uuid: 2d041583-1571-4971-9a17-8fa7ce49ffcc
updated: 1484308551
title: Burst error-correcting code
tags:
    - Definitions
    - Burst description
    - Cyclic codes for burst error correction
    - Burst error correction bounds
    - Upper bounds on burst error detection and correction
    - Further bounds on burst error correction
    - 'Fire codes[3][4][5]'
    - Proof of Theorem
    - 'Example: 5-burst error correcting fire code'
    - Binary Reed–Solomon codes
    - An example of a binary RS code
    - Interleaved codes
    - Burst error correcting capacity of interleaver
    - Block interleaver
    - Convolutional interleaver
    - Applications
    - Compact disc
categories:
    - Coding theory
---
In coding theory, burst error-correcting codes employ methods of correcting burst errors, which are errors that occur in many consecutive bits rather than occurring in bits independently of each other.
