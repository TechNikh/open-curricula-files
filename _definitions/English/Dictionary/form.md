---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/form
offline_file: ""
offline_thumbnail: ""
uuid: f88ddc19-fadc-4f9d-8426-118d319f4741
updated: 1484310346
title: form
categories:
    - Dictionary
---
form
     n 1: the phonological or orthographic sound or appearance of a
          word that can be used to describe or identify something;
          "the inflected forms of a word can be represented by a
          stem and a list of inflections to be attached" [syn: {word
          form}, {signifier}, {descriptor}]
     2: a category of things distinguished by some common
        characteristic or quality; "sculpture is a form of art";
        "what kinds of desserts are there?" [syn: {kind}, {sort},
        {variety}]
     3: a perceptual structure; "the composition presents problems
        for students of musical form"; "a visual pattern must
        include not only objects but the ...
