---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grandchildren
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484568061
title: grandchildren
categories:
    - Dictionary
---
grandchild
     n : a child of your son or daughter
     [also: {grandchildren} (pl)]
