---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Miroslav_Fiedler
offline_file: ""
offline_thumbnail: ""
uuid: 4bc7d352-84c0-4eea-868d-a96a572de5cb
updated: 1484309463
title: Miroslav Fiedler
categories:
    - Graph theorists
---
Miroslav Fiedler (7 April 1926 – 20 November 2015) was a Czech mathematician known for his contributions to linear algebra, graph theory and algebraic graph theory.
