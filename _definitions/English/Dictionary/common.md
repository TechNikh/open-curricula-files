---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/common
offline_file: ""
offline_thumbnail: ""
uuid: 5531b081-feaf-465f-8931-7be142988c8e
updated: 1484310325
title: common
categories:
    - Dictionary
---
common
     adj 1: belonging to or participated in by a community as a whole;
            public; "for the common good"; "common lands are set
            aside for use by all members of a community" [ant: {individual}]
     2: of no special distinction or quality; widely known or
        commonly encountered; average or ordinary or usual; "the
        common man"; "a common sailor"; "the common cold"; "a
        common nuisance"; "followed common procedure"; "it is
        common knowledge that she lives alone"; "the common
        housefly"; "a common brand of soap" [ant: {uncommon}]
     3: common to or shared by two or more parties; "a common
        friend"; "the mutual interests of ...
