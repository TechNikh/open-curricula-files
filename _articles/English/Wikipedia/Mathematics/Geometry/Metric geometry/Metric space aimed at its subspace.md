---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Metric_space_aimed_at_its_subspace
offline_file: ""
offline_thumbnail: ""
uuid: 47252f60-c809-40a1-8d63-d31e8c2740d4
updated: 1484309270
title: Metric space aimed at its subspace
categories:
    - Metric geometry
---
In mathematics, a metric space aimed at its subspace is a categorical construction that has a direct geometric meaning. It is also a useful step toward the construction of the metric envelope, or tight span, which are basic (injective) objects of the category of metric spaces.
