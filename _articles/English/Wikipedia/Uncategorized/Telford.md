---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Telford
offline_file: ""
offline_thumbnail: ""
uuid: 67dcafb2-0f81-45ac-9690-e6842814c300
updated: 1484308479
title: Telford
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-SunnycroftWellington.jpg
tags:
    - History
    - Early history
    - Modern history
    - geography
    - Governance
    - Demography
    - Economy
    - Landmarks
    - Education
    - Transport
categories:
    - Uncategorized
---
Telford i/ˈtɛlfərd/ is a large new town in the borough of Telford and Wrekin and ceremonial county of Shropshire, England, about 13 miles (21 km) east of Shrewsbury, and 30 miles (48 km) west of Birmingham. With an estimated population (for the borough) of 170,300 in 2010 and around 155,000 in Telford itself,[1] Telford is the largest town in Shropshire, and one of the fastest-growing towns in the United Kingdom.[2]
