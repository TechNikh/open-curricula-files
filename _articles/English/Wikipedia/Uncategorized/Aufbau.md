---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aufbau
offline_file: ""
offline_thumbnail: ""
uuid: 565d2d7f-cda0-4a75-b212-0472009522a8
updated: 1484308307
title: Aufbau
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Deraufbau_ausgabe_1.gif
tags:
    - History
    - Manfred George
    - The Aufbau database
    - The Aufbau Indexing Project
categories:
    - Uncategorized
---
Aufbau (German for "building up, construction") is a journal targeted at German-speaking Jews around the globe founded in 1934. Hannah Arendt, Albert Einstein, Thomas Mann, and Stefan Zweig wrote for the publication. Until 2004 it was published in New York City. It is now published in Zürich.
