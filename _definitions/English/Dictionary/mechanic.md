---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mechanic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484586661
title: mechanic
categories:
    - Dictionary
---
mechanic
     adj : resembling the action of a machine; "from blank to blank a
           threadless way I pushed mechanic feet"- Emily Dickenson
     n 1: a craftsman skilled in operating machine tools [syn: {machinist},
           {shop mechanic}]
     2: someone whose occupation is repairing and maintaining
        automobiles [syn: {automobile mechanic}, {auto-mechanic},
        {car-mechanic}, {grease monkey}]
