---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/street
offline_file: ""
offline_thumbnail: ""
uuid: a741be5f-6b4d-4db0-8b19-22efec1078d3
updated: 1484310391
title: street
categories:
    - Dictionary
---
street
     n 1: a thoroughfare (usually including sidewalks) that is lined
          with buildings; "they walked the streets of the small
          town"; "he lives on Nassau Street"
     2: the part of a thoroughfare between the sidewalks; the part
        of the thoroughfare on which vehicles travel; "be careful
        crossing the street"
     3: the streets of a city viewed as a depressed environment in
        which there is poverty and crime and prostitution and
        dereliction; "she tried to keep her children off the
        street"
     4: a situation offering opportunities; "he worked both sides of
        the street"; "cooperation is a two-way street"
     5: people living ...
