---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/screeching
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484597461
title: screeching
categories:
    - Dictionary
---
screeching
     adj : loud and sustained; shrill and piercing; "hordes of
           screaming fans"; "a screaming jet plane"; "a screaming
           fury of sound"; "a screeching parrot"; "screeching
           brakes"; "a horde of shrieking fans"; "shrieking winds"
           [syn: {screaming(a)}, {screeching(a)}, {shrieking(a)}]
     n 1: a high-pitched noise resembling a human cry; "he ducked at
          the screechings of shells"; "he heard the scream of the
          brakes" [syn: {screech}, {shriek}, {shrieking}, {scream},
           {screaming}]
     2: sharp piercing cry; "her screaming attracted the neighbors"
        [syn: {scream}, {screaming}, {shriek}, {shrieking}, ...
