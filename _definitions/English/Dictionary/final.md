---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/final
offline_file: ""
offline_thumbnail: ""
uuid: 89fc9cfb-b791-4f6b-ad4e-89b8bb22e59f
updated: 1484310230
title: final
categories:
    - Dictionary
---
final
     adj 1: occurring at or forming an end or termination; "his
            concluding words came as a surprise"; "the final
            chapter"; "the last days of the dinosaurs"; "terminal
            leave" [syn: {concluding}, {last}, {terminal}]
     2: conclusive in a process or progression; "the final answer";
        "a last resort"; "the net result" [syn: {last}, {net}]
     3: not to be altered or undone; "the judge's decision is
        final"; "the arbiter will have the last say" [syn: {last}]
     n 1: the final match between the winners of all previous matches
          in an elimination tournament
     2: an examination administered at the end of an academic term
        ...
