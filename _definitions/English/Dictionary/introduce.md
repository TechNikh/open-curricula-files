---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/introduce
offline_file: ""
offline_thumbnail: ""
uuid: 2751717f-64b2-4062-a231-f402d2006cf8
updated: 1484310391
title: introduce
categories:
    - Dictionary
---
introduce
     v 1: cause to come to know personally; "permit me to acquaint you
          with my son"; "introduce the new neighbors to the
          community" [syn: {present}, {acquaint}]
     2: bring something new to an environment; "A new word processor
        was introduced" [syn: {innovate}]
     3: introduce; "Insert your ticket here" [syn: {insert}, {enclose},
         {inclose}, {stick in}, {put in}]
     4: bring in a new person or object into a familiar environment;
        "He brought in a new judge"; "The new secretary introduced
        a nasty rumor" [syn: {bring in}]
     5: bring in or establish in a new place or environment;
        "introduce a rule"; "introduce exotic ...
