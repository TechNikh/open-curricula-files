---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/macroevolution
offline_file: ""
offline_thumbnail: ""
uuid: 020db893-5ae2-4711-b8a3-136f1abb654d
updated: 1484310284
title: macroevolution
categories:
    - Dictionary
---
macroevolution
     n : evolution on a large scale extending over geologic era and
         resulting in the formation of new taxonomic groups
