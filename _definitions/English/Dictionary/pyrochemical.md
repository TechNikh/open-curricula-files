---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pyrochemical
offline_file: ""
offline_thumbnail: ""
uuid: 67cb744f-a700-4b35-a573-e37caa18b5e6
updated: 1484310413
title: pyrochemical
categories:
    - Dictionary
---
pyrochemical
     adj : of or relating to or produced by chemical reactions at high
           temperatures
