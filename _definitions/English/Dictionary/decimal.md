---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decimal
offline_file: ""
offline_thumbnail: ""
uuid: c529c39c-db10-4947-af4c-405e3d613913
updated: 1484310142
title: decimal
categories:
    - Dictionary
---
decimal
     adj 1: numbered or proceeding by tens; based on ten; "the decimal
            system" [syn: {denary}]
     2: divided by tens or hundreds; "a decimal fraction"; "decimal
        coinage"
     n 1: a proper fraction whose denominator is a power of 10 [syn: {decimal
          fraction}]
     2: a number in the decimal system
