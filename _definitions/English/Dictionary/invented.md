---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invented
offline_file: ""
offline_thumbnail: ""
uuid: aaf6841e-944c-437e-bf5c-4d8b3121507a
updated: 1484310172
title: invented
categories:
    - Dictionary
---
invented
     adj : formed or conceived by the imagination; "a fabricated excuse
           for his absence"; "a fancied wrong"; "a fictional
           character"; "used fictitious names"; "a made-up story"
           [syn: {fabricated}, {fancied}, {fictional}, {fictitious},
            {made-up}]
