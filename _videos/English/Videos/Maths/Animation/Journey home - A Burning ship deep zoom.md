---
version: 1
type: video
provider: YouTube
id: cxrq-zzWagM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 01b33a0a-990d-44d2-8ce7-80d6bd9c953f
updated: 1486069645
title: 'Journey home - A Burning ship deep zoom'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/cxrq-zzWagM/default.jpg
    - https://i3.ytimg.com/vi/cxrq-zzWagM/1.jpg
    - https://i3.ytimg.com/vi/cxrq-zzWagM/2.jpg
    - https://i3.ytimg.com/vi/cxrq-zzWagM/3.jpg
---
