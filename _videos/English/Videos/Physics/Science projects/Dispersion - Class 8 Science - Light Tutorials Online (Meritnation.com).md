---
version: 1
type: video
provider: YouTube
id: m6K3WzuWscU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3b345ed5-b5b7-4116-bfdf-4727391c8df6
updated: 1486069634
title: 'Dispersion - Class 8 Science - Light Tutorials Online (Meritnation.com)'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/m6K3WzuWscU/default.jpg
    - https://i3.ytimg.com/vi/m6K3WzuWscU/1.jpg
    - https://i3.ytimg.com/vi/m6K3WzuWscU/2.jpg
    - https://i3.ytimg.com/vi/m6K3WzuWscU/3.jpg
---
