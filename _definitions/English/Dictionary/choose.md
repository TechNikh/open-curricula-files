---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/choose
offline_file: ""
offline_thumbnail: ""
uuid: d6e55ac6-3b35-4769-9840-166dccbc4ad7
updated: 1484310316
title: choose
categories:
    - Dictionary
---
choose
     v 1: pick out, select, or choose from a number of alternatives;
          "Take any one of these cards"; "Choose a good husband
          for your daughter"; "She selected a pair of shoes from
          among the dozen the salesgirl had shown her" [syn: {take},
           {select}, {pick out}]
     2: select as an alternative; choose instead; prefer as an
        alternative; "I always choose the fish over the meat
        courses in this restaurant"; "She opted for the job on the
        East coast" [syn: {prefer}, {opt}]
     3: see fit or proper to act in a certain way; decide to act in
        a certain way; "She chose not to attend classes and now
        she failed the ...
