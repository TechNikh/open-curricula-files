---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Superintegrable_Hamiltonian_system
offline_file: ""
offline_thumbnail: ""
uuid: 28dc07c8-4482-45a2-a512-55e8a2ab9d39
updated: 1484308791
title: Superintegrable Hamiltonian system
categories:
    - Hamiltonian mechanics
---
In mathematics, a superintegrable Hamiltonian system is a Hamiltonian system on a 2n-dimensional symplectic manifold for which the following conditions hold:
