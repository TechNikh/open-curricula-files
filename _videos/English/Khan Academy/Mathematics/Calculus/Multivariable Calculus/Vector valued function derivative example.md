---
version: 1
type: video
provider: YouTube
id: vcwvzUVLPw0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Vector%20valued%20function%20derivative%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9fb38c6b-eb7c-42e4-be8b-699ca1cf791c
updated: 1486069696
title: Vector valued function derivative example
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/vcwvzUVLPw0/default.jpg
    - https://i3.ytimg.com/vi/vcwvzUVLPw0/1.jpg
    - https://i3.ytimg.com/vi/vcwvzUVLPw0/2.jpg
    - https://i3.ytimg.com/vi/vcwvzUVLPw0/3.jpg
---
