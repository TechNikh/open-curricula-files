---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Angelika_Steger
offline_file: ""
offline_thumbnail: ""
uuid: 44f755de-bfcc-4a25-ac8d-cc170260ac38
updated: 1484309476
title: Angelika Steger
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kahn_Sudakov_Steger_MFO13483.jpg
categories:
    - Graph theorists
---
Angelika Steger is a mathematician and computer scientist whose research interests include graph theory, randomized algorithms, and approximation algorithms. She is a professor at ETH Zurich.[1]
