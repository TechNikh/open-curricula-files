---
version: 1
type: video
provider: YouTube
id: GPPmyxIVdCQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b1b82dec-3f81-4ddc-a698-a576a5991a16
updated: 1486069639
title: Bending the light physics experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/GPPmyxIVdCQ/default.jpg
    - https://i3.ytimg.com/vi/GPPmyxIVdCQ/1.jpg
    - https://i3.ytimg.com/vi/GPPmyxIVdCQ/2.jpg
    - https://i3.ytimg.com/vi/GPPmyxIVdCQ/3.jpg
---
