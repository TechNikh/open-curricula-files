---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unlawful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484635921
title: unlawful
categories:
    - Dictionary
---
unlawful
     adj 1: not conforming to legality, moral law, or social convention;
            "an unconventional marriage"; "improper banking
            practices" [syn: {improper}, {unconventional}]
     2: contrary to or prohibited by or defiant of law; "unlawful
        measures"; "unlawful money"; "unlawful hunters" [ant: {lawful}]
     3: not morally right or permissible; "unlawful love"
     4: having no legally established claim; "the wrongful heir to
        the throne" [syn: {wrongful}]
     5: contrary to or forbidden by law; "an illegitimate seizure of
        power"; "illicit trade"; "an outlaw strike"; "unlawful
        measures" [syn: {illegitimate}, {illicit}, {outlaw(a)}, ...
