---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mainland
offline_file: ""
offline_thumbnail: ""
uuid: c0a5a0e4-5199-4202-b174-d7e001d47205
updated: 1484310431
title: mainland
categories:
    - Dictionary
---
mainland
     n : the main land mass of a country or continent; as
         distinguished from an island or peninsula
