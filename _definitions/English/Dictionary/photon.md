---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/photon
offline_file: ""
offline_thumbnail: ""
uuid: 5b331f8e-55a0-458d-97c8-b668f401e66c
updated: 1484310160
title: photon
categories:
    - Dictionary
---
photon
     n : a quantum of electromagnetic radiation; an elementary
         particle that is its own antiparticle
