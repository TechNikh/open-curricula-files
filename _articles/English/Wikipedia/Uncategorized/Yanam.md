---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yanam
offline_file: ""
offline_thumbnail: ""
uuid: e91a3477-da2d-4a92-a56b-1099242ee99e
updated: 1484308485
title: Yanam
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Yanam_Godavari.jpg
tags:
    - Statistics
    - geography
    - Soil
    - Irrigation
    - Climate
    - Temperature
    - Demographics
    - Revenue villages
    - Literacy rate
    - Colonial history
categories:
    - Uncategorized
---
Yanam (French: Yanaon) is a town in the Indian union territory of Puducherry, located in Yanam district, which forms a 30 km² enclave in the district of East Godavari in Andhra Pradesh. It has a population of 32,000, most of whom speak Telugu. For 200 years it was a French colony, and, though united with India in 1954, is still sometimes known as French Yanam. It possesses a blend of French and Telugu culture prevailing in Andhra Pradesh. During French rule, the Tuesday market (mangalavaram santa) at Yanam was popular among Telugu people in the Madras Presidency who visited Yanam to buy foreign and smuggled goods during Yanam People's Festival, which is held in January. Also, after ...
