---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Azotobacter
offline_file: ""
offline_thumbnail: ""
uuid: 180496e9-9c07-4b1a-9fc7-2973c3a697fe
updated: 1484308471
title: Azotobacter
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Martinus_Willem_Beijerinck.png
tags:
    - Biological characteristics
    - Morphology
    - Cysts
    - Germination of cysts
    - Physiological properties
    - Pigments
    - Genome
    - Distribution
    - Nitrogen fixation
    - Nitrogenase
categories:
    - Uncategorized
---
Azotobacter is a genus of usually motile, oval or spherical bacteria that form thick-walled cysts and may produce large quantities of capsular slime. They are aerobic, free-living soil microbes which play an important role in the nitrogen cycle in nature, binding atmospheric nitrogen, which is inaccessible to plants, and releasing it in the form of ammonium ions into the soil (nitrogen fixation). In addition to being a model organism for studying diazotrophs, it is used by humans for the production of biofertilizers, food additives, and some biopolymers. The first representative of the genus, Azotobacter chroococcum, was discovered and described in 1901 by the Dutch microbiologist and ...
