---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jointly
offline_file: ""
offline_thumbnail: ""
uuid: 3d9b641b-90ed-4de3-b6fe-ee766646ffca
updated: 1484310297
title: jointly
categories:
    - Dictionary
---
jointly
     adv 1: in collaboration or cooperation; "this paper was written
            jointly"
     2: in conjunction with; combined; "Our salaries put together
        couldn't pay for the damage"; "we couldn`t pay for the
        damages with all out salaries put together" [syn: {collectively},
         {conjointly}, {together}, {put together}]
