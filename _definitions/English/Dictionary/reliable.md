---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reliable
offline_file: ""
offline_thumbnail: ""
uuid: 0f0e4c25-7bf7-4aca-8264-416b3ee61db5
updated: 1484310441
title: reliable
categories:
    - Dictionary
---
reliable
     adj 1: worthy of reliance or trust; "a reliable source of
            information"; "a dependable worker" [syn: {dependable}]
            [ant: {unreliable}, {unreliable}]
     2: conforming to fact and therefore worthy of belief; "an
        authentic account by an eyewitness"; "reliable
        information" [syn: {authentic}]
     3: worthy of being depended on; "a dependable worker"; "an
        honest working stiff"; "a reliable source of information";
        "he was true to his word"; "I would be true for there are
        those who trust me" [syn: {dependable}, {honest}, {true(p)}]
