---
version: 1
type: video
provider: YouTube
id: nok99JOhcjo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/systems%20of%20equations.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b28f901a-7378-44d6-bbb8-716f6d67f095
updated: 1486069571
title: systems of equations
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/nok99JOhcjo/default.jpg
    - https://i3.ytimg.com/vi/nok99JOhcjo/1.jpg
    - https://i3.ytimg.com/vi/nok99JOhcjo/2.jpg
    - https://i3.ytimg.com/vi/nok99JOhcjo/3.jpg
---
systems of equations
More free lessons at: http://www.khanacademy.org/video?v=nok99JOhcjo
