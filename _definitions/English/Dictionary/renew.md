---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/renew
offline_file: ""
offline_thumbnail: ""
uuid: d2b65850-bfba-41ab-ac9a-4a00193c86d8
updated: 1484310467
title: renew
categories:
    - Dictionary
---
renew
     v 1: re-establish on a new, usually improved, basis or make new
          or like new; "We renewed our friendship after a hiatus
          of twenty years"; "They renewed their membership" [syn:
          {regenerate}]
     2: cause to appear in a new form; "the old product was
        reincarnated to appeal to a younger market" [syn: {reincarnate}]
