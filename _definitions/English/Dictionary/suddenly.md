---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suddenly
offline_file: ""
offline_thumbnail: ""
uuid: 8ea07002-db74-4ae1-a1f2-c5f82e165dff
updated: 1484310290
title: suddenly
categories:
    - Dictionary
---
suddenly
     adv 1: happening unexpectedly; "suddenly she felt a sharp pain in
            her side" [syn: {all of a sudden}, {of a sudden}]
     2: quickly and without warning; "he stopped suddenly" [syn: {abruptly},
         {short}, {dead}]
     3: on impulse; without premeditation; "he decided to go to
        Chicago on the spur of the moment"; "he made up his mind
        suddenly" [syn: {on the spur of the moment}]
