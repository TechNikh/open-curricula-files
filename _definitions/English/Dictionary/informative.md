---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/informative
offline_file: ""
offline_thumbnail: ""
uuid: 18964b11-8981-4daa-80da-0a7320c8033d
updated: 1484310373
title: informative
categories:
    - Dictionary
---
informative
     adj 1: tending to increase knowledge or dissipate ignorance; "an
            enlightening glimpse of government in action" [syn: {enlightening},
             {instructive}] [ant: {unenlightening}]
     2: serving to instruct of enlighten or inform [syn: {instructive}]
        [ant: {uninstructive}]
     3: providing or conveying information [syn: {informatory}]
        [ant: {uninformative}]
