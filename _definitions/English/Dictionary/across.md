---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/across
offline_file: ""
offline_thumbnail: ""
uuid: 302ad7be-41d8-4965-bc97-0c2dcbb3731e
updated: 1484310316
title: across
categories:
    - Dictionary
---
across
     adj : placed crosswise; "spoken with a straight face but crossed
           fingers"; "crossed forks"; "seated with arms across"
           [syn: {crossed}] [ant: {uncrossed}]
     adv 1: to the opposite side; "the football field was 300 feet
            across"
     2: in such a manner as to be understood and accepted; "she
        cannot get her ideas across" [syn: {over}]
     3: transversely; "the marble slabs were cut across" [syn: {crosswise},
         {crossways}]
