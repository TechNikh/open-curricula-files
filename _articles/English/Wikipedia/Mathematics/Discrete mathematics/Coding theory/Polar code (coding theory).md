---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polar_code_(coding_theory)
offline_file: ""
offline_thumbnail: ""
uuid: b555f574-7b4f-4a1f-84f2-893a75c72db9
updated: 1484308551
title: Polar code (coding theory)
tags:
    - Simulating Polar Codes
    - Industrial Applications
categories:
    - Coding theory
---
In information theory, a polar code is a linear block error correcting code. The code construction is based on a repeated concatenation of a short kernel code which transforms the physical channel into virtual outer channels, which tend to either have high reliability or low reliability (in other words, they polarize). The construction itself was first described by Stolte,[1] and later independently by Erdal Arıkan.[2] It is the first code with an explicit construction to provably achieve the channel capacity for symmetric binary-input, discrete, memoryless channels (B-DMC) with polynomial dependence on the gap to capacity. Notably, polar codes have encoding and decoding complexity 
  
    ...
