---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stable_manifold_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 7b3de380-4a27-4cc5-b477-dc509500450e
updated: 1484308696
title: Stable manifold theorem
tags:
    - Stable manifold theorem
    - Notes
categories:
    - Theorems in dynamical systems
---
In mathematics, especially in the study of dynamical systems and differential equations, the stable manifold theorem is an important result about the structure of the set of orbits approaching a given hyperbolic fixed point.
