---
version: 1
type: article
id: https://en.wikipedia.org/wiki/David_K._Levine
offline_file: ""
offline_thumbnail: ""
uuid: 8125b811-b7b4-4e9d-bcab-9ed74b6eb0c2
updated: 1484308512
title: David K. Levine
tags:
    - Biography
    - Research
    - Publications
    - Working papers
    - Podcast
categories:
    - Game theorists
---
David Knudsen Levine (born c. 1955) is the John H. Biggs Distinguished Professor of Economics at Washington University in St. Louis. His research includes the study of intellectual property and endogenous growth in dynamic general equilibrium models, the endogenous formation of preferences, social norms and institutions, learning in games, and game theory applications to experimental economics.
