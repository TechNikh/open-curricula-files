---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/significance
offline_file: ""
offline_thumbnail: ""
uuid: 6768fc26-60c3-4a56-833c-5afdd85bebf0
updated: 1484310196
title: significance
categories:
    - Dictionary
---
significance
     n 1: the quality of being significant; "do not underestimate the
          significance of nuclear power" [ant: {insignificance}]
     2: a meaning that is not expressly stated but can be inferred;
        "the significance of his remark became clear only later";
        "the expectation was spread both by word and by
        implication" [syn: {import}, {implication}]
     3: the message that is intended or expressed or signified;
        "what is the meaning of this sentence"; "the significance
        of a red traffic light"; "the signification of Chinese
        characters"; "the import of his announcement was
        ambigtuous" [syn: {meaning}, {signification}, ...
