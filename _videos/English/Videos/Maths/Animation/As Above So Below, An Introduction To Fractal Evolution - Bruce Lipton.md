---
version: 1
type: video
provider: YouTube
id: xywpyZwm7Ko
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 97e7407f-6682-4c83-bdd9-7469b6c9aa8d
updated: 1486069643
title: 'As Above So Below, An Introduction To Fractal Evolution - Bruce Lipton'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/xywpyZwm7Ko/default.jpg
    - https://i3.ytimg.com/vi/xywpyZwm7Ko/1.jpg
    - https://i3.ytimg.com/vi/xywpyZwm7Ko/2.jpg
    - https://i3.ytimg.com/vi/xywpyZwm7Ko/3.jpg
---
