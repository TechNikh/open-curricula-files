---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alkaline
offline_file: ""
offline_thumbnail: ""
uuid: 3c3c6a38-6a1c-4f6c-a6c8-c4c415e2c63d
updated: 1484310365
title: alkaline
categories:
    - Dictionary
---
alkaline
     adj : relating to or containing an alkali; having a pH greater
           than 7; "alkaline soils derived from chalk or
           limestone" [syn: {alkalic}] [ant: {amphoteric}, {acidic}]
