---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/typically
offline_file: ""
offline_thumbnail: ""
uuid: b5467207-f838-4038-9c6a-83d5046ad6d9
updated: 1484310281
title: typically
categories:
    - Dictionary
---
typically
     adv : in a typical manner; "Tom was typically hostile" [ant: {atypically}]
