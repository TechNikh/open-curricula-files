---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rotation_system
offline_file: ""
offline_thumbnail: ""
uuid: e99a2e83-a357-45e9-b9e6-55f4472d89f1
updated: 1484309476
title: Rotation system
tags:
    - Formal definition
    - Recovering the embedding from the rotation system
    - Characterizing the surface of the embedding
    - Notes
categories:
    - Topological graph theory
---
In combinatorial mathematics, rotation systems encode embeddings of graphs onto orientable surfaces, by describing the circular ordering of a graph's edges around each vertex. A more formal definition of a rotation system involves pairs of permutations; such a pair is sufficient to determine a multigraph, a surface, and a 2-cell embedding of the multigraph onto the surface.
