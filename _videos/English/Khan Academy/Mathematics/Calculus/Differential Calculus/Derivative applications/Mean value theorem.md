---
version: 1
type: video
provider: YouTube
id: bGNMXfaNR5Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Derivative%20applications/Mean%20value%20theorem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2ac2b35a-cd3e-4c81-bc1a-1517c9820764
updated: 1486069694
title: Mean value theorem
categories:
    - Derivative applications
thumbnail_urls:
    - https://i3.ytimg.com/vi/bGNMXfaNR5Q/default.jpg
    - https://i3.ytimg.com/vi/bGNMXfaNR5Q/1.jpg
    - https://i3.ytimg.com/vi/bGNMXfaNR5Q/2.jpg
    - https://i3.ytimg.com/vi/bGNMXfaNR5Q/3.jpg
---
