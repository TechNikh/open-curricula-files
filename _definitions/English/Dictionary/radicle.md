---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radicle
offline_file: ""
offline_thumbnail: ""
uuid: 616dd832-f37c-4a18-b2e1-00b8fd446226
updated: 1484310160
title: radicle
categories:
    - Dictionary
---
radicle
     n : (anatomy) a small structure resembling a rootlet (such as a
         fibril of a nerve)
