---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Klein_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 26088777-4416-4c97-be91-9cf607c08d75
updated: 1484309226
title: Klein geometry
tags:
    - Formal definition
    - Bundle description
    - Types of Klein geometries
    - Effective geometries
    - Geometrically oriented geometries
    - Reductive geometries
    - Examples
categories:
    - Differential geometry
---
In mathematics, a Klein geometry is a type of geometry motivated by Felix Klein in his influential Erlangen program. More specifically, it is a homogeneous space X together with a transitive action on X by a Lie group G, which acts as the symmetry group of the geometry.
