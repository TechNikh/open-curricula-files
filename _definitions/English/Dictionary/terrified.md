---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terrified
offline_file: ""
offline_thumbnail: ""
uuid: 36dcb1d8-db7b-4c8c-9b18-22a08d8e2501
updated: 1484310168
title: terrified
categories:
    - Dictionary
---
terrified
     adj : thrown into a state of intense fear or desperation; "became
           panicky as the snow deepened"; "felt panicked before
           each exam"; "trying to keep back the panic-stricken
           crowd"; "the terrified horse bolted" [syn: {panicky}, {panicked},
            {panic-stricken}, {panic-struck}, {frightened}]
