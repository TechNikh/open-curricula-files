---
version: 1
type: video
provider: YouTube
id: BSuvM5_kuHA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a118c846-0a60-4b36-9e4c-b9ff576098b4
updated: 1486069622
title: 'How to Trick Your Math Teacher 12 - Math Magic Tricks'
categories:
    - Math Magic Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/BSuvM5_kuHA/default.jpg
    - https://i3.ytimg.com/vi/BSuvM5_kuHA/1.jpg
    - https://i3.ytimg.com/vi/BSuvM5_kuHA/2.jpg
    - https://i3.ytimg.com/vi/BSuvM5_kuHA/3.jpg
---
