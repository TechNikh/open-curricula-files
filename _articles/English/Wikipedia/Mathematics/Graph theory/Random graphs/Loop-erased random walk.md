---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Loop-erased_random_walk
offline_file: ""
offline_thumbnail: ""
uuid: 845a2dd1-ac49-453e-b18b-d18dd96a65fb
updated: 1484309454
title: Loop-erased random walk
tags:
    - Definition
    - Uniform spanning tree
    - The Laplacian random walk
    - Grids
    - High dimensions
    - Two dimensions
    - Three dimensions
    - Notes
categories:
    - Random graphs
---
In mathematics, loop-erased random walk is a model for a random simple path with important applications in combinatorics and, in physics, quantum field theory. It is intimately connected to the uniform spanning tree, a model for a random tree. See also random walk for more general treatment of this topic.
