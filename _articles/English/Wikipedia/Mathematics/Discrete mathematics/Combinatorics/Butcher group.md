---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Butcher_group
offline_file: ""
offline_thumbnail: ""
uuid: 08488d67-5d02-468c-af92-c48a778ea810
updated: 1484308562
title: Butcher group
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Caylrich-first-trees.png
tags:
    - Differentials and rooted trees
    - Definition using Hopf algebra of rooted trees
    - Butcher series and Runge–Kutta method
    - Lie algebra
    - Renormalization
    - Example
    - Notes
categories:
    - Combinatorics
---
In mathematics, the Butcher group, named after the New Zealand mathematician John C. Butcher by Hairer & Wanner (1974), is an infinite-dimensional Lie group[1] first introduced in numerical analysis to study solutions of non-linear ordinary differential equations by the Runge–Kutta method. It arose from an algebraic formalism involving rooted trees that provides formal power series solutions of the differential equation modeling the flow of a vector field. It was Cayley (1857), prompted by the work of Sylvester on change of variables in differential calculus, who first noted that the derivatives of a composition of functions can be conveniently expressed in terms of rooted trees and their ...
