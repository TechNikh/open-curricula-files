---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sounded
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484512201
title: sounded
categories:
    - Dictionary
---
sounded
     adj : (of water depth) measured by a line and plumb [syn: {plumbed}]
