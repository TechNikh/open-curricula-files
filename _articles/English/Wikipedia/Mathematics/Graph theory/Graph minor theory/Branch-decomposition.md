---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Branch-decomposition
offline_file: ""
offline_thumbnail: ""
uuid: bdef41ba-8e78-4662-9fc0-822fb82957d5
updated: 1484309413
title: Branch-decomposition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Branch-decomposition.svg.png
tags:
    - Definitions
    - Relation to treewidth
    - Carving width
    - Algorithms and Complexity
    - Generalization to matroids
    - Forbidden minors
    - Notes
categories:
    - Graph minor theory
---
In graph theory, a branch-decomposition of an undirected graph G is a hierarchical clustering of the edges of G, represented by an unrooted binary tree T with the edges of G as its leaves. Removing any edge from T partitions the edges of G into two subgraphs, and the width of the decomposition is the maximum number of shared vertices of any pair of subgraphs formed in this way. The branchwidth of G is the minimum width of any branch-decomposition of G.
