---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assistance
offline_file: ""
offline_thumbnail: ""
uuid: 900cbfa3-6eb3-43c6-9a7d-f3cbcdfc635a
updated: 1484310373
title: assistance
categories:
    - Dictionary
---
assistance
     n 1: the activity of contributing to the fulfillment of a need or
          furtherance of an effort or purpose; "he gave me an
          assist with the housework"; "could not walk without
          assistance"; "rescue party went to their aid"; "offered
          his help in unloading" [syn: {aid}, {assist}, {help}]
     2: a resource; "visual aids in teaching"; "economic assistance
        to depressed areas" [syn: {aid}, {help}]
