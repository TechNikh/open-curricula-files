---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/morphology
offline_file: ""
offline_thumbnail: ""
uuid: 2f1b03c8-6832-4856-bddf-bfb24fea60ca
updated: 1484310295
title: morphology
categories:
    - Dictionary
---
morphology
     n 1: the branch of biology that deals with the structure of
          animals and plants
     2: studies of the rules for forming admissible words
     3: the admissible arrangement of sounds in words [syn: {sound
        structure}, {syllable structure}, {word structure}]
     4: the branch of geology that studies the characteristics and
        configuration and evolution of rocks and land forms [syn:
        {geomorphology}]
