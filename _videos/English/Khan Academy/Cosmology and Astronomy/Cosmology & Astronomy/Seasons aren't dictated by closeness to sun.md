---
version: 1
type: video
provider: YouTube
id: SJUd5du0T08
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Seasons%20aren%27t%20dictated%20by%20closeness%20to%20sun.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9e41b50d-934d-4d3d-9ddd-13cafb5cdfd0
updated: 1486069686
title: "Seasons aren't dictated by closeness to sun"
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/SJUd5du0T08/default.jpg
    - https://i3.ytimg.com/vi/SJUd5du0T08/1.jpg
    - https://i3.ytimg.com/vi/SJUd5du0T08/2.jpg
    - https://i3.ytimg.com/vi/SJUd5du0T08/3.jpg
---
