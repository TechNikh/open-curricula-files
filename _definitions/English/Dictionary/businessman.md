---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/businessman
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484428021
title: businessman
categories:
    - Dictionary
---
businessman
     n : a person engaged in commercial or industrial business
         (especially an owner or executive) [syn: {man of affairs}]
