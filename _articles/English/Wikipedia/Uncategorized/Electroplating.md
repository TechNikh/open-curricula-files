---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electroplating
offline_file: ""
offline_thumbnail: ""
uuid: e655d34a-42eb-4323-aa03-bf6e1bd10e06
updated: 1484308319
title: Electroplating
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-PCB_copper_layer_electroplating_machine.jpg
tags:
    - Process
    - Strike
    - Electrochemical deposition
    - Pulse electroplating or pulse electrodeposition (PED)
    - Brush electroplating
    - Electroless deposition
    - Cleanliness
    - Effects
    - History
    - Uses
    - Hull cell
    - Haring-Blum Cell
    - Bibliography
categories:
    - Uncategorized
---
Electroplating is a process that uses electric current to reduce dissolved metal cations so that they form a thin coherent metal coating on an electrode. The term is also used for electrical oxidation of anions onto a solid substrate, as in the formation silver chloride on silver wire to make silver/silver-chloride electrodes. Electroplating is primarily used to change the surface properties of an object (e.g. abrasion and wear resistance, corrosion protection, lubricity, aesthetic qualities, etc.), but may also be used to build up thickness on undersized parts or to form objects by electroforming.
