---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cultivable
offline_file: ""
offline_thumbnail: ""
uuid: 4742d497-1066-450c-b50b-ea2aba79e391
updated: 1484310262
title: cultivable
categories:
    - Dictionary
---
cultivable
     adj : (of farmland) capable of being farmed productively [syn: {arable},
            {cultivatable}, {tillable}]
