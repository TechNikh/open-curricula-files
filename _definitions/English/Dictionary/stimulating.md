---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stimulating
offline_file: ""
offline_thumbnail: ""
uuid: 2d9dc517-e7a4-4008-a302-31a236b7215b
updated: 1484310319
title: stimulating
categories:
    - Dictionary
---
stimulating
     adj 1: rousing or quickening activity or the senses; "a stimulating
            discussion" [ant: {unstimulating}]
     2: that stimulates; "stimulant phenomena" [syn: {stimulant}]
     3: making lively and cheerful; "the exhilarating effect of
        mountain air" [syn: {exhilarating}]
