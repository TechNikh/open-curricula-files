---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glucagon
offline_file: ""
offline_thumbnail: ""
uuid: a12ea033-3b1e-4638-8fd1-56899293a92b
updated: 1484310160
title: glucagon
categories:
    - Dictionary
---
glucagon
     n : a hormone secreted by the pancreas; stimulates increases in
         blood sugar levels in the blood (thus opposing the action
         of insulin)
