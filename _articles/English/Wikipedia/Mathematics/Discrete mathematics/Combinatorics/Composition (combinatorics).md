---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Composition_(combinatorics)
offline_file: ""
offline_thumbnail: ""
uuid: c7938c50-1441-49bd-97b1-f9ee7d2b8da9
updated: 1484308577
title: Composition (combinatorics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Compositions_of_6.svg.png
tags:
    - Examples
    - Number of compositions
categories:
    - Combinatorics
---
In mathematics, a composition of an integer n is a way of writing n as the sum of a sequence of (strictly) positive integers. Two sequences that differ in the order of their terms define different compositions of their sum, while they are considered to define the same partition of that number. Every integer has finitely many distinct compositions. Negative numbers do not have any compositions, but 0 has one composition, the empty sequence. Each positive integer n has 2n−1 distinct compositions.
