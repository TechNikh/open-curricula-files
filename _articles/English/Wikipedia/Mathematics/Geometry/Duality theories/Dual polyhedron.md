---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dual_polyhedron
offline_file: ""
offline_thumbnail: ""
uuid: 61b78b65-022d-4e4f-81e4-55a399acfdc2
updated: 1484309237
title: Dual polyhedron
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dual_Cube-Octahedron.svg.png
tags:
    - Kinds of duality
    - Polar reciprocation
    - Canonical duals
    - Topological duality
    - Dorman Luke construction
    - Self-dual polyhedra
    - Self-dual compound polyhedra
    - Dual polytopes and tessellations
    - Self-dual polytopes and tessellations
    - Notes
    - Bibliography
categories:
    - Duality theories
---
In geometry, polyhedra are associated into pairs called duals, where the vertices of one correspond to the faces of the other. Starting with any given polyhedron, the dual of its dual is the original polyhedron. The dual of an isogonal polyhedron, having equivalent vertices, is one which is isohedral, having equivalent faces; the dual of an isotoxal polyhedron (having equivalent edges) is also isotoxal. The regular polyhedra — the Platonic solids and Kepler-Poinsot polyhedra — form dual pairs, with the exception of the regular tetrahedron which is self-dual.
