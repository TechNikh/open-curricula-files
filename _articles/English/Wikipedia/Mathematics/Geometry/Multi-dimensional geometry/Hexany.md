---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hexany
offline_file: ""
offline_thumbnail: ""
uuid: 9463dde0-c96e-4bde-898d-0be0399ced45
updated: 1484309283
title: Hexany
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Octahedron.svg.png
tags:
    - Tuning
    - "Relationship to Pascal's triangle"
    - "Coordinates for the Pascal's triangle of combination product sets"
    - Composers
categories:
    - Multi-dimensional geometry
---
In music theory, the hexany is a six-note just intonation structure, with the notes placed on the vertices of an octahedron, equivalently the faces of a cube. The notes are arranged so that every edge of the octahedron joins together notes that make a consonant dyad, and every face joins together the notes of a consonant triad.
