---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gheorghe_%C8%9Ai%C8%9Beica'
offline_file: ""
offline_thumbnail: ""
uuid: cd22ee9a-2132-4d86-b99d-a17395c645b4
updated: 1484309103
title: Gheorghe Țițeica
categories:
    - Geometers
---
Gheorghe Țițeica (Romanian pronunciation: [ˈɡe̯orɡe t͡siˈt͡sejka]; 4 October 1873 in Turnu Severin – 5 February 1939) publishing as George or Georges Tzitzeica) was a Romanian mathematician with important contributions in geometry. He is recognized as the founder of the Romanian school of differential geometry.
