---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rode
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484480041
title: rode
categories:
    - Dictionary
---
ride
     n 1: a journey in a vehicle driven by someone else; "he took the
          family for a drive in his new car" [syn: {drive}]
     2: a mechanical device that you ride for amusement or
        excitement
     v 1: sit and travel on the back of animal, usually while
          controlling its motions; "She never sat a horse!"; "Did
          you ever ride a camel?"; "The girl liked to drive the
          young mare" [syn: {sit}]
     2: be carried or travel on or in a vehicle; "I ride to work in
        a bus"; "He rides the subway downtown every day" [ant: {walk}]
     3: continue undisturbed and without interference; "Let it ride"
     4: move like a floating object; "The moon rode ...
