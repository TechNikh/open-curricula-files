---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 36050dc2-e0f8-4ad1-83ea-c5c5486e99e9
updated: 1484309279
title: Linear molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Beryllium-fluoride-3D-vdW.png
categories:
    - Molecular geometry
---
In chemistry, the linear molecular geometry describes the geometry around a central atom bonded to two other atoms (or ligands) placed at a bond-angle of 180°. Linear organic molecules, such as acetylene (HC≡CH), are often described by invoking sp orbital hybridization for their carbon centers.
