---
version: 1
type: video
provider: YouTube
id: 3mLUJSoh6i0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/30-60-90%20Triangles%20II.webm"
offline_file: ""
offline_thumbnail: ""
uuid: dce0f337-291e-42a6-8ce9-fa576957a81a
updated: 1486069575
title: 30-60-90 Triangles II
categories:
    - Geometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/3mLUJSoh6i0/default.jpg
    - https://i3.ytimg.com/vi/3mLUJSoh6i0/1.jpg
    - https://i3.ytimg.com/vi/3mLUJSoh6i0/2.jpg
    - https://i3.ytimg.com/vi/3mLUJSoh6i0/3.jpg
---
More examples using 30-60-90 triangles.
More free lessons at: http://www.khanacademy.org/video?v=3mLUJSoh6i0
