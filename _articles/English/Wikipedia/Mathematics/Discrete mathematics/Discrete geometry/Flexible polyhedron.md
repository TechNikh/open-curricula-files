---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flexible_polyhedron
offline_file: ""
offline_thumbnail: ""
uuid: 8d638a81-87e2-4306-8ea5-c5d5482e931a
updated: 1484308590
title: Flexible polyhedron
tags:
    - Bellows conjecture
    - Scissor congruence
    - Generalizations
    - Popular level
categories:
    - Discrete geometry
---
In geometry, a flexible polyhedron is a polyhedral surface that allows continuous non-rigid deformations such that all faces remain rigid. The Cauchy rigidity theorem shows that in dimension 3 such a polyhedron cannot be convex (this is also true in higher dimensions).
