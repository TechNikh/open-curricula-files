---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/verandah
offline_file: ""
offline_thumbnail: ""
uuid: 05931a7e-c1a7-4981-b074-743e3f1b3db2
updated: 1484310152
title: verandah
categories:
    - Dictionary
---
verandah
     n : a porch along the outside of a building (sometimes partly
         enclosed) [syn: {veranda}, {gallery}]
