---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_of_number_theory_topics
offline_file: ""
offline_thumbnail: ""
uuid: 8d36c78c-2040-4370-955b-f6e51208f8c0
updated: 1484308502
title: List of number theory topics
tags:
    - Factors
    - Fractions
    - Modular arithmetic
    - Arithmetic functions
    - 'Analytic number theory: additive problems'
    - Algebraic number theory
    - Quadratic forms
    - L-functions
    - Diophantine equations
    - Diophantine approximation
    - Sieve methods
    - Named primes
    - Combinatorial number theory
    - Computational number theory
    - Primality tests
    - Integer factorization
    - Pseudo-random numbers
    - History
categories:
    - Number theory
---
