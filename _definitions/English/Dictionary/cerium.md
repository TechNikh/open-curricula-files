---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cerium
offline_file: ""
offline_thumbnail: ""
uuid: 4e3b44e2-d175-48f1-ad95-faf2626462d6
updated: 1484310399
title: cerium
categories:
    - Dictionary
---
cerium
     n : a ductile gray metallic element of the lanthanide series;
         used in lighter flints; the most abundant of the
         rare-earth group [syn: {Ce}, {atomic number 58}]
