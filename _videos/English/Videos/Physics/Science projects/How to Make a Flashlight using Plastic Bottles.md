---
version: 1
type: video
provider: YouTube
id: FBu0Vy4cuN4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1e967b70-95b5-4d2a-9b6b-cd41e166b92b
updated: 1486069632
title: How to Make a Flashlight using Plastic Bottles
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/FBu0Vy4cuN4/default.jpg
    - https://i3.ytimg.com/vi/FBu0Vy4cuN4/1.jpg
    - https://i3.ytimg.com/vi/FBu0Vy4cuN4/2.jpg
    - https://i3.ytimg.com/vi/FBu0Vy4cuN4/3.jpg
---
