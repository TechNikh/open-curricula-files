---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lent
offline_file: ""
offline_thumbnail: ""
uuid: 61c03474-d74c-4332-b6d5-196c67430e15
updated: 1484310146
title: lent
categories:
    - Dictionary
---
lend
     v 1: bestow a quality on; "Her presence lends a certain cachet to
          the company"; "The music added a lot to the play"; "She
          brings a special atmosphere to our meetings"; "This adds
          a light note to the program" [syn: {impart}, {bestow}, {contribute},
           {add}, {bring}]
     2: give temporarily; let have for a limited time; "I will lend
        you my car"; "loan me some money" [syn: {loan}] [ant: {borrow}]
     3: have certain characteristics of qualities for something; be
        open or vulnerable to; "This story would lend itself well
        to serialization on television"; "The current system lends
        itself to great abuse"
     [also: ...
