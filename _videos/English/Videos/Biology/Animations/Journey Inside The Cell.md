---
version: 1
type: video
provider: YouTube
id: 1fiJupfbSpg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 15bf5c9c-200e-436f-92f7-35ba043b8190
updated: 1486069668
title: Journey Inside The Cell
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/1fiJupfbSpg/default.jpg
    - https://i3.ytimg.com/vi/1fiJupfbSpg/1.jpg
    - https://i3.ytimg.com/vi/1fiJupfbSpg/2.jpg
    - https://i3.ytimg.com/vi/1fiJupfbSpg/3.jpg
---
