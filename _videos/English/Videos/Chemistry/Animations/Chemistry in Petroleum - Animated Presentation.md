---
version: 1
type: video
provider: YouTube
id: -DK5ZXpf6mY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Chemistry%20in%20Petroleum%20-%20Animated%20Presentation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 27341c23-acd2-47cb-a187-bc1c328e5279
updated: 1486069664
title: 'Chemistry in Petroleum - Animated Presentation'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/-DK5ZXpf6mY/default.jpg
    - https://i3.ytimg.com/vi/-DK5ZXpf6mY/1.jpg
    - https://i3.ytimg.com/vi/-DK5ZXpf6mY/2.jpg
    - https://i3.ytimg.com/vi/-DK5ZXpf6mY/3.jpg
---
