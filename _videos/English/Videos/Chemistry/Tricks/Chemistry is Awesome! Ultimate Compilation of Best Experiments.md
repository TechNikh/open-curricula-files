---
version: 1
type: video
provider: YouTube
id: afD6eiKBdD4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Tricks/Chemistry%20is%20Awesome%21%20Ultimate%20Compilation%20of%20Best%20Experiments.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e7f48e06-0165-4726-9b44-6e76ec2006c2
updated: 1486069661
title: >
    Chemistry is Awesome! Ultimate Compilation of Best
    Experiments
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/afD6eiKBdD4/default.jpg
    - https://i3.ytimg.com/vi/afD6eiKBdD4/1.jpg
    - https://i3.ytimg.com/vi/afD6eiKBdD4/2.jpg
    - https://i3.ytimg.com/vi/afD6eiKBdD4/3.jpg
---
