---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rested
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484487901
title: rested
categories:
    - Dictionary
---
rested
     adj : not tired; refreshed as by sleeping or relaxing; "came back
           rested from her vacation" [ant: {tired}]
