---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 9193051f-f298-4a30-b6c3-1a3e284d599a
updated: 1484310142
title: conjecture
categories:
    - Dictionary
---
conjecture
     n 1: a hypothesis that has been formed by speculating or
          conjecturing (usually with little hard evidence);
          "speculations about the outcome of the election"; "he
          dismissed it as mere conjecture" [syn: {speculation}]
     2: a message expressing an opinion based on incomplete evidence
        [syn: {guess}, {supposition}, {surmise}, {surmisal}, {speculation},
         {hypothesis}]
     3: reasoning that involves the formation of conclusions from
        incomplete evidence
     v : to believe especially on uncertain or tentative grounds;
         "Scientists supposed that large dinosaurs lived in
         swamps" [syn: {speculate}, {theorize}, ...
