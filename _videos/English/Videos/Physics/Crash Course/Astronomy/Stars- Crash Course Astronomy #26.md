---
version: 1
type: video
provider: YouTube
id: ld75W1dz-h0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Stars-%20Crash%20Course%20Astronomy%20%2326.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3d747b7f-d71a-48b2-9186-857bb3a96f0c
updated: 1486069684
title: 'Stars- Crash Course Astronomy #26'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/ld75W1dz-h0/default.jpg
    - https://i3.ytimg.com/vi/ld75W1dz-h0/1.jpg
    - https://i3.ytimg.com/vi/ld75W1dz-h0/2.jpg
    - https://i3.ytimg.com/vi/ld75W1dz-h0/3.jpg
---
