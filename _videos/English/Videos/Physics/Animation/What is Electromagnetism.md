---
version: 1
type: video
provider: YouTube
id: -B9-icPm6Bw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20Electromagnetism.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c6b2db46-7baf-4968-83b4-8d351060b2a4
updated: 1486069654
title: What is Electromagnetism
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/-B9-icPm6Bw/default.jpg
    - https://i3.ytimg.com/vi/-B9-icPm6Bw/1.jpg
    - https://i3.ytimg.com/vi/-B9-icPm6Bw/2.jpg
    - https://i3.ytimg.com/vi/-B9-icPm6Bw/3.jpg
---
