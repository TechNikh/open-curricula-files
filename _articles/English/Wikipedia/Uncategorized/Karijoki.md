---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Botom
offline_file: ""
offline_thumbnail: ""
uuid: b0212ceb-5401-4871-8722-5967d6cb94a1
updated: 1484308327
title: Karijoki
tags:
    - Villages
    - Notable individuals
categories:
    - Uncategorized
---
It is located in the province of Western Finland and is part of the Southern Ostrobothnia region. The population of Karijoki is 1,363 (March 31, 2016)[2] and the municipality covers an area of 185.77 km2 (71.73 sq mi) of which 0.78 km2 (0.30 sq mi) is inland water (January 1, 2011).[1] The population density is 7.34/km2 (19.0/sq mi).
