---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rhombus
offline_file: ""
offline_thumbnail: ""
uuid: 4e5abee0-5d4d-4979-8035-3f71842274c6
updated: 1484310156
title: rhombus
categories:
    - Dictionary
---
rhombus
     n : a parallelogram with four equal sides; an oblique-angled
         equilateral parallelogram [syn: {rhomb}]
     [also: {rhombi} (pl)]
