---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/doob
offline_file: ""
offline_thumbnail: ""
uuid: 43d0faa1-f0fe-49bf-a9ba-ebdb59c15561
updated: 1484310307
title: doob
categories:
    - Dictionary
---
doob
     n : trailing grass native to Europe now cosmopolitan in warm
         regions; used for lawns and pastures especially in
         southern United States and India [syn: {Bermuda grass}, {devil
         grass}, {Bahama grass}, {kweek}, {scutch grass}, {star
         grass}, {Cynodon dactylon}]
