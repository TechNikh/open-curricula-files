---
version: 1
type: video
provider: YouTube
id: BNqIu0QvA8g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d5d56507-134a-4399-9eaa-f41acd303b0e
updated: 1486069641
title: "Physics Lab - 6. Ohm's Law, Series and Parallel Combination of Resistors"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/BNqIu0QvA8g/default.jpg
    - https://i3.ytimg.com/vi/BNqIu0QvA8g/1.jpg
    - https://i3.ytimg.com/vi/BNqIu0QvA8g/2.jpg
    - https://i3.ytimg.com/vi/BNqIu0QvA8g/3.jpg
---
