---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Critical_exponent
offline_file: ""
offline_thumbnail: ""
uuid: bf2ed967-2898-446e-a312-0f54986af679
updated: 1484308746
title: Critical exponent
tags:
    - Definition
    - The most important critical exponents
    - Mean field critical exponents of Ising-like systems
    - Experimental values
    - Scaling functions
    - Scaling relations
    - Anisotropy
    - Multicritical points
    - Static versus dynamic properties
    - Transport properties
    - Self-organized criticality
    - Percolation Theory
    - External links and literature
categories:
    - Critical phenomena
---
Critical exponents describe the behaviour of physical quantities near continuous phase transitions. It is believed, though not proven, that they are universal, i.e. they do not depend on the details of the physical system, but only on
