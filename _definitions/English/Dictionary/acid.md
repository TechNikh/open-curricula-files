---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acid
offline_file: ""
offline_thumbnail: ""
uuid: 5472363b-b4bb-4b13-86e2-1babc6507878
updated: 1484310333
title: acid
categories:
    - Dictionary
---
acid
     adj 1: harsh or corrosive in tone; "an acerbic tone piercing
            otherwise flowery prose"; "a barrage of acid
            comments"; "her acrid remarks make her many enemies";
            "bitter words"; "blistering criticism"; "caustic jokes
            about political assassination, talk-show hosts and
            medical ethics"; "a sulfurous denunciation" [syn: {acerb},
             {acerbic}, {acrid}, {bitter}, {blistering}, {caustic},
             {sulfurous}, {sulphurous}, {venomous}, {virulent}, {vitriolic}]
     2: containing acid; "an acid taste"
     n 1: any of various water-soluble compounds having a sour taste
          and capable of turning litmus red and ...
