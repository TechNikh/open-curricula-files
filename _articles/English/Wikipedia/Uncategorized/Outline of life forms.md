---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Living_beings
offline_file: ""
offline_thumbnail: ""
uuid: 5448abcd-896e-4153-b7fc-3517824689f8
updated: 1484308363
title: Outline of life forms
tags:
    - Archaea
    - Bacteria
    - Eukaryote
categories:
    - Uncategorized
---
Estimates on the number of Earth's current species range from 10 million to 14 million,[3] of which about 1.2 million have been documented and over 86 percent have not yet been described.[4] More recently, in May 2016, scientists reported that 1 trillion species are estimated to be on Earth currently with only one-thousandth of one percent described.[5]
