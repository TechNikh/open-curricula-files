---
version: 1
type: video
provider: YouTube
id: Fpm-E5v6ddc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Number%20Base%20Conversion%20Practice.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2dc1b47d-f4f9-4f41-bde3-409b5b0398cc
updated: 1486069565
title: Number Base Conversion Practice
tags:
    - base
    - sixteen
    - two
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/Fpm-E5v6ddc/default.jpg
    - https://i3.ytimg.com/vi/Fpm-E5v6ddc/1.jpg
    - https://i3.ytimg.com/vi/Fpm-E5v6ddc/2.jpg
    - https://i3.ytimg.com/vi/Fpm-E5v6ddc/3.jpg
---
Converting between decimal, binary and hexadecimal number systems
