---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/filled
offline_file: ""
offline_thumbnail: ""
uuid: 7a83e855-4081-439f-873e-70d11d703d56
updated: 1484310337
title: filled
categories:
    - Dictionary
---
filled
     adj 1: (usually followed by `with' or used as a combining form)
            generously supplied with; "theirs was a house filled
            with laughter"; "a large hall filled with rows of
            desks"; "fog-filled air"
     2: (of time) taken up; "well-filled hours"
