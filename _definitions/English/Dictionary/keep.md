---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/keep
offline_file: ""
offline_thumbnail: ""
uuid: 17632af5-1fa0-4261-ac2f-7b7172427ca2
updated: 1484310355
title: keep
categories:
    - Dictionary
---
keep
     n 1: the financial means whereby one lives; "each child was
          expected to pay for their keep"; "he applied to the
          state for support"; "he could no longer earn his own
          livelihood" [syn: {support}, {livelihood}, {living}, {bread
          and butter}, {sustenance}]
     2: the main tower within the walls of a medieval castle or
        fortress [syn: {donjon}, {dungeon}]
     3: a cell in a jail or prison [syn: {hold}]
     v 1: keep in a certain state, position, or activity; e.g., "keep
          clean"; "hold in place"; "She always held herself as a
          lady"; "The students keep me on my toes" [syn: {maintain},
           {hold}]
     2: continue ...
