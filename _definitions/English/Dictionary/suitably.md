---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suitably
offline_file: ""
offline_thumbnail: ""
uuid: 63e8b5bd-393d-4d5d-bd08-94eabe7bdead
updated: 1484310260
title: suitably
categories:
    - Dictionary
---
suitably
     adv : in an appropriate manner; "he was appropriately dressed"
           [syn: {appropriately}, {fittingly}, {befittingly}, {fitly}]
           [ant: {inappropriately}, {inappropriately}]
