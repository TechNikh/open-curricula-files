---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamming_weight
offline_file: ""
offline_thumbnail: ""
uuid: 2d5f423b-f1df-4335-afa3-8de64c4b0b73
updated: 1484309025
title: Hamming weight
tags:
    - History and usage
    - Efficient implementation
    - Language support
    - Processor support
categories:
    - Coding theory
---
The Hamming weight of a string is the number of symbols that are different from the zero-symbol of the alphabet used. It is thus equivalent to the Hamming distance from the all-zero string of the same length. For the most typical case, a string of bits, this is the number of 1's in the string. In this binary case, it is also called the population count, popcount, or sideways sum.[1] It is the digit sum of the binary representation of a given number and the ℓ₁ norm of a bit vector.
