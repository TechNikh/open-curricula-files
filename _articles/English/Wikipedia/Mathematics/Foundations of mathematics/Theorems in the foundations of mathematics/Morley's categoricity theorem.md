---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Morley%27s_categoricity_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 30d5d146-96c9-4742-abae-f610e20a560d
updated: 1484308997
title: "Morley's categoricity theorem"
tags:
    - History and motivation
    - Examples
categories:
    - Theorems in the foundations of mathematics
---
In model theory, a branch of mathematical logic, a theory is κ-categorical (or categorical in κ) if it has exactly one model of cardinality κ up to isomorphism. Morley's categoricity theorem is a theorem of Michael D. Morley (1965), which states that if a first-order theory in a countable language is categorical in some uncountable cardinality, then it is categorical in all uncountable cardinalities.
