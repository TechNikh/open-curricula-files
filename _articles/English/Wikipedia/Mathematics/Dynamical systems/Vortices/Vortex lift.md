---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vortex_lift
offline_file: ""
offline_thumbnail: ""
uuid: f756d17d-eed6-4e74-94a5-a4ee69930331
updated: 1484308881
title: Vortex lift
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Vortex_lift.gif
categories:
    - Vortices
---
Vortex lift works by capturing vortices generated from the sharply swept leading edge of the wing. The vortex, formed roughly parallel to the leading edge of the wing, is trapped by the airflow and remains fixed to the upper surface of the wing. As the air flows around the leading edge, it flows over the trapped vortex and is pulled in and down to generate the lift.
