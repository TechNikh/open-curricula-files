---
version: 1
type: video
provider: YouTube
id: usH9VUi2-Xg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Constructing%20a%20unit%20normal%20vector%20to%20a%20surface.webm"
offline_file: ""
offline_thumbnail: ""
uuid: dad0f10d-0a61-4497-a04e-ec88641ad17e
updated: 1486069692
title: Constructing a unit normal vector to a surface
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/usH9VUi2-Xg/default.jpg
    - https://i3.ytimg.com/vi/usH9VUi2-Xg/1.jpg
    - https://i3.ytimg.com/vi/usH9VUi2-Xg/2.jpg
    - https://i3.ytimg.com/vi/usH9VUi2-Xg/3.jpg
---
