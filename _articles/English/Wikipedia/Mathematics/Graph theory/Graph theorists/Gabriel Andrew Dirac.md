---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gabriel_Andrew_Dirac
offline_file: ""
offline_thumbnail: ""
uuid: 0d8ef616-cea4-4c07-a100-c3fbe9f9cd13
updated: 1484309461
title: Gabriel Andrew Dirac
tags:
    - Education
    - Career
    - family
    - Notes
categories:
    - Graph theorists
---
Gabriel Andrew Dirac (13 March 1925 – 20 July 1984) was a mathematician who mainly worked in graph theory. He stated a sufficient condition for a graph to contain a Hamiltonian circuit. In 1951 he conjectured that n points in the plane, not all collinear, must span at least [n/2] two-point lines, where [x] is the largest integer not exceeding x. This conjecture is still open.[1]
