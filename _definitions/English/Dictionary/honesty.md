---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/honesty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484469421
title: honesty
categories:
    - Dictionary
---
honesty
     n 1: the quality of being honest [syn: {honestness}] [ant: {dishonesty}]
     2: southeastern European plant cultivated for its fragrant
        purplish flowers and round flat papery silver-white
        seedpods that are used for indoor decoration [syn: {silver
        dollar}, {money plant}, {satin flower}, {satinpod}, {Lunaria
        annua}]
