---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comparatively
offline_file: ""
offline_thumbnail: ""
uuid: 576c1050-8393-4b61-82ca-d624c4b53edf
updated: 1484310448
title: comparatively
categories:
    - Dictionary
---
comparatively
     adv : in a relative manner; by comparison to something else; "the
           situation is relatively calm now" [syn: {relatively}]
