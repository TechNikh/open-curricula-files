---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/odd
offline_file: ""
offline_thumbnail: ""
uuid: d518fb4c-3ffa-49b3-b366-06718b787cf4
updated: 1484310453
title: odd
categories:
    - Dictionary
---
odd
     adj 1: not divisible by two [ant: {even}]
     2: not easily explained; "it is odd that his name is never
        mentioned"
     3: an indefinite quantity more than that specified; "invited
        30-odd guests"
     4: beyond or deviating from the usual or expected; "a curious
        hybrid accent"; "her speech has a funny twang"; "they have
        some funny ideas about war"; "had an odd name"; "the
        peculiar aromatic odor of cloves"; "something definitely
        queer about this town"; "what a rum fellow"; "singular
        behavior" [syn: {curious}, {funny}, {peculiar}, {queer}, {rum},
         {rummy}, {singular}]
     5: of the remaining member of a pair, of socks ...
