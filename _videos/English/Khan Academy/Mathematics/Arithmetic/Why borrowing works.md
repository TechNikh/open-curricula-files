---
version: 1
type: video
provider: YouTube
id: fWan_T0enj4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Why%20borrowing%20works.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4a435ece-ddbd-41d4-92bb-c6670c2aecd2
updated: 1486069567
title: Why borrowing works
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/fWan_T0enj4/default.jpg
    - https://i3.ytimg.com/vi/fWan_T0enj4/1.jpg
    - https://i3.ytimg.com/vi/fWan_T0enj4/2.jpg
    - https://i3.ytimg.com/vi/fWan_T0enj4/3.jpg
---
An explanation of why (not how) borrowing/regrouping works when subtracting numbers
