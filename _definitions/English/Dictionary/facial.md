---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/facial
offline_file: ""
offline_thumbnail: ""
uuid: 41d52add-ce52-45a1-a903-5762a83c3f24
updated: 1484310316
title: facial
categories:
    - Dictionary
---
facial
     adj 1: of or concerning the face; "a facial massage"; "facial
            hair"; "facial expression"
     2: of or pertaining to the outside surface of an object
     n 1: cranial nerve that supplies facial muscles [syn: {facial
          nerve}, {nervus facialis}, {seventh cranial nerve}]
     2: care for the face that usually involves cleansing and
        massage and the application of cosmetic creams
