---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/travel
offline_file: ""
offline_thumbnail: ""
uuid: 3d56e75a-a31d-4d67-88ef-6ba5adf311b7
updated: 1484310333
title: travel
categories:
    - Dictionary
---
travel
     n 1: the act of going from one place to another; "he enjoyed
          selling but he hated the travel" [syn: {traveling}, {travelling}]
     2: a movement through space that changes the location of
        something [syn: {change of location}]
     3: self-propelled movement [syn: {locomotion}]
     v 1: change location; move, travel, or proceed; "How fast does
          your new car go?"; "We travelled from Rome to Naples by
          bus"; "The policemen went from door to door looking for
          the suspect"; "The soldiers moved towards the city in an
          attempt to take it before night fell" [syn: {go}, {move},
           {locomote}] [ant: {stay in place}]
     2: ...
