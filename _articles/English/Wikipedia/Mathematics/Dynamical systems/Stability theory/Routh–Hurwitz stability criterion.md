---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Routh%E2%80%93Hurwitz_stability_criterion'
offline_file: ""
offline_thumbnail: ""
uuid: 334b5ed1-c636-4b6a-b734-ccb2cb64bfdb
updated: 1484308842
title: Routh–Hurwitz stability criterion
tags:
    - "Using Euclid's algorithm"
    - Using matrices
    - Example
    - >
        Routh–Hurwitz criterion for second, third, and
        fourth-order polynomials
    - Higher-order example
categories:
    - Stability theory
---
In control system theory, the Routh–Hurwitz stability criterion is a mathematical test that is a necessary and sufficient condition for the stability of a linear time invariant (LTI) control system. The Routh test is an efficient recursive algorithm that English mathematician Edward John Routh proposed in 1876 to determine whether all the roots of the characteristic polynomial of a linear system have negative real parts.[1] German mathematician Adolf Hurwitz independently proposed in 1895 to arrange the coefficients of the polynomial into a square matrix, called the Hurwitz matrix, and showed that the polynomial is stable if and only if the sequence of determinants of its principal ...
