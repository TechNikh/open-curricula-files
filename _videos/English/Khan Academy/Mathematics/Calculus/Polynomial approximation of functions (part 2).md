---
version: 1
type: video
provider: YouTube
id: 3JG3qn7-Sac
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Polynomial%20approximation%20of%20functions%20%28part%202%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4c53bd1d-41e8-45b6-8061-717d156a4d47
updated: 1486069693
title: Polynomial approximation of functions (part 2)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/3JG3qn7-Sac/default.jpg
    - https://i3.ytimg.com/vi/3JG3qn7-Sac/1.jpg
    - https://i3.ytimg.com/vi/3JG3qn7-Sac/2.jpg
    - https://i3.ytimg.com/vi/3JG3qn7-Sac/3.jpg
---
