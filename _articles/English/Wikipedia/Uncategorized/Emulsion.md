---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Emulsify
offline_file: ""
offline_thumbnail: ""
uuid: 9ee8c139-0ef0-4eaa-b296-a52e55f66b34
updated: 1484308409
title: Emulsion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Emulsions.svg.png
tags:
    - Appearance and properties
    - Instability
    - Monitoring physical stability
    - Accelerating methods for shelf life prediction
    - Emulsifiers
    - Mechanisms of emulsification
    - Uses
    - In food
    - Health care
    - In firefighting
    - Chemical synthesis
    - Other sources
categories:
    - Uncategorized
---
An emulsion is a mixture of two or more liquids that are normally immiscible (unmixable or unblendable). Emulsions are part of a more general class of two-phase systems of matter called colloids. Although the terms colloid and emulsion are sometimes used interchangeably, emulsion should be used when both phases, dispersed and continuous, are liquids. In an emulsion, one liquid (the dispersed phase) is dispersed in the other (the continuous phase). Examples of emulsions include vinaigrettes, homogenized milk, mayonnaise, and some cutting fluids for metal working.
