---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acidity
offline_file: ""
offline_thumbnail: ""
uuid: 1f373645-cb8a-497b-973d-27680b4d8c35
updated: 1484310379
title: acidity
categories:
    - Dictionary
---
acidity
     n 1: the property of being acidic [syn: {sourness}, {sour}]
     2: the taste experience when something acidic is taken into the
        mouth [syn: {acidulousness}]
     3: pH values below 7 [ant: {alkalinity}]
