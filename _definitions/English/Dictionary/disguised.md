---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disguised
offline_file: ""
offline_thumbnail: ""
uuid: 05f1fc35-311e-4fca-8453-6b05a16ada95
updated: 1484310453
title: disguised
categories:
    - Dictionary
---
disguised
     adj 1: having its true character concealed with the intent of
            misleading; "hidden agenda"; "masked threat" [syn: {cloaked},
             {masked}]
     2: deliberately concealed as if with a veil; "disguised
        threats"
