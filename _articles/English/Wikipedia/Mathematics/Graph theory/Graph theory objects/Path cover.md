---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Path_cover
offline_file: ""
offline_thumbnail: ""
uuid: 8e9817e7-70f7-4b3c-b72e-0a121d766701
updated: 1484309447
title: Path cover
tags:
    - Properties
    - Computational complexity
    - Applications
    - Notes
categories:
    - Graph theory objects
---
Given a directed graph G = (V, E), a path cover is a set of directed paths such that every vertex v ∈ V belongs to at least one path. Note that a path cover may include paths of length 0 (a single vertex).[1]
