---
version: 1
type: video
provider: YouTube
id: 7K4V0NvUxRg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3d00c218-650d-40eb-b1a9-e835f92f6018
updated: 1486069666
title: The story of kinetic and potential energy
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/7K4V0NvUxRg/default.jpg
    - https://i3.ytimg.com/vi/7K4V0NvUxRg/1.jpg
    - https://i3.ytimg.com/vi/7K4V0NvUxRg/2.jpg
    - https://i3.ytimg.com/vi/7K4V0NvUxRg/3.jpg
---
