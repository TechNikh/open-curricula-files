---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unconditional
offline_file: ""
offline_thumbnail: ""
uuid: 0aebc41d-a551-4593-927e-cec9fcbab993
updated: 1484310559
title: unconditional
categories:
    - Dictionary
---
unconditional
     adj 1: not conditional; "unconditional surrender" [ant: {conditional}]
     2: not modified or restricted by reservations; "a categorical
        denial"; "a flat refusal" [syn: {categoric}, {categorical},
         {flat}]
     3: not contingent; not determined or influenced by someone or
        something else
