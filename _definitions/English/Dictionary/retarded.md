---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retarded
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484554741
title: retarded
categories:
    - Dictionary
---
retarded
     adj : relatively slow in mental or emotional or physical
           development; "providing a secure and sometimes happy
           life for the retarded" [ant: {precocious}]
