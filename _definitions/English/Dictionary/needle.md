---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/needle
offline_file: ""
offline_thumbnail: ""
uuid: 88bc1ae4-e531-498e-b0bb-db0e68de0299
updated: 1484310210
title: needle
categories:
    - Dictionary
---
needle
     n 1: as of a conifer [syn: {acerate leaf}]
     2: a slender pointer for indicating the reading on the scale of
        a measuring instrument
     3: a sharp pointed implement (usually steel)
     4: a stylus that formerly made sound by following a groove in a
        phonograph record [syn: {phonograph needle}]
     v 1: goad or provoke,as by constant criticism; "He needled her
          with his sarcastic remarks" [syn: {goad}]
     2: prick with a needle
