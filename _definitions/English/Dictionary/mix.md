---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mix
offline_file: ""
offline_thumbnail: ""
uuid: 97a66883-d1d3-44bd-b8d5-0797c37daf25
updated: 1484310330
title: mix
categories:
    - Dictionary
---
mix
     n 1: a commercially prepared mixture of dry ingredients [syn: {premix}]
     2: an event that combines things in a mixture; "a gradual
        mixture of cultures" [syn: {mixture}]
     3: the act of mixing together; "paste made by a mix of flour
        and water"; "the mixing of sound channels in the recording
        studio" [syn: {commixture}, {admixture}, {mixture}, {intermixture},
         {mixing}]
     v 1: mix together different elements; "The colors blend well"
          [syn: {blend}, {flux}, {conflate}, {commingle}, {immix},
           {fuse}, {coalesce}, {meld}, {combine}, {merge}]
     2: open (a place) to members of all races and ethnic groups;
        "This school ...
