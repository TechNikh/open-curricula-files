---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meticulous
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415181
title: meticulous
categories:
    - Dictionary
---
meticulous
     adj 1: marked by precise accordance with details; "was worryingly
            meticulous about trivial details"; "punctilious in his
            attention to rules of etiquette" [syn: {punctilious}]
     2: marked by extreme care in treatment of details; "a
        meticulous craftsman"; "almost worryingly meticulous in
        his business formalities" [syn: {picky}]
