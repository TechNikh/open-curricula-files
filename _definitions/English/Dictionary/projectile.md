---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/projectile
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484320561
title: projectile
categories:
    - Dictionary
---
projectile
     adj : impelling or impelled forward; "a projectile force"; "a
           projectile missile"
     n : a weapon that is thrown or projected [syn: {missile}]
