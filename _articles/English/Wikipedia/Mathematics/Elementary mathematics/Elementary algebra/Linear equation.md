---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_equation
offline_file: ""
offline_thumbnail: ""
uuid: 97526885-2acf-4d9f-bcb6-2b54b59d6bd7
updated: 1484308909
title: Linear equation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Linear_Function_Graph.svg.png
tags:
    - One variable
    - Two variables
    - Forms for two-dimensional linear equations
    - General (or standard) form
    - Slope–intercept form
    - Point–slope form
    - Two-point form
    - Intercept form
    - Matrix form
    - Parametric form
    - 2D vector determinant form
    - Special cases
    - Connection with linear functions
    - Examples
    - More than two variables
    - Notes
categories:
    - Elementary algebra
---
A linear equation is an algebraic equation in which each term is either a constant or the product of a constant and (the first power of) a single variable. A simple example of a linear equation with only one variable, x, may be written in the form: ax + b = 0, where a and b are constants and a ≠ 0. The constants may be numbers, parameters, or even non-linear functions of parameters, and the distinction between variables and parameters may depend on the problem (for an example, see linear regression).
