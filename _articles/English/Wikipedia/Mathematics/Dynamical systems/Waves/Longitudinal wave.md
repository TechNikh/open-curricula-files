---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Longitudinal_wave
offline_file: ""
offline_thumbnail: ""
uuid: b7b3dfc2-e75f-4849-87c8-60661ad1a589
updated: 1484308889
title: Longitudinal wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Onde_compression_impulsion_1d_30_petit.gif
tags:
    - Examples
    - Sound waves
    - Pressure waves
    - Electromagnetic
categories:
    - Waves
---
Longitudinal waves, also known as "1 waves", are wave/s in which the displacement of the medium is in the same direction as, or the opposite direction to, the direction of travel of the wave. Mechanical longitudinal waves are also called compressional waves or compression waves, because they produce compression and rarefaction when traveling through a medium, and pressure waves, because they produce increases and decreases in pressure. The other main type of wave is the transverse wave, in which the displacements of the medium are at right angles to the direction of propagation. Some transverse waves are mechanical, meaning that the wave needs a medium to travel through. Transverse ...
