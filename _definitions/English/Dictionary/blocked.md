---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blocked
offline_file: ""
offline_thumbnail: ""
uuid: 80596503-9de8-4450-b908-99dd7fc3604c
updated: 1484310591
title: blocked
categories:
    - Dictionary
---
blocked
     adj 1: closed to traffic; "the repaving results in many blocked
            streets" [syn: {out of use(p)}]
     2: completely obstructed or closed off; "the storm was
        responsible for many blocked roads and bridges"; "the
        drain was plugged" [syn: {plugged}]
