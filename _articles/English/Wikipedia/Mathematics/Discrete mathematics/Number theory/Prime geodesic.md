---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Prime_geodesic
offline_file: ""
offline_thumbnail: ""
uuid: 2307abc0-81ab-4165-bbb6-003c7dd75c0f
updated: 1484308657
title: Prime geodesic
tags:
    - Technical background
    - Hyperbolic isometries
    - Closed geodesics
    - Applications of prime geodesics
    - Dynamical systems and ergodic theory
    - Number theory
    - Riemann surface theory
categories:
    - Number theory
---
In mathematics, a prime geodesic on a hyperbolic surface is a primitive closed geodesic, i.e. a geodesic which is a closed curve that traces out its image exactly once. Such geodesics are called prime geodesics because, among other things, they obey an asymptotic distribution law similar to the prime number theorem.
