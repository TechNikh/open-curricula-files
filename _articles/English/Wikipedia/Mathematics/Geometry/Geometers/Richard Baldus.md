---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Richard_Baldus
offline_file: ""
offline_thumbnail: ""
uuid: 388bdeec-0dd5-4f42-b796-bbe089b2284e
updated: 1484309071
title: Richard Baldus
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Baldus%252CRichard_1930_Jena.jpg'
categories:
    - Geometers
---
Richard Baldus was the son of a station chief of the Anatolian Railway. After his graduation (Abitur) in 1904 at Wilhelmsgymnasium München,[2] he studied in Munich and at the University of Erlangen, where he received his Ph.D. (Promotierung) in 1910 under Max Noether with thesis Über Strahlensysteme, welche unendlich viele Regelflächen 2. Grades enthalten[3] and where he received his Habilitierung in 1911. He became in 1919 Professor für Geometrie at the Technische Hochschule Karlsruhe and served there as rector in 1923–1924. In 1932 he became Professor für Geometrie (as successor to Sebastian Finsterwalder) at TU München, where in 1934 he also became the successor to the ...
