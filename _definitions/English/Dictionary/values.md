---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/values
offline_file: ""
offline_thumbnail: ""
uuid: e4170cde-55c6-485e-b628-6a2e2fde59a8
updated: 1484310264
title: values
categories:
    - Dictionary
---
values
     n : beliefs of a person or social group in which they have an
         emotional investment (either for or against something);
         "he has very conservatives values"
