---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Milk_production
offline_file: ""
offline_thumbnail: ""
uuid: d7c21c1f-00a4-4522-bd3f-7a17d1b869be
updated: 1484308368
title: Dairy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/440px-Alte_Batzenalpe_2014_Interior_06.jpg
tags:
    - History
    - Structure of the industry
    - Farming
    - Industrial processing
    - Cream and butter
    - Skimmed milk
    - Casein
    - Cheese
    - Whey
    - Yogurt
    - Milk powders
    - Other milk products
    - Milking
    - Milking machines
    - Milking shed layouts
    - Bail-style sheds
    - Herringbone milking parlours
    - Swingover milking Parlours
    - Rotary milking sheds
    - Automatic milking sheds
    - Supplementary accessories in sheds
    - Temporary milk storage
    - Waste disposal
    - Associated diseases
    - Animal welfare
    - Notes
categories:
    - Uncategorized
---
A dairy is a business enterprise established for the harvesting or processing (or both) of animal milk – mostly from cows or goats, but also from buffaloes, sheep, horses, or camels – for human consumption. A dairy is typically located on a dedicated dairy farm or in a section of a multi-purpose farm (mixed farm) that is concerned with the harvesting of milk.
