---
version: 1
type: video
provider: YouTube
id: y96bNYYnJ6k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Special%20Relativity%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3d5dbc23-7e85-4d92-9fda-f4be8c6d5bf3
updated: 1486069652
title: Special Relativity 2
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/y96bNYYnJ6k/default.jpg
    - https://i3.ytimg.com/vi/y96bNYYnJ6k/1.jpg
    - https://i3.ytimg.com/vi/y96bNYYnJ6k/2.jpg
    - https://i3.ytimg.com/vi/y96bNYYnJ6k/3.jpg
---
