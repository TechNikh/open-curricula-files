---
version: 1
type: video
provider: YouTube
id: _GiPxFvUUxs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/The%20Apostrophe/Possession%20for%20words%20ending%20in%20%E2%80%9Cs%E2%80%9D.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 631e639f-b88a-4724-b845-d4f3d6263d45
updated: 1486069599
title: Possession for words ending in “s”
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The Apostrophe
thumbnail_urls:
    - https://i3.ytimg.com/vi/_GiPxFvUUxs/default.jpg
    - https://i3.ytimg.com/vi/_GiPxFvUUxs/1.jpg
    - https://i3.ytimg.com/vi/_GiPxFvUUxs/2.jpg
    - https://i3.ytimg.com/vi/_GiPxFvUUxs/3.jpg
---
Do words that end in "s" still need _'s_ to show possession? David and Paige explain!

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/the-apostrophe/e/introduction-to-the-possessive-case

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/advanced-plural-possession-the-apostrophe-punctuation-khan-academy

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/introduction-to-the-possessive-the-apostrophe-punctuation-khan-academy

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
