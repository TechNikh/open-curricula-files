---
version: 1
type: article
id: https://en.wikipedia.org/wiki/AntiqueWeek
offline_file: ""
offline_thumbnail: ""
uuid: b18d289b-1477-4346-a256-6bebd25d71fe
updated: 1484309011
title: AntiqueWeek
categories:
    - Auctions
---
AntiqueWeek is one of the most popular antiques newspapers in the United States and has been a source for weekly antiques and collectibles news in the United States since 1968. AntiqueWeek has a weekly circulation of over 70,000 readers. The newspaper was purchased by the startup company, MidCountry Media, Inc. in September 2009.
