---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Continuum_(set_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 052f2732-b6a7-4e27-9faa-c45df3b8d72d
updated: 1484309504
title: Continuum (set theory)
categories:
    - Set theory
---
In the mathematical field of set theory, the continuum means the real numbers, or the corresponding (infinite) cardinal number, 
  
    
      
        
          
            c
          
        
      
    
    {\displaystyle {\mathfrak {c}}}
  
. Georg Cantor proved that the cardinality 
  
    
      
        
          
            c
          
        
      
    
    {\displaystyle {\mathfrak {c}}}
  
 is larger than the smallest infinity, namely, 
  
    
      
        
          ℵ
          
            0
          
        
      
    
    {\displaystyle \aleph _{0}}
  
. He also proved that 
  
    
      
        
          
            c
          
        
      
    
    ...
