---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/segregated
offline_file: ""
offline_thumbnail: ""
uuid: 9d85c476-c706-4a6c-8f39-8722beb7f9f1
updated: 1484310172
title: segregated
categories:
    - Dictionary
---
segregated
     adj : separated or isolated from others or a main group; "a
           segregated school system"; "a segregated neighborhood"
           [syn: {unintegrated}] [ant: {integrated}]
