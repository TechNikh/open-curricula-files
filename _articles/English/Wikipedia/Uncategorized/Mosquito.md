---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mosquitoes
offline_file: ""
offline_thumbnail: ""
uuid: e4892ef9-6c51-468f-9634-ab20e305e559
updated: 1484308466
title: Mosquito
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Culex_restuans_larva_diagram_en.svg.png
tags:
    - Taxonomy and evolution
    - Subfamilies
    - Genera
    - species
    - Lifecycle
    - Eggs and oviposition
    - Larva
    - Pupa
    - Adult
    - Feeding by adults
categories:
    - Uncategorized
---
Mosquitoes are small, midge-like flies that constitute the family Culicidae. Females of most species are ectoparasites, whose tube-like mouthparts (called a proboscis) pierce the hosts' skin to consume blood. The word "mosquito" (formed by mosca and diminutive -ito)[2] is Spanish for "little fly".[3] Thousands of species feed on the blood of various kinds of hosts, mainly vertebrates, including mammals, birds, reptiles, amphibians, and even some kinds of fish. Some mosquitoes also attack invertebrates, mainly other arthropods. Though the loss of blood is seldom of any importance to the victim, the saliva of the mosquito often causes an irritating rash that is a serious nuisance. Much more ...
