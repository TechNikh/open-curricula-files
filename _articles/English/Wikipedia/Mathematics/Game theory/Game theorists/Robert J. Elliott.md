---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_J._Elliott
offline_file: ""
offline_thumbnail: ""
uuid: 68b46ee5-c0fb-4038-9d49-5df9fe718795
updated: 1484308513
title: Robert J. Elliott
categories:
    - Game theorists
---
Robert James Elliott (born 1940) is a British-Canadian mathematician, known for his contributions to control theory, game theory, stochastic processes and mathematical finance.
