---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/baked
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484615281
title: baked
categories:
    - Dictionary
---
baked
     adj 1: dried out by heat or excessive exposure to sunlight; "a vast
            desert all adust"; "land lying baked in the heat";
            "parched soil"; "the earth was scorched and bare";
            "sunbaked salt flats" [syn: {adust}, {parched}, {scorched},
             {sunbaked}]
     2: (of bread and pastries) cooked by dry heat (as in an oven);
        "baked goods"
     3: hardened by subjecting to intense heat; "baked bricks";
        "burned bricks" [syn: {burned}, {burnt}]
