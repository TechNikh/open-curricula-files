---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484574421
title: contest
categories:
    - Dictionary
---
contest
     n 1: an occasion on which a winner is selected from among two or
          more contestants [syn: {competition}]
     2: a struggle between rivals
     v : to make the subject of dispute, contention, or litigation;
         "They contested the outcome of the race" [syn: {contend},
          {repugn}]
