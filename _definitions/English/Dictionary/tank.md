---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tank
offline_file: ""
offline_thumbnail: ""
uuid: 1a2c520b-a9d0-4a02-8752-48f6154c7abd
updated: 1484310321
title: tank
categories:
    - Dictionary
---
tank
     n 1: an enclosed armored military vehicle; has a cannon and moves
          on caterpillar treads [syn: {army tank}, {armored combat
          vehicle}, {armoured combat vehicle}]
     2: a large (usually metallic) vessel for holding gases or
        liquids [syn: {storage tank}]
     3: as much as a tank will hold [syn: {tankful}]
     4: a freight car that transports liquids or gases in bulk [syn:
         {tank car}]
     5: a cell for violent prisoners [syn: {cooler}]
     v 1: store in a tank by causing (something) to flow into it
     2: treat in a tank; "tank animal refuse"
