---
title: Travel of Air Through Respiratory System - Gas Exchange in the Lungs - Nose to Alveoli Pathway
youtube_id: 6uCY4x_3v0I
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - pathway
---
