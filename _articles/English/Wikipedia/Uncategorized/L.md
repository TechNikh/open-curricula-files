---
version: 1
type: article
id: https://en.wikipedia.org/wiki/L
offline_file: ""
offline_thumbnail: ""
uuid: d32478af-84b5-4086-9c62-555d928f5bd5
updated: 1484308386
title: L
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-L_cursiva.gif
tags:
    - History
    - Use in writing systems
    - english
    - Other languages
    - Other systems
    - Other uses
    - Forms and variants
    - Related characters
    - Descendants and related characters in the Latin alphabet
    - Derived signs, symbols and abbreviations
    - Ancestors and siblings in other alphabets
    - Computing codes
    - Other representations
categories:
    - Uncategorized
---
L (named el/ˈɛl/)[1] is the twelfth letter of the modern English alphabet and the ISO basic Latin alphabet, used in words such as lagoon, lantern, and less.
