---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nikolai_Lobachevsky
offline_file: ""
offline_thumbnail: ""
uuid: 4004bc79-705e-47d9-b47d-ac2cb094bca8
updated: 1484309090
title: Nikolai Lobachevsky
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Celebrating_the_birthday_of_Lobachevsky.JPG
tags:
    - life
    - Career
    - Impact
    - Honors
    - In popular culture
    - Works
categories:
    - Geometers
---
Nikolai Ivanovich Lobachevsky (Russian: Никола́й Ива́нович Лобаче́вский; IPA: [nʲikɐˈlaj ɪˈvanəvʲɪtɕ ləbɐˈtɕɛfskʲɪj] ( listen); 1 December [O.S. 20 November] 1792 – 24 February [O.S. 12 February] 1856) was a Russian mathematician and geometer, known primarily for his work on hyperbolic geometry, otherwise known as Lobachevskian geometry.
