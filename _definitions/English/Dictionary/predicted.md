---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/predicted
offline_file: ""
offline_thumbnail: ""
uuid: f77a8158-c813-49a5-b7e5-7da3ba16f64c
updated: 1484310254
title: predicted
categories:
    - Dictionary
---
predicted
     adj : known beforehand [syn: {foreseen}, {foretold}]
