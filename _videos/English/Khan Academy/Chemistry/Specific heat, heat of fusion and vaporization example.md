---
version: 1
type: video
provider: YouTube
id: zz4KbvF_X-0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Specific%20heat%2C%20heat%20of%20fusion%20and%20vaporization%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 35dae56b-1e53-4513-ae3e-8aca1880c9cb
updated: 1486069589
title: Specific heat, heat of fusion and vaporization example
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/zz4KbvF_X-0/default.jpg
    - https://i3.ytimg.com/vi/zz4KbvF_X-0/1.jpg
    - https://i3.ytimg.com/vi/zz4KbvF_X-0/2.jpg
    - https://i3.ytimg.com/vi/zz4KbvF_X-0/3.jpg
---
Specific heat and phase changes: Calculating how much heat is needed to convert 200 g of ice at -10 degrees C to 110 degree steam.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/states-of-matter-and-intermolecular-forces/states-of-matter/v/chilling-water-problem?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/states-of-matter-and-intermolecular-forces/states-of-matter/v/specific-heat-and-latent-leat-of-fusion-and-vaporization-2?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
