---
version: 1
type: video
provider: YouTube
id: N5kkwVoAtkc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Derivative%20of%20x%5E%28x%5Ex%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 831e27e7-306b-4008-b6a3-717ebe85d584
updated: 1486069692
title: Derivative of x^(x^x)
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/N5kkwVoAtkc/default.jpg
    - https://i3.ytimg.com/vi/N5kkwVoAtkc/1.jpg
    - https://i3.ytimg.com/vi/N5kkwVoAtkc/2.jpg
    - https://i3.ytimg.com/vi/N5kkwVoAtkc/3.jpg
---
