---
version: 1
type: video
provider: YouTube
id: yfawFJCRDSE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 48f0fbfc-c373-415d-8bb4-61b3f417167a
updated: 1486069636
title: "Snell's law of Refraction"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/yfawFJCRDSE/default.jpg
    - https://i3.ytimg.com/vi/yfawFJCRDSE/1.jpg
    - https://i3.ytimg.com/vi/yfawFJCRDSE/2.jpg
    - https://i3.ytimg.com/vi/yfawFJCRDSE/3.jpg
---
