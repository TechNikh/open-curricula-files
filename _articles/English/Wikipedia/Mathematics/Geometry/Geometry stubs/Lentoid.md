---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lentoid
offline_file: ""
offline_thumbnail: ""
uuid: 28008b41-2c13-45fb-af65-27268a898383
updated: 1484309365
title: Lentoid
categories:
    - Geometry stubs
---
Lentoid is a geometric shape of a three-dimensional body, best described as a circle viewed from one direction and a convex lens viewed from an orthogonal direction. The term is most often used in describing jewelry and cellular phenomena in microbiology.
