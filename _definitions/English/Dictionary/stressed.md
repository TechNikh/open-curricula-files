---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stressed
offline_file: ""
offline_thumbnail: ""
uuid: 165d5387-ce4a-4a9c-b2fb-6fcbbf0da344
updated: 1484310571
title: stressed
categories:
    - Dictionary
---
stressed
     adj 1: suffering severe physical strain or distress; "he dropped
            out of the race, clearly distressed and having
            difficulty breathing"; "the victim was in a bad way
            and needed immediate attention" [syn: {distressed}, {in
            a bad way(p)}]
     2: bearing a stress or accent; "an iambic foot consists of an
        unstressed syllable followed by a stressed syllable as in
        `delay'" [ant: {unstressed}]
