---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crucially
offline_file: ""
offline_thumbnail: ""
uuid: 6d7f5876-aae2-485c-8744-87d606eaae19
updated: 1484310549
title: crucially
categories:
    - Dictionary
---
crucially
     adv : to a crucial degree; "crucially important"; "crucially, he
           must meet us at the airport"
