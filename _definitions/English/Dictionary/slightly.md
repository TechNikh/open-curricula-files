---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slightly
offline_file: ""
offline_thumbnail: ""
uuid: 0d019f3b-fe7a-4598-9fe9-4fb8e700a801
updated: 1484310328
title: slightly
categories:
    - Dictionary
---
slightly
     adv 1: to a small degree or extent; "his arguments were somewhat
            self-contradictory"; "the children argued because one
            slice of cake was slightly larger than the other"
            [syn: {somewhat}]
     2: in a slim or slender manner; "a slenderly built woman";
        "slightly built" [syn: {slenderly}, {slimly}]
