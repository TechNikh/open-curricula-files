---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exponential_integrate-and-fire
offline_file: ""
offline_thumbnail: ""
uuid: 45963d3c-10bb-46d8-b174-a23f49c7a9fc
updated: 1484308811
title: Exponential integrate-and-fire
categories:
    - Nonlinear systems
---
The exponential integrate-and-fire model (EIF) is a biological neuron model, a simple modification of the classical integrate-and-fire model describing how neurons produce action potentials. In the EIF, the threshold for spike initiation is replaced by a depolarizing non-linearity. The model was first introduced by Nicolas Fourcaud-Trocmé, David Hansel, Carl van Vreeswijk and Nicolas Brunel.[1]
