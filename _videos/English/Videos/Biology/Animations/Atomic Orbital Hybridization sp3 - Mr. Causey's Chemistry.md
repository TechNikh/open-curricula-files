---
version: 1
type: video
provider: YouTube
id: vDREeV9nLEo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Atomic%20Orbital%20Hybridization%20sp3%20-%20Mr.%20Causey%27s%20Chemistry.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 44ab6c7b-24e8-466f-9c32-d9815f5a06c1
updated: 1486069666
title: "Atomic Orbital Hybridization sp3 - Mr. Causey's Chemistry"
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/vDREeV9nLEo/default.jpg
    - https://i3.ytimg.com/vi/vDREeV9nLEo/1.jpg
    - https://i3.ytimg.com/vi/vDREeV9nLEo/2.jpg
    - https://i3.ytimg.com/vi/vDREeV9nLEo/3.jpg
---
