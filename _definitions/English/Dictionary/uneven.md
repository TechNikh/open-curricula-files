---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uneven
offline_file: ""
offline_thumbnail: ""
uuid: b79d9000-fc6d-4d2e-86a2-460ad5f7c12c
updated: 1484310220
title: uneven
categories:
    - Dictionary
---
uneven
     adj 1: not even or uniform as e.g. in shape or texture; "an uneven
            color"; "uneven ground"; "uneven margins"; "wood with
            an uneven grain" [ant: {even}]
     2: (of a contest or contestants) not fairly matched as
        opponents; "vaudeville...waged an uneven battle against
        the church" [syn: {mismatched}]
     3: inconsistent in quality [syn: {spotty}]
