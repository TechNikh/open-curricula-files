---
version: 1
type: article
id: https://en.wikipedia.org/wiki/User:Weburbia/sandbox
offline_file: ""
offline_thumbnail: ""
uuid: 3c07e4f7-a3b0-42b2-a846-6cf4babf3e7d
updated: 1484308603
title: User:Weburbia/sandbox
categories:
    - Discrete geometry
---
Lebesgue's universal covering problem is an unsolved problem in geometry that asks for the convex shape of smallest area that can cover any planar set of diameter one. The diameter of a set by definition is the least upper bound of the distances between all pairs of points in the set. A shape covers a set if it contains a congruent subset. In other words the set may be rotated, translated or reflected to fit inside the shape.
