---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cramer%27s_paradox'
offline_file: ""
offline_thumbnail: ""
uuid: f62e92ff-34f7-448b-a3cf-512c97aae222
updated: 1484309138
title: "Cramer's paradox"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Two_cubic_curves.png
tags:
    - History
    - No paradox for lines and nondegenerate conics
    - "Cramer's example for cubic curves"
categories:
    - Algebraic geometry
---
In mathematics, Cramer's paradox or the Cramer–Euler paradox[1] is the statement that the number of points of intersection of two higher-order curves in the plane can be greater than the number of arbitrary points that are usually needed to define one such curve. It is named after the Swiss mathematician Gabriel Cramer.
