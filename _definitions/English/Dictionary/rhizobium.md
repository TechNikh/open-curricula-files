---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rhizobium
offline_file: ""
offline_thumbnail: ""
uuid: 07455fd1-0f60-4531-b6f0-2134508ec035
updated: 1484310547
title: rhizobium
categories:
    - Dictionary
---
Rhizobium
     n : the type genus of Rhizobiaceae; usually occur in the root
         nodules of legumes; can fix atmospheric oxygen [syn: {genus
         Rhizobium}]
