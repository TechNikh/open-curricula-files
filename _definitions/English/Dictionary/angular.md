---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/angular
offline_file: ""
offline_thumbnail: ""
uuid: dddd0826-28fa-419c-b5e7-4653c6197518
updated: 1484310393
title: angular
categories:
    - Dictionary
---
angular
     adj 1: measured by an angle or by the rate of change of an angle;
            "angular momentum"
     2: having angles or an angular shape [syn: {angulate}] [ant: {rounded}]
