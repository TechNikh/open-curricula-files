---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Rado_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: af3b5df4-3831-40fa-a071-3b31a776acb9
updated: 1484309504
title: Erdős–Rado theorem
categories:
    - Set theory
---
In partition calculus, part of combinatorial set theory, which is a branch of mathematics, the Erdős–Rado theorem is a basic result, extending Ramsey's theorem to uncountable sets.
