---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discuss
offline_file: ""
offline_thumbnail: ""
uuid: f8a3b606-cd6b-4671-b5eb-d2396a6b965e
updated: 1484310297
title: discuss
categories:
    - Dictionary
---
discuss
     v 1: to consider or examine in speech or writing; "The article
          covered all the different aspects of this question";
          "The class discussed Dante's `Inferno'" [syn: {discourse},
           {talk about}]
     2: speak with others about (something); talk (something) over
        in detail; have a discussion; "We discussed our household
        budget" [syn: {hash out}, {talk over}]
