---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discovery
offline_file: ""
offline_thumbnail: ""
uuid: 8a435ae4-2d86-4590-8471-04a6d961444d
updated: 1484310315
title: discovery
categories:
    - Dictionary
---
discovery
     n 1: the act of discovering something [syn: {find}, {uncovering}]
     2: something that is discovered
     3: a productive insight [syn: {breakthrough}, {find}]
     4: (law) compulsory pretrial disclosure of documents relevant
        to a case; enables one side in a litigation to elicit
        information from the other side concerning the facts in
        the case
