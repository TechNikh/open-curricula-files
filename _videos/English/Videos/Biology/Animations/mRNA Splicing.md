---
version: 1
type: video
provider: YouTube
id: FVuAwBGw_pQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/mRNA%20Splicing.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0cb5514f-6601-4df4-be3d-acdcb196077d
updated: 1486069668
title: mRNA Splicing
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/FVuAwBGw_pQ/default.jpg
    - https://i3.ytimg.com/vi/FVuAwBGw_pQ/1.jpg
    - https://i3.ytimg.com/vi/FVuAwBGw_pQ/2.jpg
    - https://i3.ytimg.com/vi/FVuAwBGw_pQ/3.jpg
---
