---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nanotubes
offline_file: ""
offline_thumbnail: ""
uuid: 592d6279-fea7-4707-8920-0e4643f053ab
updated: 1484308394
title: Nanotube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Kohlenstoffnanoroehre_Animation.gif
tags:
    - History
    - Kinds of nanotubes
categories:
    - Uncategorized
---
A nanotube is a nanometer-scale tube-like structure. A nanotube is a kind of nanoparticle, and may be large enough to serve as a pipe through which other nanoparticles can be channeled, or, depending on the material, may be used as an electrical conductor or an electrical insulator.
