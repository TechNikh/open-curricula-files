---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feasible
offline_file: ""
offline_thumbnail: ""
uuid: 67d12946-8502-40d0-8b5a-a827e3cbb443
updated: 1484310407
title: feasible
categories:
    - Dictionary
---
feasible
     adj : capable of being done with means at hand and circumstances
           as they are [syn: {executable}, {practicable}, {viable},
            {workable}]
     adv : in a practicable manner; so as to be feasible [syn: {practicably}]
