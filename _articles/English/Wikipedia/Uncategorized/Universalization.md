---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Universalisation
offline_file: ""
offline_thumbnail: ""
uuid: 0f8e0e17-d131-4044-a15e-c75ecbe8998c
updated: 1484308485
title: Universalization
categories:
    - Uncategorized
---
In social work practice universalisation[1] is a supportive intervention used by the therapist to reassure and encourage his/her client. Universalisation places the client’s experience in the context of other individuals who are experiencing the same, or similar challenges, and seeks to help the client grasp that his/her feelings and experiences are not uncommon given the circumstances. The therapist or social worker using this supportive intervention intends to "normalize" the client's experience of his/her emotions and reactions to the presenting challenge. By normalising the client’s experience the therapist is attempting to help avert the client's natural feelings of "being alone", ...
