---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/communist
offline_file: ""
offline_thumbnail: ""
uuid: 24bcbb12-85e1-4188-9fcd-850df39a482d
updated: 1484310555
title: communist
categories:
    - Dictionary
---
communist
     adj : relating to or marked by communism; "Communist Party";
           "communist governments"; "communistic propaganda" [syn:
            {communistic}]
     n 1: a member of the communist party
     2: a socialist who advocates communism [syn: {commie}]
