---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pop_up_art_auction
offline_file: ""
offline_thumbnail: ""
uuid: 91a2107d-47f9-4325-977c-3e7c336cbebc
updated: 1484309013
title: Pop up art auction
categories:
    - Auctions
---
A pop up art auction is a limited period of time art auction where the sale of artworks is conducted in less formal space aside of auction house, gallery or museum.
