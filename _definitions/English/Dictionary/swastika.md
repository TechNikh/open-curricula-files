---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swastika
offline_file: ""
offline_thumbnail: ""
uuid: 2c0e9e0d-3433-4294-a44a-1cd67a7adc83
updated: 1484310565
title: swastika
categories:
    - Dictionary
---
swastika
     n : the official emblem of the Nazi Party and the Third Reich; a
         cross with the arms bent at right angles in a clockwise
         direction [syn: {Hakenkreuz}]
