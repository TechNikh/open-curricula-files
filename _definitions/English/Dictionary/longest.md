---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/longest
offline_file: ""
offline_thumbnail: ""
uuid: 4b0cf2f3-ea45-44d3-bfb6-47d97c5f74c4
updated: 1484310416
title: longest
categories:
    - Dictionary
---
longest
     adj : having the greater length of two or the greatest length of
           several; "the longer (or long) edge of the door"; "the
           hypotenuse is the longest (or long) side of a right
           triangle" [syn: {longer}]
     adv : for the most time; "she stayed longest"
