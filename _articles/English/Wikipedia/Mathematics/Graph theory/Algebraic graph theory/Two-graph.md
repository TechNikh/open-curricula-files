---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Two-graph
offline_file: ""
offline_thumbnail: ""
uuid: f30003f3-6ebb-4b54-879d-497c8bb51f7a
updated: 1484309376
title: Two-graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Xyswitch.svg.png
tags:
    - Examples
    - Switching and graphs
    - Adjacency Matrix
    - Equiangular lines
    - Strongly regular graphs
    - Notes
categories:
    - Algebraic graph theory
---
In mathematics, a two-graph is a set of (unordered) triples chosen from a finite vertex set X, such that every (unordered) quadruple from X contains an even number of triples of the two-graph. A regular two-graph has the property that every pair of vertices lies in the same number of triples of the two-graph. Two-graphs have been studied because of their connection with equiangular lines and, for regular two-graphs, strongly regular graphs, and also finite groups because many regular two-graphs have interesting automorphism groups.
