---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/literary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484501161
title: literary
categories:
    - Dictionary
---
literary
     adj 1: of or relating to or characteristic of literature; "literary
            criticism"
     2: knowledgeable about literature; "a literary style" [syn: {well-written}]
     3: appropriate to literature rather than everyday speech or
        writing; "when trying to impress someone she spoke in an
        affected literary style"
