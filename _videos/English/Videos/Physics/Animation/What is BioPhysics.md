---
version: 1
type: video
provider: YouTube
id: g0ZTktJa07o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 21849906-f23c-442d-bd7c-acac8aa187e0
updated: 1486069654
title: What is BioPhysics
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/g0ZTktJa07o/default.jpg
    - https://i3.ytimg.com/vi/g0ZTktJa07o/1.jpg
    - https://i3.ytimg.com/vi/g0ZTktJa07o/2.jpg
    - https://i3.ytimg.com/vi/g0ZTktJa07o/3.jpg
---
