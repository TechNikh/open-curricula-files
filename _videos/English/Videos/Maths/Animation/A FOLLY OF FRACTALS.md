---
version: 1
type: video
provider: YouTube
id: PG4QK0xBkHY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 48421d65-f854-4413-a9cc-10c55b4cd845
updated: 1486069644
title: A FOLLY OF FRACTALS
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/PG4QK0xBkHY/default.jpg
    - https://i3.ytimg.com/vi/PG4QK0xBkHY/1.jpg
    - https://i3.ytimg.com/vi/PG4QK0xBkHY/2.jpg
    - https://i3.ytimg.com/vi/PG4QK0xBkHY/3.jpg
---
