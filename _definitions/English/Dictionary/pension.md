---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pension
offline_file: ""
offline_thumbnail: ""
uuid: 076da39d-8462-4ee7-81d5-1c751ec7d7cd
updated: 1484310443
title: pension
categories:
    - Dictionary
---
pension
     n : a regular payment to a person that iis intended to allow
         them to subsist without working
     v : grant a pension to [syn: {pension off}]
