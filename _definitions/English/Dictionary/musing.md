---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/musing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484467741
title: musing
categories:
    - Dictionary
---
musing
     adj : persistently or morbidly thoughtful [syn: {brooding}, {broody},
            {contemplative}, {meditative}, {pensive}, {pondering},
            {reflective}, {ruminative}]
     n : a calm lengthy intent consideration [syn: {contemplation}, {reflection},
          {reflexion}, {rumination}, {thoughtfulness}]
