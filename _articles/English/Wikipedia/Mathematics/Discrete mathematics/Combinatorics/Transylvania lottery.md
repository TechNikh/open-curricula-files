---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transylvania_lottery
offline_file: ""
offline_thumbnail: ""
uuid: b4f86786-691e-471c-a560-e9400c7cb465
updated: 1484308616
title: Transylvania lottery
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fano_plane.svg.png
categories:
    - Combinatorics
---
In mathematical combinatorics, the Transylvanian lottery is a lottery where three numbers between 1 and 14 are picked by the player, and three numbers are chosen randomly. The player wins if two of his numbers are among the random ones. The problem of how many tickets the player must buy in order to be certain of winning can be solved by the use of the Fano plane. (Mazur 2010, p.280 problem 15) (Javier Martínez, Gloria Gutiérrez & Pablo Cordero et al. 2008, p.85)
