---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_conjugate
offline_file: ""
offline_thumbnail: ""
uuid: 76fb0d0e-d456-4788-8bca-5069f9fc933e
updated: 1484309231
title: Convex conjugate
tags:
    - Definition
    - Examples
    - Connection with expected shortfall (average value at risk)
    - Ordering
    - Properties
    - Order reversing
    - Biconjugate
    - "Fenchel's inequality"
    - Convexity
    - Infimal convolution
    - Maximizing argument
    - Scaling properties
    - Behavior under linear transformations
    - Table of selected convex conjugates
categories:
    - Duality theories
---
In mathematics and mathematical optimization, the convex conjugate of a function is a generalization of the Legendre transformation. It is also known as Legendre–Fenchel transformation or Fenchel transformation (after Adrien-Marie Legendre and Werner Fenchel). It is used to transform an optimization problem into its corresponding dual problem, which can often be simpler to solve.
