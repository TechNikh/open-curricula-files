---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/basketball
offline_file: ""
offline_thumbnail: ""
uuid: 1dfe2228-0d24-4cbb-9727-5996c55f5542
updated: 1484310140
title: basketball
categories:
    - Dictionary
---
basketball
     n 1: a game played on a court by two opposing teams of 5 players;
          points are scored by throwing the basketball through an
          elevated horizontal hoop [syn: {basketball game}, {hoops}]
     2: an inflated ball used in playing basketball
