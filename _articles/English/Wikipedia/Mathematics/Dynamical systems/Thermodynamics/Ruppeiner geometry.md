---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ruppeiner_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 009ff259-e9b5-4b0a-8234-74b89089f949
updated: 1484308864
title: Ruppeiner geometry
categories:
    - Thermodynamics
---
Ruppeiner geometry is thermodynamic geometry (a type of information geometry) using the language of Riemannian geometry to study thermodynamics. George Ruppeiner proposed it in 1979. He claimed that thermodynamic systems can be represented by Riemannian geometry, and that statistical properties can be derived from the model.
