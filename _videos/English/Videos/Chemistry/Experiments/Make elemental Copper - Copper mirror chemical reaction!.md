---
version: 1
type: video
provider: YouTube
id: tkFOzFNODUE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Make%20elemental%20Copper%20-%20Copper%20mirror%20chemical%20reaction%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fe06a19f-0ac4-421f-8664-9ba52240f62e
updated: 1486069662
title: 'Make elemental Copper - Copper mirror chemical reaction!'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/tkFOzFNODUE/default.jpg
    - https://i3.ytimg.com/vi/tkFOzFNODUE/1.jpg
    - https://i3.ytimg.com/vi/tkFOzFNODUE/2.jpg
    - https://i3.ytimg.com/vi/tkFOzFNODUE/3.jpg
---
