---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/addiction
offline_file: ""
offline_thumbnail: ""
uuid: 67a26fc7-5f47-4bb8-bd06-c057f1094b92
updated: 1484310166
title: addiction
categories:
    - Dictionary
---
addiction
     n 1: being abnormally tolerant to and dependent on something that
          is psychologically or physically habit-forming
          (especially alcohol or narcotic drugs) [syn: {dependence},
           {dependency}, {habituation}]
     2: an abnormally strong craving
     3: (Roman law) a formal award by a court sentence of a thing or
        person to another (as of a debtor to his creditor); a
        surrender to a master; "under Roman law addiction was the
        justification for slavery"
