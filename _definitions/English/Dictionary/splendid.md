---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/splendid
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484480881
title: splendid
categories:
    - Dictionary
---
splendid
     adj 1: having great beauty and splendor; "a glorious spring
            morning"; "a glorious sunset"; "splendid costumes"; "a
            kind of splendiferous native simplicity" [syn: {glorious},
             {resplendent}, {splendiferous}]
     2: characterized by or attended with brilliance or grandeur;
        "the brilliant court life at Versailles"; "a glorious work
        of art"; "magnificent cathedrals"; "the splendid
        coronation ceremony" [syn: {brilliant}, {glorious}, {magnificent}]
