---
version: 1
type: video
provider: YouTube
id: 0yLtGSsK3oE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b8b0f1ea-d9e0-4b54-823d-dc590d5fc300
updated: 1486069630
title: Learn the Use of Half Letters
categories:
    - Class 2
thumbnail_urls:
    - https://i3.ytimg.com/vi/0yLtGSsK3oE/default.jpg
    - https://i3.ytimg.com/vi/0yLtGSsK3oE/1.jpg
    - https://i3.ytimg.com/vi/0yLtGSsK3oE/2.jpg
    - https://i3.ytimg.com/vi/0yLtGSsK3oE/3.jpg
---
