---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Economizer
offline_file: ""
offline_thumbnail: ""
uuid: e5dad1a4-4142-4fa2-ab4b-0881554f29cd
updated: 1484308838
title: Economizer
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-A_1940%2527s_%2527Green%2527s_Economizer%2527_in_a_boiler_house_in_Launceston%252C_Tasmania.JPG'
tags:
    - Stirling engine
    - Boilers
    - History
    - Power plants
    - HVAC
    - Refrigeration
    - Cooler Economizer
    - Vapor-Compression Refrigeration
    - Economizer setups in refrigeration
    - Two staged systems and boosters
    - Economizer gas compressors
    - Subcooling and refrigeration cycle optimizers
    - Internal heat exchangers
categories:
    - Thermodynamics
---
Economizers (US and Oxford spelling), or economisers (UK), are mechanical devices intended to reduce energy consumption, or to perform useful function such as preheating a fluid. The term economizer is used for other purposes as well. Boiler, power plant, heating, Refrigeration, ventilating, and air conditioning (HVAC) uses are discussed in this article. In simple terms, an economizer is a heat exchanger.
