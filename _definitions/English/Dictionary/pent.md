---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pent
offline_file: ""
offline_thumbnail: ""
uuid: 3876d38f-924d-426e-a19f-6f6f152e645a
updated: 1484310421
title: pent
categories:
    - Dictionary
---
pen
     n 1: a writing implement with a point from which ink flows
     2: an enclosure for confining livestock
     3: a portable enclosure in which babies may be left to play
        [syn: {playpen}]
     4: a correctional institution for those convicted of major
        crimes [syn: {penitentiary}]
     5: female swan
     v : produce a literary work; "She composed a poem"; "He wrote
         four novels" [syn: {write}, {compose}, {indite}]
     [also: {pent}, {penning}, {penned}]
