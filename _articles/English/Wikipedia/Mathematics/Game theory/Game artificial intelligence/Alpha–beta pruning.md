---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning'
offline_file: ""
offline_thumbnail: ""
uuid: 127644d9-3578-4591-ab3b-a66d6f381270
updated: 1484308999
title: Alpha–beta pruning
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-AB_pruning.svg.png
tags:
    - History
    - Improvements over naive minimax
    - Pseudocode
    - Heuristic improvements
    - Other algorithms
    - Bibliography
categories:
    - Game artificial intelligence
---
Alpha–beta pruning is a search algorithm that seeks to decrease the number of nodes that are evaluated by the minimax algorithm in its search tree. It is an adversarial search algorithm used commonly for machine playing of two-player games (Tic-tac-toe, Chess, Go, etc.). It stops completely evaluating a move when at least one possibility has been found that proves the move to be worse than a previously examined move. Such moves need not be evaluated further. When applied to a standard minimax tree, it returns the same move as minimax would, but prunes away branches that cannot possibly influence the final decision.[1]
