---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kepler_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 7c5d358d-31a0-414f-a00a-3f5f933f3451
updated: 1484308605
title: Kepler conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Closepacking.svg.png
tags:
    - Background
    - Origins
    - Nineteenth century
    - Twentieth century
    - "Hales' proof"
    - A formal proof
    - Related problems
categories:
    - Discrete geometry
---
The Kepler conjecture, named after the 17th-century mathematician and astronomer Johannes Kepler, is a mathematical conjecture about sphere packing in three-dimensional Euclidean space. It says that no arrangement of equally sized spheres filling space has a greater average density than that of the cubic close packing (face-centered cubic) and hexagonal close packing arrangements. The density of these arrangements is around 74.05%.
