---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rehabilitation
offline_file: ""
offline_thumbnail: ""
uuid: ff888483-7997-464b-93fb-802599f36634
updated: 1484310170
title: rehabilitation
categories:
    - Dictionary
---
rehabilitation
     n 1: the restoration of someone to a useful place in society
     2: the conversion of wasteland into land suitable for use of
        habitation or cultivation [syn: {reclamation}, {renewal}]
     3: vindication of a person's character and the re-establishment
        of that person's reputation
     4: the treatment of physical disabilities by massage and
        electrotherapy and exercises
