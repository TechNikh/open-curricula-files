---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hygienic
offline_file: ""
offline_thumbnail: ""
uuid: 6cc10c8a-0435-465f-ab32-742646e48cdc
updated: 1484310535
title: hygienic
categories:
    - Dictionary
---
hygienic
     adj : tending to promote or preserve health; "hygienic habits like
           using disposable tissues"; "hygienic surroundings with
           plenty of fresh air" [syn: {hygienical}]
