---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tadpole
offline_file: ""
offline_thumbnail: ""
uuid: 0695b2fb-3d34-4c72-916e-8b9d469a7899
updated: 1484310283
title: tadpole
categories:
    - Dictionary
---
tadpole
     n : a larval frog or toad [syn: {polliwog}, {pollywog}]
