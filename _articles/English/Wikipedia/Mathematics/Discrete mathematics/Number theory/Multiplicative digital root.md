---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiplicative_digital_root
offline_file: ""
offline_thumbnail: ""
uuid: 693768b3-681c-4855-8e45-01ffebc4129e
updated: 1484308652
title: Multiplicative digital root
categories:
    - Number theory
---
The multiplicative digital root of a positive integer n is found by multiplying the digits of n together, then repeating this operation until only a single digit remains. This single-digit number is called the multiplicative digital root of n.[1]
