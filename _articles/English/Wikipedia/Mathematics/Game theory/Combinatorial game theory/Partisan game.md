---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partisan_game
offline_file: ""
offline_thumbnail: ""
uuid: 7c89e213-e384-4d2e-84af-3d8ba1f84cb0
updated: 1484308535
title: Partisan game
categories:
    - Combinatorial game theory
---
Most games are partisan. For example, in chess, only one player can move the white pieces. More strongly, when analyzed using combinatorial game theory, many chess positions have values that cannot be expressed as the value of an impartial game, for instance when one side has a number of extra tempos that can be used to put the other side into zugzwang.[2]
