---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tollmien%E2%80%93Schlichting_wave'
offline_file: ""
offline_thumbnail: ""
uuid: e5650dc1-8bde-4be8-9e5e-41e9834a4eac
updated: 1484308900
title: Tollmien–Schlichting wave
tags:
    - Physical mechanism
    - Transition Phenomena
    - Initial Disturbance
    - Final Transition
categories:
    - Waves
---
In fluid dynamics, a Tollmien–Schlichting wave (often abbreviated T-S wave) is a streamwise instability which arises in a viscous boundary layer. It is one of the more common methods by which a laminar boundary layer transitions to turbulence. The waves are initiated when some disturbance (sound, for example) interacts with leading edge roughness in a process known as receptivity. These waves are slowly amplified as they move downstream until they may eventually grow large enough that nonlinearities take over and the flow transitions to turbulence.
