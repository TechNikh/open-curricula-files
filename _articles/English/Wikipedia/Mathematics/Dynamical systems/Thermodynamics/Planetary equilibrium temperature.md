---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Planetary_equilibrium_temperature
offline_file: ""
offline_thumbnail: ""
uuid: 62229df5-d1ee-4303-891d-06b3d119ff70
updated: 1484308853
title: Planetary equilibrium temperature
tags:
    - Calculation of semi-blackbody temperature
    - Theoretical model
    - Detailed derivation of the planetary equilibrium temperature
    - Calculation for extrasolar planets
categories:
    - Thermodynamics
---
The planetary equilibrium temperature is a theoretical temperature that a planet would be at when considered simply as if it were a black body being heated only by its parent star. In this model, the presence or absence of an atmosphere (and therefore any greenhouse effect) is not considered, and one treats the theoretical black body temperature as if it came from an idealized surface of the planet.
