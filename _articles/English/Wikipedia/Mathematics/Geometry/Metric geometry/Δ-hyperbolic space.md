---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%CE%94-hyperbolic_space'
offline_file: ""
offline_thumbnail: ""
uuid: 38cc0c2b-6b40-4bd6-b88e-049ca41d3a6b
updated: 1484309265
title: Δ-hyperbolic space
tags:
    - Definitions
    - Definitions using triangles
    - Definition using the Gromov product
    - Examples
    - Hyperbolicity and curvature
    - Important properties
    - Invariance under quasi-isometry
    - Approximate trees in hyperbolic spaces
    - >
        Exponential growth of distance and isoperimetric
        inequalities
    - Quasiconvex subspaces
    - Asymptotic cones
    - The boundary of an hyperbolic space
    - Definition using the Gromov product
    - Definition for proper spaces using rays
    - Examples
    - Busemann functions
    - >
        The action of isometries on the boundary and their
        classification
    - More examples
    - Notes
categories:
    - Metric geometry
---
In mathematics a δ-hyperbolic space is a geodesic metric space satisfying certain metric relations (depending quantitatively on the nonnegative real number δ) between points. The definitions are inspired by the metric properties of classical hyperbolic geometry and of trees. Hyperbolicity is a large-scale property, and is very useful to the study of certain infinite groups called (Gromov-)hyperbolic groups.
