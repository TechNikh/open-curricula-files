---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/molecular
offline_file: ""
offline_thumbnail: ""
uuid: 4ce55403-5dc9-4404-84e0-c9ee2fd15dd8
updated: 1484310371
title: molecular
categories:
    - Dictionary
---
molecular
     adj 1: relating to or produced by or consisting of molecules;
            "molecular structure"; "molecular oxygen"; "molecular
            weight is the sum of all the atoms in a molecul"
     2: relating to simple or elementary organization; "proceed by
        more and more detailed analysis to the molecular facts of
        perception"--G.A. Miller [syn: {molecular(a)}] [ant: {molar(a)}]
