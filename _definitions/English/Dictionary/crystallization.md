---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crystallization
offline_file: ""
offline_thumbnail: ""
uuid: cd499434-b761-4d71-91ba-5f4024acbdc5
updated: 1484310387
title: crystallization
categories:
    - Dictionary
---
crystallization
     n 1: the formation of crystals [syn: {crystallisation}, {crystallizing}]
     2: a rock formed by the solidification of a substance; has
        regularly repeating internal structure; external plane
        faces [syn: {crystal}]
     3: a mental synthesis that becomes fixed or concrete by a
        process resembling crystal formation
