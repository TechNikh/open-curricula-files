---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evangelical
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484407441
title: evangelical
categories:
    - Dictionary
---
evangelical
     adj 1: relating to or being a Christian church believing in
            personal conversion and the inerrancy of the Bible
            especially the 4 Gospels; "evangelical Christianity";
            "an ultraconservative evangelical message"
     2: of or pertaining to or in keeping with the Christian gospel
        especially as in the first 4 books of the New Testament
     3: marked by ardent or zealous enthusiasm for a cause [syn: {evangelistic}]
