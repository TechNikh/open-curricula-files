---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lactating
offline_file: ""
offline_thumbnail: ""
uuid: 6a7b6cb7-d4ec-44f2-abc9-94b1ca147d5f
updated: 1484310533
title: lactating
categories:
    - Dictionary
---
lactating
     adj : producing or secreting milk; "a wet nurse"; "a wet cow";
           "lactating cows" [syn: {wet}] [ant: {dry}]
