---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/similar
offline_file: ""
offline_thumbnail: ""
uuid: 1ed37b74-1f65-43ac-b1ec-813722ea28c2
updated: 1484310341
title: similar
categories:
    - Dictionary
---
similar
     adj 1: marked by correspondence or resemblance; "similar food at
            similar prices"; "problems similar to mine"; "they
            wore similar coats" [ant: {dissimilar}]
     2: having the same or similar characteristics; "all politicians
        are alike"; "they looked utterly alike"; "friends are
        generaly alike in background and taste" [syn: {alike(p)},
        {like}] [ant: {unalike}]
     3: resembling or similar; having the same or some of the same
        characteristics; often used in combination; "suits of like
        design"; "a limited circle of like minds"; "members of the
        cat family have like dispositions"; "as like as two peas
        in ...
