---
version: 1
type: video
provider: YouTube
id: tMKlPDBRJ1E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/A%20Day%20in%20the%20Life%20of%20a%20Motor%20Protein.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 71d243a8-2578-4b37-a48d-da9522daec42
updated: 1486069670
title: A Day in the Life of a Motor Protein
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/tMKlPDBRJ1E/default.jpg
    - https://i3.ytimg.com/vi/tMKlPDBRJ1E/1.jpg
    - https://i3.ytimg.com/vi/tMKlPDBRJ1E/2.jpg
    - https://i3.ytimg.com/vi/tMKlPDBRJ1E/3.jpg
---
