---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inwards
offline_file: ""
offline_thumbnail: ""
uuid: 593f4be2-819b-4300-8fad-78c731f3c44f
updated: 1484310221
title: inwards
categories:
    - Dictionary
---
inwards
     adv 1: to or toward the inside of; "come in"; "smash in the door"
            [syn: {in}, {inward}]
     2: toward the center or interior; "move the needle further
        inwards!" [syn: {inward}] [ant: {outward}]
