---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/banging
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484551861
title: banging
categories:
    - Dictionary
---
banging
     adj : (used informally) very large; "a thumping loss" [syn: {humongous},
            {thumping}, {whopping}, {walloping}]
     n 1: a continuing very loud noise
     2: the act of subjecting to strong attack [syn: {battering}]
