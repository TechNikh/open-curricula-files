---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wheat_flour
offline_file: ""
offline_thumbnail: ""
uuid: 3e4b862e-c594-4adb-afbf-a3522d0ef264
updated: 1484308363
title: Wheat flour
tags:
    - Types
    - Canada
    - Fiji
    - Indian wheat flours
    - Southeast Asia
    - USA
    - Other
    - Flour strength – W index
    - National Flour in the United Kingdom
    - Flour production
    - Madagascar
    - Kenya
categories:
    - Uncategorized
---
Wheat flour is a powder made from the grinding of wheat used for human consumption. More wheat flour is produced than any other flour.[not verified in body] Wheat varieties are called "soft" or "weak" if gluten content is low, and are called "hard" or "strong" if they have high gluten content. Hard flour, or bread flour, is high in gluten, with 12% to 14% gluten content, its dough has elastic toughness that holds its shape well once baked. Soft flour is comparatively low in gluten and thus results in a loaf with a finer, crumbly texture.[1] Soft flour is usually divided into cake flour, which is the lowest in gluten, and pastry flour, which has slightly more gluten than cake flour.
