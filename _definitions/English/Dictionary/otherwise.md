---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/otherwise
offline_file: ""
offline_thumbnail: ""
uuid: ccabad6c-8359-4e9d-a5f9-5697ce064b10
updated: 1484310366
title: otherwise
categories:
    - Dictionary
---
otherwise
     adj : other than as supposed or expected; "the outcome was
           otherwise"
     adv 1: in other respects or ways; "he is otherwise normal"; "the
            funds are not otherwise available"; "an otherwise
            hopeless situation"
     2: in another and different manner; "very soon you will know
        differently"; "she thought otherwise" [syn: {differently}]
