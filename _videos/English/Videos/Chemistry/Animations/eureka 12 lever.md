---
version: 1
type: video
provider: YouTube
id: OuPyvTQBwfQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/eureka%2012%20lever.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b9a48829-cd13-4d6a-ab71-f786aa5f9deb
updated: 1486069665
title: eureka 12 lever
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/OuPyvTQBwfQ/default.jpg
    - https://i3.ytimg.com/vi/OuPyvTQBwfQ/1.jpg
    - https://i3.ytimg.com/vi/OuPyvTQBwfQ/2.jpg
    - https://i3.ytimg.com/vi/OuPyvTQBwfQ/3.jpg
---
