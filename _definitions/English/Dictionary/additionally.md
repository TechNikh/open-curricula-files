---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/additionally
offline_file: ""
offline_thumbnail: ""
uuid: da8a49f1-8e46-4a98-8893-14143c42fcda
updated: 1484310467
title: additionally
categories:
    - Dictionary
---
additionally
     adv : by way of addition; furthermore; "he serves additionally as
           the CEO" [syn: {in addition}, {to boot}]
