---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slider
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484339401
title: slider
categories:
    - Dictionary
---
slider
     n 1: a person who slips or slides because of loss of traction
          [syn: {skidder}, {slipper}]
     2: someone who races the luge [syn: {luger}]
     3: freshwater turtle of United States and South America;
        frequently raised commercially; some young sold as pets
        [syn: {yellow-bellied terrapin}, {Pseudemys scripta}]
     4: a fastball that curves slightly away from the side from
        which it was thrown
