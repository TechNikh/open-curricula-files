---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cholecystokinin
offline_file: ""
offline_thumbnail: ""
uuid: ef240e0e-16fc-4da2-bf51-3b652cc5c7a2
updated: 1484308380
title: Cholecystokinin
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-PBB_GE_CCK_205827_at_tn.png
tags:
    - Structure
    - Function
    - Gastrointestinal
    - Digestion
    - Satiety
    - Neurological
    - Anxiogenic
    - Panicogenic
    - Hallucinogenic
    - Interactions
categories:
    - Uncategorized
---
Cholecystokinin (CCK or CCK-PZ; from Greek chole, "bile"; cysto, "sac"; kinin, "move"; hence, move the bile-sac (gallbladder)) is a peptide hormone of the gastrointestinal system responsible for stimulating the digestion of fat and protein. Cholecystokinin, previously called pancreozymin, is synthesized and secreted by enteroendocrine cells in the duodenum, the first segment of the small intestine. Its presence causes the release of digestive enzymes and bile from the pancreas and gallbladder, respectively, and also acts as a hunger suppressant.[3][4]
