---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/visiting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484395741
title: visiting
categories:
    - Dictionary
---
visiting
     adj : staying temporarily; "a visiting foreigner"; "guest
           conductor" [syn: {guest(a)}]
     n : the activity of making visits; "visiting with the neighbors
         filled her afternoons"
