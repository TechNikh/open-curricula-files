---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reversible_computing
offline_file: ""
offline_thumbnail: ""
uuid: 9a71cb07-8d34-4b8b-ad32-6e55eb06c915
updated: 1484308864
title: Reversible computing
tags:
    - Reversibility
    - Relation to thermodynamics
    - Physical reversibility
    - Logical reversibility
categories:
    - Thermodynamics
---
Reversible computing is a model of computing where the computational process to some extent is reversible, i.e., time-invertible. In a computational model that uses transitions from one state of the abstract machine to another, a necessary condition for reversibility is that the relation of the mapping from states to their successors must be one-to-one. Reversible computing is generally considered an unconventional form of computing.
