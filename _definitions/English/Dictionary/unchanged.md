---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unchanged
offline_file: ""
offline_thumbnail: ""
uuid: 83bb3254-2ba2-42e1-b63b-d46367f90588
updated: 1484310366
title: unchanged
categories:
    - Dictionary
---
unchanged
     adj 1: not made or become different; "the causes that produced them
            have remained unchanged" [ant: {changed}]
     2: remaining in an original state; "persisting unaltered
        through time" [syn: {unaltered}] [ant: {altered}]
