---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Goa
offline_file: ""
offline_thumbnail: ""
uuid: 90cfa22c-655a-4bcc-a4f6-26b78e16f3d1
updated: 1484308425
title: Goa
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Usgalimal.PNG
tags:
    - Etymology
    - History
    - Geography and Climate
    - geography
    - Climate
    - Subdivisions
    - Government and politics
    - Flora and Fauna
    - Economy
    - population
    - Demographics
    - Languages
    - religion
    - Tourism
    - Historic sites and neighbourhoods
    - Museums and science centre
    - culture
    - Dance and music
    - Theatre
    - Konkani cinema
    - Food
    - Architecture
    - Media and communication
    - Sports
    - Education
    - Transportation
    - air
    - Road
    - Rail
    - Sea
    - Citations
categories:
    - Uncategorized
---
Goa i/ˈɡoʊ.ə/ is a state in India within the coastal region known as the Konkan in the south western part of India. Bounded by Maharashtra to the north and Karnataka to the east and south, the Arabian Sea forms its western coast. It is India's smallest state by area and the fourth smallest by population. Goa is India's richest state with a GDP per capita two and a half times that of the country.[3] It was ranked the best placed state by the Eleventh Finance Commission for its infrastructure and ranked on top for the best quality of life in India by the National Commission on Population based on the 12 Indicators.[3]
