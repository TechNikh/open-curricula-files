---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/limestone
offline_file: ""
offline_thumbnail: ""
uuid: 1e1bab5b-e390-46da-a0d8-d2ba13258692
updated: 1484310413
title: limestone
categories:
    - Dictionary
---
limestone
     n : a sedimentary rock consisting mainly of calcium that was
         deposited by the remains of marine animals
