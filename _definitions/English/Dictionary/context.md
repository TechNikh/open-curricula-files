---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/context
offline_file: ""
offline_thumbnail: ""
uuid: 5e2b03ab-ef65-4f18-92c7-03f0e440d2db
updated: 1484310212
title: context
categories:
    - Dictionary
---
context
     n 1: discourse that surrounds a language unit and helps to
          determine its interpretation [syn: {linguistic context},
           {context of use}]
     2: the set of facts or circumstances that surround a situation
        or event; "the historical context" [syn: {circumstance}]
