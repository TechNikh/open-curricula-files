---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Impartial_game
offline_file: ""
offline_thumbnail: ""
uuid: 9b4b646e-233e-4967-965f-ac20aba7bc9c
updated: 1484308538
title: Impartial game
categories:
    - Combinatorial game theory
---
In combinatorial game theory, an impartial game is a game in which the allowable moves depend only on the position and not on which of the two players is currently moving, and where the payoffs are symmetric. In other words, the only difference between player 1 and player 2 is that player 1 goes first.
