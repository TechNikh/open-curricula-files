---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lying
offline_file: ""
offline_thumbnail: ""
uuid: 8e9d29b7-6a85-4da3-bed5-9a4010d09455
updated: 1484310206
title: lying
categories:
    - Dictionary
---
lie
     n 1: a statement that deviates from or perverts the truth [syn: {prevarication}]
     2: Norwegian diplomat who was the first Secretary General of
        the United Nations (1896-1968) [syn: {Trygve Lie}, {Trygve
        Halvden Lie}]
     3: position or manner in which something is situated
     v 1: be located or situated somewhere; occupy a certain position
     2: be lying, be prostrate; be in a horizontal position; "The
        sick man lay in bed all day"; "the books are lying on the
        shelf" [ant: {stand}, {sit}]
     3: originate (in); "The problems dwell in the social injustices
        in this country" [syn: {dwell}, {consist}, {belong}, {lie
        in}]
     4: ...
