---
version: 1
type: video
provider: YouTube
id: bnzmVAa46xg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a498b3f6-e135-44a9-ad31-cec5ed62d6d5
updated: 1486069657
title: Voltage and Current, Part 1
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/bnzmVAa46xg/default.jpg
    - https://i3.ytimg.com/vi/bnzmVAa46xg/1.jpg
    - https://i3.ytimg.com/vi/bnzmVAa46xg/2.jpg
    - https://i3.ytimg.com/vi/bnzmVAa46xg/3.jpg
---
