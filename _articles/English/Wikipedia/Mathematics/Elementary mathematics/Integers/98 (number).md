---
version: 1
type: article
id: https://en.wikipedia.org/wiki/98_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 324efa4b-eeb3-4943-8729-ca41c8b030d4
updated: 1484308977
title: 98 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/80px-Tabliczka_E98.svg.png
tags:
    - In mathematics
    - In astronomy
    - In computing
    - In space travel
    - In other fields
    - In sports
categories:
    - Integers
---
