---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/activity
offline_file: ""
offline_thumbnail: ""
uuid: ee9e6f65-6e9f-4b0e-a429-e0e7a03c9a5a
updated: 1484310361
title: activity
categories:
    - Dictionary
---
activity
     n 1: any specific activity; "they avoided all recreational
          activity" [ant: {inactivity}]
     2: the state of being active; "his sphere of activity"; "he is
        out of action" [syn: {action}, {activeness}] [ant: {inaction},
         {inaction}, {inaction}]
     3: an organic process that takes place in the body;
        "respiratory activity" [syn: {bodily process}, {body
        process}, {bodily function}]
     4: (chemistry) the capacity of a substance to take part in a
        chemical reaction; "catalytic activity"
     5: a process existing in or produced by nature (rather than by
        the intent of human beings); "the action of natural
        forces"; ...
