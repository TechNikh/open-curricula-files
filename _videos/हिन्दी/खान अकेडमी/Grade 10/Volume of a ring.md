---
version: 1
type: video
provider: YouTube
id: _QTwcpQRlfk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 918d7ccf-8e38-4d9b-8428-fa7167a73aae
updated: 1486069619
title: Volume of a ring
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/_QTwcpQRlfk/default.jpg
    - https://i3.ytimg.com/vi/_QTwcpQRlfk/1.jpg
    - https://i3.ytimg.com/vi/_QTwcpQRlfk/2.jpg
    - https://i3.ytimg.com/vi/_QTwcpQRlfk/3.jpg
---
