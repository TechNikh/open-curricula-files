---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometrical_progression
offline_file: ""
offline_thumbnail: ""
uuid: 289ff572-1651-4a43-86a1-c8daf85dd6b9
updated: 1484308378
title: Geometric progression
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Geometric_sequences.svg.png
tags:
    - Elementary properties
    - Geometric series
    - Derivation
    - Related formulas
    - Infinite geometric series
    - Complex numbers
    - Product
    - "Relationship to geometry and Euclid's work"
categories:
    - Uncategorized
---
In mathematics, a geometric progression, also known as a geometric sequence, is a sequence of numbers where each term after the first is found by multiplying the previous one by a fixed, non-zero number called the common ratio. For example, the sequence 2, 6, 18, 54, ... is a geometric progression with common ratio 3. Similarly 10, 5, 2.5, 1.25, ... is a geometric sequence with common ratio 1/2.
