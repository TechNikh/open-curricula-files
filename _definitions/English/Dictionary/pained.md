---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pained
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484581801
title: pained
categories:
    - Dictionary
---
pained
     adj : hurt or upset; "she looked offended"; "face had a pained and
           puzzled expression" [syn: {offended}]
