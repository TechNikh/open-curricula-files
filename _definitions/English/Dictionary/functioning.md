---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/functioning
offline_file: ""
offline_thumbnail: ""
uuid: 267a63f5-8dd5-4325-a9a4-231d104c0408
updated: 1484310200
title: functioning
categories:
    - Dictionary
---
functioning
     adj : performing or able to perform its regular function; "a
           functioning flashlight" [ant: {malfunctioning}]
     n : process or manner of functioning or operating; "the power of
         its engine determine its operation"; "the plane's
         operation in high winds"; "they compared the cooking
         performance of each oven"; "the jet's performance
         conformed to high standards" [syn: {operation}, {performance}]
