---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Knuckle_Heads
offline_file: ""
offline_thumbnail: ""
uuid: c26383b7-5eb6-4cd2-b6d2-d8a92e4de2e1
updated: 1484308527
title: Knuckle Heads
tags:
    - Gameplay
    - Characters
    - Virtual Console release
categories:
    - Cooperative games
---
Knuckle Heads (ナックルヘッズ, Nakkuru Hezzu?) is a two-on-two fighting arcade game which was released by Namco, in 1992;[3] it runs on Namco NA-2 hardware, and represents the company's answer to Capcom's 1987 hit Street Fighter. The US version was also the fifth title from the company to show the Federal Bureau of Investigation's "Winners Don't Use Drugs" screen during its attract sequence - and it is also the only game from them where points are merely awarded as a "remaining time" bonus.
