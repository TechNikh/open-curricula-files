---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maximal_ergodic_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 8ff22560-4445-401d-ae23-9a86c76eb792
updated: 1484308707
title: Maximal ergodic theorem
categories:
    - Theorems in dynamical systems
---
Suppose that 
  
    
      
        (
        X
        ,
        
          
            B
          
        
        ,
        μ
        )
      
    
    {\displaystyle (X,{\mathcal {B}},\mu )}
  
 is a probability space, that 
  
    
      
        T
        :
        X
        →
        X
      
    
    {\displaystyle T:X\to X}
  
 is a (possibly noninvertible) measure-preserving transformation, and that 
  
    
      
        f
        ∈
        
          L
          
            1
          
        
        (
        μ
        )
      
    
    {\displaystyle f\in L^{1}(\mu )}
  
. Define 
  
    
      
        
          f
          
            ∗
          
        ...
