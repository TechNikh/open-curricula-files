---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Banach_bundle
offline_file: ""
offline_thumbnail: ""
uuid: 676338f5-f71e-4dd9-b027-2113a175b4e1
updated: 1484309204
title: Banach bundle
tags:
    - Definition of a Banach bundle
    - Examples of Banach bundles
    - Morphisms of Banach bundles
    - Pull-back of a Banach bundle
categories:
    - Differential geometry
---
In mathematics, a Banach bundle is a vector bundle each of whose fibres is a Banach space, i.e. a complete normed vector space, possibly of infinite dimension.
