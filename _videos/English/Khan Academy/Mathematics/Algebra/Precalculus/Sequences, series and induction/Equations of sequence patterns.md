---
version: 1
type: video
provider: YouTube
id: _3BnyEr5fG4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Sequences%2C%20series%20and%20induction/Equations%20of%20sequence%20patterns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d1897a9d-4abf-412e-88c1-a479d8f71bff
updated: 1486069565
title: Equations of sequence patterns
categories:
    - Sequences, series and induction
thumbnail_urls:
    - https://i3.ytimg.com/vi/_3BnyEr5fG4/default.jpg
    - https://i3.ytimg.com/vi/_3BnyEr5fG4/1.jpg
    - https://i3.ytimg.com/vi/_3BnyEr5fG4/2.jpg
    - https://i3.ytimg.com/vi/_3BnyEr5fG4/3.jpg
---
