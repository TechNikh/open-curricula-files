---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partial_fraction_decomposition
offline_file: ""
offline_thumbnail: ""
uuid: cba2aaad-da02-42b2-a24d-46ae1b8ed6fd
updated: 1484308912
title: Partial fraction decomposition
tags:
    - Basic principles
    - Application to symbolic integration
    - Procedure
    - Illustration
    - Residue method
    - Over the reals
    - General result
    - Examples
    - Example 1
    - Example 2
    - Example 3
    - Example 4 (residue method)
    - Example 5 (limit method)
    - The role of the Taylor polynomial
    - Sketch of the proof
    - Fractions of integers
    - Notes
categories:
    - Elementary algebra
---
In algebra, the partial fraction decomposition or partial fraction expansion of a rational function (that is a fraction such that the numerator and the denominator are both polynomials) is the operation that consists in expressing the fraction as a sum of a polynomial (possibly zero) and one or several fractions with a simpler denominator.
