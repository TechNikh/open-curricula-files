---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Automobiles
offline_file: ""
offline_thumbnail: ""
uuid: c0dceb58-ac00-456a-ae45-876ebb920b89
updated: 1484308331
title: Car
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-401_Gridlock.jpg
tags:
    - Etymology
    - History
    - Mass production
    - Fuel and propulsion technologies
    - User interface
    - Lighting
    - weight
    - Seating and body style
    - Safety
    - Costs and benefits
    - Environmental impact
    - Emerging car technologies
    - Autonomous car
    - Open source development
    - Industry
    - Alternatives
    - Other meanings
categories:
    - Uncategorized
---
A car is a wheeled, self-powered motor vehicle used for transportation and a product of the automotive industry. Most definitions of the term specify that cars are designed to run primarily on roads, to have seating for one to eight people, to typically have four wheels with tyres, and to be constructed principally for the transport of people rather than goods.[2][3] The year 1886 is regarded as the birth year of the modern car. In that year, German inventor Karl Benz built the Benz Patent-Motorwagen. Cars did not become widely available until the early 20th century. One of the first cars that was accessible to the masses was the 1908 Model T, an American car manufactured by the Ford Motor ...
