---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Inversion_(discrete_mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: c6a3c9f9-0c95-47e7-bcb0-12b1da4a1b85
updated: 1484308570
title: Inversion (discrete mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Inversion_qtl1.svg.png
tags:
    - Definitions
    - Weak order of permutations
    - Source bibliography
    - Presortedness measures
categories:
    - Combinatorics
---
In computer science and discrete mathematics, an inversion is a pair of places of a sequence where the elements on these places are out of their natural order.
