---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/replacement
offline_file: ""
offline_thumbnail: ""
uuid: 3e8f4f85-a355-43de-80a0-4d4043a893ef
updated: 1484310535
title: replacement
categories:
    - Dictionary
---
replacement
     n 1: an event in which one thing is substituted for another; "the
          replacement of lost blood by a transfusion of donor
          blood" [syn: {substitution}, {permutation}, {transposition},
           {switch}]
     2: the act of furnishing an equivalent person or thing in the
        place of another; "replacing the star will not be easy"
        [syn: {replacing}]
     3: someone who takes the place of another person [syn: {surrogate},
         {alternate}]
     4: a person or thing that takes or can take the place of
        another [syn: {substitute}]
     5: filling again by supplying what has been used up [syn: {refilling},
         {replenishment}, ...
