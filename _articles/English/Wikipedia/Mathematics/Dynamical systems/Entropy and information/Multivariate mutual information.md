---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Multivariate_mutual_information
offline_file: ""
offline_thumbnail: ""
uuid: deef561a-17ee-44fa-90b9-47ff5624ef34
updated: 1484308770
title: Multivariate mutual information
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/256px-VennInfo3Var.svg_0.png
tags:
    - Definition
    - Properties
    - Synergy and redundancy
    - >
        Example of Positive Multivariate mutual information
        (redundancy)
    - >
        Examples of Negative Multivariate mutual information
        (synergy)
    - Bounds
    - Difficulties
categories:
    - Entropy and information
---
In information theory there have been various attempts over the years to extend the definition of mutual information to more than two random variables. These attempts have met with a great deal of confusion and a realization that interactions among many random variables are poorly understood.
