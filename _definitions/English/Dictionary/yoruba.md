---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yoruba
offline_file: ""
offline_thumbnail: ""
uuid: cd5636c3-c7ec-4440-aae1-20de037d11f7
updated: 1484310579
title: yoruba
categories:
    - Dictionary
---
Yoruba
     n 1: a member of a West African people living chiefly in
          southwestern Nigeria
     2: a Kwa language spoken by the Yoruba people in southwestern
        Nigeria [syn: {Aku}]
