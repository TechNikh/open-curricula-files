---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/milky
offline_file: ""
offline_thumbnail: ""
uuid: 28d6fc8a-8e1f-4d93-9d94-ebb4ffc52afa
updated: 1484310381
title: milky
categories:
    - Dictionary
---
milky
     adj : resembling milk in color or cloudiness; not clear; "milky
           glass" [syn: {milklike}, {whitish}]
     [also: {milkiest}, {milkier}]
