---
version: 1
type: video
provider: YouTube
id: yNpRtZej_m0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 30e08bcb-429b-4f4e-98eb-0c48f5679fc8
updated: 1486069625
title: Equivalent Multiplication (Math Hack)
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/yNpRtZej_m0/default.jpg
    - https://i3.ytimg.com/vi/yNpRtZej_m0/1.jpg
    - https://i3.ytimg.com/vi/yNpRtZej_m0/2.jpg
    - https://i3.ytimg.com/vi/yNpRtZej_m0/3.jpg
---
