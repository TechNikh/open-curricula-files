---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Inverse_trigonometric_functions
offline_file: ""
offline_thumbnail: ""
uuid: 1ea22139-2d04-4b28-875e-9383aaca8242
updated: 1484309343
title: Inverse trigonometric functions
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/168px-Arcsine_Arccosine.svg.png
tags:
    - Notation
    - Basic properties
    - Principal values
    - >
        Relationships between trigonometric functions and inverse
        trigonometric functions
    - Relationships among the inverse trigonometric functions
    - Arctangent addition formula
    - In calculus
    - Derivatives of inverse trigonometric functions
    - Expression as definite integrals
    - Infinite series
    - 'Variant: Continued fractions for arctangent'
    - Indefinite integrals of inverse trigonometric functions
    - Example
    - Extension to complex plane
    - Logarithmic forms
    - Example proof
    - Applications
    - General solutions
    - 'Application: finding the angle of a right triangle'
    - In computer science and engineering
    - Two-argument variant of arctangent
    - Arctangent function with location parameter
    - Numerical accuracy
categories:
    - Trigonometry
---
In mathematics, the inverse trigonometric functions (occasionally called cyclometric functions[1]) are the inverse functions of the trigonometric functions (with suitably restricted domains). Specifically, they are the inverses of the sine, cosine, tangent, cotangent, secant, and cosecant functions. They are used to obtain an angle from any of the angle's trigonometric ratios. Inverse trigonometric functions are widely used in engineering, navigation, physics, and geometry.
