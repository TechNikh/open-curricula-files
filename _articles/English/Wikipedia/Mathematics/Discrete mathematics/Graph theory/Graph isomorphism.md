---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_isomorphism
offline_file: ""
offline_thumbnail: ""
uuid: c399be56-998c-4525-a415-ed629f0610d1
updated: 1484308620
title: Graph isomorphism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Whitneys_theorem_exception.svg.png
tags:
    - Variations
    - Isomorphism of labeled graphs
    - Motivation
    - Whitney theorem
    - Recognition of graph isomorphism
    - Notes
categories:
    - Graph theory
---
such that any two vertices u and v of G are adjacent in G if and only if ƒ(u) and ƒ(v) are adjacent in H. This kind of bijection is commonly described as "edge-preserving bijection", in accordance with the general notion of isomorphism being a structure-preserving bijection.
