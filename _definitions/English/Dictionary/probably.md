---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/probably
offline_file: ""
offline_thumbnail: ""
uuid: d7b73e8e-d768-4d78-8e7b-ecc20d901d27
updated: 1484310220
title: probably
categories:
    - Dictionary
---
probably
     adv 1: with considerable certainty; without much doubt; "He is
            probably out of the country"; "in all likelihood we
            are headed for war" [syn: {likely}, {in all likelihood},
             {in all probability}, {belike}]
     2: easy to believe on the basis of available evidence; "he
        talked plausibly before the committee"; "he will probably
        win the election" [syn: {credibly}, {believably}, {plausibly}]
        [ant: {incredibly}]
