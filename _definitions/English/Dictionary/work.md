---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/work
offline_file: ""
offline_thumbnail: ""
uuid: 9af81520-848d-446a-bd99-ae39df392e89
updated: 1484310328
title: work
categories:
    - Dictionary
---
work
     n 1: activity directed toward making or doing something; "she
          checked several points needing further work"
     2: a product produced or accomplished through the effort or
        activity or agency of a person or thing; "it is not
        regarded as one of his more memorable works"; "the
        symphony was hailed as an ingenious work"; "he was
        indebted to the pioneering work of John Dewey"; "the work
        of an active imagination"; "erosion is the work of wind or
        water over time" [syn: {piece of work}]
     3: the occupation for which you are paid; "he is looking for
        employment"; "a lot of people are out of work" [syn: {employment}]
     4: ...
