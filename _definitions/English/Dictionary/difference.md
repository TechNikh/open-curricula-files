---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/difference
offline_file: ""
offline_thumbnail: ""
uuid: 544c774b-6431-49e7-8580-aef0141da1f3
updated: 1484310349
title: difference
categories:
    - Dictionary
---
difference
     n 1: the quality of being unlike or dissimilar; "there are many
          differences between jazz and rock" [ant: {sameness}]
     2: a variation that deviates from the standard or norm; "the
        deviation from the mean" [syn: {deviation}, {divergence},
        {departure}]
     3: a disagreement or argument about something important; "he
        had a dispute with his wife"; "there were irreconcilable
        differences"; "the familiar conflict between Republicans
        and Democrats" [syn: {dispute}, {difference of opinion}, {conflict}]
     4: a significant change; "the difference in her is amazing";
        "his support made a real difference"
     5: the number ...
