---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inversion_barrier
offline_file: ""
offline_thumbnail: ""
uuid: b1e7c626-3616-4bb4-bf51-80b502b5f403
updated: 1484309279
title: Inversion barrier
categories:
    - Molecular geometry
---
In molecular geometry, the term inversion barrier refers to the amount of energy required for the geometric structure of a molecule to undergo the conformational change of inversion; i.e. for the molecule to be turned inside out. Nitrogen inversion is one example of such a transition in the conformational structure of a molecule.
