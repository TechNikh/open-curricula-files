---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manufacture
offline_file: ""
offline_thumbnail: ""
uuid: 746b9108-c6ec-404c-8db1-1693c25a6f64
updated: 1484310384
title: manufacture
categories:
    - Dictionary
---
manufacture
     n 1: the organized action of making of goods and services for
          sale; "American industry is making increased use of
          computers to control production" [syn: {industry}]
     2: the act of making something (a product) from raw materials;
        "the synthesis and fabrication of single crystals"; "an
        improvement in the manufacture of explosives";
        "manufacturing is vital to Great Britain" [syn: {fabrication}]
     v 1: put together out of components or parts; "the company
          fabricates plastic chairs"; "They manufacture small
          toys" [syn: {fabricate}, {construct}]
     2: make up something artificial or untrue [syn: {fabricate}, ...
