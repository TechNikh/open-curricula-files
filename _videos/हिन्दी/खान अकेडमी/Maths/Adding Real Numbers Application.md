---
version: 1
type: video
provider: YouTube
id: b5smM_pJpeA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8b38b0a5-4b78-4b18-b330-b584b96ba54e
updated: 1486069613
title: Adding Real Numbers Application
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/b5smM_pJpeA/default.jpg
    - https://i3.ytimg.com/vi/b5smM_pJpeA/1.jpg
    - https://i3.ytimg.com/vi/b5smM_pJpeA/2.jpg
    - https://i3.ytimg.com/vi/b5smM_pJpeA/3.jpg
---
