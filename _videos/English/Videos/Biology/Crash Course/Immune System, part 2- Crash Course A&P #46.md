---
version: 1
type: video
provider: YouTube
id: 2DFN4IBZ3rI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0382daa7-ba05-4b1f-bc65-d916d6693b08
updated: 1486069681
title: 'Immune System, part 2- Crash Course A&P #46'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/2DFN4IBZ3rI/default.jpg
    - https://i3.ytimg.com/vi/2DFN4IBZ3rI/1.jpg
    - https://i3.ytimg.com/vi/2DFN4IBZ3rI/2.jpg
    - https://i3.ytimg.com/vi/2DFN4IBZ3rI/3.jpg
---
