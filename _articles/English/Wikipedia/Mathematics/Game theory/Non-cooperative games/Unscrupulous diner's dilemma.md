---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Unscrupulous_diner%27s_dilemma'
offline_file: ""
offline_thumbnail: ""
uuid: 0675136c-91e9-4f7c-b561-0feda46664e4
updated: 1484309059
title: "Unscrupulous diner's dilemma"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_13.png
tags:
    - Formal definition and equilibrium analysis
    - Experimental evidence
categories:
    - Non-cooperative games
---
In game theory, the unscrupulous diner's dilemma (or just diner's dilemma) is an n-player prisoner's dilemma. The situation imagined is that several individuals go out to eat, and prior to ordering, they agree to split the check equally between all of them. Each individual must now choose whether to order the expensive or inexpensive dish. It is presupposed that the expensive dish is better than the cheaper, but not by enough to warrant paying the difference when eating alone. Each individual reasons that the expense s/he adds to their bill by ordering the more expensive item is very small, and thus the improved dining experience is worth the money. However, having all reasoned thus, they ...
