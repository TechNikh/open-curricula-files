---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suitable
offline_file: ""
offline_thumbnail: ""
uuid: 6da27caf-6451-45eb-98e9-dce134947cc3
updated: 1484310315
title: suitable
categories:
    - Dictionary
---
suitable
     adj 1: suitable for the desired purpose; "Is this a suitable dress
            for the office?"
     2: meant or adapted for an occasion or use; "a tractor suitable
        (or fit) for heavy duty"; "not an appropriate (or fit)
        time for flippancy" [syn: {appropriate}, {suited}]
     3: appropriate for a condition or occasion; "everything in its
        proper place"; "the right man for the job"; "she is not
        suitable for the position" [syn: {proper}, {right}]
     4: worthy of being chosen especially as a spouse; "the parents
        found the girl suitable for their son" [syn: {desirable},
        {worthy}]
