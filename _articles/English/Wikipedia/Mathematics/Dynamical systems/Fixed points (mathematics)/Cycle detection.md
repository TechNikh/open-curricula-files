---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cycle_detection
offline_file: ""
offline_thumbnail: ""
uuid: eabf378f-a780-43cc-8990-e93ea802f907
updated: 1484308780
title: Cycle detection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Functional_graph.svg.png
tags:
    - Example
    - Definitions
    - Computer representation
    - Algorithms
    - Tortoise and hare
    - "Brent's algorithm"
    - Time–space tradeoffs
    - Applications
categories:
    - Fixed points (mathematics)
---
For any function f that maps a finite set S to itself, and any initial value x0 in S, the sequence of iterated function values
