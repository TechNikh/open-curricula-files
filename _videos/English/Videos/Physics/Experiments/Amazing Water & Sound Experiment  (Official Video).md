---
version: 1
type: video
provider: YouTube
id: H11YqPI25Wg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4d277c11-acc8-46e2-a998-ce267d76afd4
updated: 1486069637
title: 'Amazing Water & Sound Experiment  (Official Video)'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/H11YqPI25Wg/default.jpg
    - https://i3.ytimg.com/vi/H11YqPI25Wg/1.jpg
    - https://i3.ytimg.com/vi/H11YqPI25Wg/2.jpg
    - https://i3.ytimg.com/vi/H11YqPI25Wg/3.jpg
---
