---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vertex_enumeration_problem
offline_file: ""
offline_thumbnail: ""
uuid: 73ee2698-1ca0-4cdd-a1a7-221091fa7a12
updated: 1484308601
title: Vertex enumeration problem
categories:
    - Discrete geometry
---
In mathematics, the vertex enumeration problem for a polytope, a polyhedral cell complex, a hyperplane arrangement, or some other object of discrete geometry, is the problem of determination of the object's vertices given some formal representation of the object. A classical example is the problem of enumeration of the vertices of a convex polytope specified by a set of linear inequalities:[1]
