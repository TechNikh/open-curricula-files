---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unfit
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484613481
title: unfit
categories:
    - Dictionary
---
unfit
     adj 1: below the required standards for a purpose; "an unfit
            parent"; "unfit for human consumption" [ant: {fit}]
     2: not in good physical or mental condition; out of condition;
        "fat and very unfit"; "certified as unfit for army
        service"; "drunk and unfit for service" [ant: {fit}]
     3: physically unsound or diseased; "has a bad back"; "a bad
        heart"; "bad teeth"; "an unsound limb"; "unsound teeth"
        [syn: {bad}, {unsound}]
     v : make unfit or unsuitable; "Your income disqualifies you"
         [syn: {disqualify}, {indispose}] [ant: {qualify}]
     [also: {unfitting}, {unfitted}]
