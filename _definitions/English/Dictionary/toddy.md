---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/toddy
offline_file: ""
offline_thumbnail: ""
uuid: 9ef35088-7f95-42da-ad35-d457e50d18f7
updated: 1484310168
title: toddy
categories:
    - Dictionary
---
toddy
     n : a mixed drink made of liquor and water with sugar and spices
         and served hot [syn: {hot toddy}]
