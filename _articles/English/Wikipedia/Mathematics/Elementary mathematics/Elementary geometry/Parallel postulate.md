---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parallel_postulate
offline_file: ""
offline_thumbnail: ""
uuid: 3f3ae02a-add9-4473-a10b-79eb2a1e694c
updated: 1484308935
title: Parallel postulate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Parallel_postulate_en.svg.png
tags:
    - Equivalent properties
    - History
    - "Converse of Euclid's parallel postulate"
    - Criticism
    - Notes
categories:
    - Elementary geometry
---
In geometry, the parallel postulate, also called Euclid's fifth postulate because it is the fifth postulate in Euclid's Elements, is a distinctive axiom in Euclidean geometry. It states that, in two-dimensional geometry:
