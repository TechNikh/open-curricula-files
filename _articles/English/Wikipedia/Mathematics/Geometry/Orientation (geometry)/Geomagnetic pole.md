---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geomagnetic_pole
offline_file: ""
offline_thumbnail: ""
uuid: affa0f1f-7bdb-480f-b8a4-7b258f70a943
updated: 1484309293
title: Geomagnetic pole
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Geomagnetisme.svg.png
tags:
    - Definition
    - Location
    - Movement
    - Geomagnetic reversal
    - Notes
categories:
    - Orientation (geometry)
---
The geomagnetic poles are antipodal points where the axis of a best-fitting dipole intersects the Earth's surface. This dipole is equivalent to a powerful bar magnet at the center of the Earth, and it is this theoretical dipole that comes closer than any other to accounting for the magnetic field observed at the Earth's surface. In contrast, the actual Earth's magnetic poles are not antipodal—that is, they do not lie on a line passing through the center of the Earth.
