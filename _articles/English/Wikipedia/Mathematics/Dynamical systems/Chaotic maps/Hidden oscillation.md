---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hidden_oscillation
offline_file: ""
offline_thumbnail: ""
uuid: 58ac8ddf-5655-44af-8dab-cc18a3b95efc
updated: 1484308715
title: Hidden oscillation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Self-Excited_attractor_in_Chua_circuit.jpg
tags:
    - Self-excited attractor localization
    - Hidden attractor localization
categories:
    - Chaotic maps
---
An oscillation in a dynamical system can be easily localized numerically if initial conditions from its open neighborhood lead to long-run behavior that approaches the oscillation. Such an oscillation (or set of oscillations) is called an attractor, and its attracting set is called the basin of attraction. Thus, from a computational point of view the following classification of attractors based on the simplicity of finding basin of attraction in the phase space is suggested:[1][2][3] an attractor is called a hidden attractor if its basin of attraction does not intersect with small neighborhoods of equilibria, otherwise it is called a self-excited attractor.
