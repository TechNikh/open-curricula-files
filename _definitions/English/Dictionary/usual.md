---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/usual
offline_file: ""
offline_thumbnail: ""
uuid: d527a9ca-ebde-4a55-a0f2-5963d96248ae
updated: 1484310204
title: usual
categories:
    - Dictionary
---
usual
     adj 1: occurring or encountered or experienced or observed
            frequently or in accordance with regular practice or
            procedure; "grew the usual vegetables"; "the usual
            summer heat"; "came at the usual time"; "the child's
            usual bedtime" [ant: {unusual}]
     2: commonly encountered; "a common (or familiar) complaint";
        "the usual greeting" [syn: {common}]
