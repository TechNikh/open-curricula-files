---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/developmental
offline_file: ""
offline_thumbnail: ""
uuid: b34401e1-49ad-4a61-ae0f-0e0e912afd8f
updated: 1484310437
title: developmental
categories:
    - Dictionary
---
developmental
     adj : of or relating to or constituting development;
           "developmental psychology"
