---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Imaginary_point
offline_file: ""
offline_thumbnail: ""
uuid: 7d7b0034-9491-49f6-89c8-a2befcb9eafb
updated: 1484309363
title: Imaginary point
tags:
    - Definition
    - Properties
categories:
    - Geometry stubs
---
In geometry, in the context of a real geometric space extended to (or embedded in) a complex projective space, an imaginary point is a point not contained in the embedded space.
