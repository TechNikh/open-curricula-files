---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roadster
offline_file: ""
offline_thumbnail: ""
uuid: 557a60f1-6dba-4967-bf70-c0e82ce9f584
updated: 1484310561
title: roadster
categories:
    - Dictionary
---
roadster
     n 1: an open automobile having a front seat and a rumble seat
          [syn: {runabout}, {two-seater}]
     2: a small lightweight carriage; drawn by a single horse [syn:
        {buggy}]
