---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/risk
offline_file: ""
offline_thumbnail: ""
uuid: 643c70bd-a95e-4770-be09-4f4d72d053af
updated: 1484310475
title: risk
categories:
    - Dictionary
---
risk
     n 1: a source of danger; a possibility of incurring loss or
          misfortune; "drinking alcohol is a health hazard" [syn:
          {hazard}, {jeopardy}, {peril}]
     2: a venture undertaken without regard to possible loss or
        injury; "he saw the rewards but not the risks of crime";
        "there was a danger he would do the wrong thing" [syn: {peril},
         {danger}]
     3: the probability of becoming infected given that exposure to
        an infectious agent has occurred [syn: {risk of infection}]
     4: the probability of being exposed to an infectious agent
        [syn: {risk of exposure}]
     v 1: expose to a chance of loss or damage; "We risked losing a
 ...
