---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pbs
offline_file: ""
offline_thumbnail: ""
uuid: 7daae59c-8495-49e2-84a9-c4141745a955
updated: 1484308389
title: PBS
tags:
    - Overview
    - Programming
    - PBS Kids
    - Sports
    - Governance
    - Member stations
    - Participating stations
    - PBS networks
    - Independent networks
    - Criticism and controversy
    - On-air fundraising
    - Accusations of political/ideological bias
    - Lawsuit with Pacific Arts
    - Warning, Alert and Response Network (WARN)
categories:
    - Uncategorized
---
The Public Broadcasting Service (PBS) is an American public broadcaster and television program distributor. Headquartered in Arlington, Virginia, PBS is an independently operated non-profit organization and is the most prominent provider of television programming to public television stations in the United States, distributing series such as Keeping Up Appearances, BBC World News, NOVA scienceNOW, NOVA, Dragon Tales, PBS NewsHour, Walking with Dinosaurs, Masterpiece, Nature, American Masters, Frontline, and Antiques Roadshow.
