---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Angaraland
offline_file: ""
offline_thumbnail: ""
uuid: d605b7bd-9be0-4514-903d-40e096151e2e
updated: 1484308426
title: Siberia (continent)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Siberian_craton_location.jpg
tags:
    - Precambrian history
    - Paleozoic history
    - Mesozoic and Cenozoic history
categories:
    - Uncategorized
---
Siberia, also known as Angaraland (or simply Angara) and Angarida,[1] is an ancient craton located in the heart of Siberia. Today forming the Central Siberian Plateau, it formed an independent continent before the Permian.
