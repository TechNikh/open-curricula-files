---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multidimensional_system
offline_file: ""
offline_thumbnail: ""
uuid: cfe8f91f-86b7-48a2-bb02-436cd2af2355
updated: 1484308834
title: Multidimensional system
tags:
    - Applications
    - Linear multidimensional state-space model
    - Multidimensional transfer function
    - Realization of a 2d transfer function
    - 'Example: all zero or finite impulse response'
categories:
    - Stability theory
---
In mathematical systems theory, a multidimensional system or m-D system is a system in which not only one independent variable exists (like time), but there are several independent variables.
