---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grid_method_multiplication
offline_file: ""
offline_thumbnail: ""
uuid: f338e69f-9e0d-4b37-80cc-421464baeb3e
updated: 1484308918
title: Grid method multiplication
tags:
    - Calculations
    - Introductory motivation
    - Standard blocks
    - Larger numbers
    - Other applications
    - Fractions
    - Algebra
    - Mathematics
categories:
    - Elementary arithmetic
---
In [mathematics education] at the level of primary school or elementary school, the grid method (also known as the box method) of multiplication is an introductory approach to multi-digit multiplication calculations, i.e. multiplications involving numbers larger than ten.
