---
version: 1
type: video
provider: YouTube
id: EKZSwjVR594
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Stoichiometry%20of%20a%20Reaction%20in%20Solution.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0594c35e-6414-4e0f-94fa-fe4d4dbfe518
updated: 1486069585
title: Stoichiometry of a Reaction in Solution
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/EKZSwjVR594/default.jpg
    - https://i3.ytimg.com/vi/EKZSwjVR594/1.jpg
    - https://i3.ytimg.com/vi/EKZSwjVR594/2.jpg
    - https://i3.ytimg.com/vi/EKZSwjVR594/3.jpg
---
Stoichiometry of a Reaction in Solution
More free lessons at: http://www.khanacademy.org/video?v=EKZSwjVR594
