---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ballooning_instability
offline_file: ""
offline_thumbnail: ""
uuid: 14dff855-241d-4d12-b22e-de05efd2dcc9
updated: 1484308822
title: Ballooning instability
categories:
    - Stability theory
---
The ballooning instability, or ballooning mode, is a form of plasma instability seen in tokamak fusion power reactors. The name refers to the shape and action of the instability, which acts like the elongations formed in a balloon when it is squeezed.
