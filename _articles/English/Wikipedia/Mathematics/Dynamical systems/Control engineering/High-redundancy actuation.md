---
version: 1
type: article
id: https://en.wikipedia.org/wiki/High-redundancy_actuation
offline_file: ""
offline_thumbnail: ""
uuid: 832ddec2-7467-4b4f-b8d0-0eb55a6c40a9
updated: 1484308739
title: High-redundancy actuation
tags:
    - Overview
    - Inspiration of high-redundancy actuation
    - Technical realisation
    - Using actuation elements in series
    - Available technology
categories:
    - Control engineering
---
