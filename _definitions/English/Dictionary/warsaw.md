---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/warsaw
offline_file: ""
offline_thumbnail: ""
uuid: 74283a27-10ad-49e7-8cc9-a3e3bb0f4d07
updated: 1484310180
title: warsaw
categories:
    - Dictionary
---
Warsaw
     n : the capital and largest city of Poland; located in central
         Poland [syn: {Warszawa}, {capital of Poland}]
