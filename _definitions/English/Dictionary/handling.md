---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/handling
offline_file: ""
offline_thumbnail: ""
uuid: 133f8eb6-6338-4481-baee-a8adc907f3d0
updated: 1484310553
title: handling
categories:
    - Dictionary
---
handling
     n 1: manual (or mechanical) carrying or moving or delivering or
          working with something
     2: the action of touching with the hands or the skillful use of
        the hands [syn: {manipulation}]
     3: the management of someone or something; "the handling of
        prisoners"; "the treatment of water sewage"; "the right to
        equal treatment in the criminal justice system" [syn: {treatment}]
