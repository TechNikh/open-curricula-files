---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/choreography
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484539981
title: choreography
categories:
    - Dictionary
---
choreography
     n 1: a show involving artistic dancing [syn: {stage dancing}]
     2: the representation of dancing by symbols as music is
        represented by notes
     3: a notation used by choreographers
