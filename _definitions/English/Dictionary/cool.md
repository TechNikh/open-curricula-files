---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cool
offline_file: ""
offline_thumbnail: ""
uuid: 07630ddb-1c8b-4e29-835f-4de65fc36ea9
updated: 1484310343
title: cool
categories:
    - Dictionary
---
cool
     adj 1: neither warm or very cold; giving relief from heat; "a cool
            autumn day"; "a cool room"; "cool summer dresses";
            "cool drinks"; "a cool breeze" [ant: {warm}]
     2: marked by calm self-control (especially in trying
        circumstances); unemotional; "play it cool"; "keep cool";
        "stayed coolheaded in the crisis"; "the most nerveless
        winner in the history of the tournament" [syn: {coolheaded},
         {nerveless}]
     3: (color) inducing the impression of coolness; used especially
        of greens and blues and violets; "cool greens and blues
        and violets" [ant: {warm}]
     4: psychologically cool and unenthusiastic; ...
