---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/skilled
offline_file: ""
offline_thumbnail: ""
uuid: 9d6df47d-946e-498c-a69b-c0026ad8c405
updated: 1484310453
title: skilled
categories:
    - Dictionary
---
skilled
     adj : having or showing or requiring special skill; "only the most
           skilled gymnasts make an Olympic team"; "a skilled
           surgeon has many years of training and experience"; "a
           skilled reconstruction of her damaged elbow"; "a
           skilled trade" [ant: {unskilled}]
