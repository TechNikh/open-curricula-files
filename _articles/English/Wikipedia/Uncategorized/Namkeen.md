---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Namkeen
offline_file: ""
offline_thumbnail: ""
uuid: e2e01749-54a0-43f2-97f1-22c0f8f43afe
updated: 1484308460
title: Namkeen
tags:
    - Plot
    - Themes
    - Cast
    - Production
    - Release
    - Special Mention
    - Soundtrack
    - Awards
categories:
    - Uncategorized
---
Namkeen (Devanāgarī: नमकीन, English: Salty), 1982 Hindi film, directed by Gulzar, with Sharmila Tagore, Sanjeev Kumar, Shabana Azmi and Waheeda Rehman as leads. It was yet another movie by Gulzar made on some very sensitive but untouched aspects of Indian society especially in rural areas. The story Akal Basant was by Samaresh Basu on whose story, Gulzar had previously made Kitaab (1977).[1][2]
