---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enumeration
offline_file: ""
offline_thumbnail: ""
uuid: f56b66e7-cc3b-4057-989a-36d0b3e4992c
updated: 1484310484
title: enumeration
categories:
    - Dictionary
---
enumeration
     n 1: a numbered list [syn: {numbering}]
     2: the act of counting; "the counting continued for several
        hours" [syn: {count}, {counting}, {numeration}, {reckoning},
         {tally}]
