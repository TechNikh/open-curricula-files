---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coloumb
offline_file: ""
offline_thumbnail: ""
uuid: c7ff7690-137c-4ba6-acfd-2739233272ff
updated: 1484308321
title: Coulomb
tags:
    - Name and notation
    - Definition
    - SI prefixes
    - Conversions
    - Relation to elementary charge
    - In everyday terms
    - Notes and references
categories:
    - Uncategorized
---
The coulomb (unit symbol: C) is the International System of Units (SI) unit of electric charge. It is the charge (symbol: Q or q) transported by a constant current of one ampere in one second:
