---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Attack_tolerance
offline_file: ""
offline_thumbnail: ""
uuid: 0436f492-e615-4fa5-9200-3f981e30b0e9
updated: 1484309432
title: Attack tolerance
tags:
    - Attack types
    - Average node degree
    - Node persistence
    - Temporal closeness
    - Network model tolerances
    - Erdős–Rényi model
    - Scale-free model
categories:
    - Network theory
---
In the context of complex networks, attack tolerance is the network’s robustness meaning the ability to maintain the overall connectivity and diameter of the network as nodes are removed.
