---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Synergetics_(Haken)
offline_file: ""
offline_thumbnail: ""
uuid: 281ba7fb-ba69-40e6-839e-02e584322480
updated: 1484308864
title: Synergetics (Haken)
tags:
    - Order-parameter concept
categories:
    - Thermodynamics
---
Synergetics is an interdisciplinary science explaining the formation and self-organization of patterns and structures in open systems far from thermodynamic equilibrium. It is founded by Hermann Haken, inspired by the laser theory. Haken's interpretation of the laser principles as self-organization of non-equilibrium systems paved the way at the end of the 1960s to the development of synergetics. One of his successful popular books is Erfolgsgeheimnisse der Natur, translated into English as The Science of Structure: Synergetics.
