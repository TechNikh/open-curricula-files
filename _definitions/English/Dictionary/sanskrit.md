---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sanskrit
offline_file: ""
offline_thumbnail: ""
uuid: 99ddaa08-5ca5-453a-8cbd-0ef9695dc67a
updated: 1484310397
title: sanskrit
categories:
    - Dictionary
---
Sanskrit
     n : (Hinduism) an ancient language of India (the language of the
         Vedas and of Hinduism); an official language of India
         although it is now used only for religious purposes [syn:
          {Sanskritic language}]
