---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convert
offline_file: ""
offline_thumbnail: ""
uuid: 75ac9687-2eef-45f5-a254-e4df5ce21a7f
updated: 1484310264
title: convert
categories:
    - Dictionary
---
convert
     n : a person who has been converted to another religious or
         political belief
     v 1: change the nature, purpose, or function of something;
          "convert lead into gold"; "convert hotels into jails";
          "convert slaves to laborers"
     2: change from one system to another or to a new plan or
        policy; "We converted from 220 to 110 Volt" [syn: {change
        over}]
     3: change religious beliefs, or adopt a religious belief; "She
        converted to Buddhism"
     4: exchange or replace with another, usually of the same kind
        or category; "Could you convert my dollars into pounds?";
        "He changed his name"; "convert centimeters into ...
