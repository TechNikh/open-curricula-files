---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feeder
offline_file: ""
offline_thumbnail: ""
uuid: 39d6c326-4506-4f8f-b082-84333e7ae567
updated: 1484310273
title: feeder
categories:
    - Dictionary
---
feeder
     n 1: an animal being fattened or suitable for fattening
     2: someone who consumes food for nourishment [syn: {eater}]
     3: a branch that flows into the main stream [syn: {tributary},
        {affluent}] [ant: {distributary}]
     4: a machine that automatically provides a supply of some
        material; "the feeder discharged feed into a trough for
        the livestock" [syn: {self-feeder}]
