---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weak
offline_file: ""
offline_thumbnail: ""
uuid: 5fdcaeb1-7e12-47fe-b1ab-6ffaa2123839
updated: 1484310330
title: weak
categories:
    - Dictionary
---
weak
     adj 1: having little physical or spiritual strength; "a weak radio
            signal"; "a weak link" [ant: {strong}]
     2: overly diluted; thin and insipid; "washy coffee"; "watery
        milk"; "weak tea" [syn: {watery}, {washy}]
     3: lacking power [syn: {powerless}] [ant: {powerful}]
     4: used of vowels or syllables; pronounced with little or no
        stress; "a syllable that ends in a short vowel is a light
        syllable"; "a weak stress on the second syllable" [syn: {unaccented},
         {light}]
     5: having the attributes of man as opposed to e.g. divine
        beings; "I'm only human"; "frail humanity" [syn: {fallible},
         {frail}, {imperfect}]
     ...
