---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scarcity
offline_file: ""
offline_thumbnail: ""
uuid: a86b5757-5af0-49d5-8537-aa68ea39d6e0
updated: 1484310262
title: scarcity
categories:
    - Dictionary
---
scarcity
     n : a small and inadequate amount [syn: {scarceness}] [ant: {abundance}]
