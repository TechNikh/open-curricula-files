---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Misorientation
offline_file: ""
offline_thumbnail: ""
uuid: 2e0e160d-b998-406c-b0dd-d1ee3a58403b
updated: 1484309315
title: Misorientation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-MDF_rodrigues_AA5083.jpg
tags:
    - Symmetry and Misorientation
    - Misorientation Distribution
    - Graphical Representation
    - Example of Calculating Misorientation
categories:
    - Symmetry
---
In crystalline materials, the orientation of a crystallite is defined by a transformation from a sample reference frame (i.e. defined by the direction of a rolling or extrusion process and two orthogonal directions) to the local reference frame of the crystalline lattice, as defined by the basis of the unit cell. In the same way, misorientation is the transformation necessary to move from one local crystal frame to some other crystal frame. That is, it is the distance in orientation space between two distinct orientations. If the orientations are specified in terms of matrices of direction cosines gA and gB, then the misorientation operator ∆gAB going from A to B can be defined as follows:
