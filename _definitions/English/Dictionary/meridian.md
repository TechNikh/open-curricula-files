---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meridian
offline_file: ""
offline_thumbnail: ""
uuid: 74948a3d-b07e-4635-b3d6-401da26d34d7
updated: 1484310433
title: meridian
categories:
    - Dictionary
---
meridian
     adj : of or happening at noon; "meridian hour"
     n 1: a town in eastern Mississippi
     2: an imaginary great circle on the surface of the earth
        passing through the north and south poles at right angles
        to the equator; "all points on the same meridian have the
        same longitude" [syn: {longitude}, {line of longitude}]
