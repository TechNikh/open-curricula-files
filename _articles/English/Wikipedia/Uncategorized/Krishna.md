---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Krishna
offline_file: ""
offline_thumbnail: ""
uuid: 9555f24b-18ee-4f0b-8353-216ab466765e
updated: 1484308340
title: Krishna
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Sri_Mariamman_Temple_Singapore_2_amk.jpg
tags:
    - Names and epithets
    - Iconography
    - Literary sources
    - life
    - Birth
    - Childhood and youth
    - The prince
    - Kurukshetra War and Bhagavad Gita
    - family
    - Later life
    - Proposed datings
    - Worship
    - Vaishnavism
    - Early traditions
    - Bhakti tradition
    - Spread of the Krishna-bhakti movement
    - In the West
    - In South India
    - In the performing arts
    - In other religions
    - Jainism
    - Buddhism
    - "Bahá'í Faith"
    - Ahmadiyya Islam
    - Other
    - Sources
categories:
    - Uncategorized
---
Krishna (/ˈkrɪʃnə/; Sanskrit: कृष्ण, Kṛṣṇa in IAST, pronounced [ˈkr̩ʂɳə] ( listen)) is a major Hindu deity worshiped in a variety of different perspectives. Krishna is recognised as the Svayam Bhagavan which is the complete incarnation of Lord Vishnu. Krishna is one of the most widely worshiped and popular of all Hindu deities.[2] Krishna's birthday is celebrated every year by Hindus on the eighth day (Ashtami) of the Krishna Paksha (dark fortnight) of the month of Shraavana in the Hindu calendar.[3]
