---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moment_map
offline_file: ""
offline_thumbnail: ""
uuid: 28283699-0537-4cc0-a6f4-f3dbf1e63131
updated: 1484308791
title: Moment map
tags:
    - Formal definition
    - Hamiltonian group actions
    - Examples of moment maps
    - Some facts about moment maps
    - Symplectic quotients
    - Notes
categories:
    - Hamiltonian mechanics
---
In mathematics, specifically in symplectic geometry, the momentum map (or moment map) is a tool associated with a Hamiltonian action of a Lie group on a symplectic manifold, used to construct conserved quantities for the action. The moment map generalizes the classical notions of linear and angular momentum. It is an essential ingredient in various constructions of symplectic manifolds, including symplectic (Marsden–Weinstein) quotients, discussed below, and symplectic cuts and sums.
