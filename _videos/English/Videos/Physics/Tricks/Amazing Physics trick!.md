---
version: 1
type: video
provider: YouTube
id: wVKLJUqKi7A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3b0ce04d-eeef-481f-b1a0-982580dcf612
updated: 1486069658
title: Amazing Physics trick!
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/wVKLJUqKi7A/default.jpg
    - https://i3.ytimg.com/vi/wVKLJUqKi7A/1.jpg
    - https://i3.ytimg.com/vi/wVKLJUqKi7A/2.jpg
    - https://i3.ytimg.com/vi/wVKLJUqKi7A/3.jpg
---
