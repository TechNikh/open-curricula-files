---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Uae
offline_file: ""
offline_thumbnail: ""
uuid: a9c2f6b8-f505-4d29-8b0e-30b7bd1f186c
updated: 1484308450
title: United Arab Emirates
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Map_of_expansion_of_Caliphate.svg.png
tags:
    - History
    - Antiquity
    - Islam
    - Ottoman and Portuguese era
    - British era, discovery of oil
    - Independence (1971)
    - After independence
    - geography
    - Flora and Fauna
    - Climate
    - Politics
    - Foreign relations
    - Military
    - Political divisions
    - Law
    - Human rights
    - Dress code
    - Media
    - Economy
    - Transport
    - Expo 2020
    - culture
    - Food
    - Sports
    - Football
    - cricket
    - Education
    - Demographics
    - religion
    - Largest cities
    - Languages
    - Health
    - Notes
categories:
    - Uncategorized
---
The United Arab Emirates (i/juːˌnaɪtᵻd ˌærəb ˈɛmɪrᵻts/; Arabic: دولة الإمارات العربية المتحدة‎‎ Dawlat al-Imārāt al-'Arabīyah al-Muttaḥidah), sometimes simply called the Emirates (Arabic: الامارات‎‎ al-Imārāt) or the UAE, is a federal absolute monarchy in Western Asia, at the southeast end of the Arabian Peninsula and bordering seas in the Gulf of Oman, occupying the Persian Gulf. It borders with countries including Oman to the east and Saudi Arabia to the south, although the United Arab Emirates shares maritime borders with Qatar in the west and Iran in the north, and sharing sea borders with Iraq, Kuwait, and Bahrain. In 2013, ...
