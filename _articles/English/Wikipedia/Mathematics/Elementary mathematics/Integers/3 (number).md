---
version: 1
type: article
id: https://en.wikipedia.org/wiki/3_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 122b2395-1572-4898-94cf-b35f0acb14f6
updated: 1484308944
title: 3 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Shield-Trinity-Scutum-Fidei-English.svg.png
tags:
    - Evolution of the glyph
    - Flat top 3
    - In mathematics
    - In numeral systems
    - List of basic calculations
    - In science
    - In protoscience
    - In astronomy
    - In pseudoscience
    - In philosophy
    - In religion
    - In Christianity
    - In Judaism
    - In Buddhism
    - In Shinto
    - In Taoism
    - In Hinduism
    - In Zoroastrianism
    - In Norse mythology
    - In other religions
    - In esoteric tradition
    - As a lucky or unlucky number
    - In sports
categories:
    - Integers
---
