---
version: 1
type: video
provider: YouTube
id: s54MKrK9DZk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9c792a1d-206a-4ebd-90a0-17a2f885f965
updated: 1486069634
title: 'How to Make a USB Fan using CD at Home - Easy Way'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/s54MKrK9DZk/default.jpg
    - https://i3.ytimg.com/vi/s54MKrK9DZk/1.jpg
    - https://i3.ytimg.com/vi/s54MKrK9DZk/2.jpg
    - https://i3.ytimg.com/vi/s54MKrK9DZk/3.jpg
---
