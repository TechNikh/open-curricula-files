---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamiltonian_path_problem
offline_file: ""
offline_thumbnail: ""
uuid: 677b6d2e-1e04-4915-9f2a-5a9a4a0497d7
updated: 1484309394
title: Hamiltonian path problem
categories:
    - Computational problems in graph theory
---
In the mathematical field of graph theory the Hamiltonian path problem and the Hamiltonian cycle problem are problems of determining whether a Hamiltonian path (a path in an undirected or directed graph that visits each vertex exactly once) or a Hamiltonian cycle exists in a given graph (whether directed or undirected). Both problems are NP-complete.[1]
