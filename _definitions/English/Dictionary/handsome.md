---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/handsome
offline_file: ""
offline_thumbnail: ""
uuid: a7f334e8-3fc5-41dd-9d1c-a374a7589f5e
updated: 1484310152
title: handsome
categories:
    - Dictionary
---
handsome
     adj 1: pleasing in appearance especially by reason of conformity to
            ideals of form and proportion; "a fine-looking woman";
            "a good-looking man"; "better-looking than her
            sister"; "very pretty but not so extraordinarily
            handsome"- Thackeray; "our southern women are
            well-favored"- Lillian Hellman [syn: {fine-looking}, {good-looking},
             {better-looking}, {well-favored}, {well-favoured}]
     2: given or giving freely; "was a big tipper"; "the bounteous
        goodness of God"; "bountiful compliments"; "a freehanded
        host"; "a handsome allowance"; "Saturday's child is loving
        and giving"; "a ...
