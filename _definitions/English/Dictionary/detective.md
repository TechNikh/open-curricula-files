---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/detective
offline_file: ""
offline_thumbnail: ""
uuid: 51f55a95-9775-43a5-8020-ec34fc101eb6
updated: 1484310144
title: detective
categories:
    - Dictionary
---
detective
     n 1: a police officer who investigates crimes [syn: {investigator},
           {tec}, {police detective}]
     2: an investigator engaged or employed in obtaining information
        not easily available to the public
