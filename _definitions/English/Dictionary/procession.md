---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/procession
offline_file: ""
offline_thumbnail: ""
uuid: 0d410071-8364-4bae-8968-1c46bb59be40
updated: 1484310557
title: procession
categories:
    - Dictionary
---
procession
     n 1: (theology) the origination of the Holy Spirit at Pentecost;
          "the emanation of the Holy Spirit"; "the rising of the
          Holy Ghost"; "the doctrine of the procession of the Holy
          Spirit from the Father and the Son" [syn: {emanation}, {rise}]
     2: the group action of a collection of people or animals or
        vehicles moving ahead in more or less regular formation;
        "processions were forbidden"
     3: the act of moving forward toward a goal [syn: {progress}, {progression},
         {advance}, {advancement}, {forward motion}, {onward
        motion}]
