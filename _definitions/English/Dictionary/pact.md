---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pact
offline_file: ""
offline_thumbnail: ""
uuid: a719a2d0-8b70-4576-b88c-b6363b9aec50
updated: 1484310549
title: pact
categories:
    - Dictionary
---
pact
     n : a written agreement between two states or sovereigns [syn: {treaty},
          {accord}]
