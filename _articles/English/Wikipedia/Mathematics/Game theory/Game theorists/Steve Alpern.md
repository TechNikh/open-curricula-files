---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Steve_Alpern
offline_file: ""
offline_thumbnail: ""
uuid: 136cffeb-00fb-408a-baca-9fbde70b4d94
updated: 1484308520
title: Steve Alpern
categories:
    - Game theorists
---
Steve Alpern is a professor of Operational Research at the University of Warwick, where he recently moved after working for many years at the London School of Economics. His early work was mainly in the area of dynamical systems and ergodic theory, but his more recent research has been concentrated in the fields of search games and rendezvous.[1] He informally introduced the rendezvous problem as early as 1976.[2] His collaborators include Shmuel Gal, Vic Baston and Robbert Fokkink.
