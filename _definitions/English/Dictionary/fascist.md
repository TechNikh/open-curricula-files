---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fascist
offline_file: ""
offline_thumbnail: ""
uuid: c286b0f6-e795-4561-919f-8bc69e775e3a
updated: 1484310549
title: fascist
categories:
    - Dictionary
---
fascist
     adj : relating to or characteristic of fascism; "fascist
           propaganda" [syn: {fascistic}]
     n : an adherent of fascism or other right-wing authoritarian
         views
