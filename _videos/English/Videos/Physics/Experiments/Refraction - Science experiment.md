---
version: 1
type: video
provider: YouTube
id: m9cUy6B--xc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3c1b732d-48d2-4cb7-a949-cabd8b96f467
updated: 1486069637
title: 'Refraction - Science experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/m9cUy6B--xc/default.jpg
    - https://i3.ytimg.com/vi/m9cUy6B--xc/1.jpg
    - https://i3.ytimg.com/vi/m9cUy6B--xc/2.jpg
    - https://i3.ytimg.com/vi/m9cUy6B--xc/3.jpg
---
