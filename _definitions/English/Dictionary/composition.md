---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/composition
offline_file: ""
offline_thumbnail: ""
uuid: 90eb8757-cc11-4ef5-bf14-c633bb60803d
updated: 1484310384
title: composition
categories:
    - Dictionary
---
composition
     n 1: a mixture of ingredients
     2: the way in which someone or something is composed [syn: {constitution},
         {makeup}]
     3: the spatial property resulting from the arrangement of parts
        in relation to each other and to the whole; "harmonious
        composition is essential in a serious work of art" [syn: {composing}]
     4: a musical work that has been created; "the composition is
        written in four movements" [syn: {musical composition}, {opus},
         {piece}, {piece of music}]
     5: musical creation [syn: {composing}]
     6: the act of creating written works; "writing was a form of
        therapy for him"; "it was a matter of disputed ...
