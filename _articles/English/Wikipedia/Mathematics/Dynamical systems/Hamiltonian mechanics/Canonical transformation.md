---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Canonical_transformation
offline_file: ""
offline_thumbnail: ""
uuid: 5c8cd689-9fea-40c1-b958-4df8f5283eb6
updated: 1484308787
title: Canonical transformation
tags:
    - Notation
    - Direct approach
    - "Liouville's theorem"
    - Generating function approach
    - Type 1 generating function
    - Type 2 generating function
    - Type 3 generating function
    - Type 4 generating function
    - Motion as a canonical transformation
    - Modern mathematical description
    - History
categories:
    - Hamiltonian mechanics
---
In Hamiltonian mechanics, a canonical transformation is a change of canonical coordinates (q, p, t) → (Q, P, t) that preserves the form of Hamilton's equations (that is, the new Hamilton's equations resulting from the transformed Hamiltonian may be simply obtained by substituting the new coordinates for the old coordinates), although it might not preserve the Hamiltonian itself. This is sometimes known as form invariance. Canonical transformations are useful in their own right, and also form the basis for the Hamilton–Jacobi equations (a useful method for calculating conserved quantities) and Liouville's theorem (itself the basis for classical statistical mechanics).
