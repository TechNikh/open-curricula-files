---
version: 1
type: video
provider: YouTube
id: 6RztHB24djM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Doppler%20Effect.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 40a6e233-84fd-42fc-8730-4c25440a1a6a
updated: 1486069652
title: Doppler Effect
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/6RztHB24djM/default.jpg
    - https://i3.ytimg.com/vi/6RztHB24djM/1.jpg
    - https://i3.ytimg.com/vi/6RztHB24djM/2.jpg
    - https://i3.ytimg.com/vi/6RztHB24djM/3.jpg
---
