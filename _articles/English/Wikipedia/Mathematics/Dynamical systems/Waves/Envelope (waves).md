---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Envelope_(waves)
offline_file: ""
offline_thumbnail: ""
uuid: 3cdc7cd5-7c4f-4d1d-aba2-5b3a68c0f912
updated: 1484308885
title: Envelope (waves)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Signal_envelopes.png
tags:
    - 'Example: beating waves'
    - Phase and group velocity
    - 'Example: envelope function approximation'
    - 'Example: diffraction patterns'
categories:
    - Waves
---
In physics and engineering, the envelope of an oscillating signal is a smooth curve outlining its extremes.[1] The envelope thus generalizes the concept of a constant frequency. The figure illustrates a modulated sine wave varying between an upper and a lower envelope. The envelope function may be a function of time, space, angle, or indeed of any variable.
