---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cape
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484637481
title: cape
categories:
    - Dictionary
---
cape
     n 1: a strip of land projecting into a body of water [syn: {ness}]
     2: a sleeveless garment like a cloak but shorter [syn: {mantle}]
