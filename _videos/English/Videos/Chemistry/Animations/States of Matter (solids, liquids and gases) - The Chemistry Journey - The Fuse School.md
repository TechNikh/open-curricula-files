---
version: 1
type: video
provider: YouTube
id: bMbmQzV-Ezs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/States%20of%20Matter%20%28solids%2C%20liquids%20and%20gases%29%20-%20The%20Chemistry%20Journey%20-%20The%20Fuse%20School.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 75536e1d-53f1-4188-bec8-afd6e8ede04d
updated: 1486069664
title: >
    States of Matter (solids, liquids and gases) | The Chemistry
    Journey | The Fuse School
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/bMbmQzV-Ezs/default.jpg
    - https://i3.ytimg.com/vi/bMbmQzV-Ezs/1.jpg
    - https://i3.ytimg.com/vi/bMbmQzV-Ezs/2.jpg
    - https://i3.ytimg.com/vi/bMbmQzV-Ezs/3.jpg
---
