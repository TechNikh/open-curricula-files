---
version: 1
type: video
provider: YouTube
id: BUmLw5m6F9s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Systems%20of%20linear%20inequalities%20word%20problems%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: df772edd-e0aa-4ea0-9158-fbacb5815695
updated: 1486069557
title: Systems of linear inequalities word problems example
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/BUmLw5m6F9s/default.jpg
    - https://i3.ytimg.com/vi/BUmLw5m6F9s/1.jpg
    - https://i3.ytimg.com/vi/BUmLw5m6F9s/2.jpg
    - https://i3.ytimg.com/vi/BUmLw5m6F9s/3.jpg
---
