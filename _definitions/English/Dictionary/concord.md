---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concord
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484535961
title: concord
categories:
    - Dictionary
---
Concord
     n 1: capital of the state of New Hampshire; located in south
          central New Hampshire on the Merrimack river [syn: {capital
          of New Hampshire}]
     2: a harmonious state of things in general and of their
        properties (as of colors and sounds); congruity of parts
        with one another and with the whole [syn: {harmony}, {concordance}]
     3: the determination of grammatical inflection on the basis of
        word relations [syn: {agreement}]
     4: town in eastern Massachusetts near Boston where the first
        battle of the American Revolution was fought
     5: agreement of opinions [syn: {harmony}, {concordance}]
     6: the first battle of the ...
