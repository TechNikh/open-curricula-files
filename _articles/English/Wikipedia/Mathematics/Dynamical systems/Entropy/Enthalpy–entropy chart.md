---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Enthalpy%E2%80%93entropy_chart'
offline_file: ""
offline_thumbnail: ""
uuid: c2021fff-58b4-40c9-b4fb-9a446f37e576
updated: 1484308762
title: Enthalpy–entropy chart
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-HS-Wasserdampf_engl.png
tags:
    - History
    - Details
    - Applications and usage
categories:
    - Entropy
---
An enthalpy–entropy chart, also known as the h–s chart or Mollier diagram, plots the total heat against entropy,[1] describing the enthalpy of a thermodynamic system.[2] A typical chart covers a pressure range of 0.01 - 1000 bar, and temperatures up to 800 degrees Celsius.[3] It shows enthalpy 
  
    
      
        h
      
    
    {\displaystyle h}
  
 in terms of internal energy 
  
    
      
        u
      
    
    {\displaystyle u}
  
, pressure 
  
    
      
        P
      
    
    {\displaystyle P}
  
 and volume 
  
    
      
        v
      
    
    {\displaystyle v}
  
 using the relationship 
  
    
      
        h
        =
        u
        +
        P
       ...
