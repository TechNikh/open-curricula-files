---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sell
offline_file: ""
offline_thumbnail: ""
uuid: 0ddd8156-dde2-404d-887e-fb0b1208207d
updated: 1484310459
title: sell
categories:
    - Dictionary
---
sell
     n : the activity of persuading someone to buy; "it was a hard
         sell"
     v 1: exchange or deliver for money or its equivalent; "He sold
          his house in January"; "She sells her body to survive
          and support her drug habit" [ant: {buy}]
     2: be sold at a certain price or in a certain way; "These books
        sell like hot cakes"
     3: do business; offer for sale as for one's livelihood; "She
        deals in gold"; "The brothers sell shoes" [syn: {deal}, {trade}]
     4: persuade somebody to accept something; "The French try to
        sell us their image as great lovers"
     5: give up for a price or reward; "She sold her principles for
        a ...
