---
title: Rice production in Vietnam
article_ref: https://en.wikipedia.org/wiki/Rice_production_in_Vietnam
tags:
    - Geographical setting
    - Legend
    - History
    - Production
    - International cooperation
    - Rice varieties
    - Cuisine
    - 2010 drought
    - Bibliography
categories:
    - Paddy cultivation
---
