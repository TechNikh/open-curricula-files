---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coca
offline_file: ""
offline_thumbnail: ""
uuid: a6cb5c6c-d83d-458b-87b3-aa102043b4ad
updated: 1484310467
title: coca
categories:
    - Dictionary
---
coca
     n 1: a South American shrub whose leaves are chewed by natives of
          the Andes; a source of cocaine [syn: {Erythroxylon coca}]
     2: United States comedienne who starred in early television
        shows with Sid Caesar (1908-2001) [syn: {Imogene Coca}]
     3: dried leaves of the coca plant (and related plants that also
        contain cocaine); chewed by Andean people for their
        simulating effect
