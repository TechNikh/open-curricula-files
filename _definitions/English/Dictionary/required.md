---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/required
offline_file: ""
offline_thumbnail: ""
uuid: 2ee0e3ce-6b81-42ca-a781-b4a5cd281abd
updated: 1484310301
title: required
categories:
    - Dictionary
---
required
     adj 1: necessary for relief or supply; "provided them with all
            things needful" [syn: {needed}, {needful}, {requisite}]
     2: required by rule; "in most schools physical education are
        compulsory"; "attendance is mandatory"; "required reading"
        [syn: {compulsory}, {mandatory}]
