---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zero_game
offline_file: ""
offline_thumbnail: ""
uuid: b6b29050-3494-4666-9bd7-fb90f5ab8d51
updated: 1484308533
title: Zero game
categories:
    - Combinatorial game theory
---
In combinatorial game theory, the zero game is the game where neither player has any legal options. Therefore, under the normal play convention, the first player automatically loses, and it is a second-player win. The zero game has a Sprague–Grundy value of zero. The combinatorial notation of the zero game is: { | }.[1]
