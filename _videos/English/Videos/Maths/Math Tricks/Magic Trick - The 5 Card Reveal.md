---
version: 1
type: video
provider: YouTube
id: TH4NqXLBD2w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ea12e248-4232-4335-81a5-b492b1e321c0
updated: 1486069622
title: 'Magic Trick - The 5 Card Reveal'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/TH4NqXLBD2w/default.jpg
    - https://i3.ytimg.com/vi/TH4NqXLBD2w/1.jpg
    - https://i3.ytimg.com/vi/TH4NqXLBD2w/2.jpg
    - https://i3.ytimg.com/vi/TH4NqXLBD2w/3.jpg
---
