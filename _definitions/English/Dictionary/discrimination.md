---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discrimination
offline_file: ""
offline_thumbnail: ""
uuid: f77fc89e-edaa-4df8-96ce-e2b446380a98
updated: 1484310441
title: discrimination
categories:
    - Dictionary
---
discrimination
     n 1: unfair treatment of a person or group on the basis of
          prejudice [syn: {favoritism}, {favouritism}]
     2: the cognitive process whereby two or more stimuli are
        distinguished [syn: {secernment}]
