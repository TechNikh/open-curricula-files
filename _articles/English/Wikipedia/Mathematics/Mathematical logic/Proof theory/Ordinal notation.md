---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ordinal_notation
offline_file: ""
offline_thumbnail: ""
uuid: 301b7a35-44fd-49f0-97a0-9f4fee10e241
updated: 1484309497
title: Ordinal notation
tags:
    - A simplified example using a pairing function
    - ξ-notation
    - Systems of ordinal notation
    - Cantor
    - Veblen
    - Ackermann
    - Bachmann
    - Takeuti (ordinal diagrams)
    - "Feferman's θ functions"
    - Buchholz
    - |
        Kleene's
        
        
        
        
        
        O
        
        
        
        
        {\displaystyle {\mathcal {O}}}
categories:
    - Proof theory
---
In mathematical logic and set theory, an ordinal notation is a partial function from the set of all finite sequences of symbols from a finite alphabet to a countable set of ordinals, and a Gödel numbering is a function from the set of well-formed formulae (a well-formed formula is a finite sequence of symbols on which the ordinal notation function is defined) of some formal language to the natural numbers. This associates each wff with a unique natural number, called its Gödel number. If a Gödel numbering is fixed, then the subset relation on the ordinals induces an ordering on well-formed formulae which in turn induces a well-ordering on the subset of natural numbers. A recursive ...
