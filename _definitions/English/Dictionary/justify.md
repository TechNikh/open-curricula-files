---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/justify
offline_file: ""
offline_thumbnail: ""
uuid: f59cd05b-f13f-40c0-b4bb-8cad9ed945d6
updated: 1484310316
title: justify
categories:
    - Dictionary
---
justify
     v 1: show to be reasonable or provide adequate ground for; "The
          emergency does not warrant all of us buying guns"; "The
          end justifies the means" [syn: {warrant}]
     2: show to be right by providing justification or proof;
        "vindicate a claim" [syn: {vindicate}]
     3: defend, explain, clear away, or make excuses for by
        reasoning; "rationalize the child's seemingly crazy
        behavior"; "he rationalized his lack of success" [syn: {apologize},
         {apologise}, {excuse}, {rationalize}, {rationalise}]
     4: let off the hook; "I absolve you from this responsibility"
        [syn: {absolve}, {free}] [ant: {blame}]
     5: adjust the ...
