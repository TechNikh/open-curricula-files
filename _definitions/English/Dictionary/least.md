---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/least
offline_file: ""
offline_thumbnail: ""
uuid: 10a4709b-12aa-4e60-a91a-6bc9d04b3771
updated: 1484310323
title: least
categories:
    - Dictionary
---
least
     adj 1: the superlative of `little' that can be used with mass nouns
            and is usually preceded by `the'; a quantifier meaning
            smallest in amount or extent or degree; "didn't care
            the least bit"; "he has the least talent of anyone"
            [syn: {least(a)}] [ant: {most(a)}]
     2: minimal in magnitude; "lowest wages"; "the least amount of
        fat allowed"; "the smallest amount" [syn: {lowest}, {smallest}]
     3: having or being distinguished by diminutive size; "the least
        bittern" [syn: {littlest}, {smallest}]
     adv : used to form the superlative; "The garter snake is the least
           dangerous snake" [syn: {to the lowest ...
