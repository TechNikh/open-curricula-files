---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exformation
offline_file: ""
offline_thumbnail: ""
uuid: 476849e7-5252-418e-bf68-cd1c3bb76117
updated: 1484308773
title: Exformation
tags:
    - Example
    - Meaning as proposed by Nørretranders
categories:
    - Entropy and information
---
Exformation (originally spelled eksformation in Danish) is a term coined by Danish science writer Tor Nørretranders in his book The User Illusion published in English 1998. It is meant to mean explicitly discarded information. However, the term also has other meanings related to information, for instance "useful and relevant information"[1] or a specific kind of information explosion.[2]
