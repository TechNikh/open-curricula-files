---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grasshopper
offline_file: ""
offline_thumbnail: ""
uuid: 6e1e9847-7fe4-4c43-a37f-c7130b26f81f
updated: 1484310271
title: grasshopper
categories:
    - Dictionary
---
grasshopper
     n 1: terrestrial plant-eating insect with hind legs adapted for
          leaping [syn: {hopper}]
     2: a cocktail made of creme de menthe and cream (sometimes with
        creme de cacao)
