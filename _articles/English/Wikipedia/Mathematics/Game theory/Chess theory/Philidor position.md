---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Philidor_position
offline_file: ""
offline_thumbnail: ""
uuid: 8b3053c9-86ae-4ad6-92b8-95cdd01f3dea
updated: 1484309017
title: Philidor position
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Andr%25C3%25A9_Philidor.jpg'
tags:
    - "Philidor's position, rook and pawn versus rook"
    - Queen versus rook
    - Rook and bishop versus rook
categories:
    - Chess theory
---
The Philidor position (or Philidor's position) usually refers to an important chess endgame which illustrates a drawing technique when the defender has a king and rook versus a king, rook, and a pawn. It is also known as the third rank defense, because of the importance of the rook on the third rank cutting off the opposing king. It was analyzed by François-André Danican Philidor in 1777. (Also see rook and pawn versus rook endgame.) Many rook and pawn versus rook endgames reach either the Philidor Position or the Lucena Position. If played accurately the defending side tries to reach the Philidor Position; the other side tries to reach the winning Lucena Position. Jesús de la Villa said ...
