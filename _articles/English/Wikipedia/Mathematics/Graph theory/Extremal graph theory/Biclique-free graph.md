---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biclique-free_graph
offline_file: ""
offline_thumbnail: ""
uuid: 76dc0b55-8f23-4b58-a7d3-78c37385eab6
updated: 1484309408
title: Biclique-free graph
tags:
    - Properties
    - Sparsity
    - Relation to other types of sparse graph family
    - Applications
    - Discrete geometry
    - Parameterized complexity
categories:
    - Extremal graph theory
---
In graph theory, a branch of mathematics, a t-biclique-free graph is a graph that has no 2t-vertex complete bipartite graph Kt,t as a subgraph. A family of graphs is biclique-free if there exists a number t such that the graphs in the family are all t-biclique-free. The biclique-free graph families form one of the most general types of sparse graph family. They arise in incidence problems in discrete geometry, and have also been used in parameterized complexity.
