---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/truly
offline_file: ""
offline_thumbnail: ""
uuid: c90e8c78-2ec7-4f92-a077-464c6fd2dd6d
updated: 1484310525
title: truly
categories:
    - Dictionary
---
truly
     adv 1: in accordance with truth or fact or reality; "she was now
            truly American"; "a genuinely open society"; "they
            don't really listen to us" [syn: {genuinely}, {really}]
     2: by right; "baseball rightfully is the nation's pastime"
        [syn: {rightfully}]
     3: with sincerity; without pretense; "she praised him sincerely
        for his victory"; "was unfeignedly glad to see his old
        teacher"; "we are truly sorry for the inconvenience" [syn:
         {sincerely}, {unfeignedly}] [ant: {insincerely}]
     4: in fact (used as intensifiers or sentence modifiers); "in
        truth, moral decay hastened the decline of the Roman
        Empire"; ...
