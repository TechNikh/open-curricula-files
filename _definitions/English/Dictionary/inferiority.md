---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inferiority
offline_file: ""
offline_thumbnail: ""
uuid: a3afacfb-0a60-4e40-8571-609844d07f0b
updated: 1484310152
title: inferiority
categories:
    - Dictionary
---
inferiority
     n 1: the state of being inferior [syn: {lower status}, {lower
          rank}]
     2: an inferior quality [syn: {low quality}] [ant: {superiority},
         {superiority}]
     3: the quality of being a competitive disadvantage [syn: {unfavorable
        position}]
