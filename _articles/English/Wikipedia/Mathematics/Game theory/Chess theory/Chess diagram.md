---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chess_diagram
offline_file: ""
offline_thumbnail: ""
uuid: c274d991-8db4-466b-9554-51dd259d8c78
updated: 1484309017
title: Chess diagram
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Nuvola_apps_package_games_strategy_0.png
categories:
    - Chess theory
---
A chess diagram is graphic representation and stylized of a specific moment in a chess game, showing the different positions occupied by chess pieces in given time during game development.
