---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Y-%CE%94_transform'
offline_file: ""
offline_thumbnail: ""
uuid: 07968ba3-e45f-44bb-bb4d-285fac4dda40
updated: 1484309447
title: Y-Δ transform
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Theoreme_de_kennelly2.svg.png
tags:
    - Names
    - Basic Y-Δ transformation
    - Equations for the transformation from Δ to Y
    - Equations for the transformation from Y to Δ
    - >
        A proof of the existence and uniqueness of the
        transformation
    - Simplification of networks
    - Graph theory
    - Demonstration
    - Δ-load to Y-load transformation equations
    - Y-load to Δ-load transformation equations
    - Notes
categories:
    - Graph operations
---
The Y-Δ transform, also written wye-delta and also known by many other names, is a mathematical technique to simplify the analysis of an electrical network. The name derives from the shapes of the circuit diagrams, which look respectively like the letter Y and the Greek capital letter Δ. This circuit transformation theory was published by Arthur Edwin Kennelly in 1899.[1] It is widely used in analysis of three-phase electric power circuits.
