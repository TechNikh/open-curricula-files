---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Koszul_cohomology
offline_file: ""
offline_thumbnail: ""
uuid: 3ccb523d-0a30-431f-b256-ac8258146e77
updated: 1484309156
title: Koszul cohomology
categories:
    - Algebraic geometry
---
In mathematics, the Koszul cohomology groups Kp,q(X, L) are groups associated to a projective variety X with a line bundle L. They were introduced by Green (1984, 1984b), and named after Jean-Louis Koszul as they are closely related to the Koszul complex.
