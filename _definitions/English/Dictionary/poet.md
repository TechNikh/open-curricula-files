---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poet
offline_file: ""
offline_thumbnail: ""
uuid: 41d1f4e1-2c03-4ab8-9d74-48f7ec4ae3cf
updated: 1484310585
title: poet
categories:
    - Dictionary
---
poet
     n : a writer of poems (the term is usually reserved for writers
         of good poetry)
