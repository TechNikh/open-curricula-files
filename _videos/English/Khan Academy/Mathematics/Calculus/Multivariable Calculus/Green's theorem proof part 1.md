---
version: 1
type: video
provider: YouTube
id: l5zJvZKfMYE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Green%27s%20theorem%20proof%20part%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a1d3b711-5fff-4dcc-b85f-c1ca6a91ab23
updated: 1486069696
title: "Green's theorem proof part 1"
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/l5zJvZKfMYE/default.jpg
    - https://i3.ytimg.com/vi/l5zJvZKfMYE/1.jpg
    - https://i3.ytimg.com/vi/l5zJvZKfMYE/2.jpg
    - https://i3.ytimg.com/vi/l5zJvZKfMYE/3.jpg
---
