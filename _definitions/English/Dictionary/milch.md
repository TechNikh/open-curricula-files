---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/milch
offline_file: ""
offline_thumbnail: ""
uuid: 5053f445-9a42-4d66-802e-15609ee41038
updated: 1484310464
title: milch
categories:
    - Dictionary
---
milch
     adj : giving milk; bred or suitable primarily for milk production;
           "milch goats, milch camels"
