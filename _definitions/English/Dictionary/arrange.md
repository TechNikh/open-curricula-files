---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arrange
offline_file: ""
offline_thumbnail: ""
uuid: 2a17cf4f-1043-464d-9047-b58e8224c92c
updated: 1484310299
title: arrange
categories:
    - Dictionary
---
arrange
     v 1: put into a proper or systematic order; "arrange the books on
          the shelves in chronological order" [syn: {set up}]
          [ant: {disarrange}]
     2: make arrangements for; "Can you arrange a meeting with the
        President?" [syn: {fix up}]
     3: plan, organize, and carry out (an event) [syn: {stage}, {bring
        about}]
     4: set (printed matter) into a specific format; "Format this
        letter so it can be printed out" [syn: {format}]
     5: arrange attractively; "dress my hair for the wedding" [syn:
        {dress}, {set}, {do}, {coif}, {coiffe}, {coiffure}]
     6: adapt for performance in a different way; "set this poem to
        music" ...
