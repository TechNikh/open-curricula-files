---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Preissman%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 84f6ed69-ccf2-4d30-a9df-506b0908ee77
updated: 1484309369
title: "Preissman's theorem"
categories:
    - Geometry stubs
---
In Riemannian geometry, a field of mathematics, Preissman's theorem is a statement that restricts the possible topology of a negatively curved compact Riemannian manifold M. Specifically, the theorem states that every non-trivial abelian subgroup of the fundamental group of M must be isomorphic to the additive group of integers, Z.[1][2]
