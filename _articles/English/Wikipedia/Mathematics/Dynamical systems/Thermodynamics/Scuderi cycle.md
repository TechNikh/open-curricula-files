---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Scuderi_cycle
offline_file: ""
offline_thumbnail: ""
uuid: 8671ec2c-e462-4f04-ac20-6515e1309738
updated: 1484308859
title: Scuderi cycle
categories:
    - Thermodynamics
---
The adiabatic processes are impermeable to heat: heat flows rapidly into the loop through the left expanding process resulting in increasing pressure while volume is increasing; some of it flows back out through the right depressurizing process; the heat that remains does the work.
