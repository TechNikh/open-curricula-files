---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vasopressin
offline_file: ""
offline_thumbnail: ""
uuid: f565938d-e490-4acb-a1d6-67e8fb1353a8
updated: 1484310158
title: vasopressin
categories:
    - Dictionary
---
vasopressin
     n : hormone secreted by the posterior pituitary gland (trade
         name Pitressin) and also by nerve endings in the
         hypothalamus; affects blood pressure by stimulating
         capillary muscles and reduces urine flow by affecting
         reabsorption of water by kidney tubules [syn: {antidiuretic
         hormone}, {ADH}, {Pitressin}]
