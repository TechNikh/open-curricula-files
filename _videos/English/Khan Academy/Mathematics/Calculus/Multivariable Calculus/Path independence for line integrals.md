---
version: 1
type: video
provider: YouTube
id: K_fgnCJOI8I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Path%20independence%20for%20line%20integrals.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0284d79c-5288-4a45-924a-1ebd0e96e916
updated: 1486069696
title: Path independence for line integrals
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/K_fgnCJOI8I/default.jpg
    - https://i3.ytimg.com/vi/K_fgnCJOI8I/1.jpg
    - https://i3.ytimg.com/vi/K_fgnCJOI8I/2.jpg
    - https://i3.ytimg.com/vi/K_fgnCJOI8I/3.jpg
---
