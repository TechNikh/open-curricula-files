---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expiry
offline_file: ""
offline_thumbnail: ""
uuid: a8519173-5d57-4787-8dcb-4af6f9b11685
updated: 1484310517
title: expiry
categories:
    - Dictionary
---
expiry
     n : a coming to an end of a contract period; "the expiry of his
         driver's license" [syn: {termination}, {expiration}]
