---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biosphere
offline_file: ""
offline_thumbnail: ""
uuid: 438622fc-447d-4ae8-81f6-e9521efdac45
updated: 1484310273
title: biosphere
categories:
    - Dictionary
---
biosphere
     n : the regions of the surface and atmosphere of the Earth (or
         other planet) where living organisms exist
