---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Regiomontanus%27_angle_maximization_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 15532a86-b97d-4d06-9358-772f1642f230
updated: 1484309348
title: "Regiomontanus' angle maximization problem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/550px-Regiomontanus.problem.png
tags:
    - Solution by elementary geometry
    - Solution by calculus
    - Solution by algebra
    - Notes
categories:
    - Trigonometry
---
In mathematics, the Regiomontanus' angle maximization problem, is a famous optimization problem[1] posed by the 15th-century German mathematician Johannes Müller[2] (also known as Regiomontanus). The problem is as follows:
