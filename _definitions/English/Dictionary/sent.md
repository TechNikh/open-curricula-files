---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sent
offline_file: ""
offline_thumbnail: ""
uuid: c111474c-959f-472c-9206-a0e33fe215ad
updated: 1484310284
title: sent
categories:
    - Dictionary
---
sent
     adj : caused or enabled to go or be conveyed or transmitted [ant:
           {unsent}]
     n : 100 senti equal 1 kroon
     [also: {senti} (pl)]
