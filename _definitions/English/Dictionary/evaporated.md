---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evaporated
offline_file: ""
offline_thumbnail: ""
uuid: 738178b3-5dff-422c-b560-b81e6d463c11
updated: 1484310384
title: evaporated
categories:
    - Dictionary
---
evaporated
     adj : drawn off in the form of vapor; "evaporated molecules boil
           off"
