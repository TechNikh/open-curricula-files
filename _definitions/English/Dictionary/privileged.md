---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/privileged
offline_file: ""
offline_thumbnail: ""
uuid: 219d5bd7-cb72-45d7-93cf-5544790dabdf
updated: 1484310457
title: privileged
categories:
    - Dictionary
---
privileged
     adj 1: blessed with privileges; "the privileged few" [ant: {underprivileged}]
     2: not subject to usual rules or penalties; "a privileged
        statement"
     3: confined to an exclusive group; "privy to inner knowledge";
        "inside information"; "privileged information" [syn: {inside},
         {inner}]
