---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hot
offline_file: ""
offline_thumbnail: ""
uuid: 6f0eda32-3848-4cb9-b6a9-78e78b86545c
updated: 1484310341
title: hot
categories:
    - Dictionary
---
hot
     adj 1: used of physical heat; having a high or higher than
            desirable temperature or giving off heat or feeling or
            causing a sensation of heat or burning; "hot stove";
            "hot water"; "a hot August day"; "a hot stuffy room";
            "she's hot and tired"; "a hot forehead" [ant: {cold}]
     2: characterized by violent and forceful activity or movement;
        very intense; "the fighting became hot and heavy"; "a hot
        engagement"; "a raging battle"; "the river became a raging
        torrent" [syn: {raging}]
     3: extended meanings; especially of psychological heat; marked
        by intensity or vehemence especially of passion or
       ...
