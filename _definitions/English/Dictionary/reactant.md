---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reactant
offline_file: ""
offline_thumbnail: ""
uuid: 1b780d87-6111-4ce0-bdbe-db761f6445fb
updated: 1484310371
title: reactant
categories:
    - Dictionary
---
reactant
     n : a chemical substance that is present at the start of a
         chemical reaction
