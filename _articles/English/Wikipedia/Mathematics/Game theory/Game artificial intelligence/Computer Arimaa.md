---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computer_Arimaa
offline_file: ""
offline_thumbnail: ""
uuid: 7f080937-76e6-4e01-85a0-3febf53a1e3a
updated: 1484309004
title: Computer Arimaa
tags:
    - State space of Arimaa
    - Opening
    - Artificial intelligence techniques
    - Material evaluation
    - Techniques used in Arimaa bots
    - Techniques rarely used In Arimaa bots
    - Computer performance
    - Brute-force searching
    - Alpha-beta pruning
    - Comparing the Arimaa Challenge to chess challenges
    - Computer competition in Arimaa
    - Arimaa Computer Championship
    - Arimaa Challenge
    - Resources for software developers
    - Research papers
    - Footnotes
categories:
    - Game artificial intelligence
---
In 2002, Indian American computer engineer Omar Syed published the rules to Arimaa and announced a $10,000 prize, available annually until 2020, for the first computer program (running on standard, off-the-shelf hardware) able to defeat each of three top-ranked human players in a three-game series.[1] The prize was claimed in 2015, when a computer program played 7:2 against three human players.[2] The game has been the subject of several research papers.
