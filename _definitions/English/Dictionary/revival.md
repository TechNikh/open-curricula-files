---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revival
offline_file: ""
offline_thumbnail: ""
uuid: 250b805c-72fe-4dfd-bc22-c65f185fff9e
updated: 1484310567
title: revival
categories:
    - Dictionary
---
revival
     n 1: bringing again into activity and prominence; "the revival of
          trade"; "a revival of a neglected play by Moliere"; "the
          Gothic revival in architecture" [syn: {resurgence}, {revitalization},
           {revitalisation}, {revivification}]
     2: an evangelistic meeting intended to reawaken interest in
        religion [syn: {revival meeting}]
