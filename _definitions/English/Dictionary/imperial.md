---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imperial
offline_file: ""
offline_thumbnail: ""
uuid: 257ab067-1b1c-4558-9ca4-95275194bf5c
updated: 1484310577
title: imperial
categories:
    - Dictionary
---
imperial
     adj 1: relating to or associated with an empire; "imperial colony";
            "the imperial gallon was standardized legally
            throughout the British Empire"
     2: befitting or belonging to an emperor or empress; "imperial
        palace"
     3: belonging to or befitting a supreme ruler; "golden age of
        imperial splendor"; "purple tyrant"; "regal attire";
        "treated with royal acclaim"; "the royal carriage of a
        stag's head" [syn: {majestic}, {purple}, {regal}, {royal}]
     n 1: a small tufted beard worn by Emperor Napoleon III [syn: {imperial
          beard}]
     2: a piece of luggage carried on top of a coach
