---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Dobi%C5%84ski%27s_formula'
offline_file: ""
offline_thumbnail: ""
uuid: c8a1a5de-5a5d-4a06-b396-62c4c74bf205
updated: 1484308577
title: "Dobiński's formula"
tags:
    - Probabilistic content
    - Generalization
    - Proof
    - Notes and references
categories:
    - Combinatorics
---
The formula is named after G. Dobiński, who published it in 1877.
