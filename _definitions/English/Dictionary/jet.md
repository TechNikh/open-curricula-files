---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jet
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484381221
title: jet
categories:
    - Dictionary
---
jet
     adj : of the blackest black; similar to the color of jet or coal
           [syn: {coal-black}, {jet-black}, {pitchy}, {sooty}]
     n 1: an airplane powered by one or more jet engines [syn: {jet
          plane}, {jet-propelled plane}]
     2: the occurrence of a sudden discharge (as of liquid) [syn: {squirt},
         {spurt}, {spirt}]
     3: a hard black form of lignite that takes a brilliant polish
        and is used in jewellery or ornamentation
     4: street names for ketamine [syn: {K}, {super acid}, {special
        K}, {honey oil}, {green}, {cat valium}, {super C}]
     5: an artificially produced flow of water [syn: {fountain}]
     v 1: issue in a jet; come out in a ...
