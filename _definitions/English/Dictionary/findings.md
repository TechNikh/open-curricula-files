---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/findings
offline_file: ""
offline_thumbnail: ""
uuid: bb27054d-d412-4211-9606-6d64b7ab67e6
updated: 1484310539
title: findings
categories:
    - Dictionary
---
findings
     n : a collection of tools and other articles used by an artisan
         to make jewelry or clothing or shoes
