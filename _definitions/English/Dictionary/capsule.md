---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/capsule
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484365321
title: capsule
categories:
    - Dictionary
---
capsule
     n 1: a small container
     2: a pill in the form of a small rounded gelatinous container
        with medicine inside
     3: a dry dehiscent seed vessel or the spore-containing
        structure of e.g. mosses
     4: a shortened version of a written work [syn: {condensation},
        {abridgement}, {abridgment}]
     5: a structure that encloses a body part
     6: a spacecraft designed to transport people and support human
        life in outer space [syn: {space capsule}]
     7: a pilot's seat in an airplane that can be forcibly ejected
        in the case of an emergency; then the pilot descends by
        parachute [syn: {ejection seat}, {ejector seat}]
     v 1: ...
