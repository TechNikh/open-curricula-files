---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meagre
offline_file: ""
offline_thumbnail: ""
uuid: 225381f8-0b36-43b6-9ffd-580c7b3cbec7
updated: 1484310258
title: meagre
categories:
    - Dictionary
---
meagre
     adj : deficient in amount or quality or extent; "meager
           resources"; "meager fare" [syn: {meager}, {meagerly}]
           [ant: {ample}]
