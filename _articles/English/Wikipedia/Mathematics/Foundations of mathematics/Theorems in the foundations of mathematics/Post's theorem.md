---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Post%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: ba1fc6ba-86db-4931-8b0d-e9a9e8087618
updated: 1484308994
title: "Post's theorem"
tags:
    - Background
    - "Post's theorem and corollaries"
    - "Proof of Post's theorem"
    - Formalization of Turing machines in first-order arithmetic
    - Implementation example
    - Recursively enumerable sets
    - Oracle machines
    - Turing jump
    - Higher Turing jumps
categories:
    - Theorems in the foundations of mathematics
---
