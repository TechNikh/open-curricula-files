---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Radon_transform
offline_file: ""
offline_thumbnail: ""
uuid: 870ab962-3f19-40f0-811c-d3fe680c141b
updated: 1484309257
title: Radon transform
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Radon_transform.png
tags:
    - Explanation
    - Definition
    - Relationship with the Fourier transform
    - Dual transform
    - Intertwining property
    - Reconstruction approaches
    - Radon inversion formula
    - Ill-posedness
    - Iterative Reconstruction methods
    - Notes
categories:
    - Integral geometry
---
In mathematics, the Radon transform is the integral transform which takes a function f defined on the plane to a function Rf defined on the (two-dimensional) space of lines in the plane, whose value at a particular line is equal to the line integral of the function over that line. The transform was introduced in 1917 by Johann Radon,[1] who also provided a formula for the inverse transform. Radon further included formulas for the transform in three dimensions, in which the integral is taken over planes. It was later generalised to higher-dimensional Euclidean spaces, and more broadly in the context of integral geometry. The complex analog of the Radon transform is known as the Penrose ...
