---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/believe
offline_file: ""
offline_thumbnail: ""
uuid: 31192f78-77b5-4b2d-8c61-c2699e2be020
updated: 1484310325
title: believe
categories:
    - Dictionary
---
believe
     v 1: accept as true; take to be true; "I believed his report";
          "We didn't believe his stories from the War"; "She
          believes in spirits" [ant: {disbelieve}]
     2: judge or regard; look upon; judge; "I think he is very
        smart"; "I believe her to be very smart"; "I think that he
        is her boyfriend"; "The racist conceives such people to be
        inferior" [syn: {think}, {consider}, {conceive}]
     3: be confident about something; "I believe that he will come
        back from the war" [syn: {trust}]
     4: follow a credo; have a faith; be a believer; "When you hear
        his sermons, you will be able to believe, too"
     5: credit with ...
