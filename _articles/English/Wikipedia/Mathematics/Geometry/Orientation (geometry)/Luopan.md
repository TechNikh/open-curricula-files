---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Luopan
offline_file: ""
offline_thumbnail: ""
uuid: d2d1f063-19e8-4268-bfac-6429c1c11643
updated: 1484309297
title: Luopan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Luopan.jpg
tags:
    - Form and function
    - Types
    - San He
    - San Yuan
    - Zong He
    - Other types
    - History and development
categories:
    - Orientation (geometry)
---
Luopan is a Chinese magnetic compass, also known as a Feng Shui compass. It is used by a Feng Shui practitioner to determine the precise direction of a structure or other item. Since the invention of the compass for use in Feng Shui,[1] traditional feng shui has required its use.
