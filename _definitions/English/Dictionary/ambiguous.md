---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ambiguous
offline_file: ""
offline_thumbnail: ""
uuid: 1f2c057e-c852-4467-a2d2-12d97815b60e
updated: 1484310585
title: ambiguous
categories:
    - Dictionary
---
ambiguous
     adj 1: open to two or more interpretations; or of uncertain nature
            or significance; or (often) intended to mislead; "an
            equivocal statement"; "the polling had a complex and
            equivocal (or ambiguous) message for potential female
            candidates"; "the officer's equivocal behavior
            increased the victim's uneasiness"; "popularity is an
            equivocal crown"; "an equivocal response to an
            embarrassing question" [syn: {equivocal}] [ant: {unequivocal}]
     2: having more than one possible meaning; "ambiguous words";
        "frustrated by ambiguous instructions, the parents were
        unable to assemble the ...
