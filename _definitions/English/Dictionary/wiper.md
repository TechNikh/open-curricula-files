---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wiper
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484359981
title: wiper
categories:
    - Dictionary
---
wiper
     n 1: a worker who wipes
     2: contact consisting of a conducting arm that rotates over a
        series of fixed contacts and comes to rest on an outlet
        [syn: {wiper arm}, {contact arm}]
     3: a mechanical device that cleans the windshield [syn: {windshield
        wiper}, {windscreen wiper}, {wiper blade}]
