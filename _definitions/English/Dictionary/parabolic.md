---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parabolic
offline_file: ""
offline_thumbnail: ""
uuid: 75495c8f-e509-47d8-a5e0-b3ad77ce5d0b
updated: 1484310216
title: parabolic
categories:
    - Dictionary
---
parabolic
     adj 1: resembling or expressed by parables [syn: {parabolical}]
     2: having the form of a parabola [syn: {parabolical}]
