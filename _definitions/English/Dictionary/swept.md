---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swept
offline_file: ""
offline_thumbnail: ""
uuid: 9f4978d1-688d-4f7a-a4ed-680358d750c4
updated: 1484310589
title: swept
categories:
    - Dictionary
---
sweep
     n 1: a wide scope; "the sweep of the plains" [syn: {expanse}]
     2: someone who cleans soot from chimneys [syn: {chimneysweeper},
         {chimneysweep}]
     3: winning all or all but one of the tricks in bridge [syn: {slam}]
     4: a long oar used in an open boat [syn: {sweep oar}]
     5: (American football) an attempt to advance the ball by
        running around the end of the line [syn: {end run}]
     6: a movement in an arc; "a sweep of his arm"
     v 1: sweep across or over; "Her long skirt brushed the floor"; "A
          gasp swept cross the audience" [syn: {brush}]
     2: move with sweeping, effortless, gliding motions; "The diva
        swept into the room"; ...
