---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Auctionata
offline_file: ""
offline_thumbnail: ""
uuid: 44531b55-7546-4e39-85fc-b85c564776b1
updated: 1484309011
title: Auctionata
tags:
    - History
    - Technology of live stream auctions
    - Notable auctions
    - Online auction world record
    - Notable mergers and acquisitions
    - Awards
categories:
    - Auctions
---
Auctionata is an online auction house and eCommerce company specializing in luxury goods, art, antiques and collectibles. Based in Berlin, Auctionata employes 250 experts who value, authenticate, and curate art objects.[2] Since opening in February 2012, Auctionata has evaluated and verified over 21,000 pieces of art.[3] Notably, Auctionata's experts discovered a watercolor by Egon Schiele, which was valued at a starting price of $1.3 million (€1 million).[4]
