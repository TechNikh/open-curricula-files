---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pie_supper
offline_file: ""
offline_thumbnail: ""
uuid: cc09a8b2-beb1-4a13-8748-ed559941ec21
updated: 1484309013
title: Pie supper
categories:
    - Auctions
---
A pie supper is a social gathering where pies are auctioned to raise money, often for a local school or fire department. Pie suppers are predominantly associated with Ozark culture, and provided a major source of funding for many of the region's one-room schools in the late 19th and early 20th centuries.
