---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tactical
offline_file: ""
offline_thumbnail: ""
uuid: 94c98f93-9531-49c2-bd10-ee461cea34f9
updated: 1484310480
title: tactical
categories:
    - Dictionary
---
tactical
     adj : of or pertaining to tactic or tactics; "a tactical error"
