---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anchor
offline_file: ""
offline_thumbnail: ""
uuid: 7a8fdedf-10ae-4985-b5a8-a4e8f469ad00
updated: 1484310244
title: anchor
categories:
    - Dictionary
---
anchor
     n 1: a mechanical device that prevents a vessel from moving [syn:
           {ground tackle}]
     2: a central cohesive source of support and stability; "faith
        is his anchor"; "the keystone of campaign reform was the
        ban on soft money"; "he is the linchpin of this firm"
        [syn: {mainstay}, {keystone}, {backbone}, {linchpin}, {lynchpin}]
     3: a television reporter who coordinates a broadcast to which
        several correspondents contribute [syn: {anchorman}, {anchorperson}]
     v 1: fix firmly and stably; "anchor the lamppost in concrete"
          [syn: {ground}]
     2: secure a vessel with an anchor; "We anchored at Baltimore"
        [syn: {cast ...
