---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bulandshahr
offline_file: ""
offline_thumbnail: ""
uuid: 01728e99-4d5d-42c6-8324-5b16c86b25e9
updated: 1484308458
title: Bulandshahr
tags:
    - History
    - Demographics
    - Notable people
categories:
    - Uncategorized
---
Bulandshahr is a city and a municipal board in Bulandshahr district in the state of Uttar Pradesh, India. It is the administrative headquarters of Bulandshahr District. This city is part of the Delhi National Capital Region (NCR). According to Government of India, the district Bulandshahr is one of the Hindu Concentrated District in India on the basis of the 2011 census data on population, socio-economic indicators and basic amenities indicators.[1] The distance between Bulandshahr and New Delhi is 75 km and it takes 1 hour 26 mins to reach there.[2]
