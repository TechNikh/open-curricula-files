---
version: 1
type: video
provider: YouTube
id: E8GNde5nCSg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ffecac04-fb78-451c-9051-28055cf2a08d
updated: 1486069685
title: 'Saturn- Crash Course Astronomy #18'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/E8GNde5nCSg/default.jpg
    - https://i3.ytimg.com/vi/E8GNde5nCSg/1.jpg
    - https://i3.ytimg.com/vi/E8GNde5nCSg/2.jpg
    - https://i3.ytimg.com/vi/E8GNde5nCSg/3.jpg
---
