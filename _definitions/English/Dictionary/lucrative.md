---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lucrative
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484630461
title: lucrative
categories:
    - Dictionary
---
lucrative
     adj : producing a good profit; "a remunerative business" [syn: {moneymaking},
            {remunerative}]
