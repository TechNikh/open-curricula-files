---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mourning
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484449141
title: mourning
categories:
    - Dictionary
---
mourning
     adj : sorrowful through loss or deprivation; "bereft of hope"
           [syn: {bereaved}, {bereft}, {grief-stricken}, {grieving},
            {mourning(a)}, {sorrowing(a)}]
     n 1: state of sorrow over the death or departure of a loved one
          [syn: {bereavement}]
     2: the passionate and demonstrative activity of expressing
        grief [syn: {lamentation}]
