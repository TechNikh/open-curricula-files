---
version: 1
type: video
provider: YouTube
id: tCrDyJsSFok
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Area%20of%20a%20circle.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e7890f8b-6f30-4779-b220-123ee4783f22
updated: 1486069577
title: Area of a circle
tags:
    - Circle
    - area
    - pi
    - CC_7_G_4
categories:
    - Geometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/tCrDyJsSFok/default.jpg
    - https://i3.ytimg.com/vi/tCrDyJsSFok/1.jpg
    - https://i3.ytimg.com/vi/tCrDyJsSFok/2.jpg
    - https://i3.ytimg.com/vi/tCrDyJsSFok/3.jpg
---
Area of a circle and how it relates to radius and diameter
