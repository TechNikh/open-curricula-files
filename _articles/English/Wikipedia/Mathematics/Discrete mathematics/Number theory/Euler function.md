---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euler_function
offline_file: ""
offline_thumbnail: ""
uuid: c27cbd06-7be5-4454-a0c9-88de4ddccbac
updated: 1484308641
title: Euler function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Q-Eulero.jpeg
categories:
    - Number theory
---
Named after Leonhard Euler, it is a model example of a q-series, a modular form, and provides the prototypical example of a relation between combinatorics and complex analysis.
