---
version: 1
type: article
id: https://en.wikipedia.org/wiki/OODA_loop
offline_file: ""
offline_thumbnail: ""
uuid: b5f5e1d2-914d-46e3-ad13-5f073ffacd6d
updated: 1484309065
title: OODA loop
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/500px-OODA.Boyd.svg.png
tags:
    - Overview
    - Applicability
    - Notes
categories:
    - Strategy
---
The phrase OODA loop refers to the decision cycle of observe, orient, decide, and act, developed by military strategist and United States Air Force Colonel John Boyd. Boyd applied the concept to the combat operations process, often at the strategic level in military operations. It is now also often applied to understand commercial operations and learning processes. The approach favors agility over raw power in dealing with human opponents in any endeavor.
