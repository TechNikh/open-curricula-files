---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Berkovich_space
offline_file: ""
offline_thumbnail: ""
uuid: 8f88df21-d69a-4493-a849-af34d5697ba1
updated: 1484309134
title: Berkovich space
tags:
    - Berkovich spectrum
    - Examples
categories:
    - Algebraic geometry
---
In mathematics, a Berkovich space, introduced by Berkovich (1990), is an analogue of an analytic space for p-adic geometry, refining Tate's notion of a rigid analytic space.
