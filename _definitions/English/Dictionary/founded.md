---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/founded
offline_file: ""
offline_thumbnail: ""
uuid: 9492503c-aa19-4778-af7a-4d494bc2361e
updated: 1484310236
title: founded
categories:
    - Dictionary
---
founded
     adj : having a basis; often used as combining terms; "a soundly
           based argument"; "well-founded suspicions" [syn: {based}]
