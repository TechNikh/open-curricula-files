---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acquisition
offline_file: ""
offline_thumbnail: ""
uuid: c2fe3283-e9af-4fb3-9ba1-44dd88c0c768
updated: 1484310162
title: acquisition
categories:
    - Dictionary
---
acquisition
     n 1: the act of contracting or assuming or acquiring possession
          of something; "the acquisition of wealth"; "the
          acquisition of one company by another"
     2: something acquired; "a recent acquisition by the museum"
     3: the cognitive process of acquiring skill or knowledge; "the
        child's acquisition of language" [syn: {learning}]
     4: an ability that has been acquired by training [syn: {skill},
         {accomplishment}, {acquirement}, {attainment}]
