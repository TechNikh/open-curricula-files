---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bott%E2%80%93Samelson_resolution'
offline_file: ""
offline_thumbnail: ""
uuid: 99ae06c2-9cb9-4224-b67b-723821345a4e
updated: 1484309134
title: Bott–Samelson resolution
categories:
    - Algebraic geometry
---
In algebraic geometry, the Bott–Samelson resolution of a Schubert variety is a resolution of singularities. It was introduced by (Bott–Samelson 1959) in the context of compact Lie groups.[1] The algebraic formulation is due to (Hansen 1973) and (Demazure 1974).
