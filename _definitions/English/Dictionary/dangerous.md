---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dangerous
offline_file: ""
offline_thumbnail: ""
uuid: 3a851960-e5e8-40af-823c-23266e778db5
updated: 1484310198
title: dangerous
categories:
    - Dictionary
---
dangerous
     adj 1: involving or causing danger or risk; liable to hurt or harm;
            "a dangerous criminal"; "a dangerous bridge";
            "unemployment reached dangerous proportions" [syn: {unsafe}]
            [ant: {safe}]
     2: causing fear or anxiety by threatening great harm; "a
        dangerous operation"; "a grave situation"; "a grave
        illness"; "grievous bodily harm"; "a serious wound"; "a
        serious turn of events"; "a severe case of pneumonia"; "a
        life-threatening disease" [syn: {grave}, {grievous}, {serious},
         {severe}, {life-threatening}]
