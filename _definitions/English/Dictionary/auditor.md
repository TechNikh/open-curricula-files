---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/auditor
offline_file: ""
offline_thumbnail: ""
uuid: badec590-ab75-44d1-82ac-b0493caff520
updated: 1484310611
title: auditor
categories:
    - Dictionary
---
auditor
     n 1: someone who listens attentively [syn: {hearer}, {listener},
          {attender}]
     2: a student who attends a course but does not take it for
        credit
     3: a qualified accountant who inspects the accounting records
        and practices of a business or other organization
