---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blunt
offline_file: ""
offline_thumbnail: ""
uuid: 4bd6752f-76bd-4fcd-ada8-34b98f7f89b6
updated: 1484310341
title: blunt
categories:
    - Dictionary
---
blunt
     adj 1: having a broad or rounded end; "thick marks made by a blunt
            pencil"
     2: used of a knife or other blade; not sharp; "a blunt
        instrument"
     3: characterized by directness in manner or speech; without
        subtlety or evasion; "blunt talking and straight
        shooting"; "a blunt New England farmer"; "I gave them my
        candid opinion"; "forthright criticism"; "a forthright
        approach to the problem"; "tell me what you think--and you
        may just as well be frank"; "it is possible to be
        outspoken without being rude"; "plainspoken and to the
        point"; "a point-blank accusation" [syn: {candid}, {forthright},
         ...
