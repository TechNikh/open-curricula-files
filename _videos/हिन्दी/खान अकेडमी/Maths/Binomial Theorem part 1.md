---
version: 1
type: video
provider: YouTube
id: g0ilZEf1N6A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: be47959f-1528-47c1-82a0-215d7afd2004
updated: 1486069616
title: Binomial Theorem part 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/g0ilZEf1N6A/default.jpg
    - https://i3.ytimg.com/vi/g0ilZEf1N6A/1.jpg
    - https://i3.ytimg.com/vi/g0ilZEf1N6A/2.jpg
    - https://i3.ytimg.com/vi/g0ilZEf1N6A/3.jpg
---
