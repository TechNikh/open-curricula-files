---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dissimilar
offline_file: ""
offline_thumbnail: ""
uuid: a18304b7-c34d-41bc-af7c-4012efa9392c
updated: 1484310397
title: dissimilar
categories:
    - Dictionary
---
dissimilar
     adj 1: not similar; "a group of very dissimilar people"; "a pump
            not dissimilar to those once found on every farm";
            "their understanding of the world is not so dissimilar
            from our own"; "took different (or dissimilar)
            approaches to the problem" [ant: {similar}]
     2: not alike or similar; "as unalike as two people could be"
        [syn: {unalike}] [ant: {alike(p)}]
     3: not like; marked by dissimilarity; "for twins they are very
        unlike"; "people are profoundly different" [syn: {unlike},
         {different}] [ant: {like}]
