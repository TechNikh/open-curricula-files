---
version: 1
type: video
provider: YouTube
id: pDEawSfl2gQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 57551903-8ada-4061-b6fc-643240e400ca
updated: 1486069643
title: 'Animated Fractals 3D HD - Aexion Fractal (Mandelbulber)'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/pDEawSfl2gQ/default.jpg
    - https://i3.ytimg.com/vi/pDEawSfl2gQ/1.jpg
    - https://i3.ytimg.com/vi/pDEawSfl2gQ/2.jpg
    - https://i3.ytimg.com/vi/pDEawSfl2gQ/3.jpg
---
