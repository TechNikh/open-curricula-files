---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/localised
offline_file: ""
offline_thumbnail: ""
uuid: 6eb2578a-241a-48f6-b45f-89d533f5b3de
updated: 1484310480
title: localised
categories:
    - Dictionary
---
localised
     adj 1: confined or restricted to a particular location; "the
            localized infection formed a definite abscess" [syn: {localized}]
     2: made local or oriented locally; "a decentralized and
        localized political authority" [syn: {localized}]
