---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tide
offline_file: ""
offline_thumbnail: ""
uuid: 509738e0-5509-49c6-a052-03c43ee392b5
updated: 1484310539
title: tide
categories:
    - Dictionary
---
tide
     n 1: the periodic rise and fall of the sea level under the
          gravitational pull of the moon
     2: something that may increase or decrease (like the tides of
        the sea); "a rising tide of popular interest"
     3: there are usually two high and two low tides each day [syn:
        {lunar time period}]
     v 1: rise or move foward; "surging waves" [syn: {surge}] [ant: {ebb}]
     2: cause to float with the tide
     3: be carried with the tide
