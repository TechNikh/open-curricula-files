---
version: 1
type: video
provider: YouTube
id: PEgm5NFXlFM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Tricks/Where%20Do%20You%20Get%20Liquid%20Nitrogen.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d341ef51-2d33-447e-b403-09c7e4aa6dee
updated: 1486069661
title: Where Do You Get Liquid Nitrogen?
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/PEgm5NFXlFM/default.jpg
    - https://i3.ytimg.com/vi/PEgm5NFXlFM/1.jpg
    - https://i3.ytimg.com/vi/PEgm5NFXlFM/2.jpg
    - https://i3.ytimg.com/vi/PEgm5NFXlFM/3.jpg
---
