---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/editing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484317441
title: editing
categories:
    - Dictionary
---
editing
     n : putting something (as a literary work or a legislative bill)
         into acceptable form [syn: {redaction}]
