---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minimize
offline_file: ""
offline_thumbnail: ""
uuid: 07591bb8-482d-41e4-bcb7-da25ffe1a1a2
updated: 1484310214
title: minimize
categories:
    - Dictionary
---
minimize
     v 1: make small or insignificant; "Let's minimize the risk" [syn:
           {minimise}] [ant: {maximize}, {maximize}]
     2: represent as less significant or important [syn: {understate},
         {minimise}, {downplay}] [ant: {overstate}]
     3: belittle; "Don't belittle his influence" [syn: {belittle}, {denigrate},
         {derogate}]
