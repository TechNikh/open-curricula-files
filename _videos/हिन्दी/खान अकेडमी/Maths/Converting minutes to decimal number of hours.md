---
version: 1
type: video
provider: YouTube
id: spqw-sbnlpI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fd3c62b9-c2c9-429c-bfe3-fb168c76b846
updated: 1486069607
title: Converting minutes to decimal number of hours
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/spqw-sbnlpI/default.jpg
    - https://i3.ytimg.com/vi/spqw-sbnlpI/1.jpg
    - https://i3.ytimg.com/vi/spqw-sbnlpI/2.jpg
    - https://i3.ytimg.com/vi/spqw-sbnlpI/3.jpg
---
