---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hearing
offline_file: ""
offline_thumbnail: ""
uuid: 887419a3-63e0-4674-8be1-20f1c9245fba
updated: 1484310349
title: hearing
categories:
    - Dictionary
---
hearing
     adj : able to perceive sound [syn: {hearing(a)}] [ant: {deaf}]
     n 1: (law) a proceeding (usually by a court) where evidence is
          taken for the purpose of determining an issue of fact
          and reaching a decision based on that evidence
     2: an opportunity to state your case and be heard; "they
        condemned him without a hearing"; "he saw that he had lost
        his audience" [syn: {audience}]
     3: the range within which a voice can be heard; "the children
        were told to stay within earshot" [syn: {earshot}, {earreach}]
     4: the act of hearing attentively; "you can learn a lot by just
        listening"; "they make good music--you should give ...
