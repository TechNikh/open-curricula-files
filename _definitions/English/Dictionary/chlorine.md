---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chlorine
offline_file: ""
offline_thumbnail: ""
uuid: d9bf7e24-764e-4ceb-a5b2-cdddefc3e470
updated: 1484310379
title: chlorine
categories:
    - Dictionary
---
chlorine
     n : a common nonmetallic element belonging to the halogens; best
         known as a heavy yellow irritating toxic gas; used to
         purify water and as a bleaching agent and disinfectant;
         occurs naturally only as a salt (as in sea water) [syn: {Cl},
          {atomic number 17}]
