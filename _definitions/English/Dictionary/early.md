---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/early
offline_file: ""
offline_thumbnail: ""
uuid: 3c076325-7aeb-4caa-89ca-77e2bd93e4f6
updated: 1484310307
title: early
categories:
    - Dictionary
---
early
     adj 1: at or near the beginning of a period of time or course of
            events or before the usual or expected time; "early
            morning"; "an early warning"; "early diagnosis"; "an
            early death"; "took early retirement"; "an early
            spring"; "early varieties of peas and tomatoes mature
            before most standard varieties" [ant: {middle}, {late}]
     2: being or occurring at an early stage of development; "in an
        early stage"; "early forms of life"; "early man"; "an
        early computer" [ant: {late}]
     3: of the distant past; "the early inhabitants of Europe";
        "former generations"; "in other times" [syn: {early(a)}, ...
