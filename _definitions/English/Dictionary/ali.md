---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ali
offline_file: ""
offline_thumbnail: ""
uuid: a3347fc2-b209-4634-aba1-b0d1279277d8
updated: 1484310484
title: ali
categories:
    - Dictionary
---
Ali
     n 1: United States prizefighter who won the world heavyweight
          championship three times (born in 1942) [syn: {Muhammad
          Ali}, {Cassius Clay}, {Cassius Marcellus Clay}]
     2: the fourth caliph of Islam who is considered to be the first
        caliph by Shiites; he was a cousin and son-in-law of
        Muhammad; after his assination Islam was divided into
        Shiite and Sunnite sects
