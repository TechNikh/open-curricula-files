---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sunk
offline_file: ""
offline_thumbnail: ""
uuid: 356468ba-565b-43e7-a60b-83bdca45c5b7
updated: 1484310464
title: sunk
categories:
    - Dictionary
---
sunk
     adj : doomed to extinction [syn: {done for(p)}, {ruined}, {undone},
            {washed-up}]
