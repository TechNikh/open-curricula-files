---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/switching
offline_file: ""
offline_thumbnail: ""
uuid: 054bd35d-902d-49e0-9ead-20352584d654
updated: 1484310242
title: switching
categories:
    - Dictionary
---
switching
     n : the act of changing one thing or position for another; "his
         switch on abortion cost him the election" [syn: {switch},
          {shift}]
