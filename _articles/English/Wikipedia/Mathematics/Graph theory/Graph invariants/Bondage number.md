---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bondage_number
offline_file: ""
offline_thumbnail: ""
uuid: 2216f8dd-ac1c-4c4b-ad77-bd0f814f809f
updated: 1484309417
title: Bondage number
categories:
    - Graph invariants
---
In mathematics, the bondage number of a nonempty graph is the cardinality of the smallest set of edges E such that the domination number of the graph with the edges E removed is greater than the domination number of the original graph.[1][2] The concept was introduced by Fink et. al.[3]
