---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robin_Thomas_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: a07bd9bb-a311-4171-b0d8-e13519eb6c51
updated: 1484309476
title: Robin Thomas (mathematician)
categories:
    - Graph theorists
---
Thomas received his doctorate in 1985 from Charles University in Prague, Czechoslovakia (now the Czech Republic), under the supervision of Jaroslav Nešetřil.[1] He joined the faculty at Georgia Tech in 1989, and is now a Regents' Professor there.[2][3]
