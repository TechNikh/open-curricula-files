---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Zemor%27s_decoding_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: e442e54b-ce59-4e02-8587-bee32ff16fc9
updated: 1484308565
title: "Zemor's decoding algorithm"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/440px-Zemor_Decoding.jpg
tags:
    - Code construction
    - Claim 1
    - Proof
    - Claim 2
    - Proof
    - "Zemor's algorithm"
    - Decoder algorithm
    - Explanation of the algorithm
    - Theorem
    - Proof
    - "Drawbacks of Zemor's algorithm"
categories:
    - Coding theory
---
In coding theory, Zemor's algorithm, designed and developed by Gilles Zemor,[1] is a recursive low-complexity approach to code construction. It is an improvement over the algorithm of Sipser and Spielman.
