---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/said
offline_file: ""
offline_thumbnail: ""
uuid: 08501f7f-b3c9-4f9d-b056-05a7379625c4
updated: 1484310363
title: said
categories:
    - Dictionary
---
said
     adj : being the one previously mentioned or spoken of; "works of
           all the aforementioned authors"; "said party has denied
           the charges" [syn: {aforesaid(a)}, {aforementioned(a)},
            {said(a)}]
