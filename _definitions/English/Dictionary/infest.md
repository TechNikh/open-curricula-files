---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484428921
title: infest
categories:
    - Dictionary
---
infest
     v 1: invade in great numbers; "the roaches infested our kitchen"
          [syn: {overrun}]
     2: occupy in large numbers or live on a host; "the Kudzu plant
        infests much of the South and is spreading to the North"
        [syn: {invade}, {overrun}]
     3: live on or in a host, as of parasites
