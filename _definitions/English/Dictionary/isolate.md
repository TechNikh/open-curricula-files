---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/isolate
offline_file: ""
offline_thumbnail: ""
uuid: db1fbd29-2a6f-47f0-8bc2-b2dbd90c1cd9
updated: 1484310553
title: isolate
categories:
    - Dictionary
---
isolate
     v 1: place or set apart; "They isolated the political prisoners
          from the other inmates" [syn: {insulate}]
     2: obtain in pure form; "The chemist managed to isolate the
        compound"
     3: set apart from others; "The dentist sequesters the tooth he
        is working on" [syn: {sequester}, {sequestrate}, {keep
        apart}, {set apart}]
     4: separate (experiences) fromt he emotions relating to them
