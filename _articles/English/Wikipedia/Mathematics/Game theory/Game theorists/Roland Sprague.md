---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Roland_Sprague
offline_file: ""
offline_thumbnail: ""
uuid: 5e694f1d-7f7e-4e4f-851f-25213af6b775
updated: 1484309029
title: Roland Sprague
tags:
    - Biography
    - Selected works
categories:
    - Game theorists
---
Roland Percival Sprague (11 July 1894, Unterliederbach – 1 August 1967) was a German mathematician, known for the Sprague–Grundy theorem[1] and for being the first mathematician to find a perfect squared square.
