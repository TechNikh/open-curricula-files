---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thickness_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 366ebdd8-6839-43f4-9e29-4cac90d00756
updated: 1484309423
title: Thickness (graph theory)
tags:
    - Specific graphs
    - Related problems
    - Computational complexity
categories:
    - Graph invariants
---
In graph theory, the thickness of a graph G is the minimum number of planar graphs into which the edges of G can be partitioned. That is, if there exists a collection of k planar graphs, all having the same set of vertices, such that the union of these planar graphs is G, then the thickness of G is at most k.[1][2] In other words, the thickness of a graph is the minimum number of planar subgraphs whose union equals to graph G.[3]
