---
version: 1
type: video
provider: YouTube
id: 54SkZJhMmKk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20Time%20Dilatation%20in%20Theory%20Of%20Relativity..webm"
offline_file: ""
offline_thumbnail: ""
uuid: c51440a8-2be1-404e-9751-260ae9991f4a
updated: 1486069653
title: What is Time Dilatation in Theory Of Relativity.
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/54SkZJhMmKk/default.jpg
    - https://i3.ytimg.com/vi/54SkZJhMmKk/1.jpg
    - https://i3.ytimg.com/vi/54SkZJhMmKk/2.jpg
    - https://i3.ytimg.com/vi/54SkZJhMmKk/3.jpg
---
