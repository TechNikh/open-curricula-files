---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hungarian_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: 0f3f8f7c-88e3-448b-ac6e-bba4f361a478
updated: 1484309423
title: Hungarian algorithm
tags:
    - Simple explanation of the assignment problem
    - Setting
    - The algorithm in terms of bipartite graphs
    - Matrix interpretation
    - Bibliography
    - Implementations
categories:
    - Matching
---
The Hungarian method is a combinatorial optimization algorithm that solves the assignment problem in polynomial time and which anticipated later primal-dual methods. It was developed and published in 1955 by Harold Kuhn, who gave the name "Hungarian method" because the algorithm was largely based on the earlier works of two Hungarian mathematicians: Dénes Kőnig and Jenő Egerváry.[1][2]
