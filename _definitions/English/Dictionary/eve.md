---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eve
offline_file: ""
offline_thumbnail: ""
uuid: fc8387c5-2a24-41ee-b825-e779b0f68733
updated: 1484310593
title: eve
categories:
    - Dictionary
---
Eve
     n 1: (Old Testament) Adam's wife in Judeo-Christian mythology:
          the first woman and mother of the human race; God
          created Eve from Adam's rib and placed Adam and Eve in
          the Garden of Eden
     2: the day before; "he always arrives on the eve of her
        departure"
     3: the period immediately before something; "on the eve of the
        French Revolution"
     4: the latter part of the day (the period of decreasing
        daylight from late afternoon until nightfall); "he enjoyed
        the evening light across the lake" [syn: {evening}, {eventide}]
