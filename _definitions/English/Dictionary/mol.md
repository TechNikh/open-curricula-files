---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mol
offline_file: ""
offline_thumbnail: ""
uuid: 25c6c4ef-ff38-488f-abfd-a7e57e738dd2
updated: 1484310401
title: mol
categories:
    - Dictionary
---
mol
     n : the molecular weight of a substance expressed in grams; the
         basic unit of amount of substance adopted under the
         Systeme International d'Unites [syn: {gram molecule}, {mole}]
