---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/a
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484405581
title: a
categories:
    - Dictionary
---
A
     n 1: the blood group whose red cells carry the A antigen [syn: {type
          A}, {group A}]
     2: a metric unit of length equal to one ten billionth of a
        meter (or 0.0001 micron); used to specify wavelengths of
        electromagnetic radiation [syn: {angstrom}, {angstrom unit}]
     3: any of several fat-soluble vitamins essential for normal
        vision; prevents night blindness or inflammation or
        dryness of the eyes [syn: {vitamin A}, {antiophthalmic
        factor}, {axerophthol}]
     4: one of the four nucleotides used in building DNA; all four
        nucleotides have a common phosphate group and a sugar
        (ribose) [syn: {deoxyadenosine ...
