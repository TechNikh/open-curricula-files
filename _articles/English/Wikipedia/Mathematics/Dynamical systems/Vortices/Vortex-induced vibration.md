---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vortex-induced_vibration
offline_file: ""
offline_thumbnail: ""
uuid: ede51e22-1059-4b34-b4eb-96dceee592e1
updated: 1484308879
title: Vortex-induced vibration
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/215px-FIV_cylindre.gif
tags:
    - Motivation
    - Current state of art
categories:
    - Vortices
---
In fluid dynamics, vortex-induced vibrations (VIV) are motions induced on bodies interacting with an external fluid flow, produced by – or the motion producing – periodical irregularities on this flow.
