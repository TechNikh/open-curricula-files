---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transaction
offline_file: ""
offline_thumbnail: ""
uuid: 82d9ecd5-f286-4ac1-aa55-0e09b0b9f26c
updated: 1484310453
title: transaction
categories:
    - Dictionary
---
transaction
     n : the act of transacting within or between groups (as carrying
         on commercial activities); "no transactions are possible
         without him"; "he has always been honest is his dealings
         with me" [syn: {dealing}, {dealings}]
