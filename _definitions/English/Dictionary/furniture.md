---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/furniture
offline_file: ""
offline_thumbnail: ""
uuid: a9d7f613-200d-41e8-8f0e-14f166be8a96
updated: 1484310234
title: furniture
categories:
    - Dictionary
---
furniture
     n : furnishings that make a room or other area ready for
         occupancy; "they had too much furniture for the small
         apartment"; "there was only one piece of furniture in the
         room" [syn: {piece of furniture}, {article of furniture}]
