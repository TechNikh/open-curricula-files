---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Du_Bois_singularity
offline_file: ""
offline_thumbnail: ""
uuid: 73dbc4af-b9eb-47f6-98be-679c59e7e639
updated: 1484309142
title: Du Bois singularity
categories:
    - Algebraic geometry
---
Schwede (2007) gave the following characterisation of Du Bois singularities. Suppose that 
  
    
      
        X
      
    
    {\displaystyle X}
  
 is a reduced closed subscheme of a smooth scheme 
  
    
      
        Y
      
    
    {\displaystyle Y}
  
.
