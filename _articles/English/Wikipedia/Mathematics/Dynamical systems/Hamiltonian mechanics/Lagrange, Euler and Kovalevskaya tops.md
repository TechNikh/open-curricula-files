---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Lagrange,_Euler_and_Kovalevskaya_tops
offline_file: ""
offline_thumbnail: ""
uuid: 840410e0-1a3d-4edd-ba2e-d20b06496b9c
updated: 1484308791
title: Lagrange, Euler and Kovalevskaya tops
tags:
    - Hamiltonian Formulation of Classical tops
    - Euler Top
    - Lagrange Top
    - Kovalevskaya Top
categories:
    - Hamiltonian mechanics
---
In classical mechanics, the precession of a top under the influence of gravity is not, in general, an integrable problem. There are however three famous cases that are integrable, the Euler, the Lagrange and the Kovalevskaya top.[1] In addition to the energy, each of these tops involves three additional constants of motion that give rise to the integrability.
