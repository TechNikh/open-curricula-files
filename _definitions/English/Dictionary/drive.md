---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drive
offline_file: ""
offline_thumbnail: ""
uuid: 070ca9d5-33bd-412b-bf47-684a3dc037d4
updated: 1484310486
title: drive
categories:
    - Dictionary
---
drive
     n 1: the act of applying force to propel something; "after
          reaching the desired velocity the drive is cut off"
          [syn: {thrust}, {driving force}]
     2: a mechanism by which force or power is transmitted in a
        machine; "a variable speed drive permitted operation
        through a range of speeds"
     3: a series of actions advancing a principle or tending toward
        a particular end; "he supported populist campaigns"; "they
        worked in the cause of world peace"; "the team was ready
        for a drive toward the pennant"; "the movement to end
        slavery"; "contributed to the war effort" [syn: {campaign},
         {cause}, {crusade}, ...
