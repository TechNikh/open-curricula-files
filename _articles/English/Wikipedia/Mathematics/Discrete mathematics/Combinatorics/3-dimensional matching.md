---
version: 1
type: article
id: https://en.wikipedia.org/wiki/3-dimensional_matching
offline_file: ""
offline_thumbnail: ""
uuid: 7f252042-aecc-44df-86ae-835fb14019dd
updated: 1484308570
title: 3-dimensional matching
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-3-dimensional-matching.svg.png
tags:
    - Definition
    - Example
    - Comparison with bipartite matching
    - Comparison with set packing
    - Decision problem
    - Optimization problem
    - Approximation algorithms
    - Notes
categories:
    - Combinatorics
---
In the mathematical discipline of graph theory, a 3-dimensional matching is a generalization of bipartite matching (also known as 2-dimensional matching) to 3-uniform hypergraphs. Finding a largest 3-dimensional matching is a well-known NP-hard problem in computational complexity theory.
