---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/centralising
offline_file: ""
offline_thumbnail: ""
uuid: d845027d-4dae-4f76-acbd-0160ad9a7bb7
updated: 1484310599
title: centralising
categories:
    - Dictionary
---
centralising
     adj : tending to draw to a central point [syn: {centralizing(a)},
           {centralising(a)}] [ant: {decentralizing(a)}]
