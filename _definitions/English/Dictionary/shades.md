---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shades
offline_file: ""
offline_thumbnail: ""
uuid: 540e4f5c-30c7-4f71-a97e-636855a2f967
updated: 1484310595
title: shades
categories:
    - Dictionary
---
shades
     n : spectacles that are darkened or polarized to protect the
         eyes from the glare of the sun; "he was wearing a pair of
         mirrored shades" [syn: {sunglasses}, {dark glasses}]
