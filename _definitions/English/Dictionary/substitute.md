---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/substitute
offline_file: ""
offline_thumbnail: ""
uuid: 9dbf2b61-faf1-450e-9efc-5635de5bc9a8
updated: 1484310216
title: substitute
categories:
    - Dictionary
---
substitute
     adj 1: being a replacement or substitute for a regular member of a
            team [syn: {second-string}, {substitute(a)}]
     2: capable of substituting in any of several positions on a
        team; "a utility infielder" [syn: {utility(a)}, {substitute(a)}]
     3: artificial and inferior; "ersatz coffee"; "substitute
        coffee" [syn: {ersatz}]
     n 1: a person or thing that takes or can take the place of
          another [syn: {replacement}]
     2: an athlete who plays only when another member of the team
        drops out [syn: {reserve}]
     3: someone who takes the place of another (as when things get
        dangerous or difficult); "the star had a ...
