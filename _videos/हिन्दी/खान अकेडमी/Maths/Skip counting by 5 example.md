---
version: 1
type: video
provider: YouTube
id: hVEJLyEeHag
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5dee56bb-e324-42f6-8eb8-e6e7a2879d53
updated: 1486069605
title: Skip counting by 5 example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/hVEJLyEeHag/default.jpg
    - https://i3.ytimg.com/vi/hVEJLyEeHag/1.jpg
    - https://i3.ytimg.com/vi/hVEJLyEeHag/2.jpg
    - https://i3.ytimg.com/vi/hVEJLyEeHag/3.jpg
---
