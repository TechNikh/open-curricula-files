---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/characterize
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484594281
title: characterize
categories:
    - Dictionary
---
characterize
     v 1: describe or portray the character or the qualities or
          peculiarities of; "You can characterize his behavior as
          that of an egotist"; "This poem can be characterized as
          a lament for a dead lover" [syn: {qualify}, {characterise}]
     2: be characteristic of; "What characterizes a Venetian
        painting?" [syn: {characterise}]
