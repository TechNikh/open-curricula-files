---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/multinational
offline_file: ""
offline_thumbnail: ""
uuid: e1151d2a-5788-480b-9c37-4820866e98c5
updated: 1484310443
title: multinational
categories:
    - Dictionary
---
multinational
     adj : involving or operating in several nations or nationalities;
           "multinational corporations"; "transnational terrorist
           networks" [syn: {transnational}]
