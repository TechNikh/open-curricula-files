---
version: 1
type: video
provider: YouTube
id: fy_tncCQTPA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d07c80f7-7930-40a7-b0d7-599433921a2a
updated: 1486069657
title: FREE ENERGY FROM AIR CIRCUIT PART 4/24
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/fy_tncCQTPA/default.jpg
    - https://i3.ytimg.com/vi/fy_tncCQTPA/1.jpg
    - https://i3.ytimg.com/vi/fy_tncCQTPA/2.jpg
    - https://i3.ytimg.com/vi/fy_tncCQTPA/3.jpg
---
