---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Contraction_mapping
offline_file: ""
offline_thumbnail: ""
uuid: 527a28ff-3602-4dbf-ad63-2e1462abb846
updated: 1484308784
title: Contraction mapping
tags:
    - Firmly non-expansive mapping
    - Subcontraction map
categories:
    - Fixed points (mathematics)
---
In mathematics, a contraction mapping, or contraction or contractor, on a metric space (M,d) is a function f from M to itself, with the property that there is some nonnegative real number 
  
    
      
        0
        ≤
        k
        <
        1
      
    
    {\displaystyle 0\leq k<1}
  
 such that for all x and y in M,
