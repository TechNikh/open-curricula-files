---
version: 1
type: video
provider: YouTube
id: Jveer7vhjGo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/AC%20Circuits-%20Crash%20Course%20Physics%20%2336.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 43eb177a-5c89-4192-8238-adc358725cbd
updated: 1486069682
title: 'AC Circuits- Crash Course Physics #36'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/Jveer7vhjGo/default.jpg
    - https://i3.ytimg.com/vi/Jveer7vhjGo/1.jpg
    - https://i3.ytimg.com/vi/Jveer7vhjGo/2.jpg
    - https://i3.ytimg.com/vi/Jveer7vhjGo/3.jpg
---
