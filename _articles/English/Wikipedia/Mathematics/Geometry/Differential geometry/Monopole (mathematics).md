---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Monopole_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: bf0ae3ed-e678-4166-8da2-a49f1e27ee83
updated: 1484309228
title: Monopole (mathematics)
categories:
    - Differential geometry
---
In mathematics, a monopole is a connection over a principal bundle G with a section (the Higgs field) of the associated adjoint bundle. The connection and Higgs field should satisfy the Bogomolny equations and be of finite action.
