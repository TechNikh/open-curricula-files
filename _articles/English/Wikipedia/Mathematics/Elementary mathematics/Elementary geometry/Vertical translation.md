---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vertical_translation
offline_file: ""
offline_thumbnail: ""
uuid: 71ccc067-bf54-4800-b1d8-b8fe883dd4c3
updated: 1484308944
title: Vertical translation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Constant_of_integration_001.png
categories:
    - Elementary geometry
---
In geometry, a vertical translation is a translation of a geometric object in a direction parallel to the vertical axis of the Cartesian coordinate system.[1][2][3]
