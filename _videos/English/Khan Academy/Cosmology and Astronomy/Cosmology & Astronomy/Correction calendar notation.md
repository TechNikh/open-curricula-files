---
version: 1
type: video
provider: YouTube
id: N06Jy-gQog8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Correction%20calendar%20notation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5e9b7ad1-7914-4e1e-b578-69c380329356
updated: 1486069688
title: Correction calendar notation
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/N06Jy-gQog8/default.jpg
    - https://i3.ytimg.com/vi/N06Jy-gQog8/1.jpg
    - https://i3.ytimg.com/vi/N06Jy-gQog8/2.jpg
    - https://i3.ytimg.com/vi/N06Jy-gQog8/3.jpg
---
