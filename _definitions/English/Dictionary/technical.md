---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/technical
offline_file: ""
offline_thumbnail: ""
uuid: dc72c404-b4b3-462b-9d5c-83dd4e0fcd9a
updated: 1484310254
title: technical
categories:
    - Dictionary
---
technical
     adj 1: of or relating to technique; "technical innovation in recent
            novels"; "technical details"
     2: characterizing or showing skill in or specialized knowledge
        of applied arts and sciences; "a technical problem";
        "highly technical matters hardly suitable for the general
        public"; "a technical report"; "producing the A-bomb was a
        challenge to the technical people of this country";
        "technical training"; "technical language" [ant: {nontechnical}]
     3: of or relating to proficiency in a practical skill; "no
        amount of technical skill and craftsmanship can take the
        place of vital interest"- John Dewey
     ...
