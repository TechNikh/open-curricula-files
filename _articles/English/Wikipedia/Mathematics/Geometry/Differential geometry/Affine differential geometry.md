---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_differential_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 8b4d28a7-dd87-41eb-b403-a1e0ae696b3d
updated: 1484309200
title: Affine differential geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-AffineNormDrDec.jpeg
tags:
    - Preliminaries
    - The first Induced volume form
    - The second induced volume form
    - Two natural conditions
    - The conclusion
    - The affine normal line
    - Plane curves
    - Surfaces in 3-space
categories:
    - Differential geometry
---
Affine differential geometry, is a type of differential geometry in which the differential invariants are invariant under volume-preserving affine transformations. The name affine differential geometry follows from Klein's Erlangen program. The basic difference between affine and Riemannian differential geometry is that in the affine case we introduce volume forms over a manifold instead of metrics.
