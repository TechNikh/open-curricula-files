---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decade
offline_file: ""
offline_thumbnail: ""
uuid: 33f35396-0b3c-4bd8-b8a3-a8c9f786fad5
updated: 1484310445
title: decade
categories:
    - Dictionary
---
decade
     n 1: a period of 10 years [syn: {decennary}, {decennium}]
     2: the cardinal number that is the sum of nine and one; the
        base of the decimal system [syn: {ten}, {10}, {X}, {tenner}]
