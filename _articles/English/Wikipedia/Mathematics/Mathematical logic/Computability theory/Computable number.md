---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computable_number
offline_file: ""
offline_thumbnail: ""
uuid: 9722978e-cf73-41fc-8eae-7f6d6699ef04
updated: 1484309485
title: Computable number
tags:
    - Informal definition using a Turing machine as example
    - Formal definition
    - Properties
    - Countable but not computably enumerable
    - Properties as a field
    - Non-computability of the ordering
    - Other properties
    - Digit strings and the Cantor and Baire spaces
    - Can computable numbers be used instead of the reals?
    - Implementation
categories:
    - Computability theory
---
In mathematics, computable numbers are the real numbers that can be computed to within any desired precision by a finite, terminating algorithm. They are also known as the recursive numbers or the computable reals or recursive reals.
