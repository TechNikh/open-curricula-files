---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chhotanagpur
offline_file: ""
offline_thumbnail: ""
uuid: 7de009d9-bcd0-4580-a59f-1e7915da7f0b
updated: 1484308420
title: Chota Nagpur Plateau
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/270px-Damodarbksc.jpg
tags:
    - Etymology
    - Formation
    - Divisions
    - Pat region
    - Ranchi plateau
    - Hazaribagh plateau
    - Koderma plateau
    - Damodar trough
    - Palamu
    - Manbhum-Singhbhum
    - Climate
    - Ecology
    - Protected areas
    - Minerals
categories:
    - Uncategorized
---
The Chota Nagpur Plateau is a plateau in eastern India, which covers much of Jharkhand state as well as adjacent parts of Odisha, West Bengal, Bihar and Chhattisgarh. The Indo-Gangetic plain lies to the north and east of the plateau, and the basin of the Mahanadi River lies to the south. The total area of the Chota Plateau is approximately 65,000 square kilometres (25,000 sq mi).[1]
