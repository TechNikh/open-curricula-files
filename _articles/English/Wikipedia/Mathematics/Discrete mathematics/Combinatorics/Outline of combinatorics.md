---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Outline_of_combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: f25000cf-c843-4ff4-bc4d-3ff00365a83d
updated: 1484308567
title: Outline of combinatorics
tags:
    - Essence of combinatorics
    - Branches of combinatorics
    - Multi-disciplinary fields that include combinatorics
    - History of combinatorics
    - General combinatorial principles and methods
    - Data structure concepts
    - Problem solving as an art
    - Living with large numbers
    - Persons influential in the field of combinatorics
    - Combinatorics scholars
    - Journals
    - Prizes
categories:
    - Combinatorics
---
History of combinatorics
