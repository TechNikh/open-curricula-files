---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brutality
offline_file: ""
offline_thumbnail: ""
uuid: 74778166-8f8c-4ca8-831c-00db56e88fe0
updated: 1484310567
title: brutality
categories:
    - Dictionary
---
brutality
     n 1: the trait of extreme cruelty [syn: {ferociousness}, {viciousness},
           {savageness}, {savagery}]
     2: a brutal barbarous savage act [syn: {barbarity}, {barbarism},
         {savagery}]
