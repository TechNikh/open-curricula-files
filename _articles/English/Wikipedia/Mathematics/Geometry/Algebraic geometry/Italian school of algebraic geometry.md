---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Italian_school_of_algebraic_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 3819fe00-cb46-4530-9bb9-5a48cfaa46a8
updated: 1484309156
title: Italian school of algebraic geometry
tags:
    - Algebraic surfaces
    - Foundational issues
    - The geometers
    - Advent of topology
    - Collapse of the school
categories:
    - Algebraic geometry
---
In relation with the history of mathematics, the Italian school of algebraic geometry refers to the work over half a century or more (flourishing roughly 1885–1935) done internationally in birational geometry, particularly on algebraic surfaces. There were in the region of 30 to 40 leading mathematicians who made major contributions, about half of those being in fact Italian. The leadership fell to the group in Rome of Guido Castelnuovo, Federigo Enriques and Francesco Severi, who were involved in some of the deepest discoveries, as well as setting the style.
