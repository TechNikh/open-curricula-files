---
version: 1
type: video
provider: YouTube
id: kqqmJiJez6o
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Area%20and%20Perimeter.webm"
offline_file: ""
offline_thumbnail: ""
uuid: eec8aceb-0e94-4b8d-a203-751e12cd5b16
updated: 1486069581
title: Area and Perimeter
categories:
    - Geometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/kqqmJiJez6o/default.jpg
    - https://i3.ytimg.com/vi/kqqmJiJez6o/1.jpg
    - https://i3.ytimg.com/vi/kqqmJiJez6o/2.jpg
    - https://i3.ytimg.com/vi/kqqmJiJez6o/3.jpg
---
Area of rectangles and triangles.  Perimeter of rectangles.
