---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mucus
offline_file: ""
offline_thumbnail: ""
uuid: 6265d947-595b-4b16-a761-02c5a02aa99b
updated: 1484310351
title: mucus
categories:
    - Dictionary
---
mucus
     n : protective secretion of the mucous membranes; in the gut it
         lubricates the passage of food and protects the
         epithelial cells; in the nose and throat and lungs it can
         make it difficult for bacteria to penetrate the body
         through the epithelium [syn: {mucous secretion}]
