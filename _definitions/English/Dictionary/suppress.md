---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suppress
offline_file: ""
offline_thumbnail: ""
uuid: cdd05a91-5343-4196-9ef6-e765e9123732
updated: 1484310557
title: suppress
categories:
    - Dictionary
---
suppress
     v 1: to put down by force or authority; "suppress a nascent
          uprising"; "stamp down on littering"; "conquer one's
          desires" [syn: {stamp down}, {inhibit}, {subdue}, {conquer},
           {curb}]
     2: come down on or keep down by unjust use of one's authority;
        "The government oppresses political activists" [syn: {oppress},
         {crush}]
     3: control and refrain from showing; of emotions [syn: {bottle
        up}]
     4: keep under control; keep in check; "suppress a smile"; "Keep
        your temper"; "keep your cool" [syn: {restrain}, {keep}, {keep
        back}, {hold back}]
     5: put out of one's consciousness [syn: {repress}]
