---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trillions
offline_file: ""
offline_thumbnail: ""
uuid: e327599a-9e37-42b1-a3d9-37adb2ea0f72
updated: 1484310325
title: trillions
categories:
    - Dictionary
---
trillions
     n : a very large indefinite number (usually hyperbole) [syn: {millions},
          {billions}, {zillions}, {jillions}]
