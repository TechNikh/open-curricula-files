---
version: 1
type: video
provider: YouTube
id: bhzh8VDykc4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Possessive%20pronouns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: dec8bdba-ff65-4423-a88a-0b4f8abd4c4a
updated: 1486069599
title: Possessive pronouns
tags:
    - grammar
    - possessive
    - english lessons
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/bhzh8VDykc4/default.jpg
    - https://i3.ytimg.com/vi/bhzh8VDykc4/1.jpg
    - https://i3.ytimg.com/vi/bhzh8VDykc4/2.jpg
    - https://i3.ytimg.com/vi/bhzh8VDykc4/3.jpg
---
We can use pronouns to show possession relationships, describing what things belong to which people, like "her shoe" or "the book is mine." Possessive pronouns come in two flavors: adjective and noun! David, KA's resident grammarian, covers the difference between them. 

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/e/choose-the-correct-possessive-pronoun-or-adjective?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/reflexive-pronouns-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/personal-pronouns-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
