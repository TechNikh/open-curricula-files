---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/source
offline_file: ""
offline_thumbnail: ""
uuid: a6ae2471-cac7-439f-944b-a007b8a71f2d
updated: 1484310275
title: source
categories:
    - Dictionary
---
source
     n 1: the place where something begins, where it springs into
          being; "the Italian beginning of the Renaissance";
          "Jupiter was the origin of the radiation"; "Pittsburgh
          is the source of the Ohio River"; "communism's Russian
          root" [syn: {beginning}, {origin}, {root}, {rootage}]
     2: a person who supplies information [syn: {informant}]
     3: a publication (or a passage from a publication) that is
        referred to; "he carried an armful of references back to
        his desk"; "he spent hours looking for the source of that
        quotation" [syn: {reference}]
     4: a document (or organization) from which information is
        ...
