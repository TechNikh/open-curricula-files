---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uncompromising
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484437921
title: uncompromising
categories:
    - Dictionary
---
uncompromising
     adj : not making concessions; "took an uncompromising stance in
           the peace talks"; "uncompromising honesty" [syn: {inflexible}]
           [ant: {compromising}]
