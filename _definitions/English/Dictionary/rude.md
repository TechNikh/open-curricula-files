---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rude
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413261
title: rude
categories:
    - Dictionary
---
rude
     adj 1: socially incorrect in behavior; "resentment flared at such
            an unmannered intrusion" [syn: {ill-mannered}, {unmannered},
             {unmannerly}]
     2: (of persons) lacking in refinement or grace [syn: {ill-bred},
         {bounderish}, {lowbred}, {underbred}, {yokelish}]
     3: lacking civility or good manners; "want nothing from you but
        to get away from your uncivil tongue"- Willa Cather [syn:
        {uncivil}] [ant: {civil}]
     4: (used especially of commodities) in the natural unprocessed
        condition; "natural yogurt"; "natural produce"; "raw
        wool"; "raw sugar"; "bales of rude cotton" [syn: {natural},
         {raw(a)}, ...
