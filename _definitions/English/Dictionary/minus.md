---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minus
offline_file: ""
offline_thumbnail: ""
uuid: 0e48e271-0546-46d1-ac83-781d1eb1f8bf
updated: 1484310204
title: minus
categories:
    - Dictionary
---
minus
     adj 1: on the negative side or lower end of a scale; "minus 5
            degrees"; "a grade of B minus" [ant: {plus}]
     2: involving disadvantage or harm; "minus (or negative)
        factors" [syn: {negative}]
     n : an arithmetic operation in which the difference between two
         numbers is calculated; "the subtraction of three from
         four leaves one"; "four minus three equals one" [syn: {subtraction}]
