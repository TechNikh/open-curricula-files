---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euclidean_geometry
offline_file: ""
offline_thumbnail: ""
uuid: dc96a397-21e7-42ef-a971-a3737eb4d740
updated: 1484308921
title: Euclidean geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/236px-Sanzio_01_Euclid.jpg
tags:
    - The Elements
    - Axioms
    - Parallel postulate
    - Methods of proof
    - System of measurement and arithmetic
    - Notation and terminology
    - Naming of points and figures
    - Complementary and supplementary angles
    - "Modern versions of Euclid's notation"
    - Some important or well known results
    - Pons Asinorum
    - Congruence of triangles
    - Triangle angle sum
    - Pythagorean theorem
    - "Thales' theorem"
    - Scaling of area and volume
    - Applications
    - As a description of the structure of space
    - Later work
    - Archimedes and Apollonius
    - '17th century: Descartes'
    - 18th century
    - 19th century and non-Euclidean geometry
    - 20th century and general relativity
    - Treatment of infinity
    - Infinite objects
    - Infinite processes
    - Logical basis
    - Classical logic
    - Modern standards of rigor
    - Axiomatic formulations
    - Constructive approaches and pedagogy
    - Classical theorems
    - Notes
categories:
    - Elementary geometry
---
Euclidean geometry is a mathematical system attributed to the Alexandrian Greek mathematician Euclid, which he described in his textbook on geometry: the Elements. Euclid's method consists in assuming a small set of intuitively appealing axioms, and deducing many other propositions (theorems) from these. Although many of Euclid's results had been stated by earlier mathematicians,[1] Euclid was the first to show how these propositions could fit into a comprehensive deductive and logical system.[2] The Elements begins with plane geometry, still taught in secondary school as the first axiomatic system and the first examples of formal proof. It goes on to the solid geometry of three dimensions. ...
