---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/realistic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484460541
title: realistic
categories:
    - Dictionary
---
realistic
     adj 1: aware or expressing awareness of things as they really are;
            "a realistic description"; "a realistic view of the
            possibilities"; "a realistic appraisal of our
            chances"; "the actors tried to create a realistic
            portrayal of the Africans" [ant: {unrealistic}]
     2: representing what is real; not abstract or ideal; "realistic
        portraiture"; "a realistic novel"; "in naturalistic
        colors"; "the school of naturalistic writers" [syn: {naturalistic}]
     3: of or relating to the philosophical doctrine of realism; "a
        realistic system of thought"
