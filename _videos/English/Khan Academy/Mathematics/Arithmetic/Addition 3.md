---
version: 1
type: video
provider: YouTube
id: e_SpXIw_Qts
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Addition%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9d70601e-cdd1-4183-8c38-c3785b28c6d3
updated: 1486069567
title: Addition 3
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/e_SpXIw_Qts/default.jpg
    - https://i3.ytimg.com/vi/e_SpXIw_Qts/1.jpg
    - https://i3.ytimg.com/vi/e_SpXIw_Qts/2.jpg
    - https://i3.ytimg.com/vi/e_SpXIw_Qts/3.jpg
---
Practice carrying digits to add multiple digit numbers
