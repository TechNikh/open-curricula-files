---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strait
offline_file: ""
offline_thumbnail: ""
uuid: 513c872d-a59e-4172-b6a6-f6084ff94dfc
updated: 1484310439
title: strait
categories:
    - Dictionary
---
strait
     adj : strict and severe; "strait is the gate"
     n 1: a narrow channel of the sea joining two larger bodies of
          water [syn: {sound}]
     2: a bad or difficult situation or state of affairs [syn: {pass},
         {straits}]
