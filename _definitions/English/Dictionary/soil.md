---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soil
offline_file: ""
offline_thumbnail: ""
uuid: a9ee4479-1c1f-4a18-ba47-f088f1c6d806
updated: 1484310266
title: soil
categories:
    - Dictionary
---
soil
     n 1: the state of being covered with unclean things [syn: {dirt},
           {filth}, {grime}, {stain}, {grease}, {grunge}]
     2: the part of the earth's surface consisting of humus and
        disintegrated rock [syn: {dirt}]
     3: material in the top layer of the surface of the earth in
        which plants can grow (especially with reference to its
        quality or use); "the land had never been plowed"; "good
        agricultural soil" [syn: {land}, {ground}]
     4: the geographical area under the jurisdiction of a sovereign
        state; "American troops were stationed on Japanese soil"
        [syn: {territory}]
     v : make soiled, filthy, or dirty; "don't soil ...
