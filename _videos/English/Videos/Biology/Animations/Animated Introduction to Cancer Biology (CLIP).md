---
version: 1
type: video
provider: YouTube
id: tiWMCKQys68
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9702bca6-3f4c-4fa3-b40d-63514d2227e6
updated: 1486069669
title: Animated Introduction to Cancer Biology (CLIP)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/tiWMCKQys68/default.jpg
    - https://i3.ytimg.com/vi/tiWMCKQys68/1.jpg
    - https://i3.ytimg.com/vi/tiWMCKQys68/2.jpg
    - https://i3.ytimg.com/vi/tiWMCKQys68/3.jpg
---
