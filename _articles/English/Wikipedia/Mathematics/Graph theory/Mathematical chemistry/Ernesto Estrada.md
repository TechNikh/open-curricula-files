---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ernesto_Estrada
offline_file: ""
offline_thumbnail: ""
uuid: cd847bb6-47d6-4152-afef-5eae380c689b
updated: 1484309431
title: Ernesto Estrada
tags:
    - Birth and education
    - Academic career
    - Research and achievements
    - Selected publications
categories:
    - Mathematical chemistry
---
Ernesto Estrada (born May 2, 1966) is a Cuban-Spanish scientist. He is the Chair in Complexity Science, Full Professor at the Department of Mathematics and Statistics, and a member of the Institute of Complex Systems of the University of Strathclyde, Glasgow, United Kingdom. He is known by his contributions in different disciplines including chemistry, and the mathematics and physics of Complex Systems.
