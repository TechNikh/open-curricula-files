---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Image_rectification
offline_file: ""
offline_thumbnail: ""
uuid: 81668b39-30b2-4522-a1d4-853dfe29e5d9
updated: 1484309181
title: Image rectification
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Planar_rectification_for_a_rotating_camera..gif
tags:
    - In computer vision
    - Transformation
    - Algorithms
    - Implementation details
    - Example
    - Geographic information system
    - Reference implementations
categories:
    - Geometry in computer vision
---
Image rectification is a transformation process used to project two-or-more images onto a common image plane. This process has several degrees of freedom and there are many strategies for transforming images to the common plane.
