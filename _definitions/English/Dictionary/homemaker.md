---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/homemaker
offline_file: ""
offline_thumbnail: ""
uuid: 7ed816e7-da6b-40a4-bc89-eb03b949828d
updated: 1484310469
title: homemaker
categories:
    - Dictionary
---
homemaker
     n : a wife who who manages a household while her husband earns
         the family income [syn: {housewife}, {lady of the house},
          {woman of the house}]
