---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eddy_current
offline_file: ""
offline_thumbnail: ""
uuid: 8413f5ae-7832-451a-8043-82d8f164ddf9
updated: 1484308754
title: Eddy current
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Eddy_currents_due_to_magnet.svg.png
tags:
    - Origin of term
    - History
    - Explanation
    - Properties
    - Power dissipation of eddy currents
    - Skin effect
    - Diffusion equation
    - Applications
    - Electromagnetic braking
    - Repulsive effects and levitation
    - Identification of metals
    - Vibration and position sensing
    - Structural testing
    - Side effects
    - Other applications
categories:
    - Electrodynamics
---
Eddy currents (also called Foucault currents[1]) are loops of electrical current induced within conductors by a changing magnetic field in the conductor, due to Faraday's law of induction. Eddy currents flow in closed loops within conductors, in planes perpendicular to the magnetic field. They can be induced within nearby stationary conductors by a time-varying magnetic field created by an AC electromagnet or transformer, for example, or by relative motion between a magnet and a nearby conductor. The magnitude of the current in a given loop is proportional to the strength of the magnetic field, the area of the loop, and the rate of change of flux, and inversely proportional to the ...
