---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dare
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484425801
title: dare
categories:
    - Dictionary
---
dare
     n : a challenge to do something dangerous or foolhardy; "he
         could never refuse a dare" [syn: {daring}]
     v 1: take upon oneself; act presumptuously, without permission;
          "How dare you call my lawyer?" [syn: {make bold}, {presume}]
     2: to be courageous enough to try or do something; "I don't
        dare call him", "she dares to dress differently from the
        others"
     3: challenge; "I dare you!" [syn: {defy}]
