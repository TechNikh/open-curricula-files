---
version: 1
type: video
provider: YouTube
id: czt7qi_EFtk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 56e05f72-30a6-432c-b000-9913ec4fc356
updated: 1486069664
title: 12 Principles of Green Chemistry.m4v
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/czt7qi_EFtk/default.jpg
    - https://i3.ytimg.com/vi/czt7qi_EFtk/1.jpg
    - https://i3.ytimg.com/vi/czt7qi_EFtk/2.jpg
    - https://i3.ytimg.com/vi/czt7qi_EFtk/3.jpg
---
