---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/viable
offline_file: ""
offline_thumbnail: ""
uuid: 039140a6-c2c0-44e3-9aec-82ff400cc740
updated: 1484310411
title: viable
categories:
    - Dictionary
---
viable
     adj 1: capable of being done with means at hand and circumstances
            as they are [syn: {feasible}, {executable}, {practicable},
             {workable}]
     2: capable of life or normal growth and development; "viable
        seeds"; "a viable fetus"
