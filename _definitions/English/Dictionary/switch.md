---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/switch
offline_file: ""
offline_thumbnail: ""
uuid: add4c36a-abf1-4f4c-a1be-b2372cafd5de
updated: 1484310228
title: switch
categories:
    - Dictionary
---
switch
     n 1: control consisting of a mechanical or electrical or
          electronic device for making or breaking or changing the
          connections in a circuit [syn: {electric switch}, {electrical
          switch}]
     2: an event in which one thing is substituted for another; "the
        replacement of lost blood by a transfusion of donor blood"
        [syn: {substitution}, {permutation}, {transposition}, {replacement}]
     3: hairpiece consisting of a tress of false hair; used by women
        to give shape to a coiffure
     4: railroad track having two movable rails and necessary
        connections; used to turn a train from one track to
        another or to store ...
