---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pale
offline_file: ""
offline_thumbnail: ""
uuid: 727abab8-e0d9-4bc5-b4f5-3677f055b77d
updated: 1484310307
title: pale
categories:
    - Dictionary
---
pale
     adj 1: very light colored; highly diluted with white; "pale
            seagreen"; "pale blue eyes"
     2: (of light) lacking in intensity or brightness; dim or
        feeble; "the pale light of a half moon"; "a pale sun";
        "the late afternoon light coming through the el tracks
        fell in pale oblongs on the street"; "a pallid sky"; "the
        pale (or wan) stars"; "the wan light of dawn" [syn: {pallid},
         {wan}]
     3: lacking in vitality or interest or effectiveness; "a pale
        rendition of the aria"; "pale prose with the faint
        sweetness of lavender"; "a pallid performance" [syn: {pallid}]
     4: abnormally deficient in color as suggesting ...
