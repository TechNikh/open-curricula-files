---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/allowance
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484400481
title: allowance
categories:
    - Dictionary
---
allowance
     n 1: an amount allowed or granted (as during a given period);
          "travel allowance"; "my weekly allowance of two eggs";
          "a child's allowance should not be too generous"
     2: a sum granted as reimbursement for expenses
     3: an amount added or deducted on the basis of qualifying
        circumstances; "an allowance for profit" [syn: {adjustment}]
     4: a permissible difference; allowing some freedom to move
        within limits [syn: {leeway}, {margin}, {tolerance}]
     5: a reserve fund created by a charge against profits in order
        to provide for changes in the value of a company's assets
        [syn: {valuation reserve}, {valuation account}, ...
