---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inversive_geometry
offline_file: ""
offline_thumbnail: ""
uuid: a0b6575a-eeaf-4c24-8f1c-ce72f4c72c61
updated: 1484309258
title: Inversive geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Inversion_illustration1.svg.png
tags:
    - Circle inversion
    - Inverse of a point
    - Compass and straightedge construction (point outside circle)
    - Compass and straightedge construction (point inside circle)
    - Properties
    - Application
    - Pole and polar
    - Inversions in three dimensions
    - Example sphere
    - Example cylinder, cone, torus
    - Example spheroid
    - Example hyperboloid of one sheet
    - Stereographic projection as the inversion of a sphere
    - 6-sphere coordinates
    - Axiomatics and generalization
    - Relation to Erlangen program
    - Dilations
    - Reciprocation
    - Higher geometry
    - Inversion in higher dimensions
    - Anticonformal mapping property
    - Inversive geometry and hyperbolic geometry
    - Notes
categories:
    - Inversive geometry
---
In geometry, inversive geometry is the study of those properties of figures that are preserved by a generalization of a type of transformation of the Euclidean plane, called inversion. These transformations preserve angles and map generalized circles into generalized circles, where a generalized circle means either a circle or a line (loosely speaking, a circle with infinite radius). Many difficult problems in geometry become much more tractable when an inversion is applied.
