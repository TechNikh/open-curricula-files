---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/headway
offline_file: ""
offline_thumbnail: ""
uuid: b27d0d50-989d-4200-9492-c0a7354904fb
updated: 1484310479
title: headway
categories:
    - Dictionary
---
headway
     n 1: vertical space available to allow easy passage under
          something [syn: {headroom}, {clearance}]
     2: forward movement; "the ship made little headway against the
        gale" [syn: {head}]
