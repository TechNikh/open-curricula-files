---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/digested
offline_file: ""
offline_thumbnail: ""
uuid: 8d6575ee-1e90-4345-987d-8bd9c10fe4f1
updated: 1484310328
title: digested
categories:
    - Dictionary
---
digested
     adj : capable of undergoing digestion; "a supply of easily
           digested foods"
