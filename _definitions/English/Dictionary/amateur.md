---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amateur
offline_file: ""
offline_thumbnail: ""
uuid: e7c842f3-6a61-4c43-b261-0421f9c0eab3
updated: 1484310218
title: amateur
categories:
    - Dictionary
---
amateur
     adj 1: engaged in as a pastime; "an amateur painter"; "gained
            valuable experience in amateur theatricals";
            "recreational golfers"; "reading matter that is both
            recreational and mentally stimulating"; "unpaid extras
            in the documentary" [syn: {recreational}, {unpaid}]
     2: lacking professional skill or expertise; "a very amateurish
        job"; "inexpert but conscientious efforts"; "an unskilled
        painting" [syn: {amateurish}, {inexpert}, {unskilled}]
     n 1: someone who pursues a study or sport as a pastime
     2: does not play for pay [ant: {professional}]
