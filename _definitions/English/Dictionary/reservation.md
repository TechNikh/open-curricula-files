---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reservation
offline_file: ""
offline_thumbnail: ""
uuid: 2db85c3e-e200-4bca-b50c-50e688711227
updated: 1484310599
title: reservation
categories:
    - Dictionary
---
reservation
     n 1: a district that is reserved for particular purpose [syn: {reserve}]
     2: a statement that limits or restricts some claim; "he
        recommended her without any reservations" [syn: {qualification}]
     3: an unstated doubt that prevents you from accepting something
        wholeheartedly [syn: {mental reservation}, {arriere pensee}]
     4: the act of reserving (a place or passage) or engaging the
        services of (a person or group); "wondered who had made
        the booking" [syn: {booking}]
     5: the written record or promise of an arrangement by which
        accommodations are secured in advance
     6: something reserved in advance (as a hotel ...
