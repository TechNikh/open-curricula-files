---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inc
offline_file: ""
offline_thumbnail: ""
uuid: 549f2b2a-4575-4fe1-a76d-d52a1d6b6bdb
updated: 1484310597
title: inc
categories:
    - Dictionary
---
INC
     n : a heterogeneous collection of groups united in their
         opposition to Saddam Hussein's government of Iraq; formed
         in 1992 it is comprised of Sunni and Shiite Arabs and
         Kurds who hope to build a new government [syn: {Iraqi
         National Congress}]
