---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Incidence_structure
offline_file: ""
offline_thumbnail: ""
uuid: 0a457b17-9f00-47d5-9786-fb7423214617
updated: 1484308582
title: Incidence structure
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fano_plane_with_nimber_labels.svg.png
tags:
    - Formal definition and terminology
    - Examples
    - Dual structure
    - Other terminology
    - Hypergraphs
    - Block designs
    - 'Example: Fano plane'
    - Representations
    - Incidence matrix
    - Pictorial representations
    - Realizability
    - Incidence graph (Levi graph)
    - Levi graph examples
    - Generalization
    - Notes
categories:
    - Combinatorics
---
In mathematics, an abstract system consisting of two types of objects and a single relationship between these types of objects is called an incidence structure. Consider the points and lines of the Euclidean plane as the two types of objects and ignore all the properties of this geometry except for the relation of which points are on which lines for all points and lines. What is left is the incidence structure of the Euclidean plane.
