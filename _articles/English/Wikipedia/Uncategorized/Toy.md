---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Toy-makers
offline_file: ""
offline_thumbnail: ""
uuid: 2041cb97-2928-4b5d-a8ab-b8bcd95d452b
updated: 1484308460
title: Toy
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Wooden_toys_%2528cropped%2529.JPG'
tags:
    - History
    - Enlightenment era
    - Mass market
    - culture
    - Child development
    - Gender
    - Economics
    - Types
    - Construction sets
    - Dolls and miniatures
categories:
    - Uncategorized
---
A toy is an item that is generally used for children's play. Playing with toys is supposed to be an enjoyable means of training young children for life in society. Different materials like wood, clay, paper, and plastic are used to make toys. Many items are designed to serve as toys, but goods produced for other purposes can also be used. For instance, a small child may fold an ordinary piece of paper into an airplane shape and "fly it." Newer forms of toys include interactive digital entertainment. Some toys are produced primarily as collector's items and are intended for display only.
