---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quantify
offline_file: ""
offline_thumbnail: ""
uuid: 4193aa3c-29f3-4034-802e-964263997fce
updated: 1484310202
title: quantify
categories:
    - Dictionary
---
quantify
     v 1: use as a quantifier
     2: express as a number or measure or quantity; "Can you
        quantify your results?" [syn: {measure}]
     [also: {quantified}]
