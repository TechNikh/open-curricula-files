---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phocomelia
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484408582
title: phocomelia
categories:
    - Dictionary
---
phocomelia
     n : an abnormality of development in which the upper part of an
         arm or leg is missing so the hands or feet are attached
         to the body like stumps; rare condition that results from
         taking thalidomide during pregnancy [syn: {seal limbs}]
