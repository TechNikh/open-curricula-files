---
version: 1
type: video
provider: YouTube
id: bPqMLKXoEac
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Who%20versus%20whom.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3ad046e5-69b4-4d3d-9c97-938db53425f4
updated: 1486069603
title: Who versus whom
tags:
    - subject and object pronouns
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/bPqMLKXoEac/default.jpg
    - https://i3.ytimg.com/vi/bPqMLKXoEac/1.jpg
    - https://i3.ytimg.com/vi/bPqMLKXoEac/2.jpg
    - https://i3.ytimg.com/vi/bPqMLKXoEac/3.jpg
---
When do you use who, and when do you use whom? David, KA's grammarian, explains.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/e/relative-pronouns?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/subject-and-object-pronouns-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/relative-pronouns-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
