---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sooty
offline_file: ""
offline_thumbnail: ""
uuid: 850e55ee-a978-41e1-9b72-f9f436ec6607
updated: 1484310423
title: sooty
categories:
    - Dictionary
---
sooty
     adj : of the blackest black; similar to the color of jet or coal
           [syn: {coal-black}, {jet}, {jet-black}, {pitchy}]
     [also: {sootiest}, {sootier}]
