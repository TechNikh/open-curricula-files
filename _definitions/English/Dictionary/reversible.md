---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reversible
offline_file: ""
offline_thumbnail: ""
uuid: 0a55b82f-4ef6-4a89-a264-7e736b9ae535
updated: 1484310426
title: reversible
categories:
    - Dictionary
---
reversible
     adj 1: capable of reversing or being reversed; "reversible
            hypertension" [ant: {irreversible}]
     2: capable of being reversed or used with either side out; "a
        reversible jacket" [syn: {two-sided}] [ant: {nonreversible}]
     3: capable of being reversed; "a reversible decision is one
        that can be appealed or vacated"
     4: capable of assuming or producing either of two states; "a
        reversible chemical reaction"; "a reversible cell"
     n : a garment (especially a coat) that can be worn inside out
         (with either side of the cloth showing)
