---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suspicion
offline_file: ""
offline_thumbnail: ""
uuid: 77f59afb-4542-46ca-be5c-c8a8c919a1ce
updated: 1484310178
title: suspicion
categories:
    - Dictionary
---
suspicion
     n 1: an impression that something might be the case; "he had an
          intuition that something had gone wrong" [syn: {intuition},
           {hunch}]
     2: doubt about someone's honesty [syn: {misgiving}, {mistrust},
         {distrust}]
     3: the state of being suspected; "he tried to shield me from
        suspicion"
     4: being of a suspicious nature; "his suspiciousness destroyed
        his marriage" [syn: {suspiciousness}]
