---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Stanley%E2%80%93Wilf_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 4f27b91a-a3d0-43da-910c-6deb1a7edfcf
updated: 1484308671
title: Stanley–Wilf conjecture
tags:
    - Statement
    - Allowable growth rates
    - Notes
categories:
    - Theorems in discrete mathematics
---
The Stanley–Wilf conjecture, formulated independently by Richard P. Stanley and Herbert Wilf in the late 1980s, states that every permutation pattern defines a set of permutations whose growth rate is singly exponential. It was proved by Adam Marcus and Gábor Tardos (2004) and is no longer a conjecture. Marcus and Tardos actually proved a different conjecture, due to Zoltán Füredi and Péter Hajnal (1992), which had been shown to imply the Stanley–Wilf conjecture by Klazar (2000).
