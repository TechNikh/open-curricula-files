---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/way
offline_file: ""
offline_thumbnail: ""
uuid: f16ee028-7138-4924-970a-2dccaa673a2c
updated: 1484310341
title: way
categories:
    - Dictionary
---
way
     n 1: how something is done or how it happens; "her dignified
          manner"; "his rapid manner of talking"; "their nomadic
          mode of existence"; "in the characteristic New York
          style"; "a lonely way of life"; "in an abrasive fashion"
          [syn: {manner}, {mode}, {style}, {fashion}]
     2: how a result is obtained or an end is achieved; "a means of
        control"; "an example is the best agency of instruction";
        "the true way to success" [syn: {means}, {agency}]
     3: a journey or passage; "they are on the way"
     4: the condition of things generally; "that's the way it is";
        "I felt the same way"
     5: a course of conduct; "the path ...
