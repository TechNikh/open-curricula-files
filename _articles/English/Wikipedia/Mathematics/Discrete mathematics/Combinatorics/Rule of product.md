---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rule_of_product
offline_file: ""
offline_thumbnail: ""
uuid: 7718712c-a7b7-4f3c-a166-6a8d8906ae5e
updated: 1484308580
title: Rule of product
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Multiplication-principle.svg.png
tags:
    - Examples
    - Applications
    - Related concepts
categories:
    - Combinatorics
---
In combinatorics, the rule of product or multiplication principle is a basic counting principle (a.k.a. the fundamental principle of counting). Stated simply, it is the idea that if there are a ways of doing something and b ways of doing another thing, then there are a · b ways of performing both actions.[1][2]
