---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jim_Hoffman
offline_file: ""
offline_thumbnail: ""
uuid: 260264fc-3002-47f2-bf40-67236409f083
updated: 1484309085
title: Jim Hoffman
tags:
    - September 11, 2001 attacks
    - Background
    - Publications
    - Audio
categories:
    - Geometers
---
Jim Hoffman is a website engineer in Oakland, California, who created several web sites about the September 11, 2001 attacks that analyze and suggest alternative accounts for the events of that day. His primary website, 9-11 Research,[1] serves as an archive of documentation and alternative analyses about the attacks.[2] Hoffman has also written numerous technical essays[3] which focus on the World Trade Center controlled demolition hypothesis.
