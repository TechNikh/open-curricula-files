---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/morgan
offline_file: ""
offline_thumbnail: ""
uuid: c3909248-d143-4567-a316-bf624f31500c
updated: 1484310295
title: morgan
categories:
    - Dictionary
---
Morgan
     n 1: United States anthropologist who studied the Seneca
          (1818-1881) [syn: {Lewis Henry Morgan}]
     2: United States biologist who formulated the chromosome theory
        of heredity (1866-1945) [syn: {Thomas Hunt Morgan}]
     3: a Welsh buccaneer who raided Spanish colonies in the West
        Indies for the English (1635-1688) [syn: {Henry Morgan}, {Sir
        Henry Morgan}]
     4: soldier in the American Revolution who defeated the British
        in the Battle of Cowpens, South Carolina (1736-1802) [syn:
         {Daniel Morgan}]
     5: United States financier and philanthropist (1837-1913) [syn:
         {J. P. Morgan}, {John Pierpont Morgan}]
     6: an ...
