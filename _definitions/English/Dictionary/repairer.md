---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repairer
offline_file: ""
offline_thumbnail: ""
uuid: 2d0811ff-d224-43c4-9bf6-2b9d210bfc37
updated: 1484310206
title: repairer
categories:
    - Dictionary
---
repairer
     n : a skilled worker whose job is to repair things [syn: {repairman},
          {maintenance man}, {service man}, {fixer}]
