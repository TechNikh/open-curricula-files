---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regard
offline_file: ""
offline_thumbnail: ""
uuid: 8b31b607-e271-4d27-9189-3bc85dbb7b1e
updated: 1484310480
title: regard
categories:
    - Dictionary
---
regard
     n 1: (usually preceded by `in') a detail or point; "it differs in
          that respect" [syn: {respect}]
     2: paying particular notice (as to children or helpless
        people); "his attentiveness to her wishes"; "he spends
        without heed to the consequences" [syn: {attentiveness}, {heed},
         {paying attention}] [ant: {inattentiveness}]
     3: (usually plural) a polite expression of desire for someone's
        welfare; "give him my kind regards"; "my best wishes"
        [syn: {wish}, {compliments}]
     4: a long fixed look; "he fixed his paternal gaze on me" [syn:
        {gaze}]
     5: the condition of being honored (esteemed or respected or
        well ...
