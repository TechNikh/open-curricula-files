---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/install
offline_file: ""
offline_thumbnail: ""
uuid: 74678561-7ac1-432c-8597-5e3efe2ea4d3
updated: 1484310236
title: install
categories:
    - Dictionary
---
install
     v 1: set up for use; "install the washer and dryer"; "We put in a
          new sink" [syn: {instal}, {put in}, {set up}]
     2: put into an office or a position; "the new president was
        installed immediately after the election"
     3: place; "Her manager had set her up at the Ritz" [syn: {instal},
         {set up}, {establish}]
