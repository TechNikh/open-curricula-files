---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Excitable_medium
offline_file: ""
offline_thumbnail: ""
uuid: 8c5329b2-5355-47f9-bdf4-2e18f8e93a16
updated: 1484308816
title: Excitable medium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-ExcitableCellularAutomaton.gif
tags:
    - Modelling excitable media
    - With cellular automata
    - Geometries of waves
    - One-dimensional waves
    - Two-dimensional waves
    - Notes
categories:
    - Nonlinear systems
---
An excitable medium is a nonlinear dynamical system which has the capacity to propagate a wave of some description, and which cannot support the passing of another wave until a certain amount of time has passed (known as the refractory time).
