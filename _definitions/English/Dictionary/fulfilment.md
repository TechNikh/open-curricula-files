---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fulfilment
offline_file: ""
offline_thumbnail: ""
uuid: 16fa5ccc-6e53-44f1-bc09-0185639fa283
updated: 1484310607
title: fulfilment
categories:
    - Dictionary
---
fulfilment
     n 1: a feeling of satisfaction at having achieved your desires
          [syn: {fulfillment}]
     2: the act of consummating something (a desire or promise etc)
        [syn: {fulfillment}]
