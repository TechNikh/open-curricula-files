---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/union
offline_file: ""
offline_thumbnail: ""
uuid: c742ce9c-fc8f-43ae-a439-ebbd74008366
updated: 1484310238
title: union
categories:
    - Dictionary
---
Union
     adj 1: being of or having to do with the northern United States and
            those loyal to the Union during the Civil War; "Union
            soldiers"; "Federal forces"; "a Federal infantryman"
            [syn: {Federal}]
     2: of trade unions; "the union movement"; "union negotiations";
        "a union-shop clause in the contract" [ant: {nonunion}]
     n 1: an organization of employees formed to bargain with the
          employer; "you have to join the union in order to get a
          job" [syn: {labor union}, {trade union}, {trades union},
           {brotherhood}]
     2: the United States (especially the northern states during the
        American Civil War); "he ...
