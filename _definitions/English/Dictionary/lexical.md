---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lexical
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484585341
title: lexical
categories:
    - Dictionary
---
lexical
     adj 1: of or relating to words; "lexical decision task"
     2: of or relating to dictionaries
