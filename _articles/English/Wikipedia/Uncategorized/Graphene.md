---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graphene
offline_file: ""
offline_thumbnail: ""
uuid: 68750239-05a2-4c38-ba39-9d78c380e925
updated: 1484308392
title: Graphene
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Graphen.jpg
tags:
    - Definition
    - History
    - Properties
    - Structure
    - Chemical
    - Electronic
    - Electronic spectrum
    - Dispersion relation
    - Single-atom wave propagation
    - Electron transport
    - Anomalous quantum Hall effect
    - Strong magnetic fields
    - Casimir effect
    - Van der Waals force
    - "'Massive' electrons"
    - Optical
    - Saturable absorption
    - Nonlinear Kerr effect
    - Excitonic
    - Stability
    - Thermal conductivity
    - Mechanical
    - Fracture toughness
    - Spin transport
    - Strong magnetic fields
    - Magnetic
    - Biological
    - Support Substrate
    - Forms
    - Monolayer sheets
    - Bilayer graphene
    - Graphene superlattices
    - Graphene nanoribbons
    - Graphene quantum dots
    - Graphene oxide
    - Chemical modification
    - Graphene ligand/complex
    - Graphene fiber
    - 3D graphene
    - Pillared graphene
    - Reinforced graphene
    - Molded graphene
    - Graphene aerogel
    - Graphene nanocoil
    - Production
    - Exfoliation
    - Molten salts
    - Electrochemical synthesis
    - Hydrothermal self-assembly
    - Chemical vapor deposition
    - Epitaxy
    - Metal substrates
    - Sodium ethoxide pyrolysis
    - Roll-to-roll
    - Cold wall
    - Wafer scale CVD graphene
    - Nanotube slicing
    - Carbon dioxide reduction
    - Spin coating
    - Supersonic spray
    - Laser
    - Microwave-assisted oxidation
    - Ion implantation
    - Graphene analogs
    - Applications
    - Health risks
    - Sources
categories:
    - Uncategorized
---
Graphene (/ˈɡræf.iːn/)[1][2] is an allotrope of carbon in the form of a two-dimensional, atomic-scale, hexagonal lattice in which one atom forms each vertex. It is the basic structural element of other allotropes, including graphite, charcoal, carbon nanotubes and fullerenes. It can also be considered as an indefinitely large aromatic molecule, the ultimate case of the family of flat polycyclic aromatic hydrocarbons.
