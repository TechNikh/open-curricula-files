---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Askold_Khovanskii
offline_file: ""
offline_thumbnail: ""
uuid: c1d371b5-4e6b-43d7-be37-bc4183239e56
updated: 1484309090
title: Askold Khovanskii
categories:
    - Geometers
---
Askold Georgievich Khovanskii (Russian: Аскольд Георгиевич Хованский; born 3 June 1947, Moscow) is a Russian and Canadian mathematician currently a professor of mathematics at the University of Toronto, Canada.[1] His areas of research are algebraic geometry, commutative algebra, singularity theory, differential geometry and differential equations. His research is in the development of the theory of toric varieties and Newton polyhedra in algebraic geometry. He is also the inventor of the theory of fewnomials.
