---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/simile
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484544901
title: simile
categories:
    - Dictionary
---
simile
     n : a figure of speech that expresses a resemblance between
         things of different kinds (usually formed with `like' or
         `as')
