---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contained
offline_file: ""
offline_thumbnail: ""
uuid: 1aca2996-8ecf-4c1d-adc2-5802a8ed759d
updated: 1484310202
title: contained
categories:
    - Dictionary
---
contained
     adj 1: controlled; "striking with contained ferocity at my head"-
            R.L.Stevenson
     2: gotten under control; "the oil spill is contained"
