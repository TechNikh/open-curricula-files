---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bogomolov%E2%80%93Miyaoka%E2%80%93Yau_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: 16ea25e6-4191-44d8-950d-0118f60d2923
updated: 1484309204
title: Bogomolov–Miyaoka–Yau inequality
categories:
    - Differential geometry
---
between Chern numbers of compact complex surfaces of general type. Its major interest is the way it restricts the possible topological types of the underlying real 4-manifold. It was proved independently by S.-T. Yau (1977, 1978) and Yoichi Miyaoka (1977), after Van de Ven (1966) and Fedor Bogomolov (1978) proved weaker versions with the constant 3 replaced by 8 and 4.
