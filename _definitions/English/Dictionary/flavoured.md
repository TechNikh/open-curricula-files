---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flavoured
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484393221
title: flavoured
categories:
    - Dictionary
---
flavoured
     adj : having been given flavor (as by seasoning) [syn: {flavored},
            {seasoned}]
