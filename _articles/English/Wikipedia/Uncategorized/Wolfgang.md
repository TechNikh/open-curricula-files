---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wolfgang
offline_file: ""
offline_thumbnail: ""
uuid: 064b9fa8-690a-42a2-9e3d-de9f4920e879
updated: 1484308380
title: Wolfgang
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Michael_Pacher_004.jpg
tags:
    - Notable people
    - Places
categories:
    - Uncategorized
---
Wolfgang is a German male given name traditionally popular in Germany and Austria. The name is a combination of the Old High German word wulf, meaning "wolf" and gang, meaning "path, journey". The Old High German "wulf" occurs in names as the prefixes "wulf" and "wolf", as well as the suffixes "ulf" and "olf". "Wulf" is a popular element of the common dithematic German names. This is likely due the ancient reverence of the wolf as a strong, predatorial animal, also revered for its beauty. Names that contain this word also reference to Odin's wolves, Geri and Freki, as well as the apocalyptic Fenrir, and occurs in hundreds of German names. This theme exists in other names such as Adolf, ...
