---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bruce_Reed_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 0f5e5e54-6f77-417c-bd40-be0377648e74
updated: 1484309472
title: Bruce Reed (mathematician)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Bruce_Reed%252C_Bellairs_2015.jpg'
tags:
    - Academic career
    - Research
    - Selected publications
    - Articles
    - Books
categories:
    - Graph theorists
---
Bruce Alan Reed FRSC is a Canadian mathematician and computer scientist, the Canada Research Chair in Graph Theory and a professor of computer science at McGill University. His research is primarily in graph theory.[1]
