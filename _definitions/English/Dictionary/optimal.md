---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/optimal
offline_file: ""
offline_thumbnail: ""
uuid: 0abbc75c-6783-4e4f-a80a-59b7d54ee5db
updated: 1484310387
title: optimal
categories:
    - Dictionary
---
optimal
     adj : most desirable possible under a restriction expressed or
           implied; "an optimum return on capital"; "optimal
           concentration of a drug" [syn: {optimum}]
