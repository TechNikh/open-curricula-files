---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adrenal_gland
offline_file: ""
offline_thumbnail: ""
uuid: 6df8166a-ca38-41ce-bbca-403978723059
updated: 1484308372
title: Adrenal gland
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Illu_adrenal_gland.jpg
tags:
    - Structure
    - Cortex
    - Zona glomerulosa
    - Zona fasciculata
    - Zona reticularis
    - Medulla
    - Blood supply
    - Variability
    - Function
    - Corticosteroids
    - Adrenaline and noradrenaline
    - Androgens
    - Development
    - Cortex
    - Adrenarche
    - Medulla
    - Clinical significance
    - Corticosteroid overproduction
    - "Cushing's syndrome"
    - Primary aldosteronism
    - Adrenal insufficiency
    - "Addison's disease"
    - Secondary adrenal insufficiency
    - Congenital adrenal hyperplasia
    - Adrenal tumors
    - History
categories:
    - Uncategorized
---
The adrenal glands (also known as suprarenal glands) are endocrine glands that produce a variety of hormones including adrenaline and the steroids aldosterone and cortisol.[1][2] They are found above the kidneys. Each gland has an outer cortex which produces steroid hormones and an inner medulla. The adrenal cortex itself is divided into three zones: zona glomerulosa, the zona fasciculata and the zona reticularis.[3]
