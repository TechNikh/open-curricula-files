---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wives
offline_file: ""
offline_thumbnail: ""
uuid: b992e956-e3f4-4042-8323-40bf7748a9cc
updated: 1484310148
title: wives
categories:
    - Dictionary
---
wife
     n : a married woman; a man's partner in marriage [syn: {married
         woman}] [ant: {husband}]
     [also: {wives} (pl)]
