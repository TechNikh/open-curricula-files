---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adored
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484556121
title: adored
categories:
    - Dictionary
---
adored
     adj : regarded with deep or rapturous love (especially as if for a
           god); "adored grandchildren"; "an idolized wife" [syn:
           {idolized}, {idolised}, {worshipped(a)}]
