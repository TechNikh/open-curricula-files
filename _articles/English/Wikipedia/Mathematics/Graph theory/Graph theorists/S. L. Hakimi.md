---
version: 1
type: article
id: https://en.wikipedia.org/wiki/S._L._Hakimi
offline_file: ""
offline_thumbnail: ""
uuid: 4ddd390a-0c93-497e-a2bb-892da51e047e
updated: 1484309466
title: S. L. Hakimi
categories:
    - Graph theorists
---
Seifollah Louis Hakimi is an Iranian-American mathematician born in Iran, a professor emeritus at Northwestern University, where he chaired the department of electrical engineering from 1973 to 1978.[1]
