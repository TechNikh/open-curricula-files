---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Solar_compass
offline_file: ""
offline_thumbnail: ""
uuid: dce53a5f-2877-40ea-8b40-d5a36c7950b1
updated: 1484309300
title: Solar compass
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-William_Burt_Solar_Compass.jpg
tags:
    - History
    - Description
    - Sun compass
    - Sources
categories:
    - Orientation (geometry)
---
The solar compass, a surveying instrument that makes use of the sun's direction, was first invented and made by William Austin Burt.[1] He patented it on February 25, 1836, in the United States Patent Office as No 9428X.[2] It received a medal at the Great Exhibition of 1851.[3]
