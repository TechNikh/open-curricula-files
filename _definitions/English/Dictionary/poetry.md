---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poetry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484469481
title: poetry
categories:
    - Dictionary
---
poetry
     n 1: literature in metrical form [syn: {poesy}, {verse}]
     2: any communication resembling poetry in beauty or the
        evocation of feeling
