---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/margarine
offline_file: ""
offline_thumbnail: ""
uuid: 23e94de4-57cd-4979-825a-0d592ddc0158
updated: 1484310384
title: margarine
categories:
    - Dictionary
---
margarine
     n : a spread made chiefly from vegetable oils and used as a
         substitute for butter [syn: {margarin}, {oleo}, {oleomargarine},
          {marge}]
