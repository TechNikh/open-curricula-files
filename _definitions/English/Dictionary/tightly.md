---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tightly
offline_file: ""
offline_thumbnail: ""
uuid: 44f847dd-71e2-415c-b41b-d058d1dcbc46
updated: 1484310224
title: tightly
categories:
    - Dictionary
---
tightly
     adv 1: in a tight or constricted manner; "a tightly packed pub"
     2: securely fixed or fastened; "the window was tightly sealed"
