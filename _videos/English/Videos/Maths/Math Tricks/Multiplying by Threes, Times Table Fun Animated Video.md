---
version: 1
type: video
provider: YouTube
id: X-3Sl0aohQc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 82d101c5-24c5-4d8e-abfb-b1c1ccec314f
updated: 1486069625
title: Multiplying by Threes, Times Table Fun Animated Video
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/X-3Sl0aohQc/default.jpg
    - https://i3.ytimg.com/vi/X-3Sl0aohQc/1.jpg
    - https://i3.ytimg.com/vi/X-3Sl0aohQc/2.jpg
    - https://i3.ytimg.com/vi/X-3Sl0aohQc/3.jpg
---
