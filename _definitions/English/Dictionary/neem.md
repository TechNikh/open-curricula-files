---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neem
offline_file: ""
offline_thumbnail: ""
uuid: ee605105-39c1-4447-afe2-9603559cb876
updated: 1484310307
title: neem
categories:
    - Dictionary
---
neem
     n : large semi-evergreen tree of the East Indies; trunk exudes a
         tenacious gum; bitter bark used as a tonic; seeds yield
         an aromatic oil; sometimes placed in genus Melia [syn: {neem
         tree}, {nim tree}, {margosa}, {arishth}, {Azadirachta
         indica}, {Melia Azadirachta}]
