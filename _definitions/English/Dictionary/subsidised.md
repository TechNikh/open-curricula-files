---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subsidised
offline_file: ""
offline_thumbnail: ""
uuid: 71007e32-0a2d-4da5-9bb8-5c22505b3400
updated: 1484310533
title: subsidised
categories:
    - Dictionary
---
subsidised
     adj : having partial financial support from public funds; "lived
           in subsidized public housing" [syn: {subsidized}]
