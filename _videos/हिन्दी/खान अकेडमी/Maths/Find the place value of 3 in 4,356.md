---
version: 1
type: video
provider: YouTube
id: RELtyavCM3E
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e7f45125-8278-41d0-a56a-2821807d4a35
updated: 1486069607
title: Find the place value of 3 in 4,356
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/RELtyavCM3E/default.jpg
    - https://i3.ytimg.com/vi/RELtyavCM3E/1.jpg
    - https://i3.ytimg.com/vi/RELtyavCM3E/2.jpg
    - https://i3.ytimg.com/vi/RELtyavCM3E/3.jpg
---
