---
version: 1
type: video
provider: YouTube
id: 1CMDS4-PKKQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Partial%20derivatives.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 84ff629b-2340-4c2b-8793-e72adbefb3f1
updated: 1486069690
title: Partial derivatives
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/1CMDS4-PKKQ/default.jpg
    - https://i3.ytimg.com/vi/1CMDS4-PKKQ/1.jpg
    - https://i3.ytimg.com/vi/1CMDS4-PKKQ/2.jpg
    - https://i3.ytimg.com/vi/1CMDS4-PKKQ/3.jpg
---
