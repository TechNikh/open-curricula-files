---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hockey
offline_file: ""
offline_thumbnail: ""
uuid: 6e0ab2e6-71c7-44e8-b683-4398b32c4193
updated: 1484310138
title: hockey
categories:
    - Dictionary
---
hockey
     n 1: hockey played on a field; two opposing teams use curved
          sticks to drive a ball into the opponents' net [syn: {field
          hockey}]
     2: a game played on an ice rink by two opposing teams of 6
        skaters each who try to knock a flat round puck into the
        opponents' goal with hockey sticks [syn: {ice hockey}, {hockey
        game}]
