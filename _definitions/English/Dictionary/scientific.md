---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scientific
offline_file: ""
offline_thumbnail: ""
uuid: 337fd068-e5e3-4271-8ed3-ceb91eaf3f13
updated: 1484310311
title: scientific
categories:
    - Dictionary
---
scientific
     adj 1: of or relating to the practice of science; "scientific
            journals"
     2: conforming with the principles or methods used in science;
        "a scientific approach" [ant: {unscientific}]
