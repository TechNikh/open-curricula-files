---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/capitalist
offline_file: ""
offline_thumbnail: ""
uuid: fcb3c396-cb34-4f9e-bb90-941999f80f4f
updated: 1484310551
title: capitalist
categories:
    - Dictionary
---
capitalist
     adj 1: of or relating to capitalism or capitalists; "a capitalist
            nation"; "capitalistic methods and incentives" [syn: {capitalistic}]
     2: favoring or practicing capitalism [syn: {capitalistic}]
        [ant: {socialistic}]
     n 1: a conservative advocate of capitalism
     2: a person who invests capital in a business (especially a
        large business)
