---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/declared
offline_file: ""
offline_thumbnail: ""
uuid: cbb91969-9b12-4145-be90-1cf00bd94b5a
updated: 1484310533
title: declared
categories:
    - Dictionary
---
declared
     adj 1: made known or openly avowed; "their declared and their
            covert objectives"; "a declared liberal" [ant: {undeclared}]
     2: declared as fact; explicitly stated [syn: {stated}]
