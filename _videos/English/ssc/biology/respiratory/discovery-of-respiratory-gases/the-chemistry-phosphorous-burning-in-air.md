---
title: The Chemistry - Phosphorous Burning in Air
youtube_id: DwIRQBKj3ms
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - discovery-of-respiratory-gases
---
