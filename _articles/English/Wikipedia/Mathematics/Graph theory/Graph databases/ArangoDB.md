---
version: 1
type: article
id: https://en.wikipedia.org/wiki/ArangoDB
offline_file: ""
offline_thumbnail: ""
uuid: fb35dcfe-aeb7-4082-a4e1-59f2ef131b47
updated: 1484309404
title: ArangoDB
categories:
    - Graph databases
---
ArangoDB is a NoSQL database developed by triAGENS GmbH. It has been referred to as the most popular NoSQL database available that has an open source license.[1] Its creators refer to it as a "native multi-model"[2] database to indicate that it was designed specifically to allow key/value, document, and graph data to be stored together and queried with a common language.[3]
