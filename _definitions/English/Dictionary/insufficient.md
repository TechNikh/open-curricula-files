---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insufficient
offline_file: ""
offline_thumbnail: ""
uuid: ae3564ff-8d20-4c4f-8923-4e83cd9b442f
updated: 1484310484
title: insufficient
categories:
    - Dictionary
---
insufficient
     adj : of a quantity not able to fulfill a need or requirement;
           "insufficient funds" [syn: {deficient}] [ant: {sufficient}]
