---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/FitzHugh%E2%80%93Nagumo_model'
offline_file: ""
offline_thumbnail: ""
uuid: fe2f787a-d8fa-4f74-8cb6-bfaf88998b09
updated: 1484308819
title: FitzHugh–Nagumo model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fitzhugh_Nagumo_Model_Time_Domain_Graph.png
categories:
    - Nonlinear systems
---
The FitzHugh–Nagumo model (FHN), named after Richard FitzHugh (1922 – 2007) who suggested the system in 1961 and J. Nagumo et al. who created the equivalent circuit the following year, describes a prototype of an excitable system (e.g., a neuron).
