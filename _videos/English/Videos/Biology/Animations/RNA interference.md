---
version: 1
type: video
provider: YouTube
id: C-pTiFkg1aU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b971342e-8e15-4bb9-99fd-057030b3777e
updated: 1486069672
title: RNA interference
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/C-pTiFkg1aU/default.jpg
    - https://i3.ytimg.com/vi/C-pTiFkg1aU/1.jpg
    - https://i3.ytimg.com/vi/C-pTiFkg1aU/2.jpg
    - https://i3.ytimg.com/vi/C-pTiFkg1aU/3.jpg
---
