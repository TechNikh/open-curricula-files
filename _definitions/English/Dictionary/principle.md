---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/principle
offline_file: ""
offline_thumbnail: ""
uuid: b0c6928e-53a1-432b-92ef-b4da771a82c9
updated: 1484310228
title: principle
categories:
    - Dictionary
---
principle
     n 1: a basic generalization that is accepted as true and that can
          be used as a basis for reasoning or conduct; "their
          principles of composition characterized all their works"
          [syn: {rule}]
     2: a rule or standard especially of good behavior; "a man of
        principle"; "he will not violate his principles"
     3: a basic truth or law or assumption; "the principles of
        democracy"
     4: a rule or law concerning a natural phenomenon or the
        function of a complex system; "the principle of the
        conservation of mass"; "the principle of jet propulsion";
        "the right-hand rule for inductive fields" [syn: {rule}]
     5: ...
