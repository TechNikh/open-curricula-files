---
version: 1
type: video
provider: YouTube
id: tkvI370InQk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 050deab9-e054-414b-b868-b1716e5668ce
updated: 1486069605
title: Finding inscribed angle measure
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/tkvI370InQk/default.jpg
    - https://i3.ytimg.com/vi/tkvI370InQk/1.jpg
    - https://i3.ytimg.com/vi/tkvI370InQk/2.jpg
    - https://i3.ytimg.com/vi/tkvI370InQk/3.jpg
---
