---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invited
offline_file: ""
offline_thumbnail: ""
uuid: ff14f5d8-e740-4615-85d5-0ca3e030afd1
updated: 1484310551
title: invited
categories:
    - Dictionary
---
invited
     adj 1: done by invitation [ant: {uninvited}]
     2: freely permitted or invited; "invited guests" [syn: {wanted}]
