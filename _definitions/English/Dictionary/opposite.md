---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/opposite
offline_file: ""
offline_thumbnail: ""
uuid: 83575994-3af1-4a13-a757-c34b082479c7
updated: 1484310309
title: opposite
categories:
    - Dictionary
---
opposite
     adj 1: being directly across from each other; facing; "And I on the
            opposite shore will be, ready to ride and spread the
            alarm"- Longfellow; "we lived on opposite sides of the
            street"; "at opposite poles"
     2: of leaves etc; growing in pairs on either side of a stem;
        "opposite leaves" [syn: {paired}] [ant: {alternate}]
     3: moving or facing away from each other; "looking in opposite
        directions"; "they went in opposite directions"
     4: the other one of a complementary pair; "the opposite sex";
        "the two chess kings are set up on squares of opposite
        colors"
     5: altogether different in nature or ...
