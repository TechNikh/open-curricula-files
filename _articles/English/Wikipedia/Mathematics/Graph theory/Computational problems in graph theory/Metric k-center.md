---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metric_k-center
offline_file: ""
offline_thumbnail: ""
uuid: 7a178b0c-30a0-4928-b45d-541f96bad0ea
updated: 1484309392
title: Metric k-center
tags:
    - Formal definition
    - Computational complexity
    - Approximations
    - Notes
categories:
    - Computational problems in graph theory
---
In graph theory, the metric k-center or metric facility location problem is a combinatorial optimization problem studied in theoretical computer science. Given n cities with specified distances, one wants to build k warehouses in different cities and minimize the maximum distance of a city to a warehouse. In graph theory this means finding a set of k vertices for which the largest distance of any point to its closest vertex in the k-set is minimum. The vertices must be in a metric space, or in other words a complete graph that satisfies the triangle inequality.
