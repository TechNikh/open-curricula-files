---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/childhood
offline_file: ""
offline_thumbnail: ""
uuid: dc62607a-a0e4-4a73-907a-e5893ba8db13
updated: 1484310484
title: childhood
categories:
    - Dictionary
---
childhood
     n 1: the time of person's life when they are a child
     2: the state of a child between infancy and adolescence [syn: {puerility}]
