---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaos:_Making_a_New_Science
offline_file: ""
offline_thumbnail: ""
uuid: 397b5d79-7ab4-47a6-a3f8-01147a3a146d
updated: 1484308705
title: 'Chaos: Making a New Science'
tags:
    - Overview
    - Reception
categories:
    - Chaos theory
---
Chaos: Making a New Science is a debut non-fiction book by James Gleick that initially introduced the principles and early development of the chaos theory to the public.[1] It was a finalist for the National Book Award[2] and the Pulitzer Prize[3] in 1987, and was shortlisted for the Science Book Prize in 1989.[4] The book was published on October 29, 1987 by Viking Books.
