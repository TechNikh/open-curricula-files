---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cartan_connection
offline_file: ""
offline_thumbnail: ""
uuid: ade488ba-ee18-4be5-9b12-69feb598986a
updated: 1484309204
title: Cartan connection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Model_space_development.svg.png
tags:
    - Introduction
    - Motivation
    - Affine connections
    - Klein geometries as model spaces
    - Cartan connections and pseudogroups
    - Formal definition
    - Definition via gauge transitions
    - Definition via absolute parallelism
    - Cartan connections as principal connections
    - Definition by an Ehresmann connection
    - Special Cartan connections
    - Reductive Cartan connections
    - Parabolic Cartan connections
    - Associated differential operators
    - Covariant differentiation
    - The fundamental or universal derivative
    - Notes
    - Books
categories:
    - Differential geometry
---
In the mathematical field of differential geometry, a Cartan connection is a flexible generalization of the notion of an affine connection. It may also be regarded as a specialization of the general concept of a principal connection, in which the geometry of the principal bundle is tied to the geometry of the base manifold using a solder form. Cartan connections describe the geometry of manifolds modelled on homogeneous spaces.
