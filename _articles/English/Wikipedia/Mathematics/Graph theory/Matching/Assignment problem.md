---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Assignment_problem
offline_file: ""
offline_thumbnail: ""
uuid: 22ef8742-c985-4e39-a7c1-e3c7076b5eaf
updated: 1484309426
title: Assignment problem
tags:
    - Algorithms and generalizations
    - Example
    - Formal mathematical definition
categories:
    - Matching
---
The assignment problem is one of the fundamental combinatorial optimization problems in the branch of optimization or operations research in mathematics. It consists of finding a maximum weight matching (or minimum weight perfect matching) in a weighted bipartite graph.
