---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frostman_lemma
offline_file: ""
offline_thumbnail: ""
uuid: 288b0200-c130-4bf9-839b-4d9436a53407
updated: 1484309265
title: Frostman lemma
categories:
    - Metric geometry
---
In mathematics, and more specifically, in the theory of fractal dimensions, Frostman's lemma provides a convenient tool for estimating the Hausdorff dimension of sets.
