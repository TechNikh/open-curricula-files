---
version: 1
type: video
provider: YouTube
id: xazQRcSCRaY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5549e17b-8a3b-497c-ad8e-1e5eb2b10150
updated: 1486069665
title: 'The 2,400-year search for the atom - Theresa Doud'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/xazQRcSCRaY/default.jpg
    - https://i3.ytimg.com/vi/xazQRcSCRaY/1.jpg
    - https://i3.ytimg.com/vi/xazQRcSCRaY/2.jpg
    - https://i3.ytimg.com/vi/xazQRcSCRaY/3.jpg
---
