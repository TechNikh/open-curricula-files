---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pituitary_gland
offline_file: ""
offline_thumbnail: ""
uuid: 5ac57ceb-18f0-4834-960e-b7738f55d716
updated: 1484308368
title: Pituitary gland
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray1180.png
tags:
    - Structure
    - Anterior
    - Posterior
    - Function
    - Anterior
    - Intermediate
    - Posterior
    - Hormones
    - Clinical significance
    - Etymology
    - Pituitary gland
    - Hypophysis
    - Other animals
    - Intermediate lobe
    - Additional images
categories:
    - Uncategorized
---
In vertebrate anatomy, the pituitary gland, or hypophysis, is an endocrine gland about the size of a pea and weighing 0.5 grams (0.018 oz) in humans. It is a protrusion off the bottom of the hypothalamus at the base of the brain. The hypophysis rests upon the hypophysial fossa of the sphenoid bone in the center of the middle cranial fossa and is surrounded by a small bony cavity (sella turcica) covered by a dural fold (diaphragma sellae).[2] The anterior pituitary (or adenohypophysis) is a lobe of the gland that regulates several physiological processes (including stress, growth, reproduction, and lactation). The intermediate lobe synthesizes and secretes melanocyte-stimulating hormone. ...
