---
version: 1
type: video
provider: YouTube
id: IXmZI5Iw-wM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 030761f4-db52-458b-9666-8cc8d7e7f6bd
updated: 1486069653
title: CG4u.net Physics Topics in Animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/IXmZI5Iw-wM/default.jpg
    - https://i3.ytimg.com/vi/IXmZI5Iw-wM/1.jpg
    - https://i3.ytimg.com/vi/IXmZI5Iw-wM/2.jpg
    - https://i3.ytimg.com/vi/IXmZI5Iw-wM/3.jpg
---
