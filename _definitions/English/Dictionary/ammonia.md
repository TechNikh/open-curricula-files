---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ammonia
offline_file: ""
offline_thumbnail: ""
uuid: ecd05381-2bdb-4d79-852d-3f76cbb0cf09
updated: 1484310383
title: ammonia
categories:
    - Dictionary
---
ammonia
     n 1: a pungent gas compounded of nitrogen and hydrogen (NH3)
     2: a water solution of ammonia [syn: {ammonia water}, {ammonium
        hydroxide}]
