---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Points_of_the_compass
offline_file: ""
offline_thumbnail: ""
uuid: 725db055-2584-4ed7-ae40-3eee71a1f02e
updated: 1484309297
title: Points of the compass
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Compass_Card_3.png
tags:
    - Compass point names
    - 16-wind compass rose
    - 32-wind compass points
    - Traditional names
    - 32 cardinal points
    - 'Half- and quarter-points'
categories:
    - Orientation (geometry)
---
The points of the compass, specifically on the compass rose, mark divisions of a compass into the four cardinal directions — north, south, east, and west — and often into subdivisions as well. The number of points may be only the four cardinal points, or the eight principal points adding the intercardinal (or ordinal) directions northeast (NE), southeast (SE), southwest (SW), and northwest (NW). In meteorological usage further intermediate points are added to give the sixteen points of a wind compass.[1] Finally, at the most complete in European tradition, are found the full thirty-two points of the mariner's compass.[2] In ancient China 24 points of the compass were used, measuring ...
