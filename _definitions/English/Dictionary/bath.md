---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bath
offline_file: ""
offline_thumbnail: ""
uuid: 83df194c-c9de-4082-bfb3-f4001db15dc2
updated: 1484310228
title: bath
categories:
    - Dictionary
---
bath
     n 1: a vessel containing liquid in which something is immersed
          (as to process it or to maintain it at a constant
          temperature or to lubricate it); "she soaked the etching
          in an acid bath"
     2: you soak your body in a bathtub; "he has a good bath every
        morning"
     3: a relatively large open container that you fill with water
        and use to wash the body [syn: {bathtub}, {bathing tub}, {tub}]
     4: an ancient Hebrew liquid measure equal to about 10 gallons
     5: a town in southwestern England on the River Avon; famous for
        its hot springs and Roman remains
     6: a room (as in a residence) containing a bath or shower and
     ...
