---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/first
offline_file: ""
offline_thumbnail: ""
uuid: ccd4b4e0-2351-4e9c-bb89-28cb52a170f3
updated: 1484310353
title: first
categories:
    - Dictionary
---
first
     adj 1: preceding all others in time or space or degree; "the first
            house on the right"; "the first day of spring"; "his
            first political race"; "her first baby"; "the first
            time"; "the first meetings of the new party"; "the
            first phase of his training" [ant: {last}, {intermediate}]
     2: indicating the beginning unit in a series [syn: {1st}]
     3: serving to set in motion; "the magazine's inaugural issue";
        "the initiative phase in the negotiations"; "an initiatory
        step toward a treaty"; "his first (or maiden) speech in
        Congress"; "the liner's maiden voyage" [syn: {inaugural},
        {initiative}, ...
