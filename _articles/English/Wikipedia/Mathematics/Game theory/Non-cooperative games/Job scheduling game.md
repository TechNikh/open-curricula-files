---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Job_scheduling_game
offline_file: ""
offline_thumbnail: ""
uuid: 3c6bf3d2-b4b8-4e3d-95d5-7e12dc97c828
updated: 1484309052
title: Job scheduling game
tags:
    - Definition
    - Motivation
    - Main properties
    - >
        For every job scheduling game price of stability is equal to
        1
    - >
        There exist job scheduling game where Price of anarchy is
        not bounded
categories:
    - Non-cooperative games
---
In game theory, a job scheduling game is a game that models a scenario in which multiple selfish users wish to utilize multiple processing machines. Each user has a single job, and he needs to choose a single machine to process it. The incentive of each user is to have his job run as fast as possible.
