---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/excerpt
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484484481
title: excerpt
categories:
    - Dictionary
---
excerpt
     n : a passage selected from a larger work; "he presented
         excerpts from William James' philosophical writings"
         [syn: {extract}, {selection}]
     v : take out of a literary work in order to cite or copy [syn: {extract},
          {take out}]
