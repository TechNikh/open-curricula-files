---
version: 1
type: article
id: https://en.wikipedia.org/wiki/B-coloring
offline_file: ""
offline_thumbnail: ""
uuid: 098c9964-5df1-4342-9b9e-d619bcd3311f
updated: 1484309376
title: B-coloring
categories:
    - Graph coloring
---
In graph theory, a b-coloring of a graph is a coloring of the vertices where each color class contains a vertex that has a neighbor in all other color classes.
