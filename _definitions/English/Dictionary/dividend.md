---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dividend
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484310661
title: dividend
categories:
    - Dictionary
---
dividend
     n 1: that part of the earnings of a corporation that is
          distributed to its shareholders; usually paid quarterly
     2: a number to be divided by another number
     3: a bonus; something extra (especially a share of a surplus)
