---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Discrete_dipole_approximation
offline_file: ""
offline_thumbnail: ""
uuid: ee27e91d-477f-412b-8a5c-9981308f7a65
updated: 1484308749
title: Discrete dipole approximation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Shape_DDA1.svg.png
tags:
    - Basic concepts
    - Extensions
    - Discrete Dipole Approximation Codes
    - Gallery of shapes
categories:
    - Electrodynamics
---
The discrete dipole approximation (DDA) is a method for computing scattering of radiation by particles of arbitrary shape and by periodic structures. Given a target of arbitrary geometry, one seeks to calculate its scattering and absorption properties. Exact solutions to Maxwell's equations are known only for special geometries such as spheres, spheroids, or cylinders, so approximate methods are in general required. However, the DDA employs no physical approximations and can produce accurate enough results, given sufficient computer power.
