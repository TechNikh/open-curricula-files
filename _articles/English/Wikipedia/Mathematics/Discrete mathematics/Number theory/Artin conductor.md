---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Artin_conductor
offline_file: ""
offline_thumbnail: ""
uuid: 50769ab3-6b95-4580-a516-f82d9ff64eda
updated: 1484308545
title: Artin conductor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon.jpg
tags:
    - Local Artin conductors
    - Global Artin conductors
    - Artin representation and Artin character
    - Swan representation
    - Applications
    - Notes
categories:
    - Number theory
---
In mathematics, the Artin conductor is a number or ideal associated to a character of a Galois group of a local or global field, introduced by Emil Artin (1930, 1931) as an expression appearing in the functional equation of an Artin L-function.
