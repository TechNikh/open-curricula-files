---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kanamori%E2%80%93McAloon_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 000bf43c-2679-4e91-a3f0-e43b1d495dc0
updated: 1484308997
title: Kanamori–McAloon theorem
categories:
    - Theorems in the foundations of mathematics
---
In mathematical logic, the Kanamori–McAloon theorem, due to Kanamori & McAloon (1987), gives an example of an incompleteness in Peano arithmetic, similar to that of the Paris–Harrington theorem. They showed that a certain finitistic special case of a theorem in Ramsey theory due to Erdős and Rado is not provable in Peano arithmetic.
