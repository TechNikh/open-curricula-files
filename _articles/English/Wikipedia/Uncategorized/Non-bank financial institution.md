---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-bank
offline_file: ""
offline_thumbnail: ""
uuid: 9dfc39a9-d451-4a98-9a0f-50b4e04338ea
updated: 1484308432
title: Non-bank financial institution
tags:
    - Role in financial system
    - growth
    - Stability
    - Types
    - Risk-pooling institutions
    - Contractual savings institutions
    - Market makers
    - Specialized sectorial financiers
    - Financial service providers
    - In Asia
    - In Europe
    - Classification
    - In the United States
categories:
    - Uncategorized
---
A non-bank financial institution (NBFI) is a financial institution that does not have a full banking license or is not supervised by a national or international banking regulatory agency. NBFIs facilitate bank-related financial services, such as investment, risk pooling, contractual savings, and market brokering.[1] Examples of these include insurance firms, pawn shops, cashier's check issuers, check cashing locations, payday lending, currency exchanges, and microloan organizations.[2][3][4] Alan Greenspan has identified the role of NBFIs in strengthening an economy, as they provide "multiple alternatives to transform an economy's savings into capital investment [which] act as backup ...
