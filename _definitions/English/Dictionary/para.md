---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/para
offline_file: ""
offline_thumbnail: ""
uuid: bed1d15f-acf1-48f3-b93e-b6ba43f5a168
updated: 1484310166
title: para
categories:
    - Dictionary
---
para
     n 1: (obstetrics) the number of live-born children a woman has
          delivered; "the parity of the mother must be
          considered"; "a bipara is a woman who has given birth to
          two children" [syn: {parity}]
     2: 100 para equal 1 dinar
     3: a soldier in the paratroops [syn: {paratrooper}]
     4: an estuary in northern Brazil into which the Tocantins River
        flows [syn: {Para River}]
     5: port city in northern Brazil in the Amazon delta; main port
        and commercial center for the Amazon River basin [syn: {Belem},
         {Feliz Lusitania}, {Santa Maria de Belem}, {St. Mary of
        Bethlehem}]
