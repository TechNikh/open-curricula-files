---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amoeba
offline_file: ""
offline_thumbnail: ""
uuid: 6c876f7d-bf89-4db0-b012-8ccb8e4e7b31
updated: 1484310162
title: amoeba
categories:
    - Dictionary
---
amoeba
     n : naked freshwater or marine or parasitic protozoa that form
         temporary pseudopods for feeding and locomotion [syn: {ameba}]
     [also: {amoebae} (pl)]
