---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alpha_recursion_theory
offline_file: ""
offline_thumbnail: ""
uuid: 99a8af94-1d77-4f54-a139-2254084a4fc4
updated: 1484309481
title: Alpha recursion theory
categories:
    - Computability theory
---
In recursion theory, α recursion theory is a generalisation of recursion theory to subsets of admissible ordinals 
  
    
      
        α
      
    
    {\displaystyle \alpha }
  
. An admissible ordinal is closed under 
  
    
      
        
          Σ
          
            1
          
        
        (
        
          L
          
            α
          
        
        )
      
    
    {\displaystyle \Sigma _{1}(L_{\alpha })}
  
 functions. Admissible ordinals are models of Kripke–Platek set theory. In what follows 
  
    
      
        α
      
    
    {\displaystyle \alpha }
  
 is considered to be fixed.
