---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/claw
offline_file: ""
offline_thumbnail: ""
uuid: 7227da54-b65e-46a7-bbe5-2a1fb5f6d7c0
updated: 1484310284
title: claw
categories:
    - Dictionary
---
claw
     n 1: sharp curved horny process on the toe of a bird or some
          mammals or reptiles
     2: a mechanical device that is curved or bent to suspend or
        hold or pull something [syn: {hook}]
     3: a structure like a pincer on the limb of a crustacean or
        other arthropods [syn: {chela}, {nipper}, {pincer}]
     4: a bird's foot that has claws
     v 1: move as if by clawing, seizing, or digging; "They clawed
          their way to the top of the mountain"
     2: clutch as if in panic; "She clawed the doorknob"
     3: scratch, scrape, pull, or dig with claws or nails
     4: attack as if with claws; "The politician clawed his rival"
