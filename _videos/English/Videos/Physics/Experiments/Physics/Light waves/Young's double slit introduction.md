---
version: 1
type: video
provider: YouTube
id: Pk6s2OlKzKQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ddcb91d9-c8cc-4866-b802-e12f4de6f7b1
updated: 1486069637
title: "Young's double slit introduction"
categories:
    - Light waves
thumbnail_urls:
    - https://i3.ytimg.com/vi/Pk6s2OlKzKQ/default.jpg
    - https://i3.ytimg.com/vi/Pk6s2OlKzKQ/1.jpg
    - https://i3.ytimg.com/vi/Pk6s2OlKzKQ/2.jpg
    - https://i3.ytimg.com/vi/Pk6s2OlKzKQ/3.jpg
---
