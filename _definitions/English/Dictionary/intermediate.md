---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intermediate
offline_file: ""
offline_thumbnail: ""
uuid: 2f8b717c-2ee4-4758-831c-e3aeaf5f5623
updated: 1484310401
title: intermediate
categories:
    - Dictionary
---
intermediate
     adj 1: lying between two extremes in time or space or degree;
            "going from sitting to standing without intermediate
            pushes with the hands"; "intermediate stages in a
            process"; "intermediate stops on the route"; "an
            intermediate level" [ant: {first}, {last}]
     2: around the middle of a scale of evaluation of physical
        measures; "an orange of average size"; "intermediate
        capacity"; "a plane with intermediate range"; "medium
        bombers" [syn: {average}, {medium}]
     n : a substance formed during a chemical process before the
         desired product is obtained
     v : act between parties with a view to ...
