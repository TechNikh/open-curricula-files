---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/debut
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484560562
title: debut
categories:
    - Dictionary
---
debut
     n 1: the act of beginning something new; "they looked forward to
          the debut of their new product line" [syn: {introduction},
           {first appearance}, {launching}, {unveiling}, {entry}]
     2: the presentation of a debutante in society
     v 1: present for the first time to the public; "The and debuts a
          new song or two each month"
     2: appear for the first time in public; "The new ballet that
        debuts next months at Covent Garden, is already sold out"
     3: make one's debut; "This young soprano debuts next months at
        the Metropolitan Opera"
