---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/angstrom
offline_file: ""
offline_thumbnail: ""
uuid: 3427c4e4-6a8f-4819-b425-b99980bd1192
updated: 1484310405
title: angstrom
categories:
    - Dictionary
---
angstrom
     n : a metric unit of length equal to one ten billionth of a
         meter (or 0.0001 micron); used to specify wavelengths of
         electromagnetic radiation [syn: {angstrom unit}, {A}]
