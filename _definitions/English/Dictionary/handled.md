---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/handled
offline_file: ""
offline_thumbnail: ""
uuid: 75a24cdf-e236-45aa-b814-7b7ae6b420d5
updated: 1484310533
title: handled
categories:
    - Dictionary
---
handled
     adj : having a usually specified type of handle; "pearl-handled
           revolver" [ant: {handleless}]
