---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Estrada_index
offline_file: ""
offline_thumbnail: ""
uuid: 7189f029-3a21-47c0-8eed-de55f8382a4b
updated: 1484309422
title: Estrada index
categories:
    - Graph invariants
---
In chemical graph theory, the Estrada index is a topological index of protein folding. The index was first defined by Ernesto Estrada as a measure of the degree of folding of a protein,[1] which is represented as a path-graph weighted by the dihedral or torsional angles of the protein backbone. This index of degree of folding has found multiple applications in the study of protein functions and protein-ligand interactions.
