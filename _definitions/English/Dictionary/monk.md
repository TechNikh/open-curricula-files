---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monk
offline_file: ""
offline_thumbnail: ""
uuid: 92b574a9-014d-4670-8bfe-4d7f29d346e1
updated: 1484310307
title: monk
categories:
    - Dictionary
---
monk
     n 1: a male religious living in a cloister and devoting himself
          to contemplation and prayer and work [syn: {monastic}]
     2: United States jazz pianist who was one of the founders of
        the bebop style (1917-1982) [syn: {Thelonious Monk}, {Thelonious
        Sphere Monk}]
