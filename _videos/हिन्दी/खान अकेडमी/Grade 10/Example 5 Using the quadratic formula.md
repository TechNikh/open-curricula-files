---
version: 1
type: video
provider: YouTube
id: 641O39wZlKc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 28c2c174-5eb3-4a76-b684-b042b6434833
updated: 1486069618
title: Example 5 Using the quadratic formula
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/641O39wZlKc/default.jpg
    - https://i3.ytimg.com/vi/641O39wZlKc/1.jpg
    - https://i3.ytimg.com/vi/641O39wZlKc/2.jpg
    - https://i3.ytimg.com/vi/641O39wZlKc/3.jpg
---
