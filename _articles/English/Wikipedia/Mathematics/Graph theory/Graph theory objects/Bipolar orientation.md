---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bipolar_orientation
offline_file: ""
offline_thumbnail: ""
uuid: 8e668cca-8863-48ba-bd67-16cf492de12f
updated: 1484309443
title: Bipolar orientation
tags:
    - Definitions and existence
    - Applications to planarity
    - Algorithms
    - The space of all orientations
categories:
    - Graph theory objects
---
In graph theory, a bipolar orientation or st-orientation of an undirected graph is an assignment of a direction to each edge (an orientation) that causes the graph to become a directed acyclic graph with a single source s and a single sink t, and an st-numbering of the graph is a topological ordering of the resulting directed acyclic graph.[1][2]
