---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secret
offline_file: ""
offline_thumbnail: ""
uuid: ebbde408-dfe9-4e56-8284-b1f504c42941
updated: 1484310549
title: secret
categories:
    - Dictionary
---
secret
     adj 1: not open or public; kept private or not revealed; "a secret
            formula"; "secret ingredients"; "secret talks"
     2: conducted with or marked by hidden aims or methods;
        "clandestine intelligence operations"; "cloak-and-dagger
        activities behind enemy lines"; "hole-and-corner
        intrigue"; "secret missions"; "a secret agent"; "secret
        sales of arms"; "surreptitious mobilization of troops";
        "an undercover investigation"; "underground resistance"
        [syn: {clandestine}, {cloak-and-dagger}, {hole-and-corner(a)},
         {hugger-mugger}, {hush-hush}, {on the quiet(p)}, {surreptitious},
         {undercover}, {underground}]
    ...
