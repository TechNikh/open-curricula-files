---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indian
offline_file: ""
offline_thumbnail: ""
uuid: 849986bc-bac9-4407-90db-540e313dca1b
updated: 1484310242
title: indian
categories:
    - Dictionary
---
Indian
     adj 1: of or relating to or characteristic of India or the East
            Indies or their peoples or languages or cultures; "the
            Indian subcontinent"; "Indian saris"
     2: of or pertaining to American Indians or their culture or
        languages; "Native American religions"; "Indian
        arrowheads" [syn: {Amerind}, {Amerindic}, {native American}]
     n 1: a member of the race of people living in North America when
          Europeans arrived [syn: {North American Indian}, {American
          Indian}, {Red Indian}]
     2: a native or inhabitant of India
     3: any of the languages spoken by Amerindians [syn: {Amerind},
        {Amerindian language}, ...
