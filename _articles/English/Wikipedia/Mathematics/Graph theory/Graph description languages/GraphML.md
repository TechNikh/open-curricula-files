---
version: 1
type: article
id: https://en.wikipedia.org/wiki/GraphML
offline_file: ""
offline_thumbnail: ""
uuid: 10dadd56-3939-4757-96c4-8fbbbddc5de3
updated: 1484309409
title: GraphML
tags:
    - Introduction to GraphML
categories:
    - Graph description languages
---
GraphML is an XML-based file format for graphs. The GraphML file format results from the joint effort of the graph drawing community to define a common format for exchanging graph structure data. It uses an XML-based syntax and supports the entire range of possible graph structure constellations including directed, undirected, mixed graphs, hypergraphs, and application-specific attributes.[1]
