---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/north-west
offline_file: ""
offline_thumbnail: ""
uuid: 15ab728d-3143-4ffe-a2ea-d0265942ab81
updated: 1484310461
title: north-west
categories:
    - Dictionary
---
north-west
     adv : to, toward, or in the northwest [syn: {northwest}, {nor'-west}]
