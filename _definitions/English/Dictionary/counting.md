---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/counting
offline_file: ""
offline_thumbnail: ""
uuid: a3bafb39-bdc4-427e-b06a-7f2c24c726e2
updated: 1484310423
title: counting
categories:
    - Dictionary
---
counting
     n : the act of counting; "the counting continued for several
         hours" [syn: {count}, {numeration}, {enumeration}, {reckoning},
          {tally}]
