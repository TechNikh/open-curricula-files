---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Myhill%E2%80%93Nerode_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 7eeb15e8-49fa-470a-9bd2-c76a9fbe0cb4
updated: 1484308667
title: Myhill–Nerode theorem
tags:
    - Statement of the theorem
    - Proof
    - Use and consequences
    - Generalization
categories:
    - Theorems in discrete mathematics
---
In the theory of formal languages, the Myhill–Nerode theorem provides a necessary and sufficient condition for a language to be regular. The theorem is named for John Myhill and Anil Nerode, who proved it at the University of Chicago in 1958 (Nerode 1958).
