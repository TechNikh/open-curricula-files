---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Monochromatic_triangle
offline_file: ""
offline_thumbnail: ""
uuid: 27f04c51-dc3b-4d7e-a1e5-9a0873be44a5
updated: 1484309386
title: Monochromatic triangle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-RamseyTheory_K5_no_mono_K3.svg.png
tags:
    - Problem statement
    - Generalization to multiple colors
    - "Connection to Ramsey's theorem"
    - Parameterized complexity
categories:
    - Graph coloring
---
In graph theory and theoretical computer science, the monochromatic triangle problem is an algorithmic problem on graphs, in which the goal is to partition the edges of a given graph into two triangle-free subgraphs. It is NP-complete but fixed-parameter tractable on graphs of bounded treewidth.
