---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polyakov_formula
offline_file: ""
offline_thumbnail: ""
uuid: fc69e3c4-d71a-48f7-916f-0e49104459a7
updated: 1484309187
title: Polyakov formula
categories:
    - Conformal geometry
---
In differential geometry and mathematical physics (especially string theory), the Polyakov formula expresses the conformal variation of the zeta functional determinant of a Riemannian manifold. The corresponding density is local, and therefore is a Riemannian curvature invariant. In particular, whereas the functional determinant itself is prohibitively difficult to work with in general, its conformal variation can be written down explicitly.
