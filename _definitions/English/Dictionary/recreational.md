---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recreational
offline_file: ""
offline_thumbnail: ""
uuid: 33bf39c7-ed38-4a3d-846c-b52749e6130d
updated: 1484310475
title: recreational
categories:
    - Dictionary
---
recreational
     adj 1: of or relating to recreation; "a recreational area with a
            pool and ball fields"
     2: engaged in as a pastime; "an amateur painter"; "gained
        valuable experience in amateur theatricals"; "recreational
        golfers"; "reading matter that is both recreational and
        mentally stimulating"; "unpaid extras in the documentary"
        [syn: {amateur}, {unpaid}]
