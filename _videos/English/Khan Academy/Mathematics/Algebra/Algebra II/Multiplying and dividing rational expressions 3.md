---
version: 1
type: video
provider: YouTube
id: gcnk8TnzsLc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Multiplying%20and%20dividing%20rational%20expressions%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 33740362-f287-404d-9bcd-507cdc2d956a
updated: 1486069557
title: Multiplying and dividing rational expressions 3
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/gcnk8TnzsLc/default.jpg
    - https://i3.ytimg.com/vi/gcnk8TnzsLc/1.jpg
    - https://i3.ytimg.com/vi/gcnk8TnzsLc/2.jpg
    - https://i3.ytimg.com/vi/gcnk8TnzsLc/3.jpg
---
