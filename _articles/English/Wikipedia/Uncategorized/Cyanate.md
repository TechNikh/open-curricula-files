---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyanate
offline_file: ""
offline_thumbnail: ""
uuid: 9b0e531b-669a-49dd-b95d-31c1854df3c4
updated: 1484308398
title: Cyanate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Cyanate-ion-3D-vdW.png
tags:
    - Cyanate ion
    - Acid-base properties
    - Cyanate salts
    - Complexes with the cyanate ion
    - Cyanate in organic compounds
    - Bibliography
categories:
    - Uncategorized
---
The cyanate ion is an anion with the chemical formula written as [OCN]− or [NCO]−. In aqueous solution it acts as a base, forming isocyanic acid, HNCO. The cyanate ion is an ambidentate ligand, forming complexes with a metal ion in which either the nitrogen or oxygen atom may be the electron-pair donor. It can also act as a bridging ligand. Organic cyanates are called isocyanates when there is a C−NCO bond and cyanates when there is a C−OCN bond.
