---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minkowski_problem
offline_file: ""
offline_thumbnail: ""
uuid: 3c650353-0ed5-47dc-9a32-3568f989d998
updated: 1484309123
title: Minkowski problem
categories:
    - Theorems in geometry
---
In differential geometry, the Minkowski problem, named after Hermann Minkowski, asks for the construction of a strictly convex compact surface S whose Gaussian curvature is specified. More precisely, the input to the problem is a strictly positive real function ƒ defined on a sphere, and the surface that is to be constructed should have Gaussian curvature ƒ(n(x)) at the point x, where n(x) denotes the normal to S at x. Eugenio Calabi stated: "From the geometric view point it [the Minkowski problem] is the Rosetta Stone, from which several related problems can be solved."[1]
