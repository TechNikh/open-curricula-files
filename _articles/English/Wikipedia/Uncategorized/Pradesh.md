---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pradesh
offline_file: ""
offline_thumbnail: ""
uuid: bc683850-e8fc-4585-9816-7cb897de79a0
updated: 1484308344
title: Pradesh
categories:
    - Uncategorized
---
Pradesh refers to a province or state in various South Asian languages. It is written प्रदेश in Devanagari script (used for Hindi, Bhojpuri, Marathi, Konkani and Nepali), প্রদেশ in Eastern Nagari script (used for Assamese prôdex, Bengali prodesh, and Bishnupriya Manipuri), ପ୍ରଦେଶ in Odia script, પ્રદેશ in Gujarati script, ಪ್ರದೇಶ in Kannada script, പ്രദേശം in Malayalam script, ప్రదేశ్ in Telugu script, பிரதேசம் piratecam in Tamil script, and پردیش in Nasta'liq script (used for Urdu). The same word was borrowed into Thai as ประเทศ prathet, Lao as ປະເທດ pathet and ...
