---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elegant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484416441
title: elegant
categories:
    - Dictionary
---
elegant
     adj 1: refined and tasteful in appearance or behavior or style;
            "elegant handwriting"; "an elegant dark suit"; "she
            was elegant to her fingertips"; "small churches with
            elegant white spires"; "an elegant mathematical
            solution--simple and precise and lucid" [ant: {inelegant}]
     2: suggesting taste, ease, and wealth [syn: {graceful}, {refined}]
     3: of seemingly effortless beauty in form or proportion
     4: refined or imposing in manner or appearance; befitting a
        royal court; "a courtly gentleman" [syn: {courtly}, {formal},
         {stately}]
