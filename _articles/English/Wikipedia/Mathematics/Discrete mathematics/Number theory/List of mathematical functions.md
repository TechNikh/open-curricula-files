---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_of_mathematical_functions
offline_file: ""
offline_thumbnail: ""
uuid: d0239f37-1264-4a2d-a01d-c785102016af
updated: 1484308644
title: List of mathematical functions
tags:
    - Elementary functions
    - Algebraic functions
    - Elementary transcendental functions
    - Special functions
    - Basic special functions
    - Number theoretic functions
    - Antiderivatives of elementary functions
    - Gamma and related functions
    - Elliptic and related functions
    - Bessel and related functions
    - Riemann zeta and related functions
    - Hypergeometric and related functions
    - Iterated exponential and related functions
    - Other standard special functions
    - Miscellaneous functions
categories:
    - Number theory
---
In mathematics, a function or groups of functions are important enough to deserve their own names. This is a listing of articles which explain some of these functions in more detail. There is a large theory of special functions which developed out of statistics and mathematical physics. A modern, abstract point of view contrasts large function spaces, which are infinite-dimensional and within which most functions are 'anonymous', with special functions picked out by properties such as symmetry, or relationship to harmonic analysis and group representations.
