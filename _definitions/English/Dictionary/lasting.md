---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lasting
offline_file: ""
offline_thumbnail: ""
uuid: 05377607-ca46-4d3f-af25-4afcd2837856
updated: 1484310553
title: lasting
categories:
    - Dictionary
---
lasting
     adj 1: continuing or enduring without marked change in status or
            condition or place; "permanent secretary to the
            president"; "permanent address"; "literature of
            permanent value" [syn: {permanent}] [ant: {impermanent}]
     2: existing for a long time; "hopes for a durable peace"; "a
        long-lasting friendship" [syn: {durable}, {long-lasting},
        {long-lived}]
     3: retained; not shed; "persistent leaves remain attached past
        maturity"; "the persistent gills of fishes" [syn: {persistent}]
        [ant: {caducous}]
     4: lasting a long time without change; "a lasting relationship"
     5: lasting for an indefinitely long ...
