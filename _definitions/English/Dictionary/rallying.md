---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rallying
offline_file: ""
offline_thumbnail: ""
uuid: 8f44a886-bfde-4b4e-b15d-8627c8905dec
updated: 1484310609
title: rallying
categories:
    - Dictionary
---
rallying
     adj : rousing or recalling to unity and renewed effort; "a
           rallying cry"
     n 1: the act of mobilizing for a common purpose; "the bell was a
          signal for the rallying of the whole neighborhood"
     2: the feat of mustering strength for a renewed effort; "he
        singled to start a rally in the 9th inning"; "he feared
        the rallying of their troops for a counterattack" [syn: {rally}]
