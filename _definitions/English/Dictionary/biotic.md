---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biotic
offline_file: ""
offline_thumbnail: ""
uuid: 912303cb-67ec-4d4a-88e9-c451d7e766e4
updated: 1484310275
title: biotic
categories:
    - Dictionary
---
biotic
     adj : of or relating to living organisms
