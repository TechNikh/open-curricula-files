---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Riesz%E2%80%93Markov%E2%80%93Kakutani_representation_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 246a255b-614e-4478-9bee-6fc5712103c9
updated: 1484309237
title: Riesz–Markov–Kakutani representation theorem
categories:
    - Duality theories
---
In mathematics, the Riesz–Markov–Kakutani representation theorem relates linear functionals on spaces of continuous functions on a locally compact space to measures. The theorem is named for Frigyes Riesz (1909) who introduced it for continuous functions on the unit interval, Andrey Markov (1938) who extended the result to some non-compact spaces, and Shizuo Kakutani (1941) who extended the result to compact Hausdorff spaces.
