---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gruelling
offline_file: ""
offline_thumbnail: ""
uuid: d2dd21bb-1149-4cc1-9bdd-e636a1405da3
updated: 1484310571
title: gruelling
categories:
    - Dictionary
---
gruelling
     adj : characterized by toilsome effort to the point of exhaustion;
           especially physical effort; "worked their arduous way
           up the mining valley"; "a grueling campaign"; "hard
           labor"; "heavy work"; "heavy going"; "spent many
           laborious hours on the project"; "set a punishing pace"
           [syn: {arduous}, {backbreaking}, {grueling}, {hard}, {heavy},
            {laborious}, {punishing}, {toilsome}]
