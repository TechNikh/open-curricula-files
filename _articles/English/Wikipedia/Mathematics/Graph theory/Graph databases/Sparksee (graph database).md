---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sparksee_(graph_database)
offline_file: ""
offline_thumbnail: ""
uuid: b1109de6-78aa-4982-be32-f5ce763e14f7
updated: 1484309405
title: Sparksee (graph database)
tags:
    - 'Graph Model [1]'
    - Technical details
    - Also
categories:
    - Graph databases
---
Its development started in 2006 and its first version was available on Q3 - 2008. The fourth version is available since Q3-2010. There is a free community version, for academic or evaluation purposes, available to download, limited to 1 million nodes, no limit on edges.
