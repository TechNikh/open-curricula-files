---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sugarcane
offline_file: ""
offline_thumbnail: ""
uuid: 71704cc0-6ccf-4d34-a877-145e9eecf132
updated: 1484310462
title: sugarcane
categories:
    - Dictionary
---
sugar cane
     n 1: juicy canes whose sap is a source of molasses and commercial
          sugar; fresh canes are sometimes chewed for the juice
          [syn: {sugarcane}]
     2: tall tropical southeast Asian grass having stout fibrous
        jointed stalks; sap is a chief source of sugar [syn: {sugarcane},
         {Saccharum officinarum}]
