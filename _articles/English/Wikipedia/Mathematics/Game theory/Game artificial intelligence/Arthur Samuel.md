---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arthur_Samuel
offline_file: ""
offline_thumbnail: ""
uuid: 089bc945-a936-4752-a4b5-d6eae7e7bb74
updated: 1484309006
title: Arthur Samuel
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_7.jpg
tags:
    - Biography
    - Computer checkers (draughts) development
    - Awards
    - Selected works
categories:
    - Game artificial intelligence
---
Arthur Lee Samuel (December 5, 1901 – July 29, 1990)[3] was an American pioneer in the field of computer gaming, artificial intelligence, and machine learning.[1] The Samuel Checkers-playing Program appears to be the world's first self-learning program, and as such a very early demonstration of the fundamental concept of artificial intelligence (AI).[4] He was also a senior member in TeX community who devoted much time giving personal attention to the needs of users and wrote an early TeX manual in 1983.[5]
