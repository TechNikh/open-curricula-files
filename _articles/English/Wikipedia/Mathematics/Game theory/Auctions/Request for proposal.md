---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Request_for_proposal
offline_file: ""
offline_thumbnail: ""
uuid: b7daa4f8-7f1f-4a1d-9298-6010c3930a36
updated: 1484309013
title: Request for proposal
tags:
    - Specifications
    - Other requests
categories:
    - Auctions
---
A request for proposal (RFP) is a document that solicits proposal, often made through a bidding process, by an agency or company interested in procurement of a commodity, service or valuable asset, to potential suppliers to submit business proposals.[1] It is submitted early in the procurement cycle, either at the preliminary study, or procurement stage.
