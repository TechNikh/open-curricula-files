---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reorganisation
offline_file: ""
offline_thumbnail: ""
uuid: 8e243446-a3c7-4ae2-b1f7-11c991ec701e
updated: 1484310603
title: reorganisation
categories:
    - Dictionary
---
reorganisation
     n : the act of imposing a new organization; organizing
         differently (often involving extensive and drastic
         changes); "a committee was appointed to oversee the
         reorganization of the curriculum"; "top officials were
         forced out in the cabinet shakeup" [syn: {reorganization},
          {shake-up}, {shakeup}]
