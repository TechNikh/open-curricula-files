---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/antenna
offline_file: ""
offline_thumbnail: ""
uuid: 5646bd32-fd9e-45b6-bc08-7a4089a0732d
updated: 1484310216
title: antenna
categories:
    - Dictionary
---
antenna
     n 1: an electrical device that sends or receives radio or
          television signals [syn: {aerial}, {transmitting aerial}]
     2: sensitivity similar to that of a receptor organ; "he had a
        special antenna for public relations" [syn: {feeler}]
     3: one of a pair of mobile appendages on the head of e.g.
        insects and crustaceans; typically sensitive to touch and
        taste [syn: {feeler}]
     [also: {antennae} (pl)]
