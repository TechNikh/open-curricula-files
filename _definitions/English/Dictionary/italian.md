---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/italian
offline_file: ""
offline_thumbnail: ""
uuid: d849bae8-79a6-44d1-87e0-d1e5c8c13a06
updated: 1484310551
title: italian
categories:
    - Dictionary
---
Italian
     adj : of or pertaining to or characteristic of Italy or its people
           or culture or language; "Italian cooking"
     n 1: a native or inhabitant of Italy
     2: the Romance language spoken in Italy
