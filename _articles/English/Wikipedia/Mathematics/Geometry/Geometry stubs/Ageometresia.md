---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ageometresia
offline_file: ""
offline_thumbnail: ""
uuid: 7562d804-11d0-4927-8594-16f45ab75b46
updated: 1484309354
title: Ageometresia
categories:
    - Geometry stubs
---
An early usage of the word was in writings of François Viète on Copernicus.[2] As another instance, Johannes Kepler, having no direct and geometrical method of finding certain matters in his elliptical theory, namely how to calculate the true anomaly from the mean anomaly, has been charged by others with ageometresia.[3][4]
