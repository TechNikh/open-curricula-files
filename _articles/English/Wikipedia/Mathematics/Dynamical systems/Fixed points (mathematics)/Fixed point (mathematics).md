---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fixed_point_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: b6d1262d-58ad-44eb-983f-ae8d16bdd294
updated: 1484308784
title: Fixed point (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fixed_point_example.svg.png
tags:
    - Attractive fixed points
    - Applications
    - Topological fixed point property
    - 'Generalization to partial orders: prefixpoint and postfixpoint'
    - Notes
categories:
    - Fixed points (mathematics)
---
In mathematics, a fixed point (sometimes shortened to fixpoint, also known as an invariant point) of a function is an element of the function's domain that is mapped to itself by the function. That is to say, c is a fixed point of the function f(x) if and only if f(c) = c. This means f(f(...f(c)...)) = fn(c) = c, an important terminating consideration when recursively computing f. A set of fixed points is sometimes called a fixed set.
