---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spermatozoan
offline_file: ""
offline_thumbnail: ""
uuid: 5b40a76d-8a23-42d9-8cf6-c34495f626ae
updated: 1484310162
title: spermatozoan
categories:
    - Dictionary
---
spermatozoan
     n : the male reproductive cell; the male gamete; "a sperm is
         mostly a nucleus surrounded by little other cellular
         material" [syn: {sperm}, {sperm cell}, {spermatozoon}]
