---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eggs
offline_file: ""
offline_thumbnail: ""
uuid: 141a4d5b-b16d-4225-804c-a96b83dded0e
updated: 1484310297
title: eggs
categories:
    - Dictionary
---
eggs
     n : oval reproductive body of a fowl (especially a hen) used as
         food [syn: {egg}]
