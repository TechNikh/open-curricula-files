---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tetrahedron_packing
offline_file: ""
offline_thumbnail: ""
uuid: 6c18a143-5049-4187-a794-40fae2684fc4
updated: 1484309247
title: Tetrahedron packing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tetrahedron_packing_structure_png.png
tags:
    - Historical results
    - Relationship to other packing problems
categories:
    - History of geometry
---
In geometry, tetrahedron packing is the problem of arranging identical regular tetrahedra throughout three-dimensional space so as to fill the maximum possible fraction of space.
