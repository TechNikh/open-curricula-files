---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frame
offline_file: ""
offline_thumbnail: ""
uuid: 7356a878-e77b-4f81-9985-0811f904ee29
updated: 1484310218
title: frame
categories:
    - Dictionary
---
frame
     n 1: a structure supporting or containing something [syn: {framework},
           {framing}]
     2: one of a series of still transparent photographs on a strip
        of film used in making movies
     3: alternative names for the body of a human being; "Leonardo
        studied the human body"; "he has a strong physique"; "the
        spirit is willing but the flesh is weak" [syn: {human body},
         {physical body}, {material body}, {soma}, {build}, {figure},
         {physique}, {anatomy}, {shape}, {bod}, {chassis}, {form},
         {flesh}]
     4: a period of play in baseball during which each team has a
        turn at bat [syn: {inning}]
     5: the hard structure ...
