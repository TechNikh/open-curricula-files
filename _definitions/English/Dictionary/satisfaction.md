---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/satisfaction
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484454961
title: satisfaction
categories:
    - Dictionary
---
satisfaction
     n 1: the contentment you feel when you have done something right;
          "the chef tasted the sauce with great satisfaction"
          [ant: {dissatisfaction}]
     2: state of being gratified; great satisfaction; "dull
        repetitious work gives no gratification"; "to my immense
        gratification he arrived on time" [syn: {gratification}]
     3: compensation for a wrong; "we were unable to get
        satisfaction from the local store" [syn: {atonement}, {expiation}]
     4: act of fulfilling a desire or need or appetite; "the
        satisfaction of their demand for better services"
