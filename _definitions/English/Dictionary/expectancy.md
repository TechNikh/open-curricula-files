---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expectancy
offline_file: ""
offline_thumbnail: ""
uuid: 029e97b5-60ef-4c25-831b-0aeb5d7ab341
updated: 1484310447
title: expectancy
categories:
    - Dictionary
---
expectancy
     n 1: pleasurable expectation [syn: {anticipation}]
     2: something expected (as on the basis of a norm); "each of
        them had their own anticipations"; "an indicator of
        expectancy in development" [syn: {anticipation}]
