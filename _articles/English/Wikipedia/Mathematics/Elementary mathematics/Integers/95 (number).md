---
version: 1
type: article
id: https://en.wikipedia.org/wiki/95_(number)
offline_file: ""
offline_thumbnail: ""
uuid: e0794636-b27c-45d2-aa07-ddb2c61b4d01
updated: 1484308970
title: 95 (number)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/120px-Martin_Luther%252C_1529.jpg'
tags:
    - In mathematics
    - In astronomy
    - In other fields
    - In sports
categories:
    - Integers
---
