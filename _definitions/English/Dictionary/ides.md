---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ides
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484370361
title: ides
categories:
    - Dictionary
---
ides
     n : in the Roman calendar: the 15th of March or May or July or
         October or the 13th of any other month
