---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electrician
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484379901
title: electrician
categories:
    - Dictionary
---
electrician
     n : a person who installs or repairs electrical or telephone
         lines [syn: {lineman}, {linesman}]
