---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Topological_entropy
offline_file: ""
offline_thumbnail: ""
uuid: 4c248884-f6af-4108-987e-dfdb824af59f
updated: 1484308771
title: Topological entropy
tags:
    - Definition
    - Definition of Adler, Konheim, and McAndrew
    - Interpretation
    - Definition of Bowen and Dinaburg
    - Interpretation
    - Properties
    - Examples
    - Notes
categories:
    - Entropy and information
---
In mathematics, the topological entropy of a topological dynamical system is a nonnegative extended real number that is a measure of the complexity of the system. Topological entropy was first introduced in 1965 by Adler, Konheim and McAndrew. Their definition was modelled after the definition of the Kolmogorov–Sinai, or metric entropy. Later, Dinaburg and Rufus Bowen gave a different, weaker definition reminiscent of the Hausdorff dimension. The second definition clarified the meaning of the topological entropy: for a system given by an iterated function, the topological entropy represents the exponential growth rate of the number of distinguishable orbits of the iterates. An important ...
