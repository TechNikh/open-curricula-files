---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Speech_transmission_index
offline_file: ""
offline_thumbnail: ""
uuid: f18c2354-7c63-4779-88ed-20d73548ceeb
updated: 1484308905
title: Speech transmission index
tags:
    - History
    - Scale
    - Nominal qualification bands for STI
    - Standards
    - STIPA
    - Indirect method
    - List of manufacturers of STI measuring instruments
categories:
    - Waves
---
Speech Transmission Index (STI) is a measure of speech transmission quality. The absolute measurement of speech intelligibility is a complex science. The STI measures some physical characteristics of a transmission channel (a room, electro-acoustic equipment, telephone line, etc.), and expresses the ability of the channel to carry across the characteristics of a speech signal. STI is a well-established objective measurement predictor of how the characteristics of the transmission channel affects speech intelligibility.
