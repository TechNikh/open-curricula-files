---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/din
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484635381
title: din
categories:
    - Dictionary
---
din
     n 1: a loud harsh or strident noise [syn: {blare}, {blaring}, {cacophony},
           {clamor}]
     2: the act of making a noisy disturbance [syn: {commotion}, {ruction},
         {ruckus}, {rumpus}, {tumult}]
     v 1: make a resonant sound, like artillery; "His deep voice
          boomed through the hall" [syn: {boom}]
     2: instill (into a person) by constant repetition; "he dinned
        the lessons into his students"
     [also: {dinning}, {dinned}]
