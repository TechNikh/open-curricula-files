---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Takens%27_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 74cf256e-d766-4df7-9c84-18f9015dd244
updated: 1484308699
title: "Takens' theorem"
tags:
    - Simplified, slightly inaccurate version
categories:
    - Theorems in dynamical systems
---
In the study of dynamical systems, a delay embedding theorem gives the conditions under which a chaotic dynamical system can be reconstructed from a sequence of observations of the state of a dynamical system. The reconstruction preserves the properties of the dynamical system that do not change under smooth coordinate changes, but it does not preserve the geometric shape of structures in phase space.
