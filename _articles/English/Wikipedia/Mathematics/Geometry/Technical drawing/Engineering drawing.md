---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Engineering_drawing
offline_file: ""
offline_thumbnail: ""
uuid: d2beb4e3-068b-43ba-aec9-4d47a7986018
updated: 1484309325
title: Engineering drawing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/128px-Nuvola_apps_kcmsystem.svg.png
tags:
    - Relationship to artistic drawing
    - Relationship to other technical drawing types
    - Cascading of conventions by specialty
    - Legal instruments
    - Standardization and disambiguation
    - Media
    - Relationship to model-based definition (MBD/DPD)
    - Systems of dimensioning and tolerancing
    - 'Engineering drawings: common features'
    - Line styles and types
    - Multiple views and projections
    - Orthographic projection
    - Auxiliary projection
    - Isometric projection
    - Oblique projection
    - Perspective
    - Section Views
    - Scale
    - Showing dimensions
    - Sizes of drawings
    - Technical lettering
    - Conventional parts (areas) of an engineering drawing
    - Title block
    - Revisions block
    - Next assembly
    - Notes list
    - General notes
    - Flagnotes
    - Field of the drawing
    - List of materials, bill of materials, parts list
    - Parameter tabulations
    - Views and sections
    - Zones
    - Abbreviations and symbols
    - Example of an engineering drawing
    - Bibliography
categories:
    - Technical drawing
---
Engineering drawing (the activity) produces engineering drawings (the documents). More than merely the drawing of pictures, it is also a language—a graphical language that communicates ideas and information from one mind to another.[1] Most especially, it communicates all needed information from the engineer who designed a part to the workers who will make it.
