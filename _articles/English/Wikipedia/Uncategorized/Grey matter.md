---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grey_matter
offline_file: ""
offline_thumbnail: ""
uuid: 884515a9-4fe4-4611-95dd-d25b6ea9e9c1
updated: 1484308368
title: Grey matter
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Spinal_nerve.svg_0.png
tags:
    - Structure
    - Function
    - Clinical significance
    - Research
    - Volume and cognition in elderly people
    - >
        Volume associated with bipolar disorder, and substance
        misuse
    - Volume associated with smoking
    - Volume associated with alcohol use
    - Volume associated with poverty
    - Volume associated with subjective happiness score
    - Volume associated with mindfulness
    - History
    - Etymology
    - Additional images
categories:
    - Uncategorized
---
Grey matter (or gray matter) is a major component of the central nervous system, consisting of neuronal cell bodies, neuropil (dendrites and myelinated as well as unmyelinated axons), glial cells (astroglia and oligodendrocytes), synapses, and capillaries. Grey matter is distinguished from white matter, in that it contains numerous cell bodies and relatively few myelinated axons, while white matter contains relatively very few cell bodies and is composed chiefly of long-range myelinated axon tracts.[1] The colour difference arises mainly from the whiteness of myelin. In living tissue, grey matter actually has a very light grey colour with yellowish or pinkish hues, which come from capillary ...
