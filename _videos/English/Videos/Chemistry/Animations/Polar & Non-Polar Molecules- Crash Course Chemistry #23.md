---
version: 1
type: video
provider: YouTube
id: PVL24HAesnc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 65e22673-cce3-498c-b198-fb091297a8e1
updated: 1486069664
title: 'Polar & Non-Polar Molecules- Crash Course Chemistry #23'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/PVL24HAesnc/default.jpg
    - https://i3.ytimg.com/vi/PVL24HAesnc/1.jpg
    - https://i3.ytimg.com/vi/PVL24HAesnc/2.jpg
    - https://i3.ytimg.com/vi/PVL24HAesnc/3.jpg
---
