---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/barbaric
offline_file: ""
offline_thumbnail: ""
uuid: 282b91a2-a500-46e3-87ec-4ec3a561c667
updated: 1484310571
title: barbaric
categories:
    - Dictionary
---
barbaric
     adj 1: without civilizing influences; "barbarian invaders";
            "barbaric practices"; "a savage people"; "fighting is
            crude and uncivilized especially if the weapons are
            efficient"-Margaret Meade; "wild tribes" [syn: {barbarian},
             {savage}, {uncivilized}, {uncivilised}, {wild}]
     2: unrestrained and crudely rich; "barbaric use of color or
        ornament"
