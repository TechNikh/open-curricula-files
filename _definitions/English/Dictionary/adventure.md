---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adventure
offline_file: ""
offline_thumbnail: ""
uuid: cc8dd8c2-0e8b-4c5c-b42f-0e80c061fce5
updated: 1484096561
title: adventure
categories:
    - Dictionary
---
adventure
     n : a wild and exciting undertaking (not necessarily lawful)
         [syn: {escapade}, {risky venture}, {dangerous undertaking}]
     v 1: take a risk in the hope of a favorable outcome; "When you
          buy these stocks you are gambling" [syn: {gamble}, {chance},
           {risk}, {hazard}, {take chances}, {run a risk}, {take a
          chance}]
     2: put at risk; "I will stake my good reputation for this"
        [syn: {venture}, {hazard}, {stake}, {jeopardize}]
