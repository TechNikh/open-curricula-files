---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diminished
offline_file: ""
offline_thumbnail: ""
uuid: b90f4f05-fd63-446b-98e9-a228865e3b10
updated: 1484310216
title: diminished
categories:
    - Dictionary
---
diminished
     adj 1: impaired by diminution [syn: {lessened}, {vitiated}, {weakened}]
     2: (of an organ or body part) diminished in size or strength as
        a result of disease or injury or lack of use; "partial
        paralysis resulted in an atrophied left arm" [syn: {atrophied},
         {wasted}] [ant: {hypertrophied}]
     3: (of musical intervals) reduction by a semitone of any
        perfect or minor musical interval; "a diminished fifth"
     4: made to seem smaller or less (especially in worth); "her
        comments made me feel small" [syn: {belittled}, {small}]
