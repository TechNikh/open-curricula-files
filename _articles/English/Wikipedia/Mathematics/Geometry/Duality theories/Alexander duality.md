---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alexander_duality
offline_file: ""
offline_thumbnail: ""
uuid: e9bc6e6a-68e5-4343-8331-818d49285a55
updated: 1484309232
title: Alexander duality
tags:
    - Modern statement
    - "Alexander's 1915 result"
categories:
    - Duality theories
---
In mathematics, Alexander duality refers to a duality theory presaged by a result of 1915 by J. W. Alexander, and subsequently further developed, particularly by P. S. Alexandrov and Lev Pontryagin. It applies to the homology theory properties of the complement of a subspace X in Euclidean space, a sphere, or other manifold. It is generalized by Spanier-Whitehead duality.
