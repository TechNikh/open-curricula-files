---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Nucleic_acid_secondary_structure
offline_file: ""
offline_thumbnail: ""
uuid: 140f8894-1b73-4444-9aea-48c08ef97f9c
updated: 1484309277
title: Nucleic acid secondary structure
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-A-DNA%252C_B-DNA_and_Z-DNA.png'
tags:
    - Fundamental concepts
    - Base pairing
    - Nucleic acid hybridization
    - Secondary structure motifs
    - Double helix
    - Stem-loop structures
    - Pseudoknots
    - Secondary structure prediction
categories:
    - Molecular geometry
---
The secondary structure of a nucleic acid molecule refers to the basepairing interactions within a single molecule or set of interacting molecules, and can be represented as a list of bases which are paired in a nucleic acid molecule.[1] The secondary structures of biological DNA's and RNA's tend to be different: biological DNA mostly exists as fully base paired double helices, while biological RNA is single stranded and often forms complicated base-pairing interactions due to its increased ability to form hydrogen bonds stemming from the extra hydroxyl group in the ribose sugar.
