---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pencil_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 0a0f78c6-f605-4182-95f7-6076bac938ab
updated: 1484309369
title: Pencil (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Apollonian_circles.svg_1.png
categories:
    - Geometry stubs
---
A pencil in projective geometry is a family of geometric objects with a common property, for example the set of lines that pass through a given point in a projective plane.
