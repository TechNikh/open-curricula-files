---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hence
offline_file: ""
offline_thumbnail: ""
uuid: c98ba17f-d242-4cab-b06e-d3c3d357dc20
updated: 1484310344
title: hence
categories:
    - Dictionary
---
hence
     adv 1: (used to introduce a logical conclusion) from that fact or
            reason or as a result; "therefore X must be true";
            "the eggs were fresh and hence satisfactory"; "we were
            young and thence optimistic"; "it is late and thus we
            must go"; "the witness is biased and so cannot be
            trusted" [syn: {therefore}, {thence}, {thus}]
     2: from this place; "get thee hence!"
     3: from this time; "a year hence it will be forgotten"
