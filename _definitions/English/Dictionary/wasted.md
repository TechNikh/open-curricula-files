---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wasted
offline_file: ""
offline_thumbnail: ""
uuid: 50b83360-2ea1-402b-816b-1c795bec5c8f
updated: 1484310150
title: wasted
categories:
    - Dictionary
---
wasted
     adj 1: serving no useful purpose; having no excuse for being;
            "otiose lines in a play"; "advice is wasted words"
            [syn: {otiose}, {pointless}, {superfluous}]
     2: not used to good advantage; "squandered money cannot be
        replaced"; "a wasted effort" [syn: {squandered}]
     3: (of an organ or body part) diminished in size or strength as
        a result of disease or injury or lack of use; "partial
        paralysis resulted in an atrophied left arm" [syn: {atrophied},
         {diminished}] [ant: {hypertrophied}]
     4: very thin especially from disease or hunger or cold;
        "emaciated bony hands"; "a nightmare population of gaunt
        ...
