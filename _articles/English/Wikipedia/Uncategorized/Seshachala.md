---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Seshachala
offline_file: ""
offline_thumbnail: ""
uuid: ec92b9b7-7cee-46f1-b5f9-94c491b30b82
updated: 1484308425
title: Seshachala
categories:
    - Uncategorized
---
Seshachala Educational Society was established in 1998 by its founder and chairman sri S.V. Subbaiah, in Puttur of Chittoor district, Andhra Pradesh, India. The society offers education from preschool to postgraduate and has founded institutions in Puttur and Tirupathi.
