---
version: 1
type: video
provider: YouTube
id: p5e5mf_G3FI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Example-%20two-step%20equation%20with%20numerator%20x.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5ca71c52-fd48-4efc-9165-3ddf5db030f0
updated: 1486069557
title: 'Example- two-step equation with numerator x'
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/p5e5mf_G3FI/default.jpg
    - https://i3.ytimg.com/vi/p5e5mf_G3FI/1.jpg
    - https://i3.ytimg.com/vi/p5e5mf_G3FI/2.jpg
    - https://i3.ytimg.com/vi/p5e5mf_G3FI/3.jpg
---
