---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glucose
offline_file: ""
offline_thumbnail: ""
uuid: df2a5b08-1cba-4559-b1a4-3b13482d8591
updated: 1484310353
title: glucose
categories:
    - Dictionary
---
glucose
     n : a monosaccharide sugar that has several forms; an important
         source of physiological energy
