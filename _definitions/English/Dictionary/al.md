---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/al
offline_file: ""
offline_thumbnail: ""
uuid: 377a1ffc-b1d9-4f4d-b0c0-13eb49a4625c
updated: 1484310140
title: al
categories:
    - Dictionary
---
Al
     n 1: a silvery ductile metallic element found primarily in
          bauxite [syn: {aluminum}, {aluminium}, {atomic number 13}]
     2: a state in the southeastern United States on the Gulf of
        Mexico; one of the Confederate states during the American
        Civil War [syn: {Alabama}, {Heart of Dixie}, {Camellia
        State}]
