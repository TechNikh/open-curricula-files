---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Nonlinear_system_identification
offline_file: ""
offline_thumbnail: ""
uuid: 0ab1f6cf-69e7-4d4b-80b4-ac84eae02cb7
updated: 1484308805
title: Nonlinear system identification
tags:
    - Volterra series methods
    - Block-structured systems
    - Neural networks
    - NARMAX methods
categories:
    - Nonlinear systems
---
System identification is a method of identifying or measuring the mathematical model of a system from measurements of the system inputs and outputs. The applications of system identification include any system where the inputs and outputs can be measured and include industrial processes, control systems, economic data, biology and the life sciences, medicine, social systems and many more.
