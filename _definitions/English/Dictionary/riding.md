---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/riding
offline_file: ""
offline_thumbnail: ""
uuid: bca9a3ec-9191-41e9-9b55-961f8396abb3
updated: 1484310607
title: riding
categories:
    - Dictionary
---
riding
     adj : traveling by wheeled vehicle such as bicycle or automobile
           e.g.; "the riding public welcomed the new buses" [syn:
           {awheel}]
     n 1: riding a horse as a sport [syn: {horseback riding}, {equitation}]
     2: riding a horse as a means of transportation [syn: {horseback
        riding}]
