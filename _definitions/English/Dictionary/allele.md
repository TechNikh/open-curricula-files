---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/allele
offline_file: ""
offline_thumbnail: ""
uuid: 9d86204f-f6b2-4168-a49c-b605ef2f5d14
updated: 1484310297
title: allele
categories:
    - Dictionary
---
allele
     n : one of two alternate forms of a gene that can have the same
         locus on homologous chromosomes and are responsible for
         alternative traits; "some alleles are dominant over
         others" [syn: {allelomorph}]
