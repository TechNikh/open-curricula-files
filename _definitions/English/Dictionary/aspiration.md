---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aspiration
offline_file: ""
offline_thumbnail: ""
uuid: b606d4ff-cd71-4468-a0d0-c809a4c00178
updated: 1484310448
title: aspiration
categories:
    - Dictionary
---
aspiration
     n 1: a will to succeed
     2: a cherished desire; "his ambition is to own his own
        business" [syn: {ambition}, {dream}]
     3: a manner of articulation involving an audible release of
        breath
     4: the act of inhaling; the drawing in of air (or other gases)
        as in breathing [syn: {inhalation}, {inspiration}, {breathing
        in}]
