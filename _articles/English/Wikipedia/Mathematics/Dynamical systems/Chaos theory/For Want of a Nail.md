---
version: 1
type: article
id: https://en.wikipedia.org/wiki/For_Want_of_a_Nail
offline_file: ""
offline_thumbnail: ""
uuid: e99d8234-42d6-46b2-b304-09bbcbc0e088
updated: 1484308712
title: For Want of a Nail
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-WWI_winter_horseshoe.JPG
tags:
    - Analysis
    - Historical references
    - Modern day references
    - Legal
    - Literary
    - Musical
    - Poetic
    - Cinema and television
    - Video games
    - Bibliography
categories:
    - Chaos theory
---
"For Want of a Nail" is a proverb, having numerous variations over several centuries, reminding that seemingly unimportant acts or omissions can have grave and unforeseen consequences.
