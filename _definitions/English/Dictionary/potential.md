---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/potential
offline_file: ""
offline_thumbnail: ""
uuid: a59881c1-956e-448f-82ff-70c842ff8b0c
updated: 1484310264
title: potential
categories:
    - Dictionary
---
potential
     adj 1: existing in possibility; "a potential problem"; "possible
            uses of nuclear power" [syn: {possible}] [ant: {actual}]
     2: expected to become or be; in prospect; "potential clients";
        "expected income" [syn: {expected}, {likely}]
     n 1: the inherent capacity for coming into being [syn: {potentiality},
           {potency}]
     2: the difference in electrical charge between two points in a
        circuit expressed in volts [syn: {electric potential}, {potential
        difference}, {potential drop}, {voltage}]
