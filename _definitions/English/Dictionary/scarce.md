---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scarce
offline_file: ""
offline_thumbnail: ""
uuid: f2c147a5-cb21-4ec7-b8f7-7f09ceabf9d0
updated: 1484310266
title: scarce
categories:
    - Dictionary
---
scarce
     adj 1: not enough; hard to find; "meat was scarce during the war"
     2: deficient in quantity or number compared with the demand;
        "fresh vegetables were scarce during the drought" [ant: {abundant}]
     adv : by a small margin; "they could barely hear the speaker"; "we
           hardly knew them"; "just missed being hit"; "had
           scarcely rung the bell when the door flew open"; "would
           have scarce arrived before she would have found some
           excuse to leave"- W.B.Yeats [syn: {barely}, {hardly}, {just},
            {scarcely}]
