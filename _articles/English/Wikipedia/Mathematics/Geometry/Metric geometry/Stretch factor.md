---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stretch_factor
offline_file: ""
offline_thumbnail: ""
uuid: 69bec0e5-c3c9-4093-94c1-53680eb8a62e
updated: 1484309273
title: Stretch factor
categories:
    - Metric geometry
---
In mathematics, the stretch factor of an embedding measures the factor by which the embedding distorts distances. Suppose that one metric space S is embedded into another metric space T by a metric map, a continuous one-to-one function f that preserves or reduces the distance between every pair of points. Then the embedding gives rise to two different notions of distance between pairs of points in S. Any pair of points (x,y) in S has both an intrinsic distance, the distance from x to y in S, and a smaller extrinsic distance, the distance from f(x) to f(y) in T. The stretch factor of the pair is the ratio between these two distances, d(x,y)/d(f(x),f(y)). The stretch factor of the whole ...
