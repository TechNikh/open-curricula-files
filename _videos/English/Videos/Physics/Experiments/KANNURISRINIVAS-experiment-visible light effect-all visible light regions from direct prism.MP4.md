---
version: 1
type: video
provider: YouTube
id: gApxBRlLz3s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d7952158-e8b2-41ba-a303-a765c0c30bf2
updated: 1486069640
title: >
    KANNURISRINIVAS/experiment/visible light effect/all visible
    light regions from direct prism.MP4
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/gApxBRlLz3s/default.jpg
    - https://i3.ytimg.com/vi/gApxBRlLz3s/1.jpg
    - https://i3.ytimg.com/vi/gApxBRlLz3s/2.jpg
    - https://i3.ytimg.com/vi/gApxBRlLz3s/3.jpg
---
