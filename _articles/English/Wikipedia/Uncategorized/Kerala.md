---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kerala
offline_file: ""
offline_thumbnail: ""
uuid: 2a0416b0-7bf5-4ea8-82d3-deb472da66dc
updated: 1484308422
title: Kerala
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/50px-Guruvayoor_Kesavan_Statue.jpg
tags:
    - Etymology
    - History
    - Mythology
    - Pre-history
    - Ancient period
    - Early medieval period
    - Colonial era
    - Post colonial period
    - geography
    - Climate
    - Flora and Fauna
    - Subdivisions
    - Government and administration
    - Economy
    - Agriculture
    - Fisheries
    - Transport
    - Roads
    - Railways
    - Airports
    - Water transport
    - Demographics
    - Gender
    - Human Development Index
    - Healthcare
    - religion
    - Education
    - culture
    - Festivals
    - Onam
    - Dance
    - Music
    - Cinema
    - Literature
    - Cuisine
    - elephants
    - Media
    - Sports
    - Tourism
categories:
    - Uncategorized
---
Kerala (/ˈkɛrələ/), historically known as Keralam, is an Indian state in South India on the Malabar coast. It was formed on 1 November 1956 following the States Reorganisation Act by combining Malayalam-speaking regions. Spread over 38,863 km2 (15,005 sq mi), it is bordered by Karnataka to the north and northeast, Tamil Nadu to the east and south, and the Lakshadweep Sea to the west. With 33,387,677 inhabitants as per the 2011 Census, Kerala is the thirteenth-largest state by population and is divided into 14 districts with the capital being Thiruvananthapuram. Malayalam is the most widely spoken language and is also the official language of the state.
