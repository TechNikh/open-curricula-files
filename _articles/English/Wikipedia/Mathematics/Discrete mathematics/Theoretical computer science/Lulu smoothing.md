---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lulu_smoothing
offline_file: ""
offline_thumbnail: ""
uuid: 3fc1b285-7e2b-4525-adc7-5cfcfb0f673b
updated: 1484308678
title: Lulu smoothing
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-LUSmoother_width%253D1.png'
tags:
    - Properties
    - History
    - Operation
    - L operator
    - U Operator
    - Examples
categories:
    - Theoretical computer science
---
In signal processing, Lulu smoothing is a non-linear mathematical technique for removing impulsive noise from a data sequence such as a time series. It is a non-linear equivalent to taking a moving average (or other smoothing technique) of a time series, and is similar to other non-linear smoothing techniques, such as Tukey[1] or median smoothing.
