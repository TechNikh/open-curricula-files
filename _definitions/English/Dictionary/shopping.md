---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shopping
offline_file: ""
offline_thumbnail: ""
uuid: e22bccab-768a-4f9e-9e14-c31f67f649c8
updated: 1484310252
title: shopping
categories:
    - Dictionary
---
shop
     n 1: a mercantile establishment for the retail sale of goods or
          services; "he bought it at a shop on Cape Cod" [syn: {store}]
     2: small workplace where handcrafts or manufacturing are done
        [syn: {workshop}]
     3: a course of instruction in a trade (as carpentry or
        electricity); "I built a birdhouse in shop" [syn: {shop
        class}]
     v 1: do one's shopping; "She goes shopping every Friday"
     2: do one's shopping at; do business with; be a customer or
        client of [syn: {patronize}, {patronise}, {shop at}, {buy
        at}, {frequent}, {sponsor}] [ant: {boycott}, {boycott}]
     3: shop around; not necessarily buying; "I don't need ...
