---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/difficulty
offline_file: ""
offline_thumbnail: ""
uuid: 5b8d1774-5493-4685-aa2f-04a56e4f0fbe
updated: 1484310286
title: difficulty
categories:
    - Dictionary
---
difficulty
     n 1: an effort that is inconvenient; "I went to a lot of
          trouble"; "he won without any trouble"; "had difficulty
          walking"; "finished the test only with great difficulty"
          [syn: {trouble}]
     2: a factor causing trouble in achieving a positive result or
        tending to produce a negative result; "serious
        difficulties were encountered in obtaining a pure reagent"
     3: a condition or state of affairs almost beyond one's ability
        to deal with and requiring great effort to bear or
        overcome; "grappling with financial difficulties"
     4: the quality of being difficult; "they agreed about the
        difficulty of the ...
