---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/divided
offline_file: ""
offline_thumbnail: ""
uuid: 949639eb-1042-4e37-929a-3ce70c29b6c8
updated: 1484310373
title: divided
categories:
    - Dictionary
---
divided
     adj 1: separated into parts or pieces; "opinions are divided" [ant:
             {united}]
     2: having a median strip or island between lanes of traffic
        moving in opposite directions; "a divided highway" [syn: {dual-lane}]
     3: distributed in portions (often equal) on the basis of a plan
        or purpose [syn: {divided up}, {shared}, {shared out}]
