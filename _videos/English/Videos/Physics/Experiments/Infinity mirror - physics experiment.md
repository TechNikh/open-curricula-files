---
version: 1
type: video
provider: YouTube
id: XEAsxhVNDLE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 74ddb3eb-4db3-4f1e-bfe6-00df2a68ba64
updated: 1486069639
title: 'Infinity mirror - physics experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/XEAsxhVNDLE/default.jpg
    - https://i3.ytimg.com/vi/XEAsxhVNDLE/1.jpg
    - https://i3.ytimg.com/vi/XEAsxhVNDLE/2.jpg
    - https://i3.ytimg.com/vi/XEAsxhVNDLE/3.jpg
---
