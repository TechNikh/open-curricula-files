---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wijsman_convergence
offline_file: ""
offline_thumbnail: ""
uuid: 31369571-76de-491e-aed0-8c52df3104c0
updated: 1484309275
title: Wijsman convergence
tags:
    - History
    - Definition
    - Properties
categories:
    - Metric geometry
---
Wijsman convergence is a variation of Hausdorff convergence suitable for work with unbounded sets. Intuitively, Wijsman convergence is to convergence in the Hausdorff metric as pointwise convergence is to uniform convergence.
