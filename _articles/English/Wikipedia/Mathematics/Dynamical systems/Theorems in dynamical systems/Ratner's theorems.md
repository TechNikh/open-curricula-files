---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ratner%27s_theorems'
offline_file: ""
offline_thumbnail: ""
uuid: d3219764-9d0b-47e5-a666-66c4f15eaaf8
updated: 1484308692
title: "Ratner's theorems"
tags:
    - Short description
    - Expositions
    - Selected original articles
categories:
    - Theorems in dynamical systems
---
In mathematics, Ratner's theorems are a group of major theorems in ergodic theory concerning unipotent flows on homogeneous spaces proved by Marina Ratner around 1990. The theorems grew out of Ratner's earlier work on horocycle flows. The study of the dynamics of unipotent flows played a decisive role in the proof of the Oppenheim conjecture by Grigory Margulis. Ratner's theorems have guided key advances in the understanding of the dynamics of unipotent flows. Their later generalizations provide ways to both sharpen the results and extend the theory to the setting of arbitrary semisimple algebraic groups over a local field.
