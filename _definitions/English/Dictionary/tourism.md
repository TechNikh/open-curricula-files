---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tourism
offline_file: ""
offline_thumbnail: ""
uuid: a4a62db7-ac78-4362-bc47-664b6bf572df
updated: 1484310174
title: tourism
categories:
    - Dictionary
---
tourism
     n : the business of providing services to tourists; "Tourism is
         a major business in Bermuda" [syn: {touristry}]
