---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s_arcsine_law'
offline_file: ""
offline_thumbnail: ""
uuid: 801825b4-df94-44ca-9b2e-59217029f363
updated: 1484308639
title: Erdős arcsine law
categories:
    - Number theory
---
In number theory, the Erdős arcsine law, named after Paul Erdős, states that the prime divisors of a number have a distribution related to the arcsine distribution.
