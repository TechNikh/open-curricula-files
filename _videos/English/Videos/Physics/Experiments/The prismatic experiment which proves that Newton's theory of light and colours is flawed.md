---
version: 1
type: video
provider: YouTube
id: 7040Rjv1-TQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d852b1d5-3518-444d-aa49-9c876f77b3f7
updated: 1486069637
title: "The prismatic experiment which proves that Newton's theory of light and colours is flawed"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/7040Rjv1-TQ/default.jpg
    - https://i3.ytimg.com/vi/7040Rjv1-TQ/1.jpg
    - https://i3.ytimg.com/vi/7040Rjv1-TQ/2.jpg
    - https://i3.ytimg.com/vi/7040Rjv1-TQ/3.jpg
---
