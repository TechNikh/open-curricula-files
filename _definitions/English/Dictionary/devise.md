---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/devise
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484398861
title: devise
categories:
    - Dictionary
---
devise
     n 1: a will disposing of real property
     2: (law) a gift of real property by will
     v 1: come up with (an idea, plan, explanation, theory, or
          priciple) after a mental effort; "excogitate a way to
          measure the speed of light" [syn: {invent}, {contrive},
          {excogitate}, {formulate}, {forge}]
     2: arrange by systematic planning and united effort; "machinate
        a plot"; "organize a strike"; "devise a plan to take over
        the director's office" [syn: {organize}, {organise}, {prepare},
         {get up}, {machinate}]
     3: give by will, especially real property
