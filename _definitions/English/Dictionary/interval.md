---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interval
offline_file: ""
offline_thumbnail: ""
uuid: 5c2b005a-6af9-44e0-b34f-292ed74622bc
updated: 1484310232
title: interval
categories:
    - Dictionary
---
interval
     n 1: a definite length of time marked off by two instants [syn: {time
          interval}]
     2: a set containing all points (or all real numbers) between
        two given endpoints
     3: the distance between things; "fragile items require
        separation and cushioning" [syn: {separation}]
     4: the difference in pitch between two notes [syn: {musical
        interval}]
