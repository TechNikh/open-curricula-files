---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thyroid
offline_file: ""
offline_thumbnail: ""
uuid: 5a0ba1f7-5fd5-4ca3-8f73-0f2a8bf95945
updated: 1484310158
title: thyroid
categories:
    - Dictionary
---
thyroid
     adj 1: of or relating to the thyroid gland; "thyroid deficiency";
            "thyroidal uptake" [syn: {thyroidal}]
     2: suggestive of a thyroid disorder; "thyroid personality"
     n : located near the base of the neck [syn: {thyroid gland}]
