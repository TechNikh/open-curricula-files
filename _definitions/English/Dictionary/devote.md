---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/devote
offline_file: ""
offline_thumbnail: ""
uuid: c6e5f1a5-12a5-4002-9dcb-537756fa4bab
updated: 1484310563
title: devote
categories:
    - Dictionary
---
devote
     v 1: give entirely to a specific person, activity, or cause; "She
          committed herself to the work of God"; "give one's
          talents to a good cause"; "consecrate your life to the
          church" [syn: {give}, {dedicate}, {consecrate}, {commit}]
     2: dedicate; "give thought to"; "give priority to"; "pay
        attention to" [syn: {give}, {pay}]
     3: set aside or apart for a specific purpose or use; "this land
        was devoted to mining"
