---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zincite
offline_file: ""
offline_thumbnail: ""
uuid: 095f4059-358c-4858-aec3-259fb11a9214
updated: 1484308389
title: Zincite
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Zincite.jpg
categories:
    - Uncategorized
---
Zincite is the mineral form of zinc oxide (ZnO). Its crystal form is rare in nature; a notable exception to this is at the Franklin and Sterling Hill Mines in New Jersey, an area also famed for its many fluorescent minerals. It has a hexagonal crystal structure and a color that depends on the presence of impurities. The zincite found at the Franklin Furnace is red-colored, mostly due to iron and manganese dopants, and associated with willemite and franklinite.
