---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dominate
offline_file: ""
offline_thumbnail: ""
uuid: d1d438bc-9e58-48c6-97e7-08e69c531fb5
updated: 1484310301
title: dominate
categories:
    - Dictionary
---
dominate
     v 1: be larger in number, quantity, power, status or importance;
          "Money reigns supreme here"; "Hispanics predominate in
          this neighborhood" [syn: {predominate}, {rule}, {reign},
           {prevail}]
     2: be in control; rule the roost; "Her husband completely
        dominates her"
     3: have dominance or the power to defeat over; "Her pain
        completely mastered her"; "The methods can master the
        problems" [syn: {master}]
     4: look down on; "The villa dominates the town" [syn: {command},
         {overlook}, {overtop}]
