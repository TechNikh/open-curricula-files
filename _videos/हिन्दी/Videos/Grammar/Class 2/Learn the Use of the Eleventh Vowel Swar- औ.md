---
version: 1
type: video
provider: YouTube
id: 7Pu9jNZAsoQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e97a8f22-26ca-409a-a52e-86e826f89ae1
updated: 1486069630
title: 'Learn the Use of the Eleventh Vowel Swar- औ'
categories:
    - Class 2
thumbnail_urls:
    - https://i3.ytimg.com/vi/7Pu9jNZAsoQ/default.jpg
    - https://i3.ytimg.com/vi/7Pu9jNZAsoQ/1.jpg
    - https://i3.ytimg.com/vi/7Pu9jNZAsoQ/2.jpg
    - https://i3.ytimg.com/vi/7Pu9jNZAsoQ/3.jpg
---
