---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bedroom
offline_file: ""
offline_thumbnail: ""
uuid: a2ec999d-8222-4f34-aec2-d6b181f4fff7
updated: 1484310150
title: bedroom
categories:
    - Dictionary
---
bedroom
     n : a room used primarily for sleeping [syn: {sleeping room}, {chamber},
          {bedchamber}]
