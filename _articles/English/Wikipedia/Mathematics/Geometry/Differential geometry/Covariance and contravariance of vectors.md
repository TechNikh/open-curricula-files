---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Covariance_and_contravariance_of_vectors
offline_file: ""
offline_thumbnail: ""
uuid: 0b6071c4-2b95-407e-8251-5d729922f9e6
updated: 1484309212
title: Covariance and contravariance of vectors
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Vector_1-form.svg.png
tags:
    - Introduction
    - Definition
    - Contravariant transformation
    - Covariant transformation
    - Coordinates
    - >
        Covariant and contravariant components of a vector with a
        metric
    - Euclidean plane
    - Example
    - Three-dimensional Euclidean space
    - General Euclidean spaces
    - Informal usage
    - Use in tensor analysis
    - Algebra and geometry
    - Notes
    - Notes
categories:
    - Differential geometry
---
In multilinear algebra and tensor analysis, covariance and contravariance describe how the quantitative description of certain geometric or physical entities changes with a change of basis. In physics, a basis is sometimes thought of as a set of reference axes. A change of scale on the reference axes corresponds to a change of units in the problem. For instance, in changing scale from meters to centimeters (that is, dividing the scale of the reference axes by 100), the components of a measured velocity vector will multiply by 100. Vectors exhibit this behavior of changing scale inversely to changes in scale to the reference axes: they are contravariant. As a result, vectors often have units ...
