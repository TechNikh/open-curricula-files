---
version: 1
type: video
provider: YouTube
id: NIdqkwocNuE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Solid%20of%20Revolution%20%28part%205%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cc3c8fe8-9f2f-486a-810e-998df2fbea5c
updated: 1486069694
title: Solid of Revolution (part 5)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/NIdqkwocNuE/default.jpg
    - https://i3.ytimg.com/vi/NIdqkwocNuE/1.jpg
    - https://i3.ytimg.com/vi/NIdqkwocNuE/2.jpg
    - https://i3.ytimg.com/vi/NIdqkwocNuE/3.jpg
---
