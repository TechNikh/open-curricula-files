---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refraction
offline_file: ""
offline_thumbnail: ""
uuid: 7779646f-6aec-4437-a6d3-4c0ba4b86528
updated: 1484310212
title: refraction
categories:
    - Dictionary
---
refraction
     n 1: the change in direction of a propagating wave (light or
          sound) when passing from one medium to another
     2: the amount by which a propagating wave is bent [syn: {deflection},
         {deflexion}]
