---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dumped
offline_file: ""
offline_thumbnail: ""
uuid: d50b8fa6-a5af-478d-b0a9-a20c4a7b367c
updated: 1484310443
title: dumped
categories:
    - Dictionary
---
dumped
     adj : that is dropped in a heap; "his hastily dumped clothes";
           "the money was there, dumped all over the floor"
