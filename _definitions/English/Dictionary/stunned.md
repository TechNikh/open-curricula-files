---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stunned
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484520901
title: stunned
categories:
    - Dictionary
---
stun
     v 1: make senseless or dizzy by or as if by a blow; "stun fish"
          [syn: {stupefy}]
     2: surprise greatly; knock someone's socks off; "I was floored
        when I heard that I was promoted" [syn: {shock}, {floor},
        {ball over}, {blow out of the water}, {take aback}]
     3: hit something or somebody as if with a sandbag [syn: {sandbag}]
     4: overcome as with astonishment or disbelief; "The news
        stunned her" [syn: {bedaze}, {daze}]
     [also: {stunning}, {stunned}]
