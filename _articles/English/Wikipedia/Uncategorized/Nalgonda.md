---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nalgonda
offline_file: ""
offline_thumbnail: ""
uuid: b0d65f09-2070-4f65-bc70-c5718566d4f7
updated: 1484308337
title: Nalgonda
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-APSRTC_Busbay_Nalgonda.jpg
tags:
    - Etymology
    - History
    - Paleolithic Age
    - Neolithic Age
    - The Mauryas and Satavahanas (230 BC – 218 BC)
    - Ikshvakus (227 AD – 306 AD)
    - Invasion of Samudragupta
    - The Chalukyas and Rashtrakutas
    - Medieval period
    - 'Modern period: Mughals and Asaf Jahis'
    - geography
    - Demographics
    - Governance
    - Transport
    - Attractions
    - Education
categories:
    - Uncategorized
---
Nalgonda ( pronunciation (help·info)) is a city and municipality in the Indian state of Telangana. It is the headquarters of the homonymous district, as well as the headquarters of the Nalgonda mandal in the Nalgonda revenue division.[4]
