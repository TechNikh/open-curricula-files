---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cooperation
offline_file: ""
offline_thumbnail: ""
uuid: 6925cfb3-2b8b-4d38-ae50-389815230dfb
updated: 1484310593
title: cooperation
categories:
    - Dictionary
---
cooperation
     n 1: joint operation or action; "their cooperation with us was
          essential for the success of our mission" [ant: {competition}]
     2: the practice of cooperating; "economic cooperation"; "they
        agreed on a policy of cooperation"
