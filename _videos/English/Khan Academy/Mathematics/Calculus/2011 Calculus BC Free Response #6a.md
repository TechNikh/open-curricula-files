---
version: 1
type: video
provider: YouTube
id: 1BGlbx67B6s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20BC%20Free%20Response%20%236a.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b7c5341c-54bc-4813-80fd-4d80149cb9b5
updated: 1486069690
title: '2011 Calculus BC Free Response #6a'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/1BGlbx67B6s/default.jpg
    - https://i3.ytimg.com/vi/1BGlbx67B6s/1.jpg
    - https://i3.ytimg.com/vi/1BGlbx67B6s/2.jpg
    - https://i3.ytimg.com/vi/1BGlbx67B6s/3.jpg
---
