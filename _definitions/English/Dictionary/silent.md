---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silent
offline_file: ""
offline_thumbnail: ""
uuid: 2a077b35-c1df-4504-9495-2d40a93b555b
updated: 1484310539
title: silent
categories:
    - Dictionary
---
silent
     adj 1: marked by absence of sound; "a silent house"; "soundless
            footsteps on the grass"; "the night was still" [syn: {soundless},
             {still}]
     2: failing to speak or communicate etc when expected to; "the
        witness remained silent" [syn: {mum}]
     3: indicated by necessary connotation though not expressed
        directly; "gave silent consent"; "a tacit agreement"; "the
        understood provisos of a custody agreement" [syn: {implied},
         {tacit}, {understood}]
     4: not made to sound; "the silent `h' at the beginning of
        `honor'"; "in French certain letters are often unsounded"
        [syn: {unsounded}]
     5: having a ...
