---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/puller
offline_file: ""
offline_thumbnail: ""
uuid: 757c6102-d9ad-465e-84df-9284bf6abdad
updated: 1484310535
title: puller
categories:
    - Dictionary
---
puller
     n 1: someone who applies force so as to cause motion toward
          herself or himself
     2: someone who pulls or tugs or drags in an effort to move
        something [syn: {tugger}, {dragger}]
