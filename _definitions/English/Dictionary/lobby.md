---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lobby
offline_file: ""
offline_thumbnail: ""
uuid: 493cf07f-b808-42b7-a99d-99a66fb1d37c
updated: 1484310166
title: lobby
categories:
    - Dictionary
---
lobby
     n 1: a large entrance or reception room or area [syn: {anteroom},
           {antechamber}, {entrance hall}, {hall}, {foyer}, {vestibule}]
     2: a group of people who try actively to influence legislation
        [syn: {pressure group}, {third house}]
     v : detain in conversation by or as if by holding on to the
         outer garments of; as for political or economic favors
         [syn: {buttonhole}]
     [also: {lobbied}]
