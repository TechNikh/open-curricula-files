---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hyphenated
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484465521
title: hyphenated
categories:
    - Dictionary
---
hyphenated
     adj : (of words) combined by means of a hyphen to form a unit;
           "hyphenated words"; "many femninists have chosen
           hyphenated names their children--today's so-called
           hyphenated babies"; "multiculturalism nurtures respect
           for hyphenated identities"
