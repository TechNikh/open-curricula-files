---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sphere_packing
offline_file: ""
offline_thumbnail: ""
uuid: 703191fd-825b-49aa-b8b8-3d3c9f2f6b72
updated: 1484308611
title: Sphere packing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-HCP_Oranges.jpg
tags:
    - Classification and terminology
    - Regular packing
    - Dense packing
    - Other common lattice packings
    - Jammed packings with a low density
    - Irregular packing
    - Hypersphere packing
    - Unequal sphere packing
    - Hyperbolic space
    - Touching pairs, triplets, and quadruples
    - Other spaces
    - Bibliography
categories:
    - Discrete geometry
---
In geometry, a sphere packing is an arrangement of non-overlapping spheres within a containing space. The spheres considered are usually all of identical size, and the space is usually three-dimensional Euclidean space. However, sphere packing problems can be generalised to consider unequal spheres, n-dimensional Euclidean space (where the problem becomes circle packing in two dimensions, or hypersphere packing in higher dimensions) or to non-Euclidean spaces such as hyperbolic space.
