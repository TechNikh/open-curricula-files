---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/renewable
offline_file: ""
offline_thumbnail: ""
uuid: 1b7a3487-6f3b-4f46-8589-8554c0083ece
updated: 1484310254
title: renewable
categories:
    - Dictionary
---
renewable
     adj 1: that can be renewed or extended; "a renewable lease";
            "renewable subscriptions" [ant: {unrenewable}]
     2: capable of being renewed; replaceable; "renewable energy
        such as solar energy is theoretically inexhaustible"
