---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tibet
offline_file: ""
offline_thumbnail: ""
uuid: 9f2a0d5c-963a-4e27-a936-9a4b81bb3e8b
updated: 1484308435
title: Tibet
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/125px-Tibet_%2528Chinese_and_Tibetan%2529.svg.png'
tags:
    - Names
    - language
    - History
    - Early history
    - Tibetan Empire
    - Yuan dynasty
    - Phagmodrupa, Rinpungpa and Tsangpa Dynasties
    - Rise of Ganden Phodrang
    - Qing dynasty
    - Post-Qing period
    - From 1950 to present
    - geography
    - Cities, towns and villages
    - government
    - Economy
    - Development zone
    - Demographics
    - culture
    - religion
    - Buddhism
    - Christianity
    - Islam
    - Tibetan art
    - Architecture
    - Music
    - Festivals
    - Cuisine
    - Notes
categories:
    - Uncategorized
---
Tibet (i/tɪˈbɛt/; Tibetan: བོད་, Wylie: bod, Tibetan Pinyin: boew, pronounced [pøː˩˧˨]; Chinese: 西藏; pinyin: Xīzàng /ɕi⁵⁵ t͡sɑŋ⁵¹/) is a region on the Tibetan Plateau in Asia. It is the traditional homeland of the Tibetan people as well as some other ethnic groups such as Monpa, Qiang, and Lhoba peoples and is now also inhabited by considerable numbers of Han Chinese and Hui people. Tibet is the highest region on Earth, with an average elevation of 4,900 metres (16,000 ft). The highest elevation in Tibet is Mount Everest, Earth's highest mountain, rising 8,848 m (29,029 ft) above sea level.
