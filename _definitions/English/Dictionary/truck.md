---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/truck
offline_file: ""
offline_thumbnail: ""
uuid: 9df373c1-400b-4d62-b7bd-7263d8bf6835
updated: 1484310522
title: truck
categories:
    - Dictionary
---
truck
     n 1: an automotive vehicle suitable for hauling [syn: {motortruck}]
     2: a handcart that has a frame with two low wheels and a ledge
        at the bottom and handles at the top; used to move crates
        or other heavy objects [syn: {hand truck}]
     v : convey (goods etc.) by truck; "truck fresh vegetables across
         the mountains"
