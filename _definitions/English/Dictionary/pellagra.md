---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pellagra
offline_file: ""
offline_thumbnail: ""
uuid: 82233b5b-e34f-4366-9ebc-2bad02cc52c6
updated: 1484310162
title: pellagra
categories:
    - Dictionary
---
pellagra
     n : a disease caused by deficiency of niacin or tryptophan (or
         by a defect in the metabolic conversion of tryptophan to
         niacin); characterized by gastrointestinal disturbances
         and erythema and nervous or mental disorders; may be
         caused by malnutrition or alcoholism or other nutritional
         impairments [syn: {Alpine scurvy}, {mal de la rosa}, {mal
         rosso}, {maidism}, {mayidism}, {Saint Ignatius' itch}]
