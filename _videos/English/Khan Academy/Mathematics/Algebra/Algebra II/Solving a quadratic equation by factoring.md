---
version: 1
type: video
provider: YouTube
id: 2ZzuZvz33X0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Solving%20a%20quadratic%20equation%20by%20factoring.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 994fdbe8-707e-4613-945b-ffb1ecc5784c
updated: 1486069563
title: Solving a quadratic equation by factoring
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/2ZzuZvz33X0/default.jpg
    - https://i3.ytimg.com/vi/2ZzuZvz33X0/1.jpg
    - https://i3.ytimg.com/vi/2ZzuZvz33X0/2.jpg
    - https://i3.ytimg.com/vi/2ZzuZvz33X0/3.jpg
---
