---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laurasia
offline_file: ""
offline_thumbnail: ""
uuid: 3da4e606-7f00-4a81-a1ed-3c0c550eab0a
updated: 1484308415
title: Laurasia
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Laurasia-Gondwana.svg_0.png
tags:
    - Origin
    - Breakup and reformation
    - Final split
categories:
    - Uncategorized
---
Laurasia (pronunciation: /lɔːˈreɪʒə/ or /lɔːˈreɪʃiə/)[1] was the more northern of two supercontinents (the other being Gondwana) that formed part of the Pangaea supercontinent around 300 to 200 million years ago (Mya). It separated from Gondwana 200 to 180 Mya (beginning in the late Triassic period) during the breakup of Pangaea, drifting farther north after the split.[2]
