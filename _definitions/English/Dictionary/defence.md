---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defence
offline_file: ""
offline_thumbnail: ""
uuid: 40428dea-5ca1-4a77-a81b-fa7ac66dc952
updated: 1484310453
title: defence
categories:
    - Dictionary
---
defence
     n 1: (psychiatry) an unconscious process that tries to reduce the
          anxiety associated with instinctive desires [syn: {defense
          mechanism}, {defense reaction}, {defence mechanism}, {defence
          reaction}, {defense}]
     2: (sports) the team that is trying to prevent the other team
        from scoring; "his teams are always good on defense" [syn:
         {defense}, {defending team}] [ant: {offense}, {offense}]
     3: the defendant and his legal advisors collectively; "the
        defense called for a mistrial" [syn: {defense}, {defense
        team}, {defense lawyers}] [ant: {prosecution}]
     4: an organization of defenders that provides resistance
  ...
