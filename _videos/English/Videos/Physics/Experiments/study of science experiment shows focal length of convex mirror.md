---
version: 1
type: video
provider: YouTube
id: 75fma0rYw1A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 27c65e1c-fd73-4301-bb6b-2332891f7c77
updated: 1486069635
title: >
    study of science experiment shows focal length of convex
    mirror
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/75fma0rYw1A/default.jpg
    - https://i3.ytimg.com/vi/75fma0rYw1A/1.jpg
    - https://i3.ytimg.com/vi/75fma0rYw1A/2.jpg
    - https://i3.ytimg.com/vi/75fma0rYw1A/3.jpg
---
