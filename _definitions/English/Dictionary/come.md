---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/come
offline_file: ""
offline_thumbnail: ""
uuid: 5169c8b4-5fe1-441d-ac12-a0d6a41088d7
updated: 1484310337
title: come
categories:
    - Dictionary
---
come
     v 1: move toward, travel toward something or somebody or approach
          something or somebody; "He came singing down the road";
          "Come with me to the Casbah"; "come down here!"; "come
          out of the closet!"; "come into the room" [syn: {come up}]
          [ant: {go}]
     2: reach a destination; arrive by movement or progress; "She
        arrived home at 7 o'clock"; "She didn't get to Chicago
        until after midnight" [syn: {arrive}, {get}] [ant: {leave}]
     3: come to pass; arrive, as in due course; "The first success
        came three days later"; "It came as a shock"; "Dawn comes
        early in June"
     4: reach a state, relation, or condition; ...
