---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subset
offline_file: ""
offline_thumbnail: ""
uuid: 09d5c9a2-4067-4213-90a7-915135aec865
updated: 1484310138
title: subset
categories:
    - Dictionary
---
subset
     n : a set whose members are members of another set; a set
         contained within another set
