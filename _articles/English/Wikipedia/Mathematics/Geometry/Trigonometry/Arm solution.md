---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arm_solution
offline_file: ""
offline_thumbnail: ""
uuid: 9c39fb01-8c97-4cde-9c55-74c76046da50
updated: 1484309338
title: Arm solution
categories:
    - Trigonometry
---
In applied mathematics as used in the engineering field of robotics, an arm solution is a solution of equations that allow the calculation of the precise design parameters of a robot's arms in such a way as to enable it to make certain movements.
