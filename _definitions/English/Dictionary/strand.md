---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strand
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484642581
title: strand
categories:
    - Dictionary
---
strand
     n 1: a pattern forming a unity within a larger structural whole;
          "he tried to pick up the strands of his former life"; "I
          could hear several melodic strands simultaneously"
     2: line consisting of a complex of fibers or filaments that are
        twisted together to form a thread or a rope or a cable
     3: a necklace made by a stringing objects together; "a string
        of beads"; "a strand of pearls"; [syn: {chain}, {string}]
     4: a very slender natural or synthetic fiber [syn: {fibril}, {filament}]
     5: a poetic term for a shore (as the area periodically covered
        and uncovered by the tides)
     6: a street in west central London famous ...
