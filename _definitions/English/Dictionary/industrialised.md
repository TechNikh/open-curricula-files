---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/industrialised
offline_file: ""
offline_thumbnail: ""
uuid: bb4d257a-7a9b-46ad-b083-afd4cb38e71c
updated: 1484310549
title: industrialised
categories:
    - Dictionary
---
industrialised
     adj : made industrial; converted to industrialism; "industrialized
           areas" [syn: {industrialized}]
