---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forge
offline_file: ""
offline_thumbnail: ""
uuid: b1467046-c44e-4071-867b-df41af30c47f
updated: 1484310583
title: forge
categories:
    - Dictionary
---
forge
     n 1: furnace consisting of a special hearth where metal is heated
          before shaping
     2: a workplace where metal is worked by heating and hammering
        [syn: {smithy}]
     v 1: create by hammering; "hammer the silver into a bowl"; "forge
          a pair of tongues" [syn: {hammer}]
     2: make a copy of with the intent to deceive; "he faked the
        signature"; "they counterfeited dollar bills"; "She forged
        a Green Card" [syn: {fake}, {counterfeit}]
     3: come up with (an idea, plan, explanation, theory, or
        priciple) after a mental effort; "excogitate a way to
        measure the speed of light" [syn: {invent}, {contrive}, {devise},
         ...
