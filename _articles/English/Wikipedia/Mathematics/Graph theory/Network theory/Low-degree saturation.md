---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Low-degree_saturation
offline_file: ""
offline_thumbnail: ""
uuid: 99120735-4a38-40c6-a635-d7b221194be4
updated: 1484309435
title: Low-degree saturation
tags:
    - Theoretical foundation
    - Significance
categories:
    - Network theory
---
In a scale-free network the degree distribution follows a power law function. In some empirical examples this power-law fits the degree distribution well only in the high degree region, however for small degree nodes the empirical degree-distribution deviates from it. See for example the network of scientific citations.[1] This deviation of the observed degree-distribution from the theoretical prediction at the low-degree region is often referred as low-degree saturation.[2]
