---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/priority
offline_file: ""
offline_thumbnail: ""
uuid: 21e4440a-30ab-4227-b782-7d7cdd58f3b6
updated: 1484310421
title: priority
categories:
    - Dictionary
---
priority
     n 1: status established in order of importance or urgency;
          "...its precedence as the world's leading manufacturer
          of pharmaceuticals"; "national independence takes
          priority over class struggle" [syn: {precedence}, {precedency}]
     2: preceding in time [syn: {antecedence}, {antecedency}, {anteriority},
         {precedence}, {precedency}] [ant: {posteriority}]
