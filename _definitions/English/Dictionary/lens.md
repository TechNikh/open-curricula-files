---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lens
offline_file: ""
offline_thumbnail: ""
uuid: 5c6169db-3571-467d-a8de-fbe40164a001
updated: 1484310208
title: lens
categories:
    - Dictionary
---
lens
     n 1: a transparent optical device used to converge or diverge
          transmitted light and to form images [syn: {lense}, {lens
          system}]
     2: genus of small erect or climbing herbs with pinnate leaves
        and small inconspicuous white flowers and small flattened
        pods: lentils [syn: {genus Lens}]
     3: (metaphor) a channel through which something can be seen or
        understood; "the writer is the lens through which history
        can be seen"
     4: biconvex transparent body situated behind the iris in the
        eye; it focuses light waves on the retina [syn: {crystalline
        lens}]
     5: electronic equipment that uses a magnetic or ...
