---
version: 1
type: video
provider: YouTube
id: RdYA8ZpqdJE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Polynomial%20and%20rational%20functions/Solving%20rational%20equations%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b7724022-126e-403a-a200-e25d3d1f533d
updated: 1486069563
title: Solving rational equations 2
categories:
    - Polynomial and rational functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/RdYA8ZpqdJE/default.jpg
    - https://i3.ytimg.com/vi/RdYA8ZpqdJE/1.jpg
    - https://i3.ytimg.com/vi/RdYA8ZpqdJE/2.jpg
    - https://i3.ytimg.com/vi/RdYA8ZpqdJE/3.jpg
---
