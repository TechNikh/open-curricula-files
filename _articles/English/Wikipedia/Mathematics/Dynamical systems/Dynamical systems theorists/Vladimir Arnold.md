---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vladimir_Arnold
offline_file: ""
offline_thumbnail: ""
uuid: d64601d8-13af-4cfd-a9bc-6895e3386a03
updated: 1484308688
title: Vladimir Arnold
tags:
    - Biography
    - Death
    - Popular mathematical writings
    - Work
    - Dynamical systems
    - Singularity theory
    - Fluid dynamics
    - Real algebraic geometry
    - Symplectic geometry
    - Topology
    - Other
    - Honours and awards
    - Fields Medal omission
    - Selected bibliography
categories:
    - Dynamical systems theorists
---
Vladimir Igorevich Arnold (alternative spelling Arnol'd, Russian: Влади́мир И́горевич Арно́льд, 12 June 1937 – 3 June 2010)[1][2] was a Soviet and Russian mathematician. While he is best known for the Kolmogorov–Arnold–Moser theorem regarding the stability of integrable systems, he made important contributions in several areas including dynamical systems theory, catastrophe theory, topology, algebraic geometry, symplectic geometry, differential equations, classical mechanics, hydrodynamics and singularity theory, including posing the ADE classification problem, since his first main result—the solution of Hilbert's thirteenth problem in 1957 at the age of 19.
