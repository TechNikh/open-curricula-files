---
version: 1
type: video
provider: YouTube
id: _5G-o4o3f3Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 21179380-fc72-4013-97f2-94d48a60429b
updated: 1486069621
title: Arithmetic Progressions Aptitude Shortcut Tricks
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/_5G-o4o3f3Q/default.jpg
    - https://i3.ytimg.com/vi/_5G-o4o3f3Q/1.jpg
    - https://i3.ytimg.com/vi/_5G-o4o3f3Q/2.jpg
    - https://i3.ytimg.com/vi/_5G-o4o3f3Q/3.jpg
---
