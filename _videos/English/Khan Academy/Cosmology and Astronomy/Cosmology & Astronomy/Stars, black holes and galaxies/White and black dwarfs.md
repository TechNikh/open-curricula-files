---
version: 1
type: video
provider: YouTube
id: EdYyuUUY-nc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/White%20and%20black%20dwarfs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f4c8ee30-c8cd-4606-8707-3ca3e8afd7aa
updated: 1486069686
title: White and black dwarfs
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/EdYyuUUY-nc/default.jpg
    - https://i3.ytimg.com/vi/EdYyuUUY-nc/1.jpg
    - https://i3.ytimg.com/vi/EdYyuUUY-nc/2.jpg
    - https://i3.ytimg.com/vi/EdYyuUUY-nc/3.jpg
---
