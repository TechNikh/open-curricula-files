---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supreme
offline_file: ""
offline_thumbnail: ""
uuid: ed515d45-57fd-4c5b-b981-49b0ec991ca0
updated: 1484310467
title: supreme
categories:
    - Dictionary
---
supreme
     adj 1: final or last in your life or progress; "the supreme
            sacrifice"; "the supreme judgment"
     2: greatest in status or authority or power; "a supreme
        tribunal" [syn: {sovereign}]
     3: highest in excellence or achievement; "supreme among
        musicians"; "a supreme endeavor"; "supreme courage"
     4: greatest or maximal in degree; extreme; "supreme folly"
