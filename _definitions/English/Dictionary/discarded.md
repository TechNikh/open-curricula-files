---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discarded
offline_file: ""
offline_thumbnail: ""
uuid: 7231f14d-dbc6-49d5-be0f-455c732ff92b
updated: 1484310539
title: discarded
categories:
    - Dictionary
---
discarded
     adj 1: thrown away; "wearing someone's cast-off clothes";
            "throwaway children living on the streets"; "salvaged
            some thrown-away furniture" [syn: {cast-off(a)}, {throwaway(a)},
             {thrown-away(a)}]
     2: disposed of as useless; "waste paper" [syn: {cast-off(a)}, {junked},
         {scrap(a)}, {waste}]
