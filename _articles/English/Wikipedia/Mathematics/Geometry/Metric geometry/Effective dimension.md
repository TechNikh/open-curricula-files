---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Effective_dimension
offline_file: ""
offline_thumbnail: ""
uuid: 3358a421-491d-47c9-b5c9-aacdb81867a2
updated: 1484309262
title: Effective dimension
tags:
    - Rigorous definitions
    - Martingales and other gales
    - Kolmogorov complexity definition
    - Comparison to classical dimension
categories:
    - Metric geometry
---
In mathematics, effective dimension is a modification of Hausdorff dimension and other fractal dimensions which places it in a computability theory setting. There are several variations (various notions of effective dimension) of which the most common is effective Hausdorff dimension. Dimension, in mathematics, is a particular way of describing the size of an object (contrasting with measure and other, different, notions of size). Hausdorff dimension generalizes the well-known integer dimensions assigned to points, lines, planes, etc. by allowing one to distinguish between objects of intermediate size between these integer-dimensional objects. For example, fractal subsets of the plane may ...
