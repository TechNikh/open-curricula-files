---
version: 1
type: article
id: https://en.wikipedia.org/wiki/SPQR_tree
offline_file: ""
offline_thumbnail: ""
uuid: ea725634-206d-4ba0-b607-758c4bec2a9b
updated: 1484309404
title: SPQR tree
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-SPQR_tree_2.svg.png
tags:
    - Structure
    - Construction
    - Usage
    - Finding 2-vertex cuts
    - Representing all embeddings of planar graphs
    - Notes
categories:
    - Graph connectivity
---
In graph theory, a branch of mathematics, the triconnected components of a biconnected graph are a system of smaller graphs that describe all of the 2-vertex cuts in the graph. An SPQR tree is a tree data structure used in computer science, and more specifically graph algorithms, to represent the triconnected components of a graph. The SPQR tree of a graph may be constructed in linear time[1] and has several applications in dynamic graph algorithms and graph drawing.
