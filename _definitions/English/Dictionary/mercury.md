---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mercury
offline_file: ""
offline_thumbnail: ""
uuid: 79567b93-a8c9-4e14-8541-e941edc78731
updated: 1484310232
title: mercury
categories:
    - Dictionary
---
mercury
     n 1: a heavy silvery toxic univalent and bivalent metallic
          element; the only metal that is liquid at ordinary
          temperatures [syn: {quicksilver}, {hydrargyrum}, {Hg}, {atomic
          number 80}]
     2: (Roman mythology) messenger of Jupiter and god of commerce;
        counterpart of Greek Hermes
     3: the smallest planet and the nearest to the sun
     4: temperature measured by a mercury thermometer; "the mercury
        was falling rapidly"
