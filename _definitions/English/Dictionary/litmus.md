---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/litmus
offline_file: ""
offline_thumbnail: ""
uuid: 6bf20438-f3b3-41d7-8fbd-d35b8c4d476f
updated: 1484310371
title: litmus
categories:
    - Dictionary
---
litmus
     n : a coloring material (obtained from lichens) that turns red
         in acid solutions and blue in alkaline solutions; used as
         a very rough acid-base indicator [syn: {litmus test}]
