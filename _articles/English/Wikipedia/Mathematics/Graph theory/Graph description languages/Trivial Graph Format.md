---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trivial_Graph_Format
offline_file: ""
offline_thumbnail: ""
uuid: 14b18ac2-0a0c-42a6-9c59-8303b20836ad
updated: 1484309413
title: Trivial Graph Format
categories:
    - Graph description languages
---
Trivial Graph Format (TGF) is a simple text-based file format for describing graphs. It consists of a list of node definitions, which map node IDs to labels, followed by a list of edges, which specify node pairs and an optional edge label. Node IDs can be arbitrary identifiers, whereas labels for both nodes and edges are plain strings.
