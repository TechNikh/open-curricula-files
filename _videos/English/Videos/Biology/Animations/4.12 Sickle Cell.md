---
version: 1
type: video
provider: YouTube
id: LwJNW6aREYg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/4.12%20Sickle%20Cell.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cb1c1e14-d0cc-49da-9882-671658c5b315
updated: 1486069674
title: 4.12 Sickle Cell
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/LwJNW6aREYg/default.jpg
    - https://i3.ytimg.com/vi/LwJNW6aREYg/1.jpg
    - https://i3.ytimg.com/vi/LwJNW6aREYg/2.jpg
    - https://i3.ytimg.com/vi/LwJNW6aREYg/3.jpg
---
