---
version: 1
type: video
provider: YouTube
id: lVadjWOjvV8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stellar%20distance%20using%20parallax.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fc84b7cb-9f13-4f14-af07-55cc5804e09f
updated: 1486069688
title: Stellar distance using parallax
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/lVadjWOjvV8/default.jpg
    - https://i3.ytimg.com/vi/lVadjWOjvV8/1.jpg
    - https://i3.ytimg.com/vi/lVadjWOjvV8/2.jpg
    - https://i3.ytimg.com/vi/lVadjWOjvV8/3.jpg
---
