---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Order-7_heptagonal_tiling
offline_file: ""
offline_thumbnail: ""
uuid: ba09f1e1-1ab8-440c-ab07-eb109b4a9f51
updated: 1484309369
title: Order-7 heptagonal tiling
tags:
    - Related tilings
categories:
    - Geometry stubs
---
In geometry, the order-7 heptagonal tiling is a regular tiling of the hyperbolic plane. It has Schläfli symbol of {7,7}, constructed from seven heptagons around every vertex. As such, it is self-dual.
