---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inject
offline_file: ""
offline_thumbnail: ""
uuid: 08b9cbf5-b8a5-40cc-9aca-416b6414146d
updated: 1484310383
title: inject
categories:
    - Dictionary
---
inject
     v 1: give an injection to; "We injected the glucose into the
          patient's vein" [syn: {shoot}]
     2: to introduce (a new aspect or element); "He injected new
        life into the performance"
     3: force or drive (a fluid or gas) into by piercing; "inject
        hydrogen into the balloon" [syn: {shoot}]
     4: take by injection; "inject heroin"
     5: feed intravenously
     6: to insert between other elements; "She interjected clever
        remarks" [syn: {interject}, {come in}, {interpose}, {put
        in}, {throw in}]
