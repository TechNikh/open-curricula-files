---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extract
offline_file: ""
offline_thumbnail: ""
uuid: 883d0675-1e25-44fd-97e0-63b5d6958ba3
updated: 1484310246
title: extract
categories:
    - Dictionary
---
extract
     n 1: a solution obtained by steeping or soaking a substance
          (usually in water) [syn: {infusion}]
     2: a passage selected from a larger work; "he presented
        excerpts from William James' philosophical writings" [syn:
         {excerpt}, {selection}]
     v 1: draw or pull out, usually with some force or effort; also
          used in an abstract sense; "pull weeds"; "extract a bad
          tooth"; "take out a splinter"; "extract information from
          the telegram" [syn: {pull out}, {pull}, {pull up}, {take
          out}, {draw out}]
     2: get despite difficulties or obstacles; "I extracted a
        promise from the Dean for two ne positions"
     3: ...
