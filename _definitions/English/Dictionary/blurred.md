---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blurred
offline_file: ""
offline_thumbnail: ""
uuid: 4e612d33-1e33-457e-baba-04c6cc035184
updated: 1484310221
title: blurred
categories:
    - Dictionary
---
blur
     n : a hazy or indistinct representation; "it happened so fast it
         was just a blur"; "he tried to clear his head of the
         whisky fuzz" [syn: {fuzz}]
     v 1: become glassy; lose clear vision; "Her eyes glazed over from
          lack of sleep" [syn: {film over}, {glaze over}]
     2: to make less distinct or clear; "The haze blurs the hills"
        [ant: {focus}]
     3: make unclear, indistinct, or blurred; "Her remarks confused
        the debate"; "Their words obnubilate their intentions"
        [syn: {confuse}, {obscure}, {obnubilate}]
     4: make a smudge on; soil by smudging [syn: {smear}, {smudge},
        {smutch}]
     5: make dim or indistinct; "The ...
