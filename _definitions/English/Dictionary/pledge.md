---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pledge
offline_file: ""
offline_thumbnail: ""
uuid: 5b9f4469-95f5-4a5d-b8b3-381a3454e931
updated: 1484310249
title: pledge
categories:
    - Dictionary
---
pledge
     n 1: a deposit of personal property as security for a debt; "his
          saxophone was in pledge"
     2: someone accepted for membership but not yet fully admitted
        to the group
     3: a drink in honor of or to the health of a person or event
        [syn: {toast}]
     4: a binding commitment to do or give or refrain from
        something; "an assurance of help when needed"; "signed a
        pledge never to reveal the secret" [syn: {assurance}]
     v 1: promise solemnly and formally; "I pledge that will honor my
          wife" [syn: {plight}]
     2: pay (an amount of money) as a contribution to a charity or
        service, especially at regular intervals; "I ...
