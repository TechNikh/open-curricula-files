---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slide
offline_file: ""
offline_thumbnail: ""
uuid: 8433b5cd-f3cf-4e32-a7cd-b661f2aedf47
updated: 1484310333
title: slide
categories:
    - Dictionary
---
slide
     n 1: a small flat rectangular piece of glass on which specimens
          can be mounted for microscopic study [syn: {microscope
          slide}]
     2: (geology) the descent of a large mass of earth or rocks or
        snow etc.
     3: (music) rapid sliding up or down the musical scale; "the
        violinist was indulgent with his swoops and slides" [syn:
        {swoop}]
     4: plaything consisting of a sloping chute down which children
        can slide
     5: the act of moving smoothly along a surface while remaining
        in contact with it; "his slide didn't stop until the
        bottom of the hill"; "the children lined up for a coast
        down the snowy slope" ...
