---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poison
offline_file: ""
offline_thumbnail: ""
uuid: e3fdcad2-56cf-4631-92d0-38644c4eaebc
updated: 1484310541
title: poison
categories:
    - Dictionary
---
poison
     n 1: any substance that causes injury or illness or death of a
          living organism [syn: {poisonous substance}]
     2: anything that harms or destroys; "the poison of fascism"
     v 1: spoil as if by poison; "poison someone's mind"; "poison the
          atmosphere in the office"
     2: kill by its poison; "This mushrooms can kill"
     3: kill with poison; "She poisoned her husband"
     4: add poison to; "Her husband poisoned her drink in order to
        kill her" [syn: {envenom}]
     5: administer poison to; "She poisoned her husband but he did
        not die"
