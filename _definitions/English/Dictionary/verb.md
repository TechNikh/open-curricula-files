---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/verb
offline_file: ""
offline_thumbnail: ""
uuid: 966de572-dfcc-4bd3-98ac-c8a43167f1db
updated: 1484310148
title: verb
categories:
    - Dictionary
---
verb
     n 1: a word that serves as the predicate of a sentence
     2: a content word that denotes an action or a state
