---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Noncommutative_geometry
offline_file: ""
offline_thumbnail: ""
uuid: c363d2f8-557e-455b-8644-62ae340a2477
updated: 1484309290
title: Noncommutative geometry
tags:
    - Motivation
    - Applications in mathematical physics
    - Motivation from ergodic theory
    - 'Noncommutative C*-algebras, von Neumann algebras'
    - Noncommutative differentiable manifolds
    - Noncommutative affine and projective schemes
    - Invariants for noncommutative spaces
    - Examples of noncommutative spaces
    - Notes
categories:
    - Noncommutative geometry
---
Noncommutative geometry (NCG) is a branch of mathematics concerned with a geometric approach to noncommutative algebras, and with the construction of spaces that are locally presented by noncommutative algebras of functions (possibly in some generalized sense). A noncommutative algebra is an associative algebra in which the multiplication is not commutative, that is, for which 
  
    
      
        x
        y
      
    
    {\displaystyle xy}
  
 does not always equal 
  
    
      
        y
        x
      
    
    {\displaystyle yx}
  
; or more generally an algebraic structure in which one of the principal binary operations is not commutative; one also allows additional ...
