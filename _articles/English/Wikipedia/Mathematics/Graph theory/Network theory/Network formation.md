---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Network_formation
offline_file: ""
offline_thumbnail: ""
uuid: e709a083-a8bf-4575-b76b-e02325da2dd3
updated: 1484309438
title: Network formation
tags:
    - Dynamic models
    - Agent-based models
    - Growing networks in agent-based setting
categories:
    - Network theory
---
Network formation is an aspect of network science that seeks to model how a network evolves by identifying which factors affect its structure and how these mechanisms operate. Network formation hypotheses are tested by using either a dynamic model with an increasing network size or by making an agent-based model to determine which network structure is the equilibrium in a fixed-size network.
