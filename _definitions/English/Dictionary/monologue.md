---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monologue
offline_file: ""
offline_thumbnail: ""
uuid: 100e9a36-f19c-4a8b-8a5b-04fabf8a07c7
updated: 1484310277
title: monologue
categories:
    - Dictionary
---
monologue
     n 1: speech you make to yourself [syn: {soliloquy}]
     2: a long utterance by one person (especially one that prevents
        others from participating in the conversation)
     3: a (usually long) dramatic speech by a single actor
