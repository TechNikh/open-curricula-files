---
version: 1
type: video
provider: YouTube
id: duGA94-usUs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1074d46f-c19a-4cb9-bc20-cf7744a9d864
updated: 1486069668
title: 'Balloon Angioplasty and Stenting Video Animation - Coronary Angiography and Heart Stent Procedure'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/duGA94-usUs/default.jpg
    - https://i3.ytimg.com/vi/duGA94-usUs/1.jpg
    - https://i3.ytimg.com/vi/duGA94-usUs/2.jpg
    - https://i3.ytimg.com/vi/duGA94-usUs/3.jpg
---
