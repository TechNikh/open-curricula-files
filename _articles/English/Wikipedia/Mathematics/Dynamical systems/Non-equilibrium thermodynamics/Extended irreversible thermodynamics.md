---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Extended_irreversible_thermodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 0107f08c-93a8-438b-8f5a-6aff81b14388
updated: 1484308802
title: Extended irreversible thermodynamics
tags:
    - Overview
    - Basic concepts
    - An extension of classical irreversible thermodynamics
    - Applications
categories:
    - Non-equilibrium thermodynamics
---
Extended irreversible thermodynamics is a branch of non-equilibrium thermodynamics that goes beyond the local equilibrium hypothesis of classical irreversible thermodynamics. The space of state variables is enlarged by including the fluxes of mass, momentum and energy and eventually higher order fluxes. The formalism is well-suited for describing high-frequency processes and small-length scales materials.
