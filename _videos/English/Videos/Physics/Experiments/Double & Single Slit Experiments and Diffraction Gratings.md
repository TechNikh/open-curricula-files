---
version: 1
type: video
provider: YouTube
id: U02-O5Z9E6Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ea22817d-8743-4190-b07b-aab6a0f4b77b
updated: 1486069636
title: 'Double & Single Slit Experiments and Diffraction Gratings'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/U02-O5Z9E6Y/default.jpg
    - https://i3.ytimg.com/vi/U02-O5Z9E6Y/1.jpg
    - https://i3.ytimg.com/vi/U02-O5Z9E6Y/2.jpg
    - https://i3.ytimg.com/vi/U02-O5Z9E6Y/3.jpg
---
