---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/negligence
offline_file: ""
offline_thumbnail: ""
uuid: 6cc61544-6737-40e8-ac05-62b5011ada5f
updated: 1484310170
title: negligence
categories:
    - Dictionary
---
negligence
     n 1: failure to act with the prudence that a reasonable person
          would exercise under the same circumstances [syn: {carelessness},
           {neglect}, {nonperformance}]
     2: the trait of neglecting responsibilities and lacking concern
        [syn: {neglect}, {neglectfulness}]
