---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/einstein
offline_file: ""
offline_thumbnail: ""
uuid: 28bf0872-93ad-4870-bdf0-86982037323d
updated: 1484310144
title: einstein
categories:
    - Dictionary
---
Einstein
     n 1: physicist born in Germany who formulated the special theory
          of relativity and the general theory of relativity;
          Einstein also proposed that light consists of discrete
          quantized bundles of energy (later called photons)
          (1879-1955) [syn: {Albert Einstein}]
     2: someone who has exceptional intellectual ability and
        originality; "Mozart was a child genius"; "he's smart but
        he's no Einstein" [syn: {genius}, {mastermind}, {brain}]
