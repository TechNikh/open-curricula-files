---
version: 1
type: video
provider: YouTube
id: 5HdIOGG9B3c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9ffa85ea-d323-4470-87e4-1844184d9f53
updated: 1486069651
title: Magnetic phenomena in ferromagnetic materials HD
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/5HdIOGG9B3c/default.jpg
    - https://i3.ytimg.com/vi/5HdIOGG9B3c/1.jpg
    - https://i3.ytimg.com/vi/5HdIOGG9B3c/2.jpg
    - https://i3.ytimg.com/vi/5HdIOGG9B3c/3.jpg
---
