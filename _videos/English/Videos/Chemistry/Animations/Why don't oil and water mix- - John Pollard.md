---
version: 1
type: video
provider: YouTube
id: h5yIJXdItgo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Why%20don%27t%20oil%20and%20water%20mix-%20-%20John%20Pollard.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e262d0a1-f2b7-4d02-9717-82509380902a
updated: 1486069665
title: "Why don't oil and water mix? - John Pollard"
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/h5yIJXdItgo/default.jpg
    - https://i3.ytimg.com/vi/h5yIJXdItgo/1.jpg
    - https://i3.ytimg.com/vi/h5yIJXdItgo/2.jpg
    - https://i3.ytimg.com/vi/h5yIJXdItgo/3.jpg
---
