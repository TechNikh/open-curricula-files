---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phase_space
offline_file: ""
offline_thumbnail: ""
uuid: 53dcfef1-ea16-4087-bb80-48a689b54757
updated: 1484308795
title: Phase space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Focal_stability.png
tags:
    - Introduction
    - Conjugate momenta
    - Statistical ensembles in phase space
    - Examples
    - Low dimensions
    - Chaos theory
    - Phase plot
    - Quantum mechanics
    - Thermodynamics and statistical mechanics
    - Optics
    - Phase integral
categories:
    - Hamiltonian mechanics
---
In mathematics and physics, a phase space of a dynamical system is a space in which all possible states of a system are represented, with each possible state corresponding to one unique point in the phase space. For mechanical systems, the phase space usually consists of all possible values of position and momentum variables. The concept of phase space was developed in the late 19th century by Ludwig Boltzmann, Henri Poincaré, and Willard Gibbs.[1]
