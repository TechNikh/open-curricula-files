---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transformation_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 03305b72-592f-4c96-8d25-c2d46195bed2
updated: 1484309336
title: Transformation matrix
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-2D_affine_transformation_matrix.svg.png
tags:
    - Uses
    - Finding the matrix of a transformation
    - Eigenbasis and diagonal matrix
    - Examples in 2D computer graphics
    - Stretching
    - Rotation
    - Shearing
    - Reflection
    - Orthogonal projection
    - Examples in 3D computer graphics
    - Rotation
    - Reflection
    - Composing and inverting transformations
    - Other kinds of transformations
    - Affine transformations
    - Perspective projection
categories:
    - Transformation (function)
---
In linear algebra, linear transformations can be represented by matrices. If T is a linear transformation mapping Rn to Rm and 
  
    
      
        
          
            
              x
              →
            
          
        
      
    
    {\displaystyle {\vec {x}}}
  
 is a column vector with n entries, then
