---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/joined
offline_file: ""
offline_thumbnail: ""
uuid: e086d8ff-7cdf-4989-bc17-778b9a13af44
updated: 1484310405
title: joined
categories:
    - Dictionary
---
joined
     adj 1: of or relating to two people who are married to each other
            [syn: {united}]
     2: connected by a link, as railway cars or trailer trucks [syn:
         {coupled}, {linked}]
