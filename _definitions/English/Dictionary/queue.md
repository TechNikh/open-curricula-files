---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/queue
offline_file: ""
offline_thumbnail: ""
uuid: 6d4f9ed8-3832-4553-ab3a-f9ba276aa73d
updated: 1484310486
title: queue
categories:
    - Dictionary
---
queue
     n 1: a line of people or vehicles waiting for something [syn: {waiting
          line}]
     2: (information processing) an ordered list of tasks to be
        performed or messages to be transmitted
     3: a braid of hair at the back of the head
     v : form a queue, form a line, stand in line; "Customers lined
         up in front of the store" [syn: {line up}, {queue up}]
