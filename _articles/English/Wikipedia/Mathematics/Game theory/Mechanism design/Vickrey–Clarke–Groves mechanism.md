---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Vickrey%E2%80%93Clarke%E2%80%93Groves_mechanism'
offline_file: ""
offline_thumbnail: ""
uuid: c2b38527-5573-4744-add6-c69569a5c604
updated: 1484309046
title: Vickrey–Clarke–Groves mechanism
tags:
    - Notation
    - Solution family
    - Truthfulness
    - The Clarke pivot rule
    - Weighted VCG mechanism
    - Cost minimization
    - Applications
    - Auctions
    - Public project
    - Quickest paths
    - Uniqueness
    - Computational issues
categories:
    - Mechanism design
---
In mechanism design, a Vickrey–Clarke–Groves (VCG) mechanism is a generic truthful mechanism for achieving a socially-optimal solution. It is a generalization of a Vickrey–Clarke–Groves auction. A VCG auction performs a specific task: dividing items among people. A VCG mechanism is more general: it can be used to select any outcome out of a set of possible outcomes.[1]:216–233
