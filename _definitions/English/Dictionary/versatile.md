---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/versatile
offline_file: ""
offline_thumbnail: ""
uuid: 316f0ffe-b591-4c5d-b81d-f97fe18fc272
updated: 1484310416
title: versatile
categories:
    - Dictionary
---
versatile
     adj 1: (used of persons) having many skills
     2: having great diversity or variety; "his various achievements
        are impressive"; "his vast and versatile erudition" [syn:
        {various}]
     3: changeable or inconstant; "versatile moods"
     4: competent in many areas and able to turn with ease from one
        thing to another; "a versatile writer"
     5: able to move freely in all directions; "an owl's versatile
        toe can move backward and forward"; "an insect's versatile
        antennae can move up and down or laterally"; "a versatile
        anther of a flower moves freely in the wind"
