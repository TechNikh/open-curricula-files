---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/specific
offline_file: ""
offline_thumbnail: ""
uuid: fbaebd8a-114a-4b01-b50e-0293e4fe436a
updated: 1484310357
title: specific
categories:
    - Dictionary
---
specific
     adj 1: (sometimes followed by `to') applying to or characterized by
            or distinguishing something particular or special or
            unique; "rules with specific application"; "demands
            specific to the job"; "a specific and detailed account
            of the accident" [ant: {general}, {nonspecific}]
     2: stated explicitly or in detail; "needed a specific amount"
     3: relating to or distinguishing or constituting a taxonomic
        species; "specific characters"
     4: being or affecting a disease produced by a particular
        microorganism or condition; used also of stains or dyes
        used in making microscope slides; "quinine is highly
  ...
