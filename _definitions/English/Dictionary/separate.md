---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/separate
offline_file: ""
offline_thumbnail: ""
uuid: a4383f15-9c73-41bc-880a-3f3220d7abae
updated: 1484310309
title: separate
categories:
    - Dictionary
---
separate
     adj 1: independent; not united or joint; "a problem consisting of
            two separate issues"; "they went their separate ways";
            "formed a separate church" [ant: {joint}]
     2: individual and distinct; "pegged down each separate branch
        to the earth"; "a gift for every single child" [syn: {single(a)}]
     3: standing apart; not attached to or supported by anything; "a
        freestanding bell tower"; "a house with a separate garage"
        [syn: {freestanding}]
     4: not living together as man and wife; "decided to live
        apart"; "maintaining separate households"; "they are
        separated" [syn: {apart(p)}, {separated}]
     5: ...
