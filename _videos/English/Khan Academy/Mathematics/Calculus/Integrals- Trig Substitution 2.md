---
version: 1
type: video
provider: YouTube
id: fD7MbnXbTls
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Integrals-%20Trig%20Substitution%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5470523b-6903-4009-ab72-bc7287502c6d
updated: 1486069694
title: 'Integrals- Trig Substitution 2'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/fD7MbnXbTls/default.jpg
    - https://i3.ytimg.com/vi/fD7MbnXbTls/1.jpg
    - https://i3.ytimg.com/vi/fD7MbnXbTls/2.jpg
    - https://i3.ytimg.com/vi/fD7MbnXbTls/3.jpg
---
