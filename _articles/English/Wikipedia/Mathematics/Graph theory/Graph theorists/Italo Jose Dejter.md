---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Italo_Jose_Dejter
offline_file: ""
offline_thumbnail: ""
uuid: 9b3730eb-5d49-46a8-8249-38f69c5a9fa2
updated: 1484309461
title: Italo Jose Dejter
tags:
    - Algebraic and differential topology
    - Graph theory
    - Erdős–Pósa theorem and odd cycles
    - Ljubljana graph in binary 7-cube
    - Coxeter graph and Klein graph
    - Kd-ultrahomogeneous configurations
    - Hamiltonicity in graphs
    - Perfect dominating sets
    - Efficient dominating sets
    - Quasiperfect dominating sets
    - Coding theory
    - Invariants of perfect error-correcting codes
    - Generalizing perfect Lee codes
    - Total perfect codes
    - Design theory
categories:
    - Graph theorists
---
Italo Jose Dejter (Bahia Blanca, 1939) is an Argentine-born American mathematician and a professor at the University of Puerto Rico (UPRRP)[1] since 1984. Professor Dejter has conducted research in mathematics, particularly in areas that include algebraic topology, differential topology, graph theory, coding theory and design theory. He has an Erdos number of 2[2] since 1993.[3]
