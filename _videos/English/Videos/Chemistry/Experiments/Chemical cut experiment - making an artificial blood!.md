---
version: 1
type: video
provider: YouTube
id: WLI1ooXfEVs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4e5b9afd-c971-43c8-86b2-193eff1d58d2
updated: 1486069661
title: 'Chemical cut experiment - making an artificial blood!'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/WLI1ooXfEVs/default.jpg
    - https://i3.ytimg.com/vi/WLI1ooXfEVs/1.jpg
    - https://i3.ytimg.com/vi/WLI1ooXfEVs/2.jpg
    - https://i3.ytimg.com/vi/WLI1ooXfEVs/3.jpg
---
