---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/zero
offline_file: ""
offline_thumbnail: ""
uuid: 40c69594-4d95-477c-b535-f6d0eebd8d97
updated: 1484310218
title: zero
categories:
    - Dictionary
---
zero
     adj 1: indicating the absence of any or all units under
            consideration; "a zero score" [syn: {0}]
     2: indicating an initial point or origin
     3: of or relating to the null set (a set with no members)
     4: having no measurable or otherwise determinable value; "the
        goal is zero population growth" [syn: {zero(a)}]
     n 1: a quantity of no importance; "it looked like nothing I had
          ever seen before"; "reduced to nil all the work we had
          done"; "we racked up a pathetic goose egg"; "it was all
          for naught"; "I didn't hear zilch about it" [syn: {nothing},
           {nil}, {nix}, {nada}, {null}, {aught}, {cipher}, {cypher},
       ...
