---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/summer
offline_file: ""
offline_thumbnail: ""
uuid: 3cdd4c92-adc2-42fa-ac69-f98222aa06d3
updated: 1484310224
title: summer
categories:
    - Dictionary
---
summer
     n : the warmest season of the year; in the northern hemisphere
         it extends from the summer solstice to the autumnal
         equinox; "they spent a lazy summer at the shore" [syn: {summertime}]
     v : spend the summer; "We summered in Kashmir"
