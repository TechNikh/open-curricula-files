---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pathway
offline_file: ""
offline_thumbnail: ""
uuid: 4d0e4bb5-47d2-49c9-9e4c-874224d75c33
updated: 1484310333
title: pathway
categories:
    - Dictionary
---
pathway
     n 1: a bundle of mylenated nerve fibers following a path through
          the brain [syn: {nerve pathway}, {tract}, {nerve tract}]
     2: a trodden path [syn: {footpath}]
