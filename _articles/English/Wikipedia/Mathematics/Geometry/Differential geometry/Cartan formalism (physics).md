---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cartan_formalism_(physics)
offline_file: ""
offline_thumbnail: ""
uuid: 95ebd9e9-755e-4f68-9735-e23c12e74ab3
updated: 1484309207
title: Cartan formalism (physics)
tags:
    - The basic ingredients
    - 'Example: general relativity'
    - Constructions
    - The Palatini action
    - Notes
categories:
    - Differential geometry
---
The vierbein or tetrad theory much used in theoretical physics is a special case of the application of Cartan connection in four-dimensional manifolds. It applies to metrics of any signature. (See metric tensor.) This section is an approach to tetrads, but written in general terms. In dimensions other than 4, words like triad, pentad, zweibein, fünfbein, elfbein etc. have been used. Vielbein covers all dimensions. (In German, vier stands for four and viel stands for many.)
