---
version: 1
type: video
provider: YouTube
id: nL6LMX8-bPY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/What%20causes%20precession%20and%20other%20orbital%20changes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 366b89e4-3e31-40b2-89aa-494d3943bda8
updated: 1486069686
title: What causes precession and other orbital changes
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/nL6LMX8-bPY/default.jpg
    - https://i3.ytimg.com/vi/nL6LMX8-bPY/1.jpg
    - https://i3.ytimg.com/vi/nL6LMX8-bPY/2.jpg
    - https://i3.ytimg.com/vi/nL6LMX8-bPY/3.jpg
---
