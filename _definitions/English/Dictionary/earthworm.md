---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/earthworm
offline_file: ""
offline_thumbnail: ""
uuid: 8f9be103-4fc9-46cb-adcf-726e66d5fe47
updated: 1484310162
title: earthworm
categories:
    - Dictionary
---
earthworm
     n : terrestrial worm that burrows into and helps aerate soil;
         often surfaces when the ground is cool or wet; used as
         bait by anglers [syn: {angleworm}, {fishworm}, {fishing
         worm}, {wiggler}, {nightwalker}, {nightcrawler}, {crawler},
          {dew worm}, {red worm}]
