---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/return
offline_file: ""
offline_thumbnail: ""
uuid: 2c6099c0-0106-4b87-9d13-c80739b266f2
updated: 1484310431
title: return
categories:
    - Dictionary
---
return
     n 1: document giving the tax collector information about the
          taxpayer's tax liability; "his gross income was enough
          that he had to file a tax return" [syn: {tax return}, {income
          tax return}]
     2: a coming to or returning home; "on his return from Australia
        we gave him a welcoming party" [syn: {homecoming}]
     3: the occurrence of a change in direction back in the opposite
        direction [syn: {coming back}]
     4: getting something back again; "upon the restitution of the
        book to its rightful owner the child was given a tongue
        lashing" [syn: {restitution}, {restoration}, {regaining}]
     5: the act of returning to a ...
