---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Resolution_proof_reduction_via_local_context_rewriting
offline_file: ""
offline_thumbnail: ""
uuid: af6ab482-dbac-4c03-9c03-517bd9aea4d4
updated: 1484309499
title: Resolution proof reduction via local context rewriting
categories:
    - Proof theory
---
In proof theory, an area of mathematical logic, resolution proof reduction via local context rewriting is a technique for resolution proof reduction via local context rewriting.[1] This proof compression method was presented as an algorithm named ReduceAndReconstruct, that operates as a post-processing of resolution proofs.
