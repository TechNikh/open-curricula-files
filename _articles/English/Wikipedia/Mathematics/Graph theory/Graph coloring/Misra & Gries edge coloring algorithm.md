---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Misra_%26_Gries_edge_coloring_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 970252d9-5b2b-49f9-b706-172d5af7f6db
updated: 1484309387
title: 'Misra & Gries edge coloring algorithm'
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fan%252C_Misra_and_Gries_edge_coloring_algorithm.png'
tags:
    - Fans
    - Rotating a fan
    - Inverting a path
    - Algorithm
    - Proof of correctness
    - Path inversion guarantee
    - The edge coloring is proper
    - The algorithm requires at most Δ + 1 colors
    - Complexity
categories:
    - Graph coloring
---
The Misra & Gries edge coloring algorithm is a polynomial time algorithm in graph theory that finds an edge coloring of any graph. The coloring produces uses at most 
  
    
      
        Δ
        +
        1
      
    
    {\displaystyle \Delta +1}
  
 colors, where 
  
    
      
        Δ
      
    
    {\displaystyle \Delta }
  
 is the maximum degree of the graph. This is optimal for some graphs, and by Vizing's theorem it uses at most one color more than the optimal for all others.
