---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Formyl
offline_file: ""
offline_thumbnail: ""
uuid: 328b2748-d060-43e4-8be2-74584bd56180
updated: 1484308403
title: Aldehyde
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Aldehyd_-_Aldehyde.svg.png
tags:
    - Structure and bonding
    - Nomenclature
    - IUPAC names for aldehydes
    - Etymology
    - Physical properties and characterization
    - Applications and occurrence
    - Naturally occurring aldehydes
    - Synthesis
    - Oxidative routes
    - Specialty methods
    - Common reactions
    - Reduction
    - Oxidation
    - Nucleophilic addition reactions
    - Oxygen nucleophiles
    - Nitrogen nucleophiles
    - Carbon nucleophiles
    - Bisulfite reaction
    - More complex reactions
    - Dialdehydes
    - Biochemistry
    - Examples of aldehydes
    - Examples of dialdehydes
    - Uses
categories:
    - Uncategorized
---
An aldehyde /ˈældᵻhaɪd/ or alkanal is an organic compound containing a functional group with the structure −CHO, consisting of a carbonyl center (a carbon double-bonded to oxygen) with the carbon atom also bonded to hydrogen and to an R group,[1] which is any generic alkyl or side chain. The group—without R—is the aldehyde group, also known as the formyl group. Aldehydes are common in organic chemistry. Many fragrances are aldehydes.
