---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/debt
offline_file: ""
offline_thumbnail: ""
uuid: 5ccd3cb2-234d-419b-bb07-2b0296451a3c
updated: 1484310515
title: debt
categories:
    - Dictionary
---
debt
     n 1: the state of owing something (especially money); "he is
          badly in debt"
     2: money or goods or services owed by one person to another
     3: an obligation to pay or do something
