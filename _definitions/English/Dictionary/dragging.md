---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dragging
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484650621
title: dragging
categories:
    - Dictionary
---
drag
     n 1: the phenomenon of resistance to motion through a fluid [syn:
           {retarding force}]
     2: something that slows or delays progress; "taxation is a drag
        on the economy"; "too many laws are a drag on the use of
        new land"
     3: something tedious and boring; "peeling potatoes is a drag"
     4: clothing that is conventionally worn by the opposite sex
        (especially women's clothing when worn by a man); "he went
        to the party dressed in drag"; "the waitresses looked like
        missionaries in drag"
     5: a slow inhalation (as of tobacco smoke); "he took a puff on
        his pipe"; "he took a drag on his cigarette and expelled
        the ...
