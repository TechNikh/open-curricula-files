---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tsar
offline_file: ""
offline_thumbnail: ""
uuid: 82cde64e-3a86-4982-ab85-1ca55164342c
updated: 1484308479
title: Tsar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Emperor_by_Ivan_Makarov.jpg
tags:
    - Meaning in Slavic languages
    - Bulgaria
    - "Kievan Rus'"
    - Serbia
    - Russia
    - Full style of Russian Sovereigns
    - Titles in the Russian Royal/Imperial family
    - Metaphorical uses
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
Tsar /zɑːr/ or /tsɑːr/ (Old Church Slavonic: ц︢рь [usually written thus with a title] or цар, цaрь), also spelled tzar, csar, or czar, is a title used to designate certain Slavic monarchs or supreme rulers. As a system of government in the Tsardom of Russia and the Russian Empire, it is known as Tsarist autocracy, or Tsarism. The term is derived from the Latin word Caesar, which was intended to mean "Emperor" in the European medieval sense of the term—a ruler with the same rank as a Roman emperor, holding it by the approval of another emperor or a supreme ecclesiastical official (the Pope or the Ecumenical Patriarch)—but was usually considered by western Europeans to be ...
