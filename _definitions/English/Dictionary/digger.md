---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/digger
offline_file: ""
offline_thumbnail: ""
uuid: 8cf4ba1f-f115-4532-9b8d-05490a2d7734
updated: 1484310286
title: digger
categories:
    - Dictionary
---
digger
     n 1: a laborer who digs
     2: a machine for excavating [syn: {power shovel}, {excavator},
        {shovel}]
