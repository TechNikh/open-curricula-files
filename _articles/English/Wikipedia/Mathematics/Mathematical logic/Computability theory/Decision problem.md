---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Decision_problem
offline_file: ""
offline_thumbnail: ""
uuid: e3544313-eb62-4828-81ad-8234d809ee8f
updated: 1484309485
title: Decision problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Decision_Problem.svg.png
tags:
    - Definition
    - Examples
    - Decidability
    - Complete problems
    - Equivalence with function problems
categories:
    - Computability theory
---
In computability theory and computational complexity theory, a decision problem is a question in some formal system with a yes-or-no answer, depending on the values of some input parameters. Decision problems typically appear in mathematical questions of decidability, that is, the question of the existence of an effective method to determine the existence of some object or its membership in a set; some of the most important problems in mathematics are undecidable.
