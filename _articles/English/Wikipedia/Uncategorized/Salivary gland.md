---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Salivary_glands
offline_file: ""
offline_thumbnail: ""
uuid: 1daaf932-5220-4c73-bd0a-97f5785a6d6a
updated: 1484308375
title: Salivary gland
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Blausen_0780_SalivaryGlands.png
tags:
    - Structure
    - Parotid glands
    - Submandibular glands
    - Sublingual glands
    - Minor salivary glands
    - "Von Ebner's glands"
    - Innervation
    - Histology
    - Acini
    - Ducts
    - Clinical significance
    - Other animals
categories:
    - Uncategorized
---
The salivary glands in mammals are exocrine glands, glands with ducts, that produce saliva, which is formed of several things including amylase, a digestive enzyme that breaks down starch into maltose and glucose. In humans and some other mammals the secretion is alpha-amylase also known as ptyalin.
