---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Qvist%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 54b8be66-0b4b-4c6a-9903-46c2440d640b
updated: 1484309126
title: "Qvist's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Qvist-0-en.svg.png
tags:
    - Definition of an oval
    - "Statement and proof of Qvist's theorem"
    - Definition and property of hyperovals
    - Notes
categories:
    - Theorems in geometry
---
In projective geometry Qvist's theorem, named after the Finnish mathematician Bertil Qvist, is a statement on ovals in finite projective planes. Standard examples of ovals are non-degenerate (projective) conic sections. The theorem gives an answer to the question How many tangents to an oval can pass through a point in a finite projective plane? The answer depends essentially upon the order (number of points on a line −1) of the plane.
