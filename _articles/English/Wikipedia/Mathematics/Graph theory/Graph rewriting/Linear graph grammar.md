---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_graph_grammar
offline_file: ""
offline_thumbnail: ""
uuid: 12a2af87-f15a-46d5-8e1b-c2b68d0c8844
updated: 1484309452
title: Linear graph grammar
categories:
    - Graph rewriting
---
In computer science, a linear graph grammar (also a connection graph reduction system or a port graph grammar[1]) is a class of graph grammar on which nodes have a number of ports connected together by edges and edges connect exactly two ports together. Interaction nets are a special subclass of linear graph grammars in which rewriting is confluent.
