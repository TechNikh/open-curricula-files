---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Genotypic
offline_file: ""
offline_thumbnail: ""
uuid: 4e3f51f8-e0fe-4177-9f96-e447cbdb3e9e
updated: 1484308347
title: Genotype
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Punnett_square_mendel_flowers.svg.png
tags:
    - Genotypic and genomic sequence
    - Genotype and phenotype
    - Genotype and Mendelian inheritance
    - Determining genotype
categories:
    - Uncategorized
---
The genotype is the part (DNA sequence) of the genetic makeup of a cell, and therefore of an organism or individual, which determines a specific characteristic (phenotype) of that cell/organism/individual.[1] Genotype is one of three factors that determine phenotype, the other two being inherited epigenetic factors, and non-inherited environmental factors. DNA mutations which are acquired rather than inherited, such as cancer mutations, are not part of the individual's genotype; hence, scientists and physicians sometimes talk for example about the (geno)type of a particular cancer, that is the genotype of the disease as distinct from the diseased.
