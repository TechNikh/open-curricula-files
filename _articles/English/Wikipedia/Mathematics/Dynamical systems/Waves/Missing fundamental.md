---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Missing_fundamental
offline_file: ""
offline_thumbnail: ""
uuid: d4a3b9c1-b7cb-4ec1-ab5c-66b630864ab0
updated: 1484308896
title: Missing fundamental
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Illustration_of_common_periodicity_of_full_spectrum_and_missing_fundamental_waveforms.jpg
tags:
    - Explanation
    - Examples
    - Audio processing applications
categories:
    - Waves
---
A harmonic sound is said to have a missing fundamental, suppressed fundamental, or phantom fundamental when its overtones suggest a fundamental frequency but the sound lacks a component at the fundamental frequency itself. The brain perceives the pitch of a tone not only by its fundamental frequency, but also by the periodicity implied by the relationship between the higher harmonics; we may perceive the same pitch (perhaps with a different timbre) even if the fundamental frequency is missing from a tone.
