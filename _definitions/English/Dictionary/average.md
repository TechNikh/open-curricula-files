---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/average
offline_file: ""
offline_thumbnail: ""
uuid: 6c08b343-f64e-4983-8991-4cc94e6ee8e4
updated: 1484310262
title: average
categories:
    - Dictionary
---
average
     adj 1: approximating the statistical norm or average or expected
            value; "the average income in New England is below
            that of the nation"; "of average height for his age";
            "the mean annual rainfall" [syn: {mean(a)}]
     2: lacking special distinction, rank, or status; commonly
        encountered; "average people"; "the ordinary (or common)
        man in the street" [syn: {ordinary}]
     3: of no exceptional quality or ability; "a novel of average
        merit"; "only a fair performance of the sonata"; "in fair
        health"; "the caliber of the students has gone from
        mediocre to above average"; "the performance was middling
      ...
