---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484484542
title: diary
categories:
    - Dictionary
---
diary
     n 1: a daily written record of (usually personal) experiences and
          observations [syn: {journal}]
     2: a personal journal (as a physical object)
