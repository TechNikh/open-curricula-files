---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/battery
offline_file: ""
offline_thumbnail: ""
uuid: 19a4f122-e3ec-471f-b712-9f7835dc6903
updated: 1484310206
title: battery
categories:
    - Dictionary
---
battery
     n 1: group of guns or missile launchers operated together at one
          place
     2: a device that produces electricity; may have several primary
        or secondary cells arranged in parallel or series [syn: {electric
        battery}]
     3: a collection of related things intended for use together;
        "took a battery of achievement tests"
     4: a unit composed of the pitcher and catcher
     5: a series of stamps operated in one mortar for crushing ores
        [syn: {stamp battery}]
     6: the heavy fire of artillery to saturate an area rather than
        hit a specific target; "they laid down a barrage in front
        of the advancing troops"; "the shelling ...
