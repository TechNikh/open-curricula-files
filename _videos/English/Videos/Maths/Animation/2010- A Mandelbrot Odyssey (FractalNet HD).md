---
version: 1
type: video
provider: YouTube
id: F_nfHY61T-U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5b99c1b4-1e41-41c6-90dc-2ee03c173f4b
updated: 1486069645
title: '2010- A Mandelbrot Odyssey (FractalNet HD)'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/F_nfHY61T-U/default.jpg
    - https://i3.ytimg.com/vi/F_nfHY61T-U/1.jpg
    - https://i3.ytimg.com/vi/F_nfHY61T-U/2.jpg
    - https://i3.ytimg.com/vi/F_nfHY61T-U/3.jpg
---
