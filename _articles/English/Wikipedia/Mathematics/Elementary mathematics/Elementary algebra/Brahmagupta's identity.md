---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Brahmagupta%27s_identity'
offline_file: ""
offline_thumbnail: ""
uuid: 6939b1a9-738e-4acc-a18c-c8f5580df892
updated: 1484308903
title: "Brahmagupta's identity"
tags:
    - History
    - "Application to Pell's equation"
categories:
    - Elementary algebra
---
In algebra, Brahmagupta's identity says that the product of two numbers of the form 
  
    
      
        
          a
          
            2
          
        
        +
        n
        
          b
          
            2
          
        
      
    
    {\displaystyle a^{2}+nb^{2}}
  
 is itself a number of that form. In other words, the set of such numbers is closed under multiplication. Specifically:
