---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cecil_C._Rousseau
offline_file: ""
offline_thumbnail: ""
uuid: 78a3bd65-ea9b-4c3a-849b-c2acbe9b5467
updated: 1484309473
title: Cecil C. Rousseau
categories:
    - Graph theorists
---
Cecil Clyde Rousseau (born January 13, 1938 in Philadelphia)[1] is a mathematician and author who specializes in graph theory and combinatorics. He is a professor emeritus at The University of Memphis and former chair of the USAMO.[2]
