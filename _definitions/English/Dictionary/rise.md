---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rise
offline_file: ""
offline_thumbnail: ""
uuid: 872b69ae-fb62-4662-8b27-a411829d9212
updated: 1484310333
title: rise
categories:
    - Dictionary
---
rise
     n 1: a growth in strength or number or importance [ant: {fall}]
     2: the act of changing location in an upward direction [syn: {ascent},
         {ascension}, {ascending}]
     3: an upward slope or grade (as in a road); "the car couldn't
        make it up the rise" [syn: {ascent}, {acclivity}, {raise},
         {climb}, {upgrade}] [ant: {descent}]
     4: a movement upward; "they cheered the rise of the hot-air
        balloon" [syn: {rising}, {ascent}, {ascension}] [ant: {fall}]
     5: the amount a salary is increased; "he got a 3% raise"; "he
        got a wage hike" [syn: {raise}, {wage hike}, {hike}, {wage
        increase}, {salary increase}]
     6: the property ...
