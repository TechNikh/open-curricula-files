---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wilt
offline_file: ""
offline_thumbnail: ""
uuid: 3281592f-e124-409a-ad79-4b3a541e8656
updated: 1484310462
title: wilt
categories:
    - Dictionary
---
wilt
     n 1: any plant disease characterized by drooping and shriveling;
          usually caused by parasites attacking the roots [syn: {wilt
          disease}]
     2: causing to become limp or drooping [syn: {wilting}]
     v 1: lose strength; "My opponent was wilting"
     2: become limp; "The flowers wilted" [syn: {droop}]
