---
version: 1
type: video
provider: YouTube
id: L37JKsHMQvA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f148e9d7-ccd0-42c5-bc27-4b5174616a9a
updated: 1486069651
title: Polarization HD
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/L37JKsHMQvA/default.jpg
    - https://i3.ytimg.com/vi/L37JKsHMQvA/1.jpg
    - https://i3.ytimg.com/vi/L37JKsHMQvA/2.jpg
    - https://i3.ytimg.com/vi/L37JKsHMQvA/3.jpg
---
