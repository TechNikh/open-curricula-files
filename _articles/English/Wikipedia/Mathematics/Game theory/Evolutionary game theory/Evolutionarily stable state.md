---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Evolutionarily_stable_state
offline_file: ""
offline_thumbnail: ""
uuid: 969dd859-aaac-4414-b4ee-2bf566947007
updated: 1484308524
title: Evolutionarily stable state
categories:
    - Evolutionary game theory
---
"A population is said to be in an evolutionarily stable state if its genetic composition is restored by selection after a disturbance, provided the disturbance is not too large. Such a population can be genetically monomorphic or polymorphic." --Maynard Smith (1982).
