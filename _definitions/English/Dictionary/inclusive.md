---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inclusive
offline_file: ""
offline_thumbnail: ""
uuid: 5e321a23-a66a-41d6-bb77-34a0c46433c3
updated: 1484310183
title: inclusive
categories:
    - Dictionary
---
inclusive
     adj : including much or everything; and especially including
           stated limits; "an inclusive art form"; "an inclusive
           fee"; "his concept of history is modern and inclusive";
           "from Monday to Friday inclusive" [ant: {exclusive}]
