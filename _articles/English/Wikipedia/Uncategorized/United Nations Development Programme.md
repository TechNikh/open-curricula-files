---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Undp
offline_file: ""
offline_thumbnail: ""
uuid: 98588715-b46c-47da-a6c8-20e8f0ab8e57
updated: 1484308428
title: United Nations Development Programme
tags:
    - Founding
    - Budget
    - Funding information table
    - Functions
    - Democratic governance
    - Poverty reduction
    - Crisis prevention and recovery
    - Environment and Energy
    - HIV/AIDS
    - Hub for Innovative Partnerships
    - Human Development Report
    - Evaluation
    - Global Policy Centers
    - UN co-ordination role
    - United Nations Development Group
    - Resident coordinator system
    - Controversies
    - NSA surveillance
    - Allegations of UNDP resources used by Hamas
    - Criticism
    - Disarmament and controversy
    - Administrator
    - Associate Administrator
    - Assistant Administrators
    - Goodwill Ambassadors
    - Global Ambassadors
categories:
    - Uncategorized
---
Headquartered in New York City, UNDP advocates for change and connects countries to knowledge, experience and resources to help people build a better life. It provides expert advice, training, and grants support to developing countries, with increasing emphasis on assistance to the least developed countries.
