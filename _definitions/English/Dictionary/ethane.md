---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethane
offline_file: ""
offline_thumbnail: ""
uuid: cc6f91e0-c6f3-4790-b82f-359bf9fe445d
updated: 1484310414
title: ethane
categories:
    - Dictionary
---
ethane
     n : a colorless odorless alkane gas used as fuel [syn: {C2H6}]
