---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appointed
offline_file: ""
offline_thumbnail: ""
uuid: 7f805970-d84a-4776-8f03-3f5e0d2bc982
updated: 1484310448
title: appointed
categories:
    - Dictionary
---
appointed
     adj 1: subject to appointment [syn: {appointive}] [ant: {elective}]
     2: selected for a job; "the one appointed for guard duty"
     3: fixed or established especially by order or command; "at the
        time appointed (or the appointed time") [syn: {decreed}, {ordained},
         {prescribed}]
     4: provided with furnishing and accessories (especially of a
        tasteful kind); "a house that is beautifully appointed"
