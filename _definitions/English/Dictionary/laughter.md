---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laughter
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484557621
title: laughter
categories:
    - Dictionary
---
laughter
     n 1: the sound of laughing [syn: {laugh}]
     2: the activity of laughing; the manifestation of joy or mirth
        of scorn; "he enjoyed the laughter of the crowd"
