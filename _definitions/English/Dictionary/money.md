---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/money
offline_file: ""
offline_thumbnail: ""
uuid: 892a223b-778a-408e-b89f-439a61e2b9a1
updated: 1484310262
title: money
categories:
    - Dictionary
---
money
     n 1: the most common medium of exchange; functions as legal
          tender; "we tried to collect the money he owed us"
     2: wealth reckoned in terms of money; "all his money is in real
        estate"
     3: the official currency issued by a government or national
        bank; "he changed his money into francs"
