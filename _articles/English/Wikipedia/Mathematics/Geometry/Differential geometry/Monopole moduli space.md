---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Monopole_moduli_space
offline_file: ""
offline_thumbnail: ""
uuid: bc8ad1b3-72e1-4574-a2c6-f8180c6bad63
updated: 1484309228
title: Monopole moduli space
categories:
    - Differential geometry
---
In mathematics, the monopole moduli space is a space parametrizing monopoles (solutions of the Bogomolny equations). Atiyah and Hitchin (1988) studied the moduli space for 2 monopoles in detail and used it to describe the scattering of monopoles.
