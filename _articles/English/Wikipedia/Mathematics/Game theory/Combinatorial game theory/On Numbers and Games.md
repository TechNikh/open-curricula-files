---
version: 1
type: article
id: https://en.wikipedia.org/wiki/On_Numbers_and_Games
offline_file: ""
offline_thumbnail: ""
uuid: 957e55e9-6c4c-4bdc-bbca-946504442ebe
updated: 1484308533
title: On Numbers and Games
categories:
    - Combinatorial game theory
---
On Numbers and Games is a mathematics book by John Horton Conway first published in 1976.[1] The book is written by a pre-eminent mathematician, and is directed at other mathematicians. The material is, however, developed in a playful and unpretentious manner and many chapters are accessible to non-mathematicians. Martin Gardner discussed the book at length, particularly Conway's construction of Surreal numbers, in his Mathematical Games column in Scientific American in September 1976.[2]
