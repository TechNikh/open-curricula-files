---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/taught
offline_file: ""
offline_thumbnail: ""
uuid: 7c6a9c1c-7d44-4449-b2d4-bbfd342afb39
updated: 1484310573
title: taught
categories:
    - Dictionary
---
taught
     adj : (all used chiefly with qualifiers `well' or `poorly' or
           `un-') having received specific instruction;
           "unschooled ruffians"; "well tutored applicants" [syn:
           {instructed}, {schooled}, {tutored}]
