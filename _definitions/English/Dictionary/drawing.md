---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drawing
offline_file: ""
offline_thumbnail: ""
uuid: 7c3a3076-1673-446f-a3ec-6b1fc98acc4b
updated: 1484310309
title: drawing
categories:
    - Dictionary
---
drawing
     n 1: an illustration that is drawn by hand and published in a
          book or magazine; "it is shown by the drawing in Fig. 7"
     2: a representation of forms or objects on a surface by means
        of lines; "drawings of abstract forms"; "he did
        complicated pen-and-ink drawings like medieval miniatures"
     3: the creation of artistic drawings; "he learned drawing from
        his father" [syn: {draftsmanship}, {drafting}]
     4: players buy (or are given) chances and prizes are
        distributed according to the drawing of lots [syn: {lottery}]
     5: act of getting or draining something such as electricity or
        a liquid from a source; "the drawing of ...
