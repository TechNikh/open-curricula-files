---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Six_operations
offline_file: ""
offline_thumbnail: ""
uuid: ef03d3ee-502a-4fd3-8b70-ff17704dcc12
updated: 1484309239
title: Six operations
tags:
    - The operations
    - Six operations in étale cohomology
categories:
    - Duality theories
---
In mathematics, Grothendieck's six operations, named after Alexander Grothendieck, is a formalism in homological algebra. It originally sprang from the relations in étale cohomology that arise from a morphism of schemes f : X → Y. The basic insight was that many of the elementary facts relating cohomology on X and Y were formal consequences of a small number of axioms. These axioms hold in many cases completely unrelated to the original context, and therefore the formal consequences also hold. The six operations formalism has since been shown to apply to contexts such as D-modules on algebraic varieties, sheaves on locally compact topological spaces, and motives.
