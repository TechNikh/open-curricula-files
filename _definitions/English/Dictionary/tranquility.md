---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tranquility
offline_file: ""
offline_thumbnail: ""
uuid: b15588e9-bad5-4512-abd4-70cfede0c158
updated: 1484310174
title: tranquility
categories:
    - Dictionary
---
tranquility
     n 1: a disposition free from stress or emotion [syn: {repose}, {quiet},
           {placidity}, {serenity}, {tranquillity}]
     2: a state of peace and quiet [syn: {tranquillity}, {quietness},
         {quietude}]
