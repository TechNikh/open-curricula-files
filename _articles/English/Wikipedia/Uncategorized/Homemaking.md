---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homemaker
offline_file: ""
offline_thumbnail: ""
uuid: 0178446f-b04f-4cd0-b8b9-550db48fb624
updated: 1484308444
title: Homemaking
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Good_housekeeping_1908_08_a.jpg
tags:
    - Household tools
    - Housekeeping
    - Cooking
    - Cleaning
    - Laundry
    - Maintenance
    - Home maintenance
    - Lawn maintenance
    - Management
    - House organization
    - Interior design
    - De-cluttering
    - Household purchasing
    - Servants
    - Work strategies
    - Household production
    - Wages for housework
    - History
    - References and Sources
categories:
    - Uncategorized
---
Homemaking is a mainly American term for the management of a home, otherwise known as housework, housekeeping, or household management. It is the act of overseeing the organizational, day-to-day operations of a house or estate, and the managing of other domestic concerns. A person in charge of the homemaking, who isn't employed outside the home, is in the U.S. and Canada often called a homemaker, a gender-neutral term for a housewife or a househusband. The term "homemaker", however, may also refer to a social worker who manages a household during the incapacity of the housewife or househusband.[1]
