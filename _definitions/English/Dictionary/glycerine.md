---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glycerine
offline_file: ""
offline_thumbnail: ""
uuid: 0961a3ae-dd1b-4b08-9734-3fb420fff245
updated: 1484310208
title: glycerine
categories:
    - Dictionary
---
glycerine
     n : a sweet syrupy trihydroxy alcohol obtained by saponification
         of fats and oils [syn: {glycerol}, {glycerin}]
