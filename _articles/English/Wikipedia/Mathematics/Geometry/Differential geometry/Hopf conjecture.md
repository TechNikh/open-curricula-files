---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hopf_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 4df0b747-6175-4e31-b7cc-1ee12bc93960
updated: 1484309224
title: Hopf conjecture
tags:
    - Positively curved Riemannian manifolds
    - Riemannian symmetric spaces
    - Aspherical manifolds
    - Metrics with no conjugate points
categories:
    - Differential geometry
---
In mathematics, Hopf conjecture may refer to one of several conjectural statements from differential geometry and topology attributed to either Eberhard Hopf or Heinz Hopf.
