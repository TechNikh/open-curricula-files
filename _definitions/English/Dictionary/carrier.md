---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carrier
offline_file: ""
offline_thumbnail: ""
uuid: dceb3460-b054-4b74-90f1-0a35f7980d36
updated: 1484310202
title: carrier
categories:
    - Dictionary
---
carrier
     n 1: someone whose employment involves carrying something; "the
          bonds were transmitted by carrier" [syn: {bearer}, {toter}]
     2: a self-propelled wheeled vehicle designed specifically to
        carry something; "refrigerated carriers have
        revolutionized the grocery business"
     3: a large warship that carries planes and has a long flat deck
        for take-offs and landings [syn: {aircraft carrier}, {flattop},
         {attack aircraft carrier}]
     4: an inactive substance that is a vehicle for a radioactive
        tracer of the same substance and that assists in its
        recovery after some chemical reaction
     5: a person or firm in the ...
