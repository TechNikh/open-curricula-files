---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chairperson
offline_file: ""
offline_thumbnail: ""
uuid: 8ccee315-817b-4ec7-b8fe-b1589f8edf55
updated: 1484310595
title: chairperson
categories:
    - Dictionary
---
chairperson
     n : the officer who presides at the meetings of an organization;
         "address your remarks to the chairperson" [syn: {president},
          {chairman}, {chairwoman}, {chair}]
