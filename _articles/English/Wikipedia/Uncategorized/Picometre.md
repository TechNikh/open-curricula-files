---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Picometre
offline_file: ""
offline_thumbnail: ""
uuid: 7cd506de-b163-47ac-b32b-78c248341cee
updated: 1484308386
title: Picometre
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Atom.svg.png
categories:
    - Uncategorized
---
The picometre (International spelling as used by the International Bureau of Weights and Measures; SI symbol: pm) or picometer (American spelling) is a unit of length in the metric system, equal to 6988100000000000000♠1×10−12 m, or one trillionth (1/7012100000000000000♠1000000000000) of a metre, which is the SI base unit of length.
