---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wealth
offline_file: ""
offline_thumbnail: ""
uuid: f2b74853-c801-40d9-beb6-afbe7e8ec961
updated: 1484310525
title: wealth
categories:
    - Dictionary
---
wealth
     n 1: the state of being rich and affluent; having a plentiful
          supply of material goods and money; "great wealth is not
          a sign of great intelligence" [syn: {wealthiness}] [ant:
           {poverty}]
     2: the quality of profuse abundance; "she has a wealth of
        talent"
     3: an abundance of material possessions and resources [syn: {riches}]
     4: property that has economic utility: a monetary value or an
        exchange value
