---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sequence
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484462461
title: sequence
categories:
    - Dictionary
---
sequence
     n 1: serial arrangement in which things follow in logical order
          or a recurrent pattern; "the sequence of names was
          alphabetical"; "he invented a technique to determine the
          sequence of base pairs in DNA"
     2: a following of one thing after another in time; "the doctor
        saw a sequence of patients" [syn: {chronological sequence},
         {succession}, {successiveness}, {chronological succession}]
     3: film consisting of a succession of related shots that
        develop a given subject in a movie [syn: {episode}]
     4: the action of following in order; "he played the trumps in
        sequence" [syn: {succession}]
     5: several ...
