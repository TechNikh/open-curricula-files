---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Umbral_calculus
offline_file: ""
offline_thumbnail: ""
uuid: ded1474e-a602-4c28-a9d0-d63a257692dc
updated: 1484308580
title: Umbral calculus
tags:
    - The 19th-century umbral calculus
    - Umbral Taylor series
    - Bell and Riordan
    - The modern umbral calculus
    - Notes
categories:
    - Combinatorics
---
In mathematics before the 1970s, the term umbral calculus referred to the surprising similarity between seemingly unrelated polynomial equations and certain shadowy techniques used to 'prove' them. These techniques were introduced by John Blissard (1861) and are sometimes called Blissard's symbolic method. They are often attributed to Édouard Lucas (or James Joseph Sylvester), who used the technique extensively.[1]
