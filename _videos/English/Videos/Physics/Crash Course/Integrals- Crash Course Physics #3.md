---
version: 1
type: video
provider: YouTube
id: jLJLXka2wEM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7e7a02e7-aa2e-4cd7-81eb-f98fede12843
updated: 1486069682
title: 'Integrals- Crash Course Physics #3'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/jLJLXka2wEM/default.jpg
    - https://i3.ytimg.com/vi/jLJLXka2wEM/1.jpg
    - https://i3.ytimg.com/vi/jLJLXka2wEM/2.jpg
    - https://i3.ytimg.com/vi/jLJLXka2wEM/3.jpg
---
