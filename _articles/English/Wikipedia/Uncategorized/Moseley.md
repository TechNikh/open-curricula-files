---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moseley
offline_file: ""
offline_thumbnail: ""
uuid: b4a6f4ae-2081-4ad9-a6a7-7df5b7005c0b
updated: 1484308383
title: Moseley
tags:
    - History
    - Literary inspiration
    - Sporting success
    - Present
    - Education
    - Notable residents
categories:
    - Uncategorized
---
Moseley is a suburb of south Birmingham, England, 3 miles (4.8 km) south of the city centre. The area is a popular cosmopolitan residential location and leisure destination, with a number of bars and restaurants. The area also has a number of boutiques and other independent retailers.
