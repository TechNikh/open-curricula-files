---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Triangular_network_coding
offline_file: ""
offline_thumbnail: ""
uuid: 01346140-3718-47f8-bbd1-76df296d7728
updated: 1484308565
title: Triangular network coding
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/TNC%252C_coding_4_packets_together..PNG'
categories:
    - Coding theory
---
In coding theory, triangular network coding (TNC) is a network coding based packet coding scheme introduced by Qureshi, Foh & Cai (2012).[1] Previously, packet coding for network coding was done using linear network coding (LNC). The drawback of LNC over large finite field is that it resulted in high encoding and decoding computational complexity. While linear encoding and decoding over GF(2) alleviates the concern of high computational complexity, coding over GF(2) comes at the tradeoff cost of degrading throughput performance.
