---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/read
offline_file: ""
offline_thumbnail: ""
uuid: 6889ebc3-7565-456a-80e4-0a9f33b56c78
updated: 1484310236
title: read
categories:
    - Dictionary
---
read
     n : something that is read; "the article was a very good read"
     v 1: interpret something that is written or printed; "read the
          advertisement"; "Have you read Salman Rushdie?"
     2: have or contain a certain wording or form; "The passage
        reads as follows"; "What does the law say?" [syn: {say}]
     3: look at, interpret, and say out loud something that is
        written or printed; "The King will read the proclamation
        at noon"
     4: obtain data from magnetic tapes; "This dictionary can be
        read by the computer" [syn: {scan}]
     5: interpret the significance of, as of palms, tea leaves,
        intestines, the sky, etc.; also of human ...
