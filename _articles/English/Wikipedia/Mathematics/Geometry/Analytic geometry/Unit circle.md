---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unit_circle
offline_file: ""
offline_thumbnail: ""
uuid: 9512cf6f-8872-4462-961e-c732f551321d
updated: 1484309167
title: Unit circle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_14.png
tags:
    - In the complex plane
    - Trigonometric functions on the unit circle
    - Circle group
    - Complex dynamics
categories:
    - Analytic geometry
---
In mathematics, a unit circle is a circle with a radius of one. Frequently, especially in trigonometry, the unit circle is the circle of radius one centered at the origin (0, 0) in the Cartesian coordinate system in the Euclidean plane. The unit circle is often denoted S1; the generalization to higher dimensions is the unit sphere.
