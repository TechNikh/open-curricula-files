---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mario_Pieri
offline_file: ""
offline_thumbnail: ""
uuid: e119e178-3e09-4123-96ca-591440b36f1c
updated: 1484309100
title: Mario Pieri
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/65px-Mario_emblem.svg.png
categories:
    - Geometers
---
Pieri was born in Lucca, Italy, the son of Pellegrino Pieri and Ermina Luporini. Pellegrino was a lawyer. Pieri began his higher education at University of Bologna where he drew the attention of Salvatore Pincherle. Obtaining a scholarship, Pieri transferred to Scuola Normale Superiore in Pisa. There he took his degree in 1884 and worked first at a technical secondary school in Pisa.
