---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/propose
offline_file: ""
offline_thumbnail: ""
uuid: f555b500-554c-46e1-9295-0de61f652fee
updated: 1484310290
title: propose
categories:
    - Dictionary
---
propose
     v 1: make a proposal, declare a plan for something [syn: {suggest},
           {advise}]
     2: present for consideration [syn: {project}]
     3: propose or intend; "I aim to arrive at noon" [syn: {aim}, {purpose},
         {purport}]
     4: put forward; nominate for appointment to an office; "The
        President nominated her as head of the Civil Rights
        Commission" [syn: {nominate}]
     5: ask (someone) to marry you; "he popped the question on
        Sunday night"; "she proposed marriage to the man she had
        known for only two months"; "The old bachelor finally
        declared himself to the young woman" [syn: {declare
        oneself}, {offer}, {pop the ...
