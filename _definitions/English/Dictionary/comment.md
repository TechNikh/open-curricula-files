---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comment
offline_file: ""
offline_thumbnail: ""
uuid: aed412eb-ab3d-45ca-9572-ab309f01832b
updated: 1484310277
title: comment
categories:
    - Dictionary
---
comment
     n 1: a statement that expresses a personal opinion or belief;
          "from time to time she contributed a personal comment on
          his account" [syn: {remark}]
     2: a written explanation or criticism or illustration that is
        added to a book or other textual material; "he wrote an
        extended comment on the proposal" [syn: {commentary}]
     3: a report (often malicious) about the behavior of other
        people; "the divorce caused much gossip" [syn: {gossip}, {scuttlebutt}]
     v 1: make or write a comment on; "he commented the paper of his
          colleague" [syn: {notice}, {remark}, {point out}]
     2: explain or interpret something
     3: ...
