---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stable_roommates_problem
offline_file: ""
offline_thumbnail: ""
uuid: 561e3f16-fd05-4c15-b33b-aa5a6da3dc38
updated: 1484309511
title: Stable roommates problem
tags:
    - Solution
    - Algorithm
    - Example
    - Implementation in software package
categories:
    - Combinatorics
---
In mathematics, economics and computer science, particularly in the fields of combinatorics, game theory and algorithms, the stable-roommate problem (SRP) is the problem of finding a stable matching for an even-sized set. A matching is a separation of the set into disjoint pairs ("roommates"). The matching is stable if there are no two elements which are not roommates and which both prefer each other to their roommate under the matching. This is distinct from the stable-marriage problem in that the stable-roommates problem allows matches between any two elements, not just between classes of “men” and “women”.
