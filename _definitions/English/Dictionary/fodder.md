---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fodder
offline_file: ""
offline_thumbnail: ""
uuid: 54ef7724-2a7d-4204-9797-62dba2724aa8
updated: 1484310462
title: fodder
categories:
    - Dictionary
---
fodder
     n : coarse food (especially for cattle and horses) composed of
         entire plants or the leaves and stalks of a cereal crop
     v : give fodder (to domesticated animals)
