---
version: 1
type: video
provider: YouTube
id: BJ5ZAcOtisA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3f410385-22f2-48b1-8867-e1966717b67c
updated: 1486069609
title: Completing the square for vertex form
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/BJ5ZAcOtisA/default.jpg
    - https://i3.ytimg.com/vi/BJ5ZAcOtisA/1.jpg
    - https://i3.ytimg.com/vi/BJ5ZAcOtisA/2.jpg
    - https://i3.ytimg.com/vi/BJ5ZAcOtisA/3.jpg
---
