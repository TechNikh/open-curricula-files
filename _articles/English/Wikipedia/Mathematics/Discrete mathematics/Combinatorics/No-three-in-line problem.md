---
version: 1
type: article
id: https://en.wikipedia.org/wiki/No-three-in-line_problem
offline_file: ""
offline_thumbnail: ""
uuid: 9ee3c100-2335-43fc-96fb-f5206c8de659
updated: 1484308598
title: No-three-in-line problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-No-three-in-line.svg.png
tags:
    - Lower bounds
    - Conjectured upper bounds
    - Applications
    - Generalizations
    - Higher dimensions
    - Graph generalizations
    - Small values of n
    - Notes
categories:
    - Combinatorics
---
In mathematics, in the area of discrete geometry, the no-three-in-line problem asks for the maximum number of points that can be placed in the n × n grid so that no three points are collinear. This number is at most 2n, since if 2n + 1 points are placed in the grid, then by the pigeonhole principle some row and some column will contain three points. The problem was introduced by Henry Dudeney in 1917.
