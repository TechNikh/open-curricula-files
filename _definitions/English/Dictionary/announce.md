---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/announce
offline_file: ""
offline_thumbnail: ""
uuid: 27b1b8c4-1761-42e1-87b7-83071fa8d3a8
updated: 1484310477
title: announce
categories:
    - Dictionary
---
announce
     v 1: make known; make an announcement; "She denoted her feelings
          clearly" [syn: {denote}]
     2: announce publicly or officially; "The President declared
        war" [syn: {declare}]
     3: give the names of; "He announced the winners of the spelling
        bee"
     4: foreshadow or presage [syn: {annunciate}, {harbinger}, {foretell},
         {herald}]
