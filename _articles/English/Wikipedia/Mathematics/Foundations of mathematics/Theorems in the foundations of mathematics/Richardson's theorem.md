---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Richardson%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 31376d18-cae9-4e65-b4b2-e2a6d51053b0
updated: 1484309001
title: "Richardson's theorem"
tags:
    - Statement of the theorem
    - Extensions
categories:
    - Theorems in the foundations of mathematics
---
In mathematics, Richardson's theorem establishes a limit on the extent to which an algorithm can decide whether certain mathematical expressions are equal. It states that for a certain fairly natural class of expressions, it is undecidable whether a particular expression E satisfies the equation E = 0, and similarly undecidable whether the functions defined by expressions E and F are everywhere equal (in fact E = F if and only if E - F = 0). It was proved in 1968 by computer scientist Daniel Richardson of the University of Bath.
