---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/converted
offline_file: ""
offline_thumbnail: ""
uuid: 80e0352a-9a2f-4c55-a078-3f5e31e70f93
updated: 1484310268
title: converted
categories:
    - Dictionary
---
converted
     adj : spiritually reborn or converted; "a born-again Christian";
           "a converted sinner" [syn: {born-again}, {reborn}]
