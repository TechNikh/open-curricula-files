---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heronian_triangle
offline_file: ""
offline_thumbnail: ""
uuid: 2de8a73e-bac3-4b6d-894a-d1500132c1b0
updated: 1484308605
title: Heronian triangle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Triangle-heronian.svg_0.png
tags:
    - Properties
    - Exact formula for Heronian triangles
    - Examples
    - Equable triangles
    - Almost-equilateral Heronian triangles
categories:
    - Discrete geometry
---
In geometry, a Heronian triangle is a triangle that has side lengths and area that are all integers.[1][2] Heronian triangles are named after Hero of Alexandria. The term is sometimes applied more widely to triangles whose sides and area are all rational numbers.[3]
