---
version: 1
type: video
provider: YouTube
id: S530Vwa33G0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 21a4bb52-849c-4302-88a5-7f839b189556
updated: 1486069641
title: 'Like in a dream - 3D fractal trip'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/S530Vwa33G0/default.jpg
    - https://i3.ytimg.com/vi/S530Vwa33G0/1.jpg
    - https://i3.ytimg.com/vi/S530Vwa33G0/2.jpg
    - https://i3.ytimg.com/vi/S530Vwa33G0/3.jpg
---
