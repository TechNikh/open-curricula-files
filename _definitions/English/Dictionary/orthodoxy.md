---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/orthodoxy
offline_file: ""
offline_thumbnail: ""
uuid: f3cb8d51-aedb-4008-a471-46a9a2ebb05d
updated: 1484310186
title: orthodoxy
categories:
    - Dictionary
---
orthodoxy
     n 1: the quality of being orthodox (especially in religion) [ant:
           {unorthodoxy}]
     2: a belief or orientation agreeing with conventional standards
        [ant: {unorthodoxy}]
