---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fermat_number
offline_file: ""
offline_thumbnail: ""
uuid: 2b1d01e0-afad-4b4d-95ca-7dceea41b75f
updated: 1484308639
title: Fermat number
tags:
    - Basic properties
    - Primality of Fermat numbers
    - Heuristic arguments for density
    - Equivalent conditions of primality
    - Factorization of Fermat numbers
    - Pseudoprimes and Fermat numbers
    - "Selfridge's Conjecture"
    - Other theorems about Fermat numbers
    - Relationship to constructible polygons
    - Applications of Fermat numbers
    - Pseudorandom Number Generation
    - Other interesting facts
    - Generalized Fermat numbers
    - Generalized Fermat primes
    - Largest known generalized Fermat primes
    - Notes
categories:
    - Number theory
---
where n is a nonnegative integer. The first few Fermat numbers are:
