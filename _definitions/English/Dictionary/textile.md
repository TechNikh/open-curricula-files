---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/textile
offline_file: ""
offline_thumbnail: ""
uuid: e60ef912-714e-4193-8933-2a0cbe9872d2
updated: 1484310384
title: textile
categories:
    - Dictionary
---
textile
     adj : of or relating to fabrics or fabric making; "textile
           research"
     n : artifact made by weaving or felting or knitting or
         crocheting natural or synthetic fibers; "the fabric in
         the curtains was light and semitraqnsparent"; "woven
         cloth originated in Mesopotamia around 5000 BC"; "she
         measured off enough material for a dress" [syn: {fabric},
          {cloth}, {material}]
