---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/object
offline_file: ""
offline_thumbnail: ""
uuid: fda2b68e-1f27-40b0-9c20-4717eb7649e7
updated: 1484310234
title: object
categories:
    - Dictionary
---
object
     n 1: a tangible and visible entity; an entity that can cast a
          shadow; "it was full of rackets, balls and other
          objects" [syn: {physical object}]
     2: the goal intended to be attained (and which is believed to
        be attainable); "the sole object of her trip was to see
        her children" [syn: {aim}, {objective}, {target}]
     3: (grammar) a constituent that is acted upon; "the object of
        the verb"
     4: the focus of cognitions or feelings; "objects of thought";
        "the object of my affection"
     v 1: express or raise an objection or protest or criticism or
          express dissent; "She never objected to the amount of
          ...
