---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lone
offline_file: ""
offline_thumbnail: ""
uuid: e3d8fc2a-31e5-40e7-836f-7ac3092d7be4
updated: 1484310403
title: lone
categories:
    - Dictionary
---
lone
     adj 1: lacking companions or companionship; "he was alone when we
            met him"; "she is alone much of the time"; "the lone
            skier on the mountain"; "a lonely fisherman stood on a
            tuft of gravel"; "a lonely soul"; "a solitary
            traveler" [syn: {alone(p)}, {lone(a)}, {lonely(a)}, {solitary}]
     2: characterized by or preferring solitude in mode of life;
        "the eremitic element in the life of a religious colony";
        "a lone wolf"; "a man of a solitary disposition" [syn: {eremitic},
         {eremitical}, {lone(a)}, {solitary}]
     3: being the only one; single and isolated from others; "the
        lone doctor in the entire ...
