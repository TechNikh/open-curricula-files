---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secure
offline_file: ""
offline_thumbnail: ""
uuid: 916d40a2-cb5f-4c20-aa88-85dcd95c560a
updated: 1484310443
title: secure
categories:
    - Dictionary
---
secure
     adj 1: free from fear or doubt; easy in mind; "secure himself, he
            went out of his way to help others" [syn: {unafraid},
            {untroubled}] [ant: {insecure}]
     2: free from danger or risk; "secure from harm"; "his fortune
        was secure"; "made a secure place for himself in his
        field" [ant: {insecure}]
     3: kept safe or defended from danger or injury or loss; "the
        most protected spot I could find" [syn: {protected}] [ant:
         {unprotected}]
     4: not likely to fail or give way; "the lock was secure"; "a
        secure foundation"; "a secure hold on her wrist" [ant: {insecure}]
     5: able to withstand attack; "an impregnable ...
