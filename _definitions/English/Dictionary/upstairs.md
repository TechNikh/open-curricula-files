---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upstairs
offline_file: ""
offline_thumbnail: ""
uuid: 1b79be1f-834e-4e90-b33f-f999487ae6eb
updated: 1484310150
title: upstairs
categories:
    - Dictionary
---
upstairs
     adj : on or of upper floors of a building; "the upstairs maid";
           "an upstairs room" [syn: {upstair}] [ant: {downstairs}]
     adv 1: on a floor above; "they lived upstairs" [syn: {up the stairs},
             {on a higher floor}] [ant: {downstairs}]
     2: with respect to the mind; "she's a bit weak upstairs" [syn:
        {in the head}, {in the mind}]
