---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emergent
offline_file: ""
offline_thumbnail: ""
uuid: c13d98af-ce66-43d5-a1a5-8c9a9f4039ef
updated: 1484310208
title: emergent
categories:
    - Dictionary
---
emergent
     adj : coming into existence; "a nascent republic" [syn: {emerging},
            {nascent}]
