---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Uses_of_trigonometry
offline_file: ""
offline_thumbnail: ""
uuid: b6b4d74e-a405-4023-a0eb-35c8f2423b00
updated: 1484309350
title: Uses of trigonometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/236px-STS-114_Steve_Robinson_on_Canadarm2.jpg
tags:
    - "Thomas Paine's statement of the uses of trigonometry"
    - Historical use for multiplication
    - Some modern uses
    - Fourier series
    - Fourier transforms
    - Statistics, including mathematical psychology
    - Number theory
    - Solving non-trigonometric equations
categories:
    - Trigonometry
---
Amongst the lay public of non-mathematicians and non-scientists, trigonometry is known chiefly for its application to measurement problems, yet is also often used in ways that are far more subtle, such as its place in the theory of music; still other uses are more technical, such as in number theory. The mathematical topics of Fourier series and Fourier transforms rely heavily on knowledge of trigonometric functions and find application in a number of areas, including statistics.
