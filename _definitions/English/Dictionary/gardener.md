---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gardener
offline_file: ""
offline_thumbnail: ""
uuid: 47c8886f-ad42-4556-9853-fb523736d32f
updated: 1484310244
title: gardener
categories:
    - Dictionary
---
gardener
     n 1: someone who takes care of a garden [syn: {nurseryman}]
     2: someone employed to work in a garden
