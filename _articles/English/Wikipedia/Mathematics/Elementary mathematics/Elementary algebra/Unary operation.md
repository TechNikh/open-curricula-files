---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unary_operation
offline_file: ""
offline_thumbnail: ""
uuid: a45c5e64-fd6f-4c70-b10a-6c81ab5d5ba4
updated: 1484308913
title: Unary operation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_10.png
tags:
    - Unary negative and positive
    - Examples from programming languages
    - C family of languages
    - Unix Shell (Bash)
    - Other languages
    - Windows PowerShell
categories:
    - Elementary algebra
---
In mathematics, a unary operation is an operation with only one operand, i.e. a single input. An example is the function f : A → A, where A is a set. The function f is a unary operation on A.
