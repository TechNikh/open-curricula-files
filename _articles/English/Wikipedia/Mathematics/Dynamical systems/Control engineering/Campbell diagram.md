---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Campbell_diagram
offline_file: ""
offline_thumbnail: ""
uuid: a8f1b490-39a7-4d3c-8deb-df6562e06261
updated: 1484308722
title: Campbell diagram
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Campbelldiagram.png
categories:
    - Control engineering
---
A Campbell diagram plot represents a system's response spectrum as a function of its oscillation regime. It is named for Wilfred Campbell, who introduced the concept.,[1][2] also called interference diagram.[3]
