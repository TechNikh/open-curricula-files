---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/occupation
offline_file: ""
offline_thumbnail: ""
uuid: eb806e57-6771-426b-964d-d4d3ffe10031
updated: 1484310268
title: occupation
categories:
    - Dictionary
---
occupation
     n 1: the principal activity in your life that you do to earn
          money; "he's not in my line of business" [syn: {business},
           {job}, {line of work}, {line}]
     2: the control of a country by military forces of a foreign
        power [syn: {military control}]
     3: any activity that occupies a person's attention; "he missed
        the bell in his occupation with the computer game"
     4: the act of occupying or taking possession of a building;
        "occupation of a building without a certificate of
        occupancy is illegal" [syn: {occupancy}, {taking
        possession}, {moving in}]
     5: the period of time during which a place or position or
  ...
