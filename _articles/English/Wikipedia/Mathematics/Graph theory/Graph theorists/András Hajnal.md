---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Andr%C3%A1s_Hajnal'
offline_file: ""
offline_thumbnail: ""
uuid: 0209487a-4f77-4394-8d22-99714fe01b39
updated: 1484309466
title: András Hajnal
tags:
    - Biography
    - Research and publications
    - Awards and honors
categories:
    - Graph theorists
---
András Hajnal (May 13, 1931 – July 30, 2016[1][2]) was a professor of mathematics at Rutgers University[3] and a member of the Hungarian Academy of Sciences[4] known for his work in set theory and combinatorics.
