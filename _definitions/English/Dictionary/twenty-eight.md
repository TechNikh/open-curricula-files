---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/twenty-eight
offline_file: ""
offline_thumbnail: ""
uuid: dce9cb01-55ab-4c27-afbf-24a4cf3fb335
updated: 1484310256
title: twenty-eight
categories:
    - Dictionary
---
twenty-eight
     adj : being eight more than twenty [syn: {28}, {xxviii}]
     n : the cardinal number that is the sum of twenty-seven and one
         [syn: {28}, {XXVIII}]
