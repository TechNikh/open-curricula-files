---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geodesic
offline_file: ""
offline_thumbnail: ""
uuid: 66890714-dca7-4de3-b191-451eb1a15270
updated: 1484310414
title: geodesic
categories:
    - Dictionary
---
geodesic
     adj 1: of or resembling a geodesic dome
     2: of or relating to or determined by geodesy [syn: {geodetic},
         {geodesical}]
     n : (mathematics) the shortest line between two points on a
         mathematically defined surface (as a straight line on a
         plane or an arc of a great circle on a sphere) [syn: {geodesic
         line}]
