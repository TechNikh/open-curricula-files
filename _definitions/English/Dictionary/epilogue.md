---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/epilogue
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441701
title: epilogue
categories:
    - Dictionary
---
epilogue
     n 1: a short speech (often in verse) addressed directly to the
          audience by an actor at the end of a play [syn: {epilog}]
     2: a short passage added at the end of a literary work; "the
        epilogue told what eventually happened to the main
        characters" [syn: {epilog}]
