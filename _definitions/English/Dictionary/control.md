---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/control
offline_file: ""
offline_thumbnail: ""
uuid: c5da0704-bca0-426c-a0e6-37050d689bde
updated: 1484310359
title: control
categories:
    - Dictionary
---
control
     n 1: power to direct or determine; "under control"
     2: a relation of constraint of one entity (thing or person or
        group) by another; "measures for the control of disease";
        "they instituted controls over drinking on campus"
     3: (physiology) regulation or maintenance of a function or
        action or reflex etc; "the timing and control of his
        movements were unimpaired"; "he had lost control of his
        sphincters"
     4: a standard against which other conditions can be compared in
        a scientific experiment; "the control condition was
        inappropriate for the conclusions he wished to draw" [syn:
         {control condition}]
     5: ...
