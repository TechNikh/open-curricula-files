---
version: 1
type: video
provider: YouTube
id: huZBkYgGrKo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Applying%20Quadratic%20Functions%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9b235910-7e43-4d81-af75-36869a494ded
updated: 1486069557
title: Applying Quadratic Functions 3
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/huZBkYgGrKo/default.jpg
    - https://i3.ytimg.com/vi/huZBkYgGrKo/1.jpg
    - https://i3.ytimg.com/vi/huZBkYgGrKo/2.jpg
    - https://i3.ytimg.com/vi/huZBkYgGrKo/3.jpg
---
