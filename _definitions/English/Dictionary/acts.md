---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acts
offline_file: ""
offline_thumbnail: ""
uuid: 161b8867-173b-4f00-b9ec-8148197ff453
updated: 1484310316
title: acts
categories:
    - Dictionary
---
Acts
     n : a New Testament book describing the development of the early
         Church from Christ's ascension to Paul's sojourn at Rome
         [syn: {Acts of the Apostles}]
