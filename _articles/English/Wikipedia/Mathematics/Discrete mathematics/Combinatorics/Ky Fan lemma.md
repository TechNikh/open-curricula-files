---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ky_Fan_lemma
offline_file: ""
offline_thumbnail: ""
uuid: b228818b-2e7c-4429-99be-347fed33695e
updated: 1484308572
title: Ky Fan lemma
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-TuckerLemExample.png
tags:
    - Definitions
    - Statement
    - "Corollary: Tucker's lemma"
    - Proof
categories:
    - Combinatorics
---
In mathematics, Ky Fan's lemma (KFL) is a combinatorial lemma about labellings of triangulations. It is a generalization of Tucker's lemma. It was proved by Ky Fan in 1952.[1]
