---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beneficial
offline_file: ""
offline_thumbnail: ""
uuid: 536dbc8a-b072-4481-981e-fae5362e819a
updated: 1484310531
title: beneficial
categories:
    - Dictionary
---
beneficial
     adj 1: promoting or enhancing well-being; "an arms limitation
            agreement beneficial to all countries"; "the
            beneficial effects of a temperate climate"; "the
            experience was good for her" [syn: {good}]
     2: tending to promote physical well-being; beneficial to
        health; "beneficial effects of a balanced diet"; "a good
        night's sleep"; "the salutary influence of pure air" [syn:
         {good}, {salutary}]
