---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/american
offline_file: ""
offline_thumbnail: ""
uuid: 2c16636a-6352-4738-a4c8-94550151408a
updated: 1484310525
title: american
categories:
    - Dictionary
---
American
     adj 1: of or relating to the United States of America or its people
            or language or culture; "American citizens"; "American
            English"; "the American dream"
     2: of or relating to or characteristic of the continents and
        islands of the Americas; "the American hemisphere";
        "American flora and fauna"
     n 1: a native or inhabitant of the United States
     2: the English language as used in the United States [syn: {American
        English}, {American language}]
     3: a native or inhabitant of a North American or Central
        American or South American country
