---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homography_(computer_vision)
offline_file: ""
offline_thumbnail: ""
uuid: a86cc2d0-23a2-43cb-a191-3464854bda35
updated: 1484309184
title: Homography (computer vision)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Hauck_Neue_Constructionen_der_Perspective_fig1a.png
tags:
    - 3D plane to plane equation
    - Affine homography
categories:
    - Geometry in computer vision
---
In the field of computer vision, any two images of the same planar surface in space are related by a homography (assuming a pinhole camera model). This has many practical applications, such as image rectification, image registration, or computation of camera motion—rotation and translation—between two images. Once camera rotation and translation have been extracted from an estimated homography matrix, this information may be used for navigation, or to insert models of 3D objects into an image or video, so that they are rendered with the correct perspective and appear to have been part of the original scene (see Augmented reality).
