---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nothing
offline_file: ""
offline_thumbnail: ""
uuid: a4f33662-956d-4547-8c01-468551617b52
updated: 1484310311
title: nothing
categories:
    - Dictionary
---
nothing
     n 1: a quantity of no importance; "it looked like nothing I had
          ever seen before"; "reduced to nil all the work we had
          done"; "we racked up a pathetic goose egg"; "it was all
          for naught"; "I didn't hear zilch about it" [syn: {nil},
           {nix}, {nada}, {null}, {aught}, {cipher}, {cypher}, {goose
          egg}, {naught}, {zero}, {zilch}, {zip}]
     2: a nonexistent thing [syn: {nonentity}]
     adv : in no way; to no degree; "he looks nothing like his father"
