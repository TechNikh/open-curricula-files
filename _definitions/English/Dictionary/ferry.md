---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ferry
offline_file: ""
offline_thumbnail: ""
uuid: e5c7eb43-834a-47fb-bd4f-f37b0c377674
updated: 1484310522
title: ferry
categories:
    - Dictionary
---
ferry
     n 1: a boat that transports people or vehicles across a body of
          water and operates on a regular schedule [syn: {ferryboat}]
     2: transport by boat or aircraft [syn: {ferrying}]
     v 1: transport from one place to another
     2: transport by ferry
     3: travel by ferry
     [also: {ferried}]
