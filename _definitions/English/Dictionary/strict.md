---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strict
offline_file: ""
offline_thumbnail: ""
uuid: e32677a5-2b48-495c-a008-ef971f01f395
updated: 1484310192
title: strict
categories:
    - Dictionary
---
strict
     adj 1: (of rules) stringently enforced; "hard-and-fast rules" [syn:
             {hard-and-fast}]
     2: rigidly accurate; allowing no deviation from a standard;
        "rigorous application of the law"; "a strict vegetarian"
        [syn: {rigorous}]
     3: incapable of compromise or flexibility [syn: {rigid}]
     4: not indulgent; "strict parents" [syn: {nonindulgent}]
     5: unsparing and uncompromising in discipline or judgment; "a
        parent severe to the pitch of hostility"- H.G.Wells; "a
        hefty six-footer with a rather severe mien"; "a strict
        disciplinarian"; "a Spartan upbringing" [syn: {severe}, {spartan}]
     6: severe and unremitting in making ...
