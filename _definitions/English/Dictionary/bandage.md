---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bandage
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484608441
title: bandage
categories:
    - Dictionary
---
bandage
     n : a piece of soft material that covers and protects an injured
         part of the body [syn: {patch}]
     v 1: wrap around with something so as to cover or enclose [syn: {bind}]
     2: dress by covering or binding; "The nurse bandaged a sprained
        ankle"; "bandage an incision"
