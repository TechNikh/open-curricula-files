---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Baker%E2%80%93Campbell%E2%80%93Hausdorff_formula'
offline_file: ""
offline_thumbnail: ""
uuid: 1704615d-1a0d-4af2-9d7b-3f105672174e
updated: 1484308567
title: Baker–Campbell–Hausdorff formula
tags:
    - 'The Campbell–Baker–Hausdorff formula: existence'
    - An explicit Baker–Campbell–Hausdorff formula
    - Selected tractable cases
    - Matrix Lie group illustration
    - The Zassenhaus formula
    - An important lemma
    - Application in quantum mechanics
    - Notes
    - Bibliography
categories:
    - Combinatorics
---
for possibly noncommutative X and Y in the Lie algebra of a Lie group. This formula tightly links Lie groups to Lie algebras by expressing the logarithm of the product of two Lie group elements as a Lie algebra element using only Lie algebraic operations. The solution on this form, whenever defined, means that multiplication in the group can be expressed entirely in Lie algebraic terms. The solution on another form[clarification needed] is straightforward to obtain; one just substitutes the power series for exp and log in the equation and rearranges.[1] The point is to express the solution in Lie algebraic terms. This occupied the time of several prominent mathematicians.
