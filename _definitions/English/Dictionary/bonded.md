---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bonded
offline_file: ""
offline_thumbnail: ""
uuid: 7ba0c325-0cf5-4ad6-b299-90e1b004bb97
updated: 1484310403
title: bonded
categories:
    - Dictionary
---
bonded
     adj : secured by written agreement [syn: {guaranteed}, {secured},
           {warranted}]
