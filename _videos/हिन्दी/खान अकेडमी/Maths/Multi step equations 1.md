---
version: 1
type: video
provider: YouTube
id: hj25gPVO4Xc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6cbd92bf-24f5-4944-97a3-79a57023443b
updated: 1486069607
title: Multi step equations 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/hj25gPVO4Xc/default.jpg
    - https://i3.ytimg.com/vi/hj25gPVO4Xc/1.jpg
    - https://i3.ytimg.com/vi/hj25gPVO4Xc/2.jpg
    - https://i3.ytimg.com/vi/hj25gPVO4Xc/3.jpg
---
