---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Uniform_convergence_(combinatorics)
offline_file: ""
offline_thumbnail: ""
uuid: df463737-53ed-46c5-a03b-b290a26ec945
updated: 1484308605
title: Uniform convergence (combinatorics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_0.png
tags:
    - 'Uniform convergence theorem statement[1]'
    - Sauer–Shelah lemma
    - 'Proof of uniform convergence theorem [1]'
    - Symmetrization
    - Permutations
    - Reduction to a finite class
categories:
    - Combinatorics
---
For a class of predicates 
  
    
      
        H
        
        
      
    
    {\displaystyle H\,\!}
  
 defined on a set 
  
    
      
        X
        
        
      
    
    {\displaystyle X\,\!}
  
 and a set of samples 
  
    
      
        x
        =
        (
        
          x
          
            1
          
        
        ,
        
          x
          
            2
          
        
        ,
        …
        ,
        
          x
          
            m
          
        
        )
        
        
      
    
    {\displaystyle x=(x_{1},x_{2},\dots ,x_{m})\,\!}
  
, where 
  
    
      
        
          x
          
            i
          
 ...
