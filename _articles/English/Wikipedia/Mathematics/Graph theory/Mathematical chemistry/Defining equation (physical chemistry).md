---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Defining_equation_(physical_chemistry)
offline_file: ""
offline_thumbnail: ""
uuid: 1068cd80-dad4-4794-a95d-fdb5fd7357ac
updated: 1484309429
title: Defining equation (physical chemistry)
tags:
    - Introduction
    - Notes on nomenclature
    - Quantification
    - General basic quantities
    - General derived quantities
    - Kinetics and equilibria
    - Electrochemistry
    - Quantum chemistry
    - Sources
categories:
    - Mathematical chemistry
---
In physical chemistry, there are numerous quantities associated with chemical compounds and reactions; notably in terms of amounts of substance, activity or concentration of a substance, and the rate of reaction. This article uses SI units.
