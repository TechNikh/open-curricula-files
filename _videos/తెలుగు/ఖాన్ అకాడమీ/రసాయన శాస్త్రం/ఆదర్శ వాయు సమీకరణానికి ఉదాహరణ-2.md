---
version: 1
type: video
provider: YouTube
id: H_T1cUdNJB0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2f7ca73f-98eb-4969-af42-abd803855f04
updated: 1486069603
title: >
    ఆదర్శ వాయు
    సమీకరణానికి ఉదాహరణ-2
tags:
    - chemistry
    - telugu
    - Ideal gas
    - Equation
categories:
    - రసాయన శాస్త్రం
thumbnail_urls:
    - https://i3.ytimg.com/vi/H_T1cUdNJB0/default.jpg
    - https://i3.ytimg.com/vi/H_T1cUdNJB0/1.jpg
    - https://i3.ytimg.com/vi/H_T1cUdNJB0/2.jpg
    - https://i3.ytimg.com/vi/H_T1cUdNJB0/3.jpg
---
Translated from original video
Title - Ideal Gas Equation Example2
URL -https://www.khanacademy.org/science/chemistry/ideal-gas-laws/v/ideal-gas-equation-example-2
