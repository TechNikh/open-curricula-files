---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wrong_rook_pawn
offline_file: ""
offline_thumbnail: ""
uuid: 33250f37-259b-48fb-b1b2-2d58386d8447
updated: 1484309022
title: Wrong rook pawn
tags:
    - Bishop and pawn
    - Defending king in front of pawn
    - Examples from games
    - Goglidze versus Kasparian
    - Fischer versus Taimanov
    - Ķeņģis vs. Kasparov
    - Karpov versus Kasparov
    - Korchnoi versus Karpov
    - An exception
    - Opposite-colored bishops
    - Example from game
    - Rook and rook pawn versus bishop
    - Examples from games
    - In studies
    - Rauzer
    - Greco
    - Notes
categories:
    - Chess theory
---
In chess endgames with a bishop, a pawn that is a rook pawn may be the wrong rook pawn. With a single bishop, the result of a position may depend on whether or not the bishop controls the square on the chessboard on which the pawn would promote. Since a side's rook pawns promote on opposite-colored squares, one of them may be the "wrong rook pawn" (Burgess 2000:494). This situation is also known as having the wrong-colored bishop or wrong bishop, i.e. the bishop is on the wrong colored squares in relation to the rook pawn (Rosen 2003:61). In many cases, the wrong rook pawn will only draw, when any other pawn would win. A fairly common defensive tactic is to get into one of these drawn ...
