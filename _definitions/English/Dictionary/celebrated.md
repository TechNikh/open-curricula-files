---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/celebrated
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484430301
title: celebrated
categories:
    - Dictionary
---
celebrated
     adj 1: widely known and esteemed; "a famous actor"; "a celebrated
            musician"; "a famed scientist"; "an illustrious
            judge"; "a notable historian"; "a renowned painter"
            [syn: {famed}, {far-famed}, {famous}, {illustrious}, {notable},
             {noted}, {renowned}]
     2: having an illustrious past [syn: {historied}, {storied}]
