---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/annoyed
offline_file: ""
offline_thumbnail: ""
uuid: f8464a37-d0fb-4443-9410-3243b37248a3
updated: 1484310587
title: annoyed
categories:
    - Dictionary
---
annoyed
     adj 1: aroused to impatience or anger; "made an irritated gesture";
            "feeling nettled from the constant teasing"; "peeved
            about being left out"; "felt really pissed at her
            snootiness"; "riled no end by his lies"; "roiled by
            the delay" [syn: {irritated}, {miffed}, {nettled}, {peeved},
             {pissed}, {pissed off}, {riled}, {roiled}, {steamed},
             {stunng}]
     2: troubled persistently especially with petty annoyances;
        "harassed working mothers"; "a harried expression"; "her
        poor pestered father had to endure her constant
        interruptions"; "the vexed parents of an unruly teenager"
        [syn: ...
