---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mild
offline_file: ""
offline_thumbnail: ""
uuid: e4c6008e-4944-47b0-a843-4c07e52c817a
updated: 1484310383
title: mild
categories:
    - Dictionary
---
mild
     adj 1: moderate in type or degree or effect or force; far from
            extreme; "a mild winter storm"; "a mild fever";
            "fortunately the pain was mild"; "a mild rebuke";
            "mild criticism" [ant: {intense}]
     2: humble in spirit or manner; suggesting retiring mildness or
        even cowed submissiveness; "meek and self-effacing" [syn:
        {meek}, {modest}]
     3: mild and pleasant; "balmy days and nights"; "the climate was
        mild and conducive to life or growth" [syn: {balmy}]
