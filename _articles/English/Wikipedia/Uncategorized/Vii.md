---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vii
offline_file: ""
offline_thumbnail: ""
uuid: ed6d6e4f-bee3-4830-a588-e90fa2924bf0
updated: 1484308378
title: Vii
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-SportVii2.jpg
tags:
    - Games
    - VG Pocket Caplet
    - Zone 60 and Wireless 60
categories:
    - Uncategorized
---
JungleTac's Sport Vii (Chinese: 威力棒; pinyin: Wēilì bàng; lit. "Power Stick") is a Shanzhai video game console similar in aspect to Nintendo's Wii.[2] It was originally released in China in 2007. The Vii was not intended to be a seventh-generation console like the Wii, and was instead part of the dedicated console genre of inexpensive consoles with built-in games. It is based on the 16-bit Sunplus SPG CPU.
