---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/necessarily
offline_file: ""
offline_thumbnail: ""
uuid: 63e0b481-2164-4c76-9b19-172df8bd9525
updated: 1484310283
title: necessarily
categories:
    - Dictionary
---
necessarily
     adv 1: in an essential manner; "such expenses are necessarily
            incurred" [syn: {needfully}] [ant: {unnecessarily}]
     2: in such a manner as could not be otherwise; "it is
        necessarily so"; "we must needs by objective" [syn: {inevitably},
         {of necessity}, {needs}]
     3: as a highly likely consequence; "we are necessarily bound
        for federalism in Europe"
