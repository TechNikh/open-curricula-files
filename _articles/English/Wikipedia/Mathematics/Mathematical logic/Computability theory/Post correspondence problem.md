---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Post_correspondence_problem
offline_file: ""
offline_thumbnail: ""
uuid: 715ef08d-beac-48cd-85a7-2c42832be300
updated: 1484309490
title: Post correspondence problem
tags:
    - Definition of the problem
    - Alternative definition
    - Example instances of the problem
    - Example 1
    - Example 2
    - Proof sketch of undecidability
    - Variants
categories:
    - Computability theory
---
The Post correspondence problem is an undecidable decision problem that was introduced by Emil Post in 1946.[1] Because it is simpler than the halting problem and the Entscheidungsproblem it is often used in proofs of undecidability.
