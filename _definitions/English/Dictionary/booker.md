---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/booker
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484598481
title: booker
categories:
    - Dictionary
---
booker
     n : someone who engages a person or company for performances
         [syn: {booking agent}]
