---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Baker%27s_map'
offline_file: ""
offline_thumbnail: ""
uuid: e3478d50-0922-472a-819b-e6e29241e17d
updated: 1484308724
title: "Baker's map"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ising-tartan.png
tags:
    - Formal definition
    - Properties
    - As a shift operator
categories:
    - Chaotic maps
---
In dynamical systems theory, the baker's map is a chaotic map from the unit square into itself. It is named after a kneading operation that bakers apply to dough: the dough is cut in half, and the two halves are stacked on one another, and compressed.
