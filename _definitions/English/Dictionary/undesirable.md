---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undesirable
offline_file: ""
offline_thumbnail: ""
uuid: f64413bc-22d1-48a8-a2ab-da46a7a724b6
updated: 1484310277
title: undesirable
categories:
    - Dictionary
---
undesirable
     adj 1: not desirable; "undesirable impurities in steel";
            "legislation excluding undesirable aliens" [ant: {desirable}]
     2: not worthy of being chosen (especially as a spouse) [syn: {unsuitable}]
     n : one whose presence is undesirable; "rounding up vagrants and
         drunks and other undesirables"
