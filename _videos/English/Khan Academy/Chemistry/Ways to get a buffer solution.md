---
version: 1
type: video
provider: YouTube
id: l9-Qkl42-Tc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Ways%20to%20get%20a%20buffer%20solution.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bd51655a-4ffa-4a94-96ed-f2036a84f438
updated: 1486069585
title: Ways to get a buffer solution
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/l9-Qkl42-Tc/default.jpg
    - https://i3.ytimg.com/vi/l9-Qkl42-Tc/1.jpg
    - https://i3.ytimg.com/vi/l9-Qkl42-Tc/2.jpg
    - https://i3.ytimg.com/vi/l9-Qkl42-Tc/3.jpg
---
Describes the two main situations where you get a buffer: making a buffer for a specific pH by combining a weak acid and a salt containing the conjugate base, and using a neutralization reaction by combining a weak acid and a strong base.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/acid-base-equilibrium/titrations/v/titration-introduction?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/acid-base-equilibrium/buffer-solutions/v/ways-to-get-a-buffer-solution?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
