---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Atmospheric_wave
offline_file: ""
offline_thumbnail: ""
uuid: 1d8a9c6f-2f74-499b-acc4-d86ed61a6833
updated: 1484308881
title: Atmospheric wave
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Dust_Atmospheric_wave_Sep_23_2011_1200%2528UTC%2529.jpg'
categories:
    - Waves
---
An atmospheric wave is a periodic disturbance in the fields of atmospheric variables (like surface pressure or geopotential height, temperature, or wind velocity) which may either propagate (traveling wave) or not (standing wave). Atmospheric waves range in spatial and temporal scale from large-scale planetary waves (Rossby waves) to minute sound waves. Atmospheric waves with periods which are harmonics of 1 solar day (e.g. 24 hours, 12 hours, 8 hours... etc.) are known as atmospheric tides.
