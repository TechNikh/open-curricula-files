---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/easy
offline_file: ""
offline_thumbnail: ""
uuid: b92a2c20-c37f-4365-8b2e-de67af08efe9
updated: 1484310333
title: easy
categories:
    - Dictionary
---
easy
     adj 1: posing no difficulty; requiring little effort; "an easy
            job"; "an easy problem"; "an easy victory"; "the house
            is easy to heat"; "satisfied with easy answers"; "took
            the easy way out of his dilemma" [ant: {difficult}]
     2: not hurried or forced; "an easy walk around the block"; "at
        a leisurely (or easygoing) pace" [syn: {easygoing}, {leisurely}]
     3: free from worry or anxiety; "knowing that I had done my
        best, my mind was easy"; "an easy good-natured manner";
        "by the time the child faced the actual problem of reading
        she was familiar and at ease with all the elements words"
        [syn: {at ease}] ...
