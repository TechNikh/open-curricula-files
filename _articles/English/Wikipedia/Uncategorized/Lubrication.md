---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Greasing
offline_file: ""
offline_thumbnail: ""
uuid: cba9e100-7a23-43f7-9d36-3cda12a00a78
updated: 1484308309
title: Lubrication
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Steam_engine_lubrication.jpg
tags:
    - The regimes of lubrication
categories:
    - Uncategorized
---
Lubrication is the process or technique employed to reduce friction between, and wear of one or both, surfaces in proximity and moving relative to each other, by interposing a substance called a lubricant in between them. The lubricant can be a solid, (e.g. Molybdenum disulfide MoS2)[1] a solid/liquid dispersion, a liquid such as oil or water, a liquid-liquid dispersion (a grease) or a gas.
