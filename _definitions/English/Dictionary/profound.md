---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/profound
offline_file: ""
offline_thumbnail: ""
uuid: 711f8533-4926-4730-a070-31a34eca8862
updated: 1484310577
title: profound
categories:
    - Dictionary
---
profound
     adj 1: showing intellectual penetration or emotional depths; from
            the depths of your being; "the differences are
            profound"; "a profound insight"; "a profound book"; "a
            profound mind"; "profound contempt"; "profound regret"
            [ant: {superficial}]
     2: of the greatest intensity; complete; "a profound silence";
        "a state of profound shock"
     3: far-reaching and thoroughgoing in effect especially on the
        nature of something; "the fundamental revolution in human
        values that has occurred"; "the book underwent fundamental
        changes"; "committed the fundamental error of confusing
        spending with ...
