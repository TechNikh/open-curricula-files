---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boy
offline_file: ""
offline_thumbnail: ""
uuid: 85b0aba4-0d8c-4b2c-a8f9-4e3625a2d740
updated: 1484310299
title: boy
categories:
    - Dictionary
---
boy
     n 1: a youthful male person; "the baby was a boy"; "she made the
          boy brush his teeth every night"; "most soldiers are
          only boys in uniform" [syn: {male child}] [ant: {female
          child}, {female child}]
     2: a friendly informal reference to a grown man; "he likes to
        play golf with the boys"
     3: a male human offspring; "their son became a famous judge";
        "his boy is taller than he is" [syn: {son}] [ant: {daughter},
         {daughter}]
     4: (ethnic slur) offensive term for Black man; "get out of my
        way, boy"
