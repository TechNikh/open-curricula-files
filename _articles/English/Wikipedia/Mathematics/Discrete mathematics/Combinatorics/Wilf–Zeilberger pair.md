---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Wilf%E2%80%93Zeilberger_pair'
offline_file: ""
offline_thumbnail: ""
uuid: e55b8304-b590-4884-b1d2-fbfcb918a21f
updated: 1484308598
title: Wilf–Zeilberger pair
tags:
    - Definition
    - Example
categories:
    - Combinatorics
---
In mathematics, specifically combinatorics, a Wilf–Zeilberger pair, or WZ pair, is a pair of functions that can be used to certify certain combinatorial identities. WZ pairs are named after Herbert S. Wilf and Doron Zeilberger, and are instrumental in the evaluation of many sums involving binomial coefficients, factorials, and in general any hypergeometric series. A function's WZ counterpart may be used to find an equivalent, and much simpler sum. Although finding WZ pairs by hand is impractical in most cases, Gosper's algorithm provides a sure method to find a function's WZ counterpart, and can be implemented in a symbolic manipulation program.
