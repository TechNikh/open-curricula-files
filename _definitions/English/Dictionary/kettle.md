---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kettle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484450821
title: kettle
categories:
    - Dictionary
---
kettle
     n 1: a metal pot for stewing or boiling; usually has a lid [syn:
          {boiler}]
     2: the quantity a kettle will hold [syn: {kettleful}]
     3: (geology) a hollow (typically filled by a lake) that results
        from the melting of a mass of ice trapped in glacial
        deposits [syn: {kettle hole}]
     4: a large hemispherical brass or copper percussion instrument
        with a drumhead that can be tuned by adjusting the tension
        on it [syn: {kettledrum}, {tympanum}, {tympani}, {timpani}]
