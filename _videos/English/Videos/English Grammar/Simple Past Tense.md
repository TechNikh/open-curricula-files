---
version: 1
type: video
provider: YouTube
id: wQO1UcvZraM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Simple%20Past%20Tense.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 49049aec-27ca-45a1-9669-96057610c872
updated: 1486069631
title: Simple Past Tense
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/wQO1UcvZraM/default.jpg
    - https://i3.ytimg.com/vi/wQO1UcvZraM/1.jpg
    - https://i3.ytimg.com/vi/wQO1UcvZraM/2.jpg
    - https://i3.ytimg.com/vi/wQO1UcvZraM/3.jpg
---
