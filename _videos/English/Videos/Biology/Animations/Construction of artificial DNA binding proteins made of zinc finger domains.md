---
version: 1
type: video
provider: YouTube
id: j1RbaT8DgYM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Construction%20of%20artificial%20DNA%20binding%20proteins%20made%20of%20zinc%20finger%20domains.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c3ea692f-6d9a-4414-9ad1-1ed35a8ae2cd
updated: 1486069672
title: >
    Construction of artificial DNA binding proteins made of zinc
    finger domains
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/j1RbaT8DgYM/default.jpg
    - https://i3.ytimg.com/vi/j1RbaT8DgYM/1.jpg
    - https://i3.ytimg.com/vi/j1RbaT8DgYM/2.jpg
    - https://i3.ytimg.com/vi/j1RbaT8DgYM/3.jpg
---
