---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hobsbawm
offline_file: ""
offline_thumbnail: ""
uuid: 42fb98d1-a82d-46fc-b2fe-f91333ef33b9
updated: 1484308473
title: Eric Hobsbawm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Eric_Hobsbawm_Grave_in_Highgate_East_Cemetery_in_London_2016_06.jpg
tags:
    - Early life and education
    - Personal life
    - Academia
    - Works
    - Politics
    - Communism and Russia
    - Miscellaneous views
    - Praise and criticism
    - Death
    - Partial publication list
categories:
    - Uncategorized
---
Eric John Ernest Hobsbawm CH FRSL FBA (/ˈhɒbz.bɔːm/; 9 June 1917 – 1 October 2012) was a British Marxist historian of the rise of industrial capitalism, socialism and nationalism. His best-known works include his trilogy about what he called the "long 19th century" (The Age of Revolution: Europe 1789–1848, The Age of Capital: 1848–1875 and The Age of Empire: 1875–1914), The Age of Extremes on the short 20th century, and an edited volume that introduced the influential idea of "invented traditions".
