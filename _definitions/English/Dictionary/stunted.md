---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stunted
offline_file: ""
offline_thumbnail: ""
uuid: c63442f9-ca8d-4455-abab-e001a12e0aa4
updated: 1484096619
title: stunted
categories:
    - Dictionary
---
stunted
     adj : inferior in size or quality; "scrawny cattle"; "scrubby
           cut-over pine"; "old stunted thorn trees" [syn: {scrawny},
            {scrubby}]
