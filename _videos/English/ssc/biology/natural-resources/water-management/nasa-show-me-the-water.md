---
title: NASA | Show Me the Water
youtube_id: 4HSFKwho7MQ
tags:
    - nasa
    - GPM
    - Global Precipitation Measurement
    - flooding
    - Drought
    - water usage
    - Water use
    - Hydrology (Field Of Study)
    - Water cycle
    - hydrologic cycle
    - freshwater
    - fresh water
    - global water
categories:
    - Water management
---
