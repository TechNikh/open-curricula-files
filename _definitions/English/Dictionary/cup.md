---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cup
offline_file: ""
offline_thumbnail: ""
uuid: ce85ed7b-d8c6-4ab4-b723-96e30b5871a1
updated: 1484310351
title: cup
categories:
    - Dictionary
---
cup
     n 1: a United States liquid unit equal to 8 fluid ounces
     2: the quantity a cup will hold; "he drank a cup of coffee";
        "he borrowed a cup of sugar" [syn: {cupful}]
     3: a small open container usually used for drinking; usually
        has a handle; "he put the cup back in the saucer"; "the
        handle of the cup was missing"
     4: a large metal vessel with two handles that is awarded as a
        trophy to the winner of a competition; "the school kept
        the cups is a special glass case" [syn: {loving cup}]
     5: any cup-shaped concavity; "bees filled the waxen cups with
        honey"; "he wore a jock strap with a metal cup"; "the cup
        of her ...
