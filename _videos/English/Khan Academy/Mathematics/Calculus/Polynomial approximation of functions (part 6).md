---
version: 1
type: video
provider: YouTube
id: -gRNRBCG3Ow
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Polynomial%20approximation%20of%20functions%20%28part%206%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8794cd70-239d-4527-8979-478bee1de9da
updated: 1486069692
title: Polynomial approximation of functions (part 6)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/-gRNRBCG3Ow/default.jpg
    - https://i3.ytimg.com/vi/-gRNRBCG3Ow/1.jpg
    - https://i3.ytimg.com/vi/-gRNRBCG3Ow/2.jpg
    - https://i3.ytimg.com/vi/-gRNRBCG3Ow/3.jpg
---
