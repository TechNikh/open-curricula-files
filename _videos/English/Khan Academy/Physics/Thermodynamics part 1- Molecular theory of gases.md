---
version: 1
type: video
provider: YouTube
id: tQcB9BLUoVI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Thermodynamics%20part%201-%20Molecular%20theory%20of%20gases.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8d70d59e-49b5-4330-bb2f-e45a5921003b
updated: 1486069591
title: 'Thermodynamics part 1- Molecular theory of gases'
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/tQcB9BLUoVI/default.jpg
    - https://i3.ytimg.com/vi/tQcB9BLUoVI/1.jpg
    - https://i3.ytimg.com/vi/tQcB9BLUoVI/2.jpg
    - https://i3.ytimg.com/vi/tQcB9BLUoVI/3.jpg
---
Intuition of how gases generate pressure in a container and why pressure x volume is proportional to the combined kinetic energy of the molecules in the volume. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/thermodynamics/temp-kinetic-theory-ideal-gas-law/v/thermodynamics-part-2?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/fluids/fluid-dynamics/v/surface-tension-and-adhesion?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
