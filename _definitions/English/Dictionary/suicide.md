---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suicide
offline_file: ""
offline_thumbnail: ""
uuid: db253909-1256-4072-826f-7f4d17c44150
updated: 1484310569
title: suicide
categories:
    - Dictionary
---
suicide
     n 1: the act of killing yourself; "it is a crime to commit
          suicide" [syn: {self-destruction}, {self-annihilation}]
     2: a person who kills himself intentionally [syn: {felo-de-se}]
