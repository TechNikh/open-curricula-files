---
version: 1
type: article
id: https://en.wikipedia.org/wiki/6_(number)
offline_file: ""
offline_thumbnail: ""
uuid: fa40d77b-0230-4473-854a-b25a2220234d
updated: 1484308938
title: 6 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Polydactyly_01_Lhand_AP.jpg
tags:
    - In mathematics
    - In numeral systems
    - List of basic calculations
    - Greek and Latin word parts
    - Hexa
    - The prefix sex-
    - Evolution of the glyph
    - In music
    - In artists
    - In instruments
    - In music theory
    - In works
    - In religion
    - In science
    - Astronomy
    - Biology
    - chemistry
    - Medicine
    - Physics
    - In sports
    - In technology
    - In calendars
    - In the arts and entertainment
    - In other fields
categories:
    - Integers
---
The SI prefix for 10006 is exa- (E), and for its reciprocal atto- (a).
