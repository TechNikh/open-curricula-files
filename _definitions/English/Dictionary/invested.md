---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invested
offline_file: ""
offline_thumbnail: ""
uuid: f9c3d1d5-3a0d-46e5-a6e9-f7f4a2702bd8
updated: 1484310477
title: invested
categories:
    - Dictionary
---
invested
     adj : officially endowed with authority or power; "by the
           Constitution...the president is invested with
           certain...powers"- John Marshall [syn: {invested with}]
