---
version: 1
type: video
provider: YouTube
id: 7dYePWVTTTI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/The%20Comma/Commas%20in%20space%20and%20time.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3bc324b2-eeb3-45ff-8765-69600cd5ca4d
updated: 1486069601
title: Commas in space and time
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The Comma
thumbnail_urls:
    - https://i3.ytimg.com/vi/7dYePWVTTTI/default.jpg
    - https://i3.ytimg.com/vi/7dYePWVTTTI/1.jpg
    - https://i3.ytimg.com/vi/7dYePWVTTTI/2.jpg
    - https://i3.ytimg.com/vi/7dYePWVTTTI/3.jpg
---
Learn how to use commas when writing addresses (in space) and dates (in time).

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/e/salutations--valedictions--dates--and-addresses

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/v/commas-and-introductory-elements-the-comma-punctuation-khan-academy

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/v/salutations-and-valedictions-the-comma-punctuation-khan-academy

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
