---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/catching
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484486461
title: catching
categories:
    - Dictionary
---
catching
     adj : (of disease) capable of being transmitted by infection [syn:
            {communicable}, {contagious}, {contractable}, {transmissible},
            {transmittable}]
     n 1: (baseball) playing the position of catcher on a baseball
          team
     2: the act of detecting something; catching sight of something
        [syn: {detection}, {espial}, {spying}, {spotting}]
     3: becoming infected; "catching cold is sometimes unavoidable";
        "the contracting of a serious illness can be financially
        catastrophic" [syn: {contracting}]
