---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/addition
offline_file: ""
offline_thumbnail: ""
uuid: 4ab0698a-da01-4741-9083-f290e046f03a
updated: 1484310273
title: addition
categories:
    - Dictionary
---
addition
     n 1: a component that is added to something to improve it; "the
          addition of a bathroom was a major improvement"; "the
          addition of cinammon improved the flavor" [syn: {add-on},
           {improver}]
     2: the act of adding one thing to another; "the addition of
        flowers created a pleasing effect"; "the addition of a
        leap day every four years" [ant: {subtraction}]
     3: a quantity that is added; "there was an addition to property
        taxes this year"; "they recorded the cattle's gain in
        weight over a period of weeks" [syn: {increase}, {gain}]
     4: something added to what you already have; "the librarian
        shelved the ...
