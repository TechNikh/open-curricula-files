---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Curvature_form
offline_file: ""
offline_thumbnail: ""
uuid: 21143dad-940d-4702-8e75-3e5b3a6ebcf0
updated: 1484309215
title: Curvature form
tags:
    - Definition
    - Curvature form in a vector bundle
    - Bianchi identities
    - Notes
categories:
    - Differential geometry
---
In differential geometry, the curvature form describes curvature of a connection on a principal bundle. It can be considered as an alternative to or generalization of the curvature tensor in Riemannian geometry.
