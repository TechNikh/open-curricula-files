---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/originally
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484332381
title: originally
categories:
    - Dictionary
---
originally
     adv 1: in an original manner
     2: with reference to the origin or beginning [syn: {primitively},
         {in the beginning}]
     3: before now; "why didn't you tell me in the first place?"
        [syn: {in the first place}, {earlier}, {in the beginning},
         {to begin with}]
