---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perseus_(geometer)
offline_file: ""
offline_thumbnail: ""
uuid: c4bfae98-e2a9-4a5a-ab34-eab8e629ef46
updated: 1484309098
title: Perseus (geometer)
tags:
    - life
    - Spiric sections
    - Examples
categories:
    - Geometers
---
Perseus (c. 150 BC) was an ancient Greek geometer, who invented the concept of spiric sections, in analogy to the conic sections studied by Apollonius of Perga.
