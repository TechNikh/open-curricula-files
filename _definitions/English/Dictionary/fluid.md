---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fluid
offline_file: ""
offline_thumbnail: ""
uuid: 6447bf16-6539-4bb0-bb53-0eee252bfdef
updated: 1484310319
title: fluid
categories:
    - Dictionary
---
fluid
     adj 1: subject to change; variable; "a fluid situation fraught with
            uncertainty"; "everything was unstable following the
            coup" [syn: {unstable}]
     2: characteristic of a fluid; capable of flowing and easily
        changing shape [syn: {runny}]
     3: smooth and unconstrained in movement; "a long, smooth
        stride"; "the fluid motion of a cat"; "the liquid grace of
        a ballerina"; "liquid prose" [syn: {flowing}, {fluent}, {liquid},
         {smooth}]
     4: in cash or easily convertible to cash; "liquid (or fluid)
        assets" [syn: {liquid}]
     5: affording change (especially in social status); "Britain is
        not a truly fluid ...
