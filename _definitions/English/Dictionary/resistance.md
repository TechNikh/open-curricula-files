---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resistance
offline_file: ""
offline_thumbnail: ""
uuid: d17f119b-e408-4cb6-a798-256817a7dcd3
updated: 1484310200
title: resistance
categories:
    - Dictionary
---
resistance
     n 1: the action of opposing something that you disapprove or
          disagree with; "he encountered a general feeling of
          resistance from many citizens"; "despite opposition from
          the newspapers he went ahead" [syn: {opposition}]
     2: any mechanical force that tends to retard or oppose motion
     3: a material's opposition to the flow of electric current;
        measured in ohms [syn: {electric resistance}, {electrical
        resistance}, {impedance}, {resistivity}, {ohmic resistance}]
     4: the military action of resisting the enemy's advance; "the
        enemy offered little resistance"
     5: (medicine) the condition in which an organism can ...
