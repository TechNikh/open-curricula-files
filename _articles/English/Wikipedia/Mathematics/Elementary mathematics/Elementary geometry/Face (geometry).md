---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Face_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 1b274a54-82b3-41e6-8b84-cf21c094bfd5
updated: 1484308925
title: Face (geometry)
tags:
    - Polygonal face
    - Number of polygonal faces of a polyhedron
    - k-face
    - Cell or 3-face
    - Facet or (n-1)-face
    - Ridge or (n-2)-face
    - Peak or (n-3)-face
categories:
    - Elementary geometry
---
In solid geometry, a face is a flat (planar) surface that forms part of the boundary of a solid object;[1] a three-dimensional solid bounded exclusively by flat faces is a polyhedron.
