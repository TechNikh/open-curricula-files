---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cold
offline_file: ""
offline_thumbnail: ""
uuid: 0550e4c5-e9d3-4720-be5e-987f6201eb08
updated: 1484308842
title: Cold
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Antarctic_iceberg%252C_2001_-2.jpg'
tags:
    - Cooling
    - History
    - Early history
    - From the 17th century
    - 19th century
    - 20th century
    - Physiological effects
    - Notable cold locations and objects
    - Mythology and culture
categories:
    - Thermodynamics
---
Cold is the presence of low temperature, especially in the atmosphere.[4] In common usage, cold is often a subjective perception. A lower bound to temperature is absolute zero, defined as 0.00 K on the Kelvin scale, an absolute thermodynamic temperature scale. This corresponds to −273.15 °C on the Celsius scale, −459.67 °F on the Fahrenheit scale, and 0.00 °R on the Rankine scale.
