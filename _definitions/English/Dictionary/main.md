---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/main
offline_file: ""
offline_thumbnail: ""
uuid: 472f4c51-4d5b-4440-a2fe-93a75ff530a3
updated: 1484310307
title: main
categories:
    - Dictionary
---
main
     adj 1: most important element; "the chief aim of living"; "the main
            doors were of solid glass"; "the principal rivers of
            America"; "the principal example"; "policemen were
            primary targets" [syn: {chief(a)}, {main(a)}, {primary(a)},
             {principal(a)}]
     2: of a clause; able to stand alone syntactically as a complete
        sentence; "the main (or independent) clause in a complex
        sentence has at least a subject and a verb" [syn: {independent},
         {main(a)}] [ant: {dependent}]
     3: of force; of the greatest possible intensity; "by main
        strength" [syn: {main(a)}]
     n 1: any very large body of (salt) water ...
