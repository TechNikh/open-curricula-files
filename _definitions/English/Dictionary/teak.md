---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/teak
offline_file: ""
offline_thumbnail: ""
uuid: 3a3c6440-85aa-4cd0-b576-877c46bc0957
updated: 1484310254
title: teak
categories:
    - Dictionary
---
teak
     n 1: hard strong durable yellowish-brown wood of teak trees;
          resistant to insects and to warping; used for furniture
          and in shipbuilding [syn: {teakwood}]
     2: tall East Indian timber tree now planted in western Africa
        and tropical America for its hard durable wood [syn: {Tectona
        grandis}]
