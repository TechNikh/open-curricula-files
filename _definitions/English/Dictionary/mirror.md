---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mirror
offline_file: ""
offline_thumbnail: ""
uuid: 5fdc2841-9bdd-410d-adf2-87cbfe4fa4ae
updated: 1484310221
title: mirror
categories:
    - Dictionary
---
mirror
     n 1: polished surface that forms images by reflecting light
     2: a faithful depiction or reflection; "the best mirror is an
        old friend"
     v 1: reflect as if in a mirror; "The smallest pond at night
          mirrors the firmament above"
     2: reflect or resemble; "The plane crash in Milan mirrored the
        attack in the World Trade Center"
