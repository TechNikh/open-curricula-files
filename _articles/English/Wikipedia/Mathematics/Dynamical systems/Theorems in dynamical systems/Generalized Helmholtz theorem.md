---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalized_Helmholtz_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 11f050e5-285f-4075-abd4-fd6448013cbe
updated: 1484308690
title: Generalized Helmholtz theorem
categories:
    - Theorems in dynamical systems
---
The generalized Helmholtz theorem on skates is the multi-dimensional generalization of the Helmholtz theorem which is valid only in one dimension. The generalized Helmholtz theorem reads as follows.
