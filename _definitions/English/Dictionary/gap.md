---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gap
offline_file: ""
offline_thumbnail: ""
uuid: 561c4a28-fa78-48b1-ba1a-a607b8fe6959
updated: 1484310198
title: gap
categories:
    - Dictionary
---
gap
     n 1: a conspicuous disparity or difference as between two
          figures; "gap between income and outgo"; "the spread
          between lending and borrowing costs" [syn: {spread}]
     2: an open or empty space in or between things; "there was a
        small opening between the trees"; "the explosion made a
        gap in the wall" [syn: {opening}]
     3: a narrow opening; "he opened the window a crack" [syn: {crack}]
     4: a pass between mountain peaks [syn: {col}]
     5: an act of delaying or interrupting the continuity; "it was
        presented without commercial breaks" [syn: {break}, {interruption},
         {disruption}]
     v : make an opening or gap in [syn: ...
