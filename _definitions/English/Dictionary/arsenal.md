---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arsenal
offline_file: ""
offline_thumbnail: ""
uuid: 73e0d67d-501d-40dd-a6fd-7016ef2dd49f
updated: 1484310176
title: arsenal
categories:
    - Dictionary
---
arsenal
     n 1: all the weapons and equipment that a country has [syn: {armory},
           {armoury}]
     2: a military structure where arms and ammunition and other
        military equipment are stored and training is given in the
        use of arms [syn: {armory}, {armoury}]
     3: a place where arms are manufactured [syn: {armory}, {armoury}]
