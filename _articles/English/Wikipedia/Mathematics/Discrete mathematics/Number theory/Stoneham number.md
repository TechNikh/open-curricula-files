---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stoneham_number
offline_file: ""
offline_thumbnail: ""
uuid: c89fdac3-36d4-485b-b9d2-60d24cee56be
updated: 1484308661
title: Stoneham number
categories:
    - Number theory
---
In mathematics, the Stoneham numbers are a certain class of real numbers, named after mathematician Richard G. Stoneham (1920–1996). For coprime numbers b, c > 1, the Stoneham number αb,c is defined as
