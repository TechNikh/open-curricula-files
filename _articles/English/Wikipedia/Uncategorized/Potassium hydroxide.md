---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Potassium_hydroxide
offline_file: ""
offline_thumbnail: ""
uuid: 0ff250af-4a67-421f-b848-dcea0c237d24
updated: 1484308356
title: Potassium hydroxide
tags:
    - Properties and structure
    - Structure
    - Solubility and desiccating properties
    - Thermal stability
    - Reactions
    - As a base
    - As a nucleophile in organic chemistry
    - Reactions with inorganic compounds
    - Manufacture
    - Uses
    - Precursor to other potassium compounds
    - Manufacture of biodiesel
    - Manufacture of soft soaps
    - As an electrolyte
    - Niche applications
    - Petroleum refineries
categories:
    - Uncategorized
---
Along with sodium hydroxide (NaOH), this colorless solid is a prototypical strong base. It has many industrial and niche applications, most of which exploit its corrosive nature and its reactivity toward acids. An estimated 700,000 to 800,000 tonnes were produced in 2005. Approximately 100 times more NaOH than KOH is produced annually.[9] KOH is noteworthy as the precursor to most soft and liquid soaps as well as numerous potassium-containing chemicals.
