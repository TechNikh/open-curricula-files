---
version: 1
type: video
provider: YouTube
id: 11Bt6OhIeqA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Definite%20Integrals%20%28part%204%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 22df2ebf-cf38-409f-93ac-c9a3914b6140
updated: 1486069689
title: Definite Integrals (part 4)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/11Bt6OhIeqA/default.jpg
    - https://i3.ytimg.com/vi/11Bt6OhIeqA/1.jpg
    - https://i3.ytimg.com/vi/11Bt6OhIeqA/2.jpg
    - https://i3.ytimg.com/vi/11Bt6OhIeqA/3.jpg
---
