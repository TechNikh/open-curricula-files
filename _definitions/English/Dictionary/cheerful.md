---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cheerful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484632681
title: cheerful
categories:
    - Dictionary
---
cheerful
     adj 1: being full of or promoting cheer; having or showing good
            spirits; "her cheerful nature"; "a cheerful greeting";
            "a cheerful room"; "as cheerful as anyone confined to
            a hospital bed could be" [ant: {cheerless}]
     2: pleasantly (even unrealistically) optimistic [syn: {pollyannaish},
         {upbeat}]
