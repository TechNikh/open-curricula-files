---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/physician
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484385601
title: physician
categories:
    - Dictionary
---
physician
     n : a licensed medical practitioner; "I felt so bad I went to
         see my doctor" [syn: {doctor}, {doc}, {MD}, {Dr.}, {medico}]
