---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purchasing
offline_file: ""
offline_thumbnail: ""
uuid: 337ecf5b-2788-40f8-8233-0325e6f40cf0
updated: 1484310518
title: purchasing
categories:
    - Dictionary
---
purchasing
     n : the act of buying; "buying and selling fill their days";
         "shrewd purchasing requires considerable knowledge" [syn:
          {buying}]
