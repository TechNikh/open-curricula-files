---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Elias_Bassalygo_bound
offline_file: ""
offline_thumbnail: ""
uuid: 7c3d4a62-38ad-4327-a033-afce119bea40
updated: 1484308538
title: Elias Bassalygo bound
tags:
    - Definition
    - Proof
categories:
    - Coding theory
---
The Elias-Bassalygo bound is a mathematical limit used in coding theory for error correction during data transmission or communications. The properties of the Elias-Bassalygo bound are defined, below, using mathematical expressions.
