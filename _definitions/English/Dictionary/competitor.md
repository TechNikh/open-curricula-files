---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/competitor
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484331181
title: competitor
categories:
    - Dictionary
---
competitor
     n : the contestant you hope to defeat; "he had respect for his
         rivals"; "he wanted to know what the competition was
         doing" [syn: {rival}, {challenger}, {competition}, {contender}]
