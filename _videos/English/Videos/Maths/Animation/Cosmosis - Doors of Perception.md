---
version: 1
type: video
provider: YouTube
id: S1ffupfTW9A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 65c728ad-a88b-4db9-9242-e9c37c5df884
updated: 1486069644
title: 'Cosmosis - Doors of Perception'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/S1ffupfTW9A/default.jpg
    - https://i3.ytimg.com/vi/S1ffupfTW9A/1.jpg
    - https://i3.ytimg.com/vi/S1ffupfTW9A/2.jpg
    - https://i3.ytimg.com/vi/S1ffupfTW9A/3.jpg
---
