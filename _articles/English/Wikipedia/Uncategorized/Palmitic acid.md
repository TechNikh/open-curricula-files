---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Palmitic
offline_file: ""
offline_thumbnail: ""
uuid: cb59c07e-b7c3-4477-874b-42b358b4499e
updated: 1484308405
title: Palmitic acid
tags:
    - Occurrence and production
    - Biochemistry
    - Applications
    - Health effects
categories:
    - Uncategorized
---
Palmitic acid, or hexadecanoic acid in IUPAC nomenclature, is the most common fatty acid (saturated) found in animals, plants and microorganisms.[9] Its chemical formula is CH3(CH2)14COOH. As its name indicates, it is a major component of the oil from palm trees (palm oil), but can also be found in meats, cheeses, butter, and dairy products. Palmitate is a term for the salts and esters of palmitic acid. The palmitate anion is the observed form of palmitic acid at physiologic pH (7.4).
