---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/logarithm
offline_file: ""
offline_thumbnail: ""
uuid: a1a224df-4a72-4f82-b527-ff7180ac795b
updated: 1484310424
title: logarithm
categories:
    - Dictionary
---
logarithm
     n : the exponent required to produce a given number [syn: {log}]
