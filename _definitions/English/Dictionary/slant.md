---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slant
offline_file: ""
offline_thumbnail: ""
uuid: f94d202e-6399-40d0-bb2b-0d57cd921618
updated: 1484310156
title: slant
categories:
    - Dictionary
---
slant
     n 1: a biased way of looking at or presenting something [syn: {angle}]
     2: degree of deviation from a horizontal plane; "the roof had a
        steep pitch" [syn: {pitch}, {rake}]
     v 1: lie obliquely; "A scar slanted across his face"
     2: present with a bias; "He biased his presentation so as to
        please the share holders" [syn: {angle}, {weight}]
     3: to incline or bend from a vertical position; "She leaned
        over the banister" [syn: {lean}, {tilt}, {tip}, {angle}]
     4: heel over; "The tower is tilting"; "The ceiling is slanting"
        [syn: {cant}, {cant over}, {tilt}, {pitch}]
