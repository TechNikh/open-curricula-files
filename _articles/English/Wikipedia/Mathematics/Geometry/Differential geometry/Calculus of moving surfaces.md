---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Calculus_of_moving_surfaces
offline_file: ""
offline_thumbnail: ""
uuid: 197d76df-8889-418f-bbee-dc4673144ca3
updated: 1484309209
title: Calculus of moving surfaces
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Dannebrog.jpg
tags:
    - Analytical definitions
    - Properties of the δ/δt-derivative
    - Differentiation table for the δ/δt-derivative
    - Time differentiation of integrals
categories:
    - Differential geometry
---
The calculus of moving surfaces (CMS) [1] is an extension of the classical tensor calculus to deforming manifolds. Central to the CMS is the 
  
    
      
        δ
        
          /
        
        δ
        t
      
    
    {\displaystyle \delta /\delta t}
  
-derivative whose original definition [2] was put forth by Jacques Hadamard. It plays the role analogous to that of the covariant derivative 
  
    
      
        
          ∇
          
            α
          
        
      
    
    {\displaystyle \nabla _{\alpha }}
  
 on differential manifolds. In particular, it has the property that it produces a tensor when applied to a tensor.
