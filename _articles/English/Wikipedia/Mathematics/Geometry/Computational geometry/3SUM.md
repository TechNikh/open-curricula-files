---
version: 1
type: article
id: https://en.wikipedia.org/wiki/3SUM
offline_file: ""
offline_thumbnail: ""
uuid: b6f581f8-261d-4b41-bfdc-7ef8e0718fd6
updated: 1484309170
title: 3SUM
tags:
    - Quadratic algorithm
    - Variants
    - Non-zero sum
    - 3 different arrays
    - Convolution sum
    - Reduction from Conv3SUM to 3SUM
    - Reduction from 3SUM to Conv3SUM
    - 3SUM-hardness
    - Notes
categories:
    - Computational geometry
---
In computational complexity theory, the 3SUM problem asks if a given set of 
  
    
      
        n
      
    
    {\displaystyle n}
  
 real numbers contains three elements that sum to zero. A generalized version, rSUM, asks the same question of r numbers. 3SUM can be easily solved in 
  
    
      
        O
        (
        
          n
          
            2
          
        
        )
      
    
    {\displaystyle O(n^{2})}
  
 time, and matching 
  
    
      
        Ω
        (
        
          n
          
            ⌈
            r
            
              /
            
            2
            ⌉
          
        
        )
      
    
    {\displaystyle ...
