---
version: 1
type: video
provider: YouTube
id: xGiILI906C4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Surface%20integral%20ex3%20part%202-%20Evaluating%20the%20outside%20surface.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8983afce-cd3b-4e2b-a6a7-d1fd6de2e10d
updated: 1486069692
title: 'Surface integral ex3 part 2- Evaluating the outside surface'
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/xGiILI906C4/default.jpg
    - https://i3.ytimg.com/vi/xGiILI906C4/1.jpg
    - https://i3.ytimg.com/vi/xGiILI906C4/2.jpg
    - https://i3.ytimg.com/vi/xGiILI906C4/3.jpg
---
