---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defend
offline_file: ""
offline_thumbnail: ""
uuid: bf5ef221-b5b0-4f80-beaa-853498a63ef0
updated: 1484310567
title: defend
categories:
    - Dictionary
---
defend
     v 1: argue or speak in defense of; "She supported the motion to
          strike" [syn: {support}, {fend for}]
     2: be on the defensive; act against an attack [ant: {attack}]
     3: protect against a challenge or attack; "Hold that position
        behind the trees!"; "Hold the bridge against the enemy's
        attacks" [syn: {guard}, {hold}]
     4: fight against or resist strongly; "The senator said he would
        oppose the bill"; "Don't fight it!" [syn: {fight}, {oppose},
         {fight back}, {fight down}]
     5: protect or fight for as a champion [syn: {champion}]
     6: be the defense counsel for someone in a trial; "Ms. Smith
        will represent the ...
