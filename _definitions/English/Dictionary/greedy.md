---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greedy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484463181
title: greedy
categories:
    - Dictionary
---
greedy
     adj 1: immoderately desirous of acquiring e.g. wealth; "they are
            avaricious and will do anything for money"; "casting
            covetous eyes on his neighbor's fields"; "a grasping
            old miser"; "grasping commercialism"; "greedy for
            money and power"; "grew richer and greedier";
            "prehensile employers stingy with raises for their
            employees" [syn: {avaricious}, {covetous}, {grabby}, {grasping},
             {prehensile}]
     2: (often followed by `for') ardently or excessively desirous;
        "avid for adventure"; "an avid ambition to succeed";
        "fierce devouring affection"; "the esurient eyes of an
        avid ...
