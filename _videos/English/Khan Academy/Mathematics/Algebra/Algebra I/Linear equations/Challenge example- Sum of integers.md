---
version: 1
type: video
provider: YouTube
id: d8De3xcVmnw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Challenge%20example-%20Sum%20of%20integers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 763a222b-ce63-4e33-a29d-92165e5917c0
updated: 1486069559
title: 'Challenge example- Sum of integers'
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/d8De3xcVmnw/default.jpg
    - https://i3.ytimg.com/vi/d8De3xcVmnw/1.jpg
    - https://i3.ytimg.com/vi/d8De3xcVmnw/2.jpg
    - https://i3.ytimg.com/vi/d8De3xcVmnw/3.jpg
---
