---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/link
offline_file: ""
offline_thumbnail: ""
uuid: 691fd49c-7944-48d0-9466-86fc9fecea67
updated: 1484310283
title: link
categories:
    - Dictionary
---
link
     n 1: the means of connection between things linked in series
          [syn: {nexus}]
     2: a fastener that serves to join or link; "the walls are held
        together with metal links placed in the wet mortar during
        construction" [syn: {linkup}, {tie}, {tie-in}]
     3: the state of being connected; "the connection between church
        and state is inescapable" [syn: {connection}, {connectedness}]
        [ant: {disjunction}]
     4: a connecting shape [syn: {connection}, {connexion}]
     5: a unit of length equal to 1/100 of a chain
     6: (computing) an instruction that connects one part of a
        program or an element on a list to another program or list
     ...
