---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Close_relationship
offline_file: ""
offline_thumbnail: ""
uuid: 953c711a-a4dd-48e4-a68d-68f656a515a8
updated: 1484308365
title: Interpersonal relationship
tags:
    - Field of study
    - Types
    - Importance
    - Need to belong
    - Social exchange
    - Relational self
    - Power and dominance
    - Stages
    - Relationship satisfaction
    - Flourishing, budding, blooming, blossoming relationships
    - Background
    - Adult attachment and attachment theory
    - Romantic love
    - Theories and empirical research
    - Confucianism
    - Minding relationships
    - Theory of intertype relationships
    - Culture of appreciation
    - Capitalizing on positive events
    - The Vulnerability Stress Adaptation (VSA) Model
    - Other perspectives
    - Neurobiology of interpersonal connections
    - Applications
    - Controversies
categories:
    - Uncategorized
---
An interpersonal relationship is a strong, deep, or close association or acquaintance between two or more people that may range in duration from brief to enduring. This association may be based on inference, love, solidarity, regular business interactions, or some other type of social commitment. Interpersonal relationships are formed in the context of social, cultural and other influences. The context can vary from family or kinship relations, friendship, marriage, relations with associates, work, clubs, neighborhoods, and places of worship. They may be regulated by law, custom, or mutual agreement, and are the basis of social groups and society as a whole.
