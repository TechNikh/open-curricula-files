---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flame
offline_file: ""
offline_thumbnail: ""
uuid: d3e034d3-e560-461e-89af-cced57ef2276
updated: 1484310232
title: flame
categories:
    - Dictionary
---
flame
     n : the process of combustion of inflammable materials producing
         heat and light and (often) smoke; "fire was one of our
         ancestors' first discoveries" [syn: {fire}, {flaming}]
     v 1: shine with a sudden light; "The night sky flared with the
          massive bombardment" [syn: {flare}]
     2: be in flames or aflame; "The sky seemed to flame in the
        Hawaiian sunset"
     3: criticize harshly, on the e-mail
