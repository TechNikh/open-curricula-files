---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reduced_residue_system
offline_file: ""
offline_thumbnail: ""
uuid: 8e2ea275-4de4-4347-bf54-10154ee8450d
updated: 1484308944
title: Reduced residue system
tags:
    - facts
    - Notes
categories:
    - Elementary number theory
---
Here 
  
    
      
        φ
      
    
    {\displaystyle \varphi }
  
 denotes Euler's totient function.
