---
title: Carriage of carbon dioxide
youtube_id: sBSeYH704HE
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - transportation-of-gases-by-blood
---
