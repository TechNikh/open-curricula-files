---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Walter_Whiteley
offline_file: ""
offline_thumbnail: ""
uuid: 9aa53b68-b6ab-4689-9209-4767dbcd1b6f
updated: 1484309105
title: Walter Whiteley
tags:
    - Education and career
    - Awards and honours
    - Selected publications
categories:
    - Geometers
---
Walter John Whiteley is a professor in the department of mathematics and statistics at York University in Canada.[1] He specializes in geometry and mathematics education, and is known for his expertise in structural rigidity and rigidity matroids.
