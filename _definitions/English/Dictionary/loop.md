---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loop
offline_file: ""
offline_thumbnail: ""
uuid: c8a48b3f-9fc3-4fa0-a70c-0ed6e62c6cd5
updated: 1484310196
title: loop
categories:
    - Dictionary
---
loop
     n 1: fastener consisting of a metal ring for lining a small hole
          to permit the attachment of cords or lines [syn: {cringle},
           {eyelet}, {grommet}, {grummet}]
     2: anything with a round or oval shape (formed by a curve that
        is closed and does not intersect itself)
     3: (computer science) a single execution of a set of
        instructions that are to be repeated; "the solution took
        hundreds of iterations" [syn: {iteration}]
     4: an inner circle of advisors (especially under President
        Reagan); "he's no longer in the loop"
     5: the basic pattern of the human fingerprint
     6: a computer program that performs a series of ...
