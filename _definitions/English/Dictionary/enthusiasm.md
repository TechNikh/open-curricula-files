---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enthusiasm
offline_file: ""
offline_thumbnail: ""
uuid: 8cb0e974-fd47-4941-85a1-b552e42eca8c
updated: 1484310559
title: enthusiasm
categories:
    - Dictionary
---
enthusiasm
     n 1: a feeling of excitement
     2: overflowing with enthusiasm [syn: {exuberance}, {ebullience}]
     3: a lively interest; "enthusiasm for his program is growing"
