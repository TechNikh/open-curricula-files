---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Francisco_Santos_Leal
offline_file: ""
offline_thumbnail: ""
uuid: c67af6e2-60cc-4411-acd0-6df052c13d7a
updated: 1484309096
title: Francisco Santos Leal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-PacoSantos_2010.jpg
categories:
    - Geometers
---
Francisco (Paco) Santos Leal (born May 28, 1968) is a Spanish mathematician at the University of Cantabria, known for finding a counterexample to the Hirsch conjecture in polyhedral combinatorics.[1][2][3] In 2015 he won the Fulkerson Prize for this research.[4]
