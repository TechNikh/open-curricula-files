---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/religion
offline_file: ""
offline_thumbnail: ""
uuid: 5741063f-5045-4a73-a0e8-40478a6e4110
updated: 1484310469
title: religion
categories:
    - Dictionary
---
religion
     n 1: a strong belief in a supernatural power or powers that
          control human destiny; "he lost his faith but not his
          morality" [syn: {faith}, {religious belief}]
     2: institution to express belief in a divine power; "he was
        raised in the Baptist religion"; "a member of his own
        faith contradicted him" [syn: {faith}]
