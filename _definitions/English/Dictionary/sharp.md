---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sharp
offline_file: ""
offline_thumbnail: ""
uuid: 48463033-dd6e-4f6a-b426-1b539154c8dc
updated: 1484310341
title: sharp
categories:
    - Dictionary
---
sharp
     adj 1: (of something seen or heard) clearly defined; "a sharp
            photographic image"; "the sharp crack of a twig"; "the
            crisp snap of dry leaves underfoot" [syn: {crisp}]
     2: ending in a sharp point [syn: {acuate}, {acute}, {needlelike}]
     3: having or demonstrating ability to recognize or draw fine
        distinctions; "an acute observer of politics and
        politicians"; "incisive comments"; "icy knifelike
        reasoning"; "as sharp and incisive as the stroke of a
        fang"; "penetrating insight"; "frequent penetrative
        observations" [syn: {acute}, {discriminating}, {incisive},
         {keen}, {knifelike}, {penetrating}, ...
