---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adjust
offline_file: ""
offline_thumbnail: ""
uuid: e5c73196-2490-4ca9-ad4d-0abf0e2ba656
updated: 1484310212
title: adjust
categories:
    - Dictionary
---
adjust
     v 1: alter or regulate so as to achieve accuracy or conform to a
          standard; "Adjust the clock, please"; "correct the
          alignment of the front wheels" [syn: {set}, {correct}]
     2: place in a line or arrange so as to be parallel or straight;
        "align the car with the curb"; "align the sheets of paper
        on the table" [syn: {align}, {aline}, {line up}] [ant: {skew}]
     3: adapt or conform oneself to new or different conditions; "We
        must adjust to the bad economic situation" [syn: {conform},
         {adapt}]
     4: make correspondent or conformable; "Adjust your eyes to the
        darkness"
     5: decide how much is to be paid on an ...
