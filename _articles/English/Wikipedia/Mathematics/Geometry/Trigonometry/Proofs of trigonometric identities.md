---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Proofs_of_trigonometric_identities
offline_file: ""
offline_thumbnail: ""
uuid: 98e59bd4-8a7c-4167-9968-4e5f5c5558d3
updated: 1484309348
title: Proofs of trigonometric identities
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/382px-Trigonometric_Triangle.svg.png
tags:
    - Elementary trigonometric identities
    - Definitions
    - Ratio identities
    - Complementary angle identities
    - Pythagorean identities
    - Angle sum identities
    - Sine
    - Cosine
    - Tangent and cotangent
    - Double-angle identities
    - Half-angle identities
    - 'Miscellaneous -- the triple tangent identity'
    - 'Miscellaneous -- the triple cotangent identity'
    - Prosthaphaeresis identities
    - Proof of sine identities
    - Proof of cosine identities
    - Inequalities
    - Identities involving calculus
    - Preliminaries
    - Sine and angle ratio identity
    - Cosine and angle ratio identity
    - Cosine and square of angle ratio identity
    - Proof of Compositions of trig and inverse trig functions
    - Notes
categories:
    - Trigonometry
---
Proofs of trigonometric identities are used to show relations between trigonometric functions. This article will list trigonometric identities and prove them.
