---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anarchy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484530081
title: anarchy
categories:
    - Dictionary
---
anarchy
     n : a state of lawlessness and disorder (usually resulting from
         a failure of government) [syn: {lawlessness}]
