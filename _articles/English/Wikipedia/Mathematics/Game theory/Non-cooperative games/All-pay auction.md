---
version: 1
type: article
id: https://en.wikipedia.org/wiki/All-pay_auction
offline_file: ""
offline_thumbnail: ""
uuid: 80ad4a9e-aef7-401e-a1a6-f602032a99ea
updated: 1484309052
title: All-pay auction
categories:
    - Non-cooperative games
---
In economics and game theory, an all-pay auction is an auction in which every bidder must pay regardless of whether they win the prize, which is awarded to the highest bidder as in a conventional auction.
