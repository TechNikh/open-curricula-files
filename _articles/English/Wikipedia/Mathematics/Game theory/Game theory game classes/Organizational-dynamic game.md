---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Organizational-dynamic_game
offline_file: ""
offline_thumbnail: ""
uuid: 087698b0-4fdb-49d7-b9fa-c9e55ca7ab0c
updated: 1484309040
title: Organizational-dynamic game
categories:
    - Game theory game classes
---
Organizational-dynamic games are usually designed for the specific purpose of furthering personal development and character building, particularly in addressing complex organizational situations, such as managing change and innovation diffusion in a company, helping people in the organization to introduce productive collaboration patterns, managing difficult meeting situations, etc.
