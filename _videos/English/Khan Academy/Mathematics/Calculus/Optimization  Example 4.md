---
version: 1
type: video
provider: YouTube
id: T8sG4Sb3g7Y
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Optimization%20%20Example%204.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c438567b-282c-4cfd-af68-d05b46eec1e0
updated: 1486069689
title: 'Optimization  Example 4'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/T8sG4Sb3g7Y/default.jpg
    - https://i3.ytimg.com/vi/T8sG4Sb3g7Y/1.jpg
    - https://i3.ytimg.com/vi/T8sG4Sb3g7Y/2.jpg
    - https://i3.ytimg.com/vi/T8sG4Sb3g7Y/3.jpg
---
