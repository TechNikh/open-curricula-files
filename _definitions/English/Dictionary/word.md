---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/word
offline_file: ""
offline_thumbnail: ""
uuid: 58881786-65f8-4779-bbbf-4beb60280eb4
updated: 1484310277
title: word
categories:
    - Dictionary
---
word
     n 1: a unit of language that native speakers can identify; "words
          are the blocks from which sentences are made"; "he
          hardly said ten words all morning"
     2: a brief statement; "he didn't say a word about it"
     3: new information about specific and timely events; "they
        awaited news of the outcome" [syn: {news}, {intelligence},
         {tidings}]
     4: the divine word of God; the second person in the Trinity
        (incarnate in Jesus) [syn: {Son}, {Logos}]
     5: a promise; "he gave his word" [syn: {parole}, {word of honor}]
     6: a secret word or phrase known only to a restricted group;
        "he forgot the password" [syn: {password}, ...
