---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Resolution_proof_compression_by_splitting
offline_file: ""
offline_thumbnail: ""
uuid: 7c1ac41c-9151-484a-8515-795ae4fd28df
updated: 1484309499
title: Resolution proof compression by splitting
categories:
    - Proof theory
---
In mathematical logic, proof compression by splitting is an algorithm that operates as a post-process on resolution proofs. It was proposed by Scott Cotton in his paper "Two Techniques for Minimizing Resolution Proof".[1]
