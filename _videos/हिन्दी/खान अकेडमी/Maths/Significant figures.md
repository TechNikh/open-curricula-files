---
version: 1
type: video
provider: YouTube
id: 8CmlXDxFXzs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0ea22d1f-d807-490f-9724-03dcd61a27a1
updated: 1486069607
title: Significant figures
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/8CmlXDxFXzs/default.jpg
    - https://i3.ytimg.com/vi/8CmlXDxFXzs/1.jpg
    - https://i3.ytimg.com/vi/8CmlXDxFXzs/2.jpg
    - https://i3.ytimg.com/vi/8CmlXDxFXzs/3.jpg
---
