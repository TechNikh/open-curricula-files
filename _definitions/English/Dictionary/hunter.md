---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hunter
offline_file: ""
offline_thumbnail: ""
uuid: e932a8ed-f02d-4a48-8596-25c5be42df75
updated: 1484310439
title: hunter
categories:
    - Dictionary
---
hunter
     n 1: someone who hunts game [syn: {huntsman}]
     2: a person who searches for something; "a treasure hunter"
     3: a watch with a hinged metal lid to protect the crystal [syn:
         {hunting watch}]
