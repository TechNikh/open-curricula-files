---
version: 1
type: video
provider: YouTube
id: BiVOC3WocXs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Derivative%20applications/L%27Ho%CC%82pital%27s%20rule%20example%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6642c225-4545-424e-a3a3-13fad845befa
updated: 1486069693
title: "L'Hôpital's rule example 1"
categories:
    - Derivative applications
thumbnail_urls:
    - https://i3.ytimg.com/vi/BiVOC3WocXs/default.jpg
    - https://i3.ytimg.com/vi/BiVOC3WocXs/1.jpg
    - https://i3.ytimg.com/vi/BiVOC3WocXs/2.jpg
    - https://i3.ytimg.com/vi/BiVOC3WocXs/3.jpg
---
