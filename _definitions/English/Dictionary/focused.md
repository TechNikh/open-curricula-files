---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/focused
offline_file: ""
offline_thumbnail: ""
uuid: 70e414e4-10e7-4bb9-a616-f792e9b3aede
updated: 1484310353
title: focused
categories:
    - Dictionary
---
focused
     adj 1: of an optical system (e.g. eye or opera glasses) adjusted to
            produce a clear image
     2: being in focus or brought into focus [syn: {focussed}] [ant:
         {unfocused}]
     3: (of light rays) converging on a point; "focused light rays
        can set something afire" [syn: {focussed}]
     4: concentrated on or clustered around a central point or
        purpose [syn: {centered}, {centred}, {centralized}, {centralised}]
