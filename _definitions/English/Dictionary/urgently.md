---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urgently
offline_file: ""
offline_thumbnail: ""
uuid: 41df8e09-dcfe-4341-bf89-ba6b98fe216b
updated: 1484310183
title: urgently
categories:
    - Dictionary
---
urgently
     adv : with great urgency; "health care reform is needed urgently";
           "the soil desperately needed potash" [syn: {desperately}]
