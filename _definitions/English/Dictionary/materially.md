---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/materially
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484618221
title: materially
categories:
    - Dictionary
---
materially
     adv 1: with respect to material aspects; "psychologically similar
            but materially different"
     2: to a significant degree; "it aided him materially in winning
        the argument"
