---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/haughty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414821
title: haughty
categories:
    - Dictionary
---
haughty
     adj : having or showing arrogant superiority to and disdain of
           those one views as unworthy; "some economists are
           disdainful of their colleagues in other social
           disciplines"; "haughty aristocrats"; "his lordly
           manners were offensive"; "walked with a prideful
           swagger"; "very sniffy about breaches of etiquette";
           "his mother eyed my clothes with a supercilious air";
           "shaggy supercilious camels"; "a more swaggering mood
           than usual"- W.L.Shirer [syn: {disdainful}, {lordly}, {prideful},
            {sniffy}, {supercilious}, {swaggering}]
     [also: {haughtiest}, {haughtier}]
