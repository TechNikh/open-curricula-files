---
version: 1
type: video
provider: YouTube
id: v0k920igOt8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b26d4280-175f-4085-83a3-17c522d0ebc5
updated: 1486069611
title: Introduction to subtraction
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/v0k920igOt8/default.jpg
    - https://i3.ytimg.com/vi/v0k920igOt8/1.jpg
    - https://i3.ytimg.com/vi/v0k920igOt8/2.jpg
    - https://i3.ytimg.com/vi/v0k920igOt8/3.jpg
---
