---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/withdrawal
offline_file: ""
offline_thumbnail: ""
uuid: 3c51adf3-5679-4f78-a567-d69becc71279
updated: 1484310587
title: withdrawal
categories:
    - Dictionary
---
withdrawal
     n 1: a retraction of a previously held position [syn: {backdown},
           {climb-down}]
     2: the act of taking out money or other capital
     3: the act of withdrawing
     4: avoiding emotional involvement [syn: {detachment}]
     5: formal separation from an alliance or federation [syn: {secession}]
     6: the termination of drug taking [syn: {drug withdrawal}]
