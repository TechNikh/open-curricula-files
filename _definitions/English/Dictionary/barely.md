---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/barely
offline_file: ""
offline_thumbnail: ""
uuid: 470dbedd-8896-4010-857d-99bcb5bded72
updated: 1484310455
title: barely
categories:
    - Dictionary
---
barely
     adv 1: by a small margin; "they could barely hear the speaker"; "we
            hardly knew them"; "just missed being hit"; "had
            scarcely rung the bell when the door flew open";
            "would have scarce arrived before she would have found
            some excuse to leave"- W.B.Yeats [syn: {hardly}, {just},
             {scarcely}, {scarce}]
     2: in a sparse or scanty way; "a barely furnished room" [syn: {scantily}]
