---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bai
offline_file: ""
offline_thumbnail: ""
uuid: b9a52ea4-4b5d-47d2-a81e-fb249ff26a4e
updated: 1484310543
title: bai
categories:
    - Dictionary
---
Bai
     n : the Tibeto-Burman language spoken in the Dali region of
         Yunnan [syn: {Baic}]
