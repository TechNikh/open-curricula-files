---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Lorentz-violating_electrodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 5a656ee7-3c85-4558-9ac2-71ca98b36b30
updated: 1484308754
title: Lorentz-violating electrodynamics
tags:
    - Minimal Lorentz-violating electrodynamics
    - Nonminimal Lorentz-violating electrodynamics
    - Experiments
    - Vacuum birefringence
    - Vacuum dispersion
    - Resonant cavities
    - Other experiments
categories:
    - Electrodynamics
---
Searches for Lorentz violation involving photons are among the best tests of relativity.[why?] Examples range from modern versions of the classic Michelson-Morley experiment that utilize highly stable electromagnetic resonant cavities to searches for tiny deviations from c in the speed of light emitted by distant astrophysical sources. Due to the extreme distances involved, astrophysical studies have achieved sensitivities on the order of parts in 1038.
