---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ghastly
offline_file: ""
offline_thumbnail: ""
uuid: 75aebd14-16cd-42cb-9637-e18e4d3d2fa6
updated: 1484310186
title: ghastly
categories:
    - Dictionary
---
ghastly
     adj 1: shockingly repellent; inspiring horror; "ghastly wounds";
            "the grim aftermath of the bombing"; "the grim task of
            burying the victims"; "a grisly murder"; "gruesome
            evidence of human sacrifice"; "macabre tales of war
            and plague in the Middle ages"; "macabre tortures
            conceived by madmen" [syn: {grim}, {grisly}, {gruesome},
             {macabre}]
     2: gruesomely indicative of death or the dead; "a charnel smell
        came from the chest filled with dead men's bones";
        "ghastly shrieks"; "the sepulchral darkness of the
        catacombs" [syn: {charnel}, {sepulchral}]
     [also: {ghastliest}, ...
