---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/home-made
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484486641
title: home-made
categories:
    - Dictionary
---
homemade
     adj : made or produced in the home or by yourself; "homemade
           bread" [ant: {factory-made}]
