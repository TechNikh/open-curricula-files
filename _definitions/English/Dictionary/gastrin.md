---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gastrin
offline_file: ""
offline_thumbnail: ""
uuid: 8c25e4da-5fab-453e-989d-0dd8cc814f98
updated: 1484310313
title: gastrin
categories:
    - Dictionary
---
gastrin
     n : polypeptide hormone secreted by the mucous lining of the
         stomach; induces the secretion of gastric juice
