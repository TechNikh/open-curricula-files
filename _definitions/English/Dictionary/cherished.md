---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cherished
offline_file: ""
offline_thumbnail: ""
uuid: 49d10aef-a46a-4c09-9024-a54478df5611
updated: 1484310541
title: cherished
categories:
    - Dictionary
---
cherished
     adj 1: characterized by feeling or showing fond affection for; "a
            cherished friend"; "children are precious"; "a
            treasured heirloom"; "so good to feel wanted" [syn: {precious},
             {treasured}, {wanted}]
     2: thought of and clung to fondly or reverentially [syn: {held
        dear(p)}]
