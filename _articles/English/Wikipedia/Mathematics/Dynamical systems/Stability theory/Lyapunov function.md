---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lyapunov_function
offline_file: ""
offline_thumbnail: ""
uuid: 5c0c4b3d-8874-41ed-b086-1904c2c9f7c6
updated: 1484308825
title: Lyapunov function
tags:
    - Definition of a Lyapunov candidate function
    - Definition of the equilibrium point of a system
    - Basic Lyapunov theorems for autonomous systems
    - Stable equilibrium
    - Locally asymptotically stable equilibrium
    - Globally asymptotically stable equilibrium
    - Example
categories:
    - Stability theory
---
In the theory of ordinary differential equations (ODEs), Lyapunov functions are scalar functions that may be used to prove the stability of an equilibrium of an ODE. Named after the Russian mathematician Aleksandr Mikhailovich Lyapunov, Lyapunov functions (also called the Lyapunov’s second method for stability) are important to stability theory of dynamical systems and control theory. A similar concept appears in the theory of general state space Markov chains, usually under the name Foster–Lyapunov functions.
