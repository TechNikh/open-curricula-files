---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/goodbye
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484436481
title: goodbye
categories:
    - Dictionary
---
good-bye
     n : a farewell remark; "they said their good-byes" [syn: {adieu},
          {adios}, {arrivederci}, {auf wiedersehen}, {au revoir},
         {bye}, {bye-bye}, {cheerio}, {good-by}, {goodby}, {goodbye},
          {good day}, {sayonara}, {so long}]
