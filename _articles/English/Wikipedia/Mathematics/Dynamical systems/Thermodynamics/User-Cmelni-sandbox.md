---
version: 1
type: article
id: https://en.wikipedia.org/wiki/User:Cmelni/sandbox
offline_file: ""
offline_thumbnail: ""
uuid: 41664c24-8129-446c-b665-6da9a9686590
updated: 1484308834
title: User:Cmelni/sandbox
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Phonovoltaic_Cell_and_Energy_Diagram.png
tags:
    - Phonovoltaic
    - Satisfying the laws of thermodynamics
    - >
        Non-equilibrium optical phonon population and the nanoscale
        requirent
    - Entropy generation and efficiency
    - The electron-phonon coupling
    - The phonon-phonon coupling
    - pV materials
categories:
    - Thermodynamics
---
