---
version: 1
type: video
provider: YouTube
id: edxQQX8TEi8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3bc82569-0e78-4f0c-9854-7750ac046e1f
updated: 1486069628
title: Yadagama Sandhi
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/edxQQX8TEi8/default.jpg
    - https://i3.ytimg.com/vi/edxQQX8TEi8/1.jpg
    - https://i3.ytimg.com/vi/edxQQX8TEi8/2.jpg
    - https://i3.ytimg.com/vi/edxQQX8TEi8/3.jpg
---
