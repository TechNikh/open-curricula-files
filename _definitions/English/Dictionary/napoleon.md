---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/napoleon
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484425081
title: napoleon
categories:
    - Dictionary
---
Napoleon
     n 1: French general who became emperor of the French (1769-1821)
          [syn: {Napoleon I}, {Napoleon Bonaparte}, {Bonaparte}, {the
          Little Corporal}]
     2: a rectangular piece of pastry with thin flaky layers and
        filled with custard cream
     3: a card game similar to whist; usually played for stakes
        [syn: {nap}]
