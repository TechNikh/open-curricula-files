---
version: 1
type: video
provider: YouTube
id: l9ft9jpriNA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Example-%20Different%20ways%20to%20pick%20officers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a39a3044-e967-474a-818c-825a2e835fdc
updated: 1486069561
title: 'Example- Different ways to pick officers'
categories:
    - Precalculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/l9ft9jpriNA/default.jpg
    - https://i3.ytimg.com/vi/l9ft9jpriNA/1.jpg
    - https://i3.ytimg.com/vi/l9ft9jpriNA/2.jpg
    - https://i3.ytimg.com/vi/l9ft9jpriNA/3.jpg
---
