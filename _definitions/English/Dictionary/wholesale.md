---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wholesale
offline_file: ""
offline_thumbnail: ""
uuid: 3dbe973a-fece-4cf6-82ec-46892c3b668f
updated: 1484310522
title: wholesale
categories:
    - Dictionary
---
wholesale
     adj 1: selling or related to selling goods in large quantities for
            resale to the consumer; "wholesale prices"; "a
            wholesale produce market" [ant: {retail}]
     2: ignoring distinctions; "sweeping generalizations";
        "wholesale destruction" [syn: {sweeping}]
     n : the selling of goods to merchants; usually in large
         quantities for resale to consumers [ant: {retail}]
     adv 1: at a wholesale price; "I can sell it to you wholesale" [ant:
             {retail}]
     2: on a large scale without careful discrimination; "I buy food
        wholesale" [syn: {in large quantities}]
     v : sell in large quantities [ant: {retail}]
