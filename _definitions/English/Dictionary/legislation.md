---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/legislation
offline_file: ""
offline_thumbnail: ""
uuid: 46e92925-8caa-43c3-9185-5ce5907d380f
updated: 1484310609
title: legislation
categories:
    - Dictionary
---
legislation
     n 1: law enacted by a legislative body [syn: {statute law}]
     2: the act of making or enacting laws [syn: {legislating}, {lawmaking}]
