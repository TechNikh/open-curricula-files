---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Discrete_Morse_theory
offline_file: ""
offline_thumbnail: ""
uuid: a3576f9f-cc1c-4c82-9707-f434aeb98b3c
updated: 1484308570
title: Discrete Morse theory
tags:
    - Notation regarding CW complexes
    - Discrete Morse functions
    - The Morse complex
    - Basic Results
    - The Morse Inequalities
    - Discrete Morse Homology and Homotopy Type
categories:
    - Combinatorics
---
Discrete Morse theory is a combinatorial adaptation of Morse theory developed by Robin Forman. The theory has various practical applications in diverse fields of applied mathematics and computer science, such as configuration spaces,[1] homology computation,[2][3] denoising,[4] and mesh compression.[5]
