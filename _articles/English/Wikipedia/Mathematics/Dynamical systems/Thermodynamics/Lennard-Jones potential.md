---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lennard-Jones_potential
offline_file: ""
offline_thumbnail: ""
uuid: 35908939-e144-4cd8-97a7-3332827448eb
updated: 1484308846
title: Lennard-Jones potential
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-12-6-Lennard-Jones-Potential.svg.png
tags:
    - Explanation
    - Alternative expressions
    - AB form
    - Truncated and shifted form
    - Limits
categories:
    - Thermodynamics
---
The Lennard-Jones potential (also termed the L-J potential, 6-12 potential, or 12-6 potential) is a mathematically simple model that approximates the interaction between a pair of neutral atoms or molecules. A form of this interatomic potential was first proposed in 1924 by John Lennard-Jones.[1] The most common expressions of the L-J potential are:
