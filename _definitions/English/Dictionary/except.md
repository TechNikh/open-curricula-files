---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/except
offline_file: ""
offline_thumbnail: ""
uuid: e6448c3c-5857-4121-9fd7-e742e08d8689
updated: 1484310359
title: except
categories:
    - Dictionary
---
except
     v 1: take exception to; "he demurred at my suggestion to work on
          Saturday" [syn: {demur}]
     2: prevent from being included or considered or accepted; "The
        bad results were excluded from the report"; "Leave off the
        top piece" [syn: {exclude}, {leave out}, {leave off}, {omit},
         {take out}] [ant: {include}]
