---
version: 1
type: video
provider: YouTube
id: suN-sV0cT6c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Introduction%20to%20Protein%20Synthesis.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4fd4b7cb-08a1-44b0-80ef-01164e201533
updated: 1486069673
title: Introduction to Protein Synthesis
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/suN-sV0cT6c/default.jpg
    - https://i3.ytimg.com/vi/suN-sV0cT6c/1.jpg
    - https://i3.ytimg.com/vi/suN-sV0cT6c/2.jpg
    - https://i3.ytimg.com/vi/suN-sV0cT6c/3.jpg
---
