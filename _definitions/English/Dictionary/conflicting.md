---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conflicting
offline_file: ""
offline_thumbnail: ""
uuid: dd334de6-9ab1-4109-b6f3-fa7f0f8f653a
updated: 1484310441
title: conflicting
categories:
    - Dictionary
---
conflicting
     adj 1: in disagreement; "the figures are at odds with our
            findings"; "contradictory attributes of unjust justice
            and loving vindictiveness"- John Morley [syn: {at
            odds(p)}, {contradictory}, {self-contradictory}]
     2: on bad terms; "they were usually at odds over politics";
        "conflicting opinions" [syn: {at odds(p)}]
