---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/journalism
offline_file: ""
offline_thumbnail: ""
uuid: bb71a68e-cca8-4df5-9ace-8bc1018ab7ad
updated: 1484310569
title: journalism
categories:
    - Dictionary
---
journalism
     n 1: newspapers and magazines collectively [syn: {news media}, {fourth
          estate}]
     2: the profession of reporting or photographing or editing news
        stories for one of the media
