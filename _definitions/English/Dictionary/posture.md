---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/posture
offline_file: ""
offline_thumbnail: ""
uuid: c9c50861-c2d4-484f-9daf-9df09acc5c8e
updated: 1484310178
title: posture
categories:
    - Dictionary
---
posture
     n 1: position or arrangement of the body and its limbs; "he
          assumed an attitude of surrender" [syn: {position}, {attitude}]
     2: characteristic way of bearing one's body; "stood with good
        posture" [syn: {carriage}, {bearing}]
     3: a rationalized mental attitude [syn: {position}, {stance}]
     4: capability in terms of personnel and materiel that affect
        the capacity to fight a war; "we faced an army of great
        strength"; "politicians have neglected our military
        posture" [syn: {military capability}, {military strength},
         {strength}, {military posture}]
     v 1: behave affectedly or unnaturally in order to impress others;
    ...
