---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tragic
offline_file: ""
offline_thumbnail: ""
uuid: c2f6058d-e49a-4838-848c-dbb6822931a2
updated: 1484310587
title: tragic
categories:
    - Dictionary
---
tragic
     adj 1: very sad; especially involving grief or death or
            destruction; "a tragic face"; "a tragic plight"; "a
            tragic accident" [syn: {tragical}]
     2: of or relating to or characteristic of tragedy; "tragic
        hero"
