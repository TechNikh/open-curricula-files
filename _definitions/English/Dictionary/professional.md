---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/professional
offline_file: ""
offline_thumbnail: ""
uuid: 9dfa8164-15d3-4c2e-b150-ffd96953178b
updated: 1484310477
title: professional
categories:
    - Dictionary
---
professional
     adj 1: engaged in a profession or engaging in as a profession or
            means of livelihood; "the professional man or woman
            possesses distinctive qualifications"; "began her
            professional career after the Olympics"; "professional
            theater"; "professional football"; "a professional
            cook"; "professional actors and athletes" [ant: {nonprofessional}]
     2: of or relating to or suitable as a profession; "professional
        organizations"; "a professional field such as law"
     3: characteristic of or befitting a profession or one engaged
        in a profession; "professional conduct"; "professional
        ethics"; "a ...
