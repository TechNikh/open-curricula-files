---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mountain_climbing_problem
offline_file: ""
offline_thumbnail: ""
uuid: cbd241b7-3201-451e-ab1c-9d9a2710c22b
updated: 1484308614
title: Mountain climbing problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Mountain_climbing_problem.gif
tags:
    - Understanding the problem
    - Formulation
    - Proof in the piecewise linear case
    - Notes
categories:
    - Discrete geometry
---
In mathematics, the mountain climbing problem is a problem of finding the conditions that two function forming profiles of a two-dimensional mountain must satisfy, so that two climbers can start on the bottom on the opposite sides of the mountain and coordinate their movements to reach to the top while always staying at the same height. This problem was named and posed in this form by James V. Whittaker (1966), but its history goes back to Tatsuo Homma (1952), who solved a version of it. The problem has been repeatedly rediscovered and solved independently in different context by a number of people (see the references).
