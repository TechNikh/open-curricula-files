---
version: 1
type: video
provider: YouTube
id: uovYxBtWsKM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20Capacitance%20Animation%20Lesson%204.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1c404b51-ed32-4b6a-8ef0-5f33f8c8a966
updated: 1486069654
title: What is Capacitance Animation Lesson 4
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/uovYxBtWsKM/default.jpg
    - https://i3.ytimg.com/vi/uovYxBtWsKM/1.jpg
    - https://i3.ytimg.com/vi/uovYxBtWsKM/2.jpg
    - https://i3.ytimg.com/vi/uovYxBtWsKM/3.jpg
---
