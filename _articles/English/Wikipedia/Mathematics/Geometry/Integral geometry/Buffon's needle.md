---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Buffon%27s_needle'
offline_file: ""
offline_thumbnail: ""
uuid: 7ffa8f70-842b-4f93-8784-202de87f9571
updated: 1484309255
title: "Buffon's needle"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Buffon_needle.svg.png
tags:
    - Solution
    - 'Case 1: Short needle'
    - 'Case 2: Long needle'
    - Using elementary calculus
    - Estimating π
categories:
    - Integral geometry
---
Buffon's needle was the earliest problem in geometric probability to be solved; it can be solved using integral geometry. The solution, in the case where the needle length is not greater than the width of the strips, can be used to design a Monte Carlo method for approximating the number π, although that was not the original motivation for de Buffon's question.[2]
