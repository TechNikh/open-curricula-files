---
version: 1
type: video
provider: YouTube
id: hJtaY-iZRvU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/Parentheses.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0c567ea3-6ce0-47cb-a342-235d964928c1
updated: 1486069599
title: Parentheses
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Punctuation
thumbnail_urls:
    - https://i3.ytimg.com/vi/hJtaY-iZRvU/default.jpg
    - https://i3.ytimg.com/vi/hJtaY-iZRvU/1.jpg
    - https://i3.ytimg.com/vi/hJtaY-iZRvU/2.jpg
    - https://i3.ytimg.com/vi/hJtaY-iZRvU/3.jpg
---
Parentheses set off extra information (such as a writer's remarks, an interruption, or a reference) from the rest of a sentence. Learn how to use them in this video!

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/miscellaneous-punctuation/e/introduction-to-the-parenthesis

Watch the next lesson: https://www.khanacademy.org/miscellaneous-punctuation/v/dashes

Missed the previous lesson? Watch here: https://www.khanacademy.org/miscellaneous-punctuation/v/quotation-marks/

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
