---
version: 1
type: video
provider: YouTube
id: sZakNWRgw64
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/The%20Dynamics%20of%20Thrombin%20and%20Platelet%20Activation%20in%20Thrombosis%20and%20Inflammation%20on%20Vimeo.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6932f245-2195-47fd-8f93-f623b3678224
updated: 1486069668
title: >
    The Dynamics of Thrombin and Platelet Activation in
    Thrombosis and Inflammation on Vimeo
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/sZakNWRgw64/default.jpg
    - https://i3.ytimg.com/vi/sZakNWRgw64/1.jpg
    - https://i3.ytimg.com/vi/sZakNWRgw64/2.jpg
    - https://i3.ytimg.com/vi/sZakNWRgw64/3.jpg
---
