---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grama
offline_file: ""
offline_thumbnail: ""
uuid: 38bb4dae-e496-4745-9626-32c95ae69337
updated: 1484310464
title: grama
categories:
    - Dictionary
---
grama
     n : pasture grass of plains of South America and western North
         America [syn: {grama grass}, {gramma}, {gramma grass}]
