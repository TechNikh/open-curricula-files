---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/profess
offline_file: ""
offline_thumbnail: ""
uuid: 29f4e194-c1ca-470a-b4ee-bcc13807be33
updated: 1484310579
title: profess
categories:
    - Dictionary
---
profess
     v 1: practice as a profession, teach, or claim to be
          knowledgeable about; "She professes organic chemistry"
     2: confess one's faith in, or allegiance to; "The terrorists
        professed allegiance to the Muslim faith"; "he professes
        to be a Communist"
     3: admit, make a clean breast of; "She confessed that she had
        taken the money" [syn: {concede}, {confess}]
     4: state freely; "The teacher professed that he was not
        generous when it came to giving good grades"
     5: receive into a religious order or congregation
     6: take vows, as in religious order; "she professed herself as
        a nun"
     7: state insincerely; "He ...
