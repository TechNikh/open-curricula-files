---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Group_testing
offline_file: ""
offline_thumbnail: ""
uuid: 1a749158-6349-4dce-a906-45f6d1ddc77d
updated: 1484308565
title: Group testing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-False_Coin_Problem.gif
tags:
    - Background
    - Formalization of the problem
    - Formal notion of a test
    - Goal
    - Definition
    - Testing methods
    - Definition
    - Mathematical representation of the set of non-adaptive tests
    - |
        Bounds for testing on
        
        
        
        
        t
        
        a
        
        
        (
        d
        ,
        n
        )
        
        
        {\displaystyle t^{a}(d,n)}
        
        and
        
        
        
        t
        (
        d
    - |
        Lower bound on
        
        
        
        
        t
        
        a
        
        
        (
        d
        ,
        n
        )
        
        
        {\displaystyle t^{a}(d,n)}
    - |
        Upper bound on
        
        
        
        
        t
        
        a
        
        
        (
        d
        ,
        n
        )
        
        
        {\displaystyle t^{a}(d,n)}
    - |
        Upper bound on
        
        
        
        t
        (
        1
        ,
        n
        )
        
        
        {\displaystyle t(1,n)}
    - Upper bounds for non-adaptive group testing
    - Notes
categories:
    - Combinatorics
---
In combinatorial mathematics, group testing refers to any procedure which breaks up the task of locating elements of a set which have certain properties into tests on subsets ("groups") rather than on individual elements. A familiar example of this type of technique is the false coin problem of recreational mathematics. In this problem there are n coins and one of them is false, weighing less than a real coin. The objective is to find the false coin, using a balance scale, in the fewest number of weighings. By repeatedly dividing the coins in half and comparing the two halves, the false coin can be found quickly as it is always in the lighter half.[1]
