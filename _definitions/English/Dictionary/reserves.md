---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reserves
offline_file: ""
offline_thumbnail: ""
uuid: 674d88b9-3557-422e-b1eb-62127c3f7661
updated: 1484310240
title: reserves
categories:
    - Dictionary
---
reserves
     n : civilians trained as soldiers but not part of the regular
         army [syn: {militia}]
