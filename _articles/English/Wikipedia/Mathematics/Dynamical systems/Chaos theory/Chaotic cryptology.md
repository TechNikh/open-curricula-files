---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaotic_cryptology
offline_file: ""
offline_thumbnail: ""
uuid: 1e0eb41a-2803-463b-b475-63a73cca1091
updated: 1484308699
title: Chaotic cryptology
categories:
    - Chaos theory
---
Chaotic cryptography is the application of the mathematical chaos theory to the practice of the cryptography, the study or techniques used to privately and securely transmit information with the presence of a third-party or adversary. The use of chaos or randomness in cryptography has long been sought after by entities wanting a new way to encrypt messages. However, because of the lack of thorough, provable security properties and low acceptable performance, chaotic cryptography has encountered setbacks.
