---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oxidation
offline_file: ""
offline_thumbnail: ""
uuid: f5bb665a-b7e3-415e-9b7f-2e785e3777d8
updated: 1484310361
title: oxidation
categories:
    - Dictionary
---
oxidation
     n : the process of oxidizing; the addition of oxygen to a
         compound with a loss of electrons; always occurs
         accompanied by reduction [syn: {oxidization}, {oxidisation}]
