---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diatomic_molecule
offline_file: ""
offline_thumbnail: ""
uuid: 27a7412e-4b4d-4e70-b815-6b9407d6de6d
updated: 1484309277
title: Diatomic molecule
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nitrogen-3D-vdW.png
tags:
    - Heteronuclear molecules
    - Occurrence
    - Molecular geometry
    - Historical significance
    - Excited electronic states
    - Energy levels
    - Translational energies
    - Rotational energies
    - Vibrational energies
    - >
        Comparison between rotational and vibrational energy
        spacings
    - "Hund's cases"
categories:
    - Molecular geometry
---
Diatomic molecules are molecules composed of only two atoms, of the same or different chemical elements. The prefix di- is of Greek origin, meaning "two". If a diatomic molecule consists of two atoms of the same element, such as hydrogen (H2) or oxygen (O2), then it is said to be homonuclear. Otherwise, if a diatomic molecule consists of two different atoms, such as carbon monoxide (CO) or nitric oxide (NO), the molecule is said to be heteronuclear.
