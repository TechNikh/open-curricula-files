---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Harold_W._Kuhn
offline_file: ""
offline_thumbnail: ""
uuid: 98664c4b-7a2e-4b1f-9491-084c2108526c
updated: 1484308513
title: Harold W. Kuhn
tags:
    - life
    - Bibliography
categories:
    - Game theorists
---
Harold William Kuhn (July 29, 1925 – July 2, 2014) was an American mathematician who studied game theory. He won the 1980 John von Neumann Theory Prize along with David Gale and Albert W. Tucker. A former Professor Emeritus of Mathematics at Princeton University, he is known for the Karush–Kuhn–Tucker conditions, for Kuhn's theorem, for developing Kuhn poker as well as the description of the Hungarian method for the assignment problem. Recently, though, a paper by Carl Gustav Jacobi, published posthumously in 1890 in Latin, has been discovered that anticipates by many decades the Hungarian algorithm.[1][2]
