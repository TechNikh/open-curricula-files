---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rider
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484484181
title: rider
categories:
    - Dictionary
---
rider
     n 1: a traveler who actively rides an animal (as a horse or
          camel)
     2: a clause that is appended to a legislative bill
     3: a traveler who actively rides a vehicle (as a bicycle or
        motorcycle)
     4: a traveler riding in a vehicle (a boat or bus or car or
        plane or train etc) who is not operating it [syn: {passenger}]
