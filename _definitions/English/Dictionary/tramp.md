---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tramp
offline_file: ""
offline_thumbnail: ""
uuid: 9d0995b4-d9de-41ff-af2a-22408de0760d
updated: 1484310144
title: tramp
categories:
    - Dictionary
---
tramp
     n 1: a disreputable vagrant; "a homeless tramp"; "he tried to
          help the really down-and-out bums" [syn: {hobo}, {bum}]
     2: a person who engages freely in promiscuous sex [syn: {swinger}]
     3: a foot traveler; someone who goes on an extended walk (for
        pleasure) [syn: {hiker}, {tramper}]
     4: a heavy footfall; "the tramp of military boots"
     5: a commercial steamer for hire; one having no regular
        schedule [syn: {tramp steamer}]
     6: a long walk usually for exercise or pleasure [syn: {hike}]
     v 1: travel on on foot, especially on a walking expedition; "We
          went tramping about the state of Colorado"
     2: walk heavily and ...
