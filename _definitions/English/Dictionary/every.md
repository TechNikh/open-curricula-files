---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/every
offline_file: ""
offline_thumbnail: ""
uuid: f088932d-0291-4d62-86d7-c89d12bedc02
updated: 1484310359
title: every
categories:
    - Dictionary
---
every
     adj 1: each and all of a series of entities or intervals as
            specified; "every third seat"; "every two hours" [syn:
             {every(a)}]
     2: (used of count nouns) each and all of the members of a group
        considered singly and without exception; "every person is
        mortal"; "every party is welcome"; "had every hope of
        success"; "every chance of winning" [syn: {every(a)}]
