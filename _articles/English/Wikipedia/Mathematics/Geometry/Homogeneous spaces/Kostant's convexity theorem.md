---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kostant%27s_convexity_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 2953fae6-5b94-48ca-8f6b-64f7d3e6c0ff
updated: 1484309247
title: "Kostant's convexity theorem"
tags:
    - Compact Lie groups
    - Symmetric spaces
    - Proof for a compact Lie group
    - Other proofs
    - Notes
categories:
    - Homogeneous spaces
---
In mathematics, Kostant's convexity theorem, introduced by Bertram Kostant (1973), states that the projection of every coadjoint orbit of a connected compact Lie group into the dual of a Cartan subalgebra is a convex set. It is a special case of a more general result for symmetric spaces. Kostant's theorem is a generalization of a result of Schur (1923), Horn (1954) and Thompson (1972) for hermitian matrices. They proved that the projection onto the diagonal matrices of the space of all n by n complex self-adjoint matrices with given eigenvalues Λ = (λ1, ..., λn) is the convex polytope with vertices all permutations of the coordinates of Λ. In fact this result is 'Kostant's linear ...
