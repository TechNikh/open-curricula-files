---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inspire
offline_file: ""
offline_thumbnail: ""
uuid: 12c6124e-e8e8-4753-9df8-183f07cad605
updated: 1484310579
title: inspire
categories:
    - Dictionary
---
inspire
     v 1: heighten or intensify; "These paintings exalt the
          imagination" [syn: {animate}, {invigorate}, {enliven}, {exalt}]
     2: supply the inspiration for; "The article about the artist
        inspired the exhibition of his recent work"
     3: serve as the inciting cause of; "She prompted me to call my
        relatives" [syn: {prompt}, {instigate}]
     4: urge on or encourage especially by shouts; "The crowd
        cheered the demonstrating strikers" [syn: {cheer}, {urge},
         {barrack}, {urge on}, {exhort}, {pep up}]
     5: fill with revolutionary ideas [syn: {revolutionize}, {revolutionise}]
     6: draw in (air); "Inhale deeply"; "inhale the fresh ...
