---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalized_trigonometry
offline_file: ""
offline_thumbnail: ""
uuid: efa97123-c0a1-44df-ad5a-875f72fd754f
updated: 1484309343
title: Generalized trigonometry
tags:
    - Trigonometry
    - Higher-dimensions
    - Trigonometric functions
    - Other
categories:
    - Trigonometry
---
Ordinary trigonometry studies triangles in the Euclidean plane R2. There are a number of ways of defining the ordinary Euclidean geometric trigonometric functions on real numbers: right-angled triangle definitions, unit-circle definitions, series definitions, definitions via differential equations, definitions using functional equations. Generalizations of trigonometric functions are often developed by starting with one of the above methods and adapting it to a situation other than the real numbers of Euclidean geometry. Generally, trigonometry can be the study of triples of points in any kind of geometry or space. A triangle is the polygon with the smallest number of vertices, so one ...
