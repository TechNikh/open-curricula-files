---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maternity
offline_file: ""
offline_thumbnail: ""
uuid: c0f1ee18-69ac-4ae3-9cc2-092e59ee6a71
updated: 1484310515
title: maternity
categories:
    - Dictionary
---
maternity
     n 1: the state of being pregnant; the period from conception to
          birth when a woman carries a developing fetus in her
          uterus [syn: {pregnancy}, {gestation}]
     2: the kinship relation between an offspring and the mother
        [syn: {motherhood}]
     3: the quality of having or showing the tenderness and warmth
        and affection of or befitting a mother; "the girl's
        motherliness made her invaluable in caring for the
        children" [syn: {motherliness}, {maternalism}, {maternal
        quality}]
