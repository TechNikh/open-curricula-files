---
version: 1
type: video
provider: YouTube
id: GOrWy_yNBvY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 42898505-e0b7-4e73-9c85-17a1b49fa662
updated: 1486069656
title: 'Thermodynamics and the End of the Universe-  Energy, Entropy, and the fundamental laws of physics.'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/GOrWy_yNBvY/default.jpg
    - https://i3.ytimg.com/vi/GOrWy_yNBvY/1.jpg
    - https://i3.ytimg.com/vi/GOrWy_yNBvY/2.jpg
    - https://i3.ytimg.com/vi/GOrWy_yNBvY/3.jpg
---
