---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/timid
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484416021
title: timid
categories:
    - Dictionary
---
timid
     adj 1: showing fear and lack of confidence [ant: {bold}]
     2: contemptibly timid
     3: lacking self-confidence; "stood in the doorway diffident and
        abashed"; "problems that call for bold not timid
        responses"; "a very unsure young man" [syn: {diffident}, {shy},
         {unsure}]
     4: lacking conviction or boldness or courage; "faint heart
        ne'er won fair lady" [syn: {faint}, {fainthearted}]
     n : people who are fearful and cautious; "whitewater rafting is
         not for the timid" [syn: {cautious}] [ant: {brave}]
