---
version: 1
type: video
provider: YouTube
id: rSadG6EtJmY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Rational%20expressions/Direct%20variation%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 80d81624-3849-496c-9e7e-de5d98761643
updated: 1486069567
title: Direct variation 1
categories:
    - Rational expressions
thumbnail_urls:
    - https://i3.ytimg.com/vi/rSadG6EtJmY/default.jpg
    - https://i3.ytimg.com/vi/rSadG6EtJmY/1.jpg
    - https://i3.ytimg.com/vi/rSadG6EtJmY/2.jpg
    - https://i3.ytimg.com/vi/rSadG6EtJmY/3.jpg
---
