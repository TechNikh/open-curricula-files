---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Recurrence_period_density_entropy
offline_file: ""
offline_thumbnail: ""
uuid: a2076bda-750b-47ad-ba44-d3dc1490fd63
updated: 1484308771
title: Recurrence period density entropy
tags:
    - Overview
    - Method description
    - RPDE in practice
categories:
    - Entropy
---
Recurrence period density entropy (RPDE) is a method, in the fields of dynamical systems, stochastic processes, and time series analysis, for determining the periodicity, or repetitiveness of a signal.
