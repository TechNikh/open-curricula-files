---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/owed
offline_file: ""
offline_thumbnail: ""
uuid: 6ea9ba06-a7c4-4a3d-ad2b-29fa4de2b05a
updated: 1484310575
title: owed
categories:
    - Dictionary
---
owed
     adj 1: owed and payable immediately or on demand; "payment is due"
            [syn: {due}] [ant: {undue}]
     2: owed as a debt; "outstanding bills"; "the amount still
        owed"; "undischarged debts" [syn: {outstanding}, {owing(p)},
         {undischarged}]
