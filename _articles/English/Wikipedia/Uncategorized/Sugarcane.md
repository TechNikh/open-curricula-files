---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sugarcane
offline_file: ""
offline_thumbnail: ""
uuid: 0fd00e42-44c2-4610-837d-f267805bfec2
updated: 1484308435
title: Sugarcane
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Saccharum_officinarum_-_K%25C3%25B6hler%25E2%2580%2593s_Medizinal-Pflanzen-125.jpg'
tags:
    - Description
    - History
    - Cultivation
    - Pests
    - Pathogens
    - Nitrogen fixation
    - Conditions for sugarcane workers
    - Processing
    - Milling
    - Refining
    - Ribbon cane syrup
    - Pollution from sugarcane processing
    - Production
    - Cane ethanol
    - Bagasse applications
    - Electricity production
    - Biogas production
    - Sugarcane as food
categories:
    - Uncategorized
---
Sugarcane, or sugar cane, is one (Saccharum officinarum) of the several species of tall perennial true grasses of the genus Saccharum, tribe Andropogoneae, native to the warm temperate to tropical regions of South Asia and Melanesia, and used for sugar production. It has stout, jointed, fibrous stalks that are rich in the sugar sucrose, which accumulates in the stalk internodes. The plant is 2 to 6 m (6 ft 7 in to 19 ft 8 in) tall. All sugar cane species interbreed and the major commercial cultivars are complex hybrids. Sugarcane belongs to the grass family Poaceae, an economically important seed plant family that includes maize, wheat, rice, and sorghum, and many forage crops.
