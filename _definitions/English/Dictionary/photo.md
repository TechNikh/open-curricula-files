---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/photo
offline_file: ""
offline_thumbnail: ""
uuid: 1cea42b3-c136-4d59-b433-62eaa3e0f4d7
updated: 1484310210
title: photo
categories:
    - Dictionary
---
photo
     n : a picture of a person or scene in the form of a print or
         transparent slide; recorded by a camera on
         light-sensitive material [syn: {photograph}, {exposure},
         {pic}]
