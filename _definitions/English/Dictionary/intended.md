---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intended
offline_file: ""
offline_thumbnail: ""
uuid: 38be2647-18c5-43b0-b0a9-3372d91f45dd
updated: 1484310607
title: intended
categories:
    - Dictionary
---
intended
     adj 1: intentional or planned; "your intended trip abroud"; "an
            intended insult" [ant: {unintended}]
     2: future; betrothed; "his intended bride"
