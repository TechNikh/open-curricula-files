---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tint
offline_file: ""
offline_thumbnail: ""
uuid: 3a9dd568-55ca-4402-9a6e-63ab71143fd8
updated: 1484310305
title: tint
categories:
    - Dictionary
---
tint
     n : a quality of a given color that differs slightly from a
         primary color; "after several trials he mixed the shade
         of pink that she wanted" [syn: {shade}, {tincture}, {tone}]
     v : dye with a color [syn: {tinct}, {bepaint}, {tinge}, {touch}]
