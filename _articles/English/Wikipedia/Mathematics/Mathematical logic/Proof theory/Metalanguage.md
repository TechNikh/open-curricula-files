---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metalanguage
offline_file: ""
offline_thumbnail: ""
uuid: 97d64c49-2f82-4e06-935c-aad4c4d68dae
updated: 1484309495
title: Metalanguage
tags:
    - Types
    - Embedded
    - Ordered
    - Nested
    - In natural language
    - Types of expressions
    - Deductive systems
    - Metavariables
    - Metatheories and metatheorems
    - Interpretations
    - Role in metaphor
    - Metaprogramming
    - Dictionaries
categories:
    - Proof theory
---
Broadly, any metalanguage is language or symbols used when language itself is being discussed or examined.[1] In logic and linguistics, a metalanguage is a language used to make statements about statements in another language (the object language). Expressions in a metalanguage are often distinguished from those in an object language by the use of italics, quotation marks, or writing on a separate line. The structure of sentences and phrases in a metalanguage can be described by a metasyntax.
