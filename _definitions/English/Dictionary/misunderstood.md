---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/misunderstood
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484476021
title: misunderstood
categories:
    - Dictionary
---
misunderstood
     adj 1: wrongly understood; "a misunderstood criticism"; "a
            misunderstood question"
     2: not given sympathetic understanding; "a sorely misunderstood
        child"
