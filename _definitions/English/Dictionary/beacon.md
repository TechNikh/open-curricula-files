---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beacon
offline_file: ""
offline_thumbnail: ""
uuid: 0e7a28eb-2b62-435c-85ce-e85cfe9cf2bd
updated: 1484310172
title: beacon
categories:
    - Dictionary
---
beacon
     n 1: a fire (usually on a hill or tower) that can be seen from a
          distance [syn: {beacon fire}]
     2: a radio station that broadcasts a directional signal for
        navigational purposes [syn: {radio beacon}]
     3: a tower with a light that gives warning of shoals to passing
        ships [syn: {lighthouse}, {beacon light}, {pharos}]
     v 1: shine like a beacon
     2: guide with a beacon
