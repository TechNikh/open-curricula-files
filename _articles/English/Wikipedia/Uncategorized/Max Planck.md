---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Planck
offline_file: ""
offline_thumbnail: ""
uuid: e1595ef1-aaa7-462c-a256-a4b257888070
updated: 1484308309
title: Max Planck
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Max_Planck_Wirkungsquantums_20050815.jpg
tags:
    - Early life and career
    - Academic career
    - family
    - Professor at Berlin University
    - Black-body radiation
    - Einstein and the theory of relativity
    - World War I
    - Post-war and Weimar Republic
    - Quantum mechanics
    - Nazi dictatorship and World War II
    - Religious views
    - Publications
    - Sources
categories:
    - Uncategorized
---
Max Karl Ernst Ludwig Planck, FRS[3] (/plɑːŋk/;[4] 23 April 1858 – 4 October 1947) was a German theoretical physicist whose work on quantum theory won him the Nobel Prize in Physics in 1918.[5]
