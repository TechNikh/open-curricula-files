---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/viceroy
offline_file: ""
offline_thumbnail: ""
uuid: 84cd3e17-3efd-4ae4-a5cb-5af04bc9b728
updated: 1484310585
title: viceroy
categories:
    - Dictionary
---
viceroy
     n 1: governor of a country or province who rules as the
          representative of his or her king or sovereign [syn: {vicereine}]
     2: showy American butterfly resembling the monarch but smaller
        [syn: {Limenitis archippus}]
