---
version: 1
type: video
provider: YouTube
id: 6KmeGpjeLZ0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Algebra%20II/Logarithms/Vi%20and%20Sal%20talk%20about%20the%20mysteries%20of%20Benford%27s%20law.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a187dbea-82ff-4e80-8d18-00e3dd336cee
updated: 1486069696
title: "Vi and Sal talk about the mysteries of Benford's law"
categories:
    - Logarithms
thumbnail_urls:
    - https://i3.ytimg.com/vi/6KmeGpjeLZ0/default.jpg
    - https://i3.ytimg.com/vi/6KmeGpjeLZ0/1.jpg
    - https://i3.ytimg.com/vi/6KmeGpjeLZ0/2.jpg
    - https://i3.ytimg.com/vi/6KmeGpjeLZ0/3.jpg
---
