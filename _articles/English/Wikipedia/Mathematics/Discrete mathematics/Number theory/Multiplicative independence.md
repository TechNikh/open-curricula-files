---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiplicative_independence
offline_file: ""
offline_thumbnail: ""
uuid: 54603b4a-8c3c-46dc-96a1-3ede1ed5a078
updated: 1484308654
title: Multiplicative independence
categories:
    - Number theory
---
In number theory, two positive integers a and b are said to be multiplicatively independent[1] if their only common integer power is 1. That is, for all integer n and m, 
  
    
      
        
          a
          
            n
          
        
        =
        
          b
          
            m
          
        
      
    
    {\displaystyle a^{n}=b^{m}}
  
 implies 
  
    
      
        n
        =
        m
        =
        0
      
    
    {\displaystyle n=m=0}
  
. Two integers which are not multiplicatively independent are said to be multiplicatively dependent.
