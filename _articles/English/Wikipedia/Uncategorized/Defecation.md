---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Defaecation
offline_file: ""
offline_thumbnail: ""
uuid: d6e1ac53-7ba7-4b7b-afb1-037f31b25e9b
updated: 1484308340
title: Defecation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Anorectum.gif
tags:
    - Description
    - Physiology
    - Voluntary and involuntary control
    - Anal cleansing after defecation
    - Posture
    - Society and culture
    - Mythology and tradition
categories:
    - Uncategorized
---
Defecation is the final act of digestion, by which organisms eliminate solid, semisolid, and/or liquid waste material from the digestive tract via the anus.
