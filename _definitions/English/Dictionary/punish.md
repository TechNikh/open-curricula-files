---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/punish
offline_file: ""
offline_thumbnail: ""
uuid: c232d88a-46d8-4cea-a20f-ce7f6efa25e4
updated: 1484310555
title: punish
categories:
    - Dictionary
---
punish
     v : impose a penalty on; inflict punishment on; "The students
         were penalized for showing up late for class"; "we had to
         punish the dog for soiling the floor again" [syn: {penalize},
          {penalise}]
