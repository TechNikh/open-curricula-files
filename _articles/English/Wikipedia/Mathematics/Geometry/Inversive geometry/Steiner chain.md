---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Steiner_chain
offline_file: ""
offline_thumbnail: ""
uuid: db93acd1-a484-43bf-9e10-67c485c3a2d6
updated: 1484309258
title: Steiner chain
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Steiner_chain_12mer.svg.png
tags:
    - Definitions and types of tangency
    - Closed, open and multi-cyclic
    - Annular case and feasibility criterion
    - Properties under inversion
    - Infinite family
    - Elliptical/hyperbolic locus of centers
    - Conjugate chains
    - Generalizations
    - Bibliography
categories:
    - Inversive geometry
---
In geometry, a Steiner chain is a set of n circles, all of which are tangent to two given non-intersecting circles (blue and red in Figure 1), where n is finite and each circle in the chain is tangent to the previous and next circles in the chain. In the usual closed Steiner chains, the first and last (nth) circles are also tangent to each other; by contrast, in open Steiner chains, they need not be. The given circles α and β do not intersect, but otherwise are unconstrained; the smaller circle may lie completely inside or outside of the larger circle. In these cases, the centers of Steiner-chain circles lie on an ellipse or a hyperbola, respectively.
