---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fock%E2%80%93Lorentz_symmetry'
offline_file: ""
offline_thumbnail: ""
uuid: 6c5c8588-b9c8-4781-8787-40b8ea569fce
updated: 1484309309
title: Fock–Lorentz symmetry
categories:
    - Symmetry
---
Lorentz invariance follows from two independent postulates: the principle of relativity and the principle of constancy of the speed of light. Dropping the latter while keeping the former leads to a new invariance, known as Fock–Lorentz symmetry[1] or the projective Lorentz transformation.[2][3] The general study of such theories began with Fock,[4] who was motivated by the search for the general symmetry group preserving relativity without assuming the constancy of c.
