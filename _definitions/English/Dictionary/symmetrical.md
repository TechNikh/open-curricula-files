---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/symmetrical
offline_file: ""
offline_thumbnail: ""
uuid: 618eea98-b4d7-445b-9433-6dc45f75d863
updated: 1484310206
title: symmetrical
categories:
    - Dictionary
---
symmetrical
     adj 1: having similarity in size, shape, and relative position of
            corresponding parts [syn: {symmetric}] [ant: {asymmetrical}]
     2: exhibiting equivalence or correspondence among constituents
        of an entity or between different entities [syn: {harmonious},
         {proportionate}]
