---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hike
offline_file: ""
offline_thumbnail: ""
uuid: 7ef26191-e05b-415d-81ea-5fc44b11dc73
updated: 1484310146
title: hike
categories:
    - Dictionary
---
hike
     n 1: a long walk usually for exercise or pleasure [syn: {tramp}]
     2: an increase in cost; "they asked for a 10% rise in rates"
        [syn: {rise}, {boost}, {cost increase}]
     3: the amount a salary is increased; "he got a 3% raise"; "he
        got a wage hike" [syn: {raise}, {rise}, {wage hike}, {wage
        increase}, {salary increase}]
     v 1: increase; "The landlord hiked up the rents" [syn: {hike up},
           {boost}]
     2: walk a long way, as for pleasure or physical exercise; "We
        were hiking in Colorado"; "hike the Rockies"
