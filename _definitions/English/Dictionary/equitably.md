---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equitably
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484600341
title: equitably
categories:
    - Dictionary
---
equitably
     adv : in an equitable manner; "the inheritance was equitably
           divided among the sisters" [ant: {inequitably}]
