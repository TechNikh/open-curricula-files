---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-oscillation
offline_file: ""
offline_thumbnail: ""
uuid: 7bb06f78-7c35-47a2-aadb-95873fb03a11
updated: 1484308808
title: Self-oscillation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Self_excited_oscillation.svg.png
tags:
    - Mathematical basis
    - Examples in engineering
    - Railway and automotive wheels
    - Central heating thermostats
    - Automatic transmissions
    - Steering of vehicles when course corrections are delayed
    - SEIG (self-excited induction generator)
    - Self-exciting transmitters
    - Examples in other fields
    - Population cycles in biology
categories:
    - Nonlinear systems
---
Self-oscillation is the generation and maintenance of a periodic motion by a source of power that lacks any corresponding periodicity. The oscillator itself controls the phase with which the external power acts on it. Self-oscillators are therefore distinct from forced and parametric resonators, in which the power that sustains the motion must be modulated externally. In linear systems, self-oscillation appears as an instability associated with a negative damping term, which causes small perturbations to grow exponentially in amplitude. This negative damping is due to a positive feedback between the oscillation and the modulation of the external source of power. The amplitude and waveform ...
