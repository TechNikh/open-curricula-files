---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hospital
offline_file: ""
offline_thumbnail: ""
uuid: 44d02208-14d9-484f-a079-9c56c1d8945b
updated: 1484310445
title: hospital
categories:
    - Dictionary
---
hospital
     n 1: a health facility where patients receive treatment [syn: {infirmary}]
     2: a medical institution where sick or injured people are given
        medical or surgical care
