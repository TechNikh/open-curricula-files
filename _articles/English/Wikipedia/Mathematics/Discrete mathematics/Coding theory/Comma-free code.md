---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Comma-free_code
offline_file: ""
offline_thumbnail: ""
uuid: 5b9f41a0-e8c6-4c80-9043-51229617139c
updated: 1484308545
title: Comma-free code
categories:
    - Coding theory
---
Comma-free codes are also known as self-synchronizing block codes[2] because no synchronization is required to find the beginning of a code word.
