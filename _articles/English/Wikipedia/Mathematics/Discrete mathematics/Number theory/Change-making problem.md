---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Change-making_problem
offline_file: ""
offline_thumbnail: ""
uuid: 5e2b881d-c1ec-43d4-ab01-4601c9d20354
updated: 1484308635
title: Change-making problem
tags:
    - Mathematical definition
    - Non currency examples
    - Methods of solving
    - Simple dynamic programming
    - Optimal Substructure
    - Implementation
    - Dynamic programming with the probabilistic convolution tree
    - Linear programming
    - Greedy method
    - Related problems
categories:
    - Number theory
---
The change-making problem addresses the following question: how can a given amount of money be made with the least number of coins of given denominations? It is a knapsack type problem, and has applications wider than just currency.
