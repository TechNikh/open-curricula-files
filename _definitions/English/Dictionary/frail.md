---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frail
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484522641
title: frail
categories:
    - Dictionary
---
frail
     adj 1: physically weak; "an invalid's frail body" [ant: {robust}]
     2: having the attributes of man as opposed to e.g. divine
        beings; "I'm only human"; "frail humanity" [syn: {fallible},
         {imperfect}, {weak}]
     3: easily broken or damaged or destroyed; "a kite too delicate
        to fly safely"; "fragile porcelain plates"; "fragile old
        bones"; "a frail craft" [syn: {delicate}, {fragile}]
     n 1: the weight of a frail (basket) full of raisins or figs;
          between 50 and 75 pounds
     2: a basket for holding dried fruit (especially raisins or
        figs)
