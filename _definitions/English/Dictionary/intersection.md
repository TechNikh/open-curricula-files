---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intersection
offline_file: ""
offline_thumbnail: ""
uuid: 30f9aa60-5ece-48d8-b4c4-dcd7f4678b44
updated: 1484310218
title: intersection
categories:
    - Dictionary
---
intersection
     n 1: a point where lines intersect [syn: {intersection point}, {point
          of intersection}]
     2: a junction where one street or road crosses another [syn: {crossroad},
         {crossway}, {crossing}, {carrefour}]
     3: a point or set of points common to two or more geometric
        configurations
     4: the set of elements common to two or more sets; "the set of
        red hats is the intersection of the set of hats and the
        set of red things" [syn: {product}, {cartesian product}]
     5: a representation of common ground between theories or
        phenomena; "there was no overlap between their proposals"
        [syn: {overlap}, {convergence}]
     ...
