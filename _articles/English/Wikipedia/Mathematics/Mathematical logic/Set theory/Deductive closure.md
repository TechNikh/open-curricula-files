---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deductive_closure
offline_file: ""
offline_thumbnail: ""
uuid: 00927aa3-2263-4971-a5b4-84cd9ca3fa48
updated: 1484309504
title: Deductive closure
categories:
    - Set theory
---
Deductive closure is a property of a set of objects (usually the objects in question are statements). A set of objects, O, is said to exhibit closure or to be closed under a given operation, R, provided that for every object, x, if x is a member of O and x is R-related to any object, y, then y is a member of O.[1] In the context of statements, a deductive closure is the set of all the statements that can be deduced from a given set of statements.
