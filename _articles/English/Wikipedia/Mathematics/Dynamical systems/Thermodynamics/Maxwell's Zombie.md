---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Maxwell%27s_Zombie'
offline_file: ""
offline_thumbnail: ""
uuid: d8720618-2813-41bb-a75f-3943eba34f80
updated: 1484308849
title: "Maxwell's Zombie"
categories:
    - Thermodynamics
---
Maxwell Zombies (MZ) are a class of thermodynamic paradoxes closely related to the famous 19th-century thought experiment, Maxwell's Demon (MD),[1][2] which tests the foundations of the second law of thermodynamics. MZs differ from MDs insofar as they are not prone to the same physical shortcomings that ultimately foil the demons.
