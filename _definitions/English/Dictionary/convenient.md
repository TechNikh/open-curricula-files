---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convenient
offline_file: ""
offline_thumbnail: ""
uuid: 7a8a08d3-4a92-4f6f-9173-0cf85dc2e8e8
updated: 1484310335
title: convenient
categories:
    - Dictionary
---
convenient
     adj 1: suited to your comfort or purpose or needs; "a convenient
            excuse for not going" [ant: {inconvenient}]
     2: easy to reach; "found a handy spot for the can opener" [syn:
         {handy}, {ready to hand(p)}]
     3: large and roomy (`convenient' is archaic in this sense); "a
        commodious harbor"; "a commodious building suitable for
        conventions" [syn: {commodious}] [ant: {incommodious}]
