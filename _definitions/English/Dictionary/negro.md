---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/negro
offline_file: ""
offline_thumbnail: ""
uuid: 7c602218-bedd-4774-ad0b-1467eca931bc
updated: 1484310172
title: negro
categories:
    - Dictionary
---
negro
     adj : relating to or characteristic of or being a member of the
           traditional racial division of mankind having brown to
           black pigmentation and tightly curled hair
     n : a person with dark skin who comes from Africa (or whose
         ancestors came from Africa) [syn: {Black}, {Black person},
          {blackamoor}, {Negroid}]
     [also: {negroes} (pl)]
