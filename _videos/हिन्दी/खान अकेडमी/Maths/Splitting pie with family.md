---
version: 1
type: video
provider: YouTube
id: bgiXLWSUAKQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b2864209-a0df-497f-9cbd-33d6af3b8125
updated: 1486069614
title: Splitting pie with family
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/bgiXLWSUAKQ/default.jpg
    - https://i3.ytimg.com/vi/bgiXLWSUAKQ/1.jpg
    - https://i3.ytimg.com/vi/bgiXLWSUAKQ/2.jpg
    - https://i3.ytimg.com/vi/bgiXLWSUAKQ/3.jpg
---
