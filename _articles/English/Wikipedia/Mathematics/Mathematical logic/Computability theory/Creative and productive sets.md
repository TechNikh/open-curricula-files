---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Creative_and_productive_sets
offline_file: ""
offline_thumbnail: ""
uuid: 76024545-bf6b-4f26-8056-23f811ab8863
updated: 1484309485
title: Creative and productive sets
tags:
    - Definition and example
    - Properties
    - Applications in mathematical logic
    - History
    - Notes
categories:
    - Computability theory
---
In computability theory, productive sets and creative sets are types of sets of natural numbers that have important applications in mathematical logic. They are a standard topic in mathematical logic textbooks such as Soare (1987) and Rogers (1987).
