---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lunch
offline_file: ""
offline_thumbnail: ""
uuid: 620b5916-3e12-455e-8603-444a17940ac8
updated: 1484310355
title: lunch
categories:
    - Dictionary
---
lunch
     n : a midday meal [syn: {luncheon}, {tiffin}, {dejeuner}]
     v 1: take the midday meal; "At what time are you lunching?"
     2: provide a midday meal for; "She lunched us well"
