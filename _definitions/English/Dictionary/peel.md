---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peel
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484647501
title: peel
categories:
    - Dictionary
---
peel
     n 1: the tissue forming the hard outer layer (of e.g. a fruit)
          [syn: {skin}, {rind}]
     2: British politician (1788-1850) [syn: {Robert Peel}, {Sir
        Robert Peel}]
     3: the rind of a fruit or vegetable [syn: {skin}]
     v 1: strip the skin off; "pare apples" [syn: {skin}, {pare}]
     2: come off in flakes or thin small pieces; "The paint in my
        house is peeling off" [syn: {peel off}, {flake off}, {flake}]
     3: get undressed; "please don't undress in front of
        everybody!"; "She strips in front of strangers every night
        for a living" [syn: {undress}, {discase}, {uncase}, {unclothe},
         {strip}, {strip down}, {disrobe}] [ant: ...
