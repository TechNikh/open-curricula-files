---
version: 1
type: video
provider: YouTube
id: ZJf9shWlMz0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Parabola%20Focus%20and%20Directrix%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3f8f0858-0251-419a-a277-492cc1cf9390
updated: 1486069571
title: Parabola Focus and Directrix 1
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZJf9shWlMz0/default.jpg
    - https://i3.ytimg.com/vi/ZJf9shWlMz0/1.jpg
    - https://i3.ytimg.com/vi/ZJf9shWlMz0/2.jpg
    - https://i3.ytimg.com/vi/ZJf9shWlMz0/3.jpg
---
Parabola as the locus of all points equidistant from a point and a line
More free lessons at: http://www.khanacademy.org/video?v=ZJf9shWlMz0
