---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Implementation_of_mathematics_in_set_theory
offline_file: ""
offline_thumbnail: ""
uuid: 9d8032c2-dcef-4018-b36e-70ab64ad2de9
updated: 1484309507
title: Implementation of mathematics in set theory
tags:
    - Preliminaries
    - Empty set, singleton, unordered pairs and tuples
    - Ordered pair
    - Relations
    - Related definitions
    - Properties and kinds of relations
    - Functions
    - Operations on functions
    - Special kinds of function
    - Size of sets
    - Finite sets and natural numbers
    - Equivalence relations and partitions
    - Ordinal numbers
    - 'Digression: von Neumann ordinals in NFU'
    - Cardinal numbers
    - The Axiom of Counting and subversion of stratification
    - Properties of strongly cantorian sets
    - 'Familiar number systems: positive rationals, magnitudes, and reals'
    - Operations on indexed families of sets
    - The cumulative hierarchy
categories:
    - Set theory
---
This article examines the implementation of mathematical concepts in set theory. The implementation of a number of basic mathematical concepts is carried out in parallel in ZFC (the dominant set theory) and in NFU, the version of Quine's New Foundations shown to be consistent by R. B. Jensen in 1969 (here understood to include at least axioms of Infinity and Choice).
