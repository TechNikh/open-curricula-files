---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nalas
offline_file: ""
offline_thumbnail: ""
uuid: af233e7d-f60a-446b-8c2e-343eb8840c06
updated: 1484308333
title: Nalas
categories:
    - Uncategorized
---
Nalas (Persian: نلاس‎‎, also Romanized as Nalās)[1] is a village in Melkari Rural District, Vazineh District, Sardasht County, West Azerbaijan Province, Iran. At the 2006 census, its population was 5,891, in 1,088 families.[2]
