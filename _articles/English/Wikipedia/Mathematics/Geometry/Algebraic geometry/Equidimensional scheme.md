---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equidimensional_scheme
offline_file: ""
offline_thumbnail: ""
uuid: c92f7a4e-5320-421c-8ecc-c6c497bab481
updated: 1484309147
title: Equidimensional scheme
categories:
    - Algebraic geometry
---
In algebraic geometry, a field of mathematics, an equidimensional scheme (or, pure dimensional scheme) is a scheme all of whose irreducible components are of the same dimension. All irreducible schemes are equidimensional.[1]
