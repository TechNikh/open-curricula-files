---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flora
offline_file: ""
offline_thumbnail: ""
uuid: 001a6b11-dadc-4309-b3e6-f6431f74f761
updated: 1484310290
title: flora
categories:
    - Dictionary
---
flora
     n 1: all the plant life in a particular region [syn: {vegetation}]
          [ant: {fauna}]
     2: a living organism lacking the power of locomotion [syn: {plant},
         {plant life}]
     [also: {florae} (pl)]
