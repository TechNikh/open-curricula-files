---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lange
offline_file: ""
offline_thumbnail: ""
uuid: 636f3612-f04c-47c6-8c35-52427991d473
updated: 1484310551
title: lange
categories:
    - Dictionary
---
Lange
     n : United States photographer remembered for her portraits of
         rural workers during the Depression (1895-1965) [syn: {Dorothea
         Lange}]
