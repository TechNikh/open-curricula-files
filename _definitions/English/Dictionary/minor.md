---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minor
offline_file: ""
offline_thumbnail: ""
uuid: 5f9de884-ff4c-426c-9fc1-80185d9b7650
updated: 1484310397
title: minor
categories:
    - Dictionary
---
minor
     adj 1: of lesser importance or stature or rank; "a minor poet";
            "had a minor part in the play"; "a minor official";
            "many of these hardy adventurers were minor noblemen";
            "minor back roads" [ant: {major}]
     2: lesser in scope or effect; "had minor differences"; "a minor
        disturbance" [ant: {major}]
     3: inferior in number or size or amount; "a minor share of the
        profits"; "Ursa Minor" [ant: {major}]
     4: of a scale or mode; "the minor keys"; "in B flat minor"
        [ant: {major}]
     5: not of legal age; "minor children" [syn: {nonaged}, {underage}]
        [ant: {major}]
     6: of lesser seriousness or danger; ...
