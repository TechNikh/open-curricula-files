---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Internal_wave
offline_file: ""
offline_thumbnail: ""
uuid: d3b1f42a-1efa-4f1d-8401-b46768dcb6b2
updated: 1484308894
title: Internal wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-InternalWaves_Gibraltar_ISS009-E-09952_54.jpg
tags:
    - Visualization of internal waves
    - Buoyancy, reduced gravity and buoyancy frequency
    - Mathematical modeling of internal waves
    - Interfacial waves
    - Internal waves in uniformly stratified fluid
    - Internal waves in the ocean
    - Properties of internal waves
    - Onshore transport of planktonic larvae
    - Internal tidal bores
    - Surface slicks
    - Predictable downwellings
    - Trapped cores
    - Footnotes
    - Other
categories:
    - Waves
---
Internal waves are gravity waves that oscillate within a fluid medium, rather than on its surface.[1] To exist, the fluid must be stratified: the density must decrease continuously or discontinuously with depth/height due to changes, for example, in temperature and/or salinity. If the density changes over a small vertical distance (as in the case of the thermocline in lakes and oceans or an atmospheric inversion), the waves propagate horizontally like surface waves, but do so at slower speeds as determined by the density difference of the fluid below and above the interface. If the density changes continuously, the waves can propagate vertically as well as horizontally through the fluid.
