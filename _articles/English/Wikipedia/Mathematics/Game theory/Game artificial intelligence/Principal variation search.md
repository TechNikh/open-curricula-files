---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Principal_variation_search
offline_file: ""
offline_thumbnail: ""
uuid: 136dbcce-b29c-4481-b063-966534529ad6
updated: 1484309011
title: Principal variation search
categories:
    - Game artificial intelligence
---
Principal variation search (sometimes equated with the practically identical NegaScout) is a negamax algorithm that can be faster than alpha-beta pruning. Like alpha-beta pruning, NegaScout is a directional search algorithm for computing the minimax value of a node in a tree. It dominates alpha-beta pruning in the sense that it will never examine a node that can be pruned by alpha-beta; however, it relies on accurate node ordering to capitalize on this advantage.
