---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/caution
offline_file: ""
offline_thumbnail: ""
uuid: 651efb08-ecab-45c5-8153-ebaf0cac222d
updated: 1484310379
title: caution
categories:
    - Dictionary
---
caution
     n 1: the trait of being cautious; being attentive to possible
          danger; "a man of caution" [syn: {cautiousness}, {carefulness}]
          [ant: {incaution}]
     2: a warning against certain acts; "a caveat against unfair
        practices" [syn: {caveat}]
     3: judiciousness in avoiding harm or danger; "he exercised
        caution in opening the door"; "he handled the vase with
        care" [syn: {precaution}, {care}, {forethought}]
     4: the trait of being circumspect and prudent [syn: {circumspection}]
     v : warn strongly; put on guard [syn: {admonish}, {monish}]
