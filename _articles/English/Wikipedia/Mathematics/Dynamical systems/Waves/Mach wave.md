---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mach_wave
offline_file: ""
offline_thumbnail: ""
uuid: 93a7a720-ad63-45e2-a397-7d18a836f062
updated: 1484308885
title: Mach wave
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Schlierenfoto_Mach_1-2_Pfeilfl%25C3%25BCgel_-_NASA.jpg'
tags:
    - Mach Angle
categories:
    - Waves
---
In fluid dynamics, a Mach wave is a pressure wave traveling with the speed of sound caused by a slight change of pressure added to a compressible flow. These weak waves can combine in supersonic flow to become a shock wave if sufficient Mach waves are present at any location. Such a shock wave is called a Mach stem or Mach front. Thus it is possible to have shockless compression or expansion in a supersonic flow by having the production of Mach waves sufficiently spaced (cf. isentropic compression in supersonic flows). A Mach wave is the weak limit of an oblique shock wave (a normal shock is the other limit).
