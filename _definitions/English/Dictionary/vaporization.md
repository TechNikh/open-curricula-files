---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vaporization
offline_file: ""
offline_thumbnail: ""
uuid: f7713ba2-345a-4bbe-9ebb-2d766184b0b2
updated: 1484310224
title: vaporization
categories:
    - Dictionary
---
vaporization
     n 1: annihilation by vaporizing something [syn: {vaporisation}]
     2: the process of becoming a vapor [syn: {vaporisation}, {vapor},
         {vapour}, {evaporation}]
