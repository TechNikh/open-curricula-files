---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Haryana
offline_file: ""
offline_thumbnail: ""
uuid: 3660cb61-e1f8-4c2f-b8b4-91d893ef0619
updated: 1484308419
title: Haryana
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-YamunaRiver.jpg
tags:
    - Etymology
    - History
    - Pre-history
    - Medieval
    - Formation
    - geography
    - Rivers
    - Climate
    - Flora and Fauna
    - Administrative divisions
    - Districts
    - Governance
    - Law and order
    - Economy
    - Agriculture
    - Manufacturing
    - Transport
    - Demographics
    - Education
    - Healthcare
    - Communication and media
    - Utilities
    - Sports
    - Tourism
    - Notes
categories:
    - Uncategorized
---
Haryana (IPA: [ɦərɪˈjaːɳaː]) is one of the 29 states in India, situated in North India. It was carved out of the former state of East Punjab on 1 November 1966 on a linguistic basis. It stands 21st in terms of its area, which is spread about 44,212 km2 (17,070 sq mi).[1] As of 2011[update] census of India, the state is eighteenth largest by population with 25,353,081 inhabitants. The city of Chandigarh is its capital while the NCR city of Faridabad is the most populous city of the state.
