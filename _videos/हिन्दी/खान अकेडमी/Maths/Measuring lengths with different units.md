---
version: 1
type: video
provider: YouTube
id: xLf2_zU7FAE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 22774ac6-e756-4a33-81a8-e78b63079434
updated: 1486069613
title: Measuring lengths with different units
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/xLf2_zU7FAE/default.jpg
    - https://i3.ytimg.com/vi/xLf2_zU7FAE/1.jpg
    - https://i3.ytimg.com/vi/xLf2_zU7FAE/2.jpg
    - https://i3.ytimg.com/vi/xLf2_zU7FAE/3.jpg
---
