---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unorganized
offline_file: ""
offline_thumbnail: ""
uuid: e4af111c-7165-43dd-b58e-c406181ce99f
updated: 1484310166
title: unorganized
categories:
    - Dictionary
---
unorganized
     adj 1: not having or belonging to a structured whole; "unorganized
            territories lack a formal government" [syn: {unorganised}]
            [ant: {organized}]
     2: not affiliated in a trade union; "the workers in the plant
        were unorganized" [syn: {unorganised}, {nonunionized}, {nonunionised}]
