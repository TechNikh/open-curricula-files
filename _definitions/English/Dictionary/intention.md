---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intention
offline_file: ""
offline_thumbnail: ""
uuid: 749771ed-105b-4146-bfb2-d8ad7cfd5fa1
updated: 1484310172
title: intention
categories:
    - Dictionary
---
intention
     n 1: an anticipated outcome that is intended or that guides your
          planned actions; "his intent was to provide a new
          translation"; "good intentions are not enough"; "it was
          created with the conscious aim of answering immediate
          needs"; "he made no secret of his designs" [syn: {purpose},
           {intent}, {aim}, {design}]
     2: (usually plural) the goal with respect to a marriage
        proposal; "his intentions are entirely honorable"
     3: an act of intending; a volition that you intend to carry
        out; "my intention changed once I saw her"
