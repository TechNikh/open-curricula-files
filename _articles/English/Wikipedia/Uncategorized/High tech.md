---
version: 1
type: article
id: https://en.wikipedia.org/wiki/High-technology
offline_file: ""
offline_thumbnail: ""
uuid: 5136d048-5a9f-47b3-b322-0f4aa156573f
updated: 1484308450
title: High tech
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-BMW_Leipzig_MEDIA_050719_Download_Karosseriebau_max.jpg
tags:
    - Origin of the term
    - Economy
    - Technology sectors
    - Research and development intensity
    - High-tech society
categories:
    - Uncategorized
---
High technology, often abbreviated to high tech (adjective forms high-technology, high-tech or hi-tech) is technology that is at the cutting edge: the most advanced technology available.
