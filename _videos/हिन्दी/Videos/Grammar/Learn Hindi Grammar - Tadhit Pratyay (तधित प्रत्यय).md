---
version: 1
type: video
provider: YouTube
id: 8WF_GKdHjTI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7b0aa2e3-74c7-43e9-afdf-32d80f347460
updated: 1486069629
title: 'Learn Hindi Grammar - Tadhit Pratyay (तधित प्रत्यय)'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/8WF_GKdHjTI/default.jpg
    - https://i3.ytimg.com/vi/8WF_GKdHjTI/1.jpg
    - https://i3.ytimg.com/vi/8WF_GKdHjTI/2.jpg
    - https://i3.ytimg.com/vi/8WF_GKdHjTI/3.jpg
---
