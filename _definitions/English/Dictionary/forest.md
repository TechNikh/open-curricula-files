---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forest
offline_file: ""
offline_thumbnail: ""
uuid: 560c6072-442b-4390-bc45-eb801f487f88
updated: 1484310290
title: forest
categories:
    - Dictionary
---
forest
     n 1: the trees and other plants in a large densely wooded area
          [syn: {wood}, {woods}]
     2: land that is covered with trees and shrubs [syn: {woodland},
         {timberland}, {timber}]
     v : establish a forest on previously unforested land; "afforest
         the mountains" [syn: {afforest}]
