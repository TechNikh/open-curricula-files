---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kirchhoff%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: d30bdf08-2c2b-4325-adcb-aa52aea6b0c9
updated: 1484309374
title: "Kirchhoff's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Graph_with_all_its_spanning_trees.svg.png
tags:
    - An example using the matrix-tree theorem
    - Proof Outline
    - Particular cases and generalizations
    - "Cayley's formula"
    - "Kirchhoff's theorem for multigraphs"
    - Explicit enumeration of spanning trees
    - Matroids
    - "Kirchhoff's theorem for directed multigraphs"
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory Kirchhoff's theorem or Kirchhoff's matrix tree theorem named after Gustav Kirchhoff is a theorem about the number of spanning trees in a graph, showing that this number can be computed in polynomial time as the determinant of a matrix derived from the graph. It is a generalization of Cayley's formula which provides the number of spanning trees in a complete graph.
