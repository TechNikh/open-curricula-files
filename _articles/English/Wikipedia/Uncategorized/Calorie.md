---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kilocalories
offline_file: ""
offline_thumbnail: ""
uuid: 3038f129-b772-47c7-9775-d0a88e2f8552
updated: 1484308463
title: Calorie
tags:
    - Definitions
    - Nutrition
    - chemistry
categories:
    - Uncategorized
---
Although these units relate to the metric system all forms of the calorie were deemed obsolete in science after the SI system was adopted in the 1950s.[3] The unit of energy in the International System of Units is the joule. One small calorie is approximately 4.2 joules (so one large calorie is about 4.2 kilojoules). The factor used to convert calories to joules at a given temperature is numerically equivalent to the specific heat capacity of water expressed in joules per kelvin per gram or per kilogram. The precise conversion factor depends on the definition adopted.
