---
version: 1
type: video
provider: YouTube
id: 6aC-WTAWgOg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Plant%20Nutrition-%20Mineral%20Absorption%20%28Part%20One%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 87b122aa-e3f3-4cec-bdd4-60e606ff2800
updated: 1486069665
title: 'Plant Nutrition- Mineral Absorption (Part One)'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/6aC-WTAWgOg/default.jpg
    - https://i3.ytimg.com/vi/6aC-WTAWgOg/1.jpg
    - https://i3.ytimg.com/vi/6aC-WTAWgOg/2.jpg
    - https://i3.ytimg.com/vi/6aC-WTAWgOg/3.jpg
---
