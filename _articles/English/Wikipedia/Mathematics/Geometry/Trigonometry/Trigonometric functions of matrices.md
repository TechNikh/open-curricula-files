---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Trigonometric_functions_of_matrices
offline_file: ""
offline_thumbnail: ""
uuid: ed4fd3a6-bf10-4179-8663-a844de7d43fd
updated: 1484309348
title: Trigonometric functions of matrices
categories:
    - Trigonometry
---
The trigonometric functions (especially sine and cosine) for real or complex square matrices occur in solutions of second-order systems of differential equations.[1] They are defined by the same Taylor series that hold for the trigonometric functions of real and complex numbers:[2]
