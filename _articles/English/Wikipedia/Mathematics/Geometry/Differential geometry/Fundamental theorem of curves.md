---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fundamental_theorem_of_curves
offline_file: ""
offline_thumbnail: ""
uuid: ddff027a-4b60-4c63-a038-84bbd953c54e
updated: 1484309219
title: Fundamental theorem of curves
tags:
    - use
    - Congruence
categories:
    - Differential geometry
---
In differential geometry, the fundamental theorem of space curves states that every regular curve in three-dimensional space, with non-zero curvature, has its shape (and size) completely determined by its curvature and torsion.[1][2]
