---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inference
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484549041
title: inference
categories:
    - Dictionary
---
inference
     n : the reasoning involved in drawing a conclusion or making a
         logical judgment on the basis of circumstantial evidence
         and prior conclusions rather than on the basis of direct
         observation [syn: {illation}]
