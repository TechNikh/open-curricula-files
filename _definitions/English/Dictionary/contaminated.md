---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contaminated
offline_file: ""
offline_thumbnail: ""
uuid: 908ff808-c7cf-4e70-91eb-948d8f91a281
updated: 1484310409
title: contaminated
categories:
    - Dictionary
---
contaminated
     adj 1: corrupted by contact or association; "contaminated evidence"
            [ant: {uncontaminated}]
     2: contaminated with infecting organisms; "dirty wounds";
        "obliged to go into infected rooms"- Jane Austen [syn: {dirty},
         {infected}, {pestiferous}]
     3: rendered unwholesome by contaminants and pollution; "had to
        boil the contaminated water"; "polluted lakes and streams"
        [syn: {polluted}]
