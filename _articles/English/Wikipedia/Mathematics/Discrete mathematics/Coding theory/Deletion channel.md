---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deletion_channel
offline_file: ""
offline_thumbnail: ""
uuid: d0ce9323-cf69-4050-bad5-748a5a1394b6
updated: 1484308539
title: Deletion channel
tags:
    - Formal description
    - Capacity
categories:
    - Coding theory
---
A deletion channel is a communications channel model used in coding theory and information theory. In this model, a transmitter sends a bit (a zero or a one), and the receiver either receives the bit (with probability 
  
    
      
        p
      
    
    {\displaystyle p}
  
) or does not receive anything without being notified that the bit was dropped (with probability 
  
    
      
        1
        −
        p
      
    
    {\displaystyle 1-p}
  
). Determining the capacity of the deletion channel is an open problem.[1][2]
