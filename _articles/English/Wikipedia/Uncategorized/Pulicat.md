---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pulicat
offline_file: ""
offline_thumbnail: ""
uuid: ee4e5af8-4203-409f-be70-ee8fd001fd0e
updated: 1484308425
title: Pulicat
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-India_-_Pulicat_Lake_-_003_-_entrance_to_the_Dutch_cemetery.jpg
categories:
    - Uncategorized
---
Pulicat (Pazhaverkadu) (Tamil:பழவேற்காடு) is a historic seashore town in "North of Chennai" in Thiruvallur District, of Tamil Nadu state, South India. It is about 60 kilometres (37 mi) north of Chennai and 3 kilometres (1.9 mi) from Elavur, on the barrier island of Sriharikota, which separates Pulicat Lake from the Bay of Bengal. Pulicat lake is a shallow salt water lagoon which stretches about 60 kilometres (37 mi) along the coast. With lakeside and seashore development as well as several Special Economic Zones (SEZs) including a US $1 billion Medical SEZ, coming up in nearby Elavur, land prices in the area are rising.
