---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inflection_point
offline_file: ""
offline_thumbnail: ""
uuid: 58b4906b-5e9c-4899-96a8-84b924a78fd1
updated: 1484309162
title: Inflection point
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-X_cubed_plot.svg.png
tags:
    - Definition
    - A necessary but not sufficient condition
    - Categorization of points of inflection
    - Functions with discontinuities
    - References and Sources
    - Sources
categories:
    - Analytic geometry
---
In differential calculus, an inflection point, point of inflection, flex, or inflection (inflexion (British English)) is a point on a curve at which the curve changes from being concave (concave downward) to convex (concave upward), or vice versa.
