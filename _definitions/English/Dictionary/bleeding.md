---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bleeding
offline_file: ""
offline_thumbnail: ""
uuid: f8c50fb6-c077-440f-b488-9c8620ad2ec8
updated: 1484310315
title: bleeding
categories:
    - Dictionary
---
bleeding
     n : flow of blood from a ruptured blood vessels [syn: {hemorrhage},
          {haemorrhage}]
