---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indices
offline_file: ""
offline_thumbnail: ""
uuid: fb9c0123-183a-4907-afc7-5ddfd003757c
updated: 1484310214
title: indices
categories:
    - Dictionary
---
index
     n 1: a numerical scale used to compare variables with one another
          or with some reference number
     2: a number or ratio (a value on a scale of measurement)
        derived from a series of observed facts; can reveal
        relative changes as a function of time [syn: {index number},
         {indicant}, {indicator}]
     3: a mathematical notation indicating the number of times a
        quantity is multiplied by itself [syn: {exponent}, {power}]
     4: an alphabetical listing of names and topics along with page
        numbers where they are discussed
     5: the finger next to the thumb [syn: {index finger}, {forefinger}]
     v 1: list in an index
     2: provide ...
