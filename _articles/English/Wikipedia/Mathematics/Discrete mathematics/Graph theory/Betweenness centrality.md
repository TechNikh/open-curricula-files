---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Betweenness_centrality
offline_file: ""
offline_thumbnail: ""
uuid: 379f9afe-a56c-4b75-8150-c5a8c4d54205
updated: 1484308603
title: Betweenness centrality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Graph_betweenness.svg.png
tags:
    - Definition
    - The load distribution in real and model networks
    - Model networks
    - Real networks
    - Weighted networks
    - Algorithms
    - Related concepts
categories:
    - Graph theory
---
Betweenness centrality is an indicator of a node's centrality in a network. It is equal to the number of shortest paths from all vertices to all others that pass through that node. A node with high betweenness centrality has a large influence on the transfer of items through the network, under the assumption that item transfer follows the shortest paths. The concept finds wide application, including computer and social networks, biology, transport and scientific cooperation. Development of betweenness centrality is generally attributed to sociologist Linton Freeman.[1] The idea was earlier proposed by mathematician J. Anthonisse, but his work was never published.[2]
