---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_space
offline_file: ""
offline_thumbnail: ""
uuid: b6f52095-4c23-46fa-9ee1-f6957623118f
updated: 1484309130
title: Algebraic space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_10.png
tags:
    - Definition
    - Algebraic spaces as quotients of schemes
    - Algebraic spaces as sheaves
    - Algebraic spaces and schemes
    - Algebraic spaces and analytic spaces
    - Generalization
categories:
    - Algebraic geometry
---
In mathematics, algebraic spaces form a generalization of the schemes of algebraic geometry, introduced by Artin (1969, 1971) for use in deformation theory. Intuitively, schemes are given by gluing together affine schemes using the Zariski topology, while algebraic spaces are given by gluing together affine schemes using the finer étale topology. Alternatively one can think of schemes as being locally isomorphic to affine schemes in the Zariski topology, while algebraic spaces are locally isomorphic to affine schemes in the étale topology.
