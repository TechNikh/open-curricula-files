---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magnetic_anisotropy
offline_file: ""
offline_thumbnail: ""
uuid: 7c9c6c63-8791-4988-83e8-2aef8166f88a
updated: 1484309297
title: Magnetic anisotropy
tags:
    - Sources
    - Anisotropy energy of a single-domain magnet
    - Uniaxial
    - Triaxial
    - Cubic
    - Notes
categories:
    - Orientation (geometry)
---
Magnetic anisotropy is the directional dependence of a material's magnetic properties. The magnetic moment of magnetically anisotropic materials will tend to align with an "easy axis", which is an energetically favorable direction of spontaneous magnetization. The two opposite directions along an easy axis are usually equivalent, and the actual direction of magnetization can be along either of them (see spontaneous symmetry breaking).
