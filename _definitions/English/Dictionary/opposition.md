---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/opposition
offline_file: ""
offline_thumbnail: ""
uuid: 7ca2eed8-c67e-4514-894a-ef7b24e89830
updated: 1484310198
title: opposition
categories:
    - Dictionary
---
opposition
     n 1: the action of opposing something that you disapprove or
          disagree with; "he encountered a general feeling of
          resistance from many citizens"; "despite opposition from
          the newspapers he went ahead" [syn: {resistance}]
     2: the relation between opposed entities [syn: {oppositeness}]
     3: the act of opposing groups confronting each other; "the
        government was not ready for a confrontation with the
        unions"; "the invaders encountered stiff opposition" [syn:
         {confrontation}]
     4: a contestant that you are matched against [syn: {opponent},
        {opposite}]
     5: a body of people united in opposing something
     ...
