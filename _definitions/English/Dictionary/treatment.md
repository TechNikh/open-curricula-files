---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/treatment
offline_file: ""
offline_thumbnail: ""
uuid: cb328924-2f89-4a91-9f1c-836932a2d6a5
updated: 1484310387
title: treatment
categories:
    - Dictionary
---
treatment
     n 1: care by procedures or applications that are intended to
          relieve illness or injury
     2: the management of someone or something; "the handling of
        prisoners"; "the treatment of water sewage"; "the right to
        equal treatment in the criminal justice system" [syn: {handling}]
     3: a manner of dealing with something artistically; "his
        treatment of space borrows from Italian architecture"
     4: an extended communication (often interactive) dealing with
        some particular topic; "the book contains an excellent
        discussion of modal logic"; "his treatment of the race
        question is badly biased" [syn: {discussion}, ...
