---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rotational_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 69f11619-4a51-48f7-a963-5f5507747a68
updated: 1484309315
title: Rotational symmetry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-The_armoured_triskelion_on_the_flag_of_the_Isle_of_Man.svg.png
tags:
    - Formal treatment
    - Discrete rotational symmetry
    - Examples
    - Multiple symmetry axes through the same point
    - Rotational symmetry with respect to any angle
    - Rotational symmetry with translational symmetry
categories:
    - Symmetry
---
Rotational symmetry, also known as radial symmetry in biology, is the property a shape has when it looks the same after some rotation by a partial turn. An object's degree of rotational symmetry is the number of distinct orientations in which it looks the same.
