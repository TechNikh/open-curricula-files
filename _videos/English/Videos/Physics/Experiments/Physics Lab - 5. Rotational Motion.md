---
version: 1
type: video
provider: YouTube
id: rB9_UjmllZs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f0bb6e96-ebf4-4e83-8468-c54c09a6b720
updated: 1486069641
title: 'Physics Lab - 5. Rotational Motion'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/rB9_UjmllZs/default.jpg
    - https://i3.ytimg.com/vi/rB9_UjmllZs/1.jpg
    - https://i3.ytimg.com/vi/rB9_UjmllZs/2.jpg
    - https://i3.ytimg.com/vi/rB9_UjmllZs/3.jpg
---
