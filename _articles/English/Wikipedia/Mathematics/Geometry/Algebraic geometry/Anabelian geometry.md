---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anabelian_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 057e6df1-ca1f-499f-82ec-6a0f40881352
updated: 1484309134
title: Anabelian geometry
tags:
    - Formulation of a conjecture of Grothendieck on curves
    - Notes
categories:
    - Algebraic geometry
---
Anabelian geometry is a proposed theory in mathematics, describing the way the algebraic fundamental group G of an algebraic variety V, or some related geometric object, determines how V can be mapped into another geometric object W, under the assumption that G is very far from being an abelian group, in a sense to be made more precise. The word anabelian (an alpha privative an- before abelian) was introduced in Esquisse d'un Programme, an influential manuscript of Alexander Grothendieck, circulated in the 1980s.[1]
