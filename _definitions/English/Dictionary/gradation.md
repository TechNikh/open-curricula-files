---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gradation
offline_file: ""
offline_thumbnail: ""
uuid: 6abc59a2-5c37-4c4d-a4d2-3e8169c6ce26
updated: 1484310401
title: gradation
categories:
    - Dictionary
---
gradation
     n 1: relative position in a graded series; "always a step
          behind"; "subtle gradations in color"; "keep in step
          with the fashions" [syn: {step}]
     2: a degree of ablaut [syn: {grade}]
     3: the act of arranging in grades [syn: {graduation}]
