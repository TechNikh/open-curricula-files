---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Categorical_quotient
offline_file: ""
offline_thumbnail: ""
uuid: 74316012-f51c-4762-80eb-a658f7740f21
updated: 1484309136
title: Categorical quotient
categories:
    - Algebraic geometry
---
In algebraic geometry, given a category C, a categorical quotient of an object X with action of a group G is a morphism 
  
    
      
        π
        :
        X
        →
        Y
      
    
    {\displaystyle \pi :X\to Y}
  
 that
