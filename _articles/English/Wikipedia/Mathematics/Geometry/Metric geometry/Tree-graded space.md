---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tree-graded_space
offline_file: ""
offline_thumbnail: ""
uuid: fba97767-baca-40de-a2ac-1f9874d6ba7c
updated: 1484309272
title: Tree-graded space
categories:
    - Metric geometry
---
A geodesic metric space 
  
    
      
        X
      
    
    {\displaystyle X}
  
 is called tree-graded space, with respect to a collection of connected proper subsets called pieces, if any two distinct pieces intersect by at most one point, and every non-trivial simple geodesic triangle of 
  
    
      
        X
      
    
    {\displaystyle X}
  
 is contained in one of the pieces.
