---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lowering
offline_file: ""
offline_thumbnail: ""
uuid: f6e25b52-e3ad-4a46-b5f8-f744fcac366a
updated: 1484310464
title: lowering
categories:
    - Dictionary
---
lowering
     adj : darkened by clouds; "a heavy sky" [syn: {heavy}, {sullen}, {threatening}]
     n 1: the act of causing to become less
     2: the act of causing something to move to a lower level [syn:
        {letting down}]
