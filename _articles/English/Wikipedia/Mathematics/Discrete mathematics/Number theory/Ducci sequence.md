---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ducci_sequence
offline_file: ""
offline_thumbnail: ""
uuid: 6513fe7a-7fe2-4769-87d1-88dfb3b4e85d
updated: 1484308636
title: Ducci sequence
tags:
    - Properties
    - Examples
    - Modulo two form
    - Cellular automata
    - Other related topics
categories:
    - Number theory
---
Given an n-tuple of integers 
  
    
      
        (
        
          a
          
            1
          
        
        ,
        
          a
          
            2
          
        
        ,
        .
        .
        .
        ,
        
          a
          
            n
          
        
        )
      
    
    {\displaystyle (a_{1},a_{2},...,a_{n})}
  
, the next n-tuple in the sequence is formed by taking the absolute differences of neighbouring integers:
