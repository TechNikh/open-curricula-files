---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Representer_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 0295337d-ee2a-4302-983d-c327b9f3532d
updated: 1484308688
title: Representer theorem
tags:
    - Formal statement
    - Generalizations
    - Applications
categories:
    - Theoretical computer science
---
In statistical learning theory, a representer theorem is any of several related results stating that a minimizer 
  
    
      
        
          f
          
            ∗
          
        
      
    
    {\displaystyle f^{*}}
  
 of a regularized empirical risk function defined over a reproducing kernel Hilbert space can be represented as a finite linear combination of kernel products evaluated on the input points in the training set data.
