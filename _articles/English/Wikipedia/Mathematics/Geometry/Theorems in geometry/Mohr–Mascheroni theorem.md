---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Mohr%E2%80%93Mascheroni_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 7f691309-671e-4ac0-910d-27517c6c5bec
updated: 1484309119
title: Mohr–Mascheroni theorem
tags:
    - Proof approach
    - Notes
categories:
    - Theorems in geometry
---
In mathematics, the Mohr–Mascheroni theorem states that any geometric construction that can be performed by a compass and straightedge can be performed by a compass alone. The result was originally published by Georg Mohr in 1672,[1] but his proof languished in obscurity until 1928.[2][3] The theorem was independently discovered by Lorenzo Mascheroni in 1797.[4]
