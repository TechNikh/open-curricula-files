---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ind-scheme
offline_file: ""
offline_thumbnail: ""
uuid: bc20a852-8998-4180-b5ec-ffe796443825
updated: 1484309156
title: Ind-scheme
categories:
    - Algebraic geometry
---
In algebraic geometry, an ind-scheme is a set-valued functor that can be written (represented) as a direct limit (i.e., inductive limit) of closed embedding of schemes.
