---
version: 1
type: video
provider: YouTube
id: DA2t5N72mgw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7916e1a0-8a3a-4085-ac35-b2e624e321aa
updated: 1486069668
title: 'Transcription - Central Dogma Part 1'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/DA2t5N72mgw/default.jpg
    - https://i3.ytimg.com/vi/DA2t5N72mgw/1.jpg
    - https://i3.ytimg.com/vi/DA2t5N72mgw/2.jpg
    - https://i3.ytimg.com/vi/DA2t5N72mgw/3.jpg
---
