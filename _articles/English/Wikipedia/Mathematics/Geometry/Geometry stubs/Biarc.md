---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biarc
offline_file: ""
offline_thumbnail: ""
uuid: 9e171411-e884-4a3b-b6cc-f60036cee274
updated: 1484309354
title: Biarc
categories:
    - Geometry stubs
---
A biarc is a smooth curve formed from two circular arcs. In order to make the biarc smooth (G1 continuous), the two arcs should have the same tangent at the connecting point where they meet.
