---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vital
offline_file: ""
offline_thumbnail: ""
uuid: 34b1b61a-547e-4ad4-b9de-53d23a13d977
updated: 1484310363
title: vital
categories:
    - Dictionary
---
vital
     adj 1: urgently needed; absolutely necessary; "a critical element
            of the plan"; "critical medical supplies"; "vital for
            a healthy society"; "of vital interest" [syn: {critical}]
     2: performing an essential function in the living body; "vital
        organs"; "blood and other vital fluids"; "the loss of
        vital heat in shock"; "a vital spot"; "life-giving love
        and praise" [syn: {life-sustaining}]
     3: full of spirit; "a dynamic full of life woman"; "a vital and
        charismatic leader"; "this whole lively world" [syn: {full
        of life}, {lively}]
     4: manifesting or characteristic of life; "a vital, living
        organism"; ...
