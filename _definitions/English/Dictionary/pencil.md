---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pencil
offline_file: ""
offline_thumbnail: ""
uuid: d7651000-25ab-4c9d-ab42-47bdb1c518c7
updated: 1484310299
title: pencil
categories:
    - Dictionary
---
pencil
     n 1: a thin cylindrical pointed writing implement; a rod of
          marking substance encased in wood
     2: graphite (or a similar substance) used in such a way as to
        be a medium of communication; "the words were scribbled in
        pencil"; "this artist's favorite medium is pencil"
     3: a figure formed by a set of straight lines or light rays
        meeting at a point
     4: a cosmetic in a long thin stick; designed to be applied to a
        particular part of the face; "an eyebrow pencil"
     v : write, draw, or trace with a pencil; "he penciled a figure"
     [also: {pencilling}, {pencilled}]
