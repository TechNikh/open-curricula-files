---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Internal_organs
offline_file: ""
offline_thumbnail: ""
uuid: 02ce625a-2b08-492c-bc33-7e37e2b196a4
updated: 1484308377
title: Organ (anatomy)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Leber_Schaf.jpg
tags:
    - Organ systems
    - Other animals
    - Plants
    - History
    - Etymology
    - 7 Vital Organs of Antiquity
categories:
    - Uncategorized
---
In biology, an organ or viscus is a collection of tissues joined in a structural unit to serve a common function.[1] In anatomy, a viscus (/ˈvɪskəs/) is an internal organ, and viscera (/ˈvɪsərə/) is the plural form.[2][3]
