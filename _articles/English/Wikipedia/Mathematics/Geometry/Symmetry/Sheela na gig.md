---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sheela_na_gig
offline_file: ""
offline_thumbnail: ""
uuid: 7057c57a-bfa9-4485-9566-1748bd73ae73
updated: 1484309313
title: Sheela na gig
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-SheelaWiki.jpg
tags:
    - Origin
    - Etymology
    - Hypotheses
    - Survival of a pagan goddess
    - Fertility figure
    - Warning against lust
    - Protection against evil
    - Distribution
    - Parallels
    - Articles
categories:
    - Symmetry
---
Sheela na gigs are figurative carvings of naked women displaying an exaggerated vulva. They are architectural grotesques found on churches, castles, and other buildings, particularly in Ireland and Great Britain, sometimes together with male figures. One of the best examples may be found in the Round Tower at Rattoo, in County Kerry, Ireland. There is a replica of the round tower sheela na gig in the County Museum in Tralee town. Another well-known example may be seen at Kilpeck in Herefordshire, England.
