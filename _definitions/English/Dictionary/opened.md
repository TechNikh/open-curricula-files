---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/opened
offline_file: ""
offline_thumbnail: ""
uuid: 664441de-ce0b-4a72-af7e-50d5164bbda3
updated: 1484310344
title: opened
categories:
    - Dictionary
---
opened
     adj 1: used of mouth or eyes; "keep your eyes open"; "his mouth
            slightly opened" [syn: {open}] [ant: {closed}]
     2: made open or clear; "the newly opened road"
     3: not sealed or having been unsealed; "the letter was already
        open"; "the opened package lay on the table" [syn: {open}]
