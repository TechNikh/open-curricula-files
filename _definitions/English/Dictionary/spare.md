---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spare
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484485921
title: spare
categories:
    - Dictionary
---
spare
     adj 1: thin and fit; "the spare figure of a marathon runner"; "a
            body kept trim by exercise" [syn: {trim}]
     2: more than is needed, desired, or required; "trying to lose
        excess weight"; "found some extra change lying on the
        dresser"; "yet another book on heraldry might be thought
        redundant"; "skills made redundant by technological
        advance"; "sleeping in the spare room"; "supernumerary
        ornamentation"; "it was supererogatory of her to gloat";
        "delete superfluous (or unnecessary) words"; "extra ribs
        as well as other supernumerary internal parts"; "surplus
        cheese distributed to the needy" [syn: {excess}, ...
