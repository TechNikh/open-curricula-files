---
version: 1
type: video
provider: YouTube
id: oPC41uDufHU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a2c8f826-37a8-4958-9147-05a3bc3ee9c8
updated: 1486069654
title: BCS theory
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/oPC41uDufHU/default.jpg
    - https://i3.ytimg.com/vi/oPC41uDufHU/1.jpg
    - https://i3.ytimg.com/vi/oPC41uDufHU/2.jpg
    - https://i3.ytimg.com/vi/oPC41uDufHU/3.jpg
---
