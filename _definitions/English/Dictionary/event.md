---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/event
offline_file: ""
offline_thumbnail: ""
uuid: 2f892157-c474-458c-b426-ff08bd2b1afe
updated: 1484310283
title: event
categories:
    - Dictionary
---
event
     n 1: something that happens at a given place and time
     2: a special set of circumstances; "in that event, the first
        possibility is excluded"; "it may rain in which case the
        picnic will be canceled" [syn: {case}]
     3: a phenomenon located at a single point in space-time; the
        fundamental observational entity in relativity theory
     4: a phenomenon that follows and is caused by some previous
        phenomenon; "the magnetic effect was greater when the rod
        was lengthwise"; "his decision had depressing consequences
        for business"; "he acted very wise after the event" [syn:
        {consequence}, {effect}, {outcome}, {result}, {issue}, ...
