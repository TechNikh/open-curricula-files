---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inward
offline_file: ""
offline_thumbnail: ""
uuid: 6bb8ab09-4f52-4dd7-b8bf-8669b884106f
updated: 1484310365
title: inward
categories:
    - Dictionary
---
inward
     adj 1: relating to or existing in the mind or thoughts; "a concern
            with inward reflections" [ant: {outward}]
     2: directed or moving inward or toward a center; "the inbound
        train"; "inward flood of capital" [syn: {arriving(a)}, {inbound}]
     adv 1: toward the center or interior; "move the needle further
            inwards!" [syn: {inwards}] [ant: {outward}]
     2: to or toward the inside of; "come in"; "smash in the door"
        [syn: {in}, {inwards}]
