---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/satisfied
offline_file: ""
offline_thumbnail: ""
uuid: 2ff29ff1-9235-45e3-8318-6777084ebd1e
updated: 1484310414
title: satisfied
categories:
    - Dictionary
---
satisfied
     adj 1: filled with satisfaction; "a satisfied customer"
     2: allayed; "his thirst quenched he was able to continue" [syn:
         {quenched}, {slaked}]
