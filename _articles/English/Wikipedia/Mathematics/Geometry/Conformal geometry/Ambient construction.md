---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ambient_construction
offline_file: ""
offline_thumbnail: ""
uuid: 15ccec5f-c904-473b-9321-13151d3253fa
updated: 1484309187
title: Ambient construction
tags:
    - Overview
    - Details
    - The null line bundle
    - The ambient space
categories:
    - Conformal geometry
---
In conformal geometry, the ambient construction refers to a construction of Charles Fefferman and Robin Graham[1] for which a conformal manifold of dimension n is realized (ambiently) as the boundary of a certain Poincaré manifold, or alternatively as the celestial sphere of a certain pseudo-Riemannian manifold.
