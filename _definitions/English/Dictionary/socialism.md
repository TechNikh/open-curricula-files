---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/socialism
offline_file: ""
offline_thumbnail: ""
uuid: 4778a157-025b-4c6c-9599-eef9c816d6d9
updated: 1484310553
title: socialism
categories:
    - Dictionary
---
socialism
     n 1: a political theory advocating state ownership of industry
     2: an economic system based on state ownership of capital [syn:
         {socialist economy}] [ant: {capitalism}]
