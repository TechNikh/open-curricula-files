---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inversive_distance
offline_file: ""
offline_thumbnail: ""
uuid: 17e3e2f1-bb53-4b17-9f24-895c1c668c18
updated: 1484309258
title: Inversive distance
tags:
    - Properties
    - Distance formula
    - In other geometries
    - Applications
    - Steiner chains
    - Circle packings
categories:
    - Inversive geometry
---
In inversive geometry, the inversive distance is a way of measuring the "distance" between two circles, regardless of whether the circles cross each other, are tangent to each other, or are disjoint from each other.[1]
