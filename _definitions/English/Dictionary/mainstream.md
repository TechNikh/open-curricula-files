---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mainstream
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484409421
title: mainstream
categories:
    - Dictionary
---
mainstream
     n : the prevailing current of thought; "his thinking was in the
         American mainstream"
