---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maximum_flow_problem
offline_file: ""
offline_thumbnail: ""
uuid: d6c27c2b-7681-4dc1-8a3a-36409515ef70
updated: 1484309398
title: Maximum flow problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Max_flow.svg.png
tags:
    - History
    - Definition
    - Solutions
    - Integral flow theorem
    - Application
    - Multi-source multi-sink maximum flow problem
    - Minimum path cover in directed acyclic graph
    - Maximum cardinality bipartite matching
    - Maximum flow problem with vertex capacities
    - Maximum edge-disjoint path
    - Maximum independent (vertex-disjoint) path
    - Real World Applications
    - Baseball elimination
    - Airline scheduling
    - Circulation–demand problem
categories:
    - Computational problems in graph theory
---
The maximum flow problem can be seen as a special case of more complex network flow problems, such as the circulation problem. The maximum value of an s-t flow (i.e., flow from source s to sink t) is equal to the minimum capacity of an s-t cut (i.e., cut severing s from t) in the network, as stated in the max-flow min-cut theorem.
