---
version: 1
type: video
provider: YouTube
id: sX6iWfQhM4w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 790710fb-9a29-491a-a366-07bb1d096599
updated: 1486069611
title: Inverting matrices part 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/sX6iWfQhM4w/default.jpg
    - https://i3.ytimg.com/vi/sX6iWfQhM4w/1.jpg
    - https://i3.ytimg.com/vi/sX6iWfQhM4w/2.jpg
    - https://i3.ytimg.com/vi/sX6iWfQhM4w/3.jpg
---
