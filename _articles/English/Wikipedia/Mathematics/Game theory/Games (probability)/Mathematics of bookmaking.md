---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mathematics_of_bookmaking
offline_file: ""
offline_thumbnail: ""
uuid: 1dfa52ed-b10f-4db6-880b-950f91ae5624
updated: 1484309045
title: Mathematics of bookmaking
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/128px-Nuvola_apps_edu_mathematics_blue-p.svg.png
tags:
    - "Making a 'book' (and the notion of overround)"
    - Example
    - Overround on multiple bets
    - Settling winning bets
    - Singles
    - Multiple bets
    - Full-cover bets
    - Full cover bets with singles
    - Algebraic interpretation
    - Examples
    - Settling other types of winning bets
    - Algebraic interpretation
    - Examples
    - Notes
categories:
    - Games (probability)
---
In gambling parlance, making a book is the practice of laying bets on the various possible outcomes of a single event. The term originates from the practice of recording such wagers in a hard-bound ledger (the 'book') and gives the English language the term bookmaker for the person laying the bets and thus 'making the book'.[1][2]
