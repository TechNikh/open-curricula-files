---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/K%C3%A4hler%E2%80%93Einstein_metric'
offline_file: ""
offline_thumbnail: ""
uuid: 9b86c1ba-6555-4151-b1f1-e99886df3413
updated: 1484309226
title: Kähler–Einstein metric
categories:
    - Differential geometry
---
In differential geometry, a Kähler–Einstein metric on a complex manifold is a Riemannian metric that is both a Kähler metric and an Einstein metric. A manifold is said to be Kähler–Einstein if it admits a Kähler–Einstein metric. The most important special case of these are the Calabi–Yau manifolds, which are Kähler and Ricci-flat.
