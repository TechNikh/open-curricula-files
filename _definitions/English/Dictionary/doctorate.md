---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/doctorate
offline_file: ""
offline_thumbnail: ""
uuid: bdc46677-c0c1-4da8-b998-79530e1143b3
updated: 1484310429
title: doctorate
categories:
    - Dictionary
---
doctorate
     n : one of the highest academic degrees conferred by a
         university [syn: {doctor's degree}]
