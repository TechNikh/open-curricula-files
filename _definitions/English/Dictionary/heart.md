---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heart
offline_file: ""
offline_thumbnail: ""
uuid: d3fa37e8-b279-4ead-9308-85bd4856c9ba
updated: 1484310200
title: heart
categories:
    - Dictionary
---
heart
     n 1: the locus of feelings and intuitions; "in your heart you
          know it is true"; "her story would melt your bosom"
          [syn: {bosom}]
     2: the hollow muscular organ located behind the sternum and
        between the lungs; its rhythmic contractions pump blood
        through the body; "he stood still, his heart thumping
        wildly" [syn: {pump}, {ticker}]
     3: the courage to carry on; "he kept fighting on pure spunk";
        "you haven't got the heart for baseball" [syn: {mettle}, {nerve},
         {spunk}]
     4: an area that is approximately central within some larger
        region; "it is in the center of town"; "they ran forward
        into the ...
