---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/focusing
offline_file: ""
offline_thumbnail: ""
uuid: 5aceab18-f19f-4e41-82b6-820931a432c6
updated: 1484310453
title: focusing
categories:
    - Dictionary
---
focusing
     n 1: the concentration of attention or energy on something; "the
          focus of activity shifted to molecular biology"; "he had
          no direction in his life" [syn: {focus}, {focussing}, {direction},
           {centering}]
     2: the act of bringing into focus [syn: {focalization}, {focalisation}]
