---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/linear
offline_file: ""
offline_thumbnail: ""
uuid: 955b212f-39cf-44a0-9ad5-acfecfc1d80b
updated: 1484310228
title: linear
categories:
    - Dictionary
---
linear
     adj 1: designating or involving an equation whose terms are of the
            first degree [syn: {additive}] [ant: {nonlinear}]
     2: of or in or along or relating to a line; involving a single
        dimension; "a linear foot" [syn: {one-dimensional}] [ant:
        {planar}, {cubic}]
     3: of a circuit or device having an output that is proportional
        to the input; "analogue device"; "linear amplifier" [syn:
        {analogue}, {analog}] [ant: {digital}]
     4: of a leaf shape; long and narrow [syn: {elongate}]
     5: measured lengthwise; "cost of lumber per running foot" [syn:
         {running(a)}]
