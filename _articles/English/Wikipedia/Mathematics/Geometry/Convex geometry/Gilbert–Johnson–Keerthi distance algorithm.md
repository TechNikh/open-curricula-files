---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gilbert%E2%80%93Johnson%E2%80%93Keerthi_distance_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: c88e9738-046a-4463-96d2-120e7303e8b4
updated: 1484309193
title: Gilbert–Johnson–Keerthi distance algorithm
tags:
    - Overview
    - Pseudocode
    - Illustration
categories:
    - Convex geometry
---
The Gilbert–Johnson–Keerthi distance algorithm is a method of determining the minimum distance between two convex sets. Unlike many other distance algorithms, it does not require that the geometry data be stored in any specific format, but instead relies solely on a support function to iteratively generate closer simplices to the correct answer using the Minkowski sum (CSO) of two convex shapes.
