---
version: 1
type: article
id: https://en.wikipedia.org/wiki/History_of_trigonometry
offline_file: ""
offline_thumbnail: ""
uuid: 359cb475-647e-4ebd-96d0-d7d9bc9b82f2
updated: 1484309247
title: History of trigonometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-P_history.svg_3.png
tags:
    - Etymology
    - Development
    - Early trigonometry
    - Greek mathematics
    - Indian mathematics
    - Islamic mathematics
    - Chinese mathematics
    - European mathematics
    - Citations and footnotes
categories:
    - History of geometry
---
Early study of triangles can be traced to the 2nd millennium BC, in Egyptian mathematics (Rhind Mathematical Papyrus) and Babylonian mathematics. Systematic study of trigonometric functions began in Hellenistic mathematics, reaching India as part of Hellenistic astronomy. In Indian astronomy, the study of trigonometric functions flourished in the Gupta period, especially due to Aryabhata (sixth century CE). During the Middle Ages, the study of trigonometry continued in Islamic mathematics, hence it was adopted as a separate subject in the Latin West beginning in the Renaissance with Regiomontanus. The development of modern trigonometry shifted during the western Age of Enlightenment, ...
