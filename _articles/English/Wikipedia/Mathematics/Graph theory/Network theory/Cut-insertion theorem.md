---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cut-insertion_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 4152f015-4649-4f73-a0b0-33c445020cbf
updated: 1484309432
title: Cut-insertion theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Generica_rete_lineare_N.jpg
tags:
    - Statement
    - Network functions
    - Transfer function
    - >
        Evaluation of the impedance and of the admittance between
        two nodes
    - Impedance
    - Admittance
    - Comments
    - Notes
categories:
    - Network theory
---
The Cut-insertion theorem, also known as Pellegrini's theorem,[1] is a linear network theorem that allows transformation of a generic network N into another network N' that makes analysis simpler and for which the main properties are more apparent.
