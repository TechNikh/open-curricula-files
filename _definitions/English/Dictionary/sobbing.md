---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sobbing
offline_file: ""
offline_thumbnail: ""
uuid: 04a520d1-1b3d-42db-9b4d-f8126a0efc9d
updated: 1484310152
title: sobbing
categories:
    - Dictionary
---
sobbing
     n : convulsive gasp made while weeping [syn: {sob}]
