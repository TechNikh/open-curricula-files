---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/veranda
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484572501
title: veranda
categories:
    - Dictionary
---
veranda
     n : a porch along the outside of a building (sometimes partly
         enclosed) [syn: {verandah}, {gallery}]
