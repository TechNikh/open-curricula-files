---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tactics
offline_file: ""
offline_thumbnail: ""
uuid: 43ea84f5-7f08-4429-9284-ca57d2241498
updated: 1484310180
title: tactics
categories:
    - Dictionary
---
tactics
     n : the branch of military science dealing with detailed
         maneuvers to achieve objectives set by strategy
