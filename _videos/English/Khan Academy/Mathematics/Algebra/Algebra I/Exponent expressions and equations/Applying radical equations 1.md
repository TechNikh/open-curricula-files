---
version: 1
type: video
provider: YouTube
id: U3JxFFdKCKM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Exponent%20expressions%20and%20equations/Applying%20radical%20equations%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c66ed24d-3816-4db2-b895-de13710bc47f
updated: 1486069565
title: Applying radical equations 1
categories:
    - Exponent expressions and equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/U3JxFFdKCKM/default.jpg
    - https://i3.ytimg.com/vi/U3JxFFdKCKM/1.jpg
    - https://i3.ytimg.com/vi/U3JxFFdKCKM/2.jpg
    - https://i3.ytimg.com/vi/U3JxFFdKCKM/3.jpg
---
