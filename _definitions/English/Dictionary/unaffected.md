---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unaffected
offline_file: ""
offline_thumbnail: ""
uuid: bbda3465-82a9-435a-a1cd-835f9778e1a1
updated: 1484310403
title: unaffected
categories:
    - Dictionary
---
unaffected
     adj 1: undergoing no change when acted upon; "entirely unaffected
            by each other's writings"; "fibers remained apparently
            unaffected by the treatment" [ant: {affected}]
     2: (followed by `to' or `by') unaware of or indifferent to;
        "insensible to the suffering around him" [syn: {insensible(p)},
         {unaffected(p)}]
     3: free of artificiality; sincere and genuine; "an unaffected
        grace" [ant: {affected}]
     4: not touched emotionally; "was left untouched by the music"
        [syn: {unaffected(p)}, {untouched(p)}]
