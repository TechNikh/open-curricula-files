---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Difference_of_two_squares
offline_file: ""
offline_thumbnail: ""
uuid: 3688a5d2-efd5-49c7-a871-d89fde0d2942
updated: 1484308905
title: Difference of two squares
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rhombus_understood_analytically.svg.png
tags:
    - Proof
    - Geometrical demonstrations
    - Uses
    - Factorisation of polynomials
    - 'Complex number case: sum of two squares'
    - Rationalising denominators
    - Mental arithmetic
    - Difference of two perfect squares
    - Generalizations
    - Difference of two nth powers
    - Notes
categories:
    - Elementary algebra
---
In mathematics, the difference of two squares is a squared (multiplied by itself) number subtracted from another squared number. Every difference of squares may be factored according to the identity
