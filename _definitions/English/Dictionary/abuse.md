---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abuse
offline_file: ""
offline_thumbnail: ""
uuid: 85c1a409-f5c1-47e8-a36a-291216e6db74
updated: 1484310168
title: abuse
categories:
    - Dictionary
---
abuse
     n 1: cruel or inhumane treatment [syn: {maltreatment}, {ill-treatment},
           {ill-usage}]
     2: a rude expression intended to offend or hurt; "when a
        student made a stupid mistake he spared them no abuse";
        "they yelled insults at the visiting team" [syn: {insult},
         {revilement}, {contumely}, {vilification}]
     3: improper or excessive use [syn: {misuse}]
     v 1: treat badly; "This boss abuses his workers"; "She is always
          stepping on others to get ahead" [syn: {mistreat}, {maltreat},
           {ill-use}, {step}, {ill-treat}]
     2: change the inherent purpose or function of something; "Don't
        abuse the system"; "The director ...
