---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crime
offline_file: ""
offline_thumbnail: ""
uuid: 37c7e208-8300-4df2-b947-7893f4acb992
updated: 1484310591
title: crime
categories:
    - Dictionary
---
crime
     n 1: (criminal law) an act punishable by law; usually considered
          an evil act; "a long record of crimes" [syn: {law-breaking}]
     2: an evil act not necessarily punishable by law; "crimes of
        the heart"
