---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/underemployed
offline_file: ""
offline_thumbnail: ""
uuid: 86696e27-6c67-4a3b-a533-d71d247f7661
updated: 1484310455
title: underemployed
categories:
    - Dictionary
---
underemployed
     adj : employed only part-time when one needs full-time employment
           or not making full use of your skills; "migrants are
           likely to be poor and underemployed"; "able people are
           kept underemployed"
