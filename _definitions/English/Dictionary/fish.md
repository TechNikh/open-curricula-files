---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fish
offline_file: ""
offline_thumbnail: ""
uuid: 9624658c-a01c-4846-9b7e-203aeb5f762e
updated: 1484310286
title: fish
categories:
    - Dictionary
---
fish
     n 1: any of various mostly cold-blooded aquatic vertebrates
          usually having scales and breathing through gills; "the
          shark is a large fish"; "in the livingroom there was a
          tank of colorful fish"
     2: the flesh of fish used as food; "in Japan most fish is eaten
        raw"; "after the scare about foot-and-mouth disease a lot
        of people started eating fish instead of meat"; "they have
        a chef who specializes in fish"
     3: (astrology) a person who is born while the sun is in Pisces
        [syn: {Pisces}]
     4: the twelfth sign of the zodiac; the sun is in this sign from
        about February 19 to March 20 [syn: {Pisces}, {Pisces ...
