---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smith
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484637721
title: smith
categories:
    - Dictionary
---
Smith
     n 1: Rhodesian statesman who declared independence of Zimbabwe
          from Great Britain (born in 1919) [syn: {Ian Smith}, {Ian
          Douglas Smith}]
     2: United States sculptor (1906-1965) [syn: {David Smith}, {David
        Roland Smith}]
     3: United States singer noted for her rendition of patriotic
        songs (1909-1986) [syn: {Kate Smith}, {Kathryn Elizabeth
        Smith}]
     4: United States suffragist who refused to pay taxes until she
        could vote (1792-1886) [syn: {Julia Evelina Smith}]
     5: United States blues singer (1894-1937) [syn: {Bessie Smith}]
     6: religious leader who founded the Mormon Church in 1830
        (1805-1844) [syn: ...
