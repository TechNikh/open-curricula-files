---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Henri_Brocard
offline_file: ""
offline_thumbnail: ""
uuid: 94745d35-0c9a-4066-acab-7b3c25e5356b
updated: 1484309077
title: Henri Brocard
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ecole_Polytechnique_France_seen_from_lake_DSC03389.JPG
tags:
    - Biography
    - Early years
    - École Polytechnique and military years
    - Middle years
    - Discovery of Brocard points
    - Later years
    - Contributions
    - Brocard triangle, Brocard circle, and Brocard points
    - Other mathematical contributions
    - Meteorology
    - Notes
categories:
    - Geometers
---
Pierre René Jean Baptiste Henri Brocard (12 May 1845 – 16 January 1922) was a French meteorologist and mathematician, in particular a geometer.[1] His best-known achievement is the invention and discovery of the properties of the Brocard points, the Brocard circle, and the Brocard triangle, all bearing his name.[2]
