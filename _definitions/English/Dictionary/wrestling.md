---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wrestling
offline_file: ""
offline_thumbnail: ""
uuid: d3ae0e4e-4a0b-4aa3-898d-6ef22a643b75
updated: 1484310603
title: wrestling
categories:
    - Dictionary
---
wrestling
     n 1: the act of engaging in close hand-to-hand combat; "they had
          a fierce wrestle"; "we watched his grappling and
          wrestling with the bully" [syn: {wrestle}, {grapple}, {grappling},
           {hand-to-hand struggle}]
     2: the sport of hand-to-hand struggle between unarmed
        contestants who try to throw each other down [syn: {rassling},
         {grappling}]
