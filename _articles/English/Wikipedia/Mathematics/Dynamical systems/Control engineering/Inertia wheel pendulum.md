---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inertia_wheel_pendulum
offline_file: ""
offline_thumbnail: ""
uuid: ef1c06e9-1506-4263-b9e6-98780aafc72e
updated: 1484308735
title: Inertia wheel pendulum
categories:
    - Control engineering
---
An inertia wheel pendulum is a pendulum with an inertia wheel attached. It can be used as a pedagogical problem in control theory. This type of pendulum is often confused with the gyroscopic effect, which has completely different physical nature.
