---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/custodial
offline_file: ""
offline_thumbnail: ""
uuid: 236ea9af-b479-40c3-9a14-45eddd6577fc
updated: 1484310166
title: custodial
categories:
    - Dictionary
---
custodial
     adj : providing protective supervision; watching over or
           safeguarding; "daycare that is educational and not just
           custodial"; "a guardian angel"; "tutelary gods" [syn: {guardian},
            {tutelary}, {tutelar}]
