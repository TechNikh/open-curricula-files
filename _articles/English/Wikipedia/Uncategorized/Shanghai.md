---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shanghai
offline_file: ""
offline_thumbnail: ""
uuid: 54deca70-4d52-43ce-bd9d-a1f26266ee97
updated: 1484308485
title: Shanghai
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/135px-Shanghai_%2528Chinese_characters%2529.svg.png'
tags:
    - Names
    - History
    - Ancient history
    - Imperial history
    - Early modern history
    - Modern history
    - geography
    - Climate
    - Cityscape
    - Politics
categories:
    - Uncategorized
---
Shanghai (Chinese: 上海; pinyin: Shànghǎi; Shanghainese: Zånhae IPA: [z̻ɑ̃²² hᴇ⁴⁴]), often abbreviated as Hu (simplified Chinese: 沪; traditional Chinese: 滬; pinyin: Hù) or Shen (Chinese: 申; pinyin: Shēn), is one of the four direct-controlled municipalities of the People's Republic of China. It is the most populous city proper in the world, with a population of more than 24 million as of 2014[update].[9][10] It is a global financial centre[11] and transport hub, with the world's busiest container port.[12] Located in the Yangtze River Delta in East China, Shanghai sits on the south edge of the mouth of the Yangtze in the middle portion of the Chinese coast. The ...
