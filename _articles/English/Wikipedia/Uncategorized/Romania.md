---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Romania
offline_file: ""
offline_thumbnail: ""
uuid: 547225ba-b7fa-480e-b4bc-27ea13646f6e
updated: 1484094290
title: Romania
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Writ_of_the_Wallachian_voivode_Radu_cel_Frumos_from_14_October_1465.jpg
tags:
    - Etymology
    - Official names
    - History
    - Early history
    - Middle Ages
    - Independence and monarchy
    - World Wars and Greater Romania
    - Communism
    - Contemporary period
    - NATO and EU integration
categories:
    - Uncategorized
---
Romania[a] (i/roʊˈmeɪniə/ roh-MAY-nee-ə; Romanian: România i[romɨˈni.a]) is a sovereign state located in Southeastern Europe. It borders the Black Sea, Bulgaria, Ukraine, Hungary, Serbia, and Moldova. It has an area of 238,391 square kilometres (92,043 sq mi) and a temperate-continental climate. With 19.94 million inhabitants, the country is the seventh most populous member state of the European Union. The capital and largest city, Bucharest, with its 1,883,425 inhabitants is the sixth largest city in the EU.[8]
