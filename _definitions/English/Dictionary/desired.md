---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/desired
offline_file: ""
offline_thumbnail: ""
uuid: cb6f8752-5d4d-426b-a333-4a05e28e2860
updated: 1484310411
title: desired
categories:
    - Dictionary
---
desired
     adj 1: greatly desired [syn: {coveted}, {in demand(p)}, {sought
            after}]
     2: wanted intensely; "the child could no longer resist taking
        one of the craved cookies"; "it produced the desired
        effect" [syn: {craved}]
