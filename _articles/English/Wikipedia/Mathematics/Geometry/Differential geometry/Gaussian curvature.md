---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gaussian_curvature
offline_file: ""
offline_thumbnail: ""
uuid: 51f5fad7-5baf-4709-8645-ff6096d34ffb
updated: 1484309221
title: Gaussian curvature
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gaussian_curvature.svg.png
tags:
    - Informal definition
    - Relation to geometries
    - Further informal discussion
    - Alternative definitions
    - Total curvature
    - Important theorems
    - Theorema egregium
    - Gauss–Bonnet theorem
    - Surfaces of constant curvature
    - Alternative formulas
    - Books
categories:
    - Differential geometry
---
In differential geometry, the Gaussian curvature or Gauss curvature Κ of a surface at a point is the product of the principal curvatures, κ1 and κ2, at the given point:
