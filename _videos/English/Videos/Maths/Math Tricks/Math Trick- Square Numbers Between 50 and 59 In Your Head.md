---
version: 1
type: video
provider: YouTube
id: v9X1j7DspH4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2cdc2a0f-28bb-4dd5-9120-e8f69515ed69
updated: 1486069623
title: 'Math Trick- Square Numbers Between 50 and 59 In Your Head'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/v9X1j7DspH4/default.jpg
    - https://i3.ytimg.com/vi/v9X1j7DspH4/1.jpg
    - https://i3.ytimg.com/vi/v9X1j7DspH4/2.jpg
    - https://i3.ytimg.com/vi/v9X1j7DspH4/3.jpg
---
