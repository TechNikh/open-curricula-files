---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Random_walk_closeness_centrality
offline_file: ""
offline_thumbnail: ""
uuid: 971835b2-3c93-4ef6-9bca-4e5046054559
updated: 1484308630
title: Random walk closeness centrality
tags:
    - Intuition
    - Definition
    - Mean first passage time
    - Random walk closeness centrality in model networks
    - Applications for real networks
    - Random walk betweenness centrality
categories:
    - Graph theory
---
Random walk closeness centrality is a measure of centrality in a network, which describes the average speed with which randomly walking processes reach a node from other nodes of the network. It is similar to the closeness centrality except that the farness is measured by the expected length of a random walk rather than by the shortest path.
