---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/xii
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484329081
title: xii
categories:
    - Dictionary
---
xii
     adj : denoting a quantity consisting of 12 items or units [syn: {twelve},
            {12}, {dozen}]
     n : the cardinal number that is the sum of eleven and one [syn:
         {twelve}, {12}, {dozen}]
