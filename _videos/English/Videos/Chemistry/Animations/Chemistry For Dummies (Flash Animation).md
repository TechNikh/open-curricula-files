---
version: 1
type: video
provider: YouTube
id: d-YBq2PX-4o
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Chemistry%20For%20Dummies%20%28Flash%20Animation%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1272555e-4bad-4b49-830a-b04bb8ef6ae5
updated: 1486069662
title: Chemistry For Dummies (Flash Animation)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/d-YBq2PX-4o/default.jpg
    - https://i3.ytimg.com/vi/d-YBq2PX-4o/1.jpg
    - https://i3.ytimg.com/vi/d-YBq2PX-4o/2.jpg
    - https://i3.ytimg.com/vi/d-YBq2PX-4o/3.jpg
---
