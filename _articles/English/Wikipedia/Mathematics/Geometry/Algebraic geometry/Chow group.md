---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chow_group
offline_file: ""
offline_thumbnail: ""
uuid: a2e32999-c620-49e5-935b-0e0cbbc55484
updated: 1484309136
title: Chow group
tags:
    - Rational equivalence and Chow groups
    - The Chow ring
    - Examples
    - Functoriality
    - Cycle maps
    - Relation to K-theory
    - Conjectures
    - Variants
    - History
categories:
    - Algebraic geometry
---
In algebraic geometry, the Chow groups (named after W. L. Chow by Chevalley (1958)) of an algebraic variety over any field are algebro-geometric analogs of the homology of a topological space. The elements of the Chow group are formed out of subvarieties (so-called algebraic cycles) in a similar way to how simplicial or cellular homology groups are formed out of subcomplexes. When the variety is smooth, the Chow groups can be interpreted as cohomology groups (compare Poincare duality) and have a multiplication called the intersection product. The Chow groups carry rich information about an algebraic variety, and they are correspondingly hard to compute in general.
