---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defining
offline_file: ""
offline_thumbnail: ""
uuid: 7fd8cbbd-ac40-416f-9ee6-7d0c20d9e73f
updated: 1484310236
title: defining
categories:
    - Dictionary
---
defining
     n : any process serving to define the shape of something [syn: {shaping}]
