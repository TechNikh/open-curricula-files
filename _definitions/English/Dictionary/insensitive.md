---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insensitive
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484463421
title: insensitive
categories:
    - Dictionary
---
insensitive
     adj 1: not responsive to physical stimuli; "insensitive to
            radiation" [ant: {sensitive}]
     2: deficient in human sensibility; not mentally or morally
        sensitive; "insensitive to the needs of the patients"
        [ant: {sensitive}]
