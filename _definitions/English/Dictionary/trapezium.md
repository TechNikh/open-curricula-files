---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trapezium
offline_file: ""
offline_thumbnail: ""
uuid: d9a773a6-449f-4ba1-9b84-d4af58f7df81
updated: 1484310140
title: trapezium
categories:
    - Dictionary
---
trapezium
     n 1: a quadrilateral with no parallel sides [ant: {parallelogram}]
     2: a multiple star in the constellation of Orion [syn: {the
        Trapezium}]
     3: the wrist bone on the thumb side of the hand that
        articulates with the 1st and 2nd metacarpals [syn: {trapezium
        bone}, {os trapezium}]
     [also: {trapezia} (pl)]
