---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bum
offline_file: ""
offline_thumbnail: ""
uuid: 3403d1e5-bb09-4dca-9dd2-89ce0f02104f
updated: 1484310439
title: bum
categories:
    - Dictionary
---
bum
     adj : of very poor quality [syn: {cheap}, {cheesy}, {chintzy}, {crummy},
            {punk}, {sleazy}, {tinny}]
     n 1: a person who is deemed to be despicable or contemptible;
          "only a rotter would do that"; "kill the rat"; "throw
          the bum out"; "you cowardly little pukes!"; "the British
          call a contemptible person a `git'" [syn: {rotter}, {dirty
          dog}, {rat}, {skunk}, {stinker}, {stinkpot}, {puke}, {crumb},
           {lowlife}, {scum bag}, {so-and-so}, {git}]
     2: a disreputable vagrant; "a homeless tramp"; "he tried to
        help the really down-and-out bums" [syn: {tramp}, {hobo}]
     3: person who does no work; "a lazy bum" [syn: ...
