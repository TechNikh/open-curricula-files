---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circle_of_antisimilitude
offline_file: ""
offline_thumbnail: ""
uuid: e4b1b3cb-8b23-45f4-9e40-dd57d7ce0bb1
updated: 1484309258
title: Circle of antisimilitude
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Circle_of_antisimilitude0.svg.png
tags:
    - Properties
    - For three circles
categories:
    - Inversive geometry
---
In inversive geometry, the circle of antisimilitude (also known as mid-circle) of two circles, α and β, is a reference circle for which α and β are inverses of each other. If α and β are non-intersecting or tangent, a single circle of antisimilitude exists; if α and β intersect at two points, there are two circles of antisimilitude. When α and β are congruent, the circle of antisimilitude degenerates to a line of symmetry through which α and β are reflections of each other.[1][2]
