---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Stefan%E2%80%93Boltzmann_constant'
offline_file: ""
offline_thumbnail: ""
uuid: c043b018-7eb7-4746-a662-6dc8dec67d99
updated: 1484308864
title: Stefan–Boltzmann constant
categories:
    - Thermodynamics
---
The Stefan–Boltzmann constant (also Stefan's constant), a physical constant denoted by the Greek letter σ (sigma), is the constant of proportionality in the Stefan–Boltzmann law: "the total intensity radiated over all wavelengths increases as the temperature increases", of a black body which is proportional to the fourth power of the thermodynamic temperature.[1] The theory of thermal radiation lays down the theory of quantum mechanics, by using physics to relate to molecular, atomic and sub-atomic levels. Austrian physicist Josef Stefan formulated the constant in 1879, and it was later derived in 1884 by Austrian physicist Ludwig Boltzmann.[2] The equation can also be derived from ...
