---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forward
offline_file: ""
offline_thumbnail: ""
uuid: e8f8b368-c6b7-45f6-a07d-31f47e5ac247
updated: 1484310341
title: forward
categories:
    - Dictionary
---
forward
     adj 1: at or near or directed toward the front; "the forward
            section of the aircraft"; "a forward plunge down the
            stairs"; "forward motion" [ant: {backward}]
     2: moving toward a position ahead; "forward motion"; "the
        onward course of events" [syn: {onward}]
     3: used of temperament or behavior; lacking restraint or
        modesty; "a forward child badly in need of discipline"
        [ant: {backward}]
     4: of the transmission gear causing forward movement in a motor
        vehicle; "in a forward gear" [ant: {reverse}]
     5: moving forward [syn: {advancing}, {forward-moving}]
     6: situated at or toward the front; "the fore ...
