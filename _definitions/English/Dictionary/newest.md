---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/newest
offline_file: ""
offline_thumbnail: ""
uuid: cee38d54-2a59-4879-8bb1-1f8420e20f31
updated: 1484310605
title: newest
categories:
    - Dictionary
---
newest
     adj : in accord with the most fashionable ideas or style; "wears
           only the latest style"; "the last thing in swimwear";
           "knows the newest dances"; "cutting-edge technology";
           "a with-it boutique" [syn: {latest}, {last}, {up-to-date},
            {cutting-edge}, {with-it}]
