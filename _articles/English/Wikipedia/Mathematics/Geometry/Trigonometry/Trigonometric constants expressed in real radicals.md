---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Trigonometric_constants_expressed_in_real_radicals
offline_file: ""
offline_thumbnail: ""
uuid: 2f47420d-bf85-4154-acf0-0f688a44578c
updated: 1484309341
title: Trigonometric constants expressed in real radicals
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Unit_circle_angles_color.svg.png
tags:
    - Scope of this article
    - Table of some common angles
    - Further angles
    - '0°: fundamental'
    - '1.5°: regular hecatonicosagon (120-sided polygon)'
    - '1.875°: regular enneacontahexagon (96-sided polygon)'
    - '2.25°: regular octacontagon (80-sided polygon)'
    - '2.8125°: regular hexacontatetragon (64-sided polygon)'
    - '3°: regular hexacontagon (60-sided polygon)'
    - '3.75°: regular tetracontaoctagon (48-sided polygon)'
    - '4.5°: regular tetracontagon (40-sided polygon)'
    - '5.625°: regular triacontadigon (32-sided polygon)'
    - '6°: regular triacontagon (30-sided polygon)'
    - '7.5°: regular icositetragon (24-sided polygon)'
    - '9°: regular icosagon (20-sided polygon)'
    - '11.25°: regular hexadecagon (16-sided polygon)'
    - '12°: regular pentadecagon (15-sided polygon)'
    - '15°: regular dodecagon (12-sided polygon)'
    - '18°: regular decagon (10-sided polygon)[1]'
    - '21°: sum 9° + 12°'
    - '22.5°: regular octagon'
    - '24°: sum 12° + 12°'
    - '27°: sum 12° + 15°'
    - '30°: regular hexagon'
    - '33°: sum 15° + 18°'
    - '36°: regular pentagon[1]'
    - '39°: sum 18° + 21°'
    - '42°: sum 21° + 21°'
    - '45°: square'
    - '54°: sum 27° + 27°'
    - '60°: equilateral triangle'
    - '67.5°: sum 7.5° + 60°'
    - '72°: sum 36° + 36°'
    - '75°: sum 30° + 45°'
    - '90°: fundamental'
    - |
        List of trigonometric constants to
        
        
        
        
        (
        
        
        
        2
        π
        
        n
        
        
        )
    - Notes
    - Uses for constants
    - Derivation triangles
    - Calculated trigonometric values for sine and cosine
    - The trivial values
    - n × π/(5 × 2m)
    - Geometrical method
    - Algebraic method
    - n × π/20
    - n × π/30
    - n × π/60
    - Strategies for simplifying expressions
    - Rationalizing the denominator
    - Splitting a fraction in two
    - Squaring and taking square roots
    - Simplifying nested radical expressions
categories:
    - Trigonometry
---
Exact algebraic expressions for trigonometric values are sometimes useful, mainly for simplifying solutions into radical forms which allow further simplification.
