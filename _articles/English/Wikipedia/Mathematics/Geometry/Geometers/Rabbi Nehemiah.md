---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rabbi_Nehemiah
offline_file: ""
offline_thumbnail: ""
uuid: e6eabe78-7fc5-4223-95b7-8d85294a6870
updated: 1484309093
title: Rabbi Nehemiah
categories:
    - Geometers
---
He is attributed as the author of the Mishnat ha-Middot (ca. AD 150), making it the earliest known Hebrew text on geometry, although some historians assign the text to a later period by an unknown author.
