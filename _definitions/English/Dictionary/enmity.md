---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enmity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484570821
title: enmity
categories:
    - Dictionary
---
enmity
     n 1: a state of deep-seated ill-will [syn: {hostility}, {antagonism}]
     2: the feeling of a hostile person; "he could no longer contain
        his hostility" [syn: {hostility}, {ill will}]
