---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clear-sighted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484465701
title: clear-sighted
categories:
    - Dictionary
---
clear-sighted
     adj 1: having sharp clear vision
     2: mentally acute or penetratingly discerning; "too clear-eyed
        not to see what problems would follow"; "chaos could be
        prevented only by clear-sighted leadership"; "much too
        perspicacious to be taken in by so spurious an argument"
        [syn: {clear-eyed}, {perspicacious}]
