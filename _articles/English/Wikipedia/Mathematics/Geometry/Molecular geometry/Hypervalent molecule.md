---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hypervalent_molecule
offline_file: ""
offline_thumbnail: ""
uuid: 3d1572fb-d73e-447f-9edd-7d52ff4a70c6
updated: 1484309277
title: Hypervalent molecule
tags:
    - Definitions and nomenclature
    - N-X-L notation
    - History and controversy
    - Criticism
    - Alternative definition
    - Bonding in hypervalent molecules
    - Molecular orbital theory
    - Valence bond theory
    - Structure, reactivity, and kinetics
    - Structure
    - Hexacoordinated phosphorus
    - Pentacoordinated silicon
    - Reactivity
    - Silicon
    - Phosphorus
    - Ab initio calculations
    - Application
categories:
    - Molecular geometry
---
A hypervalent molecule (the phenomenon is sometimes colloquially known as expanded octet) is a molecule that contains one or more main group elements formally bearing more than eight electrons in their valence shells. Phosphorus pentachloride (PCl5), sulfur hexafluoride (SF6), chlorine trifluoride (ClF3), and the triiodide (I3−) ion are examples of hypervalent molecules.
