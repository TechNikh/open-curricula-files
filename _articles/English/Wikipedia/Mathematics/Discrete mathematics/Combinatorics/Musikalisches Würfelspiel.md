---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Musikalisches_W%C3%BCrfelspiel'
offline_file: ""
offline_thumbnail: ""
uuid: 92d0ba83-d5e2-4a37-bd04-42b800b328f4
updated: 1484308585
title: Musikalisches Würfelspiel
tags:
    - Aesthetics
    - Alleged musikalisches Würfelspiel
categories:
    - Combinatorics
---
A Musikalisches Würfelspiel (German for "musical dice game") was a system for using dice to randomly 'generate' music from precomposed options. These 'games' were quite popular throughout Western Europe in the 18th century. Several different games were devised, some that did not require dice, but merely 'choosing a random number.'
