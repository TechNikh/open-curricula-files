---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Draper_point
offline_file: ""
offline_thumbnail: ""
uuid: a0873b98-e2f4-4312-b45b-aabfbd9606aa
updated: 1484308840
title: Draper point
categories:
    - Thermodynamics
---
The Draper point is the approximate temperature above which almost all solid materials visibly glow as a result of blackbody radiation. It was established at 977 °F (525 °C, 798 K) by John William Draper in 1847.[1][2][3]
