---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/match
offline_file: ""
offline_thumbnail: ""
uuid: 78f0cd20-5f72-42c7-93ab-0072625e886c
updated: 1484310221
title: match
categories:
    - Dictionary
---
match
     n 1: lighter consisting of a thin piece of wood or cardboard
          tipped with combustible chemical; ignites with friction;
          "he always carries matches to light his pipe" [syn: {lucifer},
           {friction match}]
     2: a formal contest in which two or more persons or teams
        compete
     3: a burning piece of wood or cardboard; "if you drop a match
        in there the whole place will explode"
     4: an exact duplicate; "when a match is found an entry is made
        in the notebook" [syn: {mate}]
     5: the score needed to win a match
     6: a person regarded as a good matrimonial prospect [syn: {catch}]
     7: a person who is of equal standing with ...
