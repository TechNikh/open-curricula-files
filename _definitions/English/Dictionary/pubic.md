---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pubic
offline_file: ""
offline_thumbnail: ""
uuid: d48f5cda-1e34-48c8-a9f1-37c3bd4b7e8a
updated: 1484310475
title: pubic
categories:
    - Dictionary
---
pubic
     adj : relating or near the pubis; "pubic bones"; "pubic hair"
