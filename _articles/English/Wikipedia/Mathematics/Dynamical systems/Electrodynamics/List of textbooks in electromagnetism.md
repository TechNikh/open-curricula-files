---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_textbooks_in_electromagnetism
offline_file: ""
offline_thumbnail: ""
uuid: 9218e673-fdba-430c-9232-1d151dfdb6d8
updated: 1484308759
title: List of textbooks in electromagnetism
tags:
    - University level textbooks
    - Undergraduate
    - Graduate
    - Older classics
    - Computational techniques
categories:
    - Electrodynamics
---
