---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/degeneration
offline_file: ""
offline_thumbnail: ""
uuid: a12f916f-e07c-48fd-8c5b-4ac245f4b18b
updated: 1484310549
title: degeneration
categories:
    - Dictionary
---
degeneration
     n 1: the process of declining from a higher to a lower level of
          effective power or vitality or essential quality [syn: {devolution}]
          [ant: {development}]
     2: the state of being degenerate in mental or moral qualities
        [syn: {degeneracy}, {decadence}, {decadency}]
     3: passing from a more complex to a simpler biological form
        [syn: {retrogression}]
