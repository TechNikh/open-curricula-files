---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kirchhoff%27s_diffraction_formula'
offline_file: ""
offline_thumbnail: ""
uuid: f439dbe2-4970-4b19-be9c-aa3eda7c0ca6
updated: 1484308885
title: "Kirchhoff's diffraction formula"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Kirchhoff_1.svg.png
tags:
    - "Derivation of Kirchhoff's diffraction formula"
    - Point source
    - Equivalence to Huygens–Fresnel equation
    - Extended source
    - Fraunhofer and Fresnel diffraction equations
    - Fraunhofer diffraction
    - Fresnel diffraction
categories:
    - Waves
---
Kirchhoff's diffraction formula[1][2] (also Fresnel–Kirchhoff diffraction formula) can be used to model the propagation of light in a wide range of configurations, either analytically or using numerical modelling. It gives an expression for the wave disturbance when a monochromatic spherical wave passes through an opening in an opaque screen. The equation is derived by making several approximations to the Kirchhoff integral theorem which uses Green's theorem to derive the solution to the homogeneous wave equation.
