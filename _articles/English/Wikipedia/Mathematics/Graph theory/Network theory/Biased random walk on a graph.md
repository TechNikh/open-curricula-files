---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biased_random_walk_on_a_graph
offline_file: ""
offline_thumbnail: ""
uuid: 1c33c87d-e35b-4554-bef5-1e371e8e3eea
updated: 1484309434
title: Biased random walk on a graph
tags:
    - Model
    - Applications
categories:
    - Network theory
---
In network science, a biased random walk on a graph is a time path process in which an evolving variable jumps from its current state to one of various potential new states; unlike in a pure random walk, the probabilities of the potential new states are unequal.
