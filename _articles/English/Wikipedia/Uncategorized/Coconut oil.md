---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coconut_oil
offline_file: ""
offline_thumbnail: ""
uuid: 2c9fa713-594f-4ba9-8466-a0b48c5607e8
updated: 1484308372
title: Coconut oil
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Coconut_and_oil.jpg
tags:
    - Production
    - Dry process
    - Wet process
    - Refined oil
    - Hydrogenation
    - Fractionation
    - Figures
    - Standards
    - Composition and comparison
    - Health claims
    - Uses
    - In food
    - Industry
    - Personal uses
categories:
    - Uncategorized
---
Coconut oil, or copra oil, is an edible oil extracted from the kernel or meat of mature coconuts harvested from the coconut palm (Cocos nucifera). It has various applications. Because of its high saturated fat content, it is slow to oxidize and, thus, resistant to rancidification, lasting up to six months at 24 °C (75 °F) without spoiling.[1]
