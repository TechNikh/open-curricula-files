---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conductivity
offline_file: ""
offline_thumbnail: ""
uuid: 06d86525-f4b6-4cee-9155-3c06db502d77
updated: 1484310202
title: conductivity
categories:
    - Dictionary
---
conductivity
     n : the transmission of heat or electricity or sound [syn: {conduction}]
