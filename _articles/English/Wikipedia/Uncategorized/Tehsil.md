---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mandal
offline_file: ""
offline_thumbnail: ""
uuid: 13dc8dbb-ccc8-4ef6-af7f-e1abf987fb9f
updated: 1484308438
title: Tehsil
tags:
    - India
    - Pakistan
categories:
    - Uncategorized
---
A tehsil (also known as a mandal or taluka) is an administrative division of some countries of South Asia. It is an area of land with a city or town that serves as its administrative centre, with possible additional towns, and usually a number of villages. The terms in India have replaced earlier geographical terms, such as pargana, pergunnah and thannah, used under the Delhi Sultanate and the British Raj.
