---
version: 1
type: video
provider: YouTube
id: --bZUeb83uU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/How%20Does%20Blood%20Clot.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 22f70073-1cf8-49eb-86b8-55dfbbfdeb8a
updated: 1486069662
title: How Does Blood Clot
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/--bZUeb83uU/default.jpg
    - https://i3.ytimg.com/vi/--bZUeb83uU/1.jpg
    - https://i3.ytimg.com/vi/--bZUeb83uU/2.jpg
    - https://i3.ytimg.com/vi/--bZUeb83uU/3.jpg
---
