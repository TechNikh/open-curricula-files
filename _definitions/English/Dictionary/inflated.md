---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inflated
offline_file: ""
offline_thumbnail: ""
uuid: a4394304-6614-41e4-a3d3-bb5a915a137e
updated: 1484310303
title: inflated
categories:
    - Dictionary
---
inflated
     adj 1: enlarged beyond truth or reasonableness; "had an exaggerated
            (or inflated) opinion of himself"; "a hyperbolic
            style" [syn: {exaggerated}, {hyperbolic}]
     2: expanded by (or as if by) gas or air; "an inflated balloon"
        [ant: {deflated}]
     3: pretentious (especially with regard to language or ideals);
        "high-flown talk of preserving the moral tone of the
        school"; "a high-sounding dissertation on the means to
        attain social revolution" [syn: {high-flown}, {high-sounding}]
     4: increased especially to abnormal levels; "the raised prices
        frightened away customers"; "inflated wages"; "an inflated
        ...
