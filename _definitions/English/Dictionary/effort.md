---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/effort
offline_file: ""
offline_thumbnail: ""
uuid: 6ab388d6-37e7-4b7a-b55e-5737ca748ec7
updated: 1484310258
title: effort
categories:
    - Dictionary
---
effort
     n 1: earnest and conscientious activity intended to do or
          accomplish something; "made an effort to cover all the
          reading material"; "wished him luck in his endeavor";
          "she gave it a good try" [syn: {attempt}, {endeavor}, {endeavour},
           {try}]
     2: use of physical or mental energy; hard work; "he got an A
        for effort"; "they managed only with great exertion" [syn:
         {elbow grease}, {exertion}, {travail}, {sweat}]
     3: a series of actions advancing a principle or tending toward
        a particular end; "he supported populist campaigns"; "they
        worked in the cause of world peace"; "the team was ready
        for a ...
