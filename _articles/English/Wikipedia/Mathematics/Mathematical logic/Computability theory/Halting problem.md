---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Halting_problem
offline_file: ""
offline_thumbnail: ""
uuid: bf2d87ff-cac5-4b34-a72e-afef42bc2016
updated: 1484309485
title: Halting problem
tags:
    - Background
    - Importance and consequences
    - Representation as a set
    - Sketch of proof
    - >
        Proof as a corollary of the uncomputability of Kolmogorov
        complexity
    - Common pitfalls
    - Formalization
    - "Relationship with Gödel's incompleteness theorems"
    - Variants of the halting problem
    - Halting on all inputs
    - Recognizing partial solutions
    - Generalized to oracle machines
    - History
    - Avoiding the halting problem
    - Notes
categories:
    - Computability theory
---
In computability theory, the halting problem is the problem of determining, from a description of an arbitrary computer program and an input, whether the program will finish running or continue to run forever.
