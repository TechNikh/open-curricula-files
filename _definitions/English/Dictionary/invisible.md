---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invisible
offline_file: ""
offline_thumbnail: ""
uuid: d93f1a2c-476e-499a-8379-6c6d47e9af97
updated: 1484310391
title: invisible
categories:
    - Dictionary
---
invisible
     adj 1: impossible or nearly impossible to see; imperceptible by the
            eye; "the invisible man"; "invisible rays"; "an
            invisible hinge"; "invisible mending" [syn: {unseeable}]
            [ant: {visible}]
     2: not prominent or readily noticeable; "he pushed the string
        through an inconspicuous hole"; "the invisible man" [syn:
        {inconspicuous}] [ant: {conspicuous}]
