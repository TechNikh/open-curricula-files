---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phone
offline_file: ""
offline_thumbnail: ""
uuid: 77b06070-4789-4eea-bc45-cf726c6ee0ff
updated: 1484310451
title: phone
categories:
    - Dictionary
---
phone
     n 1: electronic equipment that converts sound into electrical
          signals that can be transmitted over distances and then
          converts received signals back into sounds; "I talked to
          him on the telephone" [syn: {telephone}, {telephone set}]
     2: (phonetics) an individual sound unit of speech without
        concern as to whether or not it is a phoneme of some
        language [syn: {speech sound}, {sound}]
     3: electro-acoustic transducer for converting electric signals
        into sounds; it is held over or inserted into the ear; "it
        was not the typing but the earphones that she disliked"
        [syn: {earphone}, {earpiece}, {headphone}]
    ...
