---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/utility
offline_file: ""
offline_thumbnail: ""
uuid: c20cb7ef-76ab-4975-91ff-57ad8e84703a
updated: 1484310170
title: utility
categories:
    - Dictionary
---
utility
     adj 1: used of beef; usable but inferior [syn: {utility(a)}, {utility-grade}]
     2: capable of substituting in any of several positions on a
        team; "a utility infielder" [syn: {utility(a)}, {substitute(a)}]
     n 1: a company that performs a public service; subject to
          government regulation [syn: {public utility}, {public-service
          corporation}]
     2: the quality of being of practical use [syn: {usefulness}]
        [ant: {inutility}, {inutility}]
     3: the service provided by a utility company; "the cost of
        utilities never decreases"
     4: (economics) a measure that is to be maximized in any
        situation involving choice
     5: ...
