---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crushing
offline_file: ""
offline_thumbnail: ""
uuid: 31dc2949-7f72-4910-bae3-abed28d6376e
updated: 1484310341
title: crushing
categories:
    - Dictionary
---
crushing
     adj : physically or spiritually devastating; often used in
           combination; "a crushing blow"; "a crushing rejection";
           "bone-crushing" [syn: {devastating}]
     n : forceful prevention; putting down by power or authority;
         "the suppression of heresy"; "the quelling of the
         rebellion"; "the stifling of all dissent" [syn: {suppression},
          {quelling}, {stifling}]
