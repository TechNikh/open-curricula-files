---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperbolic_group
offline_file: ""
offline_thumbnail: ""
uuid: eef14032-0152-4622-a1c1-9eb23a34d6d3
updated: 1484309266
title: Hyperbolic group
tags:
    - Definition
    - Remarks
    - Examples
    - Elementary hyperbolic groups
    - Free groups and groups acting on trees
    - Fuchsian groups
    - Negative curvature
    - Small cancellation groups
    - Random groups
    - Non-examples
    - Properties of hyperbolic groups
    - Algebraic properties
    - Geometric properties
    - Homological properties
    - Algorithmic properties
    - Generalisations
    - Relatively hyperbolic groups
    - Acylindrically hyperbolic groups
    - CAT(0) groups
    - Notes
categories:
    - Metric geometry
---
In group theory, more precisely in geometric group theory, a hyperbolic group, also known as a word hyperbolic group or Gromov hyperbolic group, is a finitely generated group equipped with a word metric satisfying certain properties abstracted from classical hyperbolic geometry. The notion of a hyperbolic group was introduced and developed by Gromov (1987). The inspiration came from various existing mathematical theories: hyperbolic geometry but also low-dimensional topology (in particular the results of Max Dehn concerning the fundamental group of a hyperbolic Riemann surface, and more complex phenomena in three-dimensional topology), and combinatorial group theory. In a very ...
