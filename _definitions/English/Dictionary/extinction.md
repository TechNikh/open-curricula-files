---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extinction
offline_file: ""
offline_thumbnail: ""
uuid: c099b88d-c285-450f-8b8c-c253d02d8a2c
updated: 1484310246
title: extinction
categories:
    - Dictionary
---
extinction
     n 1: no longer in existence; "the extinction of a species" [syn:
          {defunctness}]
     2: no longer active; extinguished; "the extinction of the
        volcano"
     3: the reduction of the intensity of radiation as a consequence
        of absorption and radiation
     4: complete annihilation; "they think a meteor cause the
        extinction of the dinosaurs" [syn: {extermination}]
     5: a conditioning process in which the reinforcer is removed
        and a conditioned response becomes independent of the
        conditioned stimulus [syn: {experimental extinction}]
     6: the act of extinguishing; causing to stop burning; "the
        extinction of the ...
