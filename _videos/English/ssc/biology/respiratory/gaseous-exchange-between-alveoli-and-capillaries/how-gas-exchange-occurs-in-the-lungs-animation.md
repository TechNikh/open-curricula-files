---
title: How Gas Exchange Occurs in the Lungs Animation
youtube_id: NBqVd36-Y94
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
