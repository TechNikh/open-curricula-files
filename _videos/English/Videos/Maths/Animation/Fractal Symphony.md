---
version: 1
type: video
provider: YouTube
id: cYBAjnMCmO8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4f6d2531-521b-4f36-af43-b6abbfac5160
updated: 1486069645
title: Fractal Symphony
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/cYBAjnMCmO8/default.jpg
    - https://i3.ytimg.com/vi/cYBAjnMCmO8/1.jpg
    - https://i3.ytimg.com/vi/cYBAjnMCmO8/2.jpg
    - https://i3.ytimg.com/vi/cYBAjnMCmO8/3.jpg
---
