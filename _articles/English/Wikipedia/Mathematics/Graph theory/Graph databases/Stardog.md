---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stardog
offline_file: ""
offline_thumbnail: ""
uuid: 26a35be7-b3d4-41fa-a52d-862a1fd2d636
updated: 1484309406
title: Stardog
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/He1523a_6.jpg
categories:
    - Graph databases
---
Stardog is a semantic graph database, implemented in Java. It provides support for RDF and all OWL 2 profiles providing extensive reasoning capabilities[1][2] and uses SPARQL as a query language.[3] APIs are available for the Jena and Sesame as well as SNARL, the Stardog Native API for the RDF Language. Bindings for its HTTP protocol are also available in Javascript, .Net (programming language), Ruby (programming language), Clojure, and Python. Support for the Spring (framework) is also available.
