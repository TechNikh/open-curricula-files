---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/propene
offline_file: ""
offline_thumbnail: ""
uuid: 3ab33eec-b77e-4c26-a92f-7ab937935ad3
updated: 1484310419
title: propene
categories:
    - Dictionary
---
propene
     n : a flammable gas obtained by cracking petroleum; used in
         organic synthesis [syn: {propylene}]
