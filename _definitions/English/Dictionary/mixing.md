---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mixing
offline_file: ""
offline_thumbnail: ""
uuid: fc615162-64e4-47fe-b7d2-cf6bf3f47b10
updated: 1484310339
title: mixing
categories:
    - Dictionary
---
mixing
     n : the act of mixing together; "paste made by a mix of flour
         and water"; "the mixing of sound channels in the
         recording studio" [syn: {mix}, {commixture}, {admixture},
          {mixture}, {intermixture}]
