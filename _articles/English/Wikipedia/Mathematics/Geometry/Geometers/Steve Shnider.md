---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Steve_Shnider
offline_file: ""
offline_thumbnail: ""
uuid: 79ef7004-7ef6-42a0-8daf-3cf49db51503
updated: 1484309098
title: Steve Shnider
tags:
    - Book on operads
    - Bibliography
    - Books
    - Selected papers
categories:
    - Geometers
---
Steve Shnider is professor of mathematics at Bar Ilan University.[1] He received a PhD in Mathematics from Harvard University in 1972, under Shlomo Sternberg.[2] His main interests are in the differential geometry of fiber bundles; algebraic methods in the theory of deformation of geometric structures; symplectic geometry; supersymmetry; operads; and Hopf algebras.
