---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Automated_reasoning
offline_file: ""
offline_thumbnail: ""
uuid: d820225f-2dbc-452b-95d3-c0be73811f97
updated: 1484308671
title: Automated reasoning
tags:
    - Early years
    - Significant contributions
    - Proof systems
    - Applications
    - Conferences and workshops
    - Journals
    - Communities
categories:
    - Theoretical computer science
---
Automated reasoning is an area of computer science and mathematical logic dedicated to understanding different aspects of reasoning. The study of automated reasoning helps produce computer programs that allow computers to reason completely, or nearly completely, automatically. Although automated reasoning is considered a sub-field of artificial intelligence, it also has connections with theoretical computer science, and even philosophy.
