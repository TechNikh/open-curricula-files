---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crow
offline_file: ""
offline_thumbnail: ""
uuid: c7766de3-a766-480d-804e-a01cf5b6987b
updated: 1484310221
title: crow
categories:
    - Dictionary
---
crow
     n 1: black birds having a raucous call
     2: the cry of a cock (or an imitation of it)
     3: a member of the Siouan people formerly living in eastern
        Montana
     4: a small quadrilateral constellation in the southern
        hemisphere near Virgo [syn: {Corvus}]
     5: an instance of boastful talk; "his brag is worse than his
        fight"; "whenever he won we were exposed to his gasconade"
        [syn: {brag}, {bragging}, {crowing}, {vaporing}, {line-shooting},
         {gasconade}]
     6: a Siouan language spoken by the Crow people
     v 1: dwell on with satisfaction [syn: {gloat}, {triumph}]
     2: express pleasure verbally; "She crowed with joy"
     3: ...
