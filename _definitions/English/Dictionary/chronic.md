---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chronic
offline_file: ""
offline_thumbnail: ""
uuid: 272f163f-770b-471f-9295-68177afec990
updated: 1484310252
title: chronic
categories:
    - Dictionary
---
chronic
     adj 1: being long-lasting and recurrent or characterized by long
            suffering; "chronic indigestion"; "a chronic shortage
            of funds"; "a chronic invalid" [ant: {acute}]
     2: having a habit of long standing; "a chronic smoker" [syn: {confirmed},
         {habitual}, {inveterate(a)}]
