---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pollen_grain
offline_file: ""
offline_thumbnail: ""
uuid: c498dd85-2354-43c2-8127-4db3080c7c62
updated: 1484308365
title: Pollen
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tulip_Stamen_Tip.jpg
tags:
    - The structure and formation of pollen
    - Formation
    - Structure
    - Pollination
    - Pollen in the fossil record
    - Allergy to pollen
    - Treatment
    - Nutrition
    - In humans
    - Parasites
    - Forensic palynology
    - Bibliography
categories:
    - Uncategorized
---
Pollen is a fine to coarse powdery substance comprising pollen grains which are male microgametophytes of seed plants, which produce male gametes (sperm cells). Pollen grains have a hard coat made of sporopollenin that protects the gametophytes during the process of their movement from the stamens to the pistil of flowering plants or from the male cone to the female cone of coniferous plants. If pollen lands on a compatible pistil or female cone, it germinates, producing a pollen tube that transfers the sperm to the ovule containing the female gametophyte. Individual pollen grains are small enough to require magnification to see detail. The study of pollen is called palynology and is highly ...
