---
version: 1
type: video
provider: YouTube
id: yB9HHyPpKds
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Comets-%20Crash%20Course%20Astronomy%20%2321.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 67c7914f-279e-4f00-a96e-c86bb947e2fa
updated: 1486069684
title: 'Comets- Crash Course Astronomy #21'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/yB9HHyPpKds/default.jpg
    - https://i3.ytimg.com/vi/yB9HHyPpKds/1.jpg
    - https://i3.ytimg.com/vi/yB9HHyPpKds/2.jpg
    - https://i3.ytimg.com/vi/yB9HHyPpKds/3.jpg
---
