---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/utilize
offline_file: ""
offline_thumbnail: ""
uuid: 5e3f66ee-c6b8-4670-a90b-4e089a520511
updated: 1484310260
title: utilize
categories:
    - Dictionary
---
utilize
     v 1: put into service; make work or employ (something) for a
          particular purpose or for its inherent or natural
          purpose; "use your head!"; "we only use Spanish at
          home"; "I can't make use of this tool"; "Apply a
          magnetic field here"; "This thinking was applied to many
          projects"; "How do you utilize this tool?"; "I apply
          this rule to get good results"; "use the plastic bags to
          store the food"; "He doesn't know how to use a computer"
          [syn: {use}, {utilise}, {apply}, {employ}]
     2: convert (from an investment trust to a unit trust)
