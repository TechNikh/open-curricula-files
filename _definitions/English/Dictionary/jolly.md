---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jolly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484449201
title: jolly
categories:
    - Dictionary
---
jolly
     adj : full of or showing high-spirited merriment; "when hearts
           were young and gay"; "a poet could not but be gay, in
           such a jocund company"- Wordsworth; "the jolly crowd at
           the reunion"; "jolly old Saint Nick"; "a jovial old
           gentleman"; "have a merry Christmas"; "peals of merry
           laughter"; "a mirthful laugh" [syn: {gay}, {jocund}, {jovial},
            {merry}, {mirthful}]
     n 1: a happy party
     2: a yawl used by a ship's sailors for general work [syn: {jolly
        boat}]
     adv : used as an intensifier (`jolly' is used informally in
           Britain); "pretty big"; "pretty bad"; "jolly decent of
           him" ...
