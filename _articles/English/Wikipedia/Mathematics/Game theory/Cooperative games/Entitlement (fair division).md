---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entitlement_(fair_division)
offline_file: ""
offline_thumbnail: ""
uuid: 22fc9ee7-a9a1-46bb-9839-425fd7319090
updated: 1484308531
title: Entitlement (fair division)
tags:
    - Shared costs or gains
    - Voting
    - Entitlement in the Talmud
categories:
    - Cooperative games
---
Entitlement in fair division describes that proportion of the resources or goods to be divided that a player can expect to receive. The idea is based on the normal idea of entitlement. Entitlements can in the main be determined by agreeing on a cooperative game and using its value as the entitlement.
