---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/politely
offline_file: ""
offline_thumbnail: ""
uuid: eeeb860d-d648-48e3-988d-f5dba6bc9a13
updated: 1484310561
title: politely
categories:
    - Dictionary
---
politely
     adv : in a polite manner; "the policeman answered politely, `Now
           look here, lady...'" [syn: {courteously}, {with
           courtesy}, {in a well mannered way}, {with politeness}]
           [ant: {impolitely}, {impolitely}]
