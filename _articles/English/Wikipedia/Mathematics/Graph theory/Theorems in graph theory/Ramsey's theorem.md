---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ramsey%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: db6c03b0-5bb0-4ed3-bafe-3f8c6564cb62
updated: 1484309454
title: "Ramsey's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-RamseyTheory_K5_no_mono_K3.svg.png
tags:
    - 'Example: R(3, 3) = 6'
    - Proof of the theorem
    - 2-colour case
    - General case
    - Ramsey numbers
    - Asymptotics
    - 'A multicolour example: R(3, 3, 3) = 17'
    - Extensions of the theorem
    - Infinite Ramsey theorem
    - Infinite version implies the finite
    - Directed graph Ramsey numbers
    - Notes
categories:
    - Theorems in graph theory
---
In combinatorial mathematics, Ramsey's theorem states that one will find monochromatic cliques in any edge labelling (with colours) of a sufficiently large complete graph. To demonstrate the theorem for two colours (say, blue and red), let r and s be any two positive integers. Ramsey's theorem states that there exists a least positive integer R(r, s) for which every blue-red edge colouring of the complete graph on R(r, s) vertices contains a blue clique on r vertices or a red clique on s vertices. (Here R(r, s) signifies an integer that depends on both r and s.)
