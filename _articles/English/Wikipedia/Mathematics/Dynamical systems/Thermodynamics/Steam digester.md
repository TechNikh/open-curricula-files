---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Steam_digester
offline_file: ""
offline_thumbnail: ""
uuid: 0bab4601-b535-4bc3-a27b-330a79b27a14
updated: 1484308857
title: Steam digester
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Papin%2527s_digester.gif'
tags:
    - History
categories:
    - Thermodynamics
---
The steam digester (or bone digester, and also known as Papin’s digester) is a high-pressure cooker invented by French physicist Denis Papin in 1679. It is a device for extracting fats from bones in a high-pressure steam environment, which also renders them brittle enough to be easily ground into bone meal. It is the forerunner of the autoclave and the domestic pressure cooker.[1]
