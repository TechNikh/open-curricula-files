---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conformal_Killing_equation
offline_file: ""
offline_thumbnail: ""
uuid: 0a1cb535-af8d-4d09-85b1-2429c2f854d8
updated: 1484309212
title: Conformal Killing equation
categories:
    - Differential geometry
---
In conformal geometry, the conformal Killing equation on a manifold of space-dimension n with metric 
  
    
      
        g
      
    
    {\displaystyle g}
  
 describes those vector fields 
  
    
      
        X
      
    
    {\displaystyle X}
  
 which preserve 
  
    
      
        g
      
    
    {\displaystyle g}
  
 up to scale, i.e.
