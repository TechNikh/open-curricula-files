---
version: 1
type: video
provider: YouTube
id: g6nGcnVB8BM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Exponent%20expressions%20and%20equations/Solving%20radical%20equations%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ef18933c-d952-451b-baac-87dfc6836695
updated: 1486069563
title: Solving radical equations 3
categories:
    - Exponent expressions and equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/g6nGcnVB8BM/default.jpg
    - https://i3.ytimg.com/vi/g6nGcnVB8BM/1.jpg
    - https://i3.ytimg.com/vi/g6nGcnVB8BM/2.jpg
    - https://i3.ytimg.com/vi/g6nGcnVB8BM/3.jpg
---
