---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deficient
offline_file: ""
offline_thumbnail: ""
uuid: a889cbc0-908a-49b7-bc15-02990c181e49
updated: 1484310405
title: deficient
categories:
    - Dictionary
---
deficient
     adj 1: inadequate in amount or degree; "a deficient education";
            "deficient in common sense"; "lacking in stamina";
            "tested and found wanting" [syn: {lacking(p)}, {wanting(p)}]
     2: of a quantity not able to fulfill a need or requirement;
        "insufficient funds" [syn: {insufficient}] [ant: {sufficient}]
     3: falling short of some prescribed norm; "substandard housing"
        [syn: {inferior}, {substandard}]
