---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/taxes
offline_file: ""
offline_thumbnail: ""
uuid: ea071dd7-3546-455d-aa02-27ae04fe68de
updated: 1484310531
title: taxes
categories:
    - Dictionary
---
tax
     n : charge against a citizen's person or property or activity
         for the support of government [syn: {taxation}, {revenue
         enhancement}]
     v 1: levy a tax on; "The State taxes alcohol heavily"; "Clothing
          is not taxed in our state"
     2: set or determine the amount of (a payment such as a fine)
        [syn: {assess}]
     3: use to the limit; "you are taxing my patience" [syn: {task}]
     4: make a charge against or accuse; "They taxed him failure to
        appear in court"
     [also: {taxes} (pl)]
