---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upward
offline_file: ""
offline_thumbnail: ""
uuid: 1bac2def-0a49-450c-ba52-5536ea30bb2f
updated: 1484310224
title: upward
categories:
    - Dictionary
---
upward
     adj 1: directed up; "the cards were face upward"; "an upward stroke
            of the pen"
     2: extending or moving toward a higher place; "the up
        staircase"; "a general upward movement of fish" [syn: {up(a)},
         {upward(a)}]
     adv 1: spatially or metaphorically from a lower to a higher
            position; "look up!"; "the music surged up"; "the
            fragments flew upwards"; "prices soared upwards";
            "upwardly mobile" [syn: {up}, {upwards}, {upwardly}]
            [ant: {down}, {down}, {down}, {down}]
     2: to a later time; "they moved the meeting date up"; "from
        childhood upward" [syn: {up}, {upwards}]
