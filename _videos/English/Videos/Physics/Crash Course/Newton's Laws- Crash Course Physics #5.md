---
version: 1
type: video
provider: YouTube
id: kKKM8Y-u7ds
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0c77aea6-f2ff-4e07-aea5-2b8a651074b5
updated: 1486069682
title: "Newton's Laws- Crash Course Physics #5"
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/kKKM8Y-u7ds/default.jpg
    - https://i3.ytimg.com/vi/kKKM8Y-u7ds/1.jpg
    - https://i3.ytimg.com/vi/kKKM8Y-u7ds/2.jpg
    - https://i3.ytimg.com/vi/kKKM8Y-u7ds/3.jpg
---
