---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Max_Br%C3%BCckner'
offline_file: ""
offline_thumbnail: ""
uuid: accd84aa-7249-4263-b636-09eaaa55e5ff
updated: 1484309075
title: Max Brückner
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Model_of_the_final_stellation_of_the_icosahedron.JPG
categories:
    - Geometers
---
Brückner was born on May 8, 1860 in Hartau, in the Kingdom of Saxony, a town that is now part of Zittau, Germany.[1] He completed a Ph.D. at Leipzig University in 1886, supervised by Felix Klein and Wilhelm Scheibner, with a dissertation concerning conformal maps.[1][2] After teaching at a grammar school in Zittau, he moved to the gymnasium in Bautzen.[1]
