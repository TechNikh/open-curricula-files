---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stochastic_block_model
offline_file: ""
offline_thumbnail: ""
uuid: 722c0418-55ee-43c8-b713-511502d48768
updated: 1484309454
title: Stochastic block model
tags:
    - Definition
    - Special cases
    - Typical statistical tasks
    - Detection
    - Partial recovery
    - Exact recovery
    - Statistical lower bounds and threshold behavior
    - Algorithms
    - Variants
categories:
    - Random graphs
---
The stochastic block model is a generative model for random graphs. This model tends to produce graphs containing communities, subsets characterized by being connected with one another with particular edge densities. For example, edges may be more common within communities than between communities. The stochastic block model is important in statistics, machine learning, and network science, where it serves as a useful benchmark for the task of recovering community structure in graph data.
