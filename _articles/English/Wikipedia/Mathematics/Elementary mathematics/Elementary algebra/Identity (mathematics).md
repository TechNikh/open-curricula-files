---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Identity_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 91340ced-54b3-4147-8c89-d6af9139e883
updated: 1484308907
title: Identity (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Trig_functions_on_unit_circle.PNG
tags:
    - Common identities
    - Trigonometric identities
    - Exponential identities
    - Logarithmic identities
    - Product, quotient, power and root
    - Change of base
    - Hyperbolic function identities
categories:
    - Elementary algebra
---
In mathematics an identity is an equality relation A = B, such that A and B contain some variables and A and B produce the same value as each other regardless of what values (usually numbers) are substituted for the variables. In other words, A = B is an identity if A and B define the same functions. This means that an identity is an equality between functions that are differently defined. For example, (a + b)2  =  a2 + 2ab + b2 and cos2(x) + sin2(x) = 1 are identities. Identities are sometimes indicated by the triple bar symbol ≡ instead of =, the equals sign.[1]
