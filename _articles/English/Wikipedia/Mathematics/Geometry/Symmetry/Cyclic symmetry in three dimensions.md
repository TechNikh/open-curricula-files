---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Cyclic_symmetry_in_three_dimensions
offline_file: ""
offline_thumbnail: ""
uuid: bd61f177-ad6e-4ef0-8a4c-b05d8d3e258d
updated: 1484309309
title: Cyclic symmetry in three dimensions
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Order_4_dihedral_symmetry_subgroup_tree.png
tags:
    - Types
    - Frieze groups
    - Examples
categories:
    - Symmetry
---
In three dimensional geometry, there are four infinite series of point groups in three dimensions (n≥1) with n-fold rotational or reflectional symmetry about one axis (by an angle of 360°/n) does not change the object.
