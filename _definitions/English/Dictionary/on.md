---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/on
offline_file: ""
offline_thumbnail: ""
uuid: 26e00d15-0898-40a2-a9bb-c716a52eb18a
updated: 1484310357
title: 'on'
categories:
    - Dictionary
---
on
     adj 1: in operation or operational; "left the oven on"; "the switch
            is in the on position" [ant: {off}]
     2: (of events) planned or scheduled; "the picnic is on, rain or
        shine"; "we have nothing on for Friday night" [ant: {off}]
     3: performing or scheduled for duties; "I'm on from five to
        midnight"; "Naval personnel on duty in Alaska"; "her
        on-duty hours were 11p.m. to 7 a.m." [syn: {on(p)}, {on
        duty(p)}, {on-duty(a)}]
     adv 1: with a forward motion; "we drove along admiring the view";
            "the horse trotted along at a steady pace"; "the
            circus traveled on to the next city"; "move along";
            "march ...
