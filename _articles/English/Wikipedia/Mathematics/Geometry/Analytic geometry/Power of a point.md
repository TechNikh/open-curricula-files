---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Power_of_a_point
offline_file: ""
offline_thumbnail: ""
uuid: 43863264-e061-43d8-9a52-e39c24a9df20
updated: 1484309167
title: Power of a point
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Power_point_simple.svg.png
tags:
    - Orthogonal circle
    - Theorems
    - Darboux product
    - "Laguerre's theorem"
categories:
    - Analytic geometry
---
In elementary plane geometry, the power of a point is a real number h that reflects the relative distance of a given point from a given circle. Specifically, the power of a point P with respect to a circle O of radius r is defined by (Figure 1)
