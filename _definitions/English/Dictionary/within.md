---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/within
offline_file: ""
offline_thumbnail: ""
uuid: d236599f-1bb0-4d9c-a577-54d48b0e6715
updated: 1484310344
title: within
categories:
    - Dictionary
---
within
     adv : on the inside; "inside, the car is a mess" [syn: {inside}]
           [ant: {outside}]
