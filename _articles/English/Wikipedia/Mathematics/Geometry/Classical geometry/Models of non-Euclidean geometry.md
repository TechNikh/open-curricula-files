---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Models_of_non-Euclidean_geometry
offline_file: ""
offline_thumbnail: ""
uuid: fab235cd-d892-4c6c-8583-744f5dac71a6
updated: 1484309168
title: Models of non-Euclidean geometry
categories:
    - Classical geometry
---
Models of non-Euclidean geometry are mathematical models of geometries which are non-Euclidean in the sense that it is not the case that exactly one line can be drawn parallel to a given line l through a point that is not on l. In hyperbolic geometric models, by contrast, there are infinitely many lines through A parallel to l, and in elliptic geometric models, parallel lines do not exist. (See the entries on hyperbolic geometry and elliptic geometry for more information.)
