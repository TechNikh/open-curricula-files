---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imaginary
offline_file: ""
offline_thumbnail: ""
uuid: e0f0aaaf-74c6-46e6-a67e-1e02eb2d6bf7
updated: 1484310196
title: imaginary
categories:
    - Dictionary
---
imaginary
     adj : not based on fact; dubious; "the falsehood about some
           fanciful secret treaties"- F.D.Roosevelt; "a small
           child's imaginary friends"; "her imagined fame"; "to
           create a notional world for oneself" [syn: {fanciful},
           {imagined}, {notional}]
