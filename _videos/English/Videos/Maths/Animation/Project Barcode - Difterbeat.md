---
version: 1
type: video
provider: YouTube
id: u7mGXSIZLD4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ad8c91a1-0aee-4c52-ae18-f82244eb877a
updated: 1486069647
title: 'Project Barcode - Difterbeat'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/u7mGXSIZLD4/default.jpg
    - https://i3.ytimg.com/vi/u7mGXSIZLD4/1.jpg
    - https://i3.ytimg.com/vi/u7mGXSIZLD4/2.jpg
    - https://i3.ytimg.com/vi/u7mGXSIZLD4/3.jpg
---
