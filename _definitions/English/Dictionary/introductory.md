---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/introductory
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484313481
title: introductory
categories:
    - Dictionary
---
introductory
     adj 1: serving to open or begin; "began the slide show with some
            introductory remarks"
     2: serving as a base or starting point; "a basic course in
        Russian"; "basic training for raw recruits"; "a set of
        basic tools"; "an introductory art course" [syn: {basic}]
     3: serving as an introduction or preface [syn: {prefatorial}, {prefatory}]
