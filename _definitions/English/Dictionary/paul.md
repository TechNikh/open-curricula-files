---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paul
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484420161
title: paul
categories:
    - Dictionary
---
Paul
     n 1: United States feminist (1885-1977) [syn: {Alice Paul}]
     2: (New Testament) a Christian missionary to the Gentiles;
        author of several Epistles in the New Testament; even
        though Paul was not present at the Last Supper he is
        considered an apostle; "Paul's name was Saul prior to his
        conversion to Christianity" [syn: {Saint Paul}, {St. Paul},
         {Apostle Paul}, {Paul the Apostle}, {Apostle of the
        Gentiles}, {Saul}, {Saul of Tarsus}]
