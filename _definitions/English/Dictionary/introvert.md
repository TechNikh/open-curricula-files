---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/introvert
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484416141
title: introvert
categories:
    - Dictionary
---
introvert
     n : (psychology) a person who tends to shrink from social
         contacts and to become preoccupied with their own
         thoughts [ant: {extrovert}]
     v 1: fold inwards; "some organs can invaginate" [syn: {invaginate}]
     2: turn inside; "He introverted his feelings"
