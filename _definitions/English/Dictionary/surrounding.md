---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surrounding
offline_file: ""
offline_thumbnail: ""
uuid: 49447372-0759-4924-8327-f2ab103fc4d2
updated: 1484310273
title: surrounding
categories:
    - Dictionary
---
surrounding
     adj : closely encircling; "encompassing mountain ranges"; "the
           surrounding countryside" [syn: {encompassing(a)}, {surrounding(a)}]
