---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/luxury
offline_file: ""
offline_thumbnail: ""
uuid: 23790697-08e3-4338-9c56-80f0dbf65874
updated: 1484310517
title: luxury
categories:
    - Dictionary
---
luxury
     adj : elegant and sumptuous; "a deluxe car"; "luxe
           accommodations"; "a luxury condominium" [syn: {deluxe},
            {de luxe}, {luxe}, {luxury(a)}]
     n 1: something that is an indulgence rather than a necessity
     2: the quality possessed by something that is excessively
        expensive [syn: {lavishness}, {sumptuosity}, {sumptuousness}]
     3: wealth as evidenced by sumptuous living [syn: {luxuriousness},
         {opulence}, {sumptuousness}]
