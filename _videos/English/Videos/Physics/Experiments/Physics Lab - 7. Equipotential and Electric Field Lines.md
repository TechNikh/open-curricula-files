---
version: 1
type: video
provider: YouTube
id: vM9hV-P8ImM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 66cd4084-9dd0-472d-b5f8-248c4084dfd0
updated: 1486069641
title: 'Physics Lab - 7. Equipotential and Electric Field Lines'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/vM9hV-P8ImM/default.jpg
    - https://i3.ytimg.com/vi/vM9hV-P8ImM/1.jpg
    - https://i3.ytimg.com/vi/vM9hV-P8ImM/2.jpg
    - https://i3.ytimg.com/vi/vM9hV-P8ImM/3.jpg
---
