---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/handover
offline_file: ""
offline_thumbnail: ""
uuid: d59a982c-4f6e-4b6a-a598-2735b6988d06
updated: 1484310551
title: handover
categories:
    - Dictionary
---
hand over
     v : to surrender someone or something to another; "the guard
         delivered the criminal to the police"; "render up the
         prisoners"; "render the town to the enemy"; "fork over
         the money" [syn: {fork over}, {fork out}, {fork up}, {turn
         in}, {get in}, {deliver}, {render}]
