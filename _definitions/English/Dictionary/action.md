---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/action
offline_file: ""
offline_thumbnail: ""
uuid: 51793b79-c594-4cd3-91d5-aba0758d7ff8
updated: 1484310343
title: action
categories:
    - Dictionary
---
action
     n 1: something done (usually as opposed to something said);
          "there were stories of murders and other unnatural
          actions"
     2: the state of being active; "his sphere of activity"; "he is
        out of action" [syn: {activity}, {activeness}] [ant: {inaction},
         {inaction}, {inaction}]
     3: a judicial proceeding brought by one party against another;
        one party prosecutes another for a wrong done or for
        protection of a right or for prevention of a wrong [syn: {legal
        action}, {action at law}]
     4: an act by a government body or supranational organization;
        "recent federal action undermined the segregationist
        ...
