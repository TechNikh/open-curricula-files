---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foul
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484595961
title: foul
categories:
    - Dictionary
---
foul
     adj 1: highly offensive; arousing aversion or disgust; "a
            disgusting smell"; "distasteful language"; "a
            loathsome disease"; "the idea of eating meat is
            repellent to me"; "revolting food"; "a wicked stench"
            [syn: {disgusting}, {disgustful}, {distasteful}, {loathly},
             {loathsome}, {repellent}, {repellant}, {repelling}, {revolting},
             {skanky}, {wicked}, {yucky}]
     2: offensively malodorous; "a putrid smell" [syn: {fetid}, {foetid},
         {foul-smelling}, {funky}, {noisome}, {smelly}, {putrid},
        {stinking}]
     3: violating accepted standards or rules; "a dirty fighter";
        "used foul means to ...
