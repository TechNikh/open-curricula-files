---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/checked
offline_file: ""
offline_thumbnail: ""
uuid: 0c313d2c-a9ae-4ece-b7fb-29f4c2b27b42
updated: 1484310587
title: checked
categories:
    - Dictionary
---
checked
     adj 1: held back from some action especially by force [syn: {curbed}]
     2: patterned with alternating squares of color [syn: {checkered},
         {chequered}]
