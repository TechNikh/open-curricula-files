---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biregular_graph
offline_file: ""
offline_thumbnail: ""
uuid: 8894b3c9-ce2d-488e-927e-a54c48494f08
updated: 1484308504
title: Biregular graph
tags:
    - Example
    - Vertex counts
    - Symmetry
    - Configurations
categories:
    - Graph theory
---
In graph-theoretic mathematics, a biregular graph[1] or semiregular bipartite graph[2] is a bipartite graph 
  
    
      
        G
        =
        (
        U
        ,
        V
        ,
        E
        )
      
    
    {\displaystyle G=(U,V,E)}
  
 for which every two vertices on the same side of the given bipartition have the same degree as each other. If the degree of the vertices in 
  
    
      
        U
      
    
    {\displaystyle U}
  
 is 
  
    
      
        x
      
    
    {\displaystyle x}
  
 and the degree of the vertices in 
  
    
      
        V
      
    
    {\displaystyle V}
  
 is 
  
    
      
        y
      
    
    {\displaystyle y}
  
, ...
