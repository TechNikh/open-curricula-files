---
version: 1
type: video
provider: YouTube
id: Osl6HEDZZ-E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Ideal%20gas%20equation%20example%204.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8604fed2-8053-4ae3-9ffc-557ea33bbb09
updated: 1486069589
title: Ideal gas equation example 4
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/Osl6HEDZZ-E/default.jpg
    - https://i3.ytimg.com/vi/Osl6HEDZZ-E/1.jpg
    - https://i3.ytimg.com/vi/Osl6HEDZZ-E/2.jpg
    - https://i3.ytimg.com/vi/Osl6HEDZZ-E/3.jpg
---
Figuring out the molar mass of a mystery molecule at STP. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/gases-and-kinetic-molecular-theory/ideal-gas-laws/v/introduction-to-partial-pressure?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/gases-and-kinetic-molecular-theory/ideal-gas-laws/v/ideal-gas-equation-example-3?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
