---
version: 1
type: video
provider: YouTube
id: EoCeL4SPIcA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20Basics/Solving%20and%20graphing%20linear%20inequalities%20in%20two%20variables%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fb11da8e-8ecf-4200-b009-629620ca39db
updated: 1486069557
title: Solving and graphing linear inequalities in two variables 1
categories:
    - Algebra Basics
thumbnail_urls:
    - https://i3.ytimg.com/vi/EoCeL4SPIcA/default.jpg
    - https://i3.ytimg.com/vi/EoCeL4SPIcA/1.jpg
    - https://i3.ytimg.com/vi/EoCeL4SPIcA/2.jpg
    - https://i3.ytimg.com/vi/EoCeL4SPIcA/3.jpg
---
