---
version: 1
type: video
provider: YouTube
id: Wo6MQpO24hg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7931f51e-ff3d-479e-8d5f-8055c613c028
updated: 1486069636
title: 'Experiment 11   Optical Spectroscopy    FCW F2014 V1'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/Wo6MQpO24hg/default.jpg
    - https://i3.ytimg.com/vi/Wo6MQpO24hg/1.jpg
    - https://i3.ytimg.com/vi/Wo6MQpO24hg/2.jpg
    - https://i3.ytimg.com/vi/Wo6MQpO24hg/3.jpg
---
