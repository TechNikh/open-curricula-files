---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Formulas_for_generating_Pythagorean_triples
offline_file: ""
offline_thumbnail: ""
uuid: 78b4f3bc-5d01-4b4c-aa3a-bdf8c5964585
updated: 1484308641
title: Formulas for generating Pythagorean triples
tags:
    - "Euclid's, Pythagoras', and Plato's formulas"
    - "Fibonacci's method"
    - Progressions of whole and fractional numbers
    - "Dickson's method"
    - Generalized Fibonacci sequence
    - I.
    - II.
    - III.
    - "Pythagorean triples and Descartes' circle equation"
    - 'A Ternary Tree: Generating All Primitive Pythagorean Triples'
    - Generating triples using quadratic equations
    - >
        Pythagorean triples by use of matrices and linear
        transformations
    - Area proportional to sums of squares
    - Height-excess enumeration theorem
categories:
    - Number theory
---
