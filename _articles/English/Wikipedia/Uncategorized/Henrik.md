---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Henrik
offline_file: ""
offline_thumbnail: ""
uuid: 4fbaf266-0e9c-446f-a28c-397b0d276072
updated: 1484308307
title: Henrik
categories:
    - Uncategorized
---
Henrik is a male given name of Germanic origin, primarily used in Scandinavia, Estonia, Hungary and Slovenia.[2] In Poland, the name is spelt Henryk but pronounced similarly. Equivalents in other languages are Henry (English), Heikki (Finnish), Henryk (Polish), Hendrik (Dutch), Heinrich (German), Enrico (Italian), Henri (French), Enrique (Spanish) and Henrique (Portuguese). It means 'Ruler of the home' or 'Lord of the house'.[3]
