---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Orientation_(vector_space)
offline_file: ""
offline_thumbnail: ""
uuid: b5f9f76f-35b1-4d08-8098-dabaabe83e8f
updated: 1484309165
title: Orientation (vector space)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cartesian_coordinate_system_handedness.svg.png
tags:
    - Definition
    - Zero-dimensional case
    - On a line
    - Alternate viewpoints
    - Multilinear algebra
    - Lie group theory
    - Geometric algebra
    - Orientation on manifolds
categories:
    - Analytic geometry
---
In mathematics, orientation is a geometric notion that in two dimensions allows one to say when a cycle goes around clockwise or counterclockwise, and in three dimensions when a figure is left-handed or right-handed. In linear algebra, the notion of orientation makes sense in arbitrary finite dimension. In this setting, the orientation of an ordered basis is a kind of asymmetry that makes a reflection impossible to replicate by means of a simple rotation. Thus, in three dimensions, it is impossible to make the left hand of a human figure into the right hand of the figure by applying a rotation alone, but it is possible to do so by reflecting the figure in a mirror. As a result, in the ...
