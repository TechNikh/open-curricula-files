---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mark
offline_file: ""
offline_thumbnail: ""
uuid: 94943653-42da-4718-be20-6929f4da1792
updated: 1484310339
title: mark
categories:
    - Dictionary
---
mark
     n 1: a number or letter indicating quality (especially of a
          student's performance); "she made good marks in
          algebra"; "grade A milk"; "what was your score on your
          homework?" [syn: {grade}, {score}]
     2: a distinguishing symbol; "the owner's mark was on all the
        sheep" [syn: {marker}, {marking}]
     3: a reference point to shoot at; "his arrow hit the mark"
        [syn: {target}]
     4: a visible indication made on a surface; "some previous
        reader had covered the pages with dozens of marks"; "paw
        prints were everywhere" [syn: {print}]
     5: the impression created by doing something unusual or
        extraordinary that ...
