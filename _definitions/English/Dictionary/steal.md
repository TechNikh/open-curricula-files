---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/steal
offline_file: ""
offline_thumbnail: ""
uuid: f7d9c5fb-5ac3-4397-8720-ce37949f98b1
updated: 1484310144
title: steal
categories:
    - Dictionary
---
steal
     n 1: an advantageous purchase; "she got a bargain at the
          auction"; "the stock was a real buy at that price" [syn:
           {bargain}, {buy}]
     2: a stolen base; an instance in which a base runner advances
        safely during the delivery of a pitch (without the help of
        a hit or walk or passed ball or wild pitch)
     v 1: take without the owner's consent; "Someone stole my wallet
          on the train"; "This author stole entire paragraphs from
          my dissertation"
     2: move stealthily; "The ship slipped away in the darkness"
        [syn: {slip}]
     3: steal a base
     4: to go stealthily or furtively; "..stead of sneaking around
        ...
