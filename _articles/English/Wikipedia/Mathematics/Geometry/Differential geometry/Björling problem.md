---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bj%C3%B6rling_problem'
offline_file: ""
offline_thumbnail: ""
uuid: b6a46ced-adfc-47f7-a0de-e3352d5c466a
updated: 1484309206
title: Björling problem
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Catalan%2527s_Minimal_Surface.png'
categories:
    - Differential geometry
---
In differential geometry, the Björling problem is the problem of finding a minimal surface passing through a given curve with prescribed normal (or tangent planes). The problem was posed and solved by Swedish mathematician Emanuel Gabriel Björling,[1] with further refinement by Hermann Schwarz.[2]
