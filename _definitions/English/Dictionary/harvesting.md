---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harvesting
offline_file: ""
offline_thumbnail: ""
uuid: d98a85dd-e538-4cd5-b302-c019c1ea21a7
updated: 1484310256
title: harvesting
categories:
    - Dictionary
---
harvesting
     n : the gathering of a ripened crop [syn: {harvest}, {harvest
         home}]
