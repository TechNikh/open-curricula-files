---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inclining
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484640301
title: inclining
categories:
    - Dictionary
---
inclining
     n : the act of inclining; bending forward; "an inclination of
         his head indicated his agreement" [syn: {inclination}]
