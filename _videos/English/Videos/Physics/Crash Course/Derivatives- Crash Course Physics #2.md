---
version: 1
type: video
provider: YouTube
id: ObHJJYvu3RE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b6355fb2-2a3f-4bed-8e20-be77f1c8d62f
updated: 1486069684
title: 'Derivatives- Crash Course Physics #2'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/ObHJJYvu3RE/default.jpg
    - https://i3.ytimg.com/vi/ObHJJYvu3RE/1.jpg
    - https://i3.ytimg.com/vi/ObHJJYvu3RE/2.jpg
    - https://i3.ytimg.com/vi/ObHJJYvu3RE/3.jpg
---
