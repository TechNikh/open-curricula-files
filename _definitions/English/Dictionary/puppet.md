---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/puppet
offline_file: ""
offline_thumbnail: ""
uuid: df7d2776-2416-4958-b5c1-b10b3f84667d
updated: 1484310571
title: puppet
categories:
    - Dictionary
---
puppet
     n 1: a small figure of a person operated from above with strings
          by a puppeteer [syn: {marionette}]
     2: a person who is controlled by others and is used to perform
        unpleasant or dishonest tasks for someone else [syn: {creature},
         {tool}]
     3: a doll with a hollow head of a person or animal and a cloth
        body; intended to fit over the hand and be manipulated
        with the fingers
