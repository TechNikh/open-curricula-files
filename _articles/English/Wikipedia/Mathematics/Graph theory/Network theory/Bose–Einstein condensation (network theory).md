---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bose%E2%80%93Einstein_condensation_(network_theory)'
offline_file: ""
offline_thumbnail: ""
uuid: 0ca57b8e-0832-46f2-85de-c396bf444a1c
updated: 1484309434
title: Bose–Einstein condensation (network theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Bec5555.jpg
tags:
    - Background
    - History
    - The concept
    - Connection with network theory
    - Bose–Einstein phase transition in complex networks
    - >
        Bose–Einstein condensation in evolutionary models and
        ecological systems
    - Memory understood as an equilibrium Bose gas
categories:
    - Network theory
---
Bose–Einstein condensation in networks is a phase transition observed in complex networks that can be described with the same mathematical model as that explaining Bose–Einstein condensation in physics.
