---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vibrating
offline_file: ""
offline_thumbnail: ""
uuid: dcd2e59d-775e-4113-9d89-83d927cb25f8
updated: 1484310387
title: vibrating
categories:
    - Dictionary
---
vibrating
     adj : moving very rapidly to and fro or up and down; "the
           vibrating piano strings" [syn: {vibratory}]
