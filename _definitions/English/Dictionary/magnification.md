---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnification
offline_file: ""
offline_thumbnail: ""
uuid: 2b68e9cc-2c5d-4c1d-ad53-e47fc8bf3beb
updated: 1484310214
title: magnification
categories:
    - Dictionary
---
magnification
     n 1: the act of expanding something in apparent size
     2: the ratio of the size of an image to the size of the object
     3: making to seem more important than it really is [syn: {exaggeration},
         {overstatement}] [ant: {understatement}]
     4: a photographic print that has been enlarged [syn: {enlargement},
         {blowup}]
