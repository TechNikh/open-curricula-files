---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crouched
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484505361
title: crouched
categories:
    - Dictionary
---
crouched
     adj : squatting close to the ground; "poorly clothed men huddled
           low against the wind"; "he stayed in the ditch hunkered
           down" [syn: {crouching}, {huddled}, {hunkered}, {hunkered
           down}]
