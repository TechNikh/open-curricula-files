---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meet
offline_file: ""
offline_thumbnail: ""
uuid: 6fba2245-1c35-410d-b215-fd2f8e153b97
updated: 1484310289
title: meet
categories:
    - Dictionary
---
meet
     adj : being precisely fitting and right; "it is only meet that she
           should be seated first" [syn: {fitting}]
     n : a meeting at which a number of athletic contests are held
         [syn: {sports meeting}]
     v 1: come together; "I'll probably see you at the meeting"; "How
          nice to see you again!" [syn: {ran into}, {encounter}, {run
          across}, {come across}, {see}]
     2: get together socially or for a specific purpose [syn: {get
        together}]
     3: be adjacent or come together; "The lines converge at this
        point" [syn: {converge}] [ant: {diverge}, {diverge}]
     4: fill or meet a want or need [syn: {satisfy}, {fill}, {fulfill},
     ...
