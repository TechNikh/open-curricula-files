---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rising
offline_file: ""
offline_thumbnail: ""
uuid: ef2bcd3d-6fb8-4d0c-9749-6035c0c21c68
updated: 1484310377
title: rising
categories:
    - Dictionary
---
rising
     adj 1: advancing or becoming higher or greater in degree or value
            or status; "a rising trend"; "a rising market" [ant: {falling}]
     2: (of a heavenly body) becoming visible above the horizon;
        "the rising sun" [ant: {setting}]
     3: increasing in amount or degree; "rising prices"
     4: sloping upward [syn: {acclivitous}, {uphill}]
     5: coming to maturity; "the rising generation" [syn: {emerging}]
     6: newly come into prominence; "a rising young politician"
     n 1: a movement upward; "they cheered the rise of the hot-air
          balloon" [syn: {rise}, {ascent}, {ascension}] [ant: {fall}]
     2: organized opposition to authority; a conflict in ...
