---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/froth
offline_file: ""
offline_thumbnail: ""
uuid: 97a55945-2462-4af7-9b88-afcc1d0617f0
updated: 1484310409
title: froth
categories:
    - Dictionary
---
froth
     n : a mass of small bubbles formed in or on a liquid [syn: {foam}]
     v 1: form bubbles; "The boiling soup was frothing"; "The river
          was foaming"; "Sparkling water" [syn: {foam}, {fizz}, {effervesce},
           {sparkle}]
     2: make froth or foam and become bubbly; "The river foamed"
        [syn: {spume}, {suds}]
