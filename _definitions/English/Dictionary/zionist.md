---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/zionist
offline_file: ""
offline_thumbnail: ""
uuid: 67d56fe2-c8a1-461c-83f8-84943b32b6d9
updated: 1484310178
title: zionist
categories:
    - Dictionary
---
Zionist
     adj 1: relating to or characteristic of Zionism; "the Zionist
            movement"
     2: relating to or characteristic of a supporter of Zionism;
        "the Zionist leader Theodor Herzl"
     n : a Jewish supporter of Zionism
