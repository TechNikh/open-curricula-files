---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rigidly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484628121
title: rigidly
categories:
    - Dictionary
---
rigidly
     adv : in a rigid manner; "the body was rigidly erect"; "ge sat
           bolt upright" [syn: {stiffly}, {bolt}]
