---
version: 1
type: video
provider: YouTube
id: PWx9DurgPn8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1dc7680c-dd74-4d62-9731-ae1d266b3b4e
updated: 1486069686
title: 'High Mass Stars- Crash Course Astronomy #31'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/PWx9DurgPn8/default.jpg
    - https://i3.ytimg.com/vi/PWx9DurgPn8/1.jpg
    - https://i3.ytimg.com/vi/PWx9DurgPn8/2.jpg
    - https://i3.ytimg.com/vi/PWx9DurgPn8/3.jpg
---
