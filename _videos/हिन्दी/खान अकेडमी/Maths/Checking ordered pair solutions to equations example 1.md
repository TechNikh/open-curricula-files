---
version: 1
type: video
provider: YouTube
id: ed1KMON2aCU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ff8f5f2e-6cce-4569-a175-1b3104b44c8c
updated: 1486069611
title: Checking ordered pair solutions to equations example 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ed1KMON2aCU/default.jpg
    - https://i3.ytimg.com/vi/ed1KMON2aCU/1.jpg
    - https://i3.ytimg.com/vi/ed1KMON2aCU/2.jpg
    - https://i3.ytimg.com/vi/ed1KMON2aCU/3.jpg
---
