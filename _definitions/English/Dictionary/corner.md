---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corner
offline_file: ""
offline_thumbnail: ""
uuid: 487966eb-cc48-4f99-8a54-bc4ee6ad8e6b
updated: 1484310521
title: corner
categories:
    - Dictionary
---
corner
     n 1: a place off to the side of an area; "he tripled to the
          rightfield corner"; "he glanced out of the corner of his
          eye"
     2: the point where two lines meet or intersect; "the corners of
        a rectangle"
     3: an interior angle formed be two meeting walls; "a piano was
        in one corner of the room" [syn: {nook}]
     4: the intersection of two streets; "standing on the corner
        watching all the girls go by" [syn: {street corner}, {turning
        point}]
     5: the point where three areas or surfaces meet or intersect;
        "the corners of a cube"
     6: a small concavity [syn: {recess}, {recession}, {niche}]
     7: a temporary ...
