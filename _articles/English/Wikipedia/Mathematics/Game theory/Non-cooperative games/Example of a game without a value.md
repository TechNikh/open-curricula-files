---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Example_of_a_game_without_a_value
offline_file: ""
offline_thumbnail: ""
uuid: 580abe04-cad9-4a41-aa85-c7548329eeed
updated: 1484309055
title: Example of a game without a value
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Game_with_no_value.svg.png
tags:
    - The game
    - Game value
    - Generalizations
categories:
    - Non-cooperative games
---
In game theory, and in particular the study of zero-sum continuous games, it is commonly assumed that a game has a minimax value. This is the expected value to one of the players when both play a perfect strategy (which is to choose from a particular PDF).
