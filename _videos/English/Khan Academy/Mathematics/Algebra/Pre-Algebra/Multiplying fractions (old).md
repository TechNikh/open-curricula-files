---
version: 1
type: video
provider: YouTube
id: Mnu16kCRW4U
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Multiplying%20fractions%20%28old%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 00749554-440c-445c-be47-911288e0ec54
updated: 1486069565
title: Multiplying fractions (old)
tags:
    - Fractions
    - multiplying
    - Multiplication
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/Mnu16kCRW4U/default.jpg
    - https://i3.ytimg.com/vi/Mnu16kCRW4U/1.jpg
    - https://i3.ytimg.com/vi/Mnu16kCRW4U/2.jpg
    - https://i3.ytimg.com/vi/Mnu16kCRW4U/3.jpg
---
Multiplying fractions
