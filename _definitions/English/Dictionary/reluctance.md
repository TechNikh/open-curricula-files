---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reluctance
offline_file: ""
offline_thumbnail: ""
uuid: 8596d646-968d-49e1-bf5b-cd0c0a234dd1
updated: 1484310230
title: reluctance
categories:
    - Dictionary
---
reluctance
     n 1: (physics) opposition to magnetic flux (analogous to electric
          resistance)
     2: a certain degree of unwillingness; "a reluctance to commit
        himself"; "after some hesitation he agreed" [syn: {hesitancy},
         {hesitation}, {disinclination}, {indisposition}]
