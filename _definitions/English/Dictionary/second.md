---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/second
offline_file: ""
offline_thumbnail: ""
uuid: cca38801-45da-46fc-be01-60787285d723
updated: 1484310321
title: second
categories:
    - Dictionary
---
second
     adj 1: coming next after the first in position in space or time or
            degree or magnitude [syn: {2nd}, {2d}]
     2: coming next after first; "a second chance"; "the second vice
        president"
     3: a part or voice or instrument or orchestra section lower in
        pitch than or subordinate to the first; "second flute";
        "the second violins" [ant: {first}]
     4: having the second highest gear ratio; "second gear"
     n 1: 1/60 of a minute; the basic unit of time adopted under the
          Systeme International d'Unites [syn: {sec}, {s}]
     2: an indefinitely short time; "wait just a moment"; "it only
        takes a minute"; "in just a bit" [syn: ...
