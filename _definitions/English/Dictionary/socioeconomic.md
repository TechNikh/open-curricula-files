---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/socioeconomic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484619481
title: socioeconomic
categories:
    - Dictionary
---
socioeconomic
     adj : involving social as well as economic factors; "socioeconomic
           status"
