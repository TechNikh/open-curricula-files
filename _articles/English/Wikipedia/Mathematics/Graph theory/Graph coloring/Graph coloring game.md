---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_coloring_game
offline_file: ""
offline_thumbnail: ""
uuid: fd1f810f-e4d8-48ea-a6f1-059e6250b521
updated: 1484309382
title: Graph coloring game
tags:
    - Vertex coloring game
    - Relation with other notions
    - Graph classes
    - Open problems
    - Edge coloring game
    - General case
    - Graph classes
    - Open problems
    - Incidence coloring game
    - Relations with other notions
    - Graph classes
    - Open problems
    - Notes
    - References (chronological order)
categories:
    - Graph coloring
---
The graph coloring game is a mathematical game related to graph theory. Coloring game problems arose as game-theoretic versions of well-known graph coloring problems. In a coloring game, two players use a given set of colors to construct a coloring of a graph, following specific rules depending on the game we consider. One player tries to successfully complete the coloring of the graph, when the other one tries to prevent him to achieve it.
