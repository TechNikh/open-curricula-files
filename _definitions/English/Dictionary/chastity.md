---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chastity
offline_file: ""
offline_thumbnail: ""
uuid: a3d3f473-e210-47f9-84b8-db31af9e709c
updated: 1484310569
title: chastity
categories:
    - Dictionary
---
chastity
     n 1: abstaining from sexual relations (as because of religious
          vows) [syn: {celibacy}, {sexual abstention}]
     2: morality with respect to sexual relations [syn: {virtue}, {sexual
        morality}]
