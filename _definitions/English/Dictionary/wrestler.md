---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wrestler
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484468461
title: wrestler
categories:
    - Dictionary
---
wrestler
     n : combatant who tries to throw opponent to the ground [syn: {grappler},
          {matman}]
