---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ihara_zeta_function
offline_file: ""
offline_thumbnail: ""
uuid: 55e39f72-a67e-4bb5-ac27-3d36c9abd706
updated: 1484309376
title: Ihara zeta function
tags:
    - Definition
    - "Ihara's formula"
    - Applications
categories:
    - Algebraic graph theory
---
In mathematics, the Ihara zeta-function is a zeta function associated with a finite graph. It closely resembles the Selberg zeta-function, and is used to relate closed paths to the spectrum of the adjacency matrix. The Ihara zeta-function was first defined by Yasutaka Ihara in the 1960s in the context of discrete subgroups of the two-by-two p-adic special linear group. Jean-Pierre Serre suggested in his book Trees that Ihara's original definition can be reinterpreted graph-theoretically. It was Toshikazu Sunada who put this suggestion into practice (1985). As observed by Sunada, a regular graph is a Ramanujan graph if and only if its Ihara zeta function satisfies an analogue of the Riemann ...
