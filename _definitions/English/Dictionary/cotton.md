---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cotton
offline_file: ""
offline_thumbnail: ""
uuid: ac93a804-352d-48fa-aefe-39b7d9a7827f
updated: 1484310264
title: cotton
categories:
    - Dictionary
---
cotton
     n 1: silky fibers from cotton plants in their raw state [syn: {cotton
          wool}]
     2: fabric woven from cotton fibers [syn: {cotton cloth}]
     3: erect bushy mallow plant or small tree bearing bolls
        containing seeds with many long hairy fibers [syn: {cotton
        plant}]
     4: thread made of cotton fibers
     v : take a liking to; "cotton to something"
