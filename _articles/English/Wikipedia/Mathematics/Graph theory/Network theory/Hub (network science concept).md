---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hub_(network_science_concept)
offline_file: ""
offline_thumbnail: ""
uuid: ce83ff79-760d-4f93-974c-ee619558773b
updated: 1484309435
title: Hub (network science concept)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Network_representation_of_brain_connectivity.JPG
tags:
    - Definition
    - Emergence
    - Attributes
    - Shortening the path lengths in a network
    - Aging of hubs (nodes)
    - Robustness and Attack Tolerance
    - Degree correlation
    - Spreading phenomenon
categories:
    - Network theory
---
Hub is a concept in network science which refers to a node with a number of links that greatly exceeds the average. Emergence of hubs is a consequence of a scale-free property of networks.[1] While hubs cannot be observed in a random network, they are expected to emerge in scale- free networks. Uprise of hubs in scale-free networks is associated with power- law distribution. Hubs have a significant impact on the network topology. Hubs can be found in many real networks, such as Brain Network or Internet.
