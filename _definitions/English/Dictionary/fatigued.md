---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fatigued
offline_file: ""
offline_thumbnail: ""
uuid: bac010f4-05ac-4884-ac14-fc667eb73e47
updated: 1484310559
title: fatigued
categories:
    - Dictionary
---
fatigued
     adj : drained of energy or effectiveness; extremely tired;
           completely exhausted; "the day's shopping left her
           exhausted"; "he went to bed dog-tired"; "was fagged and
           sweaty"; "the trembling of his played out limbs"; "felt
           completely washed-out"; "only worn-out horses and
           cattle"; "you look worn out" [syn: {exhausted}, {dog-tired},
            {fagged}, {played out}, {spent}, {washed-out}, {worn-out(a)},
            {worn out(p)}]
