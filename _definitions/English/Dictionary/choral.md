---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/choral
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484430241
title: choral
categories:
    - Dictionary
---
choral
     adj : related to or written for or performed by a chorus or choir;
           "choral composition"; "choral ensemble"
     n : a stately Protestant (especially Lutheran) hymn tune [syn: {chorale}]
