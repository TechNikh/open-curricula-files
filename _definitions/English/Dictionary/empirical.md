---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/empirical
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484383141
title: empirical
categories:
    - Dictionary
---
empirical
     adj 1: derived from experiment and observation rather than theory;
            "an empirical basis for an ethical theory"; "empirical
            laws"; "empirical data"; "an empirical treatment of a
            disease about which little is known" [syn: {empiric}]
            [ant: {theoretical}]
     2: relying on medical quackery; "empiric treatment" [syn: {empiric}]
