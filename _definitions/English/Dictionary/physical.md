---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/physical
offline_file: ""
offline_thumbnail: ""
uuid: d1381cb2-14c9-4c9c-a03d-70af043a4c0f
updated: 1484310277
title: physical
categories:
    - Dictionary
---
physical
     adj 1: involving the body as distinguished from the mind or spirit;
            "physical exercise"; "physical suffering"; "was sloppy
            about everything but her physical appearance" [ant: {mental}]
     2: relating to the sciences dealing with matter and energy;
        especially physics; "physical sciences"; "physical laws"
     3: having substance or material existence; perceptible to the
        senses; "a physical manifestation"; "surrounded by
        tangible objects" [syn: {tangible}, {touchable}]
     4: according with material things or natural laws (other than
        those peculiar to living matter); "a reflex response to
        physical stimuli"
     ...
