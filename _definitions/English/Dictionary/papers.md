---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/papers
offline_file: ""
offline_thumbnail: ""
uuid: 8b4bc9e8-ac2e-47a9-a634-be5ffd25ef25
updated: 1484310327
title: papers
categories:
    - Dictionary
---
papers
     n : writing that provides information (especially information of
         an official nature) [syn: {document}, {written document}]
