---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conscience
offline_file: ""
offline_thumbnail: ""
uuid: 4aca2e90-9880-4fb5-8ded-5703d28d5e58
updated: 1484310561
title: conscience
categories:
    - Dictionary
---
conscience
     n 1: motivation deriving logically from ethical or moral
          principles that govern a person's thoughts and actions
          [syn: {scruples}, {moral sense}, {sense of right and
          wrong}]
     2: conformity to one's own sense of right conduct; "a person of
        unflagging conscience"
     3: a feeling of shame when you do something immoral; "he has no
        conscience about his cruelty"
