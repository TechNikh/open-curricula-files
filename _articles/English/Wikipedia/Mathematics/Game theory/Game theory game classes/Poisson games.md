---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Poisson_games
offline_file: ""
offline_thumbnail: ""
uuid: 278730da-df7b-490c-819f-f68a0a9f6561
updated: 1484309038
title: Poisson games
tags:
    - Example
    - Formal definitions
    - Simple probabilistic properties
    - Existence of equilibrium
    - Applications
categories:
    - Game theory game classes
---
In game theory a Poisson game is a game with a random number of players, where the distribution of the number of players follows a Poisson random process.[1] An extension of games of imperfect information, Poisson games have mostly seen application to models of voting.
