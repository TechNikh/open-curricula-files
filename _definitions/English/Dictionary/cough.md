---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cough
offline_file: ""
offline_thumbnail: ""
uuid: 61423e7b-638d-4a62-8af0-8283c21e5f7b
updated: 1484310353
title: cough
categories:
    - Dictionary
---
cough
     n 1: sudden expulsion of air from the lungs that clears the air
          passages; a common symptom of upper respiratory
          infection or bronchitis or pneumonia or tuberculosis
          [syn: {coughing}]
     2: the act of exhaling air suddenly with a noise [syn: {coughing}]
     v : exhale abruptly, as when one has a chest cold or congestion;
         "The smoker coughs all day"
