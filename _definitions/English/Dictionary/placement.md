---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/placement
offline_file: ""
offline_thumbnail: ""
uuid: ab65672d-2e48-4162-aca0-90c41d1c232e
updated: 1484310384
title: placement
categories:
    - Dictionary
---
placement
     n 1: the spatial property of the way in which something is
          placed; "the arrangement of the furniture"; "the
          placement of the chairs" [syn: {arrangement}]
     2: contact established between applicants and prospective
        employees; "the agency provided placement services"
     3: the act of putting something in a certain place or location
        [syn: {location}, {locating}, {position}, {positioning}, {emplacement}]
