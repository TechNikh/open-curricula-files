---
version: 1
type: video
provider: YouTube
id: 34sRXQZpZmw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5b889936-ed99-4a00-bbde-3e864fa10a9e
updated: 1486069668
title: 'ASCB Celldance Video - ATP Synthase'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/34sRXQZpZmw/default.jpg
    - https://i3.ytimg.com/vi/34sRXQZpZmw/1.jpg
    - https://i3.ytimg.com/vi/34sRXQZpZmw/2.jpg
    - https://i3.ytimg.com/vi/34sRXQZpZmw/3.jpg
---
