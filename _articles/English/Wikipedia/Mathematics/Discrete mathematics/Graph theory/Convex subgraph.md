---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_subgraph
offline_file: ""
offline_thumbnail: ""
uuid: d29b1cb9-4d0d-4657-8353-ff30a9a58b08
updated: 1484308601
title: Convex subgraph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-6n-graf.svg.png
categories:
    - Graph theory
---
In metric graph theory, a convex subgraph of an undirected graph G is a subgraph that includes every shortest path in G between two of its vertices. Thus, it is analogous to the definition of a convex set in geometry, a set that contains the line segment between every pair of its points.
