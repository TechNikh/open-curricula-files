---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perturb
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484626741
title: perturb
categories:
    - Dictionary
---
perturb
     v 1: disturb in mind or make uneasy or cause to be worried or
          alarmed; "She was rather perturbed by the news that her
          father was seriously ill" [syn: {unhinge}, {disquiet}, {trouble},
           {cark}, {distract}, {disorder}]
     2: disturb or interfere with the usual path of an electron or
        atom; "The electrons were perturbed by the passing ion"
     3: cause a celestial body to deviate from a theoretically
        regular orbital motion, especially as a result of
        interposed or extraordinary gravitational pull; "The
        orbits of these stars were perturbed by the passings of a
        comet"
     4: throw into great confusion or ...
