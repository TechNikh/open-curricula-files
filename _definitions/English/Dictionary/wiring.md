---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wiring
offline_file: ""
offline_thumbnail: ""
uuid: 193066b3-f62f-43ca-a4ae-9c8d94479db0
updated: 1484310196
title: wiring
categories:
    - Dictionary
---
wiring
     n 1: a circuit of wires for the distribution of electricity
     2: the work of installing the wires for an electrical system or
        device
