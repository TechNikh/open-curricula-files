---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Szekeres_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 9094ba87-0465-4a67-9ea0-253305953eb4
updated: 1484308667
title: Erdős–Szekeres theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Monotone-subseq-17-5.svg.png
tags:
    - Example
    - Alternative interpretations
    - Geometric interpretation
    - Permutation pattern interpretation
    - Proofs
    - Pigeonhole principle
    - "Dilworth's theorem"
categories:
    - Theorems in discrete mathematics
---
In mathematics, the Erdős–Szekeres theorem is a finitary result that makes precise one of the corollaries of Ramsey's theorem. While Ramsey's theorem makes it easy to prove that every sequence of distinct real numbers contains a monotonically increasing infinite subsequence or a monotonically decreasing infinite subsequence, the result proved by Paul Erdős and George Szekeres goes further. For given r, s they showed that any sequence of length at least (r − 1)(s − 1) + 1 contains a monotonically increasing subsequence of length r or a monotonically decreasing subsequence of length s. The proof appeared in the same 1935 paper that mentions the Happy Ending problem.[1]
