---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/morality
offline_file: ""
offline_thumbnail: ""
uuid: 9542cbd8-26fe-4a76-9096-d72875552ee8
updated: 1484310593
title: morality
categories:
    - Dictionary
---
morality
     n 1: concern with the distinction between good and evil or right
          and wrong; right or good conduct [ant: {immorality}]
     2: motivation based on ideas of right and wrong [syn: {ethical
        motive}, {ethics}, {morals}]
