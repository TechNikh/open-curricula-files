---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eucalyptus
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484600701
title: eucalyptus
categories:
    - Dictionary
---
eucalyptus
     n 1: wood of any of various eucalyptus trees valued as timber
     2: a tree of the genus Eucalyptus [syn: {eucalypt}, {eucalyptus
        tree}]
     [also: {eucalypti} (pl)]
