---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hopefully
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484598241
title: hopefully
categories:
    - Dictionary
---
hopefully
     adv 1: with hope; in a hopeful manner; "we searched hopefully for a
            good position" [ant: {hopelessly}]
     2: it is hoped; "hopefully the weather will be fine on Sunday"
