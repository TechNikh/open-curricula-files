---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saving
offline_file: ""
offline_thumbnail: ""
uuid: de18a959-6e8e-4f0e-959c-f206959e2480
updated: 1484310266
title: saving
categories:
    - Dictionary
---
saving
     adj 1: bringing about salvation or redemption from sin; "saving
            faith"; "redemptive (or redeeming) love" [syn: {redemptive},
             {redeeming(a)}, {saving(a)}]
     2: characterized by thriftiness; "wealthy by inheritance but
        saving by constitution"- Ellen Glasgow
     n 1: an act of economizing; reduction in cost; "it was a small
          economy to walk to work every day"; "there was a saving
          of 50 cents" [syn: {economy}]
     2: recovery or preservation from loss or danger; "work is the
        deliverance of mankind"; "a surgeon's job is the saving of
        lives" [syn: {rescue}, {deliverance}, {delivery}]
     3: the activity of ...
