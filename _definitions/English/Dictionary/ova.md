---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ova
offline_file: ""
offline_thumbnail: ""
uuid: 70fceb99-bc23-4847-bd50-b94c375443bf
updated: 1484310295
title: ova
categories:
    - Dictionary
---
ovum
     n : the female reproductive cell; the female gamete [syn: {egg
         cell}]
     [also: {ova} (pl)]
