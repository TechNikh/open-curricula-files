---
version: 1
type: video
provider: YouTube
id: NX0ZPtB_QFY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 61cff98f-f81e-47ea-ad0f-cf174fa04fe2
updated: 1486069672
title: Nucleic acid structure 1
categories:
    - Chemical processes
thumbnail_urls:
    - https://i3.ytimg.com/vi/NX0ZPtB_QFY/default.jpg
    - https://i3.ytimg.com/vi/NX0ZPtB_QFY/1.jpg
    - https://i3.ytimg.com/vi/NX0ZPtB_QFY/2.jpg
    - https://i3.ytimg.com/vi/NX0ZPtB_QFY/3.jpg
---
