---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Finite-difference_time-domain_method
offline_file: ""
offline_thumbnail: ""
uuid: e0614a71-7346-490b-89ed-30e3659b393b
updated: 1484308759
title: Finite-difference time-domain method
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/450px-FDTD_Yee_grid_2d-3d.svg.png
tags:
    - History
    - "Development of FDTD and Maxwell's equations"
    - FDTD models and methods
    - Using the FDTD method
    - Strengths of FDTD modeling
    - Weaknesses of FDTD modeling
    - Grid truncation techniques
    - Popularity
    - Implementations
categories:
    - Electrodynamics
---
Finite-difference time-domain or Yee's method (named after the Chinese American applied mathematician Kane S. Yee, born 1934) is a numerical analysis technique used for modeling computational electrodynamics (finding approximate solutions to the associated system of differential equations). Since it is a time-domain method, FDTD solutions can cover a wide frequency range with a single simulation run, and treat nonlinear material properties in a natural way.
