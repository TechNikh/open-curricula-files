---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/admirable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484501941
title: admirable
categories:
    - Dictionary
---
admirable
     adj 1: deserving of the highest esteem or admiration; "an estimable
            young professor"; "trains ran with admirable
            precision"; "his taste was impeccable, his health
            admirable"
     2: inspiring admiration or approval; "among her many admirable
        qualities are generosity and graciousness"
