---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polynomial
offline_file: ""
offline_thumbnail: ""
uuid: efe1600d-6eda-4b35-ba71-5d1812373276
updated: 1484310156
title: polynomial
categories:
    - Dictionary
---
polynomial
     adj : having the character of a polynomial; "a polynomial
           expression" [syn: {multinomial}]
     n : a mathematical expression that is the sum of a number of
         terms [syn: {multinomial}]
