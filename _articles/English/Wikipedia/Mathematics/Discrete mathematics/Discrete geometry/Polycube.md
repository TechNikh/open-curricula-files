---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polycube
offline_file: ""
offline_thumbnail: ""
uuid: 81a137be-e2e2-470a-bc8b-0a29751580e3
updated: 1484308601
title: Polycube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Tetracubes.svg.png
tags:
    - Enumerating polycubes
    - Symmetries of polycubes
    - Properties of pentacubes
    - Octacubes and hypercube unfoldings
categories:
    - Discrete geometry
---
A polycube is a solid figure formed by joining one or more equal cubes face to face. Polycubes are the three-dimensional analogues of the planar polyominoes. The Soma cube, the Bedlam cube, the Diabolical cube, the Slothouber–Graatsma puzzle, and the Conway puzzle are examples of packing problems based on polycubes.[1]
