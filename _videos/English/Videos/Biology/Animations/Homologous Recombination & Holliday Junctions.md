---
version: 1
type: video
provider: YouTube
id: 3qgBKrAZCLg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0b46e286-ee91-4148-9bbf-64dd457a0275
updated: 1486069666
title: 'Homologous Recombination & Holliday Junctions'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/3qgBKrAZCLg/default.jpg
    - https://i3.ytimg.com/vi/3qgBKrAZCLg/1.jpg
    - https://i3.ytimg.com/vi/3qgBKrAZCLg/2.jpg
    - https://i3.ytimg.com/vi/3qgBKrAZCLg/3.jpg
---
