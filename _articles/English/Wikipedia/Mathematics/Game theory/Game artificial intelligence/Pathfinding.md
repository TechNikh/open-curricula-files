---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pathfinding
offline_file: ""
offline_thumbnail: ""
uuid: 9bed169f-b2ee-4e02-9f5d-4e5b18471c82
updated: 1484309006
title: Pathfinding
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Pathfinding_2D_Illustration.svg.png
tags:
    - Algorithms
    - "Dijkstra's Algorithm"
    - 'A* Algorithm'
    - Sample algorithm
    - In video games
    - Algorithms used in pathfinding
categories:
    - Game artificial intelligence
---
Pathfinding or pathing is the plotting, by a computer application, of the shortest route between two points. It is a more practical variant on solving mazes. This field of research is based heavily on Dijkstra's algorithm for finding a shortest path on a weighted graph.
