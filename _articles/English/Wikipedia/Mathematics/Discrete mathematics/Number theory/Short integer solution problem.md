---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Short_integer_solution_problem
offline_file: ""
offline_thumbnail: ""
uuid: 5396fe64-34bb-494f-a055-e181af8cc43e
updated: 1484308654
title: Short integer solution problem
tags:
    - Lattices
    - Ideal lattice
    - Cyclic lattices
    - 'Ideal latices [4][5]'
    - Short integer solution problem
    - SISn,m,q,β
    - Ring-SIS
    - R-SISm,q,β
categories:
    - Number theory
---
Short integer solution (SIS) and ring-SIS problems are two average-case problems that are used in lattice-based cryptography constructions. Lattice-based cryptography began in 1996 from a seminal work by Ajtai [1] who presented a family of one-way functions based on SIS problem. He showed that it is secure in average case if 
  
    
      
        
          
            S
            V
            P
          
          
            γ
          
        
      
    
    {\displaystyle \mathrm {SVP} _{\gamma }}
  
 (where 
  
    
      
        γ
        =
        
          n
          
            c
          
        
      
    
    {\displaystyle \gamma =n^{c}}
  
 for some ...
