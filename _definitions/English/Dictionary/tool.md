---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tool
offline_file: ""
offline_thumbnail: ""
uuid: d6a763dd-a34c-435e-9204-9d982530af8d
updated: 1484310557
title: tool
categories:
    - Dictionary
---
tool
     n 1: an implement used in the practice of a vocation
     2: the means whereby some act is accomplished; "my greed was
        the instrument of my destruction"; "science has given us
        new tools to fight disease" [syn: {instrument}]
     3: a person who is controlled by others and is used to perform
        unpleasant or dishonest tasks for someone else [syn: {creature},
         {puppet}]
     4: obscene terms for penis [syn: {cock}, {prick}, {dick}, {shaft},
         {pecker}, {peter}, {putz}]
     v 1: drive; "The convertible tooled down the street"
     2: ride in a car with no particular goal and just for the
        pleasure of it; "We tooled down the street" [syn: ...
