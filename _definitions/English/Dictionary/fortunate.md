---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fortunate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484517721
title: fortunate
categories:
    - Dictionary
---
fortunate
     adj 1: having unexpected good fortune; "other, less fortunate,
            children died"; "a fortunate choice" [ant: {unfortunate}]
     2: supremely favored or fortunate; "golden lads and girls all
        must / like chimney sweepers come to dust" [syn: {favored},
         {golden}]
     3: presaging good fortune; "she made a fortunate decision to go
        to medical school"; "rosy predictions" [syn: {hopeful}, {rosy}]
