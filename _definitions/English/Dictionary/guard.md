---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guard
offline_file: ""
offline_thumbnail: ""
uuid: ab9f9819-afd2-4a77-ae16-525a02795a5d
updated: 1484310381
title: guard
categories:
    - Dictionary
---
guard
     n 1: a person who keeps watch over something or someone
     2: the person who plays that position on a football team; "the
        left guard was injured on the play"
     3: a device designed to prevent injury [syn: {safety}, {safety
        device}]
     4: a posture of defence in boxing or fencing; "keep your guard
        up"
     5: the person who plays the position of guard on a basketball
        team
     6: a group of men who escort and protect some important person
        [syn: {bodyguard}]
     7: a precautionary measure warding off impending danger or
        damage or injury etc.; "he put an ice pack on the injury
        as a precaution"; "an insurance policy is a ...
