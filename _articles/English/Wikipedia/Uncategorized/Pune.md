---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pune
offline_file: ""
offline_thumbnail: ""
uuid: dcb779b7-203b-464c-850b-72e36358c194
updated: 1484308455
title: Pune
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bajirao_Peshwa.jpg
tags:
    - Toponymy
    - History
    - Early and medieval
    - Maratha era
    - Bhosale Jagir era
    - Peshwa rule
    - British rule (1818–1947)
    - Center of Social reform and Nationalism
    - Pune since Indian independence
    - geography
    - Seismology
    - Parts of Pune city
    - Climate
    - Demographics
    - population
    - religion
    - Spirituality
    - Economy
    - Industry
    - Administration
    - Civic administration
    - Military establishments
    - Education and research
    - Basic and special education
    - University education
    - Research Institutes
    - culture
    - Cuisine
    - Architecture
    - Museums, parks and zoos
    - Sports and recreation
    - cricket
    - Football
    - Other sports
    - Sports institutions
    - Transport
    - air
    - Rail
    - Metro
    - Roads
    - Sister cities
    - Notable people
    - Places of interest
categories:
    - Uncategorized
---
Pune (IPA: [puɳe] English pronunciation: /ˈpuːnə/;[6][7][8][9] spelt Poona during British rule) is the 9th most populous city in India and the second largest in the state of Maharashtra after the state capital Mumbai. Pune is also the 101st most populous city in the world.[10]
