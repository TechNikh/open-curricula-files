---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hubert_Bray
offline_file: ""
offline_thumbnail: ""
uuid: f8923ddb-e3c8-4c57-a7f5-1e24dc82aa39
updated: 1484309075
title: Hubert Bray
categories:
    - Geometers
---
Hubert Lewis Bray is a mathematician. He is known for having proved the Riemannian Penrose inequality.[1] He works as professor of mathematics and physics at Duke University.[2]
