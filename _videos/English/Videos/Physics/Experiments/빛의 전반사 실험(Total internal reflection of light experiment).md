---
version: 1
type: video
provider: YouTube
id: rikIwwMXW2E
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 225ae84c-4e2e-4e5e-af99-6648a14f9f8e
updated: 1486069636
title: >
    빛의 전반사 실험(Total internal reflection of light
    experiment)
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/rikIwwMXW2E/default.jpg
    - https://i3.ytimg.com/vi/rikIwwMXW2E/1.jpg
    - https://i3.ytimg.com/vi/rikIwwMXW2E/2.jpg
    - https://i3.ytimg.com/vi/rikIwwMXW2E/3.jpg
---
