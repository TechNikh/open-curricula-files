---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/charm
offline_file: ""
offline_thumbnail: ""
uuid: 91f46720-a9c6-4199-a2a6-566612e635e5
updated: 1484310152
title: charm
categories:
    - Dictionary
---
charm
     n 1: attractiveness that interests or pleases or stimulates; "his
          smile was part of his appeal to her" [syn: {appeal}, {appealingness}]
     2: a verbal formula believed to have magical force; "he
        whispered a spell as he moved his hands"; "inscribed
        around its base is a charm in Balinese" [syn: {spell}, {magic
        spell}]
     3: something believed to bring good luck [syn: {good luck charm}]
     v 1: attract; cause to be enamored; "She captured all the men's
          hearts" [syn: {capture}, {enamour}, {trance}, {catch}, {becharm},
           {enamor}, {captivate}, {beguile}, {fascinate}, {bewitch},
           {entrance}, {enchant}]
     2: control ...
