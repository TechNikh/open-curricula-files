---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/banking
offline_file: ""
offline_thumbnail: ""
uuid: 4307cc41-87cb-496e-89ee-87886029a9c3
updated: 1484310527
title: banking
categories:
    - Dictionary
---
banking
     n 1: engaging in the business of banking; maintaining savings and
          checking accounts and issuing loans and credit etc.
     2: transacting business with a bank; depositing or withdrawing
        funds or requesting a loan etc.
