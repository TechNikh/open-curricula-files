---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beg
offline_file: ""
offline_thumbnail: ""
uuid: 15810e55-01b3-4653-93b9-ff7f4e00cc4e
updated: 1484310531
title: beg
categories:
    - Dictionary
---
beg
     v 1: call upon in supplication; entreat; "I beg you to stop!"
          [syn: {implore}, {pray}]
     2: make a solicitation or entreaty for something; request
        urgently or persistently; "Henry IV solicited the Pope for
        a divorce"; "My neighbor keeps soliciting money for
        different charities" [syn: {solicit}, {tap}]
     3: ask to obtain free; "beg money and food"
     [also: {begging}, {begged}]
