---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/racist
offline_file: ""
offline_thumbnail: ""
uuid: d34b40f8-b6e9-4380-a6ed-0529f89a8ce0
updated: 1484310565
title: racist
categories:
    - Dictionary
---
racist
     adj 1: based on racial intolerance; "racist remarks"
     2: discriminatory especially on the basis of race or religion
        [syn: {antiblack}, {anti-Semitic}, {anti-Semite(a)}]
     n : a person with a prejudiced belief that one race is superior
         to others [syn: {racialist}]
