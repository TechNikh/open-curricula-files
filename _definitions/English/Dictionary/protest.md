---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/protest
offline_file: ""
offline_thumbnail: ""
uuid: c9332559-2cdc-42c5-b71c-59cdb42ade31
updated: 1484310441
title: protest
categories:
    - Dictionary
---
protest
     n 1: a formal and solemn declaration of objection; "they finished
          the game under protest to the league president"; "the
          senator rose to register his protest"; "the many
          protestations did not stay the execution" [syn: {protestation}]
     2: the act of protesting; a public (often organized)
        manifestation of dissent [syn: {objection}, {dissent}]
     3: the act of making a strong public expression of disagreement
        and disapproval; "he shouted his protests at the umpire";
        "a shower of protest was heard from the rear of the hall"
     v 1: utter words of protest
     2: express opposition through action or words; "dissent to the
 ...
