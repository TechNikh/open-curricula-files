---
version: 1
type: video
provider: YouTube
id: czsoNaf4MeY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: edd85e3f-7bea-4c57-95bc-aac765016846
updated: 1486069658
title: How to balance two forks on a toothpick
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/czsoNaf4MeY/default.jpg
    - https://i3.ytimg.com/vi/czsoNaf4MeY/1.jpg
    - https://i3.ytimg.com/vi/czsoNaf4MeY/2.jpg
    - https://i3.ytimg.com/vi/czsoNaf4MeY/3.jpg
---
