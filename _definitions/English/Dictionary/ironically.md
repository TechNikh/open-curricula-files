---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ironically
offline_file: ""
offline_thumbnail: ""
uuid: 249d3b36-393c-4168-a01d-bf5196bd8e5d
updated: 1484310541
title: ironically
categories:
    - Dictionary
---
ironically
     adv 1: contrary to plan or expectation; "ironically, he ended up
            losing money under his own plan"
     2: in an ironic manner; "she began to mimic him ironically"
