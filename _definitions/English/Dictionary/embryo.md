---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/embryo
offline_file: ""
offline_thumbnail: ""
uuid: d27aff02-377f-4989-a9a5-bdccbb856b10
updated: 1484310284
title: embryo
categories:
    - Dictionary
---
embryo
     n 1: (botany) a minute rudimentary plant contained within a seed
          or an archegonium
     2: an animal organism in the early stages of growth and
        differentiation that in higher forms merge into fetal
        stages but in lower forms terminate in commencement of
        larval life [syn: {conceptus}, {fertilized egg}]
