---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Corresponding_squares
offline_file: ""
offline_thumbnail: ""
uuid: de8c08d9-e05e-40c9-8b64-14920a4c72aa
updated: 1484309017
title: Corresponding squares
tags:
    - Details
    - Examples
    - A simple example
    - A second example
    - An example with separated key squares
    - An example with triangulation
    - Lasker-Reichhelm position
categories:
    - Chess theory
---
Corresponding squares (also called relative squares, sister squares and coordinate squares (Mednis 1987:11–12)) in chess occur in some chess endgames, usually ones that are mostly blocked. If squares x and y are corresponding squares, it means that if one player moves to x then the other player must move to y in order to hold his position. Usually there are several pairs of these squares, and the members of each pair are labeled with the same number, e.g. 1, 2, etc. In some cases they indicate which square the defending king must move to in order to keep the opposing king away. In other cases, a maneuver by one king puts the other player in a situation where he cannot move to the ...
