---
version: 1
type: video
provider: YouTube
id: jQoE_9x37mQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Rust%20-%20Prevention%20and%20treatment%20-%20Chemistry%20for%20All%20-%20The%20Fuse%20School.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 78fb789d-0d8a-4dfb-9a6f-39813745ae42
updated: 1486069662
title: 'Rust - Prevention and treatment | Chemistry for All | The Fuse School'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/jQoE_9x37mQ/default.jpg
    - https://i3.ytimg.com/vi/jQoE_9x37mQ/1.jpg
    - https://i3.ytimg.com/vi/jQoE_9x37mQ/2.jpg
    - https://i3.ytimg.com/vi/jQoE_9x37mQ/3.jpg
---
