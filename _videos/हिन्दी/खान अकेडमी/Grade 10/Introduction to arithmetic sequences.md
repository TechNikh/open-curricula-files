---
version: 1
type: video
provider: YouTube
id: zjNC0rLeKO4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f1f7c9b0-abc2-44e0-ba0c-d16b76b5eb88
updated: 1486069619
title: Introduction to arithmetic sequences
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/zjNC0rLeKO4/default.jpg
    - https://i3.ytimg.com/vi/zjNC0rLeKO4/1.jpg
    - https://i3.ytimg.com/vi/zjNC0rLeKO4/2.jpg
    - https://i3.ytimg.com/vi/zjNC0rLeKO4/3.jpg
---
