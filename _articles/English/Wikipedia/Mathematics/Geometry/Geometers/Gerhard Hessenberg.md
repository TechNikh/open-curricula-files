---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gerhard_Hessenberg
offline_file: ""
offline_thumbnail: ""
uuid: f255b101-c976-4626-b8a5-9f9989ab5966
updated: 1484309083
title: Gerhard Hessenberg
categories:
    - Geometers
---
Gerhard Hessenberg (Frankfurt, 16 August 1874 – Berlin, 16 November 1925) was a German mathematician. He received his Ph.D. from the University of Berlin in 1899 under the guidance of Hermann Schwarz and Lazarus Fuchs. His name is usually associated with projective geometry, where he is known for proving that Desargues' theorem is a consequence of Pappus's hexagon theorem,[1] and differential geometry where he is known for introducing the concept of a connection.[2][3] He was also a set theorist: the Hessenberg sum and product of ordinals are named after him. However, Hessenberg matrices are named for Karl Hessenberg, a near relative.
