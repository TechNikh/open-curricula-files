---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kumamoto
offline_file: ""
offline_thumbnail: ""
uuid: 9b31586d-2522-4e83-b1be-bdbd4923d8dd
updated: 1484308337
title: Kumamoto
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kumamoto_Metropolitan_Employment_Area_2010.svg.png
tags:
    - History
    - geography
    - Climate
    - Landmarks
    - government
    - Wards
    - Transport
    - Sports
    - Sporting events
    - Education
    - Notable people
    - Sister cities
categories:
    - Uncategorized
---
As of March 1, 2010, the city has an estimated population of 731,286 and a population density of 1,880 persons per km2. The total area is 389.53 km2.
