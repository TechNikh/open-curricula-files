---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Arrow%27s_impossibility_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 827747dd-c486-4641-a206-dadc706fd689
updated: 1484308664
title: "Arrow's impossibility theorem"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Diagram_for_part_one_of_Arrow%2527s_Impossibility_Theorem.svg.png'
tags:
    - Statement of the theorem
    - Independence of irrelevant alternatives (IIA)
    - Formal statement of the theorem
    - Informal proof
    - 'Part one: There is a "pivotal" voter for B over A'
    - 'Part two: The pivotal voter for B over A is a dictator for B over C'
    - 'Part three: There can be at most one dictator'
    - Interpretations of the theorem
    - 'Remark: Scalar rankings from a vector of attributes and the IIA property'
    - Other possibilities
    - Approaches investigating functions of preference profiles
    - Infinitely many individuals
    - Limiting the number of alternatives
    - Pairwise voting
    - Domain restrictions
    - Relaxing transitivity
    - Relaxing IIA
    - Relaxing the Pareto criterion
    - Remark
    - Social choice instead of social preference
    - Rated voting systems and other approaches
    - "Analogs to Arrow's Theorem"
    - Notes
categories:
    - Theorems in discrete mathematics
---
In Social Choice Theory, Arrow's impossibility theorem, the general possibility theorem or Arrow's paradox is an impossibility theorem stating that when voters have three or more distinct alternatives (options), no ranked order voting system can convert the ranked preferences of individuals into a community-wide (complete and transitive) ranking while also meeting a pre-specified set of criteria, unrestricted domain, non-dictatorship, Pareto efficiency, and independence of irrelevant alternatives. The theorem is often cited in discussions of voting theory as it is further interpreted by the Gibbard–Satterthwaite theorem.
