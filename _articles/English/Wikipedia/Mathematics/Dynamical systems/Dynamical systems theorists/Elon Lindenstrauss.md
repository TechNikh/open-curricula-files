---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Elon_Lindenstrauss
offline_file: ""
offline_thumbnail: ""
uuid: dfb5eda5-729d-4024-a160-c6bbfe4168a3
updated: 1484308684
title: Elon Lindenstrauss
tags:
    - Biography
    - Research
    - Awards
categories:
    - Dynamical systems theorists
---
Elon Lindenstrauss (Hebrew: אילון לינדנשטראוס‎‎, born August 1, 1970) is an Israeli mathematician, and a winner of the 2010 Fields Medal.[1][2]
