---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dyke
offline_file: ""
offline_thumbnail: ""
uuid: ed6bcde0-6bb0-44e0-af9e-4dd0c449737a
updated: 1484310236
title: dyke
categories:
    - Dictionary
---
dyke
     n 1: offensive terms for a lesbian who is noticeably masculine
          [syn: {butch}, {dike}]
     2: a barrier constructed to contain the flow of water or to
        keep out the sea [syn: {dam}, {dike}, {levee}]
     v : enclose with a dike; "dike the land to protect it from
         water" [syn: {dike}]
