---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reachability
offline_file: ""
offline_thumbnail: ""
uuid: 86499fec-a3c2-45f3-8aac-ac84d82b7aae
updated: 1484309405
title: Reachability
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Graph_suitable_for_Kameda%2527s_method.svg.png'
tags:
    - Definition
    - Algorithms
    - Floyd-Warshall Algorithm
    - "Thorup's Algorithm"
    - "Kameda's Algorithm"
    - Related problems
categories:
    - Graph connectivity
---
In graph theory, reachability refers to the ability to get from one vertex to another within a graph. A vertex 
  
    
      
        s
      
    
    {\displaystyle s}
  
 can reach a vertex 
  
    
      
        t
      
    
    {\displaystyle t}
  
 (and 
  
    
      
        t
      
    
    {\displaystyle t}
  
 is reachable from 
  
    
      
        s
      
    
    {\displaystyle s}
  
) if there exists a sequence of adjacent vertices (i.e. a path) which starts with 
  
    
      
        s
      
    
    {\displaystyle s}
  
 and ends with 
  
    
      
        t
      
    
    {\displaystyle t}
  
.
