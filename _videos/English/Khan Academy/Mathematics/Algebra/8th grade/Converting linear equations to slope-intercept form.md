---
version: 1
type: video
provider: YouTube
id: V6Xynlqc_tc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/8th%20grade/Converting%20linear%20equations%20to%20slope-intercept%20form.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e252bda5-62b3-4e86-8c64-d88d67a47342
updated: 1486069557
title: Converting linear equations to slope-intercept form
categories:
    - 8th grade
thumbnail_urls:
    - https://i3.ytimg.com/vi/V6Xynlqc_tc/default.jpg
    - https://i3.ytimg.com/vi/V6Xynlqc_tc/1.jpg
    - https://i3.ytimg.com/vi/V6Xynlqc_tc/2.jpg
    - https://i3.ytimg.com/vi/V6Xynlqc_tc/3.jpg
---
