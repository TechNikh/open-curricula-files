---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/celestial
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484320501
title: celestial
categories:
    - Dictionary
---
celestial
     adj 1: of or relating to the sky; "celestial map"; "a heavenly
            body" [syn: {heavenly}]
     2: relating to or inhabiting a divine heaven; "celestial
        beings"; "heavenly hosts" [syn: {heavenly}]
     3: of heaven or the spirit; "celestial peace"; "ethereal
        melodies"; "the supernal happiness of a quiet death" [syn:
         {ethereal}, {supernal}]
