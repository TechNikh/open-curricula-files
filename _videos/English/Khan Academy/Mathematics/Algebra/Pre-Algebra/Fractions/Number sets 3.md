---
version: 1
type: video
provider: YouTube
id: psyWUUkI-aw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Fractions/Number%20sets%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2736c4d9-d725-4a2a-9098-bb00529b0f00
updated: 1486069559
title: Number sets 3
categories:
    - Fractions
thumbnail_urls:
    - https://i3.ytimg.com/vi/psyWUUkI-aw/default.jpg
    - https://i3.ytimg.com/vi/psyWUUkI-aw/1.jpg
    - https://i3.ytimg.com/vi/psyWUUkI-aw/2.jpg
    - https://i3.ytimg.com/vi/psyWUUkI-aw/3.jpg
---
