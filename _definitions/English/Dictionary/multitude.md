---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/multitude
offline_file: ""
offline_thumbnail: ""
uuid: 8415e6f4-d839-4b2f-8696-16bc271be9a7
updated: 1484310325
title: multitude
categories:
    - Dictionary
---
multitude
     n 1: a large indefinite number; "a battalion of ants"; "a
          multitude of TV antennas"; "a plurality of religions"
          [syn: {battalion}, {large number}, {plurality}, {pack}]
     2: a large gathering of people [syn: {throng}, {concourse}]
     3: the common people generally; "separate the warriors from the
        mass"; "power to the people" [syn: {masses}, {mass}, {hoi
        polloi}, {people}]
