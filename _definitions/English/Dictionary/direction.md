---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/direction
offline_file: ""
offline_thumbnail: ""
uuid: b862928d-bda0-4bb2-949a-fb6d9998812c
updated: 1484310330
title: direction
categories:
    - Dictionary
---
direction
     n 1: a line leading to a place or point; "he looked the other
          direction"; "didn't know the way home" [syn: {way}]
     2: the spatial relation between something and the course along
        which it points or moves; "he checked the direction and
        velocity of the wind"
     3: a general course along which something has a tendency to
        develop; "I couldn't follow the direction of his
        thoughts"; "his ideals determined the direction of his
        career"; "they proposed a new direction for the firm"
     4: something that provides direction or advice as to a decision
        or course of action [syn: {guidance}, {counsel}, {counseling},
         ...
