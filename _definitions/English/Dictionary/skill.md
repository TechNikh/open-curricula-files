---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/skill
offline_file: ""
offline_thumbnail: ""
uuid: a9f6f05b-3891-4e28-b2c1-5372087c3334
updated: 1484310529
title: skill
categories:
    - Dictionary
---
skill
     n 1: an ability that has been acquired by training [syn: {accomplishment},
           {acquirement}, {acquisition}, {attainment}]
     2: ability to produce solutions in some problem domain; "the
        skill of a well-trained boxer"; "the sweet science of
        pugilism" [syn: {science}]
