---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alloyed
offline_file: ""
offline_thumbnail: ""
uuid: f88a951f-1361-4867-9518-dac03b5c6dff
updated: 1484310413
title: alloyed
categories:
    - Dictionary
---
alloyed
     adj 1: (used of metals) debased by mixture with an inferior element
     2: (used of metals) blended to obtain a desired property
