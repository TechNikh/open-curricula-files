---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alicia_Boole_Stott
offline_file: ""
offline_thumbnail: ""
uuid: 63c4e704-6654-40c7-8450-ea84ceeb2cd4
updated: 1484309073
title: Alicia Boole Stott
tags:
    - Early life
    - Early career
    - Later career
    - Legacy
    - Publications (external links)
    - Other external links
categories:
    - Geometers
---
Alicia Boole Stott (June 8, 1860 – December 17, 1940) was an Irish-English mathematician. Despite never holding an academic position, she made a number of valuable contributions to the field, receiving an honorary doctorate from University of Groningen .[1] She is best known for coining the term "polytope" for a convex solid in four (or more) dimensions, and having an impressive grasp of four-dimensional geometry from a very early age.[1]
