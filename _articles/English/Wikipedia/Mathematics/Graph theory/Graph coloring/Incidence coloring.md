---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Incidence_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 9c1864bd-56ff-4e11-83c6-486ebcd64b8f
updated: 1484309384
title: Incidence coloring
tags:
    - Definitions
    - History
    - Basic Results
    - Incidence coloring of some graph classes
    - Meshes
    - Halin graphs
    - k-degenerated graphs
    - Outerplanar graphs
    - Chordal rings
    - Powers of cycles
    - >
        Relation between incidence chromatic number and domination
        number of a graph
    - Interval incidence coloring
    - Fractional incidence coloring
    - Nordhaus–Gaddum inequality
    - Incidence coloring game
    - Additional links
categories:
    - Graph coloring
---
In graph theory, coloring generally implies assignment of labels to vertices, edges or faces in a graph. The incidence coloring is a special graph labeling where in each incidence of an edge with a vertex is assigned a color under certain constraints.
