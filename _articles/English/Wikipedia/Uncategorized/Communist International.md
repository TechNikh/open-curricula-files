---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Comintern
offline_file: ""
offline_thumbnail: ""
uuid: ba5bd607-f7de-4826-a46e-9f70dd342d23
updated: 1484308485
title: Communist International
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Communist-International-1920.jpg
tags:
    - Organizational history
    - Failure of the Second International
    - Impact of the Russian Revolution
    - Founding Congress
    - Second World Congress
    - Third World Congress
    - 'Fifth to Seventh World Congresses: 1925–1935'
    - The Second Period
    - Communist front organizations
    - The Third Period
categories:
    - Uncategorized
---
The Communist International, abbreviated as Comintern and also known as the Third International (1919–1943), was an international communist organization that advocated world communism. The International intended to fight "by all available means, including armed force, for the overthrow of the international bourgeoisie and for the creation of an international Soviet republic as a transition stage to the complete abolition of the State."[1]
