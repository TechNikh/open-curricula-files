---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/De_Gua%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 438797a2-f770-493f-aa02-ce2b2c3806fb
updated: 1484309111
title: "De Gua's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/310px-De_gua_theorem_1.svg.png
categories:
    - Theorems in geometry
---
If a tetrahedron has a right-angle corner (like the corner of a cube), then the square of the area of the face opposite the right-angle corner is the sum of the squares of the areas of the other three faces.
