---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shadow
offline_file: ""
offline_thumbnail: ""
uuid: 91143139-a7b5-4a38-ac5f-eda96fd607ba
updated: 1484310221
title: shadow
categories:
    - Dictionary
---
shadow
     n 1: shade within clear boundaries
     2: an unilluminated area; "he moved off into the darkness"
        [syn: {darkness}, {dark}]
     3: something existing in perception only; "a ghostly apparition
        at midnight" [syn: {apparition}, {phantom}, {phantasm}, {phantasma}]
     4: a premonition of something adverse; "a shadow over his
        happiness"
     5: an indication that something has been present; "there wasn't
        a trace of evidence for the claim"; "a tincture of
        condescension" [syn: {trace}, {vestige}, {tincture}]
     6: refuge from danger or observation; "he felt secure in his
        father's shadow"
     7: a dominating and pervasive presence; ...
