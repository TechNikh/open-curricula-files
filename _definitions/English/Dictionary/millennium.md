---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/millennium
offline_file: ""
offline_thumbnail: ""
uuid: f988a1ff-0add-4992-8bbc-1c2403cdc030
updated: 1484310301
title: millennium
categories:
    - Dictionary
---
millennium
     n 1: a span of 1000 years
     2: (New Testament) in Revelations it is foretold that those
        faithful to Jesus will reign with Jesus over the earth for
        a thousand years; the meaning of these words have been
        much debated; some denominations (e.g. Jehovah's
        Witnesses) expect it to be a thousand years of justice and
        peace and happiness
     3: the 1000th anniversary (or the celebration of it)
     [also: {millennia} (pl)]
