---
version: 1
type: video
provider: YouTube
id: kSx873lOgIc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Scientific%20notation%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c3146d8c-7dd3-4217-8909-da1a651d67c9
updated: 1486069567
title: Scientific notation 1
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/kSx873lOgIc/default.jpg
    - https://i3.ytimg.com/vi/kSx873lOgIc/1.jpg
    - https://i3.ytimg.com/vi/kSx873lOgIc/2.jpg
    - https://i3.ytimg.com/vi/kSx873lOgIc/3.jpg
---
