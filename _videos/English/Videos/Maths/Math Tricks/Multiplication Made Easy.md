---
version: 1
type: video
provider: YouTube
id: R2IQB9I7zX0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 789284ea-e3ad-4d88-944b-3db3eecd0299
updated: 1486069625
title: Multiplication Made Easy
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/R2IQB9I7zX0/default.jpg
    - https://i3.ytimg.com/vi/R2IQB9I7zX0/1.jpg
    - https://i3.ytimg.com/vi/R2IQB9I7zX0/2.jpg
    - https://i3.ytimg.com/vi/R2IQB9I7zX0/3.jpg
---
