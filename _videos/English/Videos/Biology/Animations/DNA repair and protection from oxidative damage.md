---
version: 1
type: video
provider: YouTube
id: VLA8eC6hjmg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b80ad949-f414-46e2-ae71-b4f5ac7e6907
updated: 1486069673
title: DNA repair and protection from oxidative damage
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/VLA8eC6hjmg/default.jpg
    - https://i3.ytimg.com/vi/VLA8eC6hjmg/1.jpg
    - https://i3.ytimg.com/vi/VLA8eC6hjmg/2.jpg
    - https://i3.ytimg.com/vi/VLA8eC6hjmg/3.jpg
---
