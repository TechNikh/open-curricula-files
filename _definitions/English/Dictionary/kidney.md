---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kidney
offline_file: ""
offline_thumbnail: ""
uuid: 88118294-8641-4465-ad03-769c26b1013e
updated: 1484310162
title: kidney
categories:
    - Dictionary
---
kidney
     n : either of two bean-shaped excretory organs that filter
         wastes (especially urea) from the blood and excrete them
         and water in urine; urine passes out of the kidney
         through ureters to the bladder
