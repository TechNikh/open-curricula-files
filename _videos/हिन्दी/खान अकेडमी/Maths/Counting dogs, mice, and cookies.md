---
version: 1
type: video
provider: YouTube
id: vZqmgFDjsss
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9c477649-bac8-42f7-a617-9ebb3f247aaf
updated: 1486069605
title: Counting dogs, mice, and cookies
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/vZqmgFDjsss/default.jpg
    - https://i3.ytimg.com/vi/vZqmgFDjsss/1.jpg
    - https://i3.ytimg.com/vi/vZqmgFDjsss/2.jpg
    - https://i3.ytimg.com/vi/vZqmgFDjsss/3.jpg
---
