---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bering
offline_file: ""
offline_thumbnail: ""
uuid: b363a86d-46e5-46b6-837e-fe6d73c51398
updated: 1484310279
title: bering
categories:
    - Dictionary
---
Bering
     n : Danish explorer who explored the northern Pacific Ocean for
         the Russians and discovered the Bering Strait (1681-1741)
         [syn: {Vitus Bering}, {Behring}, {Vitus Behring}]
