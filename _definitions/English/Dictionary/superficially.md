---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/superficially
offline_file: ""
offline_thumbnail: ""
uuid: 4fcb3eef-6fc5-4e90-b9a3-91a0c726dd8f
updated: 1484310599
title: superficially
categories:
    - Dictionary
---
superficially
     adv : in a superficial manner; "he was superficially interested"
