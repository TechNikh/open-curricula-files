---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/V%C3%ADctor_Neumann-Lara'
offline_file: ""
offline_thumbnail: ""
uuid: a76d4aac-62a2-4892-9224-aaeb6a9b0bb1
updated: 1484309470
title: Víctor Neumann-Lara
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/225px-VictorNsmall.jpg
tags:
    - Biography
    - Work
    - List of selected publications
categories:
    - Graph theorists
---
Víctor Neumann-Lara (1933–2004) was a Mexican mathematician, pioneer in the field of graph theory in Mexico. His work also covers general topology, game theory and combinatorics. He has an Erdős number of 2.
