---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silence
offline_file: ""
offline_thumbnail: ""
uuid: 84badf26-3152-45a6-b296-de1c359587e8
updated: 1484310605
title: silence
categories:
    - Dictionary
---
silence
     n 1: the state of being silent (as when no one is speaking);
          "there was a shocked silence"; "he gestured for silence"
     2: the absence of sound; "he needed silence in order to sleep";
        "the street was quiet" [syn: {quiet}] [ant: {sound}]
     3: a refusal to speak when expected; "his silence about my
        contribution was surprising" [syn: {muteness}]
     4: the trait of keeping things secret [syn: {secrecy}, {secretiveness}]
     v 1: cause to be quiet or not talk; "Please silence the children
          in the church!" [syn: {hush}, {quieten}, {still}, {shut
          up}, {hush up}] [ant: {louden}]
     2: keep from expression, for example by threats ...
