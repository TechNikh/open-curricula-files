---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/separable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484585461
title: separable
categories:
    - Dictionary
---
separable
     adj : capable of being divided or dissociated; "often drugs and
           crime are not dissociable"; "the siamese twins were not
           considered seperable"; "a song...never conceived of as
           severable from the melody"; [syn: {dissociable}, {severable}]
