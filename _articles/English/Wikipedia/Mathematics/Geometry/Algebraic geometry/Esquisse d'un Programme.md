---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Esquisse_d%27un_Programme'
offline_file: ""
offline_thumbnail: ""
uuid: cf398110-343b-411f-bb2b-f7e6fb7047f8
updated: 1484309145
title: "Esquisse d'un Programme"
tags:
    - Brief history
    - "Abstract of Grothendieck's programme"
    - "Extensions of Galois's theory for groups: Galois groupoids, categories and functors"
    - Related works by Alexander Grothendieck
    - Other related publications
categories:
    - Algebraic geometry
---
"Esquisse d'un Programme" is a famous proposal for long-term mathematical research made by the German-born, French mathematician Alexander Grothendieck in 1984.[1] He pursued the sequence of logically linked ideas in his important project proposal from 1984 until 1988, but his proposed research continues to date to be of major interest in several branches of advanced mathematics. Grothendieck's vision provides inspiration today for several developments in mathematics such as the extension and generalization of Galois theory, which is currently being extended based on his original proposal.
