---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Boiled_rice
offline_file: ""
offline_thumbnail: ""
uuid: a4d73a53-f0d6-451a-a3d2-cc08c33ad027
updated: 1484308372
title: Rice
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-White%252C_Brown%252C_Red_%2526_Wild_rice.jpg'
tags:
    - Etymology
    - Cooking
    - Preparation
    - Processing
    - Dishes
    - Nutrition and health
    - Importance
    - Comparison to other major staple foods
    - Arsenic concerns
    - Bacillus cereus
    - Rice-growing environments
    - History of domestication and cultivation
    - Regional history
    - Africa
    - Asia
    - Nepal
    - Philippines
    - Sri Lanka
    - Thailand
    - Companion plant
    - Middle East
    - Europe
    - Caribbean and Latin America
    - United States
    - Australia
    - Production and commerce
    - Production
    - Harvesting, drying and milling
    - Distribution
    - Trade
    - "World's most productive rice farms and farmers"
    - price
    - Worldwide consumption
    - Environmental impacts
    - Rainfall
    - Temperature
    - Solar radiation
    - Atmospheric water vapor
    - Wind
    - Pests and diseases
    - Insects
    - Diseases
    - Nematodes
    - Other pests
    - Integrated pest management
    - Parasitic weeds
    - Ecotypes and cultivars
    - Biotechnology
    - High-yielding varieties
    - Future potential
    - Golden rice
    - Expression of human proteins
    - Flood-tolerant rice
    - Drought-tolerant rice
    - Salt-tolerant rice
    - Environment-friendly rice
    - Meiosis and DNA repair
    - Cultural roles of rice
    - United States
categories:
    - Uncategorized
---
Rice is the seed of the grass species Oryza sativa (Asian rice) or Oryza glaberrima (African rice). As a cereal grain, it is the most widely consumed staple food for a large part of the world's human population, especially in Asia. It is the agricultural commodity with the third-highest worldwide production, after sugarcane and maize, according to 2012 FAOSTAT data.[1]
