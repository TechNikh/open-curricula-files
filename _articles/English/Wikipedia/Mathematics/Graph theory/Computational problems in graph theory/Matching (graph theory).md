---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Matching_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 2d84c7fa-bc9f-490e-8881-46e8ebf305b3
updated: 1484309394
title: Matching (graph theory)
tags:
    - Definition
    - Properties
    - Matching polynomials
    - Algorithms and computational complexity
    - In unweighted bipartite graphs
    - In weighted bipartite graphs
    - In general graphs
    - Maximal matchings
    - Counting problems
    - Finding all maximally-matchable edges
    - Characterizations and notes
    - Applications
    - Matching in general graphs
    - Matching in bipartite graphs
categories:
    - Computational problems in graph theory
---
In the mathematical discipline of graph theory, a matching or independent edge set in a graph is a set of edges without common vertices. It may also be an entire graph consisting of edges without common vertices. Bipartite matching is a special case of a network flow problem.
