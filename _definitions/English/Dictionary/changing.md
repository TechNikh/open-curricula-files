---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/changing
offline_file: ""
offline_thumbnail: ""
uuid: 86b18360-4154-4f7c-84b4-4405e48d6abc
updated: 1484310226
title: changing
categories:
    - Dictionary
---
changing
     adj : marked by continuous change or effective action [syn: {ever-changing}]
