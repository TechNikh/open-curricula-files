---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Iso-butane
offline_file: ""
offline_thumbnail: ""
uuid: 466c227d-29dc-4b93-8321-efce29868928
updated: 1484308398
title: Isobutane
tags:
    - Nomenclature
    - Uses
    - Refrigerant use
categories:
    - Uncategorized
---
Isobutane (i-butane), also known as methylpropane, is a chemical compound with molecular formula C
4H
10 and is an isomer of butane. It is the simplest alkane with a tertiary carbon. Concerns with depletion of the ozone layer by freon gases have led to increased use of isobutane as a gas for refrigeration systems, especially in domestic refrigerators and freezers, and as a propellant in aerosol sprays.
