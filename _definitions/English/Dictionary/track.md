---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/track
offline_file: ""
offline_thumbnail: ""
uuid: 71964da5-a60d-4152-a1b1-2a8268f67a7a
updated: 1484310316
title: track
categories:
    - Dictionary
---
track
     n 1: a line or route along which something travels or moves; "the
          hurricane demolished houses in its path"; "the track of
          an animal"; "the course of the river" [syn: {path}, {course}]
     2: evidence pointing to a possible solution; "the police are
        following a promising lead"; "the trail led straight to
        the perpetrator" [syn: {lead}, {trail}]
     3: a pair of parallel rails providing a runway for wheels
     4: a course over which races are run [syn: {racetrack}, {racecourse},
         {raceway}]
     5: a distinct selection of music from a recording or a compact
        disc; "he played the first cut on the cd"; "the title
        track of ...
