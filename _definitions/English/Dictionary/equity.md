---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equity
offline_file: ""
offline_thumbnail: ""
uuid: b2ca7af2-49dd-4200-a4ac-5b772d2b2038
updated: 1484310462
title: equity
categories:
    - Dictionary
---
equity
     n 1: the difference between the market value of a property and
          the claims held against it
     2: the ownership interest of shareholders in a corporation
     3: conformity with rules or standards; "the judge recognized
        the fairness of my claim" [syn: {fairness}] [ant: {unfairness},
         {unfairness}]
