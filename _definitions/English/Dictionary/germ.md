---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/germ
offline_file: ""
offline_thumbnail: ""
uuid: 079238d1-640f-44aa-8eb6-22b59e5cf425
updated: 1484310293
title: germ
categories:
    - Dictionary
---
germ
     n 1: anything that provides inspiration for later work [syn: {source},
           {seed}]
     2: a small simple structure (as a fertilized egg) from which
        new tissue can develop into a complete organism
     3: a minute life form (especially a disease-causing bacterium);
        the term is not in technical use [syn: {microbe}, {bug}]
