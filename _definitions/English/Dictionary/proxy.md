---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proxy
offline_file: ""
offline_thumbnail: ""
uuid: 05bf3713-b95b-45b8-841d-c674971f2d47
updated: 1484310180
title: proxy
categories:
    - Dictionary
---
proxy
     n 1: a person authorized to act for another [syn: {placeholder},
          {procurator}]
     2: a power of attorney document given by shareholders of a
        corporation authorizing a specific vote on their behalf at
        a corporate meeting
