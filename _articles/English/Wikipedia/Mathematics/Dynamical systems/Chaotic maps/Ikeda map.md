---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ikeda_map
offline_file: ""
offline_thumbnail: ""
uuid: 376c8410-03d7-4800-8c8b-0df709d6bcd4
updated: 1484308713
title: Ikeda map
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Ikeda_map_simulation_u%253D0.918_cropped.png'
tags:
    - Attractor
    - Point trajectories
    - Octave/MATLAB code for point trajectories
categories:
    - Chaotic maps
---
The original map was proposed first by Ikeda as a model of light going around across a nonlinear optical resonator (ring cavity containing a nonlinear dielectric medium) in a more general form. It is reduced to the above simplified "normal" form by Ikeda, Daido and Akimoto [1] 
  
    
      
        
          z
          
            n
          
        
      
    
    {\displaystyle z_{n}}
  
 stands for the electric field inside the resonator at the n-th step of rotation in the resonator, and 
  
    
      
        A
      
    
    {\displaystyle A}
  
 and 
  
    
      
        C
      
    
    {\displaystyle C}
  
 are parameters which indicates laser light applied from the ...
