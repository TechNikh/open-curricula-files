---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/celsius
offline_file: ""
offline_thumbnail: ""
uuid: 98a7a8d9-aefb-43c1-8156-e7534601a96f
updated: 1484310234
title: celsius
categories:
    - Dictionary
---
Celsius
     n : Swedish astronomer who devised the centigrade thermometer
         (1701-1744) [syn: {Anders Celsius}]
