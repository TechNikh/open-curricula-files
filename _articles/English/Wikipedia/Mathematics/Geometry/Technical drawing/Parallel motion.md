---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parallel_motion
offline_file: ""
offline_thumbnail: ""
uuid: a7b9210f-0ccb-4c1a-bc6a-02ff51fef9f1
updated: 1484309329
title: Parallel motion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Watt_Parallel_Motion_Simulation.gif
tags:
    - Description
    - Principle of operation
categories:
    - Technical drawing
---
The parallel motion is a mechanical linkage invented by the Scottish engineer James Watt in 1784 for the double-acting Watt steam engine. It allows a rod moving straight up and down to transmit motion to a beam moving in an arc, without putting sideways strain on the rod.
