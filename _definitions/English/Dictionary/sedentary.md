---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sedentary
offline_file: ""
offline_thumbnail: ""
uuid: 4d05172e-16c9-4ef6-a990-f765ec3fe50b
updated: 1484310475
title: sedentary
categories:
    - Dictionary
---
sedentary
     adj : used of persons or actions; "forced by illness to lead a
           sedentary life"
