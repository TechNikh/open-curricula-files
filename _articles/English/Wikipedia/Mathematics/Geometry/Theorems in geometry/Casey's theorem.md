---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Casey%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 3969db30-ecfb-4984-95d4-741291c71c4d
updated: 1484309111
title: "Casey's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Theorem_of_casey2.png
tags:
    - Formulation of the theorem
    - Proof
    - Further generalizations
    - Applications
categories:
    - Theorems in geometry
---
In mathematics, Casey's theorem, also known as the generalized Ptolemy's theorem, is a theorem in Euclidean geometry named after the Irish mathematician John Casey.
