---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/message
offline_file: ""
offline_thumbnail: ""
uuid: f3eed784-e623-436b-ac24-790b0ae58c51
updated: 1484310473
title: message
categories:
    - Dictionary
---
message
     n 1: a communication (usually brief) that is written or spoken or
          signaled; "he sent a three-word message"
     2: what a communication that is about something is about [syn:
        {content}, {subject matter}, {substance}]
     v 1: send a message to; "She messaged the committee"
     2: send as a message; "She messaged the final report by fax"
     3: send a message; "There is no messaging service at this
        company"
