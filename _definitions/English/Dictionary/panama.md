---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/panama
offline_file: ""
offline_thumbnail: ""
uuid: c700ace4-147f-4013-9fd8-36a09f21af48
updated: 1484310246
title: panama
categories:
    - Dictionary
---
Panama
     n 1: a republic on the Isthmus of Panama; achieved independence
          from Colombia in 1903 [syn: {Republic of Panama}]
     2: a stiff straw hat with a flat crown [syn: {boater}, {leghorn},
         {Panama hat}, {sailor}, {skimmer}, {straw hat}]
