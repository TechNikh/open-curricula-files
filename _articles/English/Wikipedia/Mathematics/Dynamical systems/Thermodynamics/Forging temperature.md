---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Forging_temperature
offline_file: ""
offline_thumbnail: ""
uuid: ea099100-878c-4794-acf4-69c95a887e98
updated: 1484308846
title: Forging temperature
categories:
    - Thermodynamics
---
Forging temperature is the temperature at which a metal becomes substantially more soft, but is lower than the melting temperature.[1] Bringing a metal to its forging temperature allows the metal's shape to be changed by applying a relatively small force, without creating cracks. The forging temperature of an alloy will lie between the temperatures of its component metals. For most metals, forging temperature will be approximately 60% of the melting temperature in Kelvin.
