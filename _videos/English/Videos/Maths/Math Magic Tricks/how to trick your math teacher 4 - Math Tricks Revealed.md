---
version: 1
type: video
provider: YouTube
id: Nr1M-L2S82w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 76eae936-d600-480c-8eaa-fc073ce6ecb7
updated: 1486069622
title: 'how to trick your math teacher 4 - Math Tricks Revealed'
categories:
    - Math Magic Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Nr1M-L2S82w/default.jpg
    - https://i3.ytimg.com/vi/Nr1M-L2S82w/1.jpg
    - https://i3.ytimg.com/vi/Nr1M-L2S82w/2.jpg
    - https://i3.ytimg.com/vi/Nr1M-L2S82w/3.jpg
---
