---
version: 1
type: video
provider: YouTube
id: AUCeEa7QS-8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5da7f40b-7cdc-4044-9913-a25f6f988de6
updated: 1486069632
title: Changing Direct into Indirect speech 2
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/AUCeEa7QS-8/default.jpg
    - https://i3.ytimg.com/vi/AUCeEa7QS-8/1.jpg
    - https://i3.ytimg.com/vi/AUCeEa7QS-8/2.jpg
    - https://i3.ytimg.com/vi/AUCeEa7QS-8/3.jpg
---
