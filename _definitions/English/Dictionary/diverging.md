---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diverging
offline_file: ""
offline_thumbnail: ""
uuid: ce00da3c-529a-4df6-8f33-e66768c22121
updated: 1484310218
title: diverging
categories:
    - Dictionary
---
diverging
     adj : tending to move apart in different directions [syn: {divergent}]
           [ant: {convergent}]
