---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weakly
offline_file: ""
offline_thumbnail: ""
uuid: 4714726b-db68-4217-a0c6-6086f837ab46
updated: 1484310389
title: weakly
categories:
    - Dictionary
---
weakly
     adj : lacking physical strength or vitality; "a feeble old woman";
           "her body looked sapless" [syn: {decrepit}, {debile}, {feeble},
            {infirm}, {sapless}, {weak}]
     adv : in a weak or feeble manner or to a minor degree; "weakly
           agreed to a compromise"; "wheezed weakly"; "he was
           weakly attracted to her" [ant: {strongly}]
     [also: {weakliest}, {weaklier}]
