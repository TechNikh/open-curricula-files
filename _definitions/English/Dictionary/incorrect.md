---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incorrect
offline_file: ""
offline_thumbnail: ""
uuid: 52b99bc7-0f25-4415-9d88-f0c4079e4f4a
updated: 1484310142
title: incorrect
categories:
    - Dictionary
---
incorrect
     adj 1: not correct; not in conformity with fact or truth; "an
            incorrect calculation"; "the report in the paper is
            wrong"; "your information is wrong"; "the clock showed
            the wrong time"; "found themselves on the wrong road";
            "based on the wrong assumptions" [syn: {wrong}] [ant:
            {correct}, {correct}]
     2: not conforming with accepted standards of propriety or
        taste; undesirable; "incorrect behavior"; "she was seen in
        all the wrong places"; "He thought it was wrong for her to
        go out to work" [syn: {inappropriate}, {wrong}]
