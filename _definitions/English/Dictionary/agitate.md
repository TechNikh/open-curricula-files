---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agitate
offline_file: ""
offline_thumbnail: ""
uuid: 604dc889-ebab-4b7e-b394-dc9dd83ea70d
updated: 1484310236
title: agitate
categories:
    - Dictionary
---
agitate
     v 1: try to stir up public opinion [syn: {foment}, {stir up}]
     2: cause to be agitated, excited, or roused; "The speaker
        charged up the crowd with his inflammatory remarks" [syn:
        {rouse}, {turn on}, {charge}, {commove}, {excite}, {charge
        up}] [ant: {calm}]
     3: exert oneself continuously, vigorously, or obtrusively to
        gain an end or engage in a crusade for a certain cause or
        person; be an advocate for; "The liberal party pushed for
        reforms"; "She is crusading for women's rights"; "The Dean
        is pushing for his favorite candidate" [syn: {crusade}, {fight},
         {press}, {campaign}, {push}]
     4: move very ...
