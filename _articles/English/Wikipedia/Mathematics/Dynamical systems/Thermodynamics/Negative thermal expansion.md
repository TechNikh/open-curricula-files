---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Negative_thermal_expansion
offline_file: ""
offline_thumbnail: ""
uuid: 42ebc7b8-cbc1-4ef6-a67f-8653f09ab936
updated: 1484308846
title: Negative thermal expansion
tags:
    - Origin of negative thermal expansion
    - Negative thermal expansion in close-packed systems
    - Applications
    - Materials
categories:
    - Thermodynamics
---
Negative thermal expansion is a physicochemical process in which some materials contract upon heating rather than expand as most materials do. Materials which undergo this unusual process have a range of potential engineering, photonic, electronic, and structural applications. For example, if one were to mix a negative thermal expansion material with a "normal" material which expands on heating, it could be possible to make a zero expansion composite material.
