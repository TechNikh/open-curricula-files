---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/opaque
offline_file: ""
offline_thumbnail: ""
uuid: f7f49ba3-1e48-43c5-a0c6-39f0b36325da
updated: 1484310221
title: opaque
categories:
    - Dictionary
---
opaque
     adj 1: not clear; not transmitting or reflecting light or radiant
            energy; "opaque windows of the jail"; "opaque to
            X-rays" [ant: {clear}]
     2: not clearly understood or expressed [syn: {unintelligible}]
