---
version: 1
type: video
provider: YouTube
id: szFW1WLyLu4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4ddc0761-b82a-4cdb-8048-b3054da1569d
updated: 1486069651
title: 'Caleidoscope Sirius - Mandelbulb 3D fractal HD feat. Psyrius "Mercury" from "Sorry Pluto"'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/szFW1WLyLu4/default.jpg
    - https://i3.ytimg.com/vi/szFW1WLyLu4/1.jpg
    - https://i3.ytimg.com/vi/szFW1WLyLu4/2.jpg
    - https://i3.ytimg.com/vi/szFW1WLyLu4/3.jpg
---
