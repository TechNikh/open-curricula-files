---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Karl_Wilhelm_Feuerbach
offline_file: ""
offline_thumbnail: ""
uuid: 3f70f620-bd99-44c5-ab42-37d0bfcf7c49
updated: 1484309085
title: Karl Wilhelm Feuerbach
categories:
    - Geometers
---
Karl Wilhelm von Feuerbach (30 May 1800 – 12 March 1834) was a German geometer and the son of legal scholar Paul Johann Anselm Ritter von Feuerbach, and the brother of philosopher Ludwig Feuerbach. After receiving his doctorate at age 22, he became a professor of mathematics at the Gymnasium at Erlangen. In 1822 he wrote a small book on mathematics noted mainly for a theorem on the nine-point circle, which is now known as Feuerbach's theorem. In 1827 he introduced homogeneous coordinates, independently of Möbius.[1]
