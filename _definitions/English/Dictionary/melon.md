---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/melon
offline_file: ""
offline_thumbnail: ""
uuid: 126825fc-7db7-4a46-bcd6-6372a11a3978
updated: 1484310232
title: melon
categories:
    - Dictionary
---
melon
     n 1: any of numerous fruits of the gourd family having a hard
          rind and sweet juicy flesh
     2: any of various fruit of cucurbitaceous vines including:
        muskmelons; watermelons; cantaloupes; cucumbers [syn: {melon
        vine}]
