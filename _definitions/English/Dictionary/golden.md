---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/golden
offline_file: ""
offline_thumbnail: ""
uuid: 5356fca0-0e56-44fe-bdef-70a77ee2065e
updated: 1484310188
title: golden
categories:
    - Dictionary
---
golden
     adj 1: having the deep slightly brownish color of gold; "long
            aureate (or golden) hair"; "a gold carpet" [syn: {aureate},
             {gilded}, {gilt}, {gold}]
     2: marked by peace and prosperity; "a golden era"; "the halcyon
        days of the clipper trade" [syn: {halcyon}, {prosperous}]
     3: made from or covered with gold; "gold coins"; "the gold dome
        of the Capitol"; "the golden calf"; "gilded icons" [syn: {gold},
         {gilded}]
     4: supremely favored or fortunate; "golden lads and girls all
        must / like chimney sweepers come to dust" [syn: {favored},
         {fortunate}]
     5: suggestive of gold; "a golden voice"
     6: very ...
