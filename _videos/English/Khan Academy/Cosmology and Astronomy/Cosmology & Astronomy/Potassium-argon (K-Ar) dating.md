---
version: 1
type: video
provider: YouTube
id: NMZ5kJEviD0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Potassium-argon%20%28K-Ar%29%20dating.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e5bd0267-5bc5-43bd-ba5c-d077f96baf5d
updated: 1486069686
title: Potassium-argon (K-Ar) dating
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/NMZ5kJEviD0/default.jpg
    - https://i3.ytimg.com/vi/NMZ5kJEviD0/1.jpg
    - https://i3.ytimg.com/vi/NMZ5kJEviD0/2.jpg
    - https://i3.ytimg.com/vi/NMZ5kJEviD0/3.jpg
---
