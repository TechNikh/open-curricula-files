---
version: 1
type: article
id: https://en.wikipedia.org/wiki/P-adic_distribution
offline_file: ""
offline_thumbnail: ""
uuid: 04f66380-a329-4845-af04-68965ffa6d60
updated: 1484308649
title: P-adic distribution
categories:
    - Number theory
---
In mathematics, a p-adic distribution is an analogue of ordinary distributions (i.e. generalized functions) that takes values in a ring of p-adic numbers.
