---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rob
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484499721
title: rob
categories:
    - Dictionary
---
rob
     v 1: take something away by force or without the consent of the
          owner; "The burglars robbed him of all his money"
     2: rip off; ask an unreasonable price [syn: {overcharge}, {soak},
         {surcharge}, {gazump}, {fleece}, {plume}, {pluck}, {hook}]
        [ant: {undercharge}]
     [also: {robbing}, {robbed}]
