---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Victor_Th%C3%A9bault'
offline_file: ""
offline_thumbnail: ""
uuid: 35789bec-ee2a-4268-ad4a-5b8861c96837
updated: 1484309100
title: Victor Thébault
categories:
    - Geometers
---
Victor Michael Jean-Marie Thébault (1882–1960) was a French mathematician best known for propounding three problems in geometry. Thébault's theorem is used in some references to refer to the first of these problems, in other references to the third.
