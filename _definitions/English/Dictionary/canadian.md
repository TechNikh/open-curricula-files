---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/canadian
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484523361
title: canadian
categories:
    - Dictionary
---
Canadian
     adj : of or relating to Canada or its people
     n 1: a native or inhabitant of Canada
     2: a river rising in northeastern New Mexico and flowing
        eastward across the Texas panhandle to become a tributary
        of the Arkansas River in Oklahoma [syn: {Canadian River}]
