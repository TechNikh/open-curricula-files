---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sort
offline_file: ""
offline_thumbnail: ""
uuid: 09296945-66f4-4326-8c4a-da33fbe39821
updated: 1484310240
title: sort
categories:
    - Dictionary
---
sort
     n 1: a category of things distinguished by some common
          characteristic or quality; "sculpture is a form of art";
          "what kinds of desserts are there?" [syn: {kind}, {form},
           {variety}]
     2: an approximate definition or example; "she wore a sort of
        magenta dress"; "she served a creamy sort of dessert
        thing"
     3: a person of a particular character or nature; "what sort of
        person is he?"; "he's a good sort"
     4: an operation that segregates items into groups according to
        a specified criterion; "the bottleneck in mail delivery it
        the process of sorting" [syn: {sorting}]
     v 1: examine in order to test ...
