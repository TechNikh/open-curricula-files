---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nearer
offline_file: ""
offline_thumbnail: ""
uuid: bda3dcdc-54ec-49d3-8e5b-0788737c5bc2
updated: 1484310266
title: nearer
categories:
    - Dictionary
---
nearer
     adj : (comparative of `near') being the one of two that is less
           distant in space; "we walked to the nearer house"
     adv : (comparative of `near' or `close') within a shorter
           distance; "come closer, my dear!"; "they drew nearer";
           "getting nearer to the true explanation" [syn: {nigher},
            {closer}]
