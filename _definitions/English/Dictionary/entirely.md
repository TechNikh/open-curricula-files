---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entirely
offline_file: ""
offline_thumbnail: ""
uuid: 6886b53b-1d62-48d8-bbc5-ca936a8e869f
updated: 1484310210
title: entirely
categories:
    - Dictionary
---
entirely
     adv 1: to a complete degree or to the full or entire extent
            (`whole' is often used informally for `wholly'); "he
            was wholly convinced"; "entirely satisfied with the
            meal"; "it was completely different from what we
            expected"; "was completely at fault"; "a totally new
            situation"; "the directions were all wrong"; "it was
            not altogether her fault"; "an altogether new
            approach"; "a whole new idea" [syn: {wholly}, {completely},
             {totally}, {all}, {altogether}, {whole}] [ant: {partially}]
     2: without any others being included or involved; "was entirely
        to blame"; "a school ...
