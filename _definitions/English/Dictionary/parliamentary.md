---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parliamentary
offline_file: ""
offline_thumbnail: ""
uuid: 7f191567-56bf-47b3-8791-c44bfe3b7f67
updated: 1484310595
title: parliamentary
categories:
    - Dictionary
---
parliamentary
     adj 1: relating to or having the nature of a parliament;
            "parliamentary reform"; "a parliamentary body"
     2: having the supreme legislative power resting with a body of
        cabinet ministers chosen from and responsible to the
        legislature or parliament; "parliamentary government"
     3: in accord with rules and customs of a legislative or
        deliberative assembly; "parliamentary law"
