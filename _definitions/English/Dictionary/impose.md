---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impose
offline_file: ""
offline_thumbnail: ""
uuid: 112efc00-d086-4f0b-a13e-47de27779069
updated: 1484310535
title: impose
categories:
    - Dictionary
---
impose
     v 1: compel to behave in a certain way; "Social relations impose
          courtesy" [syn: {enforce}]
     2: impose something unpleasant; "The principal visited his rage
        on the students" [syn: {inflict}, {bring down}, {visit}]
     3: impose and collect; "levy a fine" [syn: {levy}]
