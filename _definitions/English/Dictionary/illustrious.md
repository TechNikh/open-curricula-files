---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illustrious
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484524741
title: illustrious
categories:
    - Dictionary
---
illustrious
     adj 1: widely known and esteemed; "a famous actor"; "a celebrated
            musician"; "a famed scientist"; "an illustrious
            judge"; "a notable historian"; "a renowned painter"
            [syn: {celebrated}, {famed}, {far-famed}, {famous}, {notable},
             {noted}, {renowned}]
     2: having or conferring glory; "an illustrious achievement"
     3: having or worthy of pride; "redoubtable scholar of the
        Renaissance"; "born of a redoubtable family" [syn: {glorious},
         {redoubtable}, {respected}]
