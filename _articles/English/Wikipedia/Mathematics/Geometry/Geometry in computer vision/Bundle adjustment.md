---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bundle_adjustment
offline_file: ""
offline_thumbnail: ""
uuid: 4370d15b-7347-4547-8fc0-bc2d668eff72
updated: 1484309184
title: Bundle adjustment
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bundle_adjustment_sparse_matrix.png
tags:
    - Uses
    - General approach
    - Mathematical definition
    - Software
categories:
    - Geometry in computer vision
---
Given a set of images depicting a number of 3D points from different viewpoints, bundle adjustment can be defined as the problem of simultaneously refining the 3D coordinates describing the scene geometry, the parameters of the relative motion, and the optical characteristics of the camera(s) employed to acquire the images, according to an optimality criterion involving the corresponding image projections of all points.
