---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smoothly
offline_file: ""
offline_thumbnail: ""
uuid: 8ac20861-0a0b-4329-8b83-1d49c4445bc7
updated: 1484310321
title: smoothly
categories:
    - Dictionary
---
smoothly
     adv 1: with no problems or difficulties; "put the plans into effect
            quickly and smoothly"
     2: with great ease and success; "in spite of some mishaps,
        everything went swimmingly" [syn: {swimmingly}]
     3: in a smooth and diplomatic manner; "`And now,' he said
        smoothly, `we will continue the conversation'"
