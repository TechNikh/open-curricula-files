---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nanometre
offline_file: ""
offline_thumbnail: ""
uuid: 2456b945-514d-4572-96b1-5f5b4cf1eaa5
updated: 1484310403
title: nanometre
categories:
    - Dictionary
---
nanometre
     n : a metric unit of length equal to one billionth of a meter
         [syn: {nanometer}, {nm}, {millimicron}, {micromillimeter},
          {micromillimetre}]
