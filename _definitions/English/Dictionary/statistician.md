---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/statistician
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484383741
title: statistician
categories:
    - Dictionary
---
statistician
     n 1: a mathematician who specializes in statistics [syn: {mathematical
          statistician}]
     2: someone versed in the collection and interpretation of
        numerical data (especially someone who uses statistics to
        calculate insurance premiums) [syn: {actuary}]
