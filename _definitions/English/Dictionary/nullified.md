---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nullified
offline_file: ""
offline_thumbnail: ""
uuid: 361857c4-fa2d-460c-a30a-c98c7c2ce67b
updated: 1484310383
title: nullified
categories:
    - Dictionary
---
nullify
     v 1: declare invalid; "The contract was annulled"; "void a plea"
          [syn: {invalidate}, {annul}, {quash}, {void}, {avoid}]
          [ant: {validate}]
     2: show to be invalid [syn: {invalidate}] [ant: {validate}]
     3: make ineffective by counterbalancing the effect of; "Her
        optimism neutralizes his gloom"; "This action will negate
        the effect of my efforts" [syn: {neutralize}, {neutralise},
         {negate}]
     [also: {nullified}]
