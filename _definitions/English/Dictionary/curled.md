---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curled
offline_file: ""
offline_thumbnail: ""
uuid: df55fbba-21d0-4660-9539-05231ed243bd
updated: 1484310365
title: curled
categories:
    - Dictionary
---
curled
     adj 1: drawn up into a ball; "she lay curled up in a big armchair"
            [syn: {curled up}]
     2: having curls [syn: {curling}]
