---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jiangxi
offline_file: ""
offline_thumbnail: ""
uuid: 5f99ced1-d2f4-4eff-bbe0-9aea51f59486
updated: 1484308485
title: Jiangxi
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/125px-Jiangxi_%2528Chinese_characters%2529.svg.png'
tags:
    - History
    - geography
    - Administrative divisions
    - Politics
    - Economy
    - Economic and technological development zones
    - Demographics
    - religion
    - culture
    - Transportation
categories:
    - Uncategorized
---
Jiangxi (Chinese: 江西; pinyin:  Jiāngxī; Wade–Giles: Chiang1-hsi1; Gan: Kongsi) is a province in the People's Republic of China, located in the southeast of the country. Spanning from the banks of the Yangtze river in the north into hillier areas in the south and east, it shares a border with Anhui to the north, Zhejiang to the northeast, Fujian to the east, Guangdong to the south, Hunan to the west, and Hubei to the northwest.[3]
