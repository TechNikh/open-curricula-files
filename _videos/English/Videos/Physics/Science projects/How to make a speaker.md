---
version: 1
type: video
provider: YouTube
id: MA1n16iyaeg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fcf12a54-9c69-4110-a178-adf3fea6c215
updated: 1486069634
title: How to make a speaker
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/MA1n16iyaeg/default.jpg
    - https://i3.ytimg.com/vi/MA1n16iyaeg/1.jpg
    - https://i3.ytimg.com/vi/MA1n16iyaeg/2.jpg
    - https://i3.ytimg.com/vi/MA1n16iyaeg/3.jpg
---
