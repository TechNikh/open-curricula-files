---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/non-magnetic
offline_file: ""
offline_thumbnail: ""
uuid: c09814ff-145b-48c2-9e3d-8cc60f038ef8
updated: 1484310409
title: non-magnetic
categories:
    - Dictionary
---
nonmagnetic
     adj : not capable of being magnetized [ant: {magnetic}]
