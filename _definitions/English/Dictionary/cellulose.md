---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cellulose
offline_file: ""
offline_thumbnail: ""
uuid: 413179fc-346a-48cd-a979-92ee8c1f089f
updated: 1484310264
title: cellulose
categories:
    - Dictionary
---
cellulose
     n : a polysaccharide that is the chief constituent of all plant
         tissues and fibers
