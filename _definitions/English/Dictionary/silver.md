---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silver
offline_file: ""
offline_thumbnail: ""
uuid: 92c775e8-8232-4764-9462-d29f1482c7b2
updated: 1484310200
title: silver
categories:
    - Dictionary
---
silver
     adj 1: made from or largely consisting of silver; "silver
            bracelets"
     2: having the white lustrous sheen of silver; "a land of silver
        (or silvern) rivers where the salmon leap"; "repeated
        scrubbings have given the wood a silvery sheen" [syn: {silvern},
         {silvery}]
     3: lustrous gray; covered with or tinged with the color of
        silver; "silvery hair" [syn: {argent}, {silvery}, {silverish}]
     4: expressing yourself readily, clearly, effectively; "able to
        dazzle with his facile tongue"; "silver speech" [syn: {eloquent},
         {facile}, {fluent}, {silver-tongued}, {smooth-spoken}]
     n 1: a soft white precious univalent ...
