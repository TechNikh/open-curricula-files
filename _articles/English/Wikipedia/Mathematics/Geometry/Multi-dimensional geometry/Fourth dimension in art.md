---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fourth_dimension_in_art
offline_file: ""
offline_thumbnail: ""
uuid: 3fa52c79-ef0e-439b-8699-f85fb4af0430
updated: 1484309285
title: Fourth dimension in art
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Jouffret.gif
tags:
    - Early influence
    - Dimensionist manifesto
    - Crucifixion (Corpus Hypercubus)
    - Abstract art
    - Other forms of art
    - Sources
categories:
    - Multi-dimensional geometry
---
New possibilities opened up by the concept of four-dimensional space (and difficulties involved in trying to visualize it) helped inspire many modern artists in the first half of the twentieth century. Early Cubists, Surrealists, Futurists, and abstract artists took ideas from higher-dimensional mathematics and used them to radically advance their work.[1]
