---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seared
offline_file: ""
offline_thumbnail: ""
uuid: b1519c57-2a4d-4141-a063-66c226d98d23
updated: 1484310172
title: seared
categories:
    - Dictionary
---
seared
     adj : having the surface burned quickly with intense heat; "the
           seared meat is then covered with hot liquid for
           braising"
