---
version: 1
type: video
provider: YouTube
id: IEi6KpEIFlk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 40e3f9d2-bc53-4d4d-b438-a0c93f2d347c
updated: 1486069637
title: 'Water Refraction Experiment #1 Brent A Saulic'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/IEi6KpEIFlk/default.jpg
    - https://i3.ytimg.com/vi/IEi6KpEIFlk/1.jpg
    - https://i3.ytimg.com/vi/IEi6KpEIFlk/2.jpg
    - https://i3.ytimg.com/vi/IEi6KpEIFlk/3.jpg
---
