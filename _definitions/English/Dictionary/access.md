---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/access
offline_file: ""
offline_thumbnail: ""
uuid: bafada4a-a480-403b-aab9-a3c061698bf2
updated: 1484310457
title: access
categories:
    - Dictionary
---
access
     n 1: the right to enter [syn: {entree}, {accession}, {admittance}]
     2: the right to obtain or make use of or take advantage of
        something (as services or membership)
     3: a way of entering or leaving; "he took a wrong turn on the
        access to the bridge" [syn: {approach}]
     4: (computer science) the operation of reading or writing
        stored information [syn: {memory access}]
     5: the act of approaching or entering; "he gained access to the
        building"
     v 1: obtain or retrieve from a storage device; as of information
          on a computer
     2: reach or gain access to; "How does one access the attic in
        this house?"; "I cannot ...
