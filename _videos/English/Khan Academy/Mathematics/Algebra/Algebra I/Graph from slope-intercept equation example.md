---
version: 1
type: video
provider: YouTube
id: uk7gS3cZVp4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Graph%20from%20slope-intercept%20equation%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7517a950-4244-4538-91a2-51e5e37ad0a4
updated: 1486069565
title: Graph from slope-intercept equation example
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/uk7gS3cZVp4/default.jpg
    - https://i3.ytimg.com/vi/uk7gS3cZVp4/1.jpg
    - https://i3.ytimg.com/vi/uk7gS3cZVp4/2.jpg
    - https://i3.ytimg.com/vi/uk7gS3cZVp4/3.jpg
---
