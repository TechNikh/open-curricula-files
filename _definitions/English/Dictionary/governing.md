---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/governing
offline_file: ""
offline_thumbnail: ""
uuid: 7a79d8b6-67e1-4196-93df-54f1aac4e4e8
updated: 1484310480
title: governing
categories:
    - Dictionary
---
governing
     adj : responsible for making and enforcing rules and laws;
           "governing bodies"
     n : the act of governing; exercising authority; "regulations for
         the governing of state prisons"; "he had considerable
         experience of government" [syn: {government}, {governance},
          {government activity}]
