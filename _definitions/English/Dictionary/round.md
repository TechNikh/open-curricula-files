---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/round
offline_file: ""
offline_thumbnail: ""
uuid: 1f00f123-0e77-4132-bd0d-d0e65594a366
updated: 1484310307
title: round
categories:
    - Dictionary
---
round
     adj 1: having a circular shape [syn: {circular}] [ant: {square}]
     2: (of sounds) full and rich; "orotund tones"; "the rotund and
        reverberating phrase"; "pear-shaped vowels" [syn: {orotund},
         {rotund}, {pear-shaped}]
     3: (of numbers) to the nearest ten, hundred, or thousand; "in
        round numbers"
     n 1: a charge of ammunition for a single shot [syn: {unit of
          ammunition}, {one shot}]
     2: an interval during which a recurring sequence of events
        occurs; "the neverending cycle of the seasons" [syn: {cycle},
         {rhythm}]
     3: a regular route for a sentry or policeman; "in the old days
        a policeman walked a beat and ...
