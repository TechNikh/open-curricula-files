---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remedial
offline_file: ""
offline_thumbnail: ""
uuid: b4584322-d918-4833-b53e-e19b3fcb6d65
updated: 1484310597
title: remedial
categories:
    - Dictionary
---
remedial
     adj 1: tending or intended to rectify or improve; "a remedial
            reading course"; "remedial education"
     2: tending to cure or restore to health; "curative powers of
        herbal remedies"; "her gentle healing hand"; "remedial
        surgery"; "a sanative environment of mountains and fresh
        air"; "a therapeutic agent"; "therapeutic diets" [syn: {curative},
         {healing(p)}, {alterative}, {sanative}, {therapeutic}]
