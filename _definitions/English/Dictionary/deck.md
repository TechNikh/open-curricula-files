---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deck
offline_file: ""
offline_thumbnail: ""
uuid: 665cf909-d028-4077-b412-94c3eb3bbbde
updated: 1484310156
title: deck
categories:
    - Dictionary
---
deck
     n 1: any of various floor-like platforms built into a vessel
     2: street name for a packet of illegal drugs
     3: a pack of 52 playing cards [syn: {pack of cards}, {deck of
        cards}]
     4: a porch that resembles the deck on a ship
     v 1: be beautiful to look at; "Flowers adorned the tables
          everywhere" [syn: {adorn}, {decorate}, {grace}, {embellish},
           {beautify}]
     2: decorate; "deck the halls with holly" [syn: {bedight}, {bedeck}]
     3: knock down with force; "He decked his opponent" [syn: {coldcock},
         {dump}, {knock down}, {floor}]
