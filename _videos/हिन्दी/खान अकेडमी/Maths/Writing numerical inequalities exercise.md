---
version: 1
type: video
provider: YouTube
id: 0eUalrTi07o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 691b868b-08ae-4b80-9c09-4a674a84f9c9
updated: 1486069616
title: Writing numerical inequalities exercise
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/0eUalrTi07o/default.jpg
    - https://i3.ytimg.com/vi/0eUalrTi07o/1.jpg
    - https://i3.ytimg.com/vi/0eUalrTi07o/2.jpg
    - https://i3.ytimg.com/vi/0eUalrTi07o/3.jpg
---
