---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assert
offline_file: ""
offline_thumbnail: ""
uuid: 559bf641-9d4a-4c7d-989c-137243ce21c9
updated: 1484310189
title: assert
categories:
    - Dictionary
---
assert
     v 1: state categorically [syn: {asseverate}, {maintain}]
     2: to declare or affirm solemnly and formally as true; "Before
        God I swear I am innocent" [syn: {affirm}, {verify}, {avow},
         {aver}, {swan}, {swear}]
     3: insist on having one's opinions and rights recognized;
        "Women should assert themselves more!" [syn: {put forward}]
     4: assert to be true; "The letter asserts a free society" [syn:
         {insist}]
