---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carnivore
offline_file: ""
offline_thumbnail: ""
uuid: 96ed12c7-22cc-4aaf-a915-e2ea11d38eed
updated: 1484310271
title: carnivore
categories:
    - Dictionary
---
carnivore
     n 1: terrestrial or aquatic flesh-eating mammal; terrestrial
          carnivores have four or five clawed digits on each limb
     2: any animal that feeds on flesh; "Tyrannosaurus Rex was a
        large carnivore"; "insectivorous plants are considered
        carnivores"
