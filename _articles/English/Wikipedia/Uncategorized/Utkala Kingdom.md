---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Utkal
offline_file: ""
offline_thumbnail: ""
uuid: f1b4e936-a4b6-4b73-9b6e-d3b02b8e19e0
updated: 1484308425
title: Utkala Kingdom
tags:
    - Early Sanskrit Literature
    - References in Mahabharata
categories:
    - Uncategorized
---
Utkala Kingdom (Odia: ଉତ୍କଳ) was located in the northern and eastern portion of the modern-day Indian state of Odisha. This kingdom was mentioned in the epic Mahabharata, with the names Utkala, Utpala, Okkal and Odra desha. It is mentioned in India's national anthem, Jana Gana Mana.
