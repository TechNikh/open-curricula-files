---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roots
offline_file: ""
offline_thumbnail: ""
uuid: 79807dde-8256-4bd3-9614-5dfaefad2c30
updated: 1484310277
title: roots
categories:
    - Dictionary
---
roots
     n : the condition of belonging to a particular place or group by
         virtue of social or ethnic or cultural lineage; "his
         roots in Texas go back a long way"; "he went back to
         Sweden to search for his roots"; "his music has African
         roots"
