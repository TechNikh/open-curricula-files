---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paddy
offline_file: ""
offline_thumbnail: ""
uuid: 6e7d3a10-1837-47c4-a971-af2041c0136a
updated: 1484310260
title: paddy
categories:
    - Dictionary
---
Paddy
     n 1: (slur) a person of Irish descent [syn: {Mick}, {Mickey}]
     2: an irrigated or flooded field where rice is grown [syn: {paddy
        field}, {rice paddy}]
     3: rice in the husk either gathered or still in the field
