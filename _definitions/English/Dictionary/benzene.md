---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/benzene
offline_file: ""
offline_thumbnail: ""
uuid: 4ccd330a-d269-4351-ba08-bcbb8934ff19
updated: 1484310212
title: benzene
categories:
    - Dictionary
---
benzene
     n : a colorless liquid hydrocarbon; highly inflammable;
         carcinogenic; the simplest of the aromatic compounds
         [syn: {benzine}, {benzol}]
