---
version: 1
type: video
provider: YouTube
id: f0pMPjnfars
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d0a6f176-af2c-4970-9189-b131b086d187
updated: 1486069617
title: 'Minima  maxima and critical points'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/f0pMPjnfars/default.jpg
    - https://i3.ytimg.com/vi/f0pMPjnfars/1.jpg
    - https://i3.ytimg.com/vi/f0pMPjnfars/2.jpg
    - https://i3.ytimg.com/vi/f0pMPjnfars/3.jpg
---
