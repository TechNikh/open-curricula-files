---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yearly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484332021
title: yearly
categories:
    - Dictionary
---
yearly
     adj : occurring or payable every year; "an annual trip to Paris";
           "yearly medical examinations"; "annual (or yearly)
           income" [syn: {annual}]
     adv : without missing a year; "they travel to China annually"
           [syn: {annually}, {every year}, {each year}]
