---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inverse_image_functor
offline_file: ""
offline_thumbnail: ""
uuid: 02e9bf08-0e5d-46bb-8f46-dbe3c7391718
updated: 1484309156
title: Inverse image functor
categories:
    - Algebraic geometry
---
In mathematics, the inverse image functor is a covariant construction of sheaves. The direct image functor is the primary operation on sheaves, with the simplest definition. The inverse image exhibits some relatively subtle features.
