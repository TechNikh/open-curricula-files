---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amazement
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484452562
title: amazement
categories:
    - Dictionary
---
amazement
     n : the feeling that accompanies something extremely surprising;
         "he looked at me in astonishment" [syn: {astonishment}]
