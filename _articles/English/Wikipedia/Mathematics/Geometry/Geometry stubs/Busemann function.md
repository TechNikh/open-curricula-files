---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Busemann_function
offline_file: ""
offline_thumbnail: ""
uuid: c84d8660-1e7b-4724-b571-fb0a92a710a0
updated: 1484309357
title: Busemann function
categories:
    - Geometry stubs
---
Busemann functions were introduced by Busemann to study the large-scale geometry of metric spaces in his seminal The Geometry of Geodesics.[1] More recently, Busemann functions have been used by probabilists to study asymptotic properties in models of first-passage percolation[2][3] and directed last-passage percolation.[4]
