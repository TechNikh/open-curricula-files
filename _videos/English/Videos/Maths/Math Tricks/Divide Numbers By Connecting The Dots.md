---
version: 1
type: video
provider: YouTube
id: g0WEwGzyKwI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a35f10aa-faca-4975-8a05-d38a383d9b89
updated: 1486069623
title: Divide Numbers By Connecting The Dots
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/g0WEwGzyKwI/default.jpg
    - https://i3.ytimg.com/vi/g0WEwGzyKwI/1.jpg
    - https://i3.ytimg.com/vi/g0WEwGzyKwI/2.jpg
    - https://i3.ytimg.com/vi/g0WEwGzyKwI/3.jpg
---
