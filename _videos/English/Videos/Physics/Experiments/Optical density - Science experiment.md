---
version: 1
type: video
provider: YouTube
id: B4kOPWrRnJc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 60f34608-5d76-4a08-91a2-5dfc0677d5f1
updated: 1486069640
title: 'Optical density - Science experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/B4kOPWrRnJc/default.jpg
    - https://i3.ytimg.com/vi/B4kOPWrRnJc/1.jpg
    - https://i3.ytimg.com/vi/B4kOPWrRnJc/2.jpg
    - https://i3.ytimg.com/vi/B4kOPWrRnJc/3.jpg
---
