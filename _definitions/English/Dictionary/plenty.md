---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plenty
offline_file: ""
offline_thumbnail: ""
uuid: 9bed107e-cf7b-4860-afca-ba77834f8cca
updated: 1484310295
title: plenty
categories:
    - Dictionary
---
plenty
     n 1: a full supply; "there was plenty of food for everyone" [syn:
           {plentifulness}, {plenteousness}, {plenitude}, {plentitude}]
     2: (often followed by `of') a large number or amount or extent;
        "a batch of letters"; "a deal of trouble"; "a lot of
        money"; "he made a mint on the stock market"; "it must
        have cost plenty" [syn: {batch}, {deal}, {flock}, {good
        deal}, {great deal}, {hatful}, {heap}, {lot}, {mass}, {mess},
         {mickle}, {mint}, {muckle}, {peck}, {pile}, {pot}, {quite
        a little}, {raft}, {sight}, {slew}, {spate}, {stack}, {tidy
        sum}, {wad}, {whole lot}, {whole slew}]
     adv : as much as necessary; "Have ...
