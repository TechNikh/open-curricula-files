---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surgeon
offline_file: ""
offline_thumbnail: ""
uuid: ed3b411e-c5be-4a9b-9479-86b3bc0997ed
updated: 1484310316
title: surgeon
categories:
    - Dictionary
---
surgeon
     n : a physician who specializes in surgery [syn: {operating
         surgeon}, {sawbones}]
