---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rally
offline_file: ""
offline_thumbnail: ""
uuid: 96e0ccd8-6380-4c47-a065-65df59189bd9
updated: 1484310551
title: rally
categories:
    - Dictionary
---
rally
     n 1: a large gathering of people intended to arouse enthusiasm
          [syn: {mass meeting}]
     2: the feat of mustering strength for a renewed effort; "he
        singled to start a rally in the 9th inning"; "he feared
        the rallying of their troops for a counterattack" [syn: {rallying}]
     3: a marked recovery of strength or spirits during an illness
     4: an automobile race run over public roads
     5: (sports) an unbroken sequence of several successive strokes;
        "after a short rally Connors won the point" [syn: {exchange}]
     v 1: gather; "drum up support" [syn: {beat up}, {drum up}]
     2: call to arms; of military personnel [syn: {call up}, ...
