---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impertinence
offline_file: ""
offline_thumbnail: ""
uuid: 05fab443-ff6d-4e55-9075-93d69ff6e578
updated: 1484310583
title: impertinence
categories:
    - Dictionary
---
impertinence
     n 1: an impudent statement [syn: {impudence}, {cheek}]
     2: the trait of being rude and impertinent; inclined to take
        liberties [syn: {crust}, {gall}, {impudence}, {insolence},
         {cheekiness}, {freshness}]
     3: inappropriate playfulness [syn: {perkiness}, {pertness}, {sauciness},
         {archness}]
