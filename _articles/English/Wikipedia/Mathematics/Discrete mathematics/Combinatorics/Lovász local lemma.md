---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lov%C3%A1sz_local_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 38e0e305-e78b-4e68-ae79-b6cb36d2fa46
updated: 1484308583
title: Lovász local lemma
tags:
    - Statements of the lemma (symmetric version)
    - Asymmetric Lovász local lemma
    - Constructive versus non-constructive
    - Non-constructive proof
    - Example
    - Notes
categories:
    - Combinatorics
---
In probability theory, if a large number of events are all independent of one another and each has probability less than 1, then there is a positive (possibly small) probability that none of the events will occur. The Lovász local lemma allows one to relax the independence condition slightly: As long as the events are "mostly" independent from one another and aren't individually too likely, then there will still be a positive probability that none of them occurs. It is most commonly used in the probabilistic method, in particular to give existence proofs.
