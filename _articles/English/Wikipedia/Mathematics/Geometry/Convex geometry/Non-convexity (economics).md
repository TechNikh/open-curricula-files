---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-convexity_(economics)
offline_file: ""
offline_thumbnail: ""
uuid: 14311f05-9bed-42bb-8e60-f19a4c537a35
updated: 1484309197
title: Non-convexity (economics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-NonConvex.gif
tags:
    - Demand with many consumers
    - Supply with few producers
    - Contemporary economics
    - Optimization over time
    - Nonsmooth analysis
    - Notes
categories:
    - Convex geometry
---
In economics, non-convexity refers to violations of the convexity assumptions of elementary economics. Basic economics textbooks concentrate on consumers with convex preferences (that do not prefer extremes to in-between values) and convex budget sets and on producers with convex production sets; for convex models, the predicted economic behavior is well understood.[1][2] When convexity assumptions are violated, then many of the good properties of competitive markets need not hold: Thus, non-convexity is associated with market failures,[3][4] where supply and demand differ or where market equilibria can be inefficient.[1][4][5][6][7][8] Non-convex economies are studied with nonsmooth ...
