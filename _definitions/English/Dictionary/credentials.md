---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/credentials
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484554441
title: credentials
categories:
    - Dictionary
---
credentials
     n : a document attesting to the truth of certain stated facts
         [syn: {certificate}, {certification}, {credential}]
