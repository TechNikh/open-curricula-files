---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tract
offline_file: ""
offline_thumbnail: ""
uuid: a23f88a3-d27d-4d7a-9d94-90fcc7cf49d8
updated: 1484310323
title: tract
categories:
    - Dictionary
---
tract
     n 1: an extended area of land [syn: {piece of land}, {piece of
          ground}, {parcel of land}, {parcel}]
     2: a system of body parts that together serve some particular
        purpose
     3: a brief treatise on a subject of interest; published in the
        form of a booklet [syn: {pamphlet}]
     4: a bundle of mylenated nerve fibers following a path through
        the brain [syn: {nerve pathway}, {nerve tract}, {pathway}]
