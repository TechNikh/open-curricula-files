---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Niels
offline_file: ""
offline_thumbnail: ""
uuid: 0ae20df9-93bf-42df-b218-8d2f1e3718bf
updated: 1484308307
title: Niels
categories:
    - Uncategorized
---
Niels is a male given name, equivalent to Nicholas, which is common in Denmark, Norway and the Netherlands. The Swedish variant is Nils. The name is a developed short form of Nicholas or Greek Nicolaos after Saint Nicholas. Niels may refer to:
