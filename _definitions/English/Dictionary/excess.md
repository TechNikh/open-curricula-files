---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/excess
offline_file: ""
offline_thumbnail: ""
uuid: bc31ccc7-6ec4-4700-8b47-14b508a87c47
updated: 1484310321
title: excess
categories:
    - Dictionary
---
excess
     adj : more than is needed, desired, or required; "trying to lose
           excess weight"; "found some extra change lying on the
           dresser"; "yet another book on heraldry might be
           thought redundant"; "skills made redundant by
           technological advance"; "sleeping in the spare room";
           "supernumerary ornamentation"; "it was supererogatory
           of her to gloat"; "delete superfluous (or unnecessary)
           words"; "extra ribs as well as other supernumerary
           internal parts"; "surplus cheese distributed to the
           needy" [syn: {extra}, {redundant}, {spare}, {supererogatory},
            {superfluous}, {supernumerary}, ...
