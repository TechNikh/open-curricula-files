---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Consistency
offline_file: ""
offline_thumbnail: ""
uuid: 7c57212e-60a1-42f2-a4af-a96659f450b7
updated: 1484309492
title: Consistency
tags:
    - Consistency and completeness in arithmetic and set theory
    - First-order logic
    - Notation
    - Definition
    - Basic Results
    - "Henkin's theorem"
    - Sketch of proof
    - Model Theory
    - Footnotes
categories:
    - Proof theory
---
In classical deductive logic, a consistent theory is one that does not contain a contradiction.[1][2] The lack of contradiction can be defined in either semantic or syntactic terms. The semantic definition states that a theory is consistent if and only if it has a model, i.e., there exists an interpretation under which all formulas in the theory are true. This is the sense used in traditional Aristotelian logic, although in contemporary mathematical logic the term satisfiable is used instead. A theory 
  
    
      
        T
      
    
    {\displaystyle T}
  
 is consistent if and only if there is no formula 
  
    
      
        ϕ
      
    
    {\displaystyle \phi }
  
 such that ...
