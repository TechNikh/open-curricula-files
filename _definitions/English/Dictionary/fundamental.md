---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fundamental
offline_file: ""
offline_thumbnail: ""
uuid: 7e109f43-470b-4ce8-ba57-0ecfb55844d9
updated: 1484310387
title: fundamental
categories:
    - Dictionary
---
fundamental
     adj 1: serving as an essential component; "a cardinal rule"; "the
            central cause of the problem"; "an example that was
            fundamental to the argument"; "computers are
            fundamental to modern industrial structure" [syn: {cardinal},
             {central}, {key}, {primal}]
     2: being or involving basic facts or principles; "the
        fundamental laws of the universe"; "a fundamental
        incompatibility between them"; "these rudimentary truths";
        "underlying principles" [syn: {rudimentary}, {underlying}]
     3: far-reaching and thoroughgoing in effect especially on the
        nature of something; "the fundamental revolution in ...
