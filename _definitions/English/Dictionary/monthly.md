---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monthly
offline_file: ""
offline_thumbnail: ""
uuid: f53904fe-1d69-4de3-a40b-1b5b5a2c85c2
updated: 1484310445
title: monthly
categories:
    - Dictionary
---
monthly
     adj : occurring or payable every month; "monthly payments"; "the
           monthly newsletter"
     n : a periodical that is published every month
     adv : without missing a month; "we get paid monthly" [syn: {every
           month}, {each month}]
