---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ante
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484529001
title: ante
categories:
    - Dictionary
---
ante
     n : (poker) the initial contribution that each player makes to
         the pot
     v : place one's stake
