---
title: What is ICAN?
youtube_id: gAkW7TDmPtY
tags:
    - ICAN
    - cesarean
    - Birth
    - homebirth
    - vbac
    - HBAC
    - UBAC
    - CBAC
    - cesarean support
    - recovery
    - caesarean
    - cesarian
    - c-section
    - Awareness
    - activism
    - advocacy
    - doula
    - midwife
    - Childbirth
categories:
    - IUCN
---
