---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-government
offline_file: ""
offline_thumbnail: ""
uuid: 7abbde00-ce56-4afd-b115-140ea33e3c50
updated: 1484310557
title: self-government
categories:
    - Dictionary
---
self-government
     n : government of a country by its own people [syn: {self-determination},
          {self-rule}]
