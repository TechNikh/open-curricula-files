---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Contou-Carr%C3%A8re_symbol'
offline_file: ""
offline_thumbnail: ""
uuid: 17c05135-d1e3-498c-8a2a-55a54ed662c3
updated: 1484308635
title: Contou-Carrère symbol
categories:
    - Number theory
---
In mathematics, the Contou-Carrère symbol 〈a,b〉 is a Steinberg symbol defined on pairs of invertible elements of the ring of Laurent power series over an Artinian ring k, taking values in the group of units of k. It was introduced by Contou-Carrère (1994).
