---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disorder
offline_file: ""
offline_thumbnail: ""
uuid: fe9e5a16-274e-4858-b1d8-327e27be2430
updated: 1484310166
title: disorder
categories:
    - Dictionary
---
disorder
     n 1: condition in which there is a disturbance of normal
          functioning; "the doctor prescribed some medicine for
          the disorder"; "everyone gets stomach upsets from time
          to time" [syn: {upset}]
     2: a condition in which things are not in their expected
        places; "the files are in complete disorder" [syn: {disorderliness}]
        [ant: {orderliness}, {orderliness}]
     3: a disturbance of the peace or of public order [ant: {order}]
     v 1: disturb in mind or make uneasy or cause to be worried or
          alarmed; "She was rather perturbed by the news that her
          father was seriously ill" [syn: {perturb}, {unhinge}, {disquiet},
     ...
