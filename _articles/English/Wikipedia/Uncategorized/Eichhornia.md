---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eichornia
offline_file: ""
offline_thumbnail: ""
uuid: 5fa4066b-2eed-4dd6-b55b-ec38388de582
updated: 1484308340
title: Eichhornia
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dried_Water_Hyacinth_in_Bangladesh.jpg
tags:
    - Description
    - Invasiveness as an exotic plant
    - Uses
categories:
    - Uncategorized
---
Eichhornia, water hyacinth, is a genus of aquatic flowering plants in the family Pontederiaceae. The genus is native to South America. E. crassipes has become widely naturalized in tropical and subtropical regions and is a significant invasive species.
