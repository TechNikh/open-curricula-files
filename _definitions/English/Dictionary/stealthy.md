---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stealthy
offline_file: ""
offline_thumbnail: ""
uuid: bf1d162c-8373-4084-9d3a-8359afe62fc7
updated: 1484310162
title: stealthy
categories:
    - Dictionary
---
stealthy
     adj : marked by quiet and caution and secrecy; taking pains to
           avoid being observed; "a furtive manner"; "a lurking
           prowler"; "a sneak attack"; "stealthy footsteps"; "a
           surreptitious glance at his watch"; "someone skulking
           in the shadows" [syn: {furtive}, {lurking}, {skulking},
            {sneak(a)}, {sneaky}, {surreptitious}]
     [also: {stealthiest}, {stealthier}]
