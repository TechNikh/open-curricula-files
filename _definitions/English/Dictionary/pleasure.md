---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pleasure
offline_file: ""
offline_thumbnail: ""
uuid: d3c03bcd-f39a-4749-be59-2da2f50ea852
updated: 1484310585
title: pleasure
categories:
    - Dictionary
---
pleasure
     n 1: a fundamental feeling that is hard to define but that people
          desire to experience; "he was tingling with pleasure"
          [syn: {pleasance}] [ant: {pain}]
     2: something or someone that provides pleasure; a source of
        happiness; "a joy to behold"; "the pleasure of his
        company"; "the new car is a delight" [syn: {joy}, {delight}]
     3: a formal expression; "he serves at the pleasure of the
        President"
     4: an activity that affords enjoyment; "he puts duty before
        pleasure"
     5: sexual gratification; "he took his pleasure of her"
