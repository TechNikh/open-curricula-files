---
version: 1
type: video
provider: YouTube
id: Hmwvj9X4GNY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Parts%20of%20a%20cell.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 498cdef7-6826-4e80-b8e3-2183f917a388
updated: 1486069581
title: Parts of a cell
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/Hmwvj9X4GNY/default.jpg
    - https://i3.ytimg.com/vi/Hmwvj9X4GNY/1.jpg
    - https://i3.ytimg.com/vi/Hmwvj9X4GNY/2.jpg
    - https://i3.ytimg.com/vi/Hmwvj9X4GNY/3.jpg
---
Parts of a cell: nucleus, ribosomes, endoplasmic reticulum, Golgi bodies, mitochondria, chloroplasts, vacuoles, and vesicles
