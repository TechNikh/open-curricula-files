---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Greedy_embedding
offline_file: ""
offline_thumbnail: ""
uuid: af984bcf-2f22-4da9-9169-fcdb4b3f1231
updated: 1484309241
title: Greedy embedding
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Sextic-monomial-dessin.svg.png
tags:
    - Definitions
    - Graphs with no greedy embedding
    - Hyperbolic and succinct embeddings
    - Special classes of graphs
    - trees
    - Planar graphs
    - Unit disk graphs
categories:
    - Geometric graph theory
---
In distributed computing and geometric graph theory, greedy embedding is a process of assigning coordinates to the nodes of a telecommunications network in order to allow greedy geographic routing to be used to route messages within the network. Although greedy embedding has been proposed for use in wireless sensor networks, in which the nodes already have positions in physical space, these existing positions may differ from the positions given to them by greedy embedding, which may in some cases be points in a virtual space of a higher dimension, or in a non-Euclidean geometry. In this sense, greedy embedding may be viewed as a form of graph drawing, in which an abstract graph (the ...
