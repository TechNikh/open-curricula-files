---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/needs
offline_file: ""
offline_thumbnail: ""
uuid: efa0a4c9-a7ba-4ea6-8d44-c3b74df5ae40
updated: 1484310289
title: needs
categories:
    - Dictionary
---
needs
     adv : in such a manner as could not be otherwise; "it is
           necessarily so"; "we must needs by objective" [syn: {inevitably},
            {necessarily}, {of necessity}]
