---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Glossary_of_classical_algebraic_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 7e4223fa-13d4-458f-b011-70e48143bd0d
updated: 1484309130
title: Glossary of classical algebraic geometry
categories:
    - Algebraic geometry
---
The terminology of algebraic geometry changed drastically during the twentieth century, with the introduction of the general methods, initiated by David Hilbert and the Italian school of algebraic geometry in the beginning of the century, and later formalized by André Weil, Serre and Grothendieck. Much of the classical terminology, mainly based on case study, was simply abandoned, with the result that books and papers written before this time can be hard to read. This article lists some of this classical terminology, and describes some of the changes in conventions.
