---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Motion_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: bc932222-f6fc-4dd5-91fd-75fbef30614b
updated: 1484309230
title: Motion (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Glide_reflection.svg.png
tags:
    - In differential geometry
    - Group of motions
    - History
    - Axioms of motion
    - Notes and references
categories:
    - Differential geometry
---
In geometry, a motion is an isometry of a metric space. For instance, a plane equipped with the Euclidean distance metric is a metric space in which a mapping associating congruent figures is a motion.[1] More generally, the term motion is a synonym for surjective isometry in metric geometry,[2] including elliptic geometry and hyperbolic geometry. In the latter case, hyperbolic motions provide an approach to the subject for beginners.
