---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fallow
offline_file: ""
offline_thumbnail: ""
uuid: 7e726850-7921-4e6a-99a1-14c153f3fe9e
updated: 1484310461
title: fallow
categories:
    - Dictionary
---
fallow
     adj 1: left unplowed and unseeded during a growing season; "fallow
            farmland"
     2: undeveloped but potentially useful; "a fallow gold market"
     n : cultivated land that is not seeded for one or more growing
         seasons
