---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/zenith
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484559181
title: zenith
categories:
    - Dictionary
---
zenith
     n : the point above the observer that is directly opposite the
         nadir on the imaginary sphere against which celestial
         bodies appear to be projected [ant: {nadir}]
