---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/idealistic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414761
title: idealistic
categories:
    - Dictionary
---
idealistic
     adj 1: of or relating to the philosophical doctrine of the reality
            of ideas [syn: {ideal}]
     2: of high moral or intellectual value; elevated in nature or
        style; "an exalted ideal"; "argue in terms of high-flown
        ideals"- Oliver Franks; "a noble and lofty concept" [syn:
        {exalted}, {high-flown}, {high-minded}, {lofty}, {rarefied},
         {rarified}, {noble-minded}]
