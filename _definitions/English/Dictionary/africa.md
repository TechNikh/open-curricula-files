---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/africa
offline_file: ""
offline_thumbnail: ""
uuid: a7e56de5-dd8d-4423-91ed-9796001b6e20
updated: 1484310283
title: africa
categories:
    - Dictionary
---
Africa
     n : the second largest continent; located south of Europe and
         bordered to the west by the South Atlantic and to the
         east by the Indian Ocean
