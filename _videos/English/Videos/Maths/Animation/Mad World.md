---
version: 1
type: video
provider: YouTube
id: Ir_i58n0WPA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7b27366e-a232-472b-abd3-afe447fec8ac
updated: 1486069644
title: Mad World
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ir_i58n0WPA/default.jpg
    - https://i3.ytimg.com/vi/Ir_i58n0WPA/1.jpg
    - https://i3.ytimg.com/vi/Ir_i58n0WPA/2.jpg
    - https://i3.ytimg.com/vi/Ir_i58n0WPA/3.jpg
---
