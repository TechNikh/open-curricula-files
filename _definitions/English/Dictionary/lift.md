---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lift
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448181
title: lift
categories:
    - Dictionary
---
lift
     n 1: the act of giving temporary assistance
     2: the component of the aerodynamic forces acting on an airfoil
        that opposes gravity [syn: {aerodynamic lift}]
     3: the event of something being raised upward; "an elevation of
        the temperature in the afternoon"; "a raising of the land
        resulting from volcanic activity" [syn: {elevation}, {raising}]
     4: a wave that lifts the surface of the water or ground [syn: {rise}]
     5: a powered conveyance that carries skiers up a hill [syn: {ski
        tow}, {ski lift}]
     6: a device worn in a shoe or boot to make the wearer look
        taller or to correct a shortened leg
     7: one of the layers forming ...
