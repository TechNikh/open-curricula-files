---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ronald_Gould_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: ac34014a-4062-4404-8a8a-00946cbb4d4c
updated: 1484309466
title: Ronald Gould (mathematician)
categories:
    - Graph theorists
---
Ronald J. Gould (born April 15, 1950) is an American mathematician specializing in combinatorics and graph theory. He is most noted for his work in the area of Hamiltonian graph theory. His book Mathematics in Games, Sports, and Gambling: – The Games People Play (ISBN 978-1439801635) won the American Library Association award for Outstanding Academic Titles, 2010.
