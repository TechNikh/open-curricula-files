---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/patrolling
offline_file: ""
offline_thumbnail: ""
uuid: 3547a6e9-db39-4a94-99a7-d63ee92a0d02
updated: 1484310166
title: patrolling
categories:
    - Dictionary
---
patrol
     n 1: a detachment used for security or reconnaissance
     2: the activity of going around or through an area at regular
        intervals for security purposes
     3: a group that goes through a region at regular intervals for
        the purpose of security
     v : maintain the security of by carrying out a control [syn: {police}]
     [also: {patrolling}, {patrolled}]
