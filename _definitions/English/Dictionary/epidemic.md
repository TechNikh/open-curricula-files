---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/epidemic
offline_file: ""
offline_thumbnail: ""
uuid: 816cf014-ae1e-42f4-81e2-814a1adae663
updated: 1484310473
title: epidemic
categories:
    - Dictionary
---
epidemic
     adj : (especially of medicine) of disease or anything resembling a
           disease; attacking or affecting many individuals in a
           community or a population simultaneously; "an epidemic
           outbreak of influenza" [ant: {endemic}, {ecdemic}]
     n : a widespread outbreak of an infectious disease; many people
         are infected at the same time
