---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Monohybrid
offline_file: ""
offline_thumbnail: ""
uuid: 6de2cef9-d159-4cf4-b720-4b83a2bb96c7
updated: 1484308349
title: Monohybrid cross
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mendelian_inheritance.svg.png
tags:
    - Usage
    - "Mendel's experiment"
    - First cross
    - Second cross
    - Third cross
    - "Mendel's hypothesis"
    - Test of the hypothesis
categories:
    - Uncategorized
---
A monohybrid cross is a mating between two individuals with different alleles at one genetic locus of interest.[1][2] The character(s) being studied in a monohybrid cross are governed by two or multiple alleles for a single locus. A cross between 2 parents possessing a pair of contrasting characters is known as monohybrid cross. To carry out such a cross, each parent is chosen to be homozygous or true breeding for a given trait (locus). When a cross satisfies the conditions for a monohybrid cross, it is usually detected by a characteristic distribution of second-generation (F2) offspring that is sometimes called the monohybrid ratio.
