---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overhaul
offline_file: ""
offline_thumbnail: ""
uuid: 68fdc418-1f15-46c2-93ad-7127c19c709c
updated: 1484310595
title: overhaul
categories:
    - Dictionary
---
overhaul
     n : periodic maintenance on a car or machine; "it was time for
         an overhaul on the tractor" [syn: {inspection and repair},
          {service}]
     v 1: travel past; "The sports car passed all the trucks" [syn: {pass},
           {overtake}]
     2: make repairs or adjustments to; "You should overhaul your
        car engine" [syn: {modernize}, {modernise}]
