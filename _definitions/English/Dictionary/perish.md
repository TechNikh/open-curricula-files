---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perish
offline_file: ""
offline_thumbnail: ""
uuid: fa896a81-ab4a-45e7-ae1b-4aef40c22ca8
updated: 1484310289
title: perish
categories:
    - Dictionary
---
perish
     v : pass from physical life and lose all all bodily attributes
         and functions necessary to sustain life; "She died from
         cancer"; "They children perished in the fire"; "The
         patient went peacefully" [syn: {die}, {decease}, {go}, {exit},
          {pass away}, {expire}, {pass}] [ant: {be born}]
