---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commit
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484493361
title: commit
categories:
    - Dictionary
---
commit
     v 1: perform an act, usually with a negative connotation;
          "perpetrate a crime"; "pull a bank robbery" [syn: {perpetrate},
           {pull}]
     2: give entirely to a specific person, activity, or cause; "She
        committed herself to the work of God"; "give one's talents
        to a good cause"; "consecrate your life to the church"
        [syn: {give}, {dedicate}, {consecrate}, {devote}]
     3: cause to be admitted; of persons to an institution; "After
        the second episode, she had to be committed"; "he was
        committed to prison" [syn: {institutionalize}, {institutionalise},
         {send}, {charge}]
     4: confer a trust upon; "The messenger was ...
