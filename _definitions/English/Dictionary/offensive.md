---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/offensive
offline_file: ""
offline_thumbnail: ""
uuid: ba77ae40-fbae-4569-b972-adf86e1ba930
updated: 1484310575
title: offensive
categories:
    - Dictionary
---
offensive
     adj 1: violating or tending to violate or offend against;
            "violative of the principles of liberty"; "considered
            such depravity offensive against all laws of humanity"
            [syn: {violative}]
     2: for the purpose of attack rather than defense; "offensive
        weapons" [ant: {defensive}]
     3: causing anger or annoyance; "offensive remarks" [ant: {inoffensive}]
     4: morally offensive; "an unsavory reputation"; "an unsavory
        scandal" [syn: {unsavory}, {unsavoury}] [ant: {savory}]
     5: unpleasant or disgusting especially to the senses;
        "offensive odors" [ant: {inoffensive}]
     6: of an offensive substitute for ...
