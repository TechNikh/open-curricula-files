---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entrenched
offline_file: ""
offline_thumbnail: ""
uuid: 505d6ab4-7bf5-46d6-b4ad-f5a6e318cb3f
updated: 1484310573
title: entrenched
categories:
    - Dictionary
---
entrenched
     adj 1: dug in
     2: established firmly and securely; "the entrenched power of
        the nobility"
