---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/factorisation
offline_file: ""
offline_thumbnail: ""
uuid: f83c6620-8f16-40d2-a02b-2ed6b10c8ca2
updated: 1484310142
title: factorisation
categories:
    - Dictionary
---
factorisation
     n : (mathematics) the resolution of an integer or polynomial
         into factors such that when multiplied together they give
         the integer or polynomial [syn: {factorization}, {factoring}]
