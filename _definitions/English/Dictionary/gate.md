---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gate
offline_file: ""
offline_thumbnail: ""
uuid: b8f9d773-0d1c-4606-80ee-5c423e0d05ff
updated: 1484310451
title: gate
categories:
    - Dictionary
---
gate
     n 1: a door-like movable barrier in a fence or wall
     2: a computer circuit with several inputs but only one output
        that can be activated by particular combinations of inputs
        [syn: {logic gate}]
     3: total admission receipts at a sports event
     4: passageway (as in an air terminal) where passengers can
        embark or disembark
     v 1: supply with a gate; "The house was gated"
     2: control with a valve or other device that functions like a
        gate
     3: restrict (school boys') movement to the dormitory or campus
        as a means of punishment
