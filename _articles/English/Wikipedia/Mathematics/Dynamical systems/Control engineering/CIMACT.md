---
version: 1
type: article
id: https://en.wikipedia.org/wiki/CIMACT
offline_file: ""
offline_thumbnail: ""
uuid: 400f5acb-33f8-4d40-ad8f-313ab1a0e5d6
updated: 1484308727
title: CIMACT
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-CIMACT_AF_APP2_2011-04-11.jpg
tags:
    - Definition
    - History
    - Functionality
    - ATC-related information
    - Flight way related information
    - CIMACT supported ASTERIX ATC Standard
    - Benefit to Air Traffic Management (ATM)
    - Compliancy with European Commission’s Regulations
    - Utilisation
    - CIMACT utilisation in NATO CRC
    - CIMACT utilisation in Fighter Wing
    - Costs
    - Responsibility
    - Responsibility of the nations
    - EUROCONTROL points of contact
    - Service
    - Software and hardware
    - Summary
categories:
    - Control engineering
---
