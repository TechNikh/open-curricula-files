---
version: 1
type: video
provider: YouTube
id: qP_rkdQitPw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6d13b052-0308-4c1b-bb02-446da7355b2d
updated: 1486069649
title: 'Nature by Numbers & Fractals'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/qP_rkdQitPw/default.jpg
    - https://i3.ytimg.com/vi/qP_rkdQitPw/1.jpg
    - https://i3.ytimg.com/vi/qP_rkdQitPw/2.jpg
    - https://i3.ytimg.com/vi/qP_rkdQitPw/3.jpg
---
