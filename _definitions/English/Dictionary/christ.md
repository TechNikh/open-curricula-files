---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/christ
offline_file: ""
offline_thumbnail: ""
uuid: b4f8e048-36e7-4cb9-820f-4047a059b9f2
updated: 1484310178
title: christ
categories:
    - Dictionary
---
Christ
     n 1: a teacher and prophet born in Bethlehem and active in
          Nazareth; his life and sermons form the basis for
          Christianity (circa 4 BC - AD 29) [syn: {Jesus}, {Jesus
          of Nazareth}, {the Nazarene}, {Jesus Christ}, {Savior},
          {Saviour}, {Good Shepherd}, {Redeemer}, {Deliverer}]
     2: any expected deliverer [syn: {messiah}]
