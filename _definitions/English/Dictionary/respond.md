---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/respond
offline_file: ""
offline_thumbnail: ""
uuid: 3c3b9293-854a-45a9-b692-95cdc99a23c5
updated: 1484310531
title: respond
categories:
    - Dictionary
---
respond
     v 1: show a response or a reaction to something [syn: {react}]
     2: reply or respond to; "She didn't want to answer"; "answer
        the question"; "We answered that we would accept the
        invitation" [syn: {answer}, {reply}]
     3: respond favorably or as hoped; "The cancer responded to the
        aggressive therapy"
