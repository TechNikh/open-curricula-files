---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ruling
offline_file: ""
offline_thumbnail: ""
uuid: c6b47398-0b40-46ca-a2dd-5fc7a3b96163
updated: 1484310553
title: ruling
categories:
    - Dictionary
---
ruling
     adj : exercising power or authority [syn: {regnant}, {reigning}]
     n : the reason for a court's judgment (as opposed to the
         decision itself) [syn: {opinion}]
