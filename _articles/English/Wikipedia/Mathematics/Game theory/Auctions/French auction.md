---
version: 1
type: article
id: https://en.wikipedia.org/wiki/French_auction
offline_file: ""
offline_thumbnail: ""
uuid: 6e8c1f34-1e3f-44df-952b-6eddba4b7f35
updated: 1484309011
title: French auction
categories:
    - Auctions
---
In this offering, the firm announces a minimum (reserve) price. Investors place sealed bids for quantity and price. When the bids are in, the firm negotiates a minimum and maximum price with the market regulator (the Société des Bourses Françaises, or SBF).
