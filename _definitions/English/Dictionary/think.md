---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/think
offline_file: ""
offline_thumbnail: ""
uuid: bad2611c-6283-467e-805c-a50336a56fbb
updated: 1484310355
title: think
categories:
    - Dictionary
---
think
     n : an instance of deliberate thinking; "I need to give it a
         good think"
     v 1: judge or regard; look upon; judge; "I think he is very
          smart"; "I believe her to be very smart"; "I think that
          he is her boyfriend"; "The racist conceives such people
          to be inferior" [syn: {believe}, {consider}, {conceive}]
     2: expect, believe, or suppose; "I imagine she earned a lot of
        money with her new novel"; "I thought to find her in a bad
        state"; "he didn't think to find her in the kitchen"; "I
        guess she is angry at me for standing her up" [syn: {opine},
         {suppose}, {imagine}, {reckon}, {guess}]
     3: use or exercise ...
