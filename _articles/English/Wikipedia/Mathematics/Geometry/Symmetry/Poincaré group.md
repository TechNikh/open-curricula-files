---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poincar%C3%A9_group'
offline_file: ""
offline_thumbnail: ""
uuid: f7b6b411-543b-412a-9b5a-ef4953941753
updated: 1484309313
title: Poincaré group
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Henri_Poincare.jpg
tags:
    - Overview
    - Poincaré symmetry
    - Poincaré group
    - Poincaré algebra
    - Super-Poincaré algebra
    - Notes
categories:
    - Symmetry
---
The Poincaré group, named after Henri Poincaré (1906),[1] was first defined by Minkowski (1908) being the group of Minkowski spacetime isometries.[2][3] It is a ten-generator non-abelian Lie group of fundamental importance in physics.
