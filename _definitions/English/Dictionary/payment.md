---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/payment
offline_file: ""
offline_thumbnail: ""
uuid: 77a7c5f7-1880-4150-a272-b67e597ed015
updated: 1484310455
title: payment
categories:
    - Dictionary
---
payment
     n 1: a sum of money paid [ant: {nonpayment}]
     2: the act of paying money [syn: {defrayal}, {defrayment}]
        [ant: {evasion}]
