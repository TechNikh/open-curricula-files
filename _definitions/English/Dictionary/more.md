---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/more
offline_file: ""
offline_thumbnail: ""
uuid: 74e817f4-bcd5-4c96-8dd2-eff63ac453e4
updated: 1484310349
title: more
categories:
    - Dictionary
---
more
     adj 1: (comparative of `much' used with mass nouns) a quantifier
            meaning greater in size or amount or extent or degree;
            "more land"; "more support"; "more rain fell"; "more
            than a gallon" [syn: {more(a)}, {more than}] [ant: {less(a)}]
     2: (comparative of `many' used with count nouns) quantifier
        meaning greater in number; "a hall with more seats"; "we
        have no more bananas"; "more than one" [syn: {more(a)}]
        [ant: {fewer}]
     3: existing or coming by way of addition; "an additional
        problem"; "further information"; "there will be further
        delays"; "took more time" [syn: {additional}, {further(a)},
        ...
