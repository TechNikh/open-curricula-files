---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/generalize
offline_file: ""
offline_thumbnail: ""
uuid: 90208c22-e30f-4c80-ae3b-02b181049c31
updated: 1484310214
title: generalize
categories:
    - Dictionary
---
generalize
     v 1: draw from specific cases for more general cases [syn: {generalise},
           {extrapolate}, {infer}]
     2: speak or write in generalities [syn: {generalise}] [ant: {specify}]
     3: cater to popular taste to make popular and present to the
        general public; bring into general or common use; "They
        popularized coffee in Washington State"; "Relativity
        Theory was vulgarized by these authors" [syn: {popularize},
         {popularise}, {vulgarize}, {vulgarise}, {generalise}]
     4: become systemic and spread throughout the body; "this kind
        of infection generalizes throughout the immune system"
        [syn: {generalise}]
