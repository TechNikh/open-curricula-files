---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Auxiliary_line
offline_file: ""
offline_thumbnail: ""
uuid: a3bda369-c99e-4e33-b468-b2baafcec355
updated: 1484309354
title: Auxiliary line
categories:
    - Geometry stubs
---
An auxiliary line (or helping line) is an extra line needed to complete a proof in plane geometry.[1] Other common auxiliary constructs in elementary plane synthetic geometry are the helping circles.
