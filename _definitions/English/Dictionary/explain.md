---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/explain
offline_file: ""
offline_thumbnail: ""
uuid: 9235c9dd-54d6-4f63-b4e4-fe895ee29077
updated: 1484310321
title: explain
categories:
    - Dictionary
---
explain
     v 1: make palin and comprehensible; "He explained the laws of
          physics to his students" [syn: {explicate}]
     2: define; "The committe explained their plan for fund-raising
        to the Dean"
     3: serve as a reason or cause or justification of; "Your need
        to sleep late does not excuse your late arrival at work";
        "Her recent divorce amy explain her reluctance to date
        again" [syn: {excuse}]
