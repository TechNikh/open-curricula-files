---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/raising
offline_file: ""
offline_thumbnail: ""
uuid: 9f12b9f5-bdb5-4a19-af41-a8e06d658d78
updated: 1484310471
title: raising
categories:
    - Dictionary
---
raising
     adj : increasing in quantity or value; "a cost-raising increase in
           the basic wage rate"
     n 1: the event of something being raised upward; "an elevation of
          the temperature in the afternoon"; "a raising of the
          land resulting from volcanic activity" [syn: {elevation},
           {lift}]
     2: the properties acquired as a consequence of the way you were
        treated as a child [syn: {rearing}, {nurture}]
     3: raising someone to be an accepted member of the community;
        "they debated whether nature or nurture was more
        important" [syn: {breeding}, {bringing up}, {fostering}, {fosterage},
         {nurture}, {rearing}, ...
