---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weaving
offline_file: ""
offline_thumbnail: ""
uuid: 9d664868-a351-4be0-b027-4c29a1fd9ca7
updated: 1484310146
title: weaving
categories:
    - Dictionary
---
weaving
     adj : walking unsteadily; "a stqaggering gait" [syn: {lurching}, {stumbling},
            {staggering}]
     n : creating fabric
