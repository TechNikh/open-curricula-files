---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/friction
offline_file: ""
offline_thumbnail: ""
uuid: 1ee722d1-583a-452c-b7a9-940d6a2bd2b8
updated: 1484310369
title: friction
categories:
    - Dictionary
---
friction
     n 1: a state of conflict between persons [syn: {clash}]
     2: the resistance encountered when one body is moved in contact
        with another [syn: {rubbing}]
     3: effort expended in rubbing one object against another [syn:
        {detrition}, {rubbing}]
