---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/timidly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484452021
title: timidly
categories:
    - Dictionary
---
timidly
     adv : in a shy or timid or bashful manner; "he smiled shyly" [syn:
            {shyly}, {bashfully}]
