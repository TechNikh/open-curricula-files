---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spontaneous_process
offline_file: ""
offline_thumbnail: ""
uuid: 53a4320a-3ce2-4cd1-b856-6ca24f2ae433
updated: 1484308866
title: Spontaneous process
tags:
    - Overview
    - Using free energy to determine spontaneity
    - Using entropy to determine spontaneity
categories:
    - Thermodynamics
---
A spontaneous process is the time-evolution of a system in which it releases free energy and moves to a lower, more thermodynamically stable energy state.[1][2] The sign convention of changes in free energy follows the general convention for thermodynamic measurements, in which a release of free energy from the system corresponds to a negative change in free energy, but a positive change for the surroundings.
