---
version: 1
type: article
id: https://en.wikipedia.org/wiki/T-symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 5ba3807d-1a3b-4a4d-a575-194ee9213c4b
updated: 1484308868
title: T-symmetry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Teeter-totter.png
tags:
    - Invariance
    - 'Macroscopic phenomena: the second law of thermodynamics'
    - 'Macroscopic phenomena: black holes'
    - 'Kinetic consequences: detailed balance and Onsager reciprocal relations'
    - >
        Effect of time reversal on some variables of classical
        physics
    - Even
    - Odd
    - 'Microscopic phenomena: time reversal invariance'
    - Time reversal in quantum mechanics
    - Anti-unitary representation of time reversal
    - Electric dipole moments
    - "Kramers' theorem"
    - Time reversal of the known dynamical laws
    - Time reversal of noninvasive measurements
categories:
    - Thermodynamics
---
Although in restricted contexts one may find this symmetry, the observable universe itself does not show symmetry under time reversal, primarily due to the second law of thermodynamics. Hence time is said to be non-symmetric, or asymmetric, except for equilibrium states when the second law of thermodynamics predicts the time symmetry to hold. However, quantum noninvasive measurements are predicted to violate time symmetry even in equilibrium,[1] contrary to their classical counterparts, although it has not yet been experimentally confirmed.
