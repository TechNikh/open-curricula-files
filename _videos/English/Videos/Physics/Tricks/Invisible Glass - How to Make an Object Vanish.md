---
version: 1
type: video
provider: YouTube
id: KyWgnFm3ebc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a840c3f0-e7f8-459d-ba2a-c70b8c7d05ca
updated: 1486069660
title: 'Invisible Glass - How to Make an Object Vanish'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/KyWgnFm3ebc/default.jpg
    - https://i3.ytimg.com/vi/KyWgnFm3ebc/1.jpg
    - https://i3.ytimg.com/vi/KyWgnFm3ebc/2.jpg
    - https://i3.ytimg.com/vi/KyWgnFm3ebc/3.jpg
---
