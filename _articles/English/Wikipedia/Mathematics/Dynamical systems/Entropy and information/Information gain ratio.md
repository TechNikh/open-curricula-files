---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Information_gain_ratio
offline_file: ""
offline_thumbnail: ""
uuid: 63454d91-db17-4d74-beb7-27a275cec3a8
updated: 1484308773
title: Information gain ratio
tags:
    - Information gain calculation
    - Intrinsic value calculation
    - Information gain ratio calculation
    - Advantages
categories:
    - Entropy and information
---
In decision tree learning, Information gain ratio is a ratio of information gain to the intrinsic information. It is used to reduce a bias towards multi-valued attributes by taking the number and size of branches into account when choosing an attribute.[1]
