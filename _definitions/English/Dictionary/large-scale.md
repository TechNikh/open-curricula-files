---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/large-scale
offline_file: ""
offline_thumbnail: ""
uuid: 281dbef7-89c6-4f0b-a8a3-69cd4a25675c
updated: 1484310189
title: large-scale
categories:
    - Dictionary
---
large-scale
     adj 1: unusually large in scope; "a large-scale attack on AIDS is
            needed"
     2: constructed or drawn to a big scale; "large-scale maps"
     3: occurring widely (as to many people); "mass destruction"
        [syn: {mass}]
