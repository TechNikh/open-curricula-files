---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tending
offline_file: ""
offline_thumbnail: ""
uuid: 0aaeecc6-5e63-4362-8bbd-ee80ca2cc31b
updated: 1484310451
title: tending
categories:
    - Dictionary
---
tending
     adj : (usually followed by `to') naturally disposed toward; "he is
           apt to ignore matters he considers unimportant"; "I am
           not minded to answer any questions" [syn: {apt(p)}, {disposed(p)},
            {given(p)}, {minded(p)}, {tending(p)}]
     n : the work of caring for or attending to someone or something;
         "no medical care was required"; "the old car needed
         constant attention" [syn: {care}, {attention}, {aid}]
