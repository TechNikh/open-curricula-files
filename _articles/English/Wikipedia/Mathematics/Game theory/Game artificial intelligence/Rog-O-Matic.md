---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rog-O-Matic
offline_file: ""
offline_thumbnail: ""
uuid: 53547732-5451-46b6-8608-2e7d7ed3059a
updated: 1484309006
title: Rog-O-Matic
categories:
    - Game artificial intelligence
---
Rog-O-Matic is a bot developed in 1981 to play and win the computer game Rogue, by four graduate students in the Computer Science Department at Carnegie-Mellon University in Pittsburgh: Andrew Appel, Leonard Hamey, Guy Jacobson and Michael Loren Mauldin.[1]
