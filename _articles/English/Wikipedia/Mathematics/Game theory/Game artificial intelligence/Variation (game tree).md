---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Variation_(game_tree)
offline_file: ""
offline_thumbnail: ""
uuid: 25be0742-05f1-4283-9777-6065227a5d70
updated: 1484309010
title: Variation (game tree)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Principle_variation_16bit.PNG
categories:
    - Game artificial intelligence
---
A Variation can refer to a specific sequence of successive moves in a turn-based game, often used to specify a hypothetical future state of a game that is being played. Although the term is most commonly used in the context of Chess analysis, it has been applied to other games. It also is a useful term used when describing computer tree-search algorithms (for example minimax) for playing games such as Go[1] or Chess.
