---
version: 1
type: video
provider: YouTube
id: Hiq1MIWMlOM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ae457724-530f-4d40-a150-6e4eb279f776
updated: 1486069643
title: 'Gaudium Inside the Box - Mandelbulb 3D fractal HD - Psychedelic Trance'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Hiq1MIWMlOM/default.jpg
    - https://i3.ytimg.com/vi/Hiq1MIWMlOM/1.jpg
    - https://i3.ytimg.com/vi/Hiq1MIWMlOM/2.jpg
    - https://i3.ytimg.com/vi/Hiq1MIWMlOM/3.jpg
---
