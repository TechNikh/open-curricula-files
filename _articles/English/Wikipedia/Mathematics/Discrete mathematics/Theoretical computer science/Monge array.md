---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Monge_array
offline_file: ""
offline_thumbnail: ""
uuid: 2344f780-2680-4aed-afe4-0cfa160c768c
updated: 1484308675
title: Monge array
categories:
    - Theoretical computer science
---
In mathematics applied to computer science, Monge arrays, or Monge matrices, are mathematical objects named for their discoverer, the French mathematician Gaspard Monge.
