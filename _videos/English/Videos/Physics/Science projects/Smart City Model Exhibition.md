---
version: 1
type: video
provider: YouTube
id: 6WdbZ-Nb3wI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4a861b5f-185c-455b-88a5-cde32819e26f
updated: 1486069634
title: Smart City Model Exhibition
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/6WdbZ-Nb3wI/default.jpg
    - https://i3.ytimg.com/vi/6WdbZ-Nb3wI/1.jpg
    - https://i3.ytimg.com/vi/6WdbZ-Nb3wI/2.jpg
    - https://i3.ytimg.com/vi/6WdbZ-Nb3wI/3.jpg
---
