---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/buckminsterfullerene
offline_file: ""
offline_thumbnail: ""
uuid: d38b6b6e-856c-4f37-a177-e69265c49c35
updated: 1484310414
title: buckminsterfullerene
categories:
    - Dictionary
---
buckminsterfullerene
     n : a spheroidal fullerene; the first known example of a
         fullerene [syn: {buckyball}]
