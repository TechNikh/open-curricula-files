---
version: 1
type: video
provider: YouTube
id: C8mudsCSmcU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Simple%20Differential%20Equations.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 150622d1-4d9f-42d5-a970-42800e9f4248
updated: 1486069693
title: Simple Differential Equations
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/C8mudsCSmcU/default.jpg
    - https://i3.ytimg.com/vi/C8mudsCSmcU/1.jpg
    - https://i3.ytimg.com/vi/C8mudsCSmcU/2.jpg
    - https://i3.ytimg.com/vi/C8mudsCSmcU/3.jpg
---
