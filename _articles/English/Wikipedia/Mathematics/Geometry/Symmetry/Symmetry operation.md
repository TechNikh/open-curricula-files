---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_operation
offline_file: ""
offline_thumbnail: ""
uuid: aa77495e-4824-4743-bfec-7b2b4d833f86
updated: 1484309319
title: Symmetry operation
tags:
    - Molecules
    - Proper rotation operations
    - Improper rotation operations
    - Examples
    - Crystals
categories:
    - Symmetry
---
In the context of molecular symmetry, a symmetry operation is a permutation of atoms such that the molecule or crystal is transformed into a state indistinguishable from the starting state. Two basic facts follow from this definition, which emphasize its usefulness.
