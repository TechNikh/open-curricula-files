---
title: Oxygen Movement from Alveoli to Capillaries
youtube_id: nRpwdwm06Ic
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
