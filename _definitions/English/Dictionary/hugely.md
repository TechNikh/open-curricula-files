---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hugely
offline_file: ""
offline_thumbnail: ""
uuid: c68e1d38-8b27-4226-8fcf-1746c9514419
updated: 1484310537
title: hugely
categories:
    - Dictionary
---
hugely
     adv : extremely; "he was enormously popular" [syn: {enormously}, {tremendously},
            {staggeringly}]
