---
version: 1
type: video
provider: YouTube
id: tjQ6HRNynsk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 17f08ca5-058d-4838-9bb4-83f4efce009d
updated: 1486069640
title: 'Concave lens - physics experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/tjQ6HRNynsk/default.jpg
    - https://i3.ytimg.com/vi/tjQ6HRNynsk/1.jpg
    - https://i3.ytimg.com/vi/tjQ6HRNynsk/2.jpg
    - https://i3.ytimg.com/vi/tjQ6HRNynsk/3.jpg
---
