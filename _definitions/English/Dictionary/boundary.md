---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boundary
offline_file: ""
offline_thumbnail: ""
uuid: 2101a0b3-9752-4f16-ba2c-7db481cfd6ef
updated: 1484310210
title: boundary
categories:
    - Dictionary
---
boundary
     n 1: the line or plane indicating the limit or extent of
          something [syn: {bound}, {bounds}]
     2: a line determining the limits of an area [syn: {edge}, {bound}]
     3: the greatest possible degree of something; "what he did was
        beyond the bounds of acceptable behavior"; "to the limit
        of his ability" [syn: {limit}, {bounds}]
