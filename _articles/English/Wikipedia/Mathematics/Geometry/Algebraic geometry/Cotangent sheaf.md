---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cotangent_sheaf
offline_file: ""
offline_thumbnail: ""
uuid: da26843f-6d47-4191-955b-f21080d55493
updated: 1484309138
title: Cotangent sheaf
tags:
    - Construction through a diagonal morphism
    - Relation to a tautological line bundle
    - Cotangent stack
    - Notes
categories:
    - Algebraic geometry
---
In algebraic geometry, given a morphism f: X → S of schemes, the cotangent sheaf on X is the sheaf of 
  
    
      
        
          
            
              O
            
          
          
            X
          
        
      
    
    {\displaystyle {\mathcal {O}}_{X}}
  
-modules that represents (or classifies) S-derivations [1] in the sense: for any 
  
    
      
        
          
            
              O
            
          
          
            X
          
        
      
    
    {\displaystyle {\mathcal {O}}_{X}}
  
-modules F, there is an isomorphism
