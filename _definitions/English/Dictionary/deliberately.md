---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deliberately
offline_file: ""
offline_thumbnail: ""
uuid: 7478cf8c-5151-4658-9bf2-4c39403b440c
updated: 1484310581
title: deliberately
categories:
    - Dictionary
---
deliberately
     adv 1: with intention; in an intentional manner; "he used that word
            intentionally"; "I did this by choice" [syn: {intentionally},
             {designedly}, {on purpose}, {purposely}, {advisedly},
             {by choice}, {by design}] [ant: {by chance}, {unintentionally},
             {unintentionally}]
     2: in a deliberate unhurried manner; "she was working
        deliberately" [syn: {measuredly}]
