---
version: 1
type: video
provider: YouTube
id: P03ljxjy8Nw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Passed%20Bike%20Word%20Problem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b8f7a8e0-172a-4f97-a3f3-340ab5dd9aea
updated: 1486069571
title: Passed Bike Word Problem
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/P03ljxjy8Nw/default.jpg
    - https://i3.ytimg.com/vi/P03ljxjy8Nw/1.jpg
    - https://i3.ytimg.com/vi/P03ljxjy8Nw/2.jpg
    - https://i3.ytimg.com/vi/P03ljxjy8Nw/3.jpg
---
Fun algebra word problem involving a train passing a bike.
More free lessons at: http://www.khanacademy.org/video?v=P03ljxjy8Nw
