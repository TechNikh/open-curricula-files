---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/being
offline_file: ""
offline_thumbnail: ""
uuid: ebfee248-882e-4627-81bc-d01022be6530
updated: 1484310357
title: being
categories:
    - Dictionary
---
being
     n 1: the state or fact of existing; "a point of view gradually
          coming into being"; "laws in existence for centuries"
          [syn: {beingness}, {existence}] [ant: {nonexistence}, {nonbeing}]
     2: a living thing that has (or can develop) the ability to act
        or function independently [syn: {organism}]
