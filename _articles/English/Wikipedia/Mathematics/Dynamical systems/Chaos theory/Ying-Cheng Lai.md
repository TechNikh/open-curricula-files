---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ying-Cheng_Lai
offline_file: ""
offline_thumbnail: ""
uuid: 04f83d70-6f83-4f3c-a341-bb65eed31219
updated: 1484308715
title: Ying-Cheng Lai
tags:
    - Research areas
    - Awards
    - Books
categories:
    - Chaos theory
---
Ying-Cheng Lai is a Chinese theoretical physicist who works in the area of chaos theory. He is one among the pioneers in the nonlinear and complex systems and chaos theory. Currently he works in Arizona State University holding an ISS Chair Professorship.
