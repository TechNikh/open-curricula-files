---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Steven_Brams
offline_file: ""
offline_thumbnail: ""
uuid: 2ef0cb2e-a6b8-49ea-acec-e3102c49025e
updated: 1484308527
title: Steven Brams
tags:
    - Education
    - Career
    - Bibliography
categories:
    - Fair division
---
Steven J. Brams (born November 28, 1940, in Concord, New Hampshire) is an American game theorist and political scientist at the New York University Department of Politics. Brams is best known for using the techniques of game theory, public choice theory, and social choice theory to analyze voting systems and fair division. He is one of the independent discoverers of approval voting,[1] as well as an extension of approval voting to multiple-winner elections known as Satisfaction approval voting.[2]
