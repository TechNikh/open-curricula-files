---
version: 1
type: video
provider: YouTube
id: ld8r6NGXRts
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Three%20types%20of%20sentence.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8e4422c3-44a1-47ec-bfe5-5c98fd512d00
updated: 1486069603
title: Three types of sentence
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/ld8r6NGXRts/default.jpg
    - https://i3.ytimg.com/vi/ld8r6NGXRts/1.jpg
    - https://i3.ytimg.com/vi/ld8r6NGXRts/2.jpg
    - https://i3.ytimg.com/vi/ld8r6NGXRts/3.jpg
---
Three essential types of sentence are declarative sentences (which are statements), interrogative sentences (which are questions), and imperative sentences (which are orders).  Join us as we give examples of each!


Practice this yourself on Khan Academy right now: https://www.khanacademy.org/syntax/e/declarative--interrogative--and-imperative-sentences 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/exclamations-syntax-khan-academy

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/syntax/v/what-is-a-sentence-syntax-khan-academy

Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
