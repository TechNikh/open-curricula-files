---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cross
offline_file: ""
offline_thumbnail: ""
uuid: 8e520db5-6e94-4228-b85d-205640db9952
updated: 1484310303
title: cross
categories:
    - Dictionary
---
cross
     adj 1: extending or lying across; in a crosswise direction; at
            right angles to the long axis; "cross members should
            be all steel"; "from the transverse hall the stairway
            ascends gracefully"; "transversal vibrations";
            "transverse colon" [syn: {cross(a)}, {transverse}, {transversal},
             {thwartwise}]
     2: perversely irritable [syn: {crabbed}, {crabby}, {fussy}, {grouchy},
         {grumpy}, {bad-tempered}, {ill-tempered}]
     n 1: a wooden structure consisting of an upright post with a
          transverse piece
     2: marking consisting of crossing lines [syn: {crisscross}, {mark}]
     3: a cross as an emblem of ...
