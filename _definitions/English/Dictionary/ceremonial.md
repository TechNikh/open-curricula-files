---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ceremonial
offline_file: ""
offline_thumbnail: ""
uuid: c0d3d1cc-0074-4870-9b76-bdcee77a282e
updated: 1484310595
title: ceremonial
categories:
    - Dictionary
---
ceremonial
     adj : marked by pomp or ceremony or formality; "a ceremonial
           occasion"; "ceremonial garb"
     n : a formal event performed on a special occasion; "a ceremony
         commemorating Pearl Harbor" [syn: {ceremony}, {ceremonial
         occasion}, {observance}]
