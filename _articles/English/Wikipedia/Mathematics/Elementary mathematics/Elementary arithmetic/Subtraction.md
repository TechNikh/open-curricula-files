---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Subtraction
offline_file: ""
offline_thumbnail: ""
uuid: de3ab116-3c1c-4758-9d6b-3a7f6dfc7c31
updated: 1484308924
title: Subtraction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Subtraction01.svg.png
tags:
    - Notation and terminology
    - Of integers and real numbers
    - Integers
    - Natural numbers
    - Real numbers
    - Properties
    - Anticommutativity
    - Non-associativity
    - Predecessor
    - Units of measurement
    - Percentages
    - In computing
    - The teaching of subtraction in schools
    - In America
    - In Europe
    - Comparing the two main methods
    - Subtraction by hand
    - Austrian method
    - Subtraction from left to right
    - American method
    - Trade first
    - Partial differences
    - Nonvertical methods
    - Counting up
    - Breaking up the subtraction
    - Same change
    - Bibliography
categories:
    - Elementary arithmetic
---
Subtraction is a mathematical operation that represents the operation of removing objects from a collection. It is signified by the minus sign (−). For example, in the picture on the right, there are 5 − 2 apples—meaning 5 apples with 2 taken away, which is a total of 3 apples. Therefore, 5 − 2 = 3. Besides counting fruits, subtraction can also represent combining other physical and abstract quantities using different kinds of objects including negative numbers, fractions, irrational numbers, vectors, decimals, functions, and matrices.
