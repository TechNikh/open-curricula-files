---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Saddle-node_bifurcation
offline_file: ""
offline_thumbnail: ""
uuid: 35e412a3-cd72-406b-85f4-90cbed9b1405
updated: 1484308702
title: Saddle-node bifurcation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Saddlenode.gif
tags:
    - Normal form
    - Example in two dimensions
    - Notes
categories:
    - Bifurcation theory
---
In the mathematical area of bifurcation theory a saddle-node bifurcation, tangential bifurcation or fold bifurcation is a local bifurcation in which two fixed points (or equilibria) of a dynamical system collide and annihilate each other. The term 'saddle-node bifurcation' is most often used in reference to continuous dynamical systems. In discrete dynamical systems, the same bifurcation is often instead called a fold bifurcation. Another name is blue skies bifurcation in reference to the sudden creation of two fixed points.[1]
