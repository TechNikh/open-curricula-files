---
version: 1
type: video
provider: YouTube
id: -K8Y0ATkkAI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/DNA%20transcription%20and%20translation%20%5BHD%20animation%5D.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 87f9b145-c8ad-440e-8ff1-9eb2bdfd6c12
updated: 1486069672
title: 'DNA transcription and translation [HD animation]'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/-K8Y0ATkkAI/default.jpg
    - https://i3.ytimg.com/vi/-K8Y0ATkkAI/1.jpg
    - https://i3.ytimg.com/vi/-K8Y0ATkkAI/2.jpg
    - https://i3.ytimg.com/vi/-K8Y0ATkkAI/3.jpg
---
