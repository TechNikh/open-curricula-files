---
version: 1
type: article
id: https://en.wikipedia.org/wiki/3-4-6-12_tiling
offline_file: ""
offline_thumbnail: ""
uuid: 43e96754-5692-45db-aa26-20bd2b4921d3
updated: 1484309352
title: 3-4-6-12 tiling
tags:
    - Geometry
    - Related k-uniform tilings of regular polygons
    - Dual tiling
    - Notes
categories:
    - Geometry stubs
---
In geometry of the Euclidean plane, the 3-4-6-12 tiling is one of 20 2-uniform tilings of the Euclidean plane by regular polygons, containing regular triangles, squares, hexagons and dodecagons, arranged in two vertex configuration: 3.4.6.4 and 4.6.12.[1][2][3][4]
