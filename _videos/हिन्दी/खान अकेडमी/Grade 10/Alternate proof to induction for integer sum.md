---
version: 1
type: video
provider: YouTube
id: psYie9l_hhM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 96312e5d-474d-4283-ac5b-ea0f4ce66d37
updated: 1486069619
title: Alternate proof to induction for integer sum
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/psYie9l_hhM/default.jpg
    - https://i3.ytimg.com/vi/psYie9l_hhM/1.jpg
    - https://i3.ytimg.com/vi/psYie9l_hhM/2.jpg
    - https://i3.ytimg.com/vi/psYie9l_hhM/3.jpg
---
