---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accommodating
offline_file: ""
offline_thumbnail: ""
uuid: 9a1ae007-dc50-4e8c-99f5-2c76ac72dcb3
updated: 1484310475
title: accommodating
categories:
    - Dictionary
---
accommodating
     adj 1: helpful in bringing about a harmonious adaptation; "the
            warden was always accommodating in allowing visitors
            in"; "made a special effort to be accommodating" [syn:
             {accommodative}] [ant: {unaccommodating}]
     2: obliging; willing to do favors; "made a special effort to be
        accommodating"
