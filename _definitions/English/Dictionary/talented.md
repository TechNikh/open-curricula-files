---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/talented
offline_file: ""
offline_thumbnail: ""
uuid: cc58924c-90d6-45cb-a1bb-5ffd004652bf
updated: 1484310138
title: talented
categories:
    - Dictionary
---
talented
     adj : showing a natural aptitude for something [syn: {gifted}]
