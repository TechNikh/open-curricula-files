---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Versailles
offline_file: ""
offline_thumbnail: ""
uuid: e6435e67-05b2-4cbd-9c4d-07885c9b4b6b
updated: 1484308473
title: Palace of Versailles
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Adam_Frans_van_der_Meulen_-_Construction_of_the_Ch%25C3%25A2teau_de_Versailles_-_WGA15115.jpg'
tags:
    - History
    - Architectural history
    - 17th century
    - Cost
    - 18th century
    - 19th century
    - 20th century
    - 21st century
    - Social history
    - Politics of display
categories:
    - Uncategorized
---
The Palace of Versailles, Château de Versailles, or simply Versailles (English /vɛərˈsaɪ/ vair-SY or /vərˈsaɪ/ vər-SY; French: [vɛʁsaj]), is a royal château in Versailles in the Île-de-France region of France.
