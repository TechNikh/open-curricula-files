---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lastly
offline_file: ""
offline_thumbnail: ""
uuid: 8fedc2b4-bf04-4dfe-862e-8065edf1951f
updated: 1484310537
title: lastly
categories:
    - Dictionary
---
lastly
     adv : the item at the end; "last, I'll discuss family values"
           [syn: {last}, {in conclusion}, {finally}]
