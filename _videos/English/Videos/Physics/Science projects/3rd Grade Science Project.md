---
version: 1
type: video
provider: YouTube
id: -_UCxn5bRKM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fdc94fd2-73d7-4686-a161-90fce476e8d4
updated: 1486069634
title: 3rd Grade Science Project
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/-_UCxn5bRKM/default.jpg
    - https://i3.ytimg.com/vi/-_UCxn5bRKM/1.jpg
    - https://i3.ytimg.com/vi/-_UCxn5bRKM/2.jpg
    - https://i3.ytimg.com/vi/-_UCxn5bRKM/3.jpg
---
