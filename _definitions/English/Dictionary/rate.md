---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rate
offline_file: ""
offline_thumbnail: ""
uuid: 90936e73-dd49-46bb-9656-4a61de6f3480
updated: 1484310290
title: rate
categories:
    - Dictionary
---
rate
     n 1: amount of a charge or payment relative to some basis; "a
          10-minute phone call at that rate would cost $5" [syn: {charge
          per unit}]
     2: a magnitude or frequency relative to a time unit; "they
        traveled at a rate of 55 miles per hour"; "the rate of
        change was faster than expected"
     3: the relative speed of progress or change; "he lived at a
        fast pace"; "he works at a great rate"; "the pace of
        events accelerated" [syn: {pace}]
     v 1: assign a rank or rating to; "how would you rank these
          students?"; "The restaurant is rated highly in the food
          guide" [syn: {rank}, {range}, {order}, {grade}, {place}]
 ...
