---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kmt
offline_file: ""
offline_thumbnail: ""
uuid: f14fc06f-07a1-4d1e-89c4-89dd0813a73e
updated: 1484308485
title: Kuomintang
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/155px-KMT_%2528Chinese_characters%2529.svg_0.png'
tags:
    - History
    - Founding and Sun Yat-sen era
    - Under Chiang Kai-shek in Mainland China
    - KMT in Taiwan
    - Current issues and challenges
    - Elections and results
    - Supporter base
    - Organization
    - Leadership
    - 'Chairperson & Vice chairpersons'
categories:
    - Uncategorized
---
The Kuomintang[8][9] (/ˌɡwoʊmɪnˈdɑːŋ/ or /-ˈtæŋ/;[10] KMT), often translated as the Nationalist Party of China or Chinese Nationalist Party,[11] also spelled as Guomindang (/ˌɡwoʊmɪnˈdɑːŋ/; GMD) by its Pinyin transliteration, is a major political party in the Republic of China, currently the second-largest in the country.
