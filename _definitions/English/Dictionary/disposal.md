---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disposal
offline_file: ""
offline_thumbnail: ""
uuid: 280ccde1-b24e-46a1-8993-b538a474fbe6
updated: 1484310480
title: disposal
categories:
    - Dictionary
---
disposal
     n 1: the power to use something or someone; "used all the
          resources at his disposal"
     2: a method of tending to (especially business) matters [syn: {administration}]
     3: the act or means of getting rid of something [syn: {disposition}]
     4: a kitchen appliance for disposing of garbage [syn: {electric
        pig}, {garbage disposal}]
