---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Envy-free_item_assignment
offline_file: ""
offline_thumbnail: ""
uuid: e3e1b1b0-72c3-4b95-a75a-eee5f4c44bd5
updated: 1484308524
title: Envy-free item assignment
tags:
    - Finding an envy-free allocation if it exists
    - Finding a partial envy-free allocation
    - Bounding the maximum envy
    - Round-robin protocol
    - Unenvied-agent protocol
    - A-CEEI protocol
    - Maximum-Nash-Welfare
    - EF1 vs EFx
    - Minimizing the envy-ratio
    - General valuations
    - Additive and identical valuations
    - Additive and different valuations
    - Dividing one item
    - Deciding whether an EF allocation exists
    - Existence of EF allocations with random valuations
    - Envy-freeness vs. other fairness criteria
    - Summary table
categories:
    - Fair division
---
Envy-free item assignment (EF assignment) is a fair item assignment problem, in which the fairness criterion is envy-freeness - each agent should receive a bundle that he believes to be at least as good as the bundle of any other agent.[1]:296–297
