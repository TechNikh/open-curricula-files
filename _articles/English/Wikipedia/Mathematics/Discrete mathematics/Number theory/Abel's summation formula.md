---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Abel%27s_summation_formula'
offline_file: ""
offline_thumbnail: ""
uuid: bd730ff5-027a-489f-81f5-0fe16d63225b
updated: 1484308545
title: "Abel's summation formula"
tags:
    - Identity
    - Examples
    - Euler–Mascheroni constant
    - "Representation of Riemann's zeta function"
    - Reciprocal of Riemann zeta function
categories:
    - Number theory
---
