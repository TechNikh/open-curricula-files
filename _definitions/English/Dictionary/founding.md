---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/founding
offline_file: ""
offline_thumbnail: ""
uuid: df4bcf40-d2e5-4aeb-8aca-0e595bb96143
updated: 1484310429
title: founding
categories:
    - Dictionary
---
founding
     n : the act of starting something for the first time;
         introducing something new; "she looked forward to her
         initiation as an adult"; "the foundation of a new
         scientific society"; "he regards the fork as a modern
         introduction" [syn: {initiation}, {foundation}, {institution},
          {origination}, {creation}, {innovation}, {introduction},
          {instauration}]
