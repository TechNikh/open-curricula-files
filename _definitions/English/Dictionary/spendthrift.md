---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spendthrift
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484440441
title: spendthrift
categories:
    - Dictionary
---
spendthrift
     adj : recklessly wasteful; "prodigal in their expenditures" [syn:
           {extravagant}, {prodigal}, {profligate}]
     n : someone who spends money prodigally [syn: {spend-all}, {spender},
          {scattergood}]
