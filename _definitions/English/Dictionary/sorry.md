---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sorry
offline_file: ""
offline_thumbnail: ""
uuid: 24cf3cb0-2e89-4ec5-8e84-961d9e2e3d90
updated: 1484310150
title: sorry
categories:
    - Dictionary
---
sorry
     adj 1: keenly sorry or regretful; "felt bad about letting the team
            down"; "was sorry that she had treated him so badly";
            "felt bad about breaking the vase" [syn: {bad}]
     2: feeling or expressing sorrow or pity; "a pitying observer
        threw his coat around her shoulders"; "let him perish
        without a pitying thought of ours wasted upon him"- Thomas
        De Quincey [syn: {pitying}, {sorry for(p)}]
     3: having regret or sorrow or a sense of loss over something
        done or undone; "felt regretful over his vanished youth";
        "regretful over mistakes she had made" [syn: {regretful}]
        [ant: {unregretful}]
     4: feeling or ...
