---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/limit
offline_file: ""
offline_thumbnail: ""
uuid: 8c694f38-4b62-4cac-8ce0-f5d803e3ead9
updated: 1484310240
title: limit
categories:
    - Dictionary
---
limit
     n 1: the greatest possible degree of something; "what he did was
          beyond the bounds of acceptable behavior"; "to the limit
          of his ability" [syn: {bounds}, {boundary}]
     2: final or latest limiting point [syn: {terminus ad quem}, {terminal
        point}]
     3: the boundary of a specific area [syn: {demarcation}, {demarcation
        line}]
     4: as far as something can go
     5: the mathematical value toward which a function goes as the
        independent variable approaches infinity [syn: {limit
        point}, {point of accumulation}]
     6: the greatest amount of something that is possible or
        allowed; "there are limits on the amount you can ...
