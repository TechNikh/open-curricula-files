---
version: 1
type: video
provider: YouTube
id: 70DyJwwFnkU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Action%20potential.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6f9a79ac-a61f-4113-a6c2-94ce73889aa5
updated: 1486069669
title: Action potential
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/70DyJwwFnkU/default.jpg
    - https://i3.ytimg.com/vi/70DyJwwFnkU/1.jpg
    - https://i3.ytimg.com/vi/70DyJwwFnkU/2.jpg
    - https://i3.ytimg.com/vi/70DyJwwFnkU/3.jpg
---
