---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/find
offline_file: ""
offline_thumbnail: ""
uuid: 3addc44d-c911-4e46-977d-9a39f615e886
updated: 1484310351
title: find
categories:
    - Dictionary
---
find
     n 1: a productive insight [syn: {discovery}, {breakthrough}]
     2: the act of discovering something [syn: {discovery}, {uncovering}]
     v 1: come upon, as if by accident; meet with; "We find this idea
          in Plato"; "I happened upon the most wonderful bakery
          not very far from here"; "She chanced upon an
          interesting book in the bookstore the other day" [syn: {happen},
           {chance}, {bump}, {encounter}]
     2: discover or determine the existence, presence, or fact of;
        "She detected high levels of lead in her drinking water";
        "We found traces of lead in the paint" [syn: {detect}, {observe},
         {discover}, {notice}]
     3: ...
