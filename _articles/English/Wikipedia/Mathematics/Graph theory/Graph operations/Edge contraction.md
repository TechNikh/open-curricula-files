---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edge_contraction
offline_file: ""
offline_thumbnail: ""
uuid: 968288d5-802d-47bf-a810-1825acb8f6aa
updated: 1484309447
title: Edge contraction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-Edge_contraction.svg.png
tags:
    - Definition
    - Formal definition
    - Vertex identification
    - Vertex cleaving
    - Path contraction
    - Twisting
    - Applications
    - Notes
categories:
    - Graph operations
---
In graph theory, an edge contraction is an operation which removes an edge from a graph while simultaneously merging the two vertices that it previously joined. Edge contraction is a fundamental operation in the theory of graph minors. Vertex identification is a less restrictive form of this operation.
