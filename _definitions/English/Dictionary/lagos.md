---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lagos
offline_file: ""
offline_thumbnail: ""
uuid: 9b8e8055-dc94-44e1-a347-5838eaa83282
updated: 1484310579
title: lagos
categories:
    - Dictionary
---
Lagos
     n : chief port and economic center of Nigeria; located in
         southwestern Nigeria on the Gulf of Guinea; former
         capital of Nigeria
