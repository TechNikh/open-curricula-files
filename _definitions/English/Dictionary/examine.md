---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/examine
offline_file: ""
offline_thumbnail: ""
uuid: d081a81f-e78e-4b90-a73c-07afdaaf9eea
updated: 1484310284
title: examine
categories:
    - Dictionary
---
examine
     v 1: consider in detail and subject to an analysis in order to
          discover essential features or meaning; "analyze a
          sonnet by Shakespeare"; "analyze the evidence in a
          criminal trial"; "analyze your real motives" [syn: {analyze},
           {analyse}, {study}, {canvass}, {canvas}]
     2: observe, check out, and look over carefully or inspect; "The
        customs agent examined the baggage"; "I must see your
        passport before you can enter the country" [syn: {see}]
     3: question or examine thoroughly and closely [syn: {probe}]
     4: question closely
     5: put to the test, as for its quality, or give experimental
        use to; "This ...
