---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coverage
offline_file: ""
offline_thumbnail: ""
uuid: 6e0faca7-5f86-40d8-ad63-b7fce197a322
updated: 1484310561
title: coverage
categories:
    - Dictionary
---
coverage
     n 1: the total amount and type of insurance carried [syn: {insurance
          coverage}]
     2: the extent to which something is covered; "the dictionary's
        coverage of standard English is excellent"
     3: the news as presented by reporters for newspapers or radio
        or television; "they accused the paper of biased coverage
        of race relations" [syn: {reporting}, {reportage}]
