---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liberate
offline_file: ""
offline_thumbnail: ""
uuid: c2145a0d-42e0-4be5-8c12-27b0e88d1c5f
updated: 1484310315
title: liberate
categories:
    - Dictionary
---
liberate
     v 1: give equal rights to; of women and minorities [syn: {emancipate}]
     2: grant freedom to; free from confinement [syn: {free}, {release},
         {unloose}, {unloosen}, {loose}] [ant: {confine}]
     3: grant freedom to; "The students liberated their slaves upon
        graduating from the university" [syn: {set free}]
