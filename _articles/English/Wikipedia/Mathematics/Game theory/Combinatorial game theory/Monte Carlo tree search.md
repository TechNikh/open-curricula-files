---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Monte_Carlo_tree_search
offline_file: ""
offline_thumbnail: ""
uuid: 25acd09e-62d5-4dfa-bd16-85284ab8e8c6
updated: 1484308535
title: Monte Carlo tree search
tags:
    - Principle of operation
    - Pure Monte Carlo game search
    - Exploration and exploitation
    - Advantages and disadvantages
    - Improvements
    - History
    - Bibliography
categories:
    - Combinatorial game theory
---
In computer science, Monte Carlo tree search (MCTS) is a heuristic search algorithm for some kinds of decision processes, most notably those employed in game play. The original idea of "recursive rolling out and backtracking" with "adaptive" sampling choices in MCTS came from the Adaptive Multi-stage Sampling (AMS) algorithm of Chang et al.[1] developed within the sequential decision-making model of Markov decision process, which was published in Operations Research. A leading example of MCTS is recent computer Go programs,[2] but it also has been used in other board games, as well as real-time video games and non-deterministic games such as poker (see history section).
