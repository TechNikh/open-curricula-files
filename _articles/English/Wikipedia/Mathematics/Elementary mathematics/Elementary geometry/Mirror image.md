---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mirror_image
offline_file: ""
offline_thumbnail: ""
uuid: 5b5b4cc4-1664-4838-93ac-e8314328b95f
updated: 1484308942
title: Mirror image
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mirror.jpg
tags:
    - In geometry and geometrical optics
    - In two dimensions
    - In three dimensions
    - Effect of mirror on the lighting of the scene
    - Mirror writing
    - Systems of mirrors
categories:
    - Elementary geometry
---
A mirror image (in a plane mirror) is a reflected duplication of an object that appears almost identical, but is reversed in the direction perpendicular to the mirror surface. As an optical effect it results from reflection off of substances such as a mirror or water. It is also a concept in geometry and can be used as a conceptualization process for 3-D structures.
