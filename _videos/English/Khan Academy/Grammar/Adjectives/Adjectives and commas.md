---
version: 1
type: video
provider: YouTube
id: OfxiZdsqGeA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Adjectives/Adjectives%20and%20commas.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 95e09048-dc55-47b3-8f50-b3a2584e3294
updated: 1486069601
title: Adjectives and commas
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Adjectives
thumbnail_urls:
    - https://i3.ytimg.com/vi/OfxiZdsqGeA/default.jpg
    - https://i3.ytimg.com/vi/OfxiZdsqGeA/1.jpg
    - https://i3.ytimg.com/vi/OfxiZdsqGeA/2.jpg
    - https://i3.ytimg.com/vi/OfxiZdsqGeA/3.jpg
---
When using more than one adjective, sometimes you put a comma between them and sometimes you don't. Learn how to tell when you need a comma in this video!

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/the-modifier/e/commas-and-adjectives

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-modifier/v/intro-to-the-comparative-and-the-superlative-v2

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-modifier/v/adjective-order

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
