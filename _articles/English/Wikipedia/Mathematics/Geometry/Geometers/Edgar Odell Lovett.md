---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edgar_Odell_Lovett
offline_file: ""
offline_thumbnail: ""
uuid: 125368e1-56ed-4f3c-b0d4-96988e59f760
updated: 1484309091
title: Edgar Odell Lovett
tags:
    - Biography
    - Early life and career
    - family
    - Legacy
    - Additional reading
categories:
    - Geometers
---
He was the first president of Rice Institute (now Rice University) in Houston, Texas. Lovett was recommended to the post by Woodrow Wilson, then president of Princeton University.[1][2]
