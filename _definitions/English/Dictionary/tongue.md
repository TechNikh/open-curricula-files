---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tongue
offline_file: ""
offline_thumbnail: ""
uuid: 81f1860c-dc43-4c87-a096-5e870a166fab
updated: 1484310351
title: tongue
categories:
    - Dictionary
---
tongue
     n 1: a mobile mass of muscular tissue covered with mucous
          membrane and located in the oral cavity [syn: {lingua},
          {glossa}, {clapper}]
     2: a human written or spoken language used by a community;
        opposed to e.g. a computer language [syn: {natural
        language}] [ant: {artificial language}]
     3: any long thin projection that is transient; "tongues of
        flame licked at the walls"; "rifles exploded quick knives
        of fire into the dark" [syn: {knife}]
     4: a manner of speaking; "he spoke with a thick tongue"; "she
        has a glib tongue"
     5: a narrow strip of land that juts out into the sea [syn: {spit}]
     6: the tongue ...
