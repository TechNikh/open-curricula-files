---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greatest
offline_file: ""
offline_thumbnail: ""
uuid: fdbd8d3c-9ce8-43a1-a314-8991b6777723
updated: 1484310413
title: greatest
categories:
    - Dictionary
---
greatest
     adj 1: not to be surpassed; "his top effort" [syn: {top}]
     2: greatest in size of those under consideration [syn: {biggest},
         {largest}]
     3: greatest in importance or degree or significance or
        achievement; "our greatest statesmen"; "the country's
        leading poet"; "a preeminent archeologist" [syn: {leading(a)},
         {preeminent}]
     4: highest in quality [syn: {sterling(a)}, {superlative}]
