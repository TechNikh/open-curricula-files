---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pyre
offline_file: ""
offline_thumbnail: ""
uuid: cb1b693f-5e70-42fb-8537-2ad6c3ac4743
updated: 1484310411
title: pyre
categories:
    - Dictionary
---
pyre
     n : wood heaped for burning a dead body as a funeral rite [syn:
         {funeral pyre}]
