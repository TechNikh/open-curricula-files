---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/headman
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484565901
title: headman
categories:
    - Dictionary
---
headman
     n 1: an executioner who beheads the condemned person [syn: {headsman}]
     2: the head of a tribe or clan [syn: {tribal chief}, {chieftain}]
