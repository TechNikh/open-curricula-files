---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crowd
offline_file: ""
offline_thumbnail: ""
uuid: 47068ea4-a79e-4a5d-a725-9985697c268d
updated: 1484310183
title: crowd
categories:
    - Dictionary
---
crowd
     n 1: a large number of things or people considered together; "a
          crowd of insects assembled around the flowers"
     2: an informal body of friends; "he still hangs out with the
        same crowd" [syn: {crew}, {gang}, {bunch}]
     v 1: cause to herd, drive, or crowd together; "We herded the
          children into a spare classroom" [syn: {herd}]
     2: fill or occupy to the point of overflowing; "The students
        crowded the auditorium"
     3: to gather together in large numbers; "men in straw boaters
        and waxed mustaches crowded the verandah" [syn: {crowd
        together}]
     4: approach a certain age or speed; "She is pushing fifty"
        [syn: ...
