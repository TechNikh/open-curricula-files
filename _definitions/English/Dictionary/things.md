---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/things
offline_file: ""
offline_thumbnail: ""
uuid: 717f62fb-ac1a-411c-b3c8-ae668a58e676
updated: 1484310323
title: things
categories:
    - Dictionary
---
things
     n : any movable possession (especially articles of clothing);
         "she packed her things and left"
