---
version: 1
type: video
provider: YouTube
id: BY9VGS2eXas
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2b51ecaf-d13a-4bc5-8baa-6a84a3d5ec31
updated: 1486069664
title: "The ABC's of gas- Avogadro, Boyle, Charles - Brian Bennett"
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/BY9VGS2eXas/default.jpg
    - https://i3.ytimg.com/vi/BY9VGS2eXas/1.jpg
    - https://i3.ytimg.com/vi/BY9VGS2eXas/2.jpg
    - https://i3.ytimg.com/vi/BY9VGS2eXas/3.jpg
---
