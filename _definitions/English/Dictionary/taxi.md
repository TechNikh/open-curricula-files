---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/taxi
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484318041
title: taxi
categories:
    - Dictionary
---
taxi
     n : a car driven by a person whose job is to take passengers
         where they want to go in exchange for money [syn: {cab},
         {hack}, {taxicab}]
     v 1: travel slowly; "The plane taxied down the runway"
     2: ride in a taxicab
     [also: {taxying}, {taxies} (pl)]
