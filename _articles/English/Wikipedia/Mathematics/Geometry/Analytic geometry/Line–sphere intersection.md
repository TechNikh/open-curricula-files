---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Line%E2%80%93sphere_intersection'
offline_file: ""
offline_thumbnail: ""
uuid: 647bd337-1d1b-4fcb-a83a-fc2b25dd7642
updated: 1484309162
title: Line–sphere intersection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Line-Sphere_Intersection_Cropped.png
categories:
    - Analytic geometry
---
In analytic geometry, a line and a sphere can intersect in three ways: no intersection at all, at exactly one point, or in two points. Methods for distinguishing these cases, and determining equations for the points in the latter cases, are useful in a number of circumstances. For example, this is a common calculation to perform during ray tracing (Eberly 2006:698).
