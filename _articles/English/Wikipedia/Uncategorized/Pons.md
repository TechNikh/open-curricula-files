---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pons
offline_file: ""
offline_thumbnail: ""
uuid: 169dc564-3e0c-48ad-a467-84077fa5153f
updated: 1484308378
title: Pons
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray768.png
tags:
    - Structure
    - Development
    - Nucleus
    - Function
    - Clinical significance
    - Other animals
    - Evolution
    - Additional images
categories:
    - Uncategorized
---
The pons is part of the brainstem, and in humans and other bipeds lies between the midbrain (above) and the medulla oblongata (below) and in front of the cerebellum.
