---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rainy_season
offline_file: ""
offline_thumbnail: ""
uuid: 39c70067-00e9-459f-b40c-d410797a62e1
updated: 1484308366
title: Wet season
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cairns_climate.svg.png
tags:
    - Character of the rainfall
    - Areas affected
    - Effects
    - Floods
    - Life adaptations
    - Humans
    - Animals
categories:
    - Uncategorized
---
The rainy season, or monsoon season, is the time of year when most of a region's average annual rainfall occurs. It usually lasts one or more months.[1] The term "green season" is also sometimes used as a euphemism by tourist authorities.[2] Areas with wet seasons are dispersed across portions of the tropics and subtropics.[3]
