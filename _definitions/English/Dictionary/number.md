---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/number
offline_file: ""
offline_thumbnail: ""
uuid: c022e37a-b99b-44d1-b114-7ce4d73e62ec
updated: 1484310343
title: number
categories:
    - Dictionary
---
number
     n 1: the property possessed by a sum or total or indefinite
          quantity of units or individuals; "he had a number of
          chores to do"; "the number of parameters is small"; "the
          figure was about a thousand" [syn: {figure}]
     2: a concept of quantity derived from zero and units; "every
        number has a unique position in the sequence"
     3: a short theatrical performance that is part of a longer
        program; "he did his act three times every evening"; "she
        had a catchy little routine"; "it was one of the best
        numbers he ever did" [syn: {act}, {routine}, {turn}, {bit}]
     4: a numeral or string of numerals that is used for
     ...
