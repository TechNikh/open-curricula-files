---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insect
offline_file: ""
offline_thumbnail: ""
uuid: 8f5ec9c0-73a2-4a98-8369-4cb05f75b715
updated: 1484310295
title: insect
categories:
    - Dictionary
---
insect
     n 1: small air-breathing arthropod
     2: a person who has a nasty or unethical character undeserving
        of respect [syn: {worm}, {louse}, {dirt ball}]
