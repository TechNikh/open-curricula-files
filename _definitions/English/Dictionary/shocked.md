---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shocked
offline_file: ""
offline_thumbnail: ""
uuid: 87bb3c0a-8c8b-4bff-8d03-6a9d1da2972d
updated: 1484310150
title: shocked
categories:
    - Dictionary
---
shocked
     adj : struck with fear, dread, or consternation [syn: {aghast(p)},
            {appalled}, {dismayed}]
