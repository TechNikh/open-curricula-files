---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stimulus
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484593261
title: stimulus
categories:
    - Dictionary
---
stimulus
     n : any stimulating information or event; acts to arouse action
         [syn: {stimulation}, {stimulant}, {input}]
     [also: {stimuli} (pl)]
