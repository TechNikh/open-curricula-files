---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distributed_source_coding
offline_file: ""
offline_thumbnail: ""
uuid: 433c8baa-7675-4f71-a650-d5cd85625d3e
updated: 1484308504
title: Distributed source coding
tags:
    - History
    - Theoretical bounds
    - Slepian–Wolf bound
    - Wyner–Ziv bound
    - Virtual channel
    - Asymmetric DSC vs. symmetric DSC
    - Practical distributed source coding
    - Slepian–Wolf coding – lossless distributed coding
    - >
        General theorem of Slepian–Wolf coding with syndromes for
        two sources
    - Slepian–Wolf coding example
    - |
        Asymmetric case (
        
        
        
        
        R
        
        X
        
        
        =
        3
        
        
        {\displaystyle R_{X}=3}
        
        ,
        
        
        
        
        R
        
        Y
    - Symmetric case
    - Wyner–Ziv coding – lossy distributed coding
    - Large scale distributed quantization
    - Non-asymmetric DSC
    - Non-asymmetric DSC for more than two sources
categories:
    - Coding theory
---
Distributed source coding (DSC) is an important problem in information theory and communication. DSC problems regard the compression of multiple correlated information sources that do not communicate with each other.[1] By modeling the correlation between multiple sources at the decoder side together with channel codes, DSC is able to shift the computational complexity from encoder side to decoder side, therefore provide appropriate frameworks for applications with complexity-constrained sender, such as sensor networks and video/multimedia compression (see distributed video coding[2]). One of the main properties of distributed source coding is that the computational burden in encoders is ...
