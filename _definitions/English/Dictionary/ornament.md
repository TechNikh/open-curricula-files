---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ornament
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484569141
title: ornament
categories:
    - Dictionary
---
ornament
     n : something used to beautify [syn: {decoration}, {ornamentation}]
     v 1: make more attractive by adding ornament, colour, etc.;
          "Decorate the room for the party"; "beautify yourself
          for the special day" [syn: {decorate}, {adorn}, {grace},
           {embellish}, {beautify}]
     2: be an ornament to; "stars ornamented the Christmas tree"
