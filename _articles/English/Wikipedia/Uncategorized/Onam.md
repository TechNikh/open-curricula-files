---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Onam
offline_file: ""
offline_thumbnail: ""
uuid: eb1ac922-8e32-473e-b894-bdfa720b15f1
updated: 1484308456
title: Onam
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Blathur_Onam_celebration.jpg
tags:
    - Significance
    - The legend
    - Mahabali conquers the three worlds
    - Vamana visits Mahabali
    - "Mahabali's reign ends"
    - Alternate legend
    - The Ten Days of Onam Celebrations
    - Rituals and activities
    - Pookkalam
    - Onam Sadya
    - Music and dance
    - Boat race
    - Other customs
    - Celebrations by expatriates
categories:
    - Uncategorized
---
Onam (Malayalam: ഓണം) is the most important seasonal festival celebrated in the state of Kerala in India.[1] It is also the State festival of Kerala with State holidays on 4 days starting from Onam Eve (Uthradom) to the 3rd Onam Day. Onam is a major cultural festival celebrated by people cutting across socio-economic and religious distinctions.
