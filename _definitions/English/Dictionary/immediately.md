---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immediately
offline_file: ""
offline_thumbnail: ""
uuid: 7855da0b-be2f-4a69-b371-00d682c64fac
updated: 1484310311
title: immediately
categories:
    - Dictionary
---
immediately
     adv 1: without delay or hesitation; with no time intervening; "he
            answered immediately"; "found an answer straightaway";
            "an official accused of dishonesty should be suspended
            forthwith"; "Come here now!" [syn: {instantly}, {straightaway},
             {straight off}, {directly}, {now}, {right away}, {at
            once}, {forthwith}, {in real time}, {like a shot}]
     2: near or close by; "he passed immediately behind her"
     3: bearing an immediate relation; "this immediately concerns
        your future"
