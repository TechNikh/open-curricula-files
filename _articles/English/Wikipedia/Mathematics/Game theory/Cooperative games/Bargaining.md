---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bargaining
offline_file: ""
offline_thumbnail: ""
uuid: 94a9a2d7-50d3-4dc0-8cc6-2d05e9f21fbd
updated: 1484308533
title: Bargaining
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/340px-Pasar_Malam_Rawasari_11.JPG
tags:
    - Where it takes place
    - Regional differences
    - Theories
    - Behavioral theory
    - Game theory
    - Bargaining and posted prices in retail markets
    - Processual theory
    - Integrative theory
    - Narrative theory
    - Automated bargaining
    - Anchor Pricing
categories:
    - Cooperative games
---
Bargaining or haggling is a type of negotiation in which the buyer and seller of a good or service debate the price and exact nature of a transaction. If the bargaining produces agreement on terms, the transaction takes place. Bargaining is an alternative pricing strategy to fixed prices. Optimally, if it costs the retailer nothing to engage and allow bargaining, he/she can divine the buyer's willingness to spend. It allows for capturing more consumer surplus as it allows price discrimination, a process whereby a seller can charge a higher price to one buyer who is more eager (by being richer or more desperate). Haggling has largely disappeared in parts of the world where the cost to haggle ...
