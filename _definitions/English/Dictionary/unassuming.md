---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unassuming
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484519281
title: unassuming
categories:
    - Dictionary
---
unassuming
     adj : not arrogant or presuming; "unassuming to a fault, skeptical
           about the value of his work"; "a shy retiring girl"
           [syn: {retiring}]
