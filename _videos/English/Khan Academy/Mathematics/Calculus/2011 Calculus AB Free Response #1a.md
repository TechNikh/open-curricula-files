---
version: 1
type: video
provider: YouTube
id: ppBJWf_Wdmc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%231a.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9565b17f-c3a7-4e4b-af17-8a55e123bda3
updated: 1486069692
title: '2011 Calculus AB Free Response #1a'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/ppBJWf_Wdmc/default.jpg
    - https://i3.ytimg.com/vi/ppBJWf_Wdmc/1.jpg
    - https://i3.ytimg.com/vi/ppBJWf_Wdmc/2.jpg
    - https://i3.ytimg.com/vi/ppBJWf_Wdmc/3.jpg
---
