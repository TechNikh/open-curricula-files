---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/presence
offline_file: ""
offline_thumbnail: ""
uuid: 246c0644-5885-468a-92ec-dd84b7702e9a
updated: 1484310330
title: presence
categories:
    - Dictionary
---
presence
     n 1: the state of being present; current existence; "he tested
          for the presence of radon" [ant: {absence}]
     2: the immediate proximity of someone or something; "she
        blushed in his presence"; "he sensed the presence of
        danger"; "he was well behaved in front of company" [syn: {front}]
     3: an invisible spiritual being felt to be nearby
     4: the impression that something is present; "he felt the
        presence of an evil force"
     5: dignified manner or conduct [syn: {bearing}, {comportment},
        {mien}]
     6: the act of being present [ant: {absence}]
