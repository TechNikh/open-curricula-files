---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/non-finite
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484606161
title: non-finite
categories:
    - Dictionary
---
non-finite
     adj : of verbs; having neither person nor number nor mood (as a
           participle or gerund or infinitive); "infinite verb
           form" [syn: {infinite}] [ant: {finite}]
