---
version: 1
type: video
provider: YouTube
id: xaz5ftvZCyI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Review%20of%20B%20cells%2C%20%20CD4%2B%20T%20cells%20and%20CD8%2B%20T%20cells.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f7c32626-a379-47d8-aaba-5cff1dd4cc0e
updated: 1486069581
title: 'Review of B cells,  CD4+ T cells and CD8+ T cells'
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/xaz5ftvZCyI/default.jpg
    - https://i3.ytimg.com/vi/xaz5ftvZCyI/1.jpg
    - https://i3.ytimg.com/vi/xaz5ftvZCyI/2.jpg
    - https://i3.ytimg.com/vi/xaz5ftvZCyI/3.jpg
---
Review of B cells,  CD4+ T cells and CD8+ T cells
More free lessons at: http://www.khanacademy.org/video?v=xaz5ftvZCyI
