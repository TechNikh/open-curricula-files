---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/killing
offline_file: ""
offline_thumbnail: ""
uuid: 58e90ec7-6bb7-4077-8d62-fb83db9e6d52
updated: 1484310565
title: killing
categories:
    - Dictionary
---
killing
     adj : very funny; "a killing joke"; "sidesplitting antics" [syn: {sidesplitting}]
     n 1: an event that causes someone to die [syn: {violent death}]
     2: the act of terminating a life [syn: {kill}, {putting to
        death}]
     3: a very large profit [syn: {cleanup}]
