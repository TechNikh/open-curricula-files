---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bregman%E2%80%93Minc_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: b2b4ab89-38a8-4ea5-b0d7-13af16acec0d
updated: 1484308661
title: Bregman–Minc inequality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/310px-Perfect_matching_qtl1.svg.png
tags:
    - Statement
    - Application
    - Related statements
categories:
    - Theorems in discrete mathematics
---
In discrete mathematics, the Bregman–Minc inequality, or Bregman's theorem, allows one to estimate the permanent of a binary matrix via its row or column sums. The inequality was conjectured in 1963 by Henryk Minc and first proved in 1973 by Lev M. Bregman.[1][2] Further entropy-based proofs have been given by Alexander Schrijver and Jaikumar Radhakrishnan.[3][4] The Bregman–Minc inequality is used, for example, in graph theory to obtain upper bounds for the number of perfect matchings in a bipartite graph.
