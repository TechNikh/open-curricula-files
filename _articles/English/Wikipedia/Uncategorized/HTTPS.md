---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Https
offline_file: ""
offline_thumbnail: ""
uuid: 64b1bb59-a78d-4aa7-962f-aed14ad92de6
updated: 1484308471
title: HTTPS
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Internet2.jpg
tags:
    - Overview
    - Usage in websites
    - Browser integration
    - Security
    - Technical
    - Difference from HTTP
    - Network layers
    - Server setup
    - Acquiring certificates
    - Use as access control
categories:
    - Uncategorized
---
HTTPS (also called HTTP over TLS,[1][2] HTTP over SSL,[3] and HTTP Secure[4][5]) is a protocol for secure communication over a computer network which is widely used on the Internet. HTTPS consists of communication over Hypertext Transfer Protocol (HTTP) within a connection encrypted by Transport Layer Security, or its predecessor, Secure Sockets Layer. The main motivation for HTTPS is authentication of the visited website and protection of the privacy and integrity of the exchanged data.
