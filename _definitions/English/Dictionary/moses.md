---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moses
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484632201
title: moses
categories:
    - Dictionary
---
Moses
     n 1: (Old Testament) the Hebrew prophet who led the Israelites
          from Egypt across the Red sea on a journey known as the
          Exodus; Moses received the Ten Commandments from God on
          Mount Sinai
     2: United States painter of colorful and primitive rural scenes
        (1860-1961) [syn: {Grandma Moses}, {Anne Mary Robertson
        Moses}]
