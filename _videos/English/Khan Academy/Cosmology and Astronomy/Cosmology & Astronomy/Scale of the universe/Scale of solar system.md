---
version: 1
type: video
provider: YouTube
id: GP53b__h4ew
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Scale%20of%20solar%20system.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 36d26717-e1b4-4bf0-b135-1dad2263fd84
updated: 1486069685
title: Scale of solar system
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/GP53b__h4ew/default.jpg
    - https://i3.ytimg.com/vi/GP53b__h4ew/1.jpg
    - https://i3.ytimg.com/vi/GP53b__h4ew/2.jpg
    - https://i3.ytimg.com/vi/GP53b__h4ew/3.jpg
---
