---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/functionally
offline_file: ""
offline_thumbnail: ""
uuid: 569ba738-b603-4c5e-9679-4d06c94d1444
updated: 1484310283
title: functionally
categories:
    - Dictionary
---
functionally
     adv : with respect to function; "the two units are functionally
           interdependent"
