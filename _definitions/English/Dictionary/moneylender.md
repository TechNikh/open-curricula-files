---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moneylender
offline_file: ""
offline_thumbnail: ""
uuid: 95db6e9e-3f58-4cf2-998a-65fb421e1d97
updated: 1484310541
title: moneylender
categories:
    - Dictionary
---
moneylender
     n : someone who lends money at excessive rates of interest [syn:
          {usurer}, {loan shark}, {shylock}]
