---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Micelle
offline_file: ""
offline_thumbnail: ""
uuid: 453cb161-1c05-4ed3-a627-ae2291c20e99
updated: 1484308405
title: Micelle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Phospholipids_aqueous_solution_structures.svg.png
tags:
    - History
    - Solvation
    - Energy of formation
    - Micelle packing parameter
    - Block copolymer micelles
    - Dynamic micelles
    - Kinetically frozen micelles
    - Inverse/reverse micelles
    - Supermicelles
    - Uses
categories:
    - Uncategorized
---
A micelle (/maɪˈsɛl/) or micella (/maɪˈsɛlə/) (plural micelles or micellae, respectively) is an aggregate (or supramolecular assembly) of surfactant molecules dispersed in a liquid colloid. A typical micelle in aqueous solution forms an aggregate with the hydrophilic "head" regions in contact with surrounding solvent, sequestering the hydrophobic single-tail regions in the micelle centre. This phase is caused by the packing behavior of single-tail lipids in a bilayer. The difficulty filling all the volume of the interior of a bilayer, while accommodating the area per head group forced on the molecule by the hydration of the lipid head group, leads to the formation of the micelle. ...
