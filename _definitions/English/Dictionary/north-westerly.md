---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/north-westerly
offline_file: ""
offline_thumbnail: ""
uuid: df1bc108-78f0-4b8f-91f0-b2e2d7f345c1
updated: 1484310461
title: north-westerly
categories:
    - Dictionary
---
northwesterly
     adj 1: coming from the northwest; "northwesterly winds" [syn: {northwest}]
     2: situated in or oriented toward the northwest [syn: {northwestern},
         {northwest}]
