---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/simplified
offline_file: ""
offline_thumbnail: ""
uuid: 57b8ba49-facb-4579-ac4f-a6ef7230cdda
updated: 1484310226
title: simplified
categories:
    - Dictionary
---
simplified
     adj 1: made easy or uncomplicated
     2: reduced in complexity; "a useful if somewhat simplified
        classification system"
