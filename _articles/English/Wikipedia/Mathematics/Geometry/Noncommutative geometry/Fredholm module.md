---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fredholm_module
offline_file: ""
offline_thumbnail: ""
uuid: 387230df-ce20-44f3-bb3a-f0167cdcd977
updated: 1484309292
title: Fredholm module
categories:
    - Noncommutative geometry
---
In noncommutative geometry, a Fredholm module is a mathematical structure used to quantize the differential calculus. Such a module is, up to trivial changes, the same as the abstract elliptic operator introduced by Atiyah (1970).
