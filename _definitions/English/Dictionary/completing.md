---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/completing
offline_file: ""
offline_thumbnail: ""
uuid: b3640d09-37f0-40b7-ba55-0937dd624334
updated: 1484310445
title: completing
categories:
    - Dictionary
---
completing
     adj : acting as or providing a complement (something that
           completes the whole) [syn: {complemental}, {complementary}]
