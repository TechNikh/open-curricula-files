---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hoe
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484582161
title: hoe
categories:
    - Dictionary
---
hoe
     n : a tool with a flat blade attached at right angles to a long
         handle
     v : dig with a hoe; "He is hoeing the flower beds"
