---
version: 1
type: video
provider: YouTube
id: uOJoyAhuy0Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/The%20World%20of%20Fluid%20Mechanics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 52b46955-4154-44c0-a805-802e69c88bb5
updated: 1486069653
title: The World of Fluid Mechanics
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/uOJoyAhuy0Q/default.jpg
    - https://i3.ytimg.com/vi/uOJoyAhuy0Q/1.jpg
    - https://i3.ytimg.com/vi/uOJoyAhuy0Q/2.jpg
    - https://i3.ytimg.com/vi/uOJoyAhuy0Q/3.jpg
---
