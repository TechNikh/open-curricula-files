---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hungry
offline_file: ""
offline_thumbnail: ""
uuid: 83b053b7-8b2a-402b-827b-b931ca52a854
updated: 1484310361
title: hungry
categories:
    - Dictionary
---
hungry
     adj 1: feeling hunger; feeling a need or desire to eat food; "a
            world full of hungry people" [ant: {thirsty}]
     2: (usually followed by `for') extremely desirous; "athirst for
        knowledge"; "hungry for recognition"; "thirsty for
        informaton" [syn: {athirst(p)}, {hungry(p)}, {thirsty(p)}]
     [also: {hungriest}, {hungrier}]
