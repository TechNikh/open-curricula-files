---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/erosion
offline_file: ""
offline_thumbnail: ""
uuid: e6cb7c0a-3b94-4bfb-a89a-d4cfa310cdc1
updated: 1484310249
title: erosion
categories:
    - Dictionary
---
erosion
     n 1: (geology) the mechanical process of wearing or grinding
          something down (as by particles washing over it) [syn: {eroding},
           {eating away}, {wearing}, {wearing away}]
     2: condition in which the earth's surface is worn away by the
        action of water and wind
     3: a gradual decline of something; "after the accounting
        scandal there was an erosion of confidence in the
        auditors"
     4: erosion by chemical action [syn: {corrosion}, {corroding}]
