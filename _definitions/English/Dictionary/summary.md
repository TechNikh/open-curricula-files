---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/summary
offline_file: ""
offline_thumbnail: ""
uuid: 77d79731-d4b6-4076-81e1-5df9a8886daf
updated: 1484310611
title: summary
categories:
    - Dictionary
---
summary
     adj 1: performed speedily and without formality; "a summary
            execution"; "summary justice" [syn: {drumhead}]
     2: briefly giving the gist of something; "a short and
        compendious book"; "a compact style is brief and pithy";
        "succinct comparisons"; "a summary formulation of a
        wide-ranging subject" [syn: {compendious}, {compact}, {succinct}]
     n : a briefstatement that presents the main points in a concise
         form; "he gave a summary of the conclusions"
