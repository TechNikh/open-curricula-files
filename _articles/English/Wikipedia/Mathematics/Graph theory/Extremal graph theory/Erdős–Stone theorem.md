---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Stone_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 02ddecce-8594-4d41-9e3e-8bda488e2452
updated: 1484309409
title: Erdős–Stone theorem
tags:
    - Extremal functions of Turán graphs
    - Extremal functions of arbitrary non-bipartite graphs
    - Quantitative results
    - Notes
categories:
    - Extremal graph theory
---
In extremal graph theory, the Erdős–Stone theorem is an asymptotic result generalising Turán's theorem to bound the number of edges in an H-free graph for a non-complete graph H. It is named after Paul Erdős and Arthur Stone, who proved it in 1946,[1] and it has been described as the “fundamental theorem of extremal graph theory”.[2]
