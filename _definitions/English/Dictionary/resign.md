---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resign
offline_file: ""
offline_thumbnail: ""
uuid: 20b44305-da22-4958-9e6b-8767b3805f0e
updated: 1484310583
title: resign
categories:
    - Dictionary
---
resign
     v 1: leave (a job, post, post, or position) voluntarily; "She
          vacated the position when she got pregnant"; "The
          chairman resigned when he was found to have
          misappropriated funds" [syn: {vacate}, {renounce}, {give
          up}]
     2: give up or retire from a position; "The Secretary fo the
        Navy will leave office next month"; "The chairman resigned
        over the financial scandal" [syn: {leave office}, {quit},
        {step down}] [ant: {take office}]
     3: part with a possession or right; "I am relinquishing my
        bedroom to the long-term house guest"; "resign a claim to
        the throne" [syn: {release}, {relinquish}, {free}, ...
