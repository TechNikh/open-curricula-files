---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distance_oracle
offline_file: ""
offline_thumbnail: ""
uuid: 5542ece4-2abb-4bc9-8b1d-d4998f0ef9ae
updated: 1484308620
title: Distance oracle
tags:
    - Introduction
    - Approximate DO
    - DO for general metric spaces
    - Improvements
    - Reduction from set intersection oracle
categories:
    - Graph theory
---
