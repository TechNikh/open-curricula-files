---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dramatically
offline_file: ""
offline_thumbnail: ""
uuid: 5477b79e-78e3-4255-8572-e774c38db5ef
updated: 1484310369
title: dramatically
categories:
    - Dictionary
---
dramatically
     adv 1: in a very impressive manner; "your performance will improve
            dramatically"
     2: in a dramatic manner; "he confessed dramatically" [ant: {undramatically}]
     3: with respect to dramatic value; "the play was dramatically
        interesting, but the direction was bad"
