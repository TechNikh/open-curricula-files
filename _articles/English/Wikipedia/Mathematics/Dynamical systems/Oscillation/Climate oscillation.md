---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Climate_oscillation
offline_file: ""
offline_thumbnail: ""
uuid: 1757f204-1bff-404b-9bb0-63c2396e6d06
updated: 1484308813
title: Climate oscillation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-65_Myr_Climate_Change.png
tags:
    - Examples
    - Origins and causes
    - Effects
    - Analysis and uncertainties
    - Through geologic and historical time
categories:
    - Oscillation
---
A climate oscillation or climate cycle is any recurring cyclical oscillation within global or regional climate, and is a type of climate pattern. These fluctuations in atmospheric temperature, sea surface temperature, precipitation or other parameters can be quasi-periodic, often occurring on inter-annual, multi-annual, decadal, multidecadal, century-wide, millennial or longer timescales. They are not perfectly periodic and a Fourier analysis of the data does not give a sharp spectrum.
