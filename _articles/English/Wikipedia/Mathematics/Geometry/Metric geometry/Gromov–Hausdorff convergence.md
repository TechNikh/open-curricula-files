---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gromov%E2%80%93Hausdorff_convergence'
offline_file: ""
offline_thumbnail: ""
uuid: c8767a43-7195-4bb7-ba6f-f7f3cb8b7bb3
updated: 1484309266
title: Gromov–Hausdorff convergence
tags:
    - Gromov–Hausdorff distance
    - Some properties of Gromov–Hausdorff space
    - Pointed Gromov–Hausdorff convergence
    - Applications
categories:
    - Metric geometry
---
In mathematics, Gromov–Hausdorff convergence, named after Mikhail Gromov and Felix Hausdorff, is a notion for convergence of metric spaces which is a generalization of Hausdorff convergence.
