---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/necessary
offline_file: ""
offline_thumbnail: ""
uuid: 8ba481ca-c7c0-40e6-b12f-e4289c15349d
updated: 1484310321
title: necessary
categories:
    - Dictionary
---
necessary
     adj 1: absolutely essential [ant: {unnecessary}]
     2: unavoidably determined by prior circumstances; "the
        necessary consequences of one's actions"
     n : anything indispensable; "food and shelter are necessities of
         life"; "the essentials of the good life"; "allow farmers
         to buy their requirements under favorable conditions"; "a
         place where the requisites of water fuel and fodder can
         be obtained" [syn: {necessity}, {essential}, {requirement},
          {requisite}] [ant: {inessential}]
