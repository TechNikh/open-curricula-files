---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pink
offline_file: ""
offline_thumbnail: ""
uuid: 82436261-52ae-4854-b396-fe8cfd44a11b
updated: 1484310381
title: pink
categories:
    - Dictionary
---
pink
     adj : of a light shade of red [syn: {pinkish}]
     n 1: a light shade of red
     2: any of various flowers of plants of the genus Dianthus
        cultivated for their fragrant flowers [syn: {garden pink}]
     v 1: make light, repeated taps on a surface; "he was tapping his
          fingers on the table impatiently" [syn: {tap}, {rap}, {knock}]
     2: sound like a car engine that is firing too early; "the car
        pinged when I put in low-octane gasoline"; "The car pinked
        when the ignition was too far retarded" [syn: {ping}, {knock}]
     3: cut in a zig-zag pattern with pinking shears, in sewing
