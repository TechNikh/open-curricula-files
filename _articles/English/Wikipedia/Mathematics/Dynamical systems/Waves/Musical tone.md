---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Musical_tone
offline_file: ""
offline_thumbnail: ""
uuid: 78bb6901-5c6c-40ba-b1d4-4b0012a678a5
updated: 1484308893
title: Musical tone
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Audio_a.svg.png
tags:
    - Pure tone
    - Fourier theorem
categories:
    - Waves
---
Traditionally in Western music, a musical tone is a steady periodic sound. A musical tone is characterized by its duration, pitch, intensity (or loudness), and timbre (or quality).[1] The notes used in music can be more complex than musical tones, as they may include aperiodic aspects, such as attack transients, vibrato, and envelope modulation.
