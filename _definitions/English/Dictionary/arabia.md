---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arabia
offline_file: ""
offline_thumbnail: ""
uuid: 6d09b2a1-6052-4827-a7c7-6f590b45146c
updated: 1484310517
title: arabia
categories:
    - Dictionary
---
Arabia
     n : a peninsula between the Red Sea and the Persian Gulf;
         strategically important for its oil resources [syn: {Arabian
         Peninsula}]
