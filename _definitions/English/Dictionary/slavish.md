---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slavish
offline_file: ""
offline_thumbnail: ""
uuid: e8ee925c-0a40-464f-9ce7-b6afe42b1e46
updated: 1484310555
title: slavish
categories:
    - Dictionary
---
slavish
     adj 1: blindly imitative; "a slavish copy of the original"
     2: abjectly submissive; characteristic of a slave or servant;
        "slavish devotion to her job ruled her life"; "a slavish
        yes-man to the party bosses"- S.H.Adams; "she has become
        submissive and subservient" [syn: {subservient}, {submissive}]
