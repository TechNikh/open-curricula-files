---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saliva
offline_file: ""
offline_thumbnail: ""
uuid: 6699c83c-9c3d-4cd6-90e7-7cd3e811daaa
updated: 1484310351
title: saliva
categories:
    - Dictionary
---
saliva
     n : a clear liquid secreted into the mouth by the salivary
         glands and mucous glands of the mouth; moistens the mouth
         and starts the digestion of starches [syn: {spit}, {spittle}]
