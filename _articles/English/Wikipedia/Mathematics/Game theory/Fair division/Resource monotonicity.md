---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Resource_monotonicity
offline_file: ""
offline_thumbnail: ""
uuid: 2d51f4bc-3290-44fd-b09f-a2b13d1c507a
updated: 1484308520
title: Resource monotonicity
tags:
    - Allocation of a single continuous resource
    - Allocation of a single discrete resource
    - Allocation of two complementary resources
    - Facility location game
    - Economic growth with falling real wages
    - Bargaining
categories:
    - Fair division
---
Resource monotonicity (RM; aka aggregate monotonicity) is a principle of fair division. It says that, if there are more resources to share, then all agents should be weakly better off; no agent should lose from the increase in resources. The RM principle has been studied in various division problems.[1]:46–51
