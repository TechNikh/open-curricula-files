---
version: 1
type: video
provider: YouTube
id: RRbuz3VQ100
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Enzyme-Linked%20Immunosorbent%20Assay%20%28ELISA%29%20-%20Multi-Lingual%20Captions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: eccf6654-c4d8-4818-8ed0-f6afa274b788
updated: 1486069669
title: 'Enzyme-Linked Immunosorbent Assay (ELISA) - Multi-Lingual Captions'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/RRbuz3VQ100/default.jpg
    - https://i3.ytimg.com/vi/RRbuz3VQ100/1.jpg
    - https://i3.ytimg.com/vi/RRbuz3VQ100/2.jpg
    - https://i3.ytimg.com/vi/RRbuz3VQ100/3.jpg
---
