---
version: 1
type: video
provider: YouTube
id: Yhaa214UKvA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/The%20Apostrophe/Choosing%20between%20its%20and%20it%E2%80%99s.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0328e20f-7381-4b99-83b1-fb7303233cdc
updated: 1486069599
title: Choosing between its and it’s
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The Apostrophe
thumbnail_urls:
    - https://i3.ytimg.com/vi/Yhaa214UKvA/default.jpg
    - https://i3.ytimg.com/vi/Yhaa214UKvA/1.jpg
    - https://i3.ytimg.com/vi/Yhaa214UKvA/2.jpg
    - https://i3.ytimg.com/vi/Yhaa214UKvA/3.jpg
---
David and Paige explain the meaning of two English words that look and sound very similar but act very different: _its_ and _it's_.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/e/choosing-between-its-and-it-s

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/bonus-history-of-the-apostrophe-the-apostrophe-punctuation-khan-academy

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/apostrophes-and-plurals-the-apostrophe-punctuation-khan-academy

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
