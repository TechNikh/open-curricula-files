---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dihybrid
offline_file: ""
offline_thumbnail: ""
uuid: a2432b24-1b0e-4161-b870-ec9a031e2f8b
updated: 1484308342
title: Dihybrid cross
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Grasak.JPG
categories:
    - Uncategorized
---
Dihybrid cross is a cross between two different lines (varieties, strains) that differ in two observed traits. In the Mendelian sense, between the alleles of both these loci there is a relationship of complete dominance - recessive. In the example pictured to the right, RRYY/rryy parents result in F1 offspring that are heterozygous for both R and Y (RrYy).[1]
