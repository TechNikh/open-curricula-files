---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/three
offline_file: ""
offline_thumbnail: ""
uuid: 000ab5e1-238b-4f9a-a269-fa77e2d2c82f
updated: 1484310339
title: three
categories:
    - Dictionary
---
three
     adj : being one more than two [syn: {3}, {iii}]
     n : the cardinal number that is the sum of one and one and one
         [syn: {3}, {III}, {trio}, {threesome}, {tierce}, {leash},
          {troika}, {triad}, {trine}, {trinity}, {ternary}, {ternion},
          {triplet}, {tercet}, {terzetto}, {trey}, {deuce-ace}]
