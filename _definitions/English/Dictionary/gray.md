---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gray
offline_file: ""
offline_thumbnail: ""
uuid: 679f4d27-2080-4fee-9e07-c67c2fc6c593
updated: 1484310375
title: gray
categories:
    - Dictionary
---
gray
     adj 1: an achromatic color of any lightness between the extremes of
            black and white; "gray flannel suit"; "hair just
            turning gray" [syn: {grey}, {grayish}, {greyish}]
     2: showing characteristics of age, especially having gray or
        white hair; "whose beard with age is hoar"-Coleridge;
        "nodded his hoary head" [syn: {grey}, {gray-haired}, {grey-haired},
         {gray-headed}, {grey-headed}, {grizzly}, {hoar}, {hoary},
         {white-haired}]
     3: darkened with overcast; "a dark day"; "a dull sky"; "a gray
        rainy afternoon"; "gray clouds"; "the sky was leaden and
        thick" [syn: {dull}, {grey}, {leaden}]
     4: used to ...
