---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decoration
offline_file: ""
offline_thumbnail: ""
uuid: f7f67a47-8248-45ce-9e38-037c4c040a40
updated: 1484310236
title: decoration
categories:
    - Dictionary
---
decoration
     n 1: something used to beautify [syn: {ornament}, {ornamentation}]
     2: an award for winning a championship or commemorating some
        other event [syn: {laurel wreath}, {medal}, {medallion}, {palm},
         {ribbon}]
     3: the act of decorating something (in the hope of making it
        more attractive)
