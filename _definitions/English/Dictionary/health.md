---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/health
offline_file: ""
offline_thumbnail: ""
uuid: 7267ce2a-7738-4d3d-9fdb-b104e05c996e
updated: 1484310443
title: health
categories:
    - Dictionary
---
health
     n 1: a healthy state of wellbeing free from disease; "physicians
          should be held responsible for the health of their
          patients" [syn: {wellness}] [ant: {illness}, {illness}]
     2: the general condition of body and mind; "his delicate
        health"; "in poor health"
