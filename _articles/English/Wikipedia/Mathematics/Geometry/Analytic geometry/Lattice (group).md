---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lattice_(group)
offline_file: ""
offline_thumbnail: ""
uuid: 34f3db1e-cecb-4e31-bf64-3056b22a9a40
updated: 1484309165
title: Lattice (group)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Equilateral_Triangle_Lattice.svg.png
tags:
    - Symmetry considerations and examples
    - Dividing space according to a lattice
    - Lattice points in convex sets
    - Computing with lattices
    - 'Lattices in two dimensions: detailed discussion'
    - Lattices in three dimensions
    - Lattices in complex space
    - In Lie groups
    - Lattices in general vector-spaces
categories:
    - Analytic geometry
---
In mathematics, especially in geometry and group theory, a lattice in 
  
    
      
        
          
            R
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {R} ^{n}}
  
 is a subgroup of 
  
    
      
        
          
            R
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {R} ^{n}}
  
 which is isomorphic to 
  
    
      
        
          
            Z
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {Z} ^{n}}
  
, and which spans the real vector space 
  
    
      
        
          
            R
          
          
  ...
