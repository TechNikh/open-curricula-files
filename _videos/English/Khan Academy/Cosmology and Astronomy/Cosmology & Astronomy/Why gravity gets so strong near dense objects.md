---
version: 1
type: video
provider: YouTube
id: RpOHZc6cDIw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Why%20gravity%20gets%20so%20strong%20near%20dense%20objects.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b3b8206f-45b8-4ba8-b4e5-58c6d3d87821
updated: 1486069688
title: Why gravity gets so strong near dense objects
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/RpOHZc6cDIw/default.jpg
    - https://i3.ytimg.com/vi/RpOHZc6cDIw/1.jpg
    - https://i3.ytimg.com/vi/RpOHZc6cDIw/2.jpg
    - https://i3.ytimg.com/vi/RpOHZc6cDIw/3.jpg
---
