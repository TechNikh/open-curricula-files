---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carbonate
offline_file: ""
offline_thumbnail: ""
uuid: c598506d-0427-44c6-8e45-c9a4b4b04f55
updated: 1484310373
title: carbonate
categories:
    - Dictionary
---
carbonate
     n : a salt or ester of carbonic acid (containing the anion CO3)
     v 1: treat with carbon dioxide; "Carbonated soft drinks"
     2: turn into a carbonate
