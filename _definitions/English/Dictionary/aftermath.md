---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aftermath
offline_file: ""
offline_thumbnail: ""
uuid: e636c73f-49d1-4cde-ba40-bf3ecf06caa6
updated: 1484310587
title: aftermath
categories:
    - Dictionary
---
aftermath
     n 1: the consequences of an event (especially a catastrophic
          event); "the aftermath of war"; "in the wake of the
          accident no one knew how many had been injured" [syn: {wake},
           {backwash}]
     2: the outcome of an event especially as relative to an
        individual; "that result is of no consequence" [syn: {consequence}]
