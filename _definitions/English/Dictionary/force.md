---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/force
offline_file: ""
offline_thumbnail: ""
uuid: a56fa029-e3e1-4e8f-9a66-8c328e7af980
updated: 1484310200
title: force
categories:
    - Dictionary
---
force
     n 1: a unit that is part of some military service; "he sent
          Caesar a force of six thousand men" [syn: {military unit},
           {military force}, {military group}]
     2: one possessing or exercising power or influence or
        authority; "the mysterious presence of an evil power";
        "may the force be with you"; "the forces of evil" [syn: {power}]
     3: (physics) the influence that produces a change in a physical
        quantity; "force equals mass times acceleration"
     4: group of people willing to obey orders; "a public force is
        necessary to give security to the rights of citizens"
        [syn: {personnel}]
     5: a powerful effect or ...
