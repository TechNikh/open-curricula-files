---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/combination
offline_file: ""
offline_thumbnail: ""
uuid: 26b564cd-a16c-4a00-9d1c-48435c1994ae
updated: 1484310353
title: combination
categories:
    - Dictionary
---
combination
     n 1: a collection of things that have been combined; an
          assemblage of separate parts or qualities
     2: a coordinated sequence of chess moves
     3: a sequence of numbers or letters that opens a combination
        lock; "he forgot the combination to the safe"
     4: a group of people (often temporary) having a common purpose;
        "they were a winning combination"
     5: an alliance of people or corporations or countries for a
        special purpose (formerly to achieve some antisocial end
        but now for general political or economic purposes)
     6: the act of arranging elements into specified groups without
        regard to order
     7: the act ...
