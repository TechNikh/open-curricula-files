---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refuge
offline_file: ""
offline_thumbnail: ""
uuid: 89473b9b-9317-4fca-bb07-58099c6481e7
updated: 1484310176
title: refuge
categories:
    - Dictionary
---
refuge
     n 1: a safe place; "He ran to safety" [syn: {safety}]
     2: something or someone turned to for assistance or security;
        "his only recourse was the police"; "took refuge in lying"
        [syn: {recourse}, {resort}]
     3: a shelter from danger or hardship [syn: {sanctuary}, {asylum}]
     4: act of turning to for assistance; "have recourse to the
        courts"; "an appeal to his uncle was his last resort"
        [syn: {recourse}, {resort}]
