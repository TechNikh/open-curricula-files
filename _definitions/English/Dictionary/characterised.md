---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/characterised
offline_file: ""
offline_thumbnail: ""
uuid: b0118ed9-ee87-454f-b2d8-e2ba3b214835
updated: 1484310455
title: characterised
categories:
    - Dictionary
---
characterised
     adj : of the meaning of words or concepts; stated precisely [syn:
           {characterized}]
