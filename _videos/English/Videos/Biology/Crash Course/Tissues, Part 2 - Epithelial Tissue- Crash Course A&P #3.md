---
version: 1
type: video
provider: YouTube
id: lUe_RI_m-Vg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Tissues%2C%20Part%202%20-%20Epithelial%20Tissue-%20Crash%20Course%20A%26P%20%233.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ce4dc5ee-07bf-4c75-8989-6c331d10c66b
updated: 1486069681
title: 'Tissues, Part 2 - Epithelial Tissue- Crash Course A&P #3'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/lUe_RI_m-Vg/default.jpg
    - https://i3.ytimg.com/vi/lUe_RI_m-Vg/1.jpg
    - https://i3.ytimg.com/vi/lUe_RI_m-Vg/2.jpg
    - https://i3.ytimg.com/vi/lUe_RI_m-Vg/3.jpg
---
