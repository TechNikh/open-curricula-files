---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wavelength
offline_file: ""
offline_thumbnail: ""
uuid: 5f922654-965a-48b6-9a76-1246fadc52ad
updated: 1484310216
title: wavelength
categories:
    - Dictionary
---
wavelength
     n 1: the distance (measured in the direction of propagation)
          between two points in the same phase in consecutive
          cycles of a wave
     2: a shared orientation leading to mutual understanding; "they
        are on the same wavelength"
