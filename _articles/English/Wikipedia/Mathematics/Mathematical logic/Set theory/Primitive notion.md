---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Primitive_notion
offline_file: ""
offline_thumbnail: ""
uuid: 1bbc88ce-07d0-4694-b3ef-dd7f69645c4e
updated: 1484309509
title: Primitive notion
categories:
    - Set theory
---
In mathematics, logic, and formal systems, a primitive notion is an undefined concept. In particular, a primitive notion is not defined in terms of previously defined concepts, but is only motivated informally, usually by an appeal to intuition and everyday experience. In an axiomatic theory or other formal system, the role of a primitive notion is analogous to that of axiom. In axiomatic theories, the primitive notions are sometimes said to be "defined" by one or more axioms, but this can be misleading. Formal theories cannot dispense with primitive notions, under pain of infinite regress.
