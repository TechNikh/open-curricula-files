---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/educationally
offline_file: ""
offline_thumbnail: ""
uuid: e49e2524-c4d1-4dd8-af8c-9db991f3f6d6
updated: 1484310183
title: educationally
categories:
    - Dictionary
---
educationally
     adv : in an educational manner; "the assistant masters formed a
           committee of their own to consider what could be done
           educationally for the town"
