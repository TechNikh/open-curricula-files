---
version: 1
type: article
id: https://en.wikipedia.org/wiki/O
offline_file: ""
offline_thumbnail: ""
uuid: e9f180cb-5350-4a6f-8870-73ed381d9a74
updated: 1484308335
title: O
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-O_cursiva.gif
tags:
    - History
    - Use in writing systems
    - english
    - Other languages
    - Other systems
    - Related characters
    - Descendants and related characters in the Latin alphabet
    - Derived signs, symbols and abbreviations
    - Ancestors and siblings in other alphabets
    - Computing codes
    - Other representations
categories:
    - Uncategorized
---
