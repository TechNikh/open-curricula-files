---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discouraged
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484588041
title: discouraged
categories:
    - Dictionary
---
discouraged
     adj 1: made less hopeful or enthusiastic; "desperate demoralized
            people looking for work"; "felt discouraged by the
            magnitude of the problem"; "the disheartened
            instructor tried vainly to arouse their interest"
            [syn: {demoralized}, {demoralised}, {disheartened}]
     2: lacking in resolution; "the accident left others discouraged
        about going there"
