---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/R%C3%B6ssler_attractor'
offline_file: ""
offline_thumbnail: ""
uuid: fbaadfa1-a131-4bdd-a3b6-70d6cbbc796f
updated: 1484308722
title: Rössler attractor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Roessler_attractor.png
tags:
    - Definition
    - An analysis
    - Fixed points
    - Eigenvalues and eigenvectors
    - Poincaré map
    - Mapping local maxima
    - Variation of parameters
    - Varying a
    - Varying b
    - Varying c
    - Links to other topics
categories:
    - Chaotic maps
---
The Rössler attractor /ˈrɒslər/ is the attractor for the Rössler system, a system of three non-linear ordinary differential equations originally studied by Otto Rössler.[1][2] These differential equations define a continuous-time dynamical system that exhibits chaotic dynamics associated with the fractal properties of the attractor.[3]
