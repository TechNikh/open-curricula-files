---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vary
offline_file: ""
offline_thumbnail: ""
uuid: 7838235d-c8d3-403f-85fd-e0687af1b862
updated: 1484310305
title: vary
categories:
    - Dictionary
---
vary
     v 1: make or become different in some particular way, without
          permanently losing one's or its former characteristics
          or essence; "her mood changes in accordance with the
          weather"; "The supermarket's selection of vegetables
          varies according to the season" [syn: {change}, {alter}]
     2: be at variance with; be out of line with [syn: {deviate}, {diverge},
         {depart}] [ant: {conform}]
     3: be subject to change in accordance with a variable; "Prices
        vary"; "His moods vary depending on the weather"
     4: make something more diverse and varied; "Vary the menu"
        [syn: {variegate}, {motley}]
     [also: {varied}]
