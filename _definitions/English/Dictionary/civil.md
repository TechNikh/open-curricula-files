---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/civil
offline_file: ""
offline_thumbnail: ""
uuid: f5e0d84e-6a8a-43ff-8708-f69a14f86e5e
updated: 1484310525
title: civil
categories:
    - Dictionary
---
civil
     adj 1: applying to ordinary citizens; "civil law"; "civil
            authorities"
     2: not rude; marked by satisfactory (or especially minimal)
        adherence to social usages and sufficient but not
        noteworthy consideration for others; "even if he didn't
        like them he should have been civil"- W.S. Maugham [syn: {polite}]
        [ant: {uncivil}]
     3: of or occurring within the state or between or among
        citizens of the state; "civil affairs"; "civil strife";
        "civil disobediece"; "civil branches of government"
     4: of or relating to or befitting citizens as individuals;
        "civil rights"; "civil liberty"; "civic duties"; "civic
      ...
