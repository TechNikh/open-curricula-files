---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slovenly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414221
title: slovenly
categories:
    - Dictionary
---
slovenly
     adj : negligent of neatness especially in dress and person;
           habitually dirty and unkempt; "filled the door with her
           frowzy bulk"; "frowzy white hair"; "slovenly
           appearance" [syn: {frowsy}, {frowzy}]
