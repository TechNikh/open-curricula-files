---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Epithelial_cells
offline_file: ""
offline_thumbnail: ""
uuid: 7a9b300f-79d2-4b13-99a8-0541c4ac8264
updated: 1484308368
title: Epithelium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Illu_epithelium.jpg
tags:
    - Classification
    - Simple epithelium
    - Stratified epithelium
    - Structure
    - Location
    - Basement membrane
    - Cell junctions
    - Development
    - Function
    - Glandular tissue
    - Sensing the extracellular environment
    - Clinical significance
    - Etymology and pronunciation
    - Additional images
    - Bibliography
categories:
    - Uncategorized
---
Epithelium (epi- + thele + -ium) is one of the four basic types of animal tissue. The other three types are connective tissue, muscle tissue and nervous tissue. Epithelial tissues line the cavities and surfaces of blood vessels and organs throughout the body.
