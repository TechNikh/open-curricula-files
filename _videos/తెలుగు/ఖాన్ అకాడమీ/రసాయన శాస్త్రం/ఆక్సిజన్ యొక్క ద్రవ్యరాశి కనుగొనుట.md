---
version: 1
type: video
provider: YouTube
id: kTe04UibmzI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 948f46bf-c66e-4501-932c-45ce18a27d4c
updated: 1486069603
title: >
    ఆక్సిజన్ యొక్క
    ద్రవ్యరాశి కనుగొనుట
tags:
    - telugu
    - chemistry
    - Ideal gas
    - Equation
categories:
    - రసాయన శాస్త్రం
thumbnail_urls:
    - https://i3.ytimg.com/vi/kTe04UibmzI/default.jpg
    - https://i3.ytimg.com/vi/kTe04UibmzI/1.jpg
    - https://i3.ytimg.com/vi/kTe04UibmzI/2.jpg
    - https://i3.ytimg.com/vi/kTe04UibmzI/3.jpg
---
Translated from original video
Title - Ideal Gas Equation Example 3
URL -http://www.khanacademy.org/science/chemistry/ideal-gas-laws/v/ideal-gas-equation-example-3
