---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Li%C3%A9nard%E2%80%93Chipart_criterion'
offline_file: ""
offline_thumbnail: ""
uuid: 4df0bfdd-7808-48f5-91e9-ae4c06dbb58f
updated: 1484308825
title: Liénard–Chipart criterion
categories:
    - Stability theory
---
In control system theory, the Liénard–Chipart criterion is a stability criterion modified from Routh–Hurwitz stability criterion, proposed by A. Liénard and M. H. Chipart.[1] This criterion has a computational advantage over Routh–Hurwitz criterion because they involve only about half the number of determinant computations.[2]
