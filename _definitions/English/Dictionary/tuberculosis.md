---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tuberculosis
offline_file: ""
offline_thumbnail: ""
uuid: ca139253-2b5a-4e57-bdd0-6e99f26d8ef9
updated: 1484310535
title: tuberculosis
categories:
    - Dictionary
---
tuberculosis
     n : infection transmitted by inhalation or ingestion of tubercle
         bacilli and manifested in fever and small lesions
         (usually in the lungs but in various other parts of the
         body in acute stages) [syn: {TB}, {T.B.}]
