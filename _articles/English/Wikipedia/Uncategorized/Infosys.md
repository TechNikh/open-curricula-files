---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Infosys
offline_file: ""
offline_thumbnail: ""
uuid: 600b55f8-ffd5-4b27-bc80-871a85640f89
updated: 1484308460
title: Infosys
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Infosys_India.JPG
tags:
    - History
    - Operations
    - Products and services
    - Acquisitions
    - Initiatives
    - Infosys Foundation
    - Academic Entente
    - Infosys Labs
    - Infosys Prize
    - Employees
categories:
    - Uncategorized
---
Infosys Limited (formerly Infosys Technologies Limited) is an Indian multinational corporation that provides business consulting, information technology and outsourcing services. It has the main headquarter in Bengaluru, Karnataka.[3]
