---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tameness_theorem
offline_file: ""
offline_thumbnail: ""
uuid: e1638104-dafe-4a75-ba26-3375b355e8d2
updated: 1484309123
title: Tameness theorem
categories:
    - Theorems in geometry
---
In mathematics, the tameness theorem states that every complete hyperbolic 3-manifold with finitely generated fundamental group is topologically tame, in other words homeomorphic to the interior of a compact 3-manifold.
