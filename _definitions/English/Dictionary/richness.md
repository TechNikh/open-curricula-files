---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/richness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484641681
title: richness
categories:
    - Dictionary
---
richness
     n 1: the property of being extremely abundant [syn: {profusion},
          {profuseness}, {cornucopia}]
     2: abundant wealth [syn: {affluence}]
     3: the property of a sound that has a rich and pleasing timbre
        [syn: {fullness}, {mellowness}]
     4: the property of producing abundantly and sustaining growth;
        "he praised the richness of the soil" [syn: {prolificacy},
         {fertility}]
