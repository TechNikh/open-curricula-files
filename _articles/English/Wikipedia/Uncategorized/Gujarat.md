---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gujarat
offline_file: ""
offline_thumbnail: ""
uuid: cda2e470-ad52-4725-a4e4-e5ab672c13ff
updated: 1484308426
title: Gujarat
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rani_ki_vav_02.jpg
tags:
    - Etymology
    - History
    - Ancient history
    - Medieval history
    - Rajput Period
    - Islamic Conquests 1197–1614 AD
    - The Sultanate of Gujarat and the merchants
    - Gujarat and the Mughal Empire
    - Maratha Empire
    - European Imperialism 1614–1947 AD
    - Post independence
    - geography
    - Rann of Kutch
    - Demographics
    - religion
    - language
    - Governance and administration
    - Economy
    - Infrastructure
    - Industrial growth
    - Power plants
    - Agriculture
    - culture
    - Literature
    - Cuisine
    - Cinema
    - Music
    - Festivals
    - Diffusion of culture
    - Flora and Fauna
    - Dinosaur Park Balasinor
    - Tourism
    - Transport
    - air
    - International airports
    - >
        Domestic airports operated by the Airports Authority of
        India (AAI)
    - State-operated airports
    - Rail
    - Sea
    - Road
    - Education and research
    - Research
    - Notable individuals
categories:
    - Uncategorized
---
Gujarat (/ˌɡʊdʒəˈrɑːt/ Gujǎrāt  [ˈɡudʒ(ə)ɾaːt̪] ( listen)) is a state in Western India, sometimes referred to as the "Jewel of Western India".[4] It has an area of 196,024 km2 (75,685 sq mi) with a coastline of 1,600 km (990 mi), most of which lies on the Kathiawar peninsula, and a population in excess of 60 million. The state is bordered by Rajasthan to the north, Maharashtra to the south, Madhya Pradesh to the east, and the Arabian Sea and the Pakistani province of Sindh to the west. Its capital city is Gandhinagar, while its largest city is Ahmedabad. Gujarat is home to the Gujarati-speaking people of India.
