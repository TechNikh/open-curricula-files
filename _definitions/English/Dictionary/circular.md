---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/circular
offline_file: ""
offline_thumbnail: ""
uuid: 024e8b35-d362-4541-9122-fe94b038d726
updated: 1484310337
title: circular
categories:
    - Dictionary
---
circular
     adj 1: having a circular shape [syn: {round}] [ant: {square}]
     2: shaped like a ring [syn: {annular}, {annulate}, {annulated},
         {circinate}, {ringed}, {ring-shaped}, {doughnut-shaped}]
     3: marked by or moving in a circle [syn: {rotary}]
     n : an advertisement (usually printed on a page or in a leaflet)
         intended for wide distribution; "he mailed the circular
         to all subscribers" [syn: {handbill}, {bill}, {broadside},
          {broadsheet}, {flier}, {flyer}, {throwaway}]
