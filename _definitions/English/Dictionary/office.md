---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/office
offline_file: ""
offline_thumbnail: ""
uuid: 9bf1ca17-b69a-4231-b4a9-046f8f4866b0
updated: 1484310455
title: office
categories:
    - Dictionary
---
office
     n 1: place of business where professional or clerical duties are
          performed; "he rented an office in the new building"
          [syn: {business office}]
     2: an administrative unit of government; "the Central
        Intelligence Agency"; "the Census Bureau"; "Office of
        Management and Budget"; "Tennessee Valley Authority" [syn:
         {agency}, {federal agency}, {government agency}, {bureau},
         {authority}]
     3: the actions and activities assigned to or required or
        expected of a person or group; "the function of a
        teacher"; "the government must do its part"; "play its
        role" [syn: {function}, {part}, {role}]
     4: (of a ...
