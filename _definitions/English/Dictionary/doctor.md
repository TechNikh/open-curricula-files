---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/doctor
offline_file: ""
offline_thumbnail: ""
uuid: 4c302bc9-3315-44ff-97dd-de02b0659906
updated: 1484310212
title: doctor
categories:
    - Dictionary
---
doctor
     n 1: a licensed medical practitioner; "I felt so bad I went to
          see my doctor" [syn: {doc}, {physician}, {MD}, {Dr.}, {medico}]
     2: (Roman Catholic Church) a title conferred on 33 saints who
        distinguished themselves through the othodoxy of their
        theological teaching; "the Doctors of the Church greatly
        influenced Christian thought down to the late Middle Ages"
        [syn: {Doctor of the Church}]
     3: children take the roles of doctor or patient or nurse and
        pretend they are at the doctor's office; "the children
        explored each other's bodies by playing the game of
        doctor"
     4: a person who holds Ph.D. degree from ...
