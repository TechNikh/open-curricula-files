---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cooperative
offline_file: ""
offline_thumbnail: ""
uuid: c6d199e6-4dee-40df-823b-53624ec722bd
updated: 1484310484
title: cooperative
categories:
    - Dictionary
---
cooperative
     adj 1: involving the joint activity of two or more; "the attack was
            met by the combined strength of two divisions";
            "concerted action"; "the conjunct influence of fire
            and strong dring"; "the conjunctive focus of political
            opposition"; "a cooperative effort"; "a united
            effort"; "joint military activities" [syn: {combined},
             {concerted}, {conjunct}, {conjunctive}, {united}]
     2: done with or working with others for a common purpose or
        benefit; "a cooperative effort" [ant: {uncooperative}]
     3: willing to adjust to differences in order to obtain
        agreement [syn: {accommodative}]
     ...
