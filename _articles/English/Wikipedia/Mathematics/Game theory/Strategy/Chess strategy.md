---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chess_strategy
offline_file: ""
offline_thumbnail: ""
uuid: 06717aad-8195-45c6-9c67-49b69ee6a7a9
updated: 1484309063
title: Chess strategy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Nuvola_apps_package_games_strategy_5.png
tags:
    - Basic concepts of board evaluation
    - space
    - Control of the center
    - Initiative
    - Defending pieces
    - Exchanging pieces
    - Specific pieces
    - Pawns
    - Knights
    - Bishops
    - Rooks
    - Queen
    - king
    - Considerations for a successful long term deployment
    - Strategy and tactics
    - Opening
    - Middlegame
    - Endgame
    - Quotes
categories:
    - Strategy
---
Chess strategy is the aspect of chess playing concerned with evaluation of chess positions and setting of goals and long-term plans for future play. While evaluating a position strategically, a player must take into account such factors as the relative value of the pieces on the board, pawn structure, king safety, position of pieces, and control of key squares and groups of squares (e.g. diagonals, open files, individual squares). Chess strategy is distinguished from chess tactics, which is the aspect of role playing concerned with the move-by-move setting up of threats and defenses. Some authors distinguish static strategic imbalances (e.g. having more valuable pieces or better pawn ...
