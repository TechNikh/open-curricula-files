---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rational_point
offline_file: ""
offline_thumbnail: ""
uuid: 2274f18c-b8e7-49b3-906a-e5ed400493a5
updated: 1484309130
title: Rational point
tags:
    - Rational or K-rational points on algebraic varieties
    - Examples
    - Example 1
    - Example 2
    - Example 3
    - Rational points of schemes
categories:
    - Algebraic geometry
---
In number theory, a rational point is a point in space each of whose coordinates are rational; that is, the coordinates of the point are elements of the field of rational numbers, as well as being elements of a larger field that contains the rational numbers, such as the real numbers or the complex numbers.
