---
version: 1
type: video
provider: YouTube
id: cqYDaeIuCAw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a1313422-f793-4d0d-8a61-94b93838a4ec
updated: 1486069640
title: 'Curved mirrors - physics experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/cqYDaeIuCAw/default.jpg
    - https://i3.ytimg.com/vi/cqYDaeIuCAw/1.jpg
    - https://i3.ytimg.com/vi/cqYDaeIuCAw/2.jpg
    - https://i3.ytimg.com/vi/cqYDaeIuCAw/3.jpg
---
