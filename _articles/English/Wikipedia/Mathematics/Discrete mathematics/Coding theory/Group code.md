---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Group_code
offline_file: ""
offline_thumbnail: ""
uuid: fb165d8d-4637-4bed-b51b-42d6a2f5111b
updated: 1484308539
title: Group code
categories:
    - Coding theory
---
In computer science, group codes are a type of code. Group codes consist of 
  
    
      
        n
      
    
    {\displaystyle n}
  
 linear block codes which are subgroups of 
  
    
      
        
          G
          
            n
          
        
      
    
    {\displaystyle G^{n}}
  
, where 
  
    
      
        G
      
    
    {\displaystyle G}
  
 is a finite abelian group.
