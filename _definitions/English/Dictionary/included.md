---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/included
offline_file: ""
offline_thumbnail: ""
uuid: 0581d05a-1daa-4464-b94f-a46324bb320c
updated: 1484310401
title: included
categories:
    - Dictionary
---
included
     adj : enclosed in the same envelope or package; "the included
           check"
