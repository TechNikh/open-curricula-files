---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Quantum_heat_engines_and_refrigerators
offline_file: ""
offline_thumbnail: ""
uuid: f67dbc7b-2279-485a-b90b-46fdabbebdbb
updated: 1484308855
title: Quantum heat engines and refrigerators
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/page1-220px-Three-level-amp.pdf.jpg
tags:
    - The 3-level amplifier as a quantum heat engine
    - Types of quantum heat engines and refrigerators
    - The quantum reciprocating heat engine and refrigerator
    - Continuous quantum engines
    - >
        Equivalence of reciprocating and continuous heat machines in
        the quantum regime
    - Heat engines and open quantum systems
    - The quantum absorption refrigerator
    - Quantum refrigerators and the third law of thermodynamics
categories:
    - Thermodynamics
---
A quantum heat engine is a device that generates power from the heat flow between hot and cold reservoirs. The operation mechanism of the engine can be described by the laws of quantum mechanics. The first realization of a quantum heat engine was pointed out by Geusic, Schultz-DuBois and Scoville.,[1] showing the connection of efficiency of the Carnot engine and the 3-level maser. Quantum refrigerators share the structure of quantum heat engines with the purpose of pumping heat from a cold to a hot bath consuming power first suggested by Geusic, Bois and Scoville .[2] When the power is supplied by a laser the process is termed optical pumping or laser cooling, suggested by Weinland and ...
