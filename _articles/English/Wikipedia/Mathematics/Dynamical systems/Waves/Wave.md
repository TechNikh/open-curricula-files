---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wave
offline_file: ""
offline_thumbnail: ""
uuid: 55c61469-756d-44f2-97ab-5fe84db87e6e
updated: 1484308889
title: Wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-2006-01-14_Surface_waves.jpg
tags:
    - General features
    - Mathematical description of one-dimensional waves
    - Wave equation
    - Wave forms
    - Amplitude and modulation
    - Phase velocity and group velocity
    - Sinusoidal waves
    - Plane waves
    - Standing waves
    - Physical properties
    - Transmission and media
    - Absorption
    - Reflection
    - Interference
    - Refraction
    - Diffraction
    - Polarization
    - Dispersion
    - Mechanical waves
    - Waves on strings
    - Acoustic waves
    - Water waves
    - Seismic waves
    - Shock waves
    - Other
    - Electromagnetic waves
    - Quantum mechanical waves
    - Schrödinger equation
    - Dirac equation
    - de Broglie waves
    - Gravity waves
    - Gravitational waves
    - WKB method
    - Waves in general
    - Parameters
    - Waveforms
    - Electromagnetic waves
    - In fluids
    - In quantum mechanics
    - In relativity
    - Other specific types of waves
    - Related topics
    - Sources
categories:
    - Waves
---
In physics, a wave is an oscillation accompanied by a transfer of energy that travels through a medium (space or mass). Frequency refers to the addition of time. Wave motion transfers energy from one point to another, which displace particles of the transmission medium–that is, with little or no associated mass transport. Waves consist, instead, of oscillations or vibrations (of a physical quantity), around almost fixed locations.
