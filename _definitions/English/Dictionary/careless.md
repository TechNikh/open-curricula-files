---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/careless
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415301
title: careless
categories:
    - Dictionary
---
careless
     adj 1: marked by lack of attention or consideration or forethought
            or thoroughness; not careful; "careless about her
            clothes"; "forgotten by some careless person"; "a
            careless housekeeper"; "careless proofreading"; "it
            was a careless mistake"; "hurt by a careless remark"
            [ant: {careful}]
     2: effortless and unstudied; "an impression of careless
        elegance"; "danced with careless grace"
     3: (usually followed by `of') without due thought or
        consideration; "careless of the consequences"; "the
        proverbial grasshopper--thoughtless of tomorrow";
        "crushing the blooms with regardless tread" ...
