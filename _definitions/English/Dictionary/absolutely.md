---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/absolutely
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484411641
title: absolutely
categories:
    - Dictionary
---
absolutely
     adv 1: completely and without qualification; used informally as
            intensifiers; "an absolutely magnificent painting"; "a
            perfectly idiotic idea"; "you're perfectly right";
            "utterly miserable"; "you can be dead sure of my
            innocence"; "was dead tired"; "dead right" [syn: {perfectly},
             {utterly}, {dead}]
     2: totally and definitely; without question; "we are absolutely
        opposed to the idea"; "he forced himself to lie absolutely
        still"; "iron is absolutely necessary"
