---
title: Exchange of gases in the lungs
youtube_id: WaLswydbVkM
tags:
    - SSC
    - Biology
    - Respiration
    - Animation
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
