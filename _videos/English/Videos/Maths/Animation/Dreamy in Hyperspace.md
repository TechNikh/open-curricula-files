---
version: 1
type: video
provider: YouTube
id: H2oYRg6aEdM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9a3a5dda-fe85-45eb-a7ee-47fee7507fd4
updated: 1486069648
title: Dreamy in Hyperspace
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/H2oYRg6aEdM/default.jpg
    - https://i3.ytimg.com/vi/H2oYRg6aEdM/1.jpg
    - https://i3.ytimg.com/vi/H2oYRg6aEdM/2.jpg
    - https://i3.ytimg.com/vi/H2oYRg6aEdM/3.jpg
---
