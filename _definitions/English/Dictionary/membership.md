---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/membership
offline_file: ""
offline_thumbnail: ""
uuid: aeea1c2d-562d-4ec1-b9aa-896601d3c2be
updated: 1484310583
title: membership
categories:
    - Dictionary
---
membership
     n 1: the body of members of an organization or group; "they
          polled their membership"; "they found dissension in
          their own ranks"; "he joined the ranks of the
          unemployed" [syn: {rank}]
     2: the state of being a member
