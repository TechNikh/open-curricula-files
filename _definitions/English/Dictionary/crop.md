---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crop
offline_file: ""
offline_thumbnail: ""
uuid: caa1245c-7fc0-4fbd-8b98-f94544f5fa1f
updated: 1484310301
title: crop
categories:
    - Dictionary
---
crop
     n 1: the yield from plants in a single growing season [syn: {harvest}]
     2: a collection of people or things appearing together; "the
        annual crop of students brings a new crop of ideas"
     3: the output of something in a season; "the latest crop of
        fashions is about to hit the stores"
     4: the stock or handle of a whip
     5: a pouch in many birds and some lower animals that resembles
        a stomach for storage and preliminary maceration of food
        [syn: {craw}]
     v 1: cut short; "She wanted her hair cropped short"
     2: prepare for crops; "Work the soil"; "cultivate the land"
        [syn: {cultivate}, {work}]
     3: yield crops; "This land ...
