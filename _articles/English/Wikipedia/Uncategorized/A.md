---
version: 1
type: article
id: https://en.wikipedia.org/wiki/A
offline_file: ""
offline_thumbnail: ""
uuid: 226820b7-17ae-4afd-956d-232ec965b19f
updated: 1484308413
title: A
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-A_cursiva.gif
tags:
    - History
    - Typographic variants
    - Use in writing systems
    - english
    - Other languages
    - Other systems
    - Other uses
    - Related characters
    - Descendants and related characters in the Latin alphabet
    - Derived signs, symbols and abbreviations
    - Ancestors and siblings in other alphabets
    - Computing codes
    - Other representations
    - Notes
    - Footnotes
categories:
    - Uncategorized
---
A (named /ˈeɪ/, plural As, A's, as, a's or aes[nb 1]) is the first letter and the first vowel in the ISO basic Latin alphabet.[1] It is similar to the Ancient Greek letter alpha, from which it derives.[2] The upper-case version consists of the two slanting sides of a triangle, crossed in the middle by a horizontal bar. The lower-case version can be written in two forms: the double-storey a and single-storey ɑ. The latter is commonly used in handwriting and fonts based on it, especially fonts intended to be read by children, and is also found in italic type.
