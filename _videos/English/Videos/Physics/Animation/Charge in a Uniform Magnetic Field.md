---
version: 1
type: video
provider: YouTube
id: -36bkOzmWa8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cdddd406-7144-4079-aab3-ceb3829b7b48
updated: 1486069652
title: Charge in a Uniform Magnetic Field
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/-36bkOzmWa8/default.jpg
    - https://i3.ytimg.com/vi/-36bkOzmWa8/1.jpg
    - https://i3.ytimg.com/vi/-36bkOzmWa8/2.jpg
    - https://i3.ytimg.com/vi/-36bkOzmWa8/3.jpg
---
