---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/he
offline_file: ""
offline_thumbnail: ""
uuid: b536fa90-ec8d-42c5-b017-541b455b865a
updated: 1484310315
title: he
categories:
    - Dictionary
---
He
     n 1: a very light colorless element that is one of the six inert
          gasses; the most difficult gas to liquefy; occurs in
          economically extractable amounts in certain natural
          gases (as those found in Texas and Kansas) [syn: {helium},
           {atomic number 2}]
     2: the 5th letter of the Hebrew alphabet
