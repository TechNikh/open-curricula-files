---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/opposed
offline_file: ""
offline_thumbnail: ""
uuid: cdd157c3-207c-4e4e-844f-ff443ee4153f
updated: 1484310531
title: opposed
categories:
    - Dictionary
---
opposed
     adj 1: in opposition to (a policy or attitude etc.); "an opposing
            vote" [syn: {opposing}]
     2: being in opposition or having an opponent; "two bitterly
        opposed schools of thought" [ant: {unopposed}]
