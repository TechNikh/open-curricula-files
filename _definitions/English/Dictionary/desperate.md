---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/desperate
offline_file: ""
offline_thumbnail: ""
uuid: fb58cb94-5524-48c2-9474-ad7643390936
updated: 1484310581
title: desperate
categories:
    - Dictionary
---
desperate
     adj 1: arising from or marked by despair or loss of hope; "a
            despairing view of the world situation"; "the last
            despairing plea of the condemned criminal"; "a
            desperate cry for help"; "helpless and desperate--as
            if at the end of his tether"; "her desperate screams"
            [syn: {despairing}]
     2: desperately determined; "do-or-die revolutionaries"; "a
        do-or-die conflict" [syn: {do-or-die(a)}]
     3: (of persons) dangerously reckless or violent as from urgency
        or despair; "a desperate criminal"; "taken hostage of
        desperate men"
     4: showing extreme courage; especially of actions courageously
   ...
