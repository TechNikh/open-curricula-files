---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sensible_heat
offline_file: ""
offline_thumbnail: ""
uuid: 261255e3-0356-4eff-afb2-e2ceae832fb5
updated: 1484308857
title: Sensible heat
categories:
    - Thermodynamics
---
Sensible heat is heat exchanged by a body or thermodynamic system in which the exchange of heat changes the temperature of the body or system, and some macroscopic variables of the body or system, but leaves unchanged certain other macroscopic variables of the body or system, such as volume or pressure.[1][2][3][4]
