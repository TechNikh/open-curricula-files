---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strike
offline_file: ""
offline_thumbnail: ""
uuid: 16f8978b-9ecb-4935-a485-b2cc641efd79
updated: 1484310228
title: strike
categories:
    - Dictionary
---
strike
     n 1: a group's refusal to work in protest against low pay or bad
          work conditions; "the strike lasted more than a month
          before it was settled" [syn: {work stoppage}]
     2: an attack that is intended to seize or inflict damage on or
        destroy an objective; "the strike was scheduled to begin
        at dawn"
     3: a pitch that is in the strike zone and that the batter does
        not hit; "this pitcher throws more strikes than balls"
     4: a gentle blow [syn: {rap}, {tap}]
     5: a score in tenpins: knocking down all ten with the first
        ball; "he finished with three strikes in the tenth frame"
        [syn: {ten-strike}]
     6: a ...
