---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anders_Johan_Lexell
offline_file: ""
offline_thumbnail: ""
uuid: f4fa3a4e-d981-41c0-ba02-05d90751ead5
updated: 1484309090
title: Anders Johan Lexell
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/lossy-page1-220px-Lexell_-_Disquisitio_de_investiganda_vera_quantitate_parallaxeos_solis_ex_transitus_veneris_ante_discum_solis_anno_1769%252C_1772_-_726057_F.tif.jpg'
tags:
    - life
    - Early years
    - St. Petersburg
    - Foreign trip
    - Last years
    - Contribution to science
    - Differential equations
    - Polygonometry
    - Celestial mechanics and astronomy
categories:
    - Geometers
---
Anders Johan Lexell (24 December 1740 – 11 December [O.S. 30 November] 1784) was a Finnish-Swedish astronomer, mathematician, and physicist who spent most of his life in Russia, where he is known as Andrei Ivanovich Leksel (Андрей Иванович Лексель).
