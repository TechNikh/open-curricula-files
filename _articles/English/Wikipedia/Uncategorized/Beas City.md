---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Beas
offline_file: ""
offline_thumbnail: ""
uuid: 98f8bc54-8f24-4ca0-aa57-96908cee3a82
updated: 1484308419
title: Beas City
tags:
    - geography
    - Radha Soami
    - See Also
categories:
    - Uncategorized
---
Beas (Punjabi: ਬਿਆਸ) is a riverfront town in the Amritsar district of the Majha region of the Eastern Punjab (India). Beas lies on the banks of the Beas River.
