---
version: 1
type: video
provider: YouTube
id: 2TAO_DXeWv8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 41313471-0f12-4723-99d2-e347d6bfde1f
updated: 1486069623
title: 'Math Trick- Multiply Numbers Close To Each Other In Your Head'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/2TAO_DXeWv8/default.jpg
    - https://i3.ytimg.com/vi/2TAO_DXeWv8/1.jpg
    - https://i3.ytimg.com/vi/2TAO_DXeWv8/2.jpg
    - https://i3.ytimg.com/vi/2TAO_DXeWv8/3.jpg
---
