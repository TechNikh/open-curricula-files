---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abdominal
offline_file: ""
offline_thumbnail: ""
uuid: 068b60be-fd31-4c55-8f25-bd924d49dae9
updated: 1484310315
title: abdominal
categories:
    - Dictionary
---
abdominal
     adj : of or relating to or near the abdomen; "abdominal muscles"
     n : the muscles of the abdomen [syn: {abdominal muscle}, {ab}]
