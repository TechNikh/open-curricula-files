---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/report
offline_file: ""
offline_thumbnail: ""
uuid: f716110e-0171-4eb7-bbb8-b8fc6a1580d1
updated: 1484310226
title: report
categories:
    - Dictionary
---
report
     n 1: a written document describing the findings of some
          individual or group; "this accords with the recent study
          by Hill and Dale" [syn: {study}, {written report}]
     2: a short account of the news; "the report of his speech";
        "the story was on the 11 o'clock news"; "the account of
        his speech that was given on the evening news made the
        governor furious" [syn: {news report}, {story}, {account},
         {write up}]
     3: the act of informing by verbal report; "he heard reports
        that they were causing trouble"; "by all accounts they
        were a happy couple" [syn: {account}]
     4: a sharp explosive sound (especially the ...
