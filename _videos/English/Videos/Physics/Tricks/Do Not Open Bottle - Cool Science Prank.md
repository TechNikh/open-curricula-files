---
version: 1
type: video
provider: YouTube
id: nIBTrWcWik0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d7d2f0fc-a42d-4692-91b1-0e4e1fb51d00
updated: 1486069660
title: 'Do Not Open Bottle - Cool Science Prank'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/nIBTrWcWik0/default.jpg
    - https://i3.ytimg.com/vi/nIBTrWcWik0/1.jpg
    - https://i3.ytimg.com/vi/nIBTrWcWik0/2.jpg
    - https://i3.ytimg.com/vi/nIBTrWcWik0/3.jpg
---
