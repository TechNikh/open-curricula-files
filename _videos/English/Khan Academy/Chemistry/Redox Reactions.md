---
version: 1
type: video
provider: YouTube
id: yp60-oVxrT4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Redox%20Reactions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3b3cf251-5369-4a78-8975-428686ecd6a5
updated: 1486069585
title: Redox Reactions
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/yp60-oVxrT4/default.jpg
    - https://i3.ytimg.com/vi/yp60-oVxrT4/1.jpg
    - https://i3.ytimg.com/vi/yp60-oVxrT4/2.jpg
    - https://i3.ytimg.com/vi/yp60-oVxrT4/3.jpg
---
Oxidation reduction (or redox) reactions
