---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oor
offline_file: ""
offline_thumbnail: ""
uuid: 6306e21d-490c-4c39-8bc1-8cbfe2ef3c94
updated: 1484308403
title: OOR
categories:
    - Uncategorized
---
OOR is the oldest currently published music magazine in the Netherlands. Oor is the Dutch word for ear. Until 1984 it was published as Muziekkrant OOR.
