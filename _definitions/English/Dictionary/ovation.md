---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ovation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484526121
title: ovation
categories:
    - Dictionary
---
ovation
     n : enthusiastic recognition (especially one accompanied by loud
         applause) [syn: {standing ovation}]
