---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Half-transitive_graph
offline_file: ""
offline_thumbnail: ""
uuid: 8fc617d3-d7e1-4c8d-a9dc-55fd47ca213d
updated: 1484309376
title: Half-transitive graph
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory, a half-transitive graph is a graph that is both vertex-transitive and edge-transitive, but not symmetric.[1] In other words, a graph is half-transitive if its automorphism group acts transitively upon both its vertices and its edges, but not on ordered pairs of linked vertices.
