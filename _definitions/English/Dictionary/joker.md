---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/joker
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484362921
title: joker
categories:
    - Dictionary
---
joker
     n 1: a person who enjoys telling or playing jokes [syn: {jokester}]
     2: a person who does something thoughtless or annoying; "some
        joker is blocking the driveway" [syn: {turkey}]
