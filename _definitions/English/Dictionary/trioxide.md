---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trioxide
offline_file: ""
offline_thumbnail: ""
uuid: 1e41e988-a35c-4f31-af48-d128cb91925f
updated: 1484310373
title: trioxide
categories:
    - Dictionary
---
trioxide
     n : an oxide containing three atoms of oxygen in the molecule
