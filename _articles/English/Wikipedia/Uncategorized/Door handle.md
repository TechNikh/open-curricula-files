---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Door_knob
offline_file: ""
offline_thumbnail: ""
uuid: b78643cc-c8dd-4a65-aa06-6b6ae5874544
updated: 1484308368
title: Door handle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Gold_doorknob_crop.jpg
tags:
    - Description
    - Types
    - Usage
    - Cars
    - Foldables
    - Pocket doors
    - Infection control
    - Gallery
categories:
    - Uncategorized
---
A door handle is an attached object or mechanism used to manually open or close a door.[1] In the United States, a door handle generally can refer to any fixed or lever-operated door latch device, including on car doors. The term door knob or doorknob tends to refer to round operating mechanisms.
