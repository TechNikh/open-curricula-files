---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sick
offline_file: ""
offline_thumbnail: ""
uuid: 16833c24-18c0-443c-b62e-12f222e81462
updated: 1484310541
title: sick
categories:
    - Dictionary
---
sick
     adj 1: not in good physical or mental health; "ill from the
            monotony of his suffering" [syn: {ill}] [ant: {well}]
     2: feeling nausea; feeling about to vomit [syn: {nauseated}, {queasy},
         {sickish}]
     3: affected with madness or insanity; "a man who had gone mad"
        [syn: {brainsick}, {crazy}, {demented}, {distracted}, {disturbed},
         {mad}, {unbalanced}, {unhinged}]
     4: having a strong distaste from surfeit; "grew more and more
        disgusted"; "fed up with their complaints"; "sick of it
        all"; "sick to death of flattery"; "gossip that makes one
        sick"; "tired of the noise and smoke" [syn: {disgusted}, {fed
        up(p)}, ...
