---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meal
offline_file: ""
offline_thumbnail: ""
uuid: c75ebfde-ea36-4f2c-84d8-b5d0b0522aea
updated: 1484310384
title: meal
categories:
    - Dictionary
---
meal
     n 1: the food served and eaten at one time [syn: {repast}]
     2: any of the occasions for eating food that occur by custom or
        habit at more or less fixed times
     3: coarsely ground foodstuff; especially seeds of various
        cereal grasses or pulse
