---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Matching_pennies
offline_file: ""
offline_thumbnail: ""
uuid: 0891d302-3e23-4f04-9069-27c6f1f802c8
updated: 1484309055
title: Matching pennies
tags:
    - Theory
    - Variants
    - Laboratory experiments
    - Real-life data
categories:
    - Non-cooperative games
---
Matching pennies is the name for a simple game used in game theory. It is played between two players, Even and Odd. Each player has a penny and must secretly turn the penny to heads or tails. The players then reveal their choices simultaneously. If the pennies match (both heads or both tails), then Even keeps both pennies, so wins one from Odd (+1 for Even, −1 for Odd). If the pennies do not match (one heads and one tails) Odd keeps both pennies, so receives one from Even (−1 for Even, +1 for Odd).
