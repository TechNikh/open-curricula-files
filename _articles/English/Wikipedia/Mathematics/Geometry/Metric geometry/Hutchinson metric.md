---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hutchinson_metric
offline_file: ""
offline_thumbnail: ""
uuid: 0d5029d1-88fe-494f-b9d0-0a5b34c828ed
updated: 1484309266
title: Hutchinson metric
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Julia_set_%2528indigo%2529.png'
categories:
    - Metric geometry
---
In mathematics, the Hutchinson metric is a function which measures "the discrepancy between two images for use in fractal image processing" and "can also be applied to describe the similarity between DNA sequences expressed as real or complex genomic signals".[1][2]
