---
version: 1
type: video
provider: YouTube
id: HF1vUuZDCTk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 765dda17-af69-4835-a1ff-7d1205fccb1c
updated: 1486069623
title: 'Why It Works - Multiply Two Numbers Between 11 and 19'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/HF1vUuZDCTk/default.jpg
    - https://i3.ytimg.com/vi/HF1vUuZDCTk/1.jpg
    - https://i3.ytimg.com/vi/HF1vUuZDCTk/2.jpg
    - https://i3.ytimg.com/vi/HF1vUuZDCTk/3.jpg
---
