---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/duration
offline_file: ""
offline_thumbnail: ""
uuid: 4abdaa0c-59af-45ba-9fb0-b8a2ce1b9a95
updated: 1484310484
title: duration
categories:
    - Dictionary
---
duration
     n 1: the period of time during which something continues [syn: {continuance}]
     2: the property of enduring or continuing in time [syn: {continuance}]
     3: continuance in time; "the ceremony was of short duration";
        "he complained about the length of time required" [syn: {length}]
