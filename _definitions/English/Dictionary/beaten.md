---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beaten
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484425682
title: beaten
categories:
    - Dictionary
---
beat
     adj : very tired; "was all in at the end of the day"; "so beat I
           could flop down and go to sleep anywhere"; "bushed
           after all that exercise"; "I'm dead after that long
           trip" [syn: {all in(p)}, {beat(p)}, {bushed(p)}, {dead(p)}]
     n 1: a regular route for a sentry or policeman; "in the old days
          a policeman walked a beat and knew all his people by
          name" [syn: {round}]
     2: the rhythmic contraction and expansion of the arteries with
        each beat of the heart; "he could feel the beat of her
        heart" [syn: {pulse}, {pulsation}, {heartbeat}]
     3: the basic rhythmic unit in a piece of music; "the piece has
        a ...
