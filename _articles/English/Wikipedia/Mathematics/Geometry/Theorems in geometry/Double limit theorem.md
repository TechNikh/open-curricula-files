---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Double_limit_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 1e6d83bd-ca40-42e0-9c2c-8642f2fc4df1
updated: 1484309110
title: Double limit theorem
categories:
    - Theorems in geometry
---
In hyperbolic geometry, Thurston's double limit theorem gives condition for a sequence of quasi-Fuchsian groups to have a convergent subsequence. It was introduced in Thurston (1998, theorem 4.1) and is a major step in Thurston's proof of the hyperbolization theorem for the case of manifolds that fiber over the circle.
