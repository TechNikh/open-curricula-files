---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Locally_acyclic_morphism
offline_file: ""
offline_thumbnail: ""
uuid: 618e1e88-7c9d-46c8-bc87-3f93985ff496
updated: 1484309365
title: Locally acyclic morphism
categories:
    - Geometry stubs
---
In algebraic geometry, a morphism 
  
    
      
        f
        :
        X
        →
        S
      
    
    {\displaystyle f:X\to S}
  
 of schemes is said to be locally acyclic if, roughly, any sheaf on S and its restriction to X through f have the same étale cohomology, locally. For example, a smooth morphism is universally locally acyclic.
