---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Preordered_class
offline_file: ""
offline_thumbnail: ""
uuid: 006fdf29-a363-4c1a-8981-f0118e131c2a
updated: 1484309510
title: Preordered class
categories:
    - Set theory
---
When dealing with a class C, it is possible to define a class relation on C as a subclass of the power class C 
  
    
      
        ×
      
    
    {\displaystyle \times }
  
 C . Then, it is convenient to use the language of relations on a set.
