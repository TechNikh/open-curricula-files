---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quadrant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484360521
title: quadrant
categories:
    - Dictionary
---
quadrant
     n 1: a quarter of the circumference of a circle [syn: {right
          angle}]
     2: a quarter of the circumference of a circle [syn: {quarter-circle}]
     3: any of the four areas into which a plane is divided by two
        orthogonal coordinate axes
     4: the area enclosed by two perpendicular radii of a circle
     5: a measuring instrument for measuring altitude of heavenly
        bodies
