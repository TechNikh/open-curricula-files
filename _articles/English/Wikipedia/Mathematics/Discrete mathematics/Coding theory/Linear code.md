---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_code
offline_file: ""
offline_thumbnail: ""
uuid: 6237753a-487b-4036-865f-e9207e9d4610
updated: 1484308556
title: Linear code
tags:
    - Definition and parameters
    - Generator and check matrices
    - 'Example: Hamming codes'
    - 'Example: Hadamard codes'
    - Nearest neighbor algorithm
    - Popular notation
    - Singleton bound
    - Examples
    - Generalization
categories:
    - Coding theory
---
In coding theory, a linear code is an error-correcting code for which any linear combination of codewords is also a codeword. Linear codes are traditionally partitioned into block codes and convolutional codes, although turbo codes can be seen as a hybrid of these two types.[1] Linear codes allow for more efficient encoding and decoding algorithms than other codes (cf. syndrome decoding).
