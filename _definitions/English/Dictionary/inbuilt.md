---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inbuilt
offline_file: ""
offline_thumbnail: ""
uuid: d259d9ab-3d3d-435c-b7c3-959936ec7a29
updated: 1484310361
title: inbuilt
categories:
    - Dictionary
---
inbuilt
     adj : existing as an essential constituent or characteristic; "the
           Ptolemaic system with its built-in concept of
           periodicity"; "a constitutional inability to tell the
           truth" [syn: {built-in}, {constitutional}, {inherent},
           {integral}]
