---
version: 1
type: video
provider: YouTube
id: nz3ClBQojU8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/ROOT%20SYSTEM.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5349ed1b-5309-4429-a89e-bda9da86137a
updated: 1486069666
title: ROOT SYSTEM
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/nz3ClBQojU8/default.jpg
    - https://i3.ytimg.com/vi/nz3ClBQojU8/1.jpg
    - https://i3.ytimg.com/vi/nz3ClBQojU8/2.jpg
    - https://i3.ytimg.com/vi/nz3ClBQojU8/3.jpg
---
