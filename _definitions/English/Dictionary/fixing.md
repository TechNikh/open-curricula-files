---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fixing
offline_file: ""
offline_thumbnail: ""
uuid: f825ed60-4aea-4307-b200-6dd8eddc26fb
updated: 1484310170
title: fixing
categories:
    - Dictionary
---
fixing
     n 1: the act of putting something in working order again [syn: {repair},
           {fix}, {fixture}, {mend}, {mending}, {reparation}]
     2: restraint that attaches to something or holds something in
        place [syn: {fastener}, {fastening}, {holdfast}]
     3: the sterilization of an animal; "they took him to the vet
        for neutering" [syn: {neutering}, {altering}]
     4: (histology) the preservation and hardening of a tissue
        sample to retain as nearly as possible the same relations
        they had in the living body [syn: {fixation}]
