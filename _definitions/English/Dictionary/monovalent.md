---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monovalent
offline_file: ""
offline_thumbnail: ""
uuid: 30c98f52-7c5d-4913-9304-fa19566b4955
updated: 1484310413
title: monovalent
categories:
    - Dictionary
---
monovalent
     adj 1: containing only one kind of antibody [ant: {polyvalent}]
     2: having a valence of 1 [syn: {univalent}] [ant: {polyvalent}]
