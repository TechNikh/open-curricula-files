---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reflexive_space
offline_file: ""
offline_thumbnail: ""
uuid: 377838aa-55ac-474f-9284-6aad011bb8c3
updated: 1484309239
title: Reflexive space
tags:
    - Reflexive Banach spaces
    - Remark
    - Examples
    - Properties
    - Super-reflexive space
    - Finite trees in Banach spaces
    - Reflexive locally convex spaces
    - Examples
    - Stereotype spaces and other versions of reflexivity
    - Notes
categories:
    - Duality theories
---
In the area of mathematics known as functional analysis, a reflexive space is a Banach space (or more generally a locally convex topological vector space) that coincides with the continuous dual of its continuous dual space, both as linear space and as topological space. Reflexive Banach spaces are often characterized by their geometric properties.
