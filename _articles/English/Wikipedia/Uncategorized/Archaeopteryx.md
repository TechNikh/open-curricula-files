---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Archeopteryx
offline_file: ""
offline_thumbnail: ""
uuid: 965db939-3d24-4011-ab73-54950b8bdc7b
updated: 1484308344
title: Archaeopteryx
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Archiesizeall1.svg.png
tags:
    - Description
    - Plumage
    - Colouration
    - Palaeobiology
    - Flight
    - growth
    - Daily activity patterns
    - Paleoecology
    - History of discovery
    - Classification
    - species
    - Synonyms
    - Controversy
    - Authenticity
    - Phylogenetic position
    - In popular culture
categories:
    - Uncategorized
---
Archaeopteryx (/ˌɑːrkiːˈɒptərᵻks/), sometimes referred to by its German name Urvogel ("original bird" or "first bird"), is a genus of bird-like dinosaurs that is transitional between non-avian feathered dinosaurs and modern birds. The name derives from the ancient Greek ἀρχαῖος (archaīos) meaning "ancient", and πτέρυξ (ptéryx), meaning "feather" or "wing". Between the late nineteenth century and the early twenty-first century, Archaeopteryx had been generally accepted by palaeontologists and popular reference books as the oldest known bird (member of the group Avialae).[2] Older potential avialans have since been identified, including Anchiornis, Xiaotingia, and ...
