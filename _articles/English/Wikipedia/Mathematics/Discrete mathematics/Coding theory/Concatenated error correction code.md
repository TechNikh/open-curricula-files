---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Concatenated_error_correction_code
offline_file: ""
offline_thumbnail: ""
uuid: ec5fdfa4-2dc4-4b1b-aeca-0646806f0c4e
updated: 1484308544
title: Concatenated error correction code
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/440px-Concatenated_codes_diagram.png
tags:
    - Background
    - Description
    - Properties
    - Decoding concatenated codes
    - Remarks
    - Applications
    - 'Turbo codes: A parallel concatenation approach'
categories:
    - Coding theory
---
In coding theory, concatenated codes form a class of error-correcting codes that are derived by combining an inner code and an outer code. They were conceived in 1966 by Dave Forney as a solution to the problem of finding a code that has both exponentially decreasing error probability with increasing block length and polynomial-time decoding complexity.[1] Concatenated codes became widely used in space communications in the 1970s.
