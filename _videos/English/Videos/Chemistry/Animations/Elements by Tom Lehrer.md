---
version: 1
type: video
provider: YouTube
id: SmwlzwGMMwc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 463916cb-dbcb-4c97-a3fa-d4cf3a5e7675
updated: 1486069662
title: Elements by Tom Lehrer
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/SmwlzwGMMwc/default.jpg
    - https://i3.ytimg.com/vi/SmwlzwGMMwc/1.jpg
    - https://i3.ytimg.com/vi/SmwlzwGMMwc/2.jpg
    - https://i3.ytimg.com/vi/SmwlzwGMMwc/3.jpg
---
