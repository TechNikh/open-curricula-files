---
version: 1
type: article
id: https://en.wikipedia.org/wiki/PID_controller
offline_file: ""
offline_thumbnail: ""
uuid: ee0dd327-67aa-4374-a664-b21e298d8163
updated: 1484308738
title: PID controller
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-PID_en.svg.png
tags:
    - Fundamental operation
    - History and applications
    - Origins
    - Industrial controller development
    - Other applications
    - Present day
    - Control loop basics
    - PID controller theory
    - Proportional term
    - Steady-state error
    - Integral term
    - Derivative term
    - Loop tuning
    - Stability
    - Optimum behavior
    - Overview of methods
    - Manual tuning
    - Ziegler–Nichols method
    - PID tuning software
    - Limitations of PID control
    - Linearity
    - Noise in derivative
    - Modifications to the PID algorithm
    - Integral windup
    - Overshooting from known disturbances
    - PI controller
    - Deadband
    - Setpoint step change
    - Feed-forward
    - Bumpless operation
    - Other improvements
    - Cascade control
    - Alternative nomenclature and PID forms
    - Ideal versus standard PID form
    - Reciprocal gain
    - Basing derivative action on PV
    - Basing proportional action on PV
    - Laplace form of the PID controller
    - PID pole zero cancellation
    - Series/interacting form
    - Discrete implementation
    - Pseudocode
    - Notes
    - PID tutorials
categories:
    - Control engineering
---
A proportional–integral–derivative controller (PID controller) is a control loop feedback mechanism (controller) commonly used in industrial control systems. A PID controller continuously calculates an error value 
  
    
      
        e
        (
        t
        )
      
    
    {\displaystyle e(t)}
  
 as the difference between a desired setpoint and a measured process variable and applies a correction based on proportional, integral, and derivative terms, (sometimes denoted P, I, and D respectively) which give their name to the controller type.
