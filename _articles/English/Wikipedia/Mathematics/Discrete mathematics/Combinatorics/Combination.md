---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combination
offline_file: ""
offline_thumbnail: ""
uuid: 8c8789cf-17da-4618-943f-c2a890dd100f
updated: 1484308580
title: Combination
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Combinations_without_repetition%253B_5_choose_3.svg.png'
tags:
    - Number of k-combinations
    - Example of counting combinations
    - Enumerating k-combinations
    - Number of combinations with repetition
    - Example of counting multisubsets
    - Number of k-combinations for all k
    - 'Probability: sampling a random combination'
    - Notes
categories:
    - Combinatorics
---
In mathematics, a combination is a way of selecting items from a collection, such that (unlike permutations) the order of selection does not matter. In smaller cases it is possible to count the number of combinations. For example, given three fruits, say an apple, an orange and a pear, there are three combinations of two that can be drawn from this set: an apple and a pear; an apple and an orange; or a pear and an orange. More formally, a k-combination of a set S is a subset of k distinct elements of S. If the set has n elements, the number of k-combinations is equal to the binomial coefficient
