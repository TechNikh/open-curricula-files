---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wall_plan
offline_file: ""
offline_thumbnail: ""
uuid: d1cf0aad-1f81-4352-af24-f1da8666f7c2
updated: 1484309334
title: Wall plan
categories:
    - Technical drawing
---
A wall plan is a drawing which consists of complete details with dimensions (with an accuracy of an inch) about all four sides and ceiling of each and every room in a building. It is drawn with the help of a floor plan as the basic input.
