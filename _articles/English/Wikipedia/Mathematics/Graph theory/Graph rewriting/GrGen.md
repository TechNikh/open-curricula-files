---
version: 1
type: article
id: https://en.wikipedia.org/wiki/GrGen
offline_file: ""
offline_thumbnail: ""
uuid: 99a7b18d-a423-4a58-a324-1d37cdad7901
updated: 1484309452
title: GrGen
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-GrGenNETKochSnowflakeMatch.png
tags:
    - Specification sample
    - Conference papers
categories:
    - Graph rewriting
---
GrGen.NET is a software development tool that offers programming languages (domain specific languages) that are optimized for the processing of graph structured data. The core of the languages consists of modular graph rewrite rules, which are built on declarative graph pattern matching and rewriting; they are supplemented by many of the constructs that are used in imperative and object-oriented programming, and are completed with language devices known from database query languages.
