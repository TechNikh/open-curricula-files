---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emulate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484642701
title: emulate
categories:
    - Dictionary
---
emulate
     v 1: strive to equal or match, especially by imitating; "He is
          emulating the skating skills of his older sister"
     2: imitate the function of (another system), as by modifying
        the hardware or the software
     3: compete with successfully; approach or reach equality with;
        "This artists's drawings cannot emulate his water colors"
