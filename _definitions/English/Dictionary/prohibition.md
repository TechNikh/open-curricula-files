---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prohibition
offline_file: ""
offline_thumbnail: ""
uuid: 3f1e444e-ef7d-46f8-bef0-7da501b10dac
updated: 1484310192
title: prohibition
categories:
    - Dictionary
---
prohibition
     n 1: a law forbidding the sale of alcoholic beverages; "in 1920
          the 18th amendment to the Constitution established
          prohibition in the US"
     2: a decree that prohibits something [syn: {ban}, {proscription}]
     3: the period from 1920 to 1933 when the sale of alcoholic
        beverages was prohibited in the United States by a
        constitutional amendment [syn: {prohibition era}]
     4: refusal to approve or assent to
     5: the action of prohibiting or inhibiting or forbidding (or an
        instance thereof); "they were restrained by a prohibition
        in their charter"; "a medical inhibition of alcoholic
        beverages"; "he ignored his ...
