---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gibbons%E2%80%93Hawking_space'
offline_file: ""
offline_thumbnail: ""
uuid: 742ffa72-0f82-4bbd-850d-dff4e08bcbb9
updated: 1484309149
title: Gibbons–Hawking space
categories:
    - Algebraic geometry
---
In mathematical physics, a Gibbons–Hawking space, named after Gary Gibbons and Stephen Hawking, is essentially a hyperkähler manifold with an extra U(1) symmetry.[1] (In general, Gibbons–Hawking metrics are a subclass of hyperkähler metrics.[2]) Gibbons–Hawking spaces, especially ambipolar ones,[3] find an application in the study of black hole microstate geometries.[1][4]
