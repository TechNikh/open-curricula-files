---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sample_entropy
offline_file: ""
offline_thumbnail: ""
uuid: 73fbd248-5395-4b7d-b626-516164c31346
updated: 1484308762
title: Sample entropy
tags:
    - Definition
    - Multiscale SampEn
    - Implementation
categories:
    - Entropy
---
Sample entropy (SampEn) is a modification of approximate entropy (ApEn), used for assessing the complexity of physiological time-series signals, diagnosing diseased states.[1] SampEn has two advantages over ApEn: data length independence and a relatively trouble-free implementation. Also, there is a small computational difference: In ApEn, the comparison between the template vector (see below) and the rest of the vectors also includes comparison with itself. This guarantees that probabilities 
  
    
      
        
          C
          
            i
          
          
            ′
            
              m
            
          
        
        (
        r
        )
      
   ...
