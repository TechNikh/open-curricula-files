---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Social_complexity
offline_file: ""
offline_thumbnail: ""
uuid: 237665ee-f4c2-400e-a239-4a410a37dbbc
updated: 1484308811
title: Social complexity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Penrose_tiling.gif
tags:
    - Theoretical background
    - Methodologies
    - Complex social network analysis
    - Computational sociology
    - Sociocybernetics
    - Areas of application
    - Social science
    - General
categories:
    - Nonlinear systems
---
In sociology, social complexity is a conceptual framework used in the analysis of society. Contemporary definitions of complexity in the sciences are found in relation to systems theory, in which a phenomenon under study has many parts and many possible arrangements of the relationships between those parts. At the same time, what is complex and what is simple is relative and may change with time.[1]
