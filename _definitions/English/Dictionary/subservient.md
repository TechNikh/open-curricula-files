---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subservient
offline_file: ""
offline_thumbnail: ""
uuid: eb9dae0b-836a-4b5a-9ada-b10d72b44dce
updated: 1484310609
title: subservient
categories:
    - Dictionary
---
subservient
     adj 1: compliant and obedient to authority; "editors and
            journalists who express opinions in print that are
            opposed to the interests of the rich are dismissed and
            replaced by subservient ones"-G. B. Shaw
     2: abjectly submissive; characteristic of a slave or servant;
        "slavish devotion to her job ruled her life"; "a slavish
        yes-man to the party bosses"- S.H.Adams; "she has become
        submissive and subservient" [syn: {slavish}, {submissive}]
