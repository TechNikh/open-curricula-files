---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lyell
offline_file: ""
offline_thumbnail: ""
uuid: aec38c63-a7f2-4963-a695-4f84d43e0867
updated: 1484308345
title: Lyell
tags:
    - people
    - Places
categories:
    - Uncategorized
---
Lyell is a surname of Scotland traced to Radulphus de Insula, 11th-century Lord of Duchal. "De insula" was subsequently translated into the Old French "de l'isle" and developed into a number of variants ([de] Lyell; [de] Lisle; [de] Lyle; see below). John Lyell in 1706 emigrated to Virginia and is the progenitor of the American Lyells.
