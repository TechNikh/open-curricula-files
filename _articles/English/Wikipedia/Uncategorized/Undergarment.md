---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inners
offline_file: ""
offline_thumbnail: ""
uuid: b9796541-2248-4a62-a97c-27991becaddf
updated: 1484094290
title: Undergarment
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/267px-Knickers1.jpg
tags:
    - Terminology
    - Function
    - Religious functions
    - History
    - Ancient history
    - Middle Ages and Renaissance
    - Enlightenment and Industrial Age
    - 1900s to 1920s
    - 1930s and 1940s
    - 1950s and 60s
categories:
    - Uncategorized
---
Undergarments are items of clothing worn beneath outer clothes, usually in direct contact with the skin, although they may comprise more than a single layer. They serve to keep outer garments from being soiled or damaged by bodily excretions, to lessen the friction of outerwear against the skin, to shape the body, and to provide concealment or support for parts of it. In cold weather, long underwear is sometimes worn to provide additional warmth. Special types of undergarments have religious significance. Some items of clothing are designed as undergarments, while others, such as T-shirts and certain types of shorts, are appropriate both as undergarments and as outer clothing. If made of ...
