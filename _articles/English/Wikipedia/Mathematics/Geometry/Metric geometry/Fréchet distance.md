---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fr%C3%A9chet_distance'
offline_file: ""
offline_thumbnail: ""
uuid: 5a3ce89a-0948-4ddc-9a15-cbd74e94deb4
updated: 1484309262
title: Fréchet distance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Free-space-diagram.png
tags:
    - Intuitive definition
    - Formal definition
    - The free-space diagram
    - Variants
    - Examples
categories:
    - Metric geometry
---
In mathematics, the Fréchet distance is a measure of similarity between curves that takes into account the location and ordering of the points along the curves. It is named after Maurice Fréchet.
