---
version: 1
type: video
provider: YouTube
id: hAAIgNzBGg0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%235b.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2d0abbd2-ddf5-49d6-b53a-f44bf23856b0
updated: 1486069692
title: '2011 Calculus AB Free Response #5b'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/hAAIgNzBGg0/default.jpg
    - https://i3.ytimg.com/vi/hAAIgNzBGg0/1.jpg
    - https://i3.ytimg.com/vi/hAAIgNzBGg0/2.jpg
    - https://i3.ytimg.com/vi/hAAIgNzBGg0/3.jpg
---
