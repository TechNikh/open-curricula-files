---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Developable_surface
offline_file: ""
offline_thumbnail: ""
uuid: 33d30de6-4d5c-46de-a040-9dcb14bb1387
updated: 1484309215
title: Developable surface
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kreiszylinder.svg.png
tags:
    - Particulars
    - Application
    - Non-developable surface
    - Applications of non-developable surfaces
categories:
    - Differential geometry
---
In mathematics, a developable surface (or torse: archaic) is a surface with zero Gaussian curvature. That is, it is a surface that can be flattened onto a plane without distortion (i.e. "stretching" or "compressing"). Conversely, it is a surface which can be made by transforming a plane (i.e. "folding", "bending", "rolling", "cutting" and/or "gluing"). In three dimensions all developable surfaces are ruled surfaces (but not vice versa). There are developable surfaces in R4 which are not ruled.[1]
