---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sheet
offline_file: ""
offline_thumbnail: ""
uuid: 362bb34d-5388-4334-808c-88ca4620d048
updated: 1484310210
title: sheet
categories:
    - Dictionary
---
sheet
     n 1: any broad thin expanse or surface; "a sheet of ice"
     2: used for writing or printing [syn: {piece of paper}, {sheet
        of paper}]
     3: bed linen consisting of a large rectangular piece of cotton
        or linen cloth; used in pairs [syn: {bed sheet}]
     4: (mathematics) an unbounded two-dimensional shape; "we will
        refer to the plane of the graph as the X-Y plane"; "any
        line joining two points on a plane lies wholly on that
        plane" [syn: {plane}]
     5: newspaper with half-size pages [syn: {tabloid}, {rag}]
     6: a flat artifact that is thin relative to its length and
        width [syn: {flat solid}]
     7: (nautical) a line (rope or ...
