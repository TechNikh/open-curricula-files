---
version: 1
type: video
provider: YouTube
id: jfvMtCHv1q4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Low%20Mass%20Stars-%20Crash%20Course%20Astronomy%20%2329.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4365629c-5af9-4983-9b04-0e54f45d8532
updated: 1486069685
title: 'Low Mass Stars- Crash Course Astronomy #29'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/jfvMtCHv1q4/default.jpg
    - https://i3.ytimg.com/vi/jfvMtCHv1q4/1.jpg
    - https://i3.ytimg.com/vi/jfvMtCHv1q4/2.jpg
    - https://i3.ytimg.com/vi/jfvMtCHv1q4/3.jpg
---
