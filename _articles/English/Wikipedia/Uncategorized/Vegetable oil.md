---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oilseeds
offline_file: ""
offline_thumbnail: ""
uuid: a70ac613-1124-48b8-a4c8-55d89a512604
updated: 1484308463
title: Vegetable oil
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-%25C3%2593leo_de_jambu_0.JPG'
tags:
    - Uses of triglyceride vegetable oil
    - Culinary uses
    - Hydrogenated oils
    - Industrial uses
    - Pet food additive
    - Fuel
    - Production
    - Mechanical extraction
    - Solvent extraction
    - Hydrogenation
categories:
    - Uncategorized
---
A vegetable oil is a triglyceride extracted from a plant.[1] The term "vegetable oil" can be narrowly defined as referring only to plant oils that are liquid at room temperature,[2] or broadly defined without regard to a substance's state of matter at a given temperature.[3] For this reason, vegetable oils that are solid at room temperature are sometimes called vegetable fats. In contrast to these triglycerides, vegetable waxes lack glycerin in their structure. Although many plant parts may yield oil, in commercial practice, oil is extracted primarily from seeds.
