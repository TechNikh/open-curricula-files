---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Great_Red_Spot
offline_file: ""
offline_thumbnail: ""
uuid: a6337be1-f807-4180-a439-af86166cfda4
updated: 1484308874
title: Great Red Spot
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Jupiter_from_Voyager_1.jpg
tags:
    - Observation history
    - Structure
    - Mechanics
    - Notes
categories:
    - Vortices
---
The Great Red Spot is a persistent anticyclonic storm on the planet Jupiter, 22° south of the equator, which has lasted for at least 186 years and possibly as long as 351 years or more.[1][2] Storms such as this are not uncommon within the turbulent atmospheres of gas giants. Before the Voyager missions, astronomers were highly uncertain of the Red Spot's nature. Many believed it to be a solid or liquid feature on Jupiter's surface.[citation needed]
