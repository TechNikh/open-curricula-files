---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Henry_P._H._Bromwell
offline_file: ""
offline_thumbnail: ""
uuid: 9aedd750-e924-49fe-8e92-2a6d331b89bc
updated: 1484309073
title: Henry P. H. Bromwell
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Restorations_of_masonic_geometry.jpg
tags:
    - Family and education
    - Law and politics
    - Freemasonry
    - Free and Accepted Architects
    - Restorations of Masonic Geometry and Symbolry
categories:
    - Geometers
---
Henry Pelham Holmes Bromwell (August 26, 1823 – January 9, 1903) was an American lawyer, politician from Illinois, and prominent Freemason. He was a lawyer and judge who served as a U.S. Representative from Illinois from 1865–1869 and continued to practice law when he moved to Colorado in 1870 where he was appointed to compile the state's statutes. Bromwell was initiated into freemasonry in 1854, and he became the Grand Master of Illinois in 1864. When he moved to Colorado he became that state's first Honorary Grand Master. He developed the Free, and Accepted Architects, a new rite for Freemasonry which sought to teach its initiates the lost work of the craft embodied in Bromwell's ...
