---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indigenous
offline_file: ""
offline_thumbnail: ""
uuid: 0832504a-b386-4afb-8138-4ed164507007
updated: 1484310168
title: indigenous
categories:
    - Dictionary
---
indigenous
     adj : originating where it is found; "the autochthonal fauna of
           Australia includes the kangaroo"; "autochthonous rocks
           and people and folktales"; "endemic folkways"; "the
           Ainu are indigenous to the northernmost islands of
           Japan" [syn: {autochthonal}, {autochthonic}, {autochthonous},
            {endemic}]
