---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_decomposition
offline_file: ""
offline_thumbnail: ""
uuid: abe80267-b693-42a4-8861-af95f114e959
updated: 1484308870
title: Thermal decomposition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Priestley_Joseph_pneumatic_trough.jpg
tags:
    - Examples
    - Decomposition of nitrates, nitrites and ammonium compounds
    - Ease of decomposition
categories:
    - Thermodynamics
---
Thermal decomposition, or thermolysis, is a chemical decomposition caused by heat. The decomposition temperature of a substance is the temperature at which the substance chemically decomposes.
