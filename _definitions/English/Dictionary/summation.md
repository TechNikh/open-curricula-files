---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/summation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484398741
title: summation
categories:
    - Dictionary
---
summation
     n 1: a concluding summary (as in presenting a case before a law
          court) [syn: {summing up}, {rundown}]
     2: (physiology) the process whereby multiple stimuli can
        produce a response (in a muscle or nerve or other part)
        that one stimulus alone does not produce
     3: the final aggregate; "the sum of all our troubles did not
        equal the misery they suffered" [syn: {sum}, {sum total}]
     4: the arithmetic operation of summing; calculating the sum of
        two or more numbers; "the summation of four and three
        gives seven"; "four plus three equals seven" [syn: {addition},
         {plus}]
