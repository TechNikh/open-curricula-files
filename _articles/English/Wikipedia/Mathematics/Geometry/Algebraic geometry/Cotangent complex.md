---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cotangent_complex
offline_file: ""
offline_thumbnail: ""
uuid: dbb6cfcc-df86-4c5e-a86a-d0cc38748943
updated: 1484309142
title: Cotangent complex
tags:
    - Motivation
    - Early work on cotangent complexes
    - The definition of the cotangent complex
    - Properties of the cotangent complex
    - Flat base change
    - Vanishing properties
    - Examples
    - Notes
categories:
    - Algebraic geometry
---
In mathematics the cotangent complex is roughly a universal linearization of a morphism of geometric or algebraic objects. Cotangent complexes were originally defined in special cases by a number of authors. Luc Illusie, Daniel Quillen, and M. André independently came up with a definition that works in all cases.
