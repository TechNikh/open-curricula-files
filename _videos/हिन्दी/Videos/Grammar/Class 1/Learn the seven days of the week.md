---
version: 1
type: video
provider: YouTube
id: 8khgn8LT9A4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 40caa039-061c-40a7-8696-da055265befe
updated: 1486069630
title: Learn the seven days of the week
categories:
    - Class 1
thumbnail_urls:
    - https://i3.ytimg.com/vi/8khgn8LT9A4/default.jpg
    - https://i3.ytimg.com/vi/8khgn8LT9A4/1.jpg
    - https://i3.ytimg.com/vi/8khgn8LT9A4/2.jpg
    - https://i3.ytimg.com/vi/8khgn8LT9A4/3.jpg
---
