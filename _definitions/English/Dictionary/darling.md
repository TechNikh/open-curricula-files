---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/darling
offline_file: ""
offline_thumbnail: ""
uuid: f27028c6-b25d-4e54-86d5-6239649b718b
updated: 1484310607
title: darling
categories:
    - Dictionary
---
darling
     adj : dearly loved [syn: {beloved}, {dear}]
     n 1: a special loved one [syn: {favorite}, {favourite}, {pet}, {dearie},
           {deary}, {ducky}]
     2: an Australian river; tributary of the Murray River [syn: {Darling
        River}]
