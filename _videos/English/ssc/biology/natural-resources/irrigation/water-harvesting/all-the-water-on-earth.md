---
title: All the Water on Earth
youtube_id: OiVwfAPSEH8
tags:
    - Environment
    - Water
    - Water cycle
    - oceanography
    - ocean
    - Science
    - Research
categories:
    - Water harvesting
---
