---
version: 1
type: video
provider: YouTube
id: EFkq4qTQxgg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 33600ead-77b6-4083-8d2b-1601b228c015
updated: 1486069609
title: Losing tennis balls
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/EFkq4qTQxgg/default.jpg
    - https://i3.ytimg.com/vi/EFkq4qTQxgg/1.jpg
    - https://i3.ytimg.com/vi/EFkq4qTQxgg/2.jpg
    - https://i3.ytimg.com/vi/EFkq4qTQxgg/3.jpg
---
