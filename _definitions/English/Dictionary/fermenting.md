---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fermenting
offline_file: ""
offline_thumbnail: ""
uuid: ec395b9f-3c2f-4d9c-8908-8f9ed0c2fe50
updated: 1484310311
title: fermenting
categories:
    - Dictionary
---
fermenting
     n : a process in which an agent causes an organic substance to
         break down into simpler substances; especially, the
         anaerobic breakdown of sugar into alcohol [syn: {zymosis},
          {zymolysis}, {fermentation}, {ferment}]
