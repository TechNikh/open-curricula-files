---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apparatus
offline_file: ""
offline_thumbnail: ""
uuid: 261d23b8-b3ee-4aa8-84d5-c25738a0ecc3
updated: 1484310321
title: apparatus
categories:
    - Dictionary
---
apparatus
     n 1: equipment designed to serve a specific function [syn: {setup}]
     2: (anatomy) a group of body parts that work together to
        perform a given function; "the breathing apparatus"
