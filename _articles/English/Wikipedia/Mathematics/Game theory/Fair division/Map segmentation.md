---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Map_segmentation
offline_file: ""
offline_thumbnail: ""
uuid: 7ec10e62-4f7e-43c3-b8f3-917a59da1a49
updated: 1484308520
title: Map segmentation
tags:
    - Notation
    - Examples
    - Related problems
categories:
    - Fair division
---
In mathematics, the map segmentation problem is a kind of optimization problem. It involves a certain geographic region that has to be partitioned into smaller sub-regions in order to achieve a certain goal. Typical optimization objectives include:[1]
