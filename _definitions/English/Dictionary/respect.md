---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/respect
offline_file: ""
offline_thumbnail: ""
uuid: 2b3601ed-2a00-4881-b025-373e78532fc5
updated: 1484310273
title: respect
categories:
    - Dictionary
---
respect
     n 1: (usually preceded by `in') a detail or point; "it differs in
          that respect" [syn: {regard}]
     2: the condition of being honored (esteemed or respected or
        well regarded); "it is held in esteem"; "a man who has
        earned high regard" [syn: {esteem}, {regard}] [ant: {disesteem}]
     3: an attitude of admiration or esteem; "she lost all respect
        for him" [syn: {esteem}, {regard}] [ant: {disrespect}]
     4: a courteous expression (by word or deed) of esteem or
        regard; "his deference to her wishes was very flattering";
        "be sure to give my respects to the dean" [syn: {deference}]
     5: behavior intended to please your parents; ...
