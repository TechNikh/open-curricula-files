---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self
offline_file: ""
offline_thumbnail: ""
uuid: 709fc404-15d3-4cb0-801d-c7af5fdff1b7
updated: 1484310305
title: self
categories:
    - Dictionary
---
self
     adj 1: combining form; oneself or itself; "self-control"
     2: used as a combining form; relating to--of or by or to or
        from or for--the self; "self-knowledge";
        "self-proclaimed"; "self-induced"
     n 1: your consciousness of your own identity [syn: {ego}]
     2: a person considered as a unique individual; "one's own self"
     [also: {selves} (pl)]
