---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-sufficiency
offline_file: ""
offline_thumbnail: ""
uuid: d3a8a823-9226-4972-a5f0-d9f2a63aa294
updated: 1484310545
title: self-sufficiency
categories:
    - Dictionary
---
self-sufficiency
     n : personal independence [syn: {autonomy}, {self-direction}, {self-reliance}]
