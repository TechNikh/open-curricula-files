---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Avagadro
offline_file: ""
offline_thumbnail: ""
uuid: 40b78ef5-d6f9-4b8a-91d0-d99740aaec2e
updated: 1484308313
title: Amedeo Avogadro
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/lossy-page1-220px-Avogadro_-_M%25C3%25A9moire_sur_les_chaleurs_sp%25C3%25A9cifiques_des_corps_solides_et_liquides%252C_1833_-_6060053_TOAS005003_00003.tif.jpg'
tags:
    - Biography
    - Accomplishments
    - Response to the theory
categories:
    - Uncategorized
---
Lorenzo Romano Amedeo Carlo Avogadro di Quaregna e di Cerreto,[1] Count of Quaregna and Cerreto (9 August 1776, Turin, Piedmont-Sardinia – 9 July 1856), was an Italian scientist, most noted for his contribution to molecular theory now known as Avogadro's law, which states that equal volumes of gases under the same conditions of temperature and pressure will contain equal numbers of molecules. In tribute to him, the number of elementary entities (atoms, molecules, ions or other particles) in 1 mole of a substance, 7023602214179000000♠6.02214179(30)×1023, is known as the Avogadro constant, one of the seven SI base units and represented by NA.
