---
version: 1
type: video
provider: YouTube
id: rp7T4IItbtM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Types%20of%20immune%20responses-%20Innate%20and%20Adaptive.%20%20Humoral%20vs.%20Cell-Mediated.webm"
offline_file: ""
offline_thumbnail: ""
uuid: de7e6f92-f7e4-4301-80cd-b6c0fd7f8aa0
updated: 1486069581
title: 'Types of immune responses- Innate and Adaptive.  Humoral vs. Cell-Mediated'
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/rp7T4IItbtM/default.jpg
    - https://i3.ytimg.com/vi/rp7T4IItbtM/1.jpg
    - https://i3.ytimg.com/vi/rp7T4IItbtM/2.jpg
    - https://i3.ytimg.com/vi/rp7T4IItbtM/3.jpg
---
Overview of types of immune responses.  Difference between innate and adaptive immunity.  Differences between humoral adaptive immunity and cell-mediated adaptive immunity.
More free lessons at: http://www.khanacademy.org/video?v=rp7T4IItbtM
