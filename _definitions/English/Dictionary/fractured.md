---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fractured
offline_file: ""
offline_thumbnail: ""
uuid: d7a72a22-f3d8-464e-b6a1-57b3b4dd8511
updated: 1484310389
title: fractured
categories:
    - Dictionary
---
fractured
     adj : used of a break or crack or tear in bone or cartilage;
           "serious injuries such as broken bones and fractured
           skulls"
