---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accurate
offline_file: ""
offline_thumbnail: ""
uuid: 0c618a70-d864-4a99-835f-99d0eb1bd18b
updated: 1484310268
title: accurate
categories:
    - Dictionary
---
accurate
     adj 1: conforming exactly or almost exactly to fact or to a
            standard or performing with total accuracy; "an
            accurate reproduction"; "the accounting was accurate";
            "accurate measurements"; "an accurate scale" [ant: {inaccurate}]
     2: (of ideas, images, representations, expressions)
        characterized by perfect conformity to fact or truth ;
        strictly correct; "a precise image"; "a precise
        measurement" [syn: {exact}, {precise}]
