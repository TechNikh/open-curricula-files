---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plus-minus_sign
offline_file: ""
offline_thumbnail: ""
uuid: a191cb82-f95e-4d9f-a329-9f7afc399121
updated: 1484308913
title: Plus-minus sign
tags:
    - History
    - Usage
    - In mathematics
    - In statistics
    - In chess
    - Minus-plus sign
    - Encodings
    - Typing
    - Similar characters
categories:
    - Elementary arithmetic
---
The sign is normally pronounced "plus or minus".
