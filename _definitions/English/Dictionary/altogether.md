---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/altogether
offline_file: ""
offline_thumbnail: ""
uuid: 9c0441f1-58d4-4c13-af90-9a6b5052d3dd
updated: 1484310399
title: altogether
categories:
    - Dictionary
---
altogether
     n : informal terms for nakedness; "in the raw"; "in the
         altogether"; "in his birthday suit" [syn: {raw}, {birthday
         suit}]
     adv 1: to a complete degree or to the full or entire extent
            (`whole' is often used informally for `wholly'); "he
            was wholly convinced"; "entirely satisfied with the
            meal"; "it was completely different from what we
            expected"; "was completely at fault"; "a totally new
            situation"; "the directions were all wrong"; "it was
            not altogether her fault"; "an altogether new
            approach"; "a whole new idea" [syn: {wholly}, {entirely},
             {completely}, ...
