---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grammar-based_code
offline_file: ""
offline_thumbnail: ""
uuid: 45574c26-8d8f-4f8f-b0ca-05bbf96ab877
updated: 1484308506
title: Grammar-based code
tags:
    - Examples and characteristics
    - Practical algorithms
categories:
    - Coding theory
---
Grammar-based codes or Grammar-based compression are compression algorithms based on the idea of constructing a context-free grammar (CFG) for the string to be compressed. Examples include universal lossless data compression algorithms [1] and SEQUITUR, among others. To compress a data sequence 
  
    
      
        x
        =
        
          x
          
            1
          
        
        ⋯
        
          x
          
            n
          
        
      
    
    {\displaystyle x=x_{1}\cdots x_{n}}
  
, a grammar-based code transforms 
  
    
      
        x
      
    
    {\displaystyle x}
  
 into a context-free grammar 
  
    
      
        G
      
    
    ...
