---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fault
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484443201
title: fault
categories:
    - Dictionary
---
fault
     n 1: responsibility for a bad situation or event; "it was John's
          fault"
     2: (geology) a crack in the earth's crust resulting from the
        displacement of one side with respect to the other; "they
        built it right over a geological fault" [syn: {geological
        fault}, {shift}, {fracture}, {break}]
     3: the quality of being inadequate or falling short of
        perfection; "they discussed the merits and demerits of her
        novel"; "he knew his own faults much better than she did"
        [syn: {demerit}] [ant: {merit}]
     4: a wrong action attributable to bad judgment or ignorance or
        inattention; "he made a bad mistake"; "she was quick ...
