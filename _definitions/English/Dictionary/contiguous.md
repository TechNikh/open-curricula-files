---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contiguous
offline_file: ""
offline_thumbnail: ""
uuid: c6cf8d22-bcfe-4c27-ad88-2b7c092f58b7
updated: 1484310518
title: contiguous
categories:
    - Dictionary
---
contiguous
     adj 1: very close or connected in space or time; "contiguous
            events"; "immediate contact"; "the immediate
            vicinity"; "the immediate past" [syn: {immediate}]
     2: connecting without a break; within a common boundary; "the
        48 conterminous states"; "the contiguous 48 states" [syn:
        {conterminous}]
     3: having a common boundary or edge; touching; "abutting lots";
        "adjoining rooms"; "Rhode Island has two bordering states;
        Massachusetts and Conncecticut"; "the side of Germany
        conterminous with France"; "Utah and the contiguous state
        of Idaho"; "neighboring cities" [syn: {abutting}, {adjacent},
         ...
