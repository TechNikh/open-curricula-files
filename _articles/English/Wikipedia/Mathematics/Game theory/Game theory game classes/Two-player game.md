---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Two-player_game
offline_file: ""
offline_thumbnail: ""
uuid: bd2cc27f-777c-4975-b86a-2e2fbda5b7e0
updated: 1484309040
title: Two-player game
categories:
    - Game theory game classes
---
A two-player game is a multiplayer game that is played by just two players. This is distinct from a solitaire game, which is played by only one player.[1]
