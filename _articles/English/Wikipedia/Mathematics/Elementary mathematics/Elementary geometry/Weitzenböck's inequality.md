---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Weitzenb%C3%B6ck%27s_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: 01ea56d8-4fd8-4b4b-b167-48128b41b768
updated: 1484308942
title: "Weitzenböck's inequality"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-LabeledTriangle.svg.png
tags:
    - Geometric interpretation and proof
    - Further proofs
    - First method
    - Second method
    - Third method
    - Notes
    - 'References & further reading'
categories:
    - Elementary geometry
---
In mathematics, Weitzenböck's inequality, named after Roland Weitzenböck, states that for a triangle of side lengths 
  
    
      
        a
      
    
    {\displaystyle a}
  
, 
  
    
      
        b
      
    
    {\displaystyle b}
  
, 
  
    
      
        c
      
    
    {\displaystyle c}
  
, and area 
  
    
      
        Δ
      
    
    {\displaystyle \Delta }
  
, the following inequality holds:
