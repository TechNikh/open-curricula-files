---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/converse
offline_file: ""
offline_thumbnail: ""
uuid: bb2c4763-1b4e-4662-93b3-cb9b44a925c1
updated: 1484310218
title: converse
categories:
    - Dictionary
---
converse
     adj 1: of words so related that one reverses the relation denoted
            by the other; "`parental' and `filial' are converse
            terms"
     2: turned about in order or relation; "transposed letters"
        [syn: {reversed}, {transposed}]
     n : a proposition obtained by conversion
     v : carry on a conversation [syn: {discourse}]
