---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pendent
offline_file: ""
offline_thumbnail: ""
uuid: 0440b9e2-b0d1-4d64-9bfe-0f163b236660
updated: 1484309369
title: Pendent
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-BedfordRoyalCharter1.JPG
categories:
    - Geometry stubs
---
Pendent is an adjective that describes the condition of hanging, either literally, or figuratively,[1] as in undecided or incomplete. To be distinguished from the spelling "pendant" which is the noun.
