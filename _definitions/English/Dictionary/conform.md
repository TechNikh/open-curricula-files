---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conform
offline_file: ""
offline_thumbnail: ""
uuid: bac068ae-2d6b-47cf-b9e9-dbc42d6d246d
updated: 1484310188
title: conform
categories:
    - Dictionary
---
conform
     v 1: be similar, be in line with [ant: {deviate}]
     2: adapt or conform oneself to new or different conditions; "We
        must adjust to the bad economic situation" [syn: {adjust},
         {adapt}]
