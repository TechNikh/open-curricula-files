---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Li%C3%A9nard_equation'
offline_file: ""
offline_thumbnail: ""
uuid: 99548055-3244-40c2-86a6-8a6d872d8eed
updated: 1484308696
title: Liénard equation
tags:
    - Definition
    - Liénard system
    - Example
    - "Liénard's theorem"
    - Footnotes
categories:
    - Theorems in dynamical systems
---
In mathematics, more specifically in the study of dynamical systems and differential equations, a Liénard equation[1] is a second order differential equation, named after the French physicist Alfred-Marie Liénard.
