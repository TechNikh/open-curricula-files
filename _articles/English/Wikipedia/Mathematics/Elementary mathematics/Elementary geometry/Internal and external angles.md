---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Internal_and_external_angles
offline_file: ""
offline_thumbnail: ""
uuid: 91ec327a-fd30-4bc4-9e57-7667f0d323a2
updated: 1484308930
title: Internal and external angles
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-ExternalAngles.svg.png
categories:
    - Elementary geometry
---
In geometry, an angle of a polygon is formed by two sides of the polygon that share an endpoint. For a simple (non-self-intersecting) polygon, regardless of whether it is convex or non-convex, this angle is called an interior angle (or internal angle) if a point within the angle is in the interior of the polygon. A polygon has exactly one internal angle per vertex.
