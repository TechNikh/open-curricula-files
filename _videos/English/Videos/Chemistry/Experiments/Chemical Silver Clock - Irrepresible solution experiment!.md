---
version: 1
type: video
provider: YouTube
id: iDNo08C5HhM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Chemical%20Silver%20Clock%20-%20Irrepresible%20solution%20experiment%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2db2b2c4-2bcf-41fd-80b9-b20e53f107de
updated: 1486069660
title: 'Chemical Silver Clock - Irrepresible solution experiment!'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/iDNo08C5HhM/default.jpg
    - https://i3.ytimg.com/vi/iDNo08C5HhM/1.jpg
    - https://i3.ytimg.com/vi/iDNo08C5HhM/2.jpg
    - https://i3.ytimg.com/vi/iDNo08C5HhM/3.jpg
---
