---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maturity
offline_file: ""
offline_thumbnail: ""
uuid: 187870ff-12b0-4684-9234-faadf35d2270
updated: 1484310547
title: maturity
categories:
    - Dictionary
---
maturity
     n 1: the period of time in your life after your physical growth
          has stopped and you are fully developed [syn: {adulthood}]
     2: state of being mature; full development [syn: {matureness}]
        [ant: {immaturity}]
     3: the date on which a financial obligation must be repaid
        [syn: {maturity date}, {due date}]
