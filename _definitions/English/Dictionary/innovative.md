---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/innovative
offline_file: ""
offline_thumbnail: ""
uuid: 41e7c951-4d31-4c17-b913-7544e994b847
updated: 1484310234
title: innovative
categories:
    - Dictionary
---
innovative
     adj 1: ahead of the times; "the advanced teaching methods"; "had
            advanced views on the subject"; "a forward-looking
            corporation"; "is British industry innovative enough?"
            [syn: {advanced}, {forward-looking}, {modern}]
     2: being or producing something like nothing done or
        experienced or created before; "stylistically innovative
        works"; "innovative members of the artistic community"; "a
        mind so innovational, so original" [syn: {innovational}, {groundbreaking}]
