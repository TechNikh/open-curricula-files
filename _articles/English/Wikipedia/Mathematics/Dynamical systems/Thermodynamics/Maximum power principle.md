---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maximum_power_principle
offline_file: ""
offline_thumbnail: ""
uuid: 9b1473c0-33df-4e2f-b46a-484a1117a5c6
updated: 1484308846
title: Maximum power principle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-MaximumPowerESL.gif
tags:
    - History
    - Philosophy and theory
    - >
        Proposals for maximum power principle as 4th thermodynamic
        law
    - Definition in words
    - Mathematical definition
    - Contemporary ideas
    - Criticism
categories:
    - Thermodynamics
---
The maximum power principle has been proposed as the fourth principle of energetics in open system thermodynamics, where an example of an open system is a biological cell. According to Howard T. Odum (H.T.Odum 1995, p. 311), "The maximum power principle can be stated: During self-organization, system designs develop and prevail that maximize power intake, energy transformation, and those uses that reinforce production and efficiency."
