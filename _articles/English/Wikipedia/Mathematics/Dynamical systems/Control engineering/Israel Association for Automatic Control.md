---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Israel_Association_for_Automatic_Control
offline_file: ""
offline_thumbnail: ""
uuid: 436ece8f-6d90-423d-9b44-b26ca76f041e
updated: 1484308742
title: Israel Association for Automatic Control
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/660px-Flag_of_Israel.svg.png
categories:
    - Control engineering
---
The Israel Association for Automatic Control (IAAC) is a national member organization of the International Federation of Automatic Control (IFAC).[1] IAAC was one of the first member organizations of IFAC, and was founded in the early sixties by control system researchers from the Technion – Israel Institute of Technology, and from Rafael Advanced Defense Systems Ltd..
