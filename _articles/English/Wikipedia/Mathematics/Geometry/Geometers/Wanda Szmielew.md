---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wanda_Szmielew
offline_file: ""
offline_thumbnail: ""
uuid: 0a157795-c35b-492d-9f4f-8e281c7065c4
updated: 1484309103
title: Wanda Szmielew
categories:
    - Geometers
---
Wanda Montlak Szmielew (1918–1976) was a Polish mathematical logician who first proved the decidability of the first-order theory of abelian groups.[1]
