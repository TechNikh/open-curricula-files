---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Beltrami_vector_field
offline_file: ""
offline_thumbnail: ""
uuid: 6952678d-9e8b-4407-a222-e9328fa31d52
updated: 1484309354
title: Beltrami vector field
categories:
    - Geometry stubs
---
In vector calculus, a Beltrami vector field, named after Eugenio Beltrami, is a vector field in three dimensions that is parallel to its own curl. That is, F is a Beltrami vector field provided that
