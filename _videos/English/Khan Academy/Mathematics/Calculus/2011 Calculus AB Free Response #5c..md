---
version: 1
type: video
provider: YouTube
id: R-2Uw10QgIo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%235c..webm"
offline_file: ""
offline_thumbnail: ""
uuid: 001aa38d-3c71-497b-8955-60c0e3c06d5a
updated: 1486069692
title: '2011 Calculus AB Free Response #5c.'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/R-2Uw10QgIo/default.jpg
    - https://i3.ytimg.com/vi/R-2Uw10QgIo/1.jpg
    - https://i3.ytimg.com/vi/R-2Uw10QgIo/2.jpg
    - https://i3.ytimg.com/vi/R-2Uw10QgIo/3.jpg
---
