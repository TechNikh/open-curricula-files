---
version: 1
type: video
provider: YouTube
id: auQU-9KNG74
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Factoring%20and%20the%20Distributive%20Property.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 29a55709-7393-4971-9d1e-82e2aa451265
updated: 1486069559
title: Factoring and the Distributive Property
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/auQU-9KNG74/default.jpg
    - https://i3.ytimg.com/vi/auQU-9KNG74/1.jpg
    - https://i3.ytimg.com/vi/auQU-9KNG74/2.jpg
    - https://i3.ytimg.com/vi/auQU-9KNG74/3.jpg
---
