---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Network_Homophily
offline_file: ""
offline_thumbnail: ""
uuid: 4afb9208-ea33-48aa-bc13-b95f883753b9
updated: 1484309440
title: Network Homophily
tags:
    - Node attributes and homophily
    - Influence on network evolution
    - Long-run convergence
categories:
    - Network theory
---
Network homophily refers to the theory in network science which states that, based on node attributes, similar nodes may be more like to attach to each other than dissimilar ones. The hypothesis is linked to the model of preferential attachment and it draws from the phenomenon of homophily in social sciences and much of the scientific analysis of the creation of social ties based on similarity comes from network science.[1] In fact, empirical research seems to indicate the frequent occurrence of homophily in real networks. Homophily in social relations may lead to a commensurate distance in networks leading to the creation of clusters that have been observed in social networking ...
