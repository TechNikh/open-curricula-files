---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/connected
offline_file: ""
offline_thumbnail: ""
uuid: 02f98f30-6e33-462b-9d47-660e55e96248
updated: 1484310359
title: connected
categories:
    - Dictionary
---
connected
     adj 1: being joined in close association; "affiliated clubs"; "all
            art schools whether independent or attached to
            universities" [syn: {affiliated}, {attached}]
     2: joined or linked together [ant: {unconnected}]
     3: related to or accompanying; "Christmas and associated
        festivities" [syn: {associated}]
     4: wired together to an alarm system; "all the window alarms
        are connected"
     5: plugged in; "first check to see whether the appliance is
        connected"
     6: stored in, controlled by, or in direct communication with a
        central computer [syn: {machine-accessible}]
