---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revenue
offline_file: ""
offline_thumbnail: ""
uuid: a2601e1f-4724-47a8-80be-7d0eb19e2c9d
updated: 1484310480
title: revenue
categories:
    - Dictionary
---
revenue
     n 1: the entire amount of income before any deductions are made
          [syn: {gross}, {receipts}]
     2: government income due to taxation [syn: {tax income}, {taxation},
         {tax revenue}]
