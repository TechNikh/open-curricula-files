---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/generous
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413321
title: generous
categories:
    - Dictionary
---
generous
     adj 1: willing to give and share unstintingly; "a generous
            donation" [ant: {stingy}]
     2: not petty in character and mind; "unusually generous in his
        judgment of people" [ant: {ungenerous}]
     3: more than adequate; "a generous portion"
