---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Supersaturation
offline_file: ""
offline_thumbnail: ""
uuid: b428ec23-82b8-44a6-82c4-79207c88379a
updated: 1484308864
title: Supersaturation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rock-Candy-Sticks.jpg
tags:
    - Preparation
    - Phase change (crystallization and condensation)
    - Methodologies of measurement
    - History
    - Applications
categories:
    - Thermodynamics
---
Supersaturation is a state of a solution that contains more of the dissolved material than could be dissolved by the solvent under normal circumstances. It can also refer to a vapor of a compound that has a higher (partial) pressure than the vapor pressure of that compound.
