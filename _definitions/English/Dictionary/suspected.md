---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suspected
offline_file: ""
offline_thumbnail: ""
uuid: 8a02a174-3c33-45af-9fe8-46792c8270ea
updated: 1484310424
title: suspected
categories:
    - Dictionary
---
suspected
     adj : believed likely; "a suspected thief"; "a suspected
           infection" [ant: {unsuspected}]
