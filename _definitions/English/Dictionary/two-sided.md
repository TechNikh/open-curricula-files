---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/two-sided
offline_file: ""
offline_thumbnail: ""
uuid: 7cab5d38-abeb-48e6-8f25-dc1ab07ff021
updated: 1484310533
title: two-sided
categories:
    - Dictionary
---
two-sided
     adj 1: capable of being reversed or used with either side out; "a
            reversible jacket" [syn: {reversible}] [ant: {nonreversible}]
     2: having two sides or parts [syn: {bilateral}] [ant: {unilateral},
         {multilateral}]
