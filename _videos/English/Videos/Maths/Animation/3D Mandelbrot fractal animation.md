---
version: 1
type: video
provider: YouTube
id: 9M7XEbG9gWE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7b6aae61-c4b2-46b3-8407-a997f481f950
updated: 1486069649
title: 3D Mandelbrot fractal animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/9M7XEbG9gWE/default.jpg
    - https://i3.ytimg.com/vi/9M7XEbG9gWE/1.jpg
    - https://i3.ytimg.com/vi/9M7XEbG9gWE/2.jpg
    - https://i3.ytimg.com/vi/9M7XEbG9gWE/3.jpg
---
