---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soft-spoken
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484584561
title: soft-spoken
categories:
    - Dictionary
---
soft-spoken
     adj : having a speaking manner that is not loud or harsh; "she was
           always soft-spoken"
