---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Map-based_controller
offline_file: ""
offline_thumbnail: ""
uuid: a3e2276d-4010-4337-a974-64f50a0b6714
updated: 1484308735
title: Map-based controller
categories:
    - Control engineering
---
In the field of control engineering, a map-based controller is a controller whose outputs are based on values derived from a pre-defined lookup table. The inputs to the controller are usually values taken from one or more sensors and are used to index the output values in the lookup table. By effectively placing the transfer function as discrete entries within a lookup table, engineers free to modify smaller sections or update the whole list of entries as required.
