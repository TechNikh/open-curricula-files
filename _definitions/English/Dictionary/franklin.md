---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/franklin
offline_file: ""
offline_thumbnail: ""
uuid: 2b27850f-e04c-44b3-a4a3-7c774b7034d2
updated: 1484310295
title: franklin
categories:
    - Dictionary
---
Franklin
     n 1: United States historian noted for studies of Black American
          history (born in 1915) [syn: {John Hope Franklin}]
     2: printer whose success as an author led him to take up
        politics; he helped draw up the Declaration of
        Independence and the Constitution; he played a major role
        in the American Revolution and negotiated French support
        for the colonists; as a scientist he is remembered
        particularly for his research in electricity (1706-1790)
        [syn: {Benjamin Franklin}]
     3: a landowner (14th and 15th centuries) who was free but not
        of noble birth
