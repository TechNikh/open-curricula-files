---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Johannes_Hjelmslev
offline_file: ""
offline_thumbnail: ""
uuid: 29a30431-aea1-45c6-9cf3-82bf0567c244
updated: 1484309085
title: Johannes Hjelmslev
tags:
    - Publications
categories:
    - Geometers
---
Johannes Trolle Hjelmslev (Danish: [ˈjɛlˀmsleʊ]) (7 April 1873 – 16 February 1950) was a mathematician from Hørning, Denmark. Hjelmslev worked in geometry and history of geometry. He was the discoverer and eponym of the Hjelmslev transformation, a method for mapping an entire hyperbolic plane into a circle with a finite radius. He was the father of Louis Hjelmslev.
