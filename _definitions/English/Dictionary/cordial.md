---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cordial
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484474521
title: cordial
categories:
    - Dictionary
---
cordial
     adj 1: diffusing warmth and friendliness; "an affable smile"; "an
            amiable gathering"; "cordial relations"; "a cordial
            greeting"; "a genial host" [syn: {affable}, {amiable},
             {genial}]
     2: showing warm and heartfelt friendliness; "gave us a cordial
        reception"; "a hearty welcome" [syn: {hearty}]
     3: sincerely or intensely felt; "a cordial regard for his
        visitor's comfort"; "a cordial abhorrence of waste"; "a
        fervent hope" [syn: {fervent}]
     n : strong highly flavored sweet liquor usually drunk after a
         meal [syn: {liqueur}]
