---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dug
offline_file: ""
offline_thumbnail: ""
uuid: a56903bc-3e95-41c5-a27d-9adb84d5ad5e
updated: 1484310260
title: dug
categories:
    - Dictionary
---
dig
     n 1: the site of an archeological exploration; "they set up camp
          next to the dig" [syn: {excavation}, {archeological site}]
     2: an aggressive remark directed at a person like a missile and
        intended to have a telling effect; "his parting shot was
        `drop dead'"; "she threw shafts of sarcasm"; "she takes a
        dig at me every chance she gets" [syn: {shot}, {shaft}, {slam},
         {barb}, {jibe}, {gibe}]
     3: a small gouge (as in the cover of a book); "the book was in
        good condition except for a dig in the back cover"
     4: the act of digging; "there's an interesting excavation going
        on near Princeton" [syn: {excavation}, ...
