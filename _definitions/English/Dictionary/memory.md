---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/memory
offline_file: ""
offline_thumbnail: ""
uuid: 5794542a-7d24-41e5-bacd-cfdc8dcfa438
updated: 1484310395
title: memory
categories:
    - Dictionary
---
memory
     n 1: something that is remembered; "search as he would, the
          memory was lost"
     2: the cognitive processes whereby past experience is
        remembered; "he can do it from memory"; "he enjoyed
        remembering his father" [syn: {remembering}]
     3: the power of retaining and recalling past experience; "he
        had a good memory when he was younger" [syn: {retention},
        {retentiveness}]
     4: an electronic memory device; "a memory and the CPU form the
        central part of a computer to which peripherals are
        attached" [syn: {computer memory}, {storage}, {computer
        storage}, {store}, {memory board}]
     5: the area of cognitive ...
