---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eviction
offline_file: ""
offline_thumbnail: ""
uuid: 2192b80f-2900-43cc-bf6f-cedf90949c93
updated: 1484310477
title: eviction
categories:
    - Dictionary
---
eviction
     n 1: action by a landlord that compels a tenant to leave the
          premises (as by rendering the premises unfit for
          occupancy); no physical expulsion or legal process is
          involved [syn: {constructive eviction}]
     2: the expulsion of someone (such as a tenant) from the
        possession of land by process of law [syn: {dispossession},
         {legal ouster}]
