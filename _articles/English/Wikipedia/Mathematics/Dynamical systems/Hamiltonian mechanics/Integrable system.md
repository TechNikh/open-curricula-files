---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Integrable_system
offline_file: ""
offline_thumbnail: ""
uuid: c6eec0d4-6b9b-4fc2-a1ab-abd570b51f3a
updated: 1484308790
title: Integrable system
tags:
    - >
        Frobenius integrability (overdetermined differential
        systems)
    - General dynamical systems
    - Hamiltonian systems and Liouville integrability
    - Action-angle variables
    - The Hamilton–Jacobi approach
    - Solitons and inverse spectral methods
    - Quantum integrable systems
    - Exactly solvable models
    - List of some well-known classical integrable systems
    - Notes
categories:
    - Hamiltonian mechanics
---
In the general theory of differential systems, there is Frobenius integrability, which refers to overdetermined systems. In the classical theory of Hamiltonian dynamical systems, there is the notion of Liouville integrability. This causes the trajectories to be fixed to smaller submanifolds allowing the solution to be expressed with a sequence of integrals (the origin of the name integrable). More generally, in differentiable dynamical systems integrability relates to the existence of foliations by invariant submanifolds within the phase space. Each of these notions involves an application of the idea of foliations, but they do not coincide. There are also notions of complete integrability, ...
