---
version: 1
type: video
provider: YouTube
id: Nhn-anmubYU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra-%20Slope%20and%20Y-intercept%20intuition.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 823e2c57-874a-4197-a8c4-646c2b74292a
updated: 1486069571
title: 'Algebra- Slope and Y-intercept intuition'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/Nhn-anmubYU/default.jpg
    - https://i3.ytimg.com/vi/Nhn-anmubYU/1.jpg
    - https://i3.ytimg.com/vi/Nhn-anmubYU/2.jpg
    - https://i3.ytimg.com/vi/Nhn-anmubYU/3.jpg
---
Getting a feel for slope and y-intercept
More free lessons at: http://www.khanacademy.org/video?v=Nhn-anmubYU
