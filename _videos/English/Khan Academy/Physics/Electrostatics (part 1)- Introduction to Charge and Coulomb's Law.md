---
version: 1
type: video
provider: YouTube
id: NXMgvrS8Gr8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Electrostatics%20%28part%201%29-%20Introduction%20to%20Charge%20and%20Coulomb%27s%20Law.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8e874847-bfaa-472f-9931-05fdee531bb5
updated: 1486069593
title: "Electrostatics (part 1)- Introduction to Charge and Coulomb's Law"
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/NXMgvrS8Gr8/default.jpg
    - https://i3.ytimg.com/vi/NXMgvrS8Gr8/1.jpg
    - https://i3.ytimg.com/vi/NXMgvrS8Gr8/2.jpg
    - https://i3.ytimg.com/vi/NXMgvrS8Gr8/3.jpg
---
Introduction to Charge and Coulomb's Law (video from May 2008 that I forgot to upload)
More free lessons at: http://www.khanacademy.org/video?v=NXMgvrS8Gr8
