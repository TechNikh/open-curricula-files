---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imposed
offline_file: ""
offline_thumbnail: ""
uuid: de6d705c-a1d9-4b72-8c39-5a3441a2cf92
updated: 1484310246
title: imposed
categories:
    - Dictionary
---
imposed
     adj : set forth authoritatively as obligatory; "the imposed
           taxation"; "rules imposed by society"
