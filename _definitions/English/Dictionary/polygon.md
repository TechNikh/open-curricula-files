---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polygon
offline_file: ""
offline_thumbnail: ""
uuid: ad0c9f4c-5f28-4e13-8bda-c0b860181f94
updated: 1484310154
title: polygon
categories:
    - Dictionary
---
polygon
     n : a closed plane figure bounded by straight sides [syn: {polygonal
         shape}]
