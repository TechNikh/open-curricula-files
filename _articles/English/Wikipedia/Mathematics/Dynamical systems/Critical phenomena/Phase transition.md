---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phase_transition
offline_file: ""
offline_thumbnail: ""
uuid: 2ae7c302-2e38-45c8-b4f5-845330f929c7
updated: 1484308751
title: Phase transition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/340px-Phase_change_-_en.svg.png
tags:
    - Types of phase transition
    - Classifications
    - Ehrenfest classification
    - Modern classifications
    - Characteristic properties
    - Phase coexistence
    - Critical points
    - Symmetry
    - Order parameters
    - Relevance in cosmology
    - Critical exponents and universality classes
    - Critical slowing down and other phenomena
    - Percolation Theory
    - Phase transitions in biological systems
categories:
    - Critical phenomena
---
The term phase transition is most commonly used to describe transitions between solid, liquid and gaseous states of matter, and, in rare cases, plasma. A phase of a thermodynamic system and the states of matter have uniform physical properties. During a phase transition of a given medium certain properties of the medium change, often discontinuously, as a result of the change of some external condition, such as temperature, pressure, or others. For example, a liquid may become gas upon heating to the boiling point, resulting in an abrupt change in volume. The measurement of the external conditions at which the transformation occurs is termed the phase transition. Phase transitions are ...
