---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Level_ancestor_problem
offline_file: ""
offline_thumbnail: ""
uuid: 8bf3a8f1-254e-488c-97d2-d19c72dec50f
updated: 1484308674
title: Level ancestor problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-LevelAncestor.png
tags:
    - Naive methods
    - Jump pointer algorithm
    - Ladder algorithm
    - 'Stage 1: long-path decomposition'
    - 'Stage 2: extending the long paths into ladders'
    - 'Stage 3: combining the two approaches'
    - "Berkman and Vishkin's Solution"
categories:
    - Theoretical computer science
---
In graph theory and theoretical computer science, the level ancestor problem is the problem of preprocessing a given rooted tree T into a data structure that can determine the ancestor of a given node at a given distance from the root of the tree.
