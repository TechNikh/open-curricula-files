---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amino
offline_file: ""
offline_thumbnail: ""
uuid: 17d81430-67a2-437f-be9f-ed6b2dfbefe6
updated: 1484310315
title: amino
categories:
    - Dictionary
---
amino
     adj : pertaining to or containing any of a group of organic
           compounds of nitrogen derived from ammonia [syn: {aminic}]
     n : the radical -NH2 [syn: {amino group}]
