---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/influential
offline_file: ""
offline_thumbnail: ""
uuid: 4b2da77b-c36c-495f-8d74-50081add8598
updated: 1484310413
title: influential
categories:
    - Dictionary
---
influential
     adj : having or exercising influence or power; "an influential
           newspaper"; "influential leadership for peace" [ant: {uninfluential}]
