---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/General_covariant_transformations
offline_file: ""
offline_thumbnail: ""
uuid: 3f4dc214-797f-42da-b80b-9fc644dabe5c
updated: 1484309219
title: General covariant transformations
categories:
    - Differential geometry
---
In physics, general covariant transformations are symmetries of gravitation theory on a world manifold 
  
    
      
        X
      
    
    {\displaystyle X}
  
. They are gauge transformations whose parameter functions are vector fields on 
  
    
      
        X
      
    
    {\displaystyle X}
  
. From the physical viewpoint, general covariant transformations are treated as particular (holonomic) reference frame transformations in general relativity. In mathematics, general covariant transformations are defined as particular automorphisms of so-called natural fiber bundles.
