---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Knuth%27s_Simpath_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 9c1ba4bc-03fe-46bf-97e5-e3e1ee002e22
updated: 1484308675
title: "Knuth's Simpath algorithm"
categories:
    - Theoretical computer science
---
Simpath is an algorithm introduced by Donald Knuth that constructs a zero-suppressed decision diagram (ZDD) representing all simple paths between two vertices in a given graph.[1][2]
