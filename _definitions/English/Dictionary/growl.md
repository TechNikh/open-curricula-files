---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/growl
offline_file: ""
offline_thumbnail: ""
uuid: e6668e8d-eb2d-47b7-8f05-d4e8927b690b
updated: 1484310537
title: growl
categories:
    - Dictionary
---
growl
     n : the sound of growling (as made by animals) [syn: {growling}]
     v : to utter or emit low dull rumbling sounds; "he grumbled a
         rude response"; "Stones grumbled down the cliff" [syn: {grumble},
          {rumble}]
