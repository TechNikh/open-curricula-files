---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Angle_bisector_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 0779bd58-a973-4f8a-a3b1-fcf772f24266
updated: 1484308924
title: Angle bisector theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Triangle_ABC_with_bisector_AD.svg.png
tags:
    - Theorem
    - Proofs
    - Proof 1
    - Proof 2
    - History
categories:
    - Elementary geometry
---
In geometry, the angle bisector theorem is concerned with the relative lengths of the two segments that a triangle's side is divided into by a line that bisects the opposite angle. It equates their relative lengths to the relative lengths of the other two sides of the triangle.
