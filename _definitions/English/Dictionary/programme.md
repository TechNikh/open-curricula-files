---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/programme
offline_file: ""
offline_thumbnail: ""
uuid: ff6c205b-2592-4b33-8360-523390c0eeef
updated: 1484310252
title: programme
categories:
    - Dictionary
---
programme
     n 1: an announcement of the events that will occur as part of a
          theatrical or sporting event; "you can't tell the
          players without a program" [syn: {program}]
     2: an integrated course of academic studies; "he was admitted
        to a new program at the university" [syn: {course of study},
         {program}, {curriculum}, {syllabus}]
     3: a radio or television show; "did you see his program last
        night?" [syn: {broadcast}, {program}]
     4: (computer science) a sequence of instructions that a
        computer can interpret and execute; "the program required
        several hundred lines of code" [syn: {program}, {computer
        program}, ...
