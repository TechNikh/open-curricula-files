---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rendezvous_problem
offline_file: ""
offline_thumbnail: ""
uuid: e8d28f49-c279-4c30-8223-013dbe60f9df
updated: 1484308524
title: Rendezvous problem
categories:
    - Cooperative games
---
If they both choose to wait, of course, they will never meet. If they both choose to walk there are chances that they meet and chances that they do not. If one chooses to wait and the other chooses to walk, then there is a theoretical certainty that they will meet eventually; in practice, though, it may take too long for it to be guaranteed. The question posed, then, is: what strategies should they choose to maximize their probability of meeting?
