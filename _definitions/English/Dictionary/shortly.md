---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shortly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484647441
title: shortly
categories:
    - Dictionary
---
shortly
     adv 1: for a short time; "he was at the airport shortly before she
            was expected to arrive" [syn: {not long}]
     2: in the near future; "the doctor will soon be here"; "the
        book will appear shortly"; "she will arrive presently";
        "we should have news before long" [syn: {soon}, {presently},
         {before long}]
     3: in a curt, abrupt and discourteous manner; "he told me
        curtly to get on with it"; "he talked short with
        everyone"; "he said shortly that he didn't like it" [syn:
        {curtly}, {short}]
     4: in a concise manner; in a few words; "the history is summed
        up concisely in this book"; "she replied briefly";
    ...
