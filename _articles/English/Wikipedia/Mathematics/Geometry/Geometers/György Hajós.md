---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gy%C3%B6rgy_Haj%C3%B3s'
offline_file: ""
offline_thumbnail: ""
uuid: 840a13e3-f8ef-4442-bfd5-47607532489c
updated: 1484309083
title: György Hajós
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Haj%25C3%25B3s_Gy%25C3%25B6rgy.png'
tags:
    - Biography
    - Research
    - Awards and honors
categories:
    - Geometers
---
György Hajós (February 21, 1912, Budapest – March 17, 1972, Budapest) was a Hungarian mathematician who worked in group theory, graph theory, and geometry.[1][2]
