---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Triskelion
offline_file: ""
offline_thumbnail: ""
uuid: c1895503-1ade-4511-9c80-3fb86fd06935
updated: 1484309322
title: Triskelion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Broken_crossed_circle_2.svg.png
tags:
    - Neolithic, Bronze Age, and Iron Age use in Europe
    - Asian usage
    - Modern usage
    - Reconstructionists and neopagans
    - Occurrence in nature
    - Gallery
categories:
    - Symmetry
---
A triskelion or triskele is a motif consisting of a triple spiral exhibiting rotational symmetry. The spiral design can be based on interlocking Archimedean spirals, or represent three bent human legs.
