---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bishop
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484333581
title: bishop
categories:
    - Dictionary
---
bishop
     n 1: a clergyman having spiritual and administrative authority;
          appointed in Christian churches to oversee priests or
          ministers; considered in some churches to be successors
          of the twelve apostles of Christ
     2: port wine mulled with oranges and cloves
     3: (chess) a piece that can be moved diagonally over unoccupied
        squares of the same color
