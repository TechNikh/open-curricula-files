---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/framed
offline_file: ""
offline_thumbnail: ""
uuid: 11ba7eec-e1b0-4127-bf34-20801ad23e53
updated: 1484310405
title: framed
categories:
    - Dictionary
---
framed
     adj : provided with a frame; "there were framed snapshots of
           family and friends on her desk" [ant: {unframed}]
