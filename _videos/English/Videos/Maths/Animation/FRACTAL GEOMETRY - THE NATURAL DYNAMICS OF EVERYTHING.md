---
version: 1
type: video
provider: YouTube
id: yUM7e0tIFi0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1ff1edbb-b91b-433f-a8d1-578dc3e6e4d3
updated: 1486069645
title: 'FRACTAL GEOMETRY - THE NATURAL DYNAMICS OF EVERYTHING'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/yUM7e0tIFi0/default.jpg
    - https://i3.ytimg.com/vi/yUM7e0tIFi0/1.jpg
    - https://i3.ytimg.com/vi/yUM7e0tIFi0/2.jpg
    - https://i3.ytimg.com/vi/yUM7e0tIFi0/3.jpg
---
