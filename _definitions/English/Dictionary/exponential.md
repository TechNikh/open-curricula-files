---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exponential
offline_file: ""
offline_thumbnail: ""
uuid: 2bcf9247-74bc-4656-970e-3702b752b0e9
updated: 1484310140
title: exponential
categories:
    - Dictionary
---
exponential
     adj : of or involving exponents; "exponential growth"
     n : a function in which an independent variable appears as an
         exponent [syn: {exponential function}]
