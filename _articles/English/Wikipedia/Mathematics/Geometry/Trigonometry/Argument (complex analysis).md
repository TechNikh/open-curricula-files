---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Argument_(complex_analysis)
offline_file: ""
offline_thumbnail: ""
uuid: 025e90ac-80de-4aaa-af67-f3a16557699c
updated: 1484309340
title: Argument (complex analysis)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Complex_number_illustration_modarg.svg.png
tags:
    - Definition
    - Principal value
    - Notation
    - Covering space
    - Computation
    - Identities
    - Example
    - Notes
    - Bibliography
categories:
    - Trigonometry
---
In mathematics, arg is a function operating on complex numbers (visualized in a complex plane). It gives the angle between the positive real axis to the line joining the point to the origin, shown as φ in figure 1, known as an argument of the point.
