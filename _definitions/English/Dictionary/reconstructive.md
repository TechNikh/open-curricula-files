---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reconstructive
offline_file: ""
offline_thumbnail: ""
uuid: 7d3d01e3-55a0-4d03-8fab-b5f758173ac1
updated: 1484310168
title: reconstructive
categories:
    - Dictionary
---
reconstructive
     adj : helping to restore to good condition; "reconstructive
           surgery"; "rehabilitative exercises" [syn: {rehabilitative}]
