---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bm
offline_file: ""
offline_thumbnail: ""
uuid: 22ab715c-025d-45cf-84eb-693217117cad
updated: 1484310281
title: bm
categories:
    - Dictionary
---
BM
     n 1: solid excretory product evacuated from the bowels [syn: {fecal
          matter}, {faecal matter}, {feces}, {faeces}, {stool}, {ordure},
           {dejection}]
     2: a euphemism for defecation; "he had a bowel movement" [syn:
        {bowel movement}, {movement}]
