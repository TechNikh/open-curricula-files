---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sleepy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484434741
title: sleepy
categories:
    - Dictionary
---
sleepy
     adj : ready to fall asleep; "beginning to feel sleepy"; "a
           sleepy-eyed child with drooping eyelids"; "sleepyheaded
           students" [syn: {sleepy-eyed}, {sleepyheaded}]
     [also: {sleepiest}, {sleepier}]
