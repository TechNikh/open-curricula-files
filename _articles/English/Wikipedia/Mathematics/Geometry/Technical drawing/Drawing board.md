---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Drawing_board
offline_file: ""
offline_thumbnail: ""
uuid: 19a2ccaf-d9d5-4074-b14c-d81156423aaf
updated: 1484309325
title: Drawing board
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Architect.png
tags:
    - Modern-day idiom
    - Contemporary drafting tables
categories:
    - Technical drawing
---
A drawing board (also drawing table, drafting table or architect's table) is, in its antique form, a kind of multipurpose desk which can be used for any kind of drawing, writing or impromptu sketching on a large sheet of paper or for reading a large format book or other oversized document or for drafting precise technical illustrations. The drawing table used to be a frequent companion to a pedestal desk in a gentleman's study or private library, during the pre-industrial and early industrial era.
