---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liberation
offline_file: ""
offline_thumbnail: ""
uuid: aff50084-624e-49d5-99b8-c2a6f1dc2841
updated: 1484310371
title: liberation
categories:
    - Dictionary
---
liberation
     n 1: the act of liberating someone or something [syn: {release},
          {freeing}]
     2: the attempt to achieve equal rights or status; "she worked
        for women's liberation"
     3: the termination of someone's employment (leaving them free
        to depart) [syn: {dismissal}, {dismission}, {discharge}, {firing},
         {release}, {sack}, {sacking}]
