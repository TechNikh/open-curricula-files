---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pull
offline_file: ""
offline_thumbnail: ""
uuid: b5b7e108-fd78-4229-abb4-acf280c9691a
updated: 1484310426
title: pull
categories:
    - Dictionary
---
pull
     n 1: the act of pulling; applying force to move something toward
          or with you; "the pull up the hill had him breathing
          harder"; "his strenuous pulling strained his back" [syn:
           {pulling}]
     2: the force used in pulling; "the pull of the moon"; "the pull
        of the current"
     3: special advantage or influence; "the chairman's nephew has a
        lot of pull" [syn: {clout}]
     4: a device used for pulling something; "he grabbed the pull
        and opened the drawer"
     5: a sharp strain on muscles or ligaments; "the wrench to his
        knee occurred as he fell"; "he was sidelined with a
        hamstring pull" [syn: {wrench}, {twist}]
  ...
