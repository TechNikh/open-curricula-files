---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/affecting
offline_file: ""
offline_thumbnail: ""
uuid: 754c3ee0-e97e-456f-9b66-d2cb6683508a
updated: 1484310273
title: affecting
categories:
    - Dictionary
---
affecting
     adj : arousing affect; "the homecoming of the released hostages
           was an affecting scene"; "poignant grief cannot endure
           forever"; "his gratitude was simple and touching" [syn:
            {poignant}, {touching}]
