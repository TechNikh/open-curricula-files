---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flap
offline_file: ""
offline_thumbnail: ""
uuid: 32936982-c1cf-4906-bc16-f2c7b78cd54e
updated: 1484310313
title: flap
categories:
    - Dictionary
---
flap
     n 1: any broad thin and limber covering attached at one edge;
          hangs loose or projects freely; "he wrote on the flap of
          the envelope"
     2: an excited state of agitation; "he was in a dither"; "there
        was a terrible flap about the theft" [syn: {dither}, {pother},
         {fuss}, {tizzy}]
     3: the motion made by flapping up and down [syn: {flapping}, {flutter},
         {fluttering}]
     4: a movable piece of tissue partly connected to the body
     5: a movable airfoil that is part of an aircraft wing; used to
        increase lift or drag [syn: {flaps}]
     v 1: move in a wavy pattern or with a rising and falling motion;
          "The curtains ...
