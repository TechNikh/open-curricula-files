---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spell
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484555641
title: spell
categories:
    - Dictionary
---
spell
     n 1: a psychological state induced by (or as if induced by) a
          magical incantation [syn: {enchantment}, {trance}]
     2: a time for working (after which you will be relieved by
        someone else); "it's my go"; "a spell of work" [syn: {go},
         {tour}, {turn}]
     3: a period of indeterminate length (usually short) marked by
        some action or condition; "he was here for a little
        while"; "I need to rest for a piece"; "a spell of good
        weather"; "a patch of bad weather" [syn: {while}, {piece},
         {patch}]
     4: a verbal formula believed to have magical force; "he
        whispered a spell as he moved his hands"; "inscribed
        ...
