---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tribe
offline_file: ""
offline_thumbnail: ""
uuid: 9fc90f04-c5bb-418c-b479-c747cefc2db3
updated: 1484310244
title: tribe
categories:
    - Dictionary
---
tribe
     n 1: a social division of (usually preliterate) people [syn: {folk}]
     2: a federation (as of American Indians) [syn: {federation of
        tribes}]
     3: (biology) a taxonomic category between a genus and a
        subfamily
     4: group of people related by blood or marriage [syn: {kin}, {kin
        group}, {kinship group}, {kindred}, {clan}]
