---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ice_circle
offline_file: ""
offline_thumbnail: ""
uuid: 7fca2fe3-2879-439e-b977-60404e0dacc5
updated: 1484308878
title: Ice circle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Ice_circle_long_exposure.JPG
tags:
    - Ice discs
    - Ice pans
categories:
    - Vortices
---
Ice circles are thin and circular slabs of ice that rotate slowly in the water. It is believed[citation needed] that they form in eddy currents. Ice discs have most frequently been observed in Scandinavia and North America, but they are occasionally recorded as far south as England and Wales. An ice disc was observed in Wales in December 2008 and another was reported in England in January 2009.[1][2][3] An ice disc was observed on the Sheyenne River in North Dakota in December 2013. An ice circle of approximately 50 ft. in diameter was observed and photographed in Lake Katrine, New York on the Esopus Creek around January 23, 2014. In Idaho, extreme weather led to a rare sighting of an Ice ...
