---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/represented
offline_file: ""
offline_thumbnail: ""
uuid: 5fd71991-061b-4934-b3f0-f636005a7031
updated: 1484310305
title: represented
categories:
    - Dictionary
---
represented
     adj : represented accurately or precisely [syn: {delineated}, {delineate}]
           [ant: {undelineated}]
