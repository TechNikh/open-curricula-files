---
version: 1
type: video
provider: YouTube
id: bMYL24MP8JY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Winter%20in%20the%20Glass%21%20Christmassy%20Chemical%20Experiment%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 59750c35-2698-476d-8cd5-3430346c2221
updated: 1486069661
title: Winter in the Glass! Christmassy Chemical Experiment!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/bMYL24MP8JY/default.jpg
    - https://i3.ytimg.com/vi/bMYL24MP8JY/1.jpg
    - https://i3.ytimg.com/vi/bMYL24MP8JY/2.jpg
    - https://i3.ytimg.com/vi/bMYL24MP8JY/3.jpg
---
