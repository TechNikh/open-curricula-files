---
version: 1
type: video
provider: YouTube
id: pnPWdDOR9XE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Buffers%2C%20titrations%2C%20and%20solubility%20equilibria/Buffer%20capacity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8dd3fd96-98fc-4317-9a50-3c8a6be94e3d
updated: 1486069587
title: Buffer capacity
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Buffers, titrations, and solubility equilibria
thumbnail_urls:
    - https://i3.ytimg.com/vi/pnPWdDOR9XE/default.jpg
    - https://i3.ytimg.com/vi/pnPWdDOR9XE/1.jpg
    - https://i3.ytimg.com/vi/pnPWdDOR9XE/2.jpg
    - https://i3.ytimg.com/vi/pnPWdDOR9XE/3.jpg
---
The definition of buffer capacity, and an example showing why it depends on the absolute concentrations of the conjugate acid and base.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/acid-base-equilibrium/buffer-solutions/v/ways-to-get-a-buffer-solution?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/acid-base-equilibrium/buffer-solutions/v/ph-and-pka-relationship-for-buffers?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
