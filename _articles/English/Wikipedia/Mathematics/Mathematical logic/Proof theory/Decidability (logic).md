---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Decidability_(logic)
offline_file: ""
offline_thumbnail: ""
uuid: 0404a802-6c39-43c3-b663-3eaa100cdfb9
updated: 1484309492
title: Decidability (logic)
tags:
    - Relationship to computability
    - Decidability of a logical system
    - Decidability of a theory
    - Some decidable theories
    - Some undecidable theories
    - Semidecidability
    - Relationship with completeness
    - Notes
    - Bibliography
categories:
    - Proof theory
---
In logic, the term decidable refers to the decision problem, the question of the existence of an effective method for determining membership in a set of formulas, or, more precisely, an algorithm that can and will return a Boolean true or false value (instead of looping indefinitely). Logical systems such as propositional logic are decidable if membership in their set of logically valid formulas (or theorems) can be effectively determined. A theory (set of sentences closed under logical consequence) in a fixed logical system is decidable if there is an effective method for determining whether arbitrary formulas are included in the theory. Many important problems are undecidable, that is, it ...
