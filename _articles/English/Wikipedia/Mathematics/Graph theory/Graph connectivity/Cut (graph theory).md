---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cut_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: af7c08e4-f4ba-4ea0-b3f7-8392374e5a3e
updated: 1484309400
title: Cut (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Min-cut.svg.png
tags:
    - Definition
    - Minimum cut
    - Maximum cut
    - Sparsest cut
    - Cut space
categories:
    - Graph connectivity
---
In graph theory, a cut is a partition of the vertices of a graph into two disjoint subsets. Any cut determines a cut-set, the set of edges that have one endpoint in each subset of the partition. These edges are said to cross the cut. In a connected graph, each cut-set determines a unique cut, and in some cases cuts are identified with their cut-sets rather than with their vertex partitions.
