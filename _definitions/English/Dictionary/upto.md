---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upto
offline_file: ""
offline_thumbnail: ""
uuid: 0f4d1752-fc49-4bdf-b41f-a7cbdafb3b0e
updated: 1484310226
title: upto
categories:
    - Dictionary
---
up to
     adj 1: busy or occupied with; "what have you been up to?"; "up to
            no good"
     2: having the requisite qualities for; "equal to the task";
        "the work isn't up to the standard I require" [syn: {adequate
        to(p)}, {capable}, {equal to(p)}, {up to(p)}]
