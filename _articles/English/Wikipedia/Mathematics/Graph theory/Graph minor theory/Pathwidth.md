---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pathwidth
offline_file: ""
offline_thumbnail: ""
uuid: 8c732560-d66c-4888-9f9e-5b1da5f8d020
updated: 1484309417
title: Pathwidth
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Pathwidth.JPG
tags:
    - Definition
    - Alternative characterizations
    - Gluing sequences
    - Interval thickness
    - Vertex separation number
    - Node searching number
    - Bounds
    - Computing path-decompositions
    - Fixed-parameter tractability
    - Special classes of graphs
    - Approximation algorithms
    - Graph minors
    - Excluding a forest
    - Obstructions to bounded pathwidth
    - Structure theory
    - Applications
    - VLSI
    - Graph drawing
    - Compiler design
    - linguistics
    - Exponential algorithms
    - Notes
categories:
    - Graph minor theory
---
In graph theory, a path decomposition of a graph G is, informally, a representation of G as a "thickened" path graph,[1] and the pathwidth of G is a number that measures how much the path was thickened to form G. More formally, a path-decomposition is a sequence of subsets of vertices of G such that the endpoints of each edge appear in one of the subsets and such that each vertex appears in a contiguous subsequence of the subsets,[2] and the pathwidth is one less than the size of the largest set in such a decomposition. Pathwidth is also known as interval thickness (one less than the maximum clique size in an interval supergraph of G), vertex separation number, or node searching number.[3]
