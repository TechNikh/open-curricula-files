---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bureaucratic
offline_file: ""
offline_thumbnail: ""
uuid: ac7aa1ad-155e-4edb-b779-baa447ca522f
updated: 1484310609
title: bureaucratic
categories:
    - Dictionary
---
bureaucratic
     adj : of or relating to or resembling a bureaucrat or bureaucracy;
           "his bureaucratic behavior annoyed his colleagues"; "a
           bureaucratic nightmare"
