---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Srivastava_code
offline_file: ""
offline_thumbnail: ""
uuid: 06887072-4087-42be-afde-c6087c23eab2
updated: 1484308556
title: Srivastava code
categories:
    - Coding theory
---
In coding theory, Srivastava codes, formulated by Professor J. N. Srivastava, form a class of parameterised error-correcting codes which are a special case of alternant codes.
