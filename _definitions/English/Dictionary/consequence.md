---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consequence
offline_file: ""
offline_thumbnail: ""
uuid: 1e31f91e-1ea8-4943-b7b6-c28febdf1309
updated: 1484310323
title: consequence
categories:
    - Dictionary
---
consequence
     n 1: a phenomenon that follows and is caused by some previous
          phenomenon; "the magnetic effect was greater when the
          rod was lengthwise"; "his decision had depressing
          consequences for business"; "he acted very wise after
          the event" [syn: {effect}, {outcome}, {result}, {event},
           {issue}, {upshot}]
     2: the outcome of an event especially as relative to an
        individual; "that result is of no consequence" [syn: {aftermath}]
     3: having important effects or influence; "decisions of great
        consequence are made by the president himself"; "virtue is
        of more moment that security" [syn: {import}, {moment}]
   ...
