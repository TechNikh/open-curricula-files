---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mathematical_fallacy
offline_file: ""
offline_thumbnail: ""
uuid: 3260e6a6-d1e6-45e9-8867-74d3157f64af
updated: 1484309495
title: Mathematical fallacy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fallacy_of_the_isosceles_triangle2.svg.png
tags:
    - Howlers
    - Division by zero
    - Multivalued functions
    - calculus
    - Power and root
    - Positive and negative roots
    - Squaring both sides of an equation
    - Square roots of negative numbers
    - Complex exponents
    - Geometry
    - Fallacy of the isosceles triangle
    - Proof by induction
    - Notes
categories:
    - Proof theory
---
In mathematics, certain kinds of mistaken proof are often exhibited, and sometimes collected, as illustrations of a concept of mathematical fallacy. There is a distinction between a simple mistake and a mathematical fallacy in a proof: a mistake in a proof leads to an invalid proof just in the same way, but in the best-known examples of mathematical fallacies, there is some concealment in the presentation of the proof. For example, the reason validity fails may be a division by zero that is hidden by algebraic notation. There is a striking quality of the mathematical fallacy: as typically presented, it leads not only to an absurd result, but does so in a crafty or clever way.[1] Therefore, ...
