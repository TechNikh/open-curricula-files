---
version: 1
type: video
provider: YouTube
id: YjT3QYfoy4Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Graphing%20systems%20of%20inequalities%20and%20checking%20solutions%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e2a5fc5c-6819-4c96-bd1f-19fb7471306f
updated: 1486069563
title: >
    Graphing systems of inequalities and checking solutions
    example
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/YjT3QYfoy4Q/default.jpg
    - https://i3.ytimg.com/vi/YjT3QYfoy4Q/1.jpg
    - https://i3.ytimg.com/vi/YjT3QYfoy4Q/2.jpg
    - https://i3.ytimg.com/vi/YjT3QYfoy4Q/3.jpg
---
