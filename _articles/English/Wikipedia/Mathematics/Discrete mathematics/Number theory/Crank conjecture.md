---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crank_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: b68185bf-9e25-40b9-9060-a3fb379fc2a9
updated: 1484308635
title: Crank conjecture
categories:
    - Number theory
---
In mathematics, the crank conjecture was a conjecture about the existence of the crank of a partition that separates partitions of a number congruent to 6 mod 11 into 11 equal classes. The conjecture was introduced by Dyson (1944) and proved by Andrews and Garvan (1987).
