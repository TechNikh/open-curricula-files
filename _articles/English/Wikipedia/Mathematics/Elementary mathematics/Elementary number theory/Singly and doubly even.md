---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Singly_and_doubly_even
offline_file: ""
offline_thumbnail: ""
uuid: 0dad3b4f-76c2-49f9-8bcf-31d2c9281440
updated: 1484308944
title: Singly and doubly even
tags:
    - Definitions
    - Applications
    - Safer outs in darts
    - Irrationality of √2
    - Geometric topology
    - Other appearances
    - Related classifications
categories:
    - Elementary number theory
---
In mathematics an even integer, that is, a number that is divisible by 2, is called evenly even or doubly even if it is a multiple of 4, and oddly even or singly even if it is not. (The former names are traditional ones, derived from the ancient Greek; the latter have become common in recent decades.)
