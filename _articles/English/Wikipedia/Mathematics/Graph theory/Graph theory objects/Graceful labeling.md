---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graceful_labeling
offline_file: ""
offline_thumbnail: ""
uuid: 448bb2a8-05a6-4ab3-8c78-cb75cd3cb992
updated: 1484309447
title: Graceful labeling
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Graceful_labeling.svg.png
tags:
    - Selected results
    - Additional reading
categories:
    - Graph theory objects
---
In graph theory, a graceful labeling of a graph with m edges is a labeling of its vertices with some subset of the integers between 0 and m inclusive, such that no two vertices share a label, and such that each edge is uniquely identified by the positive, or absolute difference between its endpoints.[1] A graph which admits a graceful labeling is called a graceful graph.
