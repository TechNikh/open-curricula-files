---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Divali
offline_file: ""
offline_thumbnail: ""
uuid: ee15b286-774a-4140-a73f-fcb56ad6283e
updated: 1484308470
title: Diwali
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ravi_Varma-Lakshmi_0.jpg
tags:
    - Etymology
    - History
    - Significance
    - Spiritual significance
    - Hinduism
    - Sikhism
    - Jainism
    - Buddhism
    - Description and rituals
    - Dhanteras (Day 1)
categories:
    - Uncategorized
---
Diwali or Deepavali is the Hindu festival of lights celebrated every year in autumn in the northern hemisphere (spring in southern hemisphere).[5][6] It is an official holiday in Fiji, Guyana, India,[7] Malaysia, Mauritius, Myanmar, Nepal, Singapore, Sri Lanka, Suriname, Trinidad and Tobago and recently Sindh Province in Pakistan. One of the major festivals of Hinduism, it spiritually signifies the victory of light over darkness, good over evil, knowledge over ignorance, and hope over despair.[8][9][10] Its celebration includes millions of lights shining on housetops, outside doors and windows, around temples and other buildings in the communities and countries where it is observed.[11] The ...
