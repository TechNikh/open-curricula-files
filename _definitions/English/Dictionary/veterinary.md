---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/veterinary
offline_file: ""
offline_thumbnail: ""
uuid: 206f24c7-0138-4691-a637-cf08ab97c0b7
updated: 1484310453
title: veterinary
categories:
    - Dictionary
---
veterinary
     adj : of or relating to veterinarians or veterinary medicine
     n : a doctor who practices veterinary medicine [syn: {veterinarian},
          {veterinary surgeon}, {vet}]
