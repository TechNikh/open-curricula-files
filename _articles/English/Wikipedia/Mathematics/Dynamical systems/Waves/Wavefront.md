---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wavefront
offline_file: ""
offline_thumbnail: ""
uuid: 3360fece-cf06-490b-8f1a-2b36b97784c9
updated: 1484308905
title: Wavefront
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Plane_wave_wavefronts_3D.svg.png
tags:
    - Simple wavefronts and propagation
    - Wavefront aberrations
    - Wavefront sensor and reconstruction techniques
    - Textbooks
    - Journals
categories:
    - Waves
---
In physics, a wavefront is the locus of points characterized by propagation of position of the same phase: a propagation of a line in 1d, a curve in 2d or a surface for a wave in 3d.[1] Since infrared, optical, x-ray and gamma-ray frequencies are so high, the temporal component of electromagnetic waves is usually ignored at these wavelengths, and it is only the phase of the spatial oscillation that is described. Additionally, most optical systems and detectors are indifferent to polarization, so this property of the wave is also usually ignored. At radio wavelengths, the polarization becomes more important, and receivers are usually phase-sensitive. Many audio detectors are also ...
