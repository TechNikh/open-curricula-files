---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ent
offline_file: ""
offline_thumbnail: ""
uuid: 84d49570-33bb-4c41-bd1f-2a490dc94833
updated: 1484308312
title: Ent
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-TREEBEARD.jpg
tags:
    - Etymology
    - Description
    - Internal history
    - First Age
    - Entwives
    - Entings
    - Fate
    - The Last March of the Ents
    - Named Ents
    - In popular culture
    - Ents in music
    - Ents in television
    - Statue
    - Similar creatures
categories:
    - Uncategorized
---
Ents are a race of beings in J. R. R. Tolkien's fantasy world Middle-earth who closely resemble trees. They are similar to the talking trees in folklore around the world. Their name is derived from the Anglo-Saxon word for giant.
