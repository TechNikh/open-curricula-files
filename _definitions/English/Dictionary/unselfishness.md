---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unselfishness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484640601
title: unselfishness
categories:
    - Dictionary
---
unselfishness
     n 1: the quality of not putting yourself first but being willing
          to give your time or money or effort etc. for others;
          "rural people show more devotion and unselfishness than
          do their urban cousins" [ant: {selfishness}]
     2: acting generously [syn: {generosity}]
