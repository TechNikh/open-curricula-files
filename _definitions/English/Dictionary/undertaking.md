---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undertaking
offline_file: ""
offline_thumbnail: ""
uuid: 370534fc-c896-4f63-be25-ed7fa09094d1
updated: 1484310561
title: undertaking
categories:
    - Dictionary
---
undertaking
     n 1: any piece of work that is undertaken or attempted; "he
          prepared for great undertakings" [syn: {project}, {task},
           {labor}]
     2: the trade of a funeral director
