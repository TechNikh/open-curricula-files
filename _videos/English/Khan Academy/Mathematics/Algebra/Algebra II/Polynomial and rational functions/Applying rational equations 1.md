---
version: 1
type: video
provider: YouTube
id: gD7A1LA4jO8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Polynomial%20and%20rational%20functions/Applying%20rational%20equations%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ef002e15-9cff-409d-b529-c1af2fc41ab2
updated: 1486069557
title: Applying rational equations 1
categories:
    - Polynomial and rational functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/gD7A1LA4jO8/default.jpg
    - https://i3.ytimg.com/vi/gD7A1LA4jO8/1.jpg
    - https://i3.ytimg.com/vi/gD7A1LA4jO8/2.jpg
    - https://i3.ytimg.com/vi/gD7A1LA4jO8/3.jpg
---
