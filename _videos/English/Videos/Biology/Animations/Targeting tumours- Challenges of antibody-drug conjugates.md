---
version: 1
type: video
provider: YouTube
id: 08q2Th3GQ5w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Targeting%20tumours-%20Challenges%20of%20antibody-drug%20conjugates.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1b208dfd-5dab-4daf-8416-be75a6841311
updated: 1486069670
title: 'Targeting tumours- Challenges of antibody-drug conjugates'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/08q2Th3GQ5w/default.jpg
    - https://i3.ytimg.com/vi/08q2Th3GQ5w/1.jpg
    - https://i3.ytimg.com/vi/08q2Th3GQ5w/2.jpg
    - https://i3.ytimg.com/vi/08q2Th3GQ5w/3.jpg
---
