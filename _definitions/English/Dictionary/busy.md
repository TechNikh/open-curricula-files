---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/busy
offline_file: ""
offline_thumbnail: ""
uuid: b1c261c8-eea9-4859-a9c1-05d9f859d652
updated: 1484310525
title: busy
categories:
    - Dictionary
---
busy
     adj 1: actively or fully engaged or occupied; "busy with her work";
            "a busy man"; "too busy to eat lunch"; "the line is
            busy" [ant: {idle}]
     2: overcrowded or cluttered with detail; "a busy painting"; "a
        fussy design" [syn: {fussy}]
     3: intrusive in a meddling or offensive manner; "an interfering
        old woman"; "bustling about self-importantly making an
        officious nuisance of himself"; "busy about other people's
        business" [syn: {interfering}, {meddlesome}, {meddling}, {officious},
         {busybodied}]
     4: crowdedwith or characterized by much activity; "a very busy
        week"; "a busy life"; "a busy street"; "a ...
