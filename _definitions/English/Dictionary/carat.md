---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carat
offline_file: ""
offline_thumbnail: ""
uuid: fc662c74-801e-4a92-b196-c0d9c6314adb
updated: 1484310411
title: carat
categories:
    - Dictionary
---
carat
     n 1: a unit of weight for precious stones = 200 mg
     2: the unit of measurement for the proportion of gold in an
        alloy; 18-karat gold is 75% gold; 24-karat gold is pure
        gold [syn: {Karat}]
