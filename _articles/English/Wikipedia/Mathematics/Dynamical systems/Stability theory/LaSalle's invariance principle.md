---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/LaSalle%27s_invariance_principle'
offline_file: ""
offline_thumbnail: ""
uuid: b48a323d-6bc6-4bfd-b822-d63e6457fc02
updated: 1484308828
title: "LaSalle's invariance principle"
tags:
    - Global version
    - Local version
    - Relation to Lyapunov theory
    - 'Example: the pendulum with friction'
    - History
    - Original papers
    - Text books
    - Lectures
categories:
    - Stability theory
---
LaSalle's invariance principle (also known as the invariance principle,[1] Barbashin-Krasovskii-LaSalle principle,[2] or Krasovskii-LaSalle principle ) is a criterion for the asymptotic stability of an autonomous (possibly nonlinear) dynamical system.
