---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Transmission-line_matrix_method
offline_file: ""
offline_thumbnail: ""
uuid: 7eeb8894-31d2-43e2-97d5-e3bfa53eb333
updated: 1484308770
title: Transmission-line matrix method
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/500px-SingleNode2DTLM.png
tags:
    - Basic principle
    - The 2D TLM node
    - The scattering matrix of an 2D TLM node
    - Connection between TLM nodes
    - The shunt TLM node
    - 3D TLM models
categories:
    - Electrodynamics
---
The transmission-line matrix (TLM) method is a space and time discretising method for computation of electromagnetic fields. It is based on the analogy between the electromagnetic field and a mesh of transmission lines. The TLM method allows the computation of complex three-dimensional electromagnetic structures and has proven to be one of the most powerful time-domain methods along with the finite difference time domain (FDTD) method.
