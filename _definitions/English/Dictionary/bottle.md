---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bottle
offline_file: ""
offline_thumbnail: ""
uuid: fe0a7892-ea33-4bef-ae5a-d868a35ca0dd
updated: 1484310343
title: bottle
categories:
    - Dictionary
---
bottle
     n 1: glass or plastic vessel; cylindrical with a narrow neck; no
          handle
     2: the quantity contained in a bottle [syn: {bottleful}]
     v 1: store (liquids or gases) in bottles
     2: put into bottles; "bottle the mineral water"
