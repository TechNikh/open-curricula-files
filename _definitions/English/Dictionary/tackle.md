---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tackle
offline_file: ""
offline_thumbnail: ""
uuid: 7795b877-729f-427b-a832-25c78f9e896c
updated: 1484310457
title: tackle
categories:
    - Dictionary
---
tackle
     n 1: the person who plays that position on a football team; "the
          right tackle is a straight A student"
     2: gear consisting of ropes etc. supporting a ship's masts and
        sails [syn: {rigging}]
     3: gear used in fishing [syn: {fishing gear}, {fishing tackle},
         {fishing rig}, {rig}]
     4: a position on the line of scrimmage; "it takes a big man to
        play tackle"
     5: (American football) grasping an opposing player with the
        intention of stopping by throwing to the ground
     v 1: accept as a challenge; "I'll tackle this difficult task"
          [syn: {undertake}, {take on}]
     2: put a harness; "harness the horse" [syn: ...
