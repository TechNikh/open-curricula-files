---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/devastating
offline_file: ""
offline_thumbnail: ""
uuid: 05a64372-39f3-414f-9965-a75b2565a972
updated: 1484310575
title: devastating
categories:
    - Dictionary
---
devastating
     adj 1: making light of; "afire with annihilating invective"; "a
            devastating portrait of human folly"; "to compliments
            inflated I've a withering reply"- W.S.Gilbert [syn: {annihilating},
             {withering}]
     2: wreaking or capable of wreaking complete destruction;
        "possessing annihilative power"; "a devastating
        hurricane"; "the guns opened a withering fire" [syn: {annihilative},
         {annihilating}, {withering}]
     3: physically or spiritually devastating; often used in
        combination; "a crushing blow"; "a crushing rejection";
        "bone-crushing" [syn: {crushing}]
