---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reason
offline_file: ""
offline_thumbnail: ""
uuid: 47937fcd-b1b0-47fd-825e-a64802a98960
updated: 1484310328
title: reason
categories:
    - Dictionary
---
reason
     n 1: a rational motive for a belief or action; "the reason that
          war was declared"; "the grounds for their declaration"
          [syn: {ground}]
     2: an explanation of the cause of some phenomenon; "the reason
        a steady state was never reached was that the back
        pressure built up too slowly"
     3: the capacity for rational thought or inference or
        discrimination; "we are told that man is endowed with
        reason and capable of distinguishing good from evil" [syn:
         {understanding}, {intellect}]
     4: the state of having good sense and sound judgment; "his
        rationality may have been impaired"; "he had to rely less
        on ...
