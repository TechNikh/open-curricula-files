---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parallel
offline_file: ""
offline_thumbnail: ""
uuid: 1f726663-033a-432c-b42e-e1d5cebd4592
updated: 1484310220
title: parallel
categories:
    - Dictionary
---
parallel
     adj 1: being everywhere equidistant and not intersecting; "parallel
            lines never converge"; "concentric circles are
            parallel"; "dancers in two parallel rows" [ant: {perpendicular},
             {oblique}]
     2: of or relating to the simultaneous performance of multiple
        operations; "parallel processing"
     n 1: something having the property of being analogous to
          something else [syn: {analogue}, {analog}]
     2: an imaginary line around the Earth parallel to the equator
        [syn: {latitude}, {line of latitude}, {parallel of
        latitude}]
     v 1: be parallel to; "Their roles are paralleled by ours"
     2: make or place ...
