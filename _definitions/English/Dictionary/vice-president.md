---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vice-president
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484420881
title: vice-president
categories:
    - Dictionary
---
vice president
     n : an executive officer ranking immediately below a president;
         may serve in the president's place under certain
         circumstances [syn: {V.P.}]
