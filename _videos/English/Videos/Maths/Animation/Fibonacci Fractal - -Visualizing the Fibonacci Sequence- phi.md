---
version: 1
type: video
provider: YouTube
id: J_sjsgDTBJU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4fa477ad-25a4-493a-bea6-abea73b0a762
updated: 1486069647
title: 'Fibonacci Fractal - "Visualizing the Fibonacci Sequence" phi'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/J_sjsgDTBJU/default.jpg
    - https://i3.ytimg.com/vi/J_sjsgDTBJU/1.jpg
    - https://i3.ytimg.com/vi/J_sjsgDTBJU/2.jpg
    - https://i3.ytimg.com/vi/J_sjsgDTBJU/3.jpg
---
