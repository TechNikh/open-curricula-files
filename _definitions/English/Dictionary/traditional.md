---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/traditional
offline_file: ""
offline_thumbnail: ""
uuid: a691f645-4336-4121-b8d8-5f54ce3c0a50
updated: 1484310383
title: traditional
categories:
    - Dictionary
---
traditional
     adj 1: consisting of or derived from tradition; "traditional
            history"; "traditional morality" [ant: {nontraditional}]
     2: pertaining to time-honored orthodox doctrines; "the simple
        security of traditional assumptions has vanished"
