---
version: 1
type: article
id: https://en.wikipedia.org/wiki/P-wave
offline_file: ""
offline_thumbnail: ""
uuid: 7de83245-89fe-4b69-af6a-e7aa847057fd
updated: 1484308896
title: P-wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Onde_compression_impulsion_1d_30_petit_1.gif
tags:
    - Velocity
    - Seismic waves in the Earth
    - P-wave shadow zone
    - As an earthquake warning
categories:
    - Waves
---
P-waves are a type of elastic wave, and are one of the two main types of elastic body waves, called seismic waves in seismology, that travel through a continuum and are the first waves from an earthquake to arrive at a seismograph. The continuum is made up of gases (as sound waves), liquids, or solids, including the Earth. P-waves can be produced by earthquakes and recorded by seismographs. The name P-wave can stand for either pressure wave as it is formed from alternating compressions and rarefactions or primary wave, as it has the highest velocity and is therefore the first wave to be recorded.[1]
