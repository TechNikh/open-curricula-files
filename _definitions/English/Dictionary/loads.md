---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loads
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484564281
title: loads
categories:
    - Dictionary
---
loads
     n : a large number or amount; "made lots of new friends"; "she
         amassed a mountain of newspapers" [syn: {tons}, {dozens},
          {heaps}, {lots}, {mountain}, {piles}, {scores}, {stacks},
          {rafts}, {slews}, {wads}, {oodles}, {gobs}, {scads}, {lashings}]
