---
version: 1
type: video
provider: YouTube
id: js4c_U8aJWA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8d81c9d9-047d-4e47-a108-5256d5166711
updated: 1486069641
title: >
    4K DEMO ULTRAHD WINTER SCENE JAPAN 4K映像
    日本の冬景色 (3840×2160) RED EPIC
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/js4c_U8aJWA/default.jpg
    - https://i3.ytimg.com/vi/js4c_U8aJWA/1.jpg
    - https://i3.ytimg.com/vi/js4c_U8aJWA/2.jpg
    - https://i3.ytimg.com/vi/js4c_U8aJWA/3.jpg
---
