---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insert
offline_file: ""
offline_thumbnail: ""
uuid: ddf0c897-7556-48cb-a6bd-b2a08b3ab92a
updated: 1484310328
title: insert
categories:
    - Dictionary
---
insert
     n 1: a folded section placed between the leaves of another
          publication
     2: an artifact that is inserted or is to be inserted [syn: {inset}]
     3: (broadcasting) a local announcement inserted into a network
        program [syn: {cut-in}]
     4: (film) a still picture that is inserted and that interrupts
        the action of a film [syn: {cut-in}]
     v 1: put or introduce into something; "insert a picture into the
          text" [syn: {infix}, {enter}, {introduce}]
     2: introduce; "Insert your ticket here" [syn: {enclose}, {inclose},
         {stick in}, {put in}, {introduce}]
     3: fit snugly into; "insert your ticket into the slot"; "tuck
        your ...
