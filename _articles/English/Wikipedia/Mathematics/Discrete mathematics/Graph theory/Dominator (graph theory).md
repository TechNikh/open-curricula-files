---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dominator_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 63c8ee14-3f3e-46ed-95e5-25d66ed8c787
updated: 1484308619
title: Dominator (graph theory)
tags:
    - History
    - Applications
    - Algorithms
    - Postdominance
categories:
    - Graph theory
---
In computer science, in control flow graphs, a node d dominates a node n if every path from the entry node to n must go through d. Notationally, this is written as d dom n (or sometimes d 
  
    
      
        ≫
      
    
    {\displaystyle \gg }
  
 n). By definition, every node dominates itself.
