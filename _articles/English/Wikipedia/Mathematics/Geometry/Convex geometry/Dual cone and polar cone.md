---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dual_cone_and_polar_cone
offline_file: ""
offline_thumbnail: ""
uuid: 7986ad6d-f648-40b5-9e8a-cbc174aab743
updated: 1484309192
title: Dual cone and polar cone
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dual_cone_illustration.svg.png
tags:
    - Dual cone
    - Self-dual cones
    - Polar cone
categories:
    - Convex geometry
---
