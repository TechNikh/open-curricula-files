---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agreement
offline_file: ""
offline_thumbnail: ""
uuid: 7f512965-f902-4407-927c-4cf998fdf29c
updated: 1484310244
title: agreement
categories:
    - Dictionary
---
agreement
     n 1: the statement (oral or written) of an exchange of promises;
          "they had an agreement that they would not interfere in
          each other's business"; "there was an understanding
          between management and the workers" [syn: {understanding}]
     2: compatibility of observations; "there was no agreement
        between theory and measurement"; "the results of two tests
        were in correspondence" [syn: {correspondence}]
     3: harmony of people's opinions or actions or characters; "the
        two parties were in agreement" [syn: {accord}] [ant: {disagreement}]
     4: the thing arranged or agreed to; "they made arrangements to
        meet in ...
