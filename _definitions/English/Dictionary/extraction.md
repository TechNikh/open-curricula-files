---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extraction
offline_file: ""
offline_thumbnail: ""
uuid: 43abcb80-9776-420e-bff2-094894ebe4d6
updated: 1484310405
title: extraction
categories:
    - Dictionary
---
extraction
     n 1: the process of obtaining something from a mixture or
          compound by chemical or physical or mechanical means
     2: properties attributable to your ancestry; "he comes from
        good origins" [syn: {origin}, {descent}]
     3: the act of pulling out (as a tooth); "the dentist gave her a
        local anesthetic prior to the extraction"
