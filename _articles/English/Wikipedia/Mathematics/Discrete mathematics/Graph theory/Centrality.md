---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Centrality
offline_file: ""
offline_thumbnail: ""
uuid: 851bad76-188b-4f07-a23e-0590dd43f977
updated: 1484308614
title: Centrality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-6_centrality_measures.png
tags:
    - Definition and characterization of centrality indices
    - Characterization by network flows
    - Characterization by walk structure
    - Radial-volume centralities exist on a spectrum
    - Important limitations
    - Degree centrality
    - Closeness centrality
    - Betweenness centrality
    - Eigenvector centrality
    - Using the adjacency matrix to find eigenvector centrality
    - Katz centrality
    - PageRank centrality
    - Percolation centrality
    - Cross-clique centrality
    - Freeman Centralization
    - Dissimilarity based centrality measures
    - Extensions
    - Notes and references
categories:
    - Graph theory
---
In graph theory and network analysis, indicators of centrality identify the most important vertices within a graph. Applications include identifying the most influential person(s) in a social network, key infrastructure nodes in the Internet or urban networks, and super-spreaders of disease. Centrality concepts were first developed in social network analysis, and many of the terms used to measure centrality reflect their sociological origin.[1] They should not be confused with node influence metrics, which seek to quantify the influence of every node in the network.
