---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ratification
offline_file: ""
offline_thumbnail: ""
uuid: 70689f32-b27e-49e8-88eb-2a67d6d79c98
updated: 1484310599
title: ratification
categories:
    - Dictionary
---
ratification
     n : making something valid by formally ratifying or confirming
         it; "the ratification of the treaty"; "confirmation of
         the appointment" [syn: {confirmation}]
