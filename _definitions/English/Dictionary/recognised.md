---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recognised
offline_file: ""
offline_thumbnail: ""
uuid: abff2259-16ad-4902-bfe4-2c1add64072f
updated: 1484310281
title: recognised
categories:
    - Dictionary
---
recognised
     adj 1: provided with a secure reputation; "a recognized authority"
            [syn: {recognized}]
     2: generally approved or compelling recognition; "several
        accepted techniques for treating the condition"; "his
        recognized superiority in this kind of work" [syn: {accepted},
         {recognized}]
