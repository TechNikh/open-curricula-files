---
version: 1
type: video
provider: YouTube
id: fmXFWi-WfyU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 59dd025f-d5c5-417f-8f31-00b9837c9eac
updated: 1486069682
title: 'Rotational Motion- Crash Course Physics #11'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/fmXFWi-WfyU/default.jpg
    - https://i3.ytimg.com/vi/fmXFWi-WfyU/1.jpg
    - https://i3.ytimg.com/vi/fmXFWi-WfyU/2.jpg
    - https://i3.ytimg.com/vi/fmXFWi-WfyU/3.jpg
---
