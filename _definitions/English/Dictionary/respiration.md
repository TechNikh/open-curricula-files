---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/respiration
offline_file: ""
offline_thumbnail: ""
uuid: a4911892-01c2-4a2c-82c6-92adab9efebf
updated: 1484310363
title: respiration
categories:
    - Dictionary
---
respiration
     n 1: the metabolic processes whereby certain organisms obtain
          energy from organic moelcules; processes that take place
          in the cells and tissues during which energy is released
          and carbon dioxide is produced and absorbed by the blood
          to be transported to the lungs [syn: {internal
          respiration}, {cellular respiration}]
     2: a single complete act of breathing in and out; "thirty
        respirations per minute"
     3: the bodily process of inhalation and exhalation; the process
        of taking in oxygen from inhaled air and releasing carbon
        dioxide by exhalation [syn: {breathing}, {external
        respiration}, ...
