---
version: 1
type: video
provider: YouTube
id: ET66oPXlCJU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f5fe2714-3679-4c56-ac02-b3b2f1248d33
updated: 1486069660
title: Rubiks Cube Poster Illusion!
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/ET66oPXlCJU/default.jpg
    - https://i3.ytimg.com/vi/ET66oPXlCJU/1.jpg
    - https://i3.ytimg.com/vi/ET66oPXlCJU/2.jpg
    - https://i3.ytimg.com/vi/ET66oPXlCJU/3.jpg
---
