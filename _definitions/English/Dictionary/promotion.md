---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/promotion
offline_file: ""
offline_thumbnail: ""
uuid: 36c10cd7-9263-41b4-9507-25ce20adc7c5
updated: 1484310413
title: promotion
categories:
    - Dictionary
---
promotion
     n 1: a message issued in behalf of some product or cause or idea
          or person or institution [syn: {publicity}, {promotional
          material}, {packaging}]
     2: act of raising in rank or position [ant: {demotion}]
     3: encouragement of the progress or growth or acceptance of
        something [syn: {furtherance}, {advancement}]
     4: the advancement of some enterprise; "his experience in
        marketing resulted in the forwarding of his career" [syn:
        {forwarding}, {furtherance}]
