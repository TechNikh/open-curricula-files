---
version: 1
type: video
provider: YouTube
id: fs7fz3MLpC8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Understanding%20Logical%20Statements%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6ffa1876-fcb4-4353-9bcb-347ccf86656e
updated: 1486069561
title: Understanding Logical Statements 3
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/fs7fz3MLpC8/default.jpg
    - https://i3.ytimg.com/vi/fs7fz3MLpC8/1.jpg
    - https://i3.ytimg.com/vi/fs7fz3MLpC8/2.jpg
    - https://i3.ytimg.com/vi/fs7fz3MLpC8/3.jpg
---
