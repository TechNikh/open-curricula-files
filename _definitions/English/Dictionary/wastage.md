---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wastage
offline_file: ""
offline_thumbnail: ""
uuid: 26cf365d-2da7-4c77-ab73-858986a8d297
updated: 1484310234
title: wastage
categories:
    - Dictionary
---
wastage
     n 1: the process of wasting
     2: anything lost by wear or waste
