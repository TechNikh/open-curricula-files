---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/serbia
offline_file: ""
offline_thumbnail: ""
uuid: ac53ba39-f4da-4b7b-9f25-b89aaafb7c48
updated: 1484310551
title: serbia
categories:
    - Dictionary
---
Serbia
     n : a historical region in central and northern Yugoslavia;
         Serbs settled the region in the 6th and 7th centuries
         [syn: {Srbija}]
