---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exhausted
offline_file: ""
offline_thumbnail: ""
uuid: f07793f4-e7a2-46bf-ab31-9255c30cff5b
updated: 1484310359
title: exhausted
categories:
    - Dictionary
---
exhausted
     adj 1: drained of energy or effectiveness; extremely tired;
            completely exhausted; "the day's shopping left her
            exhausted"; "he went to bed dog-tired"; "was fagged
            and sweaty"; "the trembling of his played out limbs";
            "felt completely washed-out"; "only worn-out horses
            and cattle"; "you look worn out" [syn: {dog-tired}, {fagged},
             {fatigued}, {played out}, {spent}, {washed-out}, {worn-out(a)},
             {worn out(p)}]
     2: completely emptied of resources or properties; "impossible
        to grow tobacco on the exhausted soil"; "the exhausted
        food sources"; "exhausted oil wells" [ant: ...
