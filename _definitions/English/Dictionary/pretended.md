---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pretended
offline_file: ""
offline_thumbnail: ""
uuid: e1f4ca68-fb38-491c-90d4-9644e613c4a3
updated: 1484310146
title: pretended
categories:
    - Dictionary
---
pretended
     adj : adopted in order to deceive; "an assumed name"; "an assumed
           cheerfulness"; "a fictitious address"; "fictive
           sympathy"; "a pretended interest"; "a put-on childish
           voice"; "sham modesty" [syn: {assumed}, {false}, {fictitious},
            {fictive}, {put on}, {sham}]
