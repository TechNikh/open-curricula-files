---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/selling
offline_file: ""
offline_thumbnail: ""
uuid: de2d45bf-c92d-47c8-b371-5eda94ae40b0
updated: 1484310453
title: selling
categories:
    - Dictionary
---
selling
     n : the exchange of goods for an agreed sum of money [syn: {merchandising},
          {marketing}]
