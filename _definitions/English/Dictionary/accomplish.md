---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accomplish
offline_file: ""
offline_thumbnail: ""
uuid: 9a29104c-b926-4ed0-932c-4534f5841ed0
updated: 1484310591
title: accomplish
categories:
    - Dictionary
---
accomplish
     v 1: put in effect; "carry out a task"; "execute the decision of
          the people"; "He actioned the operation" [syn: {carry
          through}, {execute}, {carry out}, {action}, {fulfill}, {fulfil}]
     2: to gain with effort; "she achieved her goal despite
        setbacks" [syn: {achieve}, {attain}, {reach}]
