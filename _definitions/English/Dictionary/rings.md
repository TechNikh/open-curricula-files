---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rings
offline_file: ""
offline_thumbnail: ""
uuid: 578e7f6c-2f92-4b83-bee5-e55c6e70584e
updated: 1484310366
title: rings
categories:
    - Dictionary
---
rings
     n : gymnastic apparatus consisting of a pair of heavy metal
         circles (usually covered with leather) suspended by
         ropes; used for gymnastic exercises; "the rings require a
         strong upper body"
