---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/territory
offline_file: ""
offline_thumbnail: ""
uuid: a74849d2-4f68-4aae-93ab-a8de7e39c312
updated: 1484310459
title: territory
categories:
    - Dictionary
---
territory
     n 1: a region marked off for administrative or other purposes
          [syn: {district}, {territorial dominion}, {dominion}]
     2: an area of knowledge or interest; "his questions covered a
        lot of territory"
     3: the geographical area under the jurisdiction of a sovereign
        state; "American troops were stationed on Japanese soil"
        [syn: {soil}]
