---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/creation
offline_file: ""
offline_thumbnail: ""
uuid: a2e2af0b-25ca-46f9-9860-08c6fc3238c6
updated: 1484310549
title: creation
categories:
    - Dictionary
---
creation
     n 1: the human act of creating [syn: {creative activity}]
     2: an artifact that has been brought into existence by someone
     3: the event that occurred at the beginning of something; "from
        its creation the plan was doomed to failure" [syn: {conception}]
     4: the act of starting something for the first time;
        introducing something new; "she looked forward to her
        initiation as an adult"; "the foundation of a new
        scientific society"; "he regards the fork as a modern
        introduction" [syn: {initiation}, {founding}, {foundation},
         {institution}, {origination}, {innovation}, {introduction},
         {instauration}]
     5: ...
