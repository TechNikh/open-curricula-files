---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cows
offline_file: ""
offline_thumbnail: ""
uuid: 4994a41d-f384-44b5-bd9b-a897ce4af8b7
updated: 1484310533
title: cows
categories:
    - Dictionary
---
cows
     n : domesticated bovine animals as a group regardless of sex or
         age; "so many head of cattle"; "wait till the cows come
         home"; "seven thin and ill-favored kine"- Bible; "a team
         of oxen" [syn: {cattle}, {kine}, {oxen}, {Bos taurus}]
