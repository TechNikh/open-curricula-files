---
version: 1
type: video
provider: YouTube
id: 5zTWIbGGB8k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2dcfa590-9bfa-4e1d-9563-7e927fdd1fac
updated: 1486069607
title: Multiple rates word problem Reviewed
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/5zTWIbGGB8k/default.jpg
    - https://i3.ytimg.com/vi/5zTWIbGGB8k/1.jpg
    - https://i3.ytimg.com/vi/5zTWIbGGB8k/2.jpg
    - https://i3.ytimg.com/vi/5zTWIbGGB8k/3.jpg
---
