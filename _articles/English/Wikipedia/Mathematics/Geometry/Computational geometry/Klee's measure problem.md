---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Klee%27s_measure_problem'
offline_file: ""
offline_thumbnail: ""
uuid: a1940c8e-545f-4b37-bb5d-094d76ba317c
updated: 1484309173
title: "Klee's measure problem"
tags:
    - History and algorithms
    - Known bounds
    - References and further reading
    - Important papers
    - Secondary literature
categories:
    - Computational geometry
---
In computational geometry, Klee's measure problem is the problem of determining how efficiently the measure of a union of (multidimensional) rectangular ranges can be computed. Here, a d-dimensional rectangular range is defined to be a cartesian product of d intervals of real numbers, which is a subset of Rd.
