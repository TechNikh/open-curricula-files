---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Giant_component
offline_file: ""
offline_thumbnail: ""
uuid: 88728133-8999-4497-bdf9-7cebb94336dc
updated: 1484309402
title: Giant component
categories:
    - Graph connectivity
---
In network theory, a giant component is a connected component of a given random graph that contains a constant fraction of the entire graph's vertices.
