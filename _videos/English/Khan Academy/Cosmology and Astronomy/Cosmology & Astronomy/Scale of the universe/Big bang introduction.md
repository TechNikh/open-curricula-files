---
version: 1
type: video
provider: YouTube
id: eUF59jCFcyQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Big%20bang%20introduction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ad712d7e-eb00-4e8a-92c8-4c1ad1d2b318
updated: 1486069686
title: Big bang introduction
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/eUF59jCFcyQ/default.jpg
    - https://i3.ytimg.com/vi/eUF59jCFcyQ/1.jpg
    - https://i3.ytimg.com/vi/eUF59jCFcyQ/2.jpg
    - https://i3.ytimg.com/vi/eUF59jCFcyQ/3.jpg
---
