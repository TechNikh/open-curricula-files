---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meeting
offline_file: ""
offline_thumbnail: ""
uuid: d4f9faae-8247-4cc3-a4a5-f1792e7d768e
updated: 1484310218
title: meeting
categories:
    - Dictionary
---
meeting
     n 1: a formally arranged gathering; "next year the meeting will
          be in Chicago"; "the meeting elected a chairperson"
     2: the social act of assembling for some common purpose; "his
        meeting with the salesmen was the high point of his day"
        [syn: {coming together}]
     3: a small informal social gathering; "there was an informal
        meeting in my livingroom" [syn: {get together}]
     4: a casual or unexpected convergence; "he still remembers
        their meeting in Paris"; "there was a brief encounter in
        the hallway" [syn: {encounter}]
     5: the act of joining together as one; "the merging of the two
        groups occurred quickly"; ...
