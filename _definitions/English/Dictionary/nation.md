---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nation
offline_file: ""
offline_thumbnail: ""
uuid: 54855d84-b357-4d39-a148-004c86717286
updated: 1484310234
title: nation
categories:
    - Dictionary
---
nation
     n 1: a politically organized body of people under a single
          government; "the state has elected a new president";
          "African nations"; "students who had come to the
          nation's capitol"; "the country's largest manufacturer";
          "an industrialized land" [syn: {state}, {country}, {land},
           {commonwealth}, {res publica}, {body politic}]
     2: the people who live in a nation or country; "a statement
        that sums up the nation's mood"; "the news was announced
        to the nation"; "the whole country worshipped him" [syn: {land},
         {country}, {a people}]
     3: a federation of tribes (especially native American tribes);
        ...
