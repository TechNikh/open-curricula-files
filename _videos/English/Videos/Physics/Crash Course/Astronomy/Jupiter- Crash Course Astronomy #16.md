---
version: 1
type: video
provider: YouTube
id: Xwn8fQSW7-8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Jupiter-%20Crash%20Course%20Astronomy%20%2316.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4b65c34d-14b0-4042-a6d7-372c7cfcaf96
updated: 1486069684
title: 'Jupiter- Crash Course Astronomy #16'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/Xwn8fQSW7-8/default.jpg
    - https://i3.ytimg.com/vi/Xwn8fQSW7-8/1.jpg
    - https://i3.ytimg.com/vi/Xwn8fQSW7-8/2.jpg
    - https://i3.ytimg.com/vi/Xwn8fQSW7-8/3.jpg
---
