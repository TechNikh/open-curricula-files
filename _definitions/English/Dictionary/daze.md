---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/daze
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484466961
title: daze
categories:
    - Dictionary
---
daze
     n 1: the feeling of distress and disbelief that you have when
          something bad happens accidentally; "his mother's
          deathleft him in a daze"; "he was numb with shock" [syn:
           {shock}, {stupor}]
     2: confusion characterized by lack of clarity [syn: {fog}, {haze}]
     v 1: to cause someone to lose clear vision, especially from
          intense light; "She was dazzled by the bright
          headlights" [syn: {dazzle}, {bedazzle}]
     2: overcome as with astonishment or disbelief; "The news
        stunned her" [syn: {stun}, {bedaze}]
