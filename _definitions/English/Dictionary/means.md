---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/means
offline_file: ""
offline_thumbnail: ""
uuid: 45968948-4973-41f8-917e-385f861b4c6a
updated: 1484310303
title: means
categories:
    - Dictionary
---
means
     n 1: how a result is obtained or an end is achieved; "a means of
          control"; "an example is the best agency of
          instruction"; "the true way to success" [syn: {agency},
          {way}]
     2: instrumentality used to achieve an end
     3: considerable capital (wealth or income); "he is a man of
        means" [syn: {substance}]
