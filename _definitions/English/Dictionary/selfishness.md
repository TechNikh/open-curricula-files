---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/selfishness
offline_file: ""
offline_thumbnail: ""
uuid: 6b4720d7-a659-4e6c-bc2b-6868b7e833f6
updated: 1484310152
title: selfishness
categories:
    - Dictionary
---
selfishness
     n : stinginess resulting from a concern for your own welfare and
         a disregard of others [ant: {unselfishness}]
