---
version: 1
type: video
provider: YouTube
id: zO7RZZW0wSQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/AC%20current%20impedance%20-%20Alternating%20Voltage%20for%20inductors%2C%20capacitors.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 344f95ed-4647-4e19-ad1b-f7466e770c16
updated: 1486069656
title: 'AC current impedance - Alternating Voltage for inductors, capacitors'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/zO7RZZW0wSQ/default.jpg
    - https://i3.ytimg.com/vi/zO7RZZW0wSQ/1.jpg
    - https://i3.ytimg.com/vi/zO7RZZW0wSQ/2.jpg
    - https://i3.ytimg.com/vi/zO7RZZW0wSQ/3.jpg
---
