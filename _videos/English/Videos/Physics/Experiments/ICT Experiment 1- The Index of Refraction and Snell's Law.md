---
version: 1
type: video
provider: YouTube
id: plZocHWpFhk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0a968acb-35e0-4844-85bf-dfde055ed9c4
updated: 1486069639
title: "ICT Experiment 1- The Index of Refraction and Snell's Law"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/plZocHWpFhk/default.jpg
    - https://i3.ytimg.com/vi/plZocHWpFhk/1.jpg
    - https://i3.ytimg.com/vi/plZocHWpFhk/2.jpg
    - https://i3.ytimg.com/vi/plZocHWpFhk/3.jpg
---
