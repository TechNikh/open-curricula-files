---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Treewidth
offline_file: ""
offline_thumbnail: ""
uuid: f98888e8-b901-42c2-8739-a877a45a9e65
updated: 1484309414
title: Treewidth
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Tree_decomposition.svg.png
tags:
    - Definition
    - Examples
    - Bounded treewidth
    - Graph families with bounded treewidth
    - Forbidden minors
    - Algorithms
    - Computing the treewidth
    - Solving other problems on graphs of small treewidth
    - "Courcelle's theorem"
    - Related parameters
    - Pathwidth
    - Grid minor size
    - Diameter and local treewidth
    - Hadwiger number and S-functions
    - Notes
categories:
    - Graph minor theory
---
In graph theory, the treewidth of an undirected graph is a number associated with the graph. Treewidth may be defined in several equivalent ways: from the size of the largest vertex set in a tree decomposition of the graph, from the size of the largest clique in a chordal completion of the graph, from the maximum order of a haven describing a strategy for a pursuit-evasion game on the graph, or from the maximum order of a bramble, a collection of connected subgraphs that all touch each other.
