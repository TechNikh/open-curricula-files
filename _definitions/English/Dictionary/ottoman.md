---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ottoman
offline_file: ""
offline_thumbnail: ""
uuid: ef01d06b-ad9a-42d8-967b-da54cbdc1948
updated: 1484310557
title: ottoman
categories:
    - Dictionary
---
Ottoman
     adj : of or relating to the Ottoman Empire or its people or its
           culture
     n 1: a Turk (especially a Turk who is a member of the tribe of
          Osman I) [syn: {Ottoman Turk}, {Osmanli}]
     2: the Turkish dynasty that ruled the Ottoman Empire from the
        13th century to its dissolution after World War I [syn: {Ottoman
        dynasty}]
     3: thick cushion used as a seat [syn: {pouf}, {pouffe}, {puff},
         {hassock}]
     4: a low stool to rest the feet of a seated person [syn: {footstool},
         {footrest}]
