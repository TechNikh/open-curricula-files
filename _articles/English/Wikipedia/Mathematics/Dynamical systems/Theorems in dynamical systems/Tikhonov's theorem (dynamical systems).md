---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tikhonov%27s_theorem_(dynamical_systems)'
offline_file: ""
offline_thumbnail: ""
uuid: b2afc0c3-5423-4053-a7a9-4a9ff27b7e70
updated: 1484308696
title: "Tikhonov's theorem (dynamical systems)"
categories:
    - Theorems in dynamical systems
---
In applied mathematics, Tikhonov's theorem on dynamical systems is a result on stability of solutions of systems of differential equations. It has applications to chemical kinetics.[1] The theorem is named after Andrey Nikolayevich Tikhonov.
