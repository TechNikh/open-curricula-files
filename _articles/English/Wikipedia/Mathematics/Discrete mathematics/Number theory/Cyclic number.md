---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyclic_number
offline_file: ""
offline_thumbnail: ""
uuid: 41fbaef2-3e02-4d03-847f-0a762d437b2b
updated: 1484308636
title: Cyclic number
tags:
    - Details
    - Relation to repeating decimals
    - Form of cyclic numbers
    - Construction of cyclic numbers
    - Properties of cyclic numbers
    - Other numeric bases
categories:
    - Number theory
---
