---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amused
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484531341
title: amused
categories:
    - Dictionary
---
amused
     adj : pleasantly occupied; "We are not amused" -Queen Victoria
           [syn: {diverted}, {entertained}]
