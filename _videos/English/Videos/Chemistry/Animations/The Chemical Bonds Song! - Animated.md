---
version: 1
type: video
provider: YouTube
id: QIfTT-_-xLo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 38f307fa-4d8f-409b-ade0-ff29044b9305
updated: 1486069664
title: 'The Chemical Bonds Song! - Animated'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/QIfTT-_-xLo/default.jpg
    - https://i3.ytimg.com/vi/QIfTT-_-xLo/1.jpg
    - https://i3.ytimg.com/vi/QIfTT-_-xLo/2.jpg
    - https://i3.ytimg.com/vi/QIfTT-_-xLo/3.jpg
---
