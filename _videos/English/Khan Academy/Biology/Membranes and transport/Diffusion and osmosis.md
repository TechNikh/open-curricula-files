---
version: 1
type: video
provider: YouTube
id: aubZU0iWtgI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Membranes%20and%20transport/Diffusion%20and%20osmosis.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9e08af81-cfd7-48e8-91d8-b324880d5d95
updated: 1486069583
title: Diffusion and osmosis
tags:
    - dotsub
categories:
    - Membranes and transport
thumbnail_urls:
    - https://i3.ytimg.com/vi/aubZU0iWtgI/default.jpg
    - https://i3.ytimg.com/vi/aubZU0iWtgI/1.jpg
    - https://i3.ytimg.com/vi/aubZU0iWtgI/2.jpg
    - https://i3.ytimg.com/vi/aubZU0iWtgI/3.jpg
---
Learn about diffusion, osmosis, and concentration gradients and why these are important to cells.

Watch the next lesson: https://www.khanacademy.org/science/biology/membranes-and-transport/passive-transport/v/passive-transport-and-selective-permeability?utm_source=YT&utm_medium=Desc&utm_campaign=biology

Missed the previous lesson? https://www.khanacademy.org/science/biology/membranes-and-transport/diffusion-and-osmosis/v/hypotonic-isotonic-and-hypertonic-solutions-tonicity?utm_source=YT&utm_medium=Desc&utm_campaign=biology

Biology on Khan Academy: Life is beautiful! From atoms to cells, from genes to proteins, from populations to ecosystems, biology is the study of the fascinating and intricate systems that make life possible. Dive in to learn more about the many branches of biology and why they are exciting and important. Covers topics seen in a high school or first-year college biology course.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy's Biology channel: https://www.youtube.com/channel/UC82qE46vcTn7lP4tK_RHhdg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
