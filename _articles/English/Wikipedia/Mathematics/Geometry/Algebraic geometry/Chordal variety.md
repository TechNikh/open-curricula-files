---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chordal_variety
offline_file: ""
offline_thumbnail: ""
uuid: 5d816328-ec0f-4a10-9a7a-ada593acec49
updated: 1484309134
title: Chordal variety
categories:
    - Algebraic geometry
---
In algebraic geometry, a chordal variety of a variety is the union of all the chords (lines meeting 2 points), including the limiting cases of tangent lines.
