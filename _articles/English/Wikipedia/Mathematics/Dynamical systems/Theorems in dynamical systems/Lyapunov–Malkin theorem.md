---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lyapunov%E2%80%93Malkin_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 9f2c77f6-11f2-403d-8958-6a41efe87b04
updated: 1484308705
title: Lyapunov–Malkin theorem
categories:
    - Theorems in dynamical systems
---
The Lyapunov–Malkin theorem (named for Aleksandr Lyapunov and Ioel Gilevich Malkin) is a mathematical theorem detailing nonlinear stability of systems.[1]
