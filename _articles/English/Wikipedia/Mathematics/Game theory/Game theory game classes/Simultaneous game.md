---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Simultaneous_game
offline_file: ""
offline_thumbnail: ""
uuid: 6426d490-ae6f-40af-a5d2-c51356770d73
updated: 1484309040
title: Simultaneous game
categories:
    - Game theory game classes
---
In game theory, a simultaneous game is a game where each player chooses his action without knowledge of the actions chosen by other players. Normal form representations are usually used for simultaneous games.
