---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ism
offline_file: ""
offline_thumbnail: ""
uuid: 5af2136b-7af3-47c3-a0d4-6e941cf48751
updated: 1484310140
title: ism
categories:
    - Dictionary
---
ism
     n : a belief (or system of beliefs) accepted as authoritative by
         some group or school [syn: {doctrine}, {philosophy}, {philosophical
         system}, {school of thought}]
