---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Protection_(poker)
offline_file: ""
offline_thumbnail: ""
uuid: 3c360ace-df46-4ada-a15f-52dc9e7e8236
updated: 1484309059
title: Protection (poker)
categories:
    - Poker strategy
---
Protection in poker is a bet made with a strong but vulnerable hand, such as top pair when straight or flush draws are possible. The bet forces opponents with draws to either call with insufficient pot odds, or to fold, both of which are profitable for the betting player. By contrast, if he failed to protect his hand, another player could draw out on him at no cost, meaning he gets no value from his made hand.
