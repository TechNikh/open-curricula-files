---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Square_planar_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 045c3ea7-081a-4f66-9a95-72094e105a67
updated: 1484309280
title: Square planar molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Square-planar-3D-balls.png
tags:
    - Relationship to other geometries
    - Linear
    - Tetrahedral molecular geometry
    - Octahedral geometry
    - Examples
    - >
        Splitting of the energy of the d-orbitals in square planar
        transition metal complexes
categories:
    - Molecular geometry
---
The square planar molecular geometry in chemistry describes the stereochemistry (spatial arrangement of atoms) that is adopted by certain chemical compounds. As the name suggests, molecules of this geometry have their atoms positioned at the corners of a square on the same plane about a central atom.
