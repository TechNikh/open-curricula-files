---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clarify
offline_file: ""
offline_thumbnail: ""
uuid: cae2c941-525d-4fe3-bb16-b0490f4c9316
updated: 1484310163
title: clarify
categories:
    - Dictionary
---
clarify
     v 1: make clear and (more) comprehensible; "clarify the mystery
          surrounding her death" [syn: {clear up}, {elucidate}]
          [ant: {obfuscate}]
     2: make clear by removing impurities or solids, as by heating;
        "clarify the butter"; "clarify beer"
     [also: {clarified}]
