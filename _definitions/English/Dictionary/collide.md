---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collide
offline_file: ""
offline_thumbnail: ""
uuid: c7e703ff-58f6-469f-a7c9-0b4736354b7d
updated: 1484310230
title: collide
categories:
    - Dictionary
---
collide
     v 1: crash together with violent impact; "The cars collided";
          "Two meteors clashed" [syn: {clash}]
     2: be incompatible; be or come into conflict; "These colors
        clash" [syn: {clash}, {jar}]
     3: cause to collide; "The physicists collided the particles"
