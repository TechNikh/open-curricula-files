---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/judiciously
offline_file: ""
offline_thumbnail: ""
uuid: ae154e04-eee7-4905-842a-23fe7390dbfe
updated: 1484310258
title: judiciously
categories:
    - Dictionary
---
judiciously
     adv : in a judicious manner; "let's use these intelligence tests
           judiciously" [ant: {injudiciously}]
