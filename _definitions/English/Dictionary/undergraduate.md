---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undergraduate
offline_file: ""
offline_thumbnail: ""
uuid: 01359c61-bb26-4568-93de-e90036281c28
updated: 1484310479
title: undergraduate
categories:
    - Dictionary
---
undergraduate
     n : a university student who has not yet received a first degree
         [syn: {undergrad}]
