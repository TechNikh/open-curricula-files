---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polyhedral_space
offline_file: ""
offline_thumbnail: ""
uuid: 10bb87f6-de9f-4d04-ace5-c66c325ea1e8
updated: 1484309273
title: Polyhedral space
tags:
    - Examples
    - Metric singularities
    - Curvature
    - Additional structure
    - Other topics
    - History
categories:
    - Metric geometry
---
Polyhedral space is a certain metric space. A (Euclidean) polyhedral space is a (usually finite) simplicial complex in which every simplex has a flat metric. (Other spaces of interest are spherical and hypebolic polyhedral spaces, where every simplex has a metric of constant positive or negative curvature). In the sequel all polyhedral spaces are taken to be Euclidean polyhedral spaces.
