---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/K%C3%B6nig%27s_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 4009ebd7-b240-447f-b551-7a528f796364
updated: 1484309454
title: "König's lemma"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Denes_K%25C3%25B6nig_-_%25C3%259Cber_eine_Schlussweise_aus_dem_Endlichen_ins_Unendliche.png'
tags:
    - Statement of the lemma
    - Proof
    - Computability aspects
    - Relationship to constructive mathematics and compactness
    - Relationship with the axiom of choice
    - Notes
categories:
    - Theorems in graph theory
---
König's lemma or König's infinity lemma is a theorem in graph theory due to Dénes Kőnig (1927).[1][2] It gives a sufficient condition for an infinite graph to have an infinitely long path. The computability aspects of this theorem have been thoroughly investigated by researchers in mathematical logic, especially in computability theory. This theorem also has important roles in constructive mathematics and proof theory.
