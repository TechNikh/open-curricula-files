---
version: 1
type: video
provider: YouTube
id: LlKvubIqHc8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Sine%20Taylor%20Series%20at%200%20%28Maclaurin%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 586bde82-2ef7-4dfb-b374-e3ff6625d8de
updated: 1486069693
title: Sine Taylor Series at 0 (Maclaurin)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/LlKvubIqHc8/default.jpg
    - https://i3.ytimg.com/vi/LlKvubIqHc8/1.jpg
    - https://i3.ytimg.com/vi/LlKvubIqHc8/2.jpg
    - https://i3.ytimg.com/vi/LlKvubIqHc8/3.jpg
---
