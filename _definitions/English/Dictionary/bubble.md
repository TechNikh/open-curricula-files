---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bubble
offline_file: ""
offline_thumbnail: ""
uuid: 1ee6ddd5-f8af-4325-a00c-7712ca92151b
updated: 1484310226
title: bubble
categories:
    - Dictionary
---
bubble
     n 1: a hollow globule of gas (e.g., air or carbon dioxide)
     2: a speculative scheme that depends on unstable factors that
        the planner cannot control; "his proposal was nothing but
        a house of cards"; "a real estate bubble" [syn: {house of
        cards}]
     3: an impracticable and illusory idea; "he didn't want to burst
        the newcomer's bubble"
     4: a dome-shaped covering made of transparent glass or plastic
     v 1: form, produce, or emit bubbles; "The soup was bubbling"
     2: flow in an irregular current with a bubbling noise;
        "babbling brooks" [syn: {ripple}, {babble}, {guggle}, {burble},
         {gurgle}]
     3: expel gas from the ...
