---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/teenager
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484435761
title: teenager
categories:
    - Dictionary
---
teenager
     n : a juvenile between the onset of puberty and maturity [syn: {adolescent},
          {stripling}]
