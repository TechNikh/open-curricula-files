---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/predominant
offline_file: ""
offline_thumbnail: ""
uuid: e8db1c16-094d-4b3d-b3fa-721f0dd741e3
updated: 1484310384
title: predominant
categories:
    - Dictionary
---
predominant
     adj 1: most frequent or common; "prevailing winds" [syn: {prevailing}]
     2: having superior power and influence; "the predominant mood
        among policy-makers is optimism" [syn: {overriding}, {paramount},
         {predominate}, {preponderant}, {preponderating}]
