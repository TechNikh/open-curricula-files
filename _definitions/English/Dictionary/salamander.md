---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/salamander
offline_file: ""
offline_thumbnail: ""
uuid: 59be8e56-22e2-4bdf-b8be-8860d533a86d
updated: 1484310283
title: salamander
categories:
    - Dictionary
---
salamander
     n 1: any of various typically terrestrial amphibians that
          resemble lizards and that return to water only to breed
     2: reptilian creature supposed to live in fire
     3: fire iron consisting of a metal rod with a handle; used to
        stir a fire [syn: {poker}, {stove poker}, {fire hook}]
