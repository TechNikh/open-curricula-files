---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/governed
offline_file: ""
offline_thumbnail: ""
uuid: fa61c09b-974d-478c-8570-84f129c4870a
updated: 1484310295
title: governed
categories:
    - Dictionary
---
governed
     n : the body of people who are citizens of a particular
         government; "governments derive their just powers from
         the consent of the governed"--Declaration of Independence
