---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stubborn
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413441
title: stubborn
categories:
    - Dictionary
---
stubborn
     adj 1: tenaciously unwilling or marked by tenacious unwillingness
            to yield [ant: {docile}]
     2: not responding to treatment; "a stubborn infection"; "a
        refractory case of acne" [syn: {refractory}]
     3: difficult to treat or deal with; "stubborn rust stains"; "a
        stubborn case of acne"
     4: persisting in a reactionary stand [syn: {obstinate}, {unregenerate}]
