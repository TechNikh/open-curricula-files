---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lung
offline_file: ""
offline_thumbnail: ""
uuid: 0ff56a7b-7f8c-4d74-9b9a-2cbef5b6595b
updated: 1484310249
title: lung
categories:
    - Dictionary
---
lung
     n : either of two saclike respiratory organs in the chest of
         vertebrates; serves to remove carbon dioxide and provide
         oxygen to the blood
