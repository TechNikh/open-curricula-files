---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Universal_vertex
offline_file: ""
offline_thumbnail: ""
uuid: 2a1cb2a0-745a-420f-96db-b8752fce8765
updated: 1484308622
title: Universal vertex
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_1.png
categories:
    - Graph theory
---
In graph theory, a universal vertex is a vertex of an undirected graph that is adjacent to all other vertices of the graph. It may also be called a dominating vertex, as it forms a one-element dominating set in the graph. In a graph with n vertices, it is a vertex whose degree is exactly n − 1. Therefore, like the split graphs, graphs with a universal vertex can be recognized purely by their degree sequences, without looking at the structure of the graph.
