---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Amicable_numbers
offline_file: ""
offline_thumbnail: ""
uuid: 3c308476-b220-427c-8cf0-0c85b2ca4c58
updated: 1484308504
title: Amicable numbers
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Amicable_numbers_rods_220_and_284.png
tags:
    - History
    - Rules for generation
    - Thābit ibn Qurra theorem
    - "Euler's rule"
    - Regular pairs
    - Twin amicable pairs
    - Other results
    - References in popular culture
    - Generalizations
    - Amicable tuples
    - Sociable numbers
    - Searching for sociable numbers
    - Notes
categories:
    - Number theory
---
Amicable numbers are two different numbers so related that the sum of the proper divisors of each is equal to the other number. (A proper divisor of a number is a positive factor of that number other than the number itself. For example, the proper divisors of 6 are 1, 2, and 3.) A pair of amicable numbers constitutes an aliquot sequence of period 2. A related concept is that of a perfect number, which is a number that equals the sum of its own proper divisors, in other words a number which forms an aliquot sequence of period 1. Numbers that are members of an aliquot sequence with period greater than 2 are known as sociable numbers.
