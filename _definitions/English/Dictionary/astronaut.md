---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/astronaut
offline_file: ""
offline_thumbnail: ""
uuid: 711bc063-e956-443c-8597-8e0191b6566f
updated: 1484310451
title: astronaut
categories:
    - Dictionary
---
astronaut
     n : a person trained to travel in a spacecraft; "the Russians
         called their astronauts cosmonauts" [syn: {spaceman}, {cosmonaut}]
