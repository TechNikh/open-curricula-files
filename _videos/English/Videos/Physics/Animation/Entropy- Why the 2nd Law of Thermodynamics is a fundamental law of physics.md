---
version: 1
type: video
provider: YouTube
id: vE82PDJB8ow
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Entropy-%20Why%20the%202nd%20Law%20of%20Thermodynamics%20is%20a%20fundamental%20law%20of%20physics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: af8ca8d1-3900-4647-b06a-7f8c3523eb16
updated: 1486069657
title: 'Entropy- Why the 2nd Law of Thermodynamics is a fundamental law of physics'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/vE82PDJB8ow/default.jpg
    - https://i3.ytimg.com/vi/vE82PDJB8ow/1.jpg
    - https://i3.ytimg.com/vi/vE82PDJB8ow/2.jpg
    - https://i3.ytimg.com/vi/vE82PDJB8ow/3.jpg
---
