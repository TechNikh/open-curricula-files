---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/icon
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484428741
title: icon
categories:
    - Dictionary
---
icon
     n 1: (computer science) a graphic symbol (usually a simple
          picture) that denotes a program or a command or a data
          file or a concept in a graphical user interface
     2: a visual representation (of an object or scene or person or
        abstraction) produced on a surface; "they showed us the
        pictures of their wedding"; "a movie is a series of images
        projected so rapidly that the eye integrates them" [syn: {picture},
         {image}, {ikon}]
     3: a conventional religious painting in oil on a small wooden
        panel; venerated in the Eastern Church [syn: {ikon}]
