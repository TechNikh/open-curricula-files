---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bloch_oscillations
offline_file: ""
offline_thumbnail: ""
uuid: fa94cfea-f83c-474c-b840-8676e59dd44b
updated: 1484308813
title: Bloch oscillations
tags:
    - Derivation
    - Discovery and experimental realizations
categories:
    - Oscillation
---
Bloch oscillation is a phenomenon from solid state physics. It describes the oscillation of a particle (e.g. an electron) confined in a periodic potential when a constant force is acting on it. It was first pointed out by Bloch and Zener while studying the electrical properties of crystals. In particular, they predicted that the motion of electrons in a perfect crystal under the action of a constant electric field would be oscillatory instead of uniform. While in natural crystals this phenomenon is extremely hard to observe due to the scattering of electrons by lattice defects, it has been observed in semiconductor superlattices and in different physical systems such as cold atoms in an ...
