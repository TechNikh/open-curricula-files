---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compulsion
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484548981
title: compulsion
categories:
    - Dictionary
---
compulsion
     n 1: an urge to do or say something that might be better left
          undone or unsaid [syn: {irresistible impulse}]
     2: an irrational motive for performing trivial or repetitive
        actions against your will [syn: {obsession}]
     3: using force to cause something; "though pressed into rugby
        under compulsion I began to enjoy the game"; "they didn`t
        have to use coercion" [syn: {coercion}]
