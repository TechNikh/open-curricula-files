---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insignificant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484640061
title: insignificant
categories:
    - Dictionary
---
insignificant
     adj 1: not large enough to consider or notice [syn: {trivial}]
     2: not worthy of notice [syn: {undistinguished}]
     3: signifying nothing; "insignificant sounds"; "his
        response...is picayune and unmeaning"- R.B. Pearsall [syn:
         {unmeaning}]
     4: of little importance or influence or power; of minor status;
        "a minor, insignificant bureaucrat"; "peanut politicians"
        [syn: {peanut}]
     5: not important or noteworthy [syn: {unimportant}] [ant: {significant}]
