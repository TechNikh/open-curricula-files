---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inconsistent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484313841
title: inconsistent
categories:
    - Dictionary
---
inconsistent
     adj 1: displaying a lack of consistency; "inconsistent statements";
            "inconsistent with the roadmap" [ant: {consistent}]
     2: not capable of being made consistent or harmonious;
        "inconsistent accounts"
     3: not in agreement [syn: {discrepant}]
