---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/halogen
offline_file: ""
offline_thumbnail: ""
uuid: ac45b6af-86d7-4e4b-8614-b79babd111db
updated: 1484310399
title: halogen
categories:
    - Dictionary
---
halogen
     n : any of five related nonmetallic elements (fluorine or
         chlorine or bromine or iodine or astatine) that are all
         monovalent and readily form negative ions
