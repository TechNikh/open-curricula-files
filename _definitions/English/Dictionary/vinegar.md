---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vinegar
offline_file: ""
offline_thumbnail: ""
uuid: 6a1285d5-e8af-45ca-9ea2-095814d65c58
updated: 1484310343
title: vinegar
categories:
    - Dictionary
---
vinegar
     n 1: sour-tasting liquid produced usually by oxidation of the
          alcohol in wine or cider and used as a condiment or food
          preservative [syn: {acetum}]
     2: dilute acetic acid
