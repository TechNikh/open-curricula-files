---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cleaner
offline_file: ""
offline_thumbnail: ""
uuid: 742bd4fa-3a34-4691-bf3d-807d5d0629a8
updated: 1484310547
title: cleaner
categories:
    - Dictionary
---
cleaner
     n 1: a preparation used in cleaning something [syn: {cleansing
          agent}, {cleanser}]
     2: the operator of dry-cleaning establishment [syn: {dry
        cleaner}]
     3: someone whose occupation is cleaning
