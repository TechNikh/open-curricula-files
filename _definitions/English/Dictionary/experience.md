---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/experience
offline_file: ""
offline_thumbnail: ""
uuid: 2576dcbd-91d4-4378-9dd7-b5a490f1782a
updated: 1484310363
title: experience
categories:
    - Dictionary
---
experience
     n 1: the accumulation of knowledge or skill that results from
          direct participation in events or activities; "a man of
          experience"; "experience is the best teacher" [ant: {inexperience}]
     2: the content of direct observation or participation in an
        event; "he had a religious experience"; "he recalled the
        experience vividly"
     3: an event as apprehended; "a surprising experience"; "that
        painful experience certainly got our attention"
     v 1: go or live through; "We had many trials to go through"; "he
          saw action in Viet Nam" [syn: {undergo}, {see}, {go
          through}]
     2: have firsthand knowledge of states, ...
