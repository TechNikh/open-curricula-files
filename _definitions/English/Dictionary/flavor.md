---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flavor
offline_file: ""
offline_thumbnail: ""
uuid: a8d53baa-6edf-439b-a4a6-d03b7fbd5b93
updated: 1484310346
title: flavor
categories:
    - Dictionary
---
flavor
     n 1: the general atmosphere of a place or situation and the
          effect that it has on people; "the feel of the city
          excited him"; "a clergyman improved the tone of the
          meeting"; "it had the smell of treason" [syn: {spirit},
          {tone}, {feel}, {feeling}, {flavour}, {look}, {smell}]
     2: the taste experience when a savoury condiment is taken into
        the mouth [syn: {relish}, {flavour}, {sapidity}, {savor},
        {savour}, {smack}, {tang}]
     3: (physics) the kinds of quarks and antiquarks [syn: {flavour}]
     v : lend flavor to; "Season the chicken breast after roasting
         it" [syn: {season}, {flavour}]
