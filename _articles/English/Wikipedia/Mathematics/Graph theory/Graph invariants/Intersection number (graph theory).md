---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Intersection_number_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 8cceb38d-12e7-4bbd-861d-293f35b60407
updated: 1484309422
title: Intersection number (graph theory)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Erd%25C5%2591s%25E2%2580%2593Faber%25E2%2580%2593Lov%25C3%25A1sz_conjecture.svg.png'
tags:
    - Intersection graphs
    - Clique edge covers
    - Upper bounds
    - Computational complexity
categories:
    - Graph invariants
---
In the mathematical field of graph theory, the intersection number of a graph G = (V,E) is the smallest number of elements in a representation of G as an intersection graph of finite sets. Equivalently, it is the smallest number of cliques needed to cover all of the edges of G.[1][2]
