---
version: 1
type: video
provider: YouTube
id: wfUGEbCod2Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a869d938-354a-4d6c-b429-d1894f6305fc
updated: 1486069621
title: Probability Shortcut Trick for Competitive Exams
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/wfUGEbCod2Q/default.jpg
    - https://i3.ytimg.com/vi/wfUGEbCod2Q/1.jpg
    - https://i3.ytimg.com/vi/wfUGEbCod2Q/2.jpg
    - https://i3.ytimg.com/vi/wfUGEbCod2Q/3.jpg
---
