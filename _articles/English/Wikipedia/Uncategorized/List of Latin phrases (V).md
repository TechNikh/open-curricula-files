---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vice_versa
offline_file: ""
offline_thumbnail: ""
uuid: 034a04f3-1149-4c8d-bac1-63bfee73a447
updated: 1484308355
title: List of Latin phrases (V)
categories:
    - Uncategorized
---
This page lists English translations of notable Latin phrases, such as veni vidi vici and et cetera. Some of the phrases are themselves translations of Greek phrases, as Greek rhetoric and literature reached its peak centuries before the rise of ancient Rome.
