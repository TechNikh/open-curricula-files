---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mendeleev
offline_file: ""
offline_thumbnail: ""
uuid: 96b790a4-944f-486e-9b43-1a34ed9bd5cf
updated: 1484308384
title: Dmitri Mendeleev
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dmitri_Ivanowitsh_Mendeleev.jpg
tags:
    - Early life
    - Later life
    - Periodic table
    - Other achievements
    - Vodka myth
    - Commemoration
categories:
    - Uncategorized
---
Dmitri Ivanovich Mendeleev[3] (/ˌmɛndəlˈeɪəf/;[4] Russian: Дми́трий Ива́нович Менделе́ев; IPA: [ˈdmʲitrʲɪj ɪˈvanəvʲɪtɕ mʲɪndʲɪˈlʲejɪf] ( listen); 8 February 1834 – 2 February 1907 O.S. 27 January 1834 – 20 January 1907) was a Russian chemist and inventor. He formulated the Periodic Law, created a farsighted version of the periodic table of elements, and used it to correct the properties of some already discovered elements and also to predict the properties of eight elements yet to be discovered.
