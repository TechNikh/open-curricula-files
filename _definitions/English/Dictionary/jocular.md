---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jocular
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484571001
title: jocular
categories:
    - Dictionary
---
jocular
     adj : characterized by jokes and good humor [syn: {jesting}, {jocose},
            {joking}]
     adv : with humor; "they tried to deal with this painful subject
           jocularly" [syn: {jocosely}]
