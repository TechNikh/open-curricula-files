---
version: 1
type: video
provider: YouTube
id: 1dgTtufwKqw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/what%20is%20Backscatter.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d5a4a4c0-ff98-434a-91ce-d87750e181c6
updated: 1486069654
title: what is Backscatter?
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/1dgTtufwKqw/default.jpg
    - https://i3.ytimg.com/vi/1dgTtufwKqw/1.jpg
    - https://i3.ytimg.com/vi/1dgTtufwKqw/2.jpg
    - https://i3.ytimg.com/vi/1dgTtufwKqw/3.jpg
---
