---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coach
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484312341
title: coach
categories:
    - Dictionary
---
coach
     n 1: (sports) someone in charge of training an athlete or a team
          [syn: {manager}, {handler}]
     2: a person who gives private instruction (as in singing or
        acting) [syn: {private instructor}, {tutor}]
     3: a railcar where passengers ride [syn: {passenger car}, {carriage}]
     4: a carriage pulled by four horses with one driver [syn: {four-in-hand},
         {coach-and-four}]
     5: a vehicle carrying many passengers; used for public
        transport; "he always rode the bus to work" [syn: {bus}, {autobus},
         {charabanc}, {double-decker}, {jitney}, {motorbus}, {motorcoach},
         {omnibus}]
     v 1: teach and supervise (someone); act as a ...
