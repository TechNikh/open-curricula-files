---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/housing
offline_file: ""
offline_thumbnail: ""
uuid: 474a7c4d-c23e-42e5-905a-6c6fe9ade5b3
updated: 1484310473
title: housing
categories:
    - Dictionary
---
housing
     n 1: housing structures collectively; structures in which people
          are housed [syn: {lodging}, {living accommodations}]
     2: a protective cover designed to contain or support a
        mechanical component
     3: stable gear consisting of a decorated covering for a horse,
        especially (formerly) for a warhorse [syn: {caparison}, {trapping},
         {trappings}, {housings}]
