---
version: 1
type: video
provider: YouTube
id: m_BKtnjJ3aI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c8cceec8-3ab4-4d1c-970e-acd8b148b63e
updated: 1486069637
title: Optics interference experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/m_BKtnjJ3aI/default.jpg
    - https://i3.ytimg.com/vi/m_BKtnjJ3aI/1.jpg
    - https://i3.ytimg.com/vi/m_BKtnjJ3aI/2.jpg
    - https://i3.ytimg.com/vi/m_BKtnjJ3aI/3.jpg
---
