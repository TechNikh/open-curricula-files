---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alan_Weinstein
offline_file: ""
offline_thumbnail: ""
uuid: 9bfdc1eb-0c33-4c5b-8d05-28c47c1b3b4f
updated: 1484309110
title: Alan Weinstein
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-AlanWeinsteinbyMargoWeinstein.jpg
tags:
    - Books
    - Notes
categories:
    - Geometers
---
Alan David Weinstein (17 June 1943, New York City)[1] is a professor of mathematics at the University of California, Berkeley who works in symplectic geometry, Poisson Geometry and Mathematical Physics.
