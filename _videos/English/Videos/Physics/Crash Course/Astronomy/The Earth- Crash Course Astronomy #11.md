---
version: 1
type: video
provider: YouTube
id: w-9gDALvMF4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/The%20Earth-%20Crash%20Course%20Astronomy%20%2311.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c9fe1370-000c-4f67-a7de-5b67f0da2dd4
updated: 1486069684
title: 'The Earth- Crash Course Astronomy #11'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/w-9gDALvMF4/default.jpg
    - https://i3.ytimg.com/vi/w-9gDALvMF4/1.jpg
    - https://i3.ytimg.com/vi/w-9gDALvMF4/2.jpg
    - https://i3.ytimg.com/vi/w-9gDALvMF4/3.jpg
---
