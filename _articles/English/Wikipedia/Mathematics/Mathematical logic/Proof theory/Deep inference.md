---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deep_inference
offline_file: ""
offline_thumbnail: ""
uuid: fba7a418-48f3-4f4e-b4d0-40df17e44cba
updated: 1484309490
title: Deep inference
categories:
    - Proof theory
---
Deep inference names a general idea in structural proof theory that breaks with the classical sequent calculus by generalising the notion of structure to permit inference to occur in contexts of high structural complexity. The term deep inference is generally reserved for proof calculi where the structural complexity is unbounded; in this article we will use non-shallow inference to refer to calculi that have structural complexity greater than the sequent calculus, but not unboundedly so, although this is not at present established terminology.
