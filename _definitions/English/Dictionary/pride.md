---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pride
offline_file: ""
offline_thumbnail: ""
uuid: af77ae15-1e91-4796-b108-5854c759315c
updated: 1484310537
title: pride
categories:
    - Dictionary
---
pride
     n 1: a feeling of self-respect and personal worth [syn: {pridefulness}]
          [ant: {humility}]
     2: satisfaction with your (or another's) achievements; "he
        takes pride in his son's success"
     3: the trait of being spurred on by a dislike of falling below
        your standards [ant: {humility}]
     4: a group of lions
     5: unreasonable and inordinate self-esteem (personified as one
        of the deadly sins) [syn: {superbia}]
     v : be proud of; "He prides himself on making it into law
         school" [syn: {plume}, {congratulate}]
