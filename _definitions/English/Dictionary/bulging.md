---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bulging
offline_file: ""
offline_thumbnail: ""
uuid: 87dc0265-c78b-481e-8e6c-0fed7d8658b7
updated: 1484310208
title: bulging
categories:
    - Dictionary
---
bulging
     adj 1: curving or bulging outward [syn: {convex}] [ant: {concave}]
     2: curving outward [syn: {bellied}, {bellying}, {bulbous}, {bulgy},
         {protuberant}]
