---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/run-off
offline_file: ""
offline_thumbnail: ""
uuid: 0c7c29bb-f8d4-480a-a7ec-a5103be9f753
updated: 1484310461
title: run-off
categories:
    - Dictionary
---
run off
     v 1: run away; usually includes taking something or somebody
          along [syn: {abscond}, {bolt}, {absquatulate}, {decamp},
           {go off}]
     2: leave suddenly and as if in a hurry; "The listeners bolted
        when he discussed his strange ideas"; "When she started to
        tell silly stories, I ran out" [syn: {run out}, {bolt}, {bolt
        out}, {beetle off}]
     3: force to go away; used both with concrete and metaphoric
        meanings; "Drive away potential burglars"; "drive away bad
        thoughts"; "dispel doubts"; "The supermarket had to turn
        back many disappointed customers" [syn: {chase away}, {drive
        out}, {turn back}, {drive ...
