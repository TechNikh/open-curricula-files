---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relish
offline_file: ""
offline_thumbnail: ""
uuid: 4c1b0a5e-136b-42d6-a588-6c208ebda9cf
updated: 1484310346
title: relish
categories:
    - Dictionary
---
relish
     n 1: vigorous and enthusiastic enjoyment [syn: {gusto}, {zest}, {zestfulness}]
     2: spicy or savory condiment
     3: the taste experience when a savoury condiment is taken into
        the mouth [syn: {flavor}, {flavour}, {sapidity}, {savor},
        {savour}, {smack}, {tang}]
     v : derive or receive pleasure from; get enjoyment from; take
         pleasure in; "She relished her fame and basked in her
         glory" [syn: {enjoy}, {bask}, {savor}, {savour}]
