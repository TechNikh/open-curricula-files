---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/occur
offline_file: ""
offline_thumbnail: ""
uuid: de14216c-690f-4f7d-880e-a37a15879e64
updated: 1484310357
title: occur
categories:
    - Dictionary
---
occur
     v 1: come to pass; "What is happening?"; "The meeting took place
          off without an incidence"; "Nothing occurred that seemed
          important" [syn: {happen}, {hap}, {go on}, {pass off}, {pass},
           {fall out}, {come about}, {take place}]
     2: come to one's mind; suggest itself; "It occurred to me that
        we should hire another secretary"; "A great idea then came
        to her" [syn: {come}]
     3: to be found to exist; "sexism occurs in many workplaces";
        "precious stones occur in a large area in Brazil"
     [also: {occurring}, {occurred}]
