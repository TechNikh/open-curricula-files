---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dilute
offline_file: ""
offline_thumbnail: ""
uuid: 9a0789de-4e77-41db-abf8-a6d0a554b6a5
updated: 1484310339
title: dilute
categories:
    - Dictionary
---
dilute
     adj : reduced in strength or concentration or quality or purity;
           "diluted alcohol"; "a dilute solution"; "dilute acetic
           acid" [syn: {diluted}] [ant: {undiluted}]
     v 1: lessen the strength or flavor of a solution or mixture; "cut
          bourbon" [syn: {thin}, {thin out}, {reduce}, {cut}]
     2: corrupt, debase, or make impure by adding a foreign or
        inferior substance; often by replacing valuable
        ingredients with inferior ones; "adulterate liquor" [syn:
        {adulterate}, {stretch}, {debase}]
