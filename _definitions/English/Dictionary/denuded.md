---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/denuded
offline_file: ""
offline_thumbnail: ""
uuid: 753c8fc0-baee-4054-9e16-37ef19439542
updated: 1484310545
title: denuded
categories:
    - Dictionary
---
denuded
     adj : without the natural or usual covering; "a bald spot on the
           lawn"; "bare hills" [syn: {bald}, {bare}, {denudate}]
