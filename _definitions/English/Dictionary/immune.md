---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immune
offline_file: ""
offline_thumbnail: ""
uuid: 7015c24f-154b-447d-8be2-04a62f1f4a31
updated: 1484310325
title: immune
categories:
    - Dictionary
---
immune
     adj 1: relating to the condition of immunity; "the immune system"
     2: secure against; "immune from taxation as long as he resided
        in Bermuda"; "immune from criminal prosecution"
     3: relating to or conferring immunity (to disease or infection)
        [syn: {resistant}]
     4: (usually followed by `to') not affected by a given
        influence; "immune to persuasion"
     n : a person who is immune to a particular infection
