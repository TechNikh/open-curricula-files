---
version: 1
type: video
provider: YouTube
id: C5Lbjbyr1t4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/%282%5Eln%20x%29-x%20%20Antiderivative%20Example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f0cd2f95-da0f-4a8b-ab01-383ae28e5b18
updated: 1486069696
title: '(2^ln x)/x  Antiderivative Example'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/C5Lbjbyr1t4/default.jpg
    - https://i3.ytimg.com/vi/C5Lbjbyr1t4/1.jpg
    - https://i3.ytimg.com/vi/C5Lbjbyr1t4/2.jpg
    - https://i3.ytimg.com/vi/C5Lbjbyr1t4/3.jpg
---
