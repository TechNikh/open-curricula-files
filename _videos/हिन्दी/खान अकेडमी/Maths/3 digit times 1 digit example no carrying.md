---
version: 1
type: video
provider: YouTube
id: 8L4JhU08p5c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bd1af622-a06e-47a0-bb5f-b87737d2cb3a
updated: 1486069605
title: 3 digit times 1 digit example no carrying
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/8L4JhU08p5c/default.jpg
    - https://i3.ytimg.com/vi/8L4JhU08p5c/1.jpg
    - https://i3.ytimg.com/vi/8L4JhU08p5c/2.jpg
    - https://i3.ytimg.com/vi/8L4JhU08p5c/3.jpg
---
