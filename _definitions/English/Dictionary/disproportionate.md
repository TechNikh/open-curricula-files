---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disproportionate
offline_file: ""
offline_thumbnail: ""
uuid: ce726a3b-f1c8-48b9-9d3e-22cd329ff11e
updated: 1484310539
title: disproportionate
categories:
    - Dictionary
---
disproportionate
     adj 1: out of proportion [syn: {disproportional}] [ant: {proportionate}]
     2: not proportionate
