---
version: 1
type: video
provider: YouTube
id: L6pH8O3B4ak
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/%C2%96%20foreign%20plurals/Irregular%20plural%20nouns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 109f69c3-f3f7-4092-afaa-acfd24b51ec5
updated: 1486069601
title: Irregular plural nouns
tags:
    - Education
    - online learning
    - learning
    - lessons
    - grammar
    - plurals
    - latin and greek plural
    - latin in English
    - Greek in English
categories:
    -  foreign plurals
thumbnail_urls:
    - https://i3.ytimg.com/vi/L6pH8O3B4ak/default.jpg
    - https://i3.ytimg.com/vi/L6pH8O3B4ak/1.jpg
    - https://i3.ytimg.com/vi/L6pH8O3B4ak/2.jpg
    - https://i3.ytimg.com/vi/L6pH8O3B4ak/3.jpg
---
You don't need to know how to speak Latin and Greek in order to understand English, but some words in English come from those languages. It's helpful to know how some Greek and Latin words change from singular to plural.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/e/irregular-plural-nouns--foreign-plurals?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/v/bonus-video-origin-of-the-mutant-plural-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/v/irregular-plural-nouns-part-iv-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
