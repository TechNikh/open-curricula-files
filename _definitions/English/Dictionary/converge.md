---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/converge
offline_file: ""
offline_thumbnail: ""
uuid: 98924890-32cb-4311-a861-a721c39971f7
updated: 1484310268
title: converge
categories:
    - Dictionary
---
converge
     v 1: be adjacent or come together; "The lines converge at this
          point" [syn: {meet}] [ant: {diverge}, {diverge}]
     2: approach a limit as the number of terms increases without
        limit [ant: {diverge}]
     3: move or draw together at a certain location; "The crowd
        converged on the movie star" [ant: {diverge}]
     4: come together so as to form a single product; "Social forces
        converged to bring the Fascists back to power"
