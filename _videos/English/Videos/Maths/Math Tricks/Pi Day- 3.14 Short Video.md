---
version: 1
type: video
provider: YouTube
id: pojZaNuZA-k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3157ee76-c7bd-4d86-a111-5c7a6d0ced5a
updated: 1486069623
title: 'Pi Day- 3.14 Short Video'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/pojZaNuZA-k/default.jpg
    - https://i3.ytimg.com/vi/pojZaNuZA-k/1.jpg
    - https://i3.ytimg.com/vi/pojZaNuZA-k/2.jpg
    - https://i3.ytimg.com/vi/pojZaNuZA-k/3.jpg
---
