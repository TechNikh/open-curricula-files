---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Binary_entropy_function
offline_file: ""
offline_thumbnail: ""
uuid: 28895dc5-a677-4b0e-bf8d-09b34d70c5af
updated: 1484308764
title: Binary entropy function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Binary_entropy_plot.svg.png
tags:
    - Explanation
    - Derivative
    - Taylor series
categories:
    - Entropy and information
---
In information theory, the binary entropy function, denoted 
  
    
      
        H
        ⁡
        (
        p
        )
      
    
    {\displaystyle \operatorname {H} (p)}
  
 or 
  
    
      
        
          H
          
            b
          
        
        ⁡
        (
        p
        )
      
    
    {\displaystyle \operatorname {H} _{\text{b}}(p)}
  
, is defined as the entropy of a Bernoulli process with probability of success 
  
    
      
        p
      
    
    {\displaystyle p}
  
. Mathematically, the Bernoulli trial is modelled as a random variable 
  
    
      
        X
      
    
    {\displaystyle X}
  
 that can take on only two values: 0 and ...
