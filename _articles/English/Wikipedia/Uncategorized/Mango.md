---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mangoes
offline_file: ""
offline_thumbnail: ""
uuid: 8a19cc3a-3fed-4978-86cc-eda774fe1d9c
updated: 1484308342
title: Mango
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sindhri_Mango.JPG
tags:
    - Description
    - Etymology
    - Cultivation
    - Cultivars
    - Production
    - Food
    - Cuisine
    - Food constituents
    - Nutrients
    - Phytochemicals
    - Flavor
    - Potential for contact dermatitis
    - Cultural significance
    - Gallery
categories:
    - Uncategorized
---
The mango is a juicy stone fruit (drupe) belonging to the genus Mangifera, consisting of numerous tropical fruiting trees, cultivated mostly for edible fruit. The majority of these species are found in nature as wild mangoes. They all belong to the flowering plant family Anacardiaceae. The mango is native to South Asia,[2][3] from where it has been distributed worldwide to become one of the most cultivated fruits in the tropics.
