---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/school
offline_file: ""
offline_thumbnail: ""
uuid: 46edab95-a046-480d-878f-f15598585634
updated: 1484310353
title: school
categories:
    - Dictionary
---
school
     n 1: an educational institution; "the school was founded in 1900"
     2: a building where young people receive education; "the school
        was built in 1932"; "he walked to school every morning"
        [syn: {schoolhouse}]
     3: the process of being formally educated at a school; "what
        will you do when you finish school?" [syn: {schooling}]
     4: an educational institution's faculty and students; "the
        school keeps parents informed"; "the whole school turned
        out for the game"
     5: the period of instruction in a school; the time period when
        schools is in session; "stay after school"; "he didn't
        miss a single day of school"; "when ...
