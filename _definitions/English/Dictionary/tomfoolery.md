---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tomfoolery
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484456161
title: tomfoolery
categories:
    - Dictionary
---
tomfoolery
     n : foolish or senseless behavior [syn: {folly}, {foolery}, {craziness},
          {lunacy}, {indulgence}]
