---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Turing_machine
offline_file: ""
offline_thumbnail: ""
uuid: e07e50b7-2605-4dfb-9ddd-e829db0c5d90
updated: 1484308681
title: Turing machine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Turing_machine_2a.svg.png
tags:
    - Overview
    - Physical description
    - Informal description
    - Formal definition
    - >
        Additional details required to visualize or implement Turing
        machines
    - Alternative definitions
    - The "state"
    - Turing machine "state" diagrams
    - Models equivalent to the Turing machine model
    - Choice c-machines, Oracle o-machines
    - Universal Turing machines
    - Comparison with real machines
    - Limitations of Turing machines
    - Computational complexity theory
    - Concurrency
    - History
    - 'Historical background: computational machinery'
    - |
        The Entscheidungsproblem (the "decision problem"): Hilbert's tenth question of 1900
    - "Alan Turing's a- (automatic-)machine"
    - '1937–1970: The "digital computer", the birth of "computer science"'
    - '1970–present: the Turing machine as a model of computation'
    - Notes
    - Primary literature, reprints, and compilations
    - Computability theory
    - "Church's thesis"
    - Small Turing machines
    - Other
categories:
    - Theoretical computer science
---
A Turing machine is an abstract machine[1] that manipulates symbols on a strip of tape according to a table of rules; to be more exact, it is a mathematical model of computation that defines such a device.[2] Despite the model's simplicity, given any computer algorithm, a Turing machine can be constructed that is capable of simulating that algorithm's logic.[3]
