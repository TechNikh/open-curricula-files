---
version: 1
type: video
provider: YouTube
id: qs0hRHfnzTQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Conceptual%20clarification%20for%202D%20divergence%20theorem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c5d5dfff-345c-4f47-a777-c070e39911d2
updated: 1486069690
title: Conceptual clarification for 2D divergence theorem
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/qs0hRHfnzTQ/default.jpg
    - https://i3.ytimg.com/vi/qs0hRHfnzTQ/1.jpg
    - https://i3.ytimg.com/vi/qs0hRHfnzTQ/2.jpg
    - https://i3.ytimg.com/vi/qs0hRHfnzTQ/3.jpg
---
