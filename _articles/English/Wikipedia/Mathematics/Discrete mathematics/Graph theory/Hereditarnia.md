---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hereditarnia
offline_file: ""
offline_thumbnail: ""
uuid: 7c12eca2-e251-4552-b7b4-c41bc7a13f30
updated: 1484308625
title: Hereditarnia
tags:
    - Hereditary properties of graphs
    - Hereditarnia Club
    - Workshop Hereditarnia
categories:
    - Graph theory
---
Hereditarnia is a virtual pleasant place that is related to the development of the theory of hereditary properties of graphs (a part of graph theory). Hereditarnia is a special word that cannot be found in any dictionary. It was invented by Izak Broere. It is a combination of English word heredity and a quite common Polish suffix 'arnia' that is used in many words denoting pleasant places like kwiaciarnia, piwiarnia, Palmiarnia Zielenogórska, winiarnia, etc.
