---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comforting
offline_file: ""
offline_thumbnail: ""
uuid: b6fc3153-2411-40a5-ab41-5696b132561a
updated: 1484310254
title: comforting
categories:
    - Dictionary
---
comforting
     adj 1: providing freedom from worry [syn: {cheering}, {satisfying}]
     2: affording comfort or solace [syn: {consolatory}, {consoling}]
