---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distinguished
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484501521
title: distinguished
categories:
    - Dictionary
---
distinguished
     adj 1: (used of persons) standing above others in character or
            attainment or reputation; "our distinguished
            professor"; "an eminent scholar"; "a great statesman"
            [syn: {eminent}, {great}]
     2: used of a person's appearance or behavior; befitting an
        eminent person; "his distinguished bearing"; "the
        monarch's imposing presence"; "she reigned in magisterial
        beauty" [syn: {imposing}, {magisterial}]
     3: set apart from other such things
