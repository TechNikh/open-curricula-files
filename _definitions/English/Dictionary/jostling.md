---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jostling
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484562361
title: jostling
categories:
    - Dictionary
---
jostling
     n : the act of jostling (forcing your way by pushing) [syn: {jostle}]
