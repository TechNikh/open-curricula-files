---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/water
offline_file: ""
offline_thumbnail: ""
uuid: e04c95a5-99dd-493b-b039-776eabba4651
updated: 1484310346
title: water
categories:
    - Dictionary
---
water
     n 1: binary compound that occurs at room temperature as a clear
          colorless odorless tasteless liquid; freezes into ice
          below 0 degrees centigrade and boils above 100 degrees
          centigrade; widely used as a solvent [syn: {H2O}]
     2: the part of the earth's surface covered with water (such as
        a river or lake or ocean); "they invaded our territorial
        waters"; "they were sitting by the water's edge" [syn: {body
        of water}]
     3: facility that provides a source of water; "the town debated
        the purification of the water supply"; "first you have to
        cut off the water" [syn: {water system}, {water supply}]
     4: once ...
