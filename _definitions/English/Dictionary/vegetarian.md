---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vegetarian
offline_file: ""
offline_thumbnail: ""
uuid: 22e805ba-70a6-403e-8a5b-04d4262ca1d6
updated: 1484310290
title: vegetarian
categories:
    - Dictionary
---
vegetarian
     n : eater of fruits and grains and nuts; someone who eats no
         meat or fish or (often) any animal products
