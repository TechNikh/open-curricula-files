---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Auschwitz
offline_file: ""
offline_thumbnail: ""
uuid: 7ee78013-93c8-485e-992a-0f7ade853a23
updated: 1484308482
title: Auschwitz concentration camp
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Auschwitz_I_Entrance.jpg
tags:
    - History
    - Background
    - Auschwitz I
    - Auschwitz II-Birkenau
    - The Gypsy camp
    - Auschwitz III
    - Subcamps
    - Evacuation, death marches, and liberation
    - After the war
    - Command and control
categories:
    - Uncategorized
---
Auschwitz concentration camp (German: Konzentrationslager Auschwitz, also KZ Auschwitz [kɔntsɛntʁaˈtsi̯oːnsˌlaːɡɐ ˈʔaʊʃvɪts] ( listen)) was a network of German Nazi concentration camps and extermination camps built and operated by the Third Reich in Polish areas annexed by Nazi Germany during World War II. It consisted of Auschwitz I (the original camp), Auschwitz II–Birkenau (a combination concentration/extermination camp), Auschwitz III–Monowitz (a labor camp to staff an IG Farben factory), and 45 satellite camps.
