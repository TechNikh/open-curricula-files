---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greeting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484541481
title: greeting
categories:
    - Dictionary
---
greeting
     n : (usually plural) an acknowledgment or expression of good
         will (especially on meeting) [syn: {salutation}]
