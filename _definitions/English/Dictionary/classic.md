---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/classic
offline_file: ""
offline_thumbnail: ""
uuid: ca68f54e-b354-47ca-8bfb-d892b98e2d44
updated: 1484310539
title: classic
categories:
    - Dictionary
---
classic
     adj 1: characteristic of the classical artistic and literary
            traditions
     2: adhering to established standards and principles; "a classic
        proof"
     n 1: a creation of the highest excellence
     2: an artist who has created classic works
