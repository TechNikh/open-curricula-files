---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/landed
offline_file: ""
offline_thumbnail: ""
uuid: 48c14061-c3c3-4ce7-8db2-329d280df4e1
updated: 1484310471
title: landed
categories:
    - Dictionary
---
landed
     adj : owning or consisting of land or real estate; "the landed
           gentry"; "landed property" [ant: {landless}]
