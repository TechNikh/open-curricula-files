---
version: 1
type: video
provider: YouTube
id: 1lyrKSs-w5s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20Torque.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4c38fc86-11e7-4875-828a-ae4ee687d843
updated: 1486069654
title: What is Torque
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/1lyrKSs-w5s/default.jpg
    - https://i3.ytimg.com/vi/1lyrKSs-w5s/1.jpg
    - https://i3.ytimg.com/vi/1lyrKSs-w5s/2.jpg
    - https://i3.ytimg.com/vi/1lyrKSs-w5s/3.jpg
---
