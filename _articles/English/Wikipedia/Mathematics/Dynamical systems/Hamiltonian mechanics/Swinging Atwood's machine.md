---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Swinging_Atwood%27s_machine'
offline_file: ""
offline_thumbnail: ""
uuid: 23670873-729d-4583-b5ed-eedac3b5d445
updated: 1484308797
title: "Swinging Atwood's machine"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Swinging_Atwoods_Machine.svg.png
tags:
    - Equations of motion
    - System with massive pulleys
    - Integrability
    - Trajectories
    - Nonsingular orbits
    - Periodic orbits
    - Singular orbits
    - Collision orbits
    - Boundedness
    - Recent three dimensional extension
categories:
    - Hamiltonian mechanics
---
The swinging Atwood's machine (SAM) is a mechanism that resembles a simple Atwood's machine except that one of the masses is allowed to swing in a two-dimensional plane, producing a dynamical system that is chaotic for some system parameters and initial conditions.
