---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kaplan%E2%80%93Yorke_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 5dc475eb-0691-451a-b99a-30ca8c79367a
updated: 1484308797
title: Kaplan–Yorke conjecture
categories:
    - Limit sets
---
In applied mathematics, the Kaplan–Yorke conjecture concerns the dimension of an attractor, using Lyapunov exponents.[1][2] By arranging the Lyapunov exponents in order from largest to smallest 
  
    
      
        
          λ
          
            1
          
        
        ≥
        
          λ
          
            2
          
        
        ≥
        ⋯
        ≥
        
          λ
          
            n
          
        
      
    
    {\displaystyle \lambda _{1}\geq \lambda _{2}\geq \dots \geq \lambda _{n}}
  
, let j be the index for which
