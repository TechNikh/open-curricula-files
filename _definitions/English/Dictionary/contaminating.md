---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contaminating
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484614021
title: contaminating
categories:
    - Dictionary
---
contaminating
     adj 1: spreading pollution or contamination; especially radioactive
            contamination; "the air near the foundry was always
            dirty"; "the air near the foundry was always dirty";
            "a dirty bomb releases enormous amounts of long-lived
            radioactive fallout" [syn: {dirty}] [ant: {clean}]
     2: that infects or taints [syn: {corrupting}]
