---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/train
offline_file: ""
offline_thumbnail: ""
uuid: 0a882568-e59f-4be2-825e-5407f22bae8f
updated: 1484310242
title: train
categories:
    - Dictionary
---
train
     n 1: public transport provided by a line of railway cars coupled
          together and drawn by a locomotive; "express trains
          don't stop at Princeton Junction" [syn: {railroad train}]
     2: a sequentially ordered set of things or events or ideas in
        which each successive member is related to the preceding;
        "a string of islands"; "train of mourners"; "a train of
        thought" [syn: {string}]
     3: a procession (of wagons or mules or camels) traveling
        together in single file; "we were part of a caravan of
        almost a thousand camels"; "they joined the wagon train
        for safety" [syn: {caravan}, {wagon train}]
     4: a series of ...
