---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_polygon
offline_file: ""
offline_thumbnail: ""
uuid: fe1e1122-0690-4af7-965d-a0dbaf7db67b
updated: 1484309359
title: Complex polygon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/160px-Complex_polygon.svg.png
tags:
    - Geometry
    - Computer graphics
    - Citations
    - Bibliography
categories:
    - Geometry stubs
---
