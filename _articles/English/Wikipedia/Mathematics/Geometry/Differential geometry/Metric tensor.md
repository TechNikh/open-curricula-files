---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metric_tensor
offline_file: ""
offline_thumbnail: ""
uuid: 3c9e1afb-40f3-4000-ad6f-9770cf246bea
updated: 1484309230
title: Metric tensor
tags:
    - Introduction
    - Arclength
    - Coordinate transformations
    - Invariance of arclength under coordinate transformations
    - Length and angle
    - area
    - Definition
    - Components of the metric
    - Metric in coordinates
    - Signature of a metric
    - Inverse metric
    - Raising and lowering indices
    - Induced metric
    - Intrinsic definitions of a metric
    - Metric as a section of a bundle
    - Metric in a vector bundle
    - Tangent–cotangent isomorphism
    - Arclength and the line element
    - The energy, variational principles and geodesics
    - Canonical measure and volume form
    - Examples
    - The Euclidean metric
    - The round metric on a sphere
    - Lorentzian metrics from relativity
    - Notes
categories:
    - Differential geometry
---
In the mathematical field of differential geometry, a metric tensor is a type of function which takes as input a pair of tangent vectors v and w at a point of a surface (or higher dimensional differentiable manifold) and produces a real number scalar g(v, w) in a way that generalizes many of the familiar properties of the dot product of vectors in Euclidean space. In the same way as a dot product, metric tensors are used to define the length of and angle between tangent vectors. Through integration, the metric tensor allows one to define and compute the length of curves on the manifold.
