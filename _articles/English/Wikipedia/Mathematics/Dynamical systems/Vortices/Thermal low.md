---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_low
offline_file: ""
offline_thumbnail: ""
uuid: 59d89df6-c2ad-4ea2-a440-9ffb77b1c919
updated: 1484308881
title: Thermal low
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-ThermalLow.png
tags:
    - Formation
    - Role in the monsoon regime
    - Role in sea breeze formation
    - Role in air pollution
categories:
    - Vortices
---
Thermal lows, or heat lows, are non-frontal low-pressure areas that occur over the continents in the subtropics during the warm season, as the result of intense heating when compared to their surrounding environments.[1] Thermal lows occur near the Sonoran Desert, on the Mexican plateau, in California's Great Central Valley, the Sahara, over north-west Argentina in South America, over the Kimberley region of north-west Australia, the Iberian peninsula, and the Tibetan plateau.
