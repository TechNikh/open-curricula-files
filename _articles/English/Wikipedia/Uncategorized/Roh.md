---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Roh
offline_file: ""
offline_thumbnail: ""
uuid: 47e4ba0e-09da-4e2a-8857-c65933345f74
updated: 1484308398
title: Roh
categories:
    - Uncategorized
---
"Roh", sometimes spelled "Ro" is a given name of Sanskrit origins, derived from the male given name Rohit, meaning 'rising sun', or 'red horizon'. It is normally given to boys with Indian descent and Hindu religion, and is normally a name given to boys born into higher castes, (Brahmin, priestly caste, and the kingly/warrior caste).[citation needed] "Roh" also has Germanic roots as a given name and surname, the meaning of which vary from "rough soldier" to "curious knight".[citation needed]
