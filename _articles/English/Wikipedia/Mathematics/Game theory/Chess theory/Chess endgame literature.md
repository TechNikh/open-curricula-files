---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chess_endgame_literature
offline_file: ""
offline_thumbnail: ""
uuid: ea50d13a-62fd-4db5-9dff-03d1d47e5169
updated: 1484309022
title: Chess endgame literature
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Nuvola_apps_package_games_strategy_2.png
tags:
    - History of endgame literature
    - Annotated bibliography
    - Small, general one-volume books
    - Large, more comprehensive one-volume books
    - Multi-volume works
    - Some books on specific endings
    - Pawn endings
    - Rook endings
    - Minor piece endings
    - Other endings
    - Endgame strategy
    - Endgames by specific players
    - Miscellaneous endgame books
    - Magazines
    - Computer
categories:
    - Chess theory
---
Many chess writers have contributed to the theory of endgames over the centuries, including Ruy López de Segura, François-André Philidor, Josef Kling and Bernhard Horwitz, Johann Berger, Alexey Troitsky, Yuri Averbakh, and Reuben Fine. Using computers, Ken Thompson, Eugene Nalimov, and others have contributed by constructing endgame tablebases.
