---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reducing
offline_file: ""
offline_thumbnail: ""
uuid: f51cf8ad-7d77-4dca-b67e-19de2c0783d7
updated: 1484310236
title: reducing
categories:
    - Dictionary
---
reducing
     n 1: any process in which electrons are added to an atom or ion
          (as by removing oxygen or adding hydrogen); always
          occurs accompanied by oxidation of the reducing agent
          [syn: {reduction}]
     2: loss of excess weight (as by dieting); becoming slimmer; "a
        doctor supervised her reducing"
