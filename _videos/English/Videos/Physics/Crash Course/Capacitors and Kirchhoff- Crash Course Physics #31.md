---
version: 1
type: video
provider: YouTube
id: vuCJP_5KOlI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f65fadad-9ef4-4157-adf4-f4c9efa55fdf
updated: 1486069682
title: 'Capacitors and Kirchhoff- Crash Course Physics #31'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/vuCJP_5KOlI/default.jpg
    - https://i3.ytimg.com/vi/vuCJP_5KOlI/1.jpg
    - https://i3.ytimg.com/vi/vuCJP_5KOlI/2.jpg
    - https://i3.ytimg.com/vi/vuCJP_5KOlI/3.jpg
---
