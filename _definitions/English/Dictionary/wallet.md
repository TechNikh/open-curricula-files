---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wallet
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484488741
title: wallet
categories:
    - Dictionary
---
wallet
     n : a pocket-size case for holding papers and paper money [syn:
         {billfold}, {notecase}, {pocketbook}]
