---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/constrained
offline_file: ""
offline_thumbnail: ""
uuid: 774beb0c-60b9-44e0-bd4b-821393aa7a0a
updated: 1484310180
title: constrained
categories:
    - Dictionary
---
constrained
     adj : lacking spontaneity; not natural; "a constrained smile";
           "forced heartiness"; "a strained smile" [syn: {forced},
            {strained}]
