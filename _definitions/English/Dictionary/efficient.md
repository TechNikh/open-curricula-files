---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/efficient
offline_file: ""
offline_thumbnail: ""
uuid: 448fe86d-a328-4266-aaa2-dc22332ca479
updated: 1484310547
title: efficient
categories:
    - Dictionary
---
efficient
     adj 1: being effective without wasting time or effort or expense;
            "an efficient production manager"; "efficient engines
            save gas" [ant: {inefficient}]
     2: able to accomplish a purpose; functioning effectively;
        "people who will do nothing unless they get something out
        of it for themselves are often highly effective
        persons..."-G.B.Shaw; "effective personnel"; "an efficient
        secretary"; "the efficient cause of the revolution" [syn:
        {effective}]
