---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transfixed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484452501
title: transfixed
categories:
    - Dictionary
---
transfixed
     adj : having your attention fixated as though by a spell [syn: {fascinated},
            {hypnotized}, {hypnotised}, {mesmerized}, {mesmerised},
            {spellbound}]
