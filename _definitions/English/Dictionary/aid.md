---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aid
offline_file: ""
offline_thumbnail: ""
uuid: 59e9c489-50be-4225-8573-a7fd37eebd9c
updated: 1484310343
title: aid
categories:
    - Dictionary
---
aid
     n 1: a resource; "visual aids in teaching"; "economic assistance
          to depressed areas" [syn: {assistance}, {help}]
     2: the activity of contributing to the fulfillment of a need or
        furtherance of an effort or purpose; "he gave me an assist
        with the housework"; "could not walk without assistance";
        "rescue party went to their aid"; "offered his help in
        unloading" [syn: {assist}, {assistance}, {help}]
     3: a gift of money to support a worthy person or cause [syn: {economic
        aid}]
     4: the work of caring for or attending to someone or something;
        "no medical care was required"; "the old car needed
        constant ...
