---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cloud
offline_file: ""
offline_thumbnail: ""
uuid: 0843a2eb-5efc-4e4d-a124-772568bf31d5
updated: 1484310206
title: cloud
categories:
    - Dictionary
---
cloud
     n 1: any collection of particles (e.g., smoke or dust) or gases
          that is visible
     2: a visible mass of water or ice particles suspended at a
        considerable altitude
     3: out of touch with reality; "his head was in the clouds"
     4: a cause of worry or gloom or trouble; "the only cloud on the
        horizon was the possibility of dissent by the French"
     5: suspicion affecting your reputation; "after that mistake he
        was under a cloud"
     6: a group of many insects; "a swarm of insects obscured the
        light"; "a cloud of butterflies" [syn: {swarm}]
     v 1: make overcast or cloudy; "Fall weather often overcasts our
          beaches" ...
