---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chromium
offline_file: ""
offline_thumbnail: ""
uuid: 06287cbf-b164-42a7-8db3-de6e7b6e0a9a
updated: 1484310196
title: chromium
categories:
    - Dictionary
---
chromium
     n : a hard brittle blue-white multivalent metallic element;
         resistant to corrosion and tarnishing [syn: {Cr}, {atomic
         number 24}]
