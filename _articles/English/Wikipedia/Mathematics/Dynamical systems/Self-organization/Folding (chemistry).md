---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Folding_(chemistry)
offline_file: ""
offline_thumbnail: ""
uuid: 447d9d7d-301f-4eca-9600-28fb836a7a56
updated: 1484308816
title: Folding (chemistry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Lehn_Beautiful_Foldamer_HelvChimActa_1598_2003.jpg
categories:
    - Self-organization
---
In chemistry, folding is the process by which a molecule assumes its shape or conformation. The process can also be described as intramolecular self-assembly, a type of molecular self-assembly, where the molecule is directed to form a specific shape through noncovalent interactions, such as hydrogen bonding, metal coordination, hydrophobic forces, van der Waals forces, pi-pi interactions, and/or electrostatic effects.
