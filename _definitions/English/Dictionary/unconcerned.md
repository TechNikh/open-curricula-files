---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unconcerned
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484454361
title: unconcerned
categories:
    - Dictionary
---
unconcerned
     adj 1: lacking in interest or care or feeling; "the average
            American...is unconcerned that his or her plight is
            the result of a complex of personal and economic and
            governmental actions...beyond the normal citizen's
            comprehension and control"; "blithely unconcerned
            about his friend's plight" [ant: {concerned}]
     2: easy in mind; not worried; "the prisoner seems entirely
        unconcerned as to the outcome of the examination"
     3: not occupied or engaged with; "readers unconcerned with
        style"
