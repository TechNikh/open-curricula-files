---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perfection
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484557561
title: perfection
categories:
    - Dictionary
---
perfection
     n 1: the state of being without a flaw or defect [syn: {flawlessness},
           {ne plus ultra}] [ant: {imperfection}]
     2: an ideal instance; a perfect embodiment of a concept [syn: {paragon},
         {idol}, {beau ideal}]
     3: the act of making something perfect
