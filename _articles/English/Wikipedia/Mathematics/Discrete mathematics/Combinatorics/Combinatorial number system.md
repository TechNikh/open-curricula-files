---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorial_number_system
offline_file: ""
offline_thumbnail: ""
uuid: cafd274f-c961-460d-83a1-35ac97c99e7a
updated: 1484308573
title: Combinatorial number system
tags:
    - Ordering combinations
    - Place of a combination in the ordering
    - Finding the k-combination for a given number
    - Example
    - National Lottery example in Excel
    - Applications
categories:
    - Combinatorics
---
In mathematics, and in particular in combinatorics, the combinatorial number system of degree k (for some positive integer k), also referred to as combinadics, is a correspondence between natural numbers (taken to include 0) N and k-combinations, represented as strictly decreasing sequences ck > ... > c2 > c1 ≥ 0. Since the latter are strings of numbers, one can view this as a kind of numeral system for representing N, although the main utility is representing a k-combination by N rather than the other way around. Distinct numbers correspond to distinct k-combinations, and produce them in lexicographic order; moreover the numbers less than 
  
    
      
        
          
     ...
