---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Three-fifths
offline_file: ""
offline_thumbnail: ""
uuid: 8c3bc6af-9cd7-43d4-9de1-e4c2217aa585
updated: 1484308456
title: Three-Fifths Compromise
tags:
    - Enactment
    - Impact before the Civil War
    - After the Civil War
    - Bibliography
categories:
    - Uncategorized
---
The Three-Fifths Compromise was a compromise reached between delegates from southern states and those from northern states during the 1787 United States Constitutional Convention. The debate was over whether, and if so, how, slaves would be counted when determining a state's total population for legislative representation and taxing purposes. The issue was important, as this population number would then be used to determine the number of seats that the state would have in the United States House of Representatives for the next ten years. The effect was to give the southern states a third more seats in Congress and a third more electoral votes than if slaves had been ignored, but fewer than ...
