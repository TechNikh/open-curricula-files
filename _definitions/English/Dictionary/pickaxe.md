---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pickaxe
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484582101
title: pickaxe
categories:
    - Dictionary
---
pickaxe
     n : a heavy iron tool with a wooden handle and a curved head
         that is pointed on both ends; "they used picks and
         sledges to break the rocks" [syn: {pick}, {pickax}]
