---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gravity_wave
offline_file: ""
offline_thumbnail: ""
uuid: 19e663ab-60be-47df-bc61-25f598376a8b
updated: 1484308883
title: Gravity wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Waves.jpg
tags:
    - Atmosphere dynamics on Earth
    - Quantitative description
    - Deep water
    - Shallow water
    - The generation of ocean waves by wind
    - Notes
categories:
    - Waves
---
In fluid dynamics, gravity waves are waves generated in a fluid medium or at the interface between two media when the force of gravity or buoyancy tries to restore equilibrium. An example of such an interface is that between the atmosphere and the ocean, which gives rise to wind waves.
