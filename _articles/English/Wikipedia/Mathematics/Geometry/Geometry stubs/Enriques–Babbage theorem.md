---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Enriques%E2%80%93Babbage_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 4d743b25-6ad9-41a1-a729-018efa01e6a4
updated: 1484309359
title: Enriques–Babbage theorem
categories:
    - Geometry stubs
---
In algebraic geometry, the Enriques–Babbage theorem states that a canonical curve is either a set-theoretic intersection of quadrics, or trigonal, or a plane quintic. It was proved by Babbage (1939) and Enriques (1919).
