---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alsace
offline_file: ""
offline_thumbnail: ""
uuid: 7129796c-5393-4ea4-bc49-ecd93bf8fe79
updated: 1484308473
title: Alsace
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Absolute_Petite_France_02.jpg
tags:
    - Etymology
    - Alsace-Lorraine
    - History
    - Pre-Roman Alsace
    - Roman Alsace
    - Alemannic and Frankish Alsace
    - Alsace within the Holy Roman Empire
    - Incorporation into France
    - French Revolution
    - Jews
categories:
    - Uncategorized
---
Alsace (/ælˈsæsˌælˈseɪsˌˈælsæsˌˈælseɪs/,[3] French: [al.zas] ( listen); Alsatian: ’s Elsass [ˈɛlsɑs]; German: Elsass[4] [ˈɛlzas] ( listen); Latin: Alsatia) is a cultural and historical region in eastern France, now located in the administrative region of Grand Est. Alsace is located on France's eastern border and on the west bank of the upper Rhine adjacent to Germany and Switzerland.
