---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invention
offline_file: ""
offline_thumbnail: ""
uuid: 549b7151-a18d-41c0-8392-89761af180e8
updated: 1484310146
title: invention
categories:
    - Dictionary
---
invention
     n 1: the creation of something in the mind [syn: {innovation}, {excogitation},
           {conception}, {design}]
     2: a creation (a new device or process) resulting from study
        and experimentation [syn: {innovation}]
     3: the act of inventing
