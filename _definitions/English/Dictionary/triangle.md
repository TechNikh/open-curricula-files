---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/triangle
offline_file: ""
offline_thumbnail: ""
uuid: 3f95c7d8-94c2-487a-a5dd-1d3a7744ce76
updated: 1484310218
title: triangle
categories:
    - Dictionary
---
triangle
     n 1: a three-sided polygon [syn: {trigon}, {trilateral}]
     2: something approximating the shape of a triangle; "the
        coastline of Chile and Argentina and Brazil forms two legs
        of a triangle"
     3: any of various triangular drafting instruments used to draw
        straight lines at specified angles
     4: a percussion instrument consisting of a metal bar bent in
        the shape of an open triangle
