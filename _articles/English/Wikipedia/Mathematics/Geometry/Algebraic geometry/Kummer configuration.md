---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kummer_configuration
offline_file: ""
offline_thumbnail: ""
uuid: 8058206c-e8d7-469a-a22f-428a4cd8d182
updated: 1484309160
title: Kummer configuration
categories:
    - Algebraic geometry
---
In geometry, the Kummer configuration, named for Ernst Kummer, is a geometric configuration of 16 points and 16 planes such that each point lies on 6 of the planes and each plane contains 6 of the points. The 16 nodes and 16 tropes of a Kummer surface form a Kummer configuration.
