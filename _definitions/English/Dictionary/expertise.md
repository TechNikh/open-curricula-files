---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expertise
offline_file: ""
offline_thumbnail: ""
uuid: b999378f-c5ca-4a38-b563-af0a21f10021
updated: 1484310313
title: expertise
categories:
    - Dictionary
---
expertise
     n : skillfulness by virtue of possessing special knowledge [syn:
          {expertness}]
