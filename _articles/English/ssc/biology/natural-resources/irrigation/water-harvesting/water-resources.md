---
title: Water resources
article_ref: https://en.wikipedia.org/wiki/Water_resources
tags:
    - Sources of fresh water
    - Surface water
    - Under river flow
    - Groundwater
    - Frozen water
    - Desalination
    - Water uses
    - Agricultural
    - Increasing water scarcity
    - Industrial
    - Domestic
    - Recreation
    - Environmental
    - Water stress
    - Population growth
    - Expansion of business activity
    - Rapid urbanization
    - Climate change
    - Depletion of aquifers
    - Pollution and water protection
    - Water and conflicts
    - Shared water resources can promote collaboration
    - Water shortages
    - Economic considerations
categories:
    - Water harvesting
---
