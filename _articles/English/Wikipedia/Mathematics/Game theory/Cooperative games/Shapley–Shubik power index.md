---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Shapley%E2%80%93Shubik_power_index'
offline_file: ""
offline_thumbnail: ""
uuid: 241d2da8-ecf2-45eb-a104-2bd3a3206fb7
updated: 1484308531
title: Shapley–Shubik power index
tags:
    - Examples
    - Applications
categories:
    - Cooperative games
---
The Shapley–Shubik power index was formulated by Lloyd Shapley and Martin Shubik in 1954 to measure the powers of players in a voting game.[1] The index often reveals surprising power distribution that is not obvious on the surface.
