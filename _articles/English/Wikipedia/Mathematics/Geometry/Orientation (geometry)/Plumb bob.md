---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plumb_bob
offline_file: ""
offline_thumbnail: ""
uuid: 1cb058a2-98b0-4122-9b83-1f9d2c0e8678
updated: 1484309297
title: Plumb bob
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Plumb_bob.jpg
tags:
    - Etymology
    - use
    - Determining center of gravity of an irregular shape
categories:
    - Orientation (geometry)
---
A plumb bob or a plummet is a weight, usually with a pointed tip on the bottom, that is suspended from a string and used as a vertical reference line, or plumb-line. It is essentially the vertical equivalent of a "water level".
