---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grothendieck_connection
offline_file: ""
offline_thumbnail: ""
uuid: 1577b0ca-8839-47d2-995a-1c0ad3580b59
updated: 1484309149
title: Grothendieck connection
categories:
    - Algebraic geometry
---
In algebraic geometry and synthetic differential geometry, a Grothendieck connection is a way of viewing connections in terms of descent data from infinitesimal neighbourhoods of the diagonal.
