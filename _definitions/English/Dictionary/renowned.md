---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/renowned
offline_file: ""
offline_thumbnail: ""
uuid: 1682f36c-d900-4223-8cb6-098aee346a02
updated: 1484310176
title: renowned
categories:
    - Dictionary
---
renowned
     adj : widely known and esteemed; "a famous actor"; "a celebrated
           musician"; "a famed scientist"; "an illustrious judge";
           "a notable historian"; "a renowned painter" [syn: {celebrated},
            {famed}, {far-famed}, {famous}, {illustrious}, {notable},
            {noted}]
