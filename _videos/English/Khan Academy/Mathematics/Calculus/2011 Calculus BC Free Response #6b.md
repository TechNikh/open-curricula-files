---
version: 1
type: video
provider: YouTube
id: NQazgcu8c7s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20BC%20Free%20Response%20%236b.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f67559ec-b5fa-4ba8-a2a7-b558680787f2
updated: 1486069690
title: '2011 Calculus BC Free Response #6b'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/NQazgcu8c7s/default.jpg
    - https://i3.ytimg.com/vi/NQazgcu8c7s/1.jpg
    - https://i3.ytimg.com/vi/NQazgcu8c7s/2.jpg
    - https://i3.ytimg.com/vi/NQazgcu8c7s/3.jpg
---
