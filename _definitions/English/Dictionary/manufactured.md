---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manufactured
offline_file: ""
offline_thumbnail: ""
uuid: 82ba5fd2-458a-4b8e-97e3-f4df2d82742b
updated: 1484310453
title: manufactured
categories:
    - Dictionary
---
manufactured
     adj : produced in a large-scale industrial operation
