---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invest
offline_file: ""
offline_thumbnail: ""
uuid: 44ac7b64-24b3-4b19-9714-40aac3ab5e27
updated: 1484310459
title: invest
categories:
    - Dictionary
---
invest
     v 1: make an investment; "Put money into bonds" [syn: {put}, {commit},
           {place}] [ant: {divest}]
     2: give qualities or abilities to [syn: {endow}, {indue}, {gift},
         {empower}, {endue}]
     3: furnish with power or authority; of kings or emperors [syn:
        {clothe}, {adorn}]
     4: provide with power and authority; "They vested the council
        with special rights" [syn: {vest}, {enthrone}] [ant: {divest}]
     5: place ceremoniously or formally in an office or position;
        "there was a ceremony to induct the president of the
        Academy" [syn: {induct}, {seat}]
