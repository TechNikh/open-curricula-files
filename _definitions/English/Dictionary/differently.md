---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/differently
offline_file: ""
offline_thumbnail: ""
uuid: 8bb22b84-88dd-470e-b7f4-f0306fc455e2
updated: 1484310279
title: differently
categories:
    - Dictionary
---
differently
     adv : in another and different manner; "very soon you will know
           differently"; "she thought otherwise" [syn: {otherwise}]
