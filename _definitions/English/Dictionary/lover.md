---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lover
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484555401
title: lover
categories:
    - Dictionary
---
lover
     n 1: a person who loves or is loved
     2: an ardent follower and admirer [syn: {fan}, {buff}, {devotee}]
     3: a significant other to whom you are not related by marriage
