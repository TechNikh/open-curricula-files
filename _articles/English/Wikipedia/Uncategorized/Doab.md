---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Doab
offline_file: ""
offline_thumbnail: ""
uuid: e2d3ef3d-b825-49c9-9d32-a6bd55618489
updated: 1484308419
title: Doab
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Khadir-and-bangar.jpg
tags:
    - Doab
    - United Provinces Doab
    - Upper Doab
    - Central or Middle Doab
    - Lower Doab
    - The Punjab Doabs
    - Sind Sagar Doab
    - Jech Doabs
    - Rechna Doabs
    - Bari Doabs
    - Bist Doab
    - Other Doabs
    - Malwa Doab
    - Raichur Doab
    - Notes
categories:
    - Uncategorized
---
Doab (Urdu: دوآب, Hindi: दोआब,[1] from Persian: دوآب dōāb, from dō, "two" + āb, "water" or "river") is a term used in India and Pakistan for the "tongue,"[2] or tract of land lying between two converging, or confluent, rivers. It is similar to an interfluve.[3] In the Oxford Hindi-English Dictionary, R. S. McGregor defines it as "a region lying between and reaching to the confluence of two rivers (esp. that between the Ganges and Jumna)."[1]
