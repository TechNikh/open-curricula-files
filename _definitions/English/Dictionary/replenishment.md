---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/replenishment
offline_file: ""
offline_thumbnail: ""
uuid: 052949b1-beb1-4320-a586-cf35dfc79eee
updated: 1484310238
title: replenishment
categories:
    - Dictionary
---
replenishment
     n : filling again by supplying what has been used up [syn: {refilling},
          {replacement}, {renewal}]
