---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/default
offline_file: ""
offline_thumbnail: ""
uuid: 9ae52262-11ee-4918-9f5b-356951dfa1b2
updated: 1484310183
title: default
categories:
    - Dictionary
---
default
     n 1: loss due to not showing up; "he lost the game by default"
     2: act of failing to meet a financial obligation [syn: {nonpayment},
         {nonremittal}]
     3: loss resulting from failure of a debt to be paid [syn: {nonpayment},
         {nonremittal}] [ant: {payment}]
     4: an option that is selected automatically unless an
        alternative is specified [syn: {default option}]
     v : fail to pay up [syn: {default on}] [ant: {pay up}]
