---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Technical_drawing_tool
offline_file: ""
offline_thumbnail: ""
uuid: 1f41646c-e009-4839-be0b-48a356ad98dc
updated: 1484309336
title: Technical drawing tool
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-CSS_Texas_plan.jpg
tags:
    - History
    - Drawing tools
    - Pens
    - Drawing board
    - T-square
    - Drafting machine
    - French Curves
    - Rulers
    - Compass
    - Templates
    - Perspective machines
    - Drawing materials
    - Drafting paper
    - Thick draft paper
    - Cloth
    - Tracing paper
    - Tracing tube
    - Inks
    - Dry transfer
    - Reproduction
categories:
    - Technical drawing
---
Technical drawing tools are the tools used for technical drawing, including, and not limited to: pens, rulers, compasses, protractors, and drawing utilities. Drawing tools may be used for measurement and layout of drawings, or to improve the consistency and speed of creation of standard drawing elements. The tools used for manual technical drawing have been displaced in use by the advent of the personal computer and its common utilization as the main tool in computer-aided drawing, draughting and design, CADD.
