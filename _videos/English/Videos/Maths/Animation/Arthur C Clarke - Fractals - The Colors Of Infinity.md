---
version: 1
type: video
provider: YouTube
id: Lk6QU94xAb8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 14309645-e7ee-4214-b76d-9cc2aee16f44
updated: 1486069644
title: 'Arthur C Clarke - Fractals - The Colors Of Infinity'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Lk6QU94xAb8/default.jpg
    - https://i3.ytimg.com/vi/Lk6QU94xAb8/1.jpg
    - https://i3.ytimg.com/vi/Lk6QU94xAb8/2.jpg
    - https://i3.ytimg.com/vi/Lk6QU94xAb8/3.jpg
---
