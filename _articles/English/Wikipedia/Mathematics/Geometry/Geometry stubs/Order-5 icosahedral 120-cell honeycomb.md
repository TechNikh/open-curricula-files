---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Order-5_icosahedral_120-cell_honeycomb
offline_file: ""
offline_thumbnail: ""
uuid: 17efd614-6f56-4bad-8752-20af53eb7eea
updated: 1484309369
title: Order-5 icosahedral 120-cell honeycomb
categories:
    - Geometry stubs
---
In the geometry of hyperbolic 4-space, the order-5 icosahedral 120-cell honeycomb is one of four regular star-honeycombs. With Schläfli symbol {3,5,5/2,5}, it has five icosahedral 120-cells around each face. It is dual to the great 120-cell honeycomb.
