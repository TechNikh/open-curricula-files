---
version: 1
type: video
provider: YouTube
id: xd4-Uy2FLWc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/The%20%28truly%29%20Periodic%20Table.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 00aad407-fa1d-44d4-be12-23c7462c2fb4
updated: 1486069664
title: The (truly) Periodic Table
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/xd4-Uy2FLWc/default.jpg
    - https://i3.ytimg.com/vi/xd4-Uy2FLWc/1.jpg
    - https://i3.ytimg.com/vi/xd4-Uy2FLWc/2.jpg
    - https://i3.ytimg.com/vi/xd4-Uy2FLWc/3.jpg
---
