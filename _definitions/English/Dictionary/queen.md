---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/queen
offline_file: ""
offline_thumbnail: ""
uuid: 612450e0-c992-426e-8036-87e08c6893cf
updated: 1484310154
title: queen
categories:
    - Dictionary
---
queen
     n 1: the only fertile female in a colony of social insects such
          as bees and ants and termites; its function is to lay
          eggs
     2: a female sovereign ruler [syn: {queen regnant}, {female
        monarch}] [ant: {king}, {king}]
     3: the wife or widow of a king
     4: something personified as a woman who is considered the best
        or most important of her kind; "Paris is the queen of
        cities"; "the queen of ocean liners"
     5: a competitor who holds a preeminent position [syn: {king}, {world-beater}]
     6: offensive terms for an openly homosexual man [syn: {fagot},
        {faggot}, {fag}, {fairy}, {nance}, {pansy}, {queer}, {poof},
         ...
