---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Markushevich_basis
offline_file: ""
offline_thumbnail: ""
uuid: 06dc2362-abf6-4641-ad73-3f1ac415a155
updated: 1484309365
title: Markushevich basis
categories:
    - Geometry stubs
---
In geometry, a Markushevich basis (sometimes Markushevich bases[1] or M-basis[2]) is a biorthogonal system that is both complete and total.[3] It can be described by the formulation:
