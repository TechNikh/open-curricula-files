---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mobilise
offline_file: ""
offline_thumbnail: ""
uuid: aa55578c-1b07-4d53-ab3b-88f737601594
updated: 1484310571
title: mobilise
categories:
    - Dictionary
---
mobilise
     v 1: call to arms; of military personnel [syn: {call up}, {mobilize},
           {rally}] [ant: {demobilize}]
     2: get ready for war [syn: {mobilize}] [ant: {demobilize}, {demobilize}]
     3: make ready for action or use; "marshal resources" [syn: {mobilize},
         {marshal}, {summon}]
     4: cause to move around; "circulate a rumor" [syn: {mobilize},
        {circulate}]
