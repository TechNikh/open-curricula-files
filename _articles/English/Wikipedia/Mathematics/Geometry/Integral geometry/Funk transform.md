---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Funk_transform
offline_file: ""
offline_thumbnail: ""
uuid: f0f1e10e-2722-4645-93a0-432cab47a0c6
updated: 1484309257
title: Funk transform
tags:
    - Definition
    - Inversion
    - Spherical harmonics
    - "Helgason's inversion formula"
    - Generalization
    - Applications
categories:
    - Integral geometry
---
In the mathematical field of integral geometry, the Funk transform (also known as Minkowski–Funk transform, Funk–Radon transform or spherical Radon transform) is an integral transform defined by integrating a function on great circles of the sphere. It was introduced by Paul Funk in 1911, based on the work of Minkowski (1904). It is closely related to the Radon transform. The original motivation for studying the Funk transform was to describe Zoll metrics on the sphere.
