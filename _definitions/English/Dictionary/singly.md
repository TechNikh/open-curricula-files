---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/singly
offline_file: ""
offline_thumbnail: ""
uuid: 112303ff-52ff-44da-8bb3-539e28cb56b8
updated: 1484310393
title: singly
categories:
    - Dictionary
---
singly
     adv 1: one by one; one at a time; "they were arranged singly" [ant:
             {multiply}]
     2: apart from others; "taken individually, the rooms were, in
        fact, square"; "the fine points are treated singly" [syn:
        {individually}, {separately}, {severally}, {one by one}, {on
        an individual basis}]
