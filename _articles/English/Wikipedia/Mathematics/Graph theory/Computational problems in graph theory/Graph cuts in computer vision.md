---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_cuts_in_computer_vision
offline_file: ""
offline_thumbnail: ""
uuid: e9ec6506-17fd-4ab9-834d-958936a5b66e
updated: 1484309389
title: Graph cuts in computer vision
tags:
    - History
    - Binary Segmentation of Images
    - Notation
    - Existing methods
    - Energy function
    - Likelihood / Color model / Regional term
    - Histogram
    - GMM (Gaussian Mixture Model)
    - Texon
    - Prior / Coherence model / Boundary term
    - Criticism
    - Algorithm
    - Implementation (exact)
    - Implementation (approximation)
    - Software
categories:
    - Computational problems in graph theory
---
As applied in the field of computer vision, graph cuts can be employed to efficiently solve a wide variety of low-level computer vision problems (early vision[1]), such as image smoothing, the stereo correspondence problem, image segmentation, and many other computer vision problems that can be formulated in terms of energy minimization. Such energy minimization problems can be reduced to instances of the maximum flow problem in a graph (and thus, by the max-flow min-cut theorem, define a minimal cut of the graph). Under most formulations of such problems in computer vision, the minimum energy solution corresponds to the maximum a posteriori estimate of a solution. Although many computer ...
