---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paramecium
offline_file: ""
offline_thumbnail: ""
uuid: d6cea1b8-2cc0-4cf2-b4cc-2f806c63501b
updated: 1484310158
title: paramecium
categories:
    - Dictionary
---
paramecium
     n : any member of the genus Paramecium [syn: {paramecia}]
     [also: {paramecia} (pl)]
