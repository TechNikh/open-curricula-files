---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/profile
offline_file: ""
offline_thumbnail: ""
uuid: c5d85348-6ef7-4601-9410-f3db3b022fc4
updated: 1484310144
title: profile
categories:
    - Dictionary
---
profile
     n 1: an analysis (often in graphical form) representing the
          extent to which something exhibits various
          characteristics; "a biochemical profile of blood"; "a
          psychological profile of serial killers"
     2: a side view representation of an object (especially a human
        face)
     3: biographical sketch
     4: degree of exposure to public notice; "that candidate does
        not have sufficient visibility to win an election"; "he
        prefers a low profile" [syn: {visibility}]
     5: a vertical section of the Earth's crust showing the
        different horizons or layers
     v 1: write about; "The author of this article profiles a famous
  ...
