---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dealing
offline_file: ""
offline_thumbnail: ""
uuid: 73afef11-f902-4085-8f48-fd0e51366648
updated: 1484310443
title: dealing
categories:
    - Dictionary
---
dealing
     n 1: method or manner of conduct in relation to others; "honest
          dealing"
     2: the act of transacting within or between groups (as carrying
        on commercial activities); "no transactions are possible
        without him"; "he has always been honest is his dealings
        with me" [syn: {transaction}, {dealings}]
