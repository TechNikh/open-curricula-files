---
version: 1
type: video
provider: YouTube
id: J3HVVi2k2No
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Animation-%20The%20Central%20Dogma.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e240e604-3ec4-4c56-b773-b0396f6aef60
updated: 1486069673
title: 'Animation- The Central Dogma'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/J3HVVi2k2No/default.jpg
    - https://i3.ytimg.com/vi/J3HVVi2k2No/1.jpg
    - https://i3.ytimg.com/vi/J3HVVi2k2No/2.jpg
    - https://i3.ytimg.com/vi/J3HVVi2k2No/3.jpg
---
