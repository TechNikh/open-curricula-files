---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adolf
offline_file: ""
offline_thumbnail: ""
uuid: c108e2b9-4032-44a9-a4b9-e9e0b49d6a3c
updated: 1484308479
title: Adolf
tags:
    - Stigmatization
    - Monarchs and noblemen
    - Saints
    - >
        People with the given name Adolf or Adolph(e) or Adolphus or
        any other form
    - People with the surname Adolphus
    - Fictional characters
categories:
    - Uncategorized
---
Adolf, also spelled Adolph and sometimes Latinised to Adolphus, is a given name used in German-speaking countries, in Scandinavia, in the Netherlands and Flanders and to a lesser extent in various Central European countries. Adolphus can also appear as a surname, as in John Adolphus, the English historian.
