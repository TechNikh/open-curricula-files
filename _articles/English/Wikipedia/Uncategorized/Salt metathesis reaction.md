---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Double-displacement
offline_file: ""
offline_thumbnail: ""
uuid: 731daab4-529c-462c-a9b6-2215c1e07b49
updated: 1484308312
title: Salt metathesis reaction
tags:
    - Types of reactions
    - Counter-ion exchange
    - Alkylation
    - Neutralization
    - Aqueous metathesis (precipitation)
    - Aqueous metathesis (double decomposition)
    - Acid and carbonates
categories:
    - Uncategorized
---
A salt metathesis reaction (from the Greek μετάθεσις, "transposition"), sometimes called a double replacement reaction or double displacement reaction, is a chemical process involving the exchange of bonds between two reacting chemical species, which results in the creation of products with similar or identical bonding affiliations.[1] This reaction is represented by the general scheme:
