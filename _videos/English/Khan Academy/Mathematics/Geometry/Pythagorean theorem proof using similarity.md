---
version: 1
type: video
provider: YouTube
id: LrS5_l-gk94
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Pythagorean%20theorem%20proof%20using%20similarity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bc59cf3b-1eae-4eea-b614-0eadeb25c1e8
updated: 1486069575
title: Pythagorean theorem proof using similarity
categories:
    - Geometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/LrS5_l-gk94/default.jpg
    - https://i3.ytimg.com/vi/LrS5_l-gk94/1.jpg
    - https://i3.ytimg.com/vi/LrS5_l-gk94/2.jpg
    - https://i3.ytimg.com/vi/LrS5_l-gk94/3.jpg
---
Proof of the Pythagorean Theorem using similarity

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/geometry/right_triangles_topic/pythagorean_proofs/e/pythagorean-theorem-proofs?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Watch the next lesson: https://www.khanacademy.org/math/geometry/right_triangles_topic/pythagorean_proofs/v/another-pythagorean-theorem-proof?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Missed the previous lesson? 
https://www.khanacademy.org/math/geometry/right_triangles_topic/pythagorean_proofs/v/bhaskara-s-proof-of-pythagorean-theorem-avi?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Geometry on Khan Academy: We are surrounded by space. And that space contains lots of things. And these things have shapes. In geometry we are concerned with the nature of these shapes, how we define them, and what they teach us about the world at large--from math to architecture to biology to astronomy (and everything in between). Learning geometry is about more than just taking your medicine ("It's good for you!"), it's at the core of everything that exists--including you. Having said all that, some of the specific topics we'll cover include angles, intersecting lines, right triangles, perimeter, area, volume, circles, triangles, quadrilaterals, analytic geometry, and geometric constructions. Wow. That's a lot. To summarize: it's difficult to imagine any area of math that is more widely used than geometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Geometry channel:
https://www.youtube.com/channel/UCD3OtKxPRUFw8kzYlhJXa1Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
