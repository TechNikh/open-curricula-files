---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Remote_monitoring_and_control
offline_file: ""
offline_thumbnail: ""
uuid: c7f939ef-0ed8-45ac-a032-0ee48a5db2dc
updated: 1484308742
title: Remote monitoring and control
categories:
    - Control engineering
---
Remote monitoring and control (M&C) systems are designed to control large or complex facilities such as factories, power plants, network operations centers, airports, and spacecraft, with some degree of automation.
