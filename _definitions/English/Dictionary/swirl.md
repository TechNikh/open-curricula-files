---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swirl
offline_file: ""
offline_thumbnail: ""
uuid: c4800395-cc9b-43d9-a2d8-ecca2e1ca8ec
updated: 1484310383
title: swirl
categories:
    - Dictionary
---
swirl
     n : the shape of something rotating rapidly [syn: {whirl}, {vortex},
          {convolution}]
     v 1: turn in a twisting or spinning motion; "The leaves swirled
          in the autumn wind" [syn: {twirl}, {twiddle}, {whirl}]
     2: flow in a circular current, of liquids [syn: {eddy}, {purl},
         {whirlpool}, {whirl}]
