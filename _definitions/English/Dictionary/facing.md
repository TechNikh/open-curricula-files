---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/facing
offline_file: ""
offline_thumbnail: ""
uuid: 6c6363a7-6ab6-4c63-85b7-c0df826913dd
updated: 1484310216
title: facing
categories:
    - Dictionary
---
facing
     n 1: a lining applied to the edge of a garment for ornamentation
          or strengthening
     2: an ornamental coating to a building [syn: {veneer}]
     3: a protective covering that protects the outside of a
        building [syn: {cladding}]
     4: providing something with a surface of a different material
        [syn: {lining}]
     5: the act of confronting bravely; "he hated facing the facts";
        "he excelled in the face of danger" [syn: {face}]
