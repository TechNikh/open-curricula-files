---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graphon
offline_file: ""
offline_thumbnail: ""
uuid: 7b229296-a8e2-460c-9c37-b2f2e9e59a16
updated: 1484308625
title: Graphon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Exchangeable_random_graph_from_graphon.png
tags:
    - Definition
    - Examples
    - Jointly exchangeable adjacency matrices
    - Limits of sequences of dense graphs
    - Related notions
categories:
    - Graph theory
---
In graph theory and statistics, a graphon is a symmetric measurable function 
  
    
      
        W
        :
        [
        0
        ,
        1
        
          ]
          
            2
          
        
        →
        [
        0
        ,
        1
        ]
      
    
    {\displaystyle W:[0,1]^{2}\to [0,1]}
  
, that is important in the study of dense graphs. Graphons arise as the fundamental objects in two areas: as the defining objects of exchangeable random graph models and as a natural notion of limit for sequences of dense graphs. Graphons are tied to dense graphs by the following pair of observations: the random graph models defined by graphons give rise to ...
