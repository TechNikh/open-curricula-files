---
version: 1
type: video
provider: YouTube
id: zDkUFzZoQAs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Method%20of%20the%20Year%202011-%20Gene-editing%20nucleases%20-%20by%20Nature%20Video.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 685eef3b-56a6-47b5-9575-71ed8c2eda92
updated: 1486069672
title: 'Method of the Year 2011- Gene-editing nucleases - by Nature Video'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/zDkUFzZoQAs/default.jpg
    - https://i3.ytimg.com/vi/zDkUFzZoQAs/1.jpg
    - https://i3.ytimg.com/vi/zDkUFzZoQAs/2.jpg
    - https://i3.ytimg.com/vi/zDkUFzZoQAs/3.jpg
---
