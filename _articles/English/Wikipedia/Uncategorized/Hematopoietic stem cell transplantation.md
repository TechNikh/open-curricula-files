---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Marrow_transplant
offline_file: ""
offline_thumbnail: ""
uuid: c1080bdd-ce22-4006-a850-306319449084
updated: 1484308371
title: Hematopoietic stem cell transplantation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-KM_Transplantat.JPEG
tags:
    - Medical uses
    - Indications
    - Malignant
    - Non-malignant
    - Number of procedures
    - Graft types
    - Autologous
    - Allogeneic
    - Sources and storage of cells
    - Bone marrow
    - Peripheral blood stem cells
    - Amniotic fluid
    - Umbilical cord blood
    - Storage of HSC
    - Conditioning regimens
    - Myeloablative
    - Non-myeloablative
    - Engraftment
    - Complications
    - Infection
    - Veno-occlusive disease
    - Mucositis
    - Graft-versus-host disease
    - Graft-versus-tumor effect
    - Oral carcinoma
    - Prognosis
    - Risks to donor
    - Drug risks
    - Access risks
    - Clinical observations
    - Severe reactions
    - History
    - Donor registration and recruitment
    - Research
    - HIV
    - Multiple sclerosis
categories:
    - Uncategorized
---
Hematopoietic stem cell transplantation (HSCT) is the transplantation of multipotent hematopoietic stem cells, usually derived from bone marrow, peripheral blood, or umbilical cord blood.[1][2] It may be autologous (the patient's own stem cells are used), allogeneic (the stem cells come from a donor) or syngeneic (from an identical twin).[1][2] It is a medical procedure in the field of hematology, most often performed for patients with certain cancers of the blood or bone marrow, such as multiple myeloma or leukemia.[2] In these cases, the recipient's immune system is usually destroyed with radiation or chemotherapy before the transplantation. Infection and graft-versus-host disease are ...
