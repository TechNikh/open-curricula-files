---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/desperately
offline_file: ""
offline_thumbnail: ""
uuid: 3aaa5ad4-87ae-4b5e-b809-1194120687d7
updated: 1484310563
title: desperately
categories:
    - Dictionary
---
desperately
     adv 1: with great urgency; "health care reform is needed urgently";
            "the soil desperately needed potash" [syn: {urgently}]
     2: in intense despair; "the child clung desperately to her
        mother"
