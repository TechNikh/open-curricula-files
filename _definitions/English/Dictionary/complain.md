---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/complain
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484443501
title: complain
categories:
    - Dictionary
---
complain
     v 1: express complaints, discontent, displeasure, or unhappiness;
          "My mother complains all day"; "She has a lot to kick
          about" [syn: {kick}, {plain}, {sound off}, {quetch}, {kvetch}]
          [ant: {cheer}]
     2: make a formal accusation; bring a formal charge; "The
        plaintiff's lawyer complained that he defendant had
        physically abused his client"
