---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vowel
offline_file: ""
offline_thumbnail: ""
uuid: 5dc0dba3-26a8-4b71-aa59-387706688b10
updated: 1484310140
title: vowel
categories:
    - Dictionary
---
vowel
     n 1: a speech sound made with the vocal tract open [syn: {vowel
          sound}] [ant: {consonant}]
     2: a letter of the alphabet standing for a spoken vowel
