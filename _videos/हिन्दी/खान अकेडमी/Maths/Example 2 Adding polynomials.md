---
version: 1
type: video
provider: YouTube
id: VjhUWXmLkKM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ab4093a0-9070-48ea-b435-0b056ebc0677
updated: 1486069617
title: Example 2 Adding polynomials
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/VjhUWXmLkKM/default.jpg
    - https://i3.ytimg.com/vi/VjhUWXmLkKM/1.jpg
    - https://i3.ytimg.com/vi/VjhUWXmLkKM/2.jpg
    - https://i3.ytimg.com/vi/VjhUWXmLkKM/3.jpg
---
