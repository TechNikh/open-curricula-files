---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dobereiner
offline_file: ""
offline_thumbnail: ""
uuid: cd7af25d-35ca-4a0e-9d44-2e019cc05905
updated: 1484308380
title: Johann Wolfgang Döbereiner
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/150px-D%25C3%25B6bereiner_fire_gadget.png'
tags:
    - Life and work
    - Works
categories:
    - Uncategorized
---
Johann Wolfgang Döbereiner (13 December 1780 – 24 March 1849) was a German chemist who is best known for work that foreshadowed the periodic law for the chemical elements. He become a professor of chemistry and pharmacy at the university of Jena
