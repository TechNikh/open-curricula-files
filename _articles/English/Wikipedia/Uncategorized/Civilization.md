---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Civilization
offline_file: ""
offline_thumbnail: ""
uuid: ea40a82d-5a97-4815-88b3-64bd1e3582e8
updated: 1484308323
title: Civilization
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Egypt.Giza.Sphinx.02.jpg
tags:
    - History of the concept
    - characteristics
    - Cultural identity
    - Complex systems
    - History
    - Development of theories on the origins of civilization
    - Early civilizations
    - The Neolithic Era and the transition to Civilization
    - The Bronze Age
    - The Iron Age
    - Medieval to Early Modern
    - Modernity
    - Fall of civilizations
    - future
    - Notes and references
    - Bibliography
categories:
    - Uncategorized
---
A civilization (or civilisation, see spelling differences) is any complex society characterized by urban development, social stratification, symbolic communication forms (typically, writing systems), and a perceived separation from and domination over the natural environment by a cultural elite.[1][2][3][4][5][6][7][8] Civilizations are intimately associated with and often further defined by other socio-politico-economic characteristics, including centralization, the domestication of both humans and other organisms, specialization of labour, culturally ingrained ideologies of progress and supremacism, monumental architecture, taxation, societal dependence upon farming as an agricultural ...
