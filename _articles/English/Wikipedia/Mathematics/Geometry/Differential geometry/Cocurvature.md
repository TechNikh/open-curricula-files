---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cocurvature
offline_file: ""
offline_thumbnail: ""
uuid: 07d362fe-41a4-4c01-aa88-37088d85629d
updated: 1484309209
title: Cocurvature
categories:
    - Differential geometry
---
In mathematics in the branch of differential geometry, the cocurvature of a connection on a manifold is the obstruction to the integrability of the vertical bundle.
