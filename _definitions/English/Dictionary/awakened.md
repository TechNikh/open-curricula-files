---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/awakened
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484477941
title: awakened
categories:
    - Dictionary
---
awakened
     adj 1: aroused or activated; "an awakened interest in ballet" [ant:
             {unawakened}]
     2: (somewhat formal) having been waked up; "the awakened baby
        began to cry"
