---
version: 1
type: video
provider: YouTube
id: HJzWWwuycj8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bbc03afd-5852-43f2-b75c-61fbf1f46136
updated: 1486069618
title: Impact of a radius change on the area of a circle
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/HJzWWwuycj8/default.jpg
    - https://i3.ytimg.com/vi/HJzWWwuycj8/1.jpg
    - https://i3.ytimg.com/vi/HJzWWwuycj8/2.jpg
    - https://i3.ytimg.com/vi/HJzWWwuycj8/3.jpg
---
