---
version: 1
type: video
provider: YouTube
id: kfy92hdaAH0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Active%20and%20Passive%20Transport.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8e8704ce-55db-4b99-abb8-c820e41efbca
updated: 1486069669
title: Active and Passive Transport
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/kfy92hdaAH0/default.jpg
    - https://i3.ytimg.com/vi/kfy92hdaAH0/1.jpg
    - https://i3.ytimg.com/vi/kfy92hdaAH0/2.jpg
    - https://i3.ytimg.com/vi/kfy92hdaAH0/3.jpg
---
