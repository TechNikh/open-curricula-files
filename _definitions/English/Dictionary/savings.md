---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/savings
offline_file: ""
offline_thumbnail: ""
uuid: e4b6e264-bd37-4c2d-964e-3e79f25706a2
updated: 1484310453
title: savings
categories:
    - Dictionary
---
savings
     n : a fund of money put by as a reserve [syn: {nest egg}]
