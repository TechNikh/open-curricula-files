---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Incidence_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 5ba9b204-c0a1-4b66-b846-0a591bc4b2eb
updated: 1484308573
title: Incidence matrix
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Labeled_undirected_graph.svg.png
tags:
    - Graph theory
    - Undirected and directed graphs
    - Signed and bidirected graphs
    - Multigraphs
    - Hypergraphs
    - Incidence structures
    - Finite geometries
    - Block designs
categories:
    - Combinatorics
---
In mathematics, an incidence matrix is a matrix that shows the relationship between two classes of objects. If the first class is X and the second is Y, the matrix has one row for each element of X and one column for each element of Y. The entry in row x and column y is 1 if x and y are related (called incident in this context) and 0 if they are not. There are variations; see below.
