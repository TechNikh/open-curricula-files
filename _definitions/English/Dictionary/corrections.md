---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corrections
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484473441
title: corrections
categories:
    - Dictionary
---
corrections
     n 1: the department of local government that is responsible for
          managing the treatment of convicted offenders; "for a
          career in corrections turn to the web site of the New
          Jersey Department of Corrections" [syn: {department of
          corrections}]
     2: the social control of offenders through a system of
        imprisonment and rehabilitation and probation and parole
