---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gall_bladder
offline_file: ""
offline_thumbnail: ""
uuid: 249437c9-895d-4db2-a624-7c4c8fb2acd7
updated: 1484308372
title: Gallbladder
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gallbladder_%2528organ%2529.png'
tags:
    - Structure
    - Histology
    - Development
    - Variation
    - Function
    - Clinical significance
    - Gallstones
    - Inflammation
    - Cholesterolosis
    - Gallbladder polyps
    - Gallbladder removal
    - Imaging
    - Society and culture
    - Other animals
categories:
    - Uncategorized
---
In vertebrates the gallbladder (also gall bladder, biliary vesicle or cholecyst) is a small organ where bile (a fluid produced by the liver) is stored and concentrated before it is released into the small intestine. Humans can live without a gallbladder. The surgical removal of the gallbladder is called a cholecystectomy.
