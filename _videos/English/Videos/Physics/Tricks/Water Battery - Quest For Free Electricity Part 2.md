---
version: 1
type: video
provider: YouTube
id: sw2bsqh86nA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e1576150-d5c6-475e-8545-c9e20737961d
updated: 1486069657
title: 'Water Battery - Quest For Free Electricity Part 2'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/sw2bsqh86nA/default.jpg
    - https://i3.ytimg.com/vi/sw2bsqh86nA/1.jpg
    - https://i3.ytimg.com/vi/sw2bsqh86nA/2.jpg
    - https://i3.ytimg.com/vi/sw2bsqh86nA/3.jpg
---
