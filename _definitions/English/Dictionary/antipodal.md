---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/antipodal
offline_file: ""
offline_thumbnail: ""
uuid: da6edc7e-19b0-4d11-a23c-9a901492dca0
updated: 1484310158
title: antipodal
categories:
    - Dictionary
---
antipodal
     adj : relating to the antipodes or situated at opposite sides of
           the earth; "antodpodean latitudes"; "antipodal regions
           of the earth"; "antipodal points on a sphere" [syn: {antipodean}]
     n : the relation of opposition along a diameter [syn: {antipodal
         opposition}, {diametrical opposition}]
