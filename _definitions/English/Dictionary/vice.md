---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vice
offline_file: ""
offline_thumbnail: ""
uuid: 0d66f698-3fa1-43e2-b499-f9ef3454200c
updated: 1484310214
title: vice
categories:
    - Dictionary
---
vice
     n 1: moral weakness [syn: {frailty}]
     2: a specific form of evildoing; "vice offends the moral
        standards of the community"
