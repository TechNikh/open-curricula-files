---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weeds
offline_file: ""
offline_thumbnail: ""
uuid: c45aafa7-f718-4597-bf1b-50504f5d376f
updated: 1484310162
title: weeds
categories:
    - Dictionary
---
weeds
     n 1: a black garment (dress) worn by a widow as a sign of
          mourning [syn: {widow's weeds}]
     2: a black band worn by a man (on the arm or hat) as a sign of
        mourning [syn: {mourning band}]
