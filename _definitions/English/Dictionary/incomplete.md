---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incomplete
offline_file: ""
offline_thumbnail: ""
uuid: ae86dca4-534f-4af9-8e74-2c9b4f3903d4
updated: 1484310401
title: incomplete
categories:
    - Dictionary
---
incomplete
     adj 1: not complete or total; not completed; "an incomplete account
            of his life"; "political consequences of incomplete
            military success"; "an incomplete forward pass" [syn:
            {uncomplete}] [ant: {complete}]
     2: lacking one or more of the four whorls of the complete
        flower--sepals or petals or stamens or pistils; "an
        incomplete flower" [ant: {complete}, {complete}]
     3: not yet finished; "his thesis is still incomplete"; "an
        uncompleted play" [syn: {uncompleted}]
