---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/facilitated
offline_file: ""
offline_thumbnail: ""
uuid: a73c7e47-4efa-4d8d-9adc-38f6aa78c1e6
updated: 1484310529
title: facilitated
categories:
    - Dictionary
---
facilitated
     adj : freed from difficulty or impediment
