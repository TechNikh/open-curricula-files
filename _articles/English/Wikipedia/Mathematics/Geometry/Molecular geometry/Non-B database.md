---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-B_database
offline_file: ""
offline_thumbnail: ""
uuid: 4a3a5e4f-929c-4575-9a4a-615756b54195
updated: 1484309279
title: Non-B database
categories:
    - Molecular geometry
---
Non-B DB is a database integrating annotations and analysis of non-B DNA-forming sequence motifs.[1] The database provides alternative DNA structure predictions including Z-DNA motifs, quadruplex-forming motifs, inverted repeats, mirror repeats and direct repeats and their associated subsets of cruciforms, triplex and slipped structures, respectively.[2]
