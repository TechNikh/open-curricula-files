---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enfranchisement
offline_file: ""
offline_thumbnail: ""
uuid: 1a1e1433-80ec-4b1a-83b5-fee99b570c78
updated: 1484310557
title: enfranchisement
categories:
    - Dictionary
---
enfranchisement
     n 1: freedom from political subjugation or servitude
     2: a statutory right or privilege granted to a person or group
        by a government (especially the rights of citizenship and
        the right to vote) [syn: {franchise}]
     3: the act of certifying or bestowing a franchise on [syn: {certification}]
        [ant: {disenfranchisement}]
