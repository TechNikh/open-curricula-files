---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/divergence
offline_file: ""
offline_thumbnail: ""
uuid: 5cb9ec31-6fda-473b-ba6e-597cee535726
updated: 1484310216
title: divergence
categories:
    - Dictionary
---
divergence
     n 1: the act of moving away in different direction from a common
          point; "an angle is formed by the divergence of two
          straight lines" [syn: {divergency}]
     2: a variation that deviates from the standard or norm; "the
        deviation from the mean" [syn: {deviation}, {departure}, {difference}]
     3: an infinite series that has no limit [syn: {divergency}]
        [ant: {convergence}, {convergence}]
     4: a difference between conflicting facts or claims or
        opinions; "a growing divergence of opinion" [syn: {discrepancy},
         {disagreement}, {variance}]
