---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aperture_card
offline_file: ""
offline_thumbnail: ""
uuid: 308734d1-348b-4df0-afa1-961e7fe6d1cc
updated: 1484309322
title: Aperture card
tags:
    - Usage
    - Advantages
    - Disadvantages
    - Machinery
    - Conversion
categories:
    - Technical drawing
---
An aperture card is a type of punched card with a cut-out window into which a chip of microfilm is mounted. Such a card is used for archiving or for making multiple inexpensive copies of a document for ease of distribution. The card is typically punched with machine-readable metadata associated with the microfilm image, and printed across the top of the card for visual identification. The microfilm chip is most commonly 35mm in height, and contains an optically reduced image, usually of some type of reference document, such as an engineering drawing, that is the focus of the archiving process. Aperture cards have several advantages and disadvantages when compared to digital systems. ...
