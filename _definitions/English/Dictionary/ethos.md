---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethos
offline_file: ""
offline_thumbnail: ""
uuid: acfc35d1-4d8b-4a86-900e-5ff30995055c
updated: 1484310607
title: ethos
categories:
    - Dictionary
---
ethos
     n : (anthropology) the distinctive spirit of a culture or an
         era; "the Greek ethos"
