---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Z-channel_(information_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 36f24c9d-caf4-42d1-9cdd-a0c1aed1e5dd
updated: 1484308563
title: Z-channel (information theory)
tags:
    - Definition
    - Capacity
    - Bounds on the size of an asymmetric-error-correcting code
categories:
    - Coding theory
---
