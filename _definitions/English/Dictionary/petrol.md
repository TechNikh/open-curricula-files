---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/petrol
offline_file: ""
offline_thumbnail: ""
uuid: c916672d-5914-421f-ad55-d76dc0ae59fd
updated: 1484310271
title: petrol
categories:
    - Dictionary
---
petrol
     n : a volatile flammable mixture of hydrocarbons (hexane and
         heptane and octane etc.) derived from petroleum; used
         mainly as a fuel in internal-combustion engines [syn: {gasoline},
          {gasolene}, {gas}]
