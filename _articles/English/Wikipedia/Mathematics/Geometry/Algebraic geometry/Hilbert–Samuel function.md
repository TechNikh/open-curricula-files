---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hilbert%E2%80%93Samuel_function'
offline_file: ""
offline_thumbnail: ""
uuid: 4b3d6b13-2caf-4c32-a5b2-d1ef9c47ed31
updated: 1484309153
title: Hilbert–Samuel function
tags:
    - Examples
    - Degree bounds
categories:
    - Algebraic geometry
---
In commutative algebra the Hilbert–Samuel function, named after David Hilbert and Pierre Samuel,[1] of a nonzero finitely generated module 
  
    
      
        M
      
    
    {\displaystyle M}
  
 over a commutative Noetherian local ring 
  
    
      
        A
      
    
    {\displaystyle A}
  
 and a primary ideal 
  
    
      
        I
      
    
    {\displaystyle I}
  
 of 
  
    
      
        A
      
    
    {\displaystyle A}
  
 is the map 
  
    
      
        
          χ
          
            M
          
          
            I
          
        
        :
        
          N
        
        →
        
          N
        
      
    
    ...
