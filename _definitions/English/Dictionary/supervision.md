---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supervision
offline_file: ""
offline_thumbnail: ""
uuid: ffc4f1a2-623b-470e-aca6-16f36d9ef28f
updated: 1484310583
title: supervision
categories:
    - Dictionary
---
supervision
     n : management by overseeing the performance or operation of a
         person or group [syn: {supervising}, {superintendence}, {oversight}]
