---
version: 1
type: video
provider: YouTube
id: MYcCLmVLbfY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6644c344-6706-438e-90f5-4381f8fcd1fe
updated: 1486069635
title: 'Optical Mineralogy - Simple Experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/MYcCLmVLbfY/default.jpg
    - https://i3.ytimg.com/vi/MYcCLmVLbfY/1.jpg
    - https://i3.ytimg.com/vi/MYcCLmVLbfY/2.jpg
    - https://i3.ytimg.com/vi/MYcCLmVLbfY/3.jpg
---
