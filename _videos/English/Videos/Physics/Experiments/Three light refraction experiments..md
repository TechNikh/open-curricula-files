---
version: 1
type: video
provider: YouTube
id: IpLaKHz6RxY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 07371fc0-d76b-4e7a-90da-72e3181bf2e0
updated: 1486069639
title: Three light refraction experiments.
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/IpLaKHz6RxY/default.jpg
    - https://i3.ytimg.com/vi/IpLaKHz6RxY/1.jpg
    - https://i3.ytimg.com/vi/IpLaKHz6RxY/2.jpg
    - https://i3.ytimg.com/vi/IpLaKHz6RxY/3.jpg
---
