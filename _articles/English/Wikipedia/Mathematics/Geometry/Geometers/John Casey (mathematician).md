---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_Casey_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 184a4306-8181-4a30-853e-25267addb23a
updated: 1484309075
title: John Casey (mathematician)
tags:
    - Biography
    - Honours and awards
    - Major works
    - Sources
categories:
    - Geometers
---
John Casey (12 May 1820, Kilbehenny, Ireland – 3 January 1891, Dublin) was a respected Irish geometer. He is most famous for Casey's theorem on a circle that is tangent to four other circles, an extension of the problem of Apollonius. However, he contributed several novel proofs and perspectives on Euclidean geometry. He and Émile Lemoine are considered to be the co-founders of the modern geometry of the circle and the triangle.
