---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/partial
offline_file: ""
offline_thumbnail: ""
uuid: 8faa55ce-c9d0-4dc2-82a5-755f7757badc
updated: 1484310405
title: partial
categories:
    - Dictionary
---
partial
     adj 1: being or affecting only a part; not total; "a partial
            description of the suspect"; "partial collapse"; "a
            partial eclipse"; "a partial monopoly"; "partial
            immunity"
     2: showing favoritism [syn: {unfair}] [ant: {impartial}]
     3: (followed by `of' or `to') having a strong preference or
        liking for; "fond of chocolate"; "partial to horror
        movies" [syn: {fond(p)}, {partial(p)}]
     n 1: the derivative of a function of two or more variables with
          respect to a single variable while the other variables
          are considered to be constant [syn: {partial derivative}]
     2: a harmonic with a frequency that ...
