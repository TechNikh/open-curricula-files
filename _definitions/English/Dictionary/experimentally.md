---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/experimentally
offline_file: ""
offline_thumbnail: ""
uuid: 21a12c06-3ae7-42ba-9b52-e8aabd123117
updated: 1484310224
title: experimentally
categories:
    - Dictionary
---
experimentally
     adv : in an experimental fashion; "this can be experimentally
           determined" [syn: {by experimentation}, {through an
           experiment}]
