---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Redundant_proof
offline_file: ""
offline_thumbnail: ""
uuid: 2e06f675-f61b-4cd1-b2d0-57bc3c808c3d
updated: 1484309497
title: Redundant proof
tags:
    - Local redundancy
    - Global redundancy
    - Example
    - Notes
categories:
    - Proof theory
---
In mathematical logic, a redundant proof is a proof that has a subset that is a shorter proof of the same result. That is, a proof 
  
    
      
        ψ
      
    
    {\displaystyle \psi }
  
 of 
  
    
      
        κ
      
    
    {\displaystyle \kappa }
  
 is considered redundant if there exists another proof 
  
    
      
        
          ψ
          
            ′
          
        
      
    
    {\displaystyle \psi ^{\prime }}
  
 of 
  
    
      
        
          κ
          
            ′
          
        
      
    
    {\displaystyle \kappa ^{\prime }}
  
 such that 
  
    
      
        
          κ
          
            ′
          
       ...
