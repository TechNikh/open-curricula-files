---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/potato
offline_file: ""
offline_thumbnail: ""
uuid: da6c191e-8271-4b5e-beae-5c57d60ee2f8
updated: 1484310353
title: potato
categories:
    - Dictionary
---
potato
     n 1: an edible tuber native to South America; a staple food of
          Ireland [syn: {white potato}, {Irish potato}, {murphy},
          {spud}, {tater}]
     2: annual native to South America having underground stolons
        bearing edible starchy tubers; widely cultivated as a
        garden vegetable; vines are poisonous [syn: {white potato},
         {white potato vine}, {Solanum tuberosum}]
     [also: {potatoes} (pl)]
