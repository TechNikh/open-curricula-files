---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cone_(algebraic_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: b24049eb-fb04-464c-a4fa-edf51c51fa3a
updated: 1484309138
title: Cone (algebraic geometry)
tags:
    - Examples
    - Properties
    - O(1)
categories:
    - Algebraic geometry
---
of a quasi-coherent graded OX-algebra R is called the cone or affine cone of R. Similarly, the relative Proj
