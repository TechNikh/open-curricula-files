---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enclosed
offline_file: ""
offline_thumbnail: ""
uuid: 0f5c4f45-44b7-4094-8545-e45295a29532
updated: 1484310138
title: enclosed
categories:
    - Dictionary
---
enclosed
     adj : closed in or surrounded or included within; "an enclosed
           porch"; "an enclosed yard"; "the enclosed check is to
           cover shipping and handling" [ant: {unenclosed}]
