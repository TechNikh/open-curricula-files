---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sunrise
offline_file: ""
offline_thumbnail: ""
uuid: 1c334481-2f15-41ce-9247-eb3590c20226
updated: 1484310431
title: sunrise
categories:
    - Dictionary
---
sunrise
     adj : of an industry or technology; new and developing;
           "high-technology sunrise industries" [syn: {sunrise(a)}]
     n 1: the first light of day; "we got up before dawn"; "they
          talked until morning" [syn: {dawn}, {dawning}, {morning},
           {aurora}, {first light}, {daybreak}, {break of day}, {break
          of the day}, {dayspring}, {sunup}, {cockcrow}] [ant: {sunset}]
     2: atmospheric phenomena accompanying the daily appearance of
        the sun
     3: the daily event of the sun rising above the horizon
