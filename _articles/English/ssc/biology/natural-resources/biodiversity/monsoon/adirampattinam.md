---
title: Adirampattinam
article_ref: https://en.wikipedia.org/wiki/Adirampattinam
tags:
    - Demographics
    - Geography and Climate
    - Economy
    - Arab connection
    - culture
    - Transport and Communication
    - Infrastructure
categories:
    - Monsoon
---
