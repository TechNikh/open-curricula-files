---
version: 1
type: video
provider: YouTube
id: 0671cRNjeKI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Perpendicular%20Lines.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ac0add2b-51d4-49bd-ba49-1057206f0e61
updated: 1486069561
title: Perpendicular Lines
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/0671cRNjeKI/default.jpg
    - https://i3.ytimg.com/vi/0671cRNjeKI/1.jpg
    - https://i3.ytimg.com/vi/0671cRNjeKI/2.jpg
    - https://i3.ytimg.com/vi/0671cRNjeKI/3.jpg
---
