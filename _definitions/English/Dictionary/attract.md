---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attract
offline_file: ""
offline_thumbnail: ""
uuid: 9aa01bbb-e035-4915-8a54-11c60af7f7d6
updated: 1484310403
title: attract
categories:
    - Dictionary
---
attract
     v 1: direct toward itself or oneself by means of some
          psychological power or physical attributes; "Her good
          looks attract the stares of many men"; "The ad pulled in
          many potential customers"; "This pianist pulls huge
          crowds"; "The store owner was happy that the ad drew in
          many new customers" [syn: {pull}, {pull in}, {draw}, {draw
          in}] [ant: {repel}]
     2: exert a force on (a body) causing it to approach or prevent
        it from moving away; "the gravitational pull of a planet
        attracts other bodies"
     3: be attractive to; "The idea of a vacation appeals to me";
        "The beautiful garden attracted many ...
