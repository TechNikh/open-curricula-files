---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dissipated
offline_file: ""
offline_thumbnail: ""
uuid: 30d1cbfe-d53a-4450-a5b0-19669525edad
updated: 1484310271
title: dissipated
categories:
    - Dictionary
---
dissipated
     adj 1: unrestrained by convention or morality; "Congreve draws a
            debauched aristocratic society"; "deplorably
            dissipated and degraded"; "riotous living"; "fast
            women" [syn: {debauched}, {degenerate}, {degraded}, {dissolute},
             {libertine}, {profligate}, {riotous}, {fast}]
     2: preoccupied with the pursuit of pleasure and especially
        games of chance; "led a dissipated life"; "a betting man";
        "a card-playing son of a bitch"; "a gambling fool";
        "sporting gents and their ladies" [syn: {betting}, {card-playing},
         {gambling}, {sporting}]
