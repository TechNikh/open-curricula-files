---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tortured
offline_file: ""
offline_thumbnail: ""
uuid: 16b0db4b-a029-40e5-b0cc-aeaa29d3696f
updated: 1484310166
title: tortured
categories:
    - Dictionary
---
tortured
     adj 1: subjected to intense pain; "hundreds of tortured prisoners"
     2: experiencing intense pain especially mental pain; "an
        anguished conscience"; "a small tormented schoolboy"; "a
        tortured witness to another's humiliation" [syn: {anguished},
         {tormented}]
