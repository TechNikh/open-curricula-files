---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/composite
offline_file: ""
offline_thumbnail: ""
uuid: ba578464-1eb9-48de-b43d-38dec540186d
updated: 1484310142
title: composite
categories:
    - Dictionary
---
composite
     adj 1: consisting of separate interconnected parts
     2: of or relating to or belonging to the plant family
        Compositae
     3: used of color
     4: a modified Corinthian style of architecture (a combination
        of Corinthian and Ionic)
     n 1: a conceptual whole made up of complicated and related parts;
          "the complex of shopping malls, houses, and roads
          created a new town" [syn: {complex}]
     2: considered the most highly evolved dicotyledonous plants,
        characterized by florets arranged in dense heads that
        resemble single flowers [syn: {composite plant}]
