---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distinguishing
offline_file: ""
offline_thumbnail: ""
uuid: 6c9b68d8-1f5b-4595-b779-6891dd5eb254
updated: 1484310353
title: distinguishing
categories:
    - Dictionary
---
distinguishing
     adj : serving to distinguish or identify a species or group; "the
           distinguishing mark of the species is its plumage";
           "distinctive tribal tattoos"; "we were asked to
           describe any identifying marks or distinguishing
           features" [syn: {distinctive}, {identifying(a)}]
