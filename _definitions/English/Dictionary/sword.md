---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sword
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484480641
title: sword
categories:
    - Dictionary
---
sword
     n : a cutting or thrusting weapon with a long blade [syn: {blade},
          {brand}, {steel}]
