---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Great_circle
offline_file: ""
offline_thumbnail: ""
uuid: 8b6f4ec0-06e4-4963-ac45-2de9212978e2
updated: 1484308930
title: Great circle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Great_circle_hemispheres.png
tags:
    - Derivation of shortest paths
    - Applications
categories:
    - Elementary geometry
---
A great circle, also known as an orthodrome or Riemannian circle, of a sphere is the intersection of the sphere and a plane that passes through the center point of the sphere. This partial case of a circle of a sphere is opposed to a small circle, the intersection of the sphere and a plane that does not pass through the center. Any diameter of any great circle coincides with a diameter of the sphere, and therefore all great circles have the same circumference as each other, and have the same center as the sphere. A great circle is the largest circle that can be drawn on any given sphere. Every circle in Euclidean 3-space is a great circle of exactly one sphere.
