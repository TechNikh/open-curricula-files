---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conway_polyhedron_notation
offline_file: ""
offline_thumbnail: ""
uuid: 9c5df304-1ea8-4a03-9d40-834b96c320a7
updated: 1484308932
title: Conway polyhedron notation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/480px-Conway_relational_chart.png
tags:
    - Operations on polyhedra
    - Extended operators
    - Chiral extended operators
    - Generating regular seeds
    - Examples
    - Geometric coordinates of derived forms
    - Spherical examples
    - Euclidean examples
    - Hyperbolic examples
    - Other polyhedra
    - Tetrahedral symmetry
    - Octahedral symmetry
    - Icosahedral symmetry
    - Dihedral symmetry
    - External links and references
categories:
    - Elementary geometry
---
The seed polyhedra are the Platonic solids, represented by the first letter of their name (T,O,C,I,D); the prisms (Pn), antiprisms (An) and pyramids (Yn). Any convex polyhedron can serve as a seed, as long as the operations can be executed on it.
