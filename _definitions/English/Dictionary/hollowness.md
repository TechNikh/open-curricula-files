---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hollowness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484462401
title: hollowness
categories:
    - Dictionary
---
hollowness
     n 1: the state of being hollow: having an empty space within
          [ant: {solidity}]
     2: the property of having a sunken area
