---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acidified
offline_file: ""
offline_thumbnail: ""
uuid: 86cf7c36-9140-413d-992c-25eb2be664ec
updated: 1484310375
title: acidified
categories:
    - Dictionary
---
acidify
     v 1: make sour or more sour [syn: {sour}, {acidulate}, {acetify}]
          [ant: {sweeten}]
     2: turn acidic; "the solution acetified" [syn: {acetify}] [ant:
         {alkalize}]
     [also: {acidified}]
