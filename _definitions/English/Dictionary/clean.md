---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clean
offline_file: ""
offline_thumbnail: ""
uuid: 4b569ce6-5ba5-493c-950d-b883399dfeb2
updated: 1484310249
title: clean
categories:
    - Dictionary
---
clean
     adj 1: free from dirt or impurities; or having clean habits;
            "children with clean shining faces"; "clean white
            shirts"; "clean dishes"; "a spotlessly clean house";
            "cats are clean animals" [ant: {dirty}]
     2: free of restrictions or qualifications; "a clean bill of
        health"; "a clear winner" [syn: {clear}]
     3: (of sound or color) free from anything that dulls or dims;
        "efforts to obtain a clean bass in orchestral recordings";
        "clear laughter like a waterfall"; "clear reds and blues";
        "a light lilting voice like a silver bell" [syn: {clear},
        {light}, {unclouded}]
     4: free from impurities; "clean ...
