---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/multiplication
offline_file: ""
offline_thumbnail: ""
uuid: fe42fd4a-f129-4b31-a0c5-45372c89f168
updated: 1484310142
title: multiplication
categories:
    - Dictionary
---
multiplication
     n 1: an arithmetic operation that is the inverse of division; the
          product of two numbers is computed; "the multiplication
          of four by three gives twelve"; "four times three equals
          twelve" [syn: {times}]
     2: the act of producing offspring or multiplying by such
        production [syn: {generation}, {propagation}]
     3: a multiplicative increase; "repeated copying leads to a
        multiplication of errors"; "this multiplication of cells
        is a natural correlate of growth"
