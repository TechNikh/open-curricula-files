---
version: 1
type: article
id: https://en.wikipedia.org/wiki/CORDIC
offline_file: ""
offline_thumbnail: ""
uuid: 4aec6c4f-5402-460d-88e9-50303ed843ca
updated: 1484309343
title: CORDIC
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-CORDIC-illustration.png
tags:
    - History
    - Applications
    - Hardware
    - Software
    - Modes of operation
    - Rotation mode
    - Vectoring mode
    - Implementation
    - Software example
    - Hardware example
    - Related algorithms
categories:
    - Trigonometry
---
CORDIC (for COordinate Rotation DIgital Computer),[1][2][3] also known as Volder's algorithm, is a simple and efficient algorithm to calculate hyperbolic and trigonometric functions, typically converging with one digit (or bit) per iteration. It is therefore also a prominent example of digit-by-digit algorithms. CORDIC and closely related methods known as pseudo-multiplication and pseudo-division or factor combining are commonly used when no hardware multiplier is available (e.g. in simple microcontrollers and FPGAs), as the only operations it requires are addition, subtraction, bitshift and table lookup. As such, they belong to the class of shift-and-add algorithms.
