---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/veiled
offline_file: ""
offline_thumbnail: ""
uuid: d7c17289-2355-4dbb-98d9-652e020aefa2
updated: 1484310601
title: veiled
categories:
    - Dictionary
---
veiled
     adj 1: having or as if having a veil or concealing cover; "a veiled
            dancer"; "a veiled hat"; "veiled threats"; "veiled
            insults" ; "the night-veiled landscape" [ant: {unveiled}]
     2: muted or unclear; "veiled sounds"; "the image is veiled or
        foggy"
