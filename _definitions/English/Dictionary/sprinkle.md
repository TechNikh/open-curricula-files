---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sprinkle
offline_file: ""
offline_thumbnail: ""
uuid: b2db5778-eade-4c0b-8d4e-86b358eab9e7
updated: 1484310230
title: sprinkle
categories:
    - Dictionary
---
sprinkle
     n 1: a light shower that falls in some locations and not others
          nearby [syn: {scattering}, {sprinkling}]
     2: the act of sprinkling or splashing water; "baptized with a
        sprinkling of holy water"; "a sparge of warm water over
        the malt" [syn: {sprinkling}, {sparge}]
     v 1: distribute loosely; "He scattered gun powder under the
          wagon" [syn: {scatter}, {dot}, {dust}, {disperse}]
     2: cause (a liquid) to spatter about, especially with force;
        "She splashed the water around her" [syn: {splash}, {splosh}]
     3: rain gently; "It has only sprinkled, but the roads are
        slick" [syn: {spit}, {spatter}, {patter}, ...
