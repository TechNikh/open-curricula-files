---
version: 1
type: video
provider: YouTube
id: rN9rUYxk1Zs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a278a5b0-cfb9-4e7d-886d-dbcb80ca823b
updated: 1486069605
title: Introduction to dependent probability
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/rN9rUYxk1Zs/default.jpg
    - https://i3.ytimg.com/vi/rN9rUYxk1Zs/1.jpg
    - https://i3.ytimg.com/vi/rN9rUYxk1Zs/2.jpg
    - https://i3.ytimg.com/vi/rN9rUYxk1Zs/3.jpg
---
