---
version: 1
type: video
provider: YouTube
id: FU_2_czSP4I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c2c8a838-2714-4b51-ad5e-18d38cf33f4b
updated: 1486069605
title: Reasonable samples
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/FU_2_czSP4I/default.jpg
    - https://i3.ytimg.com/vi/FU_2_czSP4I/1.jpg
    - https://i3.ytimg.com/vi/FU_2_czSP4I/2.jpg
    - https://i3.ytimg.com/vi/FU_2_czSP4I/3.jpg
---
