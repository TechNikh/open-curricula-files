---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jar
offline_file: ""
offline_thumbnail: ""
uuid: f6d96ee4-342d-44b2-ac81-86092b666c3a
updated: 1484310232
title: jar
categories:
    - Dictionary
---
jar
     n 1: a vessel (usually cylindrical) with a wide mouth and without
          handles
     2: the quantity contained in a jar; "he drank a jar of beer"
        [syn: {jarful}]
     3: a sudden impact; "the door closed with a jolt" [syn: {jolt},
         {jounce}]
     v 1: be incompatible; be or come into conflict; "These colors
          clash" [syn: {clash}, {collide}]
     2: move or cause to move with a sudden jerky motion [syn: {jolt}]
     3: shock physically; "Georgia was shaken up in the Tech game"
        [syn: {shake up}, {bump around}]
     4: affect in a disagreeable way; "This play jarred the
        audience"
     5: place in a cylindrical vessel; "jar the jam"
     ...
