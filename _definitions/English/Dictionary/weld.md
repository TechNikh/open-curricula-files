---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weld
offline_file: ""
offline_thumbnail: ""
uuid: 9c98f6df-5bd3-4f60-a21e-4b0319b7191f
updated: 1484310581
title: weld
categories:
    - Dictionary
---
weld
     n 1: European mignonette cultivated as a source of yellow dye;
          naturalized in North America [syn: {dyer's rocket}, {dyer's
          mignonette}, {Reseda luteola}]
     2: United States abolitionist (1803-1895) [syn: {Theodore
        Dwight Weld}]
     3: a metal joint formed by softening with heat and fusing or
        hammering together
     v 1: join together by heating; "weld metal"
     2: unite closely or intimately; "Her gratitude welded her to
        him"
