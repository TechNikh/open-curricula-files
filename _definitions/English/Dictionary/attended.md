---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attended
offline_file: ""
offline_thumbnail: ""
uuid: 56950501-8da9-4b63-910a-2687fe4ec563
updated: 1484310551
title: attended
categories:
    - Dictionary
---
attended
     adj 1: having accompaniment or companions or escort; "there were
            lone gentlemen and gentlemen accompanied by their
            wives" [syn: {accompanied}] [ant: {unaccompanied}]
     2: having a caretaker or other watcher [syn: {tended to(p)}]
