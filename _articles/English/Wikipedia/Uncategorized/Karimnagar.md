---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Karimnagar
offline_file: ""
offline_thumbnail: ""
uuid: 6ce51a8b-1e2c-45cb-895c-2ce141b30afa
updated: 1484308333
title: Karimnagar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Apollo_karimnagar.jpg
tags:
    - Etymology
    - History
    - Satavahana dynasty
    - Demographics
    - Climate
    - Civic administration
    - Healthcare
    - culture
    - Transport
    - Education
categories:
    - Uncategorized
---
Karimnagar is a Municipal Corporation and district head quarters of Karimnagar district of Telangana state.[4][5] It is situated on the banks of Manair River, which is a tributary of the Godavari River.It is the fourth largest and fastest growing urban settlement in the state, according to 2011 census. It has registered a population growth rate of 45.46% and 38.87% respectively over the past two decades between 1991 to 2011, which is highest growth rate among major cities of Telangana.[6] It serves as a major educational and health hub for the northern districts of Telangana.[7] It is a major business center and widely known for Granite and Agro-based industries.[8][9]
