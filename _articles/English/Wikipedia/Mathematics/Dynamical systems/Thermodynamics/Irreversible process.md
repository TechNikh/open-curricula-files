---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Irreversible_process
offline_file: ""
offline_thumbnail: ""
uuid: 549ea2dd-24fd-4b91-a6b2-882b028bfa3e
updated: 1484308846
title: Irreversible process
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Adiabatic-irrevisible-state-change.svg.png
tags:
    - Absolute versus statistical reversibility
    - History
    - Examples of irreversible processes
    - Complex systems
categories:
    - Thermodynamics
---
In thermodynamics, a change in the thermodynamic state of a system and all of its surroundings cannot be precisely restored to its initial state by infinitesimal changes in some property of the system without expenditure of energy. A system that undergoes an irreversible process may still be capable of returning to its initial state; however, the impossibility occurs in restoring the environment to its own initial conditions. An irreversible process increases the entropy of the universe. However, because entropy is a state function, the change in entropy of the system is the same whether the process is reversible or irreversible. The second law of thermodynamics can be used to determine ...
