---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Church%E2%80%93Kleene_ordinal'
offline_file: ""
offline_thumbnail: ""
uuid: 687e6f4b-8993-44ae-99e9-f53e09fd1d70
updated: 1484309492
title: Church–Kleene ordinal
categories:
    - Proof theory
---
In mathematics, the Church–Kleene ordinal, 
  
    
      
        
          ω
          
            1
          
          
            
              C
              K
            
          
        
      
    
    {\displaystyle \omega _{1}^{\mathrm {CK} }}
  
, named after Alonzo Church and S. C. Kleene, is a large countable ordinal. It is the set of all recursive ordinals and the smallest non-recursive ordinal. It is also the first ordinal which is not hyperarithmetical, and the first admissible ordinal after ω.
