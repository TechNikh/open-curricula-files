---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strahler_number
offline_file: ""
offline_thumbnail: ""
uuid: e5f245dd-2e21-445e-a21c-d94780d5018a
updated: 1484309423
title: Strahler number
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Flussordnung_%2528Strahler%2529.svg.png'
tags:
    - Definition
    - Applications
    - River networks
    - Other hierarchical systems
    - Register allocation
    - Related parameters
    - Bifurcation ratio
    - Pathwidth
    - Notes
categories:
    - Graph invariants
---
These numbers were first developed in hydrology by Robert E. Horton (1945) and Arthur Newell Strahler (1952, 1957); in this application, they are referred to as the Strahler stream order and are used to define stream size based on a hierarchy of tributaries. They also arise in the analysis of L-systems and of hierarchical biological structures such as (biological) trees and animal respiratory and circulatory systems, in register allocation for compilation of high-level programming languages and in the analysis of social networks. Alternative stream ordering systems have been developed by Shreve[1][2] and Hodgkinson et al.[3]
