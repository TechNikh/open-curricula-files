---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/erode
offline_file: ""
offline_thumbnail: ""
uuid: 19439907-24ff-4c8a-9a68-ce08a20668e9
updated: 1484310531
title: erode
categories:
    - Dictionary
---
erode
     v 1: become ground down or deteriorate; "Her confidence eroded"
          [syn: {gnaw}, {gnaw at}, {eat at}, {wear away}]
     2: remove soil or rock; "Rain eroded the terraces" [syn: {eat
        away}, {fret}]
