---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greatly
offline_file: ""
offline_thumbnail: ""
uuid: 21d268d7-68c9-44ba-9942-59d811b989a9
updated: 1484310196
title: greatly
categories:
    - Dictionary
---
greatly
     adv : to a great extent or degree; "he improved greatly"; "greatly
           reduced"
