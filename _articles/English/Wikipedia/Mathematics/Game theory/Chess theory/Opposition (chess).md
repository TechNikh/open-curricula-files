---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Opposition_(chess)
offline_file: ""
offline_thumbnail: ""
uuid: 79771943-a366-46bf-bb9d-2b5ac945085f
updated: 1484309020
title: Opposition (chess)
tags:
    - Direct opposition (or) Rook opposition
    - Example
    - Diagonal opposition
    - Distant opposition
    - Teaching tool
    - Purpose
    - Notes
categories:
    - Chess theory
---
In chess, opposition (or direct opposition) is the situation occurring when two kings face each other on a rank or file, with only one square in-between them. In such a situation, the player not having to move is said to "have the opposition" (Flear 2004:12). It is a special type of zugzwang and most often occurs in endgames with only kings and pawns (Flear 2000:36). The side with the move may have to move the king away, potentially allowing the opposing king access to important squares. Taking the opposition is a means to an end (normally forcing the opponent's king to move to a weaker position) and is not always the best thing to do.
