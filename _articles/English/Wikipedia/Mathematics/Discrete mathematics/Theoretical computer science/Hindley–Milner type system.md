---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hindley%E2%80%93Milner_type_system'
offline_file: ""
offline_thumbnail: ""
uuid: 6192916e-ee31-47e6-a7a8-de3ca108a356
updated: 1484308674
title: Hindley–Milner type system
tags:
    - Introduction
    - Syntax
    - Monotypes
    - Polytype
    - Free type variables
    - Context and typing
    - Polymorphic type order
    - Deductive system
    - Typing rules
    - Principal type
    - Let-polymorphism
    - Towards an algorithm
    - Degrees of freedom choosing the rules
    - Syntax-directed rule system
    - Degrees of freedom instantiating the rules
    - Algorithm W
    - Original presentation of Algorithm W
    - Further topics
    - Recursive definitions
    - Notes
categories:
    - Theoretical computer science
---
In type theory and functional programming, Hindley–Milner (HM), also known as Damas–Milner or Damas–Hindley–Milner, is a classical type system for the lambda calculus with parametric polymorphism, first described by J. Roger Hindley[1] and later rediscovered by Robin Milner.[2] Luis Damas contributed a close formal analysis and proof of the method in his PhD thesis.[3][4]
