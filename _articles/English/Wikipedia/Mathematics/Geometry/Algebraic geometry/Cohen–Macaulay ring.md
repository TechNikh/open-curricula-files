---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cohen%E2%80%93Macaulay_ring'
offline_file: ""
offline_thumbnail: ""
uuid: 5037fda6-e754-4435-b692-11d3df8fa10d
updated: 1484309134
title: Cohen–Macaulay ring
tags:
    - Definition
    - Examples
    - "Miracle Flatness or Hironaka's criterion"
    - Properties
    - The unmixedness theorem
    - Counterexamples
    - Grothendieck duality
    - Notes
categories:
    - Algebraic geometry
---
In mathematics, a Cohen–Macaulay ring is a commutative ring with some of the algebro-geometric properties of a smooth variety, such as local equidimensionality. Under mild assumptions, a local ring is Cohen–Macaulay exactly when it is a finitely generated free module over a regular local subring. Cohen–Macaulay rings play a central role in commutative algebra: they form a very broad class, and yet they are well-understood in many ways.
