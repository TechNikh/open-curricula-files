---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retained
offline_file: ""
offline_thumbnail: ""
uuid: 856e146a-c12e-443a-b958-5099c314590c
updated: 1484310327
title: retained
categories:
    - Dictionary
---
retained
     adj : continued in your keeping or use or memory; "in...the
           retained pattern of dancers and guests remembered"
           [syn: {maintained}]
