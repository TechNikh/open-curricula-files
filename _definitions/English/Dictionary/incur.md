---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incur
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484565781
title: incur
categories:
    - Dictionary
---
incur
     v 1: make oneself subject to; bring upon oneself; become liable
          to; "People who smoke incur a great danger to their
          health"
     2: receive a specified treatment (abstract); "These aspects of
        civilization do not find expression or receive an
        interpretation"; "His movie received a good review"; "I
        got nothing but trouble for my good intentions" [syn: {receive},
         {get}, {find}, {obtain}]
     [also: {incurring}, {incurred}]
