---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/starch
offline_file: ""
offline_thumbnail: ""
uuid: e5e14ee4-41b9-4388-bf8d-ed05f3fd13bd
updated: 1484310339
title: starch
categories:
    - Dictionary
---
starch
     n : a complex carbohydrate found chiefly in seeds, fruits,
         tubers, roots and stem pith of plants, notably in corn,
         potatoes, wheat, and rice; an important foodstuff and
         used otherwise especially in adhesives and as fillers and
         stiffeners for paper and textiles [syn: {amylum}]
     v : stiffen with starch; "starch clothes"
