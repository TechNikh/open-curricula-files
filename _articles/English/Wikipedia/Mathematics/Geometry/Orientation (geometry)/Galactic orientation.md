---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Galactic_orientation
offline_file: ""
offline_thumbnail: ""
uuid: bec36372-93ba-4403-91b3-778bf66725a7
updated: 1484309293
title: Galactic orientation
tags:
    - Galaxies
    - Primordial Vorticity Model
    - Pancake Model
    - Hierarchy Model
categories:
    - Orientation (geometry)
---
Galactic clusters[1] are gravitationally bound large-scale structures of multiple galaxies. The evolution of these aggregates is determined by time and manner of formation and the process of how their structures and constituents have been changing with time. Gamow (1952) and Weizscker (1951) showed that the observed rotations of galaxies are important for cosmology. They postulated that the rotation of galaxies might be a clue of physical conditions under which these systems formed. Thus, understanding the distribution of spatial orientations of the spin vectors of galaxies is critical to understanding the origin of the angular momenta of galaxies.
