---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ugly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484499301
title: ugly
categories:
    - Dictionary
---
ugly
     adj 1: displeasing to the senses and morally revolting; "an ugly
            face"; "ugly furniture"; "war is ugly" [ant: {beautiful}]
     2: deficient in beauty; "ugly gray slums"
     3: inclined to anger or bad feelings with overtones of menace;
        "a surly waiter"; "an ugly frame of mind" [syn: {surly}]
     4: morally reprehensible; "would do something as despicable as
        murder"; "ugly crimes"; "the vile development of slavery
        appalled them" [syn: {despicable}, {vile}, {unworthy}]
     5: threatening or foreshadowing evil or tragic developments; "a
        baleful look"; "forbidding thunderclouds"; "his tone
        became menacing"; "ominous rumblings of ...
