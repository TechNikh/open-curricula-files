---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remove
offline_file: ""
offline_thumbnail: ""
uuid: 6e30c1f4-b7f5-43f4-8f5a-cee8490816f3
updated: 1484310319
title: remove
categories:
    - Dictionary
---
remove
     n : degree of figurative distance or separation; "just one
         remove from madness" or "it imitates at many removes a
         Shakespearean tragedy";
     v 1: remove something concrete, as by lifting, pushing, taking
          off, etc. or remove something abstract; "remove a
          threat"; "remove a wrapper"; "Remove the dirty dishes
          from the table"; "take the gun from your pocket"; "This
          machine withdraws heat from the environment" [syn: {take},
           {take away}, {withdraw}]
     2: remove from a position or an office
     3: dispose of; "Get rid of these old shoes!"; "The company got
        rid of all the dead wood" [syn: {get rid of}]
   ...
