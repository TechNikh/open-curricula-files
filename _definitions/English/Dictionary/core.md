---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/core
offline_file: ""
offline_thumbnail: ""
uuid: ed862197-2820-4c92-a20a-7f430d68c927
updated: 1484310183
title: core
categories:
    - Dictionary
---
core
     n 1: the center of an object; "the ball has a titanium core"
     2: a small group of indispensable persons or things; "five
        periodicals make up the core of their publishing program"
        [syn: {nucleus}, {core group}]
     3: the central part of the Earth
     4: the choicest or most essential or most vital part of some
        idea or experience; "the gist of the prosecutor's
        argument"; "the heart and soul of the Republican Party";
        "the nub of the story" [syn: {kernel}, {substance}, {center},
         {essence}, {gist}, {heart}, {heart and soul}, {inwardness},
         {marrow}, {meat}, {nub}, {pith}, {sum}, {nitty-gritty}]
     5: a cylindrical sample ...
