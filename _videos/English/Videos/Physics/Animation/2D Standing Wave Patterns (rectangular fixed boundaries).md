---
version: 1
type: video
provider: YouTube
id: NMlys8A0_4s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/2D%20Standing%20Wave%20Patterns%20%28rectangular%20fixed%20boundaries%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d7533f48-d81b-4a68-ac5d-93d51b6708bd
updated: 1486069651
title: 2D Standing Wave Patterns (rectangular fixed boundaries)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/NMlys8A0_4s/default.jpg
    - https://i3.ytimg.com/vi/NMlys8A0_4s/1.jpg
    - https://i3.ytimg.com/vi/NMlys8A0_4s/2.jpg
    - https://i3.ytimg.com/vi/NMlys8A0_4s/3.jpg
---
