---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperbolic_equilibrium_point
offline_file: ""
offline_thumbnail: ""
uuid: 1f0a4e7b-20ff-41b0-bff1-16c60e787456
updated: 1484308795
title: Hyperbolic equilibrium point
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Phase_Portrait_Sadle.svg.png
tags:
    - Maps
    - Flows
    - Example
    - Comments
    - Notes
categories:
    - Limit sets
---
In the study of dynamical systems, a hyperbolic equilibrium point or hyperbolic fixed point is a fixed point that does not have any center manifolds. Near a hyperbolic point the orbits of a two-dimensional, non-dissipative system resemble hyperbolas. This fails to hold in general. Strogatz[1] notes that "hyperbolic is an unfortunate name – it sounds like it should mean 'saddle point' – but it has become standard." Several properties hold about a neighborhood of a hyperbolic point, notably[2]
