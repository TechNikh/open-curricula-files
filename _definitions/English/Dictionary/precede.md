---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/precede
offline_file: ""
offline_thumbnail: ""
uuid: 97a7e5e9-bbb0-4825-bb4e-c0a80361ef14
updated: 1484310397
title: precede
categories:
    - Dictionary
---
precede
     v 1: be earlier in time; go back further; "Stone tools precede
          bronze tools" [syn: {predate}, {forego}, {antecede}, {antedate}]
          [ant: {postdate}]
     2: come before; "Most English adjectives precede the noun they
        modify" [syn: {predate}]
     3: be the predecessor of; "Bill preceded John in the long line
        of Susan's husbands" [syn: {come before}] [ant: {succeed}]
     4: move ahead (of others) in time or space [syn: {lead}] [ant:
        {follow}]
     5: furnish with a preface or introduction; "She always precedes
        her lectures with a joke"; "He prefaced his lecture with a
        critical remark about the institution" [syn: ...
