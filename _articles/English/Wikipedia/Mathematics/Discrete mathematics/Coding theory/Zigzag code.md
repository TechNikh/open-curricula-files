---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zigzag_code
offline_file: ""
offline_thumbnail: ""
uuid: 6d91e9a7-97f6-4c2d-b320-fbb4f2c27266
updated: 1484308556
title: Zigzag code
categories:
    - Coding theory
---
In coding theory, a zigzag code is a type of linear error-correcting code introduced by Ping, Huang & Phamdo (2001).[1] They are defined by partitioning the input data into segments of fixed size, and adding sequence of check bits to the data, where each check bit is the exclusive or of the bits in a single segment and of the previous check bit in the sequence.
