---
version: 1
type: video
provider: YouTube
id: i07DpPirsbM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6c47c149-f301-41ef-9f64-6fcc5afa213f
updated: 1486069639
title: Living On Light An Experiment 25
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/i07DpPirsbM/default.jpg
    - https://i3.ytimg.com/vi/i07DpPirsbM/1.jpg
    - https://i3.ytimg.com/vi/i07DpPirsbM/2.jpg
    - https://i3.ytimg.com/vi/i07DpPirsbM/3.jpg
---
