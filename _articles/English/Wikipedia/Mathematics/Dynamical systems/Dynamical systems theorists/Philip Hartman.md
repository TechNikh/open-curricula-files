---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Philip_Hartman
offline_file: ""
offline_thumbnail: ""
uuid: ba4f5a51-a5dc-4675-9bdf-ab516b6fa508
updated: 1484308684
title: Philip Hartman
categories:
    - Dynamical systems theorists
---
Philip Hartman (May 16, 1915 – August 28, 2015) was an American mathematician at Johns Hopkins University working on differential equations who introduced the Hartman–Grobman theorem. He served as Chairman of the Mathematics Department at Johns Hopkins for several years.
