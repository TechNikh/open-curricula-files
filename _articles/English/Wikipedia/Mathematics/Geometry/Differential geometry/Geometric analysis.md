---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometric_analysis
offline_file: ""
offline_thumbnail: ""
uuid: 1826cac7-3c2a-4b59-aa8a-367f8a697875
updated: 1484309202
title: Geometric analysis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Saddle_Tower_Minimal_Surfaces.png
categories:
    - Differential geometry
---
It includes both the use of geometrical methods in the study of partial differential equations (when it is also known as "geometric PDE"), and the application of the theory of partial differential equations to geometry. It incorporates problems involving curves and surfaces, or domains with curved boundaries, but also the study of Riemannian manifolds in arbitrary dimension. The calculus of variations is sometimes regarded as part of geometric analysis, because differential equations arising from variational principles have a strong geometric content. Geometric analysis also includes global analysis, which concerns the study of differential equations on manifolds, and the relationship ...
