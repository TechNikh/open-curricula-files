---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/timber
offline_file: ""
offline_thumbnail: ""
uuid: 6394fb7f-e926-4344-91ec-0b0aa3d752b2
updated: 1484310249
title: timber
categories:
    - Dictionary
---
timber
     n 1: the wood of trees cut and prepared for use as building
          material [syn: {lumber}]
     2: a beam made of wood
     3: a post made of wood
     4: land that is covered with trees and shrubs [syn: {forest}, {woodland},
         {timberland}]
     5: (music) the distinctive property of a complex sound (a voice
        or noise or musical sound); "the timbre of her soprano was
        rich and lovely"; "the muffled tones of the broken bell
        summoned them to meet" [syn: {timbre}, {quality}, {tone}]
