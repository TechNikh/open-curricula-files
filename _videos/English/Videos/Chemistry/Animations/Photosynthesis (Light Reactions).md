---
version: 1
type: video
provider: YouTube
id: hj_WKgnL6MI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Photosynthesis%20%28Light%20Reactions%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b4f97c27-1095-4315-bfb8-ed4bee019488
updated: 1486069664
title: Photosynthesis (Light Reactions)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/hj_WKgnL6MI/default.jpg
    - https://i3.ytimg.com/vi/hj_WKgnL6MI/1.jpg
    - https://i3.ytimg.com/vi/hj_WKgnL6MI/2.jpg
    - https://i3.ytimg.com/vi/hj_WKgnL6MI/3.jpg
---
