---
version: 1
type: article
id: https://en.wikipedia.org/wiki/M-ratio
offline_file: ""
offline_thumbnail: ""
uuid: fcb2d20f-bda1-4e54-ba62-b7c448e6bcd9
updated: 1484309062
title: M-ratio
tags:
    - Calculation
    - Effective M
categories:
    - Poker strategy
---
In no-limit or pot-limit poker, a player's M-ratio (also called "M number", "M factor"[1] or just "M") is a measure of the health of his chip stack as a function of the cost to play each round. In simple terms, a player can sit passively in the game, making only compulsory bets, for M laps of the dealer button before running out of chips. A high M means the player can afford to wait a high number of rounds before making a move. The concept applies primarily in tournament poker; in a cash game, a player can in principle manipulate his M at will, simply by purchasing more chips.
