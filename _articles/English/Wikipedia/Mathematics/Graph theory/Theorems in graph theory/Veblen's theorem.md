---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Veblen%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 52e44c93-f9fd-4911-9dcb-bb25ad9addc6
updated: 1484309457
title: "Veblen's theorem"
categories:
    - Theorems in graph theory
---
In mathematics, Veblen's theorem, introduced by Oswald Veblen (1912), states that the set of edges of a finite graph can be written as a union of disjoint simple cycles if and only if every vertex has even degree. Thus, it is closely related to the theorem of Euler (1736) that a finite graph has an Euler tour (a single non-simple cycle that covers the edges of the graph) if and only if it is connected and every vertex has even degree. Indeed, a representation of a graph as a union of simple cycles may be obtained from an Euler tour by repeatedly splitting the tour into smaller cycles whenever there is a repeated vertex. However, Veblen's theorem applies also to disconnected graphs, and can ...
