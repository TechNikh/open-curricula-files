---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chicken_(game)
offline_file: ""
offline_thumbnail: ""
uuid: 9a5c0c3a-9f10-47d9-aa1c-e70cf2472191
updated: 1484308527
title: Chicken (game)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hawk-Dove_transforming_into_Prisoner%2527s_Dilemma.gif'
tags:
    - Popular versions
    - Game theoretic applications
    - Chicken
    - Hawk-Dove
    - Hawk-Dove variants
    - Pre-commitment
    - Best response mapping and Nash equilibria
    - Strategy polymorphism vs strategy mixing
    - Symmetry breaking
    - Correlated equilibrium and Chicken
    - Uncorrelated asymmetries and solutions to the Hawk-Dove game
    - Replicator dynamics
    - Related games
    - Brinkmanship
    - War of attrition
    - Schedule chicken and project management
    - Notes
categories:
    - Evolutionary game theory
---
The game of chicken, also known as the hawk-dove game or snowdrift[1] game, is a model of conflict for two players in game theory. The principle of the game is that while it is for both players beneficial if the other player yields, their own optimal choice depends on what their opponent is doing: if their opponent yields, the player should not, but if the opponent fails to yield, the player should.
