---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cornea
offline_file: ""
offline_thumbnail: ""
uuid: 3dae3e16-551b-453f-8a8e-bfdd5c41d287
updated: 1484310309
title: cornea
categories:
    - Dictionary
---
cornea
     n : transparent anterior portion of the outer covering of the
         eye; it covers the lens and iris and is continuous with
         the sclera
     [also: {corneae} (pl)]
