---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imposing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484646061
title: imposing
categories:
    - Dictionary
---
imposing
     adj 1: impressive in appearance; "a baronial mansion"; "an imposing
            residence"; "a noble tree"; "severe-looking policemen
            sat astride noble horses"; "stately columns" [syn: {baronial},
             {noble}, {stately}]
     2: used of a person's appearance or behavior; befitting an
        eminent person; "his distinguished bearing"; "the
        monarch's imposing presence"; "she reigned in magisterial
        beauty" [syn: {distinguished}, {magisterial}]
