---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exhaustion
offline_file: ""
offline_thumbnail: ""
uuid: d7d35590-d46a-4c48-b821-7ef0c4b25933
updated: 1484310486
title: exhaustion
categories:
    - Dictionary
---
exhaustion
     n 1: extreme fatigue
     2: serious weakening and loss of energy [syn: {debilitation}, {enervation},
         {enfeeblement}]
     3: the act of exhausting something entirely
