---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pretext
offline_file: ""
offline_thumbnail: ""
uuid: 8911754d-ee72-4320-8302-865302c40732
updated: 1484310176
title: pretext
categories:
    - Dictionary
---
pretext
     n 1: something serving to conceal plans; a fictitious reason that
          is concocted in order to conceal the real reason [syn: {stalking-horse}]
     2: an artful or simulated semblance; "under the guise of
        friendship he betrayed them" [syn: {guise}, {pretense}, {pretence}]
