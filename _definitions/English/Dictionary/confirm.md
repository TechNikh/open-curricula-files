---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confirm
offline_file: ""
offline_thumbnail: ""
uuid: 885c3e15-d955-477d-89dc-f6785e606e40
updated: 1484310311
title: confirm
categories:
    - Dictionary
---
confirm
     v 1: establish or strengthen as with new evidence or facts; "his
          story confirmed my doubts"; "The evidence supports the
          defendant" [syn: {corroborate}, {sustain}, {substantiate},
           {support}, {affirm}] [ant: {negate}]
     2: strengthen or make more firm; "The witnesses confirmed the
        victim's account" [syn: {reassert}]
     3: make more firm; "Confirm thy soul in self-control!"
     4: as of a person to a position; "The Senate confirmed the
        President's candidate for Secretary of Defense"
     5: administer the rite of confirmation to; "the children were
        confirmed in their mother's faith"
