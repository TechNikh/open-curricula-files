---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conference
offline_file: ""
offline_thumbnail: ""
uuid: f0c7e342-1d09-4ddb-9cce-3ff7e4952962
updated: 1484310431
title: conference
categories:
    - Dictionary
---
conference
     n 1: a prearranged meeting for consultation or exchange of
          information or discussion (especially one with a formal
          agenda)
     2: an association of sports teams that organizes matches for
        its members [syn: {league}]
     3: a discussion among participants who have an agreed (serious)
        topic [syn: {group discussion}]
