---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equivalence_of_metrics
offline_file: ""
offline_thumbnail: ""
uuid: 43e6df26-b334-4c90-bcd1-d94d64feab3d
updated: 1484309262
title: Equivalence of metrics
tags:
    - Topological equivalence
    - Strong equivalence
    - Properties preserved by equivalence
    - Notes
categories:
    - Metric geometry
---
In the following, 
  
    
      
        X
      
    
    {\displaystyle X}
  
 will denote a non-empty set and 
  
    
      
        
          d
          
            1
          
        
      
    
    {\displaystyle d_{1}}
  
 and 
  
    
      
        
          d
          
            2
          
        
      
    
    {\displaystyle d_{2}}
  
 will denote two metrics on 
  
    
      
        X
      
    
    {\displaystyle X}
  
.
