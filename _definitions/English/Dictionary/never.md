---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/never
offline_file: ""
offline_thumbnail: ""
uuid: e32e635b-f216-4380-8e2e-d7c64ac5d0ca
updated: 1484310311
title: never
categories:
    - Dictionary
---
never
     adv 1: not ever; at no time in the past or future; "I have never
            been to China"; "I shall never forget this day"; "had
            never seen a circus"; "never on Sunday"; "I will never
            marry you!" [syn: {ne'er}] [ant: {always}]
     2: not at all; certainly not; not in any circumstances; "never
        fear"; "bringing up children is never easy"; "that will
        never do"; "what is morally wrong can never be politically
        right"
