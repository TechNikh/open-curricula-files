---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Irrigation_game
offline_file: ""
offline_thumbnail: ""
uuid: 6b321d58-e14d-41f2-81bb-9f459f6f86e2
updated: 1484308531
title: Irrigation game
tags:
    - Mathematical definition
    - Example
    - Properties and Results
categories:
    - Cooperative games
---
Irrigation games are cooperative games which model cost sharing problems on networks (more precisely, on rooted trees). The irrigation game is a transferable utility game assigned to a cost-tree problem. A common example of this cost-tree problems are the irrigation networks. The irrigation ditch is represented by a graph, its nodes are water users, the edges are sections of the ditch. There is a cost of maintaining the ditch (each section has an own maintenance cost), and we are looking for the fair division of the costs among the users. The irrigation games are mentioned first by Aadland and Kolpin 1998, but the formal concept and the characterization of the game class is introduced by ...
