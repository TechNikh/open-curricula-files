---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contractor
offline_file: ""
offline_thumbnail: ""
uuid: e1084aa2-4557-4c58-9c98-e33fecd154f8
updated: 1484310152
title: contractor
categories:
    - Dictionary
---
contractor
     n 1: someone (a person or firm) who contracts to build things
     2: the bridge player in contract bridge who wins the bidding
        and can declare which suit is to be trumps [syn: {declarer}]
     3: (law) a party to a contract
     4: a bodily organ that contracts [syn: {contractile organ}]
