---
version: 1
type: video
provider: YouTube
id: rppY07Kq6W4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 36bedddb-4e03-4542-b51c-1dba23dc2863
updated: 1486069648
title: 'Amazing Psychedelic Fractal Kaleidoscope HD - My Dream'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/rppY07Kq6W4/default.jpg
    - https://i3.ytimg.com/vi/rppY07Kq6W4/1.jpg
    - https://i3.ytimg.com/vi/rppY07Kq6W4/2.jpg
    - https://i3.ytimg.com/vi/rppY07Kq6W4/3.jpg
---
