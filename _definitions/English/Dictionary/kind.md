---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kind
offline_file: ""
offline_thumbnail: ""
uuid: 6ec5af28-85c5-4267-9446-b9e566ae0da9
updated: 1484310339
title: kind
categories:
    - Dictionary
---
kind
     adj 1: having or showing a tender and considerate and helpful
            nature; used especially of persons and their behavior;
            "kind to sick patients"; "a kind master"; "kind words
            showing understanding and sympathy"; "thanked her for
            her kind letter" [ant: {unkind}]
     2: liberal; "kind words of praise"
     3: conducive to comfort; beneficial; "the genial sunshine"; "a
        kind climate"; "hot summer pavements are anything but kind
        to the feet" [syn: {genial}]
     4: expressing sympathy
     5: characterized by mercy, and compassion; "compassionate
        toward disadvantaged people"; "kind to animals"; "a humane
        ...
