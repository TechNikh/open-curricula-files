---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chemical
offline_file: ""
offline_thumbnail: ""
uuid: 99160ced-70a2-4038-ae45-f8c9e5207d54
updated: 1484310319
title: chemical
categories:
    - Dictionary
---
chemical
     adj 1: relating to or used in chemistry; "chemical engineer";
            "chemical balance" [syn: {chemic}]
     2: of or made from or using substances produced by or used in
        reactions involving atomic or molecular changes; "chemical
        fertilizer"
     n : produced by or used in a reaction involving changes in atoms
         or molecules
