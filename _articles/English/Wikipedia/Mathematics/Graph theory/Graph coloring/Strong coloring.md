---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strong_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 9bb6fde1-6805-4d53-8477-a5eb5f77193f
updated: 1484309387
title: Strong coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Strong_coloring_sample.svg.png
categories:
    - Graph coloring
---
In graph theory, a strong coloring, with respect to a partition of the vertices into (disjoint) subsets of equal sizes, is a (proper) vertex coloring in which every color appears exactly once in every partition. When the order of the graph G is not divisible by k, we add isolated vertices to G just enough to make the order of the new graph G′ divisible by k. In that case, a strong coloring of G′ minus the previously added isolated vertices is considered a strong coloring of G. A graph is strongly k-colorable if, for each partition of the vertices into sets of size k, it admits a strong coloring.
