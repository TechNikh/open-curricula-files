---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Space_group
offline_file: ""
offline_thumbnail: ""
uuid: ee85537b-7d4a-4052-88c6-2158dcf38708
updated: 1484309319
title: Space group
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/750px-Earth-moon_2.jpg
tags:
    - History
    - Elements of a space group
    - Elements fixing a point
    - Translations
    - Glide planes
    - Screw axes
    - General formula
    - Notation for space groups
    - Classification systems for space groups
    - Space groups in other dimensions
    - "Bieberbach's theorems"
    - Classification in small dimensions
    - Magnetic groups and time reversal
    - Table of space groups in 2 dimensions (wallpaper groups)
    - Table of space groups in 3 dimensions
categories:
    - Symmetry
---
In mathematics, physics and chemistry, a space group is the symmetry group of a configuration in space, usually in three dimensions.[1] In three dimensions, there are 219 distinct types, or 230 if chiral copies are considered distinct. Space groups are also studied in dimensions other than 3 where they are sometimes called Bieberbach groups, and are discrete cocompact groups of isometries of an oriented Euclidean space.
