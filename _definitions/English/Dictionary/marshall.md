---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marshall
offline_file: ""
offline_thumbnail: ""
uuid: 7773d68f-93d0-4b2e-8557-eff34a2b5df1
updated: 1484310567
title: marshall
categories:
    - Dictionary
---
Marshall
     n 1: United States actor (1914-1998) [syn: {E. G. Marshall}]
     2: United States general and statesman who as Secretary of
        State organized the European Recovery Program (1880-1959)
        [syn: {George Marshall}, {George Catlett Marshall}]
     3: United States jurist; as chief justice of the Supreme Court
        he established the principles of United States
        constitutional law (1755-1835) [syn: {John Marshall}]
     4: (in some countries) a military officer of highest rank [syn:
         {marshal}]
     5: a law officer having duties similar to those of a sheriff in
        carrying out the judgments of a court of law [syn: {marshal}]
