---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gregor
offline_file: ""
offline_thumbnail: ""
uuid: ae960e98-d4fe-4ed7-9b83-a889391e4cd7
updated: 1484308351
title: Gregory (given name)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gregorythegreat.jpg
tags:
    - Name days
    - Forms in different languages
    - people
    - Religious leaders
    - Popes
    - Patriarchs
    - Indian (Malankara) Orthodox Church
    - Other people
    - Fictional characters
categories:
    - Uncategorized
---
The masculine first name Gregory derives from the Latin name "Gregorius," which came from the late Greek name "Γρηγόριος" (Grēgorios) meaning "watchful, alert" (derived from Greek "γρηγoρεῖν" "grēgorein" meaning "to watch").
