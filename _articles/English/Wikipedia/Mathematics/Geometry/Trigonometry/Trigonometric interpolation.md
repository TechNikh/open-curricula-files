---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trigonometric_interpolation
offline_file: ""
offline_thumbnail: ""
uuid: 2284aefa-9556-4443-a354-dc4e0d9e49c1
updated: 1484309350
title: Trigonometric interpolation
tags:
    - Formulation of the interpolation problem
    - Formulation in the complex plane
    - Solution of the problem
    - Odd number of points
    - Even number of points
    - Equidistant nodes
    - Odd number of points
    - Even number of points
    - Implementation
    - Relation with the discrete Fourier transform
    - Applications in numerical computing
categories:
    - Trigonometry
---
In mathematics, trigonometric interpolation is interpolation with trigonometric polynomials. Interpolation is the process of finding a function which goes through some given data points. For trigonometric interpolation, this function has to be a trigonometric polynomial, that is, a sum of sines and cosines of given periods. This form is especially suited for interpolation of periodic functions.
