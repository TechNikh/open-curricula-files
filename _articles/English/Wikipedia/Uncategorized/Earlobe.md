---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ear_lobe
offline_file: ""
offline_thumbnail: ""
uuid: fbeafe20-51ad-4206-bb23-44b126be834f
updated: 1484308378
title: Earlobe
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/120px-Gray904.png
tags:
    - "Earlobe's organogenesis"
    - Genetics
    - Clinical significance
    - Society and culture
    - Earlobe piercing and stretching
    - Negative effects of wearing earrings
categories:
    - Uncategorized
---
The human earlobe (lobulus auriculae) is composed of tough areolar and adipose connective tissues, lacking the firmness and elasticity of the rest of the auricle (the external structure of the ear). In some cases the lower lobe is connected to the side of the face. Since the earlobe does not contain cartilage[1] it has a large blood supply and may help to warm the ears and maintain balance. The zoologist Desmond Morris in his book The Naked Ape (1967) conjectured that the lobes developed as an additional erogenous zone to facilitate the extended sexuality necessary in the evolution of human monogamous pair bonding.[2] However, earlobes are not generally considered to have any major ...
