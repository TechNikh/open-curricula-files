---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/earthquake
offline_file: ""
offline_thumbnail: ""
uuid: 5f223709-32f5-4b5b-b864-4626019e26c8
updated: 1484310140
title: earthquake
categories:
    - Dictionary
---
earthquake
     n 1: shaking and vibration at the surface of the earth resulting
          from underground movement along a fault plane of from
          volcanic activity [syn: {quake}, {temblor}, {seism}]
     2: a disturbance that is extremely disruptive; "selling the
        company caused an earthquake among the employees"
