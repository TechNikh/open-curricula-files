---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sealed
offline_file: ""
offline_thumbnail: ""
uuid: 64bf7321-b127-49cb-8fd6-646c2dbbecc2
updated: 1484310189
title: sealed
categories:
    - Dictionary
---
sealed
     adj 1: established irrevocably; "his fate is sealed" [syn: {certain}]
            [ant: {unsealed}]
     2: closed or secured with or as if with a seal; "my lips are
        sealed"; "the package is still sealed"; "the premises are
        sealed" [ant: {unsealed}]
     3: undisclosed for the time being; "sealed orders"; "a sealed
        move in chess"
     4: determined irrevocably; "his fate is sealed"
     5: having been paved
     6: covered with a waterproof coating; "a sealed driveway"
     7: (of walls) covered with a coat of plaster [syn: {plastered}]
     8: closed so tightly as to be airtight or watertight
