---
version: 1
type: video
provider: YouTube
id: EN-x-zXXVwQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b53e1a88-3d44-4900-8d80-b2eb760b072c
updated: 1486069680
title: 'The Integumentary System, Part 2 - Skin Deeper- Crash Course A&P #7'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/EN-x-zXXVwQ/default.jpg
    - https://i3.ytimg.com/vi/EN-x-zXXVwQ/1.jpg
    - https://i3.ytimg.com/vi/EN-x-zXXVwQ/2.jpg
    - https://i3.ytimg.com/vi/EN-x-zXXVwQ/3.jpg
---
