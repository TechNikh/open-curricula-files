---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adviser
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484523241
title: adviser
categories:
    - Dictionary
---
adviser
     n : an expert who gives advice; "an adviser helped students
         select their courses"; "the United States sent military
         advisors to Guatemala" [syn: {advisor}, {consultant}]
