---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contradiction
offline_file: ""
offline_thumbnail: ""
uuid: 31921bce-367e-4a54-93d7-66bd3ab65499
updated: 1484310547
title: contradiction
categories:
    - Dictionary
---
contradiction
     n 1: opposition between two conflicting forces or ideas
     2: (logic) a statement that is necessarily false; "the
        statement `he is brave and he is not brave' is a
        contradiction" [syn: {contradiction in terms}]
     3: the speech act of contradicting someone; "he spoke as if he
        thought his claims were immune to contradiction"
