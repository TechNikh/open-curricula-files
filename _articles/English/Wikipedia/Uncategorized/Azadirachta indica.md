---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Neem
offline_file: ""
offline_thumbnail: ""
uuid: 8362427a-88a6-47c9-b43b-c8a009ce1c9b
updated: 1484308349
title: Azadirachta indica
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Neem_tree.JPG
tags:
    - Description
    - Etymology
    - Vernacular names
    - Ecology
    - Weed status
    - Uses
    - As a vegetable
    - Traditional medicinal use
    - Safety issues
    - Pest and disease control
    - Neem oil for polymeric resins
    - Construction
    - Other uses
    - Association with Hindu festivals in India
    - Chemical compounds
    - Genome and transcriptomes
    - Cultural and social impact
    - Symbolism
    - Biotechnology
    - Gallery
categories:
    - Uncategorized
---
Azadirachta indica, also known as Neem,[2] Nimtree,[2] and Indian Lilac[2] is a tree in the mahogany family Meliaceae. It is one of two species in the genus Azadirachta, and is native to India and the Indian subcontinent including Nepal, Pakistan, Bangladesh, and Sri Lanka. It typically is grown in tropical and semi-tropical regions. Neem trees now also grow in islands located in the southern part of Iran. Its fruits and seeds are the source of neem oil.
