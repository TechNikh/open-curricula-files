---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carrot
offline_file: ""
offline_thumbnail: ""
uuid: 2ff3aa71-ec80-40d1-8b19-0c61ec5fe20c
updated: 1484310381
title: carrot
categories:
    - Dictionary
---
carrot
     n 1: deep orange edible root of the cultivated carrot plant
     2: perennial plant widely cultivated as an annual in many
        varieties for its long conical deep-orange edible roots;
        temperate and tropical regions [syn: {cultivated carrot},
        {Daucus carota sativa}]
     3: orange root; important source of carotene
     4: promise of reward as in "carrot and stick"; "used the carrot
        of subsidized housing for the workers to get their vote";
