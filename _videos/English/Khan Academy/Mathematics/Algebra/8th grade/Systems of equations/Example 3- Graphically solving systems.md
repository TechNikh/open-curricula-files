---
version: 1
type: video
provider: YouTube
id: F5Nb6cIRZLU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/8th%20grade/Systems%20of%20equations/Example%203-%20Graphically%20solving%20systems.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 619f3b18-452a-4524-b90f-e428534041a5
updated: 1486069565
title: 'Example 3- Graphically solving systems'
categories:
    - Systems of equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/F5Nb6cIRZLU/default.jpg
    - https://i3.ytimg.com/vi/F5Nb6cIRZLU/1.jpg
    - https://i3.ytimg.com/vi/F5Nb6cIRZLU/2.jpg
    - https://i3.ytimg.com/vi/F5Nb6cIRZLU/3.jpg
---
