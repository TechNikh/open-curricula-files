---
version: 1
type: video
provider: YouTube
id: 9UhChwcVWq8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%233%20%28a%20%26%20b%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7bf8eaa3-afa4-4ba4-90ad-01e371369922
updated: 1486069689
title: '2011 Calculus AB Free Response #3 (a & b)'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/9UhChwcVWq8/default.jpg
    - https://i3.ytimg.com/vi/9UhChwcVWq8/1.jpg
    - https://i3.ytimg.com/vi/9UhChwcVWq8/2.jpg
    - https://i3.ytimg.com/vi/9UhChwcVWq8/3.jpg
---
