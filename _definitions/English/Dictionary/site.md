---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/site
offline_file: ""
offline_thumbnail: ""
uuid: 050e25cf-1972-434a-8dc7-47bf6ef325e2
updated: 1484310477
title: site
categories:
    - Dictionary
---
site
     n 1: the piece of land on which something is located (or is to be
          located); "a good site for the school" [syn: {land site}]
     2: physical position in relation to the surroundings; "the
        sites are determined by highly specific sequences of
        nucleotides" [syn: {situation}]
     3: a computer connected to the internet that maintains a series
        of web pages on the World Wide Web; "the Israeli web site
        was damaged by hostile hackers" [syn: {web site}, {internet
        site}]
     v : assign a location to; "The company located some of their
         agents in Los Angeles" [syn: {locate}, {place}]
