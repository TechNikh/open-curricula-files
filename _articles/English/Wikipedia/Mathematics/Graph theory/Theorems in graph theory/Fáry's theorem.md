---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/F%C3%A1ry%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: ac9c6486-3d07-4f42-ad07-71fce0667bf1
updated: 1484309454
title: "Fáry's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fary-induction.svg.png
tags:
    - Proof
    - Related results
    - Notes
categories:
    - Theorems in graph theory
---
In mathematics, Fáry's theorem states that any simple planar graph can be drawn without crossings so that its edges are straight line segments. That is, the ability to draw graph edges as curves instead of as straight line segments does not allow a larger class of graphs to be drawn. The theorem is named after István Fáry, although it was proved independently by Klaus Wagner (1936), Fáry (1948), and S. K. Stein (1951).
