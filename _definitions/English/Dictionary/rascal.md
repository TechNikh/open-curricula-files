---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rascal
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484499421
title: rascal
categories:
    - Dictionary
---
rascal
     n 1: a deceitful and unreliable scoundrel [syn: {rogue}, {knave},
           {rapscallion}, {scalawag}, {scallywag}, {varlet}]
     2: one who is playfully mischievous [syn: {imp}, {scamp}, {monkey},
         {rapscallion}, {scalawag}, {scallywag}]
