---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/viewers
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484544481
title: viewers
categories:
    - Dictionary
---
viewers
     n : the audience reached by television [syn: {viewing audience},
          {TV audience}]
