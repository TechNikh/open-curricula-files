---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/end
offline_file: ""
offline_thumbnail: ""
uuid: 163294e7-0cb8-462b-bf91-51eed2e5ccb3
updated: 1484310333
title: end
categories:
    - Dictionary
---
end
     n 1: either extremity of something that has length; "the end of
          the pier"; "she knotted the end of the thread"; "they
          rode to the end of the line"
     2: the point in time at which something ends; "the end of the
        year"; "the ending of warranty period" [syn: {ending}]
        [ant: {beginning}, {middle}]
     3: the concluding parts of an event or occurrence; "the end was
        exciting"; "I had to miss the last of the movie" [syn: {last},
         {final stage}]
     4: the state of affairs that a plan is intended to achieve and
        that (when achieved) terminates behavior intended to
        achieve it; "the ends justify the means" [syn: {goal}]
 ...
