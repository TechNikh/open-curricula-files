---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/selection
offline_file: ""
offline_thumbnail: ""
uuid: 02fe4451-024d-416f-8971-1f4bf7ac09c4
updated: 1484310295
title: selection
categories:
    - Dictionary
---
selection
     n 1: the act of choosing or selecting; "your choice of colors was
          unfortunate"; "you can take your pick" [syn: {choice}, {option},
           {pick}]
     2: an assortment of things from which a choice can be made;
        "the store carried a large selection of shoes"
     3: the person or thing chosen or selected; "he was my pick for
        mayor" [syn: {choice}, {pick}]
     4: a natural process resulting in the evolution of organisms
        best adapted to the environment [syn: {survival}, {survival
        of the fittest}, {natural selection}]
     5: a passage selected from a larger work; "he presented
        excerpts from William James' philosophical ...
