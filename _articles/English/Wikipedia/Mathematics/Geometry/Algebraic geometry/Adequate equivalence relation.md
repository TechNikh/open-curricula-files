---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adequate_equivalence_relation
offline_file: ""
offline_thumbnail: ""
uuid: 51d889af-f63d-420a-a7c9-72a39b1b010d
updated: 1484309127
title: Adequate equivalence relation
tags:
    - Definition
    - Examples of equivalence relations
    - Notes
categories:
    - Algebraic geometry
---
In algebraic geometry, a branch of mathematics, an adequate equivalence relation is an equivalence relation on algebraic cycles of smooth projective varieties used to obtain a well-working theory of such cycles, and in particular, well-defined intersection products. Pierre Samuel formalized the concept of an adequate equivalence relation in 1958.[1] Since then it has become central to theory of motives. For every adequate equivalence relation, one may define the category of pure motives with respect to that relation.
