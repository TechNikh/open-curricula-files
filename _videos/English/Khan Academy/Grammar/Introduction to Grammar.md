---
version: 1
type: video
provider: YouTube
id: O-6q-siuMik
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Introduction%20to%20Grammar.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d23f8f8f-a5fd-41f9-8aee-6489a4d514f2
updated: 1486069601
title: Introduction to Grammar
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/O-6q-siuMik/default.jpg
    - https://i3.ytimg.com/vi/O-6q-siuMik/1.jpg
    - https://i3.ytimg.com/vi/O-6q-siuMik/2.jpg
    - https://i3.ytimg.com/vi/O-6q-siuMik/3.jpg
---
Khan Academy Grammarian David Rheinstrom welcomes you to his favorite topic: the study of language, its rules, and its conventions. By understanding English – by speaking it, by writing it, by reading this very sentence – you are a grammarian yourself!

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/v/introduction-to-nouns-the-parts-of-speech-grammar-khan-academy 

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UCgk9hgdd2Uy6Fjfx_dK1j8A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
