---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/offence
offline_file: ""
offline_thumbnail: ""
uuid: 17066443-1c0e-4c34-9157-4f11f3f73f04
updated: 1484310599
title: offence
categories:
    - Dictionary
---
offence
     n 1: the action of attacking an enemy [syn: {offense}, {offensive}]
     2: the team that has the ball (or puck) and is trying to score
        [syn: {offense}] [ant: {defense}, {defense}]
     3: a feeling of anger caused by being offended; "he took
        offence at my question" [syn: {umbrage}, {offense}]
     4: a lack of politeness; a failure to show regard for others;
        wounding the feelings or others [syn: {discourtesy}, {offense},
         {offensive activity}]
     5: a crime less serious than a felony [syn: {misdemeanor}, {misdemeanour},
         {infraction}, {offense}, {violation}, {infringement}]
