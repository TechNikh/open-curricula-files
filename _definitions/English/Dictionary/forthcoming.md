---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forthcoming
offline_file: ""
offline_thumbnail: ""
uuid: baa5ff56-2ca1-4f95-9197-cec7fbe45312
updated: 1484310471
title: forthcoming
categories:
    - Dictionary
---
forthcoming
     adj 1: at ease in talking to others [syn: {extroverted}, {outgoing}]
     2: of the relatively near future; "the approaching election";
        "this coming Thursday"; "the forthcoming holidays"; "the
        upcoming spring fashions" [syn: {approaching}, {coming(a)},
         {upcoming}]
     3: available when required or as promised; "federal funds were
        not forthcoming"
