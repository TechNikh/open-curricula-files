---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insurgency
offline_file: ""
offline_thumbnail: ""
uuid: 3bb34a75-b469-47ca-afde-597492633e4c
updated: 1484310188
title: insurgency
categories:
    - Dictionary
---
insurgency
     n : an organized rebellion aimed at overthrowing a constituted
         government through the use of subversion and armed
         conflict [syn: {insurgence}]
