---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cl%C3%A9lie'
offline_file: ""
offline_thumbnail: ""
uuid: 866b31bb-5fb8-4f91-ac5c-312e988b04e7
updated: 1484309357
title: Clélie
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Clelies_curve.png
categories:
    - Geometry stubs
---
A curve of this type is confined to the surface of a sphere of radius 
  
    
      
        a
      
    
    {\displaystyle a}
  
,[2] as is obvious from the parametric equations for the surface of a sphere. When 
  
    
      
        m
        =
        1
      
    
    {\displaystyle m=1}
  
, this is Viviani's curve.
