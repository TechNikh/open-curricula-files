---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recipient
offline_file: ""
offline_thumbnail: ""
uuid: fbd0a499-4f14-4056-804e-03d2607f544f
updated: 1484310431
title: recipient
categories:
    - Dictionary
---
recipient
     n 1: a person who gets something [syn: {receiver}]
     2: the semantic role of the animate entity that is passively
        involved in the happening denoted by the verb in the
        clause [syn: {recipient role}]
