---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/militant
offline_file: ""
offline_thumbnail: ""
uuid: fb179f42-26bf-4afa-98a9-eb5682391ea8
updated: 1484310577
title: militant
categories:
    - Dictionary
---
militant
     adj 1: engaged in war; "belligerent (or warring) nations"; "a
            fighting war" [syn: {belligerent}, {fighting}, {war-ridden},
             {warring}]
     2: showing a fighting disposition without self-seeking; "highly
        competitive sales representative"; "militant in fighting
        for better wages for workers"; "his self-assertive and
        ubiquitous energy" [syn: {competitive}]
     n : a militant reformer [syn: {activist}]
