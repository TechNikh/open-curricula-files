---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relieved
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484509501
title: relieved
categories:
    - Dictionary
---
relieved
     adj : (of pain or sorrow) made easier to bear [syn: {alleviated},
           {eased}]
