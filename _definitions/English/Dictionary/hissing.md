---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hissing
offline_file: ""
offline_thumbnail: ""
uuid: b82db878-ffba-48ea-ac38-c342a079229d
updated: 1484310429
title: hissing
categories:
    - Dictionary
---
hissing
     adj : noisy sound like a sustained `s'; "`hissing' is the sound a
           snake makes"
     n : a fricative sound (especially as an expression of
         disapproval); "the performers could not be heard over the
         hissing of the audience" [syn: {hiss}, {sibilation}]
