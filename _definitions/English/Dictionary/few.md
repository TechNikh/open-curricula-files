---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/few
offline_file: ""
offline_thumbnail: ""
uuid: 55e73028-35cf-4dfc-8178-f37c9bc9fcca
updated: 1484310343
title: few
categories:
    - Dictionary
---
few
     adj : a quantifier that can be used with count nouns and is often
           preceded by `a'; a small but indefinite number; "a few
           weeks ago"; "a few more wagons than usual"; "an
           invalid's pleasures are few and far between"; "few
           roses were still blooming"; "few women have led troops
           in battle" [ant: {many}]
     n 1: an indefinite but relatively small number; "they bought a
          case of beer and drank a few"
     2: a small elite group; "it was designed for the discriminating
        few"
