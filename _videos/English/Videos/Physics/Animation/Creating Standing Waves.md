---
version: 1
type: video
provider: YouTube
id: 3BN5-JSsu_4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c56b983c-c264-42ca-9479-8198b961864e
updated: 1486069651
title: Creating Standing Waves
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/3BN5-JSsu_4/default.jpg
    - https://i3.ytimg.com/vi/3BN5-JSsu_4/1.jpg
    - https://i3.ytimg.com/vi/3BN5-JSsu_4/2.jpg
    - https://i3.ytimg.com/vi/3BN5-JSsu_4/3.jpg
---
