---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/innumerable
offline_file: ""
offline_thumbnail: ""
uuid: 7aeaf467-b2ff-4a38-9b26-74595d1f606c
updated: 1484310249
title: innumerable
categories:
    - Dictionary
---
innumerable
     adj : too numerous to be counted; "incalculable riches";
           "countless hours"; "an infinite number of reasons";
           "innumerable difficulties"; "the multitudinous seas";
           "myriad stars"; "untold thousands" [syn: {countless}, {infinite},
            {innumerous}, {myriad(a)}, {multitudinous}, {numberless},
            {uncounted}, {unnumberable}, {unnumbered}, {unnumerable}]
