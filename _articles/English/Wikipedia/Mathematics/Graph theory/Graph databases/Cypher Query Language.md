---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cypher_Query_Language
offline_file: ""
offline_thumbnail: ""
uuid: e4666114-1def-439a-9181-4c8b84663856
updated: 1484309405
title: Cypher Query Language
categories:
    - Graph databases
---
Cypher is a declarative graph query language for the graph database Neo4j that allows for expressive and efficient querying and updating of the graph store. Cypher is a relatively simple but still very powerful language. Very complicated database queries can easily be expressed through Cypher. This allows users to focus on their domain instead of getting lost in database access.[1]
