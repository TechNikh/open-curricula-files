---
version: 1
type: video
provider: YouTube
id: k1Rtxxbw7Yg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Alzheimer%27s%20Enigma.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 23a87dc6-aeb5-49f2-80d9-b879ac28f4ee
updated: 1486069670
title: "Alzheimer's Enigma"
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/k1Rtxxbw7Yg/default.jpg
    - https://i3.ytimg.com/vi/k1Rtxxbw7Yg/1.jpg
    - https://i3.ytimg.com/vi/k1Rtxxbw7Yg/2.jpg
    - https://i3.ytimg.com/vi/k1Rtxxbw7Yg/3.jpg
---
