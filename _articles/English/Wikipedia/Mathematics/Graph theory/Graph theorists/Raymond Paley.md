---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Raymond_Paley
offline_file: ""
offline_thumbnail: ""
uuid: 69cd8541-97bd-4c16-a826-fcb78dec4b78
updated: 1484309473
title: Raymond Paley
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Paley.jpg
categories:
    - Graph theorists
---
Raymond Edward Alan Christopher Paley (7 January 1907 – 7 April 1933) was an English mathematician. Paley was born in Bournemouth, England. He was educated at Eton. From there he entered Trinity College, Cambridge where he showed himself as a brilliant student. He won a Smith's Prize in 1930 and was elected a fellow of Trinity College.
