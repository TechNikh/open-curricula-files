---
version: 1
type: video
provider: YouTube
id: oRNg9Eqlxz0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f9a2b122-1f0f-45e4-9cfa-2d3a82f66823
updated: 1486069618
title: Area of a shaded region
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/oRNg9Eqlxz0/default.jpg
    - https://i3.ytimg.com/vi/oRNg9Eqlxz0/1.jpg
    - https://i3.ytimg.com/vi/oRNg9Eqlxz0/2.jpg
    - https://i3.ytimg.com/vi/oRNg9Eqlxz0/3.jpg
---
