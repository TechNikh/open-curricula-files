---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Combinatorial_commutative_algebra
offline_file: ""
offline_thumbnail: ""
uuid: bc80198d-28b3-4a34-95ef-311fe8aa0ad1
updated: 1484309142
title: Combinatorial commutative algebra
categories:
    - Algebraic geometry
---
Combinatorial commutative algebra is a relatively new, rapidly developing mathematical discipline. As the name implies, it lies at the intersection of two more established fields, commutative algebra and combinatorics, and frequently uses methods of one to address problems arising in the other. Less obviously, polyhedral geometry plays a significant role.
