---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relaxed
offline_file: ""
offline_thumbnail: ""
uuid: 35cbc38d-f297-45a4-a4b6-147c9347abf5
updated: 1484310330
title: relaxed
categories:
    - Dictionary
---
relaxed
     adj 1: without strain or anxiety; "gave the impression of being
            quite relaxed"; "a relaxed and informal discussion"
            [syn: {at ease}] [ant: {tense}]
     2: made less tense or rigid; "his relaxed muscles"
