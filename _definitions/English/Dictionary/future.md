---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/future
offline_file: ""
offline_thumbnail: ""
uuid: 448c302b-4c8f-46f5-9581-98998e40d74c
updated: 1484310254
title: future
categories:
    - Dictionary
---
future
     adj 1: yet to be or coming; "some future historian will evaluate
            him" [ant: {past}, {present(a)}]
     2: effective in or looking toward the future; "he was preparing
        for future employment opportunities"
     3: coming at a subsequent time or stage; "the future president
        entered college at the age of 16"; "awaiting future
        actions on the bill"; "later developments"; "without
        ulterior argument" [syn: {future(a)}, {later(a)}, {ulterior}]
     4: (of elected officers) elected but not yet serving; "our next
        president" [syn: {future(a)}, {next}, {succeeding(a)}]
     5: a verb tense or other formation referring to events or
        ...
