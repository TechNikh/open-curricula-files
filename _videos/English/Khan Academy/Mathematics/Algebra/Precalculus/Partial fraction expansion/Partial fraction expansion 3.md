---
version: 1
type: video
provider: YouTube
id: A52fEdPn9lg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Partial%20fraction%20expansion/Partial%20fraction%20expansion%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7835be6c-d12f-4ec6-ba13-ab47e97f07b0
updated: 1486069573
title: Partial fraction expansion 3
categories:
    - Partial fraction expansion
thumbnail_urls:
    - https://i3.ytimg.com/vi/A52fEdPn9lg/default.jpg
    - https://i3.ytimg.com/vi/A52fEdPn9lg/1.jpg
    - https://i3.ytimg.com/vi/A52fEdPn9lg/2.jpg
    - https://i3.ytimg.com/vi/A52fEdPn9lg/3.jpg
---
Dealing with repeated factors

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/precalculus/partial-fraction-expans/partial-fraction/e/partial_fraction_expansion_1?utm_source=YT&utm_medium=Desc&utm_campaign=Precalculus

Watch the next lesson: https://www.khanacademy.org/math/precalculus/limit_topic_precalc/limits_precalc/v/introduction-to-limits-hd?utm_source=YT&utm_medium=Desc&utm_campaign=Precalculus

Missed the previous lesson? 
https://www.khanacademy.org/math/precalculus/partial-fraction-expans/partial-fraction/v/partial-fraction-expansion-2?utm_source=YT&utm_medium=Desc&utm_campaign=Precalculus

Precalculus on Khan Academy: You may think that precalculus is simply the course you take before calculus. You would be right, of course, but that definition doesn't mean anything unless you have some knowledge of what calculus is. Let's keep it simple, shall we? Calculus is a conceptual framework which provides systematic techniques for solving problems. These problems are appropriately applicable to analytic geometry and algebra. Therefore....precalculus gives you the background for the mathematical concepts, problems, issues and techniques that appear in calculus, including trigonometry, functions, complex numbers, vectors, matrices, and others. There you have it ladies and gentlemen....an introduction to precalculus!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Precalculus channel:
https://www.youtube.com/channel/UCBeHztHRWuVvnlwm20u2hNA?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
