---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trousers
offline_file: ""
offline_thumbnail: ""
uuid: 4a6da18f-9372-45ba-9f61-48434119a061
updated: 1484310150
title: trousers
categories:
    - Dictionary
---
trousers
     n : (usually in the plural) a garment extending from the waist
         to the knee or ankle, covering each leg separately; "he
         had a sharp crease in his trousers" [syn: {pants}]
