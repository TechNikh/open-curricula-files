---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_canonization
offline_file: ""
offline_thumbnail: ""
uuid: 73a9a0e1-2818-464a-97b9-a3776977e495
updated: 1484308611
title: Graph canonization
categories:
    - Graph theory
---
In graph theory, a branch of mathematics, graph canonization is the problem finding a canonical form of a given graph G. A canonical form is a labeled graph Canon(G) that is isomorphic to G, such that every graph that is isomorphic to G has the same canonical form as G. Thus, from a solution to the graph canonization problem, one could also solve the problem of graph isomorphism: to test whether two graphs G and H are isomorphic, compute their canonical forms Canon(G) and Canon(H), and test whether these two canonical forms are identical.
