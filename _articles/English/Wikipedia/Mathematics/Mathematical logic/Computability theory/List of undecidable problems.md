---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_of_undecidable_problems
offline_file: ""
offline_thumbnail: ""
uuid: 16628b47-7dd7-43a5-ab1e-f97431318a04
updated: 1484309483
title: List of undecidable problems
tags:
    - Problems in logic
    - Problems about abstract machines
    - Problems about matrices
    - Problems in combinatorial group theory
    - Problems in topology
    - Problems in analysis
    - Other problems
    - Notes
    - Bibliography
categories:
    - Computability theory
---
In computability theory, an undecidable problem is a type of computational problem that requires a yes/no answer, but where there cannot possibly be any computer program that always gives the correct answer; that is any possible program would sometimes give the wrong answer or run forever without giving any answer. More formally, an undecidable problem is a problem whose language is not a recursive set; see decidability. There are uncountably many undecidable problems, so the list below is necessarily incomplete. Though undecidable languages are not recursive languages, they may be subsets of Turing recognizable languages i.e. such undecidable languages may be recursively enumerable.
