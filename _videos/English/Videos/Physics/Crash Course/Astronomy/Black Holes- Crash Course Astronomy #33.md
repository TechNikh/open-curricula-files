---
version: 1
type: video
provider: YouTube
id: qZWPBKULkdQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Black%20Holes-%20Crash%20Course%20Astronomy%20%2333.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 15d0ea42-6b06-4cf0-8787-9ab7236c4481
updated: 1486069684
title: 'Black Holes- Crash Course Astronomy #33'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/qZWPBKULkdQ/default.jpg
    - https://i3.ytimg.com/vi/qZWPBKULkdQ/1.jpg
    - https://i3.ytimg.com/vi/qZWPBKULkdQ/2.jpg
    - https://i3.ytimg.com/vi/qZWPBKULkdQ/3.jpg
---
