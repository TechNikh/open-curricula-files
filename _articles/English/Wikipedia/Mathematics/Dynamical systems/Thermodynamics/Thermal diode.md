---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_diode
offline_file: ""
offline_thumbnail: ""
uuid: 8050f764-cdad-4569-a85b-f960750dc15f
updated: 1484308870
title: Thermal diode
tags:
    - One-way heat-flow
    - Electrical diode thermal effect or function
    - Thermoelectric heat-pump or cooler
    - Peltier devices
    - Advancements
categories:
    - Thermodynamics
---
The term thermal diode is sometimes used for a (possibly non-electrical) device which causes heat to flow preferentially in one direction. Or, the term may be used to describe an electrical (semiconductor) diode in reference to a thermal effect or function. Or the term may be used to describe both situations, where an electrical diode is used as a heat-pump or thermoelectric cooler.
