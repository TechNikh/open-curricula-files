---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/selective
offline_file: ""
offline_thumbnail: ""
uuid: 30f6576a-dcdb-4cac-b749-2065e7eb8871
updated: 1484310325
title: selective
categories:
    - Dictionary
---
selective
     adj 1: tending to select; characterized by careful choice; "an
            exceptionally quick and selective reader"- John Mason
            Brown
     2: characterized by very careful or fastidious selection; "the
        school was very selective in its admissions"
