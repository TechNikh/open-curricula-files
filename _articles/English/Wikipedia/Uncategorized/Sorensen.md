---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sorensen
offline_file: ""
offline_thumbnail: ""
uuid: aad0e08b-8b2c-4846-9ce2-dc902c2849ca
updated: 1484308312
title: Sorensen
categories:
    - Uncategorized
---
Sorensen, or Sorenson, is a surname that can be of Danish or Scandinavian origin. The basic derivation is "son of Søren", the Danish variety of the name Severin. The name almost exclusively comes from Danish or Norwegian emigrants named Sørensen who altered the spelling of their names when they moved to countries outside Scandinavia whose orthographies do not use the letter ø.
