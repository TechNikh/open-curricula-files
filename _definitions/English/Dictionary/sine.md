---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sine
offline_file: ""
offline_thumbnail: ""
uuid: 84e680ba-c651-4d77-a984-c636e0d703e9
updated: 1484310214
title: sine
categories:
    - Dictionary
---
sine
     n : ratio of the opposite side to the hypotenuse of a
         right-angled triangle [syn: {sin}]
