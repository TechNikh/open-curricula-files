---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harry
offline_file: ""
offline_thumbnail: ""
uuid: 38d506ad-42f8-475c-8a98-4270f77c9a8a
updated: 1484310181
title: harry
categories:
    - Dictionary
---
harry
     v 1: annoy continually or chronically; "He is known to harry his
          staff when he is overworked"; "This man harasses his
          female co-workers" [syn: {harass}, {hassle}, {chivy}, {chivvy},
           {chevy}, {chevvy}, {beset}, {plague}, {molest}, {provoke}]
     2: make a pillaging or destructive raid on (a place), as in
        wartimes [syn: {ravage}]
     [also: {harried}]
