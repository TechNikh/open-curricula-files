---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gasoline
offline_file: ""
offline_thumbnail: ""
uuid: 0453b5f8-a77f-4a9b-9964-3bca7eeea77d
updated: 1484308340
title: Gasoline
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-GasStationHiroshima.jpg
tags:
    - History
    - Etymology and terminology
    - Octane rating
    - Stability
    - Energy content
    - density
    - Chemical analysis and production
    - Additives
    - Antiknock additives
    - Tetraethyllead
    - Lead Replacement Petrol
    - MMT
    - Fuel stabilizers (antioxidants and metal deactivators)
    - Detergents
    - Ethanol
    - European Union
    - Brazil
    - Australia
    - United States
    - India
    - Dye
    - Oxygenate blending
    - Safety
    - Environmental considerations
    - Toxicity
    - Inhalation
    - Flammability
    - Use and pricing
    - Europe
    - United States
    - Comparison with other fuels
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
Gasoline /ˈɡæsəliːn/, known as petrol /ˈpɛtrəl/ outside North America, is a transparent, petroleum-derived liquid that is used primarily as a fuel in internal combustion engines. It consists mostly of organic compounds obtained by the fractional distillation of petroleum, enhanced with a variety of additives.
