---
version: 1
type: article
id: https://en.wikipedia.org/wiki/P-adic_analysis
offline_file: ""
offline_thumbnail: ""
uuid: f3c324e1-9bb9-45c5-ad90-a1c4c7033216
updated: 1484308654
title: P-adic analysis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-3-adic_integers_with_dual_colorings.svg_0.png
tags:
    - Important results
    - "Ostrowski's theorem"
    - "Mahler's theorem"
    - "Hensel's Lemma"
    - Applications
    - P-adic quantum mechanics
    - Local-Global Principle
    - Further Reading
categories:
    - Number theory
---
The theory of complex-valued numerical functions on the p-adic numbers is part of the theory of locally compact groups. The usual meaning taken for p-adic analysis is the theory of p-adic-valued functions on spaces of interest.
