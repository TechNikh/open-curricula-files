---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/superb
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484556361
title: superb
categories:
    - Dictionary
---
superb
     adj 1: of surpassing excellence; "a brilliant performance"; "a
            superb actor" [syn: {brilliant}]
     2: surpassingly good; "a superb meal"
