---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/great
offline_file: ""
offline_thumbnail: ""
uuid: 4918c9a3-9959-413d-8bd2-ef87c0db70a4
updated: 1484310319
title: great
categories:
    - Dictionary
---
great
     adj 1: relatively large in size or number or extent; larger than
            others of its kind; "a great juicy steak"; "a great
            multitude"; "the great auk"; "a great old oak"; "a
            great ocean liner"; "a great delay"
     2: more than usual; "great expectations"; "great worry"
     3: (used of persons) standing above others in character or
        attainment or reputation; "our distinguished professor";
        "an eminent scholar"; "a great statesman" [syn: {distinguished},
         {eminent}]
     4: of major significance or importance; "a great work of art";
        "Einstein was one of the outstanding figures of the 20th
        century" [syn: ...
