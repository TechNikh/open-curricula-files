---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/awkward
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484453641
title: awkward
categories:
    - Dictionary
---
awkward
     adj 1: causing inconvenience; "they arrived at an awkward time"
     2: lacking grace or skill in manner or movement or performance;
        "an awkward dancer"; "an awkward gesture"; "too awkward
        with a needle to make her own clothes"; "his clumsy
        fingers produced an awkward knot" [ant: {graceful}]
     3: difficult to handle or manage especially because of shape;
        "an awkward bundle to carry"; "a load of bunglesome
        paraphernalia"; "clumsy wooden shoes"; "the cello, a
        rather ungainly instrument for a girl" [syn: {bunglesome},
         {clumsy}, {ungainly}]
     4: not elegant or graceful in expression; "an awkward prose
        style"; "a ...
