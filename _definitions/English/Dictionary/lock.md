---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lock
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484553841
title: lock
categories:
    - Dictionary
---
lock
     n 1: a fastener fitted to a door or drawer to keep it firmly
          closed
     2: a strand or cluster of hair [syn: {curl}, {ringlet}, {whorl}]
     3: a mechanism that detonates the charge of a gun
     4: enclosure consisting of a section of canal that can be
        closed to control the water level; used to raise or lower
        vessels that pass through it [syn: {lock chamber}]
     5: a restraint incorporated into the ignition switch to prevent
        the use of a vehicle by persons who do not have the key
        [syn: {ignition lock}]
     6: any wrestling hold in which some part of the opponent's body
        is twisted or pressured
     v 1: fasten with a lock; ...
