---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Schoof%27s_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 4ebcef6f-c69f-48f6-aa52-a95f4dc4bf1b
updated: 1484308654
title: "Schoof's algorithm"
tags:
    - Introduction
    - "Hasse's theorem"
    - The Frobenius endomorphism
    - Computation modulo primes
    - The algorithm
    - Complexity
    - "Improvements to Schoof's algorithm"
    - Implementations
categories:
    - Number theory
---
Schoof's algorithm is an efficient algorithm to count points on elliptic curves over finite fields. The algorithm has applications in elliptic curve cryptography where it is important to know the number of points to judge the difficulty of solving the discrete logarithm problem in the group of points on an elliptic curve.
