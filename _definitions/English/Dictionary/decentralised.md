---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decentralised
offline_file: ""
offline_thumbnail: ""
uuid: ac221d65-2ad0-4e14-8779-2cfef219492c
updated: 1484310597
title: decentralised
categories:
    - Dictionary
---
decentralised
     adj : withdrawn from a center or place of concentration;
           especially having power or function dispersed from a
           central to local authorities; "a decentralized school
           administration" [syn: {decentralized}] [ant: {centralized}]
