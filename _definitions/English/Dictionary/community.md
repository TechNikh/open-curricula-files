---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/community
offline_file: ""
offline_thumbnail: ""
uuid: 93438a11-1aba-4422-8fe2-f31a44d6f201
updated: 1484310268
title: community
categories:
    - Dictionary
---
community
     n 1: a group of people living in a particular local area; "the
          team is drawn from all parts of the community"
     2: a group of people having ethnic or cultural or religious
        characteristics in common; "the Christian community of the
        apostolic age"; "he was well known throughout the Catholic
        community"
     3: common ownership; "they shared a community of possessions"
     4: a group of nations having common interests; "they hoped to
        join the NATO community"
     5: the body of people in a learned occupation; "the news spread
        rapidly through the medical community" [syn: {profession}]
     6: agreement as to goals; "the ...
