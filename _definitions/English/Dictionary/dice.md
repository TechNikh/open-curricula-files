---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dice
offline_file: ""
offline_thumbnail: ""
uuid: 89e3046e-d77b-46aa-9207-06618e80574c
updated: 1484310154
title: dice
categories:
    - Dictionary
---
dice
     n : small cubes with 1 to 6 spots on the faces; used to generate
         random numbers [syn: {die}]
     v 1: cut into cubes; "cube the cheese" [syn: {cube}]
     2: play dice
