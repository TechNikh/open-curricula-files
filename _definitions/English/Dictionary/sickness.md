---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sickness
offline_file: ""
offline_thumbnail: ""
uuid: 45a091b7-17e5-4724-aa20-3dd6073ebf62
updated: 1484310457
title: sickness
categories:
    - Dictionary
---
sickness
     n 1: impairment of normal physiological function affecting part
          or all of an organism [syn: {illness}, {unwellness}, {malady}]
          [ant: {health}, {health}]
     2: the state that precedes vomiting [syn: {nausea}]
