---
version: 1
type: video
provider: YouTube
id: 7hG7Mbkj2AQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d1bbcc6c-8d26-436f-92ef-08a22d77fd73
updated: 1486069660
title: Amazing Fire Trick!
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/7hG7Mbkj2AQ/default.jpg
    - https://i3.ytimg.com/vi/7hG7Mbkj2AQ/1.jpg
    - https://i3.ytimg.com/vi/7hG7Mbkj2AQ/2.jpg
    - https://i3.ytimg.com/vi/7hG7Mbkj2AQ/3.jpg
---
