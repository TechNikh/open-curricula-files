---
version: 1
type: video
provider: YouTube
id: okNqf2NGnpk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Understanding%20Logical%20Statements%20Commentary.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9b80e580-ccc1-4104-b6a9-5984854d9cb9
updated: 1486069557
title: Understanding Logical Statements Commentary
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/okNqf2NGnpk/default.jpg
    - https://i3.ytimg.com/vi/okNqf2NGnpk/1.jpg
    - https://i3.ytimg.com/vi/okNqf2NGnpk/2.jpg
    - https://i3.ytimg.com/vi/okNqf2NGnpk/3.jpg
---
