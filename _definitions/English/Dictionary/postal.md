---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/postal
offline_file: ""
offline_thumbnail: ""
uuid: f4803f71-8bd3-4512-84b5-506d322e6173
updated: 1484310587
title: postal
categories:
    - Dictionary
---
postal
     adj : of or relating to the system for delivering mail; "postal
           delivery"
