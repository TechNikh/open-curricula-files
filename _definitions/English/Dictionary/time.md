---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/time
offline_file: ""
offline_thumbnail: ""
uuid: 201c3838-f308-4b09-a0ac-5f80d9ebfe1c
updated: 1484310355
title: time
categories:
    - Dictionary
---
time
     n 1: an instance or single occasion for some event; "this time he
          succeeded"; "he called four times"; "he could do ten at
          a clip" [syn: {clip}]
     2: an indefinite period (usually marked by specific attributes
        or activities); "he waited a long time"; "the time of year
        for planting"; "he was a great actor is his time"
     3: a period of time considered as a resource under your control
        and sufficient to accomplish something; "take time to
        smell the roses"; "I didn't have time to finish"; "it took
        more than half my time"
     4: a suitable moment; "it is time to go"
     5: the continuum of experience in which events pass ...
