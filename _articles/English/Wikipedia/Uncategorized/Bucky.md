---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bucky
offline_file: ""
offline_thumbnail: ""
uuid: 886f5b07-a86e-457d-afd0-e060b928092a
updated: 1484308392
title: Bucky
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Bucky.PNG
tags:
    - Publication history
    - Fictional character biography
    - Origin and World War II
    - Winter Soldier
    - The new Captain America
    - Fear Itself and return as Winter Soldier
    - Powers and abilities
    - Other characters called Bucky
    - 'Fred Davis - Late-World War II and post-war Bucky'
    - 'Jack Monroe - 1950s Bucky'
    - Powers and abilities
    - Rick Jones
    - Powers and abilities
    - Lemar Hoskins
    - Powers and abilitites
    - Rikki Barnes
    - Powers and abilities
    - Julia Winters
    - Other versions
    - In other media
    - Television
    - Film
    - Marvel Cinematic Universe
    - Video games
    - Collected editions
    - Reception
categories:
    - Uncategorized
---
Bucky is the name used by several different fictional superheroes appearing in American comic books published by Marvel Comics, usually as a sidekick to Captain America. The original Bucky, James Buchanan "Bucky" Barnes, was created by Joe Simon and Jack Kirby and first appeared in Captain America Comics #1 (cover-dated March 1941), which was published by Marvel's predecessor, Timely Comics.[1] In 2005 the original Bucky was brought back from supposed death as the brain-washed assassin Winter Soldier, and later briefly assumed the role of Captain America when Steve Rogers was presumed to be dead.
