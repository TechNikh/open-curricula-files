---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/niger
offline_file: ""
offline_thumbnail: ""
uuid: f1c94710-0dde-4c45-9dcf-3a82c06b4630
updated: 1484310579
title: niger
categories:
    - Dictionary
---
Niger
     n 1: an African river; flows into the South Atlantic [syn: {Niger
          River}]
     2: a landlocked republic in West Africa; gained independence
        from France in 1960; most of the country is dominated by
        the Sahara Desert [syn: {Republic of Niger}]
