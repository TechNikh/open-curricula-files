---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assuming
offline_file: ""
offline_thumbnail: ""
uuid: 572f77ee-3444-4204-9bdc-e0a1d0c513d6
updated: 1484310297
title: assuming
categories:
    - Dictionary
---
assuming
     adj : excessively forward; "an assumptive person"; "on a subject
           like this it would be too assuming for me to decide";
           "the duchess would not put up with presumptuous
           servants" [syn: {assumptive}, {presumptuous}]
