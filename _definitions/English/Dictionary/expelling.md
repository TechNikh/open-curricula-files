---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expelling
offline_file: ""
offline_thumbnail: ""
uuid: c4ac8765-fdac-4431-8ecf-2b31661f8868
updated: 1484310325
title: expelling
categories:
    - Dictionary
---
expel
     v 1: force to leave or move out; "He was expelled from his native
          country" [syn: {throw out}, {kick out}]
     2: put out or expel from a place; "The child was expelled from
        the classroom" [syn: {eject}, {chuck out}, {exclude}, {throw
        out}, {kick out}, {turf out}, {boot out}, {turn out}]
     3: remove from a position or office; "The chairman was ousted
        after he misappropriated funds" [syn: {oust}, {throw out},
         {drum out}, {boot out}, {kick out}]
     4: cause to flee; "rout out the fighters from their caves"
        [syn: {rout}, {rout out}]
     5: eliminate (substances) from the body [syn: {discharge}, {eject},
         {release}]
    ...
