---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guilty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484626561
title: guilty
categories:
    - Dictionary
---
guilty
     adj 1: responsible for or chargeable with a reprehensible act; or
            marked by guilt; "guilty of murder"; "the guilty
            person"; "secret guilty deeds"; "a guilty conscience";
            "guilty behavior" [ant: {innocent}]
     2: showing a sense of guilt; "a guilty look"; "the hangdog and
        shamefaced air of the retreating enemy"- Eric Linklater
        [syn: {hangdog}, {shamefaced}, {shamed}]
     [also: {guiltiest}, {guiltier}]
