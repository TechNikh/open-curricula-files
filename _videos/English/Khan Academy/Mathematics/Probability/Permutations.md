---
version: 1
type: video
provider: YouTube
id: XqQTXW7XfYA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Permutations.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c808de55-7093-45ee-bd0b-ebce8b377811
updated: 1486069688
title: Permutations
categories:
    - Probability
thumbnail_urls:
    - https://i3.ytimg.com/vi/XqQTXW7XfYA/default.jpg
    - https://i3.ytimg.com/vi/XqQTXW7XfYA/1.jpg
    - https://i3.ytimg.com/vi/XqQTXW7XfYA/2.jpg
    - https://i3.ytimg.com/vi/XqQTXW7XfYA/3.jpg
---
