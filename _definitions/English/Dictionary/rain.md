---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rain
offline_file: ""
offline_thumbnail: ""
uuid: 3838aca4-090b-4f35-b8ad-40f7f36c252b
updated: 1484310273
title: rain
categories:
    - Dictionary
---
rain
     n 1: water falling in drops from vapor condensed in the
          atmosphere [syn: {rainfall}]
     2: drops of fresh water that fall as precipitation from clouds
        [syn: {rainwater}]
     3: anything happening rapidly or in quick successive; "a rain
        of bullets"; "a pelting of insults" [syn: {pelting}]
     v : precipitate as rain; "If it rains much more, we can expect
         some flooding" [syn: {rain down}]
