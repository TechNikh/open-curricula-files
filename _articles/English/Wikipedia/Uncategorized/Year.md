---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Annum
offline_file: ""
offline_thumbnail: ""
uuid: f78c9bd2-50bb-4891-8701-6ad0fa082722
updated: 1484308428
title: Year
tags:
    - Etymology
    - Civil year
    - In international calendars
    - In the Persian calendar
    - Fiscal year
    - Academic year
    - In the International System of Quantities
    - Astronomical years
    - Julian year
    - Sidereal, tropical, and anomalistic years
    - Draconic year
    - Full moon cycle
    - Lunar year
    - Vague year
    - Heliacal year
    - Sothic year
    - Gaussian year
    - Besselian year
    - Variation in the length of the year and the day
    - Numerical value of year variation
    - Summary
    - "Greater" astronomical years
    - Equinoctial cycle
    - Galactic year
    - Seasonal year
    - Symbols
    - Symbol
    - SI prefix multipliers
    - Abbreviations yr and ya
    - Notes
categories:
    - Uncategorized
---
A year is the orbital period of the Earth moving in its orbit around the Sun. Due to the Earth's axial tilt, the course of a year sees the passing of the seasons, marked by changes in weather, the hours of daylight, and, consequently, vegetation and soil fertility. In temperate and subpolar regions around the globe, four seasons are generally recognized: spring, summer, autumn and winter. In tropical and subtropical regions several geographical sectors do not present defined seasons; but in the seasonal tropics, the annual wet and dry seasons are recognized and tracked.
