---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dodecahedral_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 7e62115d-7615-4f80-a0d1-0f566910a140
updated: 1484309114
title: Dodecahedral conjecture
categories:
    - Theorems in geometry
---
László Fejes Tóth, a 20th-century Hungarian geometer, considered the Voronoi decomposition of any given packing of unit spheres. He conjectured in 1943 that the minimal volume of any cell in the resulting Voronoi decomposition was at least as large as the volume of a regular dodecahedron circumscribed to a unit sphere.[1]
