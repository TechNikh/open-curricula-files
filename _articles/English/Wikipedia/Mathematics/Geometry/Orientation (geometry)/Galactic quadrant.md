---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Galactic_quadrant
offline_file: ""
offline_thumbnail: ""
uuid: c24539a6-e614-466e-b196-9ae3f50ae59b
updated: 1484309293
title: Galactic quadrant
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Galactic_longitude.JPG
tags:
    - Quadrants in the galactic coordinate system
    - Delineation
    - Constellations grouped by galactic quadrants
    - Visibility of each quadrant
    - Traditional fourfold divisions of the skies
    - Appearances in fiction
    - Star Trek
categories:
    - Orientation (geometry)
---
