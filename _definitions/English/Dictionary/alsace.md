---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alsace
offline_file: ""
offline_thumbnail: ""
uuid: 3ca42ebb-afcd-491f-8548-c0fd669a2428
updated: 1484310555
title: alsace
categories:
    - Dictionary
---
Alsace
     n : a region of northeastern France famous for its wines [syn: {Alsatia},
          {Elsass}]
