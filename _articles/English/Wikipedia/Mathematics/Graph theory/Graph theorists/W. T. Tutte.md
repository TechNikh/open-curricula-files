---
version: 1
type: article
id: https://en.wikipedia.org/wiki/W._T._Tutte
offline_file: ""
offline_thumbnail: ""
uuid: b17a1948-3e92-4c4c-af6f-8c59d925e705
updated: 1484309476
title: W. T. Tutte
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/340px-SZ42-6-wheels-lightened.jpg
tags:
    - Early life and education
    - Second World War
    - Diagnosing the cipher machine
    - "Tutte's statistical method"
    - Doctorate and career
    - Positions, honours and awards
    - Personal life and death
    - Select publications
    - Books
    - Articles
    - Notes
    - Sources
categories:
    - Graph theorists
---
William Thomas "Bill" Tutte OC FRS FRSC (/tʌt/; May 14, 1917 – May 2, 2002) was a British codebreaker and mathematician. During the Second World War, he made a brilliant and fundamental advance in cryptanalysis of the Lorenz cipher, a major Nazi German cipher system which was used for top-secret communications within the Wehrmacht High Command. The high-level, strategic nature of the intelligence obtained from Tutte's crucial breakthrough, in the bulk decrypting of Lorenz-enciphered messages specifically, contributed greatly, and perhaps even decisively, to the defeat of Nazi Germany.[2][3] He also had a number of significant mathematical accomplishments, including foundation work in the ...
