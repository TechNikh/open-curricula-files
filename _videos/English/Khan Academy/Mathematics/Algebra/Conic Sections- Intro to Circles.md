---
version: 1
type: video
provider: YouTube
id: 6r1GQCxyMKI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Conic%20Sections-%20Intro%20to%20Circles.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4a918c1c-716f-4c75-ac43-d2689332e7d0
updated: 1486069575
title: 'Conic Sections- Intro to Circles'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/6r1GQCxyMKI/default.jpg
    - https://i3.ytimg.com/vi/6r1GQCxyMKI/1.jpg
    - https://i3.ytimg.com/vi/6r1GQCxyMKI/2.jpg
    - https://i3.ytimg.com/vi/6r1GQCxyMKI/3.jpg
---
Introduction to the Circle
