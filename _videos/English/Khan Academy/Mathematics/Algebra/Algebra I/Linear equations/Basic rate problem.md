---
version: 1
type: video
provider: YouTube
id: 7WXijfIZh64
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Basic%20rate%20problem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 66f732ec-f46a-415b-bf78-7c94077a9af5
updated: 1486069557
title: Basic rate problem
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/7WXijfIZh64/default.jpg
    - https://i3.ytimg.com/vi/7WXijfIZh64/1.jpg
    - https://i3.ytimg.com/vi/7WXijfIZh64/2.jpg
    - https://i3.ytimg.com/vi/7WXijfIZh64/3.jpg
---
