---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rank_error-correcting_code
offline_file: ""
offline_thumbnail: ""
uuid: 64054363-99cf-488f-b0ac-2847e4cec7ef
updated: 1484308544
title: Rank error-correcting code
tags:
    - Rank metric
    - Rank code
    - Generating matrix
    - Applications
    - Notes
categories:
    - Coding theory
---
In coding theory, rank codes (also called Gabidulin codes) are non-binary[1] linear error-correcting codes over not Hamming but rank metric. They described a systematic way of building codes that could detect and correct multiple random rank errors. By adding redundancy with coding k-symbol word to a n-symbol word, a rank code can correct any errors of rank up to t = ⌊ (d − 1) / 2 ⌋, where d is a code distance. As an erasure code, it can correct up to d − 1 known erasures.
