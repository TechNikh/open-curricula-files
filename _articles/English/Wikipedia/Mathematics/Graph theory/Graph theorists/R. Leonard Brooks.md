---
version: 1
type: article
id: https://en.wikipedia.org/wiki/R._Leonard_Brooks
offline_file: ""
offline_thumbnail: ""
uuid: 4df22de3-03fe-49b3-b274-2e4a595f5b06
updated: 1484309461
title: R. Leonard Brooks
categories:
    - Graph theorists
---
Rowland Leonard Brooks (February 6, 1916 – June 18, 1993)[1] was an English mathematician, known for proving Brooks' theorem on the relation between the chromatic number and the degree of graphs. He was born in Lincolnshire, England, studied at Trinity College, Cambridge University, and also worked with fellow Trinity students W. T. Tutte, Cedric Smith, and Arthur Harold Stone on the problem of "Squaring the square" (partitioning rectangles and squares into unequal squares), both under their own names and under the pseudonym Blanche Descartes.[2]
