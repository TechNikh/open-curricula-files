---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Krylov%E2%80%93Bogolyubov_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 9a3516fd-5bd7-4f4d-9bdd-937e6da7b1a3
updated: 1484308702
title: Krylov–Bogolyubov theorem
tags:
    - Formulation of the theorems
    - Invariant measures for a single map
    - Invariant measures for a Markov process
    - Notes
categories:
    - Theorems in dynamical systems
---
In mathematics, the Krylov–Bogolyubov theorem (also known as the existence of invariant measures theorem) may refer to either of the two related fundamental theorems within the theory of dynamical systems. The theorems guarantee the existence of invariant measures for certain "nice" maps defined on "nice" spaces and were named after Russian-Ukrainian mathematicians and theoretical physicists Nikolay Krylov and Nikolay Bogolyubov who proved the theorems.[1]
