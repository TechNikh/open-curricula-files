---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Milliken%E2%80%93Taylor_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: c6301b20-7254-4755-87bd-a0c5d07d680a
updated: 1484308661
title: Milliken–Taylor theorem
categories:
    - Theorems in discrete mathematics
---
In mathematics, the Milliken–Taylor theorem in combinatorics is a generalization of both Ramsey's theorem and Hindman's theorem. It is named after Keith Milliken and Alan D. Taylor.
