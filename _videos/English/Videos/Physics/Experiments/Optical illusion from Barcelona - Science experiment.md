---
version: 1
type: video
provider: YouTube
id: IaEzy4bii4w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5417f3ee-763e-47bd-b874-f2ea4f08f69c
updated: 1486069637
title: 'Optical illusion from Barcelona - Science experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/IaEzy4bii4w/default.jpg
    - https://i3.ytimg.com/vi/IaEzy4bii4w/1.jpg
    - https://i3.ytimg.com/vi/IaEzy4bii4w/2.jpg
    - https://i3.ytimg.com/vi/IaEzy4bii4w/3.jpg
---
