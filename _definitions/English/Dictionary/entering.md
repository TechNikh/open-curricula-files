---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entering
offline_file: ""
offline_thumbnail: ""
uuid: 64fddb62-bde4-45b6-95ba-0dc1c3a8aace
updated: 1484310264
title: entering
categories:
    - Dictionary
---
entering
     adj : that is going in; "the entering class"; "the ingoing
           administration"; "ingoing data" [syn: {entering(p)}, {ingoing}]
     n 1: a movement into or inward [syn: {entrance}]
     2: the act of entering; "she made a grand entrance" [syn: {entrance},
         {entry}, {ingress}, {incoming}]
