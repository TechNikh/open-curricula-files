---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tomatoes
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484312281
title: tomatoes
categories:
    - Dictionary
---
tomato
     n 1: mildly acid red or yellow pulpy fruit eaten as a vegetable
     2: native to South America; widely cultivated in many varieties
        [syn: {love apple}, {tomato plant}, {Lycopersicon
        esculentum}]
     [also: {tomatoes} (pl)]
