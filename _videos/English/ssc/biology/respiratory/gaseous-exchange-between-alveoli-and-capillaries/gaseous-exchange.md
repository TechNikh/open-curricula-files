---
title: Gaseous Exchange
youtube_id: AyUtdqiOgCA
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
