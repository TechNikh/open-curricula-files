---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/galvanizing
offline_file: ""
offline_thumbnail: ""
uuid: 9d1dafe7-b89e-4cff-a35a-d3a620623b9a
updated: 1484310377
title: galvanizing
categories:
    - Dictionary
---
galvanizing
     adj : affected by emotion as if by electricity; thrilling; "gave
           an electric reading of the play"; "the new leader had a
           galvanic effect on morale" [syn: {electric}, {galvanic},
            {galvanising}]
