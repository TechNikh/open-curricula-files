---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/restructure
offline_file: ""
offline_thumbnail: ""
uuid: 83afcff2-1c9c-48a0-aaf2-0711fda87528
updated: 1484310603
title: restructure
categories:
    - Dictionary
---
restructure
     v : construct or form anew or provide with a new structure;
         "After his accident, he had to restructure his life";
         "The governing board was reconstituted" [syn: {reconstitute}]
