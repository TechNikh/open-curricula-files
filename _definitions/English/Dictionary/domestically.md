---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/domestically
offline_file: ""
offline_thumbnail: ""
uuid: b85b1028-dfd8-4a66-94fb-704e511f199a
updated: 1484310527
title: domestically
categories:
    - Dictionary
---
domestically
     adv 1: with respect to the internal affairs of a government;
            "domestically, the president proposes a more moderate
            economic policy"
     2: with respect to home or family; "the housewife bored us with
        her domestically limited conversation"
