---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shortwave_radiation
offline_file: ""
offline_thumbnail: ""
uuid: ed158661-c0f2-472e-b755-fb30f8eb24bf
updated: 1484308900
title: Shortwave radiation
tags:
    - Notes
categories:
    - Waves
---
There is no standard cut-off for the near-infrared range; therefore, the shortwave radiation range is also variously defined. It may be broadly defined to include all radiation with a wavelength between 0.1μm and 5.0μm or narrowly defined so as to include only radiation between 0.2μm and 3.0μm.
