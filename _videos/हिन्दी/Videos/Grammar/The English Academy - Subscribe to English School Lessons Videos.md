---
version: 1
type: video
provider: YouTube
id: F0ocqM_ePDU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 294414e5-08df-4482-960d-0ccd1e79845e
updated: 1486069629
title: 'The English Academy - Subscribe to English School Lessons Videos'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/F0ocqM_ePDU/default.jpg
    - https://i3.ytimg.com/vi/F0ocqM_ePDU/1.jpg
    - https://i3.ytimg.com/vi/F0ocqM_ePDU/2.jpg
    - https://i3.ytimg.com/vi/F0ocqM_ePDU/3.jpg
---
