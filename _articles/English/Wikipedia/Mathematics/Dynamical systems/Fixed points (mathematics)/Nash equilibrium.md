---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nash_equilibrium
uuid: 6286a9d4-d529-432d-aa99-0934d89343f5
updated: 1480455181
title: Nash equilibrium
tags:
    - Applications
    - History
    - Definitions
    - Informal definition
    - Formal definition
    - "Nash's Existence Theorem"
    - Examples
    - Coordination game
    - "Prisoner's dilemma"
    - Network traffic
    - Competition game
    - Nash equilibria in a payoff matrix
    - Stability
    - Occurrence
    - Where the conditions are not met
    - Where the conditions are met
    - NE and non-credible threats
    - Proof of existence
    - Proof using the Kakutani fixed point theorem
    - Alternate proof using the Brouwer fixed-point theorem
    - Computing Nash equilibria
    - Examples
    - Notes
    - Game theory textbooks
    - Original Nash papers
    - Other references
categories:
    - Fixed points (mathematics)
---
In game theory, the Nash equilibrium is a solution concept of a non-cooperative game involving two or more players in which each player is assumed to know the equilibrium strategies of the other players, and no player has anything to gain by changing only his or her own strategy.[1] If each player has chosen a strategy and no player can benefit by changing strategies while the other players keep theirs unchanged, then the current set of strategy choices and the corresponding payoffs constitutes a Nash equilibrium. The reality of the Nash equilibrium of a game can be tested using experimental economics methods.
