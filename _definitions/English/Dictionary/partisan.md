---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/partisan
offline_file: ""
offline_thumbnail: ""
uuid: a28bb532-bb90-46bf-9c69-16df01ca51a2
updated: 1484310603
title: partisan
categories:
    - Dictionary
---
partisan
     adj 1: devoted to a cause or party [syn: {partizan}] [ant: {nonpartisan}]
     2: adhering or confined to a particular sect or denomination or
        party; "denominational prejudice" [syn: {denominational}]
     n 1: a fervent and even militant proponent of something [syn: {zealot},
           {drumbeater}]
     2: an ardent and enthusiastic supporter of some person or
        activity [syn: {enthusiast}, {partizan}]
     3: a pike with a long tapering double-edged blade with lateral
        projections; 16th and 17th centuries [syn: {partizan}]
