---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linearity
offline_file: ""
offline_thumbnail: ""
uuid: a6a30eb4-d257-4a75-b5c3-e0566ae5f256
updated: 1484308915
title: Linearity
tags:
    - In mathematics
    - Linear polynomials
    - Boolean functions
    - Physics
    - Electronics
    - Integral linearity
    - Military tactical formations
    - art
    - Music
    - Measurement
categories:
    - Elementary algebra
---
Linearity is the property of a mathematical relationship or function which means that it can be graphically represented as a straight line, that is, that one quantity is simply proportional to another. Examples are the relationship of voltage and current across a resistor (Ohm's law), or the mass and weight of an object.
