---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/notified
offline_file: ""
offline_thumbnail: ""
uuid: 75777709-912d-4841-adec-f3ea799ab965
updated: 1484310477
title: notified
categories:
    - Dictionary
---
notify
     v : give information or notice to; "I advised him that the rent
         was due" [syn: {advise}, {give notice}, {send word}, {apprise},
          {apprize}]
     [also: {notified}]
