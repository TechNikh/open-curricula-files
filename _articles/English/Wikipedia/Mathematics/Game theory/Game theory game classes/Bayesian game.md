---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bayesian_game
offline_file: ""
offline_thumbnail: ""
uuid: baf41675-089c-424f-acc2-4986db9d7afd
updated: 1484309035
title: Bayesian game
tags:
    - Specification of games
    - Signaling
    - Bayesian Nash equilibrium
    - Perfect Bayesian equilibrium
    - Belief systems
    - Sequential rationality
    - Definition
    - Examples
    - "Sheriff's Dilemma"
categories:
    - Game theory game classes
---
In game theory, a Bayesian game is a game in which the players do not have complete information on the other players (e.g. on their available strategies or payoffs), but, they have beliefs with known probability distribution.
