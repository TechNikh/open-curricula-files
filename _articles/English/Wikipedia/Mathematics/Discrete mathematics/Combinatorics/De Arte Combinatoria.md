---
version: 1
type: article
id: https://en.wikipedia.org/wiki/De_Arte_Combinatoria
offline_file: ""
offline_thumbnail: ""
uuid: 357078c4-94db-4633-8fca-bfaa7200c323
updated: 1484308567
title: De Arte Combinatoria
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Fotothek_df_tg_0005486_Mathematik_%255E_Kombinatorik.jpg'
tags:
    - Summary
    - Notes
categories:
    - Combinatorics
---
The Dissertatio de arte combinatoria ("Dissertation on the Art of Combinations" or "On the Combinatorial Art") is an early work by Gottfried Leibniz published in 1666 in Leipzig.[1] It is an extended version of his first doctoral dissertation,[2] written before the author had seriously undertaken the study of mathematics.[3] The booklet was reissued without Leibniz' consent in 1690, which prompted him to publish a brief explanatory notice in the Acta Eruditorum.[4] During the following years he repeatedly expressed regrets about its being circulated as he considered it immature.[5] Nevertheless it was a very original work and it provided the author the first glimpse of fame among the ...
