---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carboxyl
offline_file: ""
offline_thumbnail: ""
uuid: 75a4173c-5b4f-4c60-85ef-23500a46c7f0
updated: 1484310419
title: carboxyl
categories:
    - Dictionary
---
carboxyl
     adj : relating to or containing the carboxyl group or carboxyl
           radical [syn: {carboxylic}]
     n : the univalent radical -COOH; present in and characteristic
         of organic acids [syn: {carboxyl group}]
