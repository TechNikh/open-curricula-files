---
version: 1
type: video
provider: YouTube
id: oHDRIwRZRVI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 12dc3f09-b219-4e8c-a513-c10e6178331b
updated: 1486069666
title: Muscle Myosin
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/oHDRIwRZRVI/default.jpg
    - https://i3.ytimg.com/vi/oHDRIwRZRVI/1.jpg
    - https://i3.ytimg.com/vi/oHDRIwRZRVI/2.jpg
    - https://i3.ytimg.com/vi/oHDRIwRZRVI/3.jpg
---
