---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Birch_and_Swinnerton-Dyer_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 8bf254a1-9371-4e17-849f-2439a04f1b0e
updated: 1484308632
title: Birch and Swinnerton-Dyer conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-BSD_data_plot_for_elliptic_curve_800h1.svg.png
tags:
    - Background
    - History
    - Current status
    - Consequences
    - Notes
categories:
    - Number theory
---
In mathematics, the Birch and Swinnerton-Dyer conjecture describes the set of rational solutions to equations defining an elliptic curve. It is an open problem in the field of number theory and is widely recognized as one of the most challenging mathematical problems. The conjecture was chosen as one of the seven Millennium Prize Problems listed by the Clay Mathematics Institute, which has offered a $1,000,000 prize for the first correct proof.[1] It is named after mathematicians Bryan Birch and Peter Swinnerton-Dyer who developed the conjecture during the first half of the 1960s with the help of machine computation. As of 2016[update], only special cases of the conjecture have been proved.
