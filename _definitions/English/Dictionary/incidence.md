---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incidence
offline_file: ""
offline_thumbnail: ""
uuid: 794b61c0-4aa1-4f9b-b15a-32bee34cb81d
updated: 1484310221
title: incidence
categories:
    - Dictionary
---
incidence
     n 1: the relative frequency of occurrence of something [syn: {relative
          incidence}]
     2: the striking of a light beam on a surface; "he measured the
        angle of incidence of the reflected light"
