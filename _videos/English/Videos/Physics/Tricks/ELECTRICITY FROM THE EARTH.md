---
version: 1
type: video
provider: YouTube
id: JaLcyFg3BXA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c72498cd-d6e8-458e-801a-3d28ecef9491
updated: 1486069658
title: ELECTRICITY FROM THE EARTH
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/JaLcyFg3BXA/default.jpg
    - https://i3.ytimg.com/vi/JaLcyFg3BXA/1.jpg
    - https://i3.ytimg.com/vi/JaLcyFg3BXA/2.jpg
    - https://i3.ytimg.com/vi/JaLcyFg3BXA/3.jpg
---
