---
version: 1
type: video
provider: YouTube
id: 8_2rEK8JAsM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 687f39ae-113f-45db-84a0-e74ca921e7a6
updated: 1486069605
title: "Average height of a building's floor"
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/8_2rEK8JAsM/default.jpg
    - https://i3.ytimg.com/vi/8_2rEK8JAsM/1.jpg
    - https://i3.ytimg.com/vi/8_2rEK8JAsM/2.jpg
    - https://i3.ytimg.com/vi/8_2rEK8JAsM/3.jpg
---
