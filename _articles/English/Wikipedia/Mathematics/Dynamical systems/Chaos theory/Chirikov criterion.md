---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chirikov_criterion
offline_file: ""
offline_thumbnail: ""
uuid: 98c0fb29-7ded-49d5-a3ba-0dec60a1259a
updated: 1484308705
title: Chirikov criterion
tags:
    - Description
categories:
    - Chaos theory
---
The Chirikov criterion or Chirikov resonance-overlap criterion was established by the Russian physicist Boris Chirikov. Back in 1959, he published a seminal article (Atom. Energ. 6: 630 (1959)), where he introduced the very first physical criterion for the onset of chaotic motion in deterministic Hamiltonian systems. He then applied such a criterion to explain puzzling experimental results on plasma confinement in magnetic bottles obtained by Rodionov at the Kurchatov Institute. As in an old oriental tale, Boris Chirikov opened such a bottle, and freed the genie of chaos, which spread the world over.
