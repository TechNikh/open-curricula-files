---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commitment
offline_file: ""
offline_thumbnail: ""
uuid: c4f2b681-bec1-446a-a45c-040a85b90103
updated: 1484310577
title: commitment
categories:
    - Dictionary
---
commitment
     n 1: the trait of sincere and steadfast fixity of purpose; "a man
          of energy and commitment" [syn: {committedness}]
     2: the act of binding yourself (intellectually or emotionally)
        to a course of action; "his long commitment to public
        service"; "they felt no loyalty to a losing team" [syn: {allegiance},
         {loyalty}, {dedication}]
     3: an engagement by contract involving financial obligation;
        "his business commitments took him to London"
     4: a message that makes a pledge [syn: {dedication}]
     5: the official act of consigning a person to confinement (as
        in a prison or mental hospital) [syn: {committal}, ...
