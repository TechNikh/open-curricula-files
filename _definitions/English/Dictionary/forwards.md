---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forwards
offline_file: ""
offline_thumbnail: ""
uuid: 64d716ba-2d78-427d-981a-73c4aade2d8d
updated: 1484310277
title: forwards
categories:
    - Dictionary
---
forwards
     adv 1: at or to or toward the front; "he faced forward"; "step
            forward"; "she practiced sewing backward as well as
            frontward on her new sewing machine"; (`forrad' and
            `forrard' are dialectal variations) [syn: {forward}, {frontward},
             {frontwards}, {forrad}, {forrard}] [ant: {back}]
     2: in a forward direction; "go ahead"; "the train moved ahead
        slowly"; "the boat lurched ahead"; "moved onward into the
        forest"; "they went slowly forward in the mud" [syn: {ahead},
         {onward}, {onwards}, {forward}, {forrader}]
