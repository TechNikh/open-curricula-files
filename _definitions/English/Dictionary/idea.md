---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/idea
offline_file: ""
offline_thumbnail: ""
uuid: e3f06114-e82c-471f-a8b1-5f4b29eea3fd
updated: 1484310295
title: idea
categories:
    - Dictionary
---
idea
     n 1: the content of cognition; the main thing you are thinking
          about; "it was not a good idea"; "the thought never
          entered my mind" [syn: {thought}]
     2: a personal view; "he has an idea that we don't like him"
     3: an approximate calculation of quantity or degree or worth;
        "an estimate of what it would cost"; "a rough idea how
        long it would take" [syn: {estimate}, {estimation}, {approximation}]
     4: your intention; what you intend to do; "he had in mind to
        see his old teacher"; "the idea of the game is to capture
        all the pieces" [syn: {mind}]
     5: (music) melodic subject of a musical composition; "the theme
        ...
