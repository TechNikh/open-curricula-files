---
version: 1
type: video
provider: YouTube
id: W0AVhZ0Vcos
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fcfe2157-3559-4174-adc0-ca18df8a2338
updated: 1486069637
title: Back EMF Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/W0AVhZ0Vcos/default.jpg
    - https://i3.ytimg.com/vi/W0AVhZ0Vcos/1.jpg
    - https://i3.ytimg.com/vi/W0AVhZ0Vcos/2.jpg
    - https://i3.ytimg.com/vi/W0AVhZ0Vcos/3.jpg
---
