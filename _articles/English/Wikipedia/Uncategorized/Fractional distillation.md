---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fractional_distillation
offline_file: ""
offline_thumbnail: ""
uuid: 3ebe93fb-41f1-4062-b90e-35a48e9c884d
updated: 1484308375
title: Fractional distillation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Fractional_distillation_lab_apparatus.svg.png
tags:
    - Laboratory setup
    - Apparatus
    - Discussion
    - Industrial distillation
    - Design of industrial distillation columns
categories:
    - Uncategorized
---
Fractional distillation is the separation of a mixture into its component parts, or fractions, separating chemical compounds by their boiling point by heating them to a temperature at which one or more fractions of the compound will vaporize. It uses distillation to fractionate. Generally the component parts have boiling points that differ by less than 25 °C from each other under a pressure of one atmosphere. If the difference in boiling points is greater than 25 °C, a simple distillation is typically used.
