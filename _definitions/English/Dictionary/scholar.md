---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scholar
offline_file: ""
offline_thumbnail: ""
uuid: f1202d4b-2451-4ac2-92a1-9693f9d61036
updated: 1484310146
title: scholar
categories:
    - Dictionary
---
scholar
     n 1: a learned person (especially in the humanities); someone who
          by long study has gained mastery in one or more
          disciplines [syn: {scholarly person}, {student}]
     2: someone (especially a child) who learns (as from a teacher)
        or takes up knowledge or beliefs [syn: {learner}, {assimilator}]
     3: a student who holds a scholarship
