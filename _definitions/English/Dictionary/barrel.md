---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/barrel
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484367661
title: barrel
categories:
    - Dictionary
---
barrel
     n 1: a tube through which a bullet travels when a gun is fired
          [syn: {gun barrel}]
     2: a cylindrical container that holds liquids [syn: {cask}]
     3: a bulging cylindrical shape; hollow with flat ends [syn: {drum}]
     4: the quantity that a barrel (of any size) will hold [syn: {barrelful}]
     5: any of various units of capacity; "a barrel of beer is 31
        gallons and a barrel of oil is 42 gallons" [syn: {bbl}]
     v : put in barrels
     [also: {barrelling}, {barrelled}]
