---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scene
offline_file: ""
offline_thumbnail: ""
uuid: 461781ce-70fb-412a-a2d8-c21c1ce1ca25
updated: 1484310525
title: scene
categories:
    - Dictionary
---
scene
     n 1: the place where some action occurs; "the police returned to
          the scene of the crime"
     2: an incident (real or imaginary); "their parting was a sad
        scene"
     3: the visual percept of a region; "the most desirable feature
        of the park are the beautiful views" [syn: {view}, {aspect},
         {prospect}, {vista}, {panorama}]
     4: a consecutive series of pictures that constitutes a unit of
        action in a film [syn: {shot}]
     5: a situation treated as an observable object; "the political
        picture is favorable"; "the religious scene in England has
        changed in the last century" [syn: {picture}]
     6: a subdivision of an act ...
