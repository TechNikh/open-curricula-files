---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Virtual_knot
offline_file: ""
offline_thumbnail: ""
uuid: 972722fc-9aef-4c57-8727-be0c2a03f0ce
updated: 1484308595
title: Virtual knot
tags:
    - Overview
categories:
    - Combinatorics
---
In knot theory, a virtual knot is a generalization of the classical idea of knots in several ways that are all equivalent, introduced by Kauffman (1999).
