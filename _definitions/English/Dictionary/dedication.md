---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dedication
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484635561
title: dedication
categories:
    - Dictionary
---
dedication
     n 1: complete and wholehearted fidelity
     2: a ceremony in which something (as a building) is dedicated
        to some goal or purpose
     3: a message that makes a pledge [syn: {commitment}]
     4: a short message (as in a book or musical work or on a
        photograph) dedicating it to someone or something [syn: {inscription}]
     5: the act of binding yourself (intellectually or emotionally)
        to a course of action; "his long commitment to public
        service"; "they felt no loyalty to a losing team" [syn: {commitment},
         {allegiance}, {loyalty}]
