---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pea
offline_file: ""
offline_thumbnail: ""
uuid: 80fc305a-e2bd-4b36-909a-e77baad19145
updated: 1484310305
title: pea
categories:
    - Dictionary
---
pea
     n 1: seed of a pea plant
     2: the fruit or seed of a pea plant
     3: a leguminous plant of the genus Pisum with small white
        flowers and long green pods containing edible green seeds
        [syn: {pea plant}]
     [also: {pease} (pl)]
