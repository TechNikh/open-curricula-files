---
version: 1
type: video
provider: YouTube
id: wToSIQJ2o_8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Parametric%20equations%20and%20polar%20coordinates/Parametric%20equations%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 028b5eaa-aabe-4354-a419-79005bb25d33
updated: 1486069694
title: Parametric equations 2
categories:
    - Parametric equations and polar coordinates
thumbnail_urls:
    - https://i3.ytimg.com/vi/wToSIQJ2o_8/default.jpg
    - https://i3.ytimg.com/vi/wToSIQJ2o_8/1.jpg
    - https://i3.ytimg.com/vi/wToSIQJ2o_8/2.jpg
    - https://i3.ytimg.com/vi/wToSIQJ2o_8/3.jpg
---
