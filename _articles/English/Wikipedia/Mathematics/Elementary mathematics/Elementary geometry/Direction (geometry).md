---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Direction_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 08a60495-6db2-479c-81ef-71dae53349db
updated: 1484308927
title: Direction (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sanas.jpg
categories:
    - Elementary geometry
---
Direction is the information contained in the relative position of one point with respect to another point without the distance information. Directions may be either relative to some indicated reference (the violins in a full orchestra are typically seated to the left of the conductor), or absolute according to some previously agreed upon frame of reference (New York City lies due west of Madrid). Direction is often indicated manually by an extended index finger or written as an arrow. On a vertically oriented sign representing a horizontal plane, such as a road sign, "forward" is usually indicated by an upward arrow. Mathematically, direction may be uniquely specified by a unit vector, or ...
