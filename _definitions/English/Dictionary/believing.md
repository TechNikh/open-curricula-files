---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/believing
offline_file: ""
offline_thumbnail: ""
uuid: db0f5558-a417-436d-80c9-917ccc5d2833
updated: 1484310144
title: believing
categories:
    - Dictionary
---
believing
     n : the cognitive process that leads to convictions; "seeing is
         believing"
