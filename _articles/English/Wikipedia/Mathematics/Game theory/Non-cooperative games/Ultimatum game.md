---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ultimatum_game
offline_file: ""
offline_thumbnail: ""
uuid: 3c4c4c9f-7448-41f1-a771-61378dc70040
updated: 1484309059
title: Ultimatum game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Ultimatum_Game_Extensive_Form.svg.png
tags:
    - Equilibrium analysis
    - Experimental results
    - Explanations
    - Neurological explanations
    - Evolutionary game theory
    - Sociological applications
    - History
    - Variants
    - Notes
categories:
    - Non-cooperative games
---
The ultimatum game is a game in economic experiments. The first player (the proposer) receives a sum of money and proposes how to divide the sum between the proposer and the other player. The second player (the responder) chooses to either accept or reject this proposal. If the second player accepts, the money is split according to the proposal. If the second player rejects, neither player receives any money. The game is typically played only once so that reciprocation is not an issue.
