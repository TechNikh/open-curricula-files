---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Broadleaf
offline_file: ""
offline_thumbnail: ""
uuid: e5573749-23b8-4b91-8d59-59bfbc08340d
updated: 1484308415
title: Flowering plant
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rose_bud.jpg
tags:
    - Angiosperm derived characteristics
    - Evolution
    - Classification
    - APG IV classification
    - History of classification
    - Flowering plant diversity
    - Vascular anatomy
    - The flower, fruit, and seed
    - Flowers
    - Fertilization and embryogenesis
    - Fruit and seed
    - Economic importance
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
The flowering plants (angiosperms), also known as Angiospermae[4][5] or Magnoliophyta,[6] are the most diverse group of land plants, with 416 families, approx. 13,164 known genera and a total of c. 295,383 known species.[7] Like gymnosperms, angiosperms are seed-producing plants; they are distinguished from gymnosperms by characteristics including flowers, endosperm within the seeds, and the production of fruits that contain the seeds. Etymologically, angiosperm means a plant that produces seeds within an enclosure, in other words, a fruiting plant. The term "angiosperm" comes from the Greek composite word (angeion, "case" or "casing", and sperma, "seed") meaning "enclosed seeds", after the ...
