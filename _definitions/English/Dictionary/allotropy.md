---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/allotropy
offline_file: ""
offline_thumbnail: ""
uuid: 9bcd2862-4b0e-4dc5-9feb-7740bea7dd8c
updated: 1484310414
title: allotropy
categories:
    - Dictionary
---
allotropy
     n : the phenomenon of an element existing in two or more
         physical forms [syn: {allotropism}]
