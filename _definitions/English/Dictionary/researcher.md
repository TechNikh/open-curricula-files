---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/researcher
offline_file: ""
offline_thumbnail: ""
uuid: 794fcf95-c959-4873-b77b-b7a60e72ce40
updated: 1484310277
title: researcher
categories:
    - Dictionary
---
researcher
     n : a scientist who devotes himself to doing research [syn: {research
         worker}, {investigator}]
