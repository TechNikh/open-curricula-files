---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oskar_Morgenstern
offline_file: ""
offline_thumbnail: ""
uuid: 6dde71c1-9667-4f60-acc6-8512c62c6cd9
updated: 1484308504
title: Oskar Morgenstern
tags:
    - Biography
    - Bibliography
    - Sources
categories:
    - Game theorists
---
Oskar Morgenstern (January 24, 1902 – July 26, 1977) was a German-born economist. In collaboration with mathematician John von Neumann, he founded the mathematical field of game theory and its application to economics (see von Neumann–Morgenstern utility theorem).[1][2]
