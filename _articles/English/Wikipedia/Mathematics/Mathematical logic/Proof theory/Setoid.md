---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Setoid
offline_file: ""
offline_thumbnail: ""
uuid: cc9f72b2-9c77-43bc-9bf5-761528eebe18
updated: 1484309499
title: Setoid
tags:
    - Proof theory
    - Type theory
    - Constructive mathematics
    - Notes
categories:
    - Proof theory
---
Setoids are studied especially in proof theory and in type-theoretic foundations of mathematics. Often in mathematics, when one defines an equivalence relation on a set, one immediately forms the quotient set (turning equivalence into equality). In contrast, setoids may be used when a difference between identity and equivalence must be maintained, often with an interpretation of intensional equality (the equality on the original set) and extensional equality (the equivalence relation, or the equality on the quotient set).
