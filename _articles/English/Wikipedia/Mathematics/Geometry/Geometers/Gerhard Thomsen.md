---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gerhard_Thomsen
offline_file: ""
offline_thumbnail: ""
uuid: 2cdc9a88-8559-4d20-8434-5bd297c89120
updated: 1484309103
title: Gerhard Thomsen
categories:
    - Geometers
---
Thomsen was born on 23 June 1899 in Hamburg. His father, Georg Thomsen, was a physician. Thomsen grew up in Hamburg and attended the Johanneum (gymnasium/highschool) from 1908 to 1917. After completing school he served in the army during the last year of World War I. In 1919 he became of the first students at the newly founded University of Hamburg majoring in mathematics and natural science. Aside from a temporary interlude Thomsen studied in Hamburg until 1923. He received a certification to teach at highschools the fall of 1922 and finally his PhD in the summer of the following year. After he worked shortly as an assistant at the Karlsruhe Institute of Technology before returning to ...
