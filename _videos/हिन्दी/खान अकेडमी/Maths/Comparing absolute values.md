---
version: 1
type: video
provider: YouTube
id: 5DuqY1l-8d8
offline_file: ""
offline_thumbnail: ""
uuid: 60522927-1706-4477-8576-11647109a304
updated: 1484040352
title: Comparing absolute values
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/5DuqY1l-8d8/default.jpg
    - https://i3.ytimg.com/vi/5DuqY1l-8d8/1.jpg
    - https://i3.ytimg.com/vi/5DuqY1l-8d8/2.jpg
    - https://i3.ytimg.com/vi/5DuqY1l-8d8/3.jpg
---
