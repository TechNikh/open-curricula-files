---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/active
offline_file: ""
offline_thumbnail: ""
uuid: 90a6f8d3-393e-43a9-af18-88f49c1a0a6c
updated: 1484310375
title: active
categories:
    - Dictionary
---
active
     adj 1: tending to become more severe or wider in scope; "active
            tuberculosis" [ant: {inactive}]
     2: engaged in or ready for military or naval operations; "on
        active duty"; "the platoon is combat-ready"; "review the
        fighting forces" [syn: {combat-ready}, {fighting(a)}]
     3: disposed to take action or effectuate change; "a director
        who takes an active interest in corporate operations"; "an
        active antagonism"; "he was active in drawing attention to
        their grievances" [ant: {passive}]
     4: taking part in an activity; "an active member of the club";
        "he was politically active"; "the participating
        ...
