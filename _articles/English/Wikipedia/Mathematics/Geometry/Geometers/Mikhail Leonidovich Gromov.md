---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mikhail_Leonidovich_Gromov
offline_file: ""
offline_thumbnail: ""
uuid: eecc0c6b-d590-43b1-b2eb-8636c38df7cd
updated: 1484309083
title: Mikhail Leonidovich Gromov
tags:
    - Biography
    - Work
    - Prizes and honors
    - Prizes
    - Honors
    - Books and other publications
    - Notes
categories:
    - Geometers
---
Mikhail Leonidovich Gromov (also Mikhael Gromov, Michael Gromov or Mischa Gromov; Russian: Михаи́л Леони́дович Гро́мов; born 23 December 1943), is a French-Russian mathematician known for important contributions in many different areas of mathematics, including geometry, analysis and group theory. He is a permanent member of IHÉS in France and a Professor of Mathematics at New York University.
