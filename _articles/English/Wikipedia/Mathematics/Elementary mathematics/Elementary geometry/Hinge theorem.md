---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hinge_theorem
offline_file: ""
offline_thumbnail: ""
uuid: b9dfe02f-df9c-4d28-a0c5-6f72b227c362
updated: 1484308932
title: Hinge theorem
categories:
    - Elementary geometry
---
In geometry, the hinge theorem states that if two sides of one triangle are congruent to two sides of another triangle, and the included angle of the first is larger than the included angle of the second, then the third side of the first triangle is longer than the third side of the second triangle. This theorem is actually Propositions 24 of Book 1 of Euclid's Elements (sometimes called the open mouth theorem). The theorem states the following:
