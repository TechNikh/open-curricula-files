---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hindi
offline_file: ""
offline_thumbnail: ""
uuid: e4796b74-6e2f-4a56-8a94-b5631193bf2a
updated: 1484308466
title: Hindi
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Hindi_devnagari.png
tags:
    - Official status
    - Outside India
    - History
    - Multilingualism
    - Script
    - vocabulary
    - Sanskrit
    - Persian and Arabic
    - Media
    - Literature
categories:
    - Uncategorized
---
Hindi (Devanagari: हिन्दी, IAST: Hindī), is an Indo-Aryan language, one of the official languages of India, and the first language of the majority of the populace in the Hindi Belt.[11] Outside India, Hindi is an official language in Fiji,[12] and is spoken in significant numbers in Mauritius, South Africa, Suriname, Guyana, and Trinidad and Tobago.[13][14][15][14] Modern Standard Hindi is the standardised and Sanskritised register of Hindi.
