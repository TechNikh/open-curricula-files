---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gamma
offline_file: ""
offline_thumbnail: ""
uuid: 4944e000-4a55-4871-ab94-a53ee969020c
updated: 1484310389
title: gamma
categories:
    - Dictionary
---
gamma
     n 1: the 3rd letter of the Greek alphabet
     2: a unit of magnetic field strength equal to
        one-hundred-thousandth of an oersted
     3: Portuguese navigator who led an expedition around the Cape
        of Good Hope in 1497; he sighted and named Natal on
        Christmas Day before crossing the Indian Ocean (1469-1524)
        [syn: {da Gamma}, {Vasco da Gamma}]
