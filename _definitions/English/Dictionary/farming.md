---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/farming
offline_file: ""
offline_thumbnail: ""
uuid: 53a8d1ad-dc74-4dd3-9de0-d8b158055e52
updated: 1484310260
title: farming
categories:
    - Dictionary
---
farming
     adj : relating to rural matters; "an agrarian (or agricultural)
           society"; "farming communities" [syn: {agrarian}, {agricultural},
            {farming(a)}]
     n 1: the practice of cultivating the land or raising stock [syn:
          {agriculture}, {husbandry}]
     2: working the land as an occupation or way of life; "farming
        is a strenuous life"; "there's no work on the land any
        more" [syn: {land}]
