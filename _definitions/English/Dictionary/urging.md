---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urging
offline_file: ""
offline_thumbnail: ""
uuid: d5767862-6928-4452-9ec1-51df57e98f6c
updated: 1484310583
title: urging
categories:
    - Dictionary
---
urging
     n 1: a verbalization that encourages you to attempt something;
          "the ceaseless prodding got on his nerves" [syn: {goad},
           {goading}, {prod}, {prodding}, {spur}, {spurring}]
     2: the act of earnestly supporting or encouraging
     3: insistent solicitation and entreaty; "his importunity left
        me no alternative but to agree" [syn: {importunity}, {urgency}]
