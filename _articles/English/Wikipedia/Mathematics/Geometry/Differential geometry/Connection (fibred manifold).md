---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Connection_(fibred_manifold)
offline_file: ""
offline_thumbnail: ""
uuid: 9bb11bcb-b92e-4c35-9659-f6d6ffeec5df
updated: 1484309209
title: Connection (fibred manifold)
tags:
    - Formal definition
    - Connection as a horizontal splitting
    - Connection as a tangent-valued form
    - Connection as a vertical-valued form
    - Connection as a jet bundle section
    - Curvature and torsion
    - Bundle of principal connections
    - Notes
categories:
    - Differential geometry
---
In differential geometry, a fibered manifold is surjective submersion of smooth manifolds 
  
    
      
        Y
        →
        X
      
    
    {\displaystyle Y\to X}
  
. Locally trivial fibered manifolds are fiber bundles. Therefore, a notion of connection on fibered manifolds provides a general framework of a connection on fiber bundles.
