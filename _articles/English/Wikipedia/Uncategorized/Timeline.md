---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Timeline
offline_file: ""
offline_thumbnail: ""
uuid: cc4095bb-a3f8-4ead-b44e-81fafa4701ff
updated: 1484308473
title: Timeline
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-A_New_Chart_of_History_color.jpg
tags:
    - Overview
    - Uses of timelines
    - In historical studies
    - In natural sciences
    - In project management
categories:
    - Uncategorized
---
A timeline is a way of displaying a list of events in chronological order, sometimes described as a project artifact.[1] It is typically a graphic design showing a long bar labelled with dates alongside itself and usually events
