---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bow_tie_(biology)
offline_file: ""
offline_thumbnail: ""
uuid: 04f448fb-b3dd-4627-8d38-17e73b2faa99
updated: 1484308813
title: Bow tie (biology)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-General_bowtie_architecture.png
categories:
    - Self-organization
---
In the biological sciences, the term bow tie (so called for its shape) is a recent concept that tries to grasp the essence of some operational and functional structures observed in biological organisms and other kinds of complex and self-organizing systems. In general, bow tie architectures refer to ordered and recurrent structures that often underlie complex technological or biological systems, and that are capable of conferring them a balance among efficiency, robustness and evolvability. In other words, bow ties are able to take into account a great diversity of inputs (fanning in to the knot), showing a much smaller diversity in the protocols and processes (the knot) able to elaborate ...
