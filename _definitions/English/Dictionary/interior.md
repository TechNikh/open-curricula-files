---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interior
offline_file: ""
offline_thumbnail: ""
uuid: 429ddb21-c29c-4149-bf91-4be3bf15d922
updated: 1484310214
title: interior
categories:
    - Dictionary
---
interior
     adj 1: situated within or suitable for inside a building; "an
            interior scene"; "interior decoration"; "an interior
            bathroom without windows" [ant: {exterior}]
     2: inside the country; "the British Home Office has broader
        responsibilities than the United States Department of the
        Interior"; "the nation's internal politics" [syn: {home(a)},
         {interior(a)}, {internal}, {national}]
     3: located inward; "Beethoven's manuscript looks like a bloody
        record of a tremendous inner battle"- Leonard Bernstein;
        "she thinks she has no soul, no interior life, but the
        truth is that she has no access to it"- David ...
