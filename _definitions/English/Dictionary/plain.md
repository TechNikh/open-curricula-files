---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plain
offline_file: ""
offline_thumbnail: ""
uuid: b977bdf9-7a0e-49d2-9343-b82b4c3f585d
updated: 1484310433
title: plain
categories:
    - Dictionary
---
plain
     adj 1: clearly apparent or obvious to the mind or senses; "the
            effects of the drought are apparent to anyone who sees
            the parched fields"; "evident hostility"; "manifest
            disapproval"; "patent advantages"; "made his meaning
            plain"; "it is plain that he is no reactionary"; "in
            plain view" [syn: {apparent}, {evident}, {manifest}, {patent}]
     2: not elaborate or elaborated; simple; "plain food"; "stuck to
        the plain facts"; "a plain blue suit"; "a plain
        rectangular brick building" [ant: {fancy}]
     3: lacking patterns especially in color [syn: {unpatterned}]
        [ant: {patterned}]
     4: not mixed ...
