---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phosphorous
offline_file: ""
offline_thumbnail: ""
uuid: d11afb89-c424-4e4d-b89c-81485b537b30
updated: 1484310403
title: phosphorous
categories:
    - Dictionary
---
phosphorous
     adj : containing or characteristic of phosphorus; "phosphoric
           acid" [syn: {phosphoric}]
