---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/essence
offline_file: ""
offline_thumbnail: ""
uuid: 355fc9e0-f669-4812-9126-9c3f5c2bb8a9
updated: 1484310379
title: essence
categories:
    - Dictionary
---
essence
     n 1: the choicest or most essential or most vital part of some
          idea or experience; "the gist of the prosecutor's
          argument"; "the heart and soul of the Republican Party";
          "the nub of the story" [syn: {kernel}, {substance}, {core},
           {center}, {gist}, {heart}, {heart and soul}, {inwardness},
           {marrow}, {meat}, {nub}, {pith}, {sum}, {nitty-gritty}]
     2: any substance possessing to a high degree the predominant
        properties of a plant or drug or other natural product
        from which it is extracted
     3: the central meaning or theme of a speech or literary work
        [syn: {effect}, {burden}, {core}, {gist}]
     4: a ...
