---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pob
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484379241
title: pob
categories:
    - Dictionary
---
POB
     n : a numbered compartment in a post office where mail is put to
         be called for [syn: {post-office box}, {call box}, {letter
         box}]
