---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geometrically
offline_file: ""
offline_thumbnail: ""
uuid: 78d12db3-6b70-4e61-9b3c-6358257a5baa
updated: 1484310138
title: geometrically
categories:
    - Dictionary
---
geometrically
     adv 1: with respect to geometry; "this shape is geometrically
            interesting"
     2: in a geometric fashion; "it grew geometrically" [ant: {linearly},
         {linearly}]
