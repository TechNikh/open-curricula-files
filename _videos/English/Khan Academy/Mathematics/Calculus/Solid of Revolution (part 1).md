---
version: 1
type: video
provider: YouTube
id: R_aqSL-q6_8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Solid%20of%20Revolution%20%28part%201%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5e1c7d66-24b8-4925-adf5-fb4ecabdcf04
updated: 1486069693
title: Solid of Revolution (part 1)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/R_aqSL-q6_8/default.jpg
    - https://i3.ytimg.com/vi/R_aqSL-q6_8/1.jpg
    - https://i3.ytimg.com/vi/R_aqSL-q6_8/2.jpg
    - https://i3.ytimg.com/vi/R_aqSL-q6_8/3.jpg
---
