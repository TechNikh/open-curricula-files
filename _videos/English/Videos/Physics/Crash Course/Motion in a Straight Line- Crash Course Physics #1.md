---
version: 1
type: video
provider: YouTube
id: ZM8ECpBuQYE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9f01e1e0-771b-4bec-a6a7-23352e19f747
updated: 1486069682
title: 'Motion in a Straight Line- Crash Course Physics #1'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZM8ECpBuQYE/default.jpg
    - https://i3.ytimg.com/vi/ZM8ECpBuQYE/1.jpg
    - https://i3.ytimg.com/vi/ZM8ECpBuQYE/2.jpg
    - https://i3.ytimg.com/vi/ZM8ECpBuQYE/3.jpg
---
