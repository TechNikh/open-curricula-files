---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/highly
offline_file: ""
offline_thumbnail: ""
uuid: c0993332-972c-4e55-942d-f2ef02f1abc5
updated: 1484310387
title: highly
categories:
    - Dictionary
---
highly
     adv 1: to a high degree or extent; favorably or with much respect;
            "highly successful"; "He spoke highly of her"; "does
            not think highly of his writing"; "extremely
            interesting" [syn: {extremely}]
     2: at a high rate or wage; "highly paid workers"
     3: in a high position or level or rank; "details known by only
        a few highly placed persons"
