---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tannakian_category
offline_file: ""
offline_thumbnail: ""
uuid: 4ea2bc71-124f-498f-8411-db8bd2a6363b
updated: 1484309241
title: Tannakian category
categories:
    - Duality theories
---
In mathematics, a tannakian category is a particular kind of monoidal category C, equipped with some extra structure relative to a given field K. The role of such categories C is to approximate, in some sense, the category of linear representations of an algebraic group G defined over K. A number of major applications of the theory have been made, or might be made in pursuit of some of the central conjectures of contemporary algebraic geometry and number theory.
