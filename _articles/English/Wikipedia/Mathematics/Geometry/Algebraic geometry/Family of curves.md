---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Family_of_curves
offline_file: ""
offline_thumbnail: ""
uuid: f073c1a3-22fa-4377-9b81-5ede7c4b1adf
updated: 1484309145
title: Family of curves
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Apollonian_circles.svg.png
categories:
    - Algebraic geometry
---
A family of curves is a set of curves, each of which is given by a function or parametrization in which one or more of the parameters is variable. In general, the parameter(s) influence the shape of the curve in a way that is more complicated than a simple linear transformation. Sets of curves given by an implicit relation may also represent families of curves.
