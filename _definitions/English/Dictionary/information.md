---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/information
offline_file: ""
offline_thumbnail: ""
uuid: 1832d5a7-eee4-43d1-aceb-c513b19617f9
updated: 1484310341
title: information
categories:
    - Dictionary
---
information
     n 1: a message received and understood [syn: {info}]
     2: a collection of facts from which conclusions may be drawn;
        "statistical data" [syn: {data}]
     3: knowledge acquired through study or experience or
        instruction
     4: (communication theory) a numerical measure of the
        uncertainty of an outcome; "the signal contained thousands
        of bits of information" [syn: {selective information}, {entropy}]
     5: formal accusation of a crime
