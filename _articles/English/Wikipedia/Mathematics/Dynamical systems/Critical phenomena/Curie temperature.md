---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Curie_temperature
offline_file: ""
offline_thumbnail: ""
uuid: 0dee736c-dc1c-4035-a5aa-54f3aa26542e
updated: 1484308746
title: Curie temperature
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Weiss_domains_in_a_ferromagnetic_material.png
tags:
    - Magnetic moments
    - >
        Materials with magnetic moments that change properties at
        the Curie temperature
    - Paramagnetic
    - Ferromagnetic
    - Ferrimagnetic
    - Antiferromagnetic and the Néel temperature
    - Curie–Weiss law
    - Physics
    - Approaching Curie temperature from above
    - Approaching Curie temperature from below
    - Approaching absolute zero (0 kelvins)
    - Ising model of phase transitions
    - Weiss domains and surface and bulk Curie temperatures
    - "Changing a material's Curie temperature"
    - Composite materials
    - Particle size
    - Pressure
    - Orbital ordering
    - >
        Curie temperature in ferroelectric and piezoelectric
        materials
    - Piezoelectric
    - Ferroelectric and dielectric
    - Relative permittivity
    - Applications
    - Notes
categories:
    - Critical phenomena
---
In physics and materials science, the Curie temperature (TC), or Curie point, is the temperature at which certain materials lose their permanent magnetic properties, to be replaced by induced magnetism. The Curie temperature is named after Pierre Curie, who showed that magnetism was lost at a critical temperature.[1]
