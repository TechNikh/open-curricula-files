---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biological
offline_file: ""
offline_thumbnail: ""
uuid: c58b4c66-d71e-47b9-8e6a-4f99101ea7b2
updated: 1484310283
title: biological
categories:
    - Dictionary
---
biological
     adj 1: pertaining to biology or to life and living things [syn: {biologic}]
     2: of parents and children; related by blood; "biological
        child" [ant: {adoptive}]
