---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/album
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484559961
title: album
categories:
    - Dictionary
---
album
     n 1: one or more recordings issued together; originally released
          on 12-inch phonograph records (usually with attractive
          record covers) and later on cassette audio tape and
          compact disc [syn: {record album}]
     2: a book of blank pages with pockets or envelopes; for
        organizing photographs or stamp collections etc
