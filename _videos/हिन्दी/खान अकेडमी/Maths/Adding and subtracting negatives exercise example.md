---
version: 1
type: video
provider: YouTube
id: vBTW5ABC15o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 925ea755-8039-4812-8bea-e668f3c8efcc
updated: 1486069613
title: Adding and subtracting negatives exercise example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/vBTW5ABC15o/default.jpg
    - https://i3.ytimg.com/vi/vBTW5ABC15o/1.jpg
    - https://i3.ytimg.com/vi/vBTW5ABC15o/2.jpg
    - https://i3.ytimg.com/vi/vBTW5ABC15o/3.jpg
---
