---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thallophyta
offline_file: ""
offline_thumbnail: ""
uuid: de6e0036-e078-4afe-879a-27b9e34d7b30
updated: 1484308312
title: Thallophyte
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-CSIRO_ScienceImage_4092_Lichen_on_tree_in_Adelaide_Hills_SA_1992.jpg
tags:
    - Definitions
    - Subdivisions
    - Bibliography
categories:
    - Uncategorized
---
The thallophytes (Thallophyta or Thallobionta) are a polyphyletic group of non-mobile organisms traditionally described as "thalloid plants", "relatively simple plants" or "lower plants". They were a defunct division of kingdom Plantae that included fungus, lichens and algae and occasionally bryophytes, bacteria and the Myxomycota. Thallophytes have a hidden reproductive system and hence they are also called Cryptogamae (together with ferns), as opposed to Phanerogamae. The thallophytes are defined as having undifferentiated bodies (thalli), as opposed to cormophytes (Cormophyta) with roots and stems.
