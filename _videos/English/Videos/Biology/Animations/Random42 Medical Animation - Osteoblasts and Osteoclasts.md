---
version: 1
type: video
provider: YouTube
id: P8OtBv775mQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3d1871e4-05de-4c1e-8c58-b54bacf50521
updated: 1486069669
title: 'Random42 Medical Animation - Osteoblasts and Osteoclasts'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/P8OtBv775mQ/default.jpg
    - https://i3.ytimg.com/vi/P8OtBv775mQ/1.jpg
    - https://i3.ytimg.com/vi/P8OtBv775mQ/2.jpg
    - https://i3.ytimg.com/vi/P8OtBv775mQ/3.jpg
---
