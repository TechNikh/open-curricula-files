---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forcefully
offline_file: ""
offline_thumbnail: ""
uuid: 9d46c7fa-8792-4d2f-a4dc-f09354e82c54
updated: 1484310464
title: forcefully
categories:
    - Dictionary
---
forcefully
     adv : with full force; "we are seeing this film too late to feel
           its original impact forcefully"
