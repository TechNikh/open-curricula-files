---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illiterate
offline_file: ""
offline_thumbnail: ""
uuid: 20c79029-5909-4049-a288-d6c01dd59a68
updated: 1484310471
title: illiterate
categories:
    - Dictionary
---
illiterate
     adj 1: not able to read or write [ant: {literate}]
     2: ignorant of the fundamentals of a given art or branch of
        knowledge; "ignorant of quantum mechanics"; "musically
        illiterate" [syn: {ignorant}]
     n : a person unable to read [syn: {illiterate person}, {nonreader}]
