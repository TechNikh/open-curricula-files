---
version: 1
type: video
provider: YouTube
id: EvvxBdNIUeQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Example%201-%20Modeling%20with%20polynomials.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 90b0fdc3-c8c2-4fc4-965a-bf03e6a51aa5
updated: 1486069559
title: 'Example 1- Modeling with polynomials'
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/EvvxBdNIUeQ/default.jpg
    - https://i3.ytimg.com/vi/EvvxBdNIUeQ/1.jpg
    - https://i3.ytimg.com/vi/EvvxBdNIUeQ/2.jpg
    - https://i3.ytimg.com/vi/EvvxBdNIUeQ/3.jpg
---
