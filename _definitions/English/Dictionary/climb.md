---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/climb
offline_file: ""
offline_thumbnail: ""
uuid: 7081bf7e-4011-4f52-8539-fa6e74cc31b3
updated: 1484310275
title: climb
categories:
    - Dictionary
---
climb
     n 1: an upward slope or grade (as in a road); "the car couldn't
          make it up the rise" [syn: {ascent}, {acclivity}, {rise},
           {raise}, {upgrade}] [ant: {descent}]
     2: an event that involves rising to a higher point (as in
        altitude or temperature or intensity etc.) [syn: {climbing},
         {mounting}]
     3: the act of climbing something; "it was a difficult climb to
        the top" [syn: {mount}]
     v 1: go upward with gradual or continuous progress; "Did you ever
          climb up the hill behind your house?" [syn: {climb up},
          {mount}, {go up}]
     2: move with difficulty, by grasping
     3: go up or advance; "Sales were climbing ...
