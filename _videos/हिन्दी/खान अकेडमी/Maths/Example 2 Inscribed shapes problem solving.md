---
version: 1
type: video
provider: YouTube
id: 4LV2hGLjCZw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a381fa76-f6cf-49f4-9d20-085e72a338a7
updated: 1486069614
title: Example 2 Inscribed shapes problem solving
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/4LV2hGLjCZw/default.jpg
    - https://i3.ytimg.com/vi/4LV2hGLjCZw/1.jpg
    - https://i3.ytimg.com/vi/4LV2hGLjCZw/2.jpg
    - https://i3.ytimg.com/vi/4LV2hGLjCZw/3.jpg
---
