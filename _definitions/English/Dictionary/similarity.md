---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/similarity
offline_file: ""
offline_thumbnail: ""
uuid: ca098d2f-fd88-42a7-935c-a74025e2530b
updated: 1484310328
title: similarity
categories:
    - Dictionary
---
similarity
     n 1: the quality of being similar [ant: {dissimilarity}]
     2: a Getalt principle of organization holding that (other
        things being equal) parts of a stimulus field that are
        similar to each other tend to be perceived as belonging
        together as a unit [syn: {law of similarity}]
