---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Green_wave
offline_file: ""
offline_thumbnail: ""
uuid: 54448234-0e1f-4ce9-8e43-712320008340
updated: 1484308883
title: Green wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-GreenWave.gif
categories:
    - Waves
---
A green wave occurs when a series of traffic lights (usually three or more) are coordinated to allow continuous traffic flow over several intersections in one main direction.
