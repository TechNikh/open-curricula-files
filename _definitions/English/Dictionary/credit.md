---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/credit
offline_file: ""
offline_thumbnail: ""
uuid: 43211748-6d6c-47d4-81b9-20cb45e64bf5
updated: 1484310459
title: credit
categories:
    - Dictionary
---
credit
     n 1: approval; "give her recognition for trying"; "he was given
          credit for his work"; "give her credit for trying"; "the
          credits were given at the end of the film" [syn: {recognition}]
     2: money available for a client to borrow
     3: an accounting entry acknowledging income or capital items
        [syn: {credit entry}] [ant: {debit}]
     4: used in the phrase `to your credit' in order to indicate an
        achievement deserving praise; "she already had several
        performances to her credit";
     5: arrangement for deferred payment for goods and services
        [syn: {deferred payment}] [ant: {cash}]
     6: recognition by a college or ...
