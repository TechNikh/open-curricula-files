---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/host
offline_file: ""
offline_thumbnail: ""
uuid: 1077c394-771b-4d69-ae2f-602d6e4bae15
updated: 1484310479
title: host
categories:
    - Dictionary
---
host
     n 1: a person who invites guests to a social event (such as a
          party in his or her own home) and who is responsible for
          them while they are there
     2: a vast multitude [syn: {horde}, {legion}]
     3: an animal or plant that nourishes and supports a parasite;
        the host does not benefit and is often harmed by the
        association [ant: {parasite}]
     4: a person who acts as host at formal occasions (makes an
        introductory speech and introduces other speakers) [syn: {master
        of ceremonies}, {emcee}]
     5: archaic terms for army [syn: {legion}]
     6: any organization that provides resources and facilities for
        a function or ...
