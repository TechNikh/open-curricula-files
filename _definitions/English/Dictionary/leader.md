---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leader
offline_file: ""
offline_thumbnail: ""
uuid: 6cccb91e-3ba4-4adf-8d86-7c0385bcab1a
updated: 1484310551
title: leader
categories:
    - Dictionary
---
leader
     n 1: a person who rules or guides or inspires others [ant: {follower}]
     2: a featured article of merchandise sold at a loss in order to
        draw customers [syn: {drawing card}, {loss leader}]
