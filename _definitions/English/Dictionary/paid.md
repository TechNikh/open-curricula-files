---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paid
offline_file: ""
offline_thumbnail: ""
uuid: ab9674c8-c655-4046-afcc-7e71431ff6b7
updated: 1484310443
title: paid
categories:
    - Dictionary
---
paid
     adj 1: marked by the reception of pay; "paid work"; "a paid
            official"; "a paid announcement"; "a paid check" [ant:
             {unpaid}]
     2: involving gainful employment in something often done as a
        hobby [syn: {nonrecreational}]
     3: yielding a fair profit [syn: {gainful}, {paying}]
