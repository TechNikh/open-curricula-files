---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Ko%E2%80%93Rado_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 7d0b2561-04ce-46ca-a8cb-b7c950df6f04
updated: 1484308664
title: Erdős–Ko–Rado theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Intersecting_set_families_2-of-4.svg.png
tags:
    - Proof
    - Families of maximum size
    - Maximal intersecting families
categories:
    - Theorems in discrete mathematics
---
In combinatorics, the Erdős–Ko–Rado theorem of Paul Erdős, Chao Ko, and Richard Rado is a theorem on intersecting set families. It is part of the theory of hypergraphs, specifically, uniform hypergraphs of rank r.
