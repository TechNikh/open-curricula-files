---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fatalist
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484439541
title: fatalist
categories:
    - Dictionary
---
fatalist
     adj 1: believing in or inclined to fatalism; "a fatalist person"
            [syn: {fatalistic}]
     2: relating to or implying fatalism; "fatalistic thinking"
        [syn: {fatalistic}]
     n : anyone who submits to the belief that they are powerless to
         change their destiny [syn: {determinist}, {predestinarian},
          {predestinationist}]
