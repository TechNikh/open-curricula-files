---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Position_resection
offline_file: ""
offline_thumbnail: ""
uuid: f1be27cc-e795-4e08-9508-b343592d6637
updated: 1484309345
title: Position resection
tags:
    - Resection versus intersection
    - In navigation
    - In surveying
    - Notes
categories:
    - Trigonometry
---
Resection is a method for determining an unknown position (position finding) measuring angles with respect to known positions. Measurements can be made with a compass and topographic map (or nautical chart),[1][2] Theodolite or with a total station using known points of a Geodetic network or landmarks of a map.
