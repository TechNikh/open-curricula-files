---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Trigonal_pyramidal_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: eb93dfa1-15d1-493e-ac1d-35609ecfb2ab
updated: 1484309285
title: Trigonal pyramidal molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pyramidal-3D-balls.png
tags:
    - Trigonal pyramidal geometry in ammonia
categories:
    - Molecular geometry
---
In chemistry, a trigonal pyramid is a molecular geometry with one atom at the apex and three atoms at the corners of a trigonal base. When all three atoms at the corners are identical, the molecule belongs to point group C3v. Some molecules and ions with trigonal pyramidal geometry are the pnictogen hydrides (XH3), xenon trioxide (XeO3), the chlorate ion, ClO−
3, and the sulfite ion, SO2−
3. In organic chemistry, molecules which have a trigonal pyramidal geometry are sometimes described as sp3 hybridized. The AXE method for VSEPR theory states that the classification is AX3E1.
