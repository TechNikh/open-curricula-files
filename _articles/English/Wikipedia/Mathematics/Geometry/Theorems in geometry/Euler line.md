---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euler_line
offline_file: ""
offline_thumbnail: ""
uuid: d14c8b4f-780d-4bfd-a6b7-ad18f0c9831e
updated: 1484309117
title: Euler line
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Triangle.EulerLine.svg.png
tags:
    - Triangle centers on the Euler line
    - Individual centers
    - A vector proof
    - Distances between centers
    - Representation
    - Equation
    - Parametric representation
    - Slope
    - Relation to inscribed equilateral triangles
    - In special triangles
    - Right triangle
    - Isosceles triangle
    - Automedian triangle
    - Systems of triangles with concurrent Euler lines
    - Generalizations
    - Quadrilateral
    - Tetrahedron
    - Simplicial polytope
    - Related constructions
categories:
    - Theorems in geometry
---
In geometry, the Euler line, named after Leonhard Euler (/ˈɔɪlər/), is a line determined from any triangle that is not equilateral. It is a central line of the triangle, and it passes through several important points determined from the triangle, including the orthocenter, the circumcenter, the centroid, the Exeter point and the center of the nine-point circle of the triangle.[1]
