---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dose-fractionation_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 2efea991-e79a-4f00-a555-258a34f6ed5e
updated: 1484309244
title: Dose-fractionation theorem
categories:
    - Geometric measurement
---
Hegerl and Hoppe.[1] have pointed out that the total dose required to achieve statistical significance for each voxel of a computed 3D reconstruction is the same as that required to obtain a single 2D image of that isolated voxel, at the same level of statistical significance. Thus a statistically significant 3D image can be computed from statistically insignificant projections, as long as the total dose that is distributed among these projections is high enough that it would have resulted in a statistically significant projection, if applied to only one image.[2]
