---
version: 1
type: video
provider: YouTube
id: wI388XoCp48
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/How%20Your%20Brain%20Looks%20When%20You%20Think%20New%20Thoughts.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7b98fe49-6cfd-46d8-ab21-5ed3b4b6a4f7
updated: 1486069668
title: How Your Brain Looks When You Think New Thoughts
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/wI388XoCp48/default.jpg
    - https://i3.ytimg.com/vi/wI388XoCp48/1.jpg
    - https://i3.ytimg.com/vi/wI388XoCp48/2.jpg
    - https://i3.ytimg.com/vi/wI388XoCp48/3.jpg
---
