---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnificent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484555041
title: magnificent
categories:
    - Dictionary
---
magnificent
     adj : characterized by or attended with brilliance or grandeur;
           "the brilliant court life at Versailles"; "a glorious
           work of art"; "magnificent cathedrals"; "the splendid
           coronation ceremony" [syn: {brilliant}, {glorious}, {splendid}]
