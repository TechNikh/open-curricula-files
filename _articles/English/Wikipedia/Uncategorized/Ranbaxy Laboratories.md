---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ranbaxy
offline_file: ""
offline_thumbnail: ""
uuid: 013c257b-5210-4fb9-8f36-3dec500ef3fe
updated: 1484308463
title: Ranbaxy Laboratories
tags:
    - History
    - Formation
    - Trading
    - Acquisition by Daiichi-Sankyo
    - Acquisition by Sun Pharmaceutical
    - Controversies
    - Notes
categories:
    - Uncategorized
---
Ranbaxy Laboratories Limited (BSE: 500359) is an Indian multinational pharmaceutical company that was incorporated in India in 1961. The company went public in 1973 and Japanese pharmaceutical company Daiichi Sankyo acquired a controlling share in 2008.[2] In 2014, Sun Pharma acquired the entire 63.4% share of Ranbaxy making the conglomerate the world’s fifth largest specialty generic pharma company.[3]
