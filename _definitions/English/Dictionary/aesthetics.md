---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aesthetics
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484522282
title: aesthetics
categories:
    - Dictionary
---
aesthetics
     n : (art) the branch of philosophy dealing with beauty and taste
         (emphasizing the evaluative criteria that are applied to
         art); "traditional aesthetics assumed the existence of
         universal and timeless criteria of artistic value" [syn:
         {esthetics}]
