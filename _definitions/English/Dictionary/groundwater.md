---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/groundwater
offline_file: ""
offline_thumbnail: ""
uuid: 72be21af-2402-4d46-9689-1800c2a0a5a1
updated: 1484310262
title: groundwater
categories:
    - Dictionary
---
ground water
     n : underground water that is held in the soil and in pervious
         rocks [syn: {spring water}, {well water}]
