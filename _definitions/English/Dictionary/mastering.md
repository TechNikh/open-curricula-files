---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mastering
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484654041
title: mastering
categories:
    - Dictionary
---
mastering
     n 1: becoming proficient in the use of something; having mastery
          of; "his mastering the art of cooking took a long time"
     2: the act of making a master recording from which copies can
        be made; "he received a bill for mastering the concert and
        making 100 copies"
