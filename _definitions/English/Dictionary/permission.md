---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/permission
offline_file: ""
offline_thumbnail: ""
uuid: e8d46b53-96ba-4ab2-ad48-d458b482456e
updated: 1484310477
title: permission
categories:
    - Dictionary
---
permission
     n 1: approval to do something; "he asked permission to leave"
     2: the act of giving a formal (usually written) authorization
        [syn: {license}, {permit}]
