---
version: 1
type: video
provider: YouTube
id: PW48gHZ9D-M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e880783d-e41b-47cb-b8e8-9ea0c2b88898
updated: 1486069621
title: Proof Radius is perpendicular to tangent line
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/PW48gHZ9D-M/default.jpg
    - https://i3.ytimg.com/vi/PW48gHZ9D-M/1.jpg
    - https://i3.ytimg.com/vi/PW48gHZ9D-M/2.jpg
    - https://i3.ytimg.com/vi/PW48gHZ9D-M/3.jpg
---
