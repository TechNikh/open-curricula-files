---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Butene
offline_file: ""
offline_thumbnail: ""
uuid: 8d2c23b8-adf2-444f-abc2-7b910d15f594
updated: 1484308398
title: Butene
categories:
    - Uncategorized
---
Butene, also known as butylene, is an alkene with the formula C4H8. It is a colourless gas that is present in crude oil as a minor constituent in quantities that are too small for viable extraction. It is therefore obtained by catalytic cracking of long-chain hydrocarbons left during refining of crude oil. Cracking produces a mixture of products, and the butene is extracted from this by fractional distillation.
