---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exceptional
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441341
title: exceptional
categories:
    - Dictionary
---
exceptional
     adj 1: far beyond what is usual in magnitude or degree; "a night of
            exceeding darkness"; "an exceptional memory";
            "olympian efforts to save the city from bankruptcy";
            "the young Mozart's prodigious talents" [syn: {exceeding},
             {olympian}, {prodigious}, {surpassing}]
     2: surpassing what is common or usual or expected; "he paid
        especial attention to her"; "exceptional kindness"; "a
        matter of particular and unusual importance"; "a special
        occasion"; "a special reason to confide in her"; "what's
        so special about the year 2000?" [syn: {especial(a)}, {particular(a)},
         {special}]
     3: ...
