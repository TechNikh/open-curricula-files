---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homological_integration
offline_file: ""
offline_thumbnail: ""
uuid: 833e5fc6-25e8-4dd7-a463-fbedc47c4d45
updated: 1484309363
title: Homological integration
categories:
    - Geometry stubs
---
In the mathematical fields of differential geometry and geometric measure theory, homological integration or geometric integration is a method for extending the notion of the integral to manifolds. Rather than functions or differential forms, the integral is defined over currents on a manifold.
