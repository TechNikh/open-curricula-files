---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disappearing
offline_file: ""
offline_thumbnail: ""
uuid: b4e48ee7-0b0c-46af-996f-0422ee85988a
updated: 1484310547
title: disappearing
categories:
    - Dictionary
---
disappearing
     adj : quickly going away and passing out of sight; "all I saw was
           his vanishing back" [syn: {vanishing}]
     n : the act of leaving secretly or without explanation [syn: {disappearance}]
         [ant: {appearance}]
