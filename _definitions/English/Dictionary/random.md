---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/random
offline_file: ""
offline_thumbnail: ""
uuid: 4551bd27-824d-4542-8d75-6f0000792613
updated: 1484310232
title: random
categories:
    - Dictionary
---
random
     adj 1: lacking any definite plan or order or purpose; governed by
            or depending on chance; "a random choice"; "bombs fell
            at random"; "random movements" [ant: {nonrandom}]
     2: taken haphazardly; "a random choice"
