---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resolved
offline_file: ""
offline_thumbnail: ""
uuid: 9b7e628e-5fbd-4986-a28b-05789b85fd76
updated: 1484310593
title: resolved
categories:
    - Dictionary
---
resolved
     adj 1: determined; "she was firmly resolved to be a doctor";
            "single-minded in his determination to stop smoking"
            [syn: {single-minded}]
     2: explained or answered; "mysteries solved and unsolved;
        problems resolved and unresolved" [syn: {solved}] [ant: {unsolved}]
