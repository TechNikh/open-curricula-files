---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Duplet
offline_file: ""
offline_thumbnail: ""
uuid: c06f8c65-66a0-4dd7-a9ba-9bd1116f4a29
updated: 1484308386
title: Tuplet
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Irrational_rhythm.png
tags:
    - Terminology
    - Triplets
    - Tuplet notation
    - Usage and purpose
    - Counting
categories:
    - Uncategorized
---
In music a tuplet (also irrational rhythm or groupings, artificial division or groupings, abnormal divisions, irregular rhythm, gruppetto, extra-metric groupings, or, rarely, contrametric rhythm) is "any rhythm that involves dividing the beat into a different number of equal subdivisions from that usually permitted by the time-signature (e.g., triplets, duplets, etc.)" (Humphries 2002, 266). This is indicated by a number (or sometimes two), indicating the fraction involved. The notes involved are also often grouped with a bracket or (in older notation) a slur. The most common type is the "triplet".
