---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reproduction
offline_file: ""
offline_thumbnail: ""
uuid: fe3a2180-6a0f-475c-a510-73515f66f095
updated: 1484310309
title: reproduction
categories:
    - Dictionary
---
reproduction
     n 1: the process of generating offspring
     2: recall that is hypothesized to work by storing the original
        stimulus input and reproducing it during recall [syn: {reproductive
        memory}]
     3: copy that is not the original; something that has been
        copied [syn: {replica}, {replication}]
     4: the act of making copies; "Gutenberg's reproduction of holy
        texts was far more efficient" [syn: {replication}]
     5: the sexual activity of conceiving and bearing offspring
        [syn: {procreation}, {breeding}, {facts of life}]
