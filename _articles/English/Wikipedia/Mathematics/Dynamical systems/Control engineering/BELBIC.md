---
version: 1
type: article
id: https://en.wikipedia.org/wiki/BELBIC
offline_file: ""
offline_thumbnail: ""
uuid: 338ed01e-3a89-4db9-9916-2f7758b80e15
updated: 1484308722
title: BELBIC
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/LearningOrganism.JPG
tags:
    - Emotions and Learning
    - A Computational Model of Emotional Conditioning
    - The Controller
    - Applications
categories:
    - Control engineering
---
In recent years, the use of biologically inspired methods such as the evolutionary algorithm have been increasingly employed to solve and analyze complex computational problems. BELBIC (Brain Emotional Learning Based Intelligent Controller) is one such controller which is proposed by Caro Lucas and adopts the network model developed by Moren and Balkenius to mimic those parts of the brain which are known to produce emotion (namely, the amygdala, orbitofrontal cortex, thalamus and sensory input cortex).[1]
