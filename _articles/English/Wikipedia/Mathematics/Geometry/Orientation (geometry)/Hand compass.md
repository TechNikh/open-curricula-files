---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hand_compass
offline_file: ""
offline_thumbnail: ""
uuid: f9afd7a7-3d5a-4d85-8345-264ac9f29360
updated: 1484309295
title: Hand compass
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Compass.JPG
tags:
    - History and use
    - Marine hand bearing compass
    - Notes
categories:
    - Orientation (geometry)
---
A hand compass (aka hand bearing compass or sighting compass) is a compact magnetic compass capable of one-hand use and fitted with a sighting device to record a precise bearing or azimuth to a given target or to determine a location.[1][2] Hand or sighting compasses include instruments with simple notch-and-post alignment ("gunsights"), prismatic sights, direct or lensatic sights,[3] and mirror/vee (reflected-image) sights. With the additional precision offered by the sighting arrangement, and depending upon construction, sighting compasses provide increased accuracy when measuring precise bearings to an objective.[4]
