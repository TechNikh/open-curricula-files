---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laterally
offline_file: ""
offline_thumbnail: ""
uuid: 8d8b5d2d-1105-4555-8ee4-e7b99f9e9a88
updated: 1484310407
title: laterally
categories:
    - Dictionary
---
laterally
     adv 1: to or by or from the side; "such women carry in their heads
            kinship knowledge of six generations depth and
            extending laterally among consanguineal kin as far as
            the grandchildren of second cousin"
     2: in a lateral direction or location; "the body is
        spindle-shaped and only slightly compressed laterally"
