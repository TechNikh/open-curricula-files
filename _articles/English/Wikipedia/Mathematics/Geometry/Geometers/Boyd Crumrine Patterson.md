---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Boyd_Crumrine_Patterson
offline_file: ""
offline_thumbnail: ""
uuid: 440bbf12-63dd-4e10-8869-cded94fd36b1
updated: 1484309096
title: Boyd Crumrine Patterson
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ex_Libris_Boyd_Crumrine_Patterson.jpg
categories:
    - Geometers
---
Patterson was born in McKeesport, Pennsylvania on April 23, 1902 and graduated from Washington and Jefferson College in 1923, completing his studies in three years.[1] He was a member of the well-known Crumrine family of Washington County and a third-generation W&J graduate.[1] His father, John P. Patterson, was a member of W&J's class of 1885; his grandfather, Boyd Crumrine, a noted local historian, was in Jefferson College's class of 1860.[1] He was also a member of the Phi Kappa Psi Fraternity.[2]
