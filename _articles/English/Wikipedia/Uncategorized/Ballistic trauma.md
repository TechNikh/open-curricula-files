---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gun_wound
offline_file: ""
offline_thumbnail: ""
uuid: b1918f2d-d252-422d-9acc-ce5dab781f7e
updated: 1484308377
title: Ballistic trauma
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Gunshot_skull.jpg
tags:
    - Destructive effects
    - Origins of medical treatment
    - Surgical advances
    - Bibliography
categories:
    - Uncategorized
---
Ballistic trauma, or gunshot wound (GSW), is a form of physical trauma sustained from the discharge of arms or munitions.[1] The most common forms of ballistic trauma stem from firearms used in armed conflicts, civilian sporting, recreational pursuits and criminal activity.[2] Ballistic trauma can be fatal or cause long-term consequences.
