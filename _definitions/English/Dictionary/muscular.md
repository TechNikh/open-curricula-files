---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/muscular
offline_file: ""
offline_thumbnail: ""
uuid: f709ba1a-32d7-442b-9d07-76498f63e521
updated: 1484310337
title: muscular
categories:
    - Dictionary
---
muscular
     adj 1: of or relating to or consisting of muscle; "muscular
            contraction"
     2: having a robust muscular body-build characterized by
        predominance of structures (bone and muscle and connective
        tissue) developed from the embryonic mesodermal layer
        [syn: {mesomorphic}] [ant: {ectomorphic}, {endomorphic}]
     3: having or suggesting great physical power or force; "the
        muscular and passionate Fifth Symphony"
     4: (of a person) possessing physical strength and weight;
        rugged and powerful; "a hefty athlete"; "a muscular
        boxer"; "powerful arms" [syn: {brawny}, {hefty}, {powerful},
         {sinewy}]
