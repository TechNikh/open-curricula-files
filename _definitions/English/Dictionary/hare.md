---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hare
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484653561
title: hare
categories:
    - Dictionary
---
hare
     n 1: swift timid long-eared mammal larger than a rabbit having a
          divided upper lip and long hind legs; young born furred
          and with open eyes
     2: flesh of any of various rabbits or hares (wild or
        domesticated) eaten as food [syn: {rabbit}]
     v : run quickly, like a hare; "He hared down the hill"
