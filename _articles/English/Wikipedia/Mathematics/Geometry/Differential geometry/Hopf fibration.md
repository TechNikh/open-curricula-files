---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hopf_fibration
offline_file: ""
offline_thumbnail: ""
uuid: 7fe138cf-88c0-4076-a898-665e88aaefe1
updated: 1484309224
title: Hopf fibration
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Hopf_Fibration.png
tags:
    - Definition and construction
    - Direct construction
    - Geometric interpretation using the complex projective line
    - Fiber bundle structure
    - Geometric interpretation using rotations
    - Explicit formulae
    - Fluid mechanics
    - Generalizations
    - Real Hopf fibrations
    - Complex Hopf fibrations
    - Quaternionic Hopf fibrations
    - Octonionic Hopf fibrations
    - Fibrations between spheres
    - Geometry and applications
    - Notes
categories:
    - Differential geometry
---
In the mathematical field of topology, the Hopf fibration (also known as the Hopf bundle or Hopf map) describes a 3-sphere (a hypersphere in four-dimensional space) in terms of circles and an ordinary sphere. Discovered by Heinz Hopf in 1931, it is an influential early example of a fiber bundle. Technically, Hopf found a many-to-one continuous function (or "map") from the 3-sphere onto the 2-sphere such that each distinct point of the 2-sphere comes from a distinct circle of the 3-sphere (Hopf 1931). Thus the 3-sphere is composed of fibers, where each fiber is a circle — one for each point of the 2-sphere.
