---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Orientation_of_a_vector_bundle
offline_file: ""
offline_thumbnail: ""
uuid: eae84262-9683-469d-959e-e859a1b73f6e
updated: 1484309165
title: Orientation of a vector bundle
tags:
    - Examples
    - Operations
    - Thom space
categories:
    - Analytic geometry
---
In mathematics, an orientation of a real vector bundle is a generalization of an orientation of a vector space; thus, given a real vector bundle π: E →B, an orientation of E means: for each fiber Ex, there is an orientation of the vector space Ex and one demands that each trivialization map (which is a bundle map)
