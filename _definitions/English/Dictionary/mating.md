---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mating
offline_file: ""
offline_thumbnail: ""
uuid: 888dc497-92b8-4920-80e1-0e1179d17463
updated: 1484310289
title: mating
categories:
    - Dictionary
---
mating
     n : the act of pairing a male and female for reproductive
         purposes; "the casual couplings of adolescents"; "the
         mating of some species occurs only in the spring" [syn: {coupling},
          {pairing}, {conjugation}, {union}, {sexual union}]
