---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 333659d4-9d70-439f-9950-fd7428ca26e9
updated: 1484309277
title: Molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Water_molecule_dimensions.svg.png
tags:
    - Determination
    - The influence of thermal excitation
    - Bonding
    - Isomers
    - Types of molecular structure
    - VSEPR table
    - 3D representations
categories:
    - Molecular geometry
---
Molecular geometry is the three-dimensional arrangement of the atoms that constitute a molecule. It determines several properties of a substance including its reactivity, polarity, phase of matter, color, magnetism, and biological activity.[1][2][3] The angles between bonds that an atom forms depend only weakly on the rest of molecule, i.e. they can be understood as approximately local and hence transferable properties.
