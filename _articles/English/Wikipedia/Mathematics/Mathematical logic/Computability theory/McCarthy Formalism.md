---
version: 1
type: article
id: https://en.wikipedia.org/wiki/McCarthy_Formalism
offline_file: ""
offline_thumbnail: ""
uuid: 7edacbab-8d8f-4e52-8560-f018de17ed93
updated: 1484309487
title: McCarthy Formalism
tags:
    - Introduction
    - "McCarthy's notion of conditional expression"
    - |
        Minsky's explanation of the "formalism"
    - Expansion of IF-THEN-ELSE to the CASE operator
    - Notes
categories:
    - Computability theory
---
In computer science and recursion theory the McCarthy Formalism (1963) of computer scientist John McCarthy clarifies the notion of recursive functions by use of the IF-THEN-ELSE construction common to computer science, together with four of the operators of primitive recursive functions: zero, successor, equality of numbers and composition. The conditional operator replaces both primitive recursion and the mu-operator.
