---
version: 1
type: video
provider: YouTube
id: oaByqI_Txtc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 526a7284-0f45-4fa9-97d6-c3c231dc3436
updated: 1486069626
title: 'Telugu grammar - Punctuations'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/oaByqI_Txtc/default.jpg
    - https://i3.ytimg.com/vi/oaByqI_Txtc/1.jpg
    - https://i3.ytimg.com/vi/oaByqI_Txtc/2.jpg
    - https://i3.ytimg.com/vi/oaByqI_Txtc/3.jpg
---
