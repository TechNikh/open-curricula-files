---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lavish
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484504041
title: lavish
categories:
    - Dictionary
---
lavish
     adj 1: very generous; "distributed gifts with a lavish hand"; "the
            critics were lavish in their praise"; "a munificent
            gift"; "his father gave him a half-dollar and his
            mother a quarter and he thought them munificent";
            "prodigal praise"; "unsparing generosity"; "his
            unstinted devotion"; "called for unstinting aid to
            Britain" [syn: {munificent}, {overgenerous}, {prodigal},
             {too-generous}, {unsparing}, {unstinted}, {unstinting}]
     2: characterized by extravagance and profusion; "a lavish
        buffet"; "a lucullan feast" [syn: {lucullan}, {lush}, {plush},
         {plushy}]
     v : expend ...
