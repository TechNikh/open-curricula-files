---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hybridise
offline_file: ""
offline_thumbnail: ""
uuid: c283f13a-2653-46f1-8d10-c95739480797
updated: 1484310414
title: hybridise
categories:
    - Dictionary
---
hybridise
     v : breed animals or plants using parents of different races and
         varieties; "cross a horse and a donkey"; "Mendel tried
         crossbreeding"; "these species do not interbreed" [syn: {crossbreed},
          {cross}, {hybridize}, {interbreed}]
