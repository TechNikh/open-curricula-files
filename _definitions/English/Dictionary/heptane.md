---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heptane
offline_file: ""
offline_thumbnail: ""
uuid: b38562bf-4d27-425f-812a-55f04a3df6d4
updated: 1484310423
title: heptane
categories:
    - Dictionary
---
heptane
     n : a colorless volatile highly flammable liquid obtained from
         petroleum and used as an anesthetic or a solvent or in
         determining octane ratings
