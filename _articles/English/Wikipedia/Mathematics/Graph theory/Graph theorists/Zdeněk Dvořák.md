---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Zden%C4%9Bk_Dvo%C5%99%C3%A1k'
offline_file: ""
offline_thumbnail: ""
uuid: fea01420-f3f8-4c14-8339-adc5dc3e64b6
updated: 1484309461
title: Zdeněk Dvořák
categories:
    - Graph theorists
---
Dvořák was born April 26, 1981, in Nové Město na Moravě.[1] He competed on the Czech national team in the 1999 International Mathematical Olympiad,[2] and in the same year in the International Olympiad in Informatics, where he won a gold medal.[3] He earned his Ph.D. in 2007 from Charles University in Prague, under the supervision of Jaroslav Nešetřil. He remained as a research fellow at Charles University until 2010, and then did postdoctoral studies at the Georgia Institute of Technology and Simon Fraser University. He then returned to the Computer Science Institute (IUUK) of Charles University, obtained his habilitation in 2012, and is now an associate professor there.[1]
