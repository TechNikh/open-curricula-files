---
version: 1
type: video
provider: YouTube
id: w7-iyyR-iSg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c5d630e6-9597-4cc5-8f31-ac6174b3f8c6
updated: 1486069651
title: '3D fractal trip - Far away...'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/w7-iyyR-iSg/default.jpg
    - https://i3.ytimg.com/vi/w7-iyyR-iSg/1.jpg
    - https://i3.ytimg.com/vi/w7-iyyR-iSg/2.jpg
    - https://i3.ytimg.com/vi/w7-iyyR-iSg/3.jpg
---
