---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Extractor_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: be8771ba-b468-4ce2-a983-aaad0f3b8230
updated: 1484308671
title: Extractor (mathematics)
categories:
    - Theoretical computer science
---
An 
  
    
      
        (
        N
        ,
        M
        ,
        D
        ,
        K
        ,
        ϵ
        )
      
    
    {\displaystyle (N,M,D,K,\epsilon )}
  
 -extractor is a bipartite graph with 
  
    
      
        N
      
    
    {\displaystyle N}
  
 nodes on the left and 
  
    
      
        M
      
    
    {\displaystyle M}
  
 nodes on the right such that each node on the left has 
  
    
      
        D
      
    
    {\displaystyle D}
  
 neighbors (on the right), which has the added property that for any subset 
  
    
      
        A
      
    
    {\displaystyle A}
  
 of the left vertices of size at least 
  
    
      
        K
     ...
