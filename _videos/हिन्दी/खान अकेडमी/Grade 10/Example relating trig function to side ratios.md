---
version: 1
type: video
provider: YouTube
id: d9QG_25R0uQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dd543cb3-ece5-4162-85da-fd839a6667c0
updated: 1486069618
title: Example relating trig function to side ratios
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/d9QG_25R0uQ/default.jpg
    - https://i3.ytimg.com/vi/d9QG_25R0uQ/1.jpg
    - https://i3.ytimg.com/vi/d9QG_25R0uQ/2.jpg
    - https://i3.ytimg.com/vi/d9QG_25R0uQ/3.jpg
---
