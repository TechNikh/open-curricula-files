---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/groves
offline_file: ""
offline_thumbnail: ""
uuid: 16f896ce-a157-4be3-bf9b-e3100a388c6f
updated: 1484310462
title: groves
categories:
    - Dictionary
---
Groves
     n : United States general who served as military director of the
         atomic bomb project (1896-1970) [syn: {Leslie Richard
         Groves}]
