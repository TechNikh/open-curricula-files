---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ruined
offline_file: ""
offline_thumbnail: ""
uuid: f33a3800-c59e-4bc9-801c-108ddf9ce826
updated: 1484310563
title: ruined
categories:
    - Dictionary
---
ruined
     adj 1: destroyed physically or morally [syn: {destroyed}]
     2: doomed to extinction [syn: {done for(p)}, {sunk}, {undone},
        {washed-up}]
     3: brought to ruin; "after the revolution the aristocracy was
        finished"; "the unsuccessful run for office left him
        ruined politically and economically" [syn: {finished}]
     4: made uninhabitable; "upon this blasted heath"- Shakespeare;
        "a wasted landscape" [syn: {blasted}, {desolate}, {desolated},
         {devastated}, {ravaged}, {wasted}]
