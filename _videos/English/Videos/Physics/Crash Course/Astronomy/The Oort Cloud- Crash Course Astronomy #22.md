---
version: 1
type: video
provider: YouTube
id: ZJscxTyI__s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 796087ff-2ecc-4a45-9e67-00721eb99b74
updated: 1486069684
title: 'The Oort Cloud- Crash Course Astronomy #22'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZJscxTyI__s/default.jpg
    - https://i3.ytimg.com/vi/ZJscxTyI__s/1.jpg
    - https://i3.ytimg.com/vi/ZJscxTyI__s/2.jpg
    - https://i3.ytimg.com/vi/ZJscxTyI__s/3.jpg
---
