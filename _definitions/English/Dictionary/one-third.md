---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/one-third
offline_file: ""
offline_thumbnail: ""
uuid: 1414e1f9-e863-4f7a-8328-d96c8eae8140
updated: 1484310238
title: one-third
categories:
    - Dictionary
---
one-third
     n : one of three equal parts of a divisible whole; "it contains
         approximately a third of the minimum daily requirement"
         [syn: {third}, {tierce}]
