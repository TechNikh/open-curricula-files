---
version: 1
type: article
id: https://en.wikipedia.org/wiki/BEST_theorem
offline_file: ""
offline_thumbnail: ""
uuid: d1635c9a-716e-4b97-b57a-2fffc42f55c7
updated: 1484309456
title: BEST theorem
tags:
    - Precise statement
    - Applications
    - History
    - Notes
categories:
    - Theorems in graph theory
---
In graph theory, a part of discrete mathematics, the BEST theorem gives a product formula for the number of Eulerian circuits in directed (oriented) graphs. The name is an acronym of the names of people who discovered it: de Bruijn, van Aardenne-Ehrenfest, Smith and Tutte.
