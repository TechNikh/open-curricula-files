---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/respectfully
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484483221
title: respectfully
categories:
    - Dictionary
---
respectfully
     adv : in a respectful manner; "might I respectfully suggest to the
           Town Council that they should adopt a policy of
           masterly inactivity?" [syn: {with all respect}] [ant: {disrespectfully}]
