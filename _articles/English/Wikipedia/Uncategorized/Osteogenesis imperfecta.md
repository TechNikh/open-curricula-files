---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Brittle_bones
offline_file: ""
offline_thumbnail: ""
uuid: b62ef37e-29eb-402a-94d9-e6a9562de8a6
updated: 1484308371
title: Osteogenesis imperfecta
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Characteristically_blue_sclerae_of_patient_with_osteogenesis_imperfecta.jpg
tags:
    - Classification
    - Type I
    - Type II
    - Type III
    - Type IV
    - Type V
    - Type VI
    - Type VII
    - Type VIII
    - Other genes
    - Pathophysiology
    - Diagnosis
    - Treatment
    - Bisphosphonates
    - Surgery
    - Physiotherapy
    - Physical aids
    - History
    - Epidemiology
    - Society and culture
    - Other animals
categories:
    - Uncategorized
---
Osteogenesis imperfecta (OI) is a group of genetic disorders that mainly affect the bones. It results in bones that break easily. The severity may be mild to severe.[1] Other symptoms may include a blue tinge to the whites of the eye, short height, loose joints, hearing loss, breathing problems, and problems with the teeth.[1][3]
