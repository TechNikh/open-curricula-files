---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eye_sight
offline_file: ""
offline_thumbnail: ""
uuid: 825da69b-2419-40db-82f6-0532b3d4210d
updated: 1484308366
title: Visual perception
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Ventral-dorsal_streams.svg.png
tags:
    - Visual system
    - study
    - Early studies
    - Unconscious inference
    - Gestalt theory
    - Analysis of eye movement
    - Face and object recognition
    - The cognitive and computational approaches
    - Transduction
    - Opponent process
    - Artificial visual perception
    - Vision deficiencies or disorders
    - Related disciplines
categories:
    - Uncategorized
---
Visual perception is the ability to interpret the surrounding environment by processing information that is contained in visible light. The resulting perception is also known as eyesight, sight, or vision (adjectival form: visual, optical, or ocular). The various physiological components involved in vision are referred to collectively as the visual system, and are the focus of much research in Linguistics, psychology, cognitive science, neuroscience, and molecular biology, collectively referred to as vision science.
