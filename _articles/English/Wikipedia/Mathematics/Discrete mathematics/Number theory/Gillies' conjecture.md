---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gillies%27_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: e739f747-9725-4b9c-8555-d1195f589603
updated: 1484308645
title: "Gillies' conjecture"
categories:
    - Number theory
---
In number theory, Gillies' conjecture is a conjecture about the distribution of prime divisors of Mersenne numbers and was made by Donald B. Gillies in a 1964 paper[1] in which he also announced the discovery of three new Mersenne primes. The conjecture is a specialization of the prime number theorem and is a refinement of conjectures due to I. J. Good[2] and Daniel Shanks.[3] The conjecture remains an open problem, although several papers have added empirical support to its validity.
