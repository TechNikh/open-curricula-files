---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convoy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484591401
title: convoy
categories:
    - Dictionary
---
convoy
     n 1: a procession of land vehicles traveling together
     2: a collection of merchant ships with an escort of warships
     3: the act of escorting while in transit
     v : escort in transit; "the trucks convoyed the cars across the
         battle zone"; "the warships convoyed the merchant ships
         across the Pacific"
