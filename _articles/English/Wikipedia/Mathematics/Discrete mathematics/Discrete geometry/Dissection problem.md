---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dissection_problem
offline_file: ""
offline_thumbnail: ""
uuid: 2c211cd2-e124-449c-902a-9f7fa83041c3
updated: 1484308598
title: Dissection problem
categories:
    - Discrete geometry
---
In geometry, a dissection problem is the problem of partitioning a geometric figure (such as a polytope or ball) into smaller pieces that may be rearranged into a new figure of equal content. In this context, the partitioning is called simply a dissection (of one polytope into another). It is usually required that the dissection use only a finite number of pieces.
