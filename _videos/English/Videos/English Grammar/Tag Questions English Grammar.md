---
version: 1
type: video
provider: YouTube
id: 5LVXzsNumOI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bb91753e-a7a7-4467-ae4d-75448addb4db
updated: 1486069631
title: Tag Questions English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/5LVXzsNumOI/default.jpg
    - https://i3.ytimg.com/vi/5LVXzsNumOI/1.jpg
    - https://i3.ytimg.com/vi/5LVXzsNumOI/2.jpg
    - https://i3.ytimg.com/vi/5LVXzsNumOI/3.jpg
---
