---
version: 1
type: video
provider: YouTube
id: xvvI_QRYxBY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Calculus%20BC%202008%202%20a.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b087b339-30b5-4b77-aaf3-2dd8ea7debc4
updated: 1486069690
title: Calculus BC 2008 2 a
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/xvvI_QRYxBY/default.jpg
    - https://i3.ytimg.com/vi/xvvI_QRYxBY/1.jpg
    - https://i3.ytimg.com/vi/xvvI_QRYxBY/2.jpg
    - https://i3.ytimg.com/vi/xvvI_QRYxBY/3.jpg
---
