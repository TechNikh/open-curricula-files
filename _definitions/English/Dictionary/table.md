---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/table
offline_file: ""
offline_thumbnail: ""
uuid: eaba362c-018e-4807-9563-e878f3ada165
updated: 1484310359
title: table
categories:
    - Dictionary
---
table
     n 1: a set of data arranged in rows and columns; "see table 1"
          [syn: {tabular array}]
     2: a piece of furniture having a smooth flat top that is
        usually supported by one or more vertical legs; "it was a
        sturdy table"
     3: a piece of furniture with tableware for a meal laid out on
        it; "I reserved a table at my favorite restaurant"
     4: flat tableland with steep edges; "the tribe was relatively
        safe on the mesa but they had to descend into the valley
        for water" [syn: {mesa}]
     5: a company of people assembled at a table for a meal or game;
        "he entertained the whole table with his witty remarks"
     6: food or ...
