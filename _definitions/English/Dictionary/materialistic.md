---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/materialistic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484640001
title: materialistic
categories:
    - Dictionary
---
materialistic
     adj 1: marked by materialism [syn: {mercenary}, {worldly-minded}]
     2: conforming to the standards and conventions of the middle
        class; "a bourgeois mentality" [syn: {bourgeois}, {conservative}]
