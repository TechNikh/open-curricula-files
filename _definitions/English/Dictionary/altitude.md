---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/altitude
offline_file: ""
offline_thumbnail: ""
uuid: 2bfe4881-9027-4683-bbbf-8f487420d5db
updated: 1484310435
title: altitude
categories:
    - Dictionary
---
altitude
     n 1: elevation especially above sea level or above the earth's
          surface; "the altitude gave her a headache" [syn: {height}]
     2: the perpendicular distance from the base of a geometric
        figure to opposite vertex (or side if parallel)
     3: angular distance above the horizon (especially of a
        celestial object) [syn: {elevation}, {EL}, {ALT}]
