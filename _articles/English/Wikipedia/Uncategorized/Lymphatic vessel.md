---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lymphatic_vessel
offline_file: ""
offline_thumbnail: ""
uuid: 861d4354-ace9-432d-8ddc-08a03531067e
updated: 1484308377
title: Lymphatic vessel
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Illu_lymph_capillary.jpg
tags:
    - Structure
    - Lymph capillaries
    - Lymph vessels
    - Function
    - Afferent vessels
    - Efferent vessels
    - Clinical significance
    - Additional images
categories:
    - Uncategorized
---
In anatomy, lymphatic vessels (or lymph vessels or lymphatics) are thin-walled, valved structures that carry lymph. As part of the lymphatic system, lymph vessels are complementary to the cardiovascular system. Lymph vessels are lined by endothelial cells, and have a thin layer of smooth muscles, and adventitia that bind the lymph vessels to the surrounding tissue. Lymph vessels are devoted to propulsion of the lymph from the lymph capillaries, which are mainly concerned with absorption of interstitial fluid from the tissues. Lymph capillaries are slightly larger than their counterpart capillaries of the vascular system. Lymph vessels that carry lymph to a lymph node are called the afferent ...
