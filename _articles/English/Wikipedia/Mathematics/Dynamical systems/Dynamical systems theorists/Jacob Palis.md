---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jacob_Palis
offline_file: ""
offline_thumbnail: ""
uuid: fea3b514-e424-4e2f-8079-ca3d40465791
updated: 1484308696
title: Jacob Palis
categories:
    - Dynamical systems theorists
---
Jacob Palis, Jr. (born 15 March 1940) is a Brazilian mathematician and professor. Palis' research interests are mainly dynamical systems and differential equations. Some themes are Global stability and hyperbolicity, bifurcations, attractors and chaotic systems.
