---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adult
offline_file: ""
offline_thumbnail: ""
uuid: 838974e2-a8d4-4ce0-8ea4-aaf220265b71
updated: 1484310284
title: adult
categories:
    - Dictionary
---
adult
     adj : (of animals) fully developed; "an adult animal"; "a grown
           woman" [syn: {big}, {full-grown}, {fully grown}, {grown},
            {grownup}]
     n 1: a fully developed person from maturity onward [syn: {grownup}]
          [ant: {juvenile}]
     2: any mature animal
