---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unfortunate
offline_file: ""
offline_thumbnail: ""
uuid: 372de8eb-ae73-44ee-bc56-6ad973fab6af
updated: 1484310150
title: unfortunate
categories:
    - Dictionary
---
unfortunate
     adj 1: not favored by fortune; marked or accompanied by or
            resulting in ill fortune; "an unfortunate turn of
            events"; "an unfortunate decision"; "unfortunate
            investments"; "an unfortunate night for all concerned"
            [ant: {fortunate}]
     2: not auspicious; boding ill [syn: {inauspicious}] [ant: {auspicious}]
     3: unsuitable or regrettable; "an unfortunate choice of words";
        "an unfortunate speech"
     n : a person who suffers misfortune [syn: {unfortunate person}]
