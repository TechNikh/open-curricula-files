---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/primate
offline_file: ""
offline_thumbnail: ""
uuid: 3195283b-72bd-48e3-8b99-74a2b68c6ebc
updated: 1484310168
title: primate
categories:
    - Dictionary
---
primate
     n 1: a senior clergyman and dignitary [syn: {archpriest}, {hierarch},
           {high priest}, {prelate}]
     2: any placental mammal of the order Primates; has good
        eyesight and flexible hands and feet
