---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/why
offline_file: ""
offline_thumbnail: ""
uuid: 5b5fe4d2-34c2-4dfc-b2a6-2190382191e7
updated: 1484310351
title: why
categories:
    - Dictionary
---
why
     n : the cause or intention underlying an action or situation,
         especially in the phrase `the whys and wherefores' [syn:
         {wherefore}]
     adv : question word; what is the reason (`how come' is a
           nonstandard variant); "why are you here?"; "how come he
           got an ice cream cone but I didn't?" [syn: {how come}]
