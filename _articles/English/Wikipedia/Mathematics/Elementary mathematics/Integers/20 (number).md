---
version: 1
type: article
id: https://en.wikipedia.org/wiki/20_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 578f1137-bd1d-4961-b1ca-522cc691fef1
updated: 1484308944
title: 20 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Icosahedron.svg.png
tags:
    - In mathematics
    - In science
    - Biology
    - As an indefinite number
    - In sports
    - Age 20
    - In other fields
categories:
    - Integers
---
