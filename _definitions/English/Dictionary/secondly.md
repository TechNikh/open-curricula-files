---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secondly
offline_file: ""
offline_thumbnail: ""
uuid: ef3949a6-702e-4352-9dcb-34f050bd7e3e
updated: 1484310529
title: secondly
categories:
    - Dictionary
---
secondly
     adv : in the second place; "second, we must consider the economy"
           [syn: {second}]
