---
version: 1
type: video
provider: YouTube
id: 9unpqe8087E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/DNA%20Replication-%20Helicase%20and%20Unwinding.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8d1fdf7f-5201-4979-b334-f40e84e6b63b
updated: 1486069672
title: 'DNA Replication- Helicase and Unwinding'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/9unpqe8087E/default.jpg
    - https://i3.ytimg.com/vi/9unpqe8087E/1.jpg
    - https://i3.ytimg.com/vi/9unpqe8087E/2.jpg
    - https://i3.ytimg.com/vi/9unpqe8087E/3.jpg
---
