---
title: What is groundwater?
youtube_id: PTdHygICVaw
tags:
    - water in western australia
    - australian water supply
    - the water cycle
    - water corporation
    - Water Education
categories:
    - Water harvesting
---
