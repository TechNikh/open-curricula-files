---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obedient
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413381
title: obedient
categories:
    - Dictionary
---
obedient
     adj : dutifully complying with the commands or instructions of
           those in authority; "an obedient soldier"; "obedient
           children"; "a little man obedient to his wife"; "the
           obedient colonies...are heavily taxed; the refractory
           remain unburdened"- Edmund Burke [ant: {disobedient}]
