---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meiosis
offline_file: ""
offline_thumbnail: ""
uuid: cef9cfff-dc4a-41e7-972a-d8f657e1ebf0
updated: 1484310284
title: meiosis
categories:
    - Dictionary
---
meiosis
     n 1: (genetics) cell division that produces reproductive cells in
          sexually reproducing organisms; the nucleus divides into
          four nuclei each containing half the chromosome number
          (leading to gametes in animals and spores in plants)
          [syn: {miosis}, {reduction division}]
     2: understatement for rhetorical effect (especially when
        expressing an affirmative by negating its contrary);
        "saying `I was not a little upset' when you mean `I was
        very upset' is an example of litotes" [syn: {litotes}]
     [also: {meioses} (pl)]
