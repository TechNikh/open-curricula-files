---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smoking
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484615461
title: smoking
categories:
    - Dictionary
---
smoking
     adj : emitting smoke in great volume; "a smoking fireplace"
     n 1: the act of smoking tobacco or other substances; "he went
          outside for a smoke"; "smoking stinks" [syn: {smoke}]
     2: a hot vapor containing fine particles of carbon being
        produced by combustion; "the fire produced a tower of
        black smoke that could be seen for miles" [syn: {smoke}]
