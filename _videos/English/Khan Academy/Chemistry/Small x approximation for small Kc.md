---
version: 1
type: video
provider: YouTube
id: owA2rUggKoA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Small%20x%20approximation%20for%20small%20Kc.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1b650d77-9cee-442a-96b1-3fe255ec7641
updated: 1486069583
title: Small x approximation for small Kc
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/owA2rUggKoA/default.jpg
    - https://i3.ytimg.com/vi/owA2rUggKoA/1.jpg
    - https://i3.ytimg.com/vi/owA2rUggKoA/2.jpg
    - https://i3.ytimg.com/vi/owA2rUggKoA/3.jpg
---
When the small x approximation is useful for solving equilibrium problems, and an example using the small x approximation when Kc is small.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/chemical-equilibrium/equilibrium-constant/v/small-x-approximation-for-large-kc?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/chemical-equilibrium/equilibrium-constant/v/heterogenous-equilibrium?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
