---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Node_influence_metric
offline_file: ""
offline_thumbnail: ""
uuid: 2dfc6991-92f2-4264-b1fb-2ef56c388279
updated: 1484308629
title: Node influence metric
tags:
    - Origin and development
    - Accessibility
    - Definition
    - Applications
    - Expected force
    - Definition
    - Applications
    - Other approaches
categories:
    - Graph theory
---
In graph theory and network analysis, node influence metrics are measures that rank or quantify the influence of every node (also called vertex) within a graph. They are related to centrality indices. Applications include measuring the influence of each person in a social network, understanding the role of infrastructure nodes in transportation networks, the Internet, or urban networks, and the participation of a given node in disease dynamics.
