---
version: 1
type: video
provider: YouTube
id: Xfmu-7JuDGg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1da6368c-d141-4a91-ba96-75282de1584b
updated: 1486069622
title: How to trick your math teacher
categories:
    - Math Magic Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Xfmu-7JuDGg/default.jpg
    - https://i3.ytimg.com/vi/Xfmu-7JuDGg/1.jpg
    - https://i3.ytimg.com/vi/Xfmu-7JuDGg/2.jpg
    - https://i3.ytimg.com/vi/Xfmu-7JuDGg/3.jpg
---
