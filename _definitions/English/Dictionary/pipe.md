---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pipe
offline_file: ""
offline_thumbnail: ""
uuid: 893471cb-8463-4f77-991a-1979bdf4125c
updated: 1484310335
title: pipe
categories:
    - Dictionary
---
pipe
     n 1: a tube with a small bowl at one end; used for smoking
          tobacco [syn: {tobacco pipe}]
     2: a long tube made of metal or plastic that is used to carry
        water or oil or gas etc. [syn: {pipage}, {piping}]
     3: a hollow cylindrical shape [syn: {tube}]
     4: a tubular wind instrument [syn: {tabor pipe}]
     5: the flues and stops on a pipe organ [syn: {organ pipe}, {pipework}]
     v 1: utter a shrill cry [syn: {shriek}, {shrill}, {pipe up}]
     2: transport by pipeline; "pipe oil, water, and gas into the
        desert"
     3: play on a pipe; "pipe a tune"
     4: trim with piping; "pipe the skirt"
