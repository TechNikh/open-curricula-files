---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terminate
offline_file: ""
offline_thumbnail: ""
uuid: 5b413ca0-8d6c-4ecb-9755-9192bbafd29f
updated: 1484310517
title: terminate
categories:
    - Dictionary
---
terminate
     v 1: bring to an end or halt; "She ended their friendship when
          she found out that he had once been convicted of a
          crime"; "The attack on Poland terminated the relatively
          peaceful period after WWI" [syn: {end}] [ant: {begin}, {get
          down}]
     2: have an end, in a temporal, spatial, or quantitative sense;
        either spatial or metaphorical; "the bronchioles terminate
        in a capillary bed"; "Your rights stop where you infringe
        upon the rights of other"; "My property ends by the
        bushes"; "The symphony ends in a pianissimo" [syn: {end},
        {stop}, {finish}, {cease}] [ant: {begin}]
     3: be the end of; be the ...
