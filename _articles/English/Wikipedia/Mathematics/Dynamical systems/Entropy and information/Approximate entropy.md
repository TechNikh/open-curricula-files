---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Approximate_entropy
offline_file: ""
offline_thumbnail: ""
uuid: b08b6250-bcd9-480d-a2cf-7fcb6f65f48f
updated: 1484308767
title: Approximate entropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Heartrate.jpg
tags:
    - The algorithm
    - The interpretation
    - One example
    - Python implementation
    - Advantages
    - Applications
    - Limitations
categories:
    - Entropy and information
---
In statistics, an approximate entropy (ApEn) is a technique used to quantify the amount of regularity and the unpredictability of fluctuations over time-series data.[1]
