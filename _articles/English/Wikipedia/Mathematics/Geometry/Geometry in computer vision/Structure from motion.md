---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Structure_from_motion
offline_file: ""
offline_thumbnail: ""
uuid: eec6bc98-4fcf-41fe-8086-df4137f41384
updated: 1484309187
title: Structure from motion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-DSM_construction_site.jpg
tags:
    - Obtaining 3D information from 2D images
    - SfM for the geosciences
    - SfM for cultural heritage structure analysis
    - Structure from motion software toolboxes
    - Open source solutions
    - Other software
categories:
    - Geometry in computer vision
---
Structure from motion (SfM) is a range imaging technique for estimating three-dimensional structures from two-dimensional image sequences that may be coupled with local motion signals. It is studied in the fields of computer vision and visual perception. In biological vision, SfM refers to the phenomenon by which humans (and other living creatures) can recover 3D structure from the projected 2D (retinal) motion field of a moving object or scene.
