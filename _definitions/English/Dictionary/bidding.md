---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bidding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484646241
title: bidding
categories:
    - Dictionary
---
bid
     n 1: an authoritative direction or instruction to do something
          [syn: {command}, {bidding}, {dictation}]
     2: an attempt to get something; "they made a futile play for
        power"; "he made a bid to gain attention" [syn: {play}]
     3: a formal proposal to buy at a specified price [syn: {tender}]
     4: (bridge) the number of tricks a bridge player is willing to
        contract to make [syn: {bidding}]
     v 1: propose a payment; "The Swiss dealer offered $2 million for
          the painting" [syn: {offer}, {tender}]
     2: invoke upon; "wish you a nice evening"; "bid farewell" [syn:
         {wish}]
     3: ask for or request earnestly; "The prophet bid all ...
