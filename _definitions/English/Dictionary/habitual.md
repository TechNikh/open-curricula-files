---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/habitual
offline_file: ""
offline_thumbnail: ""
uuid: 0961ab57-0f38-4e5b-aaa6-6db0398a9036
updated: 1484310319
title: habitual
categories:
    - Dictionary
---
habitual
     adj 1: made a norm or custom or habit; "his habitual practice was
            to eat an early supper"; "her habitual neatness"
     2: commonly used or practiced; usual; "his accustomed
        thoroughness"; "took his customary morning walk"; "his
        habitual comment"; "with her wonted candor" [syn: {accustomed},
         {customary}, {wonted(a)}]
     3: having a habit of long standing; "a chronic smoker" [syn: {chronic},
         {confirmed}, {inveterate(a)}]
