---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/song
offline_file: ""
offline_thumbnail: ""
uuid: e94423a5-0f3a-46d5-b422-67b0beaeb857
updated: 1484310545
title: song
categories:
    - Dictionary
---
song
     n 1: a short musical composition with words; "a successful
          musical must have at least three good songs"
     2: a distinctive or characteristic sound; "the song of bullets
        was in the air"; "the song of the wind"; "the wheels sang
        their song as the train rocketed ahead"
     3: the act of singing; "with a shout and a song they marched up
        to the gates" [syn: {strain}]
     4: the characteristic sound produced by a bird; "a bird will
        not learn its song unless it hears it at an early age"
        [syn: {birdcall}, {call}, {birdsong}]
     5: a very small sum; "he bought it for a song"
     6: the imperial dynasty of China from 960 to 1279; ...
