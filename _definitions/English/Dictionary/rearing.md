---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rearing
offline_file: ""
offline_thumbnail: ""
uuid: 7ab9fa60-3f98-45de-abc7-cdd95205b104
updated: 1484310477
title: rearing
categories:
    - Dictionary
---
rearing
     adj : rearing on left hind leg with forelegs elevated and head
           usually in profile; "a lion rampant" [syn: {rampant(ip)}]
     n 1: the properties acquired as a consequence of the way you were
          treated as a child [syn: {raising}, {nurture}]
     2: raising someone to be an accepted member of the community;
        "they debated whether nature or nurture was more
        important" [syn: {breeding}, {bringing up}, {fostering}, {fosterage},
         {nurture}, {raising}, {upbringing}]
