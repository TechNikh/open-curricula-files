---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/israel
offline_file: ""
offline_thumbnail: ""
uuid: 1311f5b4-cdd1-4e7a-9040-572701d7063c
updated: 1484310178
title: israel
categories:
    - Dictionary
---
Israel
     n 1: Jewish republic in southwestern Asia at eastern end of
          Mediterranean; formerly part of Palestine [syn: {State
          of Israel}, {Yisrael}, {Zion}, {Sion}]
     2: an ancient kingdom of the Hebrew tribes at the southeastern
        end of the Mediterranean Sea; founded by Saul around 1025
        BC and destroyed by the Assyrians in 721 BC
