---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/golan
offline_file: ""
offline_thumbnail: ""
uuid: a8087b45-0112-48f2-b91d-65772eecc446
updated: 1484310178
title: golan
categories:
    - Dictionary
---
Golan
     n : a fortified hilly area between southern Lebanon and southern
         Syria; "artillery on the Golan Heights can dominate a
         large area of Israel" [syn: {Golan Heights}]
