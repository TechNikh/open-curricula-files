---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cotangent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484372401
title: cotangent
categories:
    - Dictionary
---
cotangent
     n : ratio of the adjacent to the opposite side of a right-angled
         triangle [syn: {cotan}]
