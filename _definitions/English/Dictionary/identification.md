---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/identification
offline_file: ""
offline_thumbnail: ""
uuid: 59da4000-a62f-49a5-a99e-c80337bd1365
updated: 1484310571
title: identification
categories:
    - Dictionary
---
identification
     n 1: the act of designating or identifying something [syn: {designation}]
     2: attribution to yourself (consciously or unconsciously) of
        the characteristics of another person (or group of
        persons)
     3: evidence of identity; something that identifies a person or
        thing
     4: the condition of having your identity established; "the
        thief's identification was followed quickly by his arrest"
     5: the process of recognizing something or someone by
        remembering; "a politician whose recall of names was as
        remarkable as his recognition of faces"; "experimental
        psychologists measure the elapsed time from the onset ...
