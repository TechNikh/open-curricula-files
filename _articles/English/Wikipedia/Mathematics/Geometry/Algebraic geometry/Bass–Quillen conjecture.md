---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bass%E2%80%93Quillen_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 9fb62f04-5213-4f04-8881-3dec244bf8f5
updated: 1484309130
title: Bass–Quillen conjecture
tags:
    - Statement of the conjecture
    - Known cases
    - Extensions
categories:
    - Algebraic geometry
---
In mathematics, the Bass–Quillen conjecture relates vector bundles over a regular Noetherian ring A and over the polynomial ring 
  
    
      
        A
        [
        
          t
          
            1
          
        
        ,
        …
        ,
        
          t
          
            n
          
        
        ]
      
    
    {\displaystyle A[t_{1},\dots ,t_{n}]}
  
. The conjecture is named for Hyman Bass and Daniel Quillen, who formulated the conjecture.[1][2]
