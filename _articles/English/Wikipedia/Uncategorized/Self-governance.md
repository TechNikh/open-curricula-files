---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-government
offline_file: ""
offline_thumbnail: ""
uuid: 389f62c7-22fa-48dc-ac59-c52d52e02499
updated: 1484094290
title: Self-governance
categories:
    - Uncategorized
---
It may refer to personal conduct or family units but more commonly refers to larger scale activities: professions, industry bodies, religions, political units (usually referred to as local government), including autonomous regions and/or others within nation-states that enjoy some sovereign rights. It falls within the larger context of governance and principles such as consent of the governed, and may involve non-profit organizations and corporate governance.
