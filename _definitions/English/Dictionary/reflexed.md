---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reflexed
offline_file: ""
offline_thumbnail: ""
uuid: 860713b1-0e7e-4d33-9aa7-1aef1f341b51
updated: 1484310163
title: reflexed
categories:
    - Dictionary
---
reflexed
     adj : (of leaves) bent downward and outward more than 90 degrees
