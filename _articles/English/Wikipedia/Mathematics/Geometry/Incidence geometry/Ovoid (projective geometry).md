---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ovoid_(projective_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: b2c5871b-2ffb-48f1-88a2-7e25dba856c9
updated: 1484309253
title: Ovoid (projective geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ovoid-definition.svg.png
tags:
    - Definition of an ovoid
    - Examples
    - In real projective space (inhomogeneous representation)
    - Finite examples
    - Criteria for an ovoid to be a quadric
    - 'Generalization: semi ovoid'
    - Notes
categories:
    - Incidence geometry
---
In projective geometry an ovoid is a sphere like pointset (surface) in a projective space of dimension d ≥ 3. Simple examples in a real projective space are hyperspheres (quadrics). The essential geometric properties of an ovoid 
  
    
      
        
          
            O
          
        
      
    
    {\displaystyle {\mathcal {O}}}
  
 are:
