---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eugenius_Nulty
offline_file: ""
offline_thumbnail: ""
uuid: e6125e90-200e-494b-8fc2-c633178809d1
updated: 1484309091
title: Eugenius Nulty
categories:
    - Geometers
---
Eugenius Nulty (1790 – July 3, 1871) was an Irish born American mathematician of the 19th century.[1] He served on the faculty of Dickinson College from 1814 to 1816, and later taught and tutored prominent Philadelphians, including the brothers Mathew Carey Lea and Henry Charles Lea.
