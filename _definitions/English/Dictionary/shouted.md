---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shouted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484478662
title: shouted
categories:
    - Dictionary
---
shouted
     adj : in a vehement outcry; "his shouted words of encouragement
           could be heard over the crowd noises" [syn: {yelled}]
