---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wage
offline_file: ""
offline_thumbnail: ""
uuid: 00ade9fc-eba2-483e-af88-4442501850f7
updated: 1484310455
title: wage
categories:
    - Dictionary
---
wage
     n : something that remunerates; "wages were paid by check"; "he
         wasted his pay on drink"; "they saved a quarter of all
         their earnings" [syn: {pay}, {earnings}, {remuneration},
         {salary}]
     v : as of wars, battles, or campaigns; "Napoleon and Hitler
         waged war against all of Europe" [syn: {engage}]
