---
version: 1
type: video
provider: YouTube
id: C6F33Ir-sY4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Functions%20and%20their%20graphs/Domain%20of%20modeling%20functions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: efee3b66-e1b8-412f-b6e1-9146ea032e57
updated: 1486069563
title: Domain of modeling functions
categories:
    - Functions and their graphs
thumbnail_urls:
    - https://i3.ytimg.com/vi/C6F33Ir-sY4/default.jpg
    - https://i3.ytimg.com/vi/C6F33Ir-sY4/1.jpg
    - https://i3.ytimg.com/vi/C6F33Ir-sY4/2.jpg
    - https://i3.ytimg.com/vi/C6F33Ir-sY4/3.jpg
---
