---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Van_der_Pol_oscillator
offline_file: ""
offline_thumbnail: ""
uuid: 3f35d247-86d7-4f75-98e9-a26eb3b1f8e0
updated: 1484308719
title: Van der Pol oscillator
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-VanDerPolOscillator.png
tags:
    - History
    - Two-dimensional form
    - Results for the unforced oscillator
    - Hamiltonian for Van der Pol oscillator
    - Forced Van der Pol oscillator
    - Popular culture
categories:
    - Chaotic maps
---
In dynamics, the Van der Pol oscillator is a non-conservative oscillator with non-linear damping. It evolves in time according to the second-order differential equation:
