---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eyed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484499481
title: eyed
categories:
    - Dictionary
---
eyed
     adj : having an eye or eyes or eyelike feature especially as
           specified; often used in combination; "a peacock's eyed
           feathers"; "red-eyed" [ant: {eyeless}]
