---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/funding
offline_file: ""
offline_thumbnail: ""
uuid: 41d86f6e-f141-4e00-9592-11d61c522d32
updated: 1484310170
title: funding
categories:
    - Dictionary
---
funding
     n 1: financial resources provided to make some project possible;
          "the foundation provided support for the experiment"
          [syn: {support}, {financial support}, {backing}, {financial
          backing}]
     2: the act of financing [syn: {financing}]
