---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/naturalist
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484383381
title: naturalist
categories:
    - Dictionary
---
naturalist
     n 1: an advocate of the doctrine that the world can be understood
          in scientific terms
     2: a biologist knowledgeable about natural history (especially
        botany and zoology) [syn: {natural scientist}]
