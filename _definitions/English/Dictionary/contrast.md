---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contrast
offline_file: ""
offline_thumbnail: ""
uuid: 3ef00df0-e92e-4f40-b13f-e6410adb43ba
updated: 1484310439
title: contrast
categories:
    - Dictionary
---
contrast
     n 1: the opposition or dissimilarity of things that are compared;
          "in contrast to", "by contrast" [syn: {direct contrast}]
     2: the act of distinguishing by comparing differences
     3: a conceptual separation or demarcation; "there is a narrow
        line between sanity and insanity" [syn: {line}, {dividing
        line}, {demarcation}]
     4: the perceptual effect of the juxtaposition of very different
        colors
     5: the range of optical density and tone on a photographic
        negative or print (or the extent to which adjacent areas
        on a television screen differ in brightness)
     v 1: put in opposition to show or emphasize differences; ...
