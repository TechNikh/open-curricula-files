---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Probabilistic_method
offline_file: ""
offline_thumbnail: ""
uuid: d8a5dc59-2b3f-4bd0-a92b-f4b66568fcfc
updated: 1484308590
title: Probabilistic method
tags:
    - Introduction
    - Two examples due to Erdős
    - First example
    - Second example
    - Footnotes
categories:
    - Combinatorics
---
The probabilistic method is a nonconstructive method, primarily used in combinatorics and pioneered by Paul Erdős, for proving the existence of a prescribed kind of mathematical object. It works by showing that if one randomly chooses objects from a specified class, the probability that the result is of the prescribed kind is more than zero. Although the proof uses probability, the final conclusion is determined for certain, without any possible error.
