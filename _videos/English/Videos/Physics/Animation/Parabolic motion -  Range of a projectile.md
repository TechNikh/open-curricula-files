---
version: 1
type: video
provider: YouTube
id: N0H-rv9XFHk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Parabolic%20motion%20-%20%20Range%20of%20a%20projectile.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 38cd9abf-4b59-484c-91b2-d82d2832b871
updated: 1486069652
title: 'Parabolic motion -  Range of a projectile'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/N0H-rv9XFHk/default.jpg
    - https://i3.ytimg.com/vi/N0H-rv9XFHk/1.jpg
    - https://i3.ytimg.com/vi/N0H-rv9XFHk/2.jpg
    - https://i3.ytimg.com/vi/N0H-rv9XFHk/3.jpg
---
