---
version: 1
type: article
id: https://en.wikipedia.org/wiki/De_divina_proportione
offline_file: ""
offline_thumbnail: ""
uuid: 66c55dbf-de9e-4320-8985-9d68b245e3ce
updated: 1484309245
title: De divina proportione
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Divina_proportione.png
tags:
    - Contents of the book
    - Compendio divina proportione
    - "Trattato dell'architettura"
    - Libellus in tres partiales divisus
    - Illustrations
    - History
categories:
    - History of geometry
---
De divina proportione (On the Divine Proportion) is a book on mathematics written by Luca Pacioli and illustrated by Leonardo da Vinci, composed around 1498 in Milan and first printed in 1509.[1] Its subject was mathematical proportions (the title refers to the golden ratio) and their applications to geometry, visual art through perspective, and architecture. The clarity of the written material and Leonardo's excellent diagrams helped the book to achieve an impact beyond mathematical circles, popularizing contemporary geometric concepts and images.[2][3]
