---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hole
offline_file: ""
offline_thumbnail: ""
uuid: 689bc38b-ebfa-4217-943d-a4c553d3732d
updated: 1484310315
title: hole
categories:
    - Dictionary
---
hole
     n 1: an opening into or through something
     2: an opening deliberately made in or through something
     3: one playing period (from tee to green) on a golf course; "he
        played 18 holes" [syn: {golf hole}]
     4: an unoccupied space
     5: a depression hollowed out of solid matter [syn: {hollow}]
     6: a fault; "he shot holes in my argument"
     7: informal terms for a difficult situation; "he got into a
        terrible fix"; "he made a muddle of his marriage" [syn: {fix},
         {jam}, {mess}, {muddle}, {pickle}, {kettle of fish}]
     8: informal terms for the mouth [syn: {trap}, {cakehole}, {maw},
         {yap}, {gob}]
     v 1: hit the ball into the hole ...
