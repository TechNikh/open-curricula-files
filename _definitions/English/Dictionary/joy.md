---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/joy
offline_file: ""
offline_thumbnail: ""
uuid: 0c9da5b6-6fb3-48af-b4d3-a3b1d8b7646d
updated: 1484310461
title: joy
categories:
    - Dictionary
---
joy
     n 1: the emotion of great happiness [syn: {joyousness}, {joyfulness}]
          [ant: {sorrow}]
     2: something or someone that provides pleasure; a source of
        happiness; "a joy to behold"; "the pleasure of his
        company"; "the new car is a delight" [syn: {delight}, {pleasure}]
     v 1: feel happiness or joy [syn: {rejoice}]
     2: make glad or happy [syn: {gladden}] [ant: {sadden}]
