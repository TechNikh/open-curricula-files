---
version: 1
type: video
provider: YouTube
id: gvwKv6F69F0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra-%20Equation%20of%20a%20line.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ef953a46-0308-42cd-bd9f-ed02ed0c8d28
updated: 1486069571
title: 'Algebra- Equation of a line'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/gvwKv6F69F0/default.jpg
    - https://i3.ytimg.com/vi/gvwKv6F69F0/1.jpg
    - https://i3.ytimg.com/vi/gvwKv6F69F0/2.jpg
    - https://i3.ytimg.com/vi/gvwKv6F69F0/3.jpg
---
Determining the equation of a line
More free lessons at: http://www.khanacademy.org/video?v=gvwKv6F69F0
