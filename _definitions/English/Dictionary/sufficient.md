---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sufficient
offline_file: ""
offline_thumbnail: ""
uuid: 51bce736-d553-485f-a180-d768aa4b617a
updated: 1484310258
title: sufficient
categories:
    - Dictionary
---
sufficient
     adj : of a quantity that can fulfill a need or requirement but
           without being abundant; "sufficient food" [ant: {insufficient}]
