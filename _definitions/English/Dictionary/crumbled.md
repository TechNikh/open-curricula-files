---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crumbled
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484579821
title: crumbled
categories:
    - Dictionary
---
crumbled
     adj : broken into small fragments; "crumbled cookies" [syn: {fragmented}]
