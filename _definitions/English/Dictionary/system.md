---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/system
offline_file: ""
offline_thumbnail: ""
uuid: 43fce5e8-a092-42ed-bc32-cd7dfeaad93f
updated: 1484310363
title: system
categories:
    - Dictionary
---
system
     n 1: a group of independent but interrelated elements comprising
          a unified whole; "a vast system of production and
          distribution and consumption keep the country going"
          [syn: {scheme}]
     2: instrumentality that combines interrelated interacting
        artifacts designed to work as a coherent entity; "he
        bought a new stereo system"; "the system consists of a
        motor and a small computer"
     3: a complex of methods or rules governing behavior; "they have
        to operate under a system they oppose"; "that language has
        a complex system for indicating gender" [syn: {system of
        rules}]
     4: a procedure or process ...
