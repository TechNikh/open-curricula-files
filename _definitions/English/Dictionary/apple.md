---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apple
offline_file: ""
offline_thumbnail: ""
uuid: c21fa480-f31e-413b-a3fd-7e4d08ee3ac6
updated: 1484310346
title: apple
categories:
    - Dictionary
---
apple
     n 1: fruit with red or yellow or green skin and sweet to tart
          crisp whitish flesh
     2: native Eurasian tree widely cultivated in many varieties for
        its firm rounded edible fruits [syn: {orchard apple tree},
         {Malus pumila}]
