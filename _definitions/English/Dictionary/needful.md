---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/needful
offline_file: ""
offline_thumbnail: ""
uuid: 2c38581f-d496-449c-b5da-40c135970f61
updated: 1484310150
title: needful
categories:
    - Dictionary
---
needful
     adj : necessary for relief or supply; "provided them with all
           things needful" [syn: {needed}, {required}, {requisite}]
