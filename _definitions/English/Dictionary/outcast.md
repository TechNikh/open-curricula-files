---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outcast
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484596381
title: outcast
categories:
    - Dictionary
---
outcast
     adj : excluded from a society [syn: {friendless}]
     n : a person who is rejected (from society or home) [syn: {castaway},
          {pariah}, {Ishmael}]
