---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bossy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414641
title: bossy
categories:
    - Dictionary
---
bossy
     adj : offensively self-assured or given to exercising usually
           unwarranted power; "an autocratic person"; "autocratic
           behavior"; "a bossy way of ordering others around"; "a
           rather aggressive and dominating character"; "managed
           the employees in an aloof magisterial way"; "a
           swaggering peremptory manner" [syn: {autocratic}, {dominating},
            {high-and-mighty}, {magisterial}, {peremptory}]
     [also: {bossiest}, {bossier}]
