---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Standard_conditions_for_temperature_and_pressure
offline_file: ""
offline_thumbnail: ""
uuid: 088dcad9-da70-4fc6-a105-448dd9cf9792
updated: 1484308866
title: Standard conditions for temperature and pressure
tags:
    - Definitions
    - Past use
    - Current use
    - International Standard Atmosphere
    - Standard laboratory conditions
    - Molar volume of a gas
    - Notes
categories:
    - Thermodynamics
---
Standard conditions for temperature and pressure are standard sets of conditions for experimental measurements to be established to allow comparisons to be made between different sets of data. The most used standards are those of the International Union of Pure and Applied Chemistry (IUPAC) and the National Institute of Standards and Technology (NIST), although these are not universally accepted standards. Other organizations have established a variety of alternative definitions for their standard reference conditions.
