---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cube_(algebra)
offline_file: ""
offline_thumbnail: ""
uuid: 8ee50aae-7c6e-440e-a3f4-148c22f57a8c
updated: 1484308639
title: Cube (algebra)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-CubeChart.svg.png
tags:
    - In integers
    - Base ten
    - "Waring's problem for cubes"
    - "Fermat's last theorem for cubes"
    - Sum of first n cubes
    - Sum of cubes of numbers in arithmetic progression
    - Cubes as sums of successive odd integers
    - In rational numbers
    - In real numbers, other fields, and rings
    - History
    - Notes
categories:
    - Number theory
---
It is also the number multiplied by its square:
