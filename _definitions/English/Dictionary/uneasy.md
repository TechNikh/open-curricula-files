---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uneasy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484575141
title: uneasy
categories:
    - Dictionary
---
uneasy
     adj 1: lacking a sense of security or affording no ease or
            reassurance; "farmers were uneasy until rain finally
            came"; "uneasy about his health"; "gave an uneasy
            laugh"; "uneasy lies the head that wears the crown";
            "an uneasy coalition government"; "an uneasy calm";
            "an uneasy silence fell on the group" [ant: {easy}]
     2: causing or fraught with or showing anxiety; "spent an
        anxious night waiting for the test results"; "cast anxious
        glances behind her"; "those nervous moments before
        takeoff"; "an unquiet mind" [syn: {anxious}, {nervous}, {unquiet}]
     3: marked by a lack of quiet; not ...
