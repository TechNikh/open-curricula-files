---
version: 1
type: video
provider: YouTube
id: zQMU-lsMb3U
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Dividing%20fractions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 27a5658e-c125-4709-8425-d09b4e4e71ff
updated: 1486069565
title: Dividing fractions
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/zQMU-lsMb3U/default.jpg
    - https://i3.ytimg.com/vi/zQMU-lsMb3U/1.jpg
    - https://i3.ytimg.com/vi/zQMU-lsMb3U/2.jpg
    - https://i3.ytimg.com/vi/zQMU-lsMb3U/3.jpg
---
Dividing fractions
