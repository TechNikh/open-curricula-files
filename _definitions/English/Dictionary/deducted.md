---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deducted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484316901
title: deducted
categories:
    - Dictionary
---
deducted
     adj : taken off or taken away from a total; "take-home pay is what
           is left after subtraction of deducted taxes"
