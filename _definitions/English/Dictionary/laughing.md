---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laughing
offline_file: ""
offline_thumbnail: ""
uuid: 94f1477a-a2cc-406a-a775-3b6dfa609b2c
updated: 1484310517
title: laughing
categories:
    - Dictionary
---
laughing
     adj : showing or feeling mirth or pleasure or happiness; "laughing
           children" [syn: {laughing(a)}, {riant}]
