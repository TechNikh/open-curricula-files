---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Comma_code
offline_file: ""
offline_thumbnail: ""
uuid: cd1dde34-0962-48b3-87e7-b04da7a55dcf
updated: 1484308563
title: Comma code
categories:
    - Coding theory
---
A comma code is a type of prefix-free code in which a comma, a particular symbol or sequence of symbols, occurs at the end of a code word and never occurs otherwise.[1]
