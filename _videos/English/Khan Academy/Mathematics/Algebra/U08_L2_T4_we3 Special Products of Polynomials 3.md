---
version: 1
type: video
provider: YouTube
id: jYfWFy0yxB4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/U08_L2_T4_we3%20Special%20Products%20of%20Polynomials%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 117c0903-d76e-4606-87b4-b1d81ef64028
updated: 1486069563
title: U08_L2_T4_we3 Special Products of Polynomials 3
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/jYfWFy0yxB4/default.jpg
    - https://i3.ytimg.com/vi/jYfWFy0yxB4/1.jpg
    - https://i3.ytimg.com/vi/jYfWFy0yxB4/2.jpg
    - https://i3.ytimg.com/vi/jYfWFy0yxB4/3.jpg
---
