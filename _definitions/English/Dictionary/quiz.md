---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quiz
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484315341
title: quiz
categories:
    - Dictionary
---
quiz
     n : an examination consisting of a few short questions
     v : examine someone's knowledge of something; "The teacher tests
         us every week"; "We got quizzed on French irregular
         verbs" [syn: {test}]
     [also: {quizzing}, {quizzes}, {quizzed}, {quizzes} (pl)]
