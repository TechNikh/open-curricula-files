---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Online_travel_auction
offline_file: ""
offline_thumbnail: ""
uuid: 50605f3c-505d-489f-b840-04729eb20fc9
updated: 1484309017
title: Online travel auction
categories:
    - Auctions
---
The term online travel auction is a system of buying and selling travel products and services online by offering them up for auction and then awarding the item to the highest bidder.[1] The need for travel auctions emanated principally due to the high cost of travel. This high cost is also what led to the growth in popularity of low-cost carriers, a concept initially pioneered by Southwest Airlines, and later mimicked by Ryanair.
