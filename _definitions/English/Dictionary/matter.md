---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/matter
offline_file: ""
offline_thumbnail: ""
uuid: e4a5df9d-ba96-46e7-85d2-05e3526e8d37
updated: 1484310321
title: matter
categories:
    - Dictionary
---
matter
     n 1: that which has mass and occupies space; "an atom is the
          smallest indivisible unit of matter" [syn: {substance}]
     2: a vaguely specified concern; "several matters to attend to";
        "it is none of your affair"; "things are going well" [syn:
         {affair}, {thing}]
     3: some situation or event that is thought about; "he kept
        drifting off the topic"; "he had been thinking about the
        subject for several years"; "it is a matter for the
        police" [syn: {topic}, {subject}, {issue}]
     4: a problem; "is anything the matter?"
     5: (used with negation) having consequence; "they were friends
        and it was no matter who won the ...
