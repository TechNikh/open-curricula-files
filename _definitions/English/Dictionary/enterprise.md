---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enterprise
offline_file: ""
offline_thumbnail: ""
uuid: 88bb98c9-cd36-4792-b15d-1b4610f45132
updated: 1484310521
title: enterprise
categories:
    - Dictionary
---
enterprise
     n 1: a purposeful or industrious undertaking (especially one that
          requires effort or boldness); "he had doubts about the
          whole enterprise" [syn: {endeavor}, {endeavour}]
     2: an organization created for business ventures; "a growing
        enterprise must have a bold leader"
     3: readiness to embark on bold new ventures [syn: {enterprisingness},
         {initiative}, {go-ahead}]
