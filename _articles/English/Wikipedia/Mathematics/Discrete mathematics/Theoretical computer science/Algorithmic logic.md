---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algorithmic_logic
offline_file: ""
offline_thumbnail: ""
uuid: 6ca883a4-087d-4b2a-91a4-911754b5f455
updated: 1484308671
title: Algorithmic logic
categories:
    - Theoretical computer science
---
Algorithmic logic is a calculus of programs which allows the expression of semantic properties of programs by appropriate logical formulas. It provides a framework that enables proving the formulas from the axioms of program constructs such as assignment, iteration and composition instructions and from the axioms of the data structures in question see Mirkowska & Salwicki (1987), Banachowski et al. (1977).
