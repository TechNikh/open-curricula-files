---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/title
offline_file: ""
offline_thumbnail: ""
uuid: 7328b103-79a1-4e4c-a0b2-c334b2fb5b57
updated: 1484310595
title: title
categories:
    - Dictionary
---
title
     n 1: a heading that names a statute or legislative bill; may give
          a brief summary of the matters it deals with; "Title 8
          provided federal help for schools" [syn: {statute title},
           {rubric}]
     2: the name of a work of art or literary composition etc.; "he
        looked for books with the word `jazz' in the title"; "he
        refused to give titles to his paintings"; "I can never
        remember movie titles"
     3: a general or descriptive heading for a section of a written
        work; "the novel had chapter titles"
     4: the status of being a champion; "he held the title for two
        years" [syn: {championship}]
     5: a legal document ...
