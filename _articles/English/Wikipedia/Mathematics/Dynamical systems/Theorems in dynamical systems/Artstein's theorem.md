---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Artstein%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 6ee9e7c8-b1b1-42c3-9ab7-d20b4b3c366c
updated: 1484308692
title: "Artstein's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_3.jpg
categories:
    - Theorems in dynamical systems
---
Artstein's theorem states that a dynamical system has a differentiable control-Lyapunov function if and only if there exists a regular stabilizing feedback: 
  
    
      
        u
        (
        x
        )
      
    
    {\displaystyle u(x)}
  
.[1]
