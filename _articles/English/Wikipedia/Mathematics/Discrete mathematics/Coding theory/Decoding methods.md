---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Decoding_methods
offline_file: ""
offline_thumbnail: ""
uuid: c4f065a8-0b7d-405e-b86d-0cf417ec14d2
updated: 1484308539
title: Decoding methods
tags:
    - Notation
    - Ideal observer decoding
    - Decoding conventions
    - Maximum likelihood decoding
    - Minimum distance decoding
    - Syndrome decoding
    - Partial response maximum likelihood
    - Viterbi decoder
    - Sources
categories:
    - Coding theory
---
In coding theory, decoding is the process of translating received messages into codewords of a given code. There have been many common methods of mapping messages to codewords. These are often used to recover messages sent over a noisy channel, such as a binary symmetric channel.
