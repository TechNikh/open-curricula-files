---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/centre
offline_file: ""
offline_thumbnail: ""
uuid: cb22c4eb-9760-42a2-b8d0-68fceabd6c21
updated: 1484310365
title: centre
categories:
    - Dictionary
---
Centre
     n 1: a low-lying region in central France
     2: an area that is approximately central within some larger
        region; "it is in the center of town"; "they ran forward
        into the heart of the struggle"; "they were in the eye of
        the storm" [syn: {center}, {middle}, {heart}, {eye}]
     3: a point equidistant from the ends of a line or the
        extremities of a figure [syn: {center}, {midpoint}]
     4: a place where some particular activity is concentrated;
        "they received messages from several centers" [syn: {center}]
     5: the sweet central portion of a piece of candy that is
        enclosed in chocolate or some other covering [syn: {center}]
     ...
