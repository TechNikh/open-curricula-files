---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/atmosphere
offline_file: ""
offline_thumbnail: ""
uuid: 6518f25f-8611-467c-8789-36fa255caf46
updated: 1484310268
title: atmosphere
categories:
    - Dictionary
---
atmosphere
     n 1: a particular environment or surrounding influence; "there
          was an atmosphere of excitement" [syn: {ambiance}, {ambience}]
     2: a unit of pressure: the pressure that will support a column
        of mercury 760 mm high at sea level and 0 degrees
        centigrade [syn: {standard atmosphere}, {atm}, {standard
        pressure}]
     3: the mass of air surrounding the Earth; "there was great heat
        as the comet entered the atmosphere"; "it was exposed to
        the air" [syn: {air}]
     4: the weather or climate at some place; "the atmosphere was
        thick with fog" [syn: {atmospheric state}]
     5: the envelope of gases surrounding any celestial ...
