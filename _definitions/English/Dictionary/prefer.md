---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prefer
offline_file: ""
offline_thumbnail: ""
uuid: 04460562-1f14-47eb-a583-3b3cdf3156d2
updated: 1484310343
title: prefer
categories:
    - Dictionary
---
prefer
     v 1: like better; value more highly; "Some people prefer camping
          to staying in hotels"; "We prefer sleeping outside"
     2: select as an alternative; choose instead; prefer as an
        alternative; "I always choose the fish over the meat
        courses in this restaurant"; "She opted for the job on the
        East coast" [syn: {choose}, {opt}]
     3: promote over another; "he favors his second daughter" [syn:
        {favor}, {favour}]
     4: give preference to one creditor over another
     [also: {preferring}, {preferred}]
