---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Labs_septic
offline_file: ""
offline_thumbnail: ""
uuid: f2d3f66b-43a0-4d13-9a12-90b257e93f72
updated: 1484309363
title: Labs septic
categories:
    - Geometry stubs
---
In mathematics, the Labs septic surface is a degree-7 (septic) nodal surface with 99 nodes found by Labs (2006). As of 2015, it has the largest known number of nodes of a degree-7 surface, though this number is still less than the best known upper bound of 104 nodes given by Giventalʹ (1983) and Varchenko (1983).
