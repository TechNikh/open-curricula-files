---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalized_game
offline_file: ""
offline_thumbnail: ""
uuid: 431946ad-580a-46a6-8238-49e54c4da1e7
updated: 1484308542
title: Generalized game
categories:
    - Combinatorial game theory
---
In computational complexity theory, a generalized game is a game that has been generalized so that it can be played on a board of any size. For example, generalized chess is the game of chess played on an n-by-n board, with 2n pieces on each side.
