---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Varoli
offline_file: ""
offline_thumbnail: ""
uuid: b496cfdf-c42b-4d42-bce4-959b365ca3fb
updated: 1484308351
title: John Varoli
categories:
    - Uncategorized
---
John Varoli is an American public relations practitioner, who worked as a journalist in Russia from 1997 to 2010. In that period he wrote about 3000 articles for publications including the St Petersburg Times, Radio Free Europe, the New York Times,[1] the International Herald Tribune, Bloomberg News,[2] The Art Newspaper,[3] Art & Auction magazine, Art Khronika, and many more.
