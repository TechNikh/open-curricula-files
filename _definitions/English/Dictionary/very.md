---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/very
offline_file: ""
offline_thumbnail: ""
uuid: 9db7f8ab-dcea-453f-80a5-a315df2325a0
updated: 1484310325
title: very
categories:
    - Dictionary
---
very
     adj 1: precisely as stated; "the very center of town" [syn: {very(a)}]
     2: being the exact same one; not any other:; "this is the
        identical room we stayed in before"; "the themes of his
        stories are one and the same"; "saw the selfsame quotation
        in two newspapers"; "on this very spot"; "the very thing
        he said yesterday"; "the very man I want to see" [syn: {identical},
         {one and the same(p)}, {selfsame(a)}, {very(a)}]
     3: used to give emphasis to the relevance of the thing
        modified; "his very name struck terror"; "caught in the
        very act" [syn: {very(a)}]
     4: used to give emphasis; "the very essence of artistic
      ...
