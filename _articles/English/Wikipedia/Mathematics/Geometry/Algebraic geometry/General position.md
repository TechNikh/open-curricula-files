---
version: 1
type: article
id: https://en.wikipedia.org/wiki/General_position
offline_file: ""
offline_thumbnail: ""
uuid: 55551623-68fb-4600-8037-5578d1425c08
updated: 1484309149
title: General position
tags:
    - General linear position
    - More generally
    - Different geometries
    - General type
    - Other contexts
    - General position for Delaunay triangulations in the plane
    - 'Abstractly: configuration spaces'
    - Notes
categories:
    - Algebraic geometry
---
In algebraic geometry, general position is a notion of genericity for a set of points, or other geometric objects. It means the general case situation, as opposed to some more special or coincidental cases that are possible, which is referred to as special position. Its precise meaning differs in different settings.
