---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethyne
offline_file: ""
offline_thumbnail: ""
uuid: ddba9382-37ed-4f52-b1e2-0ce4da19c142
updated: 1484310414
title: ethyne
categories:
    - Dictionary
---
ethyne
     n : a colorless flammable gas used chiefly in welding and in
         organic synthesis [syn: {acetylene}, {alkyne}]
