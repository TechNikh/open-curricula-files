---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preach
offline_file: ""
offline_thumbnail: ""
uuid: b2293cc6-7a83-450b-a6c2-6ff4b4937908
updated: 1484310188
title: preach
categories:
    - Dictionary
---
preach
     v 1: deliver a sermon; "The minister is not preaching this
          Sunday" [syn: {prophesy}]
     2: speak, plead, or argue in favour of; "The doctor advocated a
        smoking ban in the entire house" [syn: {advocate}]
