---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Group-envy-free
offline_file: ""
offline_thumbnail: ""
uuid: c0688ca9-cfe4-457b-b4ec-193783371bf0
updated: 1484308518
title: Group-envy-free
tags:
    - Definitions
    - Relations to other criteria
    - Existence
categories:
    - Fair division
---
A group-envy-free[1] division (also known as: coalition-fair[2] division) is a division of a resource among several partners such that every group of partners feel that their allocated share is at least as good as the share of any other group with the same size. The term is used especially in problems of fair division, such as resource allocation and fair cake-cutting.
