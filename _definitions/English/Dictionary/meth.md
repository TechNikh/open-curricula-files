---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meth
offline_file: ""
offline_thumbnail: ""
uuid: d8d73cdc-6c3d-4f27-a9e3-5a5d69288040
updated: 1484310421
title: meth
categories:
    - Dictionary
---
meth
     n : amphetamine used in the form of a crystalline hydrochloride;
         used as a stimulant to the nervous system and as an
         appetite suppressant [syn: {methamphetamine}, {methamphetamine
         hydrochloride}, {Methedrine}, {deoxyephedrine}, {chalk},
         {chicken feed}, {crank}, {glass}, {ice}, {shabu}, {trash}]
