---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Primefree_sequence
offline_file: ""
offline_thumbnail: ""
uuid: 8294802b-118b-4009-a65c-3115cbab7222
updated: 1484308652
title: Primefree sequence
tags:
    - "Wilf's sequence"
    - Nontriviality
    - Other sequences
    - Notes
categories:
    - Number theory
---
In mathematics, a primefree sequence is a sequence of integers that does not contain any prime numbers. More specifically, it usually means a sequence defined by the same recurrence relation as the Fibonacci numbers, but with different initial conditions causing all members of the sequence to be composite numbers that do not all have a common divisor. To put it algebraically, a sequence of this type is defined by an appropriate choice of two composite numbers a1 and a2, such that the greatest common divisor GCD(a1,a2) = 1, and such that for n > 2 there are no primes in the sequence of numbers calculated from the formula
