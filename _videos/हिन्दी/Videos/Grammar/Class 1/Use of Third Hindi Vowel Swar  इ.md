---
version: 1
type: video
provider: YouTube
id: Tv1VGTjW7lo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d13bea67-9f04-4221-ade5-55750dd60087
updated: 1486069629
title: 'Use of Third Hindi Vowel Swar  इ'
categories:
    - Class 1
thumbnail_urls:
    - https://i3.ytimg.com/vi/Tv1VGTjW7lo/default.jpg
    - https://i3.ytimg.com/vi/Tv1VGTjW7lo/1.jpg
    - https://i3.ytimg.com/vi/Tv1VGTjW7lo/2.jpg
    - https://i3.ytimg.com/vi/Tv1VGTjW7lo/3.jpg
---
