---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/writings
offline_file: ""
offline_thumbnail: ""
uuid: 3b85bc32-2098-4ac1-b7bf-77c9d75c3740
updated: 1484310414
title: writings
categories:
    - Dictionary
---
Writings
     n : the third of three divisions of the Hebrew Scriptures [syn:
         {Hagiographa}, {Ketubim}]
