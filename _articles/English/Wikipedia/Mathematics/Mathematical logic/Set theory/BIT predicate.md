---
version: 1
type: article
id: https://en.wikipedia.org/wiki/BIT_predicate
offline_file: ""
offline_thumbnail: ""
uuid: 9f26f43c-50c4-44d3-9182-bc47f57c8c9e
updated: 1484309499
title: BIT predicate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/316px-Rado_graph.svg.png
tags:
    - History
    - Implementation
    - Private information retrieval
    - Mathematical logic
    - Construction of the Rado graph
categories:
    - Set theory
---
In mathematics and computer science, the BIT predicate or Ackermann coding, sometimes written BIT(i, j), is a predicate which tests whether the jth bit of the number i is 1, when i is written in binary.
