---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Feedback_vertex_set
offline_file: ""
offline_thumbnail: ""
uuid: 89c0952b-4ef9-4f68-8b2e-e6a26d931d0b
updated: 1484309389
title: Feedback vertex set
tags:
    - Definition
    - NP-hardness
    - Exact algorithms
    - Approximation
    - Bounds
    - Applications
    - Notes
    - Research articles
    - Textbooks and survey articles
categories:
    - Computational problems in graph theory
---
In the mathematical discipline of graph theory, a feedback vertex set of a graph is a set of vertices whose removal leaves a graph without cycles. In other words, each feedback vertex set contains at least one vertex of any cycle in the graph. The feedback vertex set problem is an NP-complete problem in computational complexity theory. It was among the first problems shown to be NP-complete. It has wide applications in operating systems, database systems, and VLSI chip design.
