---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stochastic_geometry
offline_file: ""
offline_thumbnail: ""
uuid: c15b0159-6ed6-4f65-bb59-fb7e37b39ddd
updated: 1484309255
title: Stochastic geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-BooleanCellCoverage.jpg
tags:
    - Models
    - Random object
    - Line and hyper-flat processes
    - Origin of the name
    - Applications
categories:
    - Integral geometry
---
In mathematics, stochastic geometry is the study of random spatial patterns. At the heart of the subject lies the study of random point patterns. This leads to the theory of spatial point processes, hence notions of Palm conditioning, which extend to the more abstract setting of random measures.
