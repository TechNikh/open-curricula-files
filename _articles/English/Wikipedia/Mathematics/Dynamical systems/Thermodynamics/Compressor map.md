---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Compressor_map
offline_file: ""
offline_thumbnail: ""
uuid: 2bd4ef3a-c131-4a9a-8d83-188692972f23
updated: 1484308837
title: Compressor map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Hpcompressorchic.gif
tags:
    - High pressure compressor map
    - Flow axis
    - Pressure ratio axis
    - Surge line
    - Surge margin
    - Speed lines
    - Efficiency axis
    - Working line
    - Fan map
    - IP compressor map
categories:
    - Thermodynamics
---
A compressor map is a chart created for a compressor in a gas turbine engine. Complete maps are based on compressor rig test results or predicted by a special computer program. Alternatively the map of a similar compressor can be suitably scaled.
