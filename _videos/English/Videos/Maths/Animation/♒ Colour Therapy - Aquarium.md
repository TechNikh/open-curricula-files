---
version: 1
type: video
provider: YouTube
id: 1purcP6UpNg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cfb2c4b9-c8b6-4a92-9bd2-32af59ed2286
updated: 1486069643
title: '♒ Colour Therapy - Aquarium'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/1purcP6UpNg/default.jpg
    - https://i3.ytimg.com/vi/1purcP6UpNg/1.jpg
    - https://i3.ytimg.com/vi/1purcP6UpNg/2.jpg
    - https://i3.ytimg.com/vi/1purcP6UpNg/3.jpg
---
