---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attractive
offline_file: ""
offline_thumbnail: ""
uuid: f5978571-5b2f-4495-bb62-0bbf5c5f983b
updated: 1484310346
title: attractive
categories:
    - Dictionary
---
attractive
     adj 1: pleasing to the eye or mind especially through beauty or
            charm; "a remarkably attractive young man"; "an
            attractive personality"; "attractive clothes"; "a book
            with attractive illustrations" [ant: {unattractive}]
     2: having power to arouse interest; "an attractive
        opportunity"; "the job is attractive because of the pay"
     3: having the properties of a magnet; the ability to draw or
        pull; "an attractive force"; "the knife hung on a magnetic
        board" [syn: {attractive(a)}, {magnetic}] [ant: {repulsive(a)}]
