---
version: 1
type: video
provider: YouTube
id: uas_HJNAzfw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f216701c-a19e-44c5-b0b3-4f01d4076ab9
updated: 1486069647
title: Fractales en la naturaleza
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/uas_HJNAzfw/default.jpg
    - https://i3.ytimg.com/vi/uas_HJNAzfw/1.jpg
    - https://i3.ytimg.com/vi/uas_HJNAzfw/2.jpg
    - https://i3.ytimg.com/vi/uas_HJNAzfw/3.jpg
---
