---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Applications_of_multiple_coordinate_systems
offline_file: ""
offline_thumbnail: ""
uuid: 77283a06-d987-48c8-a7b6-22603ee40605
updated: 1484308724
title: Applications of multiple coordinate systems
categories:
    - Control engineering
---
The application of multiple coordinate systems is an effective tool in control systems. The usage of multiple coordinate systems can improve the efficiency of calculations as well as enhance clarity in operations.
