---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/powerful
offline_file: ""
offline_thumbnail: ""
uuid: 89415a8b-c3a7-448f-a8fb-7c2add09a56a
updated: 1484310316
title: powerful
categories:
    - Dictionary
---
powerful
     adj 1: having great power or force or potency or effect; "the most
            powerful government in western Europe"; "his powerful
            arms"; "a powerful bomb"; "the horse's powerful kick";
            "powerful drugs"; "a powerful argument" [ant: {powerless}]
     2: strong enough to knock down or overwhelm; "a knock-down
        blow" [syn: {knock-down(a)}]
     3: having the power to influence or convince; "a cogent
        analysis of the problem"; "potent arguments" [syn: {cogent},
         {potent}]
     4: (of a person) possessing physical strength and weight;
        rugged and powerful; "a hefty athlete"; "a muscular
        boxer"; "powerful arms" [syn: ...
