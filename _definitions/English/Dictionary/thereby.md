---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thereby
offline_file: ""
offline_thumbnail: ""
uuid: 3d9e128e-286f-428e-bcbd-e4ad65810530
updated: 1484310323
title: thereby
categories:
    - Dictionary
---
thereby
     adv : by that means or because of that; "He knocked over the red
           wine, thereby ruining the table cloth"
