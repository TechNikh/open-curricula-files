---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purely
offline_file: ""
offline_thumbnail: ""
uuid: 66f64897-6529-48e3-b429-dd7cd8365e51
updated: 1484310397
title: purely
categories:
    - Dictionary
---
purely
     adv : restricted to something; "we talked strictly business" [syn:
            {strictly}]
