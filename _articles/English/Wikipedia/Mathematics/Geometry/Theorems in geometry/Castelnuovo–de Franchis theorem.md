---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Castelnuovo%E2%80%93de_Franchis_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 512eb867-5c9e-416e-a43e-6c3e1eac969c
updated: 1484309111
title: Castelnuovo–de Franchis theorem
categories:
    - Theorems in geometry
---
In mathematics, the Castelnuovo–de Franchis theorem is a classical result on complex algebraic surfaces. Let X be such a surface, projective and non-singular, and let
