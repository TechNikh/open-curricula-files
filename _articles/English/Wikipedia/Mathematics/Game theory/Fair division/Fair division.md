---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fair_division
offline_file: ""
offline_thumbnail: ""
uuid: 5239daca-1f40-4d58-937d-4ccb05bf676d
updated: 1484308524
title: Fair division
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Berlin_Blockade-map.svg.png
tags:
    - Definitions
    - What is divided?
    - What is fair?
    - Procedures
    - Two players
    - Many players
    - Nonadditive Utility
    - Variants
    - History
    - In popular culture
categories:
    - Fair division
---
Fair division is the problem of dividing a set of goods or resources between several people who have an entitlement to them, such that each person receives his/her due share. This problem arises in various real-world settings: auctions, divorce settlements, electronic spectrum and frequency allocation, airport traffic management, or exploitation of Earth Observation Satellites. This is an active research area in Mathematics, Economics (especially Social choice theory), Game theory, Dispute resolution, and more. The central tenet of fair division is that such a division should be performed by the players themselves, maybe using a mediator but certainly not an arbiter as only the players ...
