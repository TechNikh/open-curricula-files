---
version: 1
type: video
provider: YouTube
id: W8UI7F43_Yk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Nebulae-%20Crash%20Course%20Astronomy%20%2336.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c53ffa1e-36e9-45cf-b3c3-517c5c31ce12
updated: 1486069684
title: 'Nebulae- Crash Course Astronomy #36'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/W8UI7F43_Yk/default.jpg
    - https://i3.ytimg.com/vi/W8UI7F43_Yk/1.jpg
    - https://i3.ytimg.com/vi/W8UI7F43_Yk/2.jpg
    - https://i3.ytimg.com/vi/W8UI7F43_Yk/3.jpg
---
