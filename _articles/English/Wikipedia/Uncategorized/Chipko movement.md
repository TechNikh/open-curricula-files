---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chipko
offline_file: ""
offline_thumbnail: ""
uuid: 52666cef-672c-4833-9273-d5bc3b53be46
updated: 1484308471
title: Chipko movement
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Chipko_2004.jpg
tags:
    - History
    - Background
    - Beginnings and organization
    - Aftermath
    - Participants
    - Legacy
    - Bibliography
categories:
    - Uncategorized
---
The Chipko movement or chipko andolan was primarily a forest conservation movement in India that began in 1973 and went on to become a rallying point for many future environmental movements all over the world; it created a precedent for non-violent protest started in India.[1][2] It occurred at a time when there was hardly any environmental movement in the developing world, and its success meant that the world immediately took notice of this non-violent movement, which was to inspire in time many such eco-groups by helping to slow down the rapid deforestation, expose vested interests, increase ecological awareness, and demonstrate the viability of people power. Above all, it stirred up the ...
