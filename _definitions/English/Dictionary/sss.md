---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sss
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484346661
title: sss
categories:
    - Dictionary
---
SSS
     n : an independent federal agency that administers compulsory
         military service [syn: {Selective Service}, {Selective
         Service System}]
