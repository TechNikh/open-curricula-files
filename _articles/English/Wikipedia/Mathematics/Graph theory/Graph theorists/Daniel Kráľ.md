---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Daniel_Kr%C3%A1%C4%BE'
offline_file: ""
offline_thumbnail: ""
uuid: 14565e34-dcca-43a8-b09f-01cc117b0d46
updated: 1484309468
title: Daniel Kráľ
categories:
    - Graph theorists
---
Daniel Kráľ is a Czech mathematician and computer scientist who works as a professor of mathematics and computer science at the University of Warwick. His research primarily concerns graph theory and graph algorithms.[1]
