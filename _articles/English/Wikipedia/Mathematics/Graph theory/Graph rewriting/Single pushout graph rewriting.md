---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Single_pushout_graph_rewriting
offline_file: ""
offline_thumbnail: ""
uuid: 4740ac2f-732d-4b15-99fe-d80259779754
updated: 1484309454
title: Single pushout graph rewriting
categories:
    - Graph rewriting
---
In computer science, single pushout graph rewriting or SPO graph rewriting refers to a mathematical framework for graph rewriting, and is used in contrast to the double-pushout approach of graph rewriting.
