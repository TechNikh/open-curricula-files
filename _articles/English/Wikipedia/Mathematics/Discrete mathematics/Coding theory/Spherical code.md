---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spherical_code
offline_file: ""
offline_thumbnail: ""
uuid: 16cfdd32-6e13-4a25-8b76-c5b7361e2a81
updated: 1484308570
title: Spherical code
categories:
    - Coding theory
---
In geometry and coding theory, a spherical code with parameters (n,N,t) is a set of N points on the unit hypersphere in n dimensions for which the dot product of unit vectors from the origin to any two points is less than or equal to t. The kissing number problem may be stated as the problem of finding the maximal N for a given n for which a spherical code with parameters (n,N,1/2) exists. The Tammes problem may be stated as the problem of finding a spherical code with minimal t for given n and N.
