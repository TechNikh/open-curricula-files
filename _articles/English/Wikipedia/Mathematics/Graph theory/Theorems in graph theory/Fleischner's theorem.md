---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fleischner%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 4fddcec6-c7a9-46f4-8816-ffa441a9f176
updated: 1484309456
title: "Fleischner's theorem"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Fleischner%2527s_theorem.svg.png'
tags:
    - Definitions and statement
    - Extensions
    - History
    - Notes
    - Primary sources
    - Secondary sources
categories:
    - Theorems in graph theory
---
In graph theory, a branch of mathematics, Fleischner's theorem gives a sufficient condition for a graph to contain a Hamiltonian cycle. It states that, if G is a 2-vertex-connected graph, then the square of G is Hamiltonian. it is named after Herbert Fleischner, who published its proof in 1974.
