---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prop
offline_file: ""
offline_thumbnail: ""
uuid: 9767b6b6-29b0-4d8e-a393-c0d3569e6670
updated: 1484310421
title: prop
categories:
    - Dictionary
---
prop
     n 1: a support placed beneath or against something to keep it
          from shaking or falling
     2: any movable articles or objects used on the set of a play or
        movie; "before every scene he ran down his checklist of
        props" [syn: {property}]
     3: a propeller that rotates to push against air [syn: {airplane
        propeller}, {airscrew}]
     v : support by placing against something solid or rigid; "shore
         and buttress an old building" [syn: {prop up}, {shore up},
          {shore}]
     [also: {propping}, {propped}]
