---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upper
offline_file: ""
offline_thumbnail: ""
uuid: c479bbbc-5214-4623-a2ed-6941978d7210
updated: 1484310339
title: upper
categories:
    - Dictionary
---
upper
     adj 1: higher in place or position; "the upper bunk"; "in the upper
            center of the picture"; "the upper stories"
     2: the topmost one of two
     3: superior in rank or accomplishment; "the upper half of the
        class"
     n 1: the higher of two berths [syn: {upper berth}]
     2: piece of leather that forms the part of a shoe or boot above
        the sole
     3: a central nervous system stimulant that increases energy and
        decreases appetite; used to treat narcolepsy and some
        forms of depression [syn: {amphetamine}, {pep pill}, {speed}]
