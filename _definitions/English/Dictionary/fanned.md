---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fanned
offline_file: ""
offline_thumbnail: ""
uuid: a25515c3-198b-49e3-bd81-605fb72e1e7d
updated: 1484310585
title: fanned
categories:
    - Dictionary
---
fan
     n 1: a device for creating a current of air by movement of a
          surface or surfaces
     2: an enthusiastic devotee of sports [syn: {sports fan}]
     3: an ardent follower and admirer [syn: {buff}, {devotee}, {lover}]
     v 1: strike out (a batter), (of a pitcher)
     2: make (an emotion) fiercer; "fan hatred"
     3: agitate the air
     4: separate from chaff; "She stood there winnowing grain all
        day in the field" [syn: {winnow}]
     [also: {fanning}, {fanned}]
