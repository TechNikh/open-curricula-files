---
version: 1
type: video
provider: YouTube
id: slc7k-R3ORk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 647fc138-61e1-4115-8c9a-65f56bae15fb
updated: 1486069623
title: 'Math Magic Trick- Guessing A Crossed Out Digit'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/slc7k-R3ORk/default.jpg
    - https://i3.ytimg.com/vi/slc7k-R3ORk/1.jpg
    - https://i3.ytimg.com/vi/slc7k-R3ORk/2.jpg
    - https://i3.ytimg.com/vi/slc7k-R3ORk/3.jpg
---
