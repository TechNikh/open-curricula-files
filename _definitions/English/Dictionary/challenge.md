---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/challenge
offline_file: ""
offline_thumbnail: ""
uuid: 1bb508bf-d9c1-4c8a-bba7-04c8f829077c
updated: 1484310447
title: challenge
categories:
    - Dictionary
---
challenge
     n 1: a demanding or stimulating situation; "they reacted
          irrationally to the challenge of Russian power"
     2: a call to engage in a contest or fight
     3: questioning a statement and demanding an explanation; "his
        challenge of the assumption that Japan is still our enemy"
     4: a formal objection to the selection of a particular person
        as a juror
     5: a demand by a sentry for a password or identification
     v 1: take exception to; "She challenged his claims" [syn: {dispute},
           {gainsay}]
     2: issue a challenge to; "Fischer challenged Spassky to a
        match"
     3: ask for identification; "The illegal immigrant was
        ...
