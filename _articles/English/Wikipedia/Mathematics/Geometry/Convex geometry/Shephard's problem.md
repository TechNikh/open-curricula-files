---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Shephard%27s_problem'
offline_file: ""
offline_thumbnail: ""
uuid: d7126512-54b8-48a9-8bc0-b29ddff5feaf
updated: 1484309197
title: "Shephard's problem"
categories:
    - Convex geometry
---
In mathematics, Shephard's problem, is the following geometrical question asked by Geoffrey Colin Shephard (1964): if K and L are centrally symmetric convex bodies in n-dimensional Euclidean space such that whenever K and L are projected onto a hyperplane, the volume of the projection of K is smaller than the volume of the projection of L, then does it follow that the volume of K is smaller than that of L?
