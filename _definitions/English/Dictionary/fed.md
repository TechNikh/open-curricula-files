---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fed
offline_file: ""
offline_thumbnail: ""
uuid: 4f339c4d-e328-4c6c-ac8d-be77f7859014
updated: 1484310210
title: fed
categories:
    - Dictionary
---
Fed
     n 1: any federal law-enforcement officer [syn: {Federal}, {federal
          official}]
     2: the central bank of the United States; incorporates 12
        Federal Reserve branch banks and all national banks and
        state charted commercial banks and some trust companies;
        "the Fed seeks to control the United States economy by
        raising and lowering short-term interest rates and the
        money supply" [syn: {Federal Reserve System}, {Federal
        Reserve}, {FRS}]
