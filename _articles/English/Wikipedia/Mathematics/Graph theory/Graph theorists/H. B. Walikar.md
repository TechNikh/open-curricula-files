---
version: 1
type: article
id: https://en.wikipedia.org/wiki/H._B._Walikar
offline_file: ""
offline_thumbnail: ""
uuid: f3e1fc88-a2e5-492f-a93f-94c125c033bb
updated: 1484309479
title: H. B. Walikar
categories:
    - Graph theorists
---
H. B. Walikar (born March 18, 1952) is the vice-chancellor of the Karnatak University in Dharwad, India,[1] beginning October 2010. Walikar is the 12th Vice Chancellor of the University. Dr Walikar, who was working as Head of the Department of Computer Science, KUD comes from Yaragalla in Muddebihal taluk of Bijapur district.
