---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lamb%E2%80%93Oseen_vortex'
offline_file: ""
offline_thumbnail: ""
uuid: a986efb7-ed73-4173-828f-4f754401fd22
updated: 1484308881
title: Lamb–Oseen vortex
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Lamb-Oseen_vortex.svg.png
categories:
    - Vortices
---
In fluid dynamics, the Lamb–Oseen vortex models a line vortex that decays due to viscosity. This vortex is named after Horace Lamb and Carl Wilhelm Oseen.[1]
