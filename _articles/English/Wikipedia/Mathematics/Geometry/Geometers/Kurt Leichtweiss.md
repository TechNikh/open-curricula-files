---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kurt_Leichtweiss
offline_file: ""
offline_thumbnail: ""
uuid: 47ca1f53-6230-4048-b99a-75c3453f85cd
updated: 1484309091
title: Kurt Leichtweiss
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kurt_Leichtweiss.jpg
categories:
    - Geometers
---
Leichtweiss was born in Villingen-Schwenningen. In 1944, while still in high school he traveled to the Mathematical Institute where his mathematical interests were encouraged.[2] He studied at the University of Freiburg and the ETH Zurich. He was a student of Emanuel Sperner and Wilhelm Süss. He was then a lecturer in Freiburg and in 1963 became a professor at TU Berlin. From 1970 until his retirement in 1995, he was a professor at the University of Stuttgart.
