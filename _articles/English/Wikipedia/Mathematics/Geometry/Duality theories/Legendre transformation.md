---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Legendre_transformation
offline_file: ""
offline_thumbnail: ""
uuid: f8e1bb8d-be8c-4838-9bdb-f33c5eeccb86
updated: 1484309234
title: Legendre transformation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/256px-Legendre_transformation.png
tags:
    - Definition
    - Properties
    - Examples
    - Example 1
    - Example 2
    - Example 3
    - Example 4
    - 'Example 5: several variables'
    - An equivalent definition in the differentiable case
    - Behavior of differentials under Legendre transforms
    - Applications
    - Hamilton–Lagrange mechanics
    - Thermodynamics
    - An example – variable capacitor
    - Probability theory
    - Geometric interpretation
    - Legendre transformation in more than one dimension
    - Further properties
    - Scaling properties
    - Behavior under translation
    - Behavior under inversion
    - Behavior under linear transformations
    - Infimal convolution
    - "Fenchel's inequality"
categories:
    - Duality theories
---
In mathematics and physics, the Legendre transformation or Legendre transform, named after Adrien-Marie Legendre, is an involutive transformation on the real-valued convex functions of one real variable. Its generalization to convex functions of affine spaces is sometimes called the Legendre–Fenchel transformation.
