---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Theodosius_of_Bithynia
offline_file: ""
offline_thumbnail: ""
uuid: 11118f29-79da-4568-88e3-cb430ca4b09a
updated: 1484309100
title: Theodosius of Bithynia
categories:
    - Geometers
---
Theodosius of Bithynia (Greek: Θεοδόσιος; c. 160 BC – c. 100 BC) was a Greek astronomer and mathematician who wrote the Sphaerics, a book on the geometry of the sphere.
