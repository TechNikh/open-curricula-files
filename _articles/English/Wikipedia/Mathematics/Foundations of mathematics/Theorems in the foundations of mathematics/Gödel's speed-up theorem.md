---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/G%C3%B6del%27s_speed-up_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 418b5bfd-8da8-4280-abb6-0c6695d66ef6
updated: 1484308994
title: "Gödel's speed-up theorem"
categories:
    - Theorems in the foundations of mathematics
---
In mathematics, Gödel's speed-up theorem, proved by Gödel (1936), shows that there are theorems whose proofs can be drastically shortened by working in more powerful axiomatic systems.
