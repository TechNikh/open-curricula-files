---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/condemned
offline_file: ""
offline_thumbnail: ""
uuid: 1004107c-0855-493f-9737-36726ec9be7d
updated: 1484310565
title: condemned
categories:
    - Dictionary
---
condemned
     adj 1: pronounced or proved guilty; "the condemned man faced the
            firing squad with dignity"; "a convicted criminal"
            [syn: {convicted}]
     2: officially and strongly disapproved; "the censured conflict
        of interest"; "her condemned behavior" [syn: {censured}]
     3: taken without permission or consent especially by public
        authority; "the condemned land was used for a highway
        cloverleaf"; "the confiscated liquor was poured down the
        drain" [syn: {appropriated}, {confiscate}, {confiscated},
        {seized}, {taken over}]
     4: officially pronounced unfit for use or consumption; "a row
        of condemned bulildings"
