---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illegible
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484440801
title: illegible
categories:
    - Dictionary
---
illegible
     adj : (of handwriting, print, etc.) not legible; "illegible
           handwriting" [ant: {legible}]
