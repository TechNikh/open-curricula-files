---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/canada
offline_file: ""
offline_thumbnail: ""
uuid: 59b1c6d4-e856-473f-a134-7934e5ada1d9
updated: 1484310216
title: canada
categories:
    - Dictionary
---
Canada
     n : a nation in northern North America; the French were the
         first Europeans to settle in mainland Canada; "the border
         between the United States and Canada is the longest
         unguarded border in the world"
