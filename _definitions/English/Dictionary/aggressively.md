---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aggressively
offline_file: ""
offline_thumbnail: ""
uuid: b025451e-08b1-40b4-902f-046be63d7483
updated: 1484310553
title: aggressively
categories:
    - Dictionary
---
aggressively
     adv : in an aggressive manner; "she was being sharply questioned"
           [syn: {sharply}]
