---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Principle_of_maximum_entropy
offline_file: ""
offline_thumbnail: ""
uuid: 2ff014df-bc69-40d2-b4d2-b2c135b5fb99
updated: 1484308771
title: Principle of maximum entropy
tags:
    - History
    - Overview
    - Testable information
    - Applications
    - Prior probabilities
    - Maximum entropy models
    - >
        General solution for the maximum entropy distribution with
        linear constraints
    - Discrete case
    - Continuous case
    - Examples
    - Justifications for the principle of maximum entropy
    - "Information entropy as a measure of 'uninformativeness'"
    - The Wallis derivation
    - "Compatibility with Bayes' theorem"
    - Notes
categories:
    - Entropy and information
---
The principle of maximum entropy states that, subject to precisely stated prior data (such as a proposition that expresses testable information), the probability distribution which best represents the current state of knowledge is the one with largest entropy.
