---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sacrifice
offline_file: ""
offline_thumbnail: ""
uuid: 43402844-0425-4948-beff-5bad243cb417
updated: 1484310545
title: sacrifice
categories:
    - Dictionary
---
sacrifice
     n 1: the act of losing or surrendering something as a penalty for
          a mistake or fault or failure to perform etc. [syn: {forfeit},
           {forfeiture}]
     2: personnel that are sacrificed (e.g., surrendered or lost in
        order to gain an objective)
     3: a loss entailed by giving up or selling something at less
        than its value; "he had to sell his car at a considerable
        sacrifice"
     4: the act of killing (an animal or person) in order to
        propitiate a deity [syn: {ritual killing}]
     5: (sacrifice) an out that advances the base runners
     v 1: endure the loss of; "He gave his life for his children"; "I
          gave two sons ...
