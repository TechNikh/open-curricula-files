---
version: 1
type: video
provider: YouTube
id: E9I-Onwfqpo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e1adf39f-4aa9-4f42-b81c-eab4937ad0cf
updated: 1486069660
title: Floating Cube in Center
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/E9I-Onwfqpo/default.jpg
    - https://i3.ytimg.com/vi/E9I-Onwfqpo/1.jpg
    - https://i3.ytimg.com/vi/E9I-Onwfqpo/2.jpg
    - https://i3.ytimg.com/vi/E9I-Onwfqpo/3.jpg
---
