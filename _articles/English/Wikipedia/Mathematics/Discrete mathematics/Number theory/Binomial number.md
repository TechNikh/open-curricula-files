---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Binomial_number
offline_file: ""
offline_thumbnail: ""
uuid: a0789feb-4fcb-4383-8780-e5c7bd85eb6e
updated: 1484308622
title: Binomial number
tags:
    - Definition
    - Factorization
    - Observation
categories:
    - Number theory
---
In mathematics, specifically in number theory, a binomial number is an integer which can be obtained by evaluating a homogeneous polynomial containing two terms. It is a generalization of a Cunningham number.
