---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/under
offline_file: ""
offline_thumbnail: ""
uuid: 271822f7-5fec-4ca5-a96a-1478f3554e2f
updated: 1484310339
title: under
categories:
    - Dictionary
---
under
     adj 1: located below or beneath something else; "nether garments";
            "the under parts of a machine" [syn: {nether}]
     2: lower in rank, power, or authority; "an under secretary"
        [syn: {under(a)}]
     adv 1: down to defeat, death, or ruin; "their competitors went
            under"
     2: through a range downward; "children six and under will be
        admitted free"
     3: into unconsciousness; "this will put the patient under"
     4: in or into a state of subordination or subjugation; "we must
        keep our disappointment under"
     5: below some quantity or limit; "fifty dollars or under"
     6: below the horizon; "the sun went under"
     7: down ...
