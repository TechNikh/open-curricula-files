---
version: 1
type: video
provider: YouTube
id: uN82GLQYAUQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/DNA%20Damage.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c1b3a316-c6db-482b-a709-41c403377980
updated: 1486069672
title: DNA Damage
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/uN82GLQYAUQ/default.jpg
    - https://i3.ytimg.com/vi/uN82GLQYAUQ/1.jpg
    - https://i3.ytimg.com/vi/uN82GLQYAUQ/2.jpg
    - https://i3.ytimg.com/vi/uN82GLQYAUQ/3.jpg
---
