---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incumbent
offline_file: ""
offline_thumbnail: ""
uuid: 727d5582-f108-4ed9-85ce-fdc25ca86af0
updated: 1484310593
title: incumbent
categories:
    - Dictionary
---
incumbent
     adj 1: lying or leaning on something else; "an incumbent geological
            formation"
     2: currently holding an office; "the incumbent governor"
     n : the official who holds an office [syn: {officeholder}]
