---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/matchless
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484556181
title: matchless
categories:
    - Dictionary
---
matchless
     adj : eminent beyond or above comparison; "matchless beauty"; "the
           team's nonpareil center fielder"; "she's one girl in a
           million"; "the one and only Muhammad Ali"; "a peerless
           scholar"; "infamy unmatched in the Western world";
           "wrote with unmatchable clarity"; "unrivaled mastery of
           her art" [syn: {nonpareil}, {one(a)}, {one and only(a)},
            {peerless}, {unmatched}, {unmatchable}, {unrivaled}, {unrivalled}]
