---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Information_diagram
offline_file: ""
offline_thumbnail: ""
uuid: f1eedf44-fd26-4c7c-8eb4-d48bffeac53a
updated: 1484309506
title: Information diagram
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/256px-Entropy-mutual-information-relative-entropy-relation-diagram.svg_2.png
categories:
    - Set theory
---
An information diagram is a type of Venn diagram used in information theory to illustrate relationships among Shannon's basic measures of information: entropy, joint entropy, conditional entropy and mutual information.[1][2] Information diagrams are a useful pedagogical tool for teaching and learning about these basic measures of information, but using such diagrams carries some non-trivial implications. For example, Shannon's entropy in the context of an information diagram must be taken as a signed measure. (See the article Information theory and measure theory for more information.)
