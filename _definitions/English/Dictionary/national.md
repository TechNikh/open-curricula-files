---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/national
offline_file: ""
offline_thumbnail: ""
uuid: 3b1ba7d6-345e-4ff2-a347-523aa4377291
updated: 1484310238
title: national
categories:
    - Dictionary
---
national
     adj 1: of or relating to or belonging to a nation or country;
            "national hero"; "national anthem"; "a national
            landmark"
     2: limited to or in the interests of a particular nation;
        "national interests"; "isolationism is a strictly national
        policy" [ant: {international}]
     3: concerned with or applicable to or belonging to an entire
        nation or country; "the national government"; "national
        elections"; "of national concern"; "the national highway
        system"; "national forests" [ant: {local}]
     4: owned or maintained for the public by the national
        government; "national parks"
     5: inside the country; ...
