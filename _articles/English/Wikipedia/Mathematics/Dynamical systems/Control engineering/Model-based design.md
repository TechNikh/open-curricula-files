---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Model-based_design
offline_file: ""
offline_thumbnail: ""
uuid: b00f62ba-19ef-4f7f-93f9-1f8e63d7b51a
updated: 1484308742
title: Model-based design
tags:
    - Overview
    - History
    - Model-based design steps
    - Advantages
    - Challenges
categories:
    - Control engineering
---
Model-Based Design (MBD) is a mathematical and visual method of addressing problems associated with designing complex control,[1][2] signal processing[3] and communication systems. It is used in many motion control, industrial equipment, aerospace, and automotive applications.[4][5] Model-based design is a methodology applied in designing embedded software.[6][7][8]
