---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Surface_acoustic_wave
offline_file: ""
offline_thumbnail: ""
uuid: c96be271-ea0e-45df-90b4-87f352f41815
updated: 1484308903
title: Surface acoustic wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-TeO2SAWs.jpg
tags:
    - discovery
    - SAW devices
    - Application in electronic components
    - SAW device applications in radio and television
    - SAW in geophysics
    - SAW in microfluidics
    - SAW in flow measurement
    - External links and references
categories:
    - Waves
---
A surface acoustic wave (SAW) is an acoustic wave traveling along the surface of a material exhibiting elasticity, with an amplitude that typically decays exponentially with depth into the substrate.
