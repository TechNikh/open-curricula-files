---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Integer
offline_file: ""
offline_thumbnail: ""
uuid: 7b112fbc-ba3c-4a57-8200-38e8ce9f826a
updated: 1484308942
title: Integer
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Number-line.svg.png
tags:
    - Algebraic properties
    - Order-theoretic properties
    - Construction
    - Computer science
    - Cardinality
    - Notes
    - Sources
categories:
    - Elementary number theory
---
An integer (from the Latin integer meaning "whole")[note 1] is a number that can be written without a fractional component. For example, 21, 4, 0, and −2048 are integers, while 9.75,  5 1⁄2, and √2 are not.
