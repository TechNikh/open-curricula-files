---
version: 1
type: article
id: https://en.wikipedia.org/wiki/100_(number)
offline_file: ""
offline_thumbnail: ""
uuid: bbefea55-4dff-44b5-825b-0891115cdfa4
updated: 1484308970
title: 100 (number)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Obverse_of_the_series_2009_%2524100_Federal_Reserve_Note.jpg'
tags:
    - In mathematics
    - In science
    - In religion
    - In politics
    - In money
    - In other fields
    - In sports
categories:
    - Integers
---
In medieval contexts, it may be described as the short hundred or five score in order to differentiate the English and Germanic use of "hundred" to describe the long hundred of six score or 120.
