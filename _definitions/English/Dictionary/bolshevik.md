---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bolshevik
offline_file: ""
offline_thumbnail: ""
uuid: ed1b0df9-3bbf-4197-860d-064c6121f94b
updated: 1484310551
title: bolshevik
categories:
    - Dictionary
---
Bolshevik
     adj : of or relating to Bolshevism; "Bolshevik Revolution" [syn: {Bolshevist},
            {Bolshevistic}]
     n 1: emotionally charged terms used to refer to extreme radicals
          or revolutionaries [syn: {Marxist}, {pinko}, {red}, {bolshie}]
     2: a Russian member of the left-wing majority group that
        followed Lenin and eventually became the Russian communist
        party [syn: {Bolshevist}]
