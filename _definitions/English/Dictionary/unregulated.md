---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unregulated
offline_file: ""
offline_thumbnail: ""
uuid: d3243d97-ae82-4b8f-b414-bdc93d2dc314
updated: 1484310467
title: unregulated
categories:
    - Dictionary
---
unregulated
     adj 1: not regulated; not subject to rule or discipline;
            "unregulated off-shore fishing" [ant: {regulated}]
     2: without regulation or discipline; "an unregulated
        environment"
