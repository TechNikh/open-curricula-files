---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alfred_Kempe
offline_file: ""
offline_thumbnail: ""
uuid: eaa6b396-67d3-4364-9332-c2d786f8ba6c
updated: 1484309468
title: Alfred Kempe
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Quadruplanar_invesor_of_Sylvester_and_Kempe_Alternate.gif
categories:
    - Graph theorists
---
Sir Alfred Bray Kempe D.C.L. F.R.S. (6 July 1849, Kensington, London – 21 April 1922, London) was a mathematician best known for his work on linkages and the four color theorem.
