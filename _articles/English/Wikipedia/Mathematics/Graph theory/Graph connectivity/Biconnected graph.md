---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biconnected_graph
offline_file: ""
offline_thumbnail: ""
uuid: 38edec6a-55c3-4b32-ad80-4796954887cb
updated: 1484309396
title: Biconnected graph
tags:
    - Definition
    - Examples
categories:
    - Graph connectivity
---
In graph theory, a biconnected graph is a connected and "nonseparable" graph, meaning that if any vertex were to be removed, the graph will remain connected. Therefore a biconnected graph has no articulation vertices.
