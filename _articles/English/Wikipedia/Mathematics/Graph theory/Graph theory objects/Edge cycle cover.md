---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edge_cycle_cover
offline_file: ""
offline_thumbnail: ""
uuid: b1a05d8c-def8-4afa-bdea-b47d7af36c1a
updated: 1484309447
title: Edge cycle cover
tags:
    - Properties and applications
    - Minimum-Weight Cycle Cover
    - Double cycle cover
categories:
    - Graph theory objects
---
In mathematics, an edge cycle cover (sometimes called simply cycle cover[1]) of a graph is a set of cycles which are subgraphs of G and contain all edges of G.
