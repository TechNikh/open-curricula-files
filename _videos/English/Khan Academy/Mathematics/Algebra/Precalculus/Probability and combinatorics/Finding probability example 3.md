---
version: 1
type: video
provider: YouTube
id: cdRLBOnLTDk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Probability%20and%20combinatorics/Finding%20probability%20example%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 26df073f-cfe6-4834-945b-a4a43293b9a4
updated: 1486069563
title: Finding probability example 3
categories:
    - Probability and combinatorics
thumbnail_urls:
    - https://i3.ytimg.com/vi/cdRLBOnLTDk/default.jpg
    - https://i3.ytimg.com/vi/cdRLBOnLTDk/1.jpg
    - https://i3.ytimg.com/vi/cdRLBOnLTDk/2.jpg
    - https://i3.ytimg.com/vi/cdRLBOnLTDk/3.jpg
---
