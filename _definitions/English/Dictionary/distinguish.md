---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distinguish
offline_file: ""
offline_thumbnail: ""
uuid: a1a51030-0687-414a-a237-d14214c78063
updated: 1484310286
title: distinguish
categories:
    - Dictionary
---
distinguish
     v 1: mark as different; "We distinguish several kinds of maple"
          [syn: {separate}, {differentiate}, {secern}, {secernate},
           {severalize}, {severalise}, {tell}, {tell apart}]
     2: detect with the senses; "The fleeing convicts were picked
        out of the darkness by the watchful prison guards"; "I
        can't make out the faces in this photograph" [syn: {recognize},
         {recognise}, {discern}, {pick out}, {make out}, {tell
        apart}]
     3: be a distinctive feature, attribute, or trait; sometimes in
        a very positive sense; "His modesty distinguishes him form
        his peers" [syn: {mark}, {differentiate}]
     4: make conspicuous ...
