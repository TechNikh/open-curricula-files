---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kid
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484423461
title: kid
categories:
    - Dictionary
---
kid
     n 1: a young person of either sex; "she writes books for
          children"; "they're just kids"; "`tiddler' is a British
          term for youngsters" [syn: {child}, {youngster}, {minor},
           {shaver}, {nipper}, {small fry}, {tiddler}, {tike}, {tyke},
           {fry}, {nestling}]
     2: soft smooth leather from the hide of a young goat; "kid
        gloves" [syn: {kidskin}]
     3: English dramatist (1558-1594) [syn: {Kyd}, {Thomas Kyd}, {Thomas
        Kid}]
     4: a human offspring (son or daughter) of any age; "they had
        three children"; "they were able to send their kids to
        college" [syn: {child}] [ant: {parent}]
     5: young goat
     v 1: tell ...
