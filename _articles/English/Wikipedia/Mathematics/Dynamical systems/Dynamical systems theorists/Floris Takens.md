---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Floris_Takens
offline_file: ""
offline_thumbnail: ""
uuid: 55ba684a-33b4-40f1-affc-ebd5b3dd1f21
updated: 1484308690
title: Floris Takens
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Floris_Takens.jpg
tags:
    - Selected publications
    - Notes
categories:
    - Dynamical systems theorists
---
Together with David Ruelle, he predicted that fluid turbulence could develop through a strange attractor, a term they coined, as opposed to the then-prevailing theory of accretion of modes. The prediction was later confirmed by experiment. Takens also established the result now known as the Takens' theorem, which shows how to reconstruct a dynamical system from an observed time-series.
