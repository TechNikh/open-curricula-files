---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tremor
offline_file: ""
offline_thumbnail: ""
uuid: 161fd0ab-d430-4488-8dd2-16da09a55afb
updated: 1484310140
title: tremor
categories:
    - Dictionary
---
tremor
     n 1: an involuntary vibration (as if from illness or fear) [syn:
          {shudder}]
     2: a small earthquake [syn: {earth tremor}, {microseism}]
     3: shaking or trembling (usually resulting from weakness or
        stress or disease)
     v : shake with seismic vibrations; "The earth was quaking" [syn:
          {quake}]
