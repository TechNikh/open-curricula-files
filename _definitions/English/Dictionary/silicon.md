---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silicon
offline_file: ""
offline_thumbnail: ""
uuid: 48f7439b-fdbe-4747-8c0b-d017c1c70be5
updated: 1484310200
title: silicon
categories:
    - Dictionary
---
silicon
     n : a tetravalent nonmetallic element; next to oxygen it is the
         most abundant element in the earth's crust; occurs in
         clay and feldspar and granite and quartz and sand; used
         as a semiconductor in transistors [syn: {Si}, {atomic
         number 14}]
