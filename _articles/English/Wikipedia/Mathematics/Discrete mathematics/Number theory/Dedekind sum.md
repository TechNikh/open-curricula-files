---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dedekind_sum
offline_file: ""
offline_thumbnail: ""
uuid: f965b5cf-a8f2-47fd-986d-f63ab97854fc
updated: 1484308639
title: Dedekind sum
tags:
    - Definition
    - Simple formulae
    - Alternative forms
    - Reciprocity law
    - "Rademacher's generalization of the reciprocity law"
categories:
    - Number theory
---
In mathematics, Dedekind sums are certain sums of products of a sawtooth function, and are given by a function D of three integer variables. Dedekind introduced them to express the functional equation of the Dedekind eta function. They have subsequently been much studied in number theory, and have occurred in some problems of topology. Dedekind sums have a large number functional equations; this article lists only a small fraction of these.
