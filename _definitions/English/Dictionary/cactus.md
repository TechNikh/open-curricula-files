---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cactus
offline_file: ""
offline_thumbnail: ""
uuid: 3b669d72-7092-40f1-b9b0-d47a9d446fc7
updated: 1484310286
title: cactus
categories:
    - Dictionary
---
cactus
     n : any spiny succulent plant of the family Cactaceae native
         chiefly to arid regions of the New World
     [also: {cacti} (pl)]
