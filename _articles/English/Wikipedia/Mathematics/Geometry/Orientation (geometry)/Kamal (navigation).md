---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kamal_(navigation)
offline_file: ""
offline_thumbnail: ""
uuid: b1afbc68-328d-4406-84c7-df4265c29cc2
updated: 1484309295
title: Kamal (navigation)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Simple_Wooden_Kamal_%2528Navigation%2529.jpg'
tags:
    - Description
    - Notes
categories:
    - Orientation (geometry)
---
A kamal is a celestial navigation device that determines latitude. The invention of the kamal allowed for the earliest known latitude sailing,[1] and was thus the earliest step towards the use of quantitative methods in navigation.[2] It originated with Arab navigators of the late 9th century,[3] and was employed in the Indian Ocean from the 10th century.[1] It was adopted by Indian navigators soon after,[4] and then adopted by Chinese navigators some time before the 16th century.[2]
