---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/visualised
offline_file: ""
offline_thumbnail: ""
uuid: 077775b1-5908-4a53-9aa6-c6e040df3c83
updated: 1484310595
title: visualised
categories:
    - Dictionary
---
visualised
     adj : seen in the mind as a mental image; "the glory of his
           envisioned future"; "the snow-covered Alps pictured in
           her imagination"; "the visualized scene lacked the ugly
           details of real life" [syn: {envisioned}, {pictured}, {visualized}]
