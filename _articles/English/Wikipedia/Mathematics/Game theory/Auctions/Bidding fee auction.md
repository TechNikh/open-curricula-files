---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bidding_fee_auction
offline_file: ""
offline_thumbnail: ""
uuid: 56f138c0-6b97-47b5-9844-1f813dfb30c9
updated: 1484309011
title: Bidding fee auction
tags:
    - How it works
    - Criticism
categories:
    - Auctions
---
A bidding fee auction, also called a penny auction, is a type of all-pay auction in which all participants must pay a non-refundable fee to place each small incremental bid. The auction ends after a period of time, typically ten to twenty seconds. Without new bids the last participant to have placed a bid wins the item and also pays the final bid price. The auctioneer makes money in two ways: the fees for each bid and the payment for the winning bid, totalling typically significantly more than the value of the item.[1] Such auctions are typically held over the Internet, rather than in person.
