---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/depict
offline_file: ""
offline_thumbnail: ""
uuid: 87aa6425-28f9-4877-ac86-fb3b940bc743
updated: 1484310484
title: depict
categories:
    - Dictionary
---
depict
     v 1: show in, or as in, a picture; "This scene depicts country
          life"; "the face of the child is rendered with much
          tenderness in this painting" [syn: {picture}, {render},
          {show}]
     2: give a description of; "He drew an elaborate plan of attack"
        [syn: {describe}, {draw}]
     3: make a portrait of; "Goya wanted to portray his mistress,
        the Duchess of Alba" [syn: {portray}, {limn}]
