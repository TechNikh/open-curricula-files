---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Commutative_property
offline_file: ""
offline_thumbnail: ""
uuid: 26687ae9-29cc-412e-bb61-c384e2f1a4b3
updated: 1484308913
title: Commutative property
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Commutativity_of_binary_operations_%2528without_question_mark%2529.svg.png'
tags:
    - Common uses
    - Mathematical definitions
    - Examples
    - Commutative operations in everyday life
    - Commutative operations in mathematics
    - Noncommutative operations in everyday life
    - Noncommutative operations in mathematics
    - History and etymology
    - Propositional logic
    - Rule of replacement
    - Truth functional connectives
    - Set theory
    - Mathematical structures and commutativity
    - Related properties
    - Associativity
    - Symmetry
    - Non-commuting operators in quantum mechanics
    - Notes
    - Books
    - Articles
    - Online resources
categories:
    - Elementary algebra
---
In mathematics, a binary operation is commutative if changing the order of the operands does not change the result. It is a fundamental property of many binary operations, and many mathematical proofs depend on it. Most familiar as the name of the property that says "3 + 4 = 4 + 3" or "2 × 5 = 5 × 2", the property can also be used in more advanced settings. The name is needed because there are operations, such as division and subtraction, that do not have it (for example, "3 − 5 ≠ 5 − 3"), such operations are not commutative, or noncommutative operations. The idea that simple operations, such as multiplication and addition of numbers, are commutative was for many years implicitly ...
