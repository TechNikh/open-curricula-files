---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pollen
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484467201
title: pollen
categories:
    - Dictionary
---
pollen
     n : the fine spores that contain male gametes and that are borne
         by an anther in a flowering plant
