---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/land
offline_file: ""
offline_thumbnail: ""
uuid: 462d2f20-69c1-405a-94b4-b8a7eb0a5876
updated: 1484310281
title: land
categories:
    - Dictionary
---
land
     adj 1: relating to or characteristic of or occurring on land; "land
            vehicles" [syn: {land(a)}] [ant: {sea(a)}, {air(a)}]
     2: operating or living or growing on land [syn: {terrestrial},
        {land(a)}] [ant: {amphibious}, {aquatic}]
     n 1: the land on which real estate is located; "he built the
          house on land leased from the city"
     2: material in the top layer of the surface of the earth in
        which plants can grow (especially with reference to its
        quality or use); "the land had never been plowed"; "good
        agricultural soil" [syn: {ground}, {soil}]
     3: the solid part of the earth's surface; "the plane turned
        away ...
