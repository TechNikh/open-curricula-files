---
version: 1
type: video
provider: YouTube
id: UsPmg_Ne1po
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Introduction%20to%20Ratios.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 302b5fec-612f-48f0-8d1b-1642b2bfb8f7
updated: 1486069575
title: Introduction to Ratios
tags:
    - ratio
    - Algebra
    - math
    - CC_6_RP_1
    - CC_6_RP_2
    - CC_6_RP_3
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/UsPmg_Ne1po/default.jpg
    - https://i3.ytimg.com/vi/UsPmg_Ne1po/1.jpg
    - https://i3.ytimg.com/vi/UsPmg_Ne1po/2.jpg
    - https://i3.ytimg.com/vi/UsPmg_Ne1po/3.jpg
---
Basic ratio problems. (from KhanAcademy.org)
