---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Correlation_dimension
offline_file: ""
offline_thumbnail: ""
uuid: 1c3915c7-fd99-4255-b0f9-d2af93ab7501
updated: 1484308705
title: Correlation dimension
categories:
    - Chaos theory
---
In chaos theory, the correlation dimension (denoted by ν) is a measure of the dimensionality of the space occupied by a set of random points, often referred to as a type of fractal dimension.[1][2][3]
