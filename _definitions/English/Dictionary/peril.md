---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peril
offline_file: ""
offline_thumbnail: ""
uuid: 4e6914c8-b57f-4dda-8e76-a50472c1218f
updated: 1484310443
title: peril
categories:
    - Dictionary
---
peril
     n 1: a source of danger; a possibility of incurring loss or
          misfortune; "drinking alcohol is a health hazard" [syn:
          {hazard}, {jeopardy}, {risk}]
     2: a state of danger involving risk [syn: {riskiness}]
     3: a venture undertaken without regard to possible loss or
        injury; "he saw the rewards but not the risks of crime";
        "there was a danger he would do the wrong thing" [syn: {risk},
         {danger}]
     v 1: pose a threat to; present a danger to; "The pollution is
          endangering the crops" [syn: {endanger}, {jeopardize}, {jeopardise},
           {menace}, {threaten}, {imperil}]
     2: put in a dangerous, disadvantageous, or ...
