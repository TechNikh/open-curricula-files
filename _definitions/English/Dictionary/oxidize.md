---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oxidize
offline_file: ""
offline_thumbnail: ""
uuid: f8cc89f1-fa63-4aee-8f62-62a40b7f273a
updated: 1484310423
title: oxidize
categories:
    - Dictionary
---
oxidize
     v 1: enter into a combination with oxygen or become converted
          into an oxide; "This metal oxidizes easily" [syn: {oxidise},
           {oxidate}]
     2: add oxygen to or combine with oxygen [syn: {oxidise}, {oxidate}]
        [ant: {deoxidize}, {deoxidize}]
