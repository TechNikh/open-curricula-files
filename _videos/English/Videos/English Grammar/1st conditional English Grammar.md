---
version: 1
type: video
provider: YouTube
id: yDZaMZphLjk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 10c832f6-8cce-41c3-92a2-bbf2493b56ef
updated: 1486069632
title: 1st conditional English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/yDZaMZphLjk/default.jpg
    - https://i3.ytimg.com/vi/yDZaMZphLjk/1.jpg
    - https://i3.ytimg.com/vi/yDZaMZphLjk/2.jpg
    - https://i3.ytimg.com/vi/yDZaMZphLjk/3.jpg
---
