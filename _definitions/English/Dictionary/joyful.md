---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/joyful
offline_file: ""
offline_thumbnail: ""
uuid: 52a929dc-98a5-4253-8d20-3b4603ee9188
updated: 1484310140
title: joyful
categories:
    - Dictionary
---
joyful
     adj 1: full of or producing joy; "make a joyful noise"; "a joyful
            occasion" [ant: {sorrowful}]
     2: full of high-spirited delight [syn: {elated}, {gleeful}, {jubilant}]
     3: full of or suggesting exultant happiness; "a joyful heart";
        "a joyful occasion"; "the joyous news"; "joyous laughter"
        [syn: {joyous}]
