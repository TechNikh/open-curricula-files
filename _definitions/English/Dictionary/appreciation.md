---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appreciation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484520001
title: appreciation
categories:
    - Dictionary
---
appreciation
     n 1: understanding of the nature or meaning or quality or
          magnitude of something; "he has a good grasp of
          accounting practices" [syn: {grasp}, {hold}]
     2: delicate discrimination (especially of aesthetic values);
        "arrogance and lack of taste contributed to his rapid
        success"; "to ask at that particular time was the ultimate
        in bad taste" [syn: {taste}, {discernment}, {perceptiveness}]
     3: an expression of gratitude; "he expressed his appreciation
        in a short note"
     4: a favorable judgment; "a small token in admiration of your
        works" [syn: {admiration}]
     5: an increase in price or value; "an ...
