---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heaven
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484469541
title: heaven
categories:
    - Dictionary
---
heaven
     n 1: any place of complete bliss and delight and peace [syn: {eden},
           {paradise}, {nirvana}, {promised land}, {Shangri-la}]
     2: the abode of God and the angels [ant: {Hell}]
