---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/washed
offline_file: ""
offline_thumbnail: ""
uuid: 74a9678f-1ec9-48b7-a979-d674cafc6c10
updated: 1484310230
title: washed
categories:
    - Dictionary
---
washed
     adj 1: clean by virtue of having been washed in water [syn: {water-washed}]
     2: wet as from washing; sometimes used in combination;
        "rain-washed"
