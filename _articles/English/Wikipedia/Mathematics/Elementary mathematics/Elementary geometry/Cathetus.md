---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cathetus
offline_file: ""
offline_thumbnail: ""
uuid: 1531e6c9-ca40-49e1-a1e4-0b12a5101d3e
updated: 1484308925
title: Cathetus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Triangle_Sides.svg.png
categories:
    - Elementary geometry
---
In a right triangle, a cathetus (originally from the Greek word Κάθετος; plural: catheti), commonly known as a leg, is either of the sides that are adjacent to the right angle. It is occasionally called a "side about the right angle". The side opposite the right angle is the hypotenuse. In the context of the hypotenuse, the catheti are sometimes referred to simply as "the other two sides".
