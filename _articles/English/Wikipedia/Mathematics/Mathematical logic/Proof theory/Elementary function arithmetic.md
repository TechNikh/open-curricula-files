---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Elementary_function_arithmetic
offline_file: ""
offline_thumbnail: ""
uuid: 19ba11f6-dbdc-4cae-925b-765341348b76
updated: 1484309492
title: Elementary function arithmetic
tags:
    - Definition
    - "Friedman's grand conjecture"
    - Related systems
categories:
    - Proof theory
---
In proof theory, a branch of mathematical logic, elementary function arithmetic, also called EFA, elementary arithmetic and exponential function arithmetic, is the system of arithmetic with the usual elementary properties of 0, 1, +, ×, xy, together with induction for formulas with bounded quantifiers.
