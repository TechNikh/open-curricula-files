---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/able
offline_file: ""
offline_thumbnail: ""
uuid: 947d01d8-5e53-4608-a6b0-6561738223d6
updated: 1484310343
title: able
categories:
    - Dictionary
---
able
     adj 1: (usually followed by `to') having the necessary means or
            skill or know-how or authority to do something; "able
            to swim"; "she was able to program her computer"; "we
            were at last able to buy a car"; "able to get a grant
            for the project" [ant: {unable}]
     2: have the skills and qualifications to do things well; "able
        teachers"; "a capable administrator"; "children as young
        as 14 can be extremely capable and dependable" [syn: {capable}]
     3: having inherent physical or mental ability or capacity;
        "able to learn"; "human beings are able to walk on two
        feet"; "Superman is able to leap tall ...
