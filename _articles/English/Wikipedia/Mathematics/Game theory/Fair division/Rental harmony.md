---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rental_harmony
offline_file: ""
offline_thumbnail: ""
uuid: 74e2414b-4f8e-4d47-b31e-4955d84b19dc
updated: 1484308520
title: Rental harmony
tags:
    - Ordinal version
    - 'Su: one person per room'
    - 'Azriely and Shmaya: room-mates'
    - General properties of ordinal protocols
    - Cardinal version
    - Incompatibility of EF and NN
    - 'Brams and Kilgour: NN but not EF'
    - 'Haake and Raith and Su: EF but not NN'
    - 'Abdulkadiroglu and Sonmez and Unver: EF and NN if possible'
    - 'Sung and Vlach: EF and NN if possible'
    - Strategic considerations
    - 'Sun and Yang: Changing the problem'
    - 'Dufton and Larson: Using randomization'
    - 'Andersson and Ehlers and Svensson: Attaining partial-strategyproofness'
categories:
    - Fair division
---
Rental harmony [1] [2] is a kind of a fair division problem in which indivisible items and a fixed monetary cost have to be divided simultaneously. The housemates problem [3] [4] and room-assignment-rent-division [5] [6] are alternative names to the same problem. [7] [8]:305–328
