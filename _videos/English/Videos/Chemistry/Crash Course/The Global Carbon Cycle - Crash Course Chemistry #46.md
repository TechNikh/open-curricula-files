---
version: 1
type: video
provider: YouTube
id: aLuSi_6Ol8M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/The%20Global%20Carbon%20Cycle%20-%20Crash%20Course%20Chemistry%20%2346.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 51f16b4b-f160-426c-ab06-af177c208518
updated: 1486069680
title: 'The Global Carbon Cycle - Crash Course Chemistry #46'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/aLuSi_6Ol8M/default.jpg
    - https://i3.ytimg.com/vi/aLuSi_6Ol8M/1.jpg
    - https://i3.ytimg.com/vi/aLuSi_6Ol8M/2.jpg
    - https://i3.ytimg.com/vi/aLuSi_6Ol8M/3.jpg
---
