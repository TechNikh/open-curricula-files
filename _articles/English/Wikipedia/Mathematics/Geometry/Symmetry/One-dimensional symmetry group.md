---
version: 1
type: article
id: https://en.wikipedia.org/wiki/One-dimensional_symmetry_group
offline_file: ""
offline_thumbnail: ""
uuid: 22106790-5015-4c06-837b-bc90c507346b
updated: 1484309315
title: One-dimensional symmetry group
tags:
    - Point group
    - Discrete symmetry groups
    - Translational symmetry
    - Non-discrete symmetry groups
    - 1D-symmetry of a function vs. 2D-symmetry of its graph
    - Group action
    - Orbits and stabilizers
categories:
    - Symmetry
---
A pattern in 1D can be represented as a function f(x) for, say, the color at position x.
