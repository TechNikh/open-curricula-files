---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cyclic
offline_file: ""
offline_thumbnail: ""
uuid: 39a2c886-1044-45d2-9786-c6e4abf72264
updated: 1484310419
title: cyclic
categories:
    - Dictionary
---
cyclic
     adj 1: conforming to the Carnot cycle
     2: botany; forming a whorl or having parts arranged in a whorl;
        "cyclic petals"; "cyclic flowers" [ant: {acyclic}]
     3: of a compound having atoms arranged in a ring structure
        [ant: {acyclic}]
     4: recurring in cycles [syn: {cyclical}] [ant: {noncyclic}]
     5: marked by repeated cycles
