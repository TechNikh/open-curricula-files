---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vibgyor
offline_file: ""
offline_thumbnail: ""
uuid: 1baa3f77-be35-4338-82a5-ce99fa655b47
updated: 1484308307
title: ViBGYOR Film Festival
tags:
    - Film Spectrum
    - Genres
    - Event history
    - Themes
    - Films screened
categories:
    - Uncategorized
---
ViBGYOR Film Festival is an international short and documentary film festival held annually in Thrissur City in Kerala state of India.[1] ViBGYOR Film Festival is organised by ViBGYOR Film collective, a coalition of Chetana Media Institute, Nottam Traveling Film Festival, Navachitra Film Society, Visual Search, Moving Republic, Cense, GAIA, with the support of Thrissur Municipal Corporation, Zilla Panchayath, Federation of Film Societies of India, Kerala Chalachitra Academy, Information & Public Relations, ActionAid India, ICCO-South Asia and other various Film Societies.
