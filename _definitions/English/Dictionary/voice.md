---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/voice
offline_file: ""
offline_thumbnail: ""
uuid: eb3d2a13-f652-4b32-bcb1-d5f87f50c8f0
updated: 1484310471
title: voice
categories:
    - Dictionary
---
voice
     n 1: the distinctive quality or pitch or condition of a person's
          speech; "A shrill voice sounded behind us"
     2: the sound made by the vibration of vocal folds modified by
        the resonance of the vocal tract; "a singer takes good
        care of his voice"; "the giraffe cannot make any
        vocalizations" [syn: {vocalization}, {vocalisation}, {phonation},
         {vox}]
     3: a sound suggestive of a vocal utterance; "the noisy voice of
        the waterfall"; "the incessant voices of the artillery"
     4: expressing in coherent verbal form; "the articulation of my
        feelings"; "I gave voice to my feelings" [syn: {articulation}]
     5: a means or ...
