---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isometry_group
offline_file: ""
offline_thumbnail: ""
uuid: ee852bc7-3cdd-4f24-b0ad-94e1a5a19e1e
updated: 1484309266
title: Isometry group
categories:
    - Metric geometry
---
In mathematics, the isometry group of a metric space is the set of all bijective isometries (i.e. bijective, distance-preserving maps) from the metric space onto itself, with the function composition as group operation. Its identity element is the identity function.[1]
