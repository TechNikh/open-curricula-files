---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radar
offline_file: ""
offline_thumbnail: ""
uuid: 7ef1b3be-d897-4847-8fd0-5846376b53f2
updated: 1484310389
title: radar
categories:
    - Dictionary
---
radar
     n : measuring instrument in which the echo of a pulse of
         microwave radiation is used to detect and locate distant
         objects [syn: {microwave radar}, {radio detection and
         ranging}, {radiolocation}]
