---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reflecting
offline_file: ""
offline_thumbnail: ""
uuid: 01d2b049-afb9-4376-b22f-242465799c7e
updated: 1484310216
title: reflecting
categories:
    - Dictionary
---
reflecting
     adj : causing reflection or having a device that reflects; "a
           reflecting microscope"
