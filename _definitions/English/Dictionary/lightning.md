---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lightning
offline_file: ""
offline_thumbnail: ""
uuid: e09f5d01-f0e8-491d-944d-9ee7f98807b5
updated: 1484310206
title: lightning
categories:
    - Dictionary
---
lightning
     n 1: abrupt electric discharge from cloud to cloud or from cloud
          to earth accompanied by the emission of light
     2: the flash of light that accompanies an electric discharge in
        the atmosphere (or something resembling such a flash); can
        scintillate for a second or more
