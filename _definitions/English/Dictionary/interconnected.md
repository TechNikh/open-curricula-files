---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interconnected
offline_file: ""
offline_thumbnail: ""
uuid: f5c36ea7-7e92-4a37-9089-a7fe47a80fbf
updated: 1484310275
title: interconnected
categories:
    - Dictionary
---
interconnected
     adj 1: reciprocally connected [syn: {interrelated}]
     2: operating as a unit; "a unified utility system"; "a
        coordinated program" [syn: {coordinated}, {unified}]
