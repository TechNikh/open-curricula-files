---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meager
offline_file: ""
offline_thumbnail: ""
uuid: 05afda09-6fc0-411b-9c6e-e9b3e9a7bb9d
updated: 1484310144
title: meager
categories:
    - Dictionary
---
meager
     adj 1: deficient in amount or quality or extent; "meager
            resources"; "meager fare" [syn: {meagre}, {meagerly}]
            [ant: {ample}]
     2: barely adequate; "a meager allowance" [syn: {scrimpy}]
