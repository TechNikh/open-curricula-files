---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gasohol
offline_file: ""
offline_thumbnail: ""
uuid: 0ba85556-5d25-4947-a624-d74d79d5a69a
updated: 1484308409
title: Common ethanol fuel mixtures
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Common_ethanol_fuel_mixtures.png
tags:
    - E10 or less
    - Availability
    - E15
    - hE15
    - E20, E25
    - E70, E75
    - E85
    - ED95
    - E100
    - Use limitations
    - Modifications to engines
    - Other disadvantages
    - Notes
categories:
    - Uncategorized
---
Several common ethanol fuel mixtures are in use around the world. The use of pure hydrous or anhydrous ethanol in internal combustion engines (ICEs) is only possible if the engines are designed or modified for that purpose, and used only in automobiles, light-duty trucks and motorcycles. Anhydrous ethanol can be blended with gasoline (petrol) for use in gasoline engines, but with high ethanol content only after minor engine modifications.
