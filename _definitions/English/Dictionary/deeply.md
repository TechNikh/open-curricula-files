---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deeply
offline_file: ""
offline_thumbnail: ""
uuid: b7d1d36e-c0f2-4c44-a956-fdcd6d017bef
updated: 1484310305
title: deeply
categories:
    - Dictionary
---
deeply
     adv 1: to a great depth psychologically; "They felt the loss
            deeply" [syn: {profoundly}]
     2: to a great depth; "dived deeply"; "dug deep" [syn: {deep}]
     [also: {deeper}]
