---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unique_bid_auction
offline_file: ""
offline_thumbnail: ""
uuid: aa8c5c94-acd2-476d-9bd0-32eb2d47b7dc
updated: 1484309017
title: Unique bid auction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_12.png
tags:
    - Mechanism
    - Profitability of unique bid auctions
    - Legality
    - Paying a non-refundable fee
    - Chance
    - Receiving a reward of some kind
    - Mathematical analysis
categories:
    - Auctions
---
A unique bid auction is a type of strategy game related to traditional auctions where the winner is usually the individual with the lowest unique bid, although less commonly the auction rules may specify that the highest unique bid is the winner. Unique bid auctions are often used as a form of competition and strategy game where bidders pay a fee to make a bid, or may have to pay a subscription fee in order to be able to participate.
