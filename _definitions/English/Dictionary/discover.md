---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discover
offline_file: ""
offline_thumbnail: ""
uuid: 7fff9e9e-e0fd-4a41-bb8d-7d28eec15316
updated: 1484310246
title: discover
categories:
    - Dictionary
---
discover
     v 1: discover or determine the existence, presence, or fact of;
          "She detected high levels of lead in her drinking
          water"; "We found traces of lead in the paint" [syn: {detect},
           {observe}, {find}, {notice}]
     2: make a discovery, make a new finding; "Roentgen discovered
        X-rays"; "Physicists believe they found a new elementary
        particle" [syn: {find}]
     3: get to know or become aware of, usually accidentally; "I
        learned that she has two grown-up children"; "I see that
        you have been promoted" [syn: {learn}, {hear}, {get word},
         {get wind}, {pick up}, {find out}, {get a line}, {see}]
     4: make a ...
