---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lickorish%E2%80%93Wallace_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: d632737e-163e-44e9-b1e2-ae1e100f581f
updated: 1484309116
title: Lickorish–Wallace theorem
categories:
    - Theorems in geometry
---
In mathematics, the Lickorish–Wallace theorem in the theory of 3-manifolds states that any closed, orientable, connected 3-manifold may be obtained by performing Dehn surgery on a framed link in the 3-sphere with ±1 surgery coefficients. Furthermore, each component of the link can be assumed to be unknotted.
