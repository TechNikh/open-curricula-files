---
version: 1
type: video
provider: YouTube
id: dVynwxU322s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/3.1%20Enzyme%20Catalysis.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fa9b5777-f64d-4d60-a381-3ca7926f5572
updated: 1486069677
title: 3.1 Enzyme Catalysis
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/dVynwxU322s/default.jpg
    - https://i3.ytimg.com/vi/dVynwxU322s/1.jpg
    - https://i3.ytimg.com/vi/dVynwxU322s/2.jpg
    - https://i3.ytimg.com/vi/dVynwxU322s/3.jpg
---
