---
version: 1
type: article
id: https://en.wikipedia.org/wiki/George_W._Hart
offline_file: ""
offline_thumbnail: ""
uuid: 64a06acd-f7b9-40ea-933a-ec7bf952bb30
updated: 1484309080
title: George W. Hart
tags:
    - Bibliography
    - Gallery
    - Incompatible Food Triad
    - False solutions
    - Possible solutions
categories:
    - Geometers
---
George William Hart (born 1955[1]) is an American geometer who expresses himself both artistically and academically. He is also an interdepartmental research professor at the State University of New York in Stony Brook, New York.
