---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Y
offline_file: ""
offline_thumbnail: ""
uuid: 54173b04-b13b-4222-a0d3-1079befc166f
updated: 1484308342
title: 'Y'
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Y_cursiva.gif
tags:
    - Name
    - History
    - Vowel
    - Consonant
    - Confusion in writing with the letter thorn
    - Use in writing systems
    - english
    - Other languages
    - Other systems
    - Other uses
    - Related characters
    - Descendants and related characters in the Latin alphabet
    - Ancestors and siblings in other alphabets
    - Derived signs, symbols and abbreviations
    - Computing codes
    - Other representations
    - Notes
categories:
    - Uncategorized
---
Y (named wye[1] /ˈwaɪ/, plural wyes)[2] is the 25th and penultimate letter in the modern English alphabet and the ISO basic Latin alphabet. In the English writing system, it sometimes represents a vowel and sometimes a consonant.
