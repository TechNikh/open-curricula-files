---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imperative
offline_file: ""
offline_thumbnail: ""
uuid: 35f79c83-5885-4811-9ae2-d10f41051818
updated: 1484310148
title: imperative
categories:
    - Dictionary
---
imperative
     adj 1: requiring attention or action; "as nuclear weapons
            proliferate, preventing war becomes imperative";
            "requests that grew more and more imperative" [ant: {beseeching}]
     2: relating to verbs in the imperative mood
     n 1: a mood that expresses an intention to influence the
          listener's behavior [syn: {imperative mood}, {jussive
          mood}]
     2: some duty that is essential and urgent
