---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nontransitive_game
offline_file: ""
offline_thumbnail: ""
uuid: 14ea7eff-35a0-450a-b985-12c891b0556e
updated: 1484309035
title: Nontransitive game
categories:
    - Game theory game classes
---
A non-transitive game is a game for which the various strategies produce one or more "loops" of preferences. In a non-transitive game in which strategy A is preferred over strategy B, and strategy B is preferred over strategy C, strategy A is not necessarily preferred over strategy C.
