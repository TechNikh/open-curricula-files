---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heat
offline_file: ""
offline_thumbnail: ""
uuid: f62e548d-18cf-45d0-869b-bf5bba83c705
updated: 1484310313
title: heat
categories:
    - Dictionary
---
heat
     n 1: a form of energy that is transferred by a difference in
          temperature [syn: {heat energy}]
     2: the presence of heat [syn: {hotness}, {high temperature}]
        [ant: {coldness}]
     3: the sensation caused by heat energy [syn: {warmth}]
     4: intense passion or emotion [syn: {warmth}, {passion}]
     5: applies to nonhuman mammals: a state or period of heightened
        sexual arousal and activity [syn: {estrus}, {oestrus}, {rut}]
        [ant: {anestrus}]
     6: a preliminary race in which the winner advances to a more
        important race
     7: utility to warm a building; "the heating system wasn't
        working"; "they have radiant heating" [syn: ...
