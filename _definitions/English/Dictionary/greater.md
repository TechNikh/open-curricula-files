---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greater
offline_file: ""
offline_thumbnail: ""
uuid: 92a5be0c-cec4-4779-a3ef-44968d4c2a09
updated: 1484310273
title: greater
categories:
    - Dictionary
---
greater
     adj : greater in size or importance or degree; "for the greater
           good of the community"; "the greater Antilles" [ant: {lesser}]
