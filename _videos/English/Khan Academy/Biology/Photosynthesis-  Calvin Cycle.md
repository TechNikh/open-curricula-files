---
version: 1
type: video
provider: YouTube
id: slm6D2VEXYs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Photosynthesis-%20%20Calvin%20Cycle.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 448408fe-1da8-447a-ba88-e52357f6b148
updated: 1486069583
title: 'Photosynthesis-  Calvin Cycle'
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/slm6D2VEXYs/default.jpg
    - https://i3.ytimg.com/vi/slm6D2VEXYs/1.jpg
    - https://i3.ytimg.com/vi/slm6D2VEXYs/2.jpg
    - https://i3.ytimg.com/vi/slm6D2VEXYs/3.jpg
---
The Calvin Cycle or the light-independent (dark) reactions of photosythesis
More free lessons at: http://www.khanacademy.org/video?v=slm6D2VEXYs
