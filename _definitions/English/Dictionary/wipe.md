---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wipe
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484450521
title: wipe
categories:
    - Dictionary
---
wipe
     n : the act of rubbing or wiping; "he gave the hood a quick rub"
         [syn: {rub}]
     v : rub with a circular motion; "wipe the blackboard" [syn: {pass
         over}]
