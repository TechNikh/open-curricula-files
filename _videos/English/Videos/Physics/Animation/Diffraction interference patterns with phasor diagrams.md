---
version: 1
type: video
provider: YouTube
id: NazBRcMDOOo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dc7a362c-b3a6-4500-95ba-9307cc1fbc0f
updated: 1486069657
title: Diffraction interference patterns with phasor diagrams
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/NazBRcMDOOo/default.jpg
    - https://i3.ytimg.com/vi/NazBRcMDOOo/1.jpg
    - https://i3.ytimg.com/vi/NazBRcMDOOo/2.jpg
    - https://i3.ytimg.com/vi/NazBRcMDOOo/3.jpg
---
