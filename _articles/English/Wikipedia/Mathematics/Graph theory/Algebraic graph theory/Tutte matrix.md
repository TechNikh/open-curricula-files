---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tutte_matrix
offline_file: ""
offline_thumbnail: ""
uuid: bb7bec17-c646-4728-aacb-ea315b707540
updated: 1484309379
title: Tutte matrix
categories:
    - Algebraic graph theory
---
In graph theory, the Tutte matrix A of a graph G = (V, E) is a matrix used to determine the existence of a perfect matching: that is, a set of edges which is incident with each vertex exactly once.
