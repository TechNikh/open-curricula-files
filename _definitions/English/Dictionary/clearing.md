---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clearing
offline_file: ""
offline_thumbnail: ""
uuid: 4deb0dec-2554-410f-b214-ef95c4760e63
updated: 1484310581
title: clearing
categories:
    - Dictionary
---
clearing
     n 1: a tract of land with few or no trees in the middle of a
          wooded area [syn: {glade}]
     2: the act of freeing from suspicion
     3: the act of removing solid particles from a liquid [syn: {clarification}]
