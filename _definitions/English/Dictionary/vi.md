---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vi
offline_file: ""
offline_thumbnail: ""
uuid: b291569e-36ac-44a5-ab6c-c275681c4442
updated: 1484310319
title: vi
categories:
    - Dictionary
---
vi
     adj : denoting a quantity consisting of six items or units [syn: {six},
            {6}, {half dozen}, {half a dozen}]
     n 1: the cardinal number that is the sum of five and one [syn: {six},
           {6}, {sixer}, {sise}, {Captain Hicks}, {half a dozen},
          {sextet}, {sestet}, {sextuplet}, {hexad}]
     2: more than 130 southeastern Virgin Islands; a dependent
        territory of the United States [syn: {United States Virgin
        Islands}, {American Virgin Islands}]
