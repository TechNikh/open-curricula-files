---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Chomsky%E2%80%93Sch%C3%BCtzenberger_representation_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 6356fc69-0185-4cfa-b18c-60f912100d32
updated: 1484308662
title: Chomsky–Schützenberger representation theorem
categories:
    - Theorems in discrete mathematics
---
In formal language theory, the Chomsky–Schützenberger representation theorem is a theorem derived by Noam Chomsky and Marcel-Paul Schützenberger about representing a given context-free language in terms of two simpler languages. These two simpler languages, namely a regular language and a Dyck language, are combined by means of an intersection and a homomorphism.
