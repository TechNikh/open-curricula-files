---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rarefaction
offline_file: ""
offline_thumbnail: ""
uuid: 0245c682-ed39-4897-9378-ce18dafd6820
updated: 1484308896
title: Rarefaction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Onde_compression_impulsion_1d_30_petit_0.gif
categories:
    - Waves
---
Rarefaction is the reduction of an item's density, the opposite of compression.[1] Like compression, which can travel in waves (sound waves, for instance), rarefaction waves also exist in nature. A common rarefaction wave is the area of low relative pressure following a shock wave (see picture).
