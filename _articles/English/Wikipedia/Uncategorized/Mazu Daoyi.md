---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Baso
offline_file: ""
offline_thumbnail: ""
uuid: 5959bcb9-4096-4f5e-943a-386cce06fa95
updated: 1484308317
title: Mazu Daoyi
tags:
    - Biography
    - "Mazu's Hongzhou school"
    - Teachings
    - Buddha Nature
    - Shock techniques
    - Subitism and dhyana (zazen)
    - Use of koans
    - Appearances
    - Examples
    - Successors
    - Criticism
    - Notes
    - Book references
    - Web references
    - Sources
categories:
    - Uncategorized
---
Mazu Daoyi (709–788) (Chinese: 馬祖道一; pinyin: Mǎzŭ Dàoyī; Wade–Giles: Ma-tsu Tao-yi, Japanese: Baso Dōitsu) was an influential abbot of Chan Buddhism during the Tang dynasty. The earliest recorded use of the term "Chan school" is from his Extensive Records.[1] Master Ma's teaching style of "strange words and extraordinary actions"[2] became paradigmatic Zen lore.
