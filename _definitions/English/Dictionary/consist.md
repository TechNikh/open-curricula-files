---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consist
offline_file: ""
offline_thumbnail: ""
uuid: 5dffa41d-59b5-497d-8e60-feba0cd1d9a9
updated: 1484310363
title: consist
categories:
    - Dictionary
---
consist
     v 1: originate (in); "The problems dwell in the social injustices
          in this country" [syn: {dwell}, {lie}, {belong}, {lie in}]
     2: have its essential character; be comprised or contained in;
        be embodied in; "The payment consists in food"; "What does
        love consist in?"
     3: be consistent in form, tenor, or character; be congruous;
        "Desires are to be satisfied only so far as consists with
        an approved end"
     4: be composed of; "The land he conquered comprised several
        provinces"; "What does this dish consist of?" [syn: {comprise}]
