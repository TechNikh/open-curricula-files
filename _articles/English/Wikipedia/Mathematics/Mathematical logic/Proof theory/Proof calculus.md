---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proof_calculus
offline_file: ""
offline_thumbnail: ""
uuid: c9af19ea-afda-4a5a-9995-ef8686e03636
updated: 1484309497
title: Proof calculus
categories:
    - Proof theory
---
In mathematical logic, a proof calculus corresponds to a family of formal systems that use a common style of formal inference for its inference rules. The specific inference rules of a member of such a family characterize the theory of a logic.
