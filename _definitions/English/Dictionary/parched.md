---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parched
offline_file: ""
offline_thumbnail: ""
uuid: 1579fc7d-5ecf-42aa-bb53-f63fa3918862
updated: 1484310537
title: parched
categories:
    - Dictionary
---
parched
     adj 1: dried out by heat or excessive exposure to sunlight; "a vast
            desert all adust"; "land lying baked in the heat";
            "parched soil"; "the earth was scorched and bare";
            "sunbaked salt flats" [syn: {adust}, {baked}, {scorched},
             {sunbaked}]
     2: toasted or roasted slightly; "parched corn was a staple of
        the Indian diet"
