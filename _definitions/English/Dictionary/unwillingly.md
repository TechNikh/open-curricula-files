---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unwillingly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484569921
title: unwillingly
categories:
    - Dictionary
---
unwillingly
     adv : in an unwilling manner; "he had sinned against her
           unwillingly" [ant: {willingly}]
