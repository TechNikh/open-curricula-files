---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/penetrating
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484636941
title: penetrating
categories:
    - Dictionary
---
penetrating
     adj 1: having or demonstrating ability to recognize or draw fine
            distinctions; "an acute observer of politics and
            politicians"; "incisive comments"; "icy knifelike
            reasoning"; "as sharp and incisive as the stroke of a
            fang"; "penetrating insight"; "frequent penetrative
            observations" [syn: {acute}, {discriminating}, {incisive},
             {keen}, {knifelike}, {penetrative}, {piercing}, {sharp}]
     2: tending to penetrate; having the power of entering or
        piercing; "a toxic penetrative spray applied to the
        surface"; "a cold penetrating wind"; "a penetrating odor"
        [syn: {penetrative}]
