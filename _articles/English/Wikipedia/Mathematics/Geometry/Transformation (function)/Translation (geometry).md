---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Translation_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: d7668352-3b94-4e40-a5d6-139f738c1fe8
updated: 1484309340
title: Translation (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Traslazione_OK.svg.png
tags:
    - Matrix representation
    - Translations in physics
categories:
    - Transformation (function)
---
In Euclidean geometry, a translation is a geometric transformation that moves every point of a figure or a space by the same amount in a given direction.
