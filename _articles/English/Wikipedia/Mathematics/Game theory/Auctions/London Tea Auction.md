---
version: 1
type: article
id: https://en.wikipedia.org/wiki/London_Tea_Auction
offline_file: ""
offline_thumbnail: ""
uuid: a29a18ef-8536-4052-9fc5-0cac282eba16
updated: 1484309013
title: London Tea Auction
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Clock_Tower_-_Palace_of_Westminster%252C_London_-_May_2007_icon.png'
categories:
    - Auctions
---
This candle auction ran regularly for over 300 years from 1679 until its closure on June 29, 1998. The auction made London the centre for tea's international trade. The East India Company held the first auction in Leadenhall Street and then in 1834 - after the East India Company ceased to be a commercial enterprise - the auction was held on Mincing Lane.
