---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/envied
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484559481
title: envied
categories:
    - Dictionary
---
envy
     n 1: a feeling of grudging admiration and desire to have
          something possessed by another [syn: {enviousness}, {the
          green-eyed monster}]
     2: spite and resentment at seeing the success of another
        (personified as one of the deadly sins) [syn: {invidia}]
     v 1: feel envious towards; admire enviously
     2: be envious of; set one's heart on [syn: {begrudge}]
     [also: {envied}]
