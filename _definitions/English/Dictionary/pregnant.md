---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pregnant
offline_file: ""
offline_thumbnail: ""
uuid: bc5289d5-f18d-4bd8-a073-068dd061054f
updated: 1484310535
title: pregnant
categories:
    - Dictionary
---
pregnant
     adj 1: carrying developing offspring within the body or being about
            to produce new life [ant: {nonpregnant}]
     2: rich in significance or implication; "a meaning look";
        "pregnant with meaning" [syn: {meaning(a)}, {significant}]
     3: filled with or attended with; "words fraught with meaning";
        "an incident fraught with danger"; "a silence pregnant
        with suspense" [syn: {fraught(p)}]
