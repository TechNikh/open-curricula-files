---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roman
offline_file: ""
offline_thumbnail: ""
uuid: 8807af29-43e0-4380-a7de-5926fdb984dc
updated: 1484310397
title: roman
categories:
    - Dictionary
---
Roman
     adj 1: relating to or characteristic of people of Rome; "Roman
            virtues"; "his Roman bearing in adversity"; "a Roman
            nose"
     2: of or relating to or characteristic of Rome (especially
        ancient Rome); "Roman architecture"; "the old Roman wall"
     3: characteristic of the modern type that most directly
        represents the type used in ancient Roman inscriptions
     4: of or relating to or supporting Romanism; "the Roman
        Catholic Church" [syn: {r.c.}, {Romanist}, {romish}, {Roman
        Catholic}, {popish}, {papist}, {papistic}, {papistical}]
     n 1: a resident of modern Rome
     2: an inhabitant of the ancient Roman Empire
     3: ...
