---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/History_of_the_Theory_of_Numbers
offline_file: ""
offline_thumbnail: ""
uuid: 38b89169-fb16-4be9-b766-3a03167d616a
updated: 1484308641
title: History of the Theory of Numbers
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-P_history.svg_0.png
categories:
    - Number theory
---
History of the Theory of Numbers is a three-volume work by L. E. Dickson summarizing work in number theory up to about 1920. The style is unusual in that Dickson mostly just lists results by various authors, with little further discussion. The central topic of quadratic reciprocity and higher reciprocity laws is barely mentioned; this was apparently going to be the topic of a fourth volume that was never written (Fenster 1999).
