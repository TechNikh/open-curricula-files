---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wasteland
offline_file: ""
offline_thumbnail: ""
uuid: e7751283-6e36-429e-83f5-78b62623ff36
updated: 1484310254
title: wasteland
categories:
    - Dictionary
---
wasteland
     n : an uninhabited wilderness that is worthless for cultivation;
         "the barrens of central Africa"; "the trackless wastes of
         the desert" [syn: {barren}, {waste}]
