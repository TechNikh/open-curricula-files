---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/protecting
offline_file: ""
offline_thumbnail: ""
uuid: 923f6cb8-a3b9-4a74-8b48-28d73c41d8be
updated: 1484310236
title: protecting
categories:
    - Dictionary
---
protecting
     adj : shielding (or designed to shield) against harm or
           discomfort; "the protecting blanket of snow"; "a
           protecting alibi" [syn: {protecting(a)}]
