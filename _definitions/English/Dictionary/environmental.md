---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/environmental
offline_file: ""
offline_thumbnail: ""
uuid: 6eafa0c7-91d8-4b77-bf38-926747492151
updated: 1484310480
title: environmental
categories:
    - Dictionary
---
environmental
     adj 1: concerned with the ecological effects of altering the
            environment; "environmental pollution"
     2: of or relating to the external conditions or surroundings;
        "environmental factors"
