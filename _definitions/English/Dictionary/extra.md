---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extra
offline_file: ""
offline_thumbnail: ""
uuid: 4fcc3a1d-c0aa-4f8c-a0d4-ce376c0dca0c
updated: 1484310328
title: extra
categories:
    - Dictionary
---
extra
     adj 1: further or added; "called for additional troops"; "need
            extra help"; "an extra pair of shoes"; "I have no
            other shoes"; "there are other possibilities" [syn: {other(a)},
             {additional}]
     2: more than is needed, desired, or required; "trying to lose
        excess weight"; "found some extra change lying on the
        dresser"; "yet another book on heraldry might be thought
        redundant"; "skills made redundant by technological
        advance"; "sleeping in the spare room"; "supernumerary
        ornamentation"; "it was supererogatory of her to gloat";
        "delete superfluous (or unnecessary) words"; "extra ribs
        as ...
