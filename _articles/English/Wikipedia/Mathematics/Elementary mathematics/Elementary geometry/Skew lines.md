---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Skew_lines
offline_file: ""
offline_thumbnail: ""
uuid: 361007e5-0894-4502-9748-c8ca67bbd1bd
updated: 1484308938
title: Skew lines
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Rectrangular_parallelepiped.png
tags:
    - General position
    - Formulas
    - Testing for skewness
    - distance
    - Nearest Points
    - More than two lines
    - Configurations
    - Ruled surfaces
    - "Gallucci's theorem"
    - Skew flats in higher dimensions
    - Notes
categories:
    - Elementary geometry
---
In three-dimensional geometry, skew lines are two lines that do not intersect and are not parallel. A simple example of a pair of skew lines is the pair of lines through opposite edges of a regular tetrahedron. Two lines that both lie in the same plane must either cross each other or be parallel, so skew lines can exist only in three or more dimensions. Two lines are skew if and only if they are not coplanar.
