---
version: 1
type: video
provider: YouTube
id: oZJlZywgQL8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a9d43b35-03a0-40f1-b24e-21e2737977db
updated: 1486069666
title: |
    Nucleus' "Atherosclerosis" Using CINEMA 4D/After Effects
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/oZJlZywgQL8/default.jpg
    - https://i3.ytimg.com/vi/oZJlZywgQL8/1.jpg
    - https://i3.ytimg.com/vi/oZJlZywgQL8/2.jpg
    - https://i3.ytimg.com/vi/oZJlZywgQL8/3.jpg
---
