---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Maurer%E2%80%93Cartan_form'
offline_file: ""
offline_thumbnail: ""
uuid: 3e47510a-a03e-4fb3-9932-dd38a3953b25
updated: 1484309230
title: Maurer–Cartan form
tags:
    - Motivation and interpretation
    - Construction of the Maurer–Cartan form
    - Intrinsic construction
    - Extrinsic construction
    - Characterization as a connection
    - Properties
    - Maurer–Cartan frame
    - Maurer–Cartan form on a homogeneous space
    - Notes
categories:
    - Differential geometry
---
In mathematics, the Maurer–Cartan form for a Lie group G is a distinguished differential one-form on G that carries the basic infinitesimal information about the structure of G. It was much used by Élie Cartan as a basic ingredient of his method of moving frames, and bears his name together with that of Ludwig Maurer.
