---
version: 1
type: video
provider: YouTube
id: ADfyReSt2L0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Emphatic%20pronouns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1b14bc18-8cc2-49a6-8c83-a3fd1f436432
updated: 1486069597
title: Emphatic pronouns
tags:
    - emphasis
    - intensive pronouns
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/ADfyReSt2L0/default.jpg
    - https://i3.ytimg.com/vi/ADfyReSt2L0/1.jpg
    - https://i3.ytimg.com/vi/ADfyReSt2L0/2.jpg
    - https://i3.ytimg.com/vi/ADfyReSt2L0/3.jpg
---
Sometimes we use reflexive pronouns like "myself" and "ourselves" for emphasis in a sentence, like, "If you won't help me, then I'll do it myself!" KA's Grammar Fellow, David, explains this usage.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/e/emphatic-pronouns?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/singular-they?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/indefinite-pronouns-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
