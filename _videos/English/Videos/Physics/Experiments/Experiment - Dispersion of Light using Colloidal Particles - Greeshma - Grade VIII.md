---
version: 1
type: video
provider: YouTube
id: a2KmLSvrRuU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7c05fe42-f91a-41d0-8ea9-c9181ed148c6
updated: 1486069640
title: 'Experiment - Dispersion of Light using Colloidal Particles - Greeshma - Grade VIII'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/a2KmLSvrRuU/default.jpg
    - https://i3.ytimg.com/vi/a2KmLSvrRuU/1.jpg
    - https://i3.ytimg.com/vi/a2KmLSvrRuU/2.jpg
    - https://i3.ytimg.com/vi/a2KmLSvrRuU/3.jpg
---
