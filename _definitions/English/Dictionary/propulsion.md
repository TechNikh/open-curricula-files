---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/propulsion
offline_file: ""
offline_thumbnail: ""
uuid: d4d6ee4e-a5d8-43eb-8652-612530027bee
updated: 1484310330
title: propulsion
categories:
    - Dictionary
---
propulsion
     n 1: a propelling force
     2: the act of propelling [syn: {actuation}]
