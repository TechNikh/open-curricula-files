---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moderator
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484633941
title: moderator
categories:
    - Dictionary
---
moderator
     n 1: any substance used to slow down neutrons in nuclear reactors
     2: in the Presdyterian church, the officer who presides over a
        synod or general assembly
     3: someone who presides over a forum or debate
     4: someone who mediates disputes and attempts to avoid violence
