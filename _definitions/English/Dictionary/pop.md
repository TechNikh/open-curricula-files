---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pop
offline_file: ""
offline_thumbnail: ""
uuid: 7f4c530a-4232-4268-8ba0-3bfdc281e03e
updated: 1484310351
title: pop
categories:
    - Dictionary
---
pop
     adj : (of music or art) new and of general appeal (especially
           among young people) [syn: {popular}]
     n 1: an informal term for a father; probably derived from baby
          talk [syn: {dad}, {dada}, {daddy}, {pa}, {papa}, {pappa},
           {pater}]
     2: a sweet drink containing carbonated water and flavoring; "in
        New England they call sodas tonics" [syn: {soda}, {soda
        pop}, {soda water}, {tonic}]
     3: a sharp explosive sound as from a gunshot or drawing a cork
        [syn: {popping}]
     4: music of general appeal to teenagers; a bland watered-down
        version of rock'n'roll with more rhythm and harmony and an
        emphasis on ...
