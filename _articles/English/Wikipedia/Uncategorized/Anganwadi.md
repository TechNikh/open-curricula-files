---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anganwadi
offline_file: ""
offline_thumbnail: ""
uuid: d6254915-944d-4110-9002-43d15073974e
updated: 1484308440
title: Anganwadi
tags:
    - Worker functions
    - Worker responsibilities
    - Supervision
    - Benefits
    - Challenges and solutions
    - Integration with other official schemes
    - Criticism
    - International efforts
categories:
    - Uncategorized
---
The word Anganwadi means "courtyard shelter" in Indian languages. They were started by the Indian government in 1975 as part of the Integrated Child Development Services program to combat child hunger and malnutrition.
