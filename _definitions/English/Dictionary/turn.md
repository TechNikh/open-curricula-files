---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/turn
offline_file: ""
offline_thumbnail: ""
uuid: 37489e88-61e2-4968-81a1-9bb1f5a68dc6
updated: 1484310264
title: turn
categories:
    - Dictionary
---
turn
     n 1: a circular segment of a curve; "a bend in the road"; "a
          crook in the path" [syn: {bend}, {crook}]
     2: the act of changing or reversing the direction of the
        course; "he took a turn to the right" [syn: {turning}]
     3: the activity of doing something in an agreed succession; "it
        is my turn"; "it is still my play" [syn: {play}]
     4: an unforeseen development; "events suddenly took an awkward
        turn" [syn: {turn of events}, {twist}]
     5: a movement in a new direction; "the turning of the wind"
        [syn: {turning}]
     6: turning away or in the opposite direction; "he made an
        abrupt turn away from her"
     7: turning or ...
