---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flip_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 32891be3-3524-4717-a511-012c2b2c569d
updated: 1484309147
title: Flip (mathematics)
tags:
    - The minimal model program
    - Definition
    - Examples
categories:
    - Algebraic geometry
---
In algebraic geometry, flips and flops are codimension-2 surgery operations arising in the minimal model program, given by blowing up along a relative canonical ring. In dimension 3 flips are used to construct minimal models, and any two birationally equivalent minimal models are connected by a sequence of flops. It is conjectured that the same is true in higher dimensions.
