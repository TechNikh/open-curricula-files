---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/buy
offline_file: ""
offline_thumbnail: ""
uuid: 3c61d665-e524-40fc-a91f-b5ec559d2cee
updated: 1484310202
title: buy
categories:
    - Dictionary
---
buy
     n : an advantageous purchase; "she got a bargain at the
         auction"; "the stock was a real buy at that price" [syn:
         {bargain}, {steal}]
     v 1: obtain by purchase; acquire by means of a financial
          transaction; "The family purchased a new car"; "The
          conglomerate acquired a new company"; "She buys for the
          big department store" [syn: {purchase}] [ant: {sell}]
     2: make illegal payments to in exchange for favors or
        influence; "This judge can be bought" [syn: {bribe}, {corrupt},
         {grease one's palms}]
     3: acquire by trade or sacrifice or exchange; "She wanted to
        buy his love with her dedication to him and his ...
