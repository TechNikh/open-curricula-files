---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/giving
offline_file: ""
offline_thumbnail: ""
uuid: 8ab08caf-0722-4c5d-a1bd-a1d22978898f
updated: 1484310330
title: giving
categories:
    - Dictionary
---
giving
     adj : given or giving freely; "was a big tipper"; "the bounteous
           goodness of God"; "bountiful compliments"; "a
           freehanded host"; "a handsome allowance"; "Saturday's
           child is loving and giving"; "a liberal backer of the
           arts"; "a munificent gift"; "her fond and openhanded
           grandfather" [syn: {big}, {bighearted}, {bounteous}, {bountiful},
            {freehanded}, {handsome}, {liberal}, {openhanded}]
     n 1: the act of giving [syn: {gift}]
     2: the imparting of news or promises etc.; "he gave us the news
        and made a great show of the giving"; "giving his word of
        honor seemed to come too easily"
     3: ...
