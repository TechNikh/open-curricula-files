---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wrinkled
offline_file: ""
offline_thumbnail: ""
uuid: 1bbcccf4-0aef-42ae-a213-5cc6f7e87859
updated: 1484310305
title: wrinkled
categories:
    - Dictionary
---
wrinkled
     adj 1: marked with wrinkles or furrows; "her ancient wrinkled
            cheeks" [syn: {wrinkly}]
     2: made or become wrinkled as by crushing or folding; "tired
        travelers in wrinkled clothes" [ant: {unwrinkled}]
     3: (of linens or clothes) not ironed; "a pile of unironed
        laundry"; "wore unironed jeans" [syn: {unironed}] [ant: {ironed}]
