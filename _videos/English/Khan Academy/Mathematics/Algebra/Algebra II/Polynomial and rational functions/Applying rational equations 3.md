---
version: 1
type: video
provider: YouTube
id: B0Z4s38YIgQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Polynomial%20and%20rational%20functions/Applying%20rational%20equations%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0756cd9c-6a7a-43c7-9696-a55d8a271684
updated: 1486069563
title: Applying rational equations 3
categories:
    - Polynomial and rational functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/B0Z4s38YIgQ/default.jpg
    - https://i3.ytimg.com/vi/B0Z4s38YIgQ/1.jpg
    - https://i3.ytimg.com/vi/B0Z4s38YIgQ/2.jpg
    - https://i3.ytimg.com/vi/B0Z4s38YIgQ/3.jpg
---
