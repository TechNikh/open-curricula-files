---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prodigy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441281
title: prodigy
categories:
    - Dictionary
---
prodigy
     n 1: an unusually gifted or intelligent (young) person; someone
          whose talents excite wonder and admiration; "she is a
          chess prodigy"
     2: a sign of something about to happen; "he looked for an omen
        before going into battle" [syn: {omen}, {portent}, {presage},
         {prognostic}, {prognostication}]
     3: an impressive or wonderful example of a particular quality;
        "the Marines are expected to perform prodigies of valor"
