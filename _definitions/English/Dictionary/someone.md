---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/someone
offline_file: ""
offline_thumbnail: ""
uuid: 4e836016-934e-4ec2-9acd-0e31f09a82f2
updated: 1484310379
title: someone
categories:
    - Dictionary
---
someone
     n : a human being; "there was too much for one person to do"
         [syn: {person}, {individual}, {somebody}, {mortal}, {human},
          {soul}]
