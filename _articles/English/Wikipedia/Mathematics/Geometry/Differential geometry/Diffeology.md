---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diffeology
offline_file: ""
offline_thumbnail: ""
uuid: 404965d3-1f68-463b-bbdf-8dcb822c6e84
updated: 1484309212
title: Diffeology
tags:
    - Definition
    - Smooth manifolds
    - Examples
categories:
    - Differential geometry
---
In mathematics, a diffeology on a set declares what the smooth parametrizations in the set are. In some sense a diffeology generalizes the concept of smooth charts in a differentiable manifold.
