---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fujiki_class_C
offline_file: ""
offline_thumbnail: ""
uuid: bfa16753-126a-4068-81ef-1a7cf947a651
updated: 1484309153
title: Fujiki class C
categories:
    - Algebraic geometry
---
In algebraic geometry, a complex manifold is called Fujiki class C if it is bimeromorphic to a compact Kähler manifold. This notion was defined by Akira Fujiki.[1]
