---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalized_polygon
offline_file: ""
offline_thumbnail: ""
uuid: 1685e218-f120-4aa0-832d-7d97082b3f3c
updated: 1484309253
title: Generalized polygon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Split_Cayley_Hexagon.png
tags:
    - Definition
    - Examples
    - Restriction on parameters
    - Semi-finite generalized polygons
    - Combinatorial applications
categories:
    - Incidence geometry
---
In combinatorial theory, a generalized polygon is an incidence structure introduced by Jacques Tits in 1959. Generalized n-gons encompass as special cases projective planes (generalized triangles, n = 3) and generalized quadrangles (n = 4). Many generalized polygons arise from groups of Lie type, but there are also exotic ones that cannot be obtained in this way. Generalized polygons satisfying a technical condition known as the Moufang property have been completely classified by Tits and Weiss. Every generalized n-gon with n even is also a near polygon.
