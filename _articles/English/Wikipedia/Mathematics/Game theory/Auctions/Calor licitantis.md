---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Calor_licitantis
offline_file: ""
offline_thumbnail: ""
uuid: 94f808bb-952f-4bea-a54d-10f1cd6bac12
updated: 1484309013
title: Calor licitantis
tags:
    - Origins
    - "Bidder's Heat in Antiquity"
    - Modern Equivalents
categories:
    - Auctions
---
Calor licitantis is a Latin phrase, the literal translation of which is, "heat of soliciting." The functional use of the phrase in both modern times and antiquity [1] is "bidder's heat". This is also known as "auction fever".
