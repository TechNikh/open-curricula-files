---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Planar_projection
offline_file: ""
offline_thumbnail: ""
uuid: b0f7125a-47eb-4ac3-a42a-137e8012989d
updated: 1484309369
title: Planar projection
categories:
    - Geometry stubs
---
Planar projections are the subset of 3D graphical projections constructed by linearly mapping points in three-dimensional space to points on a two-dimensional projection plane. The projected point on the plane is chosen such that it is collinear with the corresponding three-dimensional point and the centre of projection. The lines connecting these points are commonly referred to as projectors.
