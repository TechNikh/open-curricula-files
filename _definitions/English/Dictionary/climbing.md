---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/climbing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484481241
title: climbing
categories:
    - Dictionary
---
climbing
     n : an event that involves rising to a higher point (as in
         altitude or temperature or intensity etc.) [syn: {climb},
          {mounting}]
