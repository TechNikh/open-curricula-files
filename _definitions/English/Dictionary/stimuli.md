---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stimuli
offline_file: ""
offline_thumbnail: ""
uuid: bcf86259-fd9f-40c3-93c8-ce3fed13425f
updated: 1484310327
title: stimuli
categories:
    - Dictionary
---
stimulus
     n : any stimulating information or event; acts to arouse action
         [syn: {stimulation}, {stimulant}, {input}]
     [also: {stimuli} (pl)]
