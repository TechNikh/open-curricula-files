---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rack
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484547841
title: rack
categories:
    - Dictionary
---
rack
     n 1: framework for holding objects
     2: rib section of a forequarter of veal or pork or especially
        lamb or mutton
     3: the destruction or collapse of something; "wrack and ruin"
        [syn: {wrack}]
     4: an instrument of torture that stretches or disjoints or
        mutilates victims [syn: {wheel}]
     5: a support for displaying various articles; "the newspapers
        were arranged on a rack" [syn: {stand}]
     6: a rapid gait of a horse in which each foot strikes the
        ground separately [syn: {single-foot}]
     v 1: go at a rack; "the horses single-footed" [syn: {single-foot}]
     2: stretch to the limits; "rack one's brains"
     3: put on a rack ...
