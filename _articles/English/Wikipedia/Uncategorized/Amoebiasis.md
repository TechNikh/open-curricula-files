---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Amoebiasis
offline_file: ""
offline_thumbnail: ""
uuid: 4ec9c59f-65a4-4c92-aabc-3bf57f2b6dbd
updated: 1484308340
title: Amoebiasis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Amebiasis_LifeCycle.gif
tags:
    - Signs and symptoms
    - Cause
    - Transmission
    - Diagnosis
    - Prevention
    - Treatment
    - Prognosis
    - Epidemiology
    - History
categories:
    - Uncategorized
---
Amoebiasis, also known as amebiasis or entamoebiasis,[1][2] is an infection caused by any of the amoebas of the Entamoeba group. Symptoms are most common upon infection by Entamoeba histolytica. Amoebiasis can present with no, mild, or severe symptoms. Symptoms may include abdominal pain, mild diarrhoea, bloody diarrhea or severe colitis with tissue death and perforation. This last complication may cause peritonitis. People affected may develop anemia due to loss of blood.[3]
