---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upstream
offline_file: ""
offline_thumbnail: ""
uuid: 57c09804-d4ce-4a3b-901f-3d8179b1e111
updated: 1484310462
title: upstream
categories:
    - Dictionary
---
upstream
     adj : in the direction against a stream's current [ant: {downstream}]
     adv 1: toward the source or against the current [syn: {upriver}]
            [ant: {downriver}, {downriver}]
     2: against the current; "he swam upstream"
