---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/base
offline_file: ""
offline_thumbnail: ""
uuid: 456e3e46-5e8b-41f4-ac45-414861de75de
updated: 1484310273
title: base
categories:
    - Dictionary
---
base
     adj 1: serving as or forming a base; "the painter applied a base
            coat followed by two finishing coats" [syn: {basal}]
     2: (used of metals) consisting of or alloyed with inferior
        metal; "base coins of aluminum"; "a base metal"
     3: of low birth or station (`base' is archaic in this sense);
        "baseborn wretches with dirty faces"; "of humble (or
        lowly) birth" [syn: {baseborn}, {humble}, {lowly}]
     4: not adhering to ethical or moral principles; "base and
        unpatriotic motives"; "a base, degrading way of life";
        "cheating is dishonorable"; "they considered colonialism
        immoral"; "unethical practices in handling public ...
