---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cut-off
offline_file: ""
offline_thumbnail: ""
uuid: 1749b4f8-9bcb-409e-be71-acd1d8053e4e
updated: 1484310264
title: cut-off
categories:
    - Dictionary
---
cut off
     adj : detached by cutting; "cut flowers"; "a severed head"; "an
           old tale of Anne Bolyn walking the castle walls with
           her poor cut-off head under her arm" [syn: {severed}]
     v 1: make a break in; "We interrupt the program for the following
          messages" [syn: {interrupt}, {disrupt}, {break up}]
     2: cease, stop; "cut the noise"; "We had to cut short the
        conversation" [syn: {cut}]
     3: remove by or as if by cutting; "cut off the ear"; "lop off
        the dead branch" [syn: {chop off}, {lop off}]
     4: cut off and stop; "The bicyclist was cut out by the van"
        [syn: {cut out}]
     5: break a small piece off from; "chip the ...
