---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnetite
offline_file: ""
offline_thumbnail: ""
uuid: 91ff4cc5-da42-4a43-887b-8fb4de0c298f
updated: 1484310409
title: magnetite
categories:
    - Dictionary
---
magnetite
     n : an oxide of iron that is strongly attracted by magnets [syn:
          {magnetic iron-ore}]
