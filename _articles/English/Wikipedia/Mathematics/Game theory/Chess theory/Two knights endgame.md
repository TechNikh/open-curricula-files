---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Two_knights_endgame
offline_file: ""
offline_thumbnail: ""
uuid: e2dbce6c-4e4b-47d5-8beb-df3cd3b6f50f
updated: 1484309020
title: Two knights endgame
tags:
    - Two knights cannot force checkmate
    - In the corner
    - On the edge
    - Examples from games
    - Troitsky line
    - Examples
    - Pawn beyond the Troitsky line
    - Topalov versus Karpov
    - Wang versus Anand
    - Second Troitsky line
    - More pawns
    - Example from game
    - Position of mutual zugzwang
    - Checkmate in problems
    - Angos
    - de Musset
    - Sobolevsky
    - Nadanian
    - History
categories:
    - Chess theory
---
The two knights endgame is a chess endgame with a king and two knights versus a king, possibly with some other material. The material with the defending king is usually one pawn, but some positions studied involve additional pawns or other pieces. In contrast to a king plus two bishops (on opposite-colored squares), or a bishop and a knight, a king and two knights cannot force checkmate against a lone king. (However, the superior side can force stalemate.) Although there are checkmate positions, the superior side cannot force them against proper (and easy) defense (Speelman, Tisdall & Wade 1993:11).
