---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flash
offline_file: ""
offline_thumbnail: ""
uuid: f794bfea-1e8e-40fa-9e25-2953b6999c32
updated: 1484310462
title: flash
categories:
    - Dictionary
---
flash
     adj : tastelessly showy; "a flash car"; "a flashy ring"; "garish
           colors"; "a gaudy costume"; "loud sport shirts"; "a
           meretricious yet stylish book"; "tawdry ornaments"
           [syn: {brassy}, {cheap}, {flashy}, {garish}, {gaudy}, {gimcrack},
            {loud}, {meretricious}, {tacky}, {tatty}, {tawdry}, {trashy}]
     n 1: a sudden intense burst of radiant energy
     2: a momentary brightness
     3: a short vivid experience; "a flash of emotion swept over
        him"; "the flashings of pain were a warning" [syn: {flashing}]
     4: a sudden brilliant understanding; "he had a flash of
        intuition"
     5: a very short time (as the time it takes ...
