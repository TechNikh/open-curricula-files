---
version: 1
type: video
provider: YouTube
id: r18Gi8lSkfM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 586a4243-2c20-4769-af27-d0d4799b3744
updated: 1486069656
title: Fourier Transform, Fourier Series, and frequency spectrum
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/r18Gi8lSkfM/default.jpg
    - https://i3.ytimg.com/vi/r18Gi8lSkfM/1.jpg
    - https://i3.ytimg.com/vi/r18Gi8lSkfM/2.jpg
    - https://i3.ytimg.com/vi/r18Gi8lSkfM/3.jpg
---
