---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bound
offline_file: ""
offline_thumbnail: ""
uuid: 03f5d915-fa0e-4a76-985c-30d6742e7ca0
updated: 1484310595
title: bound
categories:
    - Dictionary
---
bind
     n : something that hinders as if with bonds
     v 1: stick to firmly; "Will this wallpaper adhere to the wall?"
          [syn: {adhere}, {hold fast}, {bond}, {stick}, {stick to}]
     2: create social or emotional ties; "The grandparents want to
        bond with the child" [syn: {tie}, {attach}, {bond}]
     3: make fast; tie or secure, with or as if with a rope; "The
        Chinese would bind the feet of their women" [ant: {unbind}]
     4: wrap around with something so as to cover or enclose [syn: {bandage}]
     5: secure with or as if with ropes; "tie down the prisoners";
        "tie up the old newspapes and bring them to the recycling
        shed" [syn: {tie down}, {tie ...
