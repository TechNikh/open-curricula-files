---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wedge
offline_file: ""
offline_thumbnail: ""
uuid: 9fe8d60a-847c-44d4-abe0-537e14cddc31
updated: 1484310214
title: wedge
categories:
    - Dictionary
---
wedge
     n 1: any shape that is triangular in cross section [syn: {wedge
          shape}, {cuneus}]
     2: a large sandwich made of a long crusty roll split lengthwise
        and filled with meats and cheese (and tomato and onion and
        lettuce and condiments); different names are used in
        different sections of the United States [syn: {bomber}, {grinder},
         {hero}, {hero sandwich}, {hoagie}, {hoagy}, {Cuban
        sandwich}, {Italian sandwich}, {poor boy}, {sub}, {submarine},
         {submarine sandwich}, {torpedo}, {zep}]
     3: a diacritical mark (an inverted circumflex) placed above
        certain letters (such as c) to indicate pronunciation
        [syn: ...
