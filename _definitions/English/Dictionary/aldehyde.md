---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aldehyde
offline_file: ""
offline_thumbnail: ""
uuid: 6307b906-1507-497d-983f-7a1c9ee99b33
updated: 1484310426
title: aldehyde
categories:
    - Dictionary
---
aldehyde
     n : any of a class of highly reactive chemical compounds; used
         in making resins and dyes and organic acids
