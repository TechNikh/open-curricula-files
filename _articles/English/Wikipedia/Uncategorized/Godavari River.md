---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Godavari
offline_file: ""
offline_thumbnail: ""
uuid: ec704707-cf1a-4379-83cb-3e1b5bcfcdc6
updated: 1484308340
title: Godavari River
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/245px-Godavari-_river_basin.gif
tags:
    - Course
    - Within Maharashtra
    - Within Telangana
    - Within Andhra Pradesh
    - Tributaries
    - Etymology
    - Settlements along the Godavari
    - Places of interest
    - Flora and Fauna
    - Waterfalls
    - Crossings
    - Dams
    - Hydro power stations
    - Mineral deposits
    - Ecological concerns
    - Culture and News
categories:
    - Uncategorized
---
The Godavari is the second longest river in India after the river Ganges having its source at Tryambakeshwar, Maharashtra.[4] It starts in Maharashtra and flows east for 1,465 kilometres (910 mi) emptying into Bay of Bengal draining the Indian states Maharashtra (48.6%), Telangana( 18.8%), Andhra Pradesh (4.5%), Chhattisgarh (10.9%), Madhya Pradesh (10.0%), Odisha (5.7%) and Karnataka (1.4%)[5] through its extensive network of tributaries. Measuring up to 312,812 km2 (120,777 sq mi), it forms one of the largest river basins in the Indian subcontinent, with only the Ganges and Indus rivers having a drainage basin larger than it in India.[6] In terms of length, catchment area and ...
