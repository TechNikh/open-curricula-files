---
version: 1
type: video
provider: YouTube
id: 0w-tcUgAW3c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Past%20Perfect%20Progressive%20Tense.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1f8a9380-3757-4473-8f3b-8e602fc95d8c
updated: 1486069632
title: Past Perfect Progressive Tense
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/0w-tcUgAW3c/default.jpg
    - https://i3.ytimg.com/vi/0w-tcUgAW3c/1.jpg
    - https://i3.ytimg.com/vi/0w-tcUgAW3c/2.jpg
    - https://i3.ytimg.com/vi/0w-tcUgAW3c/3.jpg
---
