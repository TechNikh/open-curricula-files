---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reported
offline_file: ""
offline_thumbnail: ""
uuid: 52e18ca9-af5f-4e8d-add8-d06b1ca2e18b
updated: 1484310479
title: reported
categories:
    - Dictionary
---
reported
     adj : made known or told about; especially presented in a formal
           account; "his reported opinion"; "the reported
           findings" [ant: {unreported}]
