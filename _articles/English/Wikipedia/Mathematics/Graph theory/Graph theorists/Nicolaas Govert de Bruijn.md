---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nicolaas_Govert_de_Bruijn
offline_file: ""
offline_thumbnail: ""
uuid: 7bad78a4-a6ea-43df-8324-9b29e06b2646
updated: 1484309459
title: Nicolaas Govert de Bruijn
tags:
    - Biography
    - Work
    - Publications
categories:
    - Graph theorists
---
Nicolaas Govert (Dick) de Bruijn (Dutch pronunciation: [nikoːˈlaːs ˈxoːvərt də ˈbrœy̯n];[1] 9 July 1918 – 17 February 2012) was a Dutch mathematician and Emeritus Professor of mathematics at the Eindhoven University of Technology, noted for his many contributions in the fields of analysis, number theory, combinatorics and logic.[2]
