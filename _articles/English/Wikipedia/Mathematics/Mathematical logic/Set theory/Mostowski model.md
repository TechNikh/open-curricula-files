---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mostowski_model
offline_file: ""
offline_thumbnail: ""
uuid: 6e4bfd5c-4ae1-43df-8e76-ce2d28dcc621
updated: 1484309510
title: Mostowski model
categories:
    - Set theory
---
In mathematical set theory, the Mostowski model is a model of set theory with atoms where the full axiom of choice fails, but every set can be linearly ordered. It was introduced by Mostowski (1939). The Mostowski model can be constructed as the permutation model corresponding to the group of all automorphisms of the ordered set of rational numbers and the ideal of finite subsets of the rational numbers.
