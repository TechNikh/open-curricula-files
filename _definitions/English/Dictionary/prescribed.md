---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prescribed
offline_file: ""
offline_thumbnail: ""
uuid: 923190a6-bfaa-4757-834f-5be7ec5371a5
updated: 1484310517
title: prescribed
categories:
    - Dictionary
---
prescribed
     adj 1: set down as a rule or guide
     2: fixed or established especially by order or command; "at the
        time appointed (or the appointed time") [syn: {appointed},
         {decreed}, {ordained}]
     3: conforming to set usage, procedure, or discipline; "in
        prescribed order" [syn: {official}]
     4: formally laid down or imposed; "positive laws" [syn: {positive}]
