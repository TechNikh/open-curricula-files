---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cycle_index
offline_file: ""
offline_thumbnail: ""
uuid: e94ea124-06ac-4361-99d2-7bdefb2ee82a
updated: 1484308582
title: Cycle index
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Face_colored_cube.png
tags:
    - Permutation groups and group actions
    - Disjoint cycle representation of permutations
    - Definition
    - Example
    - Types of actions
    - >
        The cycle index of the edge permutation group of the
        complete graph on three vertices
    - >
        The cycle index of the edge permutation group of the
        complete graph on four vertices
    - The cycle index of the face permutations of a cube
    - Cycle indices of some permutation groups
    - Identity group En
    - Cyclic group Cn
    - Dihedral group Dn
    - The alternating group An
    - The symmetric group Sn
    - Applications
    - Notes
categories:
    - Combinatorics
---
In combinatorial mathematics a cycle index is a polynomial in several variables which is structured in such a way that information about how a group of permutations acts on a set can be simply read off from the coefficients and exponents. This compact way of storing information in an algebraic form is frequently used in combinatorial enumeration.
