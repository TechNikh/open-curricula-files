---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Sperner%27s_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: bae592b6-25db-4c9d-a47e-5e626f3930c3
updated: 1484308603
title: "Sperner's lemma"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/20px-Sperner1d.svg.png
tags:
    - Statement
    - One-dimensional case
    - Two-dimensional case
    - Multidimensional case
    - Proof
    - Commentary
    - Generalizations
    - Applications
    - Equivalent results
categories:
    - Combinatorics
---
Sperner's lemma states that every Sperner coloring (described below) of a triangulation of an n-dimensional simplex contains a cell colored with a complete set of colors.
