---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Epispiral
offline_file: ""
offline_thumbnail: ""
uuid: f2b22b1b-c169-460c-9655-5ca4e65c0e40
updated: 1484309359
title: Epispiral
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/226px-Epispiral.svg.png
categories:
    - Geometry stubs
---
There are n sections if n is odd and 2n if n is even.
