---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guess
offline_file: ""
offline_thumbnail: ""
uuid: ef607750-9106-4331-906d-cc4490381ffd
updated: 1484310341
title: guess
categories:
    - Dictionary
---
guess
     n 1: a message expressing an opinion based on incomplete evidence
          [syn: {conjecture}, {supposition}, {surmise}, {surmisal},
           {speculation}, {hypothesis}]
     2: an estimate based on little or no information [syn: {guesswork},
         {guessing}, {shot}, {dead reckoning}]
     v 1: expect, believe, or suppose; "I imagine she earned a lot of
          money with her new novel"; "I thought to find her in a
          bad state"; "he didn't think to find her in the
          kitchen"; "I guess she is angry at me for standing her
          up" [syn: {think}, {opine}, {suppose}, {imagine}, {reckon}]
     2: put forward, of a guess, in spite of possible refutation; ...
