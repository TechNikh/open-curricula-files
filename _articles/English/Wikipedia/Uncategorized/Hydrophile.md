---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hydrophilic
offline_file: ""
offline_thumbnail: ""
uuid: 6038f9d2-1c50-4817-b085-f7505ce21665
updated: 1484308405
title: Hydrophile
tags:
    - Molecules
    - Chemicals
    - Liquid chemicals
    - Alcohols
    - Solid chemicals
    - Cyclodextrins
    - Membrane filtration
categories:
    - Uncategorized
---
A hydrophile is a molecule or other molecular entity that is attracted to water molecules and tends to be dissolved by water.[1] In contrast, hydrophobes are not attracted to water and may seem to be repelled by it.
