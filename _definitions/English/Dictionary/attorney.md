---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attorney
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484421361
title: attorney
categories:
    - Dictionary
---
attorney
     n : a professional person authorized to practice law; conducts
         lawsuits or gives legal advice [syn: {lawyer}]
