---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_body
offline_file: ""
offline_thumbnail: ""
uuid: 16240517-80a7-4e4d-aa22-73877aae8348
updated: 1484309285
title: Convex body
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dodecahedron.svg.png
categories:
    - Multi-dimensional geometry
---
In mathematics, a convex body in n-dimensional Euclidean space 
  
    
      
        
          
            R
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {R} ^{n}}
  
 is a compact convex set with non-empty interior.
