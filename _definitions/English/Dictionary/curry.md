---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curry
offline_file: ""
offline_thumbnail: ""
uuid: e2315982-3309-4401-b0e1-5be016817ee9
updated: 1484310230
title: curry
categories:
    - Dictionary
---
curry
     n : (East Indian cookery) a pungent dish of vegetables or meats
         flavored with curry powder and usually eaten with rice
     v 1: season with a mixture of spices; typical of Indian cooking
     2: treat by incorporating fat; "curry tanned leather"
     3: give a neat appearance to; "groom the dogs"; "dress the
        horses" [syn: {dress}, {groom}]
     [also: {curried}]
