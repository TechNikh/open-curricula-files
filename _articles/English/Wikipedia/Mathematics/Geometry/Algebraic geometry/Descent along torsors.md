---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Descent_along_torsors
offline_file: ""
offline_thumbnail: ""
uuid: 3e130c1d-e3f6-4c26-872d-76ac2182c4fa
updated: 1484309142
title: Descent along torsors
categories:
    - Algebraic geometry
---
In mathematics, given a G-torsor X → Y and a stack F, the descent along torsors says there is a canonical equivalence between F(Y), the category of Y-points and F(X)G, the category of G-equivariant X-points.[1] It is a basic example of descent, since it says the "equivariant data" (which is an additional data) allows one to "descend" from X to Y.
