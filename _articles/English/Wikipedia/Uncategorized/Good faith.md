---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bona_fide
offline_file: ""
offline_thumbnail: ""
uuid: ceb958f6-32ab-4b13-a72a-96434d006669
updated: 1484308460
title: Good faith
tags:
    - Bona fides
    - Law
    - Philosophy
    - Good faith employment efforts
    - Good faith in wikis
categories:
    - Uncategorized
---
Good faith (Latin: bona fides), in human interactions, is a sincere intention to be fair, open, and honest, regardless of the outcome of the interaction. While some Latin phrases lose their literal meaning over centuries, this is not the case with bona fides; it is still widely used and interchangeable with its generally accepted modern day translation of good faith.[1] It is an important concept within law, philosophy, and business. The opposed concepts are bad faith, mala fides (duplicity) and perfidy (pretense). In contemporary English, the usage of bona fides (note the "s") is synonymous with credentials and identity. The phrase is sometimes used in job advertisements, and should not be ...
