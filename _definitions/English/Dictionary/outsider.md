---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outsider
offline_file: ""
offline_thumbnail: ""
uuid: 7cb9975a-1dd0-470f-a266-d57f9b9ae23e
updated: 1484310462
title: outsider
categories:
    - Dictionary
---
outsider
     n 1: someone who is excluded from or is not a member of a group
          [syn: {foreigner}]
     2: a constestant (human or animal) not considered to have a
        good chance to win
