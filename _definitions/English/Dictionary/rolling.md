---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rolling
offline_file: ""
offline_thumbnail: ""
uuid: 46e1d705-772e-436b-b896-29823e54296e
updated: 1484310579
title: rolling
categories:
    - Dictionary
---
rolling
     adj 1: characterized by reverberation; "a resonant voice"; "hear
            the rolling thunder" [syn: {resonant}, {resonating}, {resounding},
             {reverberating}, {reverberative}]
     2: uttered with a trill; "she used rolling r's as in Spanish"
        [syn: {rolled}, {trilled}]
     3: moving in surges and billows and rolls; "billowing smoke
        from burning houses"; "the rolling fog"; "the rolling
        sea"; "the tumbling water of the rapids" [syn: {billowing},
         {tumbling}]
     n 1: a deep prolonged sound (as of thunder or large bells) [syn:
          {peal}, {pealing}, {roll}]
     2: the act of robbing a helpless person; "he was charged with
    ...
