---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyclic_number_(group_theory)
offline_file: ""
offline_thumbnail: ""
uuid: d63f244f-7c0a-4fc0-aa5c-c621945a7920
updated: 1484308639
title: Cyclic number (group theory)
categories:
    - Number theory
---
A cyclic number[1] is a natural number n such that n and φ(n) are coprime. Here φ is Euler's totient function. An equivalent definition is that a number n is cyclic iff any group of order n is cyclic.[2]
