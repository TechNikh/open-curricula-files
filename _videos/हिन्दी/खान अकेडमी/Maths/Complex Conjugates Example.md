---
version: 1
type: video
provider: YouTube
id: 5MzCm16JN0k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c81fd895-657b-4ae0-a7bd-815d2004e579
updated: 1486069611
title: Complex Conjugates Example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/5MzCm16JN0k/default.jpg
    - https://i3.ytimg.com/vi/5MzCm16JN0k/1.jpg
    - https://i3.ytimg.com/vi/5MzCm16JN0k/2.jpg
    - https://i3.ytimg.com/vi/5MzCm16JN0k/3.jpg
---
