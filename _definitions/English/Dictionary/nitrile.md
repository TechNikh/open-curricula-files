---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nitrile
offline_file: ""
offline_thumbnail: ""
uuid: 3fd0002d-dfd8-4166-9ca3-dd8fd85ea494
updated: 1484310421
title: nitrile
categories:
    - Dictionary
---
nitrile
     n : any of a class of organic compounds containing the cyano
         radical -CN [syn: {nitril}, {cyanide}]
