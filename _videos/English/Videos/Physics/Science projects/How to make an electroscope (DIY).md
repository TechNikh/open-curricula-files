---
version: 1
type: video
provider: YouTube
id: 2PmWlPjV6n0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0e1ab9a0-f1eb-42df-b069-328aca67cdc9
updated: 1486069632
title: How to make an electroscope (DIY)
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/2PmWlPjV6n0/default.jpg
    - https://i3.ytimg.com/vi/2PmWlPjV6n0/1.jpg
    - https://i3.ytimg.com/vi/2PmWlPjV6n0/2.jpg
    - https://i3.ytimg.com/vi/2PmWlPjV6n0/3.jpg
---
