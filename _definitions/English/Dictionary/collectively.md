---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collectively
offline_file: ""
offline_thumbnail: ""
uuid: 20323cbe-c3b0-4610-84b9-1c36121736ab
updated: 1484310258
title: collectively
categories:
    - Dictionary
---
collectively
     adv : in conjunction with; combined; "Our salaries put together
           couldn't pay for the damage"; "we couldn`t pay for the
           damages with all out salaries put together" [syn: {jointly},
            {conjointly}, {together}, {put together}]
