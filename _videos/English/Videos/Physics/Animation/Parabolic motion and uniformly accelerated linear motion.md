---
version: 1
type: video
provider: YouTube
id: z24_ihikEqQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Parabolic%20motion%20and%20uniformly%20accelerated%20linear%20motion.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 17b54397-c82c-475e-bc29-ef3d0f0e68a5
updated: 1486069652
title: Parabolic motion and uniformly accelerated linear motion
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/z24_ihikEqQ/default.jpg
    - https://i3.ytimg.com/vi/z24_ihikEqQ/1.jpg
    - https://i3.ytimg.com/vi/z24_ihikEqQ/2.jpg
    - https://i3.ytimg.com/vi/z24_ihikEqQ/3.jpg
---
