---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gdr
offline_file: ""
offline_thumbnail: ""
uuid: 3bd566ea-e916-433d-b26c-2ea8fa4a568d
updated: 1484308482
title: East Germany
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Soviet_Sector_Germany.png
tags:
    - Naming conventions
    - History
    - Origins
    - 1949 establishment
    - Soviet role
    - Zones of occupation
    - Partition
    - GDR identity
    - Die Wende (German Reunification)
    - Politics
categories:
    - Uncategorized
---
East Germany, formally the German Democratic Republic or GDR (German: Deutsche Demokratische Republik [ˈdɔʏtʃə demoˈkʀaːtɪʃə ʀepuˈbliːk]), was an Eastern Bloc state during the Cold War period. From 1949 to 1990, it administered the region of Germany that was occupied by Soviet forces at the end of World War II—the Soviet Occupation Zone of the Potsdam Agreement, bounded on the east by the Oder–Neisse line. The Soviet zone surrounded West Berlin, but did not include it; as a result, West Berlin remained outside the jurisdiction of the GDR. The German Democratic Republic was established in the Soviet Zone, while the Federal Republic was established in the three western ...
