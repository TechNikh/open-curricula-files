---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acquire
offline_file: ""
offline_thumbnail: ""
uuid: 2891bfbc-c99d-435d-adf8-297d8bba7100
updated: 1484310226
title: acquire
categories:
    - Dictionary
---
acquire
     v 1: come into the possession of something concrete or abstract;
          "She got a lot of paintings from her uncle"; "They
          acquired a new pet"; "Get your results the next day";
          "Get permission to take a few days off from work" [syn:
          {get}]
     2: take on a certain form, attribute, or aspect; "His voice
        took on a sad tone"; "The story took a new turn"; "he
        adopted an air of superiority"; "She assumed strange
        manners"; "The gods assume human or animal form in these
        fables" [syn: {assume}, {adopt}, {take on}, {take}]
     3: come to have or undergo a change of (physical features and
        attributes); "He grew a ...
