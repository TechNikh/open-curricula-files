---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/colourful
offline_file: ""
offline_thumbnail: ""
uuid: 5cfa44da-ebe3-4f66-b3a0-7319646a7516
updated: 1484310212
title: colourful
categories:
    - Dictionary
---
colourful
     adj : having striking color; "colorful autumn leaves" [syn: {colorful}]
           [ant: {colorless}]
