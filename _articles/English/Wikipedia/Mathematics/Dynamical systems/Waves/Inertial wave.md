---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inertial_wave
offline_file: ""
offline_thumbnail: ""
uuid: 380bc5ff-b55b-4f15-9668-59a70c224be2
updated: 1484308893
title: Inertial wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Inertial_waves.jpg
tags:
    - Restoring force
    - characteristics
    - Examples of inertial waves
    - Mathematical description
categories:
    - Waves
---
Inertial waves, also known as inertial oscillations, are a type of mechanical wave possible in rotating fluids. Unlike surface gravity waves commonly seen at the beach or in the bathtub, inertial waves flow through the interior of the fluid, not at the surface. Like any other kind of wave, an inertial wave is caused by a restoring force and characterized by its wavelength and frequency. Because the restoring force for inertial waves is the Coriolis force, their wavelengths and frequencies are related in a peculiar way. Inertial waves are transverse. Most commonly they are observed in atmospheres, oceans, lakes, and laboratory experiments. Rossby waves, geostrophic currents, and geostrophic ...
