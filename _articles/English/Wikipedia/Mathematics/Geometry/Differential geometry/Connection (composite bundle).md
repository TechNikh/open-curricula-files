---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Connection_(composite_bundle)
offline_file: ""
offline_thumbnail: ""
uuid: 847121bd-fc56-4ebc-8482-938ff258974f
updated: 1484309209
title: Connection (composite bundle)
tags:
    - Composite bundle
    - Composite principal bundle
    - Jet manifolds of a composite bundle
    - Composite connection
    - Vertical covariant differential
categories:
    - Differential geometry
---
Composite bundles 
  
    
      
        Y
        →
        Σ
        →
        X
      
    
    {\displaystyle Y\to \Sigma \to X}
  
 play a prominent role in gauge theory with symmetry breaking, e.g., gauge gravitation theory, non-autonomous mechanics where 
  
    
      
        X
        =
        
          R
        
      
    
    {\displaystyle X=\mathbb {R} }
  
 is the time axis, e.g., mechanics with time-dependent parameters, and so on. There are the important relations between connections on fiber bundles 
  
    
      
        Y
        →
        X
      
    
    {\displaystyle Y\to X}
  
, 
  
    
      
        Y
        →
        Σ
      
    
    ...
