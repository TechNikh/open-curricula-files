---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tag_(game)
offline_file: ""
offline_thumbnail: ""
uuid: 9a0315a8-f5f8-465a-bf4d-6678db7037a2
updated: 1484309062
title: Tag (game)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jongensspelen_10.jpg
tags:
    - Basic rules
    - Base and truce terms
    - Bans and restrictions
    - Variants
    - British bulldogs
    - Chain tag
    - Duck, duck, goose
    - Freeze tag
    - Kiss chase
    - Last tag
    - Octopus tag
    - Shipwrecked
    - Team tag
    - Cops and Robbers
    - Zombie tag
    - Manhunt
    - "Prisoner's Base"
    - "What's the time, Mr Wolf?"
    - Ringolevio
    - Variants requiring equipment
    - "Blind man's buff"
    - Computer tag
    - Flashlight tag
    - Fox and geese
    - Kick the can
    - Laser tag
    - Muckle
    - Paintball
    - Sock tag
    - Spud
    - Vibrator Tag
    - Team tag sports
categories:
    - Pursuit-evasion
---
Tag (also known as it, tig[1][2] and many other names[3]) is a playground game that involves one or more players chasing other players in an attempt to "tag" or touch them, usually with their hands. There are many variations; most forms have no teams, scores, or equipment. Usually when a person is tagged, the tagger says, "Tag, you're it".
