---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moderated
offline_file: ""
offline_thumbnail: ""
uuid: 4eeceb4c-6147-449a-b70b-3afa9af85110
updated: 1484310451
title: moderated
categories:
    - Dictionary
---
moderated
     adj : having elements or qualities mixed in proper or suitable
           proportions; especially made less severe; "justice
           moderated with mercy" [syn: {qualified}]
