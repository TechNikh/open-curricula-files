---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equate
offline_file: ""
offline_thumbnail: ""
uuid: c0171f95-ba7d-4e42-a14d-f499ee3397d9
updated: 1484310373
title: equate
categories:
    - Dictionary
---
equate
     v 1: consider or describe as similar, equal, or analogous; "We
          can compare the Han dynasty to the Romans"; "You cannot
          equate success in financial matters with greed" [syn: {compare},
           {liken}]
     2: be equivalent or parallel, in mathematics [syn: {correspond}]
     3: make equal, uniform, corresponding, or matching; "let's
        equalize the duties among all employees in this office";
        "The company matched the discount policy of its
        competitors" [syn: {equal}, {match}, {equalize}, {equalise}]
