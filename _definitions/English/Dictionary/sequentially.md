---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sequentially
offline_file: ""
offline_thumbnail: ""
uuid: 2bc541d6-75ba-4630-bb98-c3befd569f90
updated: 1484310426
title: sequentially
categories:
    - Dictionary
---
sequentially
     adv : in a consecutive manner; "we numbered the papers
           consecutively" [syn: {consecutive}]
