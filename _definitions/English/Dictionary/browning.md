---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/browning
offline_file: ""
offline_thumbnail: ""
uuid: fd0dc233-7544-4a7b-9fe0-042fb35f6f11
updated: 1484310375
title: browning
categories:
    - Dictionary
---
Browning
     n 1: United States inventor of firearms (especially automatic
          pistols and repeating rifles and a machine gun called
          the Peacemaker) (1855-1926) [syn: {John M. Browning}, {John
          Moses Browning}]
     2: English poet and husband of Elizabeth Barrett Browning noted
        for his dramatic monologues (1812-1889) [syn: {Robert
        Browning}]
     3: English poet best remembered for love sonnets written to her
        husband Robert Browning (1806-1861) [syn: {Elizabeth
        Barrett Browning}]
     4: cooking to a brown crispiness over a fire or on a grill;
        "proper toasting should brown both sides of a piece of
        bread" [syn: ...
