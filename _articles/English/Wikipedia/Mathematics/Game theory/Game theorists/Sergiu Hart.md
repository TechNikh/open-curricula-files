---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sergiu_Hart
offline_file: ""
offline_thumbnail: ""
uuid: c6583544-245d-4937-a0a2-5fd8e832c6dc
updated: 1484308512
title: Sergiu Hart
tags:
    - Biography
    - Research contributions
    - Awards and honors
categories:
    - Game theorists
---
Sergiu Hart (born 1949) is an Israeli mathematician and economist and the past President of the Game Theory Society (2008 – 2010). He is the Kusiel-Vorreuter University Professor, Professor of Mathematics, and Professor of Economics, at the Center for the Study of Rationality at the Hebrew University of Jerusalem in Israel.
