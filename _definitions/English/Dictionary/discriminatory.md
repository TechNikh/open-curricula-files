---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discriminatory
offline_file: ""
offline_thumbnail: ""
uuid: d4e3084c-2159-4e61-86ad-1738ac526b16
updated: 1484310172
title: discriminatory
categories:
    - Dictionary
---
discriminatory
     adj 1: being biased or having a belief or attitude formed
            beforehand; "a prejudiced judge" [syn: {prejudiced}]
            [ant: {unprejudiced}]
     2: containing or implying a slight or showing prejudice;
        "discriminatory attitudes and practices"; "invidious
        comparisons" [syn: {invidious}]
     3: capable of making fine distinctions [syn: {discriminative}]
     4: manifesting partiality; "a discriminatory tax";
        "preferential tariff rates"; "preferential treatment"; "a
        preferential shop gives priority or advantage to union
        members in hiring or promoting" [syn: {preferential}]
