---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radiation
offline_file: ""
offline_thumbnail: ""
uuid: b14c62a1-a86e-47d5-820d-bccb099e8254
updated: 1484310262
title: radiation
categories:
    - Dictionary
---
radiation
     n 1: energy that is radiated or transmitted in the form of rays
          or waves or particles
     2: the act of spreading outward from a central source
     3: syndrome resulting from exposure to ionizing radiation
        (e.g., exposure to radioactive chemicals or to nuclear
        explosions); low doses cause diarrhea and nausea and
        vomiting and sometimes loss of hair; greater exposure can
        cause sterility and cataracts and some forms of cancer and
        other diseases; severe exposure can cause death within
        hours; "he was suffering from radiation" [syn: {radiation
        sickness}, {radiation syndrome}]
     4: the spontaneous emission of a ...
