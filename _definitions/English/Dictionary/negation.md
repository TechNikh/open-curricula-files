---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/negation
offline_file: ""
offline_thumbnail: ""
uuid: b5974339-047b-4bc9-b817-ffda32979f98
updated: 1484310186
title: negation
categories:
    - Dictionary
---
negation
     n 1: a negative statement; a statement that is a refusal or
          denial of some other statement
     2: the speech act of negating
     3: (logic) a proposition that is true if and only if another
        proposition is false
