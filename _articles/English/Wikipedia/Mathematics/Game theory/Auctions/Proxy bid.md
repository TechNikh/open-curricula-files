---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proxy_bid
offline_file: ""
offline_thumbnail: ""
uuid: d0feecc1-7cbc-4154-bb36-fcd983b6d53b
updated: 1484309017
title: Proxy bid
categories:
    - Auctions
---
Proxy bidding is an implementation of an English second-price auction used on eBay, in which the winning bidder pays the price of the second-highest bid plus a defined increment. It differs from a Vickrey auction in that bids are not sealed; the "current highest bid" (defined as second-highest bid plus bid increment) is always displayed.
