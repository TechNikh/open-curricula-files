---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Disk_covering_problem
offline_file: ""
offline_thumbnail: ""
uuid: 553720a3-9b93-4dd3-a2af-b9deac8a602e
updated: 1484308603
title: Disk covering problem
categories:
    - Discrete geometry
---
The disk covering problem asks for the smallest real number 
  
    
      
        r
        (
        n
        )
      
    
    {\displaystyle r(n)}
  
 such that 
  
    
      
        n
      
    
    {\displaystyle n}
  
 disks of radius 
  
    
      
        r
        (
        n
        )
      
    
    {\displaystyle r(n)}
  
 can be arranged in such a way as to cover the unit disk. Dually, for a given radius ε, one wishes to find the smallest integer n such that n disks of radius ε can cover the unit disk.[1]
