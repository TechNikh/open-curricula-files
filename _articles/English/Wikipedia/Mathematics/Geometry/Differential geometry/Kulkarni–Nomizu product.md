---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kulkarni%E2%80%93Nomizu_product'
offline_file: ""
offline_thumbnail: ""
uuid: 9d550de1-c198-4fa5-b632-2f45d536eebb
updated: 1484309226
title: Kulkarni–Nomizu product
categories:
    - Differential geometry
---
In the mathematical field of differential geometry, the Kulkarni–Nomizu product (named for Ravindra Shripad Kulkarni and Katsumi Nomizu) is defined for two (0,2)-tensors and gives as a result a (0,4)-tensor.
