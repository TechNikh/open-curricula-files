---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/origen
offline_file: ""
offline_thumbnail: ""
uuid: cdb628d9-08fd-4b7b-920e-6737e910c04e
updated: 1484310289
title: origen
categories:
    - Dictionary
---
Origen
     n : Greek philosopher and theologian who reinterpreted Christian
         doctrine through the philosophy of Neoplatonism; his work
         was later condemned as unorthodox
