---
version: 1
type: video
provider: YouTube
id: qdFD-0OWBRo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Green%27s%20theorem%20proof%20%28part%202%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f3c9202d-75f0-4f1b-b2e1-cd64576a2847
updated: 1486069696
title: "Green's theorem proof (part 2)"
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/qdFD-0OWBRo/default.jpg
    - https://i3.ytimg.com/vi/qdFD-0OWBRo/1.jpg
    - https://i3.ytimg.com/vi/qdFD-0OWBRo/2.jpg
    - https://i3.ytimg.com/vi/qdFD-0OWBRo/3.jpg
---
