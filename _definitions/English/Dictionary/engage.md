---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/engage
offline_file: ""
offline_thumbnail: ""
uuid: 91bdc346-08dd-4051-86f0-f343ba01bfca
updated: 1484310486
title: engage
categories:
    - Dictionary
---
engage
     v 1: carry out or participate in an activity; be involved in;
          "She pursued many activities"; "They engaged in a
          discussion" [syn: {prosecute}, {pursue}]
     2: engage or engross wholly; "Her interest in butterflies
        absorbs her completely" [syn: {absorb}, {engross}, {occupy}]
     3: engage or hire for work; "They hired two new secretaries in
        the department"; "How many people has she employed?" [syn:
         {hire}, {employ}] [ant: {fire}]
     4: ask to represent; of legal counsel; "I'm retaining a lawyer"
     5: give to in marriage [syn: {betroth}, {affiance}, {plight}]
     6: get caught; "make sure the gear is engaged" [ant: ...
