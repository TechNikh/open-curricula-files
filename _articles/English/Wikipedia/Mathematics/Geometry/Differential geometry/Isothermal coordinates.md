---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isothermal_coordinates
offline_file: ""
offline_thumbnail: ""
uuid: 48fe3d01-c278-4dd0-b14c-bcbd7edb9481
updated: 1484309226
title: Isothermal coordinates
tags:
    - Isothermal coordinates on surfaces
    - Beltrami equation
    - Hodge star operator
    - Gaussian curvature
    - Notes
categories:
    - Differential geometry
---
In mathematics, specifically in differential geometry, isothermal coordinates on a Riemannian manifold are local coordinates where the metric is conformal to the Euclidean metric. This means that in isothermal coordinates, the Riemannian metric locally has the form
