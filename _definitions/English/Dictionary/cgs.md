---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cgs
offline_file: ""
offline_thumbnail: ""
uuid: 8ca76d89-83c9-449d-bd8e-954cc1d84842
updated: 1484310234
title: cgs
categories:
    - Dictionary
---
cgs
     n : system of measurement based on centimeters and grams and
         seconds [syn: {cgs system}]
