---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/truant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484562601
title: truant
categories:
    - Dictionary
---
truant
     adj : absent without permission; "truant schoolboys"; "the soldier
           was AWOL for almost a week" [syn: {awol}]
     n 1: one who is absent from school without permission [syn: {hooky
          player}]
     2: someone who shirks duty [syn: {no-show}, {nonattender}]
