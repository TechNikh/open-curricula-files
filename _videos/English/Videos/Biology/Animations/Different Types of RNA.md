---
version: 1
type: video
provider: YouTube
id: Zp-lOxxg6gw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6732915e-48be-403c-ba23-5e300793e1f3
updated: 1486069670
title: Different Types of RNA
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Zp-lOxxg6gw/default.jpg
    - https://i3.ytimg.com/vi/Zp-lOxxg6gw/1.jpg
    - https://i3.ytimg.com/vi/Zp-lOxxg6gw/2.jpg
    - https://i3.ytimg.com/vi/Zp-lOxxg6gw/3.jpg
---
