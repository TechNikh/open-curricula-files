---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adjustment
offline_file: ""
offline_thumbnail: ""
uuid: 6016f984-2e1e-4163-92cb-5898233ba9c4
updated: 1484310183
title: adjustment
categories:
    - Dictionary
---
adjustment
     n 1: making or becoming suitable; adjusting to circumstances
          [syn: {accommodation}, {fitting}]
     2: the act of making something different (as e.g. the size of a
        garment) [syn: {alteration}, {modification}]
     3: the act of adjusting something to match a standard [syn: {registration},
         {readjustment}]
     4: the process of adapting to something (such as environmental
        conditions) [syn: {adaptation}]
     5: an amount added or deducted on the basis of qualifying
        circumstances; "an allowance for profit" [syn: {allowance}]
