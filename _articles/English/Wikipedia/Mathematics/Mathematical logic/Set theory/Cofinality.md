---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cofinality
offline_file: ""
offline_thumbnail: ""
uuid: 9cc90da1-7f38-412e-9cbb-395c48919125
updated: 1484309501
title: Cofinality
tags:
    - Examples
    - Properties
    - Cofinality of ordinals and other well-ordered sets
    - Regular and singular ordinals
    - Cofinality of cardinals
categories:
    - Set theory
---
In mathematics, especially in order theory, the cofinality cf(A) of a partially ordered set A is the least of the cardinalities of the cofinal subsets of A.
