---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Heat_Transfer_with_Thermal_Waves_and_Thermal_Resonance
offline_file: ""
offline_thumbnail: ""
uuid: 354fa6fd-6515-4e4b-b61f-c8e9465d0c88
updated: 1484308845
title: Heat Transfer with Thermal Waves and Thermal Resonance
categories:
    - Thermodynamics
---
Thermal waves/resonance is the one of four basic heat-transfer modes. Unlike the other three modes (heat conduction, heat convection and heat radiation) that transport heat always from high temperature to low temperature with temperature gradient as the driving force, this mode of heat transfer comes from cross-coupling among different transport processes in the medium and can transport heat isothermally or even from low temperature to high temperature. This mode of heat transfer is characterized by its wave-type distributions of temperature or its various spatial/temporal derivatives depending on and tunable by the cross coupling. Thermal waves/resonance shows the strong predominance over ...
