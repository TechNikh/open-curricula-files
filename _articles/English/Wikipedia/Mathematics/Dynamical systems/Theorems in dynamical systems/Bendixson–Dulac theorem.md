---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bendixson%E2%80%93Dulac_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: b35ce318-e5b8-44d3-8fed-5e7979a1f3ad
updated: 1484308692
title: Bendixson–Dulac theorem
categories:
    - Theorems in dynamical systems
---
In mathematics, the Bendixson–Dulac theorem on dynamical systems states that if there exists a 
  
    
      
        
          C
          
            1
          
        
      
    
    {\displaystyle C^{1}}
  
 function 
  
    
      
        φ
        (
        x
        ,
        y
        )
      
    
    {\displaystyle \varphi (x,y)}
  
 (called the Dulac function) such that the expression
