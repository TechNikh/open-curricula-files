---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maximum_disjoint_set
offline_file: ""
offline_thumbnail: ""
uuid: 68ec623c-ef18-4859-b321-56767c528ace
updated: 1484309173
title: Maximum disjoint set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Point_quadtree.svg.png
tags:
    - Greedy algorithms
    - '1-dimensional intervals: exact polynomial algorithm'
    - 'Fat shapes: constant-factor approximations'
    - Divide-and-conquer algorithms
    - 'Axis-parallel rectangles: Logarithmic-factor approximation'
    - 'Axis-parallel rectangles with the same height: 2-approximation'
    - 'Axis-parallel rectangles with the same height: PTAS'
    - 'Fat objects with identical sizes: PTAS'
    - 'Fat objects with arbitrary sizes: PTAS'
    - Geometric separator algorithms
    - 'Fat objects with arbitrary sizes: PTAS using geometric separators'
    - 'Disks with a bounded size-ratio: exact sub-exponential algorithm'
    - Local search algorithms
    - 'Pseudo-disks: a PTAS'
    - Linear programming relaxation algorithms
    - 'Pseudo-disks: a PTAS'
    - Other classes of shapes for which approximations are known
    - Notes
categories:
    - Computational geometry
---
In computational geometry, a maximum disjoint set (MDS) is a largest set of non-overlapping geometric shapes selected from a given set of candidate shapes.
