---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incremental
offline_file: ""
offline_thumbnail: ""
uuid: 1b7cfd03-cdfc-4e7e-b1b9-2cf72d0d953a
updated: 1484310517
title: incremental
categories:
    - Dictionary
---
incremental
     adj : increasing gradually by regular degrees or additions;
           "lecturers enjoy...steady incremental growth in salary"
