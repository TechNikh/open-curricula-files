---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interaction
offline_file: ""
offline_thumbnail: ""
uuid: e9cf9d76-18cb-47c9-bf0b-2ba765ae8412
updated: 1484310275
title: interaction
categories:
    - Dictionary
---
interaction
     n 1: a mutual or reciprocal action; interacting
     2: (physics) the transfer of energy between elementary
        particles or between an elementary particle and a field or
        between fields; mediated by gauge bosons [syn: {fundamental
        interaction}]
