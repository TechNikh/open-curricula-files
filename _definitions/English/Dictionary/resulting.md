---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resulting
offline_file: ""
offline_thumbnail: ""
uuid: 94bd1213-7c07-4662-b734-d1eb8941d569
updated: 1484310252
title: resulting
categories:
    - Dictionary
---
resulting
     adj : following as an effect or result; "the period of tension and
           consequent need for military preparedness"; "the
           ensuant response to his appeal"; "the resultant savings
           were considerable"; "the health of the plants and the
           resulting flowers" [syn: {consequent}, {ensuant}, {resultant},
            {resulting(a)}, {sequent}]
