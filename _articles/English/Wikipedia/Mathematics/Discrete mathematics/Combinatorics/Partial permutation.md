---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partial_permutation
offline_file: ""
offline_thumbnail: ""
uuid: 93e9df82-10f4-418d-b497-27c30a28e096
updated: 1484308592
title: Partial permutation
tags:
    - Representation
    - Combinatorial enumeration
    - Restricted partial permutations
categories:
    - Combinatorics
---
In combinatorial mathematics, a partial permutation, or sequence without repetition, on a finite set S is a bijection between two specified subsets of S. That is, it is defined by two subsets U and V of equal size, and a one-to-one mapping from U to V. Equivalently, it is a partial function on S that can be extended to a permutation.[1][2]
