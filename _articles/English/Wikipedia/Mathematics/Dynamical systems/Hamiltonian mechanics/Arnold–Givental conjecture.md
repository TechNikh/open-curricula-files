---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Arnold%E2%80%93Givental_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: ef9e2594-9003-4a92-843e-2eb3d69a6d7f
updated: 1484308790
title: Arnold–Givental conjecture
categories:
    - Hamiltonian mechanics
---
The Arnold–Givental conjecture, named after Vladimir Arnold and Alexander Givental, is a statement on Lagrangian submanifolds. It gives a lower bound in terms of the Betti numbers of L on the number of intersection points of L with a Hamiltonian isotopic Lagrangian submanifold which intersects L transversally.
