---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enthusiastically
offline_file: ""
offline_thumbnail: ""
uuid: be39515e-19c1-437d-af01-51e06306ac5b
updated: 1484310448
title: enthusiastically
categories:
    - Dictionary
---
enthusiastically
     adv 1: with enthusiasm; in an enthusiastic manner; "they discussed
            the question enthusiastically" [ant: {unenthusiastically}]
     2: in a lavish or enthusiastic manner; "he extolled her virtues
        sky-high" [syn: {sky-high}]
