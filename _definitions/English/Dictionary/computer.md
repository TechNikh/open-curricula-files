---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/computer
offline_file: ""
offline_thumbnail: ""
uuid: aa7a7483-021a-449a-9dd7-e44d61db2c95
updated: 1484310208
title: computer
categories:
    - Dictionary
---
computer
     n 1: a machine for performing calculations automatically [syn: {computing
          machine}, {computing device}, {data processor}, {electronic
          computer}, {information processing system}]
     2: an expert at calculation (or at operating calculating
        machines) [syn: {calculator}, {reckoner}, {figurer}, {estimator}]
