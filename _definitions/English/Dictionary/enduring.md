---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enduring
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484538001
title: enduring
categories:
    - Dictionary
---
enduring
     adj 1: unceasing; "an abiding belief"; "imperishable truths" [syn:
            {abiding}, {imperishable}]
     2: patiently enduring continual wrongs or trouble; "an enduring
        disposition"; "a long-suffering and uncomplaining wife"
        [syn: {long-suffering}]
