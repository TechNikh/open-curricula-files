---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ability
offline_file: ""
offline_thumbnail: ""
uuid: e7b62a78-a875-412e-946c-a6148f33ec3e
updated: 1484310200
title: ability
categories:
    - Dictionary
---
ability
     n 1: the quality of being able to perform; a quality that permits
          or facilitates achievement or accomplishment [ant: {inability}]
     2: possession of the qualities (especially mental qualities)
        required to do something or get something done; "danger
        heightened his powers of discrimination" [syn: {power}]
        [ant: {inability}]
