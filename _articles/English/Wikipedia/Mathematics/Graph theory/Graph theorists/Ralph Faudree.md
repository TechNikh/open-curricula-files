---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ralph_Faudree
offline_file: ""
offline_thumbnail: ""
uuid: af19f251-27b8-477d-9bd2-b3033c6d1acf
updated: 1484309461
title: Ralph Faudree
categories:
    - Graph theorists
---
Ralph Jasper Faudree (August 23, 1939 – January 13, 2015) was a mathematician, a professor of mathematics and the former provost of the University of Memphis.[1][2]
