---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arms
offline_file: ""
offline_thumbnail: ""
uuid: c2486807-15e5-4c37-80e2-d52d4ea54786
updated: 1484310480
title: arms
categories:
    - Dictionary
---
arms
     n 1: weapons considered collectively [syn: {weaponry}, {implements
          of war}, {weapons system}, {munition}]
     2: the official symbols of a family, state, etc. [syn: {coat of
        arms}, {blazon}, {blazonry}]
