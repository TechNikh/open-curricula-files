---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phosphorus
offline_file: ""
offline_thumbnail: ""
uuid: bfb0b95c-7701-4adc-97a7-11c6db25d3fc
updated: 1484310416
title: phosphorus
categories:
    - Dictionary
---
phosphorus
     n 1: a multivalent nonmetallic element of the nitrogen family
          that occurs commonly in inorganic phosphate rocks and as
          organic phosphates in all living cells; is highly
          reactive and occurs in several allotropic forms [syn: {P},
           {atomic number 15}]
     2: a planet (usually Venus) seen just before sunrise in the
        eastern sky [syn: {morning star}, {daystar}]
