---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euler_calculus
offline_file: ""
offline_thumbnail: ""
uuid: 13298a11-9eff-4762-a523-0ffe6c893240
updated: 1484309257
title: Euler calculus
tags:
    - Euler integration for constructible functions
categories:
    - Integral geometry
---
Euler calculus is a methodology from applied algebraic topology and integral geometry that integrates constructible functions and more recently definable functions[1] by integrating with respect to the Euler characteristic as a finitely-additive measure. In the presence of a metric, it can be extended to continuous integrands via the Gauss–Bonnet theorem.[2] It was introduced independently by Pierre Schapira[3][4][5] and Oleg Viro[6] in 1988, and is useful for enumeration problems in computational geometry and sensor networks.[7]
