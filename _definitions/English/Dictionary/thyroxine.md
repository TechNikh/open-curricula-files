---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thyroxine
offline_file: ""
offline_thumbnail: ""
uuid: 8e66e8c4-a1af-455f-b1a2-a18a06634041
updated: 1484310158
title: thyroxine
categories:
    - Dictionary
---
thyroxine
     n : hormone produced by the thyroid glands to regulate
         metabolism by controlling the rate of oxidation in cells;
         "thyroxine is 65% iodine" [syn: {thyroxin}, {tetraiodothyronine},
          {T}]
