---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiscale_decision-making
offline_file: ""
offline_thumbnail: ""
uuid: 4edb1004-f4fb-4f17-b122-c3327def6b3f
updated: 1484309046
title: Multiscale decision-making
categories:
    - Mechanism design
---
Multiscale decision-making, also referred to as multiscale decision theory (MSDT), is an approach in operations research that combines game theory, multi-agent influence diagrams, in particular dependency graphs, and Markov decision processes to solve multiscale challenges[1] in sociotechnical systems. MSDT considers interdependencies within and between the following scales: system level, time and information.
