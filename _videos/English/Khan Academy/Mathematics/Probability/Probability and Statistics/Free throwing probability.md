---
version: 1
type: video
provider: YouTube
id: iMqCSgqzmiQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Free%20throwing%20probability.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 246479f4-4dc3-44ab-a1e7-00996edb5160
updated: 1486069688
title: Free throwing probability
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/iMqCSgqzmiQ/default.jpg
    - https://i3.ytimg.com/vi/iMqCSgqzmiQ/1.jpg
    - https://i3.ytimg.com/vi/iMqCSgqzmiQ/2.jpg
    - https://i3.ytimg.com/vi/iMqCSgqzmiQ/3.jpg
---
