---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/malayalam
offline_file: ""
offline_thumbnail: ""
uuid: 1dfc7f2f-6af6-40d5-99e8-e65bb867aa1d
updated: 1484310603
title: malayalam
categories:
    - Dictionary
---
Malayalam
     n : a Dravidian language (closely related to Tamil) that is
         spoken in southwestern India
