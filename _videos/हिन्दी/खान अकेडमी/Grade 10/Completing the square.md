---
version: 1
type: video
provider: YouTube
id: lTOITt8g1sU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e2150195-23ee-45d4-a786-7a5a20eb4de0
updated: 1486069621
title: Completing the square
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/lTOITt8g1sU/default.jpg
    - https://i3.ytimg.com/vi/lTOITt8g1sU/1.jpg
    - https://i3.ytimg.com/vi/lTOITt8g1sU/2.jpg
    - https://i3.ytimg.com/vi/lTOITt8g1sU/3.jpg
---
