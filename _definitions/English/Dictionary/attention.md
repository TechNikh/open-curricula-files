---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attention
offline_file: ""
offline_thumbnail: ""
uuid: 40e73fcd-a479-4a53-9fef-16cfe461e4bb
updated: 1484310447
title: attention
categories:
    - Dictionary
---
attention
     n 1: the process whereby a person concentrates on some features
          of the environment to the (relative) exclusion of others
          [syn: {attending}] [ant: {inattention}]
     2: the work of caring for or attending to someone or something;
        "no medical care was required"; "the old car needed
        constant attention" [syn: {care}, {aid}, {tending}]
     3: a general interest that leads people to want to know more;
        "She was the center of attention"
     4: a courteous act indicating affection; "she tried to win his
        heart with her many attentions"
     5: the faculty or power of mental concentration; "keeping track
        of all the details ...
