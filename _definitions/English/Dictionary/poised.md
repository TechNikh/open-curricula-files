---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poised
offline_file: ""
offline_thumbnail: ""
uuid: 4671504a-fe36-4a62-a5d6-8454f122c7d8
updated: 1484310484
title: poised
categories:
    - Dictionary
---
poised
     adj 1: marked by balance or equilibrium and readiness for action;
            "a gull in poised flight"; "George's poised hammer"
     2: in full control of your faculties; "the witness remained
        collected throughout the cross-examination"; "perfectly
        poised and sure of himself"; "more self-contained and more
        dependable than many of the early frontiersmen"; "strong
        and self-possessed in the face of trouble" [syn: {collected},
         {equanimous}, {self-collected}, {self-contained}, {self-possessed}]
