---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concentric
offline_file: ""
offline_thumbnail: ""
uuid: 2e3580a5-3ae2-4ebf-aac9-f745d610731a
updated: 1484310154
title: concentric
categories:
    - Dictionary
---
concentric
     adj : having a common center; "concentric rings" [syn: {concentrical},
            {homocentric}] [ant: {eccentric}]
