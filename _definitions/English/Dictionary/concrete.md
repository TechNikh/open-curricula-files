---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concrete
offline_file: ""
offline_thumbnail: ""
uuid: ed2f836b-c27b-40f4-8f93-8bc0ca846fea
updated: 1484310160
title: concrete
categories:
    - Dictionary
---
concrete
     adj 1: capable of being perceived by the senses; not abstract or
            imaginary; "concrete objects such as trees" [ant: {abstract}]
     2: formed by the coalescence of particles
     n : a strong hard building material composed of sand and gravel
         and cement and water
     v 1: cover with cement; "concrete the walls"
     2: form into a solid mass; coalesce
