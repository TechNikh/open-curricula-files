---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Asymptote
offline_file: ""
offline_thumbnail: ""
uuid: ed0eb607-f0ae-4410-8969-95622ce71d64
updated: 1484309160
title: Asymptote
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/230px-Asymptotic_curve_hvo1.svg.png
tags:
    - Introduction
    - Asymptotes of functions
    - Vertical asymptotes
    - Horizontal asymptotes
    - Oblique asymptotes
    - Elementary methods for identifying asymptotes
    - General computation of oblique asymptotes for functions
    - Asymptotes for rational functions
    - Oblique asymptotes of rational functions
    - Transformations of known functions
    - General definition
    - Curvilinear asymptotes
    - Asymptotes and curve sketching
    - Algebraic curves
    - Asymptotic cone
categories:
    - Analytic geometry
---
In analytic geometry, an asymptote (/ˈæsɪmptoʊt/) of a curve is a line such that the distance between the curve and the line approaches zero as they tend to infinity. Some sources include the requirement that the curve may not cross the line infinitely often, but this is unusual for modern authors.[1] In some contexts, such as algebraic geometry, an asymptote is defined as a line which is tangent to a curve at infinity.[2][3]
