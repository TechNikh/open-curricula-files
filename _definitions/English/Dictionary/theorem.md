---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/theorem
offline_file: ""
offline_thumbnail: ""
uuid: 4cac4d06-f4c7-4853-9ba7-abf8c38084eb
updated: 1484310142
title: theorem
categories:
    - Dictionary
---
theorem
     n 1: a proposition deducible from basic postulates
     2: an idea accepted as a demonstrable truth
