---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decomposed
offline_file: ""
offline_thumbnail: ""
uuid: 39e13d3b-1e5d-43b2-89e6-635b32a4f83b
updated: 1484310281
title: decomposed
categories:
    - Dictionary
---
decomposed
     adj : broken down or disintegrated by rot; "a badly decomposed
           body"
