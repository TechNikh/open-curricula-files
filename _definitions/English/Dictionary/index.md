---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/index
offline_file: ""
offline_thumbnail: ""
uuid: 71da567d-1453-4b65-8df2-801b9518e394
updated: 1484310212
title: index
categories:
    - Dictionary
---
index
     n 1: a numerical scale used to compare variables with one another
          or with some reference number
     2: a number or ratio (a value on a scale of measurement)
        derived from a series of observed facts; can reveal
        relative changes as a function of time [syn: {index number},
         {indicant}, {indicator}]
     3: a mathematical notation indicating the number of times a
        quantity is multiplied by itself [syn: {exponent}, {power}]
     4: an alphabetical listing of names and topics along with page
        numbers where they are discussed
     5: the finger next to the thumb [syn: {index finger}, {forefinger}]
     v 1: list in an index
     2: provide ...
