---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flyer
offline_file: ""
offline_thumbnail: ""
uuid: 9f96e183-3197-4bda-b2a7-e0627d9b8291
updated: 1484310289
title: flyer
categories:
    - Dictionary
---
flyer
     n 1: an advertisement (usually printed on a page or in a leaflet)
          intended for wide distribution; "he mailed the circular
          to all subscribers" [syn: {circular}, {handbill}, {bill},
           {broadside}, {broadsheet}, {flier}, {throwaway}]
     2: someone who travels by air [syn: {flier}]
     3: someone who operates an aircraft [syn: {aviator}, {aeronaut},
         {airman}, {flier}]
