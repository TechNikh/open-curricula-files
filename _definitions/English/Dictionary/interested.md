---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interested
offline_file: ""
offline_thumbnail: ""
uuid: c65ab455-0e4a-4a20-b22b-7d5a436f64ff
updated: 1484310273
title: interested
categories:
    - Dictionary
---
interested
     adj 1: having or showing interest; especially curiosity or
            fascination or concern; "an interested audience";
            "interested in sports"; "was interested to hear about
            her family"; "interested in knowing who was on the
            telephone"; "interested spectators" [ant: {uninterested}]
     2: involved in or affected by or having a claim to or share in;
        "a memorandum to those concerned"; "an enterprise in which
        three men are concerned"; "factors concerned in the rise
        and fall of epidemics"; "the interested parties met to
        discuss the business" [syn: {concerned}]
