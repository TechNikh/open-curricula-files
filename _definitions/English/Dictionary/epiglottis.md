---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/epiglottis
offline_file: ""
offline_thumbnail: ""
uuid: cb1186f5-60cc-4477-88bf-84548dc712bb
updated: 1484310330
title: epiglottis
categories:
    - Dictionary
---
epiglottis
     n : a flap of cartilage that covers the windpipe while
         swallowing
     [also: {epiglottides} (pl)]
