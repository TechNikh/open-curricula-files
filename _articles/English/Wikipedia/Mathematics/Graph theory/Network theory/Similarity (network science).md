---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Similarity_(network_science)
offline_file: ""
offline_thumbnail: ""
uuid: 50977413-e574-4d77-8040-b84edd4a46c2
updated: 1484309443
title: Similarity (network science)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Structural_Equivalence.jpg
tags:
    - Visualizing similarity and distance
    - Clustering tools
    - Multi-dimensional scaling tools
    - Structural equivalence
    - Measures for structural equivalence
    - Cosine similarity
    - Pearson coefficient
    - Euclidean distance
    - Automorphic equivalence
    - Regular equivalence
categories:
    - Network theory
---
There are three fundamental approaches to constructing measures of network similarity: structural equivalence, automorphic equivalence, and regular equivalence.[1] There is a hierarchy of the three equivalence concepts: any set of structural equivalences are also automorphic and regular equivalences. Any set of automorphic equivalences are also regular equivalences. Not all regular equivalences are necessarily automorphic or structural; and not all automorphic equivalences are necessarily structural.[2]
