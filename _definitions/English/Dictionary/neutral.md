---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neutral
offline_file: ""
offline_thumbnail: ""
uuid: ba686165-9096-42ce-a881-95567bca62b2
updated: 1484310379
title: neutral
categories:
    - Dictionary
---
neutral
     adj 1: neither moral nor immoral; neither good nor evil, right nor
            wrong
     2: having no personal preference; "impersonal criticism"; "a
        neutral observer" [syn: {impersonal}]
     3: having only a limited ability to react chemically; not
        active; "inert matter"; "an indifferent chemical in a
        reaction" [syn: {inert}, {indifferent}]
     4: not supporting or favoring either side in a war, dispute, or
        contest
     5: having no net electric charge; not electrified [syn: {electroneutral}]
        [ant: {positive}, {negative}]
     6: lacking hue; "neutral colors like back or white"
     7: of no distinctive quality or characteristics or ...
