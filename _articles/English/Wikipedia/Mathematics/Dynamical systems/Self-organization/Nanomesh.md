---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nanomesh
offline_file: ""
offline_thumbnail: ""
uuid: d5795ab3-1fa5-4ba4-845b-5c0a3d9ea7c4
updated: 1484308819
title: Nanomesh
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nanomesh_3D.JPG
tags:
    - Structure
    - Properties
    - Preparation and analysis
    - Other forms
    - References and notes
    - Other links
categories:
    - Self-organization
---
The nanomesh is a new inorganic nanostructured two-dimensional material, similar to graphene. It was discovered in 2003 at the University of Zurich, Switzerland .[1]
