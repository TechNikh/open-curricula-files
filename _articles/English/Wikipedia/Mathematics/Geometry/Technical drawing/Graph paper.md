---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_paper
offline_file: ""
offline_thumbnail: ""
uuid: 14a6433c-7a58-4fa2-9956-8b08ed06ec9e
updated: 1484309325
title: Graph paper
tags:
    - Formats
    - Examples
categories:
    - Technical drawing
---
Graph paper, graphing paper, or millimeter paper is writing paper that is printed with fine lines making up a regular grid. The lines are often used as guides for plotting mathematical functions or experimental data and drawing two-dimensional graphs. It is commonly found in mathematics and engineering education settings and in laboratory notebooks. Graph paper is available either as loose leaf paper or bound in notebooks. It is becoming less common as computer software, such as spreadsheets and plotting software, has supplanted many of the former uses of graph paper.[citation needed] Some users of graph paper now print pdf images of the grid pattern as needed rather than buying it ...
