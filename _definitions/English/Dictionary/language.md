---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/language
offline_file: ""
offline_thumbnail: ""
uuid: d5a5d093-46d3-4609-a238-f7c02b415cfc
updated: 1484310471
title: language
categories:
    - Dictionary
---
language
     n 1: a systematic means of communicating by the use of sounds or
          conventional symbols; "he taught foreign languages";
          "the language introduced is standard throughout the
          text"; "the speed with which a program can be executed
          depends on the language in which it is written" [syn: {linguistic
          communication}]
     2: (language) communication by word of mouth; "his speech was
        garbled"; "he uttered harsh language"; "he recorded the
        spoken language of the streets" [syn: {speech}, {speech
        communication}, {spoken communication}, {spoken language},
         {voice communication}, {oral communication}]
     3: a ...
