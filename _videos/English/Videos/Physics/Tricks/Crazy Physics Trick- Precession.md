---
version: 1
type: video
provider: YouTube
id: Hj9BVd6wRLk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fba2ea77-f9c8-4a24-a5fc-7d19df8e3105
updated: 1486069658
title: 'Crazy Physics Trick- Precession'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Hj9BVd6wRLk/default.jpg
    - https://i3.ytimg.com/vi/Hj9BVd6wRLk/1.jpg
    - https://i3.ytimg.com/vi/Hj9BVd6wRLk/2.jpg
    - https://i3.ytimg.com/vi/Hj9BVd6wRLk/3.jpg
---
