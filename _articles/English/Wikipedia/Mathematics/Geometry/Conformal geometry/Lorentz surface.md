---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lorentz_surface
offline_file: ""
offline_thumbnail: ""
uuid: a8139b2d-0f23-46a4-8bbe-eae69bdefd6d
updated: 1484309187
title: Lorentz surface
categories:
    - Conformal geometry
---
In mathematics, a Lorentz surface is a two-dimensional oriented smooth manifold with a conformal equivalence class of Lorentzian metrics. It is the analogue of a Riemann surface in indefinite signature.
