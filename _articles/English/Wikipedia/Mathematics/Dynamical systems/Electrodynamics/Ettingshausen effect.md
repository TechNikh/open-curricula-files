---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ettingshausen_effect
offline_file: ""
offline_thumbnail: ""
uuid: 3cce921d-746f-4f71-afb5-f1bea7fc19f4
updated: 1484308753
title: Ettingshausen effect
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sketch_of_Ettingshausen_effect.svg.png
categories:
    - Electrodynamics
---
The Ettingshausen Effect (named for Albert von Ettingshausen) is a thermoelectric (or thermomagnetic) phenomenon that affects the electric current in a conductor when a magnetic field is present.[1]
