---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sundry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484564701
title: sundry
categories:
    - Dictionary
---
sundry
     adj : consisting of a haphazard assortment of different kinds
           (even to the point of incongruity); "an arrangement of
           assorted spring flowers"; "assorted sizes";
           "miscellaneous accessories"; "a mixed program of
           baroque and contemporary music"; "a motley crew";
           "sundry sciences commonly known as social"-
           I.A.Richards [syn: {assorted}, {miscellaneous}, {mixed},
            {motley}, {sundry(a)}]
     n : miscellaneous unspecified objects; "the trunk was full of
         stuff" [syn: {whatchamacallit}, {stuff}, {whatsis}, {sundries}]
