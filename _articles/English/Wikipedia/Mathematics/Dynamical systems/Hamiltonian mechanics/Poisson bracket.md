---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Poisson_bracket
offline_file: ""
offline_thumbnail: ""
uuid: 52ac8847-b868-4f20-a220-c69157e24fbd
updated: 1484308794
title: Poisson bracket
tags:
    - Properties
    - Definition in canonical coordinates
    - "Hamilton's equations of motion"
    - Constants of motion
    - The Poisson bracket in coordinate-free language
    - A result on conjugate momenta
    - Quantization
    - Notes
categories:
    - Hamiltonian mechanics
---
In mathematics and classical mechanics, the Poisson bracket is an important binary operation in Hamiltonian mechanics, playing a central role in Hamilton's equations of motion, which govern the time evolution of a Hamiltonian dynamical system. The Poisson bracket also distinguishes a certain class of coordinate transformations, called canonical transformations, which map canonical coordinate systems into canonical coordinate systems. A "canonical coordinate system" consists of canonical position and momentum variables (below symbolized by qi and pi, respectively) that satisfy canonical Poisson bracket relations. The set of possible canonical transformations is always very rich. For ...
