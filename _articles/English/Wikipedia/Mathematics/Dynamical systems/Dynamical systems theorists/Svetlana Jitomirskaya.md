---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Svetlana_Jitomirskaya
offline_file: ""
offline_thumbnail: ""
uuid: ee04bef3-616d-4447-9489-c531b9a101ae
updated: 1484308684
title: Svetlana Jitomirskaya
tags:
    - Honours
    - Selected publications
categories:
    - Dynamical systems theorists
---
Jitomirskaya was born in Kharkiv, Ukraine. Both her mother, Valentina Borok, and her father Yakov Zhitomirskii were professors of mathematics.[1]
