---
version: 1
type: video
provider: YouTube
id: 2ouZsj9Oll0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d23ff909-5c9d-41a8-8780-7d77cf4765e3
updated: 1486069625
title: ThreeJobOffers
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/2ouZsj9Oll0/default.jpg
    - https://i3.ytimg.com/vi/2ouZsj9Oll0/1.jpg
    - https://i3.ytimg.com/vi/2ouZsj9Oll0/2.jpg
    - https://i3.ytimg.com/vi/2ouZsj9Oll0/3.jpg
---
