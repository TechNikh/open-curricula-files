---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sectional
offline_file: ""
offline_thumbnail: ""
uuid: 97fc5078-307e-4ea2-bef2-ab0872dc1701
updated: 1484310204
title: sectional
categories:
    - Dictionary
---
sectional
     adj 1: relating to or based upon a section (i.e. as if cut through
            by an intersecting plane); "a sectional view";
            "sectional drawings"
     2: consisting of or divided into sections; "a sectional sofa";
        "sectioned plates" [syn: {sectioned}]
