---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sutlej
offline_file: ""
offline_thumbnail: ""
uuid: 50be2b4e-e66c-4a71-be21-bc174ece2b82
updated: 1484308419
title: Sutlej
tags:
    - History
    - Sources
    - Geology
    - Sutlej-Yamuna Link
    - Gallery
categories:
    - Uncategorized
---
The Sutlej River (alternatively spelled as Satluj River) (Hindi: सतलुज, Punjabi: ਸਤਲੁਜ, Sanskrit : शतद्रु (shatadru) Urdu: درياۓ ستلُج‎ ) is the longest of the five rivers that flow through the historic crossroads region of Punjab in northern India and Pakistan. The Sutlej River is also known as Satadree.[2] It is the easternmost tributary of the Indus River.
