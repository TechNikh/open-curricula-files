---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/receding
offline_file: ""
offline_thumbnail: ""
uuid: 0e9866a7-daba-40da-8387-64712a603671
updated: 1484310148
title: receding
categories:
    - Dictionary
---
receding
     adj 1: moving toward a position farther from the front; "the
            receding glaciers of the last ice age"; "retiring fogs
            revealed the rocky coastline" [syn: {retiring}]
     2: (of a hairline e.g.) moving slowly back [syn: {receding(a)}]
     n 1: a slow or gradual disappearance [syn: {fadeout}]
     2: the act of becoming more distant [syn: {recession}]
