---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/narrowing
offline_file: ""
offline_thumbnail: ""
uuid: 5320f578-51e0-424b-900d-7302f8236ff8
updated: 1484310333
title: narrowing
categories:
    - Dictionary
---
narrowing
     adj 1: becoming gradually narrower; "long tapering fingers";
            "trousers with tapered legs" [syn: {tapered}, {tapering}]
     2: (of circumstances) tending to constrict freedom [syn: {constricting},
         {constrictive}]
     n 1: an instance of becoming narrow
     2: a decrease in width [ant: {widening}]
     3: the act of making something narrower [ant: {widening}]
