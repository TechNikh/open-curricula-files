---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nearest
offline_file: ""
offline_thumbnail: ""
uuid: 68856192-7867-4cb6-816f-882771b00c27
updated: 1484310234
title: nearest
categories:
    - Dictionary
---
nearest
     adj : (superlative of `near' or `nigh') most near [syn: {nighest}]
     adv : (superlative of `near' or `close') within the shortest
           distance; "that was the time he came nearest to death"
           [syn: {nighest}, {closest}]
