---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shrine
offline_file: ""
offline_thumbnail: ""
uuid: 503f028a-74d6-4bcb-8f04-17f7cea7b9e2
updated: 1484310188
title: shrine
categories:
    - Dictionary
---
shrine
     n : a place of worship hallowed by association with some sacred
         thing or person
     v : enclose in a shrine; "the saint's bones were enshrined in
         the cathedral" [syn: {enshrine}]
