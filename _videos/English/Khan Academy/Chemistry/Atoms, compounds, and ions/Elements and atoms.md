---
version: 1
type: video
provider: YouTube
id: IFKnq9QM6_A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Atoms%2C%20compounds%2C%20and%20ions/Elements%20and%20atoms.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3067c7b0-b125-4b89-accb-cb116f099aed
updated: 1486069583
title: Elements and atoms
tags:
    - dotsub
categories:
    - Atoms, compounds, and ions
thumbnail_urls:
    - https://i3.ytimg.com/vi/IFKnq9QM6_A/default.jpg
    - https://i3.ytimg.com/vi/IFKnq9QM6_A/1.jpg
    - https://i3.ytimg.com/vi/IFKnq9QM6_A/2.jpg
    - https://i3.ytimg.com/vi/IFKnq9QM6_A/3.jpg
---
How elements relate to atoms. The basics of how protons, electrons and neutrons make up an atom.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/atomic-structure-and-properties/introduction-to-the-atom/v/atomic-number-mass-number-and-isotopes?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
