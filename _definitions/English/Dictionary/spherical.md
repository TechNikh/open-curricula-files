---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spherical
offline_file: ""
offline_thumbnail: ""
uuid: 11b86db4-0e31-4d5f-891d-ffa53f86edde
updated: 1484310220
title: spherical
categories:
    - Dictionary
---
spherical
     adj 1: of or relating to spheres or resembling a sphere; "spherical
            geometry" [ant: {nonspherical}]
     2: having the shape of a sphere or ball; "a spherical object";
        "nearly orbicular in shape"; "little globular houses like
        mud-wasp nests"- Zane Grey [syn: {ball-shaped}, {global},
        {globose}, {globular}, {orbicular}, {spheric}]
