---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humbly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484651641
title: humbly
categories:
    - Dictionary
---
humbly
     adv 1: in a humble manner; "he humbly lowered his head" [syn: {meekly}]
     2: in a miserly manner; "they lived meanly and without
        ostentation" [syn: {meanly}]
