---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kannada
offline_file: ""
offline_thumbnail: ""
uuid: a94ea486-3034-4452-a6c5-52aa792cadc2
updated: 1484308413
title: Kannada
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/80px-%25D0%259A%25D0%25B0%25D0%25BD%25D0%25BD%25D0%25B0%25D0%25B4%25D0%25B0.PNG'
tags:
    - Development
    - Influence of Sanskrit and Prakrit
    - History
    - Early traces
    - Epigraphy
    - Coins
    - Literature
    - Old Kannada
    - Middle Kannada
    - Modern Kannada
    - Areas of influence
    - Dialects
    - Status
    - Writing system
    - Obsolete Kannada letters
    - Kannada script evolution
    - dictionary
    - Kannada script in computing
    - Transliteration
    - Unicode
    - grammar
    - Compound bases
    - pronouns
    - Notes
categories:
    - Uncategorized
---
Kannada /ˈkɑːnədə, ˈkæ-/[7][8] (ಕನ್ನಡ kannaḍa; IPA: [ˈkʌnːəɖɑː]), also known as Canarese or Kanarese /kænəˈriːz/,[9] is a Dravidian language spoken predominantly by Kannada people in India, mainly in the state of Karnataka, and by linguistic minorities in the states of Andhra Pradesh, Telangana, Tamil Nadu, Maharashtra, Kerala, and Goa. The language has roughly 40 million native speakers[10] who are called Kannadigas (Kannaḍigaru), and a total of 50.8 million speakers according to a 2001 census.[1] It is one of the scheduled languages of India and the official and administrative language of the state of Karnataka.[11]
