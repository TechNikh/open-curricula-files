---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breast
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484578501
title: breast
categories:
    - Dictionary
---
breast
     n 1: the front part of the trunk from the neck to the abdomen;
          "he beat his breast in anger"
     2: either of two soft fleshy milk-secreting glandular organs on
        the chest of a woman [syn: {bosom}, {knocker}, {boob}, {tit},
         {titty}]
     3: meat carved from the breast of a fowl [syn: {white meat}]
     v 1: meet at breast level; "The runner breasted the tape"
     2: reach the summit; "They breasted the mountain"
     3: confront bodily; "breast the storm" [syn: {front}]
