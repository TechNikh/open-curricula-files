---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equivariant_index_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 666dacde-d799-4980-86a1-c1aa381a4d6b
updated: 1484309216
title: Equivariant index theorem
categories:
    - Differential geometry
---
In differential geometry, the equivariant index theorem, of which there are several variants, computes the (graded) trace of an element of a compact Lie group acting in given setting in terms of the integral over the fixed points of the element. If the element is neutral, then the theorem reduces to the usual index theorem.
