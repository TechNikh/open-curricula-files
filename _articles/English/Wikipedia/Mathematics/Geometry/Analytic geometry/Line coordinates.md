---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Line_coordinates
offline_file: ""
offline_thumbnail: ""
uuid: 62870193-3710-47f4-ab65-00ec83c3cfc0
updated: 1484309165
title: Line coordinates
tags:
    - Lines in the plane
    - Tangential equations
    - Tangential equation of a point
    - Formulas
    - Lines in three-dimensional space
    - With complex numbers
categories:
    - Analytic geometry
---
In geometry, line coordinates are used to specify the position of a line just as point coordinates (or simply coordinates) are used to specify the position of a point.
