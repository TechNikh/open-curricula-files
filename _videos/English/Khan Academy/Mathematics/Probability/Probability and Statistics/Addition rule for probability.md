---
version: 1
type: video
provider: YouTube
id: QE2uR6Z-NcU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Addition%20rule%20for%20probability.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 322b5f99-ed82-46fb-b983-ed86bc91f22b
updated: 1486069689
title: Addition rule for probability
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/QE2uR6Z-NcU/default.jpg
    - https://i3.ytimg.com/vi/QE2uR6Z-NcU/1.jpg
    - https://i3.ytimg.com/vi/QE2uR6Z-NcU/2.jpg
    - https://i3.ytimg.com/vi/QE2uR6Z-NcU/3.jpg
---
