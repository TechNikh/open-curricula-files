---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vii
offline_file: ""
offline_thumbnail: ""
uuid: 3196e40d-8a32-44b1-8c06-2cf022d3888a
updated: 1484310319
title: vii
categories:
    - Dictionary
---
vii
     adj : being one more than six [syn: {seven}, {7}]
     n : the cardinal number that is the sum of six and one [syn: {seven},
          {7}, {sevener}, {heptad}, {septet}]
