---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/void
offline_file: ""
offline_thumbnail: ""
uuid: a3c78462-5877-4399-b57e-e58ce985ab20
updated: 1484310138
title: void
categories:
    - Dictionary
---
void
     adj 1: lacking any legal or binding force; "null and void" [syn: {null}]
     2: containing nothing; "the earth was without form, and void"
     n 1: the state of nonexistence [syn: {nothingness}, {nullity}]
     2: an empty area or space; "the huge desert voids"; "the
        emptiness of outer space"; "without their support he'll be
        ruling in a vacuum" [syn: {vacancy}, {emptiness}, {vacuum}]
     v 1: declare invalid; "The contract was annulled"; "void a plea"
          [syn: {invalidate}, {annul}, {quash}, {avoid}, {nullify}]
          [ant: {validate}]
     2: clear (a room, house, place) of occupants or empty or clear
        (a place, receptacle, etc.) of something; ...
