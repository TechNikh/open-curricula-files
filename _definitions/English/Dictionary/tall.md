---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tall
offline_file: ""
offline_thumbnail: ""
uuid: 8ab19c0c-384e-44b6-a5fe-64808514d9e4
updated: 1484310305
title: tall
categories:
    - Dictionary
---
tall
     adj 1: great in vertical dimension; high in stature; "tall people";
            "tall buildings"; "tall trees"; "tall ships" [ant: {short}]
     2: lofty in style; "he engages in so much tall talk, one never
        really realizes what he is saying" [syn: {grandiloquent},
        {magniloquent}]
     3: impressively difficult; "a tall order" [syn: {tall(a)}]
     4: too improbable to admit of belief; "a tall story" [syn: {improbable},
         {marvelous}, {marvellous}, {tall(a)}]
