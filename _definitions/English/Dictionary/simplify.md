---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/simplify
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484315881
title: simplify
categories:
    - Dictionary
---
simplify
     v : make simpler or easier or reduce in complxity or extent; "We
         had to simplify the instructions"; "this move will
         simplify our lives" [ant: {complicate}]
     [also: {simplified}]
