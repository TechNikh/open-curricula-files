---
version: 1
type: video
provider: YouTube
id: 7mxV6f5nuJY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 904f2dd3-e262-4c75-9dfb-54f039332483
updated: 1486069652
title: Moment of Inertia and Rolling Down a Ramp
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/7mxV6f5nuJY/default.jpg
    - https://i3.ytimg.com/vi/7mxV6f5nuJY/1.jpg
    - https://i3.ytimg.com/vi/7mxV6f5nuJY/2.jpg
    - https://i3.ytimg.com/vi/7mxV6f5nuJY/3.jpg
---
