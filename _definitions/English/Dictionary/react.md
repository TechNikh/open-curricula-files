---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/react
offline_file: ""
offline_thumbnail: ""
uuid: 77ed57de-4792-4c24-a039-82917987ed05
updated: 1484310375
title: react
categories:
    - Dictionary
---
react
     v 1: show a response or a reaction to something [syn: {respond}]
     2: undergo a chemical reaction; react with another substance
        under certain conditions; "The hydrogen and the oxygen
        react"
     3: act against or in opposition to; "She reacts negatively to
        everything I say" [syn: {oppose}]
