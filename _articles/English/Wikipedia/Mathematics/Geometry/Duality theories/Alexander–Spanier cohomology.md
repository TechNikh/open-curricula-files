---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Alexander%E2%80%93Spanier_cohomology'
offline_file: ""
offline_thumbnail: ""
uuid: 9bbcdb51-fcd7-47f2-8f92-1a8f060026ce
updated: 1484309231
title: Alexander–Spanier cohomology
tags:
    - History
    - Definition
    - Variants
    - Connection to other cohomologies
categories:
    - Duality theories
---
