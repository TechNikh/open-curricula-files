---
version: 1
type: video
provider: YouTube
id: ifbCsha7Syc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a4fd9de0-cebe-46a0-8f04-0876dbee3a47
updated: 1486069637
title: >
    Bending of light | Laser bending demonstration | Science
    Experiment video
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/ifbCsha7Syc/default.jpg
    - https://i3.ytimg.com/vi/ifbCsha7Syc/1.jpg
    - https://i3.ytimg.com/vi/ifbCsha7Syc/2.jpg
    - https://i3.ytimg.com/vi/ifbCsha7Syc/3.jpg
---
