---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/embroidery
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484319001
title: embroidery
categories:
    - Dictionary
---
embroidery
     n 1: elaboration of an interpretation by the use of decorative
          (sometimes fictitious) detail; "the mystery has been
          heightened by many embellishments in subsequent
          retellings" [syn: {embellishment}]
     2: decorative needlework [syn: {fancywork}]
