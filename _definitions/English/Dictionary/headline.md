---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/headline
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484589481
title: headline
categories:
    - Dictionary
---
headline
     n : the heading or caption of a newspaper article [syn: {newspaper
         headline}]
     v 1: publicize widely or highly, as if with a headline
     2: provide (a newspaper page or a story) with a headline
