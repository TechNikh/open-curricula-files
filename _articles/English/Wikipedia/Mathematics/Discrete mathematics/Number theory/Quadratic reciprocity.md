---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quadratic_reciprocity
offline_file: ""
offline_thumbnail: ""
uuid: 822a7447-8c5a-46fe-8f40-4ad11527f459
updated: 1484308661
title: Quadratic reciprocity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Disquisitiones-Arithmeticae-p133.jpg
tags:
    - Motivating example
    - Terminology, data, and two statements of the theorem
    - Table of quadratic residues
    - ±1 and the first supplement
    - ±2 and the second supplement
    - ±3
    - ±5
    - "Gauss's version"
    - Table of quadratic character of primes
    - "Legendre's version"
    - Connection with cyclotomy
    - History and alternative statements
    - Fermat
    - Euler
    - Legendre and his symbol
    - "Legendre's version of quadratic reciprocity"
    - The supplementary laws using Legendre symbols
    - Gauss
    - Other statements
    - Jacobi symbol
    - Hilbert symbol
    - Other rings
    - Gaussian integers
    - Eisenstein integers
    - Imaginary quadratic fields
    - Polynomials over a finite field
    - Higher powers
    - Notes
categories:
    - Number theory
---
In number theory, the law of quadratic reciprocity is a theorem about modular arithmetic that gives conditions for the solvability of quadratic equations modulo prime numbers. There are a number of equivalent statements of the theorem. One version of the law states that for p and q odd prime numbers,
