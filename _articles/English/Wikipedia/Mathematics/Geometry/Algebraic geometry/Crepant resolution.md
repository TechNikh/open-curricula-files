---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crepant_resolution
offline_file: ""
offline_thumbnail: ""
uuid: 75da20fc-33d9-4bf4-8018-6df005b30b83
updated: 1484309138
title: Crepant resolution
categories:
    - Algebraic geometry
---
In algebraic geometry, a crepant resolution of a singularity is a resolution that does not affect the canonical class of the manifold. The term "crepant" was coined by Miles Reid (1983) by removing the prefix "dis" from the word "discrepant", to indicate that the resolutions have no discrepancy in the canonical class.
