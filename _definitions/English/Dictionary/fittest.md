---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fittest
offline_file: ""
offline_thumbnail: ""
uuid: eb8a55a3-2202-44b2-b187-8af7fb7788f8
updated: 1484310286
title: fittest
categories:
    - Dictionary
---
fit
     adj 1: meeting adequate standards for a purpose; "a fit subject for
            discussion"; "it is fit and proper that you be there";
            "water fit to drink"; "fit for duty"; "do as you see
            fit to" [syn: {fit to(a)}, {fit for(a)}] [ant: {unfit}]
     2: (usually followed by `to' or `for') on the point of or
        strongly disposed; "in no fit state to continue"; "fit to
        drop"; "laughing fit to burst"; "she was fit to scream";
        "primed for a fight"; "we are set to go at any time" [syn:
         {fit(p)}, {primed(p)}, {set(p)}]
     3: physically and mentally sound or healthy; "felt relaxed and
        fit after their holiday"; "keeps fit with ...
