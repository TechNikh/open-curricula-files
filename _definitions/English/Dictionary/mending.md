---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mending
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484570701
title: mending
categories:
    - Dictionary
---
mending
     n 1: garments that must be repaired
     2: the act of putting something in working order again [syn: {repair},
         {fix}, {fixing}, {fixture}, {mend}, {reparation}]
