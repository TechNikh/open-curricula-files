---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Miwin%27s_dice'
offline_file: ""
offline_thumbnail: ""
uuid: abd6291f-409f-4eef-b6fd-aa68f2a3c9d2
updated: 1484309046
title: "Miwin's dice"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Miwin_Wuerfel_Titan.gif
tags:
    - Description
    - "First set of Miwin's dice: III, IV, V"
    - "Second set of Miwin's dice: IX, X, XI"
    - "Third and fourth set of Miwin's dice"
    - Mathematical attributes
    - Probabilities
    - Cumulative frequency
    - Reversed intransitivity
    - Equal distribution of random numbers
    - Other distributions
    - 0 – 90 (throw 3 times)
    - 0 – 103 (throw 3 times)
    - 0 – 728 (throw 3 times)
    - "Combinations with Miwin's dice type III, IV, and V"
    - Games
    - Variants 0 – 80
    - 1st variant
    - 2nd variant
    - 3rd variant
    - Notes
    - Published games
categories:
    - Games (probability)
---
Miwin's Dice are a set of nontransitive dice invented in 1975 by the physicist Michael Winkelmann. They consist of three different dice with faces bearing numbers from 1 to 9; opposite faces sum to 9, 10, or 11. The numbers on each die give the sum of 30 and have an arithmetic mean of 5.
