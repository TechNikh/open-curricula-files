---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nutritional
offline_file: ""
offline_thumbnail: ""
uuid: a137fe28-c30e-44ee-8751-04c30e0fb716
updated: 1484310447
title: nutritional
categories:
    - Dictionary
---
nutritional
     adj : of or relating to or providing nutrition; "nutritional
           information" [syn: {nutritionary}]
