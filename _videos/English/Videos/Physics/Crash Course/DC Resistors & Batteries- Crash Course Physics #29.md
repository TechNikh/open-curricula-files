---
version: 1
type: video
provider: YouTube
id: g-wjP1otQWI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 93715e7c-23e9-4a02-9cf2-69d049dd3d2a
updated: 1486069682
title: 'DC Resistors & Batteries- Crash Course Physics #29'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/g-wjP1otQWI/default.jpg
    - https://i3.ytimg.com/vi/g-wjP1otQWI/1.jpg
    - https://i3.ytimg.com/vi/g-wjP1otQWI/2.jpg
    - https://i3.ytimg.com/vi/g-wjP1otQWI/3.jpg
---
