---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lingua
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484527861
title: lingua
categories:
    - Dictionary
---
lingua
     n : a mobile mass of muscular tissue covered with mucous
         membrane and located in the oral cavity [syn: {tongue}, {glossa},
          {clapper}]
     [also: {linguae} (pl)]
