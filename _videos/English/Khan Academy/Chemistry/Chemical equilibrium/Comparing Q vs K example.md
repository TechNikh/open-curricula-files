---
version: 1
type: video
provider: YouTube
id: pnW1PNJlmWg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Chemical%20equilibrium/Comparing%20Q%20vs%20K%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 35998479-7449-4097-9e87-2620a9534a30
updated: 1486069585
title: Comparing Q vs K example
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Chemical equilibrium
thumbnail_urls:
    - https://i3.ytimg.com/vi/pnW1PNJlmWg/default.jpg
    - https://i3.ytimg.com/vi/pnW1PNJlmWg/1.jpg
    - https://i3.ytimg.com/vi/pnW1PNJlmWg/2.jpg
    - https://i3.ytimg.com/vi/pnW1PNJlmWg/3.jpg
---
A worked example using reaction quotient Q to predict how concentrations will shift so the reaction can get to equilibrium. 

Watch the next lesson: https://www.khanacademy.org/science/chemistry/acids-and-bases-topic/acids-and-bases/v/arrhenius-definition-of-acids-and-bases?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/chemical-equilibrium/factors-that-affect-chemical-equilibrium/v/intro-to-reaction-quotient-q?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
