---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mechanical
offline_file: ""
offline_thumbnail: ""
uuid: 3d0f4a9f-8193-4f0e-9cd6-5a3ffefcd207
updated: 1484310343
title: mechanical
categories:
    - Dictionary
---
mechanical
     adj 1: using (or as if using) mechanisms or tools or devices; "a
            mechanical process"; "his smile was very mechanical";
            "a mechanical toy" [ant: {nonmechanical}]
     2: relating to or concerned with machinery or tools;
        "mechanical arts"; "mechanical design"; "mechanical
        skills" [syn: {mechanically skillful}]
     3: relating to or governed by or in accordance with mechanics;
        "a belief that the universe is a mechanical contrivance";
        "the mechanical pressure of a strong wind"
     4: lacking thought or feeling [syn: {mechanistic}]
