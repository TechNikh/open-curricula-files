---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grzegorczyk_hierarchy
offline_file: ""
offline_thumbnail: ""
uuid: 7bd44d5d-5da0-49bd-b115-1f4bc62ae742
updated: 1484309487
title: Grzegorczyk hierarchy
tags:
    - Definition
    - Properties
    - Relation to primitive recursive functions
    - Extensions
    - Notes
categories:
    - Computability theory
---
The Grzegorczyk hierarchy (pronounced: [ɡʐɛˈɡɔrt͡ʂɨk]), named after the Polish logician Andrzej Grzegorczyk, is a hierarchy of functions used in computability theory (Wagner and Wechsung 1986:43). Every function in the Grzegorczyk hierarchy is a primitive recursive function, and every primitive recursive function appears in the hierarchy at some level. The hierarchy deals with the rate at which the values of the functions grow; intuitively, functions in lower level of the hierarchy grow slower than functions in the higher levels.
