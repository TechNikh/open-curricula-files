---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eliminate
offline_file: ""
offline_thumbnail: ""
uuid: a91c7c87-4ad5-4ba0-a11b-5671a00366d5
updated: 1484310555
title: eliminate
categories:
    - Dictionary
---
eliminate
     v 1: terminate or take out; "Let's eliminate the course on
          Akkadian hieroglyphics" [syn: {get rid of}, {do away
          with}]
     2: do away with [syn: {obviate}, {rid of}] [ant: {necessitate}]
     3: kill in large numbers; "the plague wiped out an entire
        population" [syn: {annihilate}, {extinguish}, {eradicate},
         {wipe out}, {decimate}, {carry off}]
     4: dismiss from consideration; "John was ruled out as a
        possible suspect because he had a strong alibi"; "This
        possibility can be eliminated from our consideration"
        [syn: {rule out}, {reject}]
     5: eliminate from the body; "Pass a kidney stone" [syn: {excrete},
       ...
