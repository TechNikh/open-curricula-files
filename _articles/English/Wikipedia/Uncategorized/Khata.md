---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Khada
offline_file: ""
offline_thumbnail: ""
uuid: f604f2d3-3c4e-4060-a4ab-c1f15a457f45
updated: 1484308470
title: Khata
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Khada.JPG
categories:
    - Uncategorized
---
A khata (Tibetan: ཁ་བཏགས་; Dzongkha: དར་, Dhar, Mongolian : ᠬᠠᠳᠠᠭ / Mongolian: хадаг / IPA: [χɑtɑk], khadag or hatag, Nepali: खतक khada, Chinese 哈达; pinyin: hādá) is a traditional ceremonial scarf in tengrism[1] and Tibetan Buddhism. It originated in Tibetan culture[citation needed] and is common in cultures and countries where Tibetan Buddhism is practiced.
