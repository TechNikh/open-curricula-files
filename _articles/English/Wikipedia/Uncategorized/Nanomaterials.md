---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nanomaterials
offline_file: ""
offline_thumbnail: ""
uuid: 7bb092db-572e-45f5-9ad7-3691a8c43864
updated: 1484308413
title: Nanomaterials
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-C60_Buckyball.gif
tags:
    - Types
    - Natural nanomaterials
    - Fullerenes
    - Graphene nanostructures
    - Nanoparticles
    - Nanozymes
    - Synthesis
    - Bottom up methods
    - Chaotic processes
    - Controlled processes
    - Characterization
    - Uniformity
    - Legal definition
    - Safety
    - Health impact
    - Occupational safety
categories:
    - Uncategorized
---
Nanomaterials describe, in principle, materials of which a single unit is sized (in at least one dimension) between 1 and 1000 nanometres (10−9 meter) but is usually 1—100 nm (the usual definition of nanoscale[1]).
