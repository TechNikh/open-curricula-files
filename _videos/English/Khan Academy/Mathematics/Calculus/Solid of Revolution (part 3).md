---
version: 1
type: video
provider: YouTube
id: tqfU9mC2yFU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Solid%20of%20Revolution%20%28part%203%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e8ea5756-88ca-4116-aac1-1c942134bfd2
updated: 1486069694
title: Solid of Revolution (part 3)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/tqfU9mC2yFU/default.jpg
    - https://i3.ytimg.com/vi/tqfU9mC2yFU/1.jpg
    - https://i3.ytimg.com/vi/tqfU9mC2yFU/2.jpg
    - https://i3.ytimg.com/vi/tqfU9mC2yFU/3.jpg
---
