---
version: 1
type: video
provider: YouTube
id: dZnc3PtNaN4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Proof-%20d-dx%28x%5En%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4c308fe8-30bb-45b8-a074-44aa1b76dd0d
updated: 1486069693
title: 'Proof- d/dx(x^n)'
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/dZnc3PtNaN4/default.jpg
    - https://i3.ytimg.com/vi/dZnc3PtNaN4/1.jpg
    - https://i3.ytimg.com/vi/dZnc3PtNaN4/2.jpg
    - https://i3.ytimg.com/vi/dZnc3PtNaN4/3.jpg
---
