---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Game_Description_Language
offline_file: ""
offline_thumbnail: ""
uuid: 03e83292-430d-472d-983b-c3bf51ec1aa7
updated: 1484309004
title: Game Description Language
tags:
    - Purpose of GDL
    - Specification
    - Syntax
    - Keywords
    - Rules
    - Players
    - Initial state
    - Legal moves
    - Game state update
    - Termination
    - Goal states
    - Extension
categories:
    - Game artificial intelligence
---
Game Description Language, or GDL, is a language designed by Michael Genesereth as part of the General Game Playing Project at Stanford University, California. GDL describes the state of a game as a series of facts, and the game mechanics as logical rules.
