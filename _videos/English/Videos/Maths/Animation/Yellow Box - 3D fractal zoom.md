---
version: 1
type: video
provider: YouTube
id: j_dey8YBKjE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a166b007-fdf8-4e18-9daf-b8fc6be3da83
updated: 1486069644
title: 'Yellow Box - 3D fractal zoom'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/j_dey8YBKjE/default.jpg
    - https://i3.ytimg.com/vi/j_dey8YBKjE/1.jpg
    - https://i3.ytimg.com/vi/j_dey8YBKjE/2.jpg
    - https://i3.ytimg.com/vi/j_dey8YBKjE/3.jpg
---
