---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faecal
offline_file: ""
offline_thumbnail: ""
uuid: 8b38a472-8a98-4bbb-91cd-2e5fa01e0e89
updated: 1484310319
title: faecal
categories:
    - Dictionary
---
faecal
     adj : foul with waste matter; of or relating to feces [syn: {fecal},
            {feculent}]
