---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trihydroxy
offline_file: ""
offline_thumbnail: ""
uuid: e7af2d95-37fa-4551-a7d0-131b921c7465
updated: 1484308405
title: Barbituric acid
tags:
    - Synthesis
    - Properties
    - Uses
    - Health and safety
categories:
    - Uncategorized
---
Barbituric acid or malonylurea or 6-hydroxyuracil is an organic compound based on a pyrimidine heterocyclic skeleton. It is an odorless powder soluble in water. Barbituric acid is the parent compound of barbiturate drugs, although barbituric acid itself is not pharmacologically active.
