---
version: 1
type: video
provider: YouTube
id: GEPkEe9Z-RA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/XVIVO%20Scientific%20Animation%20Demo%20Video.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ebaf0038-1cce-4a11-b32b-39b1978059fc
updated: 1486069666
title: XVIVO Scientific Animation Demo Video
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/GEPkEe9Z-RA/default.jpg
    - https://i3.ytimg.com/vi/GEPkEe9Z-RA/1.jpg
    - https://i3.ytimg.com/vi/GEPkEe9Z-RA/2.jpg
    - https://i3.ytimg.com/vi/GEPkEe9Z-RA/3.jpg
---
