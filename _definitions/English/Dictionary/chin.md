---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chin
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484408101
title: chin
categories:
    - Dictionary
---
chin
     n 1: the protruding part of the lower jaw [syn: {mentum}]
     2: Kamarupan languages spoken in western Burma and Bangladesh
        and easternmost India [syn: {Kuki}, {Kuki-Chin}]
     v : in gymnastics: raise oneself while hanging from one's hands
         until one's chin is level with the support bar [syn: {chin
         up}]
     [also: {chinning}, {chinned}]
