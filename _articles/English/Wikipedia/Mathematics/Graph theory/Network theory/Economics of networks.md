---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Economics_of_networks
offline_file: ""
offline_thumbnail: ""
uuid: c1848218-0286-45f9-8d36-c003fffe621f
updated: 1484309435
title: Economics of networks
tags:
    - Models of networked markets
    - Exchange theory
    - Bilateral trading models
    - Informal exchange
    - Scale-free property and economics
    - World trade web
    - Literature
categories:
    - Network theory
---
Economics of networks is an increasing new field on the border of economics and network sciences. It is concerned with understanding of economic phenomena by using network concepts and the tools of network science. Some main author in the field are Matthew O. Jackson and Rachel Kranton.
