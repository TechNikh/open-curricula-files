---
version: 1
type: video
provider: YouTube
id: 3A-HFCVkNOc
offline_file: ""
offline_thumbnail: ""
uuid: 7116fdc6-b100-4c84-bfbc-d6d32ad5e2f2
updated: 1484040352
title: Subtracting three digit numbers without regrouping
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/3A-HFCVkNOc/default.jpg
    - https://i3.ytimg.com/vi/3A-HFCVkNOc/1.jpg
    - https://i3.ytimg.com/vi/3A-HFCVkNOc/2.jpg
    - https://i3.ytimg.com/vi/3A-HFCVkNOc/3.jpg
---
