---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/security
offline_file: ""
offline_thumbnail: ""
uuid: 296d1570-b963-4d7e-a917-4a864d50a4ef
updated: 1484310369
title: security
categories:
    - Dictionary
---
security
     n 1: the state of being free from danger or injury; "we support
          the armed services in the name of national security"
          [ant: {insecurity}]
     2: a formal declaration that documents a fact of relevance to
        finance and investment; the holder has a right to receive
        interest or dividends; "he held several valuable
        securities" [syn: {certificate}]
     3: a department responsible for the security of the
        institution's property and workers; "the head of security
        was a former policeman" [syn: {security department}]
     4: measures taken as a precaution against theft or espionage or
        sabotage etc.; "military security ...
