---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soda
offline_file: ""
offline_thumbnail: ""
uuid: fa7f61d6-b99f-4bf6-b598-2f72e5924cd4
updated: 1484310381
title: soda
categories:
    - Dictionary
---
soda
     n 1: a sodium salt of carbonic acid; used in making soap powders
          and glass and paper [syn: {sodium carbonate}, {washing
          soda}, {sal soda}, {soda ash}]
     2: a sweet drink containing carbonated water and flavoring; "in
        New England they call sodas tonics" [syn: {pop}, {soda pop},
         {soda water}, {tonic}]
