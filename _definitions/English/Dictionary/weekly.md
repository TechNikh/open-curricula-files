---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weekly
offline_file: ""
offline_thumbnail: ""
uuid: 4d2254a1-9331-4f2c-aa5f-87f322eec314
updated: 1484310479
title: weekly
categories:
    - Dictionary
---
weekly
     adj : occurring or payable every week; "a weekly trip to town";
           "weekly wages"; "weekly rent"
     n : a periodical that is published every week
     adv : without missing a week; "she visited her aunt weekly" [syn:
           {hebdomadally}, {every week}, {each week}]
