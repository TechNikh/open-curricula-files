---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ideal_number
offline_file: ""
offline_thumbnail: ""
uuid: d4da7125-2095-4e28-b1bb-7a055806f4cd
updated: 1484308645
title: Ideal number
tags:
    - Example
    - History
categories:
    - Number theory
---
In number theory an ideal number is an algebraic integer which represents an ideal in the ring of integers of a number field; the idea was developed by Ernst Kummer, and led to Richard Dedekind's definition of ideals for rings. An ideal in the ring of integers of an algebraic number field is principal if it consists of multiples of a single element of the ring, and nonprincipal otherwise. By the principal ideal theorem any nonprincipal ideal becomes principal when extended to an ideal of the Hilbert class field. This means that there is an element of the ring of integers of the Hilbert class field, which is an ideal number, such that the original nonprincipal ideal is equal to the ...
