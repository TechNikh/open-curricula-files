---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/race
offline_file: ""
offline_thumbnail: ""
uuid: 371890fc-42b0-417b-a31b-bc5db9b27533
updated: 1484310551
title: race
categories:
    - Dictionary
---
race
     n 1: any competition; "the race for the presidency"
     2: people who are believed to belong to the same genetic stock;
        "some biologists doubt that there are important genetic
        differences between races of human beings"
     3: a contest of speed; "the race is to the swift"
     4: the flow of air that is driven backwards by an aircraft
        propeller [syn: {slipstream}, {airstream}, {backwash}, {wash}]
     5: (biology) a taxonomic group that is a division of a species;
        usually arises as a consequence of geographical isolation
        within a species [syn: {subspecies}]
     6: a canal for a current of water [syn: {raceway}]
     v 1: step on it; "He ...
