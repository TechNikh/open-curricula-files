---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reasonably
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484604301
title: reasonably
categories:
    - Dictionary
---
reasonably
     adv 1: to a moderately sufficient extent or degree; "the shoes are
            priced reasonably"; "he is fairly clever with
            computers"; "they lived comfortably within reason"
            [syn: {moderately}, {within reason}, {somewhat}, {fairly},
             {middling}, {passably}] [ant: {unreasonably}, {unreasonably}]
     2: with good sense or in a reasonable or intelligent manner;
        "he acted sensibly in the crisis"; "speak more sanely
        about these affairs"; "acted quite reasonably" [syn: {sanely},
         {sensibly}] [ant: {unreasonably}]
