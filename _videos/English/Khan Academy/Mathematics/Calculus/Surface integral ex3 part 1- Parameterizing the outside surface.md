---
version: 1
type: video
provider: YouTube
id: EOpzN8ZHqpU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Surface%20integral%20ex3%20part%201-%20Parameterizing%20the%20outside%20surface.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d7b18fbc-c98d-4788-ae11-329f595ae709
updated: 1486069692
title: 'Surface integral ex3 part 1- Parameterizing the outside surface'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/EOpzN8ZHqpU/default.jpg
    - https://i3.ytimg.com/vi/EOpzN8ZHqpU/1.jpg
    - https://i3.ytimg.com/vi/EOpzN8ZHqpU/2.jpg
    - https://i3.ytimg.com/vi/EOpzN8ZHqpU/3.jpg
---
