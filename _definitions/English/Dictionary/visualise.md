---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/visualise
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484377981
title: visualise
categories:
    - Dictionary
---
visualise
     v 1: view the outline of by means of an X-ray; "The radiologist
          can visualize the cancerous liver" [syn: {visualize}]
     2: for a mental picture of something that is invisible or
        abstract; "Mathematicians often visualize" [syn: {visualize}]
     3: imagine; conceive of; see in one's mind; "I can't see him on
        horseback!"; "I can see what will happen"; "I can see a
        risk in this strategy" [syn: {visualize}, {envision}, {project},
         {fancy}, {see}, {figure}, {picture}, {image}]
     4: make visible; "With this machine, ultrasound can be
        visualized" [syn: {visualize}]
