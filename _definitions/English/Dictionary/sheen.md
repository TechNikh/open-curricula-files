---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sheen
offline_file: ""
offline_thumbnail: ""
uuid: 3ed756c6-d2c0-4600-a09d-39d9e89b13ea
updated: 1484310581
title: sheen
categories:
    - Dictionary
---
sheen
     n : the visual property of something that shines with reflected
         light [syn: {shininess}, {luster}, {lustre}]
