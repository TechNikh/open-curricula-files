---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lighter
offline_file: ""
offline_thumbnail: ""
uuid: 54886ca4-e040-4414-8a2c-b60d282be0e0
updated: 1484310416
title: lighter
categories:
    - Dictionary
---
lighter
     n 1: a substance used to ignite or kindle a fire [syn: {igniter},
           {ignitor}]
     2: a device for lighting or igniting fuel or charges or fires;
        "do you have a light?" [syn: {light}, {igniter}, {ignitor}]
     3: a flatbottom boat for carrying heavy loads (especially on
        canals) [syn: {barge}, {flatboat}, {hoy}]
     v : transport in a flatbottom boat
