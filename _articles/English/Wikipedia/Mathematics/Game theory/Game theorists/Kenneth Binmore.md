---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kenneth_Binmore
offline_file: ""
offline_thumbnail: ""
uuid: 54b9e646-b044-4e04-81a4-ab881e5f853f
updated: 1484308518
title: Kenneth Binmore
tags:
    - Education
    - Research
    - Affiliations
    - Books
    - Selected articles
    - Interviews with Binmore
categories:
    - Game theorists
---
Kenneth George "Ken" Binmore, CBE (born 27 September 1940) is a British mathematician, economist, and game theorist. He is a Professor Emeritus of Economics at University College London (UCL)[1] and a Visiting Emeritus Professor of Economics at the University of Bristol.[2]
