---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deprivation
offline_file: ""
offline_thumbnail: ""
uuid: e95fc6cb-1fb1-4f15-90ea-1cf53c4759aa
updated: 1484310599
title: deprivation
categories:
    - Dictionary
---
deprivation
     n 1: a state of extreme poverty [syn: {privation}, {want}]
     2: the disadvantage that results from losing something; "his
        loss of credibility led to his resignation"; "losing him
        is no great deprivation" [syn: {loss}]
     3: act of depriving someone of food or money or rights;
        "nutritional privation"; "deprivation of civil rights"
        [syn: {privation}]
