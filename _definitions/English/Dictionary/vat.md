---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vat
offline_file: ""
offline_thumbnail: ""
uuid: 9be38226-0dfa-45e1-bbf8-3fe11ac32a84
updated: 1484310313
title: vat
categories:
    - Dictionary
---
VAT
     n 1: a tax levied on the difference between a commodity's price
          before taxes and its cost of production [syn: {value-added
          tax}, {ad valorem tax}]
     2: a large open vessel for holding or storing liquids [syn: {tub}]
