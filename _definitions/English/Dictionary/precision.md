---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/precision
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484526781
title: precision
categories:
    - Dictionary
---
precision
     n : the quality of being reproducible in amount or performance;
         "he handled it with the preciseness of an automaton";
         "note the meticulous precision of his measurements" [syn:
          {preciseness}] [ant: {impreciseness}, {impreciseness}]
