---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lecture
offline_file: ""
offline_thumbnail: ""
uuid: 93fde1b9-5947-48ac-b628-d8aab26c393c
updated: 1484310196
title: lecture
categories:
    - Dictionary
---
lecture
     n 1: a speech that is open to the public; "he attended a lecture
          on telecommunications" [syn: {public lecture}, {talk}]
     2: a lengthy rebuke; "a good lecture was my father's idea of
        discipline"; "the teacher gave him a talking to" [syn: {speech},
         {talking to}]
     3: teaching by giving a discourse on some subject (typically to
        a class) [syn: {lecturing}]
     v 1: deliver a lecture or talk; "She will talk at Rutgers next
          week"; "Did you ever lecture at Harvard?" [syn: {talk}]
     2: censure severely or angrily; "The mother scolded the child
        for entering a stranger's car"; "The deputy ragged the
        Prime Minister"; ...
