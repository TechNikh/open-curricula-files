---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sperm
offline_file: ""
offline_thumbnail: ""
uuid: caf77bb7-691b-4ce6-8d87-8eb8eab89b26
updated: 1484310297
title: sperm
categories:
    - Dictionary
---
sperm
     n : the male reproductive cell; the male gamete; "a sperm is
         mostly a nucleus surrounded by little other cellular
         material" [syn: {sperm cell}, {spermatozoon}, {spermatozoan}]
