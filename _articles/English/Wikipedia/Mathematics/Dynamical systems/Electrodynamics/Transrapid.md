---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transrapid
offline_file: ""
offline_thumbnail: ""
uuid: cb1f2369-8ef6-4c64-b604-0381b05251d5
updated: 1484308764
title: Transrapid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Transrapid-emsland.jpg
tags:
    - Technology
    - Levitation
    - Propulsion
    - Energy requirements
    - Market segment, ecological impact and historical parallels
    - Ecological impact
    - Comparative costs
    - Track construction cost
    - Train purchase cost
    - Operational cost
    - Critics
    - Implementations
    - China
    - Germany
    - Planned systems
    - Iran
    - Switzerland
    - Colorado I-70
    - Los Angeles to Las Vegas
    - Other USA
    - Tenerife
    - Rejected systems
    - Germany
    - High-speed competition
    - Munich link
    - United Kingdom
    - Incidents
    - September 2006 accident
    - SMT fire accident
    - Alleged theft of Transrapid technology
    - Development history and versions
categories:
    - Electrodynamics
---
Transrapid is a German high-speed monorail train using magnetic levitation based on a 1934 patent. Planning for the Transrapid system started in 1969 with a test facility for the system in Emsland, Germany completed in 1987. In 1991, technical readiness for application was approved by the Deutsche Bundesbahn in cooperation with renowned universities.[1]
