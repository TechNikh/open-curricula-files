---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/statement
offline_file: ""
offline_thumbnail: ""
uuid: f8dc34c4-662e-48d2-bf3e-b1b2fe529de6
updated: 1484310366
title: statement
categories:
    - Dictionary
---
statement
     n 1: a message that is stated or declared; a communication (oral
          or written) setting forth particulars or facts etc;
          "according to his statement he was in London on that
          day"
     2: a fact or assertion offered as evidence that something is
        true; "it was a strong argument that his hypothesis was
        true" [syn: {argument}]
     3: (music) the presentation of a musical theme; "the initial
        statement of the sonata"
     4: a nonverbal message; "a Cadillac makes a statement about who
        you are"; "his tantrums are a statement of his need for
        attention"
     5: the act of affirming or asserting or stating something ...
