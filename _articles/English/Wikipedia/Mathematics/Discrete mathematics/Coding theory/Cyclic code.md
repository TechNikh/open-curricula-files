---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyclic_code
offline_file: ""
offline_thumbnail: ""
uuid: 917e5ef9-f3f1-40db-b3df-2de33787bb3d
updated: 1484308544
title: Cyclic code
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Rotate_right.svg.png
tags:
    - Definition
    - Algebraic structure
    - Examples
    - Trivial examples
    - Quasi-cyclic codes and shortened codes
    - Definition
    - Definition
    - Cyclic codes for correcting errors
    - For correcting two errors
    - Hamming code
    - Hamming code for correcting single errors
    - Cyclic codes for correcting burst errors
    - Fire codes as cyclic bounds
    - Cyclic codes on Fourier transform
    - Fourier transform over finite fields
    - Spectral description of cyclic codes
    - BCH bound
    - Hartmann-Tzeng bound
    - Roos bound
    - Quadratic residue codes
    - Generalizations
    - Notes
categories:
    - Coding theory
---
In coding theory, a cyclic code is a block code, where the circular shifts of each codeword gives another word that belongs to the code. They are error-correcting codes that have algebraic properties that are convenient for efficient error detection and correction.
