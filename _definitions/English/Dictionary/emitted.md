---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emitted
offline_file: ""
offline_thumbnail: ""
uuid: da82345c-2841-427f-9e97-4d29f9c284be
updated: 1484310391
title: emitted
categories:
    - Dictionary
---
emit
     v 1: expel (gases or odors) [syn: {breathe}, {pass off}]
     2: give off, send forth, or discharge; as of light, heat, or
        radiation, vapor, etc.; "The ozone layer blocks some
        harmful rays which the sun emits" [syn: {give out}, {give
        off}] [ant: {absorb}]
     3: express audibly; utter sounds (not necessarily words); "She
        let out a big heavy sigh"; "He uttered strange sounds that
        nobody could understand" [syn: {utter}, {let out}, {let
        loose}]
     [also: {emitting}, {emitted}]
