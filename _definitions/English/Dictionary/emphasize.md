---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emphasize
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484603161
title: emphasize
categories:
    - Dictionary
---
emphasize
     v 1: to stress, single out as important; "Dr. Jones emphasizes
          exercise in addition to a change in diet" [syn: {stress},
           {emphasise}, {punctuate}, {accent}, {accentuate}]
     2: give extra weight to (a communication); "Her gesture
        emphasized her words" [syn: {underscore}, {underline}, {emphasise}]
