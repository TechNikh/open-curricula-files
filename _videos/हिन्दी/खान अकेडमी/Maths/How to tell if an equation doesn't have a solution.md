---
version: 1
type: video
provider: YouTube
id: rv1SothKZck
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a2b97187-4c75-41b4-9507-6e76eb0cf77e
updated: 1486069618
title: "How to tell if an equation doesn't have a solution"
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/rv1SothKZck/default.jpg
    - https://i3.ytimg.com/vi/rv1SothKZck/1.jpg
    - https://i3.ytimg.com/vi/rv1SothKZck/2.jpg
    - https://i3.ytimg.com/vi/rv1SothKZck/3.jpg
---
