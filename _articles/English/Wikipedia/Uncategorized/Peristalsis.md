---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Peristaltic
offline_file: ""
offline_thumbnail: ""
uuid: 3e5c905a-b4b3-42df-89d4-879c26afa9f5
updated: 1484308378
title: Peristalsis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Peristaltic_0.jpg
tags:
    - Human physiology
    - Esophagus
    - Small intestine
    - Large intestine
    - Lymph
    - Sperm
    - Earthworms
    - Machinery
    - Related terms
    - Notes and references
categories:
    - Uncategorized
---
In much of a digestive tract such as the human gastrointestinal tract, smooth muscle tissue contracts in sequence to produce a peristaltic wave, which propels a ball of food (called a bolus while in the esophagus and upper gastrointestinal tract and chyme in the stomach) along the tract. Peristaltic movement comprises relaxation of circular smooth muscles, then their contraction behind the chewed material to keep it from moving backward, then longitudinal contraction to push it forward.
