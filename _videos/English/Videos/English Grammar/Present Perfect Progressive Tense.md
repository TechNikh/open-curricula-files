---
version: 1
type: video
provider: YouTube
id: 4SmK-iCJQ2E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Present%20Perfect%20Progressive%20Tense.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 206f3dcf-98f3-4309-9189-e34bad658020
updated: 1486069632
title: Present Perfect Progressive Tense
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/4SmK-iCJQ2E/default.jpg
    - https://i3.ytimg.com/vi/4SmK-iCJQ2E/1.jpg
    - https://i3.ytimg.com/vi/4SmK-iCJQ2E/2.jpg
    - https://i3.ytimg.com/vi/4SmK-iCJQ2E/3.jpg
---
