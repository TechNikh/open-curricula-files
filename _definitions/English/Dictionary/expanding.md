---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expanding
offline_file: ""
offline_thumbnail: ""
uuid: e516d025-4da9-4e84-b7e8-68959ff4b8d4
updated: 1484310453
title: expanding
categories:
    - Dictionary
---
expanding
     adj : increasing in area or volume or bulk or range; "an expanding
           universe"; "an expanding economy"
