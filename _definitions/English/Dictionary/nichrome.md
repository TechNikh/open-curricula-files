---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nichrome
offline_file: ""
offline_thumbnail: ""
uuid: 0b3030b1-27d3-4c7f-8f4d-eaa6f88618d0
updated: 1484310198
title: nichrome
categories:
    - Dictionary
---
Nichrome
     n : a nickel-chromium alloy with high electrical resistance and
         an ability to withstand high temperatures; used for
         resistance heating elements
