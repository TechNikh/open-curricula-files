---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/childbirth
offline_file: ""
offline_thumbnail: ""
uuid: e8d97e75-b4d0-4e7c-8fb2-62eb6e89f4c9
updated: 1484310515
title: childbirth
categories:
    - Dictionary
---
childbirth
     n : the parturition process in human beings; having a baby; the
         process of giving birth to a child [syn: {childbearing},
         {accouchement}, {vaginal birth}]
