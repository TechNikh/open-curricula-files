---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Beilinson_regulator
offline_file: ""
offline_thumbnail: ""
uuid: 113616ee-8b2d-4a41-9456-0bf71e2f04f7
updated: 1484309134
title: Beilinson regulator
categories:
    - Algebraic geometry
---
Here, X is a complex smooth projective variety, for example. It is named after Alexander Beilinson. The Beilinson regulator features in Beilinson's conjecture on special values of L-functions.
