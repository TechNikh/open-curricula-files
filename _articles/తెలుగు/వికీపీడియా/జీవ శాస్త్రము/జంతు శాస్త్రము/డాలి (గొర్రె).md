---
version: 1
type: article
id: 'https://te.wikipedia.org/wiki/%E0%B0%A1%E0%B0%BE%E0%B0%B2%E0%B0%BF_(%E0%B0%97%E0%B1%8A%E0%B0%B0%E0%B1%8D%E0%B0%B0%E0%B1%86)'
offline_file: ""
offline_thumbnail: ""
uuid: 42099127-87f2-48f1-b70a-dd0d0b1ade6b
updated: 1484308506
title: డాలి (గొర్రె)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Dolly_clone.svg.png
tags:
    - జీవితం
    - మరణం
    - మూలాలు
    - బయటి లింకులు
categories:
    - జంతు శాస్త్రము
---
డాలి - Dolly (5 జూలై 1996 – 14 ఫిబ్రవరి 2003) ఒక ఆడ గొర్రె. క్లోనింగ్ ప్రక్రియ ద్వారా మొదట సృష్టింపబడిన క్షీరదం.[2][3] ఇయాన్ విల్ముట్, కీత్ కేంప్ బెల్ మరియు సహోద్యోగులచే రోస్లిన్ ఇన్స్టిట్యూట్ ( ఎడిన్ బర్గ్ విశ్వవిద్యాలయం యునైటెడ్ కింగ్డమ్) లో క్లోనింగ్ ప్రక్రియ ద్వారా ...
