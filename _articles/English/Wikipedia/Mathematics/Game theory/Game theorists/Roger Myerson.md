---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Roger_Myerson
offline_file: ""
offline_thumbnail: ""
uuid: b8221d5a-545a-4727-9b38-5264fec81144
updated: 1484308509
title: Roger Myerson
tags:
    - Biography
    - Bank of Sweden Nobel Memorial Prize
    - Personal life
    - Publications
    - Concepts named after him
categories:
    - Game theorists
---
Roger Bruce Myerson (born March 29, 1951) is an American economist and winner of the Sveriges Riksbank Prize in Economic Sciences in Memory of Alfred Nobel, sometimes referred to as the "Nobel Prize" for economics, with Leonid Hurwicz and Eric Maskin for "having laid the foundations of mechanism design theory."[1] A professor at the University of Chicago, he has made contributions as an economist and as a political scientist.
