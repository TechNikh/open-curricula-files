---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/first-class
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484500861
title: first-class
categories:
    - Dictionary
---
first class
     n 1: the highest rank in a classification
     2: mail that includes letters and postcards and packages sealed
        against inspection [syn: {1st class}, {first-class mail},
        {1st-class mail}, {priority mail}]
     3: the most expensive accommodations on a ship or train or
        plane
