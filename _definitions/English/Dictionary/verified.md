---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/verified
offline_file: ""
offline_thumbnail: ""
uuid: b62f8cc1-73dc-4b75-9acd-19ed56d0869f
updated: 1484310365
title: verified
categories:
    - Dictionary
---
verified
     adj 1: supported or established by evidence or proof; "the
            substantiated charges"; "a verified case" [syn: {corroborated},
             {substantiated}]
     2: proved to be true; "a verified claim"
