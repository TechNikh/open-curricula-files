---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tyranny
offline_file: ""
offline_thumbnail: ""
uuid: c0c76053-1857-4c9c-8d1d-475dfef022cf
updated: 1484310593
title: tyranny
categories:
    - Dictionary
---
tyranny
     n 1: a form of government in which the ruler is an absolute
          dictator (not restricted by a constitution or laws or
          opposition etc.) [syn: {dictatorship}, {absolutism}, {authoritarianism},
           {Caesarism}, {despotism}, {monocracy}, {one-man rule},
          {shogunate}, {Stalinism}, {totalitarianism}]
     2: dominance through threat of punishment and violence [syn: {absolutism},
         {despotism}]
