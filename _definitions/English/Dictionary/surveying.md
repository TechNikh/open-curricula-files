---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surveying
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484377381
title: surveying
categories:
    - Dictionary
---
surveying
     n : the practice of measuring angles and distances on the ground
         so that they can be accurately plotted on a map; "he
         studied surveying at college"
