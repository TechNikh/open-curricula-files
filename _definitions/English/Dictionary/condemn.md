---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/condemn
offline_file: ""
offline_thumbnail: ""
uuid: 35c06b0a-32c6-4f19-98e9-54f1bcf66c95
updated: 1484310429
title: condemn
categories:
    - Dictionary
---
condemn
     v 1: express strong disapproval of; "We condemn the racism in
          South Africa"; "These ideas were reprobated" [syn: {reprobate},
           {decry}, {objurgate}, {excoriate}]
     2: declare or judge unfit; "The building was condemned by the
        inspector"
     3: compel or force into a particular state or activity; "His
        devotion to his sick wife condemned him to a lonely
        existence"
     4: demonstrate the guilt of (someone); "Her strange behavior
        condemned her"
     5: pronounce a sentence on (somebody) in a court of law; "He
        was condemned to ten years in prison" [syn: {sentence}, {doom}]
