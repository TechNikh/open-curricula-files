---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trigger_strategy
offline_file: ""
offline_thumbnail: ""
uuid: 1273d9cc-9284-4503-81a3-4cdf7030522d
updated: 1484309057
title: Trigger strategy
categories:
    - Non-cooperative games
---
In game theory, a trigger strategy is any of a class of strategies employed in a repeated non-cooperative game. A player using a trigger strategy initially cooperates but punishes the opponent if a certain level of defection (i.e., the trigger) is observed. The level of punishment and the sensitivity of the trigger vary with different trigger strategies.
