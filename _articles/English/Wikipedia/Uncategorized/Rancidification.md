---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rancidity
offline_file: ""
offline_thumbnail: ""
uuid: 8014d5ce-ea8d-4113-95ec-810a3970bd6c
updated: 1484308313
title: Rancidification
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lipid_peroxidation.svg.png
tags:
    - Rancidification pathways
    - Hydrolytic rancidity
    - Oxidative rancidity
    - Microbial rancidity
    - Health effects
    - Reducing rancidification
    - Measurement of oxidative stability
categories:
    - Uncategorized
---
Rancidification, the product of which can be described as rancidity, is the process which causes a substance to become rancid, that is, having a rank, unpleasant smell or taste. Specifically, it is the hydrolysis and/or autoxidation of fats into short-chain aldehydes and ketones which are objectionable in taste and odor.[1] When these processes occur in food, undesirable odors and flavors can result. In some cases, however, the flavors can be desirable (as in aged cheeses).[2] In processed meats, these flavors are collectively known as warmed-over flavor. Rancidification can also detract from the nutritional value of food, and some vitamins are highly sensitive to degradation.[3] Akin to ...
