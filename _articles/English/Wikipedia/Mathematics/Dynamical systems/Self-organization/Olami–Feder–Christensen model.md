---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Olami%E2%80%93Feder%E2%80%93Christensen_model'
offline_file: ""
offline_thumbnail: ""
uuid: 55d98766-d380-4157-9cff-38a912320314
updated: 1484308819
title: Olami–Feder–Christensen model
tags:
    - Model definition
    - Model rules
    - Behaviour and criticality
categories:
    - Self-organization
---
In physics, in the area of dynamical systems, the Olami–Feder–Christensen model is an earthquake model conjectured to be an example of self-organized criticality where local exchange dynamics are not conservative. Despite the original claims of the authors and subsequent claims of other authors such as Lise, whether or not the model is self organized critical remains an open question.
