---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/depressed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484409781
title: depressed
categories:
    - Dictionary
---
depressed
     adj 1: lower than previously; "the market is depressed"; "prices
            are down" [syn: {down(p)}]
     2: flattened downward as if pressed from above or flattened
        along the dorsal and ventral surfaces
     3: low in spirits; "lonely and blue in a strange city";
        "depressed by the loss of his job"; "a dispirited and
        resigned expression on her face"; "downcast after his
        defeat"; "feeling discouraged and downhearted" [syn: {blue},
         {dispirited}, {down(p)}, {downcast}, {downhearted}, {down
        in the mouth}, {low}, {low-spirited}]
     4: having the central portion lower than the margin; "a
        depressed pustule" [syn: ...
