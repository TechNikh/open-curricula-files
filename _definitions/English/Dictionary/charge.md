---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/charge
offline_file: ""
offline_thumbnail: ""
uuid: 64dd5477-ce01-4514-9c70-cbfe513661f9
updated: 1484310206
title: charge
categories:
    - Dictionary
---
charge
     n 1: (criminal law) a pleading describing some wrong or offense;
          "he was arrested on a charge of larceny" [syn: {complaint}]
     2: the price charged for some article or service; "the
        admission charge"
     3: an assertion that someone is guilty of a fault or offence;
        "the newspaper published charges that Jones was guilty of
        drunken driving" [syn: {accusation}]
     4: request for payment of a debt; "they submitted their charges
        at the end of each month" [syn: {billing}]
     5: a impetuous rush toward someone or something; "the
        wrestler's charge carried him past his adversary"; "the
        battle began with a cavalry charge"
  ...
