---
version: 1
type: video
provider: YouTube
id: rYG1D5lUE4I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/i%20as%20the%20principal%20root%20of%20-1%20%28a%20little%20technical%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bdd31eeb-a74f-47a6-b42b-408f61d998f0
updated: 1486069571
title: i as the principal root of -1 (a little technical)
categories:
    - Precalculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/rYG1D5lUE4I/default.jpg
    - https://i3.ytimg.com/vi/rYG1D5lUE4I/1.jpg
    - https://i3.ytimg.com/vi/rYG1D5lUE4I/2.jpg
    - https://i3.ytimg.com/vi/rYG1D5lUE4I/3.jpg
---
i as the principal square root of -1

Watch the next lesson: https://www.khanacademy.org/math/precalculus/imaginary_complex_precalc/complex_num_precalc/v/complex-number-intro?utm_source=YT&utm_medium=Desc&utm_campaign=Precalculus

Missed the previous lesson? 
https://www.khanacademy.org/math/precalculus/imaginary_complex_precalc/i_precalc/v/imaginary-roots-of-negative-numbers?utm_source=YT&utm_medium=Desc&utm_campaign=Precalculus

Precalculus on Khan Academy: You may think that precalculus is simply the course you take before calculus. You would be right, of course, but that definition doesn't mean anything unless you have some knowledge of what calculus is. Let's keep it simple, shall we? Calculus is a conceptual framework which provides systematic techniques for solving problems. These problems are appropriately applicable to analytic geometry and algebra. Therefore....precalculus gives you the background for the mathematical concepts, problems, issues and techniques that appear in calculus, including trigonometry, functions, complex numbers, vectors, matrices, and others. There you have it ladies and gentlemen....an introduction to precalculus!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Precalculus channel:
https://www.youtube.com/channel/UCBeHztHRWuVvnlwm20u2hNA?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
