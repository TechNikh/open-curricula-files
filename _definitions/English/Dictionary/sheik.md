---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sheik
offline_file: ""
offline_thumbnail: ""
uuid: f814f93e-8adc-49b1-99d3-6e8a20fc0eea
updated: 1484310609
title: sheik
categories:
    - Dictionary
---
sheik
     n 1: the leader of an Arab village or family [syn: {tribal sheik},
           {sheikh}, {tribal sheikh}, {Arab chief}]
     2: a man who is much concerned with his dress and appearance
        [syn: {dandy}, {dude}, {fop}, {gallant}, {beau}, {swell},
        {fashion plate}, {clotheshorse}]
