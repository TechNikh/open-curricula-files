---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kudankulam
offline_file: ""
offline_thumbnail: ""
uuid: 56ff9f3d-c33c-471a-bafb-c6db5b827b87
updated: 1484308426
title: Koodankulam
categories:
    - Uncategorized
---
Kudankulam is a town in the Tirunelveli district in Tamil Nadu, India. It is situated 24 km north-east of Kanyakumari, 36 km from Nagercoil and about 106 km from Thiruvananthapuram.
