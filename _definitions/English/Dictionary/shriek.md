---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shriek
offline_file: ""
offline_thumbnail: ""
uuid: 42be8897-1cc9-4abb-abaa-4d88a63d1374
updated: 1484310144
title: shriek
categories:
    - Dictionary
---
shriek
     n 1: sharp piercing cry; "her screaming attracted the neighbors"
          [syn: {scream}, {screaming}, {shrieking}, {screech}, {screeching}]
     2: a high-pitched noise resembling a human cry; "he ducked at
        the screechings of shells"; "he heard the scream of the
        brakes" [syn: {screech}, {screeching}, {shrieking}, {scream},
         {screaming}]
     v : utter a shrill cry [syn: {shrill}, {pipe up}, {pipe}]
