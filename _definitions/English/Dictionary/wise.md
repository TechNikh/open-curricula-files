---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wise
offline_file: ""
offline_thumbnail: ""
uuid: 5d551a79-58d6-4c0e-9c0a-89a2d822baaf
updated: 1484310346
title: wise
categories:
    - Dictionary
---
wise
     adj 1: having or prompted by wisdom or discernment; "a wise
            leader"; "a wise and perceptive comment" [ant: {foolish}]
     2: marked by the exercise of good judgment or common sense in
        practical matters; "judicious use of one's money"; "a
        sensible manager"; "a wise decision" [syn: {judicious}, {sensible}]
     3: evidencing the possession of inside information [syn: {knowing},
         {wise(p)}, {wise to(p)}]
     4: able to take a broad view of negotiations between states
        [syn: {diplomatic}]
     5: carefully considered; "a considered opinion" [syn: {considered}]
     n 1: a way of doing or being; "in no wise"; "in this wise"
     2: United ...
