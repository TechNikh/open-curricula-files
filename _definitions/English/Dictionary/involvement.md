---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/involvement
offline_file: ""
offline_thumbnail: ""
uuid: 2eaa266b-20be-49ac-bb47-3a2d8628aa65
updated: 1484310448
title: involvement
categories:
    - Dictionary
---
involvement
     n 1: the act of sharing in the activities of a group; "the
          teacher tried to increase his students' engagement in
          class activities" [syn: {engagement}, {participation}, {involution}]
          [ant: {non-engagement}, {non-engagement}, {non-engagement}]
     2: a connection of inclusion or containment; "he escaped
        involvement in the accident"; "there was additional
        involvement of the liver and spleen"
     3: a sense of concern with and curiosity about someone or
        something; "an interest in music" [syn: {interest}]
     4: a usually secretive or illicit sexual relationship [syn: {affair},
         {affaire}, {intimacy}, {liaison}, ...
