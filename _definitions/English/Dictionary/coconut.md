---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coconut
offline_file: ""
offline_thumbnail: ""
uuid: 9300df5c-5888-4995-befd-91a2180b0ec9
updated: 1484310228
title: coconut
categories:
    - Dictionary
---
coconut
     n 1: the edible white meat a coconut; often shredded for use in
          e.g. cakes and curries [syn: {coconut meat}]
     2: large hard-shelled oval nut with a fibrous husk containing
        thick white meat surrounding a central cavity filled (when
        fresh) with fluid or milk [syn: {cocoanut}]
     3: tall palm tree bearing coconuts as fruits; widely planted
        throughout the tropics [syn: {coconut palm}, {coco palm},
        {coco}, {cocoa palm}, {coconut tree}, {Cocos nucifera}]
