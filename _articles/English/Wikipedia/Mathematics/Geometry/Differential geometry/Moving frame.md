---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moving_frame
offline_file: ""
offline_thumbnail: ""
uuid: a4625060-5a82-4576-84b5-3b00b1db34fa
updated: 1484309232
title: Moving frame
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Frenet-Serret_moving_frame1.png
tags:
    - Introduction
    - Method of the moving frame
    - Moving tangent frames
    - Coframes
    - Uses
    - Further details
    - Atlases
    - Generalizations
    - Applications
    - Notes
categories:
    - Differential geometry
---
In mathematics, a moving frame is a flexible generalization of the notion of an ordered basis of a vector space often used to study the extrinsic differential geometry of smooth manifolds embedded in a homogeneous space.
