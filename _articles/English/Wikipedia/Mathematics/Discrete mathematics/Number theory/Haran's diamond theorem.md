---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Haran%27s_diamond_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 955f656c-45d8-40e4-95ec-49b241800510
updated: 1484308649
title: "Haran's diamond theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Diamond_theorem.jpg
tags:
    - Statement of the diamond theorem
    - Some corollaries
    - "Weissauer's theorem"
    - Haran–Jarden condition
categories:
    - Number theory
---
