---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ken-ichi_Kawarabayashi
offline_file: ""
offline_thumbnail: ""
uuid: bc5ca377-6903-41cb-8a07-5e9ec386c6ad
updated: 1484309468
title: Ken-ichi Kawarabayashi
categories:
    - Graph theorists
---
Ken-ichi Kawarabayashi (Japanese: 河原林 健一, born 1975) is a Japanese graph theorist who works as a professor at the National Institute of Informatics and is known for his research on graph theory (particularly the theory of graph minors) and graph algorithms.
