---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Dispersive_partial_differential_equation
offline_file: ""
offline_thumbnail: ""
uuid: 7a6da30c-5655-4c87-9f19-55e957c4ad6b
updated: 1484308816
title: Dispersive partial differential equation
tags:
    - Examples
    - Linear equations
    - Nonlinear equations
categories:
    - Nonlinear systems
---
In mathematics, a dispersive partial differential equation or dispersive PDE is a partial differential equation that is dispersive. In this context, dispersion means that waves of different wavelength propagate at different phase velocities.
