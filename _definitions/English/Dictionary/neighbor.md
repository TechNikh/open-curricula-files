---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neighbor
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484416741
title: neighbor
categories:
    - Dictionary
---
neighbor
     adj : situated near one another; "neighbor states" [syn: {neighbour},
            {neighboring(a)}, {neighbouring(a)}]
     n 1: a person who lives (or is located) near another [syn: {neighbour}]
     2: a nearby object of the same kind; "Fort Worth is a neighbor
        of Dallas"; "what is the closest neighbor to the Earth?"
        [syn: {neighbour}]
     v 1: live or be located as a neighbor; "the neighboring house"
          [syn: {neighbour}]
     2: be located near or adjacent to; "Pakistan neighbors India"
        [syn: {neighbour}]
