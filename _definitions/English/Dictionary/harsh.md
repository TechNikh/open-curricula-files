---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harsh
offline_file: ""
offline_thumbnail: ""
uuid: 2569120b-75e9-43fa-8f4f-24b262353766
updated: 1484310515
title: harsh
categories:
    - Dictionary
---
harsh
     adj 1: unpleasantly stern; "wild and harsh country full of hot sand
            and cactus"; "the nomad life is rough and hazardous"
            [syn: {rough}]
     2: disagreeable to the senses; "the harsh cry of a blue jay";
        "harsh cognac"; "the harsh white light makes you screw up
        your eyes"; "harsh irritating smoke filled the hallway"
     3: extremely unkind or cruel; "had harsh words"; "a harsh and
        unlovable old tyrant"
     4: severe; "a harsh penalty"
     5: used of circumstances (especially weather) that cause
        suffering; "brutal weather"; "northern winters can be
        cruel"; "a cruel world"; "a harsh climate"; "a rigorous
        ...
