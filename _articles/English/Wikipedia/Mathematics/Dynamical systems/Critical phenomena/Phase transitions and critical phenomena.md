---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Phase_transitions_and_critical_phenomena
offline_file: ""
offline_thumbnail: ""
uuid: f4892032-d772-46e5-a0bf-ef0dbdc9fd70
updated: 1484308754
title: Phase transitions and critical phenomena
categories:
    - Critical phenomena
---
Phase transitions and critical phenomena is a 20-volume series of books, comprising review articles on phase transitions and critical phenomena, published during 1972-2001. It is "considered the most authoritative series on the topic".[1]
