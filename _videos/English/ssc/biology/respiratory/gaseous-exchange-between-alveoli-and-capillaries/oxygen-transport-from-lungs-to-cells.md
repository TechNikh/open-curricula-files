---
title: Oxygen Transport from Lungs to Cells
youtube_id: 5LjLFrmKTSA
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
