---
version: 1
type: video
provider: YouTube
id: IGo_WMQmobs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f74e2a0a-23c1-40cb-9b2e-e01a22574558
updated: 1486069621
title: Comparing means of distributions
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/IGo_WMQmobs/default.jpg
    - https://i3.ytimg.com/vi/IGo_WMQmobs/1.jpg
    - https://i3.ytimg.com/vi/IGo_WMQmobs/2.jpg
    - https://i3.ytimg.com/vi/IGo_WMQmobs/3.jpg
---
