---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transfusion
offline_file: ""
offline_thumbnail: ""
uuid: dcb5a85c-3f4a-42d2-b72d-71508d06b95e
updated: 1484310163
title: transfusion
categories:
    - Dictionary
---
transfusion
     n 1: the introduction of blood or blood plasma into a vein or
          artery [syn: {blood transfusion}]
     2: the action of pouring a liquid from one vessel to another
