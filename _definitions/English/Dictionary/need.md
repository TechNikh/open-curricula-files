---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/need
offline_file: ""
offline_thumbnail: ""
uuid: 677892a1-608b-48f0-99fe-f9c855209f02
updated: 1484310359
title: need
categories:
    - Dictionary
---
need
     n 1: a condition requiring relief; "she satisfied his need for
          affection"; "God has no need of men to accomplish His
          work"; "there is a demand for jobs" [syn: {demand}]
     2: anything that is necessary but lacking; "he had sufficient
        means to meet his simple needs"; "I tried to supply his
        wants" [syn: {want}]
     3: the psychological feature that arouses an organism to action
        toward a desired goal; the reason for the action; that
        which gives purpose and direction to behavior; "we did not
        understand his motivation"; "he acted with the best of
        motives" [syn: {motivation}, {motive}]
     4: a state of extreme ...
