---
title: Rainwater harvesting
article_ref: https://en.wikipedia.org/wiki/Rainwater_harvesting
tags:
    - Advantages
    - Quality
    - System setup
    - Life Cycle Assessment: Design for Environment
    - Rain water harvesting by freshwater flooded forests
    - New approaches
    - History
    - Earlier
    - Present day
    - Canada
    - India
    - Israel
    - Sri Lanka
    - South Africa
    - United Kingdom
    - Non-traditional
categories:
    - Water harvesting
---
