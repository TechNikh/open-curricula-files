---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distortion
offline_file: ""
offline_thumbnail: ""
uuid: 4db0d15b-4d8a-45b9-9547-bb11d6ed683b
updated: 1484310405
title: distortion
categories:
    - Dictionary
---
distortion
     n 1: a change for the worse [syn: {deformation}]
     2: a shape resulting from distortion [syn: {distorted shape}]
     3: an optical phenomenon resulting from the failure of a lens
        or mirror to produce a good image [syn: {aberration}, {optical
        aberration}]
     4: a change (usually undesired) in the waveform of an acoustic
        or analog electrical signal; the difference between two
        measurements of a signal (as between the input and output
        signal); "heavy metal guitar players use vacuum tube
        amplifiers to produce extreme distortion"
     5: the act of distorting something so it seems to mean
        something it was not intended ...
