---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spatial_bifurcation
offline_file: ""
offline_thumbnail: ""
uuid: c614c1ae-64ef-4d93-a71d-0968481a0379
updated: 1484308707
title: Spatial bifurcation
categories:
    - Bifurcation theory
---
Spatial bifurcation is a form of bifurcation theory. The classic bifurcation analysis is referred to as an ordinary differential equation system, which is independent on the spatial variables. However, most realistic systems are spatially dependent. In order to understand spatial variable system (partial differential equations), some scientists try to treat with the spatial variable as time and use the AUTO package[1] get a bifurcation results.[2][3]
