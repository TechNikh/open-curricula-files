---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generic_property
offline_file: ""
offline_thumbnail: ""
uuid: 27873ebd-366f-4dfc-acba-bd17b24bc01a
updated: 1484309149
title: Generic property
tags:
    - 'Definitions: measure theory'
    - Probability
    - Discrete mathematics
    - 'Definitions: topology'
    - Function spaces
    - Algebraic geometry
    - Algebraic varieties
    - Generic point
    - General position
    - Genericity results
categories:
    - Algebraic geometry
---
In mathematics, properties that hold for "typical" examples are called generic properties. For instance, a generic property of a class of functions is one that is true of "almost all" of those functions, as in the statements, "A generic polynomial does not have a root at zero," or "A generic matrix is invertible." As another example, a generic property of a space is a property that holds at "almost all" points of the space, as in the statement, "If f : M → N is a smooth function between smooth manifolds, then a generic point of N is not a critical value of f." (This is by Sard's theorem.)
