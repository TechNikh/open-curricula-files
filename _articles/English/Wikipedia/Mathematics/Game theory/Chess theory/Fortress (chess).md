---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fortress_(chess)
offline_file: ""
offline_thumbnail: ""
uuid: d21b4e24-de05-442b-87c4-6811d3609222
updated: 1484309020
title: Fortress (chess)
tags:
    - Fortress in a corner
    - Rook and pawn versus queen
    - Example from game
    - Similar example
    - Example with more pawns
    - Opposite-colored bishops
    - Example from game
    - Queen versus two minor pieces
    - Bishop and knight
    - Two bishops
    - Two knights
    - With pawns
    - Knight versus a rook and pawn
    - Bishop versus rook and bishop pawn on the sixth rank
    - Defense perimeter (pawn fortress)
    - Other examples
    - Fortresses against a bishop
    - Fortresses against a knight
    - Fortresses against a rook
    - A semi-fortress
    - Positional draw
    - Notes
categories:
    - Chess theory
---
In chess, the fortress is an endgame drawing technique in which the side behind in material sets up a zone of protection around their king that cannot be penetrated by the opponent. This only works when the opponent does not have a passed pawn or cannot create one, unless that pawn can be stopped (e.g. see the opposite-colored bishops example). An elementary fortress is a theoretically drawn (i.e. a book draw) position with reduced material in which a passive defense will maintain the draw (Müller & Pajeken 2008:183).
