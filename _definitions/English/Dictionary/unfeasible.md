---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unfeasible
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484385061
title: unfeasible
categories:
    - Dictionary
---
unfeasible
     adj : not capable of being carried out or put into practice;
           "refloating the sunken ship proved impracticable
           because of its fragility"; "a suggested reform that was
           unfeasible in the prevailing circumstances" [syn: {impracticable},
            {infeasible}, {unworkable}]
