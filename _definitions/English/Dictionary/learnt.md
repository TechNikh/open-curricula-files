---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/learnt
offline_file: ""
offline_thumbnail: ""
uuid: e4e612e3-5dd6-40b4-a3e8-91b58c1543cf
updated: 1484310351
title: learnt
categories:
    - Dictionary
---
learn
     v 1: acquire or gain knowledge or skills; "She learned dancing
          from her sister"; "I learned Sanskrit"; "Children
          acquire language at an amazing rate" [syn: {larn}, {acquire}]
     2: get to know or become aware of, usually accidentally; "I
        learned that she has two grown-up children"; "I see that
        you have been promoted" [syn: {hear}, {get word}, {get
        wind}, {pick up}, {find out}, {get a line}, {discover}, {see}]
     3: commit to memory; learn by heart; "Have you memorized your
        lines for the play yet?" [syn: {memorize}, {memorise}, {con}]
     4: be a student of a certain subject; "She is reading for the
        bar exam" [syn: ...
