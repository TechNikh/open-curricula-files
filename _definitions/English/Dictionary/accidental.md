---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accidental
offline_file: ""
offline_thumbnail: ""
uuid: faec8cf2-5dd9-4783-8eea-be0751728e55
updated: 1484310405
title: accidental
categories:
    - Dictionary
---
accidental
     adj 1: associated by chance and not an integral part; "poetry is
            something to which words are the accidental, not by
            any means the essential form"- Frederick W. Robertson;
            "they had to decide whether his misconduct was
            adventitious or the result of a flaw in his character"
            [syn: {adventitious}]
     2: occurring or appearing or singled out by chance; "their
        accidental meeting led to a renewal of their friendship";
        "seek help from casual passers-by"; "a casual meeting"; "a
        chance occurrence" [syn: {casual}, {chance(a)}]
     3: without intention (especially resulting from heedless
        ...
