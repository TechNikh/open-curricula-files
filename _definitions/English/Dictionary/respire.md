---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/respire
offline_file: ""
offline_thumbnail: ""
uuid: 0e712ecc-4d22-46e6-9731-b8a006f4d488
updated: 1484310369
title: respire
categories:
    - Dictionary
---
respire
     v 1: breathe easily again, as after exertion or anxiety
     2: undergo the biomedical and metabolic processes of
        respiration by taking up oxygen and producing
        carbonmonoxide
     3: draw air into, and expel out of, the lungs; "I can breathe
        better when the air is clean"; "The patient is respiring"
        [syn: {breathe}, {take a breath}, {suspire}]
