---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Risty
offline_file: ""
offline_thumbnail: ""
uuid: f7517c50-fda2-4f7d-8df0-400c2dd338cc
updated: 1484308329
title: 'X-Men: Evolution'
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-XMEFullRoster.jpg
tags:
    - Plot
    - Season one
    - Season two
    - Season three
    - Season four
    - Final moments
    - Cast and characters
    - Music
    - Production notes
    - Successors
    - Reception
    - Awards and nominations
    - Analysis
    - Comparison with original comics
    - Evolution characters in the comics and films
    - Marvel references and cameos
    - Home media release
    - iTunes
    - DVD
    - Netflix
    - YouTube
    - Google Play
    - Hulu
    - Amazon
    - Merchandise
    - Comic books
    - Action figurines
categories:
    - Uncategorized
---
X-Men: Evolution is an American animated television series about the Marvel Comics superhero team X-Men.[1] In this incarnation, many of the characters are teenagers rather than adults. The series ran for a total of four seasons (52 episodes) from November 2000 until October 2003 on Kids' WB, which has made it the third longest-running Marvel Comics animated series, behind only Fox Kids' X-Men and Spider-Man animated series. The series began running on Disney XD on June 15, 2009.
