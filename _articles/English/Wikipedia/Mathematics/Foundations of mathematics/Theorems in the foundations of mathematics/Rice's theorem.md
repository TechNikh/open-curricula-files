---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Rice%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 35b210dd-0e49-4eec-8584-3c03ace06ac7
updated: 1484309004
title: "Rice's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rice_reduction.svg.png
tags:
    - Introduction
    - Formal statement
    - Examples
    - "Proof by Kleene's recursion theorem"
    - Proof by reduction to the halting problem
    - Proof Sketch
    - Formal proof
    - "Rice's theorem and index sets"
    - "An analogue of Rice's theorem for recursive sets"
    - Notes
categories:
    - Theorems in the foundations of mathematics
---
In computability theory, Rice's theorem states that all non-trivial, semantic properties of programs are undecidable. A semantic property is one about the programs behavior (for instance, does the program terminate for all inputs), unlike a syntactic property (for instance, does the program contain an if-then-else statement). A property is non-trivial if it is neither true for every program, nor for no program.
