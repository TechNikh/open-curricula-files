---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/week
offline_file: ""
offline_thumbnail: ""
uuid: 6e23c7b0-29f8-45cf-a004-7443c488e179
updated: 1484310242
title: week
categories:
    - Dictionary
---
week
     n 1: any period of seven consecutive days; "it rained for a week"
          [syn: {hebdomad}]
     2: a period of seven consecutive days starting on Sunday [syn:
        {calendar week}]
     3: hours or days of work in a calendar week; "they worked a
        40-hour week" [syn: {workweek}]
