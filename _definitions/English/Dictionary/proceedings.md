---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proceedings
offline_file: ""
offline_thumbnail: ""
uuid: 3e3687f6-cd8d-42d0-a9d3-2d8c8b239d07
updated: 1484310595
title: proceedings
categories:
    - Dictionary
---
proceedings
     n 1: (law) the institution of a sequence of steps by which legal
          judgments are invoked [syn: {proceeding}, {legal
          proceeding}]
     2: a written account of what transpired at a meeting [syn: {minutes},
         {transactions}]
