---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Th%C3%A9bault%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: ac325eb1-5c81-4e97-ac09-228c81986e18
updated: 1484309126
title: "Thébault's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Thebault_1_2_3.svg.png
tags:
    - "Thébault's problem I"
    - "Thébault's problem II"
    - "Thébault's problem III"
categories:
    - Theorems in geometry
---
Thébault's theorem is the name given variously to one of the geometry problems proposed by the French mathematician Victor Thébault, individually known as Thébault's problem I, II, and III.
