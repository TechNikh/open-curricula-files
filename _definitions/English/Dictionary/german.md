---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/german
offline_file: ""
offline_thumbnail: ""
uuid: ae09b9c7-57a4-4aec-9cba-e5f3d004850c
updated: 1484310200
title: german
categories:
    - Dictionary
---
German
     adj 1: of or pertaining to or characteristic of Germany or its
            people or language; "German philosophers"; "German
            universities"; "German literature"
     2: of a more or less German nature; somewhat German; "Germanic
        peoples"; "his Germanic nature"; "formidable volumes
        Teutonic in their thoroughness" [syn: {Germanic}, {Teutonic}]
     n 1: a person of German nationality
     2: the standard German language; developed historically from
        West Germanic [syn: {High German}, {German language}]
