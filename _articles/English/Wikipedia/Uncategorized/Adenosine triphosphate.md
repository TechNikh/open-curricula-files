---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adenosine_triphosphate
offline_file: ""
offline_thumbnail: ""
uuid: 372b2a26-ef6d-4ed7-9ecc-bda38ec9756a
updated: 1484308372
title: Adenosine triphosphate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-MgATP2-small.gif
tags:
    - Physical and chemical properties
    - Ionization in biological systems
    - Biosynthesis
    - glycolysis
    - Glucose
    - Beta oxidation
    - Fermentation
    - Anaerobic respiration
    - ATP replenishment by nucleoside diphosphate kinases
    - ATP production during photosynthesis
    - ATP recycling
    - Regulation of biosynthesis
    - Functions in cells
    - Metabolism, synthesis, and active transport
    - Roles in cell structure and locomotion
    - Cell signalling
    - Extracellular signalling
    - Intracellular signaling
    - DNA and RNA synthesis
    - Amino acid activation in protein synthesis
    - Amino Acid Activation
    - Binding to proteins
    - ATP analogues
categories:
    - Uncategorized
---
Adenosine triphosphate (ATP) is a nucleoside triphosphate, a small molecule used in cells as a coenzyme. It is often referred to as the "molecular unit of currency" of intracellular energy transfer.[1]
