---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flat_spline
offline_file: ""
offline_thumbnail: ""
uuid: a38d5cca-5681-45b0-9bff-9b1d05897433
updated: 1484309324
title: Flat spline
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Spline_%2528PSF%2529.png'
categories:
    - Technical drawing
---
A spline, or the more modern term flexible curve, consists of a long strip fixed in position at a number of points that relaxes to form and hold a smooth curve passing through those points for the purpose of transferring that curve to another material.
