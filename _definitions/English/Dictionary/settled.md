---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/settled
offline_file: ""
offline_thumbnail: ""
uuid: 15258166-b429-43b3-8d78-6f92b9547937
updated: 1484310525
title: settled
categories:
    - Dictionary
---
settled
     adj 1: established or decided beyond dispute or doubt; "with
            details of the wedding settled she could now sleep at
            night" [ant: {unsettled}]
     2: established in a desired position or place; not moving
        about; "nomads...absorbed among the settled people";
        "settled areas"; "I don't feel entirely settled here";
        "the advent of settled civilization" [ant: {unsettled}]
     3: inhabited by colonists [syn: {colonized}, {colonised}]
     4: clearly defined; "I have no formed opinion about the chances
        of success" [syn: {defined}, {formed}]
     5: not changeable; "a period of settled weather"
