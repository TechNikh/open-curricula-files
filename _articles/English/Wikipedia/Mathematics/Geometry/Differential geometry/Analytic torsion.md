---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Analytic_torsion
offline_file: ""
offline_thumbnail: ""
uuid: 79e5754d-8a4a-4f34-a46c-4ecdb8364be8
updated: 1484309200
title: Analytic torsion
tags:
    - Definition of analytic torsion
    - Definition of Reidemeister torsion
    - A short history of Reidemeister torsion
    - Cheeger–Müller theorem
categories:
    - Differential geometry
---
In mathematics, Reidemeister torsion (or R-torsion, or Reidemeister–Franz torsion) is a topological invariant of manifolds introduced by Kurt Reidemeister (Reidemeister (1935)) for 3-manifolds and generalized to higher dimensions by Franz (1935) and de Rham (1936). Analytic torsion (or Ray–Singer torsion) is an invariant of Riemannian manifolds defined by Ray and Singer (1971, 1973a, 1973b) as an analytic analogue of Reidemeister torsion. Cheeger (1977, 1979) and Müller (1978) proved Ray and Singer's conjecture that Reidemeister torsion and analytic torsion are the same for compact Riemannian manifolds.
