---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/implication
offline_file: ""
offline_thumbnail: ""
uuid: ccb74164-444c-4101-8664-07f759923cd1
updated: 1484310599
title: implication
categories:
    - Dictionary
---
implication
     n 1: something that is inferred (deduced or entailed or implied);
          "his resignation had political implications" [syn: {deduction},
           {entailment}]
     2: a meaning that is not expressly stated but can be inferred;
        "the significance of his remark became clear only later";
        "the expectation was spread both by word and by
        implication" [syn: {significance}, {import}]
     3: an accusation that brings into intimate and usually
        incriminating connection
     4: a logical relation between propositions p and q of the form
        `if p then q'; if p is true then q cannot be false [syn: {logical
        implication}, {conditional ...
