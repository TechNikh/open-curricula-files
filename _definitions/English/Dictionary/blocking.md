---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blocking
offline_file: ""
offline_thumbnail: ""
uuid: f9ac0efb-5c92-4935-8fce-f5bd75f536d0
updated: 1484310160
title: blocking
categories:
    - Dictionary
---
blocking
     n : (American football) the act of obstructing someone's path
         with your body; "he threw a rolling block into the line
         backer" [syn: {block}, {interference}]
