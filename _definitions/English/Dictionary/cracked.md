---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cracked
offline_file: ""
offline_thumbnail: ""
uuid: 467ee0b1-7982-4b2b-ae22-0eba930388f5
updated: 1484310411
title: cracked
categories:
    - Dictionary
---
cracked
     adj 1: used of skin roughened as a result of cold or exposure;
            "chapped lips" [syn: {chapped}, {roughened}]
     2: broken without being divided into parts but having fissures
        appear on the surface; "a cracked mirror"
     3: of paint or varnish; having the appearance of alligator hide
        [syn: {alligatored}]
     4: informal or slang terms for mentally irregular; "it used to
        drive my husband balmy" [syn: {balmy}, {barmy}, {bats}, {batty},
         {bonkers}, {buggy}, {crackers}, {daft}, {dotty}, {fruity},
         {haywire}, {kooky}, {kookie}, {loco}, {loony}, {loopy}, {nuts},
         {nutty}, {round the bend}, {around the bend}, {wacky}, ...
