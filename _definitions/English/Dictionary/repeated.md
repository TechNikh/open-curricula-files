---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repeated
offline_file: ""
offline_thumbnail: ""
uuid: 7876457e-f1ab-4d41-9b0d-ad3397cba3a9
updated: 1484310214
title: repeated
categories:
    - Dictionary
---
repeated
     adj : recurring again and again; "perennial efforts to stipulate
           the requirements" [syn: {perennial}, {recurrent}]
