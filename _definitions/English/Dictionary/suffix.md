---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suffix
offline_file: ""
offline_thumbnail: ""
uuid: 06b42d5b-de40-46bb-b775-22bff5b8b5c8
updated: 1484310421
title: suffix
categories:
    - Dictionary
---
suffix
     n : an affix that is added at the end of the word [syn: {postfix}]
     v : attach a suffix to; "suffix words" [ant: {prefix}]
