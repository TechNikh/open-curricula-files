---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Evolving_networks
offline_file: ""
offline_thumbnail: ""
uuid: 0a5bc108-5179-4b96-9b31-44abe184215f
updated: 1484309434
title: Evolving networks
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/256px-Barabasi_Albert_model.gif
tags:
    - Network theory background
    - 'First evolving network model - scale free networks'
    - Additions to BA model
    - Fitness
    - Removing nodes and rewiring links
    - Other ways of characterizing evolving networks
    - Convergence towards Equilibria
    - >
        Treat evolving networks as successive snapshots of a static
        network
    - Define dynamic properties
    - Applications
categories:
    - Network theory
---
Evolving Networks are networks that change as a function of time. They are a natural extension of network science since almost all real world networks evolve over time, either by adding or removing nodes or links over time. Often all of these processes occur simultaneously, such as in social networks where people make and lose friends over time, thereby creating and destroying edges, and some people become part of new social networks or leave their networks, changing the nodes in the network. Evolving network concepts build on established network theory and are now being introduced into studying networks in many diverse fields.
