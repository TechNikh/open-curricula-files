---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electronic_entropy
offline_file: ""
offline_thumbnail: ""
uuid: 97160e3b-282b-43e6-96b1-7a03bebe5ba4
updated: 1484308842
title: Electronic entropy
tags:
    - Density of states-based electronic entropy
    - General formulation
    - Useful approximation
    - Application to different materials classes
    - Configurational electronic entropy
categories:
    - Thermodynamics
---
Electronic entropy is the entropy of a system attributable to electrons' probabilistic occupation of states. This entropy can take a number of forms. The first form can be termed a density-of-states based entropy. The Fermi–Dirac distribution implies that each eigenstate of a system, i, is occupied with a certain probability, pi. As the entropy is given by a sum over the probabilities of occupation of those states, there is an entropy associated with the occupation of the various electronic states. In most molecular systems, the energy spacing between the highest occupied molecular orbital and the lowest occupied molecular orbital is usually large, and thus the probabilities associated ...
