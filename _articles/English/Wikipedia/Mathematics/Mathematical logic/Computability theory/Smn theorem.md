---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Smn_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 6b8d8bf8-2418-41b9-abeb-4feaed48de07
updated: 1484309490
title: Smn theorem
tags:
    - Details
    - Example
categories:
    - Computability theory
---
In computability theory the smn theorem, (also called the translation lemma, parameter theorem, or parameterization theorem) is a basic result about programming languages (and, more generally, Gödel numberings of the computable functions) (Soare 1987, Rogers 1967). It was first proved by Stephen Cole Kleene (Kleene 1943).
