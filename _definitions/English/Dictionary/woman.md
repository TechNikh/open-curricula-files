---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/woman
offline_file: ""
offline_thumbnail: ""
uuid: 972975d4-bee9-4bb4-a822-1cce7f6dcf9d
updated: 1484310297
title: woman
categories:
    - Dictionary
---
woman
     n 1: an adult female person (as opposed to a man); "the woman
          kept house while the man hunted" [syn: {adult female}]
          [ant: {man}]
     2: women as a class; "it's an insult to American womanhood";
        "woman is the glory of creation" [syn: {womanhood}]
     3: a human female who does housework; "the char will clean the
        carpet" [syn: {charwoman}, {char}, {cleaning woman}, {cleaning
        lady}]
     4: a female person who plays a significant role (wife or
        mistress or girlfriend) in the life of a particular man;
        "he was faithful to his woman" [ant: {man}]
