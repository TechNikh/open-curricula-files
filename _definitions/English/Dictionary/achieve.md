---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/achieve
offline_file: ""
offline_thumbnail: ""
uuid: 5cc85bf3-5d28-4cf9-a8d1-e937d6cca5e4
updated: 1484310260
title: achieve
categories:
    - Dictionary
---
achieve
     v : to gain with effort; "she achieved her goal despite
         setbacks" [syn: {accomplish}, {attain}, {reach}]
