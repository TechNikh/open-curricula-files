---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enemy
offline_file: ""
offline_thumbnail: ""
uuid: ccf47665-cd51-4fcf-b853-484cde29e0d0
updated: 1484310567
title: enemy
categories:
    - Dictionary
---
enemy
     n 1: an opposing military force; "the enemy attacked at dawn"
     2: an armed adversary (especially a member of an opposing
        military force); "a soldier must be prepared to kill his
        enemies" [syn: {foe}, {foeman}, {opposition}]
     3: any hostile group of people; "he viewed lawyers as the real
        enemy"
     4: a personal enemy; "they had been political foes for years"
        [syn: {foe}] [ant: {ally}]
