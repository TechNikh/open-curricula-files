---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dufour_effect
offline_file: ""
offline_thumbnail: ""
uuid: 4e36325f-bb03-4822-9aa4-8ef48687747e
updated: 1484308838
title: Dufour effect
categories:
    - Thermodynamics
---
The Dufour effect is the energy flux due to a mass concentration gradient occurring as a coupled effect of irreversible processes. It is the reciprocal phenomenon to the Soret effect.[1] The concentration gradient results in a temperature change. For binary liquid mixtures, the Dufour effect is usually considered negligible, whereas in binary gas mixtures the effect can be significant.[2]
