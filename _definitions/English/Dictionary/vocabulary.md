---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vocabulary
offline_file: ""
offline_thumbnail: ""
uuid: 921fb30b-322a-4580-9d73-43b9e722a12c
updated: 1484310152
title: vocabulary
categories:
    - Dictionary
---
vocabulary
     n 1: a listing of the words used in some enterprise
     2: a language user's knowledge of words [syn: {lexicon}, {mental
        lexicon}]
     3: the system of techniques or symbols serving as a means of
        expression (as in arts or crafts); "he introduced a wide
        vocabulary of techniques"
