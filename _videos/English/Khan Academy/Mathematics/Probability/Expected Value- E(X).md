---
version: 1
type: video
provider: YouTube
id: j__Kredt7vY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Expected%20Value-%20E%28X%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 10dab487-b70a-437a-a2d9-527b1c63677e
updated: 1486069688
title: 'Expected Value- E(X)'
categories:
    - Probability
thumbnail_urls:
    - https://i3.ytimg.com/vi/j__Kredt7vY/default.jpg
    - https://i3.ytimg.com/vi/j__Kredt7vY/1.jpg
    - https://i3.ytimg.com/vi/j__Kredt7vY/2.jpg
    - https://i3.ytimg.com/vi/j__Kredt7vY/3.jpg
---
