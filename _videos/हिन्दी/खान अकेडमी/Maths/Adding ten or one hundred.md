---
version: 1
type: video
provider: YouTube
id: kg7-PsENFMk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a6ce21f3-8bd4-4a63-a648-91fe85972607
updated: 1486069613
title: Adding ten or one hundred
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/kg7-PsENFMk/default.jpg
    - https://i3.ytimg.com/vi/kg7-PsENFMk/1.jpg
    - https://i3.ytimg.com/vi/kg7-PsENFMk/2.jpg
    - https://i3.ytimg.com/vi/kg7-PsENFMk/3.jpg
---
