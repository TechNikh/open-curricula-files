---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trail
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484396281
title: trail
categories:
    - Dictionary
---
trail
     n 1: a track or mark left by something that has passed; "there as
          a trail of blood"; "a tear left its trail on her cheek"
     2: a path or track roughly blazed through wild or hilly country
     3: evidence pointing to a possible solution; "the police are
        following a promising lead"; "the trail led straight to
        the perpetrator" [syn: {lead}, {track}]
     v 1: to lag or linger behind; "But in so many other areas we
          still are dragging" [syn: {drag}, {get behind}, {hang
          back}, {drop behind}]
     2: go after with the intent to catch; "The policeman chased the
        mugger down the alley"; "the dog chased the rabbit" [syn:
        ...
