---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/veritable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484636161
title: veritable
categories:
    - Dictionary
---
veritable
     adj 1: often used as intensifiers; "a regular morass of details";
            "a regular nincompoop"; "he's a veritable swine" [syn:
             {regular(a)}, {veritable(a)}]
     2: not counterfeit or copied; "an authentic signature"; "a bona
        fide manuscript"; "an unquestionable antique";
        "photographs taken in a veritable bull ring" [syn: {authentic},
         {bona fide}, {unquestionable}]
