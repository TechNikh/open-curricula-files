---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Daniela_K%C3%BChn'
offline_file: ""
offline_thumbnail: ""
uuid: eeadad01-0a0d-4635-8af4-5212f677740e
updated: 1484309470
title: Daniela Kühn
tags:
    - Biography
    - Research
    - Awards and honours
categories:
    - Graph theorists
---
Daniela Kühn is a German mathematician and the Mason Professor in Mathematics at the University of Birmingham in Birmingham, England.[1] She is known for her research in combinatorics, and particularly in extremal combinatorics and graph theory.
