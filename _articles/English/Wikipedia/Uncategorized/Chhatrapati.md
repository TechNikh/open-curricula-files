---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chhatrapati
offline_file: ""
offline_thumbnail: ""
uuid: 657f75e7-c113-4e9f-8433-ffa24c18852f
updated: 1484308451
title: Chhatrapati
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Shivaji_Maharaj_Raigad.jpg
tags:
    - The Royal House of Bhosle
    - The Chhatrapatis of Satara
    - The Chhatrapatis of Kolhapur
    - Notes
categories:
    - Uncategorized
---
Chhatrapati (Devanagari: छत्रपती) is an Indian royal title equivalent to an Emperor used by the Marathas. The word ‘Chhatrapati’ is from Sanskrit chhatra (roof or umbrella) and pati (master/owner/ruler); Chhatrapati thus indicates a person who gives shelter to his followers and protects them.
