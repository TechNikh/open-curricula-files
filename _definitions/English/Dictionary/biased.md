---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biased
offline_file: ""
offline_thumbnail: ""
uuid: aaa715ae-a71c-45e1-be6d-914de316a00c
updated: 1484310471
title: biased
categories:
    - Dictionary
---
biased
     adj 1: favoring one person or side over another; "a biased account
            of the trial"; "a decision that was partial to the
            defendant" [syn: {colored}, {coloured}, {one-sided}, {slanted}]
     2: excessively devoted to one faction [syn: {one-sided}]
