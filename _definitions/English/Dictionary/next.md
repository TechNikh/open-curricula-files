---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/next
offline_file: ""
offline_thumbnail: ""
uuid: c4ce39a6-9eb8-45d4-816a-0ceed790997e
updated: 1484310313
title: next
categories:
    - Dictionary
---
next
     adj 1: nearest in space or position; immediately adjoining without
            intervening space; "had adjacent rooms"; "in the next
            room"; "the person sitting next to me"; "our rooms
            were side by side" [syn: {adjacent}, {side by side(p)}]
     2: (of elected officers) elected but not yet serving; "our next
        president" [syn: {future(a)}, {succeeding(a)}]
     3: immediately following in time or order; "the following day";
        "next in line"; "the next president"; "the next item on
        the list" [syn: {following}]
     adv : at the time or occasion immediately following; "next the
           doctor examined his back"
