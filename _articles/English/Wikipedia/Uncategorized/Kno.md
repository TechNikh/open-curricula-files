---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kno
offline_file: ""
offline_thumbnail: ""
uuid: be9aac49-e011-4ad9-9bd3-9396fa9e0338
updated: 1484308313
title: Kno
tags:
    - History
    - Products
categories:
    - Uncategorized
---
Kno, Inc. is a software company that works with publishers to offer digital textbooks and other educational materials. [1] In November 2013, after raising nearly $100 million in venture capital, the company was acquired by Intel.[2]
