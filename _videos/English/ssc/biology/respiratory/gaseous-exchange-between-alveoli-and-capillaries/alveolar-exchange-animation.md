---
title: Alveolar Exchange Animation
youtube_id: eX1IU_dvKuY
tags:
    - SSC
    - Biology
    - Respiration
    - Animation
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
