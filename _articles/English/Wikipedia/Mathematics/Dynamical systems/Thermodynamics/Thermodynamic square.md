---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermodynamic_square
offline_file: ""
offline_thumbnail: ""
uuid: 6760541c-fef8-406a-a7e5-8113d8dca8f8
updated: 1484308874
title: Thermodynamic square
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Thermodynamic_square.svg.png
categories:
    - Thermodynamics
---
The thermodynamic square (also known as the thermodynamic wheel, Guggenheim scheme or Born square) is a mnemonic diagram attributed to Max Born and used to help determine thermodynamic relations. Born presented the thermodynamic square in a 1929 lecture.[1] The symmetry of thermodynamics appears in a paper by F.O. Koenig.[2] The corners represent common conjugate variables while the sides represent thermodynamic potentials. The placement and relation among the variables serves as a key to recall the relations they constitute.
