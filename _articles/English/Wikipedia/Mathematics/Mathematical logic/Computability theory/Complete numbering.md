---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complete_numbering
offline_file: ""
offline_thumbnail: ""
uuid: 097e8173-5cbe-487b-8301-6e7435ab969d
updated: 1484309485
title: Complete numbering
categories:
    - Computability theory
---
In computability theory complete numberings are generalizations of Gödel numbering first introduced by A.I. Mal'tsev in 1963. They are studied because several important results like the Kleene's recursion theorem and Rice's theorem, which were originally proven for the Gödel-numbered set of computable functions, still hold for arbitrary sets with complete numberings.
