---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nichrome
offline_file: ""
offline_thumbnail: ""
uuid: a5d3e2ff-6376-4fbd-92e3-f2cf52427b27
updated: 1484308321
title: Nichrome
tags:
    - History
    - Uses
    - Properties
    - 'Table 1: Resistance per inch (Ω), closed helix, 80/20 alloy.'
    - 'Table 2: Current (A) vs. temperature characteristics, straight wire.'
    - 'Table 3: Cold resistance (Ω at 75°F) and wire gauge vs. power output (W) at operating voltage (V).'
    - Resistance of Nichrome Wire
    - Resistance of nichrome flat/strip and weight table
    - Additional properties
categories:
    - Uncategorized
---
Nichrome (NiCr, nickel-chrome, chrome-nickel, etc.) generally refers to any alloy of nickel, chromium, and often iron and/or other elements or substances. Nichrome alloys are typically used in resistance wire. They are also used in some dental restorations (fillings) and in other applications.
