---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/degenerate
offline_file: ""
offline_thumbnail: ""
uuid: b6d31ff1-9ba5-4cd2-9588-0c2d19b70ae9
updated: 1484310395
title: degenerate
categories:
    - Dictionary
---
degenerate
     adj : unrestrained by convention or morality; "Congreve draws a
           debauched aristocratic society"; "deplorably dissipated
           and degraded"; "riotous living"; "fast women" [syn: {debauched},
            {degraded}, {dissipated}, {dissolute}, {libertine}, {profligate},
            {riotous}, {fast}]
     n : a person whose behavior deviates from what is acceptable
         especially in sexual behavior [syn: {pervert}, {deviant},
          {deviate}]
     v : grow worse; "Her condition deteriorated"; "Conditions in the
         slums degenerated"; "The discussion devolved into a
         shouting match" [syn: {devolve}, {deteriorate}, {drop}]
         [ant: ...
