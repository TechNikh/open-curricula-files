---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiplicative_distance
offline_file: ""
offline_thumbnail: ""
uuid: 00c2504c-5f9c-469d-a582-295f7251aa74
updated: 1484309365
title: Multiplicative distance
categories:
    - Geometry stubs
---
In algebraic geometry, 
  
    
      
        μ
      
    
    {\displaystyle \mu }
  
 is said to be a multiplicative distance function over a field if it satisfies,[1]
