---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cardamom
offline_file: ""
offline_thumbnail: ""
uuid: 1a795021-661d-49cb-82c5-250413f1bed1
updated: 1484310437
title: cardamom
categories:
    - Dictionary
---
cardamom
     n 1: rhizomatous herb of India having aromatic seeds used as
          seasoning [syn: {cardamon}, {Elettaria cardamomum}]
     2: aromatic seeds used as seasoning like cinnamon and cloves
        especially in pickles and barbecue sauces [syn: {cardamon},
         {cardamum}]
