---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Five-dimensional_space
offline_file: ""
offline_thumbnail: ""
uuid: f5b6ddcf-ce30-4300-8fe0-d01e683b8786
updated: 1484309285
title: Five-dimensional space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Penteract_projected.png
tags:
    - Physics
    - Five-dimensional geometry
    - Polytopes
    - Hypersphere
    - Philosophy of Science
categories:
    - Multi-dimensional geometry
---
A five-dimensional space is a space with five dimensions. If interpreted physically, that is one more than the usual three spatial dimensions and the fourth dimension of time used in relativitistic physics.[1] It is an abstraction which occurs frequently in mathematics, where it is a legitimate construct. In physics and mathematics, a sequence of N numbers can be understood to represent a location in an N-dimensional space. Whether or not the universe is five-dimensional is a topic of debate.
