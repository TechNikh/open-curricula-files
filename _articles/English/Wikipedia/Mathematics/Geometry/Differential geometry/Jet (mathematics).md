---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jet_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 89729aef-71cc-4e4e-a8a7-23aefce9fb1e
updated: 1484309223
title: Jet (mathematics)
tags:
    - Jets of functions between Euclidean spaces
    - One-dimensional case
    - Mappings from one Euclidean space to another
    - Algebraic properties of jets
    - 'Jets at a point in Euclidean space: rigorous definitions'
    - Analytic definition
    - Algebraic-geometric definition
    - "Taylor's theorem"
    - Jet spaces from a point to a point
    - Jets of functions between two manifolds
    - Jets of functions from the real line to a manifold
    - Jets of functions from a manifold to a manifold
    - Multijets
    - Jets of sections
    - Differential operators between vector bundles
categories:
    - Differential geometry
---
In mathematics, the jet is an operation that takes a differentiable function f and produces a polynomial, the truncated Taylor polynomial of f, at each point of its domain. Although this is the definition of a jet, the theory of jets regards these polynomials as being abstract polynomials rather than polynomial functions.
