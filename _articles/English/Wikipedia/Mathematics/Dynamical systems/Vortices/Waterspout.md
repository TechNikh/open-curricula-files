---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Waterspout
offline_file: ""
offline_thumbnail: ""
uuid: 6326c7ea-5333-4e09-bf41-81ce986658d4
updated: 1484308881
title: Waterspout
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Drinking_water_0.jpg
tags:
    - Formation
    - Types
    - Non-tornadic
    - Tornadic
    - Snowspout
    - Climatology
    - Life cycle
    - Nautical threat
    - Threat to Marine Animals
    - Research and forecasting
    - Szilagyi Waterspout Index (SWI)
    - International Centre for Waterspout Research (ICWR)
categories:
    - Vortices
---
A waterspout is an intense columnar vortex (usually appearing as a funnel-shaped cloud) that occurs over a body of water. They are connected to a towering cumuliform cloud or a cumulonimbus cloud.[1] In the common form, it is a non-supercell tornado over water.[1][2][3]
