---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insoluble
offline_file: ""
offline_thumbnail: ""
uuid: ab20b726-0892-43cb-ba1f-bdc6a0376280
updated: 1484310371
title: insoluble
categories:
    - Dictionary
---
insoluble
     adj 1: (of a substance) not easily dissolved [ant: {soluble}]
     2: admitting of no solution or explanation; "an insoluble
        doubt" [ant: {soluble}]
     3: without hope of solution; "an insoluble problem"
