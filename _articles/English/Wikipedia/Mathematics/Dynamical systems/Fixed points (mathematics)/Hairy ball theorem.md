---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hairy_ball_theorem
offline_file: ""
offline_thumbnail: ""
uuid: cc948840-07ef-43e4-a9bb-e2fa0b494321
updated: 1484308791
title: Hairy ball theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hairy_ball.png
tags:
    - Counting zeros
    - Cyclone consequences
    - Application to computer graphics
    - Lefschetz connection
    - Corollary
    - Higher dimensions
    - Notes
categories:
    - Fixed points (mathematics)
---
The hairy ball theorem of algebraic topology states that there is no nonvanishing continuous tangent vector field on even-dimensional n-spheres. For the ordinary sphere, or 2‑sphere, if f is a continuous function that assigns a vector in R3 to every point p on a sphere such that f(p) is always tangent to the sphere at p, then there is at least one p such that f(p) = 0. In other words, whenever one attempts to comb a hairy ball flat, there will always be at least one tuft of hair at one point on the ball. The theorem was first stated by Henri Poincaré in the late 19th century.[citation needed]
