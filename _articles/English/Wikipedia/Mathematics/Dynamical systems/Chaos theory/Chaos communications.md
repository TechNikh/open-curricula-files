---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaos_communications
offline_file: ""
offline_thumbnail: ""
uuid: 1bd2d974-4388-4fcb-bbdb-b67453f9d350
updated: 1484308705
title: Chaos communications
categories:
    - Chaos theory
---
Chaos communications is an application of chaos theory which is aimed to provide security in the transmission of information performed through telecommunications technologies. By secure communications, one has to understand that the contents of the message transmitted are inaccessible to possible eavesdroppers.
