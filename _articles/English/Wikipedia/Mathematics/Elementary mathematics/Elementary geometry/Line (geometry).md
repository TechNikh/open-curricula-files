---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Line_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 6ce53fc2-4b3f-4d6e-ad83-763e2d3ca646
updated: 1484308932
title: Line (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-FuncionLineal01.svg.png
tags:
    - Definitions versus descriptions
    - Ray
    - Euclidean geometry
    - Cartesian plane
    - Normal form
    - Polar coordinates
    - Vector equation
    - Euclidean space
    - Collinear points
    - Types of lines
    - Projective geometry
    - Geodesics
    - Notes
categories:
    - Elementary geometry
---
The notion of line or straight line was introduced by ancient mathematicians to represent straight objects (i.e., having no curvature) with negligible width and depth. Lines are an idealization of such objects. Until the 17th century, lines were defined in this manner: "The [straight or curved] line is the first species of quantity, which has only one dimension, namely length, without any width nor depth, and is nothing else than the flow or run of the point which […] will leave from its imaginary moving some vestige in length, exempt of any width. […] The straight line is that which is equally extended between its points."[1]
