---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/doomed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484467321
title: doomed
categories:
    - Dictionary
---
doomed
     adj 1: marked for certain death; "the black spot told the old
            sailor he was doomed"
     2: in danger of the eternal punishment of hell; "poor damned
        souls" [syn: {cursed}, {damned}, {unredeemed}, {unsaved}]
     3: marked by or promising bad fortune; "their business venture
        was doomed from the start"; "an ill-fated business
        venture"; "an ill-starred romance"; "the unlucky prisoner
        was again put in irons"- W.H.Prescott [syn: {ill-fated}, {ill-omened},
         {ill-starred}, {unlucky}]
     4: (usually followed by `to') determined by tragic fate;
        "doomed to unhappiness"; "fated to be the scene of
        Kennedy's ...
