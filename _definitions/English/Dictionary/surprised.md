---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surprised
offline_file: ""
offline_thumbnail: ""
uuid: baed0309-59ea-4d1f-95a3-81761d7b61ac
updated: 1484310313
title: surprised
categories:
    - Dictionary
---
surprised
     adj : taken unawares or suddenly and feeling wonder or
           astonishment; "surprised by her student's ingenuity";
           "surprised that he remembered my name"; "a surprised
           expression" [ant: {unsurprised}]
