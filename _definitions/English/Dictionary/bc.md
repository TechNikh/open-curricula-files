---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bc
offline_file: ""
offline_thumbnail: ""
uuid: 1470c100-53d4-4976-b60a-77177078b516
updated: 1484310305
title: bc
categories:
    - Dictionary
---
B.C.
     adv : before the Christian era; used following dates before the
           supposed year Christ was born; "in 200 BC" [syn: {BC},
           {before Christ}]
