---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ray%E2%80%93Dutt_twist'
offline_file: ""
offline_thumbnail: ""
uuid: d53805b0-543f-402d-93d8-ea20d7d25294
updated: 1484309280
title: Ray–Dutt twist
categories:
    - Molecular geometry
---
The Ray–Dutt twist is a mechanism proposed for the racemization of octahedral complexes containing three bidentate chelate rings. Such complexes typically adopt an octahedral molecular geometry in their ground states and are thus chiral. The pathway entails formation of an intermediate of C2v point group symmetry.[1] An alternative pathway that also does not break any metal-ligand bonds is called the Bailar twist. Both of these mechanism product complexes wherein the ligating atoms (X in the scheme) are arranged in an approximate trigonal prism. This pathway is called the Ray–Dutt twist in honor of the inorganic chemists who proposed this process.[2]
