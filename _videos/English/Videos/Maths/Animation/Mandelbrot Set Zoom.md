---
version: 1
type: video
provider: YouTube
id: gEw8xpb1aRA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d9c1e44d-ffe1-48ec-9b2d-9b2880042395
updated: 1486069644
title: Mandelbrot Set Zoom
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/gEw8xpb1aRA/default.jpg
    - https://i3.ytimg.com/vi/gEw8xpb1aRA/1.jpg
    - https://i3.ytimg.com/vi/gEw8xpb1aRA/2.jpg
    - https://i3.ytimg.com/vi/gEw8xpb1aRA/3.jpg
---
