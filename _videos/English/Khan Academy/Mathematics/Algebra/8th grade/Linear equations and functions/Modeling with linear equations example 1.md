---
version: 1
type: video
provider: YouTube
id: qPx7i1jwXX4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/8th%20grade/Linear%20equations%20and%20functions/Modeling%20with%20linear%20equations%20example%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 38f7192e-9ce4-4200-a61a-61cc09ea81e8
updated: 1486069565
title: Modeling with linear equations example 1
categories:
    - Linear equations and functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/qPx7i1jwXX4/default.jpg
    - https://i3.ytimg.com/vi/qPx7i1jwXX4/1.jpg
    - https://i3.ytimg.com/vi/qPx7i1jwXX4/2.jpg
    - https://i3.ytimg.com/vi/qPx7i1jwXX4/3.jpg
---
