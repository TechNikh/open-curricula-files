---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/veto
offline_file: ""
offline_thumbnail: ""
uuid: 0ec5a4e1-e86b-49b0-a3e6-8c64e89f31a4
updated: 1484310181
title: veto
categories:
    - Dictionary
---
veto
     n 1: a vote that blocks a decision
     2: the power or right to prohibit or reject a proposed or
        intended act (especially the power of a chief executive to
        reject a bill passed by the legislature)
     v 1: vote against; refuse to endorse; refuse to assent; "The
          President vetoed the bill" [syn: {blackball}, {negative}]
     2: command against; "I forbid you to call me late at night";
        "Mother vetoed the trip to the chocolate store" [syn: {forbid},
         {prohibit}, {interdict}, {proscribe}, {disallow}] [ant: {permit},
         {permit}]
     [also: {vetoes} (pl)]
