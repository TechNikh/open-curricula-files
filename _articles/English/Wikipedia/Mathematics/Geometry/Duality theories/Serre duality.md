---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Serre_duality
offline_file: ""
offline_thumbnail: ""
uuid: ea87dbb3-e963-463c-bc71-17b4bcb5786b
updated: 1484309239
title: Serre duality
categories:
    - Duality theories
---
In algebraic geometry, a branch of mathematics, Serre duality is a duality present on non-singular projective algebraic varieties V of dimension n (and in greater generality for vector bundles and further, for coherent sheaves). It shows that a cohomology group Hi is the dual space of another one, Hn−i.
