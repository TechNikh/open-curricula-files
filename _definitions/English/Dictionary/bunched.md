---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bunched
offline_file: ""
offline_thumbnail: ""
uuid: d46a9326-c1ea-484d-b053-fb074f69df94
updated: 1484310305
title: bunched
categories:
    - Dictionary
---
bunched
     adj : occurring close together in bunches or clusters [syn: {bunchy},
            {clustered}]
