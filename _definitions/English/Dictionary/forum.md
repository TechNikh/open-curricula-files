---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forum
offline_file: ""
offline_thumbnail: ""
uuid: 35cd0dd4-d6ab-488e-90f1-eecb1704d073
updated: 1484310181
title: forum
categories:
    - Dictionary
---
forum
     n 1: a public meeting or assembly for open discussion
     2: a public facility to meet for open discussion [syn: {assembly},
         {meeting place}]
     3: a place of assembly for the people in ancient Greece [syn: {agora},
         {public square}]
     [also: {fora} (pl)]
