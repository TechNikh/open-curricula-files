---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eventually
offline_file: ""
offline_thumbnail: ""
uuid: c718dd96-386a-4a51-b3ce-1cd72dc05b90
updated: 1484310281
title: eventually
categories:
    - Dictionary
---
eventually
     adv 1: within an indefinite time or at an unspecified future time;
            "he will understand eventually"; "he longed for the
            flowers that were yet to show themselves"; "sooner or
            later you will have to face the facts"; "in time they
            came to accept the harsh reality" [syn: {yet}, {sooner
            or later}, {in time}, {one of these days}]
     2: after a long period of time or an especially long delay; "at
        length they arrived" [syn: {finally}, {at length}]
