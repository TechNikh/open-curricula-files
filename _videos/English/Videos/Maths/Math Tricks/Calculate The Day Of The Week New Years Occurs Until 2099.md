---
version: 1
type: video
provider: YouTube
id: 7iZ1TlqcqVc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fd3b68e2-4184-4aa2-b848-91977e4c5f18
updated: 1486069623
title: Calculate The Day Of The Week New Years Occurs Until 2099
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/7iZ1TlqcqVc/default.jpg
    - https://i3.ytimg.com/vi/7iZ1TlqcqVc/1.jpg
    - https://i3.ytimg.com/vi/7iZ1TlqcqVc/2.jpg
    - https://i3.ytimg.com/vi/7iZ1TlqcqVc/3.jpg
---
