---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moral_graph
offline_file: ""
offline_thumbnail: ""
uuid: 964702bb-f62e-46d7-adcd-94ad80ac832e
updated: 1484309450
title: Moral graph
categories:
    - Graph operations
---
A moral graph is a concept in graph theory, used to find the equivalent undirected form of a directed acyclic graph. It is a key step of the junction tree algorithm, used in belief propagation on graphical models.
