---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electrodynamic_suspension
offline_file: ""
offline_thumbnail: ""
uuid: 02d55ef4-c54b-4833-be8c-ab79920eadf2
updated: 1484308751
title: Electrodynamic suspension
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Induction_levitation_cropped.jpg
tags:
    - Types
    - Bedford levitator
    - Levitation melting
    - Linear induction motor
    - Null flux
    - Inductrack
    - Electrodynamic bearing
    - Uses
    - Trains
    - Principles
    - Stability
    - Static
    - Dynamic
categories:
    - Electrodynamics
---
Electrodynamic suspension (EDS) is a form of magnetic levitation in which there are conductors which are exposed to time-varying magnetic fields. This induces eddy currents in the conductors that creates a repulsive magnetic field which holds the two objects apart.
