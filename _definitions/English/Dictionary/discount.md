---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discount
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484514781
title: discount
categories:
    - Dictionary
---
discount
     n 1: the act of reducing the selling price of merchandise [syn: {price
          reduction}, {deduction}]
     2: interest on an annual basis deducted in advance on a loan
        [syn: {discount rate}, {bank discount}]
     3: a refund of some fraction of the amount paid [syn: {rebate}]
     4: an amount or percentage deducted [syn: {deduction}]
     v 1: bar from attention or consideration; "She dismissed his
          advances" [syn: {dismiss}, {disregard}, {brush aside}, {brush
          off}, {push aside}, {ignore}]
     2: give a reduction in price on; "I never discount these
        books-they sell like hot cakes"
