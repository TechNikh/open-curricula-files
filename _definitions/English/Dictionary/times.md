---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/times
offline_file: ""
offline_thumbnail: ""
uuid: 5fa37e30-b90f-44c5-8c7d-60d9f3d69520
updated: 1484310256
title: times
categories:
    - Dictionary
---
times
     n 1: the circumstances and ideas of the present age; "behind the
          times"; "in times like these" [syn: {modern times}, {present
          time}, {modern world}, {contemporary world}]
     2: an arithmetic operation that is the inverse of division; the
        product of two numbers is computed; "the multiplication of
        four by three gives twelve"; "four times three equals
        twelve" [syn: {multiplication}]
