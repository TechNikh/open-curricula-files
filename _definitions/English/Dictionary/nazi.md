---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nazi
offline_file: ""
offline_thumbnail: ""
uuid: 9e857865-50d2-4af8-b3ad-8690fc875e65
updated: 1484310551
title: nazi
categories:
    - Dictionary
---
Nazi
     adj 1: relating to or consistent with or typical of the ideology
            and practice of Nazism or the Nazis; "the total Nazi
            crime"; "the Nazi interpretation of history"
     2: relating to a form of socialism; "the national socialist
        party came to power in 1933" [syn: {national socialist}]
     n : a German member of Adolf Hitler's political party [syn: {German
         Nazi}]
