---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hous
offline_file: ""
offline_thumbnail: ""
uuid: 10adbdc5-b494-43e9-b262-3378e6844a16
updated: 1484308440
title: House
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Vasskertentrance.jpg
tags:
    - Etymology
    - Elements
    - Layout
    - Parts
    - History of the interior
    - Communal rooms
    - Interconnecting rooms
    - Corridor
    - Employment-free house
    - Technology and privacy
    - Construction
    - Energy efficiency
    - Earthquake protection
    - Found materials
    - Legal issues
    - United Kingdom
    - Identifying houses
    - Animal houses
    - Houses and symbolism
categories:
    - Uncategorized
---
A house is a building that functions as a home, ranging from simple dwellings such as rudimentary huts of nomadic tribes and the improvised shacks in shantytowns to complex, fixed structures of wood, brick, concrete or other materials containing plumbing, ventilation and electrical systems.[1][2] Houses use a range of different roofing systems to keep precipitation such as rain from getting into the dwelling space. Houses may have doors or locks to secure the dwelling space and protect its inhabitants and contents from burglars or other trespassers. Most conventional modern houses in Western cultures will contain one or more bedrooms and bathrooms, a kitchen or cooking area, and a living ...
