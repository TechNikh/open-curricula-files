---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unionist
offline_file: ""
offline_thumbnail: ""
uuid: 22572b0a-b49f-40e7-a758-be04a2165507
updated: 1484310589
title: unionist
categories:
    - Dictionary
---
unionist
     n : a worker who belongs to a trade union [syn: {trade unionist},
          {union member}]
