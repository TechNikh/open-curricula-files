---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Collinearity
offline_file: ""
offline_thumbnail: ""
uuid: b8694bab-a15f-4e25-8504-27a8cec840cb
updated: 1484309253
title: Collinearity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/130px-Antenna-tower-collinear-et-al.jpg
tags:
    - Points on a line
    - Examples in Euclidean geometry
    - Triangles
    - Quadrilaterals
    - Hexagons
    - Conic sections
    - Cones
    - Tetrahedrons
    - Algebra
    - Collinearity of points whose coordinates are given
    - Collinearity of points whose pairwise distances are given
    - Number theory
    - Concurrency (plane dual)
    - Collinearity graph
    - Usage in statistics and econometrics
    - Usage in other areas
    - Antenna arrays
    - Photography
    - Notes
categories:
    - Incidence geometry
---
In geometry, collinearity of a set of points is the property of their lying on a single line.[1] A set of points with this property is said to be collinear (sometimes spelled as colinear[2]). In greater generality, the term has been used for aligned objects, that is, things being "in a line" or "in a row".
