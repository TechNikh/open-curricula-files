---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Automorphism
offline_file: ""
offline_thumbnail: ""
uuid: 0a4ccb26-f3c2-4a3c-8211-f05254a61780
updated: 1484309304
title: Automorphism
tags:
    - Definition
    - Automorphism group
    - Examples
    - History
    - Inner and outer automorphisms
categories:
    - Symmetry
---
In mathematics, an automorphism is an isomorphism from a mathematical object to itself. It is, in some sense, a symmetry of the object, and a way of mapping the object to itself while preserving all of its structure. The set of all automorphisms of an object forms a group, called the automorphism group. It is, loosely speaking, the symmetry group of the object.
