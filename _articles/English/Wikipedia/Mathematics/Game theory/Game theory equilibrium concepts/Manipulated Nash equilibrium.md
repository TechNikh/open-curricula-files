---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Manipulated_Nash_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: f3644755-1f28-4989-9636-524ef5920525
updated: 1484309029
title: Manipulated Nash equilibrium
categories:
    - Game theory equilibrium concepts
---
In game theory, a Manipulated Nash equilibrium or MAPNASH is a refinement of subgame perfect equilibrium used in dynamic games of imperfect information. Informally, a strategy set is a MAPNASH of a game if it would be a subgame perfect equilibrium of the game if the game had perfect information. MAPNASH were first suggested by Amershi, Sadanand, and Sadanand (1988) and has been discussed in several papers since. It is a solution concept based on how players think about other players' thought processes.
