---
version: 1
type: video
provider: YouTube
id: 66ziUq6vRko
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Example%20of%20Finding%20Reactant%20Empirical%20Formula.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 16d54e70-becd-493e-b2bf-7e308d0b29a4
updated: 1486069589
title: Example of Finding Reactant Empirical Formula
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/66ziUq6vRko/default.jpg
    - https://i3.ytimg.com/vi/66ziUq6vRko/1.jpg
    - https://i3.ytimg.com/vi/66ziUq6vRko/2.jpg
    - https://i3.ytimg.com/vi/66ziUq6vRko/3.jpg
---
Example of Finding Reactant Empirical Formula
More free lessons at: http://www.khanacademy.org/video?v=66ziUq6vRko
