---
version: 1
type: video
provider: YouTube
id: GJJGNOdhP8w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Western%20Blotting%20Animation%20-%20Part%20I.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 67b93376-9348-4ad8-ad7e-6a4f467f528a
updated: 1486069670
title: 'Western Blotting Animation - Part I'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/GJJGNOdhP8w/default.jpg
    - https://i3.ytimg.com/vi/GJJGNOdhP8w/1.jpg
    - https://i3.ytimg.com/vi/GJJGNOdhP8w/2.jpg
    - https://i3.ytimg.com/vi/GJJGNOdhP8w/3.jpg
---
