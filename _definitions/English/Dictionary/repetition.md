---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repetition
offline_file: ""
offline_thumbnail: ""
uuid: 32783376-63be-4946-b51b-ec31a80a9907
updated: 1484310395
title: repetition
categories:
    - Dictionary
---
repetition
     n 1: an event that repeats; "the events today were a repeat of
          yesterday's" [syn: {repeat}]
     2: the act of doing or performing again [syn: {repeating}]
     3: the repeated use of the same word or word pattern as a
        rhetorical device
