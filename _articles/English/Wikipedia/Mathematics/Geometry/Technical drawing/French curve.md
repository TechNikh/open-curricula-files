---
version: 1
type: article
id: https://en.wikipedia.org/wiki/French_curve
offline_file: ""
offline_thumbnail: ""
uuid: 4026cb37-8b9e-41a5-81e8-fa4dbac80754
updated: 1484309324
title: French curve
tags:
    - Modern successors
categories:
    - Technical drawing
---
A French curve is a template usually made from metal, wood or plastic composed of many different curves. It is used in manual drafting to draw smooth curves of varying radii. The shapes are segments of the Euler spiral or clothoid curve. The curve is placed on the drawing material, and a pencil, knife or other implement is traced around its curves to produce the desired result.
