---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amusement
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484454901
title: amusement
categories:
    - Dictionary
---
amusement
     n 1: a feeling of delight at being entertained
     2: a diversion that holds the attention [syn: {entertainment}]
