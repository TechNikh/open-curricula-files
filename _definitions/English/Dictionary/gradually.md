---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gradually
offline_file: ""
offline_thumbnail: ""
uuid: c2c9d1d6-4a64-4c8b-b813-499e76a49922
updated: 1484310289
title: gradually
categories:
    - Dictionary
---
gradually
     adv : in a gradual manner; "the snake moved gradually toward its
           victim" [syn: {bit by bit}, {step by step}]
