---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hexacoordinate
offline_file: ""
offline_thumbnail: ""
uuid: 6ba172bd-5a21-456e-b417-3da6a3390d2b
updated: 1484309277
title: Hexacoordinate
categories:
    - Molecular geometry
---
Hexacoordinate in chemistry is a molecule with six ligands or atomic attachments arranged around a single metal atom in the centre. Most hexacoordinate species form an octahedral molecular geometry, with four ligands arranged equatorially, and two axially.
