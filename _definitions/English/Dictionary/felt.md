---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/felt
offline_file: ""
offline_thumbnail: ""
uuid: 9367898b-9b0c-4b5d-8bd9-986ec7422121
updated: 1484310349
title: felt
categories:
    - Dictionary
---
feel
     n 1: an intuitive awareness; "he has a feel for animals" or "it's
          easy when you get the feel of it";
     2: the general atmosphere of a place or situation and the
        effect that it has on people; "the feel of the city
        excited him"; "a clergyman improved the tone of the
        meeting"; "it had the smell of treason" [syn: {spirit}, {tone},
         {feeling}, {flavor}, {flavour}, {look}, {smell}]
     3: a property perceived by touch [syn: {tactile property}]
     4: manual-genital stimulation for sexual pleasure; "the girls
        hated it when he tried to sneak a feel"
     v 1: undergo an emotional sensation; "She felt resentful"; "He
          felt ...
