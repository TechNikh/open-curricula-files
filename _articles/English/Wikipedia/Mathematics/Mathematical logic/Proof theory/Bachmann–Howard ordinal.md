---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bachmann%E2%80%93Howard_ordinal'
offline_file: ""
offline_thumbnail: ""
uuid: 8c7be808-eabe-48c3-a513-38a70eda37eb
updated: 1484309492
title: Bachmann–Howard ordinal
categories:
    - Proof theory
---
In mathematics, the Bachmann–Howard ordinal (or Howard ordinal) is a large countable ordinal. It is the proof theoretic ordinal of several mathematical theories, such as Kripke–Platek set theory (with the axiom of infinity) and the system CZF of constructive set theory. It was introduced by Heinz Bachmann (1950) and William Alvin Howard (1972).
