---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strove
offline_file: ""
offline_thumbnail: ""
uuid: b1f27516-42b8-43dc-97df-e7c92f9914c5
updated: 1484310549
title: strove
categories:
    - Dictionary
---
strive
     v 1: attempt by employing effort; "we endeavor to make our
          customers happy" [syn: {endeavor}, {endeavour}]
     2: to exert much effort or energy; "straining our ears to hear"
        [syn: {reach}, {strain}]
     [also: {strove}, {striven}]
