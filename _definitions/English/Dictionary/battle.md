---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/battle
offline_file: ""
offline_thumbnail: ""
uuid: b861cd63-2e12-45c3-8434-11f58f535a2d
updated: 1484310477
title: battle
categories:
    - Dictionary
---
battle
     n 1: a hostile meeting of opposing military forces in the course
          of a war; "Grant won a decisive victory in the battle of
          Chickamauga"; "he lost his romantic ideas about war when
          he got into a real engagement" [syn: {conflict}, {fight},
           {engagement}]
     2: an energetic attempt to achieve something; "getting through
        the crowd was a real struggle"; "he fought a battle for
        recognition" [syn: {struggle}]
     3: an open clash between two opposing groups (or individuals);
        "the harder the conflict the more glorious the
        triumph"--Thomas Paine; "police tried to control the
        battle between the pro- and ...
