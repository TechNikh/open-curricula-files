---
version: 1
type: video
provider: YouTube
id: PL9UYj2awDc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/A%20clever%20application%20of%20the%20distributive%20property%20to%20solve%20a%20multi-step%20equation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bd7ec090-3301-4230-ba13-71b15ffbd900
updated: 1486069565
title: >
    A clever application of the distributive property to solve a
    multi-step equation
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/PL9UYj2awDc/default.jpg
    - https://i3.ytimg.com/vi/PL9UYj2awDc/1.jpg
    - https://i3.ytimg.com/vi/PL9UYj2awDc/2.jpg
    - https://i3.ytimg.com/vi/PL9UYj2awDc/3.jpg
---
