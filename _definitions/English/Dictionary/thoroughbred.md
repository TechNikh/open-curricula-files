---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thoroughbred
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484470501
title: thoroughbred
categories:
    - Dictionary
---
thoroughbred
     adj : having a list of ancestors as proof of being a purebred
           animal [syn: {pedigree(a)}, {pedigreed}, {pureblood}, {pureblooded}]
     n 1: a well-bred person [ant: {mixed-blood}]
     2: a racehorse belonging to a breed that originated from a
        cross between Arabian stallions and English mares
     3: a pedigreed animal of unmixed lineage; used especially of
        horses [syn: {purebred}, {pureblood}]
