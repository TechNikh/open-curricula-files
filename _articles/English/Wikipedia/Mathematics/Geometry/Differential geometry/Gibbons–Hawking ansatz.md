---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gibbons%E2%80%93Hawking_ansatz'
offline_file: ""
offline_thumbnail: ""
uuid: 11678a04-ad5b-432f-b6ae-386a0b5313eb
updated: 1484309224
title: Gibbons–Hawking ansatz
categories:
    - Differential geometry
---
In mathematics, the Gibbons–Hawking ansatz is a method of constructing gravitational instantons introduced by Gary Gibbons and Stephen Hawking (1978, 1979). It gives examples of hyperkähler manifolds in dimension 4 that are invariant under a circle action.
