---
version: 1
type: video
provider: YouTube
id: cepY-Qoc5pw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f8138031-1967-43fd-be57-409e0ee63990
updated: 1486069673
title: RNA Protein Synthesis
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/cepY-Qoc5pw/default.jpg
    - https://i3.ytimg.com/vi/cepY-Qoc5pw/1.jpg
    - https://i3.ytimg.com/vi/cepY-Qoc5pw/2.jpg
    - https://i3.ytimg.com/vi/cepY-Qoc5pw/3.jpg
---
