---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Superstatistics
offline_file: ""
offline_thumbnail: ""
uuid: d1958ada-735a-4ca0-b936-3f84391f8d65
updated: 1484308825
title: Superstatistics
categories:
    - Nonlinear systems
---
Superstatistics[1][2] is a branch of statistical mechanics or statistical physics devoted to the study of non-linear and non-equilibrium systems. It is characterized by using the superposition of multiple differing statistical models to achieve the desired non-linearity. In terms of ordinary statistical ideas, this is equivalent to compounding the distributions of random variables and it may be considered a simple case of a doubly stochastic model.
