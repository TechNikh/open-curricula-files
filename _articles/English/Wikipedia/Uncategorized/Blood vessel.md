---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Blood_vessels
offline_file: ""
offline_thumbnail: ""
uuid: ca76df45-8e8f-47f5-b246-64155f86319f
updated: 1484308368
title: Blood vessel
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Circulatory_System_en.svg_0.png
tags:
    - Structure
    - Types
    - Physiology
    - Factors affecting blood flow resistance
    - Disease
categories:
    - Uncategorized
---
The blood vessels are the part of the circulatory system that transports blood throughout the human body. There are three major types of blood vessels: the arteries, which carry the blood away from the heart; the capillaries, which enable the actual exchange of water and chemicals between the blood and the tissues; and the veins, which carry blood from the capillaries back toward the heart. The word vascular, meaning relating to the blood vessels, is derived from the Latin vas, meaning vessel. A few structures (such as cartilage and the lens of the eye) do not contain blood vessels and are labeled.
