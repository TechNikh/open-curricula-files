---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/allegedly
offline_file: ""
offline_thumbnail: ""
uuid: 4f9c863e-42dd-439d-911e-517a3aab20ed
updated: 1484310180
title: allegedly
categories:
    - Dictionary
---
allegedly
     adv : according to what has been alleged; "he was on trial for
           allegedly murdering his wife"
