---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Principal_axis_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 0ac7d458-d3c9-4fdd-a24c-f6351a505c57
updated: 1484309123
title: Principal axis theorem
tags:
    - Motivation
    - Formal statement
categories:
    - Theorems in geometry
---
In the mathematical fields of geometry and linear algebra, a principal axis is a certain line in a Euclidean space associated with an ellipsoid or hyperboloid, generalizing the major and minor axes of an ellipse or hyperbola. The principal axis theorem states that the principal axes are perpendicular, and gives a constructive procedure for finding them.
