---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magnetic_damping
offline_file: ""
offline_thumbnail: ""
uuid: 3f4fdc73-c2e9-4cf1-96f7-98a9289310ec
updated: 1484308757
title: Magnetic damping
tags:
    - Definition
    - Equation
    - Uses
    - Observing magnetic damping
    - Dangers
    - Availability
categories:
    - Electrodynamics
---
