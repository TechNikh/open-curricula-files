---
version: 1
type: video
provider: YouTube
id: rEK9UPWT9cY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bfc0b02f-80d6-41ba-a4fd-1e75053eb273
updated: 1486069636
title: Watch an experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/rEK9UPWT9cY/default.jpg
    - https://i3.ytimg.com/vi/rEK9UPWT9cY/1.jpg
    - https://i3.ytimg.com/vi/rEK9UPWT9cY/2.jpg
    - https://i3.ytimg.com/vi/rEK9UPWT9cY/3.jpg
---
