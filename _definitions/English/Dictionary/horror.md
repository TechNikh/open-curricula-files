---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/horror
offline_file: ""
offline_thumbnail: ""
uuid: e28f553a-5189-4e7a-a69e-5853bed1aefe
updated: 1484310587
title: horror
categories:
    - Dictionary
---
horror
     n 1: intense and profound fear
     2: something that inspires horror; something horrible; "the
        painting that others found so beautiful was a horror to
        him"
     3: intense aversion [syn: {repugnance}, {repulsion}, {revulsion}]
