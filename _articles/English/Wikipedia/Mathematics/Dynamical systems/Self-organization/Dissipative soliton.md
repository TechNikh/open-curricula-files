---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dissipative_soliton
offline_file: ""
offline_thumbnail: ""
uuid: 932dd44f-e310-4804-aaf4-c69fdf5b4a21
updated: 1484308814
title: Dissipative soliton
tags:
    - Historical development
    - Origin of the soliton concept
    - Weakly and strongly dissipative systems
    - Experimental observations of DSs
    - Theoretical description of DSs
    - Particle properties and universality
    - Inline
    - Books and overview articles
categories:
    - Self-organization
---
Dissipative solitons (DSs) are stable solitary localized structures that arise in nonlinear spatially extended dissipative systems due to mechanisms of self-organization. They can be considered as an extension of the classical soliton concept in conservative systems. An alternative terminology includes autosolitons, spots and pulses.
