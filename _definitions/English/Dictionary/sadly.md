---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sadly
offline_file: ""
offline_thumbnail: ""
uuid: 5935a5a6-bda5-4833-b4f6-313d819293f0
updated: 1484310172
title: sadly
categories:
    - Dictionary
---
sadly
     adv 1: in an unfortunate way; "sadly he died before he could see
            his grandchild" [syn: {unhappily}] [ant: {happily}]
     2: with sadness; in a sad manner; "`She died last night,' he
        said sadly"
     3: in an unfortunate or deplorable manner; "he was sadly
        neglected"; "it was woefully inadequate" [syn: {deplorably},
         {lamentably}, {woefully}]
