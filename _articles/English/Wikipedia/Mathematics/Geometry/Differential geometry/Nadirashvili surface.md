---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nadirashvili_surface
offline_file: ""
offline_thumbnail: ""
uuid: 56769a01-a22a-483d-8627-9f840e585f0f
updated: 1484309231
title: Nadirashvili surface
categories:
    - Differential geometry
---
In differential geometry, a Nadirashvili surface is an immersed complete bounded minimal surface in R3 with negative curvature. The first example of such a surface was constructed by Nikolai Nadirashvili (de) in Nadirashvili (1996). This simultaneously answered a question of Hadamard about whether there was an immersed complete bounded surface in R3 with negative curvature, and a question of Eugenio Calabi and Shing-Tung Yau about whether there was an immersed complete bounded minimal surface in R3.
