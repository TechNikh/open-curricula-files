---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enlargement
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484342881
title: enlargement
categories:
    - Dictionary
---
enlargement
     n 1: the act of increasing (something) in size or volume or
          quantity or scope [syn: {expansion}] [ant: {contraction}]
     2: a discussion that provides additional information [syn: {expansion},
         {elaboration}]
     3: a photographic print that has been enlarged [syn: {blowup},
        {magnification}]
