---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/who
offline_file: ""
offline_thumbnail: ""
uuid: c7edb353-6c2c-409a-baa9-33f9495bd88d
updated: 1484310307
title: who
categories:
    - Dictionary
---
WHO
     n : a United Nations agency to coordinate international health
         activities and to help governments improve health
         services [syn: {World Health Organization}]
