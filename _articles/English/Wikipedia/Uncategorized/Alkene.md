---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alkene
offline_file: ""
offline_thumbnail: ""
uuid: 6acda1b7-91fd-4660-8638-b472bbb1215f
updated: 1484308409
title: Alkene
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/120px-Ethylene-3D-vdW_0.png
tags:
    - Structure
    - Bonding
    - Shape
    - Physical properties
    - Reactions
    - Addition reactions
    - Hydrogenation
    - Hydration
    - Halogenation
    - Hydrohalogenation
    - Halohydrin formation
    - Oxidation
    - Photooxygenation
    - Polymerization
    - Metal complexation
    - Reaction overview
    - Synthesis
    - Industrial methods
    - Elimination reactions
    - Synthesis from carbonyl compounds
    - 'Synthesis from alkenes: olefin metathesis and hydrovinylation'
    - From alkynes
    - Rearrangements and related reactions
    - Nomenclature
    - IUPAC names
    - Cis–trans notation
    - E–Z notation
    - Groups containing C=C double bonds
    - Nomenclature links
categories:
    - Uncategorized
---
In organic chemistry, an alkene is an unsaturated hydrocarbon that contains at least one carbon–carbon double bond.[1] The words alkene, olefin, and olefine are used often interchangeably (see nomenclature section below). Acyclic alkenes, with only one double bond and no other functional groups, known as mono-enes, form a homologous series of hydrocarbons with the general formula CnH2n.[2] Alkenes have two hydrogen atoms less than the corresponding alkane (with the same number of carbon atoms). The simplest alkene, ethylene (C2H4), with the International Union of Pure and Applied Chemistry (IUPAC) name ethene, is the organic compound produced on the largest scale industrially.[3] Aromatic ...
