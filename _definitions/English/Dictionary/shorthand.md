---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shorthand
offline_file: ""
offline_thumbnail: ""
uuid: 8373f885-483c-488f-86e9-d44ad3cc003a
updated: 1484310393
title: shorthand
categories:
    - Dictionary
---
shorthand
     adj : written in abbreviated or symbolic form; "shorthand notes"
     n : a method of writing rapidly [syn: {stenography}]
