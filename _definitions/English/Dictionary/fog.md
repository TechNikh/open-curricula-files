---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fog
offline_file: ""
offline_thumbnail: ""
uuid: 648737b7-26a8-41d0-b7c1-bf4aaf4b84fe
updated: 1484310226
title: fog
categories:
    - Dictionary
---
fog
     n 1: droplets of water vapor suspended in the air near the ground
     2: an atmosphere in which visibility is reduced because of a
        cloud of some substance [syn: {fogginess}, {murk}, {murkiness}]
     3: confusion characterized by lack of clarity [syn: {daze}, {haze}]
     v : make less visible or unclear; "The stars are obscured by the
         clouds" [syn: {obscure}, {befog}, {becloud}, {obnubilate},
          {haze over}, {cloud}, {mist}]
     [also: {fogging}, {fogged}]
