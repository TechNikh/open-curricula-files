---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Hadwiger_conjecture_(combinatorial_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 882cb304-590a-4a36-8294-b984ffb2db0d
updated: 1484308601
title: Hadwiger conjecture (combinatorial geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Hadwiger_covering.svg.png
tags:
    - Formal statement
    - Alternate formulation with illumination
    - Examples
    - Known results
    - Notes
categories:
    - Discrete geometry
---
In combinatorial geometry, the Hadwiger conjecture states that any convex body in n-dimensional Euclidean space can be covered by 2n or fewer smaller bodies homothetic with the original body, and that furthermore, the upper bound of 2n is necessary iff the body is a parallelepiped. There also exists an equivalent formulation in terms of the number of floodlights needed to illuminate the body.
