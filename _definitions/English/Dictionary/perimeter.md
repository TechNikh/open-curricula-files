---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perimeter
offline_file: ""
offline_thumbnail: ""
uuid: b590ce4f-06ac-47d6-8850-4ee39d1bbaaf
updated: 1484310154
title: perimeter
categories:
    - Dictionary
---
perimeter
     n 1: the boundary line or the area immediately inside the
          boundary [syn: {margin}, {border}]
     2: a line enclosing a plane areas
     3: the size of something as given by the distance around it
        [syn: {circumference}]
