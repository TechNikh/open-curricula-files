---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bones
offline_file: ""
offline_thumbnail: ""
uuid: 829114bd-cad0-4dd6-b17b-2acc6c5b7f56
updated: 1484310330
title: bones
categories:
    - Dictionary
---
bones
     n : a percussion instrument consisting of a pair of hollow
         pieces of wood or bone (usually held between the thumb
         and fingers) that are made to click together (as by
         Spanish dancers) in rhythm with the dance [syn: {castanets},
          {clappers}, {finger cymbals}, {maraca}]
