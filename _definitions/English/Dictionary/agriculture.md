---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agriculture
offline_file: ""
offline_thumbnail: ""
uuid: 1df723b2-b577-4740-afed-2c45799114dc
updated: 1484310252
title: agriculture
categories:
    - Dictionary
---
agriculture
     n 1: a large-scale farming enterprise [syn: {agribusiness}, {factory
          farm}]
     2: the practice of cultivating the land or raising stock [syn:
        {farming}, {husbandry}]
     3: the federal department that administers programs that
        provide services to farmers (including research and soil
        conservation and efforts to stabilize the farming
        economy); created in 1862 [syn: {Department of Agriculture},
         {Agriculture Department}, {USDA}]
     4: the class of people engaged in growing food
