---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/numerous
offline_file: ""
offline_thumbnail: ""
uuid: ecff7061-4f2c-4eac-a7cc-7650512b6d01
updated: 1484310271
title: numerous
categories:
    - Dictionary
---
numerous
     adj : amounting to a large indefinite number; "numerous times";
           "the family was numerous"
