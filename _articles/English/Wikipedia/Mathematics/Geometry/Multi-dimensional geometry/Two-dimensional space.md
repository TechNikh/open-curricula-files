---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Two-dimensional_space
offline_file: ""
offline_thumbnail: ""
uuid: c00e9aaf-8a99-463f-b4aa-0d82f2d66eab
updated: 1484309287
title: Two-dimensional space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Cartesian-coordinate-system.svg.png
tags:
    - History
    - In geometry
    - Coordinate systems
    - Polytopes
    - Convex
    - Degenerate (spherical)
    - Non-convex
    - Circle
    - Other shapes
    - In linear algebra
    - Dot product, angle, and length
    - In calculus
    - Gradient
    - Line integrals and double integrals
    - Fundamental theorem of line integrals
    - "Green's theorem"
    - In topology
    - In graph theory
categories:
    - Multi-dimensional geometry
---
In physics and mathematics, two-dimensional space or bi-dimensional space is a geometric model of the planar projection of the physical universe. The two dimensions are commonly called length and width. Both directions lie in the same plane.
