---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/landing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484384401
title: landing
categories:
    - Dictionary
---
landing
     n 1: an intermediate platform in a staircase
     2: structure providing a place where boats can land people or
        goods [syn: {landing place}]
     3: the act of coming down to the earth (or other surface); "the
        plane made a smooth landing"; "his landing on his feet was
        catlike"
     4: the act of coming to land after a voyage
