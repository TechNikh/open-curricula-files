---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moustache
offline_file: ""
offline_thumbnail: ""
uuid: cf83244b-4266-4be9-8803-e05bd4f2aa19
updated: 1484310150
title: moustache
categories:
    - Dictionary
---
moustache
     n : an unshaved growth of hair on the upper lip; "he looked
         younger after he shaved off his mustache" [syn: {mustache}]
