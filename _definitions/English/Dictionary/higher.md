---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/higher
offline_file: ""
offline_thumbnail: ""
uuid: 91e9febf-b838-4531-ab80-89e5a1e0d3f5
updated: 1484310264
title: higher
categories:
    - Dictionary
---
higher
     adj 1: advanced in complexity or elaboration; "high finance";
            "higher mathematics" [syn: {higher(a)}]
     2: of education beyond the secondary level; "higher education";
        "higher learning" [syn: {higher(a)}]
