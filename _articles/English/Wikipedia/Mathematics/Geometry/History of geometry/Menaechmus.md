---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Menaechmus
offline_file: ""
offline_thumbnail: ""
uuid: 5c716903-1cef-4f8d-a666-2cc21684b961
updated: 1484309244
title: Menaechmus
tags:
    - Life and work
    - Sources
categories:
    - History of geometry
---
Menaechmus (Greek: Μέναιχμος, 380–320 BC) was an ancient Greek mathematician and geometer born in Alopeconnesus in the Thracian Chersonese, who was known for his friendship with the renowned philosopher Plato and for his apparent discovery of conic sections and his solution to the then-long-standing problem of doubling the cube using the parabola and hyperbola.
