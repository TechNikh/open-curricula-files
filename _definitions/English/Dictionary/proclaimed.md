---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proclaimed
offline_file: ""
offline_thumbnail: ""
uuid: e3b6557e-2c9a-4cfd-9c77-425c99ff21ce
updated: 1484310587
title: proclaimed
categories:
    - Dictionary
---
proclaimed
     adj : declared publicly; made widely known; "their announced
           intentions"; "the newspaper's proclaimed adherence to
           the government's policy" [syn: {announced}]
