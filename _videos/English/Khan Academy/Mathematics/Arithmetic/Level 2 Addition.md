---
version: 1
type: video
provider: YouTube
id: 27Kp7HJYj2c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Level%202%20Addition.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3f5cc12f-5403-48eb-96fa-e9fbc065ba63
updated: 1486069569
title: Level 2 Addition
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/27Kp7HJYj2c/default.jpg
    - https://i3.ytimg.com/vi/27Kp7HJYj2c/1.jpg
    - https://i3.ytimg.com/vi/27Kp7HJYj2c/2.jpg
    - https://i3.ytimg.com/vi/27Kp7HJYj2c/3.jpg
---
Adding a 2 digit number to a 1 digit number.  Introduction to carrying.
More free lessons at: http://www.khanacademy.org/video?v=27Kp7HJYj2c
