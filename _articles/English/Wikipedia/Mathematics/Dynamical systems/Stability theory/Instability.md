---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Instability
offline_file: ""
offline_thumbnail: ""
uuid: c923f0a3-f494-48f4-ba55-e1e5c3c15e8b
updated: 1484308830
title: Instability
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Unstable3.svg.png
tags:
    - Instability in control systems
    - Instability in solid mechanics
    - Fluid instabilities
    - Plasma instabilities
    - Instabilities of stellar systems
    - Joint instabilities
    - Notes
categories:
    - Stability theory
---
In numerous fields of study, the component of instability within a system is generally characterized by some of the outputs or internal states growing without bounds.[1] Not all systems that are not stable are unstable; systems can also be marginally stable or exhibit limit cycle behavior.
