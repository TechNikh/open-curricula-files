---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vaccum
offline_file: ""
offline_thumbnail: ""
uuid: da9accdb-5e8c-404d-a5b3-bb5a109a1288
updated: 1484308323
title: Vacuum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kolbenluftpumpe_hg.jpg
tags:
    - Etymology
    - Historical interpretation
    - Classical field theories
    - gravity
    - Electromagnetism
    - Quantum mechanics
    - Outer space
    - Measurement
    - Relative versus absolute measurement
    - Measurements relative to 1 atm
    - Measuring instruments
    - Uses
    - Vacuum-driven machines
    - Outgassing
    - Pumping and ambient air pressure
    - Effects on humans and animals
    - Examples
    - Notes
categories:
    - Uncategorized
---
Vacuum is space void of matter. The word stems from the Latin adjective vacuus for "vacant" or "void". An approximation to such vacuum is a region with a gaseous pressure much less than atmospheric pressure.[1] Physicists often discuss ideal test results that would occur in a perfect vacuum, which they sometimes simply call "vacuum" or free space, and use the term partial vacuum to refer to an actual imperfect vacuum as one might have in a laboratory or in space. In engineering and applied physics on the other hand, vacuum refers to any space in which the pressure is lower than atmospheric pressure.[2] The Latin term in vacuo is used to describe an object that is surrounded by a vacuum.
