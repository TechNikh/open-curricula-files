---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dissected
offline_file: ""
offline_thumbnail: ""
uuid: 838f7ade-db8f-4990-80fc-682d4d93fa1a
updated: 1484310473
title: dissected
categories:
    - Dictionary
---
dissected
     adj : having one or more incisions reaching nearly to the midrib
           [syn: {cleft}]
