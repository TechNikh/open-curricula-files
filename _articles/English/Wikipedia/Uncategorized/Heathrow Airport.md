---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heathrow
offline_file: ""
offline_thumbnail: ""
uuid: 6158dc81-310c-424c-ba13-407e270a591a
updated: 1484308450
title: Heathrow Airport
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Qantas_b747_over_houses_arp.jpg
tags:
    - Location
    - History
    - Operations
    - Facilities
    - Flight movements
    - Regulation
    - Security
    - Terminals
    - Terminal 1 (Closed)
    - Terminal 2
    - Terminal 3
    - Terminal 4
    - Terminal 5
    - Terminal assignments and rearrangements
    - Current terminal assignments
    - Scheduled terminal moves
    - Airlines and destinations
    - Passenger
    - Cargo
    - Traffic and statistics
    - Overview
    - Busiest routes
    - Annual passenger numbers
    - Other facilities
    - Access
    - Public transport
    - Train
    - Bus and coach
    - Inter-terminal transport
    - Taxi
    - Car
    - Bicycle
    - Accidents and incidents
    - Terrorism and security incidents
    - Other incidents
    - Future expansion and plans
    - Runway and terminal expansion
    - Heathrow railway hub
    - Airtrack
    - Heathrow/Gatwick rail link
    - Heathrow City
    - Notes
    - Citations
    - Bibliography
categories:
    - Uncategorized
---
Heathrow Airport (IATA: LHR, ICAO: EGLL) is a major international airport outside London, England. Heathrow is the third busiest airport in the world by international passenger traffic (surpassed by Dubai International in 2014,[2] and Hong Kong International in 2016), as well as the busiest airport in Europe by passenger traffic, and sixth busiest airport in the world by total passenger traffic. In 2015, it handled a record 75 million passengers, a 2.2 percent increase from 2014.[1]
