---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tur%C3%A1n_graph'
offline_file: ""
offline_thumbnail: ""
uuid: 4d30063f-4150-4a67-aba7-55904dc80681
updated: 1484309408
title: Turán graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Turan_13-4.svg.png
tags:
    - "Turán's theorem"
    - Special cases
    - Other properties
categories:
    - Extremal graph theory
---
The Turán graph T(n,r) is a complete multipartite graph formed by partitioning a set of n vertices into r subsets, with sizes as equal as possible, and connecting two vertices by an edge if and only if they belong to different subsets. The graph will have 
  
    
      
        (
        n
        
        
          mod
          
            
          
        
        r
        )
      
    
    {\displaystyle (n\,{\bmod {\,}}r)}
  
 subsets of size 
  
    
      
        ⌈
        n
        
          /
        
        r
        ⌉
      
    
    {\displaystyle \lceil n/r\rceil }
  
, and 
  
    
      
        r
        −
        (
        n
        
        
          mod
 ...
