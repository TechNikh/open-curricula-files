---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entrepreneur
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484428621
title: entrepreneur
categories:
    - Dictionary
---
entrepreneur
     n : someone who organizes a business venture and assumes the
         risk for it [syn: {enterpriser}]
