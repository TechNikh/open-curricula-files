---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Level_structure
offline_file: ""
offline_thumbnail: ""
uuid: b977231c-00c3-4d90-97b2-68c946af0e35
updated: 1484309447
title: Level structure
tags:
    - Definition and construction
    - Properties
    - Applications
categories:
    - Graph theory objects
---
In the mathematical subfield of graph theory a level structure of an undirected graph is a partition of the vertices into subsets that have the same distance from a given root vertex.[1]
