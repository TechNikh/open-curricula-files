---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cooler
offline_file: ""
offline_thumbnail: ""
uuid: b4f9861e-e8a2-449c-90ea-795654c71d20
updated: 1484310228
title: cooler
categories:
    - Dictionary
---
cooler
     n 1: a refrigerator for cooling liquids [syn: {ice chest}]
     2: an iced drink especially white wine and fruit juice
     3: a cell for violent prisoners [syn: {tank}]
