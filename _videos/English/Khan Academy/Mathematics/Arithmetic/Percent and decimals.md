---
version: 1
type: video
provider: YouTube
id: RvtdJnYFNhc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Percent%20and%20decimals.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 049453e8-16ee-4c72-90a0-f2a977b77d4e
updated: 1486069567
title: Percent and decimals
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/RvtdJnYFNhc/default.jpg
    - https://i3.ytimg.com/vi/RvtdJnYFNhc/1.jpg
    - https://i3.ytimg.com/vi/RvtdJnYFNhc/2.jpg
    - https://i3.ytimg.com/vi/RvtdJnYFNhc/3.jpg
---
Expressing percentages as decimals.  Expressing decimals as percentages.
