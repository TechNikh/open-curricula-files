---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hydrochloric_acid
offline_file: ""
offline_thumbnail: ""
uuid: 4f622190-8aa4-4030-b671-c67e3fd0c645
updated: 1484308377
title: Hydrochloric acid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Phase_diagram_HCl_H2O_s_l.PNG
tags:
    - Etymology
    - History
    - Structure and reactions
    - Physical properties
    - Production
    - Industrial market
    - Applications
    - Pickling of steel
    - Production of organic compounds
    - Production of inorganic compounds
    - pH Control and neutralization
    - Regeneration of ion exchangers
    - Other
    - Presence in living organisms
    - Safety
categories:
    - Uncategorized
---
Hydrochloric acid is a corrosive, strong mineral acid with many industrial uses. A colorless, highly pungent solution of hydrogen chloride (HCl) in water, when it reacts with an organic base it forms a hydrochloride salt.
