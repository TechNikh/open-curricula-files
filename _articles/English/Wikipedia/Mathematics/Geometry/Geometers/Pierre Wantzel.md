---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pierre_Wantzel
offline_file: ""
offline_thumbnail: ""
uuid: 6be210f8-edbb-4cc2-9ca3-68fbeae4a3d9
updated: 1484309107
title: Pierre Wantzel
categories:
    - Geometers
---
Pierre Laurent Wantzel (5 June 1814 in Paris – 21 May 1848 in Paris) was a French mathematician who proved that several ancient geometric problems were impossible to solve using only compass and straightedge.[1]
