---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/precipitation
offline_file: ""
offline_thumbnail: ""
uuid: 9b4c8795-3449-4d06-9a66-1853cd9b5ada
updated: 1484310377
title: precipitation
categories:
    - Dictionary
---
precipitation
     n 1: the quantity of water falling to earth at a specific place
          within a specified period of time; "the storm brought
          several inches of precipitation"
     2: the process of forming a chemical precipitate
     3: the falling to earth of any form of water (rain or snow or
        hail or sleet or mist) [syn: {downfall}]
     4: the act of casting down or falling headlong from a height
     5: an unexpected acceleration or hastening; "he is responsible
        for the precipitation of his own demise"
     6: overly eager speed (and possible carelessness); "he soon
        regretted his haste" [syn: {haste}, {hastiness}, {hurry},
        {hurriedness}]
