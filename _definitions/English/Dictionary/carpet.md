---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carpet
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484576281
title: carpet
categories:
    - Dictionary
---
carpet
     n : floor covering consisting of a piece of thick heavy fabric
         (usually with nap or pile) [syn: {rug}, {carpeting}]
     v 1: form a carpet-like cover (over)
     2: cover completely, as if with a carpet; "flowers carpeted the
        meadows"
     3: cover with a carpet; "carpet the floors of the house"
