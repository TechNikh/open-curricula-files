---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wet
offline_file: ""
offline_thumbnail: ""
uuid: 8eb9c09e-b042-4cf9-bd40-1486a9f7e36e
updated: 1484310228
title: wet
categories:
    - Dictionary
---
wet
     adj 1: covered or soaked with a liquid such as water; "a wet
            bathing suit"; "wet sidewalks"; "wet paint"; "wet
            weather" [ant: {dry}]
     2: supporting or permitting the legal production and sale of
        alcoholic beverages; "a wet candidate running on a wet
        platform"; "a wet county" [ant: {dry}]
     3: producing or secreting milk; "a wet nurse"; "a wet cow";
        "lactating cows" [syn: {lactating}] [ant: {dry}]
     4: consisting of or trading in alcoholic liquor; "a wet cargo";
        "a wet canteen"
     5: very drunk [syn: {besotted}, {blind drunk}, {blotto}, {crocked},
         {cockeyed}, {fuddled}, {loaded}, {pie-eyed}, {pissed}, ...
