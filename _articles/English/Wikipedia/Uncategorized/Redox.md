---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oxidative
offline_file: ""
offline_thumbnail: ""
uuid: efdf5099-a8bb-44bb-a24e-9f51711e186e
updated: 1484308317
title: Redox
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-NaF_0.gif
tags:
    - Etymology
    - Definitions
    - Oxidizing and reducing agents
    - Oxidizers
    - Reducers
    - Standard electrode potentials (reduction potentials)
    - Examples of redox reactions
    - Metal displacement
    - Other examples
    - Corrosion and rusting
    - Redox reactions in industry
    - Redox reactions in biology
    - Redox cycling
    - Redox reactions in geology
    - Balancing redox reactions
    - Acidic media
    - Basic media
    - Memory aids
categories:
    - Uncategorized
---
Redox (short for reduction–oxidation reaction) is a chemical reaction in which the oxidation states of atoms are changed. Any such reaction involves both a reduction process and a complementary oxidation process, two key concepts involved with electron transfer processes.[1] Redox reactions include all chemical reactions in which atoms have their oxidation state changed; in general, redox reactions involve the transfer of electrons between chemical species. The chemical species from which the electron is stripped is said to have been oxidized, while the chemical species to which the electron is added is said to have been reduced. It can be explained in simple terms:
