---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mechanical_systems_drawing
offline_file: ""
offline_thumbnail: ""
uuid: e102db37-2282-4e5d-8ea3-85afdd77e963
updated: 1484309329
title: Mechanical systems drawing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Building_services_coordinated_drawing.JPG
tags:
    - Sets of drawings
    - United States
    - Arrangement drawing
    - Assembly drawing
    - Detail drawing
    - Fabrication drawings
    - United Kingdom
    - Tender drawings
    - Sketch drawing
    - Schematic drawing
    - Detailed design drawing
    - Installation drawing
    - Record (as installed, as-built) drawing
    - "Builder's work Drawing"
    - Design stage
    - Installation stage
    - Details to include
    - Job outlook
    - Income of mechanical drafters in 2008
    - ADDA certification
    - Regulations in Canada
categories:
    - Technical drawing
---
Mechanical systems drawing is a type of technical drawing that shows information about heating, ventilating, and air conditioning.[1] It is a powerful tool that helps analyze complex systems. These drawings are often a set of detailed drawings used for construction projects; it is a requirement for all HVAC work. They are based on the floor and reflected ceiling plans of the architect. After the mechanical drawings are complete, they become part of the construction drawings, which is then used to apply for a building permit. They are also used to determine the price of the project.[1]
