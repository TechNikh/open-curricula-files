---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/par
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484442961
title: par
categories:
    - Dictionary
---
par
     n 1: (golf) the standard number of strokes set for each hole on a
          golf course, or for the entire course; "a par-5 hole";
          "par for this course is 72"
     2: a state of being essentially equal or equivalent; equally
        balanced; "on a par with the best" [syn: {equality}, {equivalence},
         {equation}]
     v : make a score (on a hole) equal to par
