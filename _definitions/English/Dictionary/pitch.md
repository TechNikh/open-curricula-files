---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pitch
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484524921
title: pitch
categories:
    - Dictionary
---
pitch
     n 1: the property of sound that varies with variation in the
          frequency of vibration
     2: (baseball) the throwing of a baseball by a pitcher to a
        batter [syn: {delivery}]
     3: a vendor's position (especially on the sidewalk); "he was
        employed to see that his paper's news pitches were not
        trespassed upon by rival vendors"
     4: promotion by means of an argument and demonstration [syn: {sales
        talk}, {sales pitch}]
     5: degree of deviation from a horizontal plane; "the roof had a
        steep pitch" [syn: {rake}, {slant}]
     6: any of various dark heavy viscid substances obtained as a
        residue [syn: {tar}]
     7: a high ...
