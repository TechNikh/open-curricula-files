---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coins_in_a_fountain
offline_file: ""
offline_thumbnail: ""
uuid: d9bced49-4784-4f4c-a6d3-de5992610c7c
updated: 1484308556
title: Coins in a fountain
categories:
    - Combinatorics
---
In combinatorial mathematics, coins in a fountain is an interesting problem with an even more interesting generating function. The problem is described below:
