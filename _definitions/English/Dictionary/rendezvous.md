---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rendezvous
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484515201
title: rendezvous
categories:
    - Dictionary
---
rendezvous
     n 1: a meeting planned at a certain time and place
     2: a place where people meet; "he was waiting for them at the
        rendezvous"
     3: a date; usually with a member of the opposite sex [syn: {tryst}]
     v : meet at a rendezvous
