---
version: 1
type: video
provider: YouTube
id: fkNUYrbnOEY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 81625e8a-376d-4487-baa6-bb1ded450df6
updated: 1486069614
title: Example Expressing division in multiple ways
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/fkNUYrbnOEY/default.jpg
    - https://i3.ytimg.com/vi/fkNUYrbnOEY/1.jpg
    - https://i3.ytimg.com/vi/fkNUYrbnOEY/2.jpg
    - https://i3.ytimg.com/vi/fkNUYrbnOEY/3.jpg
---
