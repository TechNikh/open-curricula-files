---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ecosystem
offline_file: ""
offline_thumbnail: ""
uuid: 180dc066-c20b-4209-9227-c09ec39d28bd
updated: 1484310275
title: ecosystem
categories:
    - Dictionary
---
ecosystem
     n : a system formed by the interaction of a community of
         organisms with their physical environment
