---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jean_Tirole
offline_file: ""
offline_thumbnail: ""
uuid: 47cdd39b-0f53-4de5-adb0-1ecfb3d728cf
updated: 1484309029
title: Jean Tirole
tags:
    - Education
    - Career
    - Awards
    - Publications
    - Books
categories:
    - Game theorists
---
Jean Tirole (born 9 August 1953) is a French professor of economics. He focuses on industrial organization, game theory, banking and finance, and economics and psychology. In 2014 he was awarded the Nobel Memorial Prize in Economic Sciences for his analysis of market power and regulation.[1][2]
