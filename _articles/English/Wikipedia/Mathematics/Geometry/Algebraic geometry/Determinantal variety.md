---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Determinantal_variety
offline_file: ""
offline_thumbnail: ""
uuid: 279eca83-d68f-4c8a-aa6c-c1b3b9928bec
updated: 1484309142
title: Determinantal variety
tags:
    - Definition
    - Properties
    - Related topics
categories:
    - Algebraic geometry
---
In algebraic geometry, determinantal varieties are spaces of matrices with a given upper bound on their ranks. Their significance comes from the fact that many examples in algebraic geometry are of this form, such as the Segre embedding of a product of two projective spaces.
