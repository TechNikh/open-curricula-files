---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Afferent_arteriole
offline_file: ""
offline_thumbnail: ""
uuid: f4da922f-8632-4f17-bd85-9f614bf5ddb2
updated: 1484308371
title: Afferent arterioles
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray1128.png
tags:
    - Regulation
    - Additional images
categories:
    - Uncategorized
---
The afferent arterioles are a group of blood vessels that supply the nephrons in many excretory systems. They play an important role in the regulation of blood pressure as a part of the tubuloglomerular feedback mechanism.
