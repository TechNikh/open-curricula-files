---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/observe
offline_file: ""
offline_thumbnail: ""
uuid: a17819fb-a220-4c9e-a618-b94117927b63
updated: 1484310353
title: observe
categories:
    - Dictionary
---
observe
     v 1: discover or determine the existence, presence, or fact of;
          "She detected high levels of lead in her drinking
          water"; "We found traces of lead in the paint" [syn: {detect},
           {find}, {discover}, {notice}]
     2: make mention of; "She observed that his presentation took up
        too much time"; "They noted that it was a fine day to go
        sailing" [syn: {note}, {mention}, {remark}]
     3: observe with care or pay close attention to; "Take note of
        this chemical reaction" [syn: {note}, {take note}]
     4: watch attentively; "Please observe the reaction of these two
        chemicals"
     5: show respect towards; "honor your ...
