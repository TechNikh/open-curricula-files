---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irrespective
offline_file: ""
offline_thumbnail: ""
uuid: 7f078085-3ef6-43d1-8b62-e392585110cf
updated: 1484310327
title: irrespective
categories:
    - Dictionary
---
irrespective
     adv : in spite of everything; without regard to drawbacks; "he
           carried on regardless of the difficulties" [syn: {regardless},
            {disregardless}, {no matter}, {disregarding}]
