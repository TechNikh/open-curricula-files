---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ghaggar
offline_file: ""
offline_thumbnail: ""
uuid: 542e7c45-24ee-4b30-96de-e0da44771ace
updated: 1484308419
title: Ghaggar-Hakra River
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Ghaggar_river_in_Panchkula.jpg
tags:
    - Ghaggar River
    - Tributaries
    - Hakra River
    - Palaeogeography
    - Ancient tributaries
    - Sutlej
    - Yamuna
    - Association with the Harappan civilization
    - Identification with the Rigvedic Sarasvati
    - Rig Veda
    - Identification with Vedic rivers in recent scholarship
    - Bibliography
categories:
    - Uncategorized
---
The Ghaggar-Hakra River is an intermittent, endorheic river in India and Pakistan that flows only during the monsoon season. The river is known as Ghaggar before the Ottu barrage and as the Hakra downstream of the barrage.[2]
