---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/million
offline_file: ""
offline_thumbnail: ""
uuid: d811bf2d-109d-4eb3-be9d-fd40f1f9ae78
updated: 1484310327
title: million
categories:
    - Dictionary
---
million
     adj : (in Roman numerals, M written with a macron over it)
           denoting a quantity consisting of 1,000,000 items or
           units [syn: {a million}]
     n : the number that is represented as a one followed by 6 zeros
         [syn: {1000000}, {one thousand thousand}, {meg}]
