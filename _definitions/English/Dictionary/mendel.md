---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mendel
offline_file: ""
offline_thumbnail: ""
uuid: e673eb67-7cdc-418a-ba63-454981a51685
updated: 1484310307
title: mendel
categories:
    - Dictionary
---
Mendel
     n : Augustinian monk and botanist whose experiments in breeding
         garden peas led to his eventual recognition as founder of
         the science of genetics (1822-1884) [syn: {Gregor Mendel},
          {Johann Mendel}]
