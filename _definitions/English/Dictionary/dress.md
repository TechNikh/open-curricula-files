---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dress
offline_file: ""
offline_thumbnail: ""
uuid: 5f0e9a54-5734-4db9-8f8c-f42d9798ebf4
updated: 1484310451
title: dress
categories:
    - Dictionary
---
dress
     adj 1: suitable for formal occasions; "formal wear"; "a full-dress
            uniform"; "dress shoes" [syn: {full-dress}]
     2: (of an occasion) requiring formal clothes; "a dress dinner";
        "a full-dress ceremony" [syn: {full-dress}]
     n 1: a one-piece garment for a woman; has skirt and bodice [syn:
          {frock}]
     2: clothing of a distinctive style or for a particular
        occasion; "formal attire"; "battle dress" [syn: {attire},
        {garb}]
     3: clothing in general; "she was refined in her choice of
        apparel"; "he always bought his clothes at the same
        store"; "fastidious about his dress" [syn: {apparel}, {wearing
        apparel}, ...
