---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Iisc
offline_file: ""
offline_thumbnail: ""
uuid: 5904b361-67cb-47c0-b7e9-1a2afb8e417f
updated: 1484308412
title: Indian Institute of Science
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Iisc-Founder.jpg
tags:
    - History
    - Campus
    - J. R. D. Tata Memorial Library
    - Central computing facility
    - Academics
    - Organisation
    - Degrees
    - Postgraduate programs
    - Undergraduate programs
    - Rankings
    - Admissions
    - Academic and industrial collaborations
    - Student activities
    - December 2005 terror attack
    - Notable people
    - Notable faculty
    - Notable alumni
categories:
    - Uncategorized
---
Indian Institute of Science (IISc) is a public university for scientific research and higher education located in Bangalore, India. Established in 1909 with active support from Jamsetji Tata and H.H. Sir Krishnaraja Wodeyar IV, the Maharaja of Mysore. It is also locally known as the "Tata Institute".[4] It acquired the status of a Deemed University in 1958. IISc is widely regarded as India's finest institution in science,[5][6][7][8][9] and has been ranked at number 11 and 18 worldwide (and ranked 3rd and 6th in Asia) when considering the criteria of Citations per Faculty in 2014 and 2015 respectively.[6][10] IISc was the first Indian institute to feature on Times Higher Education World ...
