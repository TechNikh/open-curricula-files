---
version: 1
type: video
provider: YouTube
id: t9PafoLD3N0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 335123d3-986a-4f0a-9d35-da39c653857a
updated: 1486069617
title: Dividing numbers intro to long division
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/t9PafoLD3N0/default.jpg
    - https://i3.ytimg.com/vi/t9PafoLD3N0/1.jpg
    - https://i3.ytimg.com/vi/t9PafoLD3N0/2.jpg
    - https://i3.ytimg.com/vi/t9PafoLD3N0/3.jpg
---
