---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scoring
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484402101
title: scoring
categories:
    - Dictionary
---
scoring
     n : evaluation of performance by assigning a grade or score;
         "what he disliked about teaching was all the grading he
         had to do" [syn: {marking}, {grading}]
