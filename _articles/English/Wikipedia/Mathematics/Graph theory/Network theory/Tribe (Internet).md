---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tribe_(Internet)
offline_file: ""
offline_thumbnail: ""
uuid: 0309dc31-a890-4f12-8106-a0f3d937a8b9
updated: 1484309443
title: Tribe (Internet)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kencf0618FacebookNetwork_0.jpg
tags:
    - History
    - tribe.net starting point
    - Tribes from a technical perspective
    - An in-depth look into the research
    - Background
    - Results and discussion
    - Conclusions
    - Methods
    - Different language misspellings within tribes
    - The campfire
    - Cooperation
    - Wikipedia
    - Advantages
    - Disadvantages
    - Communication
    - Facebook
    - Twitter
    - Cognition
    - Conclusion
categories:
    - Network theory
---
The term tribe or digital tribe[1] is used as a slang term for an unofficial community of people who share a common interest, and usually who are loosely affiliated with each other through social media or other internet mechanisms. The term is related to "tribe", which traditionally refers to people closely associated in both geography and genealogy.[2] Nowadays, it looks more like a virtual community or a personal network and it is often called global digital tribe. Most anthropologists agree that a tribe is a (small) society that practices its own customs and culture, and that these define the tribe. The tribes are divided into clans, with their own customs and cultural values that ...
