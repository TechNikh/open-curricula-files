---
version: 1
type: video
provider: YouTube
id: fLKOBQ6cZHA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Red%20blood%20cells.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4daebde3-96bd-4f61-8c05-15a6ce9db3ce
updated: 1486069581
title: Red blood cells
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/fLKOBQ6cZHA/default.jpg
    - https://i3.ytimg.com/vi/fLKOBQ6cZHA/1.jpg
    - https://i3.ytimg.com/vi/fLKOBQ6cZHA/2.jpg
    - https://i3.ytimg.com/vi/fLKOBQ6cZHA/3.jpg
---
Oxygen uptake by hemoglobin in red blood cells
More free lessons at: http://www.khanacademy.org/video?v=fLKOBQ6cZHA
