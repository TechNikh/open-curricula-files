---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unspoken
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484519401
title: unspoken
categories:
    - Dictionary
---
unspoken
     adj 1: expressed without speech; especially because words would be
            inappropriate or inadequate; "a mute appeal"; "a
            silent curse"; "best grief is tongueless"- Emily
            Dickinson; "the words stopped at her lips unsounded";
            "unspoken grief"; "choking exasperation and wordless
            shame"- Thomas Wolfe [syn: {mute}, {tongueless}, {wordless}]
     2: not made explicit; "the unexpressed terms of the agreement";
        "things left unsaid"; "some kind of unspoken agreement";
        "his action is clear but his reason remains unstated"
        [syn: {unexpressed}, {unsaid}, {unstated}, {unuttered}, {unverbalized},
         ...
