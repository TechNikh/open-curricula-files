---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/complexity
offline_file: ""
offline_thumbnail: ""
uuid: cfaadb19-4969-4de7-ba5e-ee8e93931278
updated: 1484310361
title: complexity
categories:
    - Dictionary
---
complexity
     n : the quality of being intricate and compounded; "he enjoyed
         the complexity of modern computers" [syn: {complexness}]
         [ant: {simplicity}]
