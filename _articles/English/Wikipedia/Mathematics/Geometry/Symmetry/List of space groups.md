---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_of_space_groups
offline_file: ""
offline_thumbnail: ""
uuid: 5474f507-00fa-4a5e-a085-766a94811897
updated: 1484309315
title: List of space groups
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Hexagonal_latticeFRONT.svg.png
tags:
    - Symbols
    - List of Triclinic
    - List of Monoclinic
    - List of Orthorhombic
    - List of Tetragonal
    - List of Trigonal
    - List of Hexagonal
    - List of Cubic
categories:
    - Symmetry
---
There are 230 space groups in three dimensions, given by a number index, and a full name in Hermann–Mauguin notation, and a short name (international short symbol). The long names are given with spaces for readability. The groups each have a point groups of the unit cell.
