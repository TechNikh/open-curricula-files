---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soak
offline_file: ""
offline_thumbnail: ""
uuid: 44a4fb76-3fc5-48ab-b2fb-fc0caf6d9992
updated: 1484310260
title: soak
categories:
    - Dictionary
---
soak
     n 1: the process of becoming softened and saturated as a
          consequence of being immersed in water (or other
          liquid); "a good soak put life back in the wagon" [syn:
          {soakage}, {soaking}]
     2: washing something by allowing it to soak [syn: {soaking}]
     v 1: submerge in a liquid; "I soaked in the hot tub for an hour"
     2: rip off; ask an unreasonable price [syn: {overcharge}, {surcharge},
         {gazump}, {fleece}, {plume}, {pluck}, {rob}, {hook}]
        [ant: {undercharge}]
     3: cover with liquid; pour liquid onto; "souse water on his hot
        face" [syn: {drench}, {douse}, {dowse}, {sop}, {souse}]
     4: leave as a guarantee in return ...
