---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pristine
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484600401
title: pristine
categories:
    - Dictionary
---
pristine
     adj 1: completely free from dirt or contamination; "pristine
            mountain snow"
     2: immaculately clean and unused; "handed her his pristine
        white handkerchief"
