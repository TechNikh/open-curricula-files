---
version: 1
type: video
provider: YouTube
id: savJp5bs_OY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c479b007-2704-453d-b39f-5102d5b6ddbb
updated: 1486069614
title: Complex number polar form intuition exercise
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/savJp5bs_OY/default.jpg
    - https://i3.ytimg.com/vi/savJp5bs_OY/1.jpg
    - https://i3.ytimg.com/vi/savJp5bs_OY/2.jpg
    - https://i3.ytimg.com/vi/savJp5bs_OY/3.jpg
---
