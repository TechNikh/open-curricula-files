---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Myhill_isomorphism_theorem
offline_file: ""
offline_thumbnail: ""
uuid: fb4e4c22-7bf3-4e81-96d8-cf30c30b0686
updated: 1484309489
title: Myhill isomorphism theorem
categories:
    - Computability theory
---
In computability theory the Myhill isomorphism theorem, named after John Myhill, provides a characterization for two numberings to induce the same notion of computability on a set.
