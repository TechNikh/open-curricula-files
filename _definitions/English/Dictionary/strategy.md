---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strategy
offline_file: ""
offline_thumbnail: ""
uuid: 14f15b68-29c6-4994-8811-3cf7f0b09c9a
updated: 1484310238
title: strategy
categories:
    - Dictionary
---
strategy
     n 1: an elaborate and systematic plan of action [syn: {scheme}]
     2: the branch of military science dealing with military command
        and the planning and conduct of a war
