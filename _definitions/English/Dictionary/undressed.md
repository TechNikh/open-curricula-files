---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undressed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484450221
title: undressed
categories:
    - Dictionary
---
undressed
     adj 1: of lumber or stone or hides; not finished or dressed;
            "undressed granite"; "undressed hides"
     2: having removed clothing [syn: {unappareled}, {unattired}, {unclad},
         {ungarbed}, {ungarmented}]
