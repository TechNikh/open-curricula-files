---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/material
offline_file: ""
offline_thumbnail: ""
uuid: bdce7742-b156-4c19-aed9-b51a4001aa58
updated: 1484310344
title: material
categories:
    - Dictionary
---
material
     adj 1: concerned with worldly rather than spiritual interests;
            "material possessions"; "material wealth"; "material
            comforts"
     2: derived from or composed of matter; "the material universe"
        [ant: {immaterial}]
     3: directly relevant to a matter especially a law case; "his
        support made a material difference"; "evidence material to
        the issue at hand"; "facts likely to influence the
        judgment are called material facts"; "a material witness"
        [ant: {immaterial}]
     4: concerned with or affecting physical as distinct from
        intellectual or psychological well-being; "material
        needs"; "the moral and ...
