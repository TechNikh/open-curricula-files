---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hex
offline_file: ""
offline_thumbnail: ""
uuid: cdfe1bba-dd62-445d-a9b3-6b7a43c825b6
updated: 1484310421
title: hex
categories:
    - Dictionary
---
hex
     adj : of or pertaining to a number system having 16 as its base
           [syn: {hexadecimal}]
     n : an evil spell; "a witch put a curse on his whole family";
         "he put the whammy on me" [syn: {jinx}, {curse}, {whammy}]
     v : cast a spell over someone or something; put a hex on someone
         or something [syn: {bewitch}, {glamour}, {witch}, {enchant},
          {jinx}]
