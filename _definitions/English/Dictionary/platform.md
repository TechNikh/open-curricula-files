---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/platform
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484368381
title: platform
categories:
    - Dictionary
---
platform
     n 1: a raised horizontal surface; "the speaker mounted the
          platform"
     2: a document stating the aims and principles of a political
        party; "their candidate simply ignored the party
        platform"; "they won the election even though they offered
        no positive program" [syn: {political platform}, {political
        program}, {program}]
     3: the combination of a particular computer and a particular
        operating system
     4: any military structure or vehicle bearing weapons [syn: {weapons
        platform}]
     5: a woman's shoe with a very high thick sole [syn: {chopine},
        {chopines}, {platforms}]
