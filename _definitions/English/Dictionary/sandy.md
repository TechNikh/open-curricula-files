---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sandy
offline_file: ""
offline_thumbnail: ""
uuid: 0eb52633-6d4f-4501-9200-dabedf151de5
updated: 1484310437
title: sandy
categories:
    - Dictionary
---
sandy
     adj 1: composed of or covered with relatively large particles;
            "granular sugar"; "gritty sand" [syn: {farinaceous}, {coarse-grained},
             {grainy}, {granular}, {granulose}, {gritty}, {mealy}]
     2: (used of soil) loose and large-grained in consistency;
        "light sandy soil" [syn: {friable}, {light}]
     3: of hair color; pale yellowish to yellowish brown; "flaxen
        locks" [syn: {flaxen}]
     4: resembling or containing sand; or growing in sandy areas;
        "arenaceous limestone"; "arenaceous grasses" [syn: {arenaceous},
         {sandlike}] [ant: {argillaceous}]
     5: abounding in sand; "Florida's sandy beaches"
     [also: {sandiest}, ...
