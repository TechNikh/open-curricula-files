---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fox
offline_file: ""
offline_thumbnail: ""
uuid: f5ea419b-ec9a-46f7-ba1c-c9023febc1c4
updated: 1484310275
title: fox
categories:
    - Dictionary
---
fox
     n 1: alert carnivorous mammal with pointed muzzle and ears and a
          bushy tail; most are predators that do not hunt in packs
     2: a shifty deceptive person [syn: {dodger}, {slyboots}]
     3: the gray or reddish-brown fur of a fox
     4: English statesman who supported American independence and
        the French Revolution (1749-1806) [syn: {Charles James Fox}]
     5: English religious leader who founded the Society of Friends
        (1624-1691) [syn: {George Fox}]
     6: a member of an Algonquian people formerly living west of
        Lake Michigan along the Fox River
     7: the Algonquian language of the Fox people
     v 1: deceive somebody; "We tricked the ...
