---
version: 1
type: article
id: https://en.wikipedia.org/wiki/General_game_playing
offline_file: ""
offline_thumbnail: ""
uuid: d928b0f6-2455-4d5d-86e8-0059fb5b0f63
updated: 1484309004
title: General game playing
tags:
    - Stanford project
    - Other approaches
    - Algorithms
    - Assumptions
categories:
    - Game artificial intelligence
---
General game playing (GGP) is the design of artificial intelligence programs to be able to play more than one game successfully.[1] For many games like chess, computers are programmed to play these games using a specially designed algorithm, which cannot be transferred to another context. For example, a chess-playing computer program cannot play checkers. A general game playing system, if well designed, would be able to help in other areas, such as in providing intelligence for search and rescue missions.
