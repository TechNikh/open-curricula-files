---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/symptom
offline_file: ""
offline_thumbnail: ""
uuid: 11d2a02d-6f23-4538-8ed7-726ff0dbab3b
updated: 1484310599
title: symptom
categories:
    - Dictionary
---
symptom
     n 1: (medicine) any sensation or change in bodily function that
          is experienced by a patient and is associated with a
          particular disease
     2: anything that accompanies X and is regarded as an indication
        of X's existence
