---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crossed
offline_file: ""
offline_thumbnail: ""
uuid: bf83d9ad-d991-41dd-8929-7adb55d82604
updated: 1484310305
title: crossed
categories:
    - Dictionary
---
crossed
     adj 1: placed crosswise; "spoken with a straight face but crossed
            fingers"; "crossed forks"; "seated with arms across"
            [syn: {across}] [ant: {uncrossed}]
     2: (of a check) for deposit only as indicated by having two
        lines drawn across it [ant: {uncrossed}]
     3: produced by crossbreeding [syn: {hybrid}, {interbred}, {intercrossed}]
