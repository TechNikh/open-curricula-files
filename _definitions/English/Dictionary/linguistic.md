---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/linguistic
offline_file: ""
offline_thumbnail: ""
uuid: 4869913f-de3d-4079-80ba-c596e6379a12
updated: 1484310603
title: linguistic
categories:
    - Dictionary
---
linguistic
     adj 1: consisting of or related to language; "linguistic behavior";
            "a linguistic atlas"; "lingual diversity" [syn: {lingual}]
            [ant: {nonlinguistic}]
     2: of or relating to the scientific study of language;
        "linguistic theory"
