---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Veniamin_Kagan
offline_file: ""
offline_thumbnail: ""
uuid: d082da25-f214-46a3-a8ae-9ec3fa58fac8
updated: 1484309085
title: Veniamin Kagan
tags:
    - Biography
    - Mathematical work
    - trivia
categories:
    - Geometers
---
Veniamin Fedorovich Kagan (Russian: Вениами́н Фёдорович Ка́ган; 10 March 1869 – 8 May 1953) was a Russian and Soviet mathematician and expert in geometry. He is the maternal grandfather of mathematicians Yakov Sinai and Grigory Barenblatt.
