---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Pentagonal_bipyramidal_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 9cd78f0c-e0cd-427c-a86b-ab280ec7f23a
updated: 1484309280
title: Pentagonal bipyramidal molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pentagonal-bipyramidal-3D-balls.png
categories:
    - Molecular geometry
---
In chemistry, a pentagonal bipyramid (or dipyramid) is a molecular geometry with one atom at the centre with seven ligands at the corners of a pentagonal dipyramid. A perfect pentagonal bipyramid belongs to the molecular point group D5h.
