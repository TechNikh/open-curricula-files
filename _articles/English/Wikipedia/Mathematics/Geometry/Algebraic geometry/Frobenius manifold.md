---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frobenius_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 0f7184af-60d4-434a-89ba-a006bc2ce736
updated: 1484309147
title: Frobenius manifold
tags:
    - Definition
    - Elementary properties
    - Examples
categories:
    - Algebraic geometry
---
In the mathematical field of differential geometry, a Frobenius manifold is a flat Riemannian manifold with a certain compatible multiplicative structure on the tangent space. The concept generalizes the notion of Frobenius algebra to tangent bundles. They were introduced by Dubrovin.[1]
