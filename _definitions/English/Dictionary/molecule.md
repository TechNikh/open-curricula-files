---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/molecule
offline_file: ""
offline_thumbnail: ""
uuid: 48eb94af-8ced-41aa-bdf3-0a49c11c70d2
updated: 1484310337
title: molecule
categories:
    - Dictionary
---
molecule
     n 1: (physics and chemistry) the simplest structural unit of an
          element or compound
     2: (nontechnical usage) a tiny piece of anything [syn: {atom},
        {particle}, {corpuscle}, {mote}, {speck}]
