---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harbour
offline_file: ""
offline_thumbnail: ""
uuid: ab10f4b5-cc1e-4aec-9263-256c527e2406
updated: 1484310587
title: harbour
categories:
    - Dictionary
---
harbour
     n 1: a sheltered port where ships can take on or discharge cargo
          [syn: {seaport}, {haven}, {harbor}]
     2: a place of refuge and comfort and security [syn: {harbor}]
     v 1: secretly shelter (as of fugitives or criminals) [syn: {harbor}]
     2: keep in one's possession; of animals [syn: {harbor}]
     3: hold back a thought or feeling about; "She is harboring a
        grudge against him" [syn: {harbor}, {shield}]
     4: maintain (a theory, thoughts, or feelings); "bear a grudge";
        "entertain interesting notions"; "harbor a resentment"
        [syn: {harbor}, {hold}, {entertain}, {nurse}]
