---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Setpoint_(control_system)
offline_file: ""
offline_thumbnail: ""
uuid: 0cce2bbe-161e-41a1-bed8-c12ed1792f59
updated: 1484308746
title: Setpoint (control system)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Set-point_control.png
categories:
    - Control engineering
---
In cybernetics and control theory, a setpoint (also set point, set-point) is the desired or target value for an essential variable of a system,[1] often used to describe a standard configuration or norm for the system.[2] Departure of a variable from its setpoint is one basis for error-controlled regulation,[3] that is, the use of feedback to return the system to its norm, as in homeostasis. For example, a boiler might have a temperature setpoint, which is the temperature the boiler control system aims to maintain.
