---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cgs
offline_file: ""
offline_thumbnail: ""
uuid: 974b0fd3-eb0d-4e8a-8d6e-7af2eabe7b62
updated: 1484308329
title: Centimetre–gram–second system of units
tags:
    - History
    - Definition of CGS units in mechanics
    - Definitions and conversion factors of CGS units in mechanics
    - Derivation of CGS units in electromagnetism
    - CGS approach to electromagnetic units
    - Alternate derivations of CGS units in electromagnetism
    - Various extensions of the CGS system to electromagnetism
    - Electrostatic units (ESU)
    - ESU notation
    - Electromagnetic units (EMU)
    - EMU notation
    - Relations between ESU and EMU units
    - Practical cgs units
    - Other variants
    - Electromagnetic units in various CGS systems
    - Physical constants in CGS units
    - Pro and contra
    - References and notes
    - General literature
categories:
    - Uncategorized
---
The centimetre–gram–second system of units (abbreviated CGS or cgs) is a variant of the metric system based on the centimetre as the unit of length, the gram as the unit of mass, and the second as the unit of time. All CGS mechanical units are unambiguously derived from these three base units, but there are several different ways of extending the CGS system to cover electromagnetism.
