---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cloth
offline_file: ""
offline_thumbnail: ""
uuid: 0a4c6813-3fb3-4dcb-a5b1-8f7395206f9b
updated: 1484310379
title: cloth
categories:
    - Dictionary
---
cloth
     n : artifact made by weaving or felting or knitting or
         crocheting natural or synthetic fibers; "the fabric in
         the curtains was light and semitraqnsparent"; "woven
         cloth originated in Mesopotamia around 5000 BC"; "she
         measured off enough material for a dress" [syn: {fabric},
          {material}, {textile}]
