---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jealous
offline_file: ""
offline_thumbnail: ""
uuid: 21c863c6-6344-4444-b863-f30857cfdf02
updated: 1484310553
title: jealous
categories:
    - Dictionary
---
jealous
     adj 1: showing extreme cupidity; painfully desirous of another's
            advantages; "he was never covetous before he met her";
            "jealous of his success and covetous of his
            possessions"; "envious of their art collection" [syn:
            {covetous}, {envious}]
     2: suspicious or unduly suspicious or fearful of being
        displaced by a rival; "a jealous lover" [syn: {green-eyed},
         {overjealous}]
