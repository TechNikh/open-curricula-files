---
version: 1
type: video
provider: YouTube
id: peXbz0frV74
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1115597b-b46c-4bf3-9724-ab3a663cc50b
updated: 1486069593
title: Moments (part 2)
categories:
    - Moments, torque, and angular momentum
thumbnail_urls:
    - https://i3.ytimg.com/vi/peXbz0frV74/default.jpg
    - https://i3.ytimg.com/vi/peXbz0frV74/1.jpg
    - https://i3.ytimg.com/vi/peXbz0frV74/2.jpg
    - https://i3.ytimg.com/vi/peXbz0frV74/3.jpg
---
2 more moment problems. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/torque-angular-momentum/torque-tutorial/v/finding-torque-for-angled-forces?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/torque-angular-momentum/torque-tutorial/v/moments?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
