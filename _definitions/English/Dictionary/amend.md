---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amend
offline_file: ""
offline_thumbnail: ""
uuid: be3f5592-b303-47d4-83c6-a8f845db3c6d
updated: 1484310601
title: amend
categories:
    - Dictionary
---
amend
     v 1: make amendments to; "amend the document"
     2: to make better; "The editor improved the manuscript with his
        changes" [syn: {better}, {improve}, {ameliorate}, {meliorate}]
        [ant: {worsen}]
     3: set straight or right; "remedy these deficiencies"; "rectify
        the inequities in salaries"; "repair an oversight" [syn: {rectify},
         {remediate}, {remedy}, {repair}]
