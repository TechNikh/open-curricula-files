---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bastion
offline_file: ""
offline_thumbnail: ""
uuid: df16f30e-8cd5-4764-92a7-b2129a7fadbc
updated: 1484310515
title: bastion
categories:
    - Dictionary
---
bastion
     n 1: a group that defends a principle; "a bastion against
          corruption"; "the last bastion of communism"
     2: a stronghold into which people could go for shelter during a
        battle [syn: {citadel}]
     3: projecting part of a rampart or other fortification
