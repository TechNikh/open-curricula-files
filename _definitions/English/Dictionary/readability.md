---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/readability
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484492761
title: readability
categories:
    - Dictionary
---
readability
     n 1: the quality of written language that makes it easy to read
          and understand
     2: writing (print or handwriting) that can be easily read [syn:
         {legibility}] [ant: {illegibility}]
