---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bowler
offline_file: ""
offline_thumbnail: ""
uuid: 473dc42c-5b7a-4af6-8f73-698d9b88caa6
updated: 1484310150
title: bowler
categories:
    - Dictionary
---
bowler
     n 1: delivers the ball to the batsman in cricket
     2: rolls balls down an alley at pins
     3: a hat that is round and black and hard with a narrow brim;
        worn by some British businessmen [syn: {bowler hat}, {derby},
         {plug hat}]
