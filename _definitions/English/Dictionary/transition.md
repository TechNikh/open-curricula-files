---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transition
offline_file: ""
offline_thumbnail: ""
uuid: 2e0e3b16-6f70-4f64-8f32-26d1bebcdfb7
updated: 1484310399
title: transition
categories:
    - Dictionary
---
transition
     n 1: the act of passing from one state or place to the next [syn:
           {passage}]
     2: an event that results in a transformation [syn: {conversion},
         {changeover}]
     3: a change from one place or state or subject or stage to
        another
     4: a musical passage moving from one key to another [syn: {modulation}]
     5: a passage that connects a topic to one that follows
