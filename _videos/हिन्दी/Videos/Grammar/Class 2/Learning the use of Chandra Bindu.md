---
version: 1
type: video
provider: YouTube
id: vr4ZIw55K4A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ca31954f-1e7f-4ea1-a8c6-ba7ffa389567
updated: 1486069630
title: Learning the use of Chandra Bindu
categories:
    - Class 2
thumbnail_urls:
    - https://i3.ytimg.com/vi/vr4ZIw55K4A/default.jpg
    - https://i3.ytimg.com/vi/vr4ZIw55K4A/1.jpg
    - https://i3.ytimg.com/vi/vr4ZIw55K4A/2.jpg
    - https://i3.ytimg.com/vi/vr4ZIw55K4A/3.jpg
---
