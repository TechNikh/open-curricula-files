---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mainly
offline_file: ""
offline_thumbnail: ""
uuid: 0eed7d83-24ef-46e1-8d19-7172f1a6fb64
updated: 1484310357
title: mainly
categories:
    - Dictionary
---
mainly
     adv : for the most part; "he is mainly interested in butterflies"
           [syn: {chiefly}, {principally}, {primarily}, {in the
           main}]
