---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/endothermic
offline_file: ""
offline_thumbnail: ""
uuid: a408ea2b-24f8-48ca-a2c6-c1cfaf49759f
updated: 1484310371
title: endothermic
categories:
    - Dictionary
---
endothermic
     adj : (of a chemical reaction or compound) occurring or formed
           with absorption of heat [syn: {endothermal}, {heat-absorbing}]
           [ant: {exothermic}]
