---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/federation
offline_file: ""
offline_thumbnail: ""
uuid: 8687d3f5-e0e8-4cc7-8a55-985183160a6a
updated: 1484310599
title: federation
categories:
    - Dictionary
---
federation
     n 1: an organization formed by merging several groups or parties
     2: a union of political organizations [syn: {confederation}, {confederacy}]
     3: the act of constituting a political unity out of a number of
        separate states or colonies or provinces so that each
        member retains the management of its internal affairs
