---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nasal
offline_file: ""
offline_thumbnail: ""
uuid: 4ccbde12-6168-4b28-ae25-ceb1290c5c19
updated: 1484310351
title: nasal
categories:
    - Dictionary
---
nasal
     adj 1: of or in or relating to the nose; "nasal passages" [syn: {rhinal}]
     2: sounding as if the nose were pinched; "a whining nasal
        voice" [syn: {adenoidal}, {pinched}]
     n 1: a continuant consonant produced through the nose with the
          mouth closed [syn: {nasal consonant}]
     2: an elongated rectangular bone that forms the bridge of the
        nose [syn: {nasal bone}, {os nasale}]
