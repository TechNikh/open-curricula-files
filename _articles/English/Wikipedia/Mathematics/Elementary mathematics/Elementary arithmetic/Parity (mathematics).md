---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parity_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 09412be2-2fe4-4546-ad68-1cdff1bfb57f
updated: 1484308915
title: Parity (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/275px-Parity_of_5_and_6_Cuisenaire_rods.png
tags:
    - Arithmetic on even and odd numbers
    - Addition and subtraction
    - Multiplication
    - Division
    - History
    - Higher mathematics
    - Higher dimensions and more general classes of numbers
    - Number theory
    - Group theory
    - Analysis
    - Combinatorial game theory
    - Additional applications
categories:
    - Elementary arithmetic
---
Parity is a mathematical term that describes the property of an integer's inclusion in one of two categories: even or odd. An integer is even if it is 'evenly divisible' by two (the old-fashioned term "evenly divisible" is now almost always shortened to "divisible") and odd if it is not even.[1] For example, 6 is even because there is no remainder when dividing it by 2. By contrast, 3, 5, 7, 21 leave a remainder of 1 when divided by 2. Examples of even numbers include −4, 0, 8, and 1738. In particular, zero is an even number.[2] Some examples of odd numbers are −5, 3, 9, and 73. Parity does not apply to non-integer numbers.
