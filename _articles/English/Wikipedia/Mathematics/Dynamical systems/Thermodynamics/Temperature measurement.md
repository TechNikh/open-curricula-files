---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Temperature_measurement
offline_file: ""
offline_thumbnail: ""
uuid: 01deac45-4994-471e-9015-fc8dadd99fe6
updated: 1484308866
title: Temperature measurement
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Clinical_thermometer_38.7.JPG
tags:
    - History
    - Technologies
    - Non-invasive thermometry
    - Surface air temperature
    - Comparison of temperature scales
    - Standards
    - US (ASME) Standards
categories:
    - Thermodynamics
---
Temperature measurement describes the process of measuring a current local temperature for immediate or later evaluation. Datasets consisting of repeated standardized measurements can be used to assess temperature trends.
