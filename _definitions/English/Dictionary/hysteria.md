---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hysteria
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484596201
title: hysteria
categories:
    - Dictionary
---
hysteria
     n 1: state of violent mental agitation [syn: {craze}, {delirium},
           {frenzy}, {fury}]
     2: excessive or uncontrollable fear
     3: neurotic disorder characterized by violent emotional
        outbreaks and disturbances of sensory and motor functions
        [syn: {hysterical neurosis}]
