---
version: 1
type: video
provider: YouTube
id: p0vLpXw2wQA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c9e27f45-f866-4062-b24e-53c22009f402
updated: 1486069639
title: "Tom's Refraction Experiment"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/p0vLpXw2wQA/default.jpg
    - https://i3.ytimg.com/vi/p0vLpXw2wQA/1.jpg
    - https://i3.ytimg.com/vi/p0vLpXw2wQA/2.jpg
    - https://i3.ytimg.com/vi/p0vLpXw2wQA/3.jpg
---
