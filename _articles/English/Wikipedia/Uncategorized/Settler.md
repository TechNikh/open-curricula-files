---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Settled
offline_file: ""
offline_thumbnail: ""
uuid: 9331567d-98f0-4cb6-b8bc-b6504106acce
updated: 1484308470
title: Settler
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Ingolf_by_Raadsig.jpg
tags:
    - Historical usage
    - Anthropological usage
    - Modern usage
    - Implications of Settlement
    - Livelihood
    - Other usages
    - Causes of emigration
categories:
    - Uncategorized
---
A settler is a person who has migrated to an area and established a permanent residence there, often to colonize the area. Settlers are generally from a sedentary culture, as opposed to nomads who share and rotate their settlements with little or no concept of individual land ownership. Settlements are often built on land already claimed or owned by another group. Many times settlers are backed by governments or large countries.
