---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trigonometry
offline_file: ""
offline_thumbnail: ""
uuid: 9bdacf19-b046-4b57-9534-cf6177b7e93c
updated: 1484309340
title: Trigonometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Circle-trig6.svg.png
tags:
    - History
    - Overview
    - Extending the definitions
    - Mnemonics
    - Calculating trigonometric functions
    - Applications of trigonometry
    - Pythagorean identities
    - Angle transformation formulae
    - Common formulae
    - Law of sines
    - Law of cosines
    - Law of tangents
    - "Euler's formula"
    - Bibliography
categories:
    - Trigonometry
---
Trigonometry (from Greek trigōnon, "triangle" and metron, "measure"[1]) is a branch of mathematics that studies relationships involving lengths and angles of triangles. The field emerged in the Hellenistic world during the 3rd century BC from applications of geometry to astronomical studies.[2]
