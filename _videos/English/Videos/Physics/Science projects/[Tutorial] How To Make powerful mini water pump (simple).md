---
version: 1
type: video
provider: YouTube
id: -7bkJgjcnN0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a9cb5cc5-2eeb-450d-877d-3940daa52375
updated: 1486069632
title: '[Tutorial] How To Make powerful mini water pump (simple)'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/-7bkJgjcnN0/default.jpg
    - https://i3.ytimg.com/vi/-7bkJgjcnN0/1.jpg
    - https://i3.ytimg.com/vi/-7bkJgjcnN0/2.jpg
    - https://i3.ytimg.com/vi/-7bkJgjcnN0/3.jpg
---
