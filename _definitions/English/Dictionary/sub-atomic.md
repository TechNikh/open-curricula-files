---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sub-atomic
offline_file: ""
offline_thumbnail: ""
uuid: 74269304-d86e-470e-952c-b8a2f5b84a1f
updated: 1484310389
title: sub-atomic
categories:
    - Dictionary
---
subatomic
     adj 1: of or relating to constituents of the atom or forces within
            the atom; "subatomic particles"; "harnessing subatomic
            energy"
     2: of dimensions smaller than atomic dimensions
