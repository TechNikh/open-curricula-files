---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/specialty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484419141
title: specialty
categories:
    - Dictionary
---
specialty
     n 1: an asset of special worth or utility; "cooking is his forte"
          [syn: {forte}, {strong suit}, {long suit}, {metier}, {speciality},
           {strong point}, {strength}] [ant: {weak point}]
     2: a distinguishing trait [syn: {peculiarity}, {specialness}, {speciality},
         {distinctiveness}]
     3: the special line of work you have adopted as your career;
        "his specialization is gastroenterology" [syn: {specialization},
         {specialisation}, {speciality}, {specialism}]
