---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Scotland_Yard_(board_game)
offline_file: ""
offline_thumbnail: ""
uuid: fb6fc897-6c40-4a2f-9a59-2832bfc56df5
updated: 1484309065
title: Scotland Yard (board game)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Scotland.svg.png
tags:
    - Gameplay
    - Alternative versions
    - Alternative rules
categories:
    - Pursuit-evasion
---
Scotland Yard is a board game in which a team of players, as police, cooperate to track down a player controlling a criminal around a board representing the streets of London. It is named after Scotland Yard, the headquarters of London's Metropolitan Police Service. Scotland Yard is an asymmetric board game, with the detective players cooperatively solving a variant of the pursuit-evasion problem. The game is published by Ravensburger in most of Europe and Canada and by Milton Bradley in the United States. It received the Spiel des Jahres (Game of the Year) award in 1983. A sequel to Scotland Yard was released called "Mister X".
