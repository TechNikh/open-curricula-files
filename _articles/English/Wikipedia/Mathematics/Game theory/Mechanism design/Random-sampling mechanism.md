---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Random-sampling_mechanism
offline_file: ""
offline_thumbnail: ""
uuid: ba58fa8c-ecd9-4688-831c-92fb1c4e7b7a
updated: 1484309046
title: Random-sampling mechanism
tags:
    - RSM in large markets
    - Market-halving scheme
    - Profit-oracle scheme
    - RSM in small markets
    - Market-halving, digital goods
    - Single-sample, physical goods
    - Sample complexity
    - Envy
categories:
    - Mechanism design
---
A random-sampling mechanism (RSM) is a truthful mechanism that uses sampling in order to achieve approximately-optimal gain in prior-free mechanisms and prior-independent mechanisms.
