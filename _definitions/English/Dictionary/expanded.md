---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expanded
offline_file: ""
offline_thumbnail: ""
uuid: 4931c2a5-b00f-462e-ac88-22102a7255cf
updated: 1484310448
title: expanded
categories:
    - Dictionary
---
expanded
     adj : increased in extent or size or bulk or scope [ant: {contracted}]
