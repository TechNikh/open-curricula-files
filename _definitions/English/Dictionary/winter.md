---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/winter
offline_file: ""
offline_thumbnail: ""
uuid: 75c57776-59bf-40cb-a9c5-bb5fd79e4fd8
updated: 1484310226
title: winter
categories:
    - Dictionary
---
winter
     n : the coldest season of the year; in the northern hemisphere
         it extends from the winter solstice to the vernal equinox
         [syn: {wintertime}]
     v : spend the winter; "We wintered on the Riviera"
