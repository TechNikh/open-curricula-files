---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alexander_Nabutovsky
offline_file: ""
offline_thumbnail: ""
uuid: 265aed47-8587-4a53-844c-b886ea60b704
updated: 1484309091
title: Alexander Nabutovsky
categories:
    - Geometers
---
Alexander Nabutovsky is a Canadian mathematician specializing in differential geometry, geometric calculus of variations and quantitative aspects of topology of manifolds. He is a professor at the University of Toronto Department of Mathematics.
