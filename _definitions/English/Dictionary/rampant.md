---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rampant
offline_file: ""
offline_thumbnail: ""
uuid: 026ea3e4-1f32-4c36-9d8e-61fea20aadff
updated: 1484310462
title: rampant
categories:
    - Dictionary
---
rampant
     adj 1: unrestrained and violent; "rampant aggression"
     2: rearing on left hind leg with forelegs elevated and head
        usually in profile; "a lion rampant" [syn: {rampant(ip)},
        {rearing}]
