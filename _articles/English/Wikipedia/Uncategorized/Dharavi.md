---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dharavi
offline_file: ""
offline_thumbnail: ""
uuid: d4db6eb2-b998-498b-ae0b-a76cdda1240e
updated: 1484308464
title: Dharavi
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/225px-Principaux_Bidonvilles.png
tags:
    - History
    - Colonial era
    - Post-independence
    - Redevelopment plans
    - Demographics
    - Location and characteristics
    - Economy
    - Utility services
    - Sanitation issues
    - Epidemics and other disasters
categories:
    - Uncategorized
---
Dharavi is a locality in Mumbai, Maharashtra, India.[1] Its slum is one of the largest in the world;[1][2][3][4] home to between roughly 700,000[5] to about 1 million people,[6] Dharavi is currently the second-largest slum in the continent of Asia[7][8] and the third-largest slum in the world.[8][9] With an area of just over 2.1 square kilometres (0.81 sq mi)[10] and a population density of over 277,136/km2 (717,780/sq mi), Dharavi is also one of the most densely populated areas on Earth.
