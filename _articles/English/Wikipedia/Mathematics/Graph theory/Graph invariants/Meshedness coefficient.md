---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Meshedness_coefficient
offline_file: ""
offline_thumbnail: ""
uuid: 5f2d5a1c-f91e-4981-9304-0cb3553556f0
updated: 1484309422
title: Meshedness coefficient
tags:
    - Definition
    - Applications
    - Limitations
categories:
    - Graph invariants
---
In graph theory, the meshedness coefficient is a graph invariant of planar graphs that measures the number of bounded faces of the graph, as a fraction of the possible number of faces for other planar graphs with the same number of vertices. It ranges from 0 for trees to 1 for maximal planar graphs.[1] [2]
