---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distillate
offline_file: ""
offline_thumbnail: ""
uuid: 6cd139e4-6d92-413c-a58c-9eb0ef18adc1
updated: 1484310411
title: distillate
categories:
    - Dictionary
---
distillate
     n : a purified liquid produced by condensation from a vapor
         during distilling; the product of distilling [syn: {distillation}]
