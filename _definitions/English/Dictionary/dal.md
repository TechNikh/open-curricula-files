---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dal
offline_file: ""
offline_thumbnail: ""
uuid: 0029b861-7120-4c0e-9a63-9db55120f8d2
updated: 1484310605
title: dal
categories:
    - Dictionary
---
dal
     n : a metric unit of volume or capacity equal to 10 liters [syn:
          {dekaliter}, {dekalitre}, {decaliter}, {decalitre}, {dkl}]
