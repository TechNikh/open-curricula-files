---
version: 1
type: video
provider: YouTube
id: M_5KYncYNyc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Physics/Thermodynamics/Efficiency%20of%20a%20Carnot%20engine.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6ff36f7e-ec58-4786-b19e-5c4c54b5529e
updated: 1486069589
title: Efficiency of a Carnot engine
categories:
    - Thermodynamics
thumbnail_urls:
    - https://i3.ytimg.com/vi/M_5KYncYNyc/default.jpg
    - https://i3.ytimg.com/vi/M_5KYncYNyc/1.jpg
    - https://i3.ytimg.com/vi/M_5KYncYNyc/2.jpg
    - https://i3.ytimg.com/vi/M_5KYncYNyc/3.jpg
---
Definition of efficiency for a heat engine. Efficiency of a Carnot Engine. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/thermodynamics/laws-of-thermodynamics/v/carnot-efficiency-2-reversing-the-cycle?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/thermodynamics/laws-of-thermodynamics/v/more-on-entropy?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
