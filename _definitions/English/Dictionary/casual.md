---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/casual
offline_file: ""
offline_thumbnail: ""
uuid: baddf29e-b960-4447-b337-cee192b7066d
updated: 1484310453
title: casual
categories:
    - Dictionary
---
casual
     adj 1: marked by blithe unconcern; "an ability to interest casual
            students"; "showed a casual disregard for cold
            weather"; "an utterly insouciant financial policy";
            "an elegantly insouciant manner"; "drove his car with
            nonchalant abandon"; "was polite in a teasing
            nonchalant manner" [syn: {insouciant}, {nonchalant}]
     2: without or seeming to be without plan or method; offhand; "a
        casual remark"; "information collected by casual methods
        and in their spare time"
     3: suited for everyday use; "casual clothes"; "everyday
        clothes" [syn: {everyday}]
     4: occurring or appearing or singled out ...
