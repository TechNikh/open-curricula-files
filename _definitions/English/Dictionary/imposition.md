---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imposition
offline_file: ""
offline_thumbnail: ""
uuid: 4a762bb6-29c7-4478-b8bd-553f3fb88206
updated: 1484310599
title: imposition
categories:
    - Dictionary
---
imposition
     n 1: the act of imposing something (as a tax or an embargo) [syn:
           {infliction}]
     2: an uncalled-for burden; "he listened but resented the
        imposition"
