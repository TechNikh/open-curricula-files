---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lay
offline_file: ""
offline_thumbnail: ""
uuid: ebd2971b-3fc1-42bc-966f-d7d630cc4ace
updated: 1484310551
title: lay
categories:
    - Dictionary
---
lay
     adj 1: concerning those not members of the clergy; "set his collar
            in laic rather than clerical position"; "the lay
            ministry"; "the choir sings both sacred and secular
            music" [syn: {laic}, {secular}]
     2: not of or from a profession; "a lay opinion as to the cause
        of the disease"
     n 1: a narrative song with a recurrent refrain [syn: {ballad}]
     2: a narrative poem of popular origin [syn: {ballad}]
     v 1: put into a certain place or abstract location; "Put your
          things here"; "Set the tray down"; "Set the dogs on the
          scent of the missing children"; "Place emphasis on a
          certain point" [syn: {put}, ...
