---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Brouwer_fixed-point_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 9ee00fb0-4920-4891-9d76-902e9a2c4df2
updated: 1484308784
title: Brouwer fixed-point theorem
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Th%25C3%25A9or%25C3%25A8me-de-Brouwer-%2528cond-1%2529.jpg'
tags:
    - Statement
    - Importance of the pre-conditions
    - Boundedness
    - Closedness
    - Convexity
    - Notes
    - Illustrations
    - Intuitive approach
    - Explanations attributed to Brouwer
    - One-dimensional case
    - History
    - Prehistory
    - First proofs
    - Reception
    - Proof outlines
    - A proof using degree
    - A proof using homology
    - "A proof using Stokes's theorem"
    - A combinatorial proof
    - A proof by Hirsch
    - A proof using the oriented area
    - A proof using the game hex
    - A proof using the Lefschetz fixed-point theorem
    - A proof in a weak logical system
    - Generalizations
    - Equivalent results
    - Notes
categories:
    - Fixed points (mathematics)
---
Brouwer's fixed-point theorem is a fixed-point theorem in topology, named after Luitzen Brouwer. It states that for any continuous function 
  
    
      
        f
      
    
    {\displaystyle f}
  
 mapping a compact convex set into itself there is a point 
  
    
      
        
          x
          
            0
          
        
      
    
    {\displaystyle x_{0}}
  
 such that 
  
    
      
        f
        (
        
          x
          
            0
          
        
        )
        =
        
          x
          
            0
          
        
      
    
    {\displaystyle f(x_{0})=x_{0}}
  
. The simplest forms of Brouwer's theorem are for continuous ...
