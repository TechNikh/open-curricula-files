---
version: 1
type: video
provider: YouTube
id: 5324IWyVtkE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c7b80212-85ae-4f96-8ee9-7325c4a01ff5
updated: 1486069640
title: "Wave optics Lecture 8, IIT JEE Physics ( Young's Double Slit Experiment )"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/5324IWyVtkE/default.jpg
    - https://i3.ytimg.com/vi/5324IWyVtkE/1.jpg
    - https://i3.ytimg.com/vi/5324IWyVtkE/2.jpg
    - https://i3.ytimg.com/vi/5324IWyVtkE/3.jpg
---
