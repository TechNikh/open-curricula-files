---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/height
offline_file: ""
offline_thumbnail: ""
uuid: 8dddc599-fe8a-4753-9933-93f733df5ed2
updated: 1484310281
title: height
categories:
    - Dictionary
---
height
     n 1: the vertical dimension of extension; distance from the base
          of something to the top [syn: {tallness}]
     2: the highest level or degree attainable; "his landscapes were
        deemed the acme of beauty"; "the artist's gifts are at
        their acme"; "at the height of her career"; "the peak of
        perfection"; "summer was at its peak"; "...catapulted
        Einstein to the pinnacle of fame"; "the summit of his
        ambition"; "so many highest superlatives achieved by man";
        "at the top of his profession" [syn: {acme}, {elevation},
        {peak}, {pinnacle}, {summit}, {superlative}, {top}]
     3: natural height of a person or animal in an ...
