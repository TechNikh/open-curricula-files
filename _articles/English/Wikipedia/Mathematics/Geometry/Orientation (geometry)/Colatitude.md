---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Colatitude
offline_file: ""
offline_thumbnail: ""
uuid: a8acf228-438f-46e4-aa23-6663973780bc
updated: 1484309290
title: Colatitude
categories:
    - Orientation (geometry)
---
In spherical coordinates, colatitude is the complementary angle of the latitude, i.e. the difference between 90° and the latitude,[1] where southern latitudes are denoted with a minus sign.
