---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illustrate
offline_file: ""
offline_thumbnail: ""
uuid: cd0d335d-98eb-4410-953f-c797fde357f3
updated: 1484310445
title: illustrate
categories:
    - Dictionary
---
illustrate
     v 1: clarify by giving an example of [syn: {exemplify}, {instance}]
     2: depict with an illustration
     3: e.g., illustrate a book with drawings
