---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/architect
offline_file: ""
offline_thumbnail: ""
uuid: fc09ab6a-4833-45da-8181-8434d1151731
updated: 1484310414
title: architect
categories:
    - Dictionary
---
architect
     n : someone who creates plans to be used in making something
         (such as buildings) [syn: {designer}]
