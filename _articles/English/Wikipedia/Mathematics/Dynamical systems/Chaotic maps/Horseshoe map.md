---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Horseshoe_map
offline_file: ""
offline_thumbnail: ""
uuid: 032204bb-fd66-45be-a7ce-2f5e25115e85
updated: 1484308719
title: Horseshoe map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Finnhorse_stallion.jpg
tags:
    - The horseshoe map
    - Dynamics of the map
    - Orbits
    - Iterating the square
    - Invariant set
    - Symbolic dynamics
    - Periodic orbits
    - Notes
categories:
    - Chaotic maps
---
In the mathematics of chaos theory, a horseshoe map is any member of a class of chaotic maps of the square into itself. It is a core example in the study of dynamical systems. The map was introduced by Stephen Smale while studying the behavior of the orbits of the van der Pol oscillator. The action of the map is defined geometrically by squishing the square, then stretching the result into a long strip, and finally folding the strip into the shape of a horseshoe.
