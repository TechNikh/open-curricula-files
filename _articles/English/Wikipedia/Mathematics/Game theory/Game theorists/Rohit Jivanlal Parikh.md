---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rohit_Jivanlal_Parikh
offline_file: ""
offline_thumbnail: ""
uuid: 40363f89-aa34-4a33-bdaa-bc213270bb13
updated: 1484309025
title: Rohit Jivanlal Parikh
tags:
    - Posts
    - Awards and recognition
    - Academic and research appointments
    - Main publications
categories:
    - Game theorists
---
Rohit Jivanlal Parikh (born November 20, 1936) is a mathematician, logician, and philosopher who has worked in many areas in traditional logic, including recursion theory and proof theory. His catholic attitude towards logic has led to work on topics like vagueness, ultrafinitism, belief revision, logic of knowledge, game theory and social software (social procedure). This last area seeks to combine techniques from logic, computer science (especially logic of programs) and game theory to understand the structure of social algorithms. Examples of such are elections, transport systems, lectures, conferences, and monetary systems, all of which have properties of interest to those who are ...
