---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ludwig_Burmester
offline_file: ""
offline_thumbnail: ""
uuid: cda51a18-53a3-450f-adbd-5a40a0c64e5e
updated: 1484309073
title: Ludwig Burmester
categories:
    - Geometers
---
His doctoral thesis Über die Elemente einer Theorie der Isophoten (About the elements of a theory of isophotes) concerned lines on a surface defined by light direction. After a period as a teacher in Łódź he became professor of synthetic geometry at Dresden where his growing interest in kinematics culminated in his Lehrbuch der Kinematik, Erster Band, Die ebene Bewegung (Textbook of Kinematics, First Volume, Planar Motion) of 1888, developing the approach to the theory of linkages introduced by Franz Reuleaux, whereby a planar mechanism was understood as a collection of Euclidean planes in relative motion with one degree of freedom. Burmester considered both the theory of planar ...
