---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stabilising
offline_file: ""
offline_thumbnail: ""
uuid: 3e7fbaa5-881e-4217-8d3a-aaa280f3b030
updated: 1484310221
title: stabilising
categories:
    - Dictionary
---
stabilising
     adj : causing to become stable; "the family is one of the great
           stabilizing elements in society" [syn: {stabilizing}]
