---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mature
offline_file: ""
offline_thumbnail: ""
uuid: db78fcb8-b4b9-42d3-ba78-f248887ee5b3
updated: 1484310160
title: mature
categories:
    - Dictionary
---
mature
     adj 1: characteristic of maturity; "mature for her age" [ant: {immature}]
     2: fully considered and perfected; "mature plans" [syn: {matured}]
     3: having reached full natural growth or development; "a mature
        cell" [ant: {immature}]
     4: fully developed or matured and ready to be eaten or used;
        "ripe peaches"; "full-bodies mature wines" [syn: {ripe}]
        [ant: {green}]
     5: (of birds) having developed feathers or plumage; often used
        in combination [syn: {fledged}] [ant: {unfledged}]
     v 1: develop and reach maturity; undergo maturation; "He matured
          fast"; "The child grew fast" [syn: {maturate}, {grow}]
     2: develop and work ...
