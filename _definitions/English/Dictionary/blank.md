---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blank
offline_file: ""
offline_thumbnail: ""
uuid: 598dc8be-6f6a-463d-b07d-50d8f2e5740d
updated: 1484310397
title: blank
categories:
    - Dictionary
---
blank
     adj 1: of a surface; not written or printed on; "blank pages";
            "fill in the blank spaces"; "a clean page"; "wide
            white margins" [syn: {clean}, {white}]
     2: void of expression; "a blank stare"
     3: not charged with a bullet; "a blank cartridge"
     n 1: a blank character used to separate successive words in
          writing or printing; "he said the space is the most
          important character in the alphabet" [syn: {space}]
     2: a substitute for a taboo word; "I hit the blank blank car"
     3: a blank gap or missing part [syn: {lacuna}]
     4: a piece of material ready to be made into something
     5: a cartridge containing an explosive ...
