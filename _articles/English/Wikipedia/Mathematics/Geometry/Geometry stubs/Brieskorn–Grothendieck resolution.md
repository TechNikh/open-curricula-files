---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Brieskorn%E2%80%93Grothendieck_resolution'
offline_file: ""
offline_thumbnail: ""
uuid: 1bc09fbb-aeac-4e1e-8deb-1a8b1ac8529a
updated: 1484309354
title: Brieskorn–Grothendieck resolution
categories:
    - Geometry stubs
---
In mathematics, a Brieskorn–Grothendieck resolution is a resolution conjectured by Alexander Grothendieck, that in particular gives a resolution of the universal deformation of a Kleinian singularity. Brieskorn (1971) announced the construction of this resolution, and Slodowy (1980) published the details of Brieskorn's construction.
