---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/council
offline_file: ""
offline_thumbnail: ""
uuid: eba389b0-028b-42c3-9eed-c7a8a4f91e51
updated: 1484310431
title: council
categories:
    - Dictionary
---
council
     n 1: a body serving in an administrative capacity; "student
          council"
     2: (Christianity) an assembly or theologians and bishops and
        other representative of different churches or dioceses
        that is convened to regulate matters of discipline or
        doctrine
     3: a meeting of people for consultation; "emergency council"
