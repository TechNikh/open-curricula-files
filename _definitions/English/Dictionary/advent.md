---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484542441
title: advent
categories:
    - Dictionary
---
advent
     n 1: arrival that has been awaited (especially of something
          momentous); "the advent of the computer" [syn: {coming}]
     2: the season including the four Sundays preceding Christmas
     3: (Christian theology) the reappearance of Jesus as judge for
        the Last Judgment [syn: {Second Coming}, {Second Coming of
        Christ}, {Second Advent}, {Parousia}]
