---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aaa
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484346421
title: aaa
categories:
    - Dictionary
---
AAA
     n : an aneurysm of the abdominal aorta associated with old age
         and hypertension [syn: {abdominal aortic aneurysm}]
