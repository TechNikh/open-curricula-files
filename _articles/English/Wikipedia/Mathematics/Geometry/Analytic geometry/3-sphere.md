---
version: 1
type: article
id: https://en.wikipedia.org/wiki/3-sphere
offline_file: ""
offline_thumbnail: ""
uuid: 2244dc7f-c96c-4c8e-95f9-ef146fe55b3f
updated: 1484309162
title: 3-sphere
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Hypersphere_coord.PNG
tags:
    - Definition
    - Properties
    - Elementary properties
    - Topological properties
    - Geometric properties
    - Topological construction
    - Gluing
    - One-point compactification
    - Coordinate systems on the 3-sphere
    - Hyperspherical coordinates
    - Hopf coordinates
    - Stereographic coordinates
    - Group structure
    - In literature
categories:
    - Analytic geometry
---
In mathematics, a 3-sphere (also called a glome) is a higher-dimensional analogue of a sphere. It consists of the set of points equidistant from a fixed central point in 4-dimensional Euclidean space. Analogous to how an ordinary sphere (or 2-sphere) is a two-dimensional surface that forms the boundary of a ball in three dimensions, a 3-sphere is an object with three dimensions that forms the boundary of a ball in four dimensions. A 3-sphere is an example of a 3-manifold.
