---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/men
offline_file: ""
offline_thumbnail: ""
uuid: 6de21686-53a4-47b8-bb43-b675fb7bed23
updated: 1484310448
title: men
categories:
    - Dictionary
---
man
     n 1: an adult male person (as opposed to a woman); "there were
          two women and six men on the bus" [syn: {adult male}]
          [ant: {woman}]
     2: someone who serves in the armed forces; a member of a
        military force; "two men stood sentry duty" [syn: {serviceman},
         {military man}, {military personnel}] [ant: {civilian}]
     3: the generic use of the word to refer to any human being; "it
        was every man for himself"
     4: all of the inhabitants of the earth; "all the world loves a
        lover"; "she always used `humankind' because `mankind'
        seemed to slight the women" [syn: {world}, {human race}, {humanity},
         {humankind}, ...
