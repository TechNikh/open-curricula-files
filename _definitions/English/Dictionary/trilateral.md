---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trilateral
offline_file: ""
offline_thumbnail: ""
uuid: c01384e6-c181-497b-85d3-6e5cb099576d
updated: 1484310273
title: trilateral
categories:
    - Dictionary
---
trilateral
     adj 1: involving three parties or elements; "the triangular
            mother-father-child relationship"; "a trilateral
            agreement"; "a tripartite treaty"; "a tripartite
            division"; "a three-way playoff" [syn: {triangular}, {tripartite},
             {three-party}, {three-way}]
     2: having three sides; "a trilateral figure" [syn: {triangular},
         {three-sided}]
     n : a three-sided polygon [syn: {triangle}, {trigon}]
