---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/European_Journal_of_Combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: 7e39cfce-3a70-4837-aaf0-38b41d0e4206
updated: 1484308577
title: European Journal of Combinatorics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Europe_green_light.png
categories:
    - Combinatorics
---
The European Journal of Combinatorics (usually abbreviated as European J. Combin.),[2] is a peer-reviewed scientific journal for combinatorics. It is an international, bimonthly journal of pure mathematics, specializing in theories arising from combinatorial problems. The journal is primarily open to papers dealing with mathematical structures within combinatorics and/or establishing direct links between combinatorics and other branches of mathematics and the theories of computing. The journal includes full-length research papers, short notes, and research problems on important topics. This journal has been founded by Michel Deza, Michel Las Vergnas and Pierre Rosenstiehl. The current ...
