---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cable
offline_file: ""
offline_thumbnail: ""
uuid: 78943ac1-d748-4f64-b65d-a670aae3b3dd
updated: 1484310210
title: cable
categories:
    - Dictionary
---
cable
     n 1: a telegram sent abroad [syn: {cablegram}, {overseas telegram}]
     2: a conductor for transmitting electrical or optical signals
        or electric power [syn: {line}, {transmission line}]
     3: a very strong thick rope made of twisted hemp or steel wire
     4: a nautical unit of depth [syn: {cable length}, {cable's
        length}]
     5: television that is transmitted over cable directly to the
        receiver [syn: {cable television}]
     6: a television system transmitted over cables [syn: {cable
        television}, {cable system}, {cable television service}]
     v 1: send cables, wires, or telegrams [syn: {telegraph}, {wire}]
     2: fasten with a cable; ...
