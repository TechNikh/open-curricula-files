---
version: 1
type: video
provider: YouTube
id: X9ZZ6tcxArI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 697344b3-b7ec-4816-9a42-9082da37bd94
updated: 1486069684
title: 'The Heart, part 1 - Under Pressure- Crash Course A&P #25'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/X9ZZ6tcxArI/default.jpg
    - https://i3.ytimg.com/vi/X9ZZ6tcxArI/1.jpg
    - https://i3.ytimg.com/vi/X9ZZ6tcxArI/2.jpg
    - https://i3.ytimg.com/vi/X9ZZ6tcxArI/3.jpg
---
