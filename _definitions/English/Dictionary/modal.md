---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/modal
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484401081
title: modal
categories:
    - Dictionary
---
modal
     adj 1: relating to or constituting the most frequent value in a
            distribution; "the modal age at which American
            novelists reach their peak is 30" [syn: {modal(a)}, {average}]
     2: of or relating to a musical mode; especially written in an
        ecclesiastical mode
     3: relating to or expressing the mood of a verb; "modal
        auxiliary"
