---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/standpoint
offline_file: ""
offline_thumbnail: ""
uuid: a63c965c-8a8d-4f9f-b5f0-54e12d5aff30
updated: 1484310563
title: standpoint
categories:
    - Dictionary
---
standpoint
     n : a mental position from which things are viewed; "we should
         consider this problem from the viewpoint of the
         Russians"; "teaching history gave him a special point of
         view toward current events" [syn: {point of view}, {viewpoint},
          {stand}]
