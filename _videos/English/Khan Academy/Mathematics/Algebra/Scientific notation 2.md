---
version: 1
type: video
provider: YouTube
id: vDfd0bj3mo0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Scientific%20notation%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 44262951-d984-4427-b554-5373be6275ed
updated: 1486069559
title: Scientific notation 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/vDfd0bj3mo0/default.jpg
    - https://i3.ytimg.com/vi/vDfd0bj3mo0/1.jpg
    - https://i3.ytimg.com/vi/vDfd0bj3mo0/2.jpg
    - https://i3.ytimg.com/vi/vDfd0bj3mo0/3.jpg
---
