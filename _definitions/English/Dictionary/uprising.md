---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uprising
offline_file: ""
offline_thumbnail: ""
uuid: 5f23d165-daf3-4c51-905a-ff82668ba0ae
updated: 1484310559
title: uprising
categories:
    - Dictionary
---
uprising
     n : organized opposition to authority; a conflict in which one
         faction tries to wrest control from another [syn: {rebellion},
          {insurrection}, {revolt}, {rising}]
