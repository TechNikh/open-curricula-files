---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Close-packing_of_equal_spheres
offline_file: ""
offline_thumbnail: ""
uuid: 606e1065-d447-4830-a64c-fd09c1ad4474
updated: 1484308603
title: Close-packing of equal spheres
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Close_packing_box.svg.png
tags:
    - FCC and HCP Lattices
    - Cannonball problem
    - Positioning and spacing
    - Lattice generation
    - Simple hcp lattice
    - Miller indices
    - Filling the remaining space
    - Notes
categories:
    - Discrete geometry
---
In geometry, close-packing of equal spheres is a dense arrangement of congruent spheres in an infinite, regular arrangement (or lattice). Carl Friedrich Gauss proved that the highest average density – that is, the greatest fraction of space occupied by spheres – that can be achieved by a lattice packing is
