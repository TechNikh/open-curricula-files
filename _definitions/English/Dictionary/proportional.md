---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proportional
offline_file: ""
offline_thumbnail: ""
uuid: b41d251a-d00d-4e09-b2eb-165714eda4af
updated: 1484310228
title: proportional
categories:
    - Dictionary
---
proportional
     adj 1: properly related in size or degree or other measurable
            characteristics; usually followed by `to'; "punishment
            oughtt to be proportional to the crime"; "earnings
            relative to production" [syn: {relative}]
     2: increasing as the amount taxed increases [syn: {graduated}]
     n : one of the quantities in a mathematical proportion
