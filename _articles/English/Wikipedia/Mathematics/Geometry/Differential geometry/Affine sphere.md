---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_sphere
offline_file: ""
offline_thumbnail: ""
uuid: d2b335bf-53e9-4c07-ac8c-6be2f0c32769
updated: 1484309206
title: Affine sphere
categories:
    - Differential geometry
---
In mathematics, and especially differential geometry, an affine sphere is a hypersurface for which the affine normals all intersect in a single point.[1] The term affine sphere is used because they play an analogous role in affine differential geometry to that of ordinary spheres in Euclidean differential geometry.
