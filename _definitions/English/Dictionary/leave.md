---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leave
offline_file: ""
offline_thumbnail: ""
uuid: 5a1f1740-afc8-4b5b-852e-7739740b5dce
updated: 1484310330
title: leave
categories:
    - Dictionary
---
leave
     n 1: the period of time during which you are absent from work or
          duty; "a ten day's leave to visit his mother" [syn: {leave
          of absence}]
     2: permission to do something; "she was granted leave to speak"
     3: the act of departing politely; "he disliked long farewells";
        "he took his leave"; "parting is such sweet sorrow" [syn:
        {farewell}, {leave-taking}, {parting}]
     v 1: go away from a place; "At what time does your train leave?";
          "She didn't leave until midnight"; "The ship leaves at
          midnight" [syn: {go forth}, {go away}] [ant: {arrive}]
     2: go and leave behind, either intentionally or by neglect or
        ...
