---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coastal
offline_file: ""
offline_thumbnail: ""
uuid: f1755d43-1503-478a-901f-14366e9e96c4
updated: 1484310435
title: coastal
categories:
    - Dictionary
---
coastal
     adj 1: of or relating to a coast; "coastal erosion"
     2: located on or near or bordering on a coast; "coastal
        marshes"; "coastal waters"; "the Atlantic coastal plain"
        [ant: {inland}]
