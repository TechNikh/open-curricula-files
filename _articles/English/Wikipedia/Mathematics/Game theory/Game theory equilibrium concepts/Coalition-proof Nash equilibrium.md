---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Coalition-proof_Nash_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: a02510f1-ad8f-42a4-8e56-36f9eb796861
updated: 1484309032
title: Coalition-proof Nash equilibrium
categories:
    - Game theory equilibrium concepts
---
The concept of coalition-proof Nash equilibrium applies to certain "noncooperative" environments in which players can freely discuss their strategies but cannot make binding commitments.[1] It emphasizes the immunization to deviations that are self-enforcing. While the best-response property in Nash equilibrium is necessary for self-enforceability, it is not generally sufficient when players can jointly deviate in a way that is mutually beneficial.
