---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cubic_function
offline_file: ""
offline_thumbnail: ""
uuid: 1933b479-7e73-4620-8b08-19da7e18b573
updated: 1484308912
title: Cubic function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Polynomialdeg3.svg.png
tags:
    - History
    - Critical points and inflection point of a cubic function
    - >
        General solution to the cubic equation with real
        coefficients
    - Algebraic solution
    - The discriminant
    - General formula
    - Multiple roots, Δ = 0
    - Trigonometric and hyperbolic solutions
    - Reduction to a depressed cubic
    - Trigonometric solution for three real roots
    - Hyperbolic solution for one real root
    - Factorization
    - Geometric solutions
    - "Omar Khayyám's solution"
    - Solution with angle trisector
    - Nature of the roots in the case of real coefficients
    - Algebraic nature of the roots
    - Geometric interpretation of the roots
    - Three real roots
    - One real and two complex roots
    - In the Cartesian plane
    - In the complex plane
    - Derivation of the roots
    - "Cardano's method"
    - "Vieta's substitution"
    - "Lagrange's method"
    - Computation of A and B
    - >
        General solution to the cubic equation with arbitrary
        coefficients
    - Galois groups of irreducible cubics
    - Collinearities
    - Symmetry
    - Applications
    - Notes
categories:
    - Elementary algebra
---
where a is nonzero.
