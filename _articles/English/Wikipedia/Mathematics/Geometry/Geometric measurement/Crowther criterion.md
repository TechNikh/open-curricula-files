---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crowther_criterion
offline_file: ""
offline_thumbnail: ""
uuid: 01c58701-d2f8-47e3-9729-ae30f6bd1389
updated: 1484309245
title: Crowther criterion
categories:
    - Geometric measurement
---
The conventional method to evaluate the resolution of an tomography reconstruction is determined by the Crowther criterion.[1] The minimum number of views, m, to reconstruct a particle of diameter D to a resolution of d (=1/R) is given by
