---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proclamation
offline_file: ""
offline_thumbnail: ""
uuid: 311f4b2e-920b-4161-940d-54b66321fada
updated: 1484310172
title: proclamation
categories:
    - Dictionary
---
proclamation
     n 1: a formal public statement; "the government made an
          announcement about changes in the drug war"; "a
          declaration of independence" [syn: {announcement}, {annunciation},
           {declaration}]
     2: the formal act of proclaiming; giving public notice; "his
        promulgation of the policy proved to be premature" [syn: {promulgation}]
