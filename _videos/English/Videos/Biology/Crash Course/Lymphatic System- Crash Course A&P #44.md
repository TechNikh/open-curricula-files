---
version: 1
type: video
provider: YouTube
id: I7orwMgTQ5I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Lymphatic%20System-%20Crash%20Course%20A%26P%20%2344.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 27c885b2-a3b4-4323-814e-6d975e3f0f2e
updated: 1486069680
title: 'Lymphatic System- Crash Course A&P #44'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/I7orwMgTQ5I/default.jpg
    - https://i3.ytimg.com/vi/I7orwMgTQ5I/1.jpg
    - https://i3.ytimg.com/vi/I7orwMgTQ5I/2.jpg
    - https://i3.ytimg.com/vi/I7orwMgTQ5I/3.jpg
---
