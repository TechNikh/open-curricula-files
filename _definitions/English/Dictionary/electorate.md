---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electorate
offline_file: ""
offline_thumbnail: ""
uuid: 15367147-c03e-41da-a37e-5799ac4eabb9
updated: 1484310585
title: electorate
categories:
    - Dictionary
---
electorate
     n : the body of enfranchised citizens; those qualified to vote
