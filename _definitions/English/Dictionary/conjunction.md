---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conjunction
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484546341
title: conjunction
categories:
    - Dictionary
---
conjunction
     n 1: the temporal property of two things happening at the same
          time; "the interval determining the coincidence gate is
          adjustable" [syn: {concurrence}, {coincidence}, {co-occurrence}]
     2: the state of being joined together [syn: {junction}, {conjugation},
         {colligation}]
     3: an uninflected function word that serves to conjoin words or
        phrases or clauses or sentences [syn: {conjunctive}, {connective}]
     4: the grammatical relation between linguistic units (words or
        phrases or clauses) that are connected by a conjunction
     5: (astronomy) apparent meeting or passing of two or more
        celestial bodies in the same ...
