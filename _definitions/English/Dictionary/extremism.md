---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extremism
offline_file: ""
offline_thumbnail: ""
uuid: 1f27111e-b63f-4980-bb9d-b38f44cea204
updated: 1484310188
title: extremism
categories:
    - Dictionary
---
extremism
     n : any political theory favoring immoderate uncompromising
         policies
