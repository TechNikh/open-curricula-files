---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kin
offline_file: ""
offline_thumbnail: ""
uuid: 579752c8-af62-4a06-a91b-0aa73eefa8a4
updated: 1484310543
title: kin
categories:
    - Dictionary
---
kin
     adj : related by blood [syn: {akin(p)}, {blood-related}, {cognate},
            {consanguine}, {consanguineous}, {kin(p)}]
     n 1: a person having kinship with another or others; "he's kin";
          "he's family" [syn: {kinsperson}, {family}]
     2: group of people related by blood or marriage [syn: {kin
        group}, {kinship group}, {kindred}, {clan}, {tribe}]
