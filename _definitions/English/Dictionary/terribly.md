---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terribly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484534461
title: terribly
categories:
    - Dictionary
---
terribly
     adv 1: used as intensifiers; "terribly interesting"; "I'm awful
            sorry" [syn: {awfully}, {awful}, {frightfully}]
     2: in a terrible manner; "she sings terribly" [syn: {atrociously},
         {awfully}, {abominably}, {abysmally}, {rottenly}]
