---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/framework
offline_file: ""
offline_thumbnail: ""
uuid: 25904fef-994e-492f-8f03-2a9bba6f0d01
updated: 1484310297
title: framework
categories:
    - Dictionary
---
framework
     n 1: a simplified description of a complex entity or process;
          "the computer program was based on a model of the
          circulatory and respiratory systems" [syn: {model}, {theoretical
          account}]
     2: the underlying structure; "restoring the framework of the
        bombed building"; "it is part of the fabric of society"
        [syn: {fabric}]
     3: a structure supporting or containing something [syn: {frame},
         {framing}]
