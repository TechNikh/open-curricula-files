---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thunderous
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484525461
title: thunderous
categories:
    - Dictionary
---
thunderous
     adj 1: loud enough to cause (temporary) hearing loss [syn: {deafening},
             {earsplitting}, {roaring}, {thundery}]
     2: extremely ominous; "world events of thunderous import"
