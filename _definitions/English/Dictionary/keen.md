---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/keen
offline_file: ""
offline_thumbnail: ""
uuid: 001fc7d9-1b88-4de7-953d-1ef1bd449fb6
updated: 1484310448
title: keen
categories:
    - Dictionary
---
keen
     adj 1: having or demonstrating ability to recognize or draw fine
            distinctions; "an acute observer of politics and
            politicians"; "incisive comments"; "icy knifelike
            reasoning"; "as sharp and incisive as the stroke of a
            fang"; "penetrating insight"; "frequent penetrative
            observations" [syn: {acute}, {discriminating}, {incisive},
             {knifelike}, {penetrating}, {penetrative}, {piercing},
             {sharp}]
     2: intense or sharp; "suffered exquisite pain"; "felt exquisite
        pleasure" [syn: {exquisite}]
     3: very penetrating and clear and sharp in operation; "an
        incisive mind"; "a keen ...
