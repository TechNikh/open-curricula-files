---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unified
offline_file: ""
offline_thumbnail: ""
uuid: 704c9b02-e804-41f7-85d3-ae5e72d54f7b
updated: 1484310373
title: unified
categories:
    - Dictionary
---
unified
     adj 1: formed or united into a whole [syn: {incorporate}, {incorporated},
             {integrated}, {merged}]
     2: operating as a unit; "a unified utility system"; "a
        coordinated program" [syn: {coordinated}, {interconnected}]
