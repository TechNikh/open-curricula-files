---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Autowave
offline_file: ""
offline_thumbnail: ""
uuid: 5773023c-6005-4450-9549-197d2222910b
updated: 1484308808
title: Autowave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Travelling_wave_for_Fisher_equation.svg.png
tags:
    - Introduction
    - Relevance and significance
    - A brief history of autowave researches
    - The basic definitions
    - The simplest examples
    - Basic properties of autowaves
    - Main known autowave objects
    - One-dimensional autowaves
    - Two-dimensional autowaves
    - Three-dimensional autowaves
    - Examples of autowave processes in nature
    - Autowave regime of boiling
    - Autowaves in chemical solutions
    - Autowave models of biological tissues
    - Autowave models of retina
    - Autowave models of nerve fibres
    - Autowave models of myocardium
    - Autowaves in blood coagulation system
    - The population autowaves
    - Examples of individual-based models of population autowaves
    - Notes
categories:
    - Nonlinear systems
---
Autowaves are self-supporting non-linear waves in active media (i.e. those that provide distributed energy sources). The term is generally used in processes where the waves carry relatively low energy, which is necessary for synchronization or switching the active medium.
