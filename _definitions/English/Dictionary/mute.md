---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mute
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484536141
title: mute
categories:
    - Dictionary
---
mute
     adj 1: expressed without speech; especially because words would be
            inappropriate or inadequate; "a mute appeal"; "a
            silent curse"; "best grief is tongueless"- Emily
            Dickinson; "the words stopped at her lips unsounded";
            "unspoken grief"; "choking exasperation and wordless
            shame"- Thomas Wolfe [syn: {tongueless}, {unspoken}, {wordless}]
     2: lacking power of speech [syn: {tongueless}]
     3: unable to speak because of hereditary deafness [syn: {dumb},
         {silent}]
     n 1: a deaf person who is unable to speak [syn: {deaf-mute}, {deaf-and-dumb
          person}]
     2: a device used to soften the tone of a ...
