---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/generator
offline_file: ""
offline_thumbnail: ""
uuid: c3c4ea9f-ba64-4424-8bde-e4ea39d7a4fe
updated: 1484310198
title: generator
categories:
    - Dictionary
---
generator
     n 1: an apparatus that produces a vapor or gas
     2: engine that converts mechanical energy into electrical
        energy by electromagnetic induction
     3: someone who originates or causes or initiates something; "he
        was the generator of several complaints" [syn: {source}, {author}]
     4: an electronic device for producing a signal voltage
