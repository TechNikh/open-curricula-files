---
version: 1
type: video
provider: YouTube
id: XEYY5qPccxE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1b00ab1e-fce6-4057-9be9-fd2be3187693
updated: 1486069609
title: Relative Complement Or Difference Between Sets
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/XEYY5qPccxE/default.jpg
    - https://i3.ytimg.com/vi/XEYY5qPccxE/1.jpg
    - https://i3.ytimg.com/vi/XEYY5qPccxE/2.jpg
    - https://i3.ytimg.com/vi/XEYY5qPccxE/3.jpg
---
