---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hunt%E2%80%93McIlroy_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 03307fb7-020f-4a74-8c00-bd797f08cc2f
updated: 1484308577
title: Hunt–McIlroy algorithm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Longest_Common_Subsequence_Recursion.png
tags:
    - Algorithm
    - Basic Longest Common Subsequence Solution
    - Algorithm
    - Example
    - Complexity
    - Essential Matches
    - k-candidates
    - Connecting k-candidates
categories:
    - Combinatorics
---
In computer science, the Hunt–McIlroy algorithm is a solution to the longest common subsequence problem. It was one of the first non-heuristic algorithms used in diff. To this day, variations of this algorithm are found in incremental version control systems, wiki engines, and molecular phylogenetics research software.
