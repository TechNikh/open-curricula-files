---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arise
offline_file: ""
offline_thumbnail: ""
uuid: 546297e7-39ab-4b35-8e88-4506e9dd6bb1
updated: 1484310289
title: arise
categories:
    - Dictionary
---
arise
     v 1: come into existence; take on form or shape; "A new religious
          movement originated in that country"; "a love that
          sprang up from friendship"; "the idea for the book grew
          out of a short story"; "An interesting phenomenon
          uprose" [syn: {originate}, {rise}, {develop}, {uprise},
          {spring up}, {grow}]
     2: originate or come into being; "aquestion arose" [syn: {come
        up}, {bob up}]
     3: rise to one's feet; "The audience got up and applauded"
        [syn: {rise}, {uprise}, {get up}, {stand up}] [ant: {sit
        down}, {lie down}]
     4: occur; "A slight unpleasantness arose from this discussion"
        [syn: {come ...
