---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Chamberlain%27s_Theory_of_Strategy'
offline_file: ""
offline_thumbnail: ""
uuid: 4f296b91-85cc-4f29-b868-fbc5d55deaf8
updated: 1484309062
title: "Chamberlain's Theory of Strategy"
tags:
    - Factor 1. What strategy is.
    - Factor 2. The forces that shape strategy.
    - Factor 3. The processes that form strategy.
    - Factor 4. The mechanisms by which strategy can take effect.
    - Usage
    - Current Practical Limitations of Chamberlain’s Theory
    - Note
categories:
    - Strategy
---
Geoffrey P. Chamberlain’s theory of strategy [1] was first published in 2010. The theory draws on the work of Alfred D. Chandler, Jr.,[2] Kenneth R. Andrews,[3] Henry Mintzberg [4] and James Brian Quinn [5] but is more specific and attempts to cover the main areas they did not address. Chamberlain analyzes the strategy construct by treating it as a combination of four factors.
