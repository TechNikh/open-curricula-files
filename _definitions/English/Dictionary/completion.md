---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/completion
offline_file: ""
offline_thumbnail: ""
uuid: 0813c69a-615d-4581-b5ca-8ef489de196c
updated: 1484310333
title: completion
categories:
    - Dictionary
---
completion
     n 1: (American football) a successful forward pass in football
          [syn: {pass completion}]
     2: a concluding action [syn: {culmination}, {closing}, {windup},
         {mop up}]
