---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soft
offline_file: ""
offline_thumbnail: ""
uuid: e8eee248-d5a0-4125-b8db-1b965e425a25
updated: 1484310221
title: soft
categories:
    - Dictionary
---
soft
     adj 1: lacking in hardness relatively or comparatively [ant: {hard}]
     2: metaphorically soft; "my father is a soft touch"; "soft
        light"; "a soft rain"; "a soft Southern drawl"; "soft
        brown eyes"; "a soft glance" [ant: {hard}]
     3: of sound; relatively low in volume; "soft voices"; "soft
        music" [ant: {loud}]
     4: easily hurt; "soft hands"; "a baby's delicate skin" [syn: {delicate}]
     5: used chiefly as a direction or description in music; "the
        piano passages in the composition" [syn: {piano}] [ant: {forte}]
     6: used of beverages; not containing alcohol; "nonalcoholic
        beverages"; "soft drinks" [syn: {nonalcoholic}] [ant: ...
