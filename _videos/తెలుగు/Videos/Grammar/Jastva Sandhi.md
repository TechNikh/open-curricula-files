---
version: 1
type: video
provider: YouTube
id: i92ydAZuC7w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 92dd0545-f8d0-4bb9-90fb-45d541554cd3
updated: 1486069628
title: Jastva Sandhi
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/i92ydAZuC7w/default.jpg
    - https://i3.ytimg.com/vi/i92ydAZuC7w/1.jpg
    - https://i3.ytimg.com/vi/i92ydAZuC7w/2.jpg
    - https://i3.ytimg.com/vi/i92ydAZuC7w/3.jpg
---
