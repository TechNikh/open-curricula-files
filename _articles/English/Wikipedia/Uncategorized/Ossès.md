---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Osses
offline_file: ""
offline_thumbnail: ""
uuid: 6e203dee-ca05-4423-9297-44af72f01343
updated: 1484308440
title: Ossès
tags:
    - geography
    - Neighboring communes
    - History
    - Sights
    - Economy
    - Transport
    - Village festival
categories:
    - Uncategorized
---
Ossès lies in the traditional Basque province of Lower Navarre.
