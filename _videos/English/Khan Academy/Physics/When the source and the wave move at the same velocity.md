---
version: 1
type: video
provider: YouTube
id: 4Xgk3vusxT8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/When%20the%20source%20and%20the%20wave%20move%20at%20the%20same%20velocity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 45cc23e6-96de-4363-8be8-475ae045256b
updated: 1486069595
title: When the source and the wave move at the same velocity
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/4Xgk3vusxT8/default.jpg
    - https://i3.ytimg.com/vi/4Xgk3vusxT8/1.jpg
    - https://i3.ytimg.com/vi/4Xgk3vusxT8/2.jpg
    - https://i3.ytimg.com/vi/4Xgk3vusxT8/3.jpg
---
What happens when both the listener and the source are moving together? Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/mechanical-waves-and-sound/doppler-effect/v/doppler-effect-for-a-moving-observer?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/mechanical-waves-and-sound/doppler-effect/v/doppler-effect-formula-when-source-is-moving-away?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
