---
version: 1
type: video
provider: YouTube
id: AbmQNC-iE84
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Exponent%20Rules%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 51b6570c-96d5-410f-a3b8-d79ff027ad3a
updated: 1486069559
title: Exponent Rules 3
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/AbmQNC-iE84/default.jpg
    - https://i3.ytimg.com/vi/AbmQNC-iE84/1.jpg
    - https://i3.ytimg.com/vi/AbmQNC-iE84/2.jpg
    - https://i3.ytimg.com/vi/AbmQNC-iE84/3.jpg
---
