---
version: 1
type: video
provider: YouTube
id: 8F0gdO643Tc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Eigenvalues%20and%20Eigenvectors%2C%20Imaginary%20and%20Real.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cdb37786-b67d-4380-95db-e3c323f6dc59
updated: 1486069657
title: Eigenvalues and Eigenvectors, Imaginary and Real
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/8F0gdO643Tc/default.jpg
    - https://i3.ytimg.com/vi/8F0gdO643Tc/1.jpg
    - https://i3.ytimg.com/vi/8F0gdO643Tc/2.jpg
    - https://i3.ytimg.com/vi/8F0gdO643Tc/3.jpg
---
