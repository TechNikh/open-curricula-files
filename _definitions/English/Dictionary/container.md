---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/container
offline_file: ""
offline_thumbnail: ""
uuid: 749cd648-627b-44ac-b387-54afb9ab58df
updated: 1484310230
title: container
categories:
    - Dictionary
---
container
     n : any object that can be used to hold things (especially a
         large metal boxlike object of standardized dimensions
         that can be loaded from one form of transport to another)
