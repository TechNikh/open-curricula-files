---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Betwa
offline_file: ""
offline_thumbnail: ""
uuid: d2026f2d-01fe-4a52-aa7f-f4e478355020
updated: 1484308438
title: Betwa River
tags:
    - History
    - future
    - Dams
categories:
    - Uncategorized
---
The Betwa or Betravati is a river in Northern India, and a tributary of the Yamuna. Also known as the Vetravati, the Betwa rises in the Vindhya Range just north of Hoshangabad in Madhya Pradesh and flows north-east through Madhya Pradesh and Orchha to Uttar Pradesh. Nearly half of its course, which is not navigable, runs over the Malwa Plateau. The confluence of the Betwa and the Yamuna Rivers is Hamirpur town in Uttar Pradesh, in the vicinity of Orchha.[2]
