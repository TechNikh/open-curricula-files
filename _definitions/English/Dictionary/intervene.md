---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intervene
offline_file: ""
offline_thumbnail: ""
uuid: f4c8a59f-1055-4dea-86a6-b37ec574f7a4
updated: 1484310561
title: intervene
categories:
    - Dictionary
---
intervene
     v 1: get involved, so as to alter or hinder an action, or through
          force or threat of force; "Why did the U.S. not
          intervene earlier in WW II?" [syn: {step in}, {interfere},
           {interpose}]
     2: be placed or located between other things or extend between
        spaces and events; "This interludes intervenes between the
        two movements"; "Eight days intervened"
     3: occur between other event or between certain points of time;
        "the war intervened between the birth of her two children"
