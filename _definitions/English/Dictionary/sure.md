---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sure
offline_file: ""
offline_thumbnail: ""
uuid: 35b4679e-032e-4f4a-8533-2487f81b0140
updated: 1484310321
title: sure
categories:
    - Dictionary
---
sure
     adj 1: having or feeling no doubt or uncertainty; confident and
            assured; "felt certain of success"; "was sure (or
            certain) she had seen it"; "was very sure in his
            beliefs"; "sure of her friends" [syn: {certain(p)}]
            [ant: {uncertain}, {uncertain}]
     2: exercising or taking care great enough to bring assurance;
        "be certain to disconnect the iron when you are through";
        "be sure to lock the doors" [syn: {certain}]
     3: certain to occur; destined or inevitable; "he was certain to
        fail"; "his fate is certain"; "In this life nothing is
        certain but death and taxes"- Benjamin Franklin; "he faced
        ...
