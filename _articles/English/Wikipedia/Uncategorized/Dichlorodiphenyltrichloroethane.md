---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ddt
offline_file: ""
offline_thumbnail: ""
uuid: 6eebfd98-69c4-4c49-96c2-98533f806c75
updated: 1484308466
title: Dichlorodiphenyltrichloroethane
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/175px-Radical_DDT.JPG
tags:
    - Properties and chemistry
    - Isomers and related compounds
    - Production and use
    - Mechanism of insecticide action
    - History
    - Use in the 1940s and 1950s
    - United States ban
    - Restrictions on usage
    - Environmental impact
    - Effects on wildlife and eggshell thinning
categories:
    - Uncategorized
---
Dichlorodiphenyltrichloroethane (DDT) is a colorless, crystalline, tasteless, and almost odorless organochlorine known for its insecticidal properties and environmental impacts. DDT has been formulated in multiple forms, including solutions in xylene or petroleum distillates, emulsifiable concentrates, water-wettable powders, granules, aerosols, smoke candles and charges for vaporizers and lotions.[7][8]
