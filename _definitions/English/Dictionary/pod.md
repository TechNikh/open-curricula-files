---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pod
offline_file: ""
offline_thumbnail: ""
uuid: 2ce20f71-780f-4abc-b8ac-53ef3a6199f3
updated: 1484310307
title: pod
categories:
    - Dictionary
---
pod
     n 1: the vessel that contains the seeds of a plant (not the seeds
          themselves) [syn: {cod}, {seedcase}]
     2: a several-seeded dehiscent fruit as e.g. of a leguminous
        plant [syn: {seedpod}]
     3: a group of aquatic mammals
     4: a detachable container of fuel on an airplane [syn: {fuel
        pod}]
     v 1: take something out of its shell or pod; "pod peas or beans"
     2: produce pods, of plants
     [also: {podding}, {podded}]
