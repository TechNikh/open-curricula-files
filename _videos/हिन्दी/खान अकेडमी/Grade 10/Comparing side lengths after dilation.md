---
version: 1
type: video
provider: YouTube
id: W2jR7TIJVLY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 682061e5-b720-455f-b2a7-5877af7aa268
updated: 1486069617
title: Comparing side lengths after dilation
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/W2jR7TIJVLY/default.jpg
    - https://i3.ytimg.com/vi/W2jR7TIJVLY/1.jpg
    - https://i3.ytimg.com/vi/W2jR7TIJVLY/2.jpg
    - https://i3.ytimg.com/vi/W2jR7TIJVLY/3.jpg
---
