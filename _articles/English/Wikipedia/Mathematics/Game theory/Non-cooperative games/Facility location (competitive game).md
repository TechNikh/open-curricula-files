---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Facility_location_(competitive_game)
offline_file: ""
offline_thumbnail: ""
uuid: 24dd52eb-cacc-41d3-bcd3-733f621a70ff
updated: 1484309055
title: Facility location (competitive game)
categories:
    - Non-cooperative games
---
The competitive facility location game is a kind of competitive game in which service-providers select locations to place their facilities in order to maximize their profits.[1][2]:502-506 The game has the following components:
