---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plastic
offline_file: ""
offline_thumbnail: ""
uuid: ebb57f86-bba0-49e3-b1b6-7045667f2cb3
updated: 1484310238
title: plastic
categories:
    - Dictionary
---
plastic
     adj 1: used of the imagination; "material...transformed by the
            plastic power of the imagination"--Coleridge
     2: capable of being molded or modeled (especially of earth or
        clay or other soft material); "plastic substances such as
        wax or clay" [syn: {fictile}, {moldable}]
     3: capable of being influenced or formed; "the plastic minds of
        children"; "a pliant nature" [syn: {pliant}]
     n : generic name for certain synthetic or semisynthetic
         materials that can be molded or extruded into objects or
         films or filaments or used for making e.g. coatings and
         adhesives
