---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lap
offline_file: ""
offline_thumbnail: ""
uuid: 18415367-7482-4a64-b41f-49f64463255d
updated: 1484310543
title: lap
categories:
    - Dictionary
---
lap
     n 1: the upper side of the thighs of a seated person; "he picked
          up the little girl and plopped her down in his lap"
     2: an area of control or responsibility; "the job fell right in
        my lap"
     3: the part of a piece of clothing that covers the thighs; "his
        lap was covered with food stains" [syn: {lap covering}]
     4: a flap that lies over another part; "the lap of the shingles
        should be at least ten inches" [syn: {overlap}]
     5: movement once around a course; "he drove an extra lap just
        for insurance" [syn: {circle}, {circuit}]
     6: touching with the tongue; "the dog's laps were warm and wet"
        [syn: {lick}]
     v 1: ...
