---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Paul_%C3%89mile_Appell'
offline_file: ""
offline_thumbnail: ""
uuid: ee06b0a0-595d-4550-8bb6-078a7f702f3f
updated: 1484309073
title: Paul Émile Appell
tags:
    - life
    - Work
    - Appell series
    - Mechanics
    - Publications
categories:
    - Geometers
---
Paul Appell (27 September 1855 in Strasbourg – 24 October 1930 in Paris), also known as Paul Émile Appel, was a French mathematician and Rector of the University of Paris. The concept of Appell polynomials is named after him, as is rue Paul Appell in the 14th arrondissement of Paris and the minor planet 988 Appella.
