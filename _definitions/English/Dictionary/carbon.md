---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carbon
offline_file: ""
offline_thumbnail: ""
uuid: 89bf0cb3-077f-4280-b93e-9880ca88ab37
updated: 1484310319
title: carbon
categories:
    - Dictionary
---
carbon
     n 1: an abundant nonmetallic tetravalent element occurring in
          three allotropic forms: amorphous carbon and graphite
          and diamond; occurs in all organic compounds [syn: {C},
          {atomic number 6}]
     2: a thin paper coated on one side with a dark waxy substance
        (often containing carbon); used to transfer characters
        from the original to an under sheet of paper [syn: {carbon
        paper}]
     3: a copy made with carbon paper [syn: {carbon copy}]
