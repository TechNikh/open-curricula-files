---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coprime_integers
offline_file: ""
offline_thumbnail: ""
uuid: 5dd00f87-1e24-4724-8b46-1a2c3cbd081d
updated: 1484308627
title: Coprime integers
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Coprime-lattice.svg.png
tags:
    - Properties
    - Coprimality in sets
    - Coprimality in ring ideals
    - Probabilities
    - Generating all coprime pairs
categories:
    - Number theory
---
In number theory, two integers a and b are said to be relatively prime, mutually prime, or coprime (also spelled co-prime)[1] if the only positive integer that divides both of them is 1. That is, the only common positive factor of the two numbers is 1. This is equivalent to their greatest common divisor being 1.[2] The numerator and denominator of a reduced fraction are coprime. In addition to 
  
    
      
        gcd
        (
        a
        ,
        b
        )
        =
        1
        
      
    
    {\displaystyle \gcd(a,b)=1\;}
  
 and 
  
    
      
        (
        a
        ,
        b
        )
        =
        1
        ,
        
      
    
    {\displaystyle ...
