---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accidentally
offline_file: ""
offline_thumbnail: ""
uuid: 22472293-a682-4821-a926-9ce5af9f3fa1
updated: 1484310315
title: accidentally
categories:
    - Dictionary
---
accidentally
     adv 1: without advance planning; "they met accidentally" [syn: {by
            chance}, {circumstantially}, {unexpectedly}] [ant: {intentionally}]
     2: in an incidental manner; "these magnificent achievements
        were only incidentally influenced by Oriental models"
        [syn: {incidentally}, {by chance}]
     3: without intention; in an unintentional manner; "she hit him
        unintentionally" [syn: {unintentionally}] [ant: {intentionally},
         {intentionally}]
