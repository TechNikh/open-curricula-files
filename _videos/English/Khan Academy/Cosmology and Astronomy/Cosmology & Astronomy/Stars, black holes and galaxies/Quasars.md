---
version: 1
type: video
provider: YouTube
id: 4LmIyMyAuN0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Quasars.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7afe6079-50b2-488b-bd80-a10946f44820
updated: 1486069685
title: Quasars
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/4LmIyMyAuN0/default.jpg
    - https://i3.ytimg.com/vi/4LmIyMyAuN0/1.jpg
    - https://i3.ytimg.com/vi/4LmIyMyAuN0/2.jpg
    - https://i3.ytimg.com/vi/4LmIyMyAuN0/3.jpg
---
