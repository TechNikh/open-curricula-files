---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/executed
offline_file: ""
offline_thumbnail: ""
uuid: aba4f205-2efe-4a2c-9e95-f83c06e628c1
updated: 1484310581
title: executed
categories:
    - Dictionary
---
executed
     adj : put to death as punishment; "claimed the body of the
           executed traitor"
