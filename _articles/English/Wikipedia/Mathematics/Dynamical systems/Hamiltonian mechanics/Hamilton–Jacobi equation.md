---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hamilton%E2%80%93Jacobi_equation'
offline_file: ""
offline_thumbnail: ""
uuid: 2332f761-3d6a-430c-8810-4c094cbf315e
updated: 1484308791
title: Hamilton–Jacobi equation
tags:
    - Notation
    - Mathematical formulation
    - Comparison with other formulations of mechanics
    - Derivation
    - "Action and Hamilton's functions"
    - Separation of variables
    - Examples in various coordinate systems
    - Spherical coordinates
    - Elliptic cylindrical coordinates
    - Parabolic cylindrical coordinates
    - >
        Eikonal approximation and relationship to the Schrödinger
        equation
    - HJE in a gravitational field
    - HJE in electromagnetic fields
categories:
    - Hamiltonian mechanics
---
In mathematics, the Hamilton–Jacobi equation (HJE) is a necessary condition describing extremal geometry in generalizations of problems from the calculus of variations, and is a special case of the Hamilton–Jacobi–Bellman equation. It is named for William Rowan Hamilton and Carl Gustav Jacob Jacobi. In physics, it is a formulation of classical mechanics, equivalent to other formulations such as Newton's laws of motion[citation needed], Lagrangian mechanics and Hamiltonian mechanics. The Hamilton–Jacobi equation is particularly useful in identifying conserved quantities for mechanical systems, which may be possible even when the mechanical problem itself cannot be solved completely.
