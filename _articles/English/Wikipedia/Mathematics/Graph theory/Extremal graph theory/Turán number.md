---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tur%C3%A1n_number'
offline_file: ""
offline_thumbnail: ""
uuid: 9a52dee2-235c-4171-8676-d014f7421257
updated: 1484309409
title: Turán number
tags:
    - Definitions
    - Example
    - Relations to other combinatorial designs
    - Bibliography
categories:
    - Extremal graph theory
---
In mathematics, the Turán number T(n,k,r) for r-uniform hypergraphs of order n is the smallest number of r-edges such that every induced subgraph on k vertices contains an edge. This number was determined for r = 2 by Turán (1941), and the problem for general r was introduced in Turán (1961). The paper (Sidorenko 1995) gives a survey of Turán numbers.
