---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/haughtily
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484650561
title: haughtily
categories:
    - Dictionary
---
haughtily
     adv : in a haughty manner; "he peered haughtily down his nose"
