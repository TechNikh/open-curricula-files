---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sperm
offline_file: ""
offline_thumbnail: ""
uuid: 069527fa-4a2c-4ed5-bc9e-7ea9dfd1b3a3
updated: 1484308347
title: Sperm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Complete_diagram_of_a_human_spermatozoa_en.svg.png
tags:
    - Sperm in animals
    - Anatomy
    - Origin
    - Sperm quality
    - Sperm size
    - Market for human sperm
    - History
    - Forensic analysis
    - Sperm in plants
    - Motile sperm cells
    - Non-motile sperm cells
    - Sperm nuclei
categories:
    - Uncategorized
---
Sperm is the male reproductive cell and is derived from the Greek word (σπέρμα) sperma (meaning "seed"). In the types of sexual reproduction known as anisogamy and its subtype oogamy, there is a marked difference in the size of the gametes with the smaller one being termed the "male" or sperm cell. A uniflagellar sperm cell that is motile is referred to as a spermatozoon, whereas a non-motile sperm cell is referred to as a spermatium. Sperm cells cannot divide and have a limited life span, but after fusion with egg cells during fertilization, a new organism begins developing, starting as a totipotent zygote. The human sperm cell is haploid, so that its 23 chromosomes can join the 23 ...
