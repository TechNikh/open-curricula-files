---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/myanmar
offline_file: ""
offline_thumbnail: ""
uuid: d5ebb08c-fe16-41d6-8bcf-467aa0b5d977
updated: 1484310439
title: myanmar
categories:
    - Dictionary
---
Myanmar
     n : a mountainous republic in southeastern Asia on the Bay of
         Bengal; "much opium is grown in Myanmar" [syn: {Union of
         Burma}, {Burma}]
