---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/softly
offline_file: ""
offline_thumbnail: ""
uuid: 9d426210-64d6-4a45-9343-ad824f40c85e
updated: 1484310146
title: softly
categories:
    - Dictionary
---
softly
     adv 1: with low volume; "speak softly but carry a big stick"; "she
            spoke quietly to the child"; "the radio was playing
            softly" [syn: {quietly}] [ant: {loudly}]
     2: in a manner that is pleasing to the senses; "she smiled
        softly"
     3: with little weight or force; "she kissed him lightly on the
        forehead" [syn: {lightly}, {gently}]
     4: used as a direction in music; to be played relatively softly
        [syn: {piano}] [ant: {forte}]
