---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consider
offline_file: ""
offline_thumbnail: ""
uuid: 7751401d-4cca-4c4e-a5a5-5c79e1dfb241
updated: 1484310313
title: consider
categories:
    - Dictionary
---
consider
     v 1: deem to be; "She views this quite differently from me"; "I
          consider her to be shallow"; "I don't see the situation
          quite as negatively as you do" [syn: {see}, {reckon}, {view},
           {regard}]
     2: give careful consideration to; "consider the possibility of
        moving" [syn: {study}]
     3: take into consideration for exemplifying purposes; "Take the
        case of China"; "Consider the following case" [syn: {take},
         {deal}, {look at}]
     4: show consideration for; take into account; "You must
        consider her age"; "The judge considered the offender's
        youth and was lenient" [syn: {count}, {weigh}]
     5: think ...
