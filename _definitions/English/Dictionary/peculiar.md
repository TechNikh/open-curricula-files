---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peculiar
offline_file: ""
offline_thumbnail: ""
uuid: 158e1379-19d9-432a-b05a-a1e0d3a57939
updated: 1484310214
title: peculiar
categories:
    - Dictionary
---
peculiar
     adj 1: beyond or deviating from the usual or expected; "a curious
            hybrid accent"; "her speech has a funny twang"; "they
            have some funny ideas about war"; "had an odd name";
            "the peculiar aromatic odor of cloves"; "something
            definitely queer about this town"; "what a rum
            fellow"; "singular behavior" [syn: {curious}, {funny},
             {odd}, {queer}, {rum}, {rummy}, {singular}]
     2: unique or specific to a person or thing or category; "the
        particular demands of the job"; "has a paraticular
        preference for Chinese art"; "a peculiar bond of sympathy
        between them"; "an expression peculiar to ...
