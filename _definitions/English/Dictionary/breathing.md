---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breathing
offline_file: ""
offline_thumbnail: ""
uuid: 3c7a2e32-db9d-4de7-9c14-ed86fe7ccb4d
updated: 1484310459
title: breathing
categories:
    - Dictionary
---
breathing
     adj : passing or able to pass air in and out of the lungs
           normally; sometimes used in combination; "the boy was
           disappointed to find only skeletons instead of living
           breathing dinosaurs"; "the heavy-breathing person on
           the telephone" [syn: {eupneic}, {eupnoeic}] [ant: {breathless}]
     n : the bodily process of inhalation and exhalation; the process
         of taking in oxygen from inhaled air and releasing carbon
         dioxide by exhalation [syn: {external respiration}, {respiration},
          {ventilation}]
