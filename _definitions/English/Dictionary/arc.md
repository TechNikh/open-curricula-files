---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arc
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484331721
title: arc
categories:
    - Dictionary
---
arc
     n 1: electrical conduction through a gas in an applied electric
          field [syn: {discharge}, {spark}, {electric arc}, {electric
          discharge}]
     2: a continuous portion of a circle
     3: something curved in shape [syn: {bow}]
     v : form an arch or curve; "her back arches"; "her hips curve
         nicely" [syn: {arch}, {curve}]
     [also: {arcking}, {arcked}]
