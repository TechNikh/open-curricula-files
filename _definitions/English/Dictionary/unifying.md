---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unifying
offline_file: ""
offline_thumbnail: ""
uuid: c7a4d249-50f7-4699-84ed-9f937bc66777
updated: 1484310609
title: unifying
categories:
    - Dictionary
---
unifying
     adj 1: combining into a single unit [syn: {consolidative}]
     2: tending to unify [syn: {centripetal}, {unifying(a)}]
