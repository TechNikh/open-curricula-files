---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thinner
offline_file: ""
offline_thumbnail: ""
uuid: 23ead364-f57a-416d-974f-870d2a6c139b
updated: 1484310214
title: thinner
categories:
    - Dictionary
---
thin
     adj 1: of relatively small extent from one surface to the opposite
            or in cross section; "thin wire"; "a thin chiffon
            blouse"; "a thin book"; "a thin layer of paint" [ant:
            {thick}]
     2: lacking excess flesh; "you can't be too rich or too thin";
        "Yon Cassius has a lean and hungry look"-Shakespeare [syn:
         {lean}] [ant: {fat}]
     3: very narrow; "a thin line across the page" [syn: {slender}]
     4: having little substance or significance; "a flimsy excuse";
        "slight evidence"; "a tenuous argument"; "a thin plot"
        [syn: {flimsy}, {slight}, {tenuous}]
     5: not dense; "a thin beard"; "trees were sparse" [syn: ...
