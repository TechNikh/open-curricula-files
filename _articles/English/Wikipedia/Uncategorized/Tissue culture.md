---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tissue_culture
offline_file: ""
offline_thumbnail: ""
uuid: f43f937e-77fc-4e70-953f-14624ca8219b
updated: 1484308366
title: Tissue culture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tissue_culture_vials_nci-vol-2142-300.jpg
tags:
    - Historical usage
    - Modern usage
categories:
    - Uncategorized
---
Tissue culture is the growth of tissues or cells separate from the organism. This is typically facilitated via use of a liquid, semi-solid, or solid growth medium, such as broth or agar. Tissue culture commonly refers to the culture of animal cells and tissues, with the more specific term plant tissue culture being used for plants. The term "tissue culture" was coined by American pathologist Montrose Thomas Burrows, M.D.[1]
