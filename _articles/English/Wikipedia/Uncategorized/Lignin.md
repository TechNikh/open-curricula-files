---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lignin
offline_file: ""
offline_thumbnail: ""
uuid: 5657df3d-46f1-468b-90bf-74cde91bfc75
updated: 1484308340
title: Lignin
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-LigninStructure.png
tags:
    - History
    - Composition
    - Biological function
    - Ecological function
    - Economic significance
    - Structure
    - Biosynthesis
    - Biodegradation
    - Pyrolysis
    - Chemical analysis
categories:
    - Uncategorized
---
Lignin is a class of complex organic polymers that form important structural materials in the support tissues of vascular plants and some algae.[1] Lignins are particularly important in the formation of cell walls, especially in wood and bark, because they lend rigidity and do not rot easily. Chemically, lignins are cross-linked phenolic polymers.[2]
