---
version: 1
type: article
id: https://en.wikipedia.org/wiki/E._Sampathkumar
offline_file: ""
offline_thumbnail: ""
uuid: 391b8563-4b41-46e6-9c9d-39079008da0f
updated: 1484309476
title: E. Sampathkumar
categories:
    - Graph theorists
---
E. Sampathkumar (born June 10, 1936) is a professor emeritus[1] of graph theory from University of Mysore. He has contributed to domination number, bipartite double cover, and reconstruction theory. He was chairman of the department of mathematics of the Karnataka university, Dharwar[2] and the University of Mysore (1992–95).[3]
