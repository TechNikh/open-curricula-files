---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Richard_Rado
offline_file: ""
offline_thumbnail: ""
uuid: 801190a6-f952-4dba-9c5f-27a079979be5
updated: 1484309473
title: Richard Rado
categories:
    - Graph theorists
---
Richard Rado FRS[1] (28 April 1906 – 23 December 1989) was a German-born British mathematician whose research concerned combinatorics and graph theory. He was Jewish and left Germany to escape Nazi persecution. He earned two Ph.D.s: in 1933 from the University of Berlin, and in 1935 from the University of Cambridge.[2][3][4] He was interviewed in Berlin by Lord Cherwell for a scholarship given by the chemist Sir Robert Mond which provided financial support to study at Cambridge. After he was awarded the scholarship, Rado and his wife left for the UK in 1933.
