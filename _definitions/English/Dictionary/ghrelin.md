---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ghrelin
offline_file: ""
offline_thumbnail: ""
uuid: 7dd4607c-59a0-43b7-af74-aac053ee102c
updated: 1484310357
title: ghrelin
categories:
    - Dictionary
---
ghrelin
     n : a hormone produced by stomach cells; "ghrelin is thought to
         increase feelings of hunger"
