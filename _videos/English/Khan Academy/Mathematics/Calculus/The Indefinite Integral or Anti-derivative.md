---
version: 1
type: video
provider: YouTube
id: xRspb-iev-g
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/The%20Indefinite%20Integral%20or%20Anti-derivative.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f2753840-bbec-4168-9dd3-af2f30492333
updated: 1486069694
title: The Indefinite Integral or Anti-derivative
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/xRspb-iev-g/default.jpg
    - https://i3.ytimg.com/vi/xRspb-iev-g/1.jpg
    - https://i3.ytimg.com/vi/xRspb-iev-g/2.jpg
    - https://i3.ytimg.com/vi/xRspb-iev-g/3.jpg
---
