---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/analogy
offline_file: ""
offline_thumbnail: ""
uuid: 0a1951d6-6c7f-450e-be17-8a933b09e737
updated: 1484310210
title: analogy
categories:
    - Dictionary
---
analogy
     n 1: an inference that if things agree in some respects they
          probably agree in others
     2: drawing a comparison in order to show a similarity in some
        respect; "the operation of a computer presents and
        interesting analogy to the working of the brain"; "the
        models show by analogy how matter is built up"
     3: the religious belief that between creature and creator no
        similarity can be found so great but that the
        dissimilarity is always greater; language can point in the
        right direction but any analogy between God and humans
        will always be inadequate [syn: {doctrine of analogy}]
        [ant: {apophatism}, ...
