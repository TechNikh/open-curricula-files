---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/antarctica
offline_file: ""
offline_thumbnail: ""
uuid: 3ce52fb9-2513-43f4-926b-bda24f41c53a
updated: 1484310249
title: antarctica
categories:
    - Dictionary
---
Antarctica
     n : an extremely cold continent at the south pole almost
         entirely below the Antarctic Circle; covered by an ice
         cap up to 13,000 feet deep [syn: {Antarctic continent}]
