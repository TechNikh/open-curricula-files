---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conspire
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484554921
title: conspire
categories:
    - Dictionary
---
conspire
     v 1: engage in plotting or enter into a conspiracy, swear
          together; "They conspired to overthrow the government"
          [syn: {cabal}, {complot}, {conjure}, {machinate}]
     2: act in unison or agreement and in secret towards a deceitful
        or illegal purpose; "The two companies conspired to cause
        the value of the stock to fall" [syn: {collude}]
