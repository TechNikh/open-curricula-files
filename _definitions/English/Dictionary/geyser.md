---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geyser
offline_file: ""
offline_thumbnail: ""
uuid: 44f25c08-86ea-4832-aa7a-ea384d85d38e
updated: 1484310224
title: geyser
categories:
    - Dictionary
---
geyser
     n : a spring that discharges hot water and steam
     v : to overflow like a geyser
