---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/notice
offline_file: ""
offline_thumbnail: ""
uuid: ffe7fe55-bbfc-49ca-b6f2-edc74bf38309
updated: 1484310275
title: notice
categories:
    - Dictionary
---
notice
     n 1: an announcement containing information about a future event;
          "you didn't give me enough notice"
     2: the act of noticing or paying attention; "he escaped the
        notice of the police" [syn: {observation}, {observance}]
     3: a request for payment; "the notification stated the grace
        period and the penalties for defaulting" [syn: {notification}]
     4: advance notification (usually written) of the intention to
        withdraw from an arrangement or contract; "we received a
        notice to vacate the premises"; "he gave notice two months
        before he moved"
     5: a sign posted in a public place as an advertisement; "a
        poster ...
