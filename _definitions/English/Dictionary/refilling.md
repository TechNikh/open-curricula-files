---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refilling
offline_file: ""
offline_thumbnail: ""
uuid: 702dcc2d-55f3-4aa7-baca-2034894bb885
updated: 1484310249
title: refilling
categories:
    - Dictionary
---
refilling
     n : filling again by supplying what has been used up [syn: {replenishment},
          {replacement}, {renewal}]
