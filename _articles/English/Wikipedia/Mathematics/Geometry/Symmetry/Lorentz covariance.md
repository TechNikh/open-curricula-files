---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lorentz_covariance
offline_file: ""
offline_thumbnail: ""
uuid: d74c230a-e147-496c-b5bd-2d1ff65d98c9
updated: 1484309313
title: Lorentz covariance
tags:
    - Examples
    - Scalars
    - Four-vectors
    - Four-tensors
    - Lorentz violating models
categories:
    - Symmetry
---
In physics, Lorentz symmetry, named for Hendrik Lorentz, is "the feature of nature that says experimental results are independent of the orientation or the boost velocity of the laboratory through space".[1] In everyday language, it means that the laws of physics stay the same for all observers that are moving with respect to one another with a uniform velocity. Lorentz covariance, a related concept, is a key property of spacetime following from the special theory of relativity. Lorentz covariance has two distinct, but closely related meanings:
