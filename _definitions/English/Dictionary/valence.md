---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/valence
offline_file: ""
offline_thumbnail: ""
uuid: bb7498c8-ceea-4222-ab67-038d970f8653
updated: 1484310399
title: valence
categories:
    - Dictionary
---
valence
     n 1: (biology) a relative capacity to unite or react or interact
          as with antigens or a biological substrate [syn: {valency}]
     2: (chemistry) a property of atoms or radicals; their combining
        power given in terms of the number of hydrogen atoms (or
        the equivalent) [syn: {valency}]
