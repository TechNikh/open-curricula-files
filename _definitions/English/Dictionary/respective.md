---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/respective
offline_file: ""
offline_thumbnail: ""
uuid: 6c0ad50c-e418-42ba-ba68-c886643c3e39
updated: 1484310204
title: respective
categories:
    - Dictionary
---
respective
     adj : considered individually; "the respective club members";
           "specialists in their several fields"; "the various
           reports all agreed" [syn: {respective(a)}, {several(a)},
            {various(a)}]
