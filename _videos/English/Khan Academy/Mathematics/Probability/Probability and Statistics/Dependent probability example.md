---
version: 1
type: video
provider: YouTube
id: xPUm5SUVzTE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Dependent%20probability%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e191cbec-bd8f-49c4-8f2c-9c637dbc49de
updated: 1486069688
title: Dependent probability example
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/xPUm5SUVzTE/default.jpg
    - https://i3.ytimg.com/vi/xPUm5SUVzTE/1.jpg
    - https://i3.ytimg.com/vi/xPUm5SUVzTE/2.jpg
    - https://i3.ytimg.com/vi/xPUm5SUVzTE/3.jpg
---
