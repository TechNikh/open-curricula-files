---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dibang
offline_file: ""
offline_thumbnail: ""
uuid: 85736ef9-1bb0-487f-9bbd-abc66ecefff7
updated: 1484308435
title: Dibang
tags:
    - Career
    - Filmography
categories:
    - Uncategorized
---
Dibang (दिबांग in Hindi) is a senior journalist and news anchor. He hosts daily prime-time show 'Jan Man' and weekly show 'Press Conference' on the Hindi news channel ABP News. A member of debate panels of the channel, he is rated among the best Hindi anchors in the industry today.[1]
