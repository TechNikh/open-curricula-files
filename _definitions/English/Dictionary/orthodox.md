---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/orthodox
offline_file: ""
offline_thumbnail: ""
uuid: ef3d8fdf-ee18-4f2b-b1d6-beea9c2d3d33
updated: 1484310188
title: orthodox
categories:
    - Dictionary
---
Orthodox
     adj 1: of or pertaining to or characteristic of Judaism; "Orthodox
            Judaism" [syn: {Jewish-Orthodox}]
     2: adhering to what is commonly accepted; "an orthodox view of
        the world" [ant: {unorthodox}]
     3: of or relating to or characteristic of the Eastern Orthodox
        Church [syn: {Eastern Orthodox}, {Russian Orthodox}, {Greek
        Orthodox}]
