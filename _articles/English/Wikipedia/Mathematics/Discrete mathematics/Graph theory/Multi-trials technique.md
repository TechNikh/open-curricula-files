---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multi-trials_technique
offline_file: ""
offline_thumbnail: ""
uuid: e4b02bcb-a137-42f0-913e-7a3b155a75de
updated: 1484308630
title: Multi-trials technique
categories:
    - Graph theory
---
The multi-trials technique by Schneider et al. is employed for distributed algorithms and allows to break symmetry efficiently. Symmetry breaking is necessary, for instance, in resource allocation problems, where many entities want to access the same resource concurrently. Many message passing algorithms typically employ one attempt to break symmetry per message exchange. The multi-trials technique transcends this approach through employing more attempts with every message exchange.[1]
