---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Blondie24
offline_file: ""
offline_thumbnail: ""
uuid: 5b1b17f4-4593-4502-b2a7-d204438a4f4b
updated: 1484309001
title: Blondie24
categories:
    - Game artificial intelligence
---
Blondie24 is an artificial intelligence checkers-playing computer program named after the screen name used by a team led by David B. Fogel. The purpose was to determine the effectiveness of an artificial intelligence checkers-playing computer program.
