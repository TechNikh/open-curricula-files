---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flight
offline_file: ""
offline_thumbnail: ""
uuid: acf90b8c-2f07-456d-be7f-598d3dc40789
updated: 1484310156
title: flight
categories:
    - Dictionary
---
flight
     n 1: a formation of aircraft in flight
     2: an instance of traveling by air; "flying was still an
        exciting adventure for him" [syn: {flying}]
     3: a stairway (set of steps) between one floor or landing and
        the next [syn: {flight of stairs}, {flight of steps}]
     4: the act of escaping physically; "he made his escape from the
        mental hospital"; "the canary escaped from its cage"; "his
        flight was an indication of his guilt" [syn: {escape}]
     5: an air force unit smaller than a squadron
     6: passing above and beyond ordinary bounds; "a flight of
        fancy"; "flights of rhetoric"; "flights of imagination"
     7: the path followed by ...
