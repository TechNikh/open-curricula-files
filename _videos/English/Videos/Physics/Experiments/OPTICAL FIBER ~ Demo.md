---
version: 1
type: video
provider: YouTube
id: qNVM4_WFomM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 49eba9b5-f1a3-4607-b047-cd69cce2f2cb
updated: 1486069640
title: OPTICAL FIBER ~ Demo
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/qNVM4_WFomM/default.jpg
    - https://i3.ytimg.com/vi/qNVM4_WFomM/1.jpg
    - https://i3.ytimg.com/vi/qNVM4_WFomM/2.jpg
    - https://i3.ytimg.com/vi/qNVM4_WFomM/3.jpg
---
