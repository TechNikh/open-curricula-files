---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ternary_Golay_code
offline_file: ""
offline_thumbnail: ""
uuid: f791dc7a-3230-4682-ac9e-84cf365dc3fd
updated: 1484308570
title: Ternary Golay code
tags:
    - Properties
    - Ternary Golay code
    - Extended ternary Golay code
    - History
categories:
    - Coding theory
---
In coding theory, the ternary Golay codes are two closely related error-correcting codes. The code generally known simply as the ternary Golay code is an 
  
    
      
        [
        11
        ,
        6
        ,
        5
        
          ]
          
            3
          
        
      
    
    {\displaystyle [11,6,5]_{3}}
  
-code, that is, it is a linear code over a ternary alphabet; the relative distance of the code is as large as it possibly can be for a ternary code, and hence, the ternary Golay code is a perfect code. The extended ternary Golay code is a [12, 6, 6] linear code obtained by adding a zero-sum check digit to the [11, 6, 5] code. In finite group theory, ...
