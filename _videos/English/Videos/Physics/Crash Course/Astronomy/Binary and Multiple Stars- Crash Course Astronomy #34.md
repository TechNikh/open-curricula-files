---
version: 1
type: video
provider: YouTube
id: pIFiCLhJmig
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Binary%20and%20Multiple%20Stars-%20Crash%20Course%20Astronomy%20%2334.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 88665a74-da34-428d-9906-8242fca8333c
updated: 1486069684
title: 'Binary and Multiple Stars- Crash Course Astronomy #34'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/pIFiCLhJmig/default.jpg
    - https://i3.ytimg.com/vi/pIFiCLhJmig/1.jpg
    - https://i3.ytimg.com/vi/pIFiCLhJmig/2.jpg
    - https://i3.ytimg.com/vi/pIFiCLhJmig/3.jpg
---
