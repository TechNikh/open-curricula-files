---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distraction
offline_file: ""
offline_thumbnail: ""
uuid: 4fe3f3b3-9076-46c2-a1cb-56d750ea3de1
updated: 1484310605
title: distraction
categories:
    - Dictionary
---
distraction
     n 1: mental turmoil; "he drives me to distraction"
     2: an obstacle to attention
     3: an entertainment that provokes pleased interest and
        distracts you from worries and vexations [syn: {beguilement}]
     4: the act of distracting; drawing someone's attention away
        from something; "conjurers are experts at misdirection"
        [syn: {misdirection}]
