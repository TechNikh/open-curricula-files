---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/topic
offline_file: ""
offline_thumbnail: ""
uuid: 18fdd4d6-8255-4290-96a1-9bee253b075e
updated: 1484310150
title: topic
categories:
    - Dictionary
---
topic
     n 1: the subject matter of a conversation or discussion; "he
          didn't want to discuss that subject"; "it was a very
          sensitive topic"; "his letters were always on the theme
          of love" [syn: {subject}, {theme}]
     2: some situation or event that is thought about; "he kept
        drifting off the topic"; "he had been thinking about the
        subject for several years"; "it is a matter for the
        police" [syn: {subject}, {issue}, {matter}]
