---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/complaining
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484587741
title: complaining
categories:
    - Dictionary
---
complaining
     adj : expressing pain or dissatisfaction of resentment; "a
           complaining wife" [syn: {complaining(a)}, {complaintive}]
           [ant: {uncomplaining}]
