---
version: 1
type: video
provider: YouTube
id: 48i_htIMOtc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2675cef7-7b98-4011-ba83-2ebf6699af2b
updated: 1486069618
title: Introduction to addition
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/48i_htIMOtc/default.jpg
    - https://i3.ytimg.com/vi/48i_htIMOtc/1.jpg
    - https://i3.ytimg.com/vi/48i_htIMOtc/2.jpg
    - https://i3.ytimg.com/vi/48i_htIMOtc/3.jpg
---
