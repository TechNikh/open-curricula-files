---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/globalization
offline_file: ""
offline_thumbnail: ""
uuid: ae741347-7418-460d-b6a3-be821d6fb241
updated: 1484310163
title: globalization
categories:
    - Dictionary
---
globalization
     n : growth to a global or worldwide scale; "the globalization of
         the communication industry" [syn: {globalisation}]
