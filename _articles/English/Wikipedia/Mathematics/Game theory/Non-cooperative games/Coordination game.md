---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coordination_game
offline_file: ""
offline_thumbnail: ""
uuid: 45436cc3-6af5-44b1-9e63-512d8babd045
updated: 1484309052
title: Coordination game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Reaction-correspondence-stag-hunt.jpg
tags:
    - Examples
    - Mixed strategy Nash equilibrium
    - Coordination and equilibrium selection
    - Other games with externalities
categories:
    - Non-cooperative games
---
In game theory, coordination games are a class of games with multiple pure strategy Nash equilibria in which players choose the same or corresponding strategies. Coordination games are a formalization of the idea of a coordination problem, which is widespread in the social sciences, including economics, meaning situations in which all parties can realize mutual gains, but only by making mutually consistent decisions. A common application is the choice of technological standards.
