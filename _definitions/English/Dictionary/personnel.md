---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/personnel
offline_file: ""
offline_thumbnail: ""
uuid: abad056f-cda6-4222-b882-70a3ae491dd0
updated: 1484310244
title: personnel
categories:
    - Dictionary
---
personnel
     n 1: group of people willing to obey orders; "a public force is
          necessary to give security to the rights of citizens"
          [syn: {force}]
     2: the department responsible for hiring and training and
        placing employees and for seting policies for personnel
        management [syn: {personnel department}, {personnel office},
         {staff office}]
