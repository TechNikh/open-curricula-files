---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alain_A._Lewis
offline_file: ""
offline_thumbnail: ""
uuid: 36d3d6a4-8457-46a0-8aa9-3cd6fb1adef7
updated: 1484308506
title: Alain A. Lewis
categories:
    - Game theorists
---
Alain A. Lewis (born 1947) is an American mathematician. A student of the mathematical economist Kenneth Arrow, Lewis is credited by the historian of economics Philip Mirowski with making Arrow aware of computational limits to economic agency.[1]
