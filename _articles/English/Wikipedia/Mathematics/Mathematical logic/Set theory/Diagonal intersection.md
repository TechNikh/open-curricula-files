---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diagonal_intersection
offline_file: ""
offline_thumbnail: ""
uuid: 4621edd4-e288-465b-98cd-92be1919f168
updated: 1484309504
title: Diagonal intersection
categories:
    - Set theory
---
If 
  
    
      
        
          δ
        
      
    
    {\displaystyle \displaystyle \delta }
  
 is an ordinal number and 
  
    
      
        
          ⟨
          
            X
            
              α
            
          
          ∣
          α
          <
          δ
          ⟩
        
      
    
    {\displaystyle \displaystyle \langle X_{\alpha }\mid \alpha <\delta \rangle }
  
 is a sequence of subsets of 
  
    
      
        
          δ
        
      
    
    {\displaystyle \displaystyle \delta }
  
, then the diagonal intersection, denoted by
