---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lope
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484339341
title: lope
categories:
    - Dictionary
---
lope
     n 1: a slow pace of running [syn: {jog}, {trot}]
     2: a smooth 3-beat gait; between a trot and a gallop [syn: {canter}]
     v : run easily
