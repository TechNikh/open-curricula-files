---
version: 1
type: video
provider: YouTube
id: T5Ap6rkyBHY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fc5c6c5f-6553-4408-bd4e-5b748ebf105c
updated: 1486069648
title: 'Colourform - Kaleidoscope [Music Video]'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/T5Ap6rkyBHY/default.jpg
    - https://i3.ytimg.com/vi/T5Ap6rkyBHY/1.jpg
    - https://i3.ytimg.com/vi/T5Ap6rkyBHY/2.jpg
    - https://i3.ytimg.com/vi/T5Ap6rkyBHY/3.jpg
---
