---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alter
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484319421
title: alter
categories:
    - Dictionary
---
alter
     v 1: cause to change; make different; cause a transformation;
          "The advent of the automobile may have altered the
          growth pattern of the city"; "The discussion has changed
          my thinking about the issue" [syn: {change}, {modify}]
     2: make or become different in some particular way, without
        permanently losing one's or its former characteristics or
        essence; "her mood changes in accordance with the
        weather"; "The supermarket's selection of vegetables
        varies according to the season" [syn: {change}, {vary}]
     3: make an alteration to; "This dress needs to be altered"
     4: insert words into texts, often falsifying it ...
