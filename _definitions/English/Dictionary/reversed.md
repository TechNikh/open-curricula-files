---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reversed
offline_file: ""
offline_thumbnail: ""
uuid: 73b6e075-9f20-4ec1-b2cc-44a6fca38d5d
updated: 1484310333
title: reversed
categories:
    - Dictionary
---
reversed
     adj 1: turned inside out and resewn; "the reversed collar looked as
            good as new"
     2: turned about in order or relation; "transposed letters"
        [syn: {converse}, {transposed}]
