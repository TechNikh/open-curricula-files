---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chemistry
offline_file: ""
offline_thumbnail: ""
uuid: 883d4294-fa07-4907-9564-99434a8e2929
updated: 1484310395
title: chemistry
categories:
    - Dictionary
---
chemistry
     n 1: the science of matter; the branch of the natural sciences
          dealing with the composition of substances and their
          properties and reactions [syn: {chemical science}]
     2: the way two individuals relate to each other; "their
        chemistry was wrong from the beginning -- they hated each
        other"; "a mysterious alchemy brought them together" [syn:
         {interpersonal chemistry}, {alchemy}]
