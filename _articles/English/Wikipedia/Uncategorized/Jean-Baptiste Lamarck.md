---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lamarck
offline_file: ""
offline_thumbnail: ""
uuid: 05c98121-65f5-4160-afdc-c7f71ddbe59b
updated: 1484308345
title: Jean-Baptiste Lamarck
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jean-Baptiste_Lamarck.jpg
tags:
    - Biography
    - Lamarckian evolution
    - 'Le pouvoir de la vie: The complexifying force'
    - "L'influence des circonstances: The adaptive force"
    - Religious views
    - Legacy
    - Species and other taxa named by Lamarck
    - Species named in his honour
    - Major works
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
Jean-Baptiste Pierre Antoine de Monet, Chevalier de Lamarck (1 August 1744 – 18 December 1829), often known simply as Lamarck (/ləˈmɑːrk/;[1] French: [lamaʁk]), was a French naturalist. He was a soldier, biologist, academic, and an early proponent of the idea that evolution occurred and proceeded in accordance with natural laws. He gave the term biology a broader meaning by coining the term for special sciences, chemistry, meteorology, geology, and botany-zoology.[2]
