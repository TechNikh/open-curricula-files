---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Riemannian_Penrose_inequality
offline_file: ""
offline_thumbnail: ""
uuid: 69899950-a565-45dc-bff7-532de44abd9c
updated: 1484309126
title: Riemannian Penrose inequality
tags:
    - Physical motivation
    - Case of equality
    - Penrose conjecture
categories:
    - Theorems in geometry
---
In mathematical general relativity, the Penrose inequality, first conjectured by Sir Roger Penrose, estimates the mass of a spacetime in terms of the total area of its black holes and is a generalization of the positive mass theorem. The Riemannian Penrose inequality is an important special case. Specifically, if (M, g) is an asymptotically flat Riemannian 3-manifold with nonnegative scalar curvature and ADM mass m, and A is the area of the outermost minimal surface (possibly with multiple connected components), then the Riemannian Penrose inequality asserts
