---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Alkali-metal_thermal_to_electric_converter
offline_file: ""
offline_thumbnail: ""
uuid: 0c0bbb42-6e99-4f56-b58c-21db5089e588
updated: 1484308832
title: Alkali-metal thermal to electric converter
categories:
    - Thermodynamics
---
The alkali-metal thermal-to-electric converter (AMTEC), originally called the sodium heat engine (SHE) was invented by Joseph T. Kummer and Neill Weber at Ford in 1966, and is described in US Patents 3404036, 3458356, 3535163 and 4049877. It is a thermally regenerative electrochemical device for the direct conversion of heat to electrical energy.[1][2] It is characterized by high potential efficiencies and no moving parts except the working fluid, which make it a candidate for space power applications.[2]
