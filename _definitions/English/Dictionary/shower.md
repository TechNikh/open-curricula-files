---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shower
offline_file: ""
offline_thumbnail: ""
uuid: d375475d-3d31-41bd-9c52-84abcb037f92
updated: 1484310242
title: shower
categories:
    - Dictionary
---
shower
     n 1: a plumbing fixture that sprays water over you; "they
          installed a shower in the bathroom"
     2: washing yourself in a shower; you stand upright under water
        sprayed from a nozzle; "he took a shower after the game"
        [syn: {shower bath}]
     3: a brief period of precipitation; "the game was interrupted
        by a brief shower" [syn: {rain shower}]
     4: a sudden downpour (as of tears or sparks etc) likened to a
        rain shower; "a little shower of rose petals"; "a sudden
        cascade of sparks" [syn: {cascade}]
     5: someone who organizes an exhibit for others to see [syn: {exhibitor},
         {exhibitioner}]
     6: a party of friends ...
