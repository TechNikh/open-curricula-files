---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graphic_matroid
offline_file: ""
offline_thumbnail: ""
uuid: 520407c9-1204-4315-8953-7ae1e86297a7
updated: 1484309402
title: Graphic matroid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Nonisomorphic_planar_duals.svg.png
tags:
    - Definition
    - The lattice of flats
    - Representation
    - Matroid connectivity
    - Minors and duality
    - Algorithms
    - Related classes of matroids
categories:
    - Graph connectivity
---
In matroid theory, a discipline within mathematics, a graphic matroid (also called a cycle matroid or polygon matroid) is a matroid whose independent sets are the forests in a given undirected graph. The dual matroids of graphic matroids are called co-graphic matroids or bond matroids.[1] A matroid that is both graphic and co-graphic is called a planar matroid; these are exactly the graphic matroids formed from planar graphs.
