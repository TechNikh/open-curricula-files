---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/input
offline_file: ""
offline_thumbnail: ""
uuid: 248d5a8e-194f-4a7a-a22b-c0ee3a356f1e
updated: 1484310453
title: input
categories:
    - Dictionary
---
input
     n 1: signal going into an electronic system [syn: {input signal}]
     2: any stimulating information or event; acts to arouse action
        [syn: {stimulation}, {stimulus}, {stimulant}]
     v : enter (data or a program) into a computer
