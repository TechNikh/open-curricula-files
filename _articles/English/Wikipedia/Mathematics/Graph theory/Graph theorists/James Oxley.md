---
version: 1
type: article
id: https://en.wikipedia.org/wiki/James_Oxley
offline_file: ""
offline_thumbnail: ""
uuid: 332bfdad-2dff-4d0e-aae7-5e99dd02e7e4
updated: 1484309473
title: James Oxley
categories:
    - Graph theorists
---
James G. Oxley is an Australian–American mathematician, Boyd Professor of Mathematics at Louisiana State University.[1] He is known for his expertise in matroid theory and graph theory.
