---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extension
offline_file: ""
offline_thumbnail: ""
uuid: f9d63261-c5f7-4329-90d8-33a5cb386734
updated: 1484310198
title: extension
categories:
    - Dictionary
---
extension
     n 1: a mutually agreed delay in the date set for the completion
          of a job or payment of a debt; "they applied for an
          extension of the loan"
     2: act of expanding in scope; making more widely available;
        "extension of the program to all in need"
     3: the spreading of something (a belief or practice) into new
        regions [syn: {propagation}]
     4: an educational opportunity provided by colleges and
        universities to people who not enrolled as regular
        students [syn: {extension service}, {university extension}]
     5: act of stretching or straightening out a flexed limb [ant: {flexion}]
     6: a string of characters beginning ...
