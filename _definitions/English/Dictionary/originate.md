---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/originate
offline_file: ""
offline_thumbnail: ""
uuid: 5ebf94fd-21a5-4445-a447-331c2a8ed196
updated: 1484310437
title: originate
categories:
    - Dictionary
---
originate
     v 1: come into existence; take on form or shape; "A new religious
          movement originated in that country"; "a love that
          sprang up from friendship"; "the idea for the book grew
          out of a short story"; "An interesting phenomenon
          uprose" [syn: {arise}, {rise}, {develop}, {uprise}, {spring
          up}, {grow}]
     2: bring into being; "He initiated a new program"; "Start a
        foundation" [syn: {initiate}, {start}]
     3: begin a trip at a certain point, as of a plane, train, bus,
        etc.; "The flight originates in Calcutta"
