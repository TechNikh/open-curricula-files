---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Scientific_community_metaphor
offline_file: ""
offline_thumbnail: ""
uuid: 5977de93-5a55-4206-9452-963431f7435f
updated: 1484308681
title: Scientific community metaphor
tags:
    - Development
    - Qualities of scientific research
    - Ether
    - Emphasis on communities rather than individuals
    - Current applications
categories:
    - Theoretical computer science
---
In computer science, the scientific community metaphor is a metaphor used to aid understanding scientific communities. The first publications on the Scientific Community Metaphor in 1981 and 1982[1] involved the development of a programming language named Ether that invoked procedural plans to process goals and assertions concurrently by dynamically creating new rules during program execution. Ether also addressed issues of conflict and contradiction with multiple sources of knowledge and multiple viewpoints.
