---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corn
offline_file: ""
offline_thumbnail: ""
uuid: 6ddaacd5-ca20-4e81-a68d-a884926a4e63
updated: 1484310246
title: corn
categories:
    - Dictionary
---
corn
     n 1: tall annual cereal grass bearing kernels on large ears:
          widely cultivated in America in many varieties; the
          principal cereal in Mexico and Central and South America
          since pre-Columbian times [syn: {maize}, {Indian corn},
          {Zea mays}]
     2: the dried grains or kernels or corn used as animal feed or
        ground for meal
     3: ears of corn grown for human food [syn: {edible corn}]
     4: a hard thickening of the skin (especially on the top or
        sides of the toes) caused by the pressure of ill-fitting
        shoes [syn: {clavus}]
     5: annual or biennial grass having erect flower spikes and
        light brown grains [syn: ...
