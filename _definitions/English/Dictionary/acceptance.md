---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acceptance
offline_file: ""
offline_thumbnail: ""
uuid: e6f32a98-c1ba-4dd5-ac52-339d327a2fba
updated: 1484310443
title: acceptance
categories:
    - Dictionary
---
acceptance
     n 1: the mental attitude that something is believable and should
          be accepted as true; "he gave credence to the gossip";
          "acceptance of Newtonian mechanics was unquestioned for
          200 years" [syn: {credence}]
     2: the act of accepting with approval; favorable reception;
        "its adoption by society"; "the proposal found wide
        acceptance" [syn: {adoption}, {acceptation}, {espousal}]
     3: the state of being acceptable and accepted; "torn jeans
        received no acceptance at the country club" [ant: {rejection}]
     4: (contract law) words signifying consent to the terms of an
        offer (thereby creating a contract)
     5: ...
