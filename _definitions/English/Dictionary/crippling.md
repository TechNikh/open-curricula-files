---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crippling
offline_file: ""
offline_thumbnail: ""
uuid: 26bc75f9-833b-4edd-85bc-295e71760b36
updated: 1484310148
title: crippling
categories:
    - Dictionary
---
crippling
     adj : that cripples or disables or incapacitates; "a crippling
           injury" [syn: {disabling}, {incapacitating}]
