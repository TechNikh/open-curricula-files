---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Independent_set_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: dc6f364d-3cbd-49e1-893d-01d716c7f98f
updated: 1484309394
title: Independent set (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Independent_set_graph.svg.png
tags:
    - Properties
    - Relationship to other graph parameters
    - Maximal independent set
    - Finding independent sets
    - Maximum independent sets and maximum cliques
    - Finding maximum independent sets
    - Exact algorithms
    - Approximation algorithms
    - Independent sets in interval intersection graphs
    - Independent sets in geometric intersection graphs
    - Finding maximal independent sets
    - Software for searching maximum independent set
    - Notes
categories:
    - Computational problems in graph theory
---
In graph theory, an independent set or stable set is a set of vertices in a graph, no two of which are adjacent. That is, it is a set S of vertices such that for every two vertices in S, there is no edge connecting the two. Equivalently, each edge in the graph has at most one endpoint in S. The size of an independent set is the number of vertices it contains. Independent sets have also been called internally stable sets.[1]
