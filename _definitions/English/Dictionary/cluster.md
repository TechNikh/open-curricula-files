---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cluster
offline_file: ""
offline_thumbnail: ""
uuid: b9023c1b-dcf9-4a16-987c-58e65dce7ddc
updated: 1484310426
title: cluster
categories:
    - Dictionary
---
cluster
     n : a grouping of a number of similar things; "a bunch of
         trees"; "a cluster of admirers" [syn: {bunch}, {clump}, {clustering}]
     v 1: come together as in a cluster or flock; "The poets
          constellate in this town every summer" [syn: {constellate},
           {flock}, {clump}]
     2: gather or cause to gather into a cluster; "She bunched her
        fingers into a fist"; "The students bunched up at the
        registration desk" [syn: {bunch}, {bunch up}, {bundle}, {clump}]
