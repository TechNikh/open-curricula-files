---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/room
offline_file: ""
offline_thumbnail: ""
uuid: 894672a8-8858-44d6-9e48-bf79cd373090
updated: 1484310232
title: room
categories:
    - Dictionary
---
room
     n 1: an area within a building enclosed by walls and floor and
          ceiling; "the rooms were very small but they had a nice
          view"
     2: space for movement; "room to pass"; "make way for"; "hardly
        enough elbow room to turn around" [syn: {way}, {elbow room}]
     3: opportunity for; "room for improvement"
     4: the people who are present in a room; "the whole room was
        cheering"
     v : live and take one's meals at or in; "she rooms in an old
         boarding house" [syn: {board}]
