---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ionisation
offline_file: ""
offline_thumbnail: ""
uuid: 0469c595-9e0b-4e34-9f90-b4ed87f60859
updated: 1484310403
title: ionisation
categories:
    - Dictionary
---
ionisation
     n 1: the condition of being dissociated into ions (as by heat or
          radiation or chemical reaction or electrical discharge);
          "the ionization of a gas" [syn: {ionization}]
     2: the process of ionizing; the formation of ions by separating
        atoms or molecules or radicals or by adding or subtracting
        electrons from atoms by strong electric fields in a gas
        [syn: {ionization}]
