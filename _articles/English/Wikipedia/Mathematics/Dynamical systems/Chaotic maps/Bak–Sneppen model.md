---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bak%E2%80%93Sneppen_model'
offline_file: ""
offline_thumbnail: ""
uuid: 15a8cefd-e130-478a-b93a-556f8b18619b
updated: 1484308713
title: Bak–Sneppen model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Bak_sneppen_model.png
tags:
    - Description
categories:
    - Chaotic maps
---
The Bak–Sneppen model is a simple model of co-evolution between interacting species. It was developed to show how self-organized criticality may explain key features of the fossil record, such as the distribution of sizes of extinction events and the phenomenon of punctuated equilibrium. It is named after Per Bak and Kim Sneppen.
