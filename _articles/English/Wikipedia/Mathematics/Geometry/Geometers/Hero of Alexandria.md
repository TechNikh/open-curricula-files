---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hero_of_Alexandria
offline_file: ""
offline_thumbnail: ""
uuid: 22bd5f67-50f8-4932-8b7f-2bc7d5b2bb06
updated: 1484309085
title: Hero of Alexandria
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Aeolipile_illustration.png
tags:
    - Career
    - Inventions
    - Mathematics
    - In media
    - Bibliography
categories:
    - Geometers
---
Heron of Alexandria (Greek: Ἥρων ὁ Ἀλεξανδρεύς, Heron ho Alexandreus; also known as Hero of Alexandria c. 10 AD – c. 70 AD) was a Greek mathematician and engineer who was active in his native city of Alexandria, Roman Egypt. He is considered the greatest experimenter of antiquity[1] and his work is representative of the Hellenistic scientific tradition.[2]
