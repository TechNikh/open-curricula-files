---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/idol
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484369761
title: idol
categories:
    - Dictionary
---
idol
     n 1: a material effigy that is worshipped as a god; "thou shalt
          not make unto thee any graven image"; "money was his
          god" [syn: {graven image}, {god}]
     2: someone who is adored blindly and excessively [syn: {matinee
        idol}]
     3: an ideal instance; a perfect embodiment of a concept [syn: {paragon},
         {perfection}, {beau ideal}]
