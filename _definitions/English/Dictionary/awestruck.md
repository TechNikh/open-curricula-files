---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/awestruck
offline_file: ""
offline_thumbnail: ""
uuid: c3f917db-d90a-4db1-b5be-9d1e8afa60dc
updated: 1484310170
title: awestruck
categories:
    - Dictionary
---
awestruck
     adj : having or showing a feeling of mixed reverence and respect
           and wonder and dread; "stood in awed silence before the
           shrine"; "in grim despair and awestruck wonder" [syn: {awed},
            {awestricken}, {in awe of}] [ant: {unawed}]
