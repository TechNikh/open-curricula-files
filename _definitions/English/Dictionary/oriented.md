---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oriented
offline_file: ""
offline_thumbnail: ""
uuid: d3dc68c1-5fbf-4159-8079-14ed0eed6bce
updated: 1484310365
title: oriented
categories:
    - Dictionary
---
oriented
     adj : adjusted or located in relation to surroundings or
           circumstances; sometimes used in combination; "the
           house had its large windows oriented toward the ocean
           view"; "helping freshmen become oriented to college
           life"; "the book is value-oriented throughout" [syn: {orientated}]
           [ant: {unoriented}]
