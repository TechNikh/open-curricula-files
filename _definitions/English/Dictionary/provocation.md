---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/provocation
offline_file: ""
offline_thumbnail: ""
uuid: 6eb86182-4e81-4f10-90cb-54d371412450
updated: 1484310551
title: provocation
categories:
    - Dictionary
---
provocation
     n 1: unfriendly behavior that causes anger or resentment [syn: {aggravation},
           {irritation}]
     2: something that incites or provokes; a means of arousing or
        stirring to action [syn: {incitement}, {incitation}]
     3: needed encouragement; "the result was a provocation of
        vigorous investigation" [syn: {incitement}]
