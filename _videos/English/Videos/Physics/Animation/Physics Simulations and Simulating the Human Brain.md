---
version: 1
type: video
provider: YouTube
id: -nbTrPwQudo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Physics%20Simulations%20and%20Simulating%20the%20Human%20Brain.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 939b790e-8a62-43db-98bd-b89ee3cef09e
updated: 1486069656
title: Physics Simulations and Simulating the Human Brain
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/-nbTrPwQudo/default.jpg
    - https://i3.ytimg.com/vi/-nbTrPwQudo/1.jpg
    - https://i3.ytimg.com/vi/-nbTrPwQudo/2.jpg
    - https://i3.ytimg.com/vi/-nbTrPwQudo/3.jpg
---
