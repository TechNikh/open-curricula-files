---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prospective
offline_file: ""
offline_thumbnail: ""
uuid: 79452a47-db95-4c50-bd22-a5e6ebed1233
updated: 1484310517
title: prospective
categories:
    - Dictionary
---
prospective
     adj 1: concerned with or related to the future; "prospective
            earnings"; "a prospective mother"; "the statute is
            solely prospective in operation" [ant: {retrospective}]
     2: anticipated for the near future; "the prospective students";
        "his prospective bride" [syn: {prospective(a)}]
