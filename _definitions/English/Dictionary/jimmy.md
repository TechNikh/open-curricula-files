---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jimmy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484451061
title: jimmy
categories:
    - Dictionary
---
jimmy
     n : a short crowbar; "in England they call a jimmy and jemmy"
         [syn: {jemmy}]
     v : to move or force, especially in an effort to get something
         open; "The burglar jimmied the lock", "Raccoons managed
         to pry the lid off the garbage pail" [syn: {pry}, {prise},
          {prize}, {lever}]
     [also: {jimmied}]
