---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roasting
offline_file: ""
offline_thumbnail: ""
uuid: 1f327aeb-bcf9-47d8-aa62-b10194dc6ce3
updated: 1484310409
title: roasting
categories:
    - Dictionary
---
roasting
     n : cooking (meat) by dry heat in an oven (usually with fat
         added); "the slow roasting took several hours"
