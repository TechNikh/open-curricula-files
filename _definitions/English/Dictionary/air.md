---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/air
offline_file: ""
offline_thumbnail: ""
uuid: 17e9f6be-257c-41be-ba03-c8b083e2919d
updated: 1484310353
title: air
categories:
    - Dictionary
---
air
     adj : relating to or characteristic of or occurring in the air;
           "air war"; "air safety"; "air travel" [syn: {air(a)}]
           [ant: {land(a)}, {sea(a)}]
     n 1: a mixture of gases (especially oxygen) required for
          breathing; the stuff that the wind consists of; "air
          pollution"; "a smell of chemicals in the air"; "open a
          window and let in some air"; "I need some fresh air"
     2: travel via aircraft; "air travel involves too much waiting
        in airports"; "if you've time to spare go by air" [syn: {air
        travel}, {aviation}]
     3: the region above the ground; "her hand stopped in mid air";
        "he threw the ball into the ...
