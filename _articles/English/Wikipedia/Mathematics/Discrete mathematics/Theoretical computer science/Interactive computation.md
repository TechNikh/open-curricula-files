---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Interactive_computation
offline_file: ""
offline_thumbnail: ""
uuid: f1340650-300b-4cd2-8254-23b1c5e4830d
updated: 1484308675
title: Interactive computation
categories:
    - Theoretical computer science
---
In computer science, interactive computation is a mathematical model for computation that involves input/output communication with the external world during computation. This is in contrast to the traditional understanding of computation which assumes reading input only before computation and writing output only after computation, thus defining a kind of "closed" computation.
