---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deceased
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484591162
title: deceased
categories:
    - Dictionary
---
deceased
     adj : dead; "he is deceased"; "our dear departed friend" [syn: {asleep(p)},
            {at peace(p)}, {at rest(p)}, {departed}, {gone}]
     n : someone who is no longer alive; "I wonder what the dead
         person would have done" [syn: {dead person}, {dead soul},
          {deceased person}, {decedent}, {departed}]
