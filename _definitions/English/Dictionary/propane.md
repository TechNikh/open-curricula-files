---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/propane
offline_file: ""
offline_thumbnail: ""
uuid: af267880-8116-4932-a25f-87b13b11c510
updated: 1484310371
title: propane
categories:
    - Dictionary
---
propane
     n : colorless gas found in natural gas and petroleum; used as a
         fuel
