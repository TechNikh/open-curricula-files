---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/baseless
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484578561
title: baseless
categories:
    - Dictionary
---
baseless
     adj : without a basis in reason or fact; "baseless gossip"; "the
           allegations proved groundless"; "idle fears";
           "unfounded suspicions"; "unwarranted jealousy" [syn: {groundless},
            {idle}, {unfounded}, {unwarranted}]
