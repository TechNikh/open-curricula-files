---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/orient
offline_file: ""
offline_thumbnail: ""
uuid: b29d2068-3773-4e35-bc28-ba9f7a52ad3f
updated: 1484310405
title: orient
categories:
    - Dictionary
---
orient
     adj : (poetic) eastern; "the orient sun"
     n 1: the countries of Asia [syn: {East}]
     2: the hemisphere that includes Eurasia and Africa and
        Australia [syn: {eastern hemisphere}]
     v 1: be oriented; "The weather vane points North" [syn: {point}]
     2: determine one's position with reference to another point
        [syn: {orientate}] [ant: {disorient}]
     3: cause to point; "Orient the house towards the West"
