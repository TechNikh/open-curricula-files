---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Biological_applications_of_bifurcation_theory
offline_file: ""
offline_thumbnail: ""
uuid: c75f1ceb-5e7d-47ae-8e65-d6b0e9a8694c
updated: 1484308705
title: Biological applications of bifurcation theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Skotheimsystem.jpg
tags:
    - Biological networks and dynamical systems
    - Input/output motifs
    - Bifurcations
    - Examples
categories:
    - Bifurcation theory
---
Biological applications of bifurcation theory provide a framework for understanding the behavior of biological networks modeled as dynamical systems. In the context of a biological system, bifurcation theory describes how small changes in an input parameter can cause a bifurcation or qualitative change in the behavior of the system. The ability to make dramatic change in system output is often essential to organism function, and bifurcations are therefore ubiquitous in biological networks such as the switches of the cell cycle.
