---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/handed
offline_file: ""
offline_thumbnail: ""
uuid: e81eab68-9417-4315-89e1-0004785485e2
updated: 1484310575
title: handed
categories:
    - Dictionary
---
handed
     adj : having or involving the use of hands; "a handed, tree-living
           animal"; "a four-handed card game" [ant: {handless}]
