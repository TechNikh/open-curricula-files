---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/freak
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484411041
title: freak
categories:
    - Dictionary
---
freak
     n 1: a person or animal that is markedly unusual or deformed
          [syn: {monster}, {monstrosity}, {lusus naturae}]
     2: someone who is so ardently devoted to something that it
        resembles an addiction; "a golf addict"; "a car nut"; "a
        news junkie" [syn: {addict}, {nut}, {junkie}, {junky}]
     v : lose one's nerve; "When he saw the accident, he freaked out"
         [syn: {freak out}, {gross out}]
