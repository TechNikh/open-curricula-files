---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appreciate
offline_file: ""
offline_thumbnail: ""
uuid: a7414ebf-c218-4765-9d64-d4d6bfabe9a0
updated: 1484310355
title: appreciate
categories:
    - Dictionary
---
appreciate
     v 1: recognize with gratitude; be grateful for
     2: be fully aware of; realize fully; "Do you appreciate the
        full meaning of this letter?" [syn: {take account}]
     3: hold dear; "I prize these old photographs" [syn: {prize}, {value},
         {treasure}]
     4: gain in value; "The yen appreciated again!" [syn: {apprize},
         {apprise}, {revalue}] [ant: {depreciate}]
     5: increase the value of; "The Germans want to appreciate the
        Deutsche Mark" [syn: {apprize}, {apprise}] [ant: {depreciate}]
