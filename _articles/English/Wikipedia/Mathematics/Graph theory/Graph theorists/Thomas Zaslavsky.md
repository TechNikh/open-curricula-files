---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thomas_Zaslavsky
offline_file: ""
offline_thumbnail: ""
uuid: 294c434e-c4dd-423d-a6e7-e68301c84e80
updated: 1484309479
title: Thomas Zaslavsky
categories:
    - Graph theorists
---
Zaslavsky's mother Claudia Zaslavsky was a high school mathematics teacher at New York; his father Sam Zaslavsky (from Manhattan) was an electrical engineer. Thomas Zaslavsky graduated from City College of New York. At M.I.T. he studied hyperplane arrangements with Curtis Greene and received a Ph.D. in 1974. In 1975 the American Mathematical Society published his doctoral thesis.
