---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Molecular_graph
offline_file: ""
offline_thumbnail: ""
uuid: bfd5d6c8-7d80-465c-9536-8aa96e4307f1
updated: 1484309431
title: Molecular graph
categories:
    - Mathematical chemistry
---
In chemical graph theory and in mathematical chemistry, a molecular graph or chemical graph is a representation of the structural formula of a chemical compound in terms of graph theory. A chemical graph is a labeled graph whose vertices correspond to the atoms of the compound and edges correspond to chemical bonds. Its vertices are labeled with the kinds of the corresponding atoms and edges are labeled with the types of bonds.[1] For particular purposes any of the labelings may be ignored.
