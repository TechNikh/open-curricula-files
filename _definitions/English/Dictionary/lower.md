---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lower
offline_file: ""
offline_thumbnail: ""
uuid: 2f43428f-be5c-4fa1-9859-39db4148f398
updated: 1484310344
title: lower
categories:
    - Dictionary
---
lower
     adj 1: (usually preceded by `no') lower in esteem; "no less a
            person than the king himself" [syn: {less}]
     2: inferior in rank or status; "the junior faculty"; "a lowly
        corporal"; "petty officialdom"; "a subordinate
        functionary" [syn: {junior-grade}, {inferior}, {lower-ranking},
         {lowly}, {petty(a)}, {secondary}, {subaltern}, {subordinate}]
     3: the bottom one of two; "he chose the lower number"
     4: of the underworld; "nether regions" [syn: {chthonian}, {chthonic},
         {nether}]
     n : the lower of two berths [syn: {lower berth}]
     v 1: move something or somebody to a lower position; "take down
          the vase from the ...
