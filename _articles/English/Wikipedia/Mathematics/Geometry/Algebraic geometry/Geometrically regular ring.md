---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometrically_regular_ring
offline_file: ""
offline_thumbnail: ""
uuid: ba137722-5b04-4fe4-8949-a010ae1d48ec
updated: 1484309149
title: Geometrically regular ring
categories:
    - Algebraic geometry
---
In algebraic geometry, a geometrically regular ring is a Noetherian ring over a field that remains a regular ring after any finite extension of the base field. Geometrically regular schemes are defined in a similar way. In older terminology, points with regular local rings were called simple points, and points with geometrically regular local rings were called absolutely simple points. Over fields that are of characteristic 0, or algebraically closed, or more generally perfect, geometrically regular rings are the same as regular rings. Geometric regularity originated when Chevalley and Weil pointed out to Zariski (1947) that, over non-perfect fields, the Jacobian criterion for a simple ...
