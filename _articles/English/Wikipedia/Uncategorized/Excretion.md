---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Excretion
offline_file: ""
offline_thumbnail: ""
uuid: 0b719a35-cf7f-46e2-a7e5-499cfcbcf950
updated: 1484308377
title: Excretion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Feces_and_uric-acid.jpg
categories:
    - Uncategorized
---
Excretion is the process by which metabolic wastes and other non-useful materials are eliminated from an organism. In vertebrates this is primarily carried out by the lungs, kidneys and skin.[1] This is in contrast with secretion, where the substance may have specific tasks after leaving the cell. Excretion is an essential process in all forms of life. For example, in mammals urine is expelled through the urethra, which is part of the excretory system. In single-celled organisms, waste products are discharged directly through the surface of the cell.
