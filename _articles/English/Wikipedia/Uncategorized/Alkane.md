---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paraffins
offline_file: ""
offline_thumbnail: ""
uuid: 078a7c6b-f5cc-4e96-a285-81df25f7473e
updated: 1484308403
title: Alkane
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Methane-2D-stereo.svg_0.png
tags:
    - Structure classification
    - Isomerism
    - Nomenclature
    - Linear alkanes
    - Branched alkanes
    - Saturated cyclic hydrocarbons
    - Trivial/common names
    - Physical properties
    - Table of alkanes
    - Boiling point
    - Melting points
    - Conductivity and solubility
    - Molecular geometry
    - Bond lengths and bond angles
    - Conformation
    - Spectroscopic properties
    - Infrared spectroscopy
    - NMR spectroscopy
    - Mass spectrometry
    - Chemical properties
    - Reactions with oxygen (combustion reaction)
    - Reactions with halogens
    - Cracking
    - Isomerization and reformation
    - Other reactions
    - Occurrence
    - Occurrence of alkanes in the Universe
    - Occurrence of alkanes on Earth
    - Biological occurrence
    - Ecological relations
    - Production
    - Petroleum refining
    - Fischer–Tropsch
    - Laboratory preparation
    - Applications
    - Environmental transformations
    - Hazards
categories:
    - Uncategorized
---
In organic chemistry, an alkane, or paraffin (a historical name that also has other meanings), is an acyclic saturated hydrocarbon. In other words, an alkane consists of hydrogen and carbon atoms arranged in a tree structure in which all the carbon-carbon bonds are single.[1] Alkanes have the general chemical formula CnH2n+2. The alkanes range in complexity from the simplest case of methane, CH4 where n = 1 (sometimes called the parent molecule), to arbitrarily large molecules.
