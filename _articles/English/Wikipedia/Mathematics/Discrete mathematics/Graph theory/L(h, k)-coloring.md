---
version: 1
type: article
id: https://en.wikipedia.org/wiki/L(h,_k)-coloring
offline_file: ""
offline_thumbnail: ""
uuid: 373f97ec-d21d-4315-864d-9b3d8febb104
updated: 1484308627
title: L(h, k)-coloring
categories:
    - Graph theory
---
L(h, k) coloring in graph theory, is a (proper) vertex coloring in which every pair of adjacent vertices has color numbers that differ by at least h, and any pair of vertices at distance 2 have their colors differ by at least k.[1] When h=1 and k=0, it is the usual (proper) vertex coloring.
