---
version: 1
type: video
provider: YouTube
id: kiQa8ni0NXQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/2nd%20Person%20English%20Grammar.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f0b366aa-dc17-4886-9aee-0558dfd9f5e8
updated: 1486069631
title: 2nd Person English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/kiQa8ni0NXQ/default.jpg
    - https://i3.ytimg.com/vi/kiQa8ni0NXQ/1.jpg
    - https://i3.ytimg.com/vi/kiQa8ni0NXQ/2.jpg
    - https://i3.ytimg.com/vi/kiQa8ni0NXQ/3.jpg
---
