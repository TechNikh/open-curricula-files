---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/damaged
offline_file: ""
offline_thumbnail: ""
uuid: 78eb6e3c-4a45-4e69-8761-6cb0de0b4bae
updated: 1484310522
title: damaged
categories:
    - Dictionary
---
damaged
     adj 1: harmed or injured or spoiled; "I wont't buy damaged goods";
            "the storm left a wake of badly damaged buildings"
            [ant: {undamaged}]
     2: being unjustly brought into disrepute; "a discredited
        politician"; "her damaged reputation" [syn: {discredited}]
     3: especially of reputation; "the senator's seriously damaged
        reputation"; "a flyblown reputation"; "a tarnished
        reputation"; "inherited a spotted name" [syn: {besmirched},
         {flyblown}, {spotted}, {stained}, {sullied}, {tainted}, {tarnished}]
