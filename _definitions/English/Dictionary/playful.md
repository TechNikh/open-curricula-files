---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/playful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484615341
title: playful
categories:
    - Dictionary
---
playful
     adj : full of fun and high spirits; "playful children sjust let
           loose from school" [ant: {unplayful}]
