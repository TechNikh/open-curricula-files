---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gambling_mathematics
offline_file: ""
offline_thumbnail: ""
uuid: 3e065568-6eca-462c-8f37-144c4e2f63b6
updated: 1484309040
title: Gambling mathematics
tags:
    - Experiments, events, probability spaces
    - The probability model
    - Combinations
    - Expectation and strategy
    - House advantage or edge
    - Standard deviation
categories:
    - Games (probability)
---
The mathematics of gambling are a collection of probability applications encountered in games of chance and can be included in game theory. From a mathematical point of view, the games of chance are experiments generating various types of aleatory events, the probability of which can be calculated by using the properties of probability on a finite space of events.
