---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agricultural
offline_file: ""
offline_thumbnail: ""
uuid: 74b44231-87a8-4cc0-94dd-7c23e0e7a661
updated: 1484310271
title: agricultural
categories:
    - Dictionary
---
agricultural
     adj 1: relating to or used in or promoting agriculture or farming;
            "agricultural engineering"; "modern agricultural (or
            farming) methods"; "agricultural (or farm) equipment";
            "an agricultural college"
     2: relating to rural matters; "an agrarian (or agricultural)
        society"; "farming communities" [syn: {agrarian}, {farming(a)}]
