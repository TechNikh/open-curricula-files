---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heat_kernel_signature
offline_file: ""
offline_thumbnail: ""
uuid: a629f857-97be-4eb9-82c6-cec481aa97ae
updated: 1484309223
title: Heat kernel signature
tags:
    - Overview
    - Technical details
    - Practical considerations
    - Limitations
    - Non-repeating eigenvalues
    - Time parameter selection
    - Time complexity
    - Non-isometric transformations
    - 'Relation with other methods[2]'
    - Curvature
    - Wave kernel signature (WKS)
    - Global point signature (GPS)
    - Spectral graph wavelet signature (SGWS)
    - Extensions
    - Scale invariance
    - Volumetric HKS
    - Shape Search
categories:
    - Differential geometry
---
A heat kernel signature (HKS) is a feature descriptor for use in deformable shape analysis and belongs to the group of spectral shape analysis methods. For each point in the shape, HKS defines its feature vector representing the point's local and global geometric properties. Applications include segmentation, classification, structure discovery, shape matching and shape retrieval.
