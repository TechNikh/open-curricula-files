---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Discrepancy_theory
offline_file: ""
offline_thumbnail: ""
uuid: 386c7965-e67f-4624-bdec-12e293d15c3a
updated: 1484308577
title: Discrepancy theory
tags:
    - History
    - Classic theorems
    - Major open problems
    - Applications
categories:
    - Combinatorics
---
In mathematics, discrepancy theory describes the deviation of a situation from the state one would like it to be in. It is also called the theory of irregularities of distribution. This refers to the theme of classical discrepancy theory, namely distributing points in some space such that they are evenly distributed with respect to some (mostly geometrically defined) subsets. The discrepancy (irregularity) measures how far a given distribution deviates from an ideal one.
