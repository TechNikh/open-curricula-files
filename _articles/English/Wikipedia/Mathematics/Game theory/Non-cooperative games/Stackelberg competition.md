---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stackelberg_competition
offline_file: ""
offline_thumbnail: ""
uuid: 770c93d2-9c46-4e35-9efb-021066a1179a
updated: 1484309062
title: Stackelberg competition
tags:
    - Subgame perfect Nash equilibrium
    - Examples
    - Economic analysis
    - Credible and non-credible threats by the follower
    - Stackelberg compared with Cournot
    - Game theoretic considerations
    - Comparison with other oligopoly models
    - Applications
categories:
    - Non-cooperative games
---
The Stackelberg leadership model is a strategic game in economics in which the leader firm moves first and then the follower firms move sequentially. It is named after the German economist Heinrich Freiherr von Stackelberg who published Market Structure and Equilibrium (Marktform und Gleichgewicht) in 1934 which described the model.
