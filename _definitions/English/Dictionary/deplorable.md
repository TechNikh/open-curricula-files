---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deplorable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484518081
title: deplorable
categories:
    - Dictionary
---
deplorable
     adj 1: bad; unfortunate; "my finances were in a deplorable state";
            "a lamentable decision"; "her clothes were in sad
            shape"; "a sorry state of affairs" [syn: {distressing},
             {lamentable}, {pitiful}, {sad}, {sorry}]
     2: of very poor quality or condition; "deplorable housing
        conditions in the inner city"; "woeful treatment of the
        accused"; "woeful errors of judgment" [syn: {execrable}, {miserable},
         {woeful}, {wretched}]
     3: bringing or deserving severe rebuke or censure; "a criminal
        waste of talent"; "a deplorable act of violence";
        "adultery is as reprehensible for a husband as for a wife"
    ...
