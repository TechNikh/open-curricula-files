---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Goodstein%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: ea92477a-0dac-4d9f-b2ba-c83fa63dc3ae
updated: 1484308991
title: "Goodstein's theorem"
tags:
    - Hereditary base-n notation
    - Goodstein sequences
    - "Proof of Goodstein's theorem"
    - "Extended Goodstein's theorem"
    - Sequence length as a function of the starting value
    - Application to computable functions
    - Bibliography
categories:
    - Theorems in the foundations of mathematics
---
In mathematical logic, Goodstein's theorem is a statement about the natural numbers, proved by Reuben Goodstein in 1944, which states that every Goodstein sequence eventually terminates at 0. Kirby and Paris[1] showed that it is unprovable in Peano arithmetic (but it can be proven in stronger systems, such as second order arithmetic). This was the third example of a true statement that is unprovable in Peano arithmetic, after Gödel's incompleteness theorem and Gerhard Gentzen's 1943 direct proof of the unprovability of ε0-induction in Peano arithmetic. The Paris–Harrington theorem was a later example.
