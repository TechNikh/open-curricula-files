---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Linear_canonical_transformation
offline_file: ""
offline_thumbnail: ""
uuid: de6f7df2-eb58-4e69-941d-cc74d82c55c2
updated: 1484308795
title: Linear canonical transformation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-TFA_LCT_fresnel.jpg
tags:
    - Definition
    - Special cases
    - Composition
    - In optics and quantum mechanics
    - Applications
    - Electromagnetic wave propagation
    - Spherical lens
    - Spherical Mirror
    - Example
    - Notes
categories:
    - Hamiltonian mechanics
---
In Hamiltonian mechanics, the linear canonical transformation (LCT) is a family of integral transforms that generalizes many classical transforms. It has 4 parameters and 1 constraint, so it is a 3-dimensional family, and can be visualized as the action of the special linear group SL2(R) on the time–frequency plane (domain).
