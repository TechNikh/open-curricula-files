---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/net
offline_file: ""
offline_thumbnail: ""
uuid: 748216ef-5c4b-460a-aa49-c1710a5aecbb
updated: 1484310258
title: net
categories:
    - Dictionary
---
net
     adj 1: remaining after all deductions; "net profit" [syn: {nett}]
            [ant: {gross}]
     2: conclusive in a process or progression; "the final answer";
        "a last resort"; "the net result" [syn: {final}, {last}]
     n 1: a computer network consisting of a worldwide network of
          computer networks that use the TCP/IP network protocols
          to facilitate data transmission and exchange [syn: {Internet},
           {cyberspace}]
     2: a trap made of netting to catch fish or birds or insects
     3: the excess of revenues over outlays in a given period of
        time (including depreciation and other non-cash expenses)
        [syn: {net income}, {net ...
