---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cole_Prize
offline_file: ""
offline_thumbnail: ""
uuid: 14f7fb0f-1fef-4cba-9cd6-b107eb627c98
updated: 1484308544
title: Cole Prize
tags:
    - Frank Nelson Cole Prize in Algebra
    - Frank Nelson Cole Prize in Number Theory
categories:
    - Number theory
---
The Frank Nelson Cole Prize, or Cole Prize for short, is one of two prizes awarded to mathematicians by the American Mathematical Society, one for an outstanding contribution to algebra, and the other for an outstanding contribution to number theory.[1] The prize is named after Frank Nelson Cole, who served the Society for 25 years. The Cole Prize in algebra was funded by Cole himself, from funds given to him as a retirement gift; the prize fund was later augmented by his son, leading to the double award.[1][2]
