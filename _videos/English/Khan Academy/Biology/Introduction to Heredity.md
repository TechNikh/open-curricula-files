---
version: 1
type: video
provider: YouTube
id: eEUvRrhmcxM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Introduction%20to%20Heredity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5f0e1e09-5531-496b-93eb-c5e9e6121635
updated: 1486069583
title: Introduction to Heredity
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/eEUvRrhmcxM/default.jpg
    - https://i3.ytimg.com/vi/eEUvRrhmcxM/1.jpg
    - https://i3.ytimg.com/vi/eEUvRrhmcxM/2.jpg
    - https://i3.ytimg.com/vi/eEUvRrhmcxM/3.jpg
---
Heredity and Classical Genetics.  Dominant and recessive traits.  Heterozygous and homozygous genotypes.
More free lessons at: http://www.khanacademy.org/video?v=eEUvRrhmcxM
