---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Steal_(poker)
offline_file: ""
offline_thumbnail: ""
uuid: 3c5ceffa-13f9-4506-b75d-00656b60893e
updated: 1484309059
title: Steal (poker)
categories:
    - Poker strategy
---
In poker, a steal is a type of a bluff, a raise during the first betting round made with an inferior hand and meant to make other players fold superior hands because of shown strength. A steal is normally either an "ante steal" or "blind steal" (depending on whether the game being played uses antes or blinds).
