---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/victory
offline_file: ""
offline_thumbnail: ""
uuid: 06db5415-316a-4482-ad96-2c1094e414f2
updated: 1484310567
title: victory
categories:
    - Dictionary
---
victory
     n : a successful ending of a struggle or contest; "the general
         always gets credit for his army's victory"; "the
         agreement was a triumph for common sense" [syn: {triumph}]
         [ant: {defeat}]
