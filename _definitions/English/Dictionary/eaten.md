---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eaten
offline_file: ""
offline_thumbnail: ""
uuid: 9ac56082-52d2-4f43-a24b-e9367bd92dc0
updated: 1484310313
title: eaten
categories:
    - Dictionary
---
eaten
     adj : having been taken into the mouth for consumption [ant: {uneaten}]
