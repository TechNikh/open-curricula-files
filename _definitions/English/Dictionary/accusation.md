---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accusation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484517841
title: accusation
categories:
    - Dictionary
---
accusation
     n 1: a formal charge of wrongdoing brought against a person; the
          act of imputing blame or guilt [syn: {accusal}]
     2: an assertion that someone is guilty of a fault or offence;
        "the newspaper published charges that Jones was guilty of
        drunken driving" [syn: {charge}]
