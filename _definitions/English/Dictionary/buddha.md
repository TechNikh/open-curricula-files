---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/buddha
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484566981
title: buddha
categories:
    - Dictionary
---
Buddha
     n 1: founder of Buddhism; worshipped as a god (c 563-483 BC)
          [syn: {the Buddha}, {Siddhartha}, {Gautama}, {Gautama
          Siddhartha}, {Gautama Buddha}]
     2: one who has achieved a state of perfect enlightenment
