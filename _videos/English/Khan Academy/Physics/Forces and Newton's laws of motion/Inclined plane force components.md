---
version: 1
type: video
provider: YouTube
id: TC23wD34C7k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Forces%20and%20Newton%27s%20laws%20of%20motion/Inclined%20plane%20force%20components.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a13ef5cd-b59d-4636-b6d4-410f99af02a0
updated: 1486069591
title: Inclined plane force components
categories:
    - "Forces and Newton's laws of motion"
thumbnail_urls:
    - https://i3.ytimg.com/vi/TC23wD34C7k/default.jpg
    - https://i3.ytimg.com/vi/TC23wD34C7k/1.jpg
    - https://i3.ytimg.com/vi/TC23wD34C7k/2.jpg
    - https://i3.ytimg.com/vi/TC23wD34C7k/3.jpg
---
Figuring out the components of the force due to gravity that are parallel and perpendicular to the surface of an inclined plane. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/forces-newtons-laws/lubricon-vi/v/normal-forces-on-lubricon-vi?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/forces-newtons-laws/lubricon-vi/v/slow-sock-on-lubricon-vi?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
