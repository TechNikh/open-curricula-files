---
version: 1
type: video
provider: YouTube
id: U9PZizBDBZw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8e359597-c04a-4ccc-a733-6efbe9e54628
updated: 1486069660
title: Amazing Fire Illusion!
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/U9PZizBDBZw/default.jpg
    - https://i3.ytimg.com/vi/U9PZizBDBZw/1.jpg
    - https://i3.ytimg.com/vi/U9PZizBDBZw/2.jpg
    - https://i3.ytimg.com/vi/U9PZizBDBZw/3.jpg
---
