---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recommend
offline_file: ""
offline_thumbnail: ""
uuid: 200017bb-731c-4024-9a49-ec3a2b47d93a
updated: 1484310192
title: recommend
categories:
    - Dictionary
---
recommend
     v 1: push for something; "The travel agent recommended strongly
          that we not travel on Thanksgiving Day" [syn: {urge}, {advocate}]
     2: express a good opinion of [syn: {commend}]
     3: make attractive or acceptable; "Honesty recommends any
        person"
