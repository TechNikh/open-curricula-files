---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Donaldson%E2%80%93Thomas_theory'
offline_file: ""
offline_thumbnail: ""
uuid: 66886d50-a841-4e1d-888a-0522d51c9b89
updated: 1484309145
title: Donaldson–Thomas theory
tags:
    - Definition and examples
    - facts
    - Generalizations
categories:
    - Algebraic geometry
---
In mathematics, specifically algebraic geometry, Donaldson–Thomas theory is the theory of Donaldson–Thomas invariants. Given a compact moduli space of sheaves on a Calabi–Yau threefold, its Donaldson–Thomas invariant is the virtual number of its points, i.e., the integral of the cohomology class 1 against the virtual fundamental class. The Donaldson–Thomas invariant is a holomorphic analogue of the Casson invariant. The invariants were introduced by Simon Donaldson and Richard Thomas (1998). Donaldson–Thomas invariants have close connections to Gromov–Witten invariants of algebraic three-folds and the theory of stable pairs due to Pandharipande and Thomas.
