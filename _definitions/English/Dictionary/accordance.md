---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accordance
offline_file: ""
offline_thumbnail: ""
uuid: 0c44968f-9d56-425d-8e3b-0f0970e1145a
updated: 1484310240
title: accordance
categories:
    - Dictionary
---
accordance
     n 1: concurrence of opinion; "we are in accord with your
          proposal" [syn: {accord}, {conformity}]
     2: the act of granting rights; "the accordance to Canada of
        rights of access" [syn: {accordance of rights}]
