---
version: 1
type: video
provider: YouTube
id: _g_ml8tAnWE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Tricks/Turn%20Pennies%20Silver%20and%20Gold%20%28Chemistry%20Trick%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a1572b09-290e-4121-8303-84a59bf08ef6
updated: 1486069661
title: Turn Pennies Silver and Gold (Chemistry Trick)
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/_g_ml8tAnWE/default.jpg
    - https://i3.ytimg.com/vi/_g_ml8tAnWE/1.jpg
    - https://i3.ytimg.com/vi/_g_ml8tAnWE/2.jpg
    - https://i3.ytimg.com/vi/_g_ml8tAnWE/3.jpg
---
