---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ment
offline_file: ""
offline_thumbnail: ""
uuid: 3e324820-1d38-4c8a-8bc1-988690415319
updated: 1484308312
title: Adverb
tags:
    - Functions
    - Formation and comparison
    - Adverbs as a "catch-all" category
    - In specific languages
    - Bibliography
categories:
    - Uncategorized
---
An adverb is a word that modifies a verb, adjective, another adverb, determiner, noun phrase, clause, or sentence. Adverbs typically express manner, place, time, frequency, degree, level of certainty, etc., answering questions such as how?, in what way?, when?, where?, and to what extent?. This function is called the adverbial function, and may be realised by single words (adverbs) or by multi-word expressions (adverbial phrases and adverbial clauses).
