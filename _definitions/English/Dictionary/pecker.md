---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pecker
offline_file: ""
offline_thumbnail: ""
uuid: f0d68789-768c-420e-ae7f-6ee06955aceb
updated: 1484310289
title: pecker
categories:
    - Dictionary
---
pecker
     n 1: obscene terms for penis [syn: {cock}, {prick}, {dick}, {shaft},
           {peter}, {tool}, {putz}]
     2: bird with strong claws and a stiff tail adapted for climbing
        and a hard chisel-like bill for boring into wood for
        insects [syn: {woodpecker}, {peckerwood}]
     3: horny projecting mouth of a bird [syn: {beak}, {bill}, {neb},
         {nib}]
