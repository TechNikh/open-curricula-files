---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distillation
offline_file: ""
offline_thumbnail: ""
uuid: 7ed61e6b-6ae0-4ba0-8e14-ba7dd713e4c6
updated: 1484310407
title: distillation
categories:
    - Dictionary
---
distillation
     n 1: the process of purifying a liquid by boiling it and
          condensing its vapors [syn: {distillment}]
     2: a purified liquid produced by condensation from a vapor
        during distilling; the product of distilling [syn: {distillate}]
