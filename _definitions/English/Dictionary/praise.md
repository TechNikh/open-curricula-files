---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/praise
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484483161
title: praise
categories:
    - Dictionary
---
praise
     n 1: an expression of approval and commendation; "he always
          appreciated praise for his work" [syn: {congratulations},
           {kudos}]
     2: offering words of homage as an act of worship; "they sang a
        hymn of praise to God"
     v : express approval of; "The parents praised their children for
         their academic performance" [ant: {knock}]
