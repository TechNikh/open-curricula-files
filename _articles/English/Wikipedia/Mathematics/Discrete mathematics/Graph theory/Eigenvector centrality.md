---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eigenvector_centrality
offline_file: ""
offline_thumbnail: ""
uuid: 2c561ba4-7b48-48d3-9e54-1f2505b03814
updated: 1484308616
title: Eigenvector centrality
categories:
    - Graph theory
---
In graph theory, eigenvector centrality (also called eigencentrality) is a measure of the influence of a node in a network. It assigns relative scores to all nodes in the network based on the concept that connections to high-scoring nodes contribute more to the score of the node in question than equal connections to low-scoring nodes.
