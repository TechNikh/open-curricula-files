---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Soundness_(interactive_proof)
offline_file: ""
offline_thumbnail: ""
uuid: 0e07ea5f-2afc-402f-b7f4-d377c4ed1995
updated: 1484309499
title: Soundness (interactive proof)
categories:
    - Proof theory
---
Soundness is a property of interactive proof systems that requires that no prover can make the verifier accept for a wrong statement 
  
    
      
        y
        ∉
        L
      
    
    {\displaystyle y\not \in L}
  
 except with some small probability. The upper bound of this probability is referred to as the soundness error of a proof system.
