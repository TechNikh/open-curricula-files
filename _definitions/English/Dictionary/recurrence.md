---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recurrence
offline_file: ""
offline_thumbnail: ""
uuid: c5a322ac-fadb-43f0-880d-53214081da82
updated: 1484310561
title: recurrence
categories:
    - Dictionary
---
recurrence
     n : happening again (especially at regular intervals); "the
         return of spring" [syn: {return}]
