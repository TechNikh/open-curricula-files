---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strategic_Network_Formation
offline_file: ""
offline_thumbnail: ""
uuid: 4ffcbdaa-d6e4-473d-9611-96b12f13674b
updated: 1484309442
title: Strategic Network Formation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/page1-300px-15th_Century_Florentine_Marriges_Data_from_Padgett_and_Ansell.pdf.jpg
tags:
    - Introduction
    - Modeling Network Formation
    - Extensive form game modeling
    - Simultaneous move game modeling
    - Pairwise Stability
    - Network Efficiency
    - Distance-Based Utility
    - Externalities
categories:
    - Network theory
---
Strategic Network Formation defines how and why networks take particular forms. In many networks, the relation between nodes is determined by the choice of the participating players involved, not by an arbitrary rule. A “strategic” modeling of network requires defining a network’s costs and benefits and predicts how individual preferences become outcomes.
