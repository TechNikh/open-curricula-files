---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blamed
offline_file: ""
offline_thumbnail: ""
uuid: 847c4a65-b907-4299-888c-51fae4cbf840
updated: 1484310585
title: blamed
categories:
    - Dictionary
---
blamed
     adj : expletives used informally as intensifiers; "he's a blasted
           idiot"; "it's a blamed shame"; "a blame cold winter";
           "not a blessed dime"; "I'll be damned (or blessed or
           darned or goddamned) if I'll do any such thing"; "he's
           a damn (or goddam or goddamned) fool"; "a deuced
           idiot"; "tired or his everlasting whimpering"; "an
           infernal nuisance" [syn: {blasted}, {blame}, {blessed},
            {damn}, {damned}, {darned}, {deuced}, {everlasting}, {goddam},
            {goddamn}, {goddamned}, {infernal}]
