---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enable
offline_file: ""
offline_thumbnail: ""
uuid: 671cf48a-be43-46e0-a31d-b72a69ce52bb
updated: 1484310341
title: enable
categories:
    - Dictionary
---
enable
     v : render capable or able for some task; "This skill will
         enable you to find a job on Wall Street"; "The rope
         enables you to secure yourself when you climb the
         mountain" [ant: {disable}]
