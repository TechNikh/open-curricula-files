---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sometimes
offline_file: ""
offline_thumbnail: ""
uuid: 05024c4c-1653-4571-897a-cc965e4a9cff
updated: 1484310357
title: sometimes
categories:
    - Dictionary
---
sometimes
     adv : on certain occasions or in certain cases but not always;
           "sometimes she wished she were back in England";
           "sometimes her photography is breathtaking"; "sometimes
           they come for a month; at other times for six months"
