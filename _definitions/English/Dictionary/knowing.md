---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/knowing
offline_file: ""
offline_thumbnail: ""
uuid: e853a6b8-ffca-4059-a188-443e880b6241
updated: 1484310230
title: knowing
categories:
    - Dictionary
---
knowing
     adj 1: evidencing the possession of inside information [syn: {wise(p)},
             {wise to(p)}]
     2: by conscious design or purpose; "intentional damage"; "a
        knowing attempt to defraud"; "a willful waste of time"
        [syn: {deliberate}, {intentional}, {willful}, {wilful}]
     3: alert and fully informed; "politically aware"; "a knowing
        collector of rare books"; "the most...technically aware of
        the novelists under thirty"- W.S.Graham; "surprisingly
        knowledgeable about what was going on" [syn: {aware(p)}, {knowledgeable}]
     4: highly educated; having extensive information or
        understanding; "an enlightened public"; "knowing
    ...
