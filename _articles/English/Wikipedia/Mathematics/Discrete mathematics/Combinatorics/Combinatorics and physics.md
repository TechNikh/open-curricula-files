---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorics_and_physics
offline_file: ""
offline_thumbnail: ""
uuid: 857d9fae-7d4f-4a7a-a3e8-54134bb3708c
updated: 1484308570
title: Combinatorics and physics
tags:
    - Combinatorics and statistical physics
    - Conference proceedings
categories:
    - Combinatorics
---
"Combinatorial Physics is an emerging area which unites combinatorial and discrete mathematical techniques applied to theoretical physics, especially Quantum Theory.".[1]
