---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/holder
offline_file: ""
offline_thumbnail: ""
uuid: 58eb3513-73bf-4585-ad28-ba6c48872ee5
updated: 1484310206
title: holder
categories:
    - Dictionary
---
holder
     n 1: a holding device; "a towel holder"
     2: the person who is in possession of a check or note or bond
        or document of title that is endorsed to him or to whoever
        holds it; "the bond was marked `payable to bearer'" [syn:
        {bearer}]
