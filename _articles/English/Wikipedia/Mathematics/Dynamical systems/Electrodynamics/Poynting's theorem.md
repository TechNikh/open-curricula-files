---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poynting%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: bf0031d1-b3e0-4cb6-b1f2-54cb034d1be2
updated: 1484308757
title: "Poynting's theorem"
tags:
    - Statement
    - General
    - Electrical engineering
    - Derivation
    - "Poynting's theorem"
    - Poynting vector
    - Alternative forms
    - Generalization
    - Notes
categories:
    - Electrodynamics
---
In electrodynamics, Poynting's theorem is a statement of conservation of energy for the electromagnetic field,[clarification needed] in the form of a partial differential equation, due to the British physicist John Henry Poynting.[1] Poynting's theorem is analogous to the work-energy theorem in classical mechanics, and mathematically similar to the continuity equation, because it relates the energy stored in the electromagnetic field to the work done on a charge distribution (i.e. an electrically charged object), through energy flux.
