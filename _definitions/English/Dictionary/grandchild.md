---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grandchild
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484610241
title: grandchild
categories:
    - Dictionary
---
grandchild
     n : a child of your son or daughter
     [also: {grandchildren} (pl)]
