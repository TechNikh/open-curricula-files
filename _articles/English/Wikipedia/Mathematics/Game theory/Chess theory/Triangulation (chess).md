---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Triangulation_(chess)
offline_file: ""
offline_thumbnail: ""
uuid: 3ad5ac8c-c44e-464a-8178-77abcfbb1e8d
updated: 1484309020
title: Triangulation (chess)
tags:
    - Example
    - Triangulation with the king
    - Second example
    - Example in king and pawn endgame
    - Triangulation with other pieces
    - Example with a rook
    - Notes
categories:
    - Chess theory
---
Triangulation is a tactic used in chess to put one's opponent in zugzwang. That is, it is a tactic, the goal of which is to return to the initial position in such a way that one's opponent is then forced to move first in the position, when it is a disadvantage for that player to move, e.g. he must abandon a blockade and let the other player penetrate his position. Triangulation is also called losing a tempo or losing a move.
