---
version: 1
type: video
provider: YouTube
id: UPMrrG9ExJ4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bceeb070-9d36-4694-b784-98d9751a9a47
updated: 1486069623
title: Math trick ( multiplication by counting) part 1
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/UPMrrG9ExJ4/default.jpg
    - https://i3.ytimg.com/vi/UPMrrG9ExJ4/1.jpg
    - https://i3.ytimg.com/vi/UPMrrG9ExJ4/2.jpg
    - https://i3.ytimg.com/vi/UPMrrG9ExJ4/3.jpg
---
