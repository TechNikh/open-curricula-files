---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incredible
offline_file: ""
offline_thumbnail: ""
uuid: 997ad65d-5e96-4d24-932d-8d482f66f35f
updated: 1484310443
title: incredible
categories:
    - Dictionary
---
incredible
     adj : beyond belief or understanding; "at incredible speed"; "the
           book's plot is simply incredible" [syn: {unbelievable}]
           [ant: {credible}]
