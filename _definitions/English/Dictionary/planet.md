---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/planet
offline_file: ""
offline_thumbnail: ""
uuid: ed85d75f-6fd2-4529-a191-ee592d0615db
updated: 1484310279
title: planet
categories:
    - Dictionary
---
planet
     n 1: any of the celestial bodies (other than comets or
          satellites) that revolve around the sun in the solar
          system
     2: a person who follows or serves another [syn: {satellite}]
