---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shaved
offline_file: ""
offline_thumbnail: ""
uuid: 0bfb8e70-7448-449b-8b2a-73364e1ddf7e
updated: 1484310168
title: shaved
categories:
    - Dictionary
---
shaved
     adj : having the beard or hair cut off close to the skin [syn: {shaven}]
           [ant: {unshaven}]
