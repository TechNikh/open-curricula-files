---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sudden
offline_file: ""
offline_thumbnail: ""
uuid: e4302795-2c54-4569-9a81-142813ea400e
updated: 1484310461
title: sudden
categories:
    - Dictionary
---
sudden
     adj : happening without warning or in a short space of time; "a
           sudden storm"; "a sudden decision"; "a sudden cure"
           [ant: {gradual}]
