---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tension
offline_file: ""
offline_thumbnail: ""
uuid: 7ce8c00b-ea61-4e34-9612-47795f7e8e0a
updated: 1484310321
title: tension
categories:
    - Dictionary
---
tension
     n 1: feelings of hostility that are not manifest; "he could sense
          her latent hostility to him"; "the diplomats' first
          concern was to reduce international tensions" [syn: {latent
          hostility}]
     2: (psychology) a state of mental or emotional strain or
        suspense; "he suffered from fatigue and emotional
        tension"; "stress is a vasoconstrictor" [syn: {tenseness},
         {stress}]
     3: the physical condition of being stretched or strained; "it
        places great tension on the leg muscles"; "he could feel
        the tenseness of her body" [syn: {tensity}, {tenseness}, {tautness}]
     4: a balance between and interplay of opposing ...
