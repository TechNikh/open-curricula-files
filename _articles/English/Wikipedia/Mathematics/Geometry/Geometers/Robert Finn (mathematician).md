---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_Finn_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: e5093958-0a57-4549-804b-036170d14848
updated: 1484309080
title: Robert Finn (mathematician)
categories:
    - Geometers
---
Finn received in 1951 his PhD from Syracuse University under Abe Gelbart with thesis On some properties of the solution of a class of non-linear partial differential equations.[1] As a postdoc he was in 1953 at the Institute for Advanced Study and in 1953/54 at the Institute for Hydrodynamics at the University of Maryland. He became in 1954 an assistant professor at the University of Southern California and in 1956 an associate professor at Caltech. Since 1959 he has been a professor at Stanford University.[2]
