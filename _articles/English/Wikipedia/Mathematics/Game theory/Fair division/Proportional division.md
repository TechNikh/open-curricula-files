---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proportional_division
offline_file: ""
offline_thumbnail: ""
uuid: a1b921ab-0d1d-4f38-a4ab-c3562a98bc64
updated: 1484308513
title: Proportional division
tags:
    - Existence
    - Variants
    - Super-proportional division
    - Relations to other fairness criteria
    - Implications between proportionality and envy-freeness
    - Stability to voluntary exchanges
    - Individual rationality
categories:
    - Fair division
---
A proportional division is a kind of fair division in which a resource is divided among n partners with subjective valuations, giving each partner at least 1/n of the resource by his/her own subjective valuation. For example, consider a land asset that has to be divided among 3 heirs: Alice and Bob who think that it's worth 3 million dollars, and George who thinks that it's worth $4.5M. In a proportional division, Alice receives a land-plot that she believes to be worth at least $1M, Bob receives a land-plot that he believes to be worth at least $1M (even though Alice may think it is worth less), and George receives a land-plot that he believes to be worth at least $1.5M.
