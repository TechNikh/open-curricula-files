---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/list
offline_file: ""
offline_thumbnail: ""
uuid: c085b638-f237-472b-bbf6-875a2d244c49
updated: 1484310319
title: list
categories:
    - Dictionary
---
list
     n 1: a database containing an ordered array of items (names or
          topics) [syn: {listing}]
     2: the property possessed by a line or surface that departs
        from the vertical; "the tower had a pronounced tilt"; "the
        ship developed a list to starboard"; "he walked with a
        heavy inclination to the right" [syn: {tilt}, {inclination},
         {lean}, {leaning}]
     v 1: give or make a list of; name individually; give the names
          of; "List the states west of the Mississippi" [syn: {name}]
     2: include in a list; "Am I listed in your register?"
     3: enumerate; "We must number the names of the great
        mathematicians" [syn: {number}]
     ...
