---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chenab
offline_file: ""
offline_thumbnail: ""
uuid: 5fd8eedb-8367-4c80-9f0b-c7be13236813
updated: 1484308419
title: Chenab River
categories:
    - Uncategorized
---
The Chenab River is a major river of India and Pakistan. It forms in the upper Himalayas in the Lahaul and Spiti district of Himachal Pradesh, India, and flows through the Jammu region of Jammu and Kashmir into the plains of the Punjab, Pakistan. The waters of the Chenab are allocated to Pakistan under the terms of the Indus Waters Treaty.[2][3]
