---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reality
offline_file: ""
offline_thumbnail: ""
uuid: 134032b3-cd3f-4fb7-a57b-dc754fa8a7d3
updated: 1484310448
title: reality
categories:
    - Dictionary
---
reality
     n 1: all of your experiences that determine how things appear to
          you; "his world was shattered"; "we live in different
          worlds"; "for them demons were as much a part of reality
          as trees were" [syn: {world}]
     2: the state of being actual or real; "the reality of his
        situation slowly dawned on him" [syn: {realness}, {realism}]
        [ant: {unreality}]
     3: the state of the world as it really is rather than as you
        might want it to be; "businessmen have to face harsh
        realities"
     4: the quality possessed by something that is real [ant: {unreality}]
