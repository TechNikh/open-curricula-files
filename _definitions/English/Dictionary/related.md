---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/related
offline_file: ""
offline_thumbnail: ""
uuid: 89c6ed55-a49d-4fee-98c3-f64b683b5bd8
updated: 1484310353
title: related
categories:
    - Dictionary
---
related
     adj 1: being connected or associated; "painting and the related
            arts"; "school-related activities"; "related to
            micelle formation is the...ability of detergent
            actives to congregate at oil-water interfaces" [syn: {related
            to}] [ant: {unrelated}]
     2: connected by kinship, common origin, or marriage [ant: {unrelated}]
     3: similar or related in quality or character; "a feeling akin
        to terror"; "kindred souls"; "the amateur is closely
        related to the collector" [syn: {akin(p)}, {kindred}]
     4: having close kinship and appropriateness; "he asks questions
        that are germane and central to the issue" [syn: ...
