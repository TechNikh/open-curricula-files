---
version: 1
type: article
id: https://en.wikipedia.org/wiki/All-pairs_testing
offline_file: ""
offline_thumbnail: ""
uuid: a140f1dc-e305-47fa-af06-578c2c1dfaa4
updated: 1484308567
title: All-pairs testing
tags:
    - Rationale
    - N-wise testing
    - Example
    - Notes
categories:
    - Combinatorics
---
In computer science, all-pairs testing or pairwise testing is a combinatorial method of software testing that, for each pair of input parameters to a system (typically, a software algorithm), tests all possible discrete combinations of those parameters. Using carefully chosen test vectors, this can be done much faster than an exhaustive search of all combinations of all parameters, by "parallelizing" the tests of parameter pairs.
