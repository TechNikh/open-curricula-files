---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Turing_reduction
offline_file: ""
offline_thumbnail: ""
uuid: d2a4ce09-4756-48f9-8e72-3980d7bef6d1
updated: 1484309492
title: Turing reduction
tags:
    - Definition
    - >
        Relation of Turing completeness to computational
        universality
    - Example
    - Properties
    - The use of a reduction
    - Stronger reductions
    - Weaker reductions
categories:
    - Computability theory
---
In computability theory, a Turing reduction from a problem A to a problem B, is a reduction which solves A, assuming the solution to B is already known (Rogers 1967, Soare 1987). It can be understood as an algorithm that could be used to solve A if it had available to it a subroutine for solving B. More formally, a Turing reduction is a function computable by an oracle machine with an oracle for B. Turing reductions can be applied to both decision problems and function problems.
