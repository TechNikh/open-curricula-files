---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/draped
offline_file: ""
offline_thumbnail: ""
uuid: 6ca8c973-c275-465d-92be-65820e1d942d
updated: 1484310601
title: draped
categories:
    - Dictionary
---
draped
     adj 1: covered with or as if with clothes or a wrap or cloak;
            "leaf-clothed trees"; "fog-cloaked meadows"; "a beam
            draped with cobwebs"; "cloud-wrapped peaks" [syn: {cloaked},
             {clothed}, {mantled}, {wrapped}]
     2: covered in folds of cloth; "velvet-draped windows"
