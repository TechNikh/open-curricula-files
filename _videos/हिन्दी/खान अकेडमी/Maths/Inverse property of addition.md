---
version: 1
type: video
provider: YouTube
id: xlASG4r4rDg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4ed7f4d0-affd-4707-b6ca-7814101655fd
updated: 1486069611
title: Inverse property of addition
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/xlASG4r4rDg/default.jpg
    - https://i3.ytimg.com/vi/xlASG4r4rDg/1.jpg
    - https://i3.ytimg.com/vi/xlASG4r4rDg/2.jpg
    - https://i3.ytimg.com/vi/xlASG4r4rDg/3.jpg
---
