---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jamaican
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484644021
title: jamaican
categories:
    - Dictionary
---
Jamaican
     adj : of or relating to Jamaica (the island or the country) or to
           its inhabitants; "Jamaican rum"; "the Jamaican Prime
           Minister"
     n : a native or inhabitant of Jamaica
