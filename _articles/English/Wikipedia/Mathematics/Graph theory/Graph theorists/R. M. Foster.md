---
version: 1
type: article
id: https://en.wikipedia.org/wiki/R._M._Foster
offline_file: ""
offline_thumbnail: ""
uuid: 63d165e1-dd1b-4f06-a6b7-32c72dd0d9e4
updated: 1484309461
title: R. M. Foster
tags:
    - Education
    - Professional career
    - Publications
categories:
    - Graph theorists
---
Ronald Martin Foster (3 October 1896 – 2 February 1998), was a Bell Labs mathematician whose work was of significance regarding electronic filters for use on telephone lines. He published an important paper, A Reactance Theorem,[1] (see Foster's reactance theorem) which quickly inspired Wilhelm Cauer to begin his program of network synthesis filters which put the design of filters on a firm mathematical footing.[2] He is also known for the Foster census of cubic symmetric graphs[3] and the 90-vertex cubic symmetric Foster graph.
