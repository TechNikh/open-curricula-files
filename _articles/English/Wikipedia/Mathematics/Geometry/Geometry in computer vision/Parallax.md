---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parallax
offline_file: ""
offline_thumbnail: ""
uuid: e54f4faa-b0d4-4ac1-8073-2a491149d3a9
updated: 1484309184
title: Parallax
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Parallax_Example.svg.png
tags:
    - Visual perception
    - Parallax in astronomy
    - Stellar parallax
    - Distance measurement
    - Diurnal parallax
    - Lunar parallax
    - Solar parallax
    - Dynamic or moving-cluster parallax
    - Derivation
    - Parallax error in astronomy
    - Spatio-temporal parallax
    - Parallax error in measurement instruments
    - Photogrammetric parallax
    - Parallax error in photography
    - Parallax in sights
    - Parallax in optical sights
    - Artillery gunfire
    - Parallax rangefinders
    - As a metaphor
    - Notes
categories:
    - Geometry in computer vision
---
Parallax is a displacement or difference in the apparent position of an object viewed along two different lines of sight, and is measured by the angle or semi-angle of inclination between those two lines.[1][2] The term is derived from the Greek word παράλλαξις (parallaxis), meaning "alteration". Due to foreshortening, nearby objects have a larger parallax than more distant objects when observed from different positions, so parallax can be used to determine distances.
