---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bio-inspired_computing
offline_file: ""
offline_thumbnail: ""
uuid: 5fb8a9c2-b498-46f3-a67c-e01a807f977f
updated: 1484308671
title: Bio-inspired computing
tags:
    - Areas of research
    - Artificial intelligence
categories:
    - Theoretical computer science
---
Bio-inspired computing, short for biologically inspired computing, is a field of study that loosely knits together subfields related to the topics of connectionism, social behaviour and emergence. It is often closely related to the field of artificial intelligence, as many of its pursuits can be linked to machine learning. It relies heavily on the fields of biology, computer science and mathematics. Briefly put, it is the use of computers to model the living phenomena, and simultaneously the study of life to improve the usage of computers. Biologically inspired computing is a major subset of natural computation.
