---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lagrange_bracket
offline_file: ""
offline_thumbnail: ""
uuid: 6f581fc8-76a3-43fc-a3e4-21af168f6c42
updated: 1484308787
title: Lagrange bracket
tags:
    - Definition
    - Properties
categories:
    - Hamiltonian mechanics
---
Lagrange brackets are certain expressions closely related to Poisson brackets that were introduced by Joseph Louis Lagrange in 1808–1810 for the purposes of mathematical formulation of classical mechanics, but unlike the Poisson brackets, have fallen out of use.
