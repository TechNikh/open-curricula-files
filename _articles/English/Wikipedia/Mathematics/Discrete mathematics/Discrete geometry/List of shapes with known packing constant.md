---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_shapes_with_known_packing_constant
offline_file: ""
offline_thumbnail: ""
uuid: c19a4d22-4e17-468a-bd5f-569522d8a056
updated: 1484308612
title: List of shapes with known packing constant
categories:
    - Discrete geometry
---
The packing constant of a geometric body is the largest average density achieved by packing arrangements of congruent copies of the body. For most bodies the value of the packing constant is unknown.[1] The following is a list of bodies in Euclidean spaces whose packing constant is known.[1] Fejes Tóth proved that in the plane, a point symmetric body has a packing constant that is equal to its translative packing constant and its lattice packing constant.[2] Therefore, any such body for which the lattice packing constant was previously known, such as any ellipse, consequently has a known packing constant. In addition to these bodies, the packing constants of hyperspheres in 8 and 24 ...
