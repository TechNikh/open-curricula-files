---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recently
offline_file: ""
offline_thumbnail: ""
uuid: ee628353-cdc2-4cae-b3b2-55592dc42ad7
updated: 1484310473
title: recently
categories:
    - Dictionary
---
recently
     adv 1: in the recent past; "he was in Paris recently"; "lately the
            rules have been enforced"; "as late as yesterday she
            was fine"; "feeling better of late"; "the spelling was
            first affected, but latterly the meaning also" [syn: {late},
             {lately}, {of late}, {latterly}]
     2: very recently; "they are newly married"; "newly raised
        objections"; "a newly arranged hairdo"; "grass new washed
        by the rain"; "a freshly cleaned floor"; "we are fresh out
        of tomatoes" [syn: {newly}, {freshly}, {fresh}, {new}]
