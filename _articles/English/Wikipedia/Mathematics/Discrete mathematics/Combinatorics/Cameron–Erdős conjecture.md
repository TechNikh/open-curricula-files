---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cameron%E2%80%93Erd%C5%91s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 64b6166a-2a50-4b1c-b076-7c9c2c7796c0
updated: 1484308582
title: Cameron–Erdős conjecture
categories:
    - Combinatorics
---
In combinatorics, the Cameron–Erdős conjecture (now a theorem) is the statement that the number of sum-free sets contained in 
  
    
      
        
          |
        
        N
        
          |
        
        =
        {
        1
        ,
        …
        ,
        N
        }
      
    
    {\displaystyle |N|=\{1,\ldots ,N\}}
  
 is 
  
    
      
        O
        
          (
          
            
              2
              
                N
                
                  /
                
                2
              
            
          
          )
        
        .
      
    
    {\displaystyle O\left({2^{N/2}}\right).}
