---
version: 1
type: video
provider: YouTube
id: CK9NWbfyY9s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 23c0b3f3-9487-4a5c-9a8d-2e7dee7faaee
updated: 1486069622
title: 'Cube Root  Shortcut Trick for Competitive Exams'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/CK9NWbfyY9s/default.jpg
    - https://i3.ytimg.com/vi/CK9NWbfyY9s/1.jpg
    - https://i3.ytimg.com/vi/CK9NWbfyY9s/2.jpg
    - https://i3.ytimg.com/vi/CK9NWbfyY9s/3.jpg
---
