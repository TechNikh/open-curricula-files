---
version: 1
type: video
provider: YouTube
id: rESzyhPOJ7I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Chemical%20reactions%20and%20stoichiometry/Stoichiometry-%20Limiting%20reagent.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0ac148f7-959a-43f5-a7f6-f2b43f285890
updated: 1486069585
title: 'Stoichiometry- Limiting reagent'
categories:
    - Chemical reactions and stoichiometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/rESzyhPOJ7I/default.jpg
    - https://i3.ytimg.com/vi/rESzyhPOJ7I/1.jpg
    - https://i3.ytimg.com/vi/rESzyhPOJ7I/2.jpg
    - https://i3.ytimg.com/vi/rESzyhPOJ7I/3.jpg
---
Stoichiometry problem where we find the limiting reagent and calculate grams of product formed. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/limiting-reagent-stoichiometry/v/limiting-reactant-example-problem-1?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/stoichiometry-ideal/v/stoichiometry-example-problem-2?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
