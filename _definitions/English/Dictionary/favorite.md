---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/favorite
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484471101
title: favorite
categories:
    - Dictionary
---
favorite
     adj 1: appealing to the general public; "a favorite tourist
            attraction" [syn: {favourite}]
     2: preferred above all others and treated with partiality; "the
        favored child" [syn: {favored}, {favorite(a)}, {favourite(a)},
         {pet}, {preferred}]
     n 1: something regarded with special favor or liking; "that book
          is one of my favorites" [syn: {favourite}]
     2: a special loved one [syn: {darling}, {favourite}, {pet}, {dearie},
         {deary}, {ducky}]
     3: a competitor thought likely to win [syn: {front-runner}, {favourite}]
