---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eleven
offline_file: ""
offline_thumbnail: ""
uuid: 08ccfe86-def6-4873-b953-88328b1e125c
updated: 1484310395
title: eleven
categories:
    - Dictionary
---
eleven
     adj : being one more than ten [syn: {11}, {xi}]
     n 1: the cardinal number that is the sum of ten and one [syn: {11},
           {XI}]
     2: a team that plays football [syn: {football team}]
