---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jalgaon
offline_file: ""
offline_thumbnail: ""
uuid: 1983c1ed-863b-4f37-b400-3a5d28991407
updated: 1484308455
title: Jalgaon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Khandesh_Central.jpg
tags:
    - geography
    - Demographics
    - Business
    - Connections
    - Road
    - Rail
    - air
    - Temples
    - Gandhi Teerth
    - Education
categories:
    - Uncategorized
---
Jalgaon ( pronunciation (help·info)) is a city in western India, north of Maharashtra in Jalgaon district and in the agricultural region of Khandesh. Jalgaon has a municipal corporation, and had 460,468 residents at the 2011 census. It is 59 km (37 mi) from the Ajanta Caves. Jalgaon, in a Central Railway zone, can be reached by National Highway 6 and has an airport.
