---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proudly
offline_file: ""
offline_thumbnail: ""
uuid: d6cba213-36ae-44b1-a4c8-b98c0c9b8f33
updated: 1484310174
title: proudly
categories:
    - Dictionary
---
proudly
     adv : in a proud manner; "he walked proudly into town" [syn: {with
           pride}]
