---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fractional_graph_isomorphism
offline_file: ""
offline_thumbnail: ""
uuid: f7a37862-e67a-4542-b5c9-a7898c6e29a4
updated: 1484308627
title: Fractional graph isomorphism
tags:
    - Computational complexity
    - Equivalence to coarsest equitable partition
categories:
    - Graph theory
---
In graph theory, a fractional isomorphism of graphs whose adjacency matrices are denoted A and B is a doubly stochastic matrix D such that DA = BD. If the doubly stochastic matrix is a permutation matrix, then it constitutes a graph isomorphism.
