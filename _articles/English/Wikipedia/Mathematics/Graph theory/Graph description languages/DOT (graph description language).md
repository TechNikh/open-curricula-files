---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/DOT_(graph_description_language)
offline_file: ""
offline_thumbnail: ""
uuid: 75b6aec2-a86a-406a-9209-fe4ffec61c3b
updated: 1484309409
title: DOT (graph description language)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-DotLanguageUndirected.svg.png
tags:
    - Syntax
    - Graph types
    - Undirected graphs
    - Directed graphs
    - Attributes
    - Comments
    - A simple example
    - Layout programs
    - Limitations
    - Notes
categories:
    - Graph description languages
---
DOT is a plain text graph description language. DOT graphs are typically files with the file extension gv or dot. The extension gv is preferred to avoid confusion with the extension dot used by early (pre-2007) versions of Microsoft Word.[1]
