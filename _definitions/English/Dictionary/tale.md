---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tale
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484614261
title: tale
categories:
    - Dictionary
---
tale
     n 1: a message that tells the particulars of an act or occurrence
          or course of events; presented in writing or drama or
          cinema or as a radio or television program; "his
          narrative was interesting"; "Disney's stories entertain
          adults as well as children" [syn: {narrative}, {narration},
           {story}]
     2: a trivial lie; "he told a fib about eating his spinach";
        "how can I stop my child from telling stories?" [syn: {fib},
         {story}, {tarradiddle}, {taradiddle}]
