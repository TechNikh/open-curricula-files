---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roam
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484610181
title: roam
categories:
    - Dictionary
---
roam
     v : move about aimlessly or without any destination, often in
         search of food or employment; "The gypsies roamed the
         woods"; "roving vagabonds"; "the wandering Jew"; "The
         cattle roam across the prairie"; "the laborers drift from
         one town to the next"; "They rolled from town to town"
         [syn: {roll}, {wander}, {swan}, {stray}, {tramp}, {cast},
          {ramble}, {rove}, {range}, {drift}, {vagabond}]
