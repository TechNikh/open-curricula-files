---
version: 1
type: video
provider: YouTube
id: o9OvyW2Tf2I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fa8a8fb4-fbf4-450c-b6f4-b41844871d34
updated: 1486069654
title: Equipartition Theorem (Animation) Lesson 1
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/o9OvyW2Tf2I/default.jpg
    - https://i3.ytimg.com/vi/o9OvyW2Tf2I/1.jpg
    - https://i3.ytimg.com/vi/o9OvyW2Tf2I/2.jpg
    - https://i3.ytimg.com/vi/o9OvyW2Tf2I/3.jpg
---
