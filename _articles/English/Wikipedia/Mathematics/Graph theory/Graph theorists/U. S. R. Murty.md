---
version: 1
type: article
id: https://en.wikipedia.org/wiki/U._S._R._Murty
offline_file: ""
offline_thumbnail: ""
uuid: f41a2e45-fd4c-4e9e-96e0-01be7b91e383
updated: 1484309468
title: U. S. R. Murty
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/160px-USRMurty-UNICAMP-1996.png
categories:
    - Graph theorists
---
Uppaluri Siva Ramachandra Murty,[1][2] or U. S. R. Murty (as he prefers to write his name), is a Professor Emeritus of the Department of Combinatorics and Optimization, University of Waterloo.[3][4]
