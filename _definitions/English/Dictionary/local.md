---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/local
offline_file: ""
offline_thumbnail: ""
uuid: 0c4ee25f-f6b6-4a61-ba0f-184544cb3010
updated: 1484310238
title: local
categories:
    - Dictionary
---
local
     adj 1: relating to or applicable to or concerned with the
            administration of a city or town or district rather
            than a larger area; "local taxes"; "local authorities"
            [ant: {national}]
     2: of or belonging to or characteristic of a particular
        locality or neighborhood; "local customs"; "local
        schools"; "the local citizens"; "a local point of view";
        "local outbreaks of flu"; "a local bus line"
     3: affecting only a restricted part or area of the body; "local
        anesthesia" [ant: {general}]
     n 1: public transport consisting of a bus or train that stops at
          all stations or stops; "the local seemed to ...
