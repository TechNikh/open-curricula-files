---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surviving
offline_file: ""
offline_thumbnail: ""
uuid: 4631b4e5-7e0a-4b9f-8220-5ea558c463f6
updated: 1484310471
title: surviving
categories:
    - Dictionary
---
surviving
     adj : still in existence; "the Wollemi pine found in Australia is
           a surviving specimen of a conifer thought to have been
           long extinct and therefore known as a living fossil";
           "the only surviving frontier blockhouse in
           Pennsylvania" [syn: {living}]
