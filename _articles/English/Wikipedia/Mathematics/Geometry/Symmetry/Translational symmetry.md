---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Translational_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 8ed02db6-54b3-4534-a3b4-52e5efad50b2
updated: 1484309322
title: Translational symmetry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Translation_of_a_set.svg.png
tags:
    - Geometry
    - Examples
    - Text
    - calculus
categories:
    - Symmetry
---
In physics and mathematics, continuous translational symmetry is the invariance of a system of equations under any translation. Discrete translational symmetry is invariant under discrete translation.
