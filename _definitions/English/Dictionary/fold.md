---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fold
offline_file: ""
offline_thumbnail: ""
uuid: e1254a76-66f6-4864-a9c2-f01cce6e01b0
updated: 1484310327
title: fold
categories:
    - Dictionary
---
fold
     n 1: an angular or rounded shape made by folding; "a fold in the
          napkin"; "a crease in his trousers"; "a plication on her
          blouse"; "a flexure of the colon"; "a bend of his elbow"
          [syn: {crease}, {plication}, {flexure}, {crimp}, {bend}]
     2: a group of people who adhere to a common faith and
        habitually attend a given church [syn: {congregation}, {faithful}]
     3: a folded part (as a fold of skin or muscle) [syn: {plica}]
     4: a pen for sheep [syn: {sheepfold}, {sheep pen}, {sheepcote}]
     5: the act of folding; "he gave the napkins a double fold"
        [syn: {folding}]
     v 1: bend or lay so that one part covers the other; "fold ...
