---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/red
offline_file: ""
offline_thumbnail: ""
uuid: df0183cd-f3a6-4dfc-bb3c-40bdd2e4bb8e
updated: 1484310323
title: red
categories:
    - Dictionary
---
red
     adj 1: having any of numerous bright or strong colors reminiscent
            of the color of blood or cherries or tomatoes or
            rubies [syn: {reddish}, {ruddy}, {blood-red}, {carmine},
             {cerise}, {cherry}, {cherry-red}, {crimson}, {ruby},
            {ruby-red}, {scarlet}]
     2: characterized by violence or bloodshed; "writes of crimson
        deeds and barbaric days"- Andrea Parke; "fann'd by
        Conquest's crimson wing"- Thomas Gray; "convulsed with red
        rage"- Hudson Strode [syn: {crimson}, {violent}]
     3: (especially of the face) reddened or suffused with or as if
        with blood from emotion or exertion; "crimson with fury";
        ...
