---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/centered
offline_file: ""
offline_thumbnail: ""
uuid: 510c11bc-591a-41a3-b172-ceba9c8fe1e6
updated: 1484310480
title: centered
categories:
    - Dictionary
---
centered
     adj 1: being or placed in the center
     2: concentrated on or clustered around a central point or
        purpose [syn: {centred}, {centralized}, {centralised}, {focused}]
