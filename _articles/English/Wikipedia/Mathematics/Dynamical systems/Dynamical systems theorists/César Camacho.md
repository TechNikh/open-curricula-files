---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/C%C3%A9sar_Camacho'
offline_file: ""
offline_thumbnail: ""
uuid: dfdfaf49-554f-4174-8c40-c77ac9867ccb
updated: 1484308678
title: César Camacho
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/240px-C%25C3%25A9sarCamacho.jpg'
categories:
    - Dynamical systems theorists
---
César Leopoldo Camacho Manco (15 April 1943, Lima, Peru), better known as simply César Camacho, is a Peruvian-born Brazilian mathematician and former director of the IMPA. His area of research is dynamical systems theory.[1]
