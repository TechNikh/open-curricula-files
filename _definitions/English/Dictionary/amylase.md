---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amylase
offline_file: ""
offline_thumbnail: ""
uuid: 98e5bc0e-2189-450c-bd00-aa01089de186
updated: 1484310339
title: amylase
categories:
    - Dictionary
---
amylase
     n : any of a group of proteins found in saliva and pancreatic
         juice and parts of plants; help convert starch to sugar
