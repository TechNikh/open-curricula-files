---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Onsager_reciprocal_relations
offline_file: ""
offline_thumbnail: ""
uuid: f96eb635-e175-4cd4-8e3f-7ea679443b72
updated: 1484308814
title: Onsager reciprocal relations
tags:
    - 'Example: Fluid system'
    - The fundamental equation
    - The continuity equations
    - The phenomenological equations
    - The rate of entropy production
    - The Onsager reciprocal relations
    - Abstract formulation
    - Proof
categories:
    - Non-equilibrium thermodynamics
---
In thermodynamics, the Onsager reciprocal relations express the equality of certain ratios between flows and forces in thermodynamic systems out of equilibrium, but where a notion of local equilibrium exists.
