---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Precision_tests_of_QED
offline_file: ""
offline_thumbnail: ""
uuid: 9468aafc-6dfc-46c5-8c20-f15d126d369d
updated: 1484308757
title: Precision tests of QED
tags:
    - >
        Measurements of the fine-structure constant using different
        systems
    - Low-energy measurements
    - Anomalous magnetic dipole moments
    - Atom-recoil measurements
    - Neutron Compton wavelength
    - Hyperfine splitting
    - Lamb shift
    - Positronium
    - High-energy QED processes
    - Condensed matter systems
    - Other tests
categories:
    - Electrodynamics
---
The most precise and specific tests of QED consist of measurements of the electromagnetic fine structure constant, α, in various physical systems. Checking the consistency of such measurements tests the theory.
