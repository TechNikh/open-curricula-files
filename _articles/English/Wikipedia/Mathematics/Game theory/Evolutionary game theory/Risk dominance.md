---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Risk_dominance
offline_file: ""
offline_thumbnail: ""
uuid: bdf3ef3f-c599-43d9-a176-d2042b3ca43a
updated: 1484308520
title: Risk dominance
tags:
    - Formal definition
    - Equilibrium selection
    - Notes
categories:
    - Evolutionary game theory
---
Risk dominance and payoff dominance are two related refinements of the Nash equilibrium (NE) solution concept in game theory, defined by John Harsanyi and Reinhard Selten. A Nash equilibrium is considered payoff dominant if it is Pareto superior to all other Nash equilibria in the game.1 When faced with a choice among equilibria, all players would agree on the payoff dominant equilibrium since it offers to each player at least as much payoff as the other Nash equilibria. Conversely, a Nash equilibrium is considered risk dominant if it has the largest basin of attraction (i.e. is less risky). This implies that the more uncertainty players have about the actions of the other player(s), the ...
