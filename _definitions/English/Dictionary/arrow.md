---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arrow
offline_file: ""
offline_thumbnail: ""
uuid: 62b10a3f-4abb-46a6-b58b-04c493537760
updated: 1484310206
title: arrow
categories:
    - Dictionary
---
arrow
     n 1: a mark to indicate a direction or relation [syn: {pointer}]
     2: a projectile with a straight thin shaft and an arrowhead on
        one end and stabilizing vanes on the other; intended to be
        shot from a bow
