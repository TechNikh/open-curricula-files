---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greenish
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484598841
title: greenish
categories:
    - Dictionary
---
greenish
     adj : similar to the color of fresh grass; "a green tree"; "green
           fields"; "green paint" [syn: {green}, {light-green}, {dark-green}]
