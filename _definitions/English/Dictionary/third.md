---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/third
offline_file: ""
offline_thumbnail: ""
uuid: bcbd3c0c-3f2b-4576-91b7-b37576b74d5f
updated: 1484310299
title: third
categories:
    - Dictionary
---
third
     adj 1: coming next after the second and just before the fourth in
            position [syn: {3rd}, {tertiary}]
     2: being one of three equal parts; "a third share of the money"
        [syn: {third(a)}]
     n 1: one of three equal parts of a divisible whole; "it contains
          approximately a third of the minimum daily requirement"
          [syn: {one-third}, {tierce}]
     2: the fielding position of the player on a baseball team who
        is stationed near 3rd base; "he is playing third" [syn: {third
        base}]
     3: following the second position in an ordering or series; "a
        distant third"; "he answered the first question willingly,
        the second ...
