---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rigidity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484599261
title: rigidity
categories:
    - Dictionary
---
rigidity
     n 1: the physical property of being stiff and resisting bending
          [syn: {rigidness}]
     2: the quality of being rigid and rigorously severe [syn: {inflexibility}]
        [ant: {flexibility}]
