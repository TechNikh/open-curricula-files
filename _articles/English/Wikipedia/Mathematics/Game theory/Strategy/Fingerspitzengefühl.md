---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fingerspitzengef%C3%BChl'
offline_file: ""
offline_thumbnail: ""
uuid: 2d9c9d29-ad9b-41a5-855c-4bde3f5a8fad
updated: 1484309065
title: Fingerspitzengefühl
tags:
    - Military context
    - Related concepts
categories:
    - Strategy
---
Fingerspitzengefühl [ˈfɪŋɐˌʃpɪtsənɡəˌfyːl] is a German term, literally meaning "finger tips feeling" and meaning intuitive flair or instinct, which has been appropriated by the English language as a loanword. It describes a great situational awareness, and the ability to respond most appropriately and tactfully. It can also be applied to diplomats, bearers of bad news, or to describe a superior ability to respond to an escalated situation. According to Thomas Rongen, calling a soccer game and describing the brilliance of Christian Pulisic, the term is in common use to describe the instinctive play of certain players.
