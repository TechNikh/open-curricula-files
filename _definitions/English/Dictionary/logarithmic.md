---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/logarithmic
offline_file: ""
offline_thumbnail: ""
uuid: da68e1e7-88b5-4c23-a9af-00eab6e67dd6
updated: 1484310140
title: logarithmic
categories:
    - Dictionary
---
logarithmic
     adj : of or relating to or using logarithms; "logarithmic
           function"
