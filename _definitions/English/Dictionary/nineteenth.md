---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nineteenth
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484377321
title: nineteenth
categories:
    - Dictionary
---
nineteenth
     adj : coming next after the eighteenth in position [syn: {19th}]
     n : position 19 in a countable series of things
