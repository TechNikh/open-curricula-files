---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Glossary_of_algebraic_geometry
offline_file: ""
offline_thumbnail: ""
uuid: fed14479-f42d-4710-98a7-f8aec8bea133
updated: 1484309153
title: Glossary of algebraic geometry
categories:
    - Algebraic geometry
---
See also glossary of commutative algebra, glossary of classical algebraic geometry, and glossary of ring theory. For the number-theoretic applications, see glossary of arithmetic and Diophantine geometry.
