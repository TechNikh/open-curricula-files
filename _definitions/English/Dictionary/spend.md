---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spend
offline_file: ""
offline_thumbnail: ""
uuid: f27a06dc-5d77-4d0f-987e-84525d49ffba
updated: 1484310260
title: spend
categories:
    - Dictionary
---
spend
     v 1: pass (time) in a specific way; "How are you spending your
          summer vacation?" [syn: {pass}]
     2: pay out; "spend money" [syn: {expend}, {drop}]
     3: spend completely; "I spend my pocket money in two days"
     [also: {spent}]
