---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/champion
offline_file: ""
offline_thumbnail: ""
uuid: 5da0afd1-977d-459b-9e03-7066784c8f6e
updated: 1484310181
title: champion
categories:
    - Dictionary
---
champion
     adj : holding first place in a contest; "a champion show dog"; "a
           prizewinning wine" [syn: {prizewinning}]
     n 1: someone who has won first place in a competition [syn: {champ},
           {title-holder}]
     2: someone who fights for a cause [syn: {fighter}, {hero}, {paladin}]
     3: a person who backs a politician or a team etc.; "all their
        supporters came out for the game"; "they are friends of
        the library" [syn: {supporter}, {protagonist}, {admirer},
        {booster}, {friend}]
     4: someone who is dazzlingly skilled in any field [syn: {ace},
        {adept}, {sensation}, {maven}, {mavin}, {virtuoso}, {genius},
         {hotshot}, {star}, ...
