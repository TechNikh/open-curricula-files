---
version: 1
type: video
provider: YouTube
id: 2l2FgUrln1A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Parallel%20structure.webm"
offline_file: ""
offline_thumbnail: ""
uuid: dff65a20-3f55-4ee2-b255-7d5f39f4f27b
updated: 1486069595
title: Parallel structure
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/2l2FgUrln1A/default.jpg
    - https://i3.ytimg.com/vi/2l2FgUrln1A/1.jpg
    - https://i3.ytimg.com/vi/2l2FgUrln1A/2.jpg
    - https://i3.ytimg.com/vi/2l2FgUrln1A/3.jpg
---
Parallel structure isn’t a set rule, but more of a stylistic choice: it helps sentence elements maintain a pattern. 

This is a very special episode because it’s Paige’s last video with us as a KA intern! We will miss having Paige as a contributor to the Grammar Team.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/syntax/e/parallel-structure 


Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/syntax/v/relative-clauses-syntax-khan-academy 

Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
