---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/implemented
offline_file: ""
offline_thumbnail: ""
uuid: 27a45e53-50d3-4311-a4b2-fd623a211330
updated: 1484310258
title: implemented
categories:
    - Dictionary
---
implemented
     adj : forced or compelled or put in force; "a life of enforced
           inactivity"; "enforced obedience" [syn: {enforced}]
           [ant: {unenforced}]
