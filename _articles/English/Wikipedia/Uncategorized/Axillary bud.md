---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lateral_buds
offline_file: ""
offline_thumbnail: ""
uuid: fc1c63d5-e5da-418b-95c3-3930bc92932a
updated: 1484308368
title: Axillary bud
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Plant_morphology_buds_axillary.png
tags:
    - Overview
    - Effects of Auxin
    - Plant Diseases That Affect Axillary Buds
categories:
    - Uncategorized
---
The axillary bud (or lateral bud) is an embryonic shoot located in the axil of a leaf. Each bud has the potential to form shoots, and may be specialized in producing either vegetative shoots (stems and branches) or reproductive shoots (flowers). Once formed, a bud may remain dormant for some time, or it may form a shoot immediately.[1]
