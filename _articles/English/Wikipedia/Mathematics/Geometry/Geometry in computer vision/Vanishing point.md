---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vanishing_point
offline_file: ""
offline_thumbnail: ""
uuid: 2af747cf-718f-4890-892e-94935250bf02
updated: 1484309184
title: Vanishing point
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/182px-Railroad_in_Northumberland_County%252C_Pennsylvania.JPG'
tags:
    - Vector notation
    - Theorem
    - Vanishing line
    - Properties of vanishing points
    - Curvilinear and reverse perspective
    - Detection of vanishing points
    - Applications of vanishing points
categories:
    - Geometry in computer vision
---
In graphical perspective, a vanishing point is a point in the picture plane that is the intersection of the projections (or drawings) of a set of parallel lines in space on to the picture plane. When the set of parallels is perpendicular to the picture plane, the construction is known as one-point perspective and their vanishing point corresponds to the oculus or eye point from which the image should be viewed for correct perspective geometry.[1] Traditional linear drawings use objects with one to three sets of parallels, defining one to three vanishing points.
