---
version: 1
type: video
provider: YouTube
id: E3HnW5NdMp8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 76eaa5ff-fc97-4ddf-8177-2e33c5566df6
updated: 1486069644
title: 'Maths Gallery- Design Animation'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/E3HnW5NdMp8/default.jpg
    - https://i3.ytimg.com/vi/E3HnW5NdMp8/1.jpg
    - https://i3.ytimg.com/vi/E3HnW5NdMp8/2.jpg
    - https://i3.ytimg.com/vi/E3HnW5NdMp8/3.jpg
---
