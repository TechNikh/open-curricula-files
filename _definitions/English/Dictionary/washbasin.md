---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/washbasin
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484358361
title: washbasin
categories:
    - Dictionary
---
washbasin
     n 1: a bathroom or lavatory sink that is permanently installed
          and connected to a water supply and drainpipe; where you
          wash your hands and face; "he ran some water in the
          basin and splashed it on his face" [syn: {basin}, {washbowl},
           {washstand}, {lavatory}]
     2: a basin for washing the hands (`wash-hand basin' is a
        British expression) [syn: {handbasin}, {washbowl}, {lavabo},
         {wash-hand basin}]
