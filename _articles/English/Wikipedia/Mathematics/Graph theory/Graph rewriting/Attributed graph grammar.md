---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Attributed_graph_grammar
offline_file: ""
offline_thumbnail: ""
uuid: 3bcbf587-b75b-4ba6-9dbb-37626b044a98
updated: 1484309452
title: Attributed graph grammar
categories:
    - Graph rewriting
---
In computer science, an attributed graph grammar is a class of graph grammar that associates vertices with a set of attributes and rewrites with functions on attributes. In the algebraic approach to graph grammars, they are usually formulated using the double-pushout approach or the single-pushout approach.
