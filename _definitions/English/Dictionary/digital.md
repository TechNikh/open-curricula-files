---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/digital
offline_file: ""
offline_thumbnail: ""
uuid: 77c68f13-d0f8-4cde-a750-46819ea2f941
updated: 1484310202
title: digital
categories:
    - Dictionary
---
digital
     adj 1: of a circuit or device that represents magnitudes in digits;
            "digital computer" [ant: {analogue}]
     2: displaying numbers rather than scale positions; "digital
        clock"; "digital readout"
     3: relating to or performed with the fingers; "digital
        examination"
