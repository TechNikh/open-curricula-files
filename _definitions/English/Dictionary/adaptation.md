---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adaptation
offline_file: ""
offline_thumbnail: ""
uuid: aa6721aa-1e9f-49a7-92ff-2b92220f909e
updated: 1484310293
title: adaptation
categories:
    - Dictionary
---
adaptation
     n 1: a written work (as a novel) that has been recast in a new
          form; "the play is an adaptation of a short novel" [syn:
           {version}]
     2: the process of adapting to something (such as environmental
        conditions) [syn: {adjustment}]
     3: (physiology) the responsive adjustment of a sense organ (as
        the eye) to varying conditions (as of light)
