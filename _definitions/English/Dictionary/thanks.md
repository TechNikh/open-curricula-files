---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thanks
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484512981
title: thanks
categories:
    - Dictionary
---
thanks
     n 1: an acknowledgment of appreciation
     2: with the help of or owing to; "thanks to hard work it was a
        great success"
