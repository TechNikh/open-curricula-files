---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maxima_of_a_point_set
offline_file: ""
offline_thumbnail: ""
uuid: 2582626b-c6c1-46dc-ab1a-8e239b2ddccb
updated: 1484309176
title: Maxima of a point set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Maxima_of_a_point_set.svg.png
tags:
    - Two dimensions
    - Three dimensions
    - Higher dimensions
categories:
    - Computational geometry
---
In computational geometry, a point p in a finite set of points S is said to be maximal or non-dominated if there is no other point q in S whose coordinates are all greater than or equal to the corresponding coordinates of p. The maxima of a point set S are all the maximal points of S. The problem of finding all maximal points, sometimes called the problem of the maxima or maxima set problem, has been studied as a variant of the convex hull and orthogonal convex hull problems. It is equivalent to finding the Pareto frontier of a collection of points, and was called the floating-currency problem by Herbert Freeman based on an application involving comparing the relative wealth of individuals ...
