---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hausdorff_distance
offline_file: ""
offline_thumbnail: ""
uuid: d286ef89-b9b5-4797-8e9a-415dbed6aa0a
updated: 1484309266
title: Hausdorff distance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Hausdorff_distance_sample.svg.png
tags:
    - Definition
    - Remark
    - Properties
    - Motivation
    - Applications
    - Related concepts
categories:
    - Metric geometry
---
In mathematics, the Hausdorff distance, or Hausdorff metric, also called Pompeiu–Hausdorff distance,[1] measures how far two subsets of a metric space are from each other. It turns the set of non-empty compact subsets of a metric space into a metric space in its own right. It is named after Felix Hausdorff.
