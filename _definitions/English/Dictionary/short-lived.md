---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/short-lived
offline_file: ""
offline_thumbnail: ""
uuid: 56e75996-330e-4242-a580-38cd6e080707
updated: 1484310589
title: short-lived
categories:
    - Dictionary
---
short-lived
     adj : enduring a very short time; "the ephemeral joys of
           childhood"; "a passing fancy"; "youth's transient
           beauty"; "love is transitory but at is eternal";
           "fugacious blossoms" [syn: {ephemeral}, {passing}, {transient},
            {transitory}, {fugacious}]
