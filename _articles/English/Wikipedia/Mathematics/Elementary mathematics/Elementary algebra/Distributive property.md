---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distributive_property
offline_file: ""
offline_thumbnail: ""
uuid: 4463c520-17ba-4d1e-820b-8ca6f3306301
updated: 1484308909
title: Distributive property
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Illustration_of_distributive_property_with_rectangles.svg.png
tags:
    - Definition
    - Meaning
    - Examples
    - Real numbers
    - Matrices
    - Other examples
    - Propositional logic
    - Rule of replacement
    - Truth functional connectives
    - Distributivity and rounding
    - Distributivity in rings
    - Generalizations of distributivity
    - Notions of antidistributivity
    - Notes
categories:
    - Elementary algebra
---
In abstract algebra and formal logic, the distributive property of binary operations generalizes the distributive law from elementary algebra. In propositional logic, distribution refers to two valid rules of replacement. The rules allow one to reformulate conjunctions and disjunctions within logical proofs.
