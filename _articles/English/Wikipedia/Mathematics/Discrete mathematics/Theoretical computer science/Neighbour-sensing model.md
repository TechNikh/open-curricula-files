---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Neighbour-sensing_model
offline_file: ""
offline_thumbnail: ""
uuid: f4567c08-ef1e-4388-a38e-5895f1e1bf6f
updated: 1484308675
title: Neighbour-sensing model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Neighbour_Sensing_model_cone.jpg
categories:
    - Theoretical computer science
---
The neighbour-sensing model is the proposed hypothesis of the fungal morphogenesis. The hypothesis suggests that each hypha in the fungal mycelium generates a certain abstract field that (like the known physical fields) decreases when increasing the distance. The proposed mathematical models deal with both scalar and vector fields. The field and its gradient are sensed by the hyphal tips that choose the growth direction following some supposed algorithm. The model was suggested by Audrius Meškauskas and David Moore in 2004 and supported using the supercomputing facilities of University of Manchester.
