---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/majesty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484480461
title: majesty
categories:
    - Dictionary
---
majesty
     n : impressiveness in scale or proportion [syn: {stateliness}, {loftiness}]
