---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Desargues%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: cc24a090-373a-4cbd-8948-c16e2e361396
updated: 1484309111
title: "Desargues's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Desargues_theorem_alt.svg.png
tags:
    - History
    - Projective versus affine spaces
    - Self-duality
    - "Proof of Desargues's theorem"
    - Three-dimensional proof
    - Two-dimensional proof
    - "Relation to Pappus's theorem"
    - The Desargues configuration
    - Notes
categories:
    - Theorems in geometry
---
Denote the three vertices of one triangle by a, b and c, and those of the other by A, B and C. Axial perspectivity means that lines ab and AB meet in a point, lines ac and AC meet in a second point, and lines bc and BC meet in a third point, and that these three points all lie on a common line called the axis of perspectivity. Central perspectivity means that the three lines Aa, Bb and Cc are concurrent, at a point called the center of perspectivity.
