---
version: 1
type: article
id: https://en.wikipedia.org/wiki/T-shaped_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 34ab96db-f569-4317-ba4e-d75ede93e786
updated: 1484309282
title: T-shaped molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Chlorine-trifluoride-3D-vdW.png
categories:
    - Molecular geometry
---
In chemistry, T-shaped molecular geometry describes the structures of some molecules where a central atom has three ligands. Ordinarily, three-coordinated compounds adopt trigonal planar or pyramidal geometries. Examples of T-shaped molecules are the halogen trifluorides, such as ClF3.[1]
