---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mobilisation
offline_file: ""
offline_thumbnail: ""
uuid: f5aa3e43-d6ad-401e-9eb9-8f3dcfe0dc91
updated: 1484310525
title: mobilisation
categories:
    - Dictionary
---
mobilisation
     n 1: act of marshaling and organizing and making ready for use or
          action; "mobilization of the country's economic
          resources" [syn: {mobilization}]
     2: act of assembling and putting into readiness for war or
        other emergency: "mobilization of the troops" [syn: {mobilization},
         {militarization}, {militarisation}] [ant: {demobilization}]
