---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Factorial_number_system
offline_file: ""
offline_thumbnail: ""
uuid: dd7ff981-abac-4839-acc8-47b9a9960c20
updated: 1484308582
title: Factorial number system
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Symmetric_group_4%253B_permutohedron_3D%253B_Lehmer_codes.svg.png'
tags:
    - Definition
    - Examples
    - Permutations
    - Fractional values
categories:
    - Combinatorics
---
In combinatorics, the factorial number system, also called factoradic, is a mixed radix numeral system adapted to numbering permutations. It is also called factorial base, although factorials do not function as base, but as place value of digits. By converting a number less than n! to factorial representation, one obtains a sequence of n digits that can be converted to a permutation of n in a straightforward way, either using them as Lehmer code or as inversion table[1] representation; in the former case the resulting map from integers to permutations of n lists them in lexicographical order. General mixed radix systems were studied by Georg Cantor.[2] The term "factorial number system" is ...
