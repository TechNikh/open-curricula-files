---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beef
offline_file: ""
offline_thumbnail: ""
uuid: 4ad7e509-4d43-4216-b789-255c72e5a4e0
updated: 1484310311
title: beef
categories:
    - Dictionary
---
beef
     n 1: cattle that are reared for their meat [syn: {beef cattle}]
     2: meat from an adult domestic bovine [syn: {boeuf}]
     3: informal terms for objecting; "I have a gripe about the
        service here" [syn: {gripe}, {kick}, {bitch}, {squawk}]
     v : complain; "What was he hollering about?" [syn: {gripe}, {grouse},
          {crab}, {squawk}, {bellyache}, {holler}]
     [also: {beeves} (pl)]
