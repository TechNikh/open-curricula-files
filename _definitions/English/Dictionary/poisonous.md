---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poisonous
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484616541
title: poisonous
categories:
    - Dictionary
---
poisonous
     adj 1: having the qualities or effects of a poison [syn: {toxicant}]
     2: not safe to eat
     3: marked by deep ill will; deliberately harmful; "a malevolent
        lie"; "poisonous hate...in his eyes"- Ernest Hemingway;
        "venomous criticism"; "vicious gossip" [syn: {venomous}, {vicious}]
