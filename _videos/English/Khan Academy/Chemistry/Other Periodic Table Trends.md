---
version: 1
type: video
provider: YouTube
id: XMLd-O6PgVs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Other%20Periodic%20Table%20Trends.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2359669c-28c4-4829-8bd7-7e618a20c251
updated: 1486069589
title: Other Periodic Table Trends
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/XMLd-O6PgVs/default.jpg
    - https://i3.ytimg.com/vi/XMLd-O6PgVs/1.jpg
    - https://i3.ytimg.com/vi/XMLd-O6PgVs/2.jpg
    - https://i3.ytimg.com/vi/XMLd-O6PgVs/3.jpg
---
Electronegativity, metallic nature and atomic radius.
