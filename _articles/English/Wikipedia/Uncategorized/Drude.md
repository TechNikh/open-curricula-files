---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Drude
offline_file: ""
offline_thumbnail: ""
uuid: cab97348-e5d9-42df-a4f4-145c6b813088
updated: 1484308321
title: Drude
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Wappen_Weiler-Rems.png
categories:
    - Uncategorized
---
In German folklore, a drude (German: Drude, pl. Druden) is a kind of malevolent nocturnal spirit (an elf (Alp) or kobold or a hag) associated with nightmares, prevalent especially in Southern Germany. Druden were said to participate in the Wild Hunt and were considered a particular class of demon in Alfonso de Spina's hierarchy. The word also came to be used as a generic term for "witch" in the 16th century (Hans Sachs).
