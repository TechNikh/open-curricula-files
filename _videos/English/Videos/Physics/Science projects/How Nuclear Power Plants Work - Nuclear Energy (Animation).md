---
version: 1
type: video
provider: YouTube
id: _UwexvaCMWA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 69f751e5-db1f-4d77-a1ea-1631c3f9f55b
updated: 1486069634
title: How Nuclear Power Plants Work / Nuclear Energy (Animation)
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/_UwexvaCMWA/default.jpg
    - https://i3.ytimg.com/vi/_UwexvaCMWA/1.jpg
    - https://i3.ytimg.com/vi/_UwexvaCMWA/2.jpg
    - https://i3.ytimg.com/vi/_UwexvaCMWA/3.jpg
---
