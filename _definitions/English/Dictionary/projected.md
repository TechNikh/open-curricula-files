---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/projected
offline_file: ""
offline_thumbnail: ""
uuid: 49d00580-4050-4279-95f0-1cc8923980a1
updated: 1484310475
title: projected
categories:
    - Dictionary
---
projected
     adj 1: planned for the future; "the first volume of a proposed
            series" [syn: {proposed}]
     2: extending out above or beyond a surface or boundary; "the
        jutting limb of a tree"; "massive projected buttresses";
        "his protruding ribs"; "a pile of boards sticking over the
        end of his truck" [syn: {jutting}, {projecting}, {protruding},
         {sticking(p)}, {sticking out(p)}]
