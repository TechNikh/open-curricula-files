---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grand
offline_file: ""
offline_thumbnail: ""
uuid: 72074e2c-f4c2-403b-b306-95a2c1845eff
updated: 1484310309
title: grand
categories:
    - Dictionary
---
grand
     adj 1: impressive in scale; "an expansive lifestyle"; "in the grand
            manner" [syn: {expansive}]
     2: of or befitting a lord; "heir to a lordly fortune"; "of
        august lineage" [syn: {august}, {lordly}]
     3: impressive in size or scope; "heroic undertakings" [syn: {heroic}]
     n 1: the cardinal number that is the product of 10 and 100 [syn:
          {thousand}, {one thousand}, {1000}, {M}, {K}, {chiliad},
           {G}, {thou}, {yard}]
     2: a piano with the strings on a harp-shaped frame; usually
        supported by 3 legs [syn: {grand piano}]
