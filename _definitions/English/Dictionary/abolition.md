---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abolition
offline_file: ""
offline_thumbnail: ""
uuid: 7af633c1-eb04-4691-82f0-e599580b2c5d
updated: 1484310573
title: abolition
categories:
    - Dictionary
---
abolition
     n : the act of abolishing a system or practice or institution
         (especially abolishing slavery); "the abolition of
         capital punishment" [syn: {abolishment}]
