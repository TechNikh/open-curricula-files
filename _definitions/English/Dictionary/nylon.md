---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nylon
offline_file: ""
offline_thumbnail: ""
uuid: 1d65916b-08f9-4039-8be0-446f1047fbb1
updated: 1484310240
title: nylon
categories:
    - Dictionary
---
nylon
     n 1: a thermoplastic polyamide; a family of high-strength
          resilient synthetic materials
     2: a synthetic fabric
