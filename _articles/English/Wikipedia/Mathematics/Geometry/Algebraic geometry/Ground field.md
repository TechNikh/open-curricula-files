---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ground_field
offline_file: ""
offline_thumbnail: ""
uuid: 1c1a65c8-b781-418d-bf69-32062a936052
updated: 1484309127
title: Ground field
tags:
    - use
    - In linear algebra
    - In algebraic geometry
    - In Lie theory
    - In Galois theory
    - In Diophantine geometry
    - Notes
categories:
    - Algebraic geometry
---
