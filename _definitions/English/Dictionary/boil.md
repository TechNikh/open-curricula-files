---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boil
offline_file: ""
offline_thumbnail: ""
uuid: bbfe1d01-d3dc-46db-9449-95bd1dbb258c
updated: 1484310224
title: boil
categories:
    - Dictionary
---
boil
     n 1: a painful sore with a hard pus-filled core [syn: {furuncle}]
     2: the temperature at which a liquid boils at sea level; "the
        brought to water to a boil" [syn: {boiling point}]
     v 1: come to the boiling point and change from a liquid to vapor;
          "Water boils at 100 degrees Celsius" [ant: {freeze}]
     2: cook in boiling liquid; "boil potatoes"
     3: bring to, or maintain at, the boiling point; "boil this
        liquid until it evaporates"
     4: be agitated; "the sea was churning in the storm" [syn: {churn},
         {moil}, {roil}]
     5: be in an agitated emotional state; "The customer was
        seething with anger" [syn: {seethe}]
