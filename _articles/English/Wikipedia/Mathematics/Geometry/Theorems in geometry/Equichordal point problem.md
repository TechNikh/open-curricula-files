---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equichordal_point_problem
offline_file: ""
offline_thumbnail: ""
uuid: a55ba279-0810-4a65-b4a3-6c612657a72d
updated: 1484309116
title: Equichordal point problem
tags:
    - Problem statement
    - Excentricity (or eccentricity)
    - The history of the problem
    - "Rychlik's proof"
categories:
    - Theorems in geometry
---
In Euclidean plane geometry, the equichordal point problem is the question whether a closed planar convex body can have two equichordal points.[1] The problem was originally posed in 1916 by Fujiwara and in 1917 by Wilhelm Blaschke, Hermann Rothe, and Roland Weitzenböck.[2] A generalization of this problem statement was answered in the negative in 1997 by Marek R. Rychlik.[3]
