---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gingelly
offline_file: ""
offline_thumbnail: ""
uuid: 82f63fdb-d796-4792-b763-8ba46ec822b8
updated: 1484308335
title: Sesame
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sesamum_indicum_NP.jpg
tags:
    - Etymology and languages
    - Origins
    - Botany
    - Cultivation
    - Processing
    - Production and trade
    - Trade
    - Nutritional information
    - Chemical composition
    - Cuisine
    - Allergy
    - Literature
    - Gallery
categories:
    - Uncategorized
---
Sesame (/ˈsɛsəmiː/; Sesamum indicum) is a flowering plant in the genus Sesamum, also called benne.[2] Numerous wild relatives occur in Africa and a smaller number in India. It is widely naturalized in tropical regions around the world and is cultivated for its edible seeds, which grow in pods or "buns". The world harvested 4.2 million metric tonnes of sesame seeds in 2013, with India and China as the largest producers.[3]
