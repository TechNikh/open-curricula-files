---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chisso
offline_file: ""
offline_thumbnail: ""
uuid: 21694cdc-dbe4-49c3-8215-7022f854562b
updated: 1484308335
title: Chisso
tags:
    - History
    - Foundation
    - Expansion
    - Nichitsu in Korea
    - Dissolution and reorganization
    - Minamata disease
categories:
    - Uncategorized
---
The Chisso Corporation (チッソ株式会社, Chisso kabushiki kaisha?), since 2012 reorganized as JNC (Japan New Chisso),[1] is a Japanese chemical company. It is an important supplier of liquid crystal used for LCDs, but is best known for its role in the 34-year-long contamination of the water supply in Minamata, Japan that led to thousands of deaths and victims of disease.
