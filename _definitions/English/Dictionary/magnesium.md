---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnesium
offline_file: ""
offline_thumbnail: ""
uuid: be176357-f01c-4cd8-b377-4fd3bc72a63b
updated: 1484310373
title: magnesium
categories:
    - Dictionary
---
magnesium
     n : a light silver-white ductile bivalent metallic element; in
         pure form it burns with brilliant white flame; occurs
         naturally only in combination (as in magnesite and
         dolomite and carnallite and spinel and olivine) [syn: {Mg},
          {atomic number 12}]
