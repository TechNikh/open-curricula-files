---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Succinct_game
offline_file: ""
offline_thumbnail: ""
uuid: 59251288-2ee5-4c4b-ba42-2e0252249a0d
updated: 1484309038
title: Succinct game
tags:
    - Types of succinct games
    - Graphical games
    - Sparse games
    - Symmetric games
    - Anonymous games
    - Polymatrix games
    - Circuit games
    - Other representations
    - Summary of complexities of finding equilibria
    - Notes
categories:
    - Game theory game classes
---
In algorithmic game theory, a succinct game or a succinctly representable game is a game which may be represented in a size much smaller than its normal form representation. Without placing constraints on player utilities, describing a game of 
  
    
      
        n
      
    
    {\displaystyle n}
  
 players, each facing 
  
    
      
        s
      
    
    {\displaystyle s}
  
 strategies, requires listing 
  
    
      
        n
        
          s
          
            n
          
        
      
    
    {\displaystyle ns^{n}}
  
 utility values. Even trivial algorithms are capable of finding a Nash equilibrium in a time polynomial in the length of such a large input. A ...
