---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wasteful
offline_file: ""
offline_thumbnail: ""
uuid: 641bd2aa-294b-4ef3-bd08-a64b277f5f76
updated: 1484310242
title: wasteful
categories:
    - Dictionary
---
wasteful
     adj 1: inefficient in use of time and effort and materials; "a
            clumsy and wasteful process"; "wasteful duplication of
            effort"; "uneconomical ebb and flow of power" [syn: {uneconomical}]
     2: tending to squander and waste [ant: {thrifty}]
     3: laying waste; "when wasteful war shall statues overturn"-
        Shakespeare
