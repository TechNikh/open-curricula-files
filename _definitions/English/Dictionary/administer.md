---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/administer
offline_file: ""
offline_thumbnail: ""
uuid: d4cee8cb-fb44-46b1-b6c4-8612b3898477
updated: 1484310192
title: administer
categories:
    - Dictionary
---
administer
     v 1: work in an administrative capacity; supervise; "administer a
          program" [syn: {administrate}]
     2: administer ritually; of church sacraments
     3: administer or bestow, as in small portions; "administer
        critical remarks to everyone present"; "dole out some
        money"; "shell out pocket money for the children"; "deal a
        blow to someone" [syn: {distribute}, {mete out}, {deal}, {parcel
        out}, {lot}, {dispense}, {shell out}, {deal out}, {dish
        out}, {allot}, {dole out}]
     4: give or apply (medications) [syn: {dispense}]
