---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hausdorff_gap
offline_file: ""
offline_thumbnail: ""
uuid: a87040cf-4492-4591-a925-bc368916dc35
updated: 1484309507
title: Hausdorff gap
categories:
    - Set theory
---
In mathematics, a Hausdorff gap consists roughly of two collections of sequences of integers, such that there is no sequence lying between the two collections. The first example was found by Hausdorff (1909). The existence of Hausdorff gaps shows that the partially ordered set of possible growth rates of sequences is not complete.
