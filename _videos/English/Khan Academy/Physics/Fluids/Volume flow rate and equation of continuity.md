---
version: 1
type: video
provider: YouTube
id: G4CgOF4ccXk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Fluids/Volume%20flow%20rate%20and%20equation%20of%20continuity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: be3c1df5-4106-48f9-bb6b-887a65bd42af
updated: 1486069593
title: Volume flow rate and equation of continuity
categories:
    - Fluids
thumbnail_urls:
    - https://i3.ytimg.com/vi/G4CgOF4ccXk/default.jpg
    - https://i3.ytimg.com/vi/G4CgOF4ccXk/1.jpg
    - https://i3.ytimg.com/vi/G4CgOF4ccXk/2.jpg
    - https://i3.ytimg.com/vi/G4CgOF4ccXk/3.jpg
---
Sal introduces the notion of moving fluids and laminar flow. Then he uses the incompressibility of a liquid to show that the volume flow rate (flux) must remain constant. Sal then derives the equation of continuity in terms of the area and speed. . Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/fluids/fluid-dynamics/v/fluids-part-8?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/fluids/buoyant-force-and-archimedes-principle/v/fluids-part-6?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
