---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hesse%27s_principle_of_transfer'
offline_file: ""
offline_thumbnail: ""
uuid: 0b0dad8a-3636-4fda-8ba7-e57ec95f4d49
updated: 1484309311
title: "Hesse's principle of transfer"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Hesse.svg_1.png
tags:
    - Original reference
categories:
    - Symmetry
---
In geometry, Hesse's principle of transfer (German: Uebertragungsprinzip) states that if the points of the projective line P1 are depicted by a rational normal curve in Pn, then the group of the projective transformations of Pn that preserve the curve is isomorphic to the group of the projective transformations of P1 (this is a generalization of the original Hesse's principle, in a form suggested by Wilhelm Franz Meyer).[1][2] It was originally introduced by Otto Hesse in 1866, in a more restricted form. It influenced Felix Klein in the development of the Erlangen program.[3][4][5] Since its original conception, it was generalized by many mathematicians, including Klein, Fano, and Cartan.[6]
