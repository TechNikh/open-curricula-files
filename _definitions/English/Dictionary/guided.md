---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guided
offline_file: ""
offline_thumbnail: ""
uuid: 1fc409f2-70fe-4356-893c-f48a86000b98
updated: 1484310391
title: guided
categories:
    - Dictionary
---
guided
     adj : subject to guidance or control especially after launching;
           "a guided missile" [ant: {unguided}]
