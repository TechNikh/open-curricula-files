---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/covalent
offline_file: ""
offline_thumbnail: ""
uuid: b7c7e077-2aa9-49af-9b67-e0e34d8d9399
updated: 1484310401
title: covalent
categories:
    - Dictionary
---
covalent
     adj : of or relating to or characterized by covalence; "covalent
           bond"
