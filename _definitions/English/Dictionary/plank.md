---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plank
offline_file: ""
offline_thumbnail: ""
uuid: 61d326dc-c06c-4991-9de3-7ed60a2dad56
updated: 1484310212
title: plank
categories:
    - Dictionary
---
plank
     n 1: a stout length of sawn timber; made in a wide variety of
          sizes and used for many purposes [syn: {board}]
     2: an endorsed policy in the platform of a political party
     v 1: cover with planks; "The streets were planked" [syn: {plank
          over}]
     2: set (something or oneself) down with or as if with a noise;
        "He planked the money on the table"; "He planked himself
        into the sofa" [syn: {flump}, {plonk}, {plop}, {plunk}, {plump
        down}, {plunk down}, {plump}]
     3: cook and serve on a plank; "Planked vegetable"; "Planked
        shad"
