---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/own
offline_file: ""
offline_thumbnail: ""
uuid: 3866fe6a-67eb-448b-baa8-9aba09e27175
updated: 1484310325
title: own
categories:
    - Dictionary
---
own
     adj : belonging to or on behalf of a specified person (especially
           yourself); preceded by a possessive; "for your own
           use"; "do your own thing"; "she makes her own clothes";
           "`ain' is Scottish" [syn: {own(a)}, {ain}]
     v : have ownership or possession of; "He owns three houses in
         Florida"; "How many cars does she have?" [syn: {have}, {possess}]
