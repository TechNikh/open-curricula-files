---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/socially
offline_file: ""
offline_thumbnail: ""
uuid: 5e7fbd28-cf58-4e2c-ba24-6d50b1c75fde
updated: 1484310256
title: socially
categories:
    - Dictionary
---
socially
     adv 1: by or with respect to society; "socially accepted norms"
     2: in a social manner; "socially unpopular"
