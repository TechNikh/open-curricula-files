---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shmuel_Gal
offline_file: ""
offline_thumbnail: ""
uuid: 6fc6b833-6139-4d9d-b31b-53fc43d84986
updated: 1484308513
title: Shmuel Gal
categories:
    - Game theorists
---
He devised the Gal's accurate tables method for the computer evaluation of elementary functions.[1][2] With Zvi Yehudai he developed in 1993 a new algorithm for sorting which is used by IBM.[3]
