---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pretty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484411701
title: pretty
categories:
    - Dictionary
---
pretty
     adj 1: pleasing by delicacy or grace; not imposing; "pretty girl";
            "pretty song"; "pretty room"
     2: (used ironically) unexpectedly bad; "a pretty mess"; "a
        pretty kettle of fish"
     adv : used as an intensifier (`jolly' is used informally in
           Britain); "pretty big"; "pretty bad"; "jolly decent of
           him" [syn: {jolly}]
     [also: {prettied}, {prettiest}, {prettier}]
