---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cobbler
offline_file: ""
offline_thumbnail: ""
uuid: 57ec0e90-9fcd-46a1-9ee5-606f17875823
updated: 1484310457
title: cobbler
categories:
    - Dictionary
---
cobbler
     n 1: a person who makes or repairs shoes [syn: {shoemaker}]
     2: tall sweetened iced drink of wine or liquor with fruit
     3: made of fruit with rich biscuit dough usually only on top of
        the fruit [syn: {deep-dish pie}]
