---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/income
offline_file: ""
offline_thumbnail: ""
uuid: c6b7d37c-ad95-46a6-ace2-e95e5041f9ae
updated: 1484310262
title: income
categories:
    - Dictionary
---
income
     n : the financial gain (earned or unearned) accruing over a
         given period of time [ant: {outgo}]
