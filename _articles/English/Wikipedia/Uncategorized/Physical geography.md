---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Physiographic
offline_file: ""
offline_thumbnail: ""
uuid: 031b137b-a960-40f8-afe4-3a0877c145f5
updated: 1484308428
title: Physical geography
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Land_ocean_ice_cloud_1024.jpg
tags:
    - Sub-branches
    - Journals and literature
    - Historical evolution of the discipline
    - Notable physical geographers
categories:
    - Uncategorized
---
Physical geography (also known as geosystems or physiography) is one of the two major sub-fields of geography.[1][2][3] Physical geography is that branch of natural science which deals with the study of processes and patterns in the natural environment like the atmosphere, hydrosphere, biosphere, and geosphere, as opposed to the cultural or built environment, the domain of human geography.
