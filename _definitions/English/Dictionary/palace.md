---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/palace
offline_file: ""
offline_thumbnail: ""
uuid: 92a4239f-abc4-4b4a-807c-a76673510a3a
updated: 1484310246
title: palace
categories:
    - Dictionary
---
palace
     n 1: a large and stately mansion [syn: {castle}]
     2: the governing group of a kingdom; "the palace issued an
        order binding on all subjects"
     3: a large ornate exhibition hall
     4: official residence of an exalted person (as a sovereign)
