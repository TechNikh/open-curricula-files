---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transient_(oscillation)
offline_file: ""
offline_thumbnail: ""
uuid: f6677b9a-5554-441c-9069-b6f0697bbd17
updated: 1484308813
title: Transient (oscillation)
tags:
    - Electrical engineering
    - Electromagnetics
    - Acoustics
categories:
    - Oscillation
---
The source of the transient energy may be an internal event or a nearby event. The energy then couples to other parts of the system, typically appearing as a short burst of oscillation.
