---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/steepness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484339641
title: steepness
categories:
    - Dictionary
---
steepness
     n : the property possessed by a slope that is very steep [syn: {abruptness},
          {precipitousness}] [ant: {gradualness}]
