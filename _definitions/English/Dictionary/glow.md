---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glow
offline_file: ""
offline_thumbnail: ""
uuid: 02fcb6c8-edee-4e54-8cf6-0de586ef6f8d
updated: 1484310206
title: glow
categories:
    - Dictionary
---
glow
     n 1: an alert and refreshed state [syn: {freshness}]
     2: light from nonthermal sources [syn: {luminescence}]
     3: the phenomenon of light emission by a body as its
        temperature is raised [syn: {incandescence}]
     4: a feeling of considerable warmth; "the glow of new love"; "a
        glow of regret"
     5: a steady even light without flames
     6: the amount of electromagnetic radiation leaving or arriving
        at a point on a surface [syn: {radiance}, {glowing}]
     7: an appearance of reflected light [syn: {gleam}, {gleaming},
        {lambency}]
     v 1: emit a steady even light without flames; "The fireflies were
          glowing and flying about in the ...
