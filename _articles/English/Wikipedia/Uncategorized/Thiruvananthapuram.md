---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thiruvananthapuram
offline_file: ""
offline_thumbnail: ""
uuid: c762c6de-34ed-4494-9d32-16cb20a7c77c
updated: 1484308456
title: Thiruvananthapuram
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Aruvikkara_Dam.jpg
tags:
    - Etymology
    - History
    - Demographics
    - Economy
    - Administration
    - Geography and Climate
    - Infrastructure
    - Tourism
    - Transport
    - culture
    - Education
    - Media
    - Sports
    - Notable people
    - Sister cities
    - Diplomatic Missions
categories:
    - Uncategorized
---
Thiruvananthapuram (Tiruvaṉantapuram, IPA: [t̪iruʋənən̪t̪əpurəm] ( listen)), formerly known as Trivandrum, is the capital and the largest city of the Indian state of Kerala. It is located on the west coast of India near the extreme south of the mainland. Referred to by Mahatma Gandhi as the "Evergreen city of India",[3][4] it is classified as a Tier-II city by the Government of India.
