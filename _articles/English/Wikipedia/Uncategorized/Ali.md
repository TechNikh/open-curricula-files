---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ali
offline_file: ""
offline_thumbnail: ""
uuid: 65747f1d-03fd-4cb7-a00c-89284a8c2aab
updated: 1484308451
title: Ali
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Levha_%2528panel%2529_in_honor_of_Imam_%2527Ali.jpg'
tags:
    - In Mecca
    - Early life
    - Acceptance of Islam
    - After declaration of Islam
    - Migration to Medina
    - In Medina
    - "Muhammad's era"
    - Family life
    - Military career
    - Missions for Islam
    - Incident of Mubahala
    - Ghadir Khumm
    - Succession to Muhammad
    - After Muhammad
    - Ali and the Rashidun caliphs
    - Siege of Uthman
    - Caliphate
    - Election as caliph
    - Reign as caliph
    - First Fitna
    - Policies
    - Death
    - Burial
    - Aftermath
    - Knowledge
    - Works
    - Descendants
    - Views
    - Muslim views
    - Shia
    - Sunni
    - Sufi
    - Titles
    - As a "deity"
    - Alawites
    - Ali-Illahism
    - Druze
    - Ali in the Quran
    - Historiography
    - Footnotes
    - Notes
    - Bibliography
    - Original sources
    - Secondary sources
categories:
    - Uncategorized
---
Ali ibn Abi Talib (/ˈɑːli, ɑːˈliː/;[6] Arabic: علي بن أبي طالب‎, translit. ʿAlī bin Abī Ṭālib‎, Arabic pronunciation: [ʕaliː bɪn ʔabiː t̪ˤaːlɪb]; 13 Rajab, 21 BH – 21 Ramadan, 40 AH; 15 September 601[3] – 29 January 661)[2][3] was the cousin and son-in-law of the Islamic prophet Muhammad, ruling over the Islamic caliphate from 656 to 661.[7]
