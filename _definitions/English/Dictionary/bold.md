---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bold
offline_file: ""
offline_thumbnail: ""
uuid: c6a2bc70-6b03-4d8b-9535-d53d2e8cdaba
updated: 1484310545
title: bold
categories:
    - Dictionary
---
bold
     adj 1: fearless and daring; "bold settlers on some foreign shore";
            "a bold speech"; "a bold adventure" [ant: {timid}]
     2: clear and distinct; "bold handwriting"; "a figure carved in
        bold relief"; "a bold design"
     3: very steep; having a prominent and almost vertical front; "a
        bluff headland"; "where the bold chalk cliffs of England
        rise"; "a sheer descent of rock" [syn: {bluff}, {sheer}]
     n : a typeface with thick heavy lines [syn: {boldface}, {bold
         face}]
