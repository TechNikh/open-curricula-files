---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strongly_regular_graph
offline_file: ""
offline_thumbnail: ""
uuid: 874dc7f4-59fc-4a21-9f20-399d3b4268b1
updated: 1484309376
title: Strongly regular graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Paley13.svg.png
tags:
    - Properties
    - Relationship between Parameters
    - Adjacency Matrix
    - Eigenvalues
    - Examples
    - Moore graphs
    - Notes
categories:
    - Algebraic graph theory
---
In graph theory, a strongly regular graph is defined as follows. Let G = (V,E) be a regular graph with v vertices and degree k. G is said to be strongly regular if there are also integers λ and μ such that:
