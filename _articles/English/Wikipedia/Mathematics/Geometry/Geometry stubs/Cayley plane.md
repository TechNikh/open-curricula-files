---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cayley_plane
offline_file: ""
offline_thumbnail: ""
uuid: 33b7128b-19cd-4f2e-9c74-6f4825b9c912
updated: 1484309356
title: Cayley plane
tags:
    - Properties
    - Notes
categories:
    - Geometry stubs
---
In mathematics, the Cayley plane (or octonionic projective plane) P2(O) is a projective plane over the octonions.[1] It was discovered in 1933 by Ruth Moufang, and is named after Arthur Cayley (for his 1845 paper describing the octonions).
