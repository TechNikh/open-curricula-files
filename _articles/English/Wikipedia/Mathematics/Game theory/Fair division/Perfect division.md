---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perfect_division
offline_file: ""
offline_thumbnail: ""
uuid: 85ecb528-767a-4382-bf43-91f63b3befd1
updated: 1484308518
title: Perfect division
categories:
    - Fair division
---
A perfect division is a kind of fair division in which a resource is divided among n partners with subjective valuations, giving each partner exactly 1/n of the resource according to the valuations of all partners. It is a special case of exact division in which all weights are 1/n. For more details see exact division.
