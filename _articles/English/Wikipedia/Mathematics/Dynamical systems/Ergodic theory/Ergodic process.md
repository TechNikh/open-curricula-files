---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ergodic_process
offline_file: ""
offline_thumbnail: ""
uuid: e1552d39-d2ea-413e-8d90-d35578207ea9
updated: 1484308780
title: Ergodic process
tags:
    - Specific definitions
    - Discrete-time random processes
    - Example of a non-ergodic random process
    - Notes
categories:
    - Ergodic theory
---
In econometrics and signal processing, a stochastic process is said to be ergodic if its statistical properties can be deduced from a single, sufficiently long, random sample of the process. The reasoning is that any collection of random samples from a process must represent the average statistical properties of the entire process. In other words, regardless of what the individual samples are, a birds-eye view of the collection of samples must represent the whole process. Conversely, a process that is not ergodic is a process that changes erratically at an inconsistent rate.[1]
