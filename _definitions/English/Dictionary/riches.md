---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/riches
offline_file: ""
offline_thumbnail: ""
uuid: d7455b4b-9619-4d3b-a01b-a3f6adcdba20
updated: 1484310148
title: riches
categories:
    - Dictionary
---
riches
     n : an abundance of material possessions and resources [syn: {wealth}]
