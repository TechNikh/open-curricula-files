---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ruled_surface
offline_file: ""
offline_thumbnail: ""
uuid: b42f6c66-2b99-4132-a63d-999d9565acd2
updated: 1484309168
title: Ruled surface
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ruled_hyperboloid.jpg
tags:
    - Ruled surfaces in differential geometry
    - Parametric representation
    - Developable surface
    - Ruled surfaces in algebraic geometry
    - Ruled surfaces in architecture
categories:
    - Analytic geometry
---
In geometry, a surface S is ruled (also called a scroll) if through every point of S there is a straight line that lies on S. The most familiar examples (illustrated here in three-dimensional Euclidean space) are the plane and the curved surface of a cylinder or cone. Other examples are a conical surface with elliptical directrix, the right conoid, the helicoid, and the tangent developable of a smooth curve in space.
