---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/just
offline_file: ""
offline_thumbnail: ""
uuid: a18efd98-a639-4cf2-9223-d58c62ee0178
updated: 1484310349
title: just
categories:
    - Dictionary
---
just
     adj 1: used especially of what is legally or ethically right or
            proper or fitting; "a just and lasting peace"-
            A.Lincoln; "a kind and just man"; "a just reward";
            "his just inheritance" [ant: {unjust}]
     2: implying justice dictated by reason, conscience, and a
        natural sense of what is fair to all; "equitable treatment
        of all citizens"; "an equitable distribution of gifts
        among the children" [syn: {equitable}] [ant: {inequitable}]
     3: free from favoritism or self-interest or bias or deception;
        or conforming with established standards or rules; "a fair
        referee"; "fair deal"; "on a fair footing"; "a ...
