---
title: Lung Volumes and Capacities EXPLAINED UNDER 5 MINUTES
youtube_id: QJcAJHFqXZg
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
