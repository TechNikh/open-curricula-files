---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biangular_coordinates
offline_file: ""
offline_thumbnail: ""
uuid: 445fd38c-6bc5-4126-b9bf-a40564198c08
updated: 1484309354
title: Biangular coordinates
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/BiangularCoordinateSystem.JPG
categories:
    - Geometry stubs
---
In mathematics, biangular coordinates are a coordinate system for the plane where 
  
    
      
        
          C
          
            1
          
        
        
        
      
    
    {\displaystyle C_{1}\,\!}
  
 and 
  
    
      
        
          C
          
            2
          
        
        
        
      
    
    {\displaystyle C_{2}\,\!}
  
 are two fixed points, and the position of a point P not on the line 
  
    
      
        
          
            
              
                C
                
                  1
                
              
              
                C
                
                  2
                
              
 ...
