---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aligned
offline_file: ""
offline_thumbnail: ""
uuid: 67c9a0e4-c97b-4f4b-8a93-5302962c7da6
updated: 1484310605
title: aligned
categories:
    - Dictionary
---
aligned
     adj 1: brought into agreement or cooperation on the side of a
            faction, party, or cause [ant: {nonaligned}]
     2: in a straight line; "pearly teeth evenly aligned"
