---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ring
offline_file: ""
offline_thumbnail: ""
uuid: 214a1a51-5e14-4803-8262-017c8d1cd12c
updated: 1484310366
title: ring
categories:
    - Dictionary
---
ring
     n 1: a characteristic sound; "it has the ring of sincerity"
     2: a toroidal shape; "a ring of ships in the harbor"; "a halo
        of smoke" [syn: {halo}, {annulus}, {anulus}, {doughnut}, {anchor
        ring}]
     3: a rigid circular band of metal or wood or other material
        used for holding or fastening or hanging or pulling;
        "there was still a rusty iron hoop for tying a horse"
        [syn: {hoop}]
     4: (chemistry) a chain of atoms in a molecule that forms a
        closed loop [syn: {closed chain}] [ant: {open chain}]
     5: an association of criminals; "police tried to break up the
        gang"; "a pack of thieves" [syn: {gang}, {pack}, {mob}]
     6: ...
