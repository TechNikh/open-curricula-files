---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/really
offline_file: ""
offline_thumbnail: ""
uuid: a60b12f6-0add-4d8b-a944-98a07670974c
updated: 1484310353
title: really
categories:
    - Dictionary
---
really
     adv 1: in accordance with truth or fact or reality; "she was now
            truly American"; "a genuinely open society"; "they
            don't really listen to us" [syn: {truly}, {genuinely}]
     2: in actual fact; "to be nominally but not actually
        independent"; "no one actually saw the shark"; "large
        meteorites actually come from the asteroid belt" [syn: {actually}]
     3: in fact (used as intensifiers or sentence modifiers); "in
        truth, moral decay hastened the decline of the Roman
        Empire"; "really, you shouldn't have done it"; "a truly
        awful book" [syn: {in truth}, {truly}]
     4: used as intensifiers; `real' is sometimes used ...
