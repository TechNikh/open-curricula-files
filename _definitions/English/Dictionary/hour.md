---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hour
offline_file: ""
offline_thumbnail: ""
uuid: b27318a9-c4e1-4441-8e58-4a1eb55fa148
updated: 1484310355
title: hour
categories:
    - Dictionary
---
hour
     n 1: a period of time equal to 1/24th of a day; "the job will
          take more than an hour" [syn: {hr}, {60 minutes}]
     2: clock time; "the hour is getting late" [syn: {time of day}]
     3: a special and memorable period; "it was their finest hour"
     4: distance measured by the time taken to cover it; "we live an
        hour from the airport"; "its just 10 minutes away" [syn: {minute}]
