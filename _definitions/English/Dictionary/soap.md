---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soap
offline_file: ""
offline_thumbnail: ""
uuid: 0121003e-b26c-48e1-a724-3473dda01432
updated: 1484310381
title: soap
categories:
    - Dictionary
---
soap
     n 1: a cleansing agent made from the salts of vegetable or animal
          fats
     2: money offered as a bribe
     3: street names for gamma hydroxybutyrate [syn: {scoop}, {max},
         {liquid ecstasy}, {grievous bodily harm}, {goop}, {Georgia
        home boy}, {easy lay}]
     v : rub soap all over, usually with the purpose of cleaning
         [syn: {lather}]
