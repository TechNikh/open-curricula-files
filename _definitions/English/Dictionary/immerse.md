---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immerse
offline_file: ""
offline_thumbnail: ""
uuid: 0f0b33e4-8469-4beb-970d-0d3017db907d
updated: 1484310214
title: immerse
categories:
    - Dictionary
---
immerse
     v 1: thrust or throw into; "Immerse yourself in hot water" [syn:
          {plunge}]
     2: engross (oneself) fully; "He immersed himself into his
        studies" [syn: {steep}, {engulf}, {plunge}, {engross}, {absorb},
         {soak up}]
     3: enclose or envelop completely, as if by swallowing; "The
        huge waves swallowed the small boat and it sank shortly
        thereafter" [syn: {swallow}, {swallow up}, {bury}, {eat up}]
     4: cause to be immersed; "The professor plunged his students
        into the study of the Italian text" [syn: {plunge}]
