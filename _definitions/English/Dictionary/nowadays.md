---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nowadays
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484508721
title: nowadays
categories:
    - Dictionary
---
nowadays
     n : the period of time that is happening now; any continuous
         stretch of time including the moment of speech; "that is
         enough for the present"; "he lives in the present with no
         thought of tomorrow" [syn: {present}]
     adv : in these times; "it is solely by their language that the
           upper classes nowadays are distinguished"- Nancy
           Mitford; "we now rarely see horse-drawn vehicles on
           city streets"; "today almost every home has television"
           [syn: {now}, {today}]
