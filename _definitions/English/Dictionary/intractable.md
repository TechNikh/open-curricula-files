---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intractable
offline_file: ""
offline_thumbnail: ""
uuid: 4daa9781-5da9-4489-b60f-c7fe161b22d1
updated: 1484310168
title: intractable
categories:
    - Dictionary
---
intractable
     adj : not tractable; difficult to manage or mold; "an intractable
           disposition"; "intractable pain"; "the most intractable
           issue of our era"; "intractable metal" [ant: {tractable}]
