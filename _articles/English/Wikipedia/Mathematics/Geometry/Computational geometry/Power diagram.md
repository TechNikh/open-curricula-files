---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Power_diagram
offline_file: ""
offline_thumbnail: ""
uuid: c1358c20-e530-4ce6-883e-72a47b537bf0
updated: 1484309173
title: Power diagram
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Power_diagram.svg.png
tags:
    - Definition
    - Related constructions
    - Algorithms and applications
    - History
categories:
    - Computational geometry
---
In computational geometry, a power diagram is a partition of the Euclidean plane into polygonal cells defined from a set of circles, where the cell for a given circle C consists of all the points for which the power distance to C is smaller than the power distance to the other circles. It is a form of generalized Voronoi diagram, and coincides with the Voronoi diagram of the circle centers in the case that all the circles have equal radii.[1][2][3][4]
