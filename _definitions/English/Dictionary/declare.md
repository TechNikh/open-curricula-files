---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/declare
offline_file: ""
offline_thumbnail: ""
uuid: 60a458ab-e2ec-4b76-940c-4f13e5c701de
updated: 1484310593
title: declare
categories:
    - Dictionary
---
declare
     v 1: state emphatically and authoritatively; "He declared that he
          needed more money to carry out the task he was charged
          with"
     2: announce publicly or officially; "The President declared
        war" [syn: {announce}]
     3: state firmly; "He declared that he was innocent"
     4: declare to be; "She was declared incompetent"; "judge held
        that the defendant was innocent" [syn: {adjudge}, {hold}]
     5: authorize payments of; "declare dividends"
     6: designate (a trump suit or no-trump) with the final bid of a
        hand
     7: make a declaration (of dutiable goods) to a customs
        official; "Do you have anything to declare?"
     8: ...
