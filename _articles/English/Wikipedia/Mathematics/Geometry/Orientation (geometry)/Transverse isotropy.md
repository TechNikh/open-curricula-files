---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transverse_isotropy
offline_file: ""
offline_thumbnail: ""
uuid: 389c7cb6-52bd-4714-ba2e-80f09efbae56
updated: 1484309302
title: Transverse isotropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-USA_10052_Grand_Canyon_Luca_Galuzzi_2007.jpg
tags:
    - Example of transversely isotropic materials
    - Material symmetry matrix
    - Transverse isotropy in physics
    - Transverse isotropy in linear elasticity
    - Condition for material symmetry
    - Elasticity tensor
    - Transverse isotropy in geophysics
    - Backus upscaling (Long wavelength approximation)
    - Short and medium wavelength approximation
    - Thomsen parameters
    - Simplified expressions for wave velocities
categories:
    - Orientation (geometry)
---
A transversely isotropic material is one with physical properties which are symmetric about an axis that is normal to a plane of isotropy. This transverse plane has infinite planes of symmetry and thus, within this plane, the material properties are the same in all directions. Hence, such materials are also known as "polar anisotropic" materials.
