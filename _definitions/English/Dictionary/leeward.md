---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leeward
offline_file: ""
offline_thumbnail: ""
uuid: 311f868b-65dd-490a-a1d9-b73e2374c3fb
updated: 1484310437
title: leeward
categories:
    - Dictionary
---
leeward
     adj : on the side away from the wind; "on the leeward side of the
           island" [ant: {windward}]
     n 1: the direction in which the wind is blowing [ant: {windward}]
     2: the side of something that is sheltered from the wind [syn:
        {lee}, {lee side}]
     adv : toward the wind; "they were sailing leeward" [syn: {upwind}]
           [ant: {windward}, {windward}]
