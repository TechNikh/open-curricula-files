---
version: 1
type: video
provider: YouTube
id: iCbJJ-XA5YE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f14cd5d6-5497-4bbd-9170-97b8fd9c9988
updated: 1486069636
title: 'Ghost in Mirror Experiment - by 187 P.I.'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/iCbJJ-XA5YE/default.jpg
    - https://i3.ytimg.com/vi/iCbJJ-XA5YE/1.jpg
    - https://i3.ytimg.com/vi/iCbJJ-XA5YE/2.jpg
    - https://i3.ytimg.com/vi/iCbJJ-XA5YE/3.jpg
---
