---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pantheist
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484529781
title: pantheist
categories:
    - Dictionary
---
pantheist
     adj : of or relating to pantheism [syn: {pantheistic}]
     n : someone who believes that God and the universe are the same
