---
version: 1
type: video
provider: YouTube
id: 9zmCFMFnvyk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9b2b1ca3-23af-45da-be32-ef06959db85e
updated: 1486069607
title: Subtracting Regrouping from zero
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/9zmCFMFnvyk/default.jpg
    - https://i3.ytimg.com/vi/9zmCFMFnvyk/1.jpg
    - https://i3.ytimg.com/vi/9zmCFMFnvyk/2.jpg
    - https://i3.ytimg.com/vi/9zmCFMFnvyk/3.jpg
---
