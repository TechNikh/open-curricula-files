---
version: 1
type: article
id: https://en.wikipedia.org/wiki/K_shortest_path_routing
offline_file: ""
offline_thumbnail: ""
uuid: b285bdfa-cf3d-41b9-b2cd-38849b4e3c85
updated: 1484309392
title: K shortest path routing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-15-node_network_containing_a_combination_of_bi-directional_and_uni-directional_links.png
tags:
    - History
    - Algorithm
    - First variant
    - Second variant
    - Paths are not required to be loopless
    - Loopless K shortest path routing algorithm
    - Some examples and description
    - 'Example #1'
    - 'Example #2'
    - 'Example #3'
    - Applications
    - Variations
    - Related problems
    - Notes
categories:
    - Computational problems in graph theory
---
It is sometimes crucial to have more than one path between two nodes in a given network. In the event there are additional constraints, other paths different from the shortest path can be computed. To find the shortest path one can use shortest path algorithms such as Dijkstra’s algorithm or Bellman Ford algorithm and extend them to find more than one path. The K Shortest path routing algorithm is a generalization of the shortest path problem. The algorithm not only finds the shortest path, but also K-1 other paths in non-decreasing order of cost. K is the number of shortest paths to find. The problem can be restricted to have the K shortest path without loops (loopless K shortest path) ...
