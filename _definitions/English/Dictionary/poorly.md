---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poorly
offline_file: ""
offline_thumbnail: ""
uuid: ea1efee4-3c57-4200-a776-f036cef55d1a
updated: 1484310295
title: poorly
categories:
    - Dictionary
---
poorly
     adj : somewhat ill or prone to illness; "my poor ailing
           grandmother"; "feeling a bit indisposed today"; "you
           look a little peaked"; "feeling poorly"; "a sickly
           child"; "is unwell and can't come to work" [syn: {ailing},
            {indisposed}, {peaked(p)}, {poorly(p)}, {sickly}, {unwell},
            {under the weather}]
     adv : (`ill' is often used as a combining form) in a poor or
           improper or unsatisfactory manner; not well; "he was
           ill prepared"; "it ill befits a man to betray old
           friends"; "the car runs badly"; "he performed badly on
           the exam"; "the team played poorly"; "ill-fitting
           ...
