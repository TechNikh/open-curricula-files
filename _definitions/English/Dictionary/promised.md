---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/promised
offline_file: ""
offline_thumbnail: ""
uuid: d871de3b-13e2-4f63-89c2-20a81969f324
updated: 1484310561
title: promised
categories:
    - Dictionary
---
promised
     adj : assured by (usually) spoken agreement; "the promised toy";
           "the promised land"
