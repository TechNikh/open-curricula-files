---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/football
offline_file: ""
offline_thumbnail: ""
uuid: b61b4fdd-55a9-4dd7-9b44-4f73cae5f279
updated: 1484310140
title: football
categories:
    - Dictionary
---
football
     n 1: any of various games played with a ball (round or oval) in
          which two teams try to kick or carry or propel the ball
          into each other's goal [syn: {football game}]
     2: the inflated oblong ball used in playing American football
