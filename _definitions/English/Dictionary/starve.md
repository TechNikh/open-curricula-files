---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/starve
offline_file: ""
offline_thumbnail: ""
uuid: 9a1ee8a4-60d9-4f3a-82f3-2a9cbb37ea02
updated: 1484310148
title: starve
categories:
    - Dictionary
---
starve
     v 1: be hungry; go without food; "Let's eat--I'm starving!" [syn:
           {hunger}, {famish}] [ant: {be full}]
     2: die of food deprivation; "The political prisoners starved to
        death"; "Many famished in the countryside during the
        drought" [syn: {famish}]
     3: deprive of food; "They starved the prisoners" [syn: {famish}]
        [ant: {feed}]
     4: have a craving, appetite, or great desire for [syn: {crave},
         {hunger}, {thirst}, {lust}]
     5: deprive of a necessity and cause suffering; "he is starving
        her of love"; "The engine was starved of fuel"
