---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/technological
offline_file: ""
offline_thumbnail: ""
uuid: c48d1cff-4d95-47d9-a23d-5f58aac5de55
updated: 1484310480
title: technological
categories:
    - Dictionary
---
technological
     adj 1: based in scientific and industrial progress; "a
            technological civilization"
     2: of or relating to a practical subject that is organized
        according to scientific principles; "technical college";
        "technological development" [syn: {technical}]
