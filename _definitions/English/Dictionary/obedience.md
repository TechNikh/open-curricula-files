---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obedience
offline_file: ""
offline_thumbnail: ""
uuid: 31b800c3-e4bb-4ee5-ae53-5022cd70530e
updated: 1484310593
title: obedience
categories:
    - Dictionary
---
obedience
     n 1: the act of obeying; dutiful or submissive behavior with
          respect to another person [syn: {obeisance}] [ant: {disobedience}]
     2: the trait of being willing to obey [ant: {disobedience}]
     3: behavior intended to please your parents; "their children
        were never very strong on obedience"; "he went to law
        school out of respect for his father's wishes" [syn: {respect}]
