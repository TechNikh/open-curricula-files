---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/developing
offline_file: ""
offline_thumbnail: ""
uuid: ee4ce1e0-6947-41cc-aff6-29513a141366
updated: 1484310441
title: developing
categories:
    - Dictionary
---
developing
     adj 1: relating to societies in which capital needed to
            industrialize is in short supply [syn: {underdeveloped}]
     2: gradually unfolding or growing (especially as of something
        latent); "his developing social conscience"; "after the
        long winter they took joy in the developing warmth of
        spring"
     3: making or becoming visible through or as if through the
        action of a chemical agent; "he watched as the developing
        photograph became clearer and sharper"
     n : processing a photosensitive material in order to make an
         image visible; "the development and printing of his
         pictures took only two hours" [syn: ...
