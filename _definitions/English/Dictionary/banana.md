---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/banana
offline_file: ""
offline_thumbnail: ""
uuid: 3744855c-680f-4840-a662-1280d9c88368
updated: 1484310383
title: banana
categories:
    - Dictionary
---
banana
     n 1: any of several tropical and subtropical treelike herbs of
          the genus Musa having a terminal crown of large entire
          leaves and usually bearing hanging clusters of elongated
          fruits [syn: {banana tree}]
     2: elongated crescent-shaped yellow fruit with soft sweet flesh
