---
version: 1
type: article
id: https://en.wikipedia.org/wiki/72_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 0c7eea40-3fe7-4b02-8e66-2ffb39b5d8ab
updated: 1484308967
title: 72 (number)
tags:
    - In mathematics
    - In science
    - In astronomy
    - In religion
    - In other fields
    - In sports and games
    - In Software
    - Reference
    - Footnotes
categories:
    - Integers
---
