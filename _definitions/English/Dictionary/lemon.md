---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lemon
offline_file: ""
offline_thumbnail: ""
uuid: ce946e70-7607-4379-aeff-d3897ed3a757
updated: 1484310214
title: lemon
categories:
    - Dictionary
---
lemon
     n 1: yellow oval fruit with juicy acidic flesh
     2: a strong yellow color [syn: {gamboge}, {lemon yellow}, {maize}]
     3: a small evergreen tree that originated in Asia but is widely
        cultivated for its fruit [syn: {lemon tree}, {Citrus limon}]
     4: a distinctive tart flavor characteristic of lemons
     5: an artifact (especially an automobile) that is defective or
        unsatisfactory [syn: {stinker}]
