---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Carath%C3%A9odory_metric'
offline_file: ""
offline_thumbnail: ""
uuid: a47a126d-ac05-45da-af0f-d2ce30712913
updated: 1484309262
title: Carathéodory metric
tags:
    - Definition
    - Properties
    - Carathéodory length of a tangent vector
categories:
    - Metric geometry
---
In mathematics, the Carathéodory metric is a metric defined on the open unit ball of a complex Banach space that has many similar properties to the Poincaré metric of hyperbolic geometry. It is named after the Greek mathematician Constantin Carathéodory.
