---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demarcation
offline_file: ""
offline_thumbnail: ""
uuid: 30638939-463e-42fd-9185-677888f0c46e
updated: 1484310401
title: demarcation
categories:
    - Dictionary
---
demarcation
     n 1: the boundary of a specific area [syn: {limit}, {demarcation
          line}]
     2: a conceptual separation or demarcation; "there is a narrow
        line between sanity and insanity" [syn: {line}, {dividing
        line}, {contrast}]
