---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/specially
offline_file: ""
offline_thumbnail: ""
uuid: 784c55b9-6152-4a6b-8539-fb78397ddd00
updated: 1484310413
title: specially
categories:
    - Dictionary
---
specially
     adv 1: in a special manner; "a specially arranged dinner" [syn: {especially}]
     2: to a distinctly greater extent or degree than is common; "he
        was particularly fussy about spelling"; "a particularly
        gruesome attack"; "under peculiarly tragic circumstances";
        "an especially (or specially) cautious approach to the
        danger" [syn: {particularly}, {peculiarly}, {especially}]
