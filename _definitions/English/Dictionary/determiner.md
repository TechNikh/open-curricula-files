---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/determiner
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484468341
title: determiner
categories:
    - Dictionary
---
determiner
     n 1: an argument that is conclusive [syn: {clincher}, {determining
          factor}]
     2: one of a limited class of noun modifiers that determine the
        referents of noun phrases [syn: {determinative}]
     3: a determining or causal element or factor; "education is an
        important determinant of one's outlook on life" [syn: {determinant},
         {determinative}, {determining factor}, {causal factor}]
