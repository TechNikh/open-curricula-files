---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carnallite
offline_file: ""
offline_thumbnail: ""
uuid: fa40471b-be5e-4be4-8af9-eb4d090e076b
updated: 1484310409
title: carnallite
categories:
    - Dictionary
---
carnallite
     n : a white or reddish mineral consisting of hydrous chlorides
         of potassium and magnesium; used as a fertilizer and as a
         source of potassium and magnesium
