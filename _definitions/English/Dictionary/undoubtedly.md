---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undoubtedly
offline_file: ""
offline_thumbnail: ""
uuid: 7cfe5178-4f2f-4560-9702-01561d2aa2c5
updated: 1484310601
title: undoubtedly
categories:
    - Dictionary
---
undoubtedly
     adv : with certainty; "it's undoubtedly very beautiful" [syn: {doubtless},
            {beyond question}, {without doubt}, {beyond any doubt}]
