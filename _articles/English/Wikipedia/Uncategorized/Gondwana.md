---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gondwana
offline_file: ""
offline_thumbnail: ""
uuid: aa05c741-0cb1-4ef0-a499-328b47451e57
updated: 1484308413
title: Gondwana
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Laurasia-Gondwana.svg.png
tags:
    - Formation
    - Pangaea
    - Climate
    - Breakup
    - Mesozoic
    - Cenozoic
categories:
    - Uncategorized
---
In paleogeography, Gondwana (pronunciation: /ɡɒndˈwɑːnə/),[1][2] also Gondwanaland, is the name given to an ancient supercontinent. It is believed to have sutured between about 570 and 510 million years ago (Mya), joining East Gondwana to West Gondwana. Gondwana formed prior to Pangaea, and later became part of it.[3]
