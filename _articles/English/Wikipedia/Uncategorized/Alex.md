---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alex
offline_file: ""
offline_thumbnail: ""
uuid: a2f3e07e-f301-49f3-9b5c-2d897308b26c
updated: 1484308349
title: Alex
tags:
    - People whose first name is Alex
    - People commonly named or nicknamed Alex
    - American footballers
    - Association footballers
    - Basketballers
    - Music
    - Animals
    - Fictional characters
categories:
    - Uncategorized
---
Alex is a common given name commonly associated with the Greek name Alexandros. In English, it is usually a diminutive of the male given name Alexander, or its female equivalent Alexandra or Alexandria. Aleck or Alec is the Scottish form of Alex. The East European male name Alexey (Aleksei, Alexis, Aleksa) is also sometimes shortened to Alex. It is a commonly used nickname in Spanish for Alejandro, Alexandro, Alejandrino and Alexandrino, and related names like Alexa and Alexis.
