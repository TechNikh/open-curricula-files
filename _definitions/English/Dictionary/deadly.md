---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deadly
offline_file: ""
offline_thumbnail: ""
uuid: 431e6a5c-472c-41c4-a6c3-e7f8df2b5071
updated: 1484310577
title: deadly
categories:
    - Dictionary
---
deadly
     adj 1: causing or capable of causing death; "a fatal accident"; "a
            deadly enemy"; "mortal combat"; "a mortal illness"
            [syn: {deathly}, {mortal}]
     2: of an instrument of certain death; "deadly poisons"; "lethal
        weapon"; "a lethal injection" [syn: {lethal}]
     3: extremely poisonous or injurious; producing venom; "venomous
        snakes"; "a virulent insect bite" [syn: {venomous}, {virulent}]
     4: involving loss of divine grace or spiritual death; "the
        seven deadly sins" [syn: {mortal(a)}]
     5: exceedingly harmful [syn: {baneful}, {pernicious}, {pestilent}]
     6: (of a disease) having a rapid course and violent effect
     adv ...
