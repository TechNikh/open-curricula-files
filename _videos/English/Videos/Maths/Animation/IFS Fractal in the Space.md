---
version: 1
type: video
provider: YouTube
id: NIWz3BuE4nE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 595a7405-2fca-41fb-8e55-ac8cd1b1c9a1
updated: 1486069643
title: IFS Fractal in the Space
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/NIWz3BuE4nE/default.jpg
    - https://i3.ytimg.com/vi/NIWz3BuE4nE/1.jpg
    - https://i3.ytimg.com/vi/NIWz3BuE4nE/2.jpg
    - https://i3.ytimg.com/vi/NIWz3BuE4nE/3.jpg
---
