---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Null_model
offline_file: ""
offline_thumbnail: ""
uuid: 5e781f60-7ac9-40b3-812e-b8fcfac20a67
updated: 1484308625
title: Null model
categories:
    - Graph theory
---
In mathematics, in the study of statistical properties of graphs, the null model is a graph which matches one specific graph in some of its structural features, but which is otherwise taken to be an instance of a random graph. The null model is used as a term of comparison, to verify whether the graph in question displays some feature, such as community structure, or not.
