---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gleaming
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484610481
title: gleaming
categories:
    - Dictionary
---
gleaming
     adj : bright with a steady but subdued shining; "from the plane we
           saw the city below agleam with lights"; "the gleaming
           brass on the altar"; "Nereids beneath the nitid moon"
           [syn: {agleam}, {nitid}]
     n 1: a flash of light (especially reflected light) [syn: {gleam},
           {glimmer}]
     2: an appearance of reflected light [syn: {gleam}, {glow}, {lambency}]
