---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tetrachloride
offline_file: ""
offline_thumbnail: ""
uuid: e9370fbd-af45-4ed5-9c4f-15b95764153d
updated: 1484310424
title: tetrachloride
categories:
    - Dictionary
---
tetrachloride
     n : any compound that contains four chlorine atoms per molecule
