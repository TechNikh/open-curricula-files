---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grounds
offline_file: ""
offline_thumbnail: ""
uuid: e6597fe5-c3b2-43a4-ac31-5d4a974dfebe
updated: 1484310441
title: grounds
categories:
    - Dictionary
---
grounds
     n 1: your basis for belief or disbelief; knowledge on which to
          base belief; "the evidence that smoking causes lung
          cancer is very compelling" [syn: {evidence}]
     2: the enclosed land around a house or other building; "it was
        a small house with almost no yard" [syn: {yard}, {curtilage}]
     3: a tract of land cleared for some special purposes
        (recreation or burial etc.)
     4: a justification for something existing or happening; "he had
        no cause to complain"; "they had good reason to rejoice"
        [syn: {cause}, {reason}]
     5: sediment that has settled at the bottom of a liquid [syn: {dregs},
         {settlings}]
