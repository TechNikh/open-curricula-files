---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gromov%27s_inequality_for_complex_projective_space'
offline_file: ""
offline_thumbnail: ""
uuid: 752b11e1-25ff-465b-a31a-12e761edb9f8
updated: 1484309223
title: "Gromov's inequality for complex projective space"
categories:
    - Differential geometry
---
valid for an arbitrary Riemannian metric on the complex projective space, where the optimal bound is attained by the symmetric Fubini–Study metric, providing a natural geometrisation of quantum mechanics. Here 
  
    
      
        
          s
          t
          s
          y
          
            s
            
              2
            
          
        
      
    
    {\displaystyle \operatorname {stsys_{2}} }
  
 is the stable 2-systole, which in this case can be defined as the infimum of the areas of rational 2-cycles representing the class of the complex projective line 
  
    
      
        
          
            C
            P
          
          
            1
   ...
