---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computable_function
offline_file: ""
offline_thumbnail: ""
uuid: 72e10dd7-7109-496f-9135-a843f9618120
updated: 1484309485
title: Computable function
tags:
    - Definition
    - Characteristics of computable functions
    - Computable sets and relations
    - Formal languages
    - Examples
    - Church–Turing thesis
    - Provability
    - Relation to recursively defined functions
    - Total functions that are not provably total
    - Uncomputable functions and unsolvable problems
    - Extensions of computability
    - Relative computability
    - Higher recursion theory
    - Hyper-computation
categories:
    - Computability theory
---
Computable functions are the basic objects of study in computability theory. Computable functions are the formalized analogue of the intuitive notion of algorithm, in the sense that a function is computable if there exists an algorithm that can do the job of the function, i.e. given an input of the function domain it can return the corresponding output. Computable functions are used to discuss computability without referring to any concrete model of computation such as Turing machines or register machines. Any definition, however, must make reference to some specific model of computation but all valid definitions yield the same class of functions. Particular models of computability that ...
