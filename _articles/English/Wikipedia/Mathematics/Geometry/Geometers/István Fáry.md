---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Istv%C3%A1n_F%C3%A1ry'
offline_file: ""
offline_thumbnail: ""
uuid: 16f4c9a1-24c9-45ff-b2b4-01206da62594
updated: 1484309077
title: István Fáry
tags:
    - Biography
    - Selected publications
categories:
    - Geometers
---
István Fáry (30 June 1922 – 2 November 1984) was a Hungarian-born mathematician known for his work in geometry and algebraic topology.[1] He proved Fáry's theorem that every planar graph has a straight line embedding in 1948, and the Fary–Milnor theorem lower-bounding the curvature of a nontrivial knot in 1949.
