---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hiss
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484505481
title: hiss
categories:
    - Dictionary
---
hiss
     n 1: a fricative sound (especially as an expression of
          disapproval); "the performers could not be heard over
          the hissing of the audience" [syn: {hissing}, {sibilation}]
     2: a cry or noise made to express displeasure or contempt [syn:
         {boo}, {hoot}, {Bronx cheer}, {raspberry}, {razzing}, {snort},
         {bird}]
     v 1: make a sharp hissing sound, as if to show disapproval [syn:
          {siss}, {sizz}, {sibilate}]
     2: move with a whooshing sound [syn: {whoosh}]
     3: express or utter with a hiss [syn: {sizz}, {siss}, {sibilate}]
     4: show displeasure, as after a performance or speech [syn: {boo}]
        [ant: {applaud}]
