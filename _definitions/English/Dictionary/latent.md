---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/latent
offline_file: ""
offline_thumbnail: ""
uuid: 9612590f-81ee-493e-8ac4-9e9e3a8bfd60
updated: 1484310224
title: latent
categories:
    - Dictionary
---
latent
     adj 1: potentially existing but not presently evident or realized;
            "a latent fingerprint"; "latent talent"
     2: not presently active; "latent infection"; "latent diabetes"
