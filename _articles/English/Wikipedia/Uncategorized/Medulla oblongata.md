---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oblongata
offline_file: ""
offline_thumbnail: ""
uuid: bb4f7cf7-4d3c-4a3f-a347-f8d830bbcd0b
updated: 1484308378
title: Medulla oblongata
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-1311_Brain_Stem.jpg
tags:
    - Anatomy
    - External surfaces
    - Blood supply
    - Development
    - Function
    - Clinical significance
    - Other animals
    - Additional images
categories:
    - Uncategorized
---
The medulla oblongata (or medulla) is located in the hindbrain, anterior to the cerebellum. It is a cone-shaped neuronal mass responsible for autonomic (involuntary) functions ranging from vomiting to sneezing. The medulla contains the cardiac, respiratory, vomiting and vasomotor centers and therefore deals with the autonomic functions of breathing, heart rate and blood pressure.
