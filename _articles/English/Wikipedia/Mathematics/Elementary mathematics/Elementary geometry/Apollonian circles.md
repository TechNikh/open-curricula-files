---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Apollonian_circles
offline_file: ""
offline_thumbnail: ""
uuid: aa1a3267-e7ba-479a-b59c-fa4c0998c79f
updated: 1484308927
title: Apollonian circles
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Apollonian_circles.svg.png
tags:
    - Definition
    - Bipolar coordinates
    - Pencils of circles
    - Radical axis and central line
    - >
        Inversive geometry, orthogonal intersection, and coordinate
        systems
    - Notes
categories:
    - Elementary geometry
---
Apollonian circles are two families of circles such that every circle in the first family intersects every circle in the second family orthogonally, and vice versa. These circles form the basis for bipolar coordinates. They were discovered by Apollonius of Perga, a renowned Greek geometer.
