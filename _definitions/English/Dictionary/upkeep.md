---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upkeep
offline_file: ""
offline_thumbnail: ""
uuid: 2130a02d-4d2c-40f0-aa1f-3cbe6a23f0be
updated: 1484310448
title: upkeep
categories:
    - Dictionary
---
upkeep
     n 1: activity involved in maintaining something in good working
          order; "he wrote the manual on car care" [syn: {care}, {maintenance}]
     2: the act of sustaining life by food or providing a means of
        subsistence; "they were in want of sustenance"; "fishing
        was their main sustainment" [syn: {sustenance}, {sustentation},
         {sustainment}, {maintenance}]
