---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Padma
offline_file: ""
offline_thumbnail: ""
uuid: d5e55f5a-d78f-4a6d-a608-1d172c5d3db6
updated: 1484308412
title: Padma River
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Boat_Sailing_up_Padma_River_Bangladesh.jpg
tags:
    - History
    - Etymology
    - Geographic effects
    - geography
    - Pabna District
    - Kushtia District
    - Rajshahi District
    - Infrastructure
    - Damming
    - Padma Bridge
categories:
    - Uncategorized
---
Padma is a major river in Bangladesh. It is the main distributary of the Ganges, flowing generally southeast for 120 kilometres (75 mi) to its confluence with the Meghna River near the Bay of Bengal.[1] The city of Rajshahi is situated on the banks of the river.[2]
