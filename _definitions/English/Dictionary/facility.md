---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/facility
offline_file: ""
offline_thumbnail: ""
uuid: bf6f606d-5d9a-4e4f-a78e-0794e24cf27e
updated: 1484310477
title: facility
categories:
    - Dictionary
---
facility
     n 1: a building or place that provides a particular service or is
          used for a particular industry; "the assembly plant is
          an enormous facility" [syn: {installation}]
     2: skillful performance or ability without difficulty; "his
        quick adeptness was a product of good design"; "he was
        famous for his facility as an archer" [syn: {adeptness}, {adroitness},
         {deftness}, {quickness}]
     3: a natural effortlessness; "they conversed with great
        facility"; "a happy readiness of conversation"--Jane
        Austen [syn: {readiness}]
     4: services and space and equipment provided for a particular
        purpose; "catering ...
