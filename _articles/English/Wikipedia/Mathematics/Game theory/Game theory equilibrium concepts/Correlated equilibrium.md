---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Correlated_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 7358ef29-3fe4-457b-8d23-67bd5c0e73d2
updated: 1484309032
title: Correlated equilibrium
tags:
    - Formal definition
    - An example
    - Learning correlated equilibria
    - Sources
categories:
    - Game theory equilibrium concepts
---
In game theory, a correlated equilibrium is a solution concept that is more general than the well known Nash equilibrium. It was first discussed by mathematician Robert Aumann (1974). The idea is that each player chooses his/her action according to his/her observation of the value of the same public signal. A strategy assigns an action to every possible observation a player can make. If no player would want to deviate from the recommended strategy (assuming the others don't deviate), the distribution is called a correlated equilibrium.
