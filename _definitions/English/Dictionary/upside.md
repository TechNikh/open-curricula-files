---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upside
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484394661
title: upside
categories:
    - Dictionary
---
upside
     n : the highest or uppermost side of anything; "put your books
         on top of the desk"; "only the top side of the box was
         painted" [syn: {top}, {top side}, {upper side}]
