---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dehusking
offline_file: ""
offline_thumbnail: ""
uuid: 42832030-464b-4a3e-9f15-7eb32d8b0813
updated: 1484308440
title: Husk
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-E8025-Milyanfan-corn-huskers.jpg
categories:
    - Uncategorized
---
Husk (or hull) in botany is the outer shell or coating of a seed. It often refers to the leafy outer covering of an ear of maize (corn) as it grows on the plant. Literally, a husk or hull includes the protective outer covering of a seed, fruit or vegetable. It can also refer to the exuvia of bugs or small animals left behind after moulting.
