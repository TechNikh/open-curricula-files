---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hatred
offline_file: ""
offline_thumbnail: ""
uuid: fd59bfd4-d938-4da8-8215-34ce33ec832f
updated: 1484310547
title: hatred
categories:
    - Dictionary
---
hatred
     n : the emotion of hate; a feeling of dislike so strong that it
         demands action [syn: {hate}] [ant: {love}]
