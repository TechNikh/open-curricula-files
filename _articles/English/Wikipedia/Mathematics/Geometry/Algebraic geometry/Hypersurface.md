---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hypersurface
offline_file: ""
offline_thumbnail: ""
uuid: cc22b523-6b64-4fc1-8d0d-5b5fe4b32d64
updated: 1484309156
title: Hypersurface
categories:
    - Algebraic geometry
---
In geometry, a hypersurface is a generalization of the concept of hyperplane. Suppose an enveloping manifold M has n dimensions; then any submanifold of M of n − 1 dimensions is a hypersurface. Equivalently, the codimension of a hypersurface is one. For example, the n-sphere in Rn+1 is called a hypersphere. Hypersurfaces occur frequently in multivariable calculus as level sets.
