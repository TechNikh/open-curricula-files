---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stimulation
offline_file: ""
offline_thumbnail: ""
uuid: dfb66889-2c3f-4ee7-be29-db68b03bc5a2
updated: 1484310357
title: stimulation
categories:
    - Dictionary
---
stimulation
     n 1: the act of arousing an organism to action
     2: any stimulating information or event; acts to arouse action
        [syn: {stimulus}, {stimulant}, {input}]
     3: (physiology) the effect of a stimulus (on nerves or organs
        etc.)
     4: mutual sexual stimulation prior to sexual intercourse [syn:
        {foreplay}, {arousal}]
