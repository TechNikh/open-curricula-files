---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vanilla
offline_file: ""
offline_thumbnail: ""
uuid: 8bf7c8a9-2e7a-4e18-81e7-daaf54f083ce
updated: 1484310379
title: vanilla
categories:
    - Dictionary
---
vanilla
     n 1: any of numerous climbing plants of the genus Vanilla having
          fleshy leaves and clusters of large waxy highly fragrant
          white or green or topaz flowers
     2: a flavoring prepared from vanilla beans macerated in alcohol
        (or imitating vanilla beans) [syn: {vanilla extract}]
     3: a distinctive fragrant flavor characteristic of vanilla
        beans
