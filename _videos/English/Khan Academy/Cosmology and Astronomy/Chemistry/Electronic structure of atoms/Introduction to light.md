---
version: 1
type: video
provider: YouTube
id: rLNM8zI4Q_M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Chemistry/Electronic%20structure%20of%20atoms/Introduction%20to%20light.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1ea7c155-2305-4e03-869f-a9eaae3eb131
updated: 1486069686
title: Introduction to light
categories:
    - Electronic structure of atoms
thumbnail_urls:
    - https://i3.ytimg.com/vi/rLNM8zI4Q_M/default.jpg
    - https://i3.ytimg.com/vi/rLNM8zI4Q_M/1.jpg
    - https://i3.ytimg.com/vi/rLNM8zI4Q_M/2.jpg
    - https://i3.ytimg.com/vi/rLNM8zI4Q_M/3.jpg
---
