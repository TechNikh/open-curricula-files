---
version: 1
type: video
provider: YouTube
id: ELtSFDod3Dg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Eureka%2018%20-%20Evaporation%20%26%20Condensation.mov.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 345e76f8-ed45-4669-9e2c-c6e1ae5a9e46
updated: 1486069666
title: 'Eureka 18 - Evaporation & Condensation.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/ELtSFDod3Dg/default.jpg
    - https://i3.ytimg.com/vi/ELtSFDod3Dg/1.jpg
    - https://i3.ytimg.com/vi/ELtSFDod3Dg/2.jpg
    - https://i3.ytimg.com/vi/ELtSFDod3Dg/3.jpg
---
