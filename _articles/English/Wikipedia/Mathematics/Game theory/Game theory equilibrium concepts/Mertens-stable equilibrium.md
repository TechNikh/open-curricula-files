---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mertens-stable_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 648a459c-c216-4445-b5af-37f6f88aeeb7
updated: 1484309032
title: Mertens-stable equilibrium
tags:
    - Desirable Properties of a Refinement
    - Properties of Stable Sets
    - Definition of a Stable Set
categories:
    - Game theory equilibrium concepts
---
Mertens stability is a solution concept used to predict the outcome of a non-cooperative game. A tentative definition of stability was proposed by Elon Kohlberg and Jean-François Mertens[1] for games with finite numbers of players and strategies. Later, Mertens[2] proposed a stronger definition that was elaborated further by Srihari Govindan and Mertens.[3] This solution concept is now called Mertens stability, or just stability.
