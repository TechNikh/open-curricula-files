---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dish
offline_file: ""
offline_thumbnail: ""
uuid: d7947b7b-7c5c-49e8-bf72-aca37468c850
updated: 1484310353
title: dish
categories:
    - Dictionary
---
dish
     n 1: a piece of dishware normally used as a container for holding
          or serving food; "we gave them a set of dishes for a
          wedding present"
     2: a particular item of prepared food; "she prepared a special
        dish for dinner"
     3: the quantity that a dish will hold; "they served me a dish
        of rice" [syn: {dishful}]
     4: a very attractive or seductive looking woman [syn: {smasher},
         {stunner}, {knockout}, {beauty}, {ravisher}, {sweetheart},
         {peach}, {lulu}, {looker}, {mantrap}]
     5: directional antenna consisting of a parabolic reflector for
        microwave or radio frequency radiation [syn: {dish aerial},
         {dish ...
