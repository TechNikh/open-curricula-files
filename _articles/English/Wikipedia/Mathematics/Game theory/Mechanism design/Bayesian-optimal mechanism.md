---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bayesian-optimal_mechanism
offline_file: ""
offline_thumbnail: ""
uuid: a31242c5-5cda-485e-b01f-69325e2c1a52
updated: 1484309045
title: Bayesian-optimal mechanism
tags:
    - Example
    - Notation
    - The Myerson mechanism
    - Truthfulness
    - Single-item auction
    - Digital-goods auction
    - Alternatives
categories:
    - Mechanism design
---
A Bayesian-optimal mechanism (BOM) is a mechanism in which the designer does not know the valuations of the agents for whom the mechanism is designed, but he knows that they are random variables and he knows the probability distribution of these variables.
