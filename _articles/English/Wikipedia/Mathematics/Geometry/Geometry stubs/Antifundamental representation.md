---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Antifundamental_representation
offline_file: ""
offline_thumbnail: ""
uuid: 6ea265e0-96fe-4c1e-9456-219a8c10a3fb
updated: 1484309354
title: Antifundamental representation
categories:
    - Geometry stubs
---
In mathematics, an antifundamental representation of a Lie group is the complex conjugate of the fundamental representation,[1] although the distinction between the fundamental and the antifundamental representation is a matter of convention. However, these two are often non-equivalent, because each of them is a complex representation.
