---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Acoustic_shadow
offline_file: ""
offline_thumbnail: ""
uuid: 39abc576-90d7-4fe5-a7b1-3614c1617b87
updated: 1484308883
title: Acoustic shadow
tags:
    - Short distance acoustic shadow
    - Long distance acoustic shadow
categories:
    - Waves
---
An acoustic shadow or sound shadow is an area through which sound waves fail to propagate, due to topographical obstructions or disruption of the waves via phenomena such as wind currents, buildings, or sound barriers.
