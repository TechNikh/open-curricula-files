---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/whistle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484513641
title: whistle
categories:
    - Dictionary
---
whistle
     n 1: the sound made by something moving rapidly or by steam
          coming out of a small aperture [syn: {whistling}]
     2: the act of signalling (e.g., summoning) by whistling or
        blowing a whistle; "the whistle signalled the end of the
        game" [syn: {whistling}]
     3: acoustic device that forces air or steam against an edge or
        into a cavity and so produces a loud shrill sound
     4: an inexpensive fipple flute [syn: {pennywhistle}, {tin
        whistle}]
     v 1: make whistling sounds; "He lay there, snoring and whistling"
     2: move with, or as with, a whistling sound; "The bullets
        whistled past him"
     3: utter or express by ...
