---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soldier
offline_file: ""
offline_thumbnail: ""
uuid: 49bac502-3cf1-44e6-b27f-edb797ed9490
updated: 1484310459
title: soldier
categories:
    - Dictionary
---
soldier
     n 1: an enlisted man or woman who serves in an army; "the
          soldiers stood at attention"
     2: a wingless sterile ant or termite having a large head and
        powerful jaws adapted for defending the colony
     v : serve as a soldier in the military
