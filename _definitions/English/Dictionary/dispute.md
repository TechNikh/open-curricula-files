---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dispute
offline_file: ""
offline_thumbnail: ""
uuid: 7da1671c-be6f-4b86-99cc-004243fdc1f0
updated: 1484310467
title: dispute
categories:
    - Dictionary
---
dispute
     n 1: a disagreement or argument about something important; "he
          had a dispute with his wife"; "there were irreconcilable
          differences"; "the familiar conflict between Republicans
          and Democrats" [syn: {difference}, {difference of
          opinion}, {conflict}]
     2: coming into conflict with [syn: {contravention}]
     v 1: take exception to; "She challenged his claims" [syn: {challenge},
           {gainsay}]
     2: have a disagreement over something; "We quarreled over the
        question as to who discovered America"; "These tewo
        fellows are always scrapping over something" [syn: {quarrel},
         {scrap}, {argufy}, {altercate}]
