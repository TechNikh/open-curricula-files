---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ramachandra
offline_file: ""
offline_thumbnail: ""
uuid: 4a4e9e81-399b-4642-b6a0-7b1e02b5b704
updated: 1484308412
title: Rama
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Lord_Rama_with_arrows_0.jpg
tags:
    - Etymology
    - Literary sources
    - Balkand
    - Rama and Sita
    - "Sita's Exile"
    - children
    - Later life
    - Abilities
    - Maryada Purushottama
    - Companions
    - Bharata and Lakshmana
    - 'Jatayu, Hanuman & Vibhishana'
    - Rama In War
    - Varuna
    - Facing Ravana
    - Rama Rajya
    - International Influence
    - Rama Worship
    - Festivals
    - Rama Navami
    - Vijayadashmi
    - Diwali
    - Temples
    - In Jainism
    - Notes
categories:
    - Uncategorized
---
Rama (/ˈrɑːmə/;[1] Sanskrit: राम Rāma) or Srī Rāmachandra (Sanskrit : श्री रामचन्द्र) is the seventh avatar of the Hindu God Vishnu. He is the central figure of the Hindu epic Ramayana, which is the principal narration of the events connected to his incarnation on Earth, his ideals and his greatness. Rama is one of the many deities in Hinduism and especially of the various Vaishnava sects. Religious texts and scriptures based on his life have been a formative component in numerous cultures of South and Southeast Asia.[2] Along with Krishna, Rama is considered to be one of the most important avatars of Vishnu. In Rama-centric sects, he is considered ...
