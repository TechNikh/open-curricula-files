---
version: 1
type: video
provider: YouTube
id: I109P6Cx9Bo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8168ff6e-e8f8-44c2-9316-ae40de3ce41a
updated: 1486069634
title: 'Water Turbidity and Hydroelectric Production - 8th Grade Science Fair Project'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/I109P6Cx9Bo/default.jpg
    - https://i3.ytimg.com/vi/I109P6Cx9Bo/1.jpg
    - https://i3.ytimg.com/vi/I109P6Cx9Bo/2.jpg
    - https://i3.ytimg.com/vi/I109P6Cx9Bo/3.jpg
---
