---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ghana
offline_file: ""
offline_thumbnail: ""
uuid: 6a1ea30a-20b2-4b33-9631-d973ee6738c6
updated: 1484310581
title: ghana
categories:
    - Dictionary
---
Ghana
     n : a republic in West Africa on the Gulf of Guinea; "Ghana was
         colonized as the Gold Coast by the British" [syn: {Republic
         of Ghana}, {Gold Coast}]
