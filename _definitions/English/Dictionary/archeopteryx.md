---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/archeopteryx
offline_file: ""
offline_thumbnail: ""
uuid: f0d49b6a-0113-418c-8ffb-89116d19c8f0
updated: 1484310281
title: archeopteryx
categories:
    - Dictionary
---
archeopteryx
     n : extinct primitive toothed bird of the Jurassic period having
         a long feathered tail and hollow bones; usually
         considered the most primitive of all birds [syn: {archaeopteryx},
          {Archaeopteryx lithographica}]
