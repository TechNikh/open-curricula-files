---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Triangulation_(surveying)
offline_file: ""
offline_thumbnail: ""
uuid: b0b61f2f-3dc8-483f-b284-557dce71eb6b
updated: 1484308941
title: Triangulation (surveying)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Theb1604_-_Flickr_-_NOAA_Photo_Library.jpg
tags:
    - Distance to a point by measuring two fixed angles
    - Calculation
    - History
    - Gemma Frisius and triangulation for mapmaking
    - Willebrord Snell and modern triangulation networks
categories:
    - Elementary geometry
---
In surveying, triangulation is the process of determining the location of a point by measuring only angles to it from known points at either end of a fixed baseline, rather than measuring distances to the point directly as in trilateration. The point can then be fixed as the third point of a triangle with one known side and two known angles.
