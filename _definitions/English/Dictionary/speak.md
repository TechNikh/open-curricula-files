---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/speak
offline_file: ""
offline_thumbnail: ""
uuid: e904560e-9cc1-49aa-96bb-72b9c6f22a6b
updated: 1484310531
title: speak
categories:
    - Dictionary
---
speak
     v 1: express in speech; "She talks a lot of nonsense"; "This
          depressed patient does not verbalize" [syn: {talk}, {utter},
           {mouth}, {verbalize}, {verbalise}]
     2: exchange thoughts; talk with; "We often talk business";
        "Actions talk louder than words" [syn: {talk}]
     3: use language; "the baby talks already"; "the prisoner won't
        speak"; "they speak a strange dialect" [syn: {talk}]
     4: give a speech to; "The chairman addressed the board of
        trustees" [syn: {address}]
     5: make a characteristic or natural sound; "The drums spoke"
     [also: {spoken}, {spoke}]
