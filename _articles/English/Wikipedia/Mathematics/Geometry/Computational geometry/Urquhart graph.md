---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Urquhart_graph
offline_file: ""
offline_thumbnail: ""
uuid: f2c95391-6a44-41ae-bd7d-afb24246cd59
updated: 1484309176
title: Urquhart graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Urquhart_graph.svg.png
categories:
    - Computational geometry
---
In computational geometry, the Urquhart graph of a set of points in the plane, named after Roderick B. Urquhart, is obtained by removing the longest edge from each triangle in the Delaunay triangulation.
