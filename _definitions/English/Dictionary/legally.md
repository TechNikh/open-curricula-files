---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/legally
offline_file: ""
offline_thumbnail: ""
uuid: c0c2121b-8fc1-4d09-8d0f-dde6f88b7674
updated: 1484310477
title: legally
categories:
    - Dictionary
---
legally
     adv 1: by law; conforming to the law; "we are lawfully wedded now"
            [syn: {lawfully}, {wrongfully}, {de jure}] [ant: {unlawfully}]
     2: in a legal manner; "he acted legally"
