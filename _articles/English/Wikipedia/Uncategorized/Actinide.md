---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Actinoids
offline_file: ""
offline_thumbnail: ""
uuid: 0678450a-5348-40f3-9072-2315897db1bd
updated: 1484308383
title: Actinide
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nagasakibomb.jpg
tags:
    - Discovery, isolation and synthesis
    - From actinium to uranium
    - Neptunium and above
    - Isotopes
    - Distribution in nature
    - Extraction
    - Properties
    - Physical properties
    - Chemical properties
    - Compounds
    - Oxides and hydroxides
    - Salts
    - Applications
    - Toxicity
    - References and notes
    - Bibliography
categories:
    - Uncategorized
---
The actinide /ˈæktᵻnaɪd/ or actinoid /ˈæktᵻnɔɪd/ (IUPAC nomenclature) series encompasses the 15 metallic chemical elements with atomic numbers from 89 to 103, actinium through lawrencium.[2][3][4][5]
