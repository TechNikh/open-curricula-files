---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smallest
offline_file: ""
offline_thumbnail: ""
uuid: 0b01525d-5a40-40c1-aa20-b70bf0e4067f
updated: 1484310216
title: smallest
categories:
    - Dictionary
---
smallest
     adj 1: having or being distinguished by diminutive size; "the least
            bittern" [syn: {least}, {littlest}]
     2: minimal in magnitude; "lowest wages"; "the least amount of
        fat allowed"; "the smallest amount" [syn: {least}, {lowest}]
