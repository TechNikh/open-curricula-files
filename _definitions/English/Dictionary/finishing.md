---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finishing
offline_file: ""
offline_thumbnail: ""
uuid: 9ae43fd7-232f-4d49-9b16-0a931a3d30b0
updated: 1484310152
title: finishing
categories:
    - Dictionary
---
finishing
     n 1: a decorative texture or appearance of a surface (or the
          substance that gives it that appearance); "the boat had
          a metallic finish"; "he applied a coat of a clear
          finish"; "when the finish is too thin it is difficult to
          apply evenly" [syn: {coating}, {finish}]
     2: the act of finishing; "his best finish in a major tournament
        was third"; "the speaker's finishing was greeted with
        applause" [syn: {finish}] [ant: {beginning}]
