---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Akihiko_Matsui_(economist)
offline_file: ""
offline_thumbnail: ""
uuid: 4c09357b-6b95-4781-8ad4-3da08ecc5f73
updated: 1484308509
title: Akihiko Matsui (economist)
tags:
    - Biography
    - Published works
    - Books
    - Journal article
    - Honors
categories:
    - Game theorists
---
