---
version: 1
type: video
provider: YouTube
id: KleVi33vd_M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 38b15975-d293-4bd4-bb8f-8dfa9c721caa
updated: 1486069621
title: 'Ratio and proportion- fast tricks maths'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/KleVi33vd_M/default.jpg
    - https://i3.ytimg.com/vi/KleVi33vd_M/1.jpg
    - https://i3.ytimg.com/vi/KleVi33vd_M/2.jpg
    - https://i3.ytimg.com/vi/KleVi33vd_M/3.jpg
---
