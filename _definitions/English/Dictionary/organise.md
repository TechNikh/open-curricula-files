---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/organise
offline_file: ""
offline_thumbnail: ""
uuid: cc0d8b1f-41b4-4da7-b89b-54650f1e1d57
updated: 1484310445
title: organise
categories:
    - Dictionary
---
organise
     v 1: bring order and organization to; "Can you help me organize
          my files?" [syn: {organize}, {coordinate}]
     2: create (as an entity); "social groups form everywhere";
        "They formed a company" [syn: {form}, {organize}]
     3: form or join a union; "The autoworkers decided to unionize"
        [syn: {unionize}, {unionise}, {organize}]
     4: cause to be structured or ordered or operating according to
        some principle or idea [syn: {organize}] [ant: {disorganize},
         {disorganize}]
     5: arrange by systematic planning and united effort; "machinate
        a plot"; "organize a strike"; "devise a plan to take over
        the director's office" ...
