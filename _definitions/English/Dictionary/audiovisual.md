---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/audiovisual
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484492401
title: audiovisual
categories:
    - Dictionary
---
audiovisual
     adj : involving both hearing and seeing (usually relating to
           teaching aids); "the school's audiovisual department"
     n : materials using sight or sound to present information;
         "language tapes and videocassettes and other
         audiovisuals" [syn: {audiovisual aid}]
