---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/words
offline_file: ""
offline_thumbnail: ""
uuid: 4365ff39-3cc3-466e-bae2-b5d47c9ea21a
updated: 1484310363
title: words
categories:
    - Dictionary
---
words
     n 1: the words that are spoken; "I listened to his words very
          closely"
     2: the text of a popular song or musical-comedy number; "his
        compositions always started with the lyrics"; "he wrote
        both words and music"; "the song uses colloquial language"
        [syn: {lyric}, {language}]
     3: language that is spoken or written; "he has a gift for
        words"; "she put her thoughts into words"
     4: an angry dispute; "they had a quarrel"; "they had words"
        [syn: {quarrel}, {wrangle}, {row}, {run-in}, {dustup}]
     5: words making up the dialogue of a play; "the actor forgot
        his speech" [syn: {actor's line}, {speech}]
