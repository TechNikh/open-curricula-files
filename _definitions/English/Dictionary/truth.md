---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/truth
offline_file: ""
offline_thumbnail: ""
uuid: 699b3e4f-ead1-43e7-a37b-919840402fb0
updated: 1484310240
title: truth
categories:
    - Dictionary
---
truth
     n 1: a fact that has been verified; "at last he knew the truth";
          "the truth is the he didn't want to do it"
     2: conformity to reality or actuality; "they debated the truth
        of the proposition"; "the situation brought home to us the
        blunt truth of the military threat"; "he was famous for
        the truth of his portraits"; "he turned to religion in his
        search for eternal verities" [syn: {the true}, {verity}]
        [ant: {falsity}]
     3: a true statement; "he told the truth"; "he thought of
        answering with the truth but he knew they wouldn't believe
        it" [syn: {true statement}] [ant: {falsehood}]
     4: the quality of ...
