---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alexander_Macfarlane
offline_file: ""
offline_thumbnail: ""
uuid: af83f9b8-42e8-4ba6-8310-644ed9c2a071
updated: 1484309090
title: Alexander Macfarlane
tags:
    - Space analysis
    - Works
    - Notes and references
categories:
    - Geometers
---
Macfarlane was born in Blairgowrie, Scotland and studied at the University of Edinburgh. His doctoral thesis "The disruptive discharge of electricity"[1] reported on experimental results from the laboratory of Peter Guthrie Tait. In 1878 Macfarlane was elected a Fellow of the Royal Society of Edinburgh
