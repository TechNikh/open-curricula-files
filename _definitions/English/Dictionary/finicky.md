---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finicky
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415121
title: finicky
categories:
    - Dictionary
---
finicky
     adj : exacting especially about details; "a finicky eater"; "fussy
           about clothes"; "very particular about how her food was
           prepared" [syn: {finical}, {fussy}, {particular}]
