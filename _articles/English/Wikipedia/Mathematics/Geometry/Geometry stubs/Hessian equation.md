---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hessian_equation
offline_file: ""
offline_thumbnail: ""
uuid: b786fbf5-9cf1-4248-8e50-1c36027d8cc3
updated: 1484309362
title: Hessian equation
categories:
    - Geometry stubs
---
In mathematics, k-Hessian equations (or Hessian equations for short) are partial differential equations (PDEs) based on the Hessian matrix. More specifically, a Hessian equation is the k-trace, or the kth elementary symmetric polynomial of eigenvalues of the Hessian matrix. When k ≥ 2, the k-Hessian equation is a fully nonlinear partial differential equation.[1]
