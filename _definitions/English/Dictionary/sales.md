---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sales
offline_file: ""
offline_thumbnail: ""
uuid: 7676fcb1-a446-4e73-84d8-ad58a42f6719
updated: 1484310517
title: sales
categories:
    - Dictionary
---
sales
     n : income (at invoice values) received for goods and services
         over some given period of time [syn: {gross sales}, {gross
         revenue}]
