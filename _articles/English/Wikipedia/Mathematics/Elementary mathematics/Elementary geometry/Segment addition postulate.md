---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Segment_addition_postulate
offline_file: ""
offline_thumbnail: ""
uuid: 12a4578b-1fcd-4a69-869b-681a709f5fe3
updated: 1484308930
title: Segment addition postulate
categories:
    - Elementary geometry
---
In geometry, the segment addition postulate states that given 2 points A and C, a third point B lies on the line segment AC if and only if the distances between the points satisfy the equation AB + BC = AC. This is related to the triangle inequality, which states that AB + BC 
  
    
      
        ≥
      
    
    {\displaystyle \geq }
  
 AC with equality if and only if A, B, and C are collinear (on the same line). This in turn is equivalent to the proposition that the shortest distance between two points lies on a straight line.
