---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/china
offline_file: ""
offline_thumbnail: ""
uuid: 1c9620cb-8460-452e-8d7a-6b3fa898bf75
updated: 1484310249
title: china
categories:
    - Dictionary
---
China
     n 1: a communist nation that covers a vast territory in eastern
          Asia; the most populous country in the world [syn: {People's
          Republic of China}, {mainland China}, {Communist China},
           {Red China}, {PRC}]
     2: high quality porcelain originally made only in China
     3: a government on the island of Taiwan established in 1949 by
        Chiang Kai-shek after the conquest of mainland China by
        the communists led by Mao Zedong [syn: {Taiwan}, {Nationalist
        China}, {Republic of China}]
     4: dishware made of china [syn: {chinaware}]
