---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/filament
offline_file: ""
offline_thumbnail: ""
uuid: 8db601f4-663e-429f-95f3-a48a9ccef032
updated: 1484310198
title: filament
categories:
    - Dictionary
---
filament
     n 1: a very slender natural or synthetic fiber [syn: {fibril}, {strand}]
     2: the stalk of a stamen
     3: a threadlike anatomical structure or chainlike series of
        cells [syn: {filum}]
     4: a thin wire (usually tungsten) that is heated white hot by
        the passage of an electric current
