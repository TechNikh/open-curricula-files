---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phyllis_Chinn
offline_file: ""
offline_thumbnail: ""
uuid: 23270e53-d0c2-496a-bd16-5eed86ad373d
updated: 1484309461
title: Phyllis Chinn
categories:
    - Graph theorists
---
Phyllis Zweig Chinn (born September 26, 1941) is an American mathematician who holds a professorship in mathematics, women's studies, and teaching preparation at Humboldt State University in California. Her publications concern graph theory, mathematics education, and the history of women in mathematics.[1]
