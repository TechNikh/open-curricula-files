---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fran%C3%A7ois_Labourie'
offline_file: ""
offline_thumbnail: ""
uuid: 3293e5f8-ad95-4ba8-9044-6becd2694a5d
updated: 1484309091
title: François Labourie
categories:
    - Geometers
---
François Labourie (born 15 December 1960) is a French mathematician who has made various contributions to geometry, including pseudoholomorphic curves, Anosov diffeomorphism, and convex geometry. In a series of papers with Yves Benoist and Patrick Foulon, he solved a conjecture on Anosov's flows in compact contact manifolds.
