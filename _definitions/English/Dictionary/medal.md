---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/medal
offline_file: ""
offline_thumbnail: ""
uuid: 1ba51e74-c4ac-4238-912b-08e781b49427
updated: 1484310431
title: medal
categories:
    - Dictionary
---
medal
     n : an award for winning a championship or commemorating some
         other event [syn: {decoration}, {laurel wreath}, {medallion},
          {palm}, {ribbon}]
     [also: {medalling}, {medalled}]
