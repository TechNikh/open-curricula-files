---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/penetrate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484643061
title: penetrate
categories:
    - Dictionary
---
penetrate
     v 1: pass into or through, often by overcoming resistance; "The
          bullet penetrated her chest" [syn: {perforate}]
     2: come to understand [syn: {fathom}, {bottom}]
     3: become clear or enter one's consciousness or emotions; "It
        dawned on him that she had betrayed him"; "she was
        penetrated with sorrow" [syn: {click}, {get through}, {dawn},
         {come home}, {get across}, {sink in}, {fall into place}]
     4: enter a group or organization in order to spy on the
        members; "The student organization was infiltrated by a
        traitor" [syn: {infiltrate}]
     5: make one's way deeper into ar through; "The hikers did not
        manage to ...
