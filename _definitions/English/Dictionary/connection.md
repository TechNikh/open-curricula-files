---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/connection
offline_file: ""
offline_thumbnail: ""
uuid: 380d8c92-dc10-4bf0-af74-37b3c2245daa
updated: 1484310325
title: connection
categories:
    - Dictionary
---
connection
     n 1: a relation between things or events (as in the case of one
          causing the other or sharing features with it); "there
          was a connection between eating that pickle and having
          that nightmare" [syn: {connexion}, {connectedness}]
          [ant: {unconnectedness}]
     2: the state of being connected; "the connection between church
        and state is inescapable" [syn: {link}, {connectedness}]
        [ant: {disjunction}]
     3: an instrumentality that connects; "he soldered the
        connection"; "he didn't have the right connector between
        the amplifier and the speakers" [syn: {connexion}, {connector},
         {connecter}, ...
