---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/huge
offline_file: ""
offline_thumbnail: ""
uuid: 0b1ade7d-b016-4ca4-a16a-5666345cad0b
updated: 1484310234
title: huge
categories:
    - Dictionary
---
huge
     adj : unusually great in size or amount or degree or especially
           extent or scope; "huge government spending"; "huge
           country estates"; "huge popular demand for higher
           education"; "a huge wave"; "the Los Angeles aqueduct
           winds like an immense snake along the base of the
           mountains"; "immense numbers of birds"; "at vast (or
           immense) expense"; "the vast reaches of outer space";
           "the vast accumulation of knowledge...which we call
           civilization"- W.R.Inge [syn: {immense}, {vast}, {Brobdingnagian}]
