---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Chemical_reaction_network_theory
offline_file: ""
offline_thumbnail: ""
uuid: 94cd8d10-0f36-468a-8ff2-8fe96a8b37a3
updated: 1484309429
title: Chemical reaction network theory
tags:
    - History
    - Overview
    - Common assumptions
    - Types of results
    - Number of steady states
    - Stability of steady states
    - Persistence
    - Existence of stable periodic solutions
    - Network structure and dynamical properties
categories:
    - Mathematical chemistry
---
Chemical reaction network theory is an area of applied mathematics that attempts to model the behaviour of real world chemical systems. Since its foundation in the 1960s, it has attracted a growing research community, mainly due to its applications in biochemistry and theoretical chemistry. It has also attracted interest from pure mathematicians due to the interesting problems that arise from the mathematical structures involved.
