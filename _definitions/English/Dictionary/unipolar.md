---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unipolar
offline_file: ""
offline_thumbnail: ""
uuid: 9e98efdb-aa28-4b1e-87e5-f83999aa2f03
updated: 1484310176
title: unipolar
categories:
    - Dictionary
---
unipolar
     adj : having a single pole [ant: {bipolar}]
