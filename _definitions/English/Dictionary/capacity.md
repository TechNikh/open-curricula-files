---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/capacity
offline_file: ""
offline_thumbnail: ""
uuid: cd86222a-9842-4396-b05c-91ad25c00d19
updated: 1484310254
title: capacity
categories:
    - Dictionary
---
capacity
     n 1: ability to perform or produce [ant: {incapacity}]
     2: the susceptibility of something to a particular treatment;
        "the capability of a metal to be fused" [syn: {capability}]
     3: the amount that can be contained; "the gas tank has a
        capacity of 12 gallons" [syn: {content}]
     4: the maximum production possible; "the plant is working at 80
        per cent capacity"
     5: a specified function; "he was employed in the capacity of
        director"; "he should be retained in his present capacity
        at a higher salary"
     6: (computer science) the amount of information (in bytes) that
        can be stored on a disk drive; "the capacity of a ...
