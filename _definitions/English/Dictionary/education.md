---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/education
offline_file: ""
offline_thumbnail: ""
uuid: aafb3310-eccf-4b69-8521-a3a1f46c7877
updated: 1484310441
title: education
categories:
    - Dictionary
---
education
     n 1: the activities of educating or instructing or teaching;
          activities that impart knowledge or skill; "he received
          no formal education"; "our instruction was carefully
          programmed"; "good teaching is seldom rewarded" [syn: {instruction},
           {teaching}, {pedagogy}, {educational activity}]
     2: knowledge acquired by learning and instruction; "it was
        clear that he had a very broad education"
     3: the gradual process of acquiring knowledge; "education is a
        preparation for life"; "a girl's education was less
        important than a boy's"
     4: the profession of teaching (especially at a school or
        college or ...
