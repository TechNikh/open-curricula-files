---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/program
offline_file: ""
offline_thumbnail: ""
uuid: 428c2f5c-91cb-494c-86a0-52dcb7f929fa
updated: 1484310573
title: program
categories:
    - Dictionary
---
program
     n 1: a system of projects or services intended to meet a public
          need; "he proposed an elaborate program of public
          works"; "working mothers rely on the day care program"
          [syn: {programme}]
     2: a series of steps to be carried out or goals to be
        accomplished; "they drew up a six-step plan"; "they
        discussed plans for a new bond issue" [syn: {plan}, {programme}]
     3: (computer science) a sequence of instructions that a
        computer can interpret and execute; "the program required
        several hundred lines of code" [syn: {programme}, {computer
        program}, {computer programme}]
     4: an integrated course of academic ...
