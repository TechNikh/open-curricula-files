---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kurnool
offline_file: ""
offline_thumbnail: ""
uuid: 0c34e2ea-faeb-4209-aaf7-784e4992e83a
updated: 1484308447
title: Kurnool
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-21_-_Front_View_of_Kondareddy_Buruju.JPG
tags:
    - Etymology
    - History
    - Palaeolithic era
    - Early Feudal Era
    - Mughal princely state
    - British Raj
    - Post independence
    - Geography and Climate
    - Location
    - Cityscape
    - Climate
    - Demographics
    - Politics
    - Education
    - Transport
    - Roads
    - Railways
    - Airport
    - Economy
categories:
    - Uncategorized
---
Kurnool is a city, former princely state and the headquarters of Kurnool district in the Indian state of Andhra Pradesh.[5] The city is often referred as The Gateway of Rayalaseema.[6] It was the capital of Andhra State from 1 October 1953 to 31 October 1956. As of 2011[update] census, it is the fifth most populous city in the state with a population of 460,184.[1][2]
