---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sylver_coinage
offline_file: ""
offline_thumbnail: ""
uuid: 9d468d24-4179-440e-8043-75c813c83e4b
updated: 1484308531
title: Sylver coinage
tags:
    - Example
    - Analysis
categories:
    - Combinatorial game theory
---
Sylver Coinage is a mathematical game for two players, invented by John H. Conway. It is discussed in chapter 18 of Winning Ways for Your Mathematical Plays. This article summarizes that chapter.
