---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/checker
offline_file: ""
offline_thumbnail: ""
uuid: c5c65efe-af20-48ca-95e5-979150367742
updated: 1484310277
title: checker
categories:
    - Dictionary
---
checker
     n 1: an attendant who checks coats or baggage
     2: one who checks the correctness of something
     3: one of the flat round pieces used in playing checkers
     v 1: mark into squares or draw squares on; draw crossed lines on
          [syn: {check}, {chequer}]
     2: variegate with different colors, shades, or patterns [syn: {chequer}]
