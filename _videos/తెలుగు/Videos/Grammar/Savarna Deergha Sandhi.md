---
version: 1
type: video
provider: YouTube
id: hAljVrczVI0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 771bb325-d196-4333-a8c6-3c3a95208861
updated: 1486069628
title: Savarna Deergha Sandhi
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/hAljVrczVI0/default.jpg
    - https://i3.ytimg.com/vi/hAljVrczVI0/1.jpg
    - https://i3.ytimg.com/vi/hAljVrczVI0/2.jpg
    - https://i3.ytimg.com/vi/hAljVrczVI0/3.jpg
---
