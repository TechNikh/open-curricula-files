---
version: 1
type: video
provider: YouTube
id: UJtPe3LnOSU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 865b4d76-f77b-4c7c-814d-17f31b0daea5
updated: 1486069657
title: 'Human levitation - physics experiment'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/UJtPe3LnOSU/default.jpg
    - https://i3.ytimg.com/vi/UJtPe3LnOSU/1.jpg
    - https://i3.ytimg.com/vi/UJtPe3LnOSU/2.jpg
    - https://i3.ytimg.com/vi/UJtPe3LnOSU/3.jpg
---
