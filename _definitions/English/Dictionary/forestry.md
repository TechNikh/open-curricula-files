---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forestry
offline_file: ""
offline_thumbnail: ""
uuid: 1abe070d-b556-4d0c-8b08-6656816ddab4
updated: 1484310246
title: forestry
categories:
    - Dictionary
---
forestry
     n : the science of planting and caring for forests and the
         management of growing timber
