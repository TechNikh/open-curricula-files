---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tendency
offline_file: ""
offline_thumbnail: ""
uuid: 5ea9aa05-9523-48bb-a537-2ba5604da982
updated: 1484310403
title: tendency
categories:
    - Dictionary
---
tendency
     n 1: an attitude of mind especially one that favors one
          alternative over others; "he had an inclination to give
          up too easily"; "a tendency to be too strict" [syn: {inclination},
           {disposition}]
     2: an inclination to do something; "he felt leanings toward
        frivolity" [syn: {leaning}, {propensity}]
     3: a characteristic likelihood of or natural disposition toward
        a certain condition or character or effect; "the alkaline
        inclination of the local waters"; "fabric with a tendency
        to shrink" [syn: {inclination}]
     4: a general direction in which something tends to move; "the
        shoreward tendency of the ...
