---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Yang%E2%80%93Mills_theory'
offline_file: ""
offline_thumbnail: ""
uuid: c59573aa-acd2-4d54-bce6-8ea3a506e4bd
updated: 1484309322
title: Yang–Mills theory
tags:
    - History and theoretical description
    - Mathematical overview
    - Quantization of Yang–Mills theory
    - Propagators
    - Beta function and running coupling
    - Open problems
categories:
    - Symmetry
---
Yang–Mills theory is a gauge theory based on the SU(N) group, or more generally any compact, semi-simple Lie group. Yang–Mills theory seeks to describe the behavior of elementary particles using these non-Abelian Lie groups and is at the core of the unification of the electromagnetic and weak forces (i.e. U(1) × SU(2)) as well as quantum chromodynamics, the theory of the strong force (based on SU(3)). Thus it forms the basis of our understanding of the Standard Model of particle physics.
