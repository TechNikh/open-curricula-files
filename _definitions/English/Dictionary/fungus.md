---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fungus
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464262
title: fungus
categories:
    - Dictionary
---
fungus
     n : a parasitic plant lacking chlorophyll and leaves and true
         stems and roots and reproducing by spores
     [also: {fungi} (pl)]
