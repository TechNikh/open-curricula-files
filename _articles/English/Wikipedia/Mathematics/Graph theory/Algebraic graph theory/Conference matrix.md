---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conference_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 52227788-6ed4-47ff-83f4-af40a878d9f9
updated: 1484309371
title: Conference matrix
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Conference_matrix_2-port.svg.png
tags:
    - Symmetric conference matrices
    - Example
    - Antisymmetric conference matrices
    - Generalizations
    - Telephone conference circuits
    - Notes
categories:
    - Algebraic graph theory
---
In mathematics, a conference matrix (also called a C-matrix) is a square matrix C with 0 on the diagonal and +1 and −1 off the diagonal, such that CTC is a multiple of the identity matrix I. Thus, if the matrix has order n, CTC = (n−1)I. Some authors use a more general definition, which requires there to be a single 0 in each row and column but not necessarily on the diagonal.[1][2]
