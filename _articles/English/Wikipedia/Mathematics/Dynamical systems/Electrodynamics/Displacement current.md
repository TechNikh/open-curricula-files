---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Displacement_current
offline_file: ""
offline_thumbnail: ""
uuid: 0ab96a5a-0e00-4564-bec3-210a4f36d71b
updated: 1484308749
title: Displacement current
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Current_continuity_in_capacitor.JPG
tags:
    - Explanation
    - Isotropic dielectric case
    - Necessity
    - "Generalizing Ampère's circuital law"
    - Current in capacitors
    - Mathematical formulation
    - Wave propagation
    - History and interpretation
    - "Maxwell's papers"
categories:
    - Electrodynamics
---
In electromagnetism, displacement current is a quantity appearing in Maxwell's equations that is defined in terms of the rate of change of electric displacement field. Displacement current has the units of electric current density, and it has an associated magnetic field just as actual currents do. However it is not an electric current of moving charges, but a time-varying electric field. In physical materials (as opposed to vacuum), there is also a contribution from the slight motion of charges bound in atoms, dielectric polarization.
