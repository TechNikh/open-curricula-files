---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/presbyopia
offline_file: ""
offline_thumbnail: ""
uuid: 9dbd3578-8482-4691-bbd6-d325d074e2bf
updated: 1484310156
title: presbyopia
categories:
    - Dictionary
---
presbyopia
     n : farsightedness resulting from a reduced ability to focus
         caused by loss of elasticity of the crystalline lens with
         age
