---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ponder
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484552282
title: ponder
categories:
    - Dictionary
---
ponder
     v : reflect deeply on a subject; "I mulled over the events of
         the afternoon"; "philosophers have speculated on the
         question of God for thousands of years"; "The scientist
         must stop to observe and start to excogitate" [syn: {chew
         over}, {think over}, {meditate}, {excogitate}, {contemplate},
          {muse}, {reflect}, {mull}, {mull over}, {ruminate}, {speculate}]
