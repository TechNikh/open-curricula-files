---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alongside
offline_file: ""
offline_thumbnail: ""
uuid: 7ff653b5-98be-428c-ad73-95e8ad050399
updated: 1484310587
title: alongside
categories:
    - Dictionary
---
alongside
     adv : side by side; "anchored close aboard another ship" [syn: {aboard}]
