---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Matroid_minor
offline_file: ""
offline_thumbnail: ""
uuid: 5d7ee55a-e711-4cb1-91db-0b134dd5540b
updated: 1484309413
title: Matroid minor
tags:
    - Definitions
    - Forbidden matroid characterizations
    - Branchwidth
    - Well-quasi-ordering
    - Matroid decompositions
    - Algorithms and Complexity
    - Notes
categories:
    - Graph minor theory
---
In the mathematical theory of matroids, a minor of a matroid M is another matroid N that is obtained from M by a sequence of restriction and contraction operations. Matroid minors are closely related to graph minors, and the restriction and contraction operations by which they are formed correspond to edge deletion and edge contraction operations in graphs. The theory of matroid minors leads to structural decompositions of matroids, and characterizations of matroid families by forbidden minors, analogous to the corresponding theory in graphs.
