---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Traveler%27s_dilemma'
offline_file: ""
offline_thumbnail: ""
uuid: 8eba67b7-49bd-41c3-a37e-d445e63058fe
updated: 1484309055
title: "Traveler's dilemma"
tags:
    - Formulation
    - Analysis
    - Experimental results
    - Variation
    - Payoff matrix
categories:
    - Non-cooperative games
---
In game theory, the traveler's dilemma (sometimes abbreviated TD) is a type of non-zero-sum game in which two players attempt to maximize their own payoff, without any concern for the other player's payoff.
