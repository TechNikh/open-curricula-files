---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/substantiated
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484412661
title: substantiated
categories:
    - Dictionary
---
substantiated
     adj : supported or established by evidence or proof; "the
           substantiated charges"; "a verified case" [syn: {corroborated},
            {verified}]
