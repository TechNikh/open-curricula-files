---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/widely
offline_file: ""
offline_thumbnail: ""
uuid: 82ce8d10-56f1-4879-80c1-a30156ba408d
updated: 1484310411
title: widely
categories:
    - Dictionary
---
widely
     adv 1: to a great degree; "her work is widely known"
     2: to or over a great extent or range; far; "wandered wide
        through many lands"; "he traveled widely" [syn: {wide}]
     3: so as to leave much space or distance between; "widely
        separated"
