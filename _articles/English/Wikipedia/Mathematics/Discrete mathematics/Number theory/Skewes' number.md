---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Skewes%27_number'
offline_file: ""
offline_thumbnail: ""
uuid: a076e629-4d94-412c-9754-8421a6f66536
updated: 1484308664
title: "Skewes' number"
tags:
    - "Skewes' numbers"
    - More recent estimates
    - "Riemann's formula"
categories:
    - Number theory
---
In number theory, Skewes' number is any of several extremely large numbers used by the South African mathematician Stanley Skewes as upper bounds for the smallest natural number x for which
