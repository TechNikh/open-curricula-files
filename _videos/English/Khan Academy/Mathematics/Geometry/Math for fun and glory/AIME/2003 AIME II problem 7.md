---
version: 1
type: video
provider: YouTube
id: jpKjXtywTlQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Math%20for%20fun%20and%20glory/AIME/2003%20AIME%20II%20problem%207.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 76d8ddc3-68ab-404e-90f5-112502b8412b
updated: 1486069577
title: 2003 AIME II problem 7
categories:
    - AIME
thumbnail_urls:
    - https://i3.ytimg.com/vi/jpKjXtywTlQ/default.jpg
    - https://i3.ytimg.com/vi/jpKjXtywTlQ/1.jpg
    - https://i3.ytimg.com/vi/jpKjXtywTlQ/2.jpg
    - https://i3.ytimg.com/vi/jpKjXtywTlQ/3.jpg
---
Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/math/math-for-fun-and-glory/aime/2003-aime/v/2003-aime-ii-problem-8?utm_source=YT&utm_medium=Desc&utm_campaign=mathforfunandglory

Missed the previous lesson? https://www.khanacademy.org/math/math-for-fun-and-glory/aime/2003-aime/v/2003-aime-ii-problem-6?utm_source=YT&utm_medium=Desc&utm_campaign=mathforfunandglory

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Math For Fun and Glory channel: https://www.youtube.com/channel/UCrHuSfezaER8svQZiec1mIQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
