---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Riemannian_circle
offline_file: ""
offline_thumbnail: ""
uuid: 76a053ac-57e4-43ea-9682-6a5bb1287330
updated: 1484309273
title: Riemannian circle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sphere_halve.png
categories:
    - Metric geometry
---
In metric space theory and Riemannian geometry, the Riemannian circle (named after Bernhard Riemann) is a great circle equipped with its great-circle distance. In more detail, it is the circle equipped with its intrinsic Riemannian metric of a compact 1-dimensional manifold of total length 2π, as opposed to the extrinsic metric obtained by restriction of the Euclidean metric to the unit circle in the plane. Thus, the distance between a pair of points is defined to be the length of the shorter of the two arcs into which the circle is partitioned by the two points.
