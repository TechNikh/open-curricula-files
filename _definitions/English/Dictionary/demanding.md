---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demanding
offline_file: ""
offline_thumbnail: ""
uuid: 1e8a8280-5303-47a5-a8fd-9470af91908c
updated: 1484310529
title: demanding
categories:
    - Dictionary
---
demanding
     adj : requiring more than usually expected or thought due;
           especially great patience and effort and skill; "found
           the job very demanding"; "a baby can be so demanding"
           [ant: {undemanding}]
