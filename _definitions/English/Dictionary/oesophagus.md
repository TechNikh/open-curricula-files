---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oesophagus
offline_file: ""
offline_thumbnail: ""
uuid: c1328b96-54ef-4227-9d60-b0baf7b94b54
updated: 1484310337
title: oesophagus
categories:
    - Dictionary
---
oesophagus
     n : the passage between the pharynx and the stomach [syn: {esophagus},
          {gorge}, {gullet}]
