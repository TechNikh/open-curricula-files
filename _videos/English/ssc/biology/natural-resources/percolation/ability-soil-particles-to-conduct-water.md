---
title: Ability of Sand, Silt, and Clay Particles to Conduct Water
youtube_id: cC7SPH2KEY4
tags:
    - Sandy
    - Loamy
    - Experiment
categories:
    - Percolation
---
