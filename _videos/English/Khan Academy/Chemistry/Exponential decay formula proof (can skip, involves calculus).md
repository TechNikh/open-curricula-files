---
version: 1
type: video
provider: YouTube
id: H7nrVDV8ahc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Exponential%20decay%20formula%20proof%20%28can%20skip%2C%20involves%20calculus%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cfa67dad-1d67-4ed9-ba00-7d321fb55698
updated: 1486069585
title: >
    Exponential decay formula proof (can skip, involves
    calculus)
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/H7nrVDV8ahc/default.jpg
    - https://i3.ytimg.com/vi/H7nrVDV8ahc/1.jpg
    - https://i3.ytimg.com/vi/H7nrVDV8ahc/2.jpg
    - https://i3.ytimg.com/vi/H7nrVDV8ahc/3.jpg
---
Showing that N(t)=Ne^(-kt) describes the amount of a radioactive substance we have at time T. For students with background in Calculus. Not necessary for intro chemistry class.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/nuclear-chemistry/radioactive-decay/v/introduction-to-exponential-decay?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/nuclear-chemistry/radioactive-decay/v/half-life-of-radioactive-isotopes?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
