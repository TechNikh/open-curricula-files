---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preservation
offline_file: ""
offline_thumbnail: ""
uuid: 7477a910-059a-4f8e-bccc-6380e2888334
updated: 1484310563
title: preservation
categories:
    - Dictionary
---
preservation
     n 1: the activity of protecting something from loss or danger
          [syn: {saving}]
     2: the condition of being (well or ill) preserved
     3: a process that saves organic substances from decay
     4: an occurrence of improvement by virtue of preventing loss or
        injury or other change [syn: {conservation}]
