---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/survive
offline_file: ""
offline_thumbnail: ""
uuid: c928d49c-cc48-49fc-b554-8c6d0631d437
updated: 1484310290
title: survive
categories:
    - Dictionary
---
survive
     v 1: continue to live; endure or last; "We went without water and
          food for 3 days"; "These superstitions survive in the
          backwaters of America"; "The racecar driver lived
          through several very serious accidents" [syn: {last}, {live},
           {live on}, {go}, {endure}, {hold up}, {hold out}]
     2: continue in existence after (an adversity, etc.); "He
        survived the cancer against all odds" [syn: {pull through},
         {pull round}, {come through}, {make it}] [ant: {succumb}]
     3: support oneself; "he could barely exist on such a low wage";
        "Can you live on $2000 a month in New York City?"; "Many
        people in the world have ...
