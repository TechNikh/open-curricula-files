---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ply_(game_theory)
offline_file: ""
offline_thumbnail: ""
uuid: d564016f-65df-4199-a1b5-7c037bf51ead
updated: 1484309006
title: Ply (game theory)
categories:
    - Game artificial intelligence
---
In two-player sequential games, a ply refers to one turn taken by one of the players. The word is used to clarify what is meant when one might otherwise say "turn".
