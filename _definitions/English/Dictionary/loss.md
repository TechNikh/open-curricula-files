---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loss
offline_file: ""
offline_thumbnail: ""
uuid: af8d5afa-406e-4723-a4c1-d5907850e495
updated: 1484310264
title: loss
categories:
    - Dictionary
---
loss
     n 1: the act of losing; "everyone expected him to win so his loss
          was a shock"
     2: something that is lost; "the car was a total loss"; "loss of
        livestock left the rancher bankrupt"
     3: the amount by which the cost of a business exceeds its
        revenue; "the company operated at a loss last year"; "the
        company operated in the red last year" [syn: {red ink}, {red}]
        [ant: {gain}]
     4: gradual decline in amount or activity; "weight loss"; "a
        serious loss of business"
     5: the disadvantage that results from losing something; "his
        loss of credibility led to his resignation"; "losing him
        is no great deprivation" ...
