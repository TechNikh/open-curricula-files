---
version: 1
type: article
id: https://en.wikipedia.org/wiki/CPCTC
offline_file: ""
offline_thumbnail: ""
uuid: 4746555a-ed93-485e-82d5-f84ff3d3e9ec
updated: 1484309111
title: CPCTC
categories:
    - Theorems in geometry
---
In geometry, "Corresponding parts of congruent triangles are congruent" (CPCTC) is a succinct statement of a theorem regarding congruent trigonometry, defined as triangles either of which is an isometry of the other.[1] CPCTC states that if two or more triangles are congruent, then all of their corresponding angles and sides are congruent as well. CPCTC is useful in proving various theorems about triangles and other polygons.[2]
