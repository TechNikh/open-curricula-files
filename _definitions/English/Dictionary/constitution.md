---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/constitution
offline_file: ""
offline_thumbnail: ""
uuid: 34464f7a-9e54-45a2-96ca-41fc2d43eb27
updated: 1484310299
title: constitution
categories:
    - Dictionary
---
constitution
     n 1: law determining the fundamental political principles of a
          government [syn: {fundamental law}, {organic law}]
     2: the act of forming something; "the constitution of a PTA
        group last year"; "it was the establishment of his
        reputation"; "he still remembers the organization of the
        club" [syn: {establishment}, {formation}, {organization},
        {organisation}]
     3: the way in which someone or something is composed [syn: {composition},
         {makeup}]
     4: United States 44-gun frigate that was one of the first three
        naval ships built by the United States; it won brilliant
        victories over British frigates during ...
