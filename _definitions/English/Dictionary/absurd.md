---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/absurd
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484511301
title: absurd
categories:
    - Dictionary
---
absurd
     adj 1: inconsistent with reason or logic or common sense; "the
            absurd predicament of seeming to argue that virtue is
            highly desirable but intensely unpleasant"- Walter
            Lippman [syn: {unreasonable}]
     2: completely devoid of wisdom or good sense; "the absurd
        excuse that the dog ate his homework"; "that's a cockeyed
        idea"; "ask a nonsensical question and get a nonsensical
        answer"; "a contribution so small as to be laughable"; "it
        is ludicrous to call a cottage a mansion"; "a preposterous
        attempt to turn back the pages of history"; "her conceited
        assumption of universal interest in her rather ...
