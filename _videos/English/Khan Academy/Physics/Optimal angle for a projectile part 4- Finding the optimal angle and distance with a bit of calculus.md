---
version: 1
type: video
provider: YouTube
id: snw0BrCBQYQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Optimal%20angle%20for%20a%20projectile%20part%204-%20Finding%20the%20optimal%20angle%20and%20distance%20with%20a%20bit%20of%20calculus.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fa00b668-deaa-4f42-a29b-a82533fa906d
updated: 1486069589
title: 'Optimal angle for a projectile part 4- Finding the optimal angle and distance with a bit of calculus'
tags:
    - Optimal
    - angle
    - for
    - projectile
    - part
    - Finding
    - THE
    - Optimal
    - and
    - distance
    - with
    - bit
    - Of
    - calculus
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/snw0BrCBQYQ/default.jpg
    - https://i3.ytimg.com/vi/snw0BrCBQYQ/1.jpg
    - https://i3.ytimg.com/vi/snw0BrCBQYQ/2.jpg
    - https://i3.ytimg.com/vi/snw0BrCBQYQ/3.jpg
---
Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/forces-newtons-laws/newtons-laws-of-motion/v/newton-s-1st-law-of-motion?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/two-dimensional-motion/optimal-projectile-angle/v/optimal-angle-for-a-projectile-part-3-horizontal-distance-as-a-function-of-angle-and-speed?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
