---
version: 1
type: video
provider: YouTube
id: 2brXTMJFWCQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ab05aa44-6e68-4548-9a4e-d4e6c781a22f
updated: 1486069658
title: Steve Spangler on Ellen (full)
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/2brXTMJFWCQ/default.jpg
    - https://i3.ytimg.com/vi/2brXTMJFWCQ/1.jpg
    - https://i3.ytimg.com/vi/2brXTMJFWCQ/2.jpg
    - https://i3.ytimg.com/vi/2brXTMJFWCQ/3.jpg
---
