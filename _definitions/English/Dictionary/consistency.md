---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consistency
offline_file: ""
offline_thumbnail: ""
uuid: 66dce41c-f814-4d7e-9667-8c1b86983eb1
updated: 1484310328
title: consistency
categories:
    - Dictionary
---
consistency
     n 1: the property of holding together and retaining its shape;
          "when the dough has enough consistency it is ready to
          bake" [syn: {consistence}, {body}]
     2: a harmonious uniformity or agreement among things or parts
        [syn: {consistence}] [ant: {inconsistency}]
     3: logical coherence and accordance with the facts; "a rambling
        argument that lacked any consistency"
     4: (logic) an attribute of a logical system that is so
        constituted that none of the propositions deducible from
        the axioms contradict one another
