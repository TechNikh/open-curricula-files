---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equidistributed_sequence
offline_file: ""
offline_thumbnail: ""
uuid: a8adf751-8145-4c4f-93bb-127a03b509d6
updated: 1484308775
title: Equidistributed sequence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Van_der_Corput_sequence_999.svg.png
tags:
    - Definition
    - Discrepancy
    - Riemann integral criterion for equidistribution
    - Equidistribution modulo 1
    - Examples
    - "Weyl's criterion"
    - Generalizations
    - Example of usage
    - "van der Corput's difference theorem"
    - Metric theorems
    - Well-distributed sequence
    - >
        Sequences equidistributed with respect to an arbitrary
        measure
categories:
    - Ergodic theory
---
In mathematics, a sequence {s1, s2, s3, ...} of real numbers is said to be equidistributed, or uniformly distributed, if the proportion of terms falling in a subinterval is proportional to the length of that interval. Such sequences are studied in Diophantine approximation theory and have applications to Monte Carlo integration.
