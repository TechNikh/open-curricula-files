---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circular_coloring
offline_file: ""
offline_thumbnail: ""
uuid: dc6861f1-debf-488a-9086-2331918a5d66
updated: 1484309379
title: Circular coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-J5_circular_color.svg.png
categories:
    - Graph coloring
---
In graph theory, circular coloring may be viewed as a refinement of usual graph coloring. The circular chromatic number of a graph 
  
    
      
        G
      
    
    {\displaystyle G}
  
, denoted 
  
    
      
        
          χ
          
            c
          
        
        (
        G
        )
      
    
    {\displaystyle \chi _{c}(G)}
  
 can be given by any of the following definitions, all of which are equivalent (for finite graphs).
