---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oath
offline_file: ""
offline_thumbnail: ""
uuid: eb3ac544-5326-42a9-b39b-ea4ce460af06
updated: 1484310565
title: oath
categories:
    - Dictionary
---
oath
     n 1: profane or obscene expression usually of surprise or anger;
          "expletives were deleted" [syn: {curse}, {curse word}, {expletive},
           {swearing}, {swearword}, {cuss}]
     2: a commitment to tell the truth (especially in a court of
        law); to lie under oath is to become subject to
        prosecution for perjury [syn: {swearing}]
     3: a solemn promise, usually invoking a divine witness,
        regarding your future acts or behavior; "they took an oath
        of allegience"
