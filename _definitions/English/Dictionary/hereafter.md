---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hereafter
offline_file: ""
offline_thumbnail: ""
uuid: c2fde961-a171-469d-8eb1-360be3703716
updated: 1484310178
title: hereafter
categories:
    - Dictionary
---
hereafter
     n 1: life after death [syn: {afterlife}]
     2: the time yet to come [syn: {future}, {futurity}, {time to
        come}] [ant: {past}]
     adv 1: in a subsequent part of this document or statement or matter
            etc.; "the landlord demises unto the tenant the
            premises hereinafter called the demised premises";
            "the terms specified hereunder" [syn: {hereinafter}, {hereunder}]
     2: in a future life or state; "hope to win salvation hereafter"
     3: following this in time or order or place; after this;
        "hereafter you will no longer receive an allowance"
