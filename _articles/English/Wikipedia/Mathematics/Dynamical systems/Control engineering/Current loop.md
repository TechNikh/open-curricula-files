---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Current_loop
offline_file: ""
offline_thumbnail: ""
uuid: bc3137a3-0df3-4fa9-b92d-56360372c50b
updated: 1484308732
title: Current loop
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Analogue_control_loop_evolution.png
tags:
    - Process control 4-20 mA loops
    - Active and passive devices
    - Evolution of analogue control signals
    - Long circuits
    - Discrete control
    - Two-way radio use
categories:
    - Control engineering
---
In electrical signalling an analog current loop is used where a device must be monitored or controlled remotely over a pair of conductors. Only one current level can be present at any time.
