---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/damages
offline_file: ""
offline_thumbnail: ""
uuid: a833a0aa-5b21-42ef-9381-268cbd0364e1
updated: 1484310200
title: damages
categories:
    - Dictionary
---
damages
     n : a sum of money paid in compensation for loss or injury [syn:
          {amends}, {indemnity}, {indemnification}, {restitution},
          {redress}]
