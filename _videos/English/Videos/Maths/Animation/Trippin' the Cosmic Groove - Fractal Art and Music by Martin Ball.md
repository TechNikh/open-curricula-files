---
version: 1
type: video
provider: YouTube
id: DPUsRkvaRJ4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bfb803e8-909d-416a-9feb-69ef33a2f321
updated: 1486069647
title: "Trippin' the Cosmic Groove - Fractal Art and Music by Martin Ball"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/DPUsRkvaRJ4/default.jpg
    - https://i3.ytimg.com/vi/DPUsRkvaRJ4/1.jpg
    - https://i3.ytimg.com/vi/DPUsRkvaRJ4/2.jpg
    - https://i3.ytimg.com/vi/DPUsRkvaRJ4/3.jpg
---
