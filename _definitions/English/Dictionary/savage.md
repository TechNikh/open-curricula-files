---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/savage
offline_file: ""
offline_thumbnail: ""
uuid: 08c93e9b-c241-4e7a-8672-94501bdef568
updated: 1484310583
title: savage
categories:
    - Dictionary
---
savage
     adj 1: (of persons or their actions) able or disposed to inflict
            pain or suffering; "a barbarous crime"; "brutal
            beatings"; "cruel tortures"; "Stalin's roughshod
            treatment of the kulaks"; "a savage slap"; "vicious
            kicks" [syn: {barbarous}, {brutal}, {cruel}, {fell}, {roughshod},
             {vicious}]
     2: wild and menacing; "a ferocious dog" [syn: {feral}, {ferine}]
     3: without civilizing influences; "barbarian invaders";
        "barbaric practices"; "a savage people"; "fighting is
        crude and uncivilized especially if the weapons are
        efficient"-Margaret Meade; "wild tribes" [syn: {barbarian},
         ...
