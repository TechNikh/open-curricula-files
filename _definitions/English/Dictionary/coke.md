---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coke
offline_file: ""
offline_thumbnail: ""
uuid: b8bc8045-e51a-4bdd-8c18-3f0b17ef7cc9
updated: 1484310411
title: coke
categories:
    - Dictionary
---
coke
     n 1: carbon fuel produced by distillation of coal
     2: Coca Cola is a trademarked cola [syn: {Coca Cola}]
     3: street names for cocaine [syn: {blow}, {nose candy}, {snow},
         {C}]
     v : become coke; "petroleum oils coke after distillation"
