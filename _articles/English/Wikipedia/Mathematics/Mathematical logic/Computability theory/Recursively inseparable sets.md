---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Recursively_inseparable_sets
offline_file: ""
offline_thumbnail: ""
uuid: ab51dfbb-e8a2-4f73-bb70-1c15774f0632
updated: 1484309490
title: Recursively inseparable sets
categories:
    - Computability theory
---
In computability theory, recursively inseparable sets are pairs of sets of natural numbers that cannot be "separated" with a recursive set (Monk 1976, p. 100). These sets arise in the study of computability theory itself, particularly in relation to Π0
1 classes. Recursively inseparable sets also arise in the study of Gödel's incompleteness theorem.
