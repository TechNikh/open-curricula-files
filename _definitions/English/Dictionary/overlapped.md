---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overlapped
offline_file: ""
offline_thumbnail: ""
uuid: daaf478f-854a-4f35-9869-919b231f3a41
updated: 1484310413
title: overlapped
categories:
    - Dictionary
---
overlap
     n 1: a representation of common ground between theories or
          phenomena; "there was no overlap between their
          proposals" [syn: {convergence}, {intersection}]
     2: the property of partial coincidence in time
     3: a flap that lies over another part; "the lap of the shingles
        should be at least ten inches" [syn: {lap}]
     v 1: coincide partially or wholly; "Our vacations overlap"
     2: extend over and cover a part of; "The roofs of the houses
        overlap in this crowded city"
     [also: {overlapping}, {overlapped}]
