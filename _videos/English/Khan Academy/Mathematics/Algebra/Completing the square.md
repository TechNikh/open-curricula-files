---
version: 1
type: video
provider: YouTube
id: gzm-uhj06q8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Completing%20the%20square.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 672516d2-1010-4e59-b15b-8a622939693f
updated: 1486069573
title: Completing the square
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/gzm-uhj06q8/default.jpg
    - https://i3.ytimg.com/vi/gzm-uhj06q8/1.jpg
    - https://i3.ytimg.com/vi/gzm-uhj06q8/2.jpg
    - https://i3.ytimg.com/vi/gzm-uhj06q8/3.jpg
---
Solving a quadratic by completing the square.
