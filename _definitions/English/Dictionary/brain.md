---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brain
offline_file: ""
offline_thumbnail: ""
uuid: 822b9bc2-6dc6-4711-9433-6bdfdce8c644
updated: 1484310355
title: brain
categories:
    - Dictionary
---
brain
     n 1: that part of the central nervous system that includes all
          the higher nervous centers; enclosed within the skull;
          continuous with the spinal cord [syn: {encephalon}]
     2: mental ability; "he's got plenty of brains but no common
        sense" [syn: {brainpower}, {learning ability}, {mental
        capacity}, {mentality}, {wit}]
     3: that which is responsible for one's thoughts and feelings;
        the seat of the faculty of reason; "his mind wandered"; "I
        couldn't get his words out of my head" [syn: {mind}, {head},
         {psyche}, {nous}]
     4: someone who has exceptional intellectual ability and
        originality; "Mozart was a child ...
