---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instructions
offline_file: ""
offline_thumbnail: ""
uuid: 8430a11f-5bda-4e26-8e1c-787531436ff3
updated: 1484310527
title: instructions
categories:
    - Dictionary
---
instructions
     n : a manual usually accompanying a technical device and
         explaining how to install or operate it [syn: {instruction
         manual}, {book of instructions}, {operating instructions}]
