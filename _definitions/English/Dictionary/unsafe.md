---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unsafe
offline_file: ""
offline_thumbnail: ""
uuid: 55f439a7-7f38-4e03-9c0d-0586d3774d97
updated: 1484310539
title: unsafe
categories:
    - Dictionary
---
unsafe
     adj 1: lacking in security or safety; "his fortune was increasingly
            insecure"; "an insecure future" [syn: {insecure}]
            [ant: {secure}]
     2: involving or causing danger or risk; liable to hurt or harm;
        "a dangerous criminal"; "a dangerous bridge";
        "unemployment reached dangerous proportions" [syn: {dangerous}]
        [ant: {safe}]
     3: not safe from attack [syn: {insecure}]
