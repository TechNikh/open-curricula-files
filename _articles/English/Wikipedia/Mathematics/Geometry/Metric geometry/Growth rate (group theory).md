---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Growth_rate_(group_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 1725f39c-e179-4b54-8dd7-0f05a0b1a8ba
updated: 1484309268
title: Growth rate (group theory)
tags:
    - Definition
    - Polynomial and exponential growth
    - Examples
categories:
    - Metric geometry
---
In group theory, the growth rate of a group with respect to a symmetric generating set describes the size of balls in the group. Every element in the group can be written as a product of generators, and the growth rate counts the number of elements that can be written as a product of length n.
