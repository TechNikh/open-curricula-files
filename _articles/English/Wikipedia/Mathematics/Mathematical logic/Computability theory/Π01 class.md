---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%CE%A001_class'
offline_file: ""
offline_thumbnail: ""
uuid: ee14c5bc-ffbb-48ea-bba0-7c1ddcad1495
updated: 1484309490
title: Π01 class
tags:
    - Definition
    - As effectively closed sets
    - Relationship with effective theories
categories:
    - Computability theory
---
In computability theory, a Π01 class is a subset of 2ω of a certain form. These classes are of interest as a technical tool within recursion theory and effective descriptive set theory. They are also used in the application of recursion theory to other branches of mathematics (Cenzer 1999, p. 39).
