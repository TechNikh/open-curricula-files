---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/differentiated
offline_file: ""
offline_thumbnail: ""
uuid: 9e75ab3d-e1d4-41d0-a645-0bbe88d87d38
updated: 1484310567
title: differentiated
categories:
    - Dictionary
---
differentiated
     adj 1: made different (especially in the course of development) or
            shown to be different; "the differentiated markings of
            butterflies"; "the regionally differentiated results"
            [ant: {undifferentiated}]
     2: exhibiting biological specialization; adapted during
        development to a specific function or environment
