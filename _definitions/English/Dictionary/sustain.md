---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sustain
offline_file: ""
offline_thumbnail: ""
uuid: bac601b9-eaee-4d24-bf0f-6425adee8b35
updated: 1484310262
title: sustain
categories:
    - Dictionary
---
sustain
     v 1: lengthen or extend in duration or space; "We sustained the
          diplomatic negociations as long as possible"; "prolong
          the treatment of the patient"; "keep up the good work"
          [syn: {prolong}, {keep up}]
     2: undergo (as of injuries and illnesses); "She suffered a
        fracture in the accident"; "He had an insulin shock after
        eating three candy bars"; "She got a bruise on her leg";
        "He got his arm broken in the scuffle" [syn: {suffer}, {have},
         {get}]
     3: provide with nourishment; "We sustained ourselves on bread
        and water"; "This kind of food is not nourishing for young
        children" [syn: {nourish}, ...
