---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oxide
offline_file: ""
offline_thumbnail: ""
uuid: 251e6773-0057-4f33-8f25-edae17978199
updated: 1484310369
title: oxide
categories:
    - Dictionary
---
oxide
     n : any compound of oxygen with another element or a radical
