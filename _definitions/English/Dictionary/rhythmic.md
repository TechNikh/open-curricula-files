---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rhythmic
offline_file: ""
offline_thumbnail: ""
uuid: 809a6ec5-23c6-45e8-a814-e9b35ac3df9c
updated: 1484310325
title: rhythmic
categories:
    - Dictionary
---
rhythmic
     adj : recurring with measured regularity; "the rhythmic chiming of
           church bells"- John Galsworthy; "rhythmical prose"
           [syn: {rhythmical}] [ant: {unrhythmical}]
