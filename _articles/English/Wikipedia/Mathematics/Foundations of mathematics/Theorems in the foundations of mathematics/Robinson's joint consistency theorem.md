---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Robinson%27s_joint_consistency_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: acd80a88-3de4-4326-a990-da231ea338fb
updated: 1484308999
title: "Robinson's joint consistency theorem"
categories:
    - Theorems in the foundations of mathematics
---
The classical formulation of Robinson's joint consistency theorem is as follows:
