---
version: 1
type: video
provider: YouTube
id: -u0mqFqpMNY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Partial%20derivatives%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1832b569-daf3-4203-8849-7b336394d184
updated: 1486069690
title: Partial derivatives 2
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/-u0mqFqpMNY/default.jpg
    - https://i3.ytimg.com/vi/-u0mqFqpMNY/1.jpg
    - https://i3.ytimg.com/vi/-u0mqFqpMNY/2.jpg
    - https://i3.ytimg.com/vi/-u0mqFqpMNY/3.jpg
---
