---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lowered
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484563921
title: lowered
categories:
    - Dictionary
---
lowered
     adj : below the surround or below the normal position; "with
           lowered eyes" [ant: {raised}]
