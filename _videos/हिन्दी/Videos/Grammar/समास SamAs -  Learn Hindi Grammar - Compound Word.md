---
version: 1
type: video
provider: YouTube
id: XZvWHItPOno
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 80070b6d-d2e4-450f-b648-3343f5639aa7
updated: 1486069628
title: 'समास SamAs -  Learn Hindi Grammar - Compound Word'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/XZvWHItPOno/default.jpg
    - https://i3.ytimg.com/vi/XZvWHItPOno/1.jpg
    - https://i3.ytimg.com/vi/XZvWHItPOno/2.jpg
    - https://i3.ytimg.com/vi/XZvWHItPOno/3.jpg
---
