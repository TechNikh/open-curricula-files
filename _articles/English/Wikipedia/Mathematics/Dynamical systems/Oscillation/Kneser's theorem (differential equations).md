---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kneser%27s_theorem_(differential_equations)'
offline_file: ""
offline_thumbnail: ""
uuid: b6777560-202d-413c-bcff-6cc6be60112b
updated: 1484308823
title: "Kneser's theorem (differential equations)"
tags:
    - Statement of the theorem
    - Example
    - Extensions
categories:
    - Oscillation
---
In mathematics, in the field of ordinary differential equations, the Kneser theorem, named after Adolf Kneser, provides criteria to decide whether a differential equation is oscillating or not.
