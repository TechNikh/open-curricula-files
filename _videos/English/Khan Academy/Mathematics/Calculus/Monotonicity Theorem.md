---
version: 1
type: video
provider: YouTube
id: WrEcQsa-1ME
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Monotonicity%20Theorem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 73332f83-c1f0-4267-ba27-d61ebc38be21
updated: 1486069689
title: Monotonicity Theorem
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/WrEcQsa-1ME/default.jpg
    - https://i3.ytimg.com/vi/WrEcQsa-1ME/1.jpg
    - https://i3.ytimg.com/vi/WrEcQsa-1ME/2.jpg
    - https://i3.ytimg.com/vi/WrEcQsa-1ME/3.jpg
---
