---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kill
offline_file: ""
offline_thumbnail: ""
uuid: 4adc713a-cfd4-45ec-8603-e2c3b5e839fc
updated: 1484310321
title: kill
categories:
    - Dictionary
---
kill
     n 1: the act of terminating a life [syn: {killing}, {putting to
          death}]
     2: the destruction of an enemy plane or ship or tank or
        missile; "the pilot reported two kills during the mission"
     v 1: cause to die; put to death, usually intentionally or
          knowingly; "This man killed several people when he tried
          to rob a bank"; "The farmer killed a pig for the
          holidays"
     2: thwart the passage of; "kill a motion"; "he shot down the
        student's proposal" [syn: {shoot down}, {defeat}, {vote
        down}, {vote out}]
     3: cause the death of, without intention; "She was killed in
        the collision of three cars"
     4: ...
