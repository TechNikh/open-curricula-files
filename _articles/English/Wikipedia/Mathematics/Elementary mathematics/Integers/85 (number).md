---
version: 1
type: article
id: https://en.wikipedia.org/wiki/85_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 5aa6bf01-13ec-4503-8be0-188f475e3135
updated: 1484308964
title: 85 (number)
tags:
    - In mathematics
    - In astronomy
    - In titles and names
    - In sports
    - In other fields
    - In military technology
categories:
    - Integers
---
