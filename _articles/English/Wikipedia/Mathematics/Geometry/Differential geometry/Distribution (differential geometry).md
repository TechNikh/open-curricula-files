---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Distribution_(differential_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 6fa0330b-9c65-4dfc-a150-e24ef80817b4
updated: 1484309216
title: Distribution (differential geometry)
tags:
    - Definition
    - Involutive distributions
    - Generalized distributions
categories:
    - Differential geometry
---
In differential geometry, a discipline within mathematics, a distribution is a subset of the tangent bundle of a manifold satisfying certain properties. Distributions are used to build up notions of integrability, and specifically of a foliation of a manifold.
