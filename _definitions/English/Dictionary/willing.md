---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/willing
offline_file: ""
offline_thumbnail: ""
uuid: 95d6eff4-2c25-4fb0-9b25-349d11004000
updated: 1484310561
title: willing
categories:
    - Dictionary
---
willing
     adj 1: disposed or inclined toward; "a willing participant";
            "willing helpers" [ant: {unwilling}]
     2: not brought about by coercion or force; "the confession was
        uncoerced" [syn: {uncoerced}, {unforced}]
     3: disposed or willing to comply; "someone amenable to
        persuasion"; "the spirit indeed is willing but the flesh
        is weak"- Matthew 26:41 [syn: {amenable}, {conformable}]
     n : the act of making a choice; "followed my father of my own
         volition" [syn: {volition}]
