---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lanczos_tensor
offline_file: ""
offline_thumbnail: ""
uuid: 9dbc3fb0-97ba-4e4d-abbd-722166de1040
updated: 1484309226
title: Lanczos tensor
tags:
    - Definition
    - Weyl–Lanczos equations
    - Wave equation
    - Example
categories:
    - Differential geometry
---
The Lanczos tensor or Lanczos potential is a rank 3 tensor in general relativity that generates the Weyl tensor.[1] It was first introduced by Cornelius Lanczos in 1949.[2] The theoretical importance of the Lanczos tensor is that it serves as the gauge field for the gravitational field in the same way that, by analogy, the electromagnetic four-potential generates the electromagnetic field.[3][4]
