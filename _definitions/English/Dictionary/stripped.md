---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stripped
offline_file: ""
offline_thumbnail: ""
uuid: ecd97f46-c028-41f2-aba1-826b0663d1e8
updated: 1484310571
title: stripped
categories:
    - Dictionary
---
strip
     n 1: a relatively long narrow piece of something; "he felt a flat
          strip of muscle"
     2: artifact consisting of a narrow flat piece of material [syn:
         {slip}]
     3: an airfield without normal airport facilities [syn: {airstrip},
         {flight strip}, {landing strip}]
     4: a sequence of drawings telling a story in a newspaper or
        comic book [syn: {comic strip}, {cartoon strip}]
     5: thin piece of wood or metal
     6: a form of erotic entertainment in which a dancer gradually
        undresses to music; "she did a strip right in front of
        everyone" [syn: {striptease}, {strip show}]
     v 1: take away possessions from someone; "The ...
