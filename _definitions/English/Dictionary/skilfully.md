---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/skilfully
offline_file: ""
offline_thumbnail: ""
uuid: d2935c3e-2f76-43d0-ba38-bd4c0565696f
updated: 1484310607
title: skilfully
categories:
    - Dictionary
---
skilfully
     adv : with skill; "fragments of a nearly complete jug, skillfully
           restored at the institute of archaeology" [syn: {skillfully}]
