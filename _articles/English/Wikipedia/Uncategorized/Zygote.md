---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fertilized_egg
offline_file: ""
offline_thumbnail: ""
uuid: b8fb3d8f-3baf-4163-9ab4-b5a23ced9a58
updated: 1484308366
title: Zygote
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray3.png
tags:
    - Fungi
    - Plants
    - Humans
    - In other species
    - In protozoa
categories:
    - Uncategorized
---
A zygote (from Greek ζυγωτός zygōtos "joined" or "yoked", from ζυγοῦν zygoun "to join" or "to yoke"),[1] is a eukaryotic cell formed by a fertilization event between two gametes. The zygote's genome is a combination of the DNA in each gamete, and contains all of the genetic information necessary to form a new individual. In multicellular organisms, the zygote is the earliest developmental stage. In single-celled organisms, the zygote can divide asexually by mitosis to produce identical offspring.
