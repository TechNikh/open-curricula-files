---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surf
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484411581
title: surf
categories:
    - Dictionary
---
surf
     n : waves breaking on the shore [syn: {breaker}, {breakers}]
     v 1: ride the waves of the sea with a surfboard; "Californians
          love to surf"
     2: look around casually and randomly, without seeking anything
        in particular; "browse a computer directory"; "surf the
        internet or the world wide web" [syn: {browse}]
     3: switch channels, on television [syn: {channel-surf}]
