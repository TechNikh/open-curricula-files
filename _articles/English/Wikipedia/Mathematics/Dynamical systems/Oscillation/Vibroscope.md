---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vibroscope
offline_file: ""
offline_thumbnail: ""
uuid: 78085fb1-8dfe-46c6-b8cc-ff29219c149a
updated: 1484308816
title: Vibroscope
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mechanical_Vibroscope.gif
categories:
    - Oscillation
---
For example, a primitive mechanical vibroscope consists of a vibrating object with a pointy end which leaves a wave trace on a smoked surface of a rotating cylinder.[3]
