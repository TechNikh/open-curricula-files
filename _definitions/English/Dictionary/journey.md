---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/journey
offline_file: ""
offline_thumbnail: ""
uuid: 6fa0d4f8-50bb-46e2-9a1d-da89b779106b
updated: 1484310357
title: journey
categories:
    - Dictionary
---
journey
     n : the act of traveling from one place to another [syn: {journeying}]
     v 1: undertake a journey or trip [syn: {travel}]
     2: travel upon or across; "travel the oceans" [syn: {travel}]
