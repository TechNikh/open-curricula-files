---
version: 1
type: video
provider: YouTube
id: MeVFZjT-ABM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Derivative%20applications/L%27Ho%CC%82pital%27s%20rule%20example%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bea2350f-c1a7-44e4-8927-215373204f19
updated: 1486069692
title: "L'Hôpital's rule example 3"
categories:
    - Derivative applications
thumbnail_urls:
    - https://i3.ytimg.com/vi/MeVFZjT-ABM/default.jpg
    - https://i3.ytimg.com/vi/MeVFZjT-ABM/1.jpg
    - https://i3.ytimg.com/vi/MeVFZjT-ABM/2.jpg
    - https://i3.ytimg.com/vi/MeVFZjT-ABM/3.jpg
---
