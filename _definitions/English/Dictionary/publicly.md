---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/publicly
offline_file: ""
offline_thumbnail: ""
uuid: f9eee510-f95f-41fa-88ed-365e24510b9d
updated: 1484310563
title: publicly
categories:
    - Dictionary
---
publicly
     adv 1: in a manner accessible to or observable by the public;
            openly; "she admitted publicly to being a communist"
            [syn: {publically}, {in public}] [ant: {privately}]
     2: by the public or the people generally; "publicly provided
        medical care"; "publicly financed schools" [ant: {privately}]
