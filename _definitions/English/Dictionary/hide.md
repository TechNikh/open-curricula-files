---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hide
offline_file: ""
offline_thumbnail: ""
uuid: 1c08fb35-c8be-4aad-8c9c-b64b06927dff
updated: 1484310445
title: hide
categories:
    - Dictionary
---
hide
     n 1: the dressed skin of an animal (especially a large animal)
          [syn: {fell}]
     2: body covering of a living animal [syn: {pelt}, {skin}]
     v 1: prevent from being seen or discovered; "Muslim women hide
          their faces"; "hide the money" [syn: {conceal}] [ant: {show}]
     2: be or go into hiding; keep out of sight, as for protection
        and safety; "Probably his horse would be close to where he
        was hiding"; "She is hiding out in a cabin in Montana"
        [syn: {hide out}]
     3: cover as if with a shroud; "The origins of this civilization
        are shrouded in mystery" [syn: {shroud}, {enshroud}, {cover}]
     4: make undecipherable or ...
