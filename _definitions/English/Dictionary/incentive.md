---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incentive
offline_file: ""
offline_thumbnail: ""
uuid: 1eb8ac3c-bc2c-4ad6-a99a-555385100dee
updated: 1484310603
title: incentive
categories:
    - Dictionary
---
incentive
     n 1: a positive motivational influence [syn: {inducement}, {motivator}]
          [ant: {disincentive}]
     2: an additional payment (or other remuneration) to employees
        as a means of increasing output [syn: {bonus}]
