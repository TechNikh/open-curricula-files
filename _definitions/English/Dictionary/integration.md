---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/integration
offline_file: ""
offline_thumbnail: ""
uuid: 3640108f-5023-49f9-bb4e-fc7b1d006c1a
updated: 1484310359
title: integration
categories:
    - Dictionary
---
integration
     n 1: the action of incorporating a racial or religious group into
          a community [syn: {integrating}, {desegregation}] [ant:
          {segregation}]
     2: the act of combining into an integral whole; "a
        consolidation of two corporations"; "after their
        consolidation the two bills were passed unanimously"; "the
        defendants asked for a consolidation of the actions
        against them" [syn: {consolidation}]
     3: an operation used in the calculus whereby the integral of a
        function is determined
