---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circuit_topology
offline_file: ""
offline_thumbnail: ""
uuid: 13abdf46-c001-46ca-9494-eaed614a9aa1
updated: 1484309275
title: Circuit topology
categories:
    - Molecular geometry
---
The circuit topology of a linear polymer refers to arrangement of its intra-molecular contacts. Examples of linear polymers with intra-molecular contacts are nucleic acids and proteins. For defining the circuit topology, contacts are defined depending on the context. For proteins with disulfide bonds, these bonds could be considered as contacts. In a context where beta-beta interactions in proteins are more relevant, these interactions are used to define the circuit topology. As such, circuit topology framework can be applied to a wide range of applications including protein folding and analysis of genome architecture.[1]
