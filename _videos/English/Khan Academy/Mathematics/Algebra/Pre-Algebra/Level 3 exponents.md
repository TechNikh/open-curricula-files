---
version: 1
type: video
provider: YouTube
id: aYE26a5E1iU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Level%203%20exponents.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5dd2b2d0-879d-45aa-83fa-fb8f5b93c6bd
updated: 1486069565
title: Level 3 exponents
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/aYE26a5E1iU/default.jpg
    - https://i3.ytimg.com/vi/aYE26a5E1iU/1.jpg
    - https://i3.ytimg.com/vi/aYE26a5E1iU/2.jpg
    - https://i3.ytimg.com/vi/aYE26a5E1iU/3.jpg
---
fractional exponents
