---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homogeneous_variety
offline_file: ""
offline_thumbnail: ""
uuid: e7a4d542-454c-4e1f-b3da-64374d8d42c9
updated: 1484309363
title: Homogeneous variety
categories:
    - Geometry stubs
---
In algebraic geometry, a homogeneous variety is an algebraic variety of the form G/P, G a linear algebraic group, P a parabolic subgroup. It is a smooth projective variety. If P is a Borel subgroup, it is usually called a flag variety.
