---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Human_disease_network
offline_file: ""
offline_thumbnail: ""
uuid: 6ce1eb2f-5692-478a-bdd8-f79f5c96c95b
updated: 1484309435
title: Human disease network
tags:
    - History
    - Properties
categories:
    - Network theory
---
A human disease network is a network of human disorders and diseases with reference to their genetic origins or other features. More specifically, it is the map of human diseases associations referring mostly to disease genes. For example, in a human disease network, two diseases are linked if they share at least one associated gene. A typical human disease network usually derives from bipartite networks which consist of both diseases and genes information. Additionally, some human disease networks use other features such as symptoms and proteins to associate diseases.
