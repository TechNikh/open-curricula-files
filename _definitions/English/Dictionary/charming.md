---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/charming
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484508301
title: charming
categories:
    - Dictionary
---
charming
     adj 1: pleasing or delighting; "endowed with charming manners"; "a
            charming little cottage"; "a charming personality"
     2: possessing or using or characteristic of or appropriate to
        supernatural powers; "charming incantations"; "magic signs
        that protect against adverse influence"; "a magical
        spell"; "'tis now the very witching time of night"-
        Shakespeare; "wizard wands"; "wizardly powers" [syn: {magic},
         {magical}, {sorcerous}, {witching(a)}, {wizard(a)}, {wizardly}]
