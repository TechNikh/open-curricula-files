---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coated
offline_file: ""
offline_thumbnail: ""
uuid: d8aa9578-559a-4604-80a0-99b77bb88bad
updated: 1484310335
title: coated
categories:
    - Dictionary
---
coated
     adj 1: having a coating; covered with an outer layer or film; often
            used in combination; "coated paper has a smooth
            polished coating especially suitable for halftone
            printing"; "sugar-coated pills" [ant: {uncoated}]
     2: having or dressed in a coat
