---
title: Q&A with SPB- What is ICAN?
youtube_id: hvSTdWNdzd4
tags:
    - YouTube Editor
    - cesarean
    - Cesarean birth
    - ICAN
    - International Cesarean Awareness Network
    - Cesarean Awareness Month
    - CAM
    - Choices in Childbirth
    - Childbirth
    - Birth
    - Informed Consent
categories:
    - IUCN
---
