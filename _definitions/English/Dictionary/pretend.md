---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pretend
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484555881
title: pretend
categories:
    - Dictionary
---
pretend
     adj : imagined as in a play; "the make-believe world of theater";
           "play money"; "dangling their legs in the water to
           catch pretend fish" [syn: {make-believe}]
     n : the enactment of a pretense; "it was just pretend" [syn: {make-believe}]
     v 1: make believe with the intent to deceive; "He feigned that he
          was ill"; "He shammed a headache" [syn: {feign}, {sham},
           {affect}, {dissemble}]
     2: behave unnaturally or affectedly; "She's just acting" [syn:
        {dissemble}, {act}]
     3: put forward a claim and assert right or possession of;
        "pretend the title of King"
     4: put forward, of a guess, in spite of possible ...
