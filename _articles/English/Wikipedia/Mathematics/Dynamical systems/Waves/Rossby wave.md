---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rossby_wave
offline_file: ""
offline_thumbnail: ""
uuid: 65fda838-54a9-4cd3-861f-86d7e78ff29a
updated: 1484308896
title: Rossby wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/440px-Jetstream_-_Rossby_Waves_-_N_hemisphere.svg.png
tags:
    - Rossby wave types
    - Atmospheric waves
    - Poleward-propagating atmospheric waves
    - Oceanic waves
    - Waves in astrophysical discs
    - Definitions
    - >
        Free barotropic Rossby waves under a zonal flow with
        linearized vorticity equation
    - Meaning of beta
    - Quasiresonant amplification of Rossby waves
    - Bibliography
categories:
    - Waves
---
Rossby waves, also known as planetary waves, are a natural phenomenon in the atmosphere and oceans of planets that largely owe their properties to rotation. Rossby waves are a subset of inertial waves.
