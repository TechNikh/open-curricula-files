---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unwanted_pregnancies
offline_file: ""
offline_thumbnail: ""
uuid: 32038276-6f00-4908-b956-139b0d2ac216
updated: 1484308380
title: Unintended pregnancy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_23.png
tags:
    - Intention
    - Causes
    - facts
    - Induced abortions
    - Maternal deaths
    - Prevention
    - Epidemiology
    - Incidence
    - By country/region
    - Europe
    - Britain
    - France
    - Russia
    - United States of America
    - History
    - Costs and potential savings
    - Teens
    - Prevention
    - Rape
    - History
categories:
    - Uncategorized
---
Unintended pregnancies are pregnancies that are mistimed, unplanned or unwanted at the time of conception.[1] Unintended pregnancies may also result from rape, incest or various other forms of forced or unwanted sex.
