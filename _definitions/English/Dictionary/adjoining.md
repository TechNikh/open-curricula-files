---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adjoining
offline_file: ""
offline_thumbnail: ""
uuid: 20390cb5-cb95-4e2e-b4a5-e91d6b0f6ae3
updated: 1484310154
title: adjoining
categories:
    - Dictionary
---
adjoining
     adj : having a common boundary or edge; touching; "abutting lots";
           "adjoining rooms"; "Rhode Island has two bordering
           states; Massachusetts and Conncecticut"; "the side of
           Germany conterminous with France"; "Utah and the
           contiguous state of Idaho"; "neighboring cities" [syn:
           {abutting}, {adjacent}, {conterminous}, {contiguous}, {neighboring(a)}]
