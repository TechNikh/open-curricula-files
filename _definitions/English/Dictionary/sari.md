---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sari
offline_file: ""
offline_thumbnail: ""
uuid: 89876e20-cde6-4873-89ca-1a8ad5b30565
updated: 1484310148
title: sari
categories:
    - Dictionary
---
sari
     n : a dress worn primarily by Hindu women; consists of several
         yards of light material that is draped around the body
         [syn: {saree}]
