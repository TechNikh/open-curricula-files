---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maths
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484434081
title: maths
categories:
    - Dictionary
---
maths
     n : a science (or group of related sciences) dealing with the
         logic of quantity and shape and arrangement [syn: {mathematics},
          {math}]
