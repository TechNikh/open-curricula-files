---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/destiny
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484596441
title: destiny
categories:
    - Dictionary
---
destiny
     n 1: an event (or a course of events) that will inevitably happen
          in the future [syn: {fate}]
     2: the ultimate agency that predetermines the course of events
        (often personified as a woman); "we are helpless in the
        face of Destiny" [syn: {Fate}]
     3: your overall circumstances or condition in life (including
        everything that happens to you); "whatever my fortune may
        be"; "deserved a better fate"; "has a happy lot"; "the
        luck of the Irish"; "a victim of circumstances"; "success
        that was her portion" [syn: {fortune}, {fate}, {luck}, {lot},
         {circumstances}, {portion}]
