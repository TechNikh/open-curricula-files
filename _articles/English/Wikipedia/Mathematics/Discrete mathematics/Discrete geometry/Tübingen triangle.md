---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/T%C3%BCbingen_triangle'
offline_file: ""
offline_thumbnail: ""
uuid: 998bf80d-7c41-4ece-aed7-3fe90f8817e8
updated: 1484308619
title: Tübingen triangle
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/170px-T%25C3%25BCbinger_Dreieck.gif'
categories:
    - Discrete geometry
---
The Tübingen triangle is, apart from the Penrose rhomb tilings and their variations, a classical candidate to model 5-fold (respectively 10-fold) quasicrystals. The inflation factor is – as in the Penrose case – the golden mean, 
  
    
      
        φ
        =
        
          
            a
            b
          
        
        =
        
          
            
              1
              +
              
                
                  5
                
              
            
            2
          
        
        ≈
        1.618.
      
    
    {\displaystyle \varphi ={\frac {a}{b}}={\frac {1+{\sqrt {5}}}{2}}\approx 1.618.}
