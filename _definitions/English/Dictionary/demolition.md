---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demolition
offline_file: ""
offline_thumbnail: ""
uuid: e178958e-c86c-46e1-a787-4a32419b1a30
updated: 1484310609
title: demolition
categories:
    - Dictionary
---
demolition
     n 1: an event (or the result of an event) that completely
          destroys something [syn: {destruction}, {wipeout}]
     2: the act of demolishing
