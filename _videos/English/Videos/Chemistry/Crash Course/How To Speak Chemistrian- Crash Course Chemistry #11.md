---
version: 1
type: video
provider: YouTube
id: mlRhLicNo8Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1edf54f8-a04d-4e28-9889-ab7800215271
updated: 1486069680
title: 'How To Speak Chemistrian- Crash Course Chemistry #11'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/mlRhLicNo8Q/default.jpg
    - https://i3.ytimg.com/vi/mlRhLicNo8Q/1.jpg
    - https://i3.ytimg.com/vi/mlRhLicNo8Q/2.jpg
    - https://i3.ytimg.com/vi/mlRhLicNo8Q/3.jpg
---
