---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/actor
offline_file: ""
offline_thumbnail: ""
uuid: 8b70f1e0-6b99-4e58-ae59-601d8212fd21
updated: 1484310192
title: actor
categories:
    - Dictionary
---
actor
     n 1: a theatrical performer [syn: {histrion}, {player}, {thespian},
           {role player}]
     2: a person who acts and gets things done; "he's a principal
        actor in this affair"; "when you want something done get a
        doer"; "he's a miracle worker" [syn: {doer}, {worker}]
