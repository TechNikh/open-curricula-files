---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Attractor
offline_file: ""
offline_thumbnail: ""
uuid: be3c983d-327a-457a-bb7a-0fe260e19729
updated: 1484308794
title: Attractor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/333px-Atractor_Poisson_Saturne.jpg
tags:
    - Motivation
    - Mathematical definition
    - Types of attractors
    - Fixed point
    - Finite number of points
    - Limit cycle
    - Limit torus
    - Strange attractor
    - Effect of parameters on the attractor
    - Basins of attraction
    - Linear equation or system
    - Nonlinear equation or system
    - Partial differential equations
    - 'Numerical localization (visualization) of attractors: self-excited and hidden attractors'
categories:
    - Limit sets
---
In the mathematical field of dynamical systems, an attractor is a set of numerical values toward which a system tends to evolve, for a wide variety of starting conditions of the system.[1] System values that get close enough to the attractor values remain close even if slightly disturbed.
