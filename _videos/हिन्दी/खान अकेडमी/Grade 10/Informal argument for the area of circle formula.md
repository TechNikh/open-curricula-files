---
version: 1
type: video
provider: YouTube
id: MqRWFqRQk1Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d8d44b35-5da7-44e4-aeea-fbaba6f9e305
updated: 1486069619
title: Informal argument for the area of circle formula
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/MqRWFqRQk1Y/default.jpg
    - https://i3.ytimg.com/vi/MqRWFqRQk1Y/1.jpg
    - https://i3.ytimg.com/vi/MqRWFqRQk1Y/2.jpg
    - https://i3.ytimg.com/vi/MqRWFqRQk1Y/3.jpg
---
