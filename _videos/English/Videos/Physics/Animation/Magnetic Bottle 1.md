---
version: 1
type: video
provider: YouTube
id: FiwqNDJuGkI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Magnetic%20Bottle%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5edf901f-70bc-4c10-99c8-cea14ea61e6e
updated: 1486069653
title: Magnetic Bottle 1
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/FiwqNDJuGkI/default.jpg
    - https://i3.ytimg.com/vi/FiwqNDJuGkI/1.jpg
    - https://i3.ytimg.com/vi/FiwqNDJuGkI/2.jpg
    - https://i3.ytimg.com/vi/FiwqNDJuGkI/3.jpg
---
