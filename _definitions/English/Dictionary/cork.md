---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cork
offline_file: ""
offline_thumbnail: ""
uuid: aef4c9f1-f0ea-459a-9b53-264c7b23193f
updated: 1484310375
title: cork
categories:
    - Dictionary
---
cork
     n 1: outer bark of the cork oak; used for stoppers for bottles
          etc.
     2: (botany) outer tissue of bark; a protective layer of dead
        cells [syn: {phellem}]
     3: a port city in southern Ireland
     4: the plug in the mouth of a bottle (especially a wine bottle)
     5: a small float usually made of cork; attached to a fishing
        line [syn: {bob}, {bobber}, {bobfloat}]
     v 1: close a bottle with a cork [syn: {cork up}] [ant: {uncork}]
     2: stuff with cork; "The baseball player stuffed his bat with
        cork to make it lighter"
