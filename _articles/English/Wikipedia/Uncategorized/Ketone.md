---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ketone
offline_file: ""
offline_thumbnail: ""
uuid: b042cdf7-f228-413c-a32c-88b125ec9aa9
updated: 1484308394
title: Ketone
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Ketone-group-2D-skeletal.svg_0.png
tags:
    - Nomenclature and etymology
    - Structure and properties
    - Classes of ketones
    - Diketones
    - Unsaturated ketones
    - Cyclic ketones
    - Keto-enol tautomerization
    - Acidity of ketones
    - Characterization
    - Spectroscopy
    - Qualitative organic tests
    - Synthesis
    - Reactions
    - Biochemistry
    - Applications
    - Toxicity
categories:
    - Uncategorized
---
In chemistry, a ketone (alkanone) /ˈkiːtoʊn/ is an organic compound with the structure RC(=O)R', where R and R' can be a variety of carbon-containing substituents. Ketones and aldehydes are simple compounds that contain a carbonyl group (a carbon-oxygen double bond). They are considered "simple" because they do not have reactive groups like −OH or −Cl attached directly to the carbon atom in the carbonyl group, as in carboxylic acids containing −COOH.[1] Many ketones are known and many are of great importance in industry and in biology. Examples include many sugars (ketoses) and the industrial solvent acetone, which is the smallest ketone.
