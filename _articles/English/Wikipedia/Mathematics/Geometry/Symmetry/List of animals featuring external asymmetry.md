---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_animals_featuring_external_asymmetry
offline_file: ""
offline_thumbnail: ""
uuid: e68e8311-fbe0-4966-8ab1-7dcd7c7b006a
updated: 1484309305
title: List of animals featuring external asymmetry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Anarhynchus_frontalis_head_1869.jpg
tags:
    - birds
    - Fish
    - Mammals
    - Invertebrates
categories:
    - Symmetry
---
This is a list of animals that markedly feature external asymmetry in some form. They are exceptions to the general pattern of symmetry in biology. In particular, these animals do not exhibit bilateral symmetry which permits streamlining and is common in animals.[1]
