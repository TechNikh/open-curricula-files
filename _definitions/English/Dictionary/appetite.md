---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appetite
offline_file: ""
offline_thumbnail: ""
uuid: ac9ebd8b-fe5b-45c8-b1da-126dbb90268b
updated: 1484310349
title: appetite
categories:
    - Dictionary
---
appetite
     n : a feeling of craving something; "an appetite for life"; "the
         object of life is to satisfy as many appetencies as
         possible"- Granville Hicks [syn: {appetency}, {appetence}]
