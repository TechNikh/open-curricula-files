---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quadratic_residue_code
offline_file: ""
offline_thumbnail: ""
uuid: 893630de-7c35-4d5f-ba02-f73a31321a26
updated: 1484308562
title: Quadratic residue code
categories:
    - Coding theory
---
There is a quadratic residue code of length 
  
    
      
        p
      
    
    {\displaystyle p}
  
 over the finite field 
  
    
      
        G
        F
        (
        l
        )
      
    
    {\displaystyle GF(l)}
  
 whenever 
  
    
      
        p
      
    
    {\displaystyle p}
  
 and 
  
    
      
        l
      
    
    {\displaystyle l}
  
 are primes, 
  
    
      
        p
      
    
    {\displaystyle p}
  
 is odd, and 
  
    
      
        l
      
    
    {\displaystyle l}
  
 is a quadratic residue modulo 
  
    
      
        p
      
    
    {\displaystyle p}
  
. Its generator polynomial as a cyclic code is given by
