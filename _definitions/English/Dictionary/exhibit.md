---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exhibit
offline_file: ""
offline_thumbnail: ""
uuid: 868b12c0-8b5c-426e-80f2-36c6bc0455f2
updated: 1484310414
title: exhibit
categories:
    - Dictionary
---
exhibit
     n 1: an object or statement produced before a court of law and
          referred to while giving evidence
     2: something shown to the public; "the museum had many exhibits
        of oriental art" [syn: {display}, {showing}]
     v 1: show an attribute, property, knowledge, or skill; "he
          exhibits a great talent"
     2: to show, make visible or apparent; "The Metropolitan Museum
        is exhibiting Goya's works this month"; "Why don't you
        show your nice legs and wear shorter skirts?"; "National
        leaders will have to display the highest skills of
        statesmanship" [syn: {expose}, {display}]
     3: show or demonstrate something to an ...
