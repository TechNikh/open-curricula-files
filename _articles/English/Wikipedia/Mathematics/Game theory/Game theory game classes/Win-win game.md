---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Win-win_game
offline_file: ""
offline_thumbnail: ""
uuid: 769597c6-ac2f-42f9-9422-7b7a3cb80059
updated: 1484309040
title: Win-win game
tags:
    - Types
    - Group dynamics
categories:
    - Game theory game classes
---
A win–win game is a game which is designed in a way that all participants can profit from it in one way or the other. In conflict resolution, a win–win strategy is a conflict resolution process that aims to accommodate all disputants.[1][2][3]
