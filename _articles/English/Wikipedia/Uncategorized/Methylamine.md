---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Methanamine
offline_file: ""
offline_thumbnail: ""
uuid: b3a5eb59-e52f-46c3-b44a-eafa4b0e4591
updated: 1484308398
title: Methylamine
tags:
    - Industrial production
    - Laboratory methods
    - Reactivity and applications
    - Biological chemistry
    - Safety
categories:
    - Uncategorized
---
Methylamine is an organic compound with a formula of CH3NH2. This colorless gas is a derivative of ammonia, but with one hydrogen atom being replaced by a methyl group. It is the simplest primary amine. It is sold as a solution in methanol, ethanol, tetrahydrofuran, or water, or as the anhydrous gas in pressurized metal containers. Industrially, methylamine is transported in its anhydrous form in pressurized railcars and tank trailers. It has a strong odor similar to fish. Methylamine is used as a building block for the synthesis of many other commercially available compounds.
