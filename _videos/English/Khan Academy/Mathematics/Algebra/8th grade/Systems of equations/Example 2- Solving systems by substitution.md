---
version: 1
type: video
provider: YouTube
id: wB3QCk0MGuw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/8th%20grade/Systems%20of%20equations/Example%202-%20Solving%20systems%20by%20substitution.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2644f442-d76a-4274-a67c-0c0ef68a7ff2
updated: 1486069557
title: 'Example 2- Solving systems by substitution'
categories:
    - Systems of equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/wB3QCk0MGuw/default.jpg
    - https://i3.ytimg.com/vi/wB3QCk0MGuw/1.jpg
    - https://i3.ytimg.com/vi/wB3QCk0MGuw/2.jpg
    - https://i3.ytimg.com/vi/wB3QCk0MGuw/3.jpg
---
