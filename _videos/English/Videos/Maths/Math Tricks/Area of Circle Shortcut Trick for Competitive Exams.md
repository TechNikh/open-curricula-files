---
version: 1
type: video
provider: YouTube
id: Z35Iflnj-kU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 434c3d3e-2e59-4313-a183-dbb64d503e65
updated: 1486069621
title: Area of Circle Shortcut Trick for Competitive Exams
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Z35Iflnj-kU/default.jpg
    - https://i3.ytimg.com/vi/Z35Iflnj-kU/1.jpg
    - https://i3.ytimg.com/vi/Z35Iflnj-kU/2.jpg
    - https://i3.ytimg.com/vi/Z35Iflnj-kU/3.jpg
---
