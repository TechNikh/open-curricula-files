---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nativity
offline_file: ""
offline_thumbnail: ""
uuid: 426132c8-fc65-4d3e-a529-8f2d38953e26
updated: 1484310163
title: nativity
categories:
    - Dictionary
---
nativity
     n 1: the event of being born; "they celebrated the birth of their
          first child" [syn: {birth}, {nascency}, {nascence}]
          [ant: {death}]
     2: the theological doctrine that Jesus Christ had no human
        father; Christians believe that Jesus's birth fulfilled
        Old Testament prophecies and was attended by miracles; the
        Nativity is celebrated at Christmas [syn: {Virgin Birth}]
