---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Position_tolerance
offline_file: ""
offline_thumbnail: ""
uuid: f73d2ea7-60b6-4ca2-944c-33d353aa409b
updated: 1484309332
title: Position tolerance
categories:
    - Technical drawing
---
Position Tolerance (symbol: ⌖) is a geometric dimensioning and tolerancing (GD&T) location control used on engineering drawings to specify desired location, as well as allowed deviation to the position of a feature on a part. Position tolerance must only be applied to features of size, which requires that the feature have at least two opposable points.
