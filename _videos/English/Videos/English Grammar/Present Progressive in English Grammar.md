---
version: 1
type: video
provider: YouTube
id: nUqXvEbRpWw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: eb51bb48-54b5-4fd9-b3c1-2440c239c194
updated: 1486069631
title: Present Progressive in English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/nUqXvEbRpWw/default.jpg
    - https://i3.ytimg.com/vi/nUqXvEbRpWw/1.jpg
    - https://i3.ytimg.com/vi/nUqXvEbRpWw/2.jpg
    - https://i3.ytimg.com/vi/nUqXvEbRpWw/3.jpg
---
