---
version: 1
type: video
provider: YouTube
id: YYEgq1bweN4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Detectable%20civilizations%20in%20our%20galaxy%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f793518d-5408-4d0d-805c-2f708c86ffbd
updated: 1486069688
title: Detectable civilizations in our galaxy 1
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/YYEgq1bweN4/default.jpg
    - https://i3.ytimg.com/vi/YYEgq1bweN4/1.jpg
    - https://i3.ytimg.com/vi/YYEgq1bweN4/2.jpg
    - https://i3.ytimg.com/vi/YYEgq1bweN4/3.jpg
---
