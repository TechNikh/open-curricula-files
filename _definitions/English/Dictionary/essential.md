---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/essential
offline_file: ""
offline_thumbnail: ""
uuid: b08befb8-35e0-4795-a7a8-a65f8cdfdb05
updated: 1484310268
title: essential
categories:
    - Dictionary
---
essential
     adj 1: absolutely necessary; vitally necessary; "essential tools
            and materials"; "funds essential to the completion of
            the project"; "an indispensable worker" [syn: {indispensable}]
     2: basic and fundamental; "the essential feature" [ant: {inessential}]
     3: of the greatest importance; "the all-important subject of
        disarmament"; "crucial information"; "in chess cool nerves
        are of the essence" [syn: {all-important(a)}, {all
        important(p)}, {crucial}, {of the essence(p)}]
     4: being or relating to or containing the essence of a plant
        etc; "essential oil"
     5: applying to essential legal principles and rules of ...
