---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lodge
offline_file: ""
offline_thumbnail: ""
uuid: fd6dc107-29f9-4a51-b7cd-1b9b3ae74434
updated: 1484310518
title: lodge
categories:
    - Dictionary
---
Lodge
     n 1: English physicist who studied electromagnetic radiation and
          was a pioneer of radiotelegraphy (1851-1940) [syn: {Sir
          Oliver Lodge}, {Sir Oliver Joseph Lodge}]
     2: a formal association of people with similar interests; "he
        joined a golf club"; "they formed a small lunch society";
        "men from the fraternal order will staff the soup kitchen
        today" [syn: {club}, {society}, {guild}, {gild}, {order}]
     3: small house at the entrance to the grounds of a country
        mansion; usually occupied by a gatekeeper or gardener
     4: a small (rustic) house used as a temporary shelter [syn: {hunting
        lodge}]
     5: any of various ...
