---
version: 1
type: video
provider: YouTube
id: fLL8xxjhDPI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Knetics/Finding%20units%20of%20rate%20constant%20k.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0f072410-d35d-419e-867f-20722527b4f0
updated: 1486069589
title: Finding units of rate constant k
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Knetics
thumbnail_urls:
    - https://i3.ytimg.com/vi/fLL8xxjhDPI/default.jpg
    - https://i3.ytimg.com/vi/fLL8xxjhDPI/1.jpg
    - https://i3.ytimg.com/vi/fLL8xxjhDPI/2.jpg
    - https://i3.ytimg.com/vi/fLL8xxjhDPI/3.jpg
---
How to find the units for the rate constant k for a zero, first, or second order reaction. 

Watch the next lesson: https://www.khanacademy.org/science/chemistry/chem-kinetics/reaction-rates/v/experimental-determination-of-rate-laws?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/chem-kinetics/reaction-rates/v/rate-law-and-reaction-order?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
