---
version: 1
type: video
provider: YouTube
id: tWnvt-8wSeA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cc138a47-4fa2-4a2f-91ab-2f15a386e597
updated: 1486069656
title: 'Polynomial derivatives-  3D visualization'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/tWnvt-8wSeA/default.jpg
    - https://i3.ytimg.com/vi/tWnvt-8wSeA/1.jpg
    - https://i3.ytimg.com/vi/tWnvt-8wSeA/2.jpg
    - https://i3.ytimg.com/vi/tWnvt-8wSeA/3.jpg
---
