---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ionic
offline_file: ""
offline_thumbnail: ""
uuid: 86f5dcf9-9cbf-4d6c-b22d-6b3714fffa17
updated: 1484310383
title: ionic
categories:
    - Dictionary
---
ionic
     adj 1: containing or involving or occurring in the form of ions;
            "ionic charge"; "ionic crystals"; "ionic hydrogen"
            [ant: {nonionic}]
     2: an order of classical Greek architecture [ant: {corinthian},
         {doric}]
     n : the dialect of Ancient Greek spoken in Ionia
