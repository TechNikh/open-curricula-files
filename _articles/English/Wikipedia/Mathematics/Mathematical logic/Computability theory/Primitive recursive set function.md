---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Primitive_recursive_set_function
offline_file: ""
offline_thumbnail: ""
uuid: 61e41c31-b3bc-4f05-9ecc-56b4dca26a35
updated: 1484309490
title: Primitive recursive set function
categories:
    - Computability theory
---
In mathematics, primitive recursive set functions or primitive recursive ordinal functions are analogs of primitive recursive functions, defined for sets or ordinals rather than natural numbers. They were introduced by Jensen & Karp (1971).
