---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/successfully
offline_file: ""
offline_thumbnail: ""
uuid: c4d5893a-ca67-4d7c-ad0b-43e115d251b0
updated: 1484310240
title: successfully
categories:
    - Dictionary
---
successfully
     adv : in a successful manner; "she performed the surgery
           successfully" [syn: {with success}] [ant: {unsuccessfully}]
