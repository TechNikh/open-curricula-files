---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/numeral
offline_file: ""
offline_thumbnail: ""
uuid: 0de7bfa0-b2c9-4580-acab-bcfd0c6e1c4a
updated: 1484310399
title: numeral
categories:
    - Dictionary
---
numeral
     adj : of or relating to or denoting numbers; "a numeral
           adjective"; "numerical analysis" [syn: {numerical}, {numeric}]
     n : a symbol used to represent a number; "he learned to write
         the numerals before he went to school" [syn: {number}]
