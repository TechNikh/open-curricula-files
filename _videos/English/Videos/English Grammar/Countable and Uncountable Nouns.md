---
version: 1
type: video
provider: YouTube
id: aLQ5_zjdhQ8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e5e46705-386e-4ba6-865f-09dab671a5db
updated: 1486069631
title: Countable and Uncountable Nouns
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/aLQ5_zjdhQ8/default.jpg
    - https://i3.ytimg.com/vi/aLQ5_zjdhQ8/1.jpg
    - https://i3.ytimg.com/vi/aLQ5_zjdhQ8/2.jpg
    - https://i3.ytimg.com/vi/aLQ5_zjdhQ8/3.jpg
---
