---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/verifying
offline_file: ""
offline_thumbnail: ""
uuid: 88c61475-f820-43e9-b40a-30b1abd23572
updated: 1484310216
title: verifying
categories:
    - Dictionary
---
verifying
     adj : serving to support or corroborate; "collateral evidence"
           [syn: {collateral}, {confirmative}, {confirming}, {confirmatory},
            {corroborative}, {corroboratory}, {substantiating}, {substantiative},
            {validating}, {validatory}, {verificatory}]
