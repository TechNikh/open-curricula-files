---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/highest
offline_file: ""
offline_thumbnail: ""
uuid: 0e3c93af-4e51-4d4b-80bb-4805b02e6486
updated: 1484310260
title: highest
categories:
    - Dictionary
---
highest
     adj 1: approaching or constituting a maximum; "maximal
            temperature"; "maximum speed"; "working at peak
            efficiency" [syn: {peak(a)}]
     2: highest and most significant; "his highest achievement"
