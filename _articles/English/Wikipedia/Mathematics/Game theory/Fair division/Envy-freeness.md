---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Envy-freeness
offline_file: ""
offline_thumbnail: ""
uuid: 17368fc9-2d16-43fa-89ec-52e8ec05ac76
updated: 1484308520
title: Envy-freeness
tags:
    - Definitions
    - History
    - Relations to other fairness criteria
    - Implications between proportionality and envy-freeness
categories:
    - Fair division
---
Envy-freeness is a criterion of fair division. An envy-free division is a division in which every partner feels that his or her allocated share is at least as good as any other share.
