---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hammer
offline_file: ""
offline_thumbnail: ""
uuid: 25edd2c5-8b6f-493f-9c40-dc13fb442f80
updated: 1484310587
title: hammer
categories:
    - Dictionary
---
hammer
     n 1: the part of a gunlock that strikes the percussion cap when
          the trigger is pulled [syn: {cock}]
     2: a hand tool with a heavy rigid head and a handle; used to
        deliver an impulsive force by striking
     3: an athletic competition in which a heavy metal ball that is
        attached to a flexible wire is hurled as far as possible
        [syn: {hammer throw}]
     4: the ossicle attached to the eardrum [syn: {malleus}]
     5: a heavy metal sphere attached to a flexible wire; used in
        the hammer throw
     6: a striker that is covered in felt and that causes the piano
        strings to vibrate
     7: a power tool for drilling rocks [syn: {power ...
