---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terms
offline_file: ""
offline_thumbnail: ""
uuid: 583ae372-d829-417c-a6fe-01532ed61eba
updated: 1484310268
title: terms
categories:
    - Dictionary
---
terms
     n 1: status with respect to the relations between people or
          groups; "on good terms with her in-laws"; "on a friendly
          footing" [syn: {footing}]
     2: the amount of money needed to purchase something; "the price
        of gasoline"; "he got his new car on excellent terms";
        "how much is the damage?" [syn: {price}, {damage}]
