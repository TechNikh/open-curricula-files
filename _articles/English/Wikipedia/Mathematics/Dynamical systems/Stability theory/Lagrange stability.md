---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lagrange_stability
offline_file: ""
offline_thumbnail: ""
uuid: 2ced39c5-8db9-477f-9ed1-d59ea50b757f
updated: 1484308828
title: Lagrange stability
categories:
    - Stability theory
---
For any point in the state space, 
  
    
      
        x
        ∈
        M
      
    
    {\displaystyle x\in M}
  
 in a real continuous dynamical system 
  
    
      
        (
        T
        ,
        M
        ,
        Φ
        )
      
    
    {\displaystyle (T,M,\Phi )}
  
, where 
  
    
      
        T
      
    
    {\displaystyle T}
  
 is 
  
    
      
        
          R
        
      
    
    {\displaystyle \mathbb {R} }
  
, the motion 
  
    
      
        Φ
        (
        t
        ,
        x
        )
      
    
    {\displaystyle \Phi (t,x)}
  
 is said to be positively Lagrange stable if the positive semi-orbit 
  
    
      
        
    ...
