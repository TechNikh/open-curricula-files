---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pea_family
offline_file: ""
offline_thumbnail: ""
uuid: acb38f9b-d2e3-421f-8d55-2b33c8228cb8
updated: 1484308366
title: Fabaceae
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Gymnocladus-dioicus.jpg
tags:
    - Etymology
    - Description
    - Growth habit
    - Leaves
    - Roots
    - Flowers
    - fruit
    - Physiology and biochemistry
    - Ecology
    - Distribution and habitat
    - Biological nitrogen fixation
    - Evolution, phylogeny and taxonomy
    - Evolution
    - Phylogeny and taxonomy
    - Phylogeny
    - Taxonomy
    - Genera
    - Economic and cultural importance
    - Food and forage
    - Industrial uses
    - Natural gums
    - Dyes
    - Ornamentals
    - Emblematic Leguminosae
    - Image gallery
categories:
    - Uncategorized
---
The Fabaceae, Leguminosae or Papilionaceae,[6] commonly known as the legume, pea, or bean family, are a large and economically important family of flowering plants. It includes trees, shrubs, and perennial or annual herbaceous plants, which are easily recognized by their fruit (legume) and their compound, stipulated leaves. The family is widely distributed, and is the third-largest land plant family in terms of number of species, behind only the Orchidaceae and Asteraceae, with about 751 genera and some 19,000 known species[7][8][9] . The five largest of the genera are Astragalus (over 3,000 species), Acacia (over 1000 species), Indigofera (around 700 species), Crotalaria (around 700 ...
