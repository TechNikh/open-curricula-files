---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/warning
offline_file: ""
offline_thumbnail: ""
uuid: 89d7fd2f-85f9-48b7-8aba-b0a3355b0ef5
updated: 1484310196
title: warning
categories:
    - Dictionary
---
warning
     adj : serving to warn; "shook a monitory finger at him"; "an
           exemplary jail sentence" [syn: {admonitory}, {cautionary},
            {exemplary}, {monitory}, {warning(a)}]
     n 1: a message informing of danger
     2: cautionary advice about something imminent (especially
        imminent danger) [syn: {admonition}, {monition}, {word of
        advice}]
