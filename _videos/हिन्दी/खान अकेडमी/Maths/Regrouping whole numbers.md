---
version: 1
type: video
provider: YouTube
id: mRXTg1teiWg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ced5cb92-6ecc-442d-8687-e2689f122714
updated: 1486069617
title: Regrouping whole numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/mRXTg1teiWg/default.jpg
    - https://i3.ytimg.com/vi/mRXTg1teiWg/1.jpg
    - https://i3.ytimg.com/vi/mRXTg1teiWg/2.jpg
    - https://i3.ytimg.com/vi/mRXTg1teiWg/3.jpg
---
