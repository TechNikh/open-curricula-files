---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quantum
offline_file: ""
offline_thumbnail: ""
uuid: 4b960878-042e-4ed1-967b-3540b7886879
updated: 1484310391
title: quantum
categories:
    - Dictionary
---
quantum
     n 1: a discrete amount of something that is analogous to the
          quantum in quantum theory
     2: (physics) the smallest discrete quantity of some physical
        property that a system can possess (according to quantum
        theory)
     [also: {quanta} (pl)]
