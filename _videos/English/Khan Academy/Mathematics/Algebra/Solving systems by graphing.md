---
version: 1
type: video
provider: YouTube
id: 1CiBuN6qraY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Solving%20systems%20by%20graphing.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a7765297-515f-42c9-9d9d-7e6bff392366
updated: 1486069565
title: Solving systems by graphing
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/1CiBuN6qraY/default.jpg
    - https://i3.ytimg.com/vi/1CiBuN6qraY/1.jpg
    - https://i3.ytimg.com/vi/1CiBuN6qraY/2.jpg
    - https://i3.ytimg.com/vi/1CiBuN6qraY/3.jpg
---
