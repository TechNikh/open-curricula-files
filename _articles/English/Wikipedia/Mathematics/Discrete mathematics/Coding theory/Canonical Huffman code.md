---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Canonical_Huffman_code
offline_file: ""
offline_thumbnail: ""
uuid: 9342c68c-fa23-423f-979a-5a68e20dddae
updated: 1484308556
title: Canonical Huffman code
tags:
    - Algorithm
    - As a fractional binary number
    - Encoding the codebook
    - Pseudo code
    - Algorithm
categories:
    - Coding theory
---
Data compressors generally work in one of two ways. Either the decompressor can infer what codebook the compressor has used from previous context, or the compressor must tell the decompressor what the codebook is. Since a canonical Huffman codebook can be stored especially efficiently, most compressors start by generating a "normal" Huffman codebook, and then convert it to canonical Huffman before using it.
