---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manure
offline_file: ""
offline_thumbnail: ""
uuid: 9c7b5b2f-1a9a-4895-857a-4a41a32884a2
updated: 1484310543
title: manure
categories:
    - Dictionary
---
manure
     n : any animal or plant material used to fertilize land
         especially animal excreta usually with litter material
     v : spread manure, as for fertilization [syn: {muck}]
