---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foreign
offline_file: ""
offline_thumbnail: ""
uuid: 77dacadf-595c-4e4d-a005-e55c199feab0
updated: 1484310323
title: foreign
categories:
    - Dictionary
---
foreign
     adj 1: of concern to or concerning the affairs of other nations
            (other than your own); "foreign trade"; "a foreign
            office" [ant: {domestic}]
     2: relating to or originating in or characteristic of another
        place or part of the world; "foreign nations"; "a foreign
        accent"; "on business in a foreign city" [ant: {native}]
     3: not contained in or deriving from the essential nature of
        something; "an economic theory alien to the spirit of
        capitalism"; "the mysticism so foreign to the French mind
        and temper"; "jealousy is foreign to her nature" [syn: {alien}]
     4: not belonging to that in which it is contained; ...
