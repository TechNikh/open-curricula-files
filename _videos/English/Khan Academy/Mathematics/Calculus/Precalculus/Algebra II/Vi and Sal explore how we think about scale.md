---
version: 1
type: video
provider: YouTube
id: 4xfOq00BzJA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Algebra%20II/Vi%20and%20Sal%20explore%20how%20we%20think%20about%20scale.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 521836bf-694b-4f73-8373-e8d2f357dfd3
updated: 1486069696
title: Vi and Sal explore how we think about scale
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/4xfOq00BzJA/default.jpg
    - https://i3.ytimg.com/vi/4xfOq00BzJA/1.jpg
    - https://i3.ytimg.com/vi/4xfOq00BzJA/2.jpg
    - https://i3.ytimg.com/vi/4xfOq00BzJA/3.jpg
---
