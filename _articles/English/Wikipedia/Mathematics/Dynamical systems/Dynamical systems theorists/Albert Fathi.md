---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Albert_Fathi
offline_file: ""
offline_thumbnail: ""
uuid: e5cd3148-f3e6-4e8f-91f6-a7af47aab0fd
updated: 1484308684
title: Albert Fathi
categories:
    - Dynamical systems theorists
---
Albert Fathi (27 October 1951, Egypt) is an Egyptian-French mathematician. He specializes in dynamic systems and is currently a professor at the École normale supérieure de Lyon.
