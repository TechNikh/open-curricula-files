---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Michael_Herman_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 6ffb421d-fcf8-4aaf-a899-1744d3b81451
updated: 1484308692
title: Michael Herman (mathematician)
categories:
    - Dynamical systems theorists
---
Michael Robert Herman (6 November 1942 – 2 November 2000) was a French American mathematician. He was one of the leading experts on the theory of dynamical systems.
