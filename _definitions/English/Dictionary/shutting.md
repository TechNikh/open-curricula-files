---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shutting
offline_file: ""
offline_thumbnail: ""
uuid: 9e4967c6-7d41-4aed-a38e-433215d6df08
updated: 1484310559
title: shutting
categories:
    - Dictionary
---
shut
     adj 1: not open; "the door slammed shut" [syn: {unopen}, {closed}]
            [ant: {open}]
     2: used especially of mouth or eyes; "he sat quietly with
        closed eyes"; "his eyes were shut against the sunlight"
        [syn: {closed}] [ant: {open}]
     v 1: move so that an opening or passage is obstructed; make shut;
          "Close the door"; "shut the window" [syn: {close}] [ant:
           {open}]
     2: become closed; "The windows closed with a loud bang" [syn: {close}]
        [ant: {open}]
     3: prevent from entering; shut out; "The trees were shutting
        out all sunlight"; "This policy excludes people who have a
        criminal record from entering the ...
