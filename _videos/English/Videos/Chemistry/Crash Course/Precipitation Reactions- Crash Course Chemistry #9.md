---
version: 1
type: video
provider: YouTube
id: IIu16dy3ThI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Precipitation%20Reactions-%20Crash%20Course%20Chemistry%20%239.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 130b43a9-1607-4c75-9d85-fdda0bc41dba
updated: 1486069678
title: 'Precipitation Reactions- Crash Course Chemistry #9'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/IIu16dy3ThI/default.jpg
    - https://i3.ytimg.com/vi/IIu16dy3ThI/1.jpg
    - https://i3.ytimg.com/vi/IIu16dy3ThI/2.jpg
    - https://i3.ytimg.com/vi/IIu16dy3ThI/3.jpg
---
