---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bundle_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 6aad9bba-f485-45c0-8fbc-44144e0282e9
updated: 1484309167
title: Bundle theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Bundletheorem-moeb.svg.png
categories:
    - Classical geometry
---
In geometry, the bundle theorem is in the simplest case a statement on six circles and eight points in the real Euclidean plane. In general it is a property of a Möbius plane that is fulfilled by ovoidal Möbius planes only.
