---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/logic
offline_file: ""
offline_thumbnail: ""
uuid: 4626783e-c2d6-4f8b-80b6-ea6d469fafec
updated: 1484310547
title: logic
categories:
    - Dictionary
---
logic
     n 1: the branch of philosophy that analyzes inference
     2: reasoned and reasonable judgment; "it made a certain kind of
        logic"
     3: the principles that guide reasoning within a given field or
        situation; "economic logic requires it"; "by the logic of
        war"
     4: a system of reasoning [syn: {logical system}, {system of
        logic}]
