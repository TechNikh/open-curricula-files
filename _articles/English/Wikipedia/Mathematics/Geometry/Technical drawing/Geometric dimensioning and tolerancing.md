---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Geometric_dimensioning_and_tolerancing
offline_file: ""
offline_thumbnail: ""
uuid: 7206643f-0b14-4f50-a03c-46f2ab2945e9
updated: 1484309325
title: Geometric dimensioning and tolerancing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Geometric_dimenioning_and_tolerancing-hole.svg.png
tags:
    - Dimensioning and tolerancing philosophy
    - Symbols
    - Datums and datum references
    - Data exchange
    - Documents and standards
    - ISO TC 10 Technical product documentation
    - >
        ISO/TC 213 Dimensional and geometrical product
        specifications and verification
    - ASME standards American Society of Mechanical Engineers
    - 'GD&T standards for data exchange and integration'
categories:
    - Technical drawing
---
Geometric Dimensioning and Tolerancing (GD&T) is a system for defining and communicating engineering tolerances. It uses a symbolic language on engineering drawings and computer-generated three-dimensional solid models that explicitly describes nominal geometry and its allowable variation. It tells the manufacturing staff and machines what degree of accuracy and precision is needed on each controlled feature of the part. GD&T is used to define the nominal (theoretically perfect) geometry of parts and assemblies, to define the allowable variation in form and possible size of individual features, and to define the allowable variation between features..
