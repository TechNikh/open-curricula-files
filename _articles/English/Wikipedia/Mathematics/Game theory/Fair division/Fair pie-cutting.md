---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fair_pie-cutting
offline_file: ""
offline_thumbnail: ""
uuid: f0060aa9-53fa-4a39-b123-282f621157ab
updated: 1484308520
title: Fair pie-cutting
tags:
    - Model
    - Two partners
    - Envy-free division
    - Envy-free and Pareto-efficient division
    - Additivity constraints
    - Consensus division and weighted proportional division
    - Weighted envy-free division
    - Equitable division
    - Truthful division
    - Three or more partners
    - Envy-free division for 3 partners
    - Envy-free and Pareto-efficient division
    - Proportional division with different entitlements
categories:
    - Fair division
---
As an example, consider a birthday cake shaped as a disk. The cake should be divided among several children such that no child envies another child (as in a standard cake-cutting problem), with the additional constraint that the cuts must be radial, so that each child receives a circular sector.
