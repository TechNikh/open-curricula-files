---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quartz
offline_file: ""
offline_thumbnail: ""
uuid: 1d383a7d-0040-4ace-bc07-0dad4f097e53
updated: 1484310212
title: quartz
categories:
    - Dictionary
---
quartz
     n 1: colorless glass made of almost pure silica [syn: {quartz
          glass}, {vitreous silica}, {lechatelierite}, {crystal}]
     2: a hard glossy mineral consisting of silicon dioxide in
        crystal form; present in most rocks (especially sandstone
        and granite); yellow sand is quartz with iron oxide
        impurities
