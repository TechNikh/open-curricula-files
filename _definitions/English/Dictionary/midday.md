---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/midday
offline_file: ""
offline_thumbnail: ""
uuid: 20017540-3460-4266-8d19-779e5703165c
updated: 1484310469
title: midday
categories:
    - Dictionary
---
midday
     n : the middle of the day [syn: {noon}, {twelve noon}, {high
         noon}, {noonday}, {noontide}]
