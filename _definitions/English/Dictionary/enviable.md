---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enviable
offline_file: ""
offline_thumbnail: ""
uuid: 7e19355e-a8b3-407e-8bcc-1237bf8836c4
updated: 1484310611
title: enviable
categories:
    - Dictionary
---
enviable
     adj : causing envy; "an enviable position"
