---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fundamental_domain
uuid: 9639ed5d-9d90-45e3-a5dc-61b21c696435
updated: 1480455189
title: Fundamental domain
tags:
    - Hints at general definition
    - Examples
    - Fundamental domain for the modular group
categories:
    - Ergodic theory
---
Given a topological space and a group acting on it, the images of a single point under the group action form an orbit of the action. A fundamental domain is a subset of the space which contains exactly one point from each of these orbits. It serves as a geometric realization for the abstract set of representatives of the orbits.
