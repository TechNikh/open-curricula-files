---
version: 1
type: article
id: https://en.wikipedia.org/wiki/4_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 3dbb5b83-2a02-4d52-8cac-323c0bd3318d
updated: 1484308942
title: 4 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-ICS_Four.svg.png
tags:
    - In mathematics
    - List of basic calculations
    - Origins
    - In religion
    - In politics
    - In computing
    - In science
    - In astronomy
    - In biology
    - In chemistry
    - In physics
    - In logic and philosophy
    - In technology
    - In transport
    - In sports
    - In other fields
    - In music
    - Groups of four
categories:
    - Integers
---
