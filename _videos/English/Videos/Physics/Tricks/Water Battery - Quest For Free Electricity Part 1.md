---
version: 1
type: video
provider: YouTube
id: b-GAvA0YCxE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3c37b54b-184a-4adf-ac5c-54fe6521eb90
updated: 1486069658
title: 'Water Battery - Quest For Free Electricity Part 1'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/b-GAvA0YCxE/default.jpg
    - https://i3.ytimg.com/vi/b-GAvA0YCxE/1.jpg
    - https://i3.ytimg.com/vi/b-GAvA0YCxE/2.jpg
    - https://i3.ytimg.com/vi/b-GAvA0YCxE/3.jpg
---
