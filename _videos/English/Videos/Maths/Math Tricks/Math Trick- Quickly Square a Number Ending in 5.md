---
version: 1
type: video
provider: YouTube
id: oV7Kxm3Zr_c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2d7ba73f-664a-4dc5-b088-2775c77c4d36
updated: 1486069623
title: 'Math Trick- Quickly Square a Number Ending in 5'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/oV7Kxm3Zr_c/default.jpg
    - https://i3.ytimg.com/vi/oV7Kxm3Zr_c/1.jpg
    - https://i3.ytimg.com/vi/oV7Kxm3Zr_c/2.jpg
    - https://i3.ytimg.com/vi/oV7Kxm3Zr_c/3.jpg
---
