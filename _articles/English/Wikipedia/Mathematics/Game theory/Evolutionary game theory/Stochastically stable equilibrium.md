---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Stochastically_stable_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 5b29da33-81c7-4d91-b59d-19bcdcfb12ed
updated: 1484308527
title: Stochastically stable equilibrium
categories:
    - Evolutionary game theory
---
In game theory, a stochastically stable equilibrium is a refinement of the evolutionarily stable state in evolutionary game theory, proposed by Dean Foster and Peyton Young. An evolutionary stable state S is also stochastically stable if under vanishing noise the probability that the population is in the vicinity of state S does not go to zero.
