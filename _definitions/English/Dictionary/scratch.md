---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scratch
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484406001
title: scratch
categories:
    - Dictionary
---
scratch
     n 1: an abraded area where the skin is torn or worn off [syn: {abrasion},
           {scrape}, {excoriation}]
     2: a depression scratched or carved into a surface [syn: {incision},
         {prick}, {slit}, {dent}]
     3: informal terms for money [syn: {boodle}, {bread}, {cabbage},
         {clams}, {dinero}, {dough}, {gelt}, {kale}, {lettuce}, {lolly},
         {lucre}, {loot}, {moolah}, {pelf}, {shekels}, {simoleons},
         {sugar}, {wampum}]
     4: dry mash for poultry [syn: {chicken feed}]
     5: a harsh noise made by scraping; "the scrape of violin bows
        distracted her" [syn: {scrape}, {scraping}, {scratching}]
     6: poor handwriting [syn: {scribble}, ...
