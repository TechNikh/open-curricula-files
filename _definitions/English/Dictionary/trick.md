---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trick
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484408641
title: trick
categories:
    - Dictionary
---
trick
     n 1: a cunning or deceitful action or device; "he played a trick
          on me"; "he pulled a fast one and got away with it"
          [syn: {fast one}]
     2: a period of work or duty
     3: an attempt to get you to do something foolish or imprudent;
        "that offer was a dirty trick"
     4: a ludicrous or grotesque act done for fun and amusement
        [syn: {antic}, {joke}, {prank}, {caper}, {put-on}]
     5: an illusory feat; considered magical by naive observers
        [syn: {magic trick}, {conjuring trick}, {magic}, {legerdemain},
         {conjuration}, {illusion}, {deception}]
     v : deceive somebody; "We tricked the teacher into thinking that
         class ...
