---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nick
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484406362
title: nick
categories:
    - Dictionary
---
nick
     n 1: an impression in a surface (as made by a blow) [syn: {dent},
           {gouge}]
     2: a small cut [syn: {notch}, {snick}]
     v 1: cut slightly, with a razor; "The barber's knife nicked his
          cheek" [syn: {snick}]
     2: cut a nick into [syn: {chip}]
     3: divide or reset the tail muscles of; "nick horses"
     4: mate successfully; of livestock
