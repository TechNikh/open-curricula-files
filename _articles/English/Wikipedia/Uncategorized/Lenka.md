---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lenka
offline_file: ""
offline_thumbnail: ""
uuid: a1fc34ef-5693-4bf2-8836-87d4838c500f
updated: 1484308463
title: Lenka
tags:
    - Early life
    - Career
    - Personal life
    - Filmography
    - Discography
categories:
    - Uncategorized
---
Lenka (born Lenka Kripac; 19 March 1978) is an Australian singer and actress best known for her song "The Show", from her self-titled album Lenka. Her song "Everything at Once" was used in a Windows 8 television advertisement and in a Disney Movie Rewards commercial.[4]
