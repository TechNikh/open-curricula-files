---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multi-level_technique
offline_file: ""
offline_thumbnail: ""
uuid: a9a03fe9-9a4c-493d-8d23-484c3a498786
updated: 1484308622
title: Multi-level technique
categories:
    - Graph theory
---
The idea of the multi-level technique is to reduce the magnitude of a graph by merging vertices together, compute a partition on this reduced graph, and finally project this partition on the original graph.
