---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dr
offline_file: ""
offline_thumbnail: ""
uuid: 2ea4f449-4ab2-45af-8a12-6397ad2ecbb0
updated: 1484310315
title: dr
categories:
    - Dictionary
---
Dr.
     n 1: a person who holds Ph.D. degree from an academic
          institution; "she is a doctor of philosophy in physics"
          [syn: {doctor}]
     2: a licensed medical practitioner; "I felt so bad I went to
        see my doctor" [syn: {doctor}, {doc}, {physician}, {MD}, {medico}]
