---
version: 1
type: video
provider: YouTube
id: tCHWAYW_GKc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 272860cb-eafd-4f7e-b959-c64a3ffe0ccc
updated: 1486069621
title: 'Clock Questions  Shortcut Trick for Competitive Exams'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/tCHWAYW_GKc/default.jpg
    - https://i3.ytimg.com/vi/tCHWAYW_GKc/1.jpg
    - https://i3.ytimg.com/vi/tCHWAYW_GKc/2.jpg
    - https://i3.ytimg.com/vi/tCHWAYW_GKc/3.jpg
---
