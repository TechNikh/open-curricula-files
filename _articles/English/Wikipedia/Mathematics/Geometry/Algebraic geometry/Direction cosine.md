---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Direction_cosine
offline_file: ""
offline_thumbnail: ""
uuid: 73fb1761-2c1f-4241-8981-1a81f1c3de6c
updated: 1484309142
title: Direction cosine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Direction_cosine_vector.svg.png
tags:
    - Three-dimensional Cartesian coordinates
    - General meaning
categories:
    - Algebraic geometry
---
In analytic geometry, the direction cosines (or directional cosines) of a vector are the cosines of the angles between the vector and the three coordinate axes. Equivalently, they are the contributions of each component of the basis to a unit vector in that direction.
