---
version: 1
type: video
provider: YouTube
id: L-Wtlev6suc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Naked%20Eye%20Observations-%20Crash%20Course%20Astronomy%20%232.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 55b3d018-2bd9-4cec-8328-930b94b034f1
updated: 1486069684
title: 'Naked Eye Observations- Crash Course Astronomy #2'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/L-Wtlev6suc/default.jpg
    - https://i3.ytimg.com/vi/L-Wtlev6suc/1.jpg
    - https://i3.ytimg.com/vi/L-Wtlev6suc/2.jpg
    - https://i3.ytimg.com/vi/L-Wtlev6suc/3.jpg
---
