---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pile
offline_file: ""
offline_thumbnail: ""
uuid: 97f9a2f3-c8e4-48fe-973e-4cd27e578d5d
updated: 1484310403
title: pile
categories:
    - Dictionary
---
pile
     n 1: a collection of objects laid on top of each other [syn: {heap},
           {mound}, {cumulus}]
     2: (often followed by `of') a large number or amount or extent;
        "a batch of letters"; "a deal of trouble"; "a lot of
        money"; "he made a mint on the stock market"; "it must
        have cost plenty" [syn: {batch}, {deal}, {flock}, {good
        deal}, {great deal}, {hatful}, {heap}, {lot}, {mass}, {mess},
         {mickle}, {mint}, {muckle}, {peck}, {plenty}, {pot}, {quite
        a little}, {raft}, {sight}, {slew}, {spate}, {stack}, {tidy
        sum}, {wad}, {whole lot}, {whole slew}]
     3: a large sum of money (especially as pay or profit); "she
        made ...
