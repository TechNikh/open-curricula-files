---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exhaust
offline_file: ""
offline_thumbnail: ""
uuid: f4a9344a-b514-4712-aa21-faac6fa77e5f
updated: 1484310537
title: exhaust
categories:
    - Dictionary
---
exhaust
     n 1: gases ejected from an engine as waste products [syn: {exhaust
          fumes}, {fumes}]
     2: system consisting of the parts of an engine through which
        burned gases or steam are discharged [syn: {exhaust system}]
     v 1: wear out completely; "This kind of work exhausts me"; "I'm
          beat"; "He was all washed up after the exam" [syn: {wash
          up}, {beat}, {tucker}, {tucker out}]
     2: use up (resources or materials); "this car consumes a lot of
        gas"; "We exhausted our savings"; "They run through 20
        bottles of wine a week" [syn: {consume}, {eat up}, {use up},
         {eat}, {deplete}, {run through}, {wipe out}]
     3: deplete; ...
