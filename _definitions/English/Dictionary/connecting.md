---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/connecting
offline_file: ""
offline_thumbnail: ""
uuid: 1ac13fa7-5a2e-403d-b009-3c9f2a33c5e4
updated: 1484310281
title: connecting
categories:
    - Dictionary
---
connecting
     adj 1: having a connection; "connecting rooms"
     2: syntactically connecting sentences or elements of a
        sentence; "`and' is a copulative conjunction" [syn: {copulative}]
