---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stop_watch
offline_file: ""
offline_thumbnail: ""
uuid: 93d4a1bd-cceb-48d1-aca9-0a1abbe6c2a9
updated: 1484308375
title: Stopwatch
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Stopwatch.gif
categories:
    - Uncategorized
---
A stopwatch is a handheld timepiece designed to measure the amount of time elapsed from a particular time when it is activated to the time when the piece is deactivated. A large digital version of a stopwatch designed for viewing at a distance, as in a sports stadium, is called a stopclock. In manual timing, the clock is started and stopped by a person pressing a button. In fully automatic time, both starting and stopping are triggered automatically, by sensors.
