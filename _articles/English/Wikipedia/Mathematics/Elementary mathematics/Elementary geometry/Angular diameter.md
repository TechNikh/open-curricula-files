---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Angular_diameter
offline_file: ""
offline_thumbnail: ""
uuid: 26dd4999-c2cf-41b5-93e3-b16fcd7a4fd7
updated: 1484308925
title: Angular diameter
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Angular_dia_formula.JPG
tags:
    - Formula
    - Estimating angular diameter using outstretched hand
    - Use in astronomy
    - Non-circular objects
    - Defect of illumination
categories:
    - Elementary geometry
---
The angular diameter or apparent size is an angular measurement describing how large a sphere or circle appears from a given point of view. In the vision sciences it is called the visual angle and in optics it is the angular aperture (of a lens). The angular diameter can alternatively be thought of as the angle through which an eye or camera must rotate to look from one side of an apparent circle to the opposite side. Angular radius equals half the angular diameter.
