---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liberalise
offline_file: ""
offline_thumbnail: ""
uuid: c839a389-603f-4c96-aa4c-167f9c1f45bd
updated: 1484310527
title: liberalise
categories:
    - Dictionary
---
liberalise
     v 1: become more liberal; "The laws liberalized after
          Prohibition" [syn: {liberalize}]
     2: make liberal or more liberal, of laws and rules [syn: {liberalize}]
