---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thailand
offline_file: ""
offline_thumbnail: ""
uuid: b26793ad-d029-4afe-8080-2ce618f2f12d
updated: 1484308450
title: Thailand
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-King_Mongkut_%2528Rama_IV%2529_of_Siam_Signature_%2528English%2529.svg.png'
tags:
    - Etymology
    - Etymology of "Siam"
    - Etymology of "Thailand"
    - History
    - 20th century
    - World War II
    - Politics and government
    - Constitutional history
    - 28 June 1932
    - 1932 to 1973
    - 1973 to 1997
    - 1997 to 2001
    - 2001 to 2008
    - "2006 coup d'état"
    - 2008–2010 political crisis
    - 2013–2014 political crisis
    - "2014 coup d'état"
    - 2014 to present
    - Administrative divisions
    - Southern region
    - Foreign relations
    - Armed forces
    - geography
    - Climate
    - Environment
    - wildlife
    - Education
    - science and technology
    - Internet
    - Economy
    - Recent economic history
    - Exports and manufacturing
    - Tourism
    - Agriculture
    - Energy
    - Transportation
    - Demographics
    - Ethnic groups
    - Population centres
    - language
    - religion
    - culture
    - Cuisine
    - Media
    - Units of measurement
    - Sports
    - Sporting venues
    - Sport events
    - Multi-sport event
    - International sports federations events
    - Beauty pageants
    - "Women's pageants"
    - International rankings
    - Bibliography
categories:
    - Uncategorized
---
Thailand (/ˈtaɪlænd/ TY-land or /ˈtaɪlənd/ TY-lənd;[13] Thai: ประเทศไทย, rtgs: Prathet Thai, pronounced [pra.tʰêːt tʰaj] ( listen)), officially the Kingdom of Thailand (Thai: ราชอาณาจักรไทย, rtgs: Ratcha-anachak Thai  [râːt.t͡ɕʰa.ʔaː.naː.t͡ɕàk tʰaj] ( listen)), formerly known as Siam (Thai: สยาม, rtgs: Sayam  [sa.jǎːm]), is a country at the centre of the Indochinese peninsula in Southeast Asia. With a total area of approximately 513,000 km2 (198,000 sq mi), Thailand is the world's 51st-largest country. It is the 20th-most-populous country in the world, with around 66 million people. The capital and ...
