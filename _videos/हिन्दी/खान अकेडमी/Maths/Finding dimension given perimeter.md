---
version: 1
type: video
provider: YouTube
id: 0XHGulAEs5Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fb82af85-7f49-41cb-9802-74a30df6374a
updated: 1486069614
title: Finding dimension given perimeter
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/0XHGulAEs5Q/default.jpg
    - https://i3.ytimg.com/vi/0XHGulAEs5Q/1.jpg
    - https://i3.ytimg.com/vi/0XHGulAEs5Q/2.jpg
    - https://i3.ytimg.com/vi/0XHGulAEs5Q/3.jpg
---
