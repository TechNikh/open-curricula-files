---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/troposphere
offline_file: ""
offline_thumbnail: ""
uuid: 82687391-190c-4152-96ae-3176949c5415
updated: 1484310162
title: troposphere
categories:
    - Dictionary
---
troposphere
     n : the lowest atmospheric layer; from 4 to 11 miles high
         (depending on latitude)
