---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/steadily
offline_file: ""
offline_thumbnail: ""
uuid: 8403f6a2-2da9-4870-967a-e011d8ebfb92
updated: 1484310409
title: steadily
categories:
    - Dictionary
---
steadily
     adv 1: at a steady rate or pace; "his interest eroded steadily"
     2: in a steady manner; "he could still walk steadily" [syn: {steady}]
        [ant: {unsteadily}]
