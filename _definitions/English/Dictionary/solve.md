---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solve
offline_file: ""
offline_thumbnail: ""
uuid: 1d9c4953-8faf-4d77-8356-40ab25d57ff6
updated: 1484310220
title: solve
categories:
    - Dictionary
---
solve
     v 1: find the solution to (a problem or question) or understand
          the meaning of; "did you solve the problem?"; "Work out
          your problems with the boss"; "this unpleasant situation
          isn't going to work itself out"; "did you get it?"; "Did
          you get my meaning?"; "He could not work the math
          problem" [syn: {work out}, {figure out}, {puzzle out}, {lick},
           {work}]
     2: find the solution; "solve an equation"; "solve for x" [syn:
        {resolve}]
     3: settle, as of a debt; "clear a debt"; "solve an old debt"
        [syn: {clear}]
