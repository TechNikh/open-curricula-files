---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parity-check_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 91e8fa56-a475-408d-a9e8-9ae69a404f7b
updated: 1484308547
title: Parity-check matrix
tags:
    - Definition
    - Creating a parity check matrix
    - Syndromes
    - Notes
categories:
    - Coding theory
---
In coding theory, a parity-check matrix of a linear block code C is a matrix which describes the linear relations that the components of a codeword must satisfy. It can be used to decide whether a particular vector is a codeword and is also used in decoding algorithms.
