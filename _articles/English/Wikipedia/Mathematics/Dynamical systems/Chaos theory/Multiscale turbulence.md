---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiscale_turbulence
offline_file: ""
offline_thumbnail: ""
uuid: 7d95b365-c9ff-4419-a0cf-ff75f91c5fe6
updated: 1484308719
title: Multiscale turbulence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/page1-220px-Fractalgrids.pdf.jpg
categories:
    - Chaos theory
---
Multiscale turbulence is a class of turbulent flows in which the chaotic motion of the fluid is forced at different length and/or time scales.[1][2] This is usually achieved by immersing in a moving fluid a body with a multiscale, often fractal-like, arrangement of length scales. This arrangement of scales can be either passive[3][4] or active[5]
