---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Queen_and_pawn_versus_queen_endgame
offline_file: ""
offline_thumbnail: ""
uuid: ac59ae30-6cef-4c83-b54d-70bffbd695dd
updated: 1484309017
title: Queen and pawn versus queen endgame
tags:
    - History
    - General considerations
    - Rook pawn
    - Knight pawn
    - Bishop pawn
    - Central pawn
    - Examples from games
    - Botvinnik vs. Minev
    - Botvinnik vs. Ravinsky
    - Queen and two pawns versus a queen
    - Queen and two pawns versus a queen and pawn
categories:
    - Chess theory
---
The queen and pawn versus queen endgame is a chess endgame in which both sides have a queen and one side has a pawn, which he is trying to promote. It is very complicated and difficult to play. Cross-checks are often used as a device to win the game by forcing the exchange of queens. It is almost always a draw if the defending king is in front of the pawn (Nunn 2007:148).
