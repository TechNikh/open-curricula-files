---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complete_metric_space
offline_file: ""
offline_thumbnail: ""
uuid: f7da222e-9876-4920-b3ef-987385decadd
updated: 1484309265
title: Complete metric space
tags:
    - Examples
    - Some theorems
    - Completion
    - Topologically complete spaces
    - Alternatives and generalizations
    - Notes
categories:
    - Metric geometry
---
In mathematical analysis, a metric space M is called complete (or a Cauchy space) if every Cauchy sequence of points in M has a limit that is also in M or, alternatively, if every Cauchy sequence in M converges in M.
