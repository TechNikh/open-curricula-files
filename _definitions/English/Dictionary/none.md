---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/none
offline_file: ""
offline_thumbnail: ""
uuid: 3b8cbc49-bf11-46a3-bea4-984aa234f029
updated: 1484310194
title: none
categories:
    - Dictionary
---
none
     adj : not any; "thou shalt have none other gods before me"
     n 1: a canonical hour that is the ninth hour of the day counting
          from sunrise
     2: a service in the Roman Catholic Church formerly read or
        chanted at 3 PM (the ninth hour counting from sunrise) but
        now somewhat earlier
     adv : not at all or in no way; "seemed none too pleased with his
           dinner"; "shirt looked none the worse for having been
           slept in"; "none too prosperous"; "the passage is none
           too clear"
