---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quasisymmetric_map
offline_file: ""
offline_thumbnail: ""
uuid: f4335647-cf4b-4708-9143-8e22d112def0
updated: 1484309272
title: Quasisymmetric map
tags:
    - Definition
    - Basic properties
    - Examples
    - Weakly quasisymmetric maps
    - δ-monotone maps
    - Quasisymmetric maps and doubling measures
    - The real line
    - Euclidean space
    - Quasisymmetry and quasiconformality in Euclidean space
categories:
    - Metric geometry
---
In mathematics, a quasisymmetric homeomorphism between metric spaces is a map that generalizes bi-Lipschitz maps. While bi-Lipschitz maps shrink or expand the diameter of a set by no more than a multiplicative factor, quasisymmetric maps satisfy the weaker geometric property that they preserve the relative sizes of sets: if two sets A and B have diameters t and are no more than distance t apart, then the ratio of their sizes changes by no more than a multiplicative constant. These maps are also related to quasiconformal maps, since in many circumstances they are in fact equivalent.[1]
