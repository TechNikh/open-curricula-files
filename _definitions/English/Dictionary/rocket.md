---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rocket
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484320441
title: rocket
categories:
    - Dictionary
---
rocket
     n 1: any vehicle propelled by a rocket engine
     2: a jet engine containing its own propellant and driven by
        reaction propulsion [syn: {rocket engine}]
     3: erect European annual often grown as a salad crop to be
        harvested when young and tender [syn: {roquette}, {garden
        rocket}, {rocket salad}, {arugula}, {Eruca sativa}, {Eruca
        vesicaria sativa}]
     4: propels bright light high in the sky, or used to propel a
        lifesaving line or harpoon [syn: {skyrocket}]
     5: sends a firework display high into the sky [syn: {skyrocket}]
     v 1: shoot up abruptly, like a rocket; "prices skyrocketed" [syn:
           {skyrocket}]
     2: propel ...
