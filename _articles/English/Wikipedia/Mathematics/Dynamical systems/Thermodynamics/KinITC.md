---
version: 1
type: article
id: https://en.wikipedia.org/wiki/KinITC
offline_file: ""
offline_thumbnail: ""
uuid: e06361ed-78b9-4ce6-a51d-6b66a59b5809
updated: 1484308849
title: KinITC
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-KinITC_logo.png
tags:
    - What is kinITC ?
    - Basic reminder on ITC
    - How to overcome this limitation ?
    - 'Simplified analysis for the evaluation of d[C]/dt'
    - The practical problems
    - >
        Obtaining the kinetic parameters kon and koff in simple
        situations
    - The interest of kinITC for more complex situations
categories:
    - Thermodynamics
---
