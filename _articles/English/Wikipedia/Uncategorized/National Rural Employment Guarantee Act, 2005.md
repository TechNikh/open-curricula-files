---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mgnrega
offline_file: ""
offline_thumbnail: ""
uuid: 57cee882-7637-4729-8397-2ccd5beb5455
updated: 1484308458
title: National Rural Employment Guarantee Act, 2005
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-NREGA2_page_1.png
tags:
    - Past Scenario
    - History
    - Overview
    - The law and the Constitution of India
    - The law in action
    - Independent Academic Research
    - Assessment of the act by the constitutional auditor
    - Evaluation of the law by the government
    - Social audit
    - Save MGNREGA
    - New Amendments Proposed in 2014
    - Controversies
    - Corruption
    - Notes
categories:
    - Uncategorized
---
National Rural Employment Guarantee Act 2005 (or, NREGA No 42) was later renamed as the "Mahatma Gandhi National Rural Employment Guarantee Act" (or, MGNREGA), is an Indian labour law and social security measure that aims to guarantee the 'right to work'. It aims to enhance livelihood security in rural areas by providing at least 100 days of wage employment in a financial year to every household whose adult members volunteer to do unskilled manual work.[1][2]
