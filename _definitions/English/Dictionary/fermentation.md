---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fermentation
offline_file: ""
offline_thumbnail: ""
uuid: 04cff44e-69a5-4427-b998-87f16406763c
updated: 1484310424
title: fermentation
categories:
    - Dictionary
---
fermentation
     n 1: a process in which an agent causes an organic substance to
          break down into simpler substances; especially, the
          anaerobic breakdown of sugar into alcohol [syn: {zymosis},
           {zymolysis}, {fermenting}, {ferment}]
     2: a state of agitation or turbulent change or development;
        "the political ferment produced a new leadership"; "social
        unrest" [syn: {agitation}, {ferment}, {unrest}]
     3: a chemical phenomenon in which an organic molecule splits
        into simpler substances [syn: {ferment}]
