---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/again
offline_file: ""
offline_thumbnail: ""
uuid: eb2f90b0-fbb4-492d-b2d3-68734a9743bc
updated: 1484310353
title: again
categories:
    - Dictionary
---
again
     adv : anew; "she tried again"; "they rehearsed the scene again"
           [syn: {once again}, {once more}, {over again}]
