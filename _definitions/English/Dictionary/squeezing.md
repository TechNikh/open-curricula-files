---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/squeezing
offline_file: ""
offline_thumbnail: ""
uuid: 6caeef55-759c-4025-9fd3-7b46cfda80c8
updated: 1484310337
title: squeezing
categories:
    - Dictionary
---
squeezing
     n : the act of gripping and pressing firmly; "he gave her cheek
         a playful squeeze" [syn: {squeeze}]
