---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_(physics)
offline_file: ""
offline_thumbnail: ""
uuid: b903d885-3672-4c91-be28-142a250d8a23
updated: 1484309317
title: Symmetry (physics)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Brillouin_Zone_%25281st%252C_FCC%2529.svg.png'
tags:
    - Symmetry as a kind of invariance
    - Invariance in force
    - Local and global symmetries
    - Continuous symmetries
    - Spacetime symmetries
    - Discrete symmetries
    - C, P, and T symmetries
    - Supersymmetry
    - Mathematics of physical symmetry
    - Conservation laws and symmetry
    - Mathematics
    - General readers
    - Technical readers
categories:
    - Symmetry
---
In physics, a symmetry of a physical system is a physical or mathematical feature of the system (observed or intrinsic) that is preserved or remains unchanged under some transformation.
