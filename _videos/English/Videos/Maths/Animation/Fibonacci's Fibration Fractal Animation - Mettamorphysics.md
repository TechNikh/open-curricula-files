---
version: 1
type: video
provider: YouTube
id: q3YFpHQz8dk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bd25e0e7-91a4-4274-860f-f9d4b1bdf9a4
updated: 1486069649
title: "Fibonacci's Fibration Fractal Animation - Mettamorphysics"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/q3YFpHQz8dk/default.jpg
    - https://i3.ytimg.com/vi/q3YFpHQz8dk/1.jpg
    - https://i3.ytimg.com/vi/q3YFpHQz8dk/2.jpg
    - https://i3.ytimg.com/vi/q3YFpHQz8dk/3.jpg
---
