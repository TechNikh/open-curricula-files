---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Present_value_of_revenues_auction
offline_file: ""
offline_thumbnail: ""
uuid: d41e3e73-40c2-40d3-ae9e-1f22246dd2b8
updated: 1484309020
title: Present value of revenues auction
categories:
    - Auctions
---
A present value of revenues auction, sometimes called a least present value of revenues auction, is a method of awarding contracts in which the bids are for the total present value of cash flows from the project. The lowest bid wins, and the winner collects user fees until the present value of user fees equals their bid.[1] This system has been proposed for private highway construction.[citation needed]
