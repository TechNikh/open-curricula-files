---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tonnage
offline_file: ""
offline_thumbnail: ""
uuid: f4cce87c-e3de-4c73-b45b-cc231e00440d
updated: 1484310579
title: tonnage
categories:
    - Dictionary
---
tonnage
     n : a tax imposed on ships that enter the US; based on the
         tonnage of the ship [syn: {tunnage}, {tonnage duty}]
