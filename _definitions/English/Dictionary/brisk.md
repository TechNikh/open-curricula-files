---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brisk
offline_file: ""
offline_thumbnail: ""
uuid: 03b24ad5-de19-440e-84b5-e3798a3f7024
updated: 1484310429
title: brisk
categories:
    - Dictionary
---
brisk
     adj 1: quick and energetic; "a brisk walk in the park"; "a lively
            gait"; "a merry chase"; "traveling at a rattling
            rate"; "a snappy pace"; "a spanking breeze" [syn: {lively},
             {merry}, {rattling}, {snappy}, {spanking}, {zippy}]
     2: imparting vitality and energy; "the bracing mountain air"
        [syn: {bracing}, {energizing}, {energising}, {fresh}, {refreshing},
         {refreshful}, {tonic}]
     3: very active; "doing a brisk business"
     v : become brisk; "business brisked up" [syn: {brisk up}, {brisken}]
