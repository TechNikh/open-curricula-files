---
version: 1
type: video
provider: YouTube
id: XEblO51pF5I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Functions%20Part%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 24fe64b7-e52b-4360-9e80-dabbb65cceb1
updated: 1486069575
title: Functions Part 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/XEblO51pF5I/default.jpg
    - https://i3.ytimg.com/vi/XEblO51pF5I/1.jpg
    - https://i3.ytimg.com/vi/XEblO51pF5I/2.jpg
    - https://i3.ytimg.com/vi/XEblO51pF5I/3.jpg
---
More examples of solving function problems
More free lessons at: http://www.khanacademy.org/video?v=XEblO51pF5I
