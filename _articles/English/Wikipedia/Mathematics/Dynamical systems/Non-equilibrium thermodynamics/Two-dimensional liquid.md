---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Two-dimensional_liquid
offline_file: ""
offline_thumbnail: ""
uuid: 8d55d403-2dc2-4b4b-961c-913bb4579e0c
updated: 1484308808
title: Two-dimensional liquid
tags:
    - Relations between 2D and 3D liquids
    - Relation to other states of aggregation
categories:
    - Non-equilibrium thermodynamics
---
