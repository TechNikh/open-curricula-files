---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swiftness
offline_file: ""
offline_thumbnail: ""
uuid: 6c8ae80f-6ff9-4d39-930f-cba681181a27
updated: 1484310144
title: swiftness
categories:
    - Dictionary
---
swiftness
     n : a rate (usually rapid) at which something happens; "the
         project advanced with gratifying speed" [syn: {speed}, {fastness}]
