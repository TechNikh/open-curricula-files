---
version: 1
type: video
provider: YouTube
id: w3IKEa_GOYs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Star%20field%20and%20nebula%20images.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ba8f202f-180a-42bb-8ba7-e68b77f3e3c8
updated: 1486069686
title: Star field and nebula images
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/w3IKEa_GOYs/default.jpg
    - https://i3.ytimg.com/vi/w3IKEa_GOYs/1.jpg
    - https://i3.ytimg.com/vi/w3IKEa_GOYs/2.jpg
    - https://i3.ytimg.com/vi/w3IKEa_GOYs/3.jpg
---
