---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rubber_elasticity
offline_file: ""
offline_thumbnail: ""
uuid: 79854a87-1717-440e-897b-f170d81e1eaa
updated: 1484308860
title: Rubber elasticity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/FJCpolymersmall.JPG
tags:
    - Thermodynamics
    - Models
    - Freely-jointed chain model
    - Worm-like chain model
    - History
categories:
    - Thermodynamics
---
