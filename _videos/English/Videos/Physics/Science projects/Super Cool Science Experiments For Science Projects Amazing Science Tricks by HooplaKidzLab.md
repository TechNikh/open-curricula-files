---
version: 1
type: video
provider: YouTube
id: Hse_5NAUqUQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7bfe0ef7-0cc0-4fb3-82d8-671e33723f21
updated: 1486069634
title: >
    Super Cool Science Experiments For Science Projects Amazing
    Science Tricks by HooplaKidzLab
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/Hse_5NAUqUQ/default.jpg
    - https://i3.ytimg.com/vi/Hse_5NAUqUQ/1.jpg
    - https://i3.ytimg.com/vi/Hse_5NAUqUQ/2.jpg
    - https://i3.ytimg.com/vi/Hse_5NAUqUQ/3.jpg
---
