---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hopf_bifurcation
offline_file: ""
offline_thumbnail: ""
uuid: 3c7d2e6b-3c28-4b72-b701-38bc4357ccf4
updated: 1484308702
title: Hopf bifurcation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Hopf-bif.gif
tags:
    - Overview
    - Supercritical and subcritical Hopf bifurcations
    - Remarks
    - Example
    - Definition of a Hopf bifurcation
    - Routh–Hurwitz criterion
    - Sturm series
    - Propositions
    - Example
categories:
    - Bifurcation theory
---
In the mathematical theory of bifurcations, a Hopf bifurcation is a critical point where a system's stability switches and a periodic solution arises.[1] More accurately, it is a local bifurcation in which a fixed point of a dynamical system loses stability, as a pair of complex conjugate eigenvalues (of the linearization around the fixed point) cross the complex plane imaginary axis. Under reasonably generic assumptions about the dynamical system,a small-amplitude limit cycle branches from the fixed point.
