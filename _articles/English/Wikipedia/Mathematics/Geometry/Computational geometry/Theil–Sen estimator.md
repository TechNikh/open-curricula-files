---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Theil%E2%80%93Sen_estimator'
offline_file: ""
offline_thumbnail: ""
uuid: 0f57bb18-0e1e-4693-bedd-eddd155e8ce6
updated: 1484309511
title: Theil–Sen estimator
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Thiel-Sen_estimator.svg.png
tags:
    - Definition
    - Variations
    - Statistical properties
    - Algorithms
    - Applications
    - Notes
categories:
    - Computational geometry
---
In non-parametric statistics, there is a method for robustly fitting a line to a set of points (simple linear regression) that chooses the median slope among all lines through pairs of two-dimensional sample points. It has been called the Theil–Sen estimator, Sen's slope estimator,[1][2] slope selection,[3][4] the single median method,[5] the Kendall robust line-fit method,[6] and the Kendall–Theil robust line.[7] It is named after Henri Theil and Pranab K. Sen, who published papers on this method in 1950 and 1968 respectively, and after Maurice Kendall.
