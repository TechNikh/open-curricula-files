---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entrapped
offline_file: ""
offline_thumbnail: ""
uuid: 178680e2-24f4-43a1-8925-3624b46baf31
updated: 1484310426
title: entrapped
categories:
    - Dictionary
---
entrap
     v 1: take or catch as if in a snare or trap; "I was set up!";
          "The innocent man was framed by the police" [syn: {ensnare},
           {frame}, {set up}]
     2: catch in or as if in a trap; "The men trap foxes" [syn: {trap},
         {snare}, {ensnare}, {trammel}]
     [also: {entrapping}, {entrapped}]
