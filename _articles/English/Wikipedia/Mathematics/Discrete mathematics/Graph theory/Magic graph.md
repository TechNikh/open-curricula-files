---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magic_graph
offline_file: ""
offline_thumbnail: ""
uuid: 2eda92e9-9284-4212-b4c0-0029d0a16bb7
updated: 1484308625
title: Magic graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-4x4_magic_square_hierarchy.svg.png
categories:
    - Graph theory
---
A magic graph is a graph whose edges are labelled by positive integers, so that the sum over the edges incident with any vertex is the same, independent of the choice of vertex; or it is a graph that has such a labelling. If the integers are the first q positive integers, where q is the number of edges, the graph and the labelling are called supermagic.
