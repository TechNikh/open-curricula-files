---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Absolute_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 45594f3e-04b3-498c-b57f-bc9e3906f373
updated: 1484309168
title: Absolute geometry
tags:
    - Properties
    - Relation to other geometries
    - Incompleteness
    - Notes
categories:
    - Classical geometry
---
Absolute geometry is a geometry based on an axiom system for Euclidean geometry with the parallel postulate removed and none of its alternatives used in place of it.[1] The term was introduced by János Bolyai in 1832.[2] It is sometimes referred to as neutral geometry,[3] as it is neutral with respect to the parallel postulate.
