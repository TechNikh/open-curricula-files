---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Noether%27s_second_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: dd3a0b36-cb5d-46f7-aa26-e81edc657c0f
updated: 1484309313
title: "Noether's second theorem"
tags:
    - Notes
categories:
    - Symmetry
---
In mathematics and theoretical physics, Noether's second theorem relates symmetries of an action functional with a system of differential equations.[1] The action S of a physical system is an integral of a so-called Lagrangian function L, from which the system's behavior can be determined by the principle of least action.
