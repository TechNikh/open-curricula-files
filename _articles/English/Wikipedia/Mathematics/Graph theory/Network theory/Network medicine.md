---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Network_medicine
offline_file: ""
offline_thumbnail: ""
uuid: 046178a8-70db-4466-aefe-43547924cb45
updated: 1484309440
title: Network medicine
tags:
    - Background
    - Relevant areas
    - Interactome
    - Diseasome
    - Pharmacology
    - Network epidemics
    - Implementation
categories:
    - Network theory
---
Network medicine is the application of network science towards identifying, preventing, and treating diseases. This field focuses on using network topology and network dynamics towards identifying diseases and developing medical drugs. Biological networks, such as protein-protein interactions and metabolic pathways, are utilized by network medicine. Disease networks, which map relationships between diseases and biological factors, also play an important role in the field. Epidemiology is extensively studied using network science as well; social networks and transportation networks are used to model the spreading of disease across populations.
