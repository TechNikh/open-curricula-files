---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Masreliez%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: e6c883fd-5f6a-46f8-b57d-5cee2cd41217
updated: 1484308743
title: "Masreliez's theorem"
categories:
    - Control engineering
---
Masreliez theorem describes a recursive algorithm within the technology of extended Kalman filter, named after the Swedish-American physicist John Masreliez, who is its author.[1] The algorithm estimates the state of a dynamic system with the help of often incomplete measurements marred by distortion.[2]
