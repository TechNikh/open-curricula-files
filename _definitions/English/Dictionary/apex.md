---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apex
offline_file: ""
offline_thumbnail: ""
uuid: 344854ec-44c1-4e01-a646-46079442b74f
updated: 1484310407
title: apex
categories:
    - Dictionary
---
apex
     n 1: the highest point (of something); "at the peak of the
          pyramid" [syn: {vertex}, {peak}, {acme}]
     2: the point on the celestial sphere toward which the sun and
        solar system appear to be moving relative to the fixed
        stars [syn: {solar apex}, {apex of the sun's way}] [ant: {antapex}]
     [also: {apices} (pl)]
