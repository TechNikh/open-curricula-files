---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/low
offline_file: ""
offline_thumbnail: ""
uuid: cdd329b0-0291-4282-b29a-f4c9cde0f76c
updated: 1484310293
title: low
categories:
    - Dictionary
---
low
     adj 1: less than normal in degree or intensity or amount; "low
            prices"; "the reservoire is low" [ant: {high}]
     2: literal meanings; being at or having a relatively small
        elevation or upward extension; "low ceilings"; "low
        clouds"; "low hills"; "the sun is low"; "low furniture";
        "a low bow" [ant: {high}]
     3: very low in volume; "a low murmur"; "the low-toned murmur of
        the surf" [syn: {low-toned}]
     4: unrefined in character; "low comedy"
     5: used of sounds and voices; low in pitch or frequency [syn: {low-pitched}]
        [ant: {high}]
     6: of the most contemptible kind; "abject cowardice"; "a low
        stunt to pull"; ...
