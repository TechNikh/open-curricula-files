---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mikhail
offline_file: ""
offline_thumbnail: ""
uuid: 7b856c64-2457-4774-848f-d79860224db8
updated: 1484308479
title: Michael
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Guido_Reni_031.jpg
tags:
    - religion
    - Popularity
categories:
    - Uncategorized
---
Michael /ˈmaɪkəl/ is a male given name that comes from Hebrew: מִיכָאֵל / מיכאל‎‎ (Mīkhāʼēl, pronounced [miχaˈʔel]), derived from the question מי כאל mī kāʼēl, meaning "Who is like God?".[1] In English, it is sometimes shortened to Mike, Mikey, Mickey, Micha, or Mick.
