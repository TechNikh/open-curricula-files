---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drier
offline_file: ""
offline_thumbnail: ""
uuid: 1d4217b0-b154-40ef-b2fa-459e28e09a87
updated: 1484310435
title: drier
categories:
    - Dictionary
---
drier
     n 1: a substance that promotes drying (e.g., calcium oxide
          absorbs water and is used to remove moisture) [syn: {desiccant},
           {drying agent}, {sicative}]
     2: an appliance that removes moisture [syn: {dryer}]
