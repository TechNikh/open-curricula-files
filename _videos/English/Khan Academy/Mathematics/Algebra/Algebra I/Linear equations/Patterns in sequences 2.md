---
version: 1
type: video
provider: YouTube
id: zIcxrhyJs6M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Patterns%20in%20sequences%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 932c6fc6-7372-454e-9cfc-3a6cf1fdc02e
updated: 1486069563
title: Patterns in sequences 2
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/zIcxrhyJs6M/default.jpg
    - https://i3.ytimg.com/vi/zIcxrhyJs6M/1.jpg
    - https://i3.ytimg.com/vi/zIcxrhyJs6M/2.jpg
    - https://i3.ytimg.com/vi/zIcxrhyJs6M/3.jpg
---
