---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/puzzle
offline_file: ""
offline_thumbnail: ""
uuid: 3319afca-557c-41b7-999e-f78c3812741d
updated: 1484310152
title: puzzle
categories:
    - Dictionary
---
puzzle
     n 1: a particularly baffling problem that is said to have a
          correct solution; "he loved to solve chessmate puzzles";
          "that's a real puzzler" [syn: {puzzler}, {mystifier}, {teaser}]
     2: a toy that tests your ingenuity
     v 1: be a mystery or bewildering to; "This beats me!"; "Got me--I
          don't know the answer!"; "a vexing problem"; "This
          question really stuck me" [syn: {perplex}, {vex}, {stick},
           {get}, {mystify}, {baffle}, {beat}, {pose}, {bewilder},
           {flummox}, {stupefy}, {nonplus}, {gravel}, {amaze}, {dumbfound}]
     2: be uncertain about; think about without fully understanding
        or being able to decide; ...
