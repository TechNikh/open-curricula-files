---
version: 1
type: video
provider: YouTube
id: GifspsU2kps
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9122e25f-dadc-47d9-b0f4-f035d4023f9f
updated: 1486069640
title: '[Cbse Class 12] Prism Deviation Physics Practical Experiment Video'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/GifspsU2kps/default.jpg
    - https://i3.ytimg.com/vi/GifspsU2kps/1.jpg
    - https://i3.ytimg.com/vi/GifspsU2kps/2.jpg
    - https://i3.ytimg.com/vi/GifspsU2kps/3.jpg
---
