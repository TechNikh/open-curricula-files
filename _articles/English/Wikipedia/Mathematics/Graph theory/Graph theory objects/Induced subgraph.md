---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Induced_subgraph
offline_file: ""
offline_thumbnail: ""
uuid: d1ad4dbf-594a-4aaf-b8bf-5c33dfb13652
updated: 1484309445
title: Induced subgraph
tags:
    - Definition
    - Examples
    - Computation
categories:
    - Graph theory objects
---
In graph theory, an induced subgraph of a graph is another graph, formed from a subset of the vertices of the graph and all of the edges connecting pairs of vertices in that subset.
