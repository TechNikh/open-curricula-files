---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Accretion_disk
offline_file: ""
offline_thumbnail: ""
uuid: 8c2cd077-67cb-4545-a834-5eca5058fcf6
updated: 1484308875
title: Accretion disk
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/370px-ACC_Map_crop_2014.png
tags:
    - Manifestations
    - Accretion disk physics
    - α-Disk Model
    - Magnetorotational instability
    - Magnetic fields and jets
    - >
        Analytic models of sub-Eddington accretion disks (thin
        disks, ADAFs)
    - >
        Analytic models of super-Eddington accretion disks (slim
        disks, Polish doughnuts)
    - Excretion disk
categories:
    - Vortices
---
An accretion disk is a structure (often a circumstellar disk) formed by diffused material in orbital motion around a massive central body. The central body is typically a star. Gravity causes material in the disk to spiral inward towards the central body. Gravitational and frictional forces compress and raise the temperature of the material, causing the emission of electromagnetic radiation. The frequency range of that radiation depends on the central object's mass. Accretion disks of young stars and protostars radiate in the infrared; those around neutron stars and black holes in the X-ray part of the spectrum. The study of oscillation modes in accretion disks is referred to as ...
