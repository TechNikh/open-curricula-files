---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plateau
offline_file: ""
offline_thumbnail: ""
uuid: 27afd4b6-dd2f-4501-8547-a9ec965ab987
updated: 1484310433
title: plateau
categories:
    - Dictionary
---
plateau
     n : a relatively flat highland [syn: {tableland}]
     [also: {plateaux} (pl)]
