---
version: 1
type: article
id: https://en.wikipedia.org/wiki/CAT(k)_space
offline_file: ""
offline_thumbnail: ""
uuid: 46b3c91d-9604-4f77-886a-05fe0d6181d0
updated: 1484309261
title: CAT(k) space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-End_of_universe.jpg
tags:
    - Definitions
    - Examples
    - Hadamard spaces
    - |
        Properties of
        
        
        
        CAT
        ⁡
        (
        k
        )
        
        
        {\displaystyle \operatorname {CAT} (k)}
        
        spaces
categories:
    - Metric geometry
---
In mathematics, a 
  
    
      
        
          
            
              CAT
            
          
          ⁡
          (
          k
          )
        
      
    
    {\displaystyle \mathbf {\operatorname {\textbf {CAT}} (k)} }
  
 space, where 
  
    
      
        k
      
    
    {\displaystyle k}
  
 is a real number, is a specific type of metric space. Intuitively, triangles in a 
  
    
      
        CAT
        ⁡
        (
        k
        )
      
    
    {\displaystyle \operatorname {CAT} (k)}
  
 space are "slimmer" than corresponding "model triangles" in a standard space of constant curvature 
  
    
      
        k
      
    
    {\displaystyle k}
  ...
