---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Brahmagupta%E2%80%93Fibonacci_identity'
offline_file: ""
offline_thumbnail: ""
uuid: 77aec1cc-e774-4bb4-ad57-978fbe047b13
updated: 1484308909
title: Brahmagupta–Fibonacci identity
tags:
    - History
    - Related identities
    - Relation to complex numbers
    - Interpretation via norms
    - "Application to Pell's equation"
categories:
    - Elementary algebra
---
In algebra, the Diophantus–Fibonacci identity, Brahmagupta-Fibonacci identity or simply Fibonacci's identity (and in fact due to Diophantus of Alexandria) says that the product of two sums each of two squares is itself a sum of two squares. In other words, the set of all sums of two squares is closed under multiplication. Specifically:
