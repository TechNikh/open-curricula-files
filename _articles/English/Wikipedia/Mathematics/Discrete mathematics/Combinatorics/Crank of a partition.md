---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crank_of_a_partition
offline_file: ""
offline_thumbnail: ""
uuid: 83d136fe-cfd0-4834-85bc-a07a15c38dfb
updated: 1484308556
title: Crank of a partition
tags:
    - "Dyson's crank"
    - Definition of crank
    - Notations
    - Basic result
    - Ramanujan and cranks
categories:
    - Combinatorics
---
In number theory, the crank of a partition of an integer is a certain integer associated with the partition. The term was first introduced without a definition by Freeman Dyson in a 1944 paper published in Eureka, a journal published by the Mathematics Society of Cambridge University.[1] Dyson then gave a list of properties this yet-to-be-defined quantity should have. In 1988, George E. Andrews and Frank Garvan discovered a definition for the crank satisfying the properties hypothesized for it by Dyson.[2]
