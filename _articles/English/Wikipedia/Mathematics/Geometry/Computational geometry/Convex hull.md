---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_hull
offline_file: ""
offline_thumbnail: ""
uuid: cf8b859e-9977-40e3-b5e2-f213100bc5a3
updated: 1484309173
title: Convex hull
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Extreme_points.svg.png
tags:
    - Definitions
    - Convex hull of a finite point set
    - Computation of convex hulls
    - Minkowski addition and convex hulls
    - Relations to other structures
    - Applications
    - Notes
categories:
    - Computational geometry
---
In mathematics, the convex hull or convex envelope of a set X of points in the Euclidean plane or Euclidean space is the smallest convex set that contains X. For instance, when X is a bounded subset of the plane, the convex hull may be visualized as the shape enclosed by a rubber band stretched around X.[1]
