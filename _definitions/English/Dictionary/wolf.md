---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wolf
offline_file: ""
offline_thumbnail: ""
uuid: dbd64b1d-70ec-4b48-a5e0-8832810f005f
updated: 1484310273
title: wolf
categories:
    - Dictionary
---
wolf
     n 1: any of various predatory carnivorous canine mammals of North
          America and Eurasia that usually hunt in packs
     2: Austrian composer (1860-1903) [syn: {Hugo Wolf}]
     3: German classical scholar who claimed that the Iliad and
        Odyssey were composed by several authors (1759-1824) [syn:
         {Friedrich August Wolf}]
     4: a man who is aggressive in making amorous advances to women
        [syn: {woman chaser}, {skirt chaser}, {masher}]
     5: a cruelly rapacious person [syn: {beast}, {savage}, {brute},
         {wildcat}]
     v : eat hastily; "The teenager wolfed down the pizza" [syn: {wolf
         down}]
     [also: {wolves} (pl)]
