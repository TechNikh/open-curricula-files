---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ooty
offline_file: ""
offline_thumbnail: ""
uuid: 6b511d84-d235-4546-8fd5-d80ad4e0a683
updated: 1484308422
title: Ooty
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ootacamund_Ooty_Map_1911_0.jpg
tags:
    - Etymology
    - History
    - Climate
    - Demographics
    - Administration and politics
    - Economy
    - Transport
    - Road
    - Rail
    - air
    - Education
    - Recreation
    - Places of interest
categories:
    - Uncategorized
---
Udhagamandalam (also Ootacamund ( listen (help·info))) and abbreviated as Udhagai and Ooty ( listen (help·info)) is a town and municipality in the Indian state of Tamil Nadu. It is located 80 km north of Coimbatore and is the capital of the Nilgiris district. It is a popular hill station located in the Nilgiri Hills.
