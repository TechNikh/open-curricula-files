---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inclined
offline_file: ""
offline_thumbnail: ""
uuid: e15b73a5-598c-493c-a4ca-2eea60f7f05b
updated: 1484310166
title: inclined
categories:
    - Dictionary
---
inclined
     adj 1: (often followed by `to') having a preference, disposition,
            or tendency; "wasn't inclined to believe the excuse";
            "inclined to be moody" [syn: {inclined(p)}] [ant: {disinclined}]
     2: at an angle to the horizontal or vertical position; "an
        inclined plane" [ant: {horizontal}, {vertical}]
     3: having made preparations; "prepared to take risks" [syn: {disposed(p)},
         {fain}, {inclined(p)}, {prepared}]
     4: used especially of the head or upper back; "a bent head and
        sloping shoulders" [syn: {bent}, {bowed}]
