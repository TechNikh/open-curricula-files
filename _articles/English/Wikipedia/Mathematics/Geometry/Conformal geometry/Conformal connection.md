---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conformal_connection
offline_file: ""
offline_thumbnail: ""
uuid: fc58879e-7048-4c07-979a-d3a19c72d79b
updated: 1484309185
title: Conformal connection
tags:
    - Normal Cartan connection
    - Formal definition
categories:
    - Conformal geometry
---
In conformal differential geometry, a conformal connection is a Cartan connection on an n-dimensional manifold M arising as a deformation of the Klein geometry given by the celestial n-sphere, viewed as the homogeneous space
