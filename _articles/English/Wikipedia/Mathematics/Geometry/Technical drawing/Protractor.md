---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Protractor
offline_file: ""
offline_thumbnail: ""
uuid: a38db6e0-e3a7-498e-8f60-7b8b8a67f832
updated: 1484309331
title: Protractor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Protractor2.jpg
tags:
    - Bevel protractor
    - Gallery
categories:
    - Technical drawing
---
A protractor is a measuring instrument, typically made of transparent plastic or glass, for measuring angles. Most protractors measure angles in degrees (°). Radian-scale protractors measure angles in radians. Most protractors are divided into 180 equal parts.
