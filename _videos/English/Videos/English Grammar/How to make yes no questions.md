---
version: 1
type: video
provider: YouTube
id: 5gye8cMLu2w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/How%20to%20make%20yes%20no%20questions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2cf4e06a-b43a-4a23-aff0-0f6473837b61
updated: 1486069632
title: How to make yes no questions
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/5gye8cMLu2w/default.jpg
    - https://i3.ytimg.com/vi/5gye8cMLu2w/1.jpg
    - https://i3.ytimg.com/vi/5gye8cMLu2w/2.jpg
    - https://i3.ytimg.com/vi/5gye8cMLu2w/3.jpg
---
