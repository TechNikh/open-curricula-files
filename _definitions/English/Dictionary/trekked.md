---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trekked
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484630821
title: trekked
categories:
    - Dictionary
---
trek
     n 1: a journey by ox wagon (especially an organized migration by
          a group of settlers)
     2: any long and difficult trip
     v 1: journey on foot, especially in the mountains; "We spent the
          summer trekking in the foothills of the Himalayas"
     2: make a long and difficult journey; "They trekked towards the
        North Pole with sleds and skis"
     [also: {trekking}, {trekked}]
