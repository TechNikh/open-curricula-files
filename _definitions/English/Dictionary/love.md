---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/love
offline_file: ""
offline_thumbnail: ""
uuid: 10f0266a-a01d-4bd9-892a-660db105ad05
updated: 1484310551
title: love
categories:
    - Dictionary
---
love
     n 1: a strong positive emotion of regard and affection; "his love
          for his work"; "children need a lot of love" [ant: {hate}]
     2: any object of warm affection or devotion; "the theater was
        her first love" or "he has a passion for cock fighting";
        [syn: {passion}]
     3: a beloved person; used as terms of endearment [syn: {beloved},
         {dear}, {dearest}, {loved one}, {honey}]
     4: a deep feeling of sexual desire and attraction; "their love
        left them indifferent to their surroundings"; "she was his
        first love"
     5: a score of zero in tennis or squash; "it was 40 love"
     6: sexual activities (often including sexual ...
