---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bus_bunching
offline_file: ""
offline_thumbnail: ""
uuid: 16bb6764-36b6-432e-8b36-3cca8fae532d
updated: 1484308712
title: Bus bunching
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-London_Bus_route_55_Buses%252C_Clapton_Pond.jpg'
tags:
    - Theory
    - Hypothesized causes
    - Abnormal passenger loads
    - Speed of individual drivers
    - Corrective measures
    - Prevention
categories:
    - Chaos theory
---
In public transport, bus bunching, clumping, convoying, or platooning refers to a group of two or more transit vehicles (such as buses or trains), which were scheduled to be evenly spaced running along the same route, instead running in the same location at the same time. This occurs when at least one of the vehicles is unable to keep to its schedule and therefore ends up in the same location as one or more other vehicles of the same route at the same time.
