---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/servant
offline_file: ""
offline_thumbnail: ""
uuid: 6e84bf1e-2c63-4787-b05c-b9624231f8bd
updated: 1484310484
title: servant
categories:
    - Dictionary
---
servant
     n 1: a person working in the service of another (especially in
          the household) [syn: {retainer}]
     2: in a subordinate position; "theology should be the
        handmaiden of ethics"; "the state cannot be a servant of
        the church" [syn: {handmaid}, {handmaiden}]
