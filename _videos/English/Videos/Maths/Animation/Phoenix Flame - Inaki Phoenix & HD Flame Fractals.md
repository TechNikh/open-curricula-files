---
version: 1
type: video
provider: YouTube
id: 0IeX8e0fKQs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 536dbc30-874f-4678-a4b9-d0e8479a46b1
updated: 1486069647
title: 'Phoenix Flame - Inaki Phoenix & HD Flame Fractals'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/0IeX8e0fKQs/default.jpg
    - https://i3.ytimg.com/vi/0IeX8e0fKQs/1.jpg
    - https://i3.ytimg.com/vi/0IeX8e0fKQs/2.jpg
    - https://i3.ytimg.com/vi/0IeX8e0fKQs/3.jpg
---
