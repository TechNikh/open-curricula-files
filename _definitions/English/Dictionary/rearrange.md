---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rearrange
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484322841
title: rearrange
categories:
    - Dictionary
---
rearrange
     v : put into a new order or arrangement; "Please rearrange these
         files"; "rearrange the furniture in my room"
