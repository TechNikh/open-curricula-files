---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Larmor_formula
offline_file: ""
offline_thumbnail: ""
uuid: b41fa7cc-5d2c-4e07-a845-fcde25485dea
updated: 1484308757
title: Larmor formula
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Montreal-tower-top.thumb2.jpg
tags:
    - Derivation
    - 'Derivation 1: Mathematical approach (using CGS units)'
    - 'Derivation 2: Edward M. Purcell approach'
    - Relativistic generalization
    - Covariant form
    - Non-covariant form
    - Angular distribution
    - Issues and implications
    - Radiation reaction
    - Atomic physics
    - Notes
categories:
    - Electrodynamics
---
The Larmor formula is used to calculate the total power radiated by a non relativistic point charge as it accelerates or decelerates. This is used in the branch of physics known as electrodynamics and is not to be confused with the Larmor precession from classical nuclear magnetic resonance. It was first derived by J. J. Larmor in 1897,[1] in the context of the wave theory of light.
