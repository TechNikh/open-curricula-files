---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Albertson_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 42f5daec-e3fd-409c-aa10-df944b61ccac
updated: 1484309376
title: Albertson conjecture
tags:
    - A conjectured formula for the minimum crossing number
    - Asymptotic bounds
    - Special cases
    - Related conjectures
    - Notes
categories:
    - Graph coloring
---
In combinatorial mathematics, the Albertson conjecture is an unproven relationship between the crossing number and the chromatic number of a graph. It is named after Michael O. Albertson, a professor at Smith College, who stated it as a conjecture in 2007;[1] it is one of his many conjectures in graph coloring theory.[2] The conjecture states that, among all graphs requiring n colors, the complete graph Kn is the one with the smallest crossing number. Equivalently, if a graph can be drawn with fewer crossings than Kn, then, according to the conjecture, it may be colored with fewer than n colors.
