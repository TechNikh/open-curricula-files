---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shade
offline_file: ""
offline_thumbnail: ""
uuid: a6e91c2c-8846-4c1e-b6b9-72c04743d80a
updated: 1484310521
title: shade
categories:
    - Dictionary
---
shade
     n 1: relative darkness caused by light rays being intercepted by
          an opaque body; "it is much cooler in the shade";
          "there's too much shadiness to take good photographs"
          [syn: {shadiness}, {shadowiness}]
     2: a quality of a given color that differs slightly from a
        primary color; "after several trials he mixed the shade of
        pink that she wanted" [syn: {tint}, {tincture}, {tone}]
     3: protective covering that protects something from direct
        sunlight; "they used umbrellas as shades"; "as the sun
        moved he readjusted the shade"
     4: a subtle difference in meaning or opinion or attitude;
        "without understanding ...
