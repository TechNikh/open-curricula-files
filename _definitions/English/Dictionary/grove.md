---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grove
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484324881
title: grove
categories:
    - Dictionary
---
grove
     n 1: a small growth of trees without underbrush
     2: garden consisting of a small cultivated wood without
        undergrowth [syn: {woodlet}, {orchard}, {plantation}]
