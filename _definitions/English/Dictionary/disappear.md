---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disappear
offline_file: ""
offline_thumbnail: ""
uuid: 3103079a-a863-454f-ad41-27d6cc2cae91
updated: 1484310228
title: disappear
categories:
    - Dictionary
---
disappear
     v 1: get lost, especially without warning or explanation; "He
          disappeared without a trace" [syn: {vanish}, {go away}]
          [ant: {appear}]
     2: become invisible or unnoticeable; "The effect vanished when
        day broke" [syn: {vanish}, {go away}]
     3: cease to exist; "An entire civilization vanished" [syn: {vanish}]
        [ant: {appear}]
     4: become less intense and fade away gradually; "her resistance
        melted under his charm" [syn: {melt}]
