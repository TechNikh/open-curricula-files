---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/functional
offline_file: ""
offline_thumbnail: ""
uuid: 369e7cc2-a2b8-467c-98d2-ea51b644c92d
updated: 1484310335
title: functional
categories:
    - Dictionary
---
functional
     adj 1: designed for or capable of a particular function or use; "a
            style of writing in which every word is functional";
            "functional architecture" [ant: {nonfunctional}]
     2: involving or affecting function rather than physiology;
        "functional deafness" [ant: {organic}]
     3: relating to or based on function especially as opposed to
        structure; "the problem now is not a constitutional one;
        it is a functional one"; "delegates elected on a
        functional rather than a geographical basis"
     4: fit or ready for use or service; "the toaster was still
        functional even after being dropped"; "the lawnmower is a
        ...
