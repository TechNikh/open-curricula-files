---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collectivisation
offline_file: ""
offline_thumbnail: ""
uuid: 538d0bd5-9996-4d1e-b556-74efaac36ed1
updated: 1484310567
title: collectivisation
categories:
    - Dictionary
---
collectivisation
     n : the organization of a nation or economy on the basis of
         collectivism [syn: {collectivization}]
