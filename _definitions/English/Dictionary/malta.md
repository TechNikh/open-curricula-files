---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/malta
offline_file: ""
offline_thumbnail: ""
uuid: 5f8c61c0-7a98-4cba-809a-772d606b9d21
updated: 1484310180
title: malta
categories:
    - Dictionary
---
Malta
     n 1: a republic on the island of Malta in the Mediterranean;
          achieved independence from the United Kingdom in 1964
          [syn: {Republic of Malta}]
     2: a strategically located island south of Sicily in the
        Mediterranean Sea
