---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dimensions_(animation)
offline_file: ""
offline_thumbnail: ""
uuid: 241ce6df-bbfe-4e83-a85f-a70d9b421a4a
updated: 1484309360
title: Dimensions (animation)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dimensions-math_4A-5.jpg
categories:
    - Geometry stubs
---
Dimensions is a French project that makes educational movies about mathematics, focusing on spatial geometry.[1] It uses POV-Ray to render some of the animations, and the films are released under a Creative Commons licence.
