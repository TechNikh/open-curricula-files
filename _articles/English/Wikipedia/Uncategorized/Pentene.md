---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pentene
offline_file: ""
offline_thumbnail: ""
uuid: eaf9c6be-d2a9-4ee9-afc0-834a43a92ce5
updated: 1484308398
title: Pentene
tags:
    - Straight-chain isomers
    - Branched-chain isomers
categories:
    - Uncategorized
---
Pentenes are alkenes with chemical formula C
5H
10. Each contains one double bond within its molecular structure. There are a total of six different compounds in this class, differing from each other by whether the carbon atoms are attached linearly or in a branched structure, and whether the double bond has a cis or trans form.
