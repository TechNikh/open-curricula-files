---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_Aumann
offline_file: ""
offline_thumbnail: ""
uuid: 05bc4464-05c5-413a-9c34-d293bbf45b1b
updated: 1484308518
title: Robert Aumann
tags:
    - Biography
    - Early years
    - Mathematical and scientific contribution
    - Political views
    - Torah codes controversy
    - Personal life
    - Honours and awards
    - Publications
categories:
    - Game theorists
---
Robert John Aumann (Hebrew name: ישראל אומן, Yisrael Aumann; born June 8, 1930) is an Israeli-American mathematician and a member of the United States National Academy of Sciences. He is a professor at the Center for the Study of Rationality in the Hebrew University of Jerusalem in Israel. He also holds a visiting position at Stony Brook University and is one of the founding members of the Stony Brook Center for Game Theory.
