---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homeomorphism_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 9364f4e9-d290-47d5-8440-c6f88bf9279d
updated: 1484308614
title: Homeomorphism (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/794px-Big_single-family_home_2.jpg
tags:
    - Subdivision and smoothing
    - Barycentric Subdivisions
    - Embedding on a surface
    - Example
categories:
    - Graph theory
---
In graph theory, two graphs 
  
    
      
        G
      
    
    {\displaystyle G}
  
 and 
  
    
      
        
          G
          ′
        
      
    
    {\displaystyle G'}
  
 are homeomorphic if there is a graph isomorphism from some subdivision of 
  
    
      
        G
      
    
    {\displaystyle G}
  
 to some subdivision of 
  
    
      
        
          G
          ′
        
      
    
    {\displaystyle G'}
  
. If the edges of a graph are thought of as lines drawn from one vertex to another (as they are usually depicted in illustrations), then two graphs are homeomorphic to each other in the graph-theoretic sense precisely if they are homeomorphic in ...
