---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mandate
offline_file: ""
offline_thumbnail: ""
uuid: b2a037fb-2e5a-4514-9532-6f9d14687246
updated: 1484310591
title: mandate
categories:
    - Dictionary
---
mandate
     n 1: a document giving an official instruction or command [syn: {authorization},
           {authorisation}]
     2: a territory surrendered by Turkey or Germany after World War
        I and put under the tutelage of some other European power
        until they ar able to stand by themselves [syn: {mandatory}]
     3: the commission that is given to a government and its
        policies through an electoral victory
     v 1: assign under a mandate; "mandate a colony"
     2: make mandatory; "the new director of the schoolbaord
        mandated regular tests"
     3: assign authority to
