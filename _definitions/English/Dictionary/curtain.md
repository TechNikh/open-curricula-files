---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curtain
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484444941
title: curtain
categories:
    - Dictionary
---
curtain
     n 1: hanging cloth used as a blind (especially for a window)
          [syn: {drape}, {drapery}, {mantle}, {pall}]
     2: any barrier to communication or vision; "a curtain of
        secrecy"; "a curtain of trees"
     v : provide with drapery; "curtain the bedrooms"
