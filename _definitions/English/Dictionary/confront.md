---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confront
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484626861
title: confront
categories:
    - Dictionary
---
confront
     v 1: oppose, as in hostility or a competition; "You must confront
          your opponent"; "Jackson faced Smith in the boxing
          ring"; "The two enemies finally confronted each other"
          [syn: {face}]
     2: deal with (something unpleasant) head on; "You must confront
        your problems"; "He faced the terrible consequences of his
        mistakes" [syn: {face up}, {face}] [ant: {avoid}]
     3: present somebody with something, usually to accuse or
        criticize; "We confronted him with the evidence"; "He was
        faced with all the evidence and could no longer deny his
        actions"; "An enormous dilemma faces us" [syn: {face}, {present}]
     4: ...
