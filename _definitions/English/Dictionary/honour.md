---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/honour
offline_file: ""
offline_thumbnail: ""
uuid: db6b239a-b474-405e-a512-18a895b8b502
updated: 1484310196
title: honour
categories:
    - Dictionary
---
honour
     n 1: the state of being honored [syn: {honor}, {laurels}] [ant: {dishonor}]
     2: a tangible symbol signifying approval or distinction; "an
        award for bravery" [syn: {award}, {accolade}, {honor}, {laurels}]
     3: the quality of being honorable and having a good name; "a
        man of honor" [syn: {honor}] [ant: {dishonor}]
     4: a woman's virtue or chastity [syn: {honor}, {purity}]
     v 1: bestow honor or rewards upon; "Today we honor our soldiers";
          "The scout was rewarded for courageus action" [syn: {honor},
           {reward}] [ant: {dishonor}]
     2: show respect towards; "honor your parents!" [syn: {respect},
         {honor}, {abide by}, ...
