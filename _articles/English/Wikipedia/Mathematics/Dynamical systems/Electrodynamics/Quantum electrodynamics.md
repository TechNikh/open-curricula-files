---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_electrodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 5a76b860-dfe3-466f-ad1d-7418e01ba4de
updated: 1484308759
title: Quantum electrodynamics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Dirac_3.jpg
tags:
    - History
    - "Feynman's view of quantum electrodynamics"
    - Introduction
    - Basic constructions
    - Probability amplitudes
    - Propagators
    - Mass renormalization
    - Conclusions
    - Mathematics
    - Equations of motion
    - Interaction picture
    - Feynman diagrams
    - Renormalizability
    - Nonconvergence of series
    - Books
    - Journals
categories:
    - Electrodynamics
---
In particle physics, quantum electrodynamics (QED) is the relativistic quantum field theory of electrodynamics. In essence, it describes how light and matter interact and is the first theory where full agreement between quantum mechanics and special relativity is achieved. QED mathematically describes all phenomena involving electrically charged particles interacting by means of exchange of photons and represents the quantum counterpart of classical electromagnetism giving a complete account of matter and light interaction.
