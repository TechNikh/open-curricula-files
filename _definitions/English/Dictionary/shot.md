---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shot
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484393101
title: shot
categories:
    - Dictionary
---
shoot
     n 1: a new branch
     2: the act of shooting at targets; "they hold a shoot every
        weekend during the summer"
     v 1: hit with a missile from a weapon [syn: {hit}, {pip}]
     2: kill by firing a missile [syn: {pip}]
     3: fire a shot
     4: make a film or photograph of something; "take a scene";
        "shoot a movie" [syn: {film}, {take}]
     5: send forth suddenly, intensely, swiftly; "shoot a glance"
     6: run or move very quickly or hastily; "She dashed into the
        yard" [syn: {dart}, {dash}, {scoot}, {scud}, {flash}]
     7: move quickly and violently; "The car tore down the street";
        "He came charging into my office" [syn: {tear}, {shoot
       ...
