---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pantograph
offline_file: ""
offline_thumbnail: ""
uuid: 230e7321-ccdb-4218-a559-0ce4ce6d3278
updated: 1484309327
title: Pantograph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pantograph_in_action.png
tags:
    - History
    - Uses
    - Drafting
    - Sculpture and minting
    - Acoustic cylinder duplication
    - Milling machines
    - Other uses
categories:
    - Technical drawing
---
A pantograph (Greek roots παντ- "all, every" and γραφ- "to write", from their original use for copying writing) is a mechanical linkage connected in a manner based on parallelograms so that the movement of one pen, in tracing an image, produces identical movements in a second pen. If a line drawing is traced by the first point, an identical, enlarged, or miniaturized copy will be drawn by a pen fixed to the other. Using the same principle, different kinds of pantographs are used for other forms of duplication in areas such as sculpture, minting, engraving and milling.
