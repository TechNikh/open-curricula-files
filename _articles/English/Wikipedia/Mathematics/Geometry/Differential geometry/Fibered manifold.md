---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fibered_manifold
offline_file: ""
offline_thumbnail: ""
uuid: a8ac4d24-6342-4b90-b228-0caa817c76d8
updated: 1484309218
title: Fibered manifold
tags:
    - History
    - Formal definition
    - Examples
    - Properties
    - Fibered coordinates
    - Local trivialization and fiber bundles
    - Notes
    - Historical
categories:
    - Differential geometry
---
i.e. a surjective differentiable mapping such that at each point y ∈ E the tangent mapping
