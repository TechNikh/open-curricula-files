---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sapphire
offline_file: ""
offline_thumbnail: ""
uuid: 8f415a87-d431-48bc-9089-bc34515196ee
updated: 1484310216
title: sapphire
categories:
    - Dictionary
---
sapphire
     adj : having the color of a blue sapphire; "sapphire eyes"
     n 1: a precious transparent stone of rich blue corundum valued as
          a gemstone
     2: a transparent piece of sapphire that has been cut and
        polished and is valued as a precious gem
     3: a light shade of blue [syn: {azure}, {cerulean}, {lazuline},
         {sky-blue}]
