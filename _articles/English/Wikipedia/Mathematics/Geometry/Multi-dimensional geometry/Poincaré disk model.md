---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poincar%C3%A9_disk_model'
offline_file: ""
offline_thumbnail: ""
uuid: 74fb6ef8-3dfe-4f6c-966e-869192d49e84
updated: 1484309287
title: Poincaré disk model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Poincare_disc_hyperbolic_parallel_lines.svg.png
tags:
    - Properties
    - Lines
    - Compass and straightedge construction
    - distance
    - Circles
    - Hypercycles
    - Horocycles
    - Euclidean synopsis
    - Metric
    - Relation to other models of hyperbolic geometry
    - Relation to the Klein disk model
    - Relation to the Poincaré half-plane model
    - Relation to the hyperboloid model
    - Analytic geometry constructions in the hyperbolic plane
    - Angles
    - Isometric Transformations
    - Artistic realizations
categories:
    - Multi-dimensional geometry
---
In geometry, the Poincaré disk model also called the conformal disk model, is a model of 2-dimensional hyperbolic geometry in which the points of the geometry are inside the unit disk, and the straight lines consist of all segments of circles contained within that disk that are orthogonal to the boundary of the disk, plus all diameters of the disk.
