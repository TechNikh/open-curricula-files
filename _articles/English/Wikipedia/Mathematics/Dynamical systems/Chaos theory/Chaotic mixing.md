---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaotic_mixing
offline_file: ""
offline_thumbnail: ""
uuid: e5401398-ac9a-4c0e-8ca2-d4f8412c4592
updated: 1484308715
title: Chaotic mixing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Chaotic_mixing.png
tags:
    - Context of chaotic advection
    - Fluid flows
    - Conditions for chaotic advection
    - Shear
    - Characterization of chaotic advection
    - Lyapunov exponents
    - Filament growth versus evolution of the tracer gradient
    - Contour advection
    - Poincaré sections
    - Fractal dimension
    - >
        Evolution of tracer concentration fields in chaotic
        advection
    - History of chaotic advection
categories:
    - Chaos theory
---
In chaos theory and fluid dynamics, chaotic mixing is a process by which flow tracers develop into complex fractals under the action of a fluid flow. The flow is characterized by an exponential growth of fluid filaments.[1] Even very simple flows, such as the blinking vortex, or finitely resolved wind fields can generate exceptionally complex patterns from initially simple tracer fields.[2]
