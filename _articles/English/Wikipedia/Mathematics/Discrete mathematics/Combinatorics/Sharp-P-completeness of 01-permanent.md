---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Sharp-P-completeness_of_01-permanent
offline_file: ""
offline_thumbnail: ""
uuid: 73b4b712-8bee-43ae-8860-4a4686c22d0d
updated: 1484308582
title: Sharp-P-completeness of 01-permanent
tags:
    - Significance
    - "Ben-Dor and Halevi's proof"
    - Overview
    - Constructing the integer graph
    - Notable properties of the graph
    - The clause component
    - 01-Matrix
    - Reduction to a non-negative matrix
    - Reduction to powers of 2
    - Reduction to 0–1
    - "Aaronson's proof"
categories:
    - Combinatorics
---
The #P-completeness of 01-permanent, sometimes known as Valiant's theorem,[1] is a mathematical proof about the permanent of matrices, considered a seminal result in computational complexity theory.[2][3] In a 1979 scholarly paper, Leslie Valiant proved[4] that the computational problem of computing the permanent of a matrix is #P-hard, even if the matrix is restricted to have entries that are all 0 or 1. In this restricted case, computing the permanent is even #P-complete, because it corresponds to the #P problem of counting the number of permutation matrices one can get by changing ones into zeroes.
