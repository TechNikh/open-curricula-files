---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/innovation
offline_file: ""
offline_thumbnail: ""
uuid: 09620040-cdb1-4c4b-a593-ab4564d21acb
updated: 1484310601
title: innovation
categories:
    - Dictionary
---
innovation
     n 1: a creation (a new device or process) resulting from study
          and experimentation [syn: {invention}]
     2: the creation of something in the mind [syn: {invention}, {excogitation},
         {conception}, {design}]
     3: the act of starting something for the first time;
        introducing something new; "she looked forward to her
        initiation as an adult"; "the foundation of a new
        scientific society"; "he regards the fork as a modern
        introduction" [syn: {initiation}, {founding}, {foundation},
         {institution}, {origination}, {creation}, {introduction},
         {instauration}]
