---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coherent_sheaf
offline_file: ""
offline_thumbnail: ""
uuid: 271a5d39-67a2-42a2-b830-053d6220658d
updated: 1484309136
title: Coherent sheaf
tags:
    - Definitions
    - Basic constructions of coherent sheaves
    - Functoriality
    - Local behavior of coherent sheaves
    - Examples of vector bundles
    - Chern classes and algebraic K-theory
    - The category of quasi-coherent sheaves
    - Coherent cohomology
    - Notes
categories:
    - Algebraic geometry
---
In mathematics, especially in algebraic geometry and the theory of complex manifolds, coherent sheaves are a class of sheaves closely linked to the geometric properties of the underlying space. The definition of coherent sheaves is made with reference to a sheaf of rings that codifies this geometric information.
