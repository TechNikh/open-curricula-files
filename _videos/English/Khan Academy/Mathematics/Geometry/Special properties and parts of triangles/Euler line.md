---
version: 1
type: video
provider: YouTube
id: tUqyJgmGY7k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Special%20properties%20and%20parts%20of%20triangles/Euler%20line.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3ca33921-254b-4341-9e8b-b69556f5a7c2
updated: 1486069581
title: Euler line
categories:
    - Special properties and parts of triangles
thumbnail_urls:
    - https://i3.ytimg.com/vi/tUqyJgmGY7k/default.jpg
    - https://i3.ytimg.com/vi/tUqyJgmGY7k/1.jpg
    - https://i3.ytimg.com/vi/tUqyJgmGY7k/2.jpg
    - https://i3.ytimg.com/vi/tUqyJgmGY7k/3.jpg
---
The magic and mystery of the Euler Line

Watch the next lesson: https://www.khanacademy.org/math/geometry/triangle-properties/triangle_property_review/v/euler-s-line-proof?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Missed the previous lesson? 
https://www.khanacademy.org/math/geometry/triangle-properties/triangle_property_review/v/review-of-triangle-properties?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Geometry on Khan Academy: We are surrounded by space. And that space contains lots of things. And these things have shapes. In geometry we are concerned with the nature of these shapes, how we define them, and what they teach us about the world at large--from math to architecture to biology to astronomy (and everything in between). Learning geometry is about more than just taking your medicine ("It's good for you!"), it's at the core of everything that exists--including you. Having said all that, some of the specific topics we'll cover include angles, intersecting lines, right triangles, perimeter, area, volume, circles, triangles, quadrilaterals, analytic geometry, and geometric constructions. Wow. That's a lot. To summarize: it's difficult to imagine any area of math that is more widely used than geometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Geometry channel:
https://www.youtube.com/channel/UCD3OtKxPRUFw8kzYlhJXa1Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
