---
version: 1
type: video
provider: YouTube
id: NY-bnY0yjWw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c3d23131-118d-40a9-bb8b-534dc9b1d6b4
updated: 1486069661
title: Rainbow Flame! Coloured Fire Experiment!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/NY-bnY0yjWw/default.jpg
    - https://i3.ytimg.com/vi/NY-bnY0yjWw/1.jpg
    - https://i3.ytimg.com/vi/NY-bnY0yjWw/2.jpg
    - https://i3.ytimg.com/vi/NY-bnY0yjWw/3.jpg
---
