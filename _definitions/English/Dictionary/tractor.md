---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tractor
offline_file: ""
offline_thumbnail: ""
uuid: 1eb02e1e-e1af-485e-b0ee-c911ef259311
updated: 1484310522
title: tractor
categories:
    - Dictionary
---
tractor
     n 1: a wheeled vehicle with large wheels; used in farming and
          other applications
     2: a truck that has a cab but no body; used for pulling large
        trailers or vans
