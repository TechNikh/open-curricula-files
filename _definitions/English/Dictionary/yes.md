---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yes
offline_file: ""
offline_thumbnail: ""
uuid: 2c478475-84f0-490e-8d74-597649a78cf9
updated: 1484310220
title: 'yes'
categories:
    - Dictionary
---
yes
     n : an affirmative; "I was hoping for a yes" [ant: {no}]
