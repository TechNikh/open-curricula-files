---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/academically
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484515501
title: academically
categories:
    - Dictionary
---
academically
     adv : in regard to academic matters; "academically, this is a good
           school"
