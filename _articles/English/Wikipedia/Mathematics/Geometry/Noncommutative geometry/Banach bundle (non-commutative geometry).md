---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Banach_bundle_(non-commutative_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 81102890-9ff1-4412-9ea8-df45e6a7ed2d
updated: 1484309290
title: Banach bundle (non-commutative geometry)
tags:
    - Definition
    - Examples
    - Trivial bundle
categories:
    - Noncommutative geometry
---
