---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yoma
offline_file: ""
offline_thumbnail: ""
uuid: c826a998-57e7-4a5e-97bd-ee4f8c2606a3
updated: 1484308425
title: Yoma
tags:
    - Preparations of the High Priest before Yom Kippur
    - Services of the Day
    - Afflictions on Yom Kippur
categories:
    - Uncategorized
---
Yoma (Aramaic: יומא, lit. "The Day") is the fifth tractate of Seder Moed ("Order of Festivals") of the Mishnah and of the Talmud. It is concerned mainly with the laws of the Jewish holiday Yom Kippur, on which Jews atone for their sins from the previous year. It consists of eight chapters and has a Gemara ("Completion") from both the Jerusalem Talmud and the Babylonian Talmud.
