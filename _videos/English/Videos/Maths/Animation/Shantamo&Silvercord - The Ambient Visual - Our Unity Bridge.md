---
version: 1
type: video
provider: YouTube
id: I3-lg1__ddU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ee019136-0552-49d6-ba4a-ad81c1f85abf
updated: 1486069648
title: 'Shantamo&Silvercord - The Ambient Visual -- Our Unity Bridge'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/I3-lg1__ddU/default.jpg
    - https://i3.ytimg.com/vi/I3-lg1__ddU/1.jpg
    - https://i3.ytimg.com/vi/I3-lg1__ddU/2.jpg
    - https://i3.ytimg.com/vi/I3-lg1__ddU/3.jpg
---
