---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/backwardness
offline_file: ""
offline_thumbnail: ""
uuid: 97bd77c5-c20d-4225-ab50-e7ee350e8c90
updated: 1484310549
title: backwardness
categories:
    - Dictionary
---
backwardness
     n : lack of normal development of intellectual capacities [syn:
         {retardation}, {mental retardation}, {slowness}, {subnormality}]
