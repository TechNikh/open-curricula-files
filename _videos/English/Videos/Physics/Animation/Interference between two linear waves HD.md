---
version: 1
type: video
provider: YouTube
id: V6bCg-VoJOU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Interference%20between%20two%20linear%20waves%20HD.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3c0d8f51-152f-4f00-a671-3da3273b1372
updated: 1486069649
title: Interference between two linear waves HD
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/V6bCg-VoJOU/default.jpg
    - https://i3.ytimg.com/vi/V6bCg-VoJOU/1.jpg
    - https://i3.ytimg.com/vi/V6bCg-VoJOU/2.jpg
    - https://i3.ytimg.com/vi/V6bCg-VoJOU/3.jpg
---
