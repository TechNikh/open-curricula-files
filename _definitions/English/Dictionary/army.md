---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/army
offline_file: ""
offline_thumbnail: ""
uuid: 69af0051-f1f0-40ea-a334-d8a8d3395dab
updated: 1484310316
title: army
categories:
    - Dictionary
---
army
     n 1: a permanent organization of the military land forces of a
          nation or state [syn: {regular army}, {ground forces}]
     2: a large number of people united for some specific purpose
