---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Intersection_theorem
offline_file: ""
offline_thumbnail: ""
uuid: dc025feb-2d45-4235-9d81-5fb880e8f5f1
updated: 1484309253
title: Intersection theorem
categories:
    - Incidence geometry
---
In projective geometry, an intersection theorem or incidence theorem is a statement concerning an incidence structure – consisting of points, lines, and possibly higher-dimensional objects and their incidences – together with a pair of objects A and B (for instance, a point and a line). The "theorem" states that, whenever a set of objects satisfies the incidences (i.e. can be identified with the objects of the incidence structure in such a way that incidence is preserved), then the objects A and B must also be incident. An intersection theorem is not necessarily true in all projective geometries; it is a property that some geometries satisfy but others don't.
