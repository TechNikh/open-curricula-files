---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Check-raise
offline_file: ""
offline_thumbnail: ""
uuid: b5116afb-f534-4d78-b798-eb6a169f4076
updated: 1484309059
title: Check-raise
categories:
    - Poker strategy
---
A check-raise in poker is a common deceptive play in which a player checks early in a betting round, hoping someone else will open. The player who checked then raises in the same round.
