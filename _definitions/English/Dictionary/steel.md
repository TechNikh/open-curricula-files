---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/steel
offline_file: ""
offline_thumbnail: ""
uuid: 34fbd9e8-2a69-4a3a-9af0-bbc9e0c47d65
updated: 1484310224
title: steel
categories:
    - Dictionary
---
steel
     n 1: an alloy of iron with small amounts of carbon; widely used
          in construction; mechanical properties can be varied
          over a wide range
     2: a cutting or thrusting weapon with a long blade [syn: {sword},
         {blade}, {brand}]
     3: knife sharpener consisting of a ridged steel rod
     v 1: get ready for something difficult or unpleasant [syn: {nerve}]
     2: cover, plate, or edge with steel
