---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Herical
offline_file: ""
offline_thumbnail: ""
uuid: 9fb9aa3f-0f36-4903-adac-c6dcf4e79892
updated: 1484308323
title: Hericium erinaceus
tags:
    - chemistry
    - Culinary use
    - Alternative names
    - Medical research and use
    - Gallery
categories:
    - Uncategorized
---
Hericium erinaceus (also called lion's mane mushroom, bearded tooth mushroom, satyr's beard, bearded hedgehog mushroom, pom pom mushroom, or bearded tooth fungus) is an edible and medicinal mushroom belonging to the tooth fungus group. Native to North America, Europe and Asia it can be identified by its long spines (greater than 1 cm length), its appearance on hardwoods and its tendency to grow a single clump of dangling spines.[1] Hericium erinaceus can be mistaken for other species of Hericium, all popular edibles, which grow across the same range. In the wild, these mushrooms are common during late summer and fall on hardwoods, particularly American Beech.
