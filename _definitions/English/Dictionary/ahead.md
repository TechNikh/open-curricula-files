---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ahead
offline_file: ""
offline_thumbnail: ""
uuid: 2ec86f80-c026-4086-9324-4611c1a8d2d5
updated: 1484310433
title: ahead
categories:
    - Dictionary
---
ahead
     adj : having the leading position or higher score in a contest;
           "he is ahead by a pawn"; "the leading team in the
           pennant race" [syn: {ahead(p)}, {in the lead}, {leading}]
     adv 1: at or in the front; "I see the lights of a town ahead"; "the
            road ahead is foggy"; "staring straight ahead"; "we
            couldn't see over the heads of the people in front";
            "with the cross of Jesus marching on before" [syn: {in
            front}, {before}]
     2: toward the future; forward in time; "I like to look ahead in
        imagination to what the future may bring"; "I look forward
        to seeing you" [syn: {forward}] [ant: {back}, ...
