---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/qualification
offline_file: ""
offline_thumbnail: ""
uuid: ca61dc51-c770-42ca-a79f-7fd1325be99c
updated: 1484310144
title: qualification
categories:
    - Dictionary
---
qualification
     n 1: an attribute that must be met or complied with and that fits
          a person for something; "her qualifications for the job
          are excellent"; "one of the qualifications for admission
          is an academic degree"; "she has the makings of fine
          musician" [syn: {makings}]
     2: the act of modifying or changing the strength of some idea;
        "his new position involves a qualification of his party's
        platform"
     3: a statement that limits or restricts some claim; "he
        recommended her without any reservations" [syn: {reservation}]
