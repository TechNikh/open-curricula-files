---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/investment
offline_file: ""
offline_thumbnail: ""
uuid: c6ebef71-693e-4c9d-b866-e75c5867dda9
updated: 1484310525
title: investment
categories:
    - Dictionary
---
investment
     n 1: the act of investing; laying out money or capital in an
          enterprise with the expectation of profit [syn: {investing}]
     2: money that is invested with an expectation of profit [syn: {investment
        funds}]
     3: outer layer or covering of an organ or part or organism
     4: the act of putting on robes or vestments
     5: the ceremonial act of clothing someone in the insignia of an
        office; the formal promotion of a person to an office or
        rank [syn: {investiture}]
