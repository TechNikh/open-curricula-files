---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ratio
offline_file: ""
offline_thumbnail: ""
uuid: 95f90487-33b6-404e-a56d-a3f5a706efea
updated: 1484310316
title: ratio
categories:
    - Dictionary
---
ratio
     n : the relative magnitudes of two quantities (usually expressed
         as a quotient)
