---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bee
offline_file: ""
offline_thumbnail: ""
uuid: 0e02e218-ec15-4f9f-a494-0b6a5877b6b1
updated: 1484310383
title: bee
categories:
    - Dictionary
---
bee
     n 1: any of numerous hairy-bodied insects including social and
          solitary species
     2: a social gathering to carry out some communal task or to
        hold competitions
