---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cage
offline_file: ""
offline_thumbnail: ""
uuid: d441bee7-03de-4acf-a1f4-5ddf9714e6fe
updated: 1484310319
title: cage
categories:
    - Dictionary
---
cage
     n 1: an enclosure made or wire or metal bars in which birds or
          animals are kept [syn: {coop}]
     2: something that restricts freedom as a cage restricts
        movement
     3: United States composer of avant-garde music (1912-1992)
        [syn: {John Cage}, {John Milton Cage Jr.}]
     4: the net that is the goal in ice hockey
     5: a movable screen placed behind home base to catch balls
        during batting practice [syn: {batting cage}]
     v : confine in a cage; "The animal was caged" [syn: {cage in}]
