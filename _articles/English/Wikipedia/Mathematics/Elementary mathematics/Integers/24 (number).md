---
version: 1
type: article
id: https://en.wikipedia.org/wiki/24_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 12348545-1d1a-4a94-ba29-07e6801dac55
updated: 1484308948
title: 24 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Prague_Apr04_015_0.jpg
tags:
    - In mathematics
    - In science
    - In religion
    - In music
    - In sports
    - In other fields
categories:
    - Integers
---
The SI prefix for 1024 is yotta (Y), and for 10−24 (i.e., the reciprocal of 1024) yocto (y). These numbers are the largest and smallest number to receive an SI prefix to date.
