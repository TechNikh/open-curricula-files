---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Admissible_set
offline_file: ""
offline_thumbnail: ""
uuid: 650e81ca-6de9-403b-9a6b-9e8b7d666305
updated: 1484309501
title: Admissible set
categories:
    - Set theory
---
In set theory, a discipline within mathematics, an admissible set is a transitive set 
  
    
      
        A
        
      
    
    {\displaystyle A\,}
  
 such that 
  
    
      
        ⟨
        A
        ,
        ∈
        ⟩
      
    
    {\displaystyle \langle A,\in \rangle }
  
 is a model of Kripke–Platek set theory (Barwise 1975).
