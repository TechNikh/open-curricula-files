---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/left
offline_file: ""
offline_thumbnail: ""
uuid: 80c18fb2-7e96-48a2-bab1-71066dc5039b
updated: 1484310321
title: left
categories:
    - Dictionary
---
left
     adj 1: being or located on or directed toward the side of the body
            to the west when facing north; "my left hand"; "left
            center field"; "the left bank of a river is bank on
            your left side when you are facing downstream" [ant: {right}]
     2: not used up; "leftover meatloaf"; "she had a little money
        left over so she went to a movie"; "some odd dollars
        left"; "saved the remaining sandwiches for supper";
        "unexpended provisions" [syn: {leftover}, {left over(p)},
        {left(p)}, {odd}, {remaining}, {unexpended}]
     3: intended for the left hand; "I rarely lose a left-hand
        glove" [syn: {left(a)}, {left-hand(a)}]
   ...
