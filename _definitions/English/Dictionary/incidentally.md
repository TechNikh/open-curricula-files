---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incidentally
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484591821
title: incidentally
categories:
    - Dictionary
---
incidentally
     adv 1: introducing a different topic; "by the way, I won't go to
            the party" [syn: {by the way}, {by the bye}]
     2: in an incidental manner; "these magnificent achievements
        were only incidentally influenced by Oriental models"
        [syn: {accidentally}, {by chance}]
     3: by the way; "apropos, can you lend me some money for the
        weekend?" [syn: {apropos}]
