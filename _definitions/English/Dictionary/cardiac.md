---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cardiac
offline_file: ""
offline_thumbnail: ""
uuid: fc320bf6-719a-4b24-82b7-ce44c547fd3a
updated: 1484310316
title: cardiac
categories:
    - Dictionary
---
cardiac
     adj : of or relating to the heart; "cardiac arrest"
