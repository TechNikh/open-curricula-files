---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cot
offline_file: ""
offline_thumbnail: ""
uuid: 07d3078d-3a5b-48ca-b3e6-d9d2594e7f8d
updated: 1484310154
title: cot
categories:
    - Dictionary
---
cot
     n 1: a sheath worn to protect a finger [syn: {fingerstall}]
     2: baby bed with high sides [syn: {crib}]
     3: a small bed that folds up for storage or transport [syn: {camp
        bed}]
