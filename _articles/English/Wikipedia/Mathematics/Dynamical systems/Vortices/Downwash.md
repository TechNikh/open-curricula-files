---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Downwash
offline_file: ""
offline_thumbnail: ""
uuid: f0f08787-aeae-4193-b93f-19d6583a31d7
updated: 1484308872
title: Downwash
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-thumbnail.jpg
categories:
    - Vortices
---
In aeronautics downwash is the change in direction of air deflected by the aerodynamic action of an airfoil, wing or helicopter rotor blade in motion, as part of the process of producing lift.[1]
