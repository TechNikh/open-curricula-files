---
version: 1
type: video
provider: YouTube
id: 2MYA8Ba2PvM
offline_file: ""
offline_thumbnail: ""
uuid: 5c45e195-e393-43a7-8362-7f3e57429578
updated: 1484040398
title: Die rolling probability with independent events
categories:
    - Precalculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/2MYA8Ba2PvM/default.jpg
    - https://i3.ytimg.com/vi/2MYA8Ba2PvM/1.jpg
    - https://i3.ytimg.com/vi/2MYA8Ba2PvM/2.jpg
    - https://i3.ytimg.com/vi/2MYA8Ba2PvM/3.jpg
---
