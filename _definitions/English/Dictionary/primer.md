---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/primer
offline_file: ""
offline_thumbnail: ""
uuid: f57e4237-84ba-43a9-9b51-79b08104835a
updated: 1484310168
title: primer
categories:
    - Dictionary
---
primer
     n 1: an introductory textbook
     2: any igniter that is used to initiate the burning of a
        propellant [syn: {fuse}, {fuze}, {fusee}, {fuzee}, {priming}]
     3: the first or preliminary coat of paint or size applied to a
        surface [syn: {flat coat}, {ground}, {priming}, {primer
        coat}, {priming coat}, {undercoat}]
