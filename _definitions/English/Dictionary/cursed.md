---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cursed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484478901
title: cursed
categories:
    - Dictionary
---
cursed
     adj 1: deserving a curse; sometimes used as an intensifier;
            "villagers shun the area believing it to be cursed";
            "cursed with four daughter"; "not a cursed drop"; "his
            cursed stupidity"; "I'll be cursed if I can see your
            reasoning" [syn: {curst}] [ant: {blessed}]
     2: in danger of the eternal punishment of hell; "poor damned
        souls" [syn: {damned}, {doomed}, {unredeemed}, {unsaved}]
