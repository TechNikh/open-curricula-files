---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Apeirotope
offline_file: ""
offline_thumbnail: ""
uuid: a726e6cf-61c2-4e60-a688-d1d5a327e9fc
updated: 1484309283
title: Apeirotope
tags:
    - Honeycombs
    - Skew apeirotopes
    - Skew apeirogons
    - Infinite skew polyhedra
categories:
    - Multi-dimensional geometry
---
