---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ignorant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484480581
title: ignorant
categories:
    - Dictionary
---
ignorant
     adj 1: lacking general education or knowledge; "an ignorant man";
            "nescient of contemporary literature"; "an unlearned
            group incapable of understanding complex issues";
            "exhibiting contempt for his unlettered companions"
            [syn: {nescient}, {unenlightened}, {unlearned}, {unlettered}]
     2: ignorant of the fundamentals of a given art or branch of
        knowledge; "ignorant of quantum mechanics"; "musically
        illiterate" [syn: {illiterate}]
     3: lacking basic knowledge; "how can someone that age be so
        ignorant?"; "inexperienced and new to the real world"
        [syn: {inexperienced}]
     4: used of things; ...
