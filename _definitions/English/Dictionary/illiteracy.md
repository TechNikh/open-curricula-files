---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illiteracy
offline_file: ""
offline_thumbnail: ""
uuid: c159635b-fa73-44d7-a143-44ffd43ab29f
updated: 1484310601
title: illiteracy
categories:
    - Dictionary
---
illiteracy
     n 1: ignorance resulting from not reading
     2: an inability to read [syn: {analphabetism}] [ant: {literacy}]
