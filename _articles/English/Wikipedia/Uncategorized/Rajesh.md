---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rajesh
offline_file: ""
offline_thumbnail: ""
uuid: 89fe34be-df8a-432f-8378-208e5895c571
updated: 1484308351
title: Rajesh
categories:
    - Uncategorized
---
Rajesh (Rajeshwer) is a popular Nepalese and Indian name. It originates from the word Raja, which means king and Eshwar, or the ultimate power. The complete word is Rajesh (Rajeshwer). The meaning of Rajesh is "the ultimate god, who is the God of Kings".[citation needed]
