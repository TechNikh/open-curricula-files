---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agra
offline_file: ""
offline_thumbnail: ""
uuid: a698d0eb-8355-494f-abf5-63395b9087e5
updated: 1484310431
title: agra
categories:
    - Dictionary
---
Agra
     n : a city in northern India; former capital of the Mogul
         empire; site of the Taj Mahal
