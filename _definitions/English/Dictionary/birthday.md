---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/birthday
offline_file: ""
offline_thumbnail: ""
uuid: 6020ae08-8b82-4cad-a318-cb8e43dfb8b4
updated: 1484310192
title: birthday
categories:
    - Dictionary
---
birthday
     n 1: an anniversary of the day on which a person was born (or the
          celebration of it)
     2: the date on which a person was born [syn: {natal day}]
