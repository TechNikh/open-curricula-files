---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ball_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 6415b77a-78a7-489d-8e51-93358285fda2
updated: 1484309261
title: Ball (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sphere_wireframe.svg.png
tags:
    - Balls in Euclidean space
    - The volume
    - Balls in general metric spaces
    - Balls in normed vector spaces
    - p-norm
    - General convex norm
    - Topological balls
categories:
    - Metric geometry
---
In mathematics, a ball is the space bounded by a sphere. It may be a closed ball (including the boundary points that constitute the sphere) or an open ball (excluding them).
