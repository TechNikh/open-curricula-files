---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Slant_height
offline_file: ""
offline_thumbnail: ""
uuid: 1a8860cc-9947-45d9-881d-425cc2964953
updated: 1484309245
title: Slant height
tags:
    - Formula
    - Derivation
    - use
    - Relationship with height and radius
categories:
    - Geometric measurement
---
