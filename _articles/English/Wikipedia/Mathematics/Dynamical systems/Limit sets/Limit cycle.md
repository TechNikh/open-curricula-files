---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Limit_cycle
offline_file: ""
offline_thumbnail: ""
uuid: fc97d6ed-c1ec-45ec-8558-2024c9662fbc
updated: 1484308791
title: Limit cycle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Limit_cycle_Poincare_map.svg.png
tags:
    - Definition
    - Properties
    - Stable, unstable and semi-stable limit cycles
    - Finding limit cycles
    - Open problems
categories:
    - Limit sets
---
In mathematics, in the study of dynamical systems with two-dimensional phase space, a limit cycle is a closed trajectory in phase space having the property that at least one other trajectory spirals into it either as time approaches infinity or as time approaches negative infinity. Such behavior is exhibited in some nonlinear systems. Limit cycles have been used to model the behavior of a great many real world oscillatory systems. The study of limit cycles was initiated by Henri Poincaré (1854-1912).
