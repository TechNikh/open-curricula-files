---
version: 1
type: video
provider: YouTube
id: i-NNWI8Ccas
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Birth%20of%20stars.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9edbcd5a-f7e3-49c3-aa98-2ea8920b9d01
updated: 1486069686
title: Birth of stars
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/i-NNWI8Ccas/default.jpg
    - https://i3.ytimg.com/vi/i-NNWI8Ccas/1.jpg
    - https://i3.ytimg.com/vi/i-NNWI8Ccas/2.jpg
    - https://i3.ytimg.com/vi/i-NNWI8Ccas/3.jpg
---
