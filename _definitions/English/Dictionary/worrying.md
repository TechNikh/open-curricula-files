---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worrying
offline_file: ""
offline_thumbnail: ""
uuid: aadb59ef-3ba3-4d70-ac7e-f90b080612cc
updated: 1484310537
title: worrying
categories:
    - Dictionary
---
worrying
     adj : causing distress or worry or anxiety; "distressing (or
           disturbing) news"; "lived in heroic if something
           distressful isolation"; "a disturbing amount of crime";
           "a revelation that was most perturbing"; "a new and
           troubling thought"; "in a particularly worrisome
           predicament"; "a worrying situation"; "a worrying time"
           [syn: {distressing}, {distressful}, {disturbing}, {perturbing},
            {troubling}, {worrisome}]
     n 1: the act of harassing someone [syn: {badgering}, {torment}, {bedevilment}]
     2: the act of moving something by repeated tugs or pushes;
        "vigorous worrying finally loosened ...
