---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chewing
offline_file: ""
offline_thumbnail: ""
uuid: 9f62aad0-0361-499b-8da5-bd0cd45bfcd2
updated: 1484310349
title: chewing
categories:
    - Dictionary
---
chewing
     n : biting and grinding food in your mouth so it becomes soft
         enough to swallow [syn: {chew}, {mastication}, {manduction}]
