---
version: 1
type: video
provider: YouTube
id: sSD2la0vj2Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cf622b29-c945-4546-85db-bed0ece5a571
updated: 1486069634
title: Young’s Double Slit Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/sSD2la0vj2Y/default.jpg
    - https://i3.ytimg.com/vi/sSD2la0vj2Y/1.jpg
    - https://i3.ytimg.com/vi/sSD2la0vj2Y/2.jpg
    - https://i3.ytimg.com/vi/sSD2la0vj2Y/3.jpg
---
