---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/P-adic_Teichm%C3%BCller_theory'
offline_file: ""
offline_thumbnail: ""
uuid: 27db90b5-24e1-40dc-925d-2230f32d8967
updated: 1484308657
title: P-adic Teichmüller theory
categories:
    - Number theory
---
In mathematics, p-adic Teichmüller theory describes the "uniformization" of p-adic curves and their moduli, generalizing the usual Teichmüller theory that describes the uniformization of Riemann surfaces and their moduli. It was introduced and developed by Mochizuki (1996, 1999).
