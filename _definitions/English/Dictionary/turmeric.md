---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/turmeric
offline_file: ""
offline_thumbnail: ""
uuid: 5f4ed0dc-d488-4533-9d90-593ffb987754
updated: 1484310379
title: turmeric
categories:
    - Dictionary
---
turmeric
     n 1: widely cultivated tropical plant of India having yellow
          flowers and a large aromatic deep yellow rhizome; source
          of a condiment and a yellow dye [syn: {Curcuma longa}, {Curcuma
          domestica}]
     2: ground dried rhizome of the turmeric plant used as seasoning
