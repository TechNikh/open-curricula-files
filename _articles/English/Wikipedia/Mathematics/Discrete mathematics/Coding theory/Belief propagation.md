---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Belief_propagation
offline_file: ""
offline_thumbnail: ""
uuid: 29995efa-f5e2-4e68-b173-fc6d702a8d17
updated: 1484308545
title: Belief propagation
tags:
    - Description of the sum-product algorithm
    - Exact algorithm for trees
    - Approximate algorithm for general graphs
    - Related algorithm and complexity issues
    - Relation to free energy
    - Generalized belief propagation (GBP)
    - Gaussian belief propagation (GaBP)
categories:
    - Coding theory
---
Belief propagation, also known as sum-product message passing, is a message passing algorithm for performing inference on graphical models, such as Bayesian networks and Markov random fields. It calculates the marginal distribution for each unobserved node, conditional on any observed nodes. Belief propagation is commonly used in artificial intelligence and information theory and has demonstrated empirical success in numerous applications including low-density parity-check codes, turbo codes, free energy approximation, and satisfiability.[1]
