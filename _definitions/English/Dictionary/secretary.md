---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secretary
offline_file: ""
offline_thumbnail: ""
uuid: 19e4cb61-5f86-4ef8-b60a-5f64fafc6685
updated: 1484310595
title: secretary
categories:
    - Dictionary
---
secretary
     n 1: a person who is head of an administrative department of
          government
     2: an assistant who handles correspondence and clerical work
        for a boss or an organization [syn: {secretarial assistant}]
     3: a person to whom a secret is entrusted [syn: {repository}]
     4: a desk used for writing [syn: {writing table}, {escritoire},
         {secretaire}]
