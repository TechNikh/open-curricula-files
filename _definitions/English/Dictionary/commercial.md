---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commercial
offline_file: ""
offline_thumbnail: ""
uuid: b2fa4d3a-1a75-4082-8737-e89cb32eff18
updated: 1484310475
title: commercial
categories:
    - Dictionary
---
commercial
     adj 1: connected with or engaged in or sponsored by or used in
            commerce or commercial enterprises; "commercial
            trucker"; "commercial TV"; "commercial diamonds" [ant:
             {noncommercial}]
     2: of or relating to commercialism; "a commercial attache";
        "commercial paper"; "commercial law"
     3: of the kind or quality used in commerce; average or
        inferior; "commercial grade of beef"; "commercial oxalic
        acid"
     n : a commercially sponsored ad on radio or television [syn: {commercial
         message}]
