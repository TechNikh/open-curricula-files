---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/medicinal
offline_file: ""
offline_thumbnail: ""
uuid: c502ad2b-9390-450e-8363-d0c2b24843a5
updated: 1484310416
title: medicinal
categories:
    - Dictionary
---
medicinal
     adj : having the properties of medicine; "medicative drugs";
           "medicinal herbs"; "medicinal properties" [syn: {medicative}]
