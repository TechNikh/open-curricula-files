---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/archives
offline_file: ""
offline_thumbnail: ""
uuid: 836812d7-ddd0-4381-b519-2571319b9f6d
updated: 1484310453
title: archives
categories:
    - Dictionary
---
archives
     n 1: collection of records especially about an institution
     2: a depository containing historical records and documents
        [syn: {archive}]
