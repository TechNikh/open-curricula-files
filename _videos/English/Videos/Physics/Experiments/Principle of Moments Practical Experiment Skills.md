---
version: 1
type: video
provider: YouTube
id: j71rLH-NpPw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5fe2166d-6a9f-421c-910a-2aaec3d716fe
updated: 1486069636
title: Principle of Moments Practical Experiment Skills
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/j71rLH-NpPw/default.jpg
    - https://i3.ytimg.com/vi/j71rLH-NpPw/1.jpg
    - https://i3.ytimg.com/vi/j71rLH-NpPw/2.jpg
    - https://i3.ytimg.com/vi/j71rLH-NpPw/3.jpg
---
