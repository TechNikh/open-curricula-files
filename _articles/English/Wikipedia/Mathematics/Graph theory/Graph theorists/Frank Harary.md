---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frank_Harary
offline_file: ""
offline_thumbnail: ""
uuid: 61701610-119a-4028-84c5-fe716fbc236b
updated: 1484309463
title: Frank Harary
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Wagner_and_Harary.jpg
tags:
    - Biography
    - Mathematics
    - Tree square root
    - Bibliography
categories:
    - Graph theorists
---
Frank Harary (March 11, 1921 – January 4, 2005) was an American mathematician, who specialized in graph theory. He was widely recognized as one of the "fathers" of modern graph theory.[1] Harary was a master of clear exposition and, together with his many doctoral students, he standardized the terminology of graphs. He broadened the reach of this field to include physics, psychology, sociology, and even anthropology. Gifted with a keen sense of humor, Harary challenged and entertained audiences at all levels of mathematical sophistication. A particular trick he employed was to turn theorems into games - for instance, students would try to add red edges to a graph on six vertices in order ...
