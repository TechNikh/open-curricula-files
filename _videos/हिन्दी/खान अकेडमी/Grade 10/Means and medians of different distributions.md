---
version: 1
type: video
provider: YouTube
id: qvNwm2S7gVo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: eb18aa87-8d87-4178-b367-6a4fc45f1531
updated: 1486069619
title: Means and medians of different distributions
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/qvNwm2S7gVo/default.jpg
    - https://i3.ytimg.com/vi/qvNwm2S7gVo/1.jpg
    - https://i3.ytimg.com/vi/qvNwm2S7gVo/2.jpg
    - https://i3.ytimg.com/vi/qvNwm2S7gVo/3.jpg
---
