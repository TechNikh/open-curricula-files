---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anaerobic
offline_file: ""
offline_thumbnail: ""
uuid: 10e02ba8-d664-4931-b598-25569000d865
updated: 1484310268
title: anaerobic
categories:
    - Dictionary
---
anaerobic
     adj 1: living or active in the absence of free oxygen; "anaerobic
            bacteria" [syn: {anaerobiotic}] [ant: {aerobic}]
     2: not aerobic; "isometric exercises are anaerobic" [ant: {aerobic}]
