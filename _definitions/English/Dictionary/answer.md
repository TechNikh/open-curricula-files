---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/answer
offline_file: ""
offline_thumbnail: ""
uuid: 9f8a93a5-96c5-4c1f-ab90-ca9d6a4880a3
updated: 1484310339
title: answer
categories:
    - Dictionary
---
answer
     n 1: a statement (either spoken or written) that is made in reply
          to a question or request or criticism or accusation; "I
          waited several days for his answer"; "he wrote replies
          to several of his critics" [syn: {reply}, {response}]
     2: a statement that solves a problem or explains how to solve
        the problem; "they were trying to find a peaceful
        solution"; "the answers were in the back of the book"; "he
        computed the result to four decimal places" [syn: {solution},
         {result}, {resolution}, {solvent}]
     3: the speech act of replying to a question [ant: {question}]
     4: the principle pleading by the defendant in ...
