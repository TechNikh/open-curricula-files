---
version: 1
type: video
provider: YouTube
id: jci6KCp0fUc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 13d3b296-565a-4c16-bbdd-cf9f3709b51b
updated: 1486069611
title: Relationships between patterns
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/jci6KCp0fUc/default.jpg
    - https://i3.ytimg.com/vi/jci6KCp0fUc/1.jpg
    - https://i3.ytimg.com/vi/jci6KCp0fUc/2.jpg
    - https://i3.ytimg.com/vi/jci6KCp0fUc/3.jpg
---
