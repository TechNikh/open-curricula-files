---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/morning
offline_file: ""
offline_thumbnail: ""
uuid: cbaf93c0-5ef5-420c-a894-84dc8a99b8c2
updated: 1484310316
title: morning
categories:
    - Dictionary
---
morning
     adj : in the morning; "the morning hours" [syn: {morning(a)}]
     n 1: the time period between dawn and noon; "I spent the morning
          running errands" [syn: {morn}, {morning time}, {forenoon}]
     2: a conventional expression of greeting or farewell [syn: {good
        morning}]
     3: the first light of day; "we got up before dawn"; "they
        talked until morning" [syn: {dawn}, {dawning}, {aurora}, {first
        light}, {daybreak}, {break of day}, {break of the day}, {dayspring},
         {sunrise}, {sunup}, {cockcrow}] [ant: {sunset}]
     4: the earliest period; "the dawn of civilization"; "the
        morning of the world" [syn: {dawn}]
