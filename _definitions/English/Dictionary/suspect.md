---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suspect
offline_file: ""
offline_thumbnail: ""
uuid: b171cd82-4d15-4bcb-a620-e61f8410fd10
updated: 1484310424
title: suspect
categories:
    - Dictionary
---
suspect
     adj : not as expected; "there was something fishy about the
           accident"; "up to some funny business"; "some
           definitely queer goings-on"; "a shady deal"; "her
           motives were suspect"; "suspicious behavior" [syn: {fishy},
            {funny}, {queer}, {shady}, {suspicious}]
     n 1: someone who is under suspicion
     2: a person or institution against whom an action is brought in
        a court of law; the person being sued or accused [syn: {defendant}]
        [ant: {plaintiff}]
     v 1: imagine to be the case or true or probable; "I suspect he is
          a fugitive"; "I surmised that the butler did it" [syn: {surmise}]
     2: regard as ...
