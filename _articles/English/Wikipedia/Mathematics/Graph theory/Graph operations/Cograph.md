---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cograph
offline_file: ""
offline_thumbnail: ""
uuid: 3545a6c4-e09e-4710-b9a8-2c11d1dc71d6
updated: 1484309445
title: Cograph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Turan_13-4.svg_0.png
tags:
    - Definition
    - Recursive construction
    - Other characterizations
    - Cotrees
    - Computational properties
    - Enumeration
    - Related graph families
    - Subclasses
    - Superclasses
    - Notes
categories:
    - Graph operations
---
In graph theory, a cograph, or complement-reducible graph, or P4-free graph, is a graph that can be generated from the single-vertex graph K1 by complementation and disjoint union. That is, the family of cographs is the smallest class of graphs that includes K1 and is closed under complementation and disjoint union.
