---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ore%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 217908d4-dfe4-42e7-9356-8693c13aa9ce
updated: 1484309408
title: "Ore's theorem"
tags:
    - Formal statement
    - Proof
    - Algorithm
    - Related results
categories:
    - Extremal graph theory
---
Ore's theorem is a result in graph theory proved in 1960 by Norwegian mathematician Øystein Ore. It gives a sufficient condition for a graph to be Hamiltonian, essentially stating that a graph with "sufficiently many edges" must contain a Hamilton cycle. Specifically, the theorem considers the sum of the degrees of pairs of non-adjacent vertices: if every such pair has a sum that at least equals the total number of vertices in the graph, then the graph is Hamiltonian.
