---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/latin
offline_file: ""
offline_thumbnail: ""
uuid: 3977ac2c-2b8c-4512-9dd3-c344e86118e0
updated: 1484310473
title: latin
categories:
    - Dictionary
---
Latin
     adj 1: of or relating to the ancient Latins or the Latin language;
            "Latin verb conjugations"
     2: having or resembling the psychology or temper characteristic
        of people of Latin America; "very Latin in temperament";
        "a Latin disdain"; "his hot Latin blood"
     3: relating to people or countries speaking Romance languages;
        "Latin America"
     4: relating to languages derived from Latin; "Romance
        languages" [syn: {Romance}]
     5: of or relating to the ancient region of Latium; "Latin
        towns"
     n 1: any dialect of the language of ancient Rome
     2: an inhabitant of ancient Latium
     3: a person who is a member of those ...
