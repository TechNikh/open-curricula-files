---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Petri_dishes
offline_file: ""
offline_thumbnail: ""
uuid: 738f0bc2-83b3-484a-b374-9b5ce864a985
updated: 1484308375
title: Petri dish
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Szalka_petriego.jpg
tags:
    - Microbiology
    - Other uses
    - Notes and references
categories:
    - Uncategorized
---
A Petri dish (sometimes spelled "Petrie dish" and alternatively known as a Petri plate or cell-culture dish), named after the German bacteriologist Julius Richard Petri,[1][2] is a shallow cylindrical glass or plastic lidded dish that biologists use to culture cells[3] – such as bacteria – or small mosses.[4]
