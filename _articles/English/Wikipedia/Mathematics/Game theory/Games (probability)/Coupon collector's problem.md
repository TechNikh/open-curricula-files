---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Coupon_collector%27s_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 5aa8302e-ead9-4554-afb5-d9649166148e
updated: 1484309045
title: "Coupon collector's problem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Coupon_collector_problem.svg.png
tags:
    - Understanding the problem
    - Answer
    - Solution
    - Calculating the expectation
    - Calculating the variance
    - Tail estimates
    - Extensions and generalizations
    - Notes
categories:
    - Games (probability)
---
In probability theory, the coupon collector's problem describes the "collect all coupons and win" contests. It asks the following question: Suppose that there is an urn of n different coupons, from which coupons are being collected, equally likely, with replacement. What is the probability that more than t sample trials are needed to collect all n coupons? An alternative statement is: Given n coupons, how many coupons do you expect you need to draw with replacement before having drawn each coupon at least once? The mathematical analysis of the problem reveals that the expected number of trials needed grows as 
  
    
      
        Θ
        (
        n
        log
        ⁡
        (
  ...
