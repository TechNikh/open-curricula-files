---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kemnitz%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: c45c2eed-81fa-411c-941b-65ccf104a119
updated: 1484308588
title: "Kemnitz's conjecture"
categories:
    - Combinatorics
---
In additive number theory, Kemnitz's conjecture states that every set of lattice points in the plane has a large subset whose centroid is also a lattice point. It was proved independently in the autumn of 2003 by Christian Reiher and Carlos di Fiore.
