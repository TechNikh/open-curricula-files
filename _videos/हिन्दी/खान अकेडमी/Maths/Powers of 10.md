---
version: 1
type: video
provider: YouTube
id: W65b1sACyTA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1b93a71e-ee9a-44ac-8807-2fb48b91bdd3
updated: 1486069607
title: Powers of 10
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/W65b1sACyTA/default.jpg
    - https://i3.ytimg.com/vi/W65b1sACyTA/1.jpg
    - https://i3.ytimg.com/vi/W65b1sACyTA/2.jpg
    - https://i3.ytimg.com/vi/W65b1sACyTA/3.jpg
---
