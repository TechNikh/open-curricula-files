---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confident
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413501
title: confident
categories:
    - Dictionary
---
confident
     adj 1: having or marked by confidence or assurance; "a confident
            speaker"; "a confident reply"; "his manner is more
            confident these days"; "confident of fulfillment"
            [ant: {unconfident}]
     2: persuaded of; very sure; "were convinced that it would be to
        their advantage to join"; "I am positive he is lying";
        "was confident he would win" [syn: {convinced(p)}, {positive(p)},
         {confident(p)}]
     3: not liable to error in judgment or action; "most surefooted
        of the statesmen who dealt with the depression"- Walter
        Lippman; "demonstrates a surefooted storytelling talent"-
        Michiko Kakutani [syn: ...
