---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maker
offline_file: ""
offline_thumbnail: ""
uuid: 6909e363-0887-4c10-b42a-9f8607cd62ac
updated: 1484310206
title: maker
categories:
    - Dictionary
---
maker
     n 1: a person who makes things [syn: {shaper}]
     2: terms referring to the Judeo-Christian God [syn: {Godhead},
        {Lord}, {Creator}, {Divine}, {God Almighty}, {Almighty}, {Jehovah}]
     3: a business engaged in manufacturing some product [syn: {manufacturer},
         {manufacturing business}]
