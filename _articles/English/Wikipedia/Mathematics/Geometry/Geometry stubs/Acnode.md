---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Acnode
offline_file: ""
offline_thumbnail: ""
uuid: e16bb09f-328a-4686-b592-ce59f3abbe37
updated: 1484309352
title: Acnode
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Isolated-point.svg.png
categories:
    - Geometry stubs
---
An acnode is an isolated point not on a curve, but whose coordinates satisfy the equation of the curve. The term "isolated point or hermit point" is an equivalent term.[1]
