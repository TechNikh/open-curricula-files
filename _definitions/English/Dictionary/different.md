---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/different
offline_file: ""
offline_thumbnail: ""
uuid: 9745187b-cbd8-439b-b3b4-7bd0937e7f25
updated: 1484310359
title: different
categories:
    - Dictionary
---
different
     adj 1: unlike in nature or quality or form or degree; "took
            different approaches to the problem"; "came to a
            different conclusion"; "different parts of the
            country"; "on different sides of the issue"; "this
            meeting was different from the earlier one" [ant: {same}]
     2: distinctly separate from the first; "that's another (or
        different) issue altogether" [syn: {another(a)}]
     3: differing from all others; not ordinary; "advertising that
        strives continually to be different"; "this new music is
        certainly different but I don't really like it"
     4: not like; marked by dissimilarity; "for twins they are ...
