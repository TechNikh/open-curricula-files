---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quietly
offline_file: ""
offline_thumbnail: ""
uuid: 12d9840f-d1b4-43af-a63d-b31d50155454
updated: 1484310561
title: quietly
categories:
    - Dictionary
---
quietly
     adv 1: with low volume; "speak softly but carry a big stick"; "she
            spoke quietly to the child"; "the radio was playing
            softly" [syn: {softly}] [ant: {loudly}]
     2: with little or no sound; "the class was listening quietly
        and intently"; "she was crying quietly" [ant: {noisily}]
     3: with little or no activity or no agitation (`quiet' is a
        nonstandard variant for `quietly'); "her hands rested
        quietly in her lap"; "the rock star was quietly led out
        the back door"; "sit here as quiet as you can" [syn: {quiet}]
        [ant: {unquietly}]
     4: in a restful manner; "the streets are restfully sunny and
        still for ...
