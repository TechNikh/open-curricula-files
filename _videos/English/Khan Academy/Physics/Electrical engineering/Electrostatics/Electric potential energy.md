---
version: 1
type: video
provider: YouTube
id: elJUghWSVh4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Electrical%20engineering/Electrostatics/Electric%20potential%20energy.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cbf43260-5b70-40c1-80eb-355511e96979
updated: 1486069595
title: Electric potential energy
categories:
    - Electrostatics
thumbnail_urls:
    - https://i3.ytimg.com/vi/elJUghWSVh4/default.jpg
    - https://i3.ytimg.com/vi/elJUghWSVh4/1.jpg
    - https://i3.ytimg.com/vi/elJUghWSVh4/2.jpg
    - https://i3.ytimg.com/vi/elJUghWSVh4/3.jpg
---
Introduction to electric potential energy. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/electrical-engineering/robots/all-about-spout/v/spout-bot-at-santa-rita-elementary-school?utm_source=YT&utm_medium=Desc&utm_campaign=electricalengineering

Missed the previous lesson? https://www.khanacademy.org/science/electrical-engineering/ee-electrostatics/ee-fields-potential-voltage/v/proof-advanced-field-from-infinite-plate-part-2?utm_source=YT&utm_medium=Desc&utm_campaign=electricalengineering

Electrical engineering on Khan Academy: A summary of the math and science preparation that will help you have the best experience with electrical engineering taught on Khan Academy. Become familiar with engineering numbers and notation, and learn about the two most important electrical quantities: current and voltage.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Electrical Engineering channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
