---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/norm
offline_file: ""
offline_thumbnail: ""
uuid: 7e9e9af8-49f4-4c63-8815-19dbb8163c4a
updated: 1484310448
title: norm
categories:
    - Dictionary
---
norm
     n 1: a standard or model or pattern regarded as typical; "the
          current middle-class norm of two children per family"
     2: a statistic describing the location of a distribution; "it
        set the norm for American homes" [syn: {average}]
