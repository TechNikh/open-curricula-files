---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biographical
offline_file: ""
offline_thumbnail: ""
uuid: 100a7c04-1b41-43a5-97a5-ddde6dbedc06
updated: 1484310148
title: biographical
categories:
    - Dictionary
---
biographical
     adj : of or relating to or being biography; "biographical data"
           [syn: {biographic}]
