---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/physically
offline_file: ""
offline_thumbnail: ""
uuid: de85e709-c603-44a5-8ce3-7a422680bb03
updated: 1484310469
title: physically
categories:
    - Dictionary
---
physically
     adv : in accord with physical laws; "it is physically impossible"
