---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reihen
offline_file: ""
offline_thumbnail: ""
uuid: a48bfb84-b812-4a8d-b988-a0a04d8afb67
updated: 1484308380
title: Sinsheim
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/230px-Sinsheim_Hauptstr_127-133.jpg
tags:
    - geography
    - Overview
    - Subdivisions
    - History
    - Demographics
    - Main sights
    - Sport
    - Stadium
    - Personalities
categories:
    - Uncategorized
---
Sinsheim (German pronunciation: [ˈzɪnshaɪ̯m]) is a town in southwestern Germany, in the Rhine Neckar Area of the state Baden-Württemberg about 22 kilometers southeast of Heidelberg and about 28 kilometers northwest of Heilbronn in the district Rhein-Neckar.
