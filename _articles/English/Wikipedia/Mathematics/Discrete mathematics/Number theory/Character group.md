---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Character_group
offline_file: ""
offline_thumbnail: ""
uuid: ca5f0c8c-46ed-41df-a3dd-924e9ce2f19a
updated: 1484308630
title: Character group
tags:
    - Preliminaries
    - Definition
    - Orthogonality of characters
categories:
    - Number theory
---
In mathematics, a character group is the group of representations of a group by complex-valued functions. These functions can be thought of as one-dimensional matrix representations and so are special cases of the group characters that arise in the related context of character theory. Whenever a group is represented by matrices, the function defined by the trace of the matrices is called a character; however, these traces do not in general form a group. Some important properties of these one-dimensional characters apply to characters in general:
