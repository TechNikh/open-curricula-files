---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proper
offline_file: ""
offline_thumbnail: ""
uuid: 07f2759e-7936-4012-8f14-cb62717197ba
updated: 1484310337
title: proper
categories:
    - Dictionary
---
proper
     adj 1: marked by suitability or rightness or appropriateness;
            "proper medical treatment"; "proper manners" [ant: {improper}]
     2: limited to the thing specified; "the city proper"; "his
        claim is connected with the deed proper" [syn: {proper(ip)}]
     3: appropriate for a condition or occasion; "everything in its
        proper place"; "the right man for the job"; "she is not
        suitable for the position" [syn: {right}, {suitable}]
     4: having all the qualities typical of the thing specified;
        "wanted a proper dinner; not just a snack"; "he finally
        has a proper job" [syn: {proper(a)}]
