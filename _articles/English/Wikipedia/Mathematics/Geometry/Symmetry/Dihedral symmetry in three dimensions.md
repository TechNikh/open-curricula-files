---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Dihedral_symmetry_in_three_dimensions
offline_file: ""
offline_thumbnail: ""
uuid: c13e9afa-0bfa-4b97-89bc-f9461b9720df
updated: 1484309307
title: Dihedral symmetry in three dimensions
tags:
    - Types
    - Subgroups
    - Examples
categories:
    - Symmetry
---
In geometry, dihedral symmetry in three dimensions is one of three infinite sequences of point groups in three dimensions which have a symmetry group that as abstract group is a dihedral group Dihn ( n ≥ 2 ).
