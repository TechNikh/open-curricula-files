---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wire_netting
offline_file: ""
offline_thumbnail: ""
uuid: ea09f1f3-b750-494c-b0f2-021416397f14
updated: 1484308368
title: Chain-link fencing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/230px-Wallpaper_group-p2-4.jpg
tags:
    - Development of chain-link fencing
    - Sizes and uses
    - Installation
    - Manufacturing
    - Notable uses
    - Notes
categories:
    - Uncategorized
---
A chain-link fence (also referred to as wire netting, wire-mesh fence, chain-wire fence, cyclone fence, hurricane fence, or diamond-mesh fence) is a type of woven fence usually made from galvanized or LLDPE-coated steel wire. The wires run vertically and are bent into a zig-zag pattern so that each "zig" hooks with the wire immediately on one side and each "zag" with the wire immediately on the other. This forms the characteristic diamond pattern seen in this type of fence.
