---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/focal
offline_file: ""
offline_thumbnail: ""
uuid: 4bbcefcb-1aa2-4100-aeeb-effb5b6e2c27
updated: 1484310218
title: focal
categories:
    - Dictionary
---
focal
     adj 1: having or localized centrally at a focus; "focal point";
            "focal infection"
     2: of or relating to a focus; "focal length"
