---
version: 1
type: video
provider: YouTube
id: qQAhhithHa8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Example%20of%20calculating%20a%20surface%20integral%20part%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6cf9e534-d82d-41e0-93be-d1fe0555df82
updated: 1486069692
title: Example of calculating a surface integral part 2
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/qQAhhithHa8/default.jpg
    - https://i3.ytimg.com/vi/qQAhhithHa8/1.jpg
    - https://i3.ytimg.com/vi/qQAhhithHa8/2.jpg
    - https://i3.ytimg.com/vi/qQAhhithHa8/3.jpg
---
