---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Product_metric
offline_file: ""
offline_thumbnail: ""
uuid: db77ebed-87cb-412c-969b-749b810522dd
updated: 1484309270
title: Product metric
categories:
    - Metric geometry
---
In mathematics, a product metric is a metric on the Cartesian product of finitely many metric spaces 
  
    
      
        (
        
          X
          
            1
          
        
        ,
        
          d
          
            
              X
              
                1
              
            
          
        
        )
        ,
        …
        ,
        (
        
          X
          
            n
          
        
        ,
        
          d
          
            
              X
              
                n
              
            
          
        
        )
      
    
    {\displaystyle (X_{1},d_{X_{1}}),\ldots ...
