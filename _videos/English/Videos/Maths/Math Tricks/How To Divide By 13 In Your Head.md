---
version: 1
type: video
provider: YouTube
id: kBhrNd0cC-0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 501e0958-0d57-481f-b590-ccd00a40dd1b
updated: 1486069623
title: How To Divide By 13 In Your Head
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/kBhrNd0cC-0/default.jpg
    - https://i3.ytimg.com/vi/kBhrNd0cC-0/1.jpg
    - https://i3.ytimg.com/vi/kBhrNd0cC-0/2.jpg
    - https://i3.ytimg.com/vi/kBhrNd0cC-0/3.jpg
---
