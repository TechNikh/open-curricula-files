---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Angara
offline_file: ""
offline_thumbnail: ""
uuid: 3cbd2850-2350-44b5-bb5d-a0e00d99daee
updated: 1484308413
title: Angara River
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kitchen-21-Russia-Angara-2815.jpg
tags:
    - Dams and reservoirs
    - Navigation
    - Tributaries
    - Photo gallery
    - Citations
    - Bibliography
categories:
    - Uncategorized
---
The Angara River (Buryat: Ангар, Angar, lit. "Cleft"; Russian: Ангара́, Angará) is a 1,779-kilometer-long (1,105 mi) river in Siberia, which traces a course through Russia's Irkutsk Oblast and Krasnoyarsk Krai. It is the river that drains Lake Baikal and is the headwater tributary of the Yenisei River.[1] It was formerly known as the Lower or Nizhnyaya Angara (distinguishing it from the Upper Angara).[2] Below its junction with the Ilim, it was formerly known as the Upper Tunguska (Russian: Верхняя Тунгуска, Verkhnyaya Tunguska, distinguishing it from the Lower Tunguska)[3][4] and, with the names reversed, as the Lower Tunguska.[5]
