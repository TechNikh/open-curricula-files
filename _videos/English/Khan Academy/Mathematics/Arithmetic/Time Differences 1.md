---
version: 1
type: video
provider: YouTube
id: SnkUkc23YC0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Time%20Differences%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 95712b0a-c577-411a-ad81-9ef20b1f21f6
updated: 1486069569
title: Time Differences 1
tags:
    - Time
    - Calculation
    - CC_3_MD_1
    - CC_4_MD_2
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/SnkUkc23YC0/default.jpg
    - https://i3.ytimg.com/vi/SnkUkc23YC0/1.jpg
    - https://i3.ytimg.com/vi/SnkUkc23YC0/2.jpg
    - https://i3.ytimg.com/vi/SnkUkc23YC0/3.jpg
---
Finding how long passes between two times
