---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nth
offline_file: ""
offline_thumbnail: ""
uuid: 887b713b-126f-4718-8459-4dc749d9286b
updated: 1484310212
title: nth
categories:
    - Dictionary
---
nth
     adj : last or greatest in an indefinitely large series; "to the
           nth degree"
