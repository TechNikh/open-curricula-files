---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multipartition
offline_file: ""
offline_thumbnail: ""
uuid: 32e04190-199c-404a-89ab-04381e289044
updated: 1484308577
title: Multipartition
categories:
    - Combinatorics
---
In number theory and combinatorics, a multipartition of a positive integer n is a way of writing n as a sum, each element of which is in turn a partition. The concept is also found in the theory of Lie algebras.
