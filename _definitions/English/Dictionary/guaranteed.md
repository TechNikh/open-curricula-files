---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guaranteed
offline_file: ""
offline_thumbnail: ""
uuid: 8abf483c-f3df-4825-a4a2-7c9f4067c2da
updated: 1484310593
title: guaranteed
categories:
    - Dictionary
---
guaranteed
     adj : secured by written agreement [syn: {bonded}, {secured}, {warranted}]
