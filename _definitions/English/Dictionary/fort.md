---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fort
offline_file: ""
offline_thumbnail: ""
uuid: c42866f5-2087-4dc1-bb6d-cd48462b1aea
updated: 1484310316
title: fort
categories:
    - Dictionary
---
fort
     n 1: a fortified military post where troops are stationed [syn: {garrison}]
     2: a fortified defensive structure [syn: {fortress}]
     v 1: gather in, or as if in, a fort, as for protection or defense
          [syn: {fort up}]
     2: enclose by or as if by a fortification [syn: {fortify}]
     3: station (troops) in a fort
