---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Erasure_code
offline_file: ""
offline_thumbnail: ""
uuid: 872d5b3f-53f1-40a3-9297-922ac91da17a
updated: 1484309020
title: Erasure code
tags:
    - Optimal erasure codes
    - Parity check
    - Polynomial oversampling
    - 'Example: Err-mail (k = 2)'
    - General case
    - Real world implementation
    - Near-optimal erasure codes
    - Examples
    - Near optimal erasure codes
    - Near optimal fountain (rateless erasure) codes
    - Optimal erasure codes
    - Other
categories:
    - Coding theory
---
In information theory, an erasure code is a forward error correction (FEC) code for the binary erasure channel, which transforms a message of k symbols into a longer message (code word) with n symbols such that the original message can be recovered from a subset of the n symbols. The fraction r = k/n is called the code rate, the fraction k’/k, where k’ denotes the number of symbols required for recovery, is called reception efficiency.
