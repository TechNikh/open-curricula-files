---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/both
offline_file: ""
offline_thumbnail: ""
uuid: 143edc70-85e3-44c9-a830-91854289e4d4
updated: 1484310355
title: both
categories:
    - Dictionary
---
both
     adj : (used with count nouns) two considered together; the two;
           "both girls are pretty" [syn: {both(a)}]
