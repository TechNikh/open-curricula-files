---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fourth
offline_file: ""
offline_thumbnail: ""
uuid: 0b80ebd1-c353-4ba6-be8c-4949ed37d7d8
updated: 1484310264
title: fourth
categories:
    - Dictionary
---
fourth
     adj : coming next after the third and just before the fifth in
           position or time or degree or magnitude; "the
           quaternary period of geologic time extends from the end
           of the tertiary period to the present" [syn: {4th}, {quaternary}]
     n 1: following the third position; number four in a countable
          series
     2: one of four equal parts; "a quarter of a pound" [syn: {one-fourth},
         {quarter}, {fourth part}, {twenty-five percent}, {quartern}]
     3: the musical interval between one note and another four notes
        away from it
     adv : in the fourth place; "fourthly, you must pay the rent on the
           first of the month" ...
