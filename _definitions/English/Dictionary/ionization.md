---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ionization
offline_file: ""
offline_thumbnail: ""
uuid: 6d9e78f2-d4c8-4d01-8738-506a55d7912c
updated: 1484310401
title: ionization
categories:
    - Dictionary
---
ionization
     n 1: the condition of being dissociated into ions (as by heat or
          radiation or chemical reaction or electrical discharge);
          "the ionization of a gas" [syn: {ionisation}]
     2: the process of ionizing; the formation of ions by separating
        atoms or molecules or radicals or by adding or subtracting
        electrons from atoms by strong electric fields in a gas
        [syn: {ionisation}]
