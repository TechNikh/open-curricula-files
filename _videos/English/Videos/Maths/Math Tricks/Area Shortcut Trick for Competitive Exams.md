---
version: 1
type: video
provider: YouTube
id: FQAvryj1Y5I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8407508c-f7ab-402c-9aa8-02f522e01351
updated: 1486069622
title: Area Shortcut Trick for Competitive Exams
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/FQAvryj1Y5I/default.jpg
    - https://i3.ytimg.com/vi/FQAvryj1Y5I/1.jpg
    - https://i3.ytimg.com/vi/FQAvryj1Y5I/2.jpg
    - https://i3.ytimg.com/vi/FQAvryj1Y5I/3.jpg
---
