---
version: 1
type: video
provider: YouTube
id: 6ldtdWjDwes
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Sanger%20Sequencing.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9f9f0247-1b36-41c7-b31b-398019fd2bf0
updated: 1486069672
title: Sanger Sequencing
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/6ldtdWjDwes/default.jpg
    - https://i3.ytimg.com/vi/6ldtdWjDwes/1.jpg
    - https://i3.ytimg.com/vi/6ldtdWjDwes/2.jpg
    - https://i3.ytimg.com/vi/6ldtdWjDwes/3.jpg
---
