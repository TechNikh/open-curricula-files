---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/importing
offline_file: ""
offline_thumbnail: ""
uuid: 56c52987-3153-4486-b99c-b26909dcc4c2
updated: 1484310529
title: importing
categories:
    - Dictionary
---
importing
     n : the commercial activity of buying and bringing in goods from
         a foreign country [syn: {importation}]
