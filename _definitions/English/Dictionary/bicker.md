---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bicker
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484635621
title: bicker
categories:
    - Dictionary
---
bicker
     n : a quarrel about petty points [syn: {bickering}, {spat}, {tiff},
          {squabble}, {pettifoggery}, {fuss}]
     v : argue over petty things; "Let's not quibble over pennies"
         [syn: {quibble}, {niggle}, {pettifog}, {squabble}, {brabble}]
