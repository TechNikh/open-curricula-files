---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Small-bias_sample_space
offline_file: ""
offline_thumbnail: ""
uuid: 689f3db4-21f0-46d9-8643-7f7a9fa38680
updated: 1484308684
title: Small-bias sample space
tags:
    - Definition
    - Bias
    - ϵ-biased sample space
    - ϵ-biased set
    - ϵ-biased generator
    - Connection with epsilon-balanced error-correcting codes
    - Constructions of small epsilon-biased sets
    - Theoretical bounds
    - Explicit constructions
    - 'Application: almost k-wise independence'
    - k-wise independent spaces
    - Constructions and bounds
    - "Joffe's construction"
    - Almost k-wise independent spaces
    - Constructions
    - Notes
categories:
    - Theoretical computer science
---
In theoretical computer science, a small-bias sample space (also known as 
  
    
      
        ϵ
      
    
    {\displaystyle \epsilon }
  
-biased sample space, 
  
    
      
        ϵ
      
    
    {\displaystyle \epsilon }
  
-biased generator, or small-bias probability space) is a probability distribution that fools parity functions. In other words, no parity function can distinguish between a small-bias sample space and the uniform distribution with high probability, and hence, small-bias sample spaces naturally give rise to pseudorandom generators for parity functions.
