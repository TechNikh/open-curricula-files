---
version: 1
type: video
provider: YouTube
id: Y2DtwCTS0ic
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: afdc594f-1ece-4871-bff2-281e00a9e6cd
updated: 1486069639
title: Fun Light Experiments
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/Y2DtwCTS0ic/default.jpg
    - https://i3.ytimg.com/vi/Y2DtwCTS0ic/1.jpg
    - https://i3.ytimg.com/vi/Y2DtwCTS0ic/2.jpg
    - https://i3.ytimg.com/vi/Y2DtwCTS0ic/3.jpg
---
