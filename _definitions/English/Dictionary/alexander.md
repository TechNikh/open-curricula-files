---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alexander
offline_file: ""
offline_thumbnail: ""
uuid: b66baa11-06ec-4a9e-adbf-acdadd51fb65
updated: 1484310172
title: alexander
categories:
    - Dictionary
---
Alexander
     n 1: European herb somewhat resembling celery widely naturalized
          in Britain coastal regions and often cultivated as a
          potherb [syn: {Alexanders}, {black lovage}, {horse
          parsley}, {Smyrnium olusatrum}]
     2: king of Macedon; conqueror of Greece and Egypt and Persia;
        founder of Alexandria (356-323 BC) [syn: {Alexander the
        Great}]
