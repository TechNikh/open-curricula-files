---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fair_item_assignment
offline_file: ""
offline_thumbnail: ""
uuid: ea04d460-f9dc-4be4-9c1f-ab6ae0475f28
updated: 1484308520
title: Fair item assignment
tags:
    - Preferences
    - Combinatorial preferences
    - Additive preferences
    - Compact preference representation languages
    - Fairness criteria
    - Individual guarantee criteria
    - Global optimization criteria
    - Assignment algorithms
    - Max-min-share fairness
    - Proportionality
    - Min-max-share fairness
    - Envy-freeness (without money)
    - Envy-freeness (with money)
    - Egalitarian-optimal allocations
    - Nash-optimal allocations
    - Picking sequences
    - Empirical evidence
categories:
    - Fair division
---
Fair item assignment is a kind of a fair division problem in which the items to divide are indivisible. The items have to be divided among several partners who value them differently, and each item has to be given as a whole to a single person. This situation arises in various real-life scenarios:
