---
version: 1
type: video
provider: YouTube
id: XDsk6tZX55g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 43d739ee-966a-44b6-ba6e-3f54469c1a3e
updated: 1486069682
title: 'The Physics of Music- Crash Course Physics #19'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/XDsk6tZX55g/default.jpg
    - https://i3.ytimg.com/vi/XDsk6tZX55g/1.jpg
    - https://i3.ytimg.com/vi/XDsk6tZX55g/2.jpg
    - https://i3.ytimg.com/vi/XDsk6tZX55g/3.jpg
---
