---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Morse/Long-range_potential
offline_file: ""
offline_thumbnail: ""
uuid: 9d5e06a0-e48f-4fea-8370-1da9a88cbf1f
updated: 1484308855
title: Morse/Long-range potential
tags:
    - Historical origins
    - Function
    - Applications
categories:
    - Thermodynamics
---
Owing to the simplicity of the Morse potential (it only has three adjustable parameters), it is not used in modern spectroscopy. The MLR (Morse/Long-range) potential is a modern version of the Morse potential which has the correct theoretical long-range form of the potential naturally built into it.[1] It was first introduced by professor Robert J. Le Roy of University of Waterloo, professor Nikesh S. Dattani of Oxford University and professor John A. Coxon of Dalhousie University in 2009[1] Since then it has been an important tool for spectroscopists to represent experimental data, verify measurements, and make predictions. It is particularly renowned for its extrapolation capability when ...
