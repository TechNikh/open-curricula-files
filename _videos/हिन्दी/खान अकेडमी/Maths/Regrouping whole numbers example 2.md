---
version: 1
type: video
provider: YouTube
id: aczWKh-lz7g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 91ad54e7-ec7d-4476-9be6-b55eb76fdbf9
updated: 1486069613
title: Regrouping whole numbers example 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/aczWKh-lz7g/default.jpg
    - https://i3.ytimg.com/vi/aczWKh-lz7g/1.jpg
    - https://i3.ytimg.com/vi/aczWKh-lz7g/2.jpg
    - https://i3.ytimg.com/vi/aczWKh-lz7g/3.jpg
---
