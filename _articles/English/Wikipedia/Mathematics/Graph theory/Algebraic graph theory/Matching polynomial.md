---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Matching_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: 4a281099-023c-4ece-be58-c79531ba1740
updated: 1484309376
title: Matching polynomial
tags:
    - Definition
    - Connections to other polynomials
    - Complementation
    - Applications in chemical informatics
    - Computational complexity
categories:
    - Algebraic graph theory
---
In the mathematical fields of graph theory and combinatorics, a matching polynomial (sometimes called an acyclic polynomial) is a generating function of the numbers of matchings of various sizes in a graph.
