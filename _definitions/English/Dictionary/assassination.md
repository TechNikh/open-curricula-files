---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assassination
offline_file: ""
offline_thumbnail: ""
uuid: 907a2d1d-d3d3-4803-8ba2-ef997819bfd1
updated: 1484310589
title: assassination
categories:
    - Dictionary
---
assassination
     n 1: an attack intended to ruin someone's reputation [syn: {character
          assassination}, {blackwash}]
     2: murder of a public figure by surprise attack
