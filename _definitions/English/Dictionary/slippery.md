---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slippery
offline_file: ""
offline_thumbnail: ""
uuid: b5f9cd59-39d8-4af8-940a-1c30ec14abe6
updated: 1484310335
title: slippery
categories:
    - Dictionary
---
slippery
     adj 1: being such as to cause things to slip or slide; "slippery
            sidewalks"; "a slippery bar of soap"; "the streets are
            still slippy from the rain" [syn: {slippy}] [ant: {nonslippery}]
     2: not to be trusted; "how extraordinarily slippery a liar the
        camera is"- James Agee; "they called Reagan the teflon
        president because mud never stuck to him" [syn: {tricky},
        {teflon}]
