---
version: 1
type: video
provider: YouTube
id: 2yqpdQsepWo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 82a89dbf-1187-42b4-82af-fd79092159a8
updated: 1486069613
title: Counting by tens examples
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/2yqpdQsepWo/default.jpg
    - https://i3.ytimg.com/vi/2yqpdQsepWo/1.jpg
    - https://i3.ytimg.com/vi/2yqpdQsepWo/2.jpg
    - https://i3.ytimg.com/vi/2yqpdQsepWo/3.jpg
---
