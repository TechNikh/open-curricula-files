---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/even
offline_file: ""
offline_thumbnail: ""
uuid: f9cf0090-916f-4bfa-8d9f-a393f9a9a654
updated: 1484310357
title: even
categories:
    - Dictionary
---
even
     adj 1: divisible by two [ant: {odd}]
     2: equal in degree or extent or amount; or equally matched or
        balanced; "even amounts of butter and sugar"; "on even
        terms"; "it was a fifty-fifty (or even) split"; "had a
        fifty-fifty (or even) chance"; "an even fight" [syn: {fifty-fifty}]
     3: being level or straight or regular and without variation as
        e.g. in shape or texture; or being in the same plane or at
        the same height as something else (i.e. even with); "an
        even application of varnish"; "an even floor"; "the road
        was not very even"; "the picture is even with the window"
        [ant: {uneven}]
     4: symmetrically ...
