---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydrophilic
offline_file: ""
offline_thumbnail: ""
uuid: 3566d681-aa34-496a-bba3-f9bea1b4db4f
updated: 1484310429
title: hydrophilic
categories:
    - Dictionary
---
hydrophilic
     adj : having a strong affinity for water; tending to dissolve in,
           mix with, or be wetted by water [ant: {hydrophobic}]
