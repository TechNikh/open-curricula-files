---
version: 1
type: annotation
id: 'http://eschool2go.org/sites/default/files/chapter_3%20Production%20and%20Employment.pdf'
uuid: 9be59193-52d8-4d38-8b3f-04173c5c60e7
updated: "1483665301"
annotations:
    - 
        quote: gdp
        id: https://en.wikipedia.org/wiki/Gdp
        uuid: 697959ab-c85a-4767-90e2-57aa6158c342
        text: Gross domestic product
        comment: >
            Gross domestic product (GDP) is a monetary measure of the
            market value of all final goods and services produced in a
            period (quarterly or yearly). Nominal GDP estimates are
            commonly used to determine the economic performance of a
            whole country or region, and to make international
            comparisons. Nominal GDP per capita does not, however,
            reflect differences in the cost of living and the inflation
            rates of the countries; therefore using a GDP PPP per capita
            basis is arguably more useful when comparing differences in
            living standards between nations.
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/370px-Countries_by_GDP_%2528Nominal%2529_in_2014.svg.png'
            uuid: d06efee4-d476-48f7-826a-ee507a412b7c
            comment: 'A map of world economies by size of GDP (nominal) in USD, World Bank, 2014.[1]'
    - 
        quote: bee-keeper
        id: https://en.wikipedia.org/wiki/Bee-keeper
        uuid: 9d2a4020-a55d-4635-88a7-ef82fa19a4cb
        text: Beekeeper
        comment: >
            Honey bees produce commodities such as honey, beeswax,
            pollen, propolis, and royal jelly, while some beekeepers
            also raise queens and bees to sell to other farmers and to
            satisfy scientific curiosity. Beekeepers also use honeybees
            to provide pollination services to fruit and vegetable
            growers. Many people keep bees as a hobby. Others do it for
            income, either as a sideline to other work or as a
            commercial operator. These factors affect the number of
            colonies maintained by the beekeeper.
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Beekeeper_keeping_bees.jpg
            uuid: 4a98c270-4981-43e5-84b6-1c9df095343d
            comment: A beekeeper holding a brood frame, in Lower Saxony, Germany
    - 
        quote: agg
        id: https://en.wikipedia.org/wiki/Agg
        uuid: ee8c0b3a-c0d6-48e4-9119-961fa1192403
        text: AGG
        comment: 'As an acronym:'
    - 
        quote: idli
        id: https://en.wikipedia.org/wiki/Idli
        uuid: e84b6271-0559-41cd-a350-bcfe28bf209a
        text: Idli
        comment: >
            Idli /ɪdliː/) is a traditional breakfast in South Indian
            households. Idli is a savoury cake that is popular
            throughout India and neighbouring countries like Sri Lanka.
            The cakes are made by steaming a batter consisting of
            fermented black lentils (de-husked) and rice. The
            fermentation process breaks down the starches so that they
            are more readily metabolized by the body.
    - 
        quote: dosa
        id: https://en.wikipedia.org/wiki/Dosa
        uuid: 9e5eb19a-242b-448c-988d-9ecabb6279d1
        text: Dosa
        comment: >
            Dosa is a type of pancake made from a fermented batter. It
            is not a crepe. Its main ingredients are rice and black
            gram. Dosa is a typical part of the South Indian diet and
            popular all over the Indian subcontinent. Traditionally,
            Dosa is served hot along with sambar and chutney. It can be
            consumed with idli podi as well.
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Masala_Dosa_as_served_in_Tamil_Nadu%252CIndia.JPG'
            uuid: 1d2561db-22ed-4bfb-b8c6-fb10692640a2
            comment: Masala Dosai as served in Tamil Nadu, India.
    - 
        quote: post-office
        id: https://en.wikipedia.org/wiki/Post-office
        uuid: 98e54640-b056-418b-ad36-2fdf114a7cc2
        text: Post office
        comment: 'A post office is a customer service facility forming part of a national postal system.[1] Post offices offer mail-related services such as acceptance of letters and parcels; provision of post office boxes; and sale of postage stamps, packaging, and stationery. In addition, many post offices offer additional services: providing and accepting government forms (such as passport applications), processing government services and fees (such as road tax), and banking services (such as savings accounts and money orders).[2] The chief administrator of a post office is a postmaster.'
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-%25E7%259C%25BA%25E6%259C%259B%25E4%25B8%258A%25E6%25B5%25B7%25E5%25B8%2582%25E9%2582%25AE%25E6%2594%25BF%25E5%25B1%2580.jpg'
            uuid: 47d39cc9-91f3-40a4-ab84-97af025b093a
            comment: The General Post Office Building in Shanghai, China.
    - 
        quote: non-bank
        id: https://en.wikipedia.org/wiki/Non-bank
        uuid: 9dfc39a9-d451-4a98-9a0f-50b4e04338ea
        text: Non-bank financial institution
        comment: |
            A non-bank financial institution (NBFI) is a financial institution that does not have a full banking license or is not supervised by a national or international banking regulatory agency. NBFIs facilitate bank-related financial services, such as investment, risk pooling, contractual savings, and market brokering.[1] Examples of these include insurance firms, pawn shops, cashier's check issuers, check cashing locations, payday lending, currency exchanges, and microloan organizations.[2][3][4] Alan Greenspan has identified the role of NBFIs in strengthening an economy, as they provide "multiple alternatives to transform an economy's savings into capital investment [which] act as backup ...
    - 
        quote: underemployed
        id: https://en.wikipedia.org/wiki/Underemployed
        uuid: 19f32490-68b7-41f1-866b-9b4607fd919b
        text: Underemployed
        comment: 'Underemployed may refer to:'
    - 
        quote: gayathri
        id: https://en.wikipedia.org/wiki/Gayathri
        uuid: d52eddca-efe8-49ec-ae94-f1020136df2b
        text: Gayatri
        comment: 'Gayatri (Sanskrit: gāyatrī) is the feminine form of gāyatra, a Sanskrit word for a song or a hymn, having a Vedic meter of three padas, or lines, of eight syllables. In particular, it refers to the Gayatri Mantra and the Goddess Gāyatrī as that mantra personified.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gayatri1.jpg
            uuid: 08883470-c1b6-4664-b4ca-50eec80b47bd
            comment: >
                Illustration by Raja Ravi Verma. In illustrations, the
                goddess often sits on a lotus flower and appears with five
                heads and five pairs of hands, representing the incarnations
                of the goddess as Parvati, Saraswati etc. She is Saraswatī.
                She is the ...
    - 
        quote: jowar
        id: https://en.wikipedia.org/wiki/Jowar
        uuid: c99c3775-f386-4906-84cf-6b6f579d07bd
        text: Sorghum bicolor
        comment: "Sorghum bicolor, commonly called sorghum[2] (/ˈsɔːrɡəm/) and also known as great millet,[3] durra, jowari, or milo, is a grass species cultivated for its grain, which is used for food, both for animals and humans, and for ethanol production. Sorghum originated in northern Africa, and is now cultivated widely in tropical and subtropical regions.[4] Sorghum is the world's fifth most important cereal crop after rice, wheat, maize and barley. S. bicolor is typically an annual, but some cultivars are perennial. It grows in clumps that may reach over 4 m high. The grain is small, ranging from 2 to 4 mm in diameter. Sweet sorghums are sorghum cultivars that are primarily grown for foliage, ..."
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sorghum_head_in_India.jpg
            uuid: f6056ec1-c798-4c0a-bee8-198f7ab9d6f2
            comment: Seed head of sorghum in India
    - 
        quote: redgram
        id: https://en.wikipedia.org/wiki/Redgram
        uuid: eb32d144-311b-4e2e-a5f7-5ebbf69f024e
        text: Pigeon pea
        comment: >
            The pigeon pea (Cajanus cajan) is a perennial legume from
            the family Fabaceae. Since its domestication in India at
            least 3,500 years ago, its seeds have become a common food
            grain in Asia, Africa, and Latin America. It is consumed on
            a large scale mainly in south Asia and is a major source of
            protein for the population of that subcontinent.
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cajanus_cajan.jpg
            uuid: 234dd512-d97a-439b-a340-eb8c398e4ad4
            comment: Pigeon pea is a perennial which can grow into a small tree.
    - 
        quote: ector
        id: https://en.wikipedia.org/wiki/Ector
        uuid: 9d9f1f26-b6a5-44ee-ada3-de1518ec1dfa
        text: Ector
        comment: 'Ector can refer to:'
    - 
        quote: narasimha
        id: https://en.wikipedia.org/wiki/Narasimha
        uuid: ffb39b8b-5f03-4fdc-a439-4cdcf58a7910
        text: Narasimha
        comment: 'Narsingh (Sanskrit: IAST: Narasiṃha, lit. man-lion), Narasingh, and Narasingha and Narasimhar in Dravidian languages, is an avatar of the Hindu god Vishnu, who is regarded as the supreme God in Vaishnavism and a popular deity in the broader Hinduism. The avatar of Narasimha is evidenced in early epics, iconography, and temple and festival worship for over a millennium.[1]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Narasimha_oil_colour.jpg
            uuid: b5d577f1-f3fd-4b3c-b963-a19c84a49a3c
            comment: Narasimha, the Protector
    - 
        quote: sharecroppers
        id: https://en.wikipedia.org/wiki/Sharecroppers
        uuid: be58a077-86f6-4154-80bc-088449beddc3
        text: Sharecropping
        comment: 'Sharecropping is a form of agriculture in which a landowner allows a tenant to use the land in return for a share of the crops produced on their portion of land. Sharecropping has a long history and there are a wide range of different situations and types of agreements that have used a form of the system. Some are governed by tradition, and others by law. Legal contract systems such as the Italian mezzadria, the French métayage, the Spanish mediero, or the Islamic system of muqasat, occur widely.[citation needed]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Greene_Co_Ga1941_Delano.jpg
            uuid: f73ce0f1-9446-4f5b-82bc-677d1b40447e
            comment: >
                A FSA photo of a cropper family chopping the weeds from
                cotton near White Plains, in Georgia, USA (1941).
    - 
        quote: irrigated
        id: https://en.wikipedia.org/wiki/Irrigated
        uuid: 17b0a4ec-6ada-4f2a-81f0-7113593e94c4
        text: Irrigation
        comment: 'Irrigation is the method in which a controlled amount of water is supplied to plants at regular intervals for agriculture. It is used to assist in the growing of agricultural crops, maintenance of landscapes, and revegetation of disturbed soils in dry areas and during periods of inadequate rainfall. Additionally, irrigation also has a few other uses in crop production, which include protecting plants against frost,[1] suppressing weed growth in grain fields[2] and preventing soil consolidation.[3] In contrast, agriculture that relies only on direct rainfall is referred to as rain-fed or dry land farming.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Irrigational_sprinkler.jpg
            uuid: cafeecfb-6543-4923-8eb1-8aec8bd40648
            comment: An irrigation sprinkler watering a lawn
    - 
        quote: millets
        id: https://en.wikipedia.org/wiki/Millets
        uuid: 5fe615bb-1e03-433d-bf80-28f8afbe7931
        text: Millet
        comment: 'Millets are a group of highly variable small-seeded grasses, widely grown around the world as cereal crops or grains for fodder and human food. Millets are important crops in the semiarid tropics of Asia and Africa (especially in India, Mali, Nigeria, and Niger), with 97% of millet production in developing countries.[1] The crop is favored due to its productivity and short growing season under dry, high-temperature conditions.'
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Grain_millet%252C_early_grain_fill%252C_Tifton%252C_7-3-02.jpg'
            uuid: 71ed08b3-dbc6-4b56-8716-7f490c057fab
            comment: Pearl millet in the field
    - 
        quote: ii
        id: https://en.wikipedia.org/wiki/Ii
        uuid: 10f34941-72be-420f-b7f4-afe2c4e4c301
        text: II (disambiguation)
        comment: 'II is the Roman numeral for two. This may also refer to:'
    - 
        quote: i
        id: https://en.wikipedia.org/wiki/I
        uuid: bc0200d2-5d0b-4174-ad55-9c94eb72838b
        text: I
        comment: ""
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-I_cursiva.gif
            uuid: 419a4be9-26f7-4e8d-868e-3f91bfa75abc
            comment: Writing cursive forms of I
---
