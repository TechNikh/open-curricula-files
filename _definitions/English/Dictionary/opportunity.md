---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/opportunity
offline_file: ""
offline_thumbnail: ""
uuid: 624db7b5-1f86-4732-ae89-b8613aba4dc9
updated: 1484310441
title: opportunity
categories:
    - Dictionary
---
opportunity
     n : a possibility due to a favorable combination of
         circumstances; "the holiday gave us the opportunity to
         visit Washington"; "now is your chance" [syn: {chance}]
