---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fei%E2%80%93Ranis_model_of_economic_growth'
offline_file: ""
offline_thumbnail: ""
uuid: ff0dc106-8e85-43f0-b018-5d3ad90e2347
updated: 1484309234
title: Fei–Ranis model of economic growth
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Capital_labour.jpg
tags:
    - Basics of the model
    - Connectivity between sectors
    - Agricultural sector
    - Industrial sector
    - Agricultural surplus
    - Generation of agricultural surplus
    - Agricultural surplus as wage fund
    - Significance of agriculture in the Fei–Ranis model
    - The indispensability of labor reallocation
    - Growth without development
    - Reactions to the model
categories:
    - Duality theories
---
The Fei–Ranis model of economic growth is a dualism model in developmental economics or welfare economics that has been developed by John C. H. Fei and Gustav Ranis and can be understood as an extension of the Lewis model. It is also known as the Surplus Labor model. It recognizes the presence of a dual economy comprising both the modern and the primitive sector and takes the economic situation of unemployment and underemployment of resources into account, unlike many other growth models that consider underdeveloped countries to be homogenous in nature.[1] According to this theory, the primitive sector consists of the existing agricultural sector in the economy, and the modern sector is ...
