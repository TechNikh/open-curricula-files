---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/melt
offline_file: ""
offline_thumbnail: ""
uuid: 54d570b5-229a-4935-aa24-fc96f5b59329
updated: 1484310196
title: melt
categories:
    - Dictionary
---
melt
     n : the process whereby heat changes something from a solid to a
         liquid; "the power failure caused a refrigerator melt
         that was a disaster"; "the thawing of a frozen turkey
         takes several hours" [syn: {thaw}, {thawing}, {melting}]
     v 1: reduce or cause to be reduced from a solid to a liquid
          state, usually by heating; "melt butter"; "melt down
          gold"; "The wax melted in the sun" [syn: {run}, {melt
          down}]
     2: become or cause to become soft or liquid; "The sun melted
        the ice"; "the ice thawed"; "the ice cream melted"; "The
        heat melted the wax"; "The giant iceberg dissolved over
        the years during the ...
