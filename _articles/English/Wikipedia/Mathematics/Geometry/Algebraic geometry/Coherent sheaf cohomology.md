---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coherent_sheaf_cohomology
offline_file: ""
offline_thumbnail: ""
uuid: 9d444f31-4955-4046-a049-db2086a953c2
updated: 1484309136
title: Coherent sheaf cohomology
tags:
    - Coherent sheaves
    - Sheaf cohomology
    - Vanishing theorems in the affine case
    - Cech cohomology, and the cohomology of projective space
    - Finite-dimensionality of cohomology
    - Serre duality
    - GAGA theorems
    - Vanishing theorems
    - Hodge theory
    - Riemann–Roch theorems
    - Notes
categories:
    - Algebraic geometry
---
In mathematics, especially in algebraic geometry and the theory of complex manifolds, coherent sheaf cohomology is a technique for producing functions with specified properties. Many geometric questions can be formulated as questions about the existence of sections of line bundles or of more general coherent sheaves; such sections can be viewed as generalized functions. Cohomology provides computable tools for producing sections, or explaining why they do not exist. It also provides invariants to distinguish one algebraic variety from another.
