---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/responsible
offline_file: ""
offline_thumbnail: ""
uuid: 578f8535-47f6-4e18-82cc-adc02d48f5c6
updated: 1484310363
title: responsible
categories:
    - Dictionary
---
responsible
     adj 1: worthy of or requiring responsibility or trust; or held
            accountable; "a responsible adult"; "responsible
            journalism"; "a responsible position"; "the captain is
            responsible for the ship's safety"; "the cabinet is
            responsible to the parliament" [ant: {irresponsible}]
     2: being the agent or cause; "determined who was the
        responsible party"; "termites were responsible for the
        damage" [syn: {responsible for(p)}]
     3: having an acceptable credit rating; "a responsible borrower"
        [syn: {creditworthy}]
