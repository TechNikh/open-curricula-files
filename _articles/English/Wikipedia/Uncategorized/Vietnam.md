---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vietnam
offline_file: ""
offline_thumbnail: ""
uuid: fa679f2d-d0ff-4810-8705-7d8cb3d890b5
updated: 1484308482
title: Vietnam
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/190px-Vietnam_Expand1.gif
tags:
    - Etymology
    - History
    - Prehistory and ancient history
    - Dynastic Vietnam
    - '1862–1945: French Indochina'
    - '1946–54: First Indochina War'
    - '1954–1975: Vietnam War'
    - '1976–present: reunification and reforms'
    - Government and politics
    - Legislature
categories:
    - Uncategorized
---
Vietnam (UK /ˌvjɛtˈnæm, -ˈnɑːm/, US i/ˌviːətˈnɑːm, -ˈnæm/;[6] Vietnamese: Việt Nam [viət˨ nam˧] ( listen)), officially the Socialist Republic of Vietnam (SRV; Vietnamese: Cộng hòa Xã hội chủ nghĩa Việt Nam ( listen)), is the easternmost country on the Indochina Peninsula in Southeast Asia. With an estimated 90.5 million inhabitants as of 2014[update], it is the world's 14th-most-populous country, and the eighth-most-populous Asian country. Vietnam is bordered by China to the north, Laos to the northwest, Cambodia to the southwest, and Malaysia across the South China Sea to the southeast.[d] Its capital city has been Hanoi since the reunification of North and ...
