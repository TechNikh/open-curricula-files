---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attitude
offline_file: ""
offline_thumbnail: ""
uuid: 489359da-cc29-4a2b-94df-5c65e3ce37bb
updated: 1484310583
title: attitude
categories:
    - Dictionary
---
attitude
     n 1: a complex mental state involving beliefs and feelings and
          values and dispositions to act in certain ways; "he had
          the attitude that work was fun" [syn: {mental attitude}]
     2: position or arrangement of the body and its limbs; "he
        assumed an attitude of surrender" [syn: {position}, {posture}]
     3: a theatrical pose created for effect; "the actor struck just
        the right attitude"
     4: position of aircraft or spacecraft relative to a frame of
        reference (the horizon or direction of motion)
