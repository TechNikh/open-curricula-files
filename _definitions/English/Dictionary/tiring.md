---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tiring
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484485141
title: tiring
categories:
    - Dictionary
---
tiring
     adj : producing exhaustion; "an exhausting march"; "the visit was
           especially wearing" [syn: {exhausting}, {wearing}, {wearying}]
