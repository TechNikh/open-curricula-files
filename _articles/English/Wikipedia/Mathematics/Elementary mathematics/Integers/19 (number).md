---
version: 1
type: article
id: https://en.wikipedia.org/wiki/19_(number)
offline_file: ""
offline_thumbnail: ""
uuid: e35e99d7-5387-4751-a9c4-5659e8600b11
updated: 1484308948
title: 19 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Centered_triangular_number_19.svg.png
tags:
    - Mathematics
    - Technology
    - Science
    - religion
    - Islam
    - "Baha'i faith"
    - Music
    - Literature
    - Games
    - Age 19
    - In sports
    - Other fields
categories:
    - Integers
---
In English speech, the numbers 19 and 90 are often confused, as sounding very similar. When carefully enunciated, they differ in which syllable is stressed: 19 /naɪnˈtiːn/ vs 90 /ˈnaɪnti/. However, in dates such as 1999, and when contrasting numbers in the teens and when counting, such as 17, 18, 19, the stress shifts to the first syllable: 19 /ˈnaɪntiːn/.
