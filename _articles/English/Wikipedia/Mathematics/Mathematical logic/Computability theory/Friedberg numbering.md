---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Friedberg_numbering
offline_file: ""
offline_thumbnail: ""
uuid: d3cc2c70-f071-435e-8dab-9ba9be4661e4
updated: 1484309487
title: Friedberg numbering
categories:
    - Computability theory
---
In computability theory, a Friedberg numbering is a numbering (enumeration) of the set of all partial recursive functions that has no repetitions: each partial recursive function appears exactly once in the enumeration (Vereščagin and Shen 2003:30).
