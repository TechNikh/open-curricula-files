---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Radio_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 46d8135b-a10d-4f75-bb82-538e84bfa65b
updated: 1484308506
title: Radio coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Radio_icon.png
categories:
    - Graph theory
---
In graph theory, a branch of mathematics, a radio coloring of an undirected graph is a form of graph coloring in which one assigns positive integer labels to the graphs such that the labels of adjacent vertices differ by at least two, and the labels of vertices at distance two from each other differ by at least one. Radio coloring was first studied by Griggs & Yeh (1992), under a different name, L(2,1)-labeling.[1][2] It was called radio coloring by Frank Harary because it models the problem of channel assignment in radio broadcasting, while avoiding electromagnetic interference between radio stations that are near each other both in the graph and in their assigned channel frequencies.
