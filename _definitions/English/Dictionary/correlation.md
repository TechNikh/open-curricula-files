---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/correlation
offline_file: ""
offline_thumbnail: ""
uuid: 376c6677-b7ed-4ae0-8eec-3d5424787e86
updated: 1484310156
title: correlation
categories:
    - Dictionary
---
correlation
     n 1: a reciprocal relation between two or more things [syn: {correlativity}]
     2: a statistic representing how closely two variables co-vary;
        it can vary from -1 (perfect negative correlation) through
        0 (no correlation) to +1 (perfect positive correlation);
        "what is the correlation between those two variables?"
        [syn: {correlation coefficient}, {coefficient of
        correlation}]
     3: a statistical relation between two or more variables such
        that systematic changes in the value of one variable are
        accompanied by systematic changes in the other [syn: {correlational
        statistics}]
