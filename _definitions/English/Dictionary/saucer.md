---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saucer
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484546402
title: saucer
categories:
    - Dictionary
---
saucer
     n 1: something with a round shape like a flat circular plate
          [syn: {disk}, {disc}]
     2: a small shallow dish for holding a cup at the table
     3: directional antenna consisting of a parabolic reflector for
        microwave or radio frequency radiation [syn: {dish}, {dish
        aerial}, {dish antenna}]
     4: a disk used in throwing competitions [syn: {discus}]
