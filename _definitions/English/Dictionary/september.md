---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/september
offline_file: ""
offline_thumbnail: ""
uuid: 29df107d-96d2-4526-b97d-c21d47e0d87d
updated: 1484310441
title: september
categories:
    - Dictionary
---
September
     n : the month following August and preceding October [syn: {Sep},
          {Sept}]
