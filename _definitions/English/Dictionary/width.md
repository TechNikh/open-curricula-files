---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/width
offline_file: ""
offline_thumbnail: ""
uuid: 4b062ab1-f04c-4af1-8a48-26ce09986240
updated: 1484310435
title: width
categories:
    - Dictionary
---
width
     n : the extent of something from side to side [syn: {breadth}]
