---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tungsten
offline_file: ""
offline_thumbnail: ""
uuid: 237e6d10-39a1-4cd2-909b-752832022b94
updated: 1484310200
title: tungsten
categories:
    - Dictionary
---
tungsten
     n : a heavy gray-white metallic element; the pure form is used
         mainly in electrical applications; it is found in several
         ores including wolframite and scheelite [syn: {wolfram},
         {W}, {atomic number 74}]
