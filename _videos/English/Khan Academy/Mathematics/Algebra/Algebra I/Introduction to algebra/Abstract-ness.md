---
version: 1
type: video
provider: YouTube
id: ZOYRb2sYrL0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Introduction%20to%20algebra/Abstract-ness.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1c73fb02-9f50-46f1-8204-a49656c676bd
updated: 1486069573
title: Abstract-ness
categories:
    - Introduction to algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZOYRb2sYrL0/default.jpg
    - https://i3.ytimg.com/vi/ZOYRb2sYrL0/1.jpg
    - https://i3.ytimg.com/vi/ZOYRb2sYrL0/2.jpg
    - https://i3.ytimg.com/vi/ZOYRb2sYrL0/3.jpg
---
The general idea behind the word 'abstract'

Watch the next lesson: https://www.khanacademy.org/math/algebra/introduction-to-algebra/overview_hist_alg/v/the-beauty-of-algebra?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraI

Missed the previous lesson? 
https://www.khanacademy.org/math/algebra/introduction-to-algebra/overview_hist_alg/v/origins-of-algebra?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraI

Algebra I on Khan Academy: Algebra is the language through which we describe patterns. Think of it as a shorthand, of sorts. As opposed to having to do something over and over again, algebra gives you a simple way to express that repetitive process. It's also seen as a "gatekeeper" subject. Once you achieve an understanding of algebra, the higher-level math subjects become accessible to you. Without it, it's impossible to move forward. It's used by people with lots of different jobs, like carpentry, engineering, and fashion design. In these tutorials, we'll cover a lot of ground. Some of the topics include linear equations, linear inequalities, linear functions, systems of equations, factoring expressions, quadratic expressions, exponents, functions, and ratios.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Algebra channel:
https://www.youtube.com/channel/UCYZrCV8PNENpJt36V0kd-4Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
