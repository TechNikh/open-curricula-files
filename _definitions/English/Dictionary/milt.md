---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/milt
offline_file: ""
offline_thumbnail: ""
uuid: 83989981-9fc1-41b2-b3d3-0fa00907af2e
updated: 1484310162
title: milt
categories:
    - Dictionary
---
milt
     n 1: fish sperm or sperm-filled reproductive gland; having a
          creamy texture [syn: {soft roe}]
     2: seminal fluid produced by male fish
