---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edge-graceful_labeling
offline_file: ""
offline_thumbnail: ""
uuid: 553d58d7-4b88-4657-b1f1-9b934cc7d180
updated: 1484309445
title: Edge-graceful labeling
tags:
    - Definition
    - Examples
    - Paths
    - Cycles
    - A necessary condition
    - Further selected results
categories:
    - Graph theory objects
---
In graph theory, an edge-graceful graph labeling is a type of graph labeling. This is a labeling for simple graphs in which no two distinct edges connect the same two distinct vertices, no edge connects a vertex to itself, and the graph is connected. Edge-graceful labelings were first introduced by S. Lo in his seminal paper.[1]
