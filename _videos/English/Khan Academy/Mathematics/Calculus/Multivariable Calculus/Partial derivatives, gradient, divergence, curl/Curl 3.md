---
version: 1
type: video
provider: YouTube
id: fYzoiWIBjP8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Partial%20derivatives%2C%20gradient%2C%20divergence%2C%20curl/Curl%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d48a0521-7554-43a3-9f42-fc1a9c63fbef
updated: 1486069690
title: Curl 3
categories:
    - Partial derivatives, gradient, divergence, curl
thumbnail_urls:
    - https://i3.ytimg.com/vi/fYzoiWIBjP8/default.jpg
    - https://i3.ytimg.com/vi/fYzoiWIBjP8/1.jpg
    - https://i3.ytimg.com/vi/fYzoiWIBjP8/2.jpg
    - https://i3.ytimg.com/vi/fYzoiWIBjP8/3.jpg
---
