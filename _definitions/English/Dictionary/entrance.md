---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entrance
offline_file: ""
offline_thumbnail: ""
uuid: 1246b5ae-ce55-4571-b72a-bfa79e71f75d
updated: 1484310447
title: entrance
categories:
    - Dictionary
---
entrance
     n 1: something that provides access (entry or exit); "they waited
          at the entrance to the garden"; "beggars waited just
          outside the entryway to the cathedral" [syn: {entranceway},
           {entryway}, {entry}, {entree}]
     2: a movement into or inward [syn: {entering}]
     3: the act of entering; "she made a grand entrance" [syn: {entering},
         {entry}, {ingress}, {incoming}]
     v 1: attract; cause to be enamored; "She captured all the men's
          hearts" [syn: {capture}, {enamour}, {trance}, {catch}, {becharm},
           {enamor}, {captivate}, {beguile}, {charm}, {fascinate},
           {bewitch}, {enchant}]
     2: put into a trance [syn: ...
