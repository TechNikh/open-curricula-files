---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ruled
offline_file: ""
offline_thumbnail: ""
uuid: 1e3df72b-a8e7-4421-855f-9c3d1015c8d4
updated: 1484310475
title: ruled
categories:
    - Dictionary
---
ruled
     adj : subject to a ruling authority; "the ruled mass"
