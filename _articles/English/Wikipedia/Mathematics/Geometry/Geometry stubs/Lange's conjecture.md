---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lange%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 6623aa9e-56eb-4d25-9be8-4f2d7dcf7303
updated: 1484309365
title: "Lange's conjecture"
categories:
    - Geometry stubs
---
In algebraic geometry, Lange's conjecture is a conjecture about stability of vector bundles over curves, introduced by Lange (1983) and proved by Montserrat Teixidor i Bigas and Barbara Russo in 1999.
