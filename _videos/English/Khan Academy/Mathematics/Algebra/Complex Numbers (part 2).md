---
version: 1
type: video
provider: YouTube
id: bPqB9a1uk_8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Complex%20Numbers%20%28part%202%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9e09d104-d05c-4a88-aaa9-6f59fde3993b
updated: 1486069575
title: Complex Numbers (part 2)
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/bPqB9a1uk_8/default.jpg
    - https://i3.ytimg.com/vi/bPqB9a1uk_8/1.jpg
    - https://i3.ytimg.com/vi/bPqB9a1uk_8/2.jpg
    - https://i3.ytimg.com/vi/bPqB9a1uk_8/3.jpg
---
Dividing complex numbers.  Complex conjugates.
