---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outwards
offline_file: ""
offline_thumbnail: ""
uuid: 007f21ec-99a9-4186-85bf-518c6ab95fdf
updated: 1484310220
title: outwards
categories:
    - Dictionary
---
outwards
     adv : toward the outside; "move the needle further outward!" [syn:
            {outward}] [ant: {inward}]
