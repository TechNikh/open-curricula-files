---
version: 1
type: article
id: https://en.wikipedia.org/wiki/6-sphere_coordinates
offline_file: ""
offline_thumbnail: ""
uuid: afabcf3e-1808-4eae-bad6-83c8760cf183
updated: 1484309258
title: 6-sphere coordinates
categories:
    - Inversive geometry
---
In mathematics, 6-sphere coordinates are the coordinate system created by inverting the Cartesian coordinates across the unit sphere. They are so named because the loci where one coordinate is constant form spheres tangent to the origin from one of six sides (depending on which coordinate is held constant and whether its value is positive or negative).
