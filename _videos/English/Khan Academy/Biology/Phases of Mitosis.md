---
version: 1
type: video
provider: YouTube
id: LLKX_4DHE3I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Phases%20of%20Mitosis.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ae7a669d-e007-4164-bca0-1985713d05de
updated: 1486069583
title: Phases of Mitosis
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/LLKX_4DHE3I/default.jpg
    - https://i3.ytimg.com/vi/LLKX_4DHE3I/1.jpg
    - https://i3.ytimg.com/vi/LLKX_4DHE3I/2.jpg
    - https://i3.ytimg.com/vi/LLKX_4DHE3I/3.jpg
---
Explanation of the phases of mitosis.
More free lessons at: http://www.khanacademy.org/video?v=LLKX_4DHE3I
