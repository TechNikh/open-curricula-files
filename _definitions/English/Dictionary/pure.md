---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pure
offline_file: ""
offline_thumbnail: ""
uuid: 7e20391b-420c-42f7-9d94-b9dfd6fa95eb
updated: 1484310311
title: pure
categories:
    - Dictionary
---
pure
     adj 1: free of extraneous elements of any kind; "pure air and
            water"; "pure gold"; "pure primary colors"; "the
            violin's pure and lovely song"; "pure tones" [ant: {impure}]
     2: without qualification; used informally as (often pejorative)
        intensifiers; "an arrant fool"; "a complete coward"; "a
        consummate fool"; "a double-dyed villain"; "gross
        negligence"; "a perfect idiot"; "pure folly"; "what a
        sodding mess"; "stark staring mad"; "a thoroughgoing
        villain"; "utter nonsense" [syn: {arrant(a)}, {complete(a)},
         {consummate(a)}, {double-dyed(a)}, {everlasting(a)}, {gross(a)},
         {perfect(a)}, {pure(a)}, ...
