---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extraordinary
offline_file: ""
offline_thumbnail: ""
uuid: d4b16aed-0eff-43dc-ade9-94fa0bceaba6
updated: 1484310399
title: extraordinary
categories:
    - Dictionary
---
extraordinary
     adj 1: beyond what is ordinary or usual; highly unusual or
            exceptional or remarkable; "extraordinary authority";
            "an extraordinary achievement"; "her extraordinary
            beauty"; "enjoyed extraordinary popularity"; "an
            extraordinary capacity for work"; "an extraordinary
            session of the legislature" [ant: {ordinary}]
     2: far more than usual or expected; "an extraordinary desire
        for approval"; "it was an over-the-top experience" [syn: {over-the-top}]
     3: (of an official) serving an unusual or special function in
        addition to those of the regular officials; "an ambassador
        extraordinary" [syn: ...
