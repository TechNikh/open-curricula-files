---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalized_taxicab_number
offline_file: ""
offline_thumbnail: ""
uuid: 81c345ba-8861-4db7-bdb8-b6e3ee653bbc
updated: 1484308649
title: Generalized taxicab number
categories:
    - Number theory
---
In mathematics, the generalized taxicab number Taxicab(k, j, n) is the smallest number which can be expressed as the sum of j kth positive powers in n different ways. For k = 3 and j = 2, they coincide with taxicab numbers.
