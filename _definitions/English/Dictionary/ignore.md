---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ignore
offline_file: ""
offline_thumbnail: ""
uuid: 8589f4ed-6535-4653-aaa2-0697b16bffd1
updated: 1484310366
title: ignore
categories:
    - Dictionary
---
ignore
     v 1: refuse to acknowledge; "She cut him dead at the meeting"
          [syn: {disregard}, {snub}, {cut}]
     2: bar from attention or consideration; "She dismissed his
        advances" [syn: {dismiss}, {disregard}, {brush aside}, {brush
        off}, {discount}, {push aside}]
     3: fail to notice [ant: {notice}]
     4: give little or no attention to; "Disregard the errors" [syn:
         {neglect}, {disregard}]
     5: be ignorant of or in the dark about [ant: {know}]
