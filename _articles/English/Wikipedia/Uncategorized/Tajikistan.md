---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tajikistan
offline_file: ""
offline_thumbnail: ""
uuid: 7f507550-094a-4c2a-b656-63353f76507f
updated: 1484094290
title: Tajikistan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-MansurISamanidPaintingHistoryofIran.jpg
tags:
    - Name
    - History
    - Early history
    - Russian Tajikistan
    - Soviet Tajikistan
    - Independence
    - Politics
    - geography
    - Administrative divisions
    - Lakes
categories:
    - Uncategorized
---
Tajikistan (i/tɑːˈdʒiːkᵻstɑːn/, /təˈdʒiːkᵻstæn/, or /tæˈdʒiːkiːstæn/; Тоҷикистон [tɔd͡ʒikɪsˈtɔn]), officially the Republic of Tajikistan (Tajik: Ҷумҳурии Тоҷикистон, Çumhuriji Toçikiston), is a mountainous, landlocked country in Central Asia with an estimated 8 million people in 2013, and an area of 143,100 km2 (55,300 sq mi). It is bordered by Afghanistan to the south, Uzbekistan to the west, Kyrgyzstan to the north, and China to the east. Pakistan lies to the south, separated by the narrow Wakhan Corridor. Traditional homelands of Tajik people included present-day Tajikistan, Afghanistan and Uzbekistan.
