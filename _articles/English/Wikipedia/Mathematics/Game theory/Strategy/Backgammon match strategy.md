---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Backgammon_match_strategy
offline_file: ""
offline_thumbnail: ""
uuid: d0b641af-2fcd-4899-a2bf-e1ba2b71cad1
updated: 1484309065
title: Backgammon match strategy
tags:
    - Match format
    - The doubling cube
    - The Crawford rule
    - Doubling strategy
    - General
    - Match specific doubling strategies
    - Double match point
    - 2-away, 2-away
    - 1-away, 2-away (Crawford game)
    - '1-away, 2-away (Post-Crawford game) - the "free drop"'
categories:
    - Strategy
---
In backgammon, there are a number of strategies that are distinct to match play as opposed to money play. These differences are most apparent when a player is within a few points of winning the match.
