---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bundle
offline_file: ""
offline_thumbnail: ""
uuid: bf140904-36b1-4070-b76c-8c1a7691d354
updated: 1484310148
title: bundle
categories:
    - Dictionary
---
bundle
     n 1: a collection of things wrapped or boxed together [syn: {package},
           {packet}, {parcel}]
     2: a package of several things tied together for carrying or
        storing [syn: {sheaf}]
     3: a large sum of money (especially as pay or profit); "she
        made a bundle selling real estate"; "they sank megabucks
        into their new house" [syn: {pile}, {big bucks}, {megabucks},
         {big money}]
     v 1: make into a bundle; "he bundled up his few possessions"
          [syn: {bundle up}, {roll up}]
     2: gather or cause to gather into a cluster; "She bunched her
        fingers into a fist"; "The students bunched up at the
        registration desk" ...
