---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proof
offline_file: ""
offline_thumbnail: ""
uuid: a4bd7abd-b572-47e3-87d5-96d17a706512
updated: 1484310387
title: proof
categories:
    - Dictionary
---
proof
     adj : (used in combination or as a suffix) able to withstand;
           "temptation-proof"; "childproof locks" [syn: {proof(p)}]
     n 1: any factual evidence that helps to establish the truth of
          something; "if you have any proof for what you say, now
          is the time to produce it" [syn: {cogent evidence}]
     2: a formal series of statements showing that if one thing is
        true something else necessarily follows from it
     3: a measure of alcoholic strength expressed as an integer
        twice the percentage of alcohol present (by volume)
     4: (printing) an impression made to check for errors [syn: {test
        copy}, {trial impression}]
     5: a ...
