---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confederation
offline_file: ""
offline_thumbnail: ""
uuid: c3b092a7-6c36-4c2d-81ce-17abcc9d7526
updated: 1484310589
title: confederation
categories:
    - Dictionary
---
confederation
     n 1: the state of being allied or confederated [syn: {alliance}]
     2: a union of political organizations [syn: {confederacy}, {federation}]
     3: the act of forming an alliance or confederation [syn: {alliance}]
