---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/edible
offline_file: ""
offline_thumbnail: ""
uuid: a845ea0b-c11a-4a9c-9c10-5fe9a80fedc1
updated: 1484310384
title: edible
categories:
    - Dictionary
---
edible
     adj : suitable for use as food [syn: {comestible}, {eatable}]
           [ant: {inedible}]
     n : any substance that can be used as food [syn: {comestible}, {eatable},
          {pabulum}, {victual}, {victuals}]
