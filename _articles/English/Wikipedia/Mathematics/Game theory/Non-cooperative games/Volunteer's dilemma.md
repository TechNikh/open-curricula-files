---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Volunteer%27s_dilemma'
offline_file: ""
offline_thumbnail: ""
uuid: e922efcc-806d-473b-8ab1-a76939fe130f
updated: 1484309059
title: "Volunteer's dilemma"
tags:
    - Payoff matrix
    - Examples in real life
    - The murder of Kitty Genovese
    - The meerkat
categories:
    - Non-cooperative games
---
The volunteer's dilemma game models a situation in which each of X players faces the decision of either making a small sacrifice from which all will benefit, or freeriding.
