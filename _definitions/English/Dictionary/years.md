---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/years
offline_file: ""
offline_thumbnail: ""
uuid: 8602b588-ae88-4190-9b03-46dddad3de1a
updated: 1484310307
title: years
categories:
    - Dictionary
---
years
     n 1: a late time of life; "old age is not for sissies"; "he's
          showing his years"; "age hasn't slowed him down at all";
          "a beard white with eld"; "on the brink of geezerhood"
          [syn: {old age}, {age}, {eld}, {geezerhood}]
     2: a prolonged period of time; "we've known each other for
        ages"; "I haven't been there for years and years" [syn: {long
        time}, {age}]
     3: the time during which someone's life continues; "the
        monarch's last days"; "in his final years" [syn: {days}]
