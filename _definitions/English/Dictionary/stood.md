---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stood
offline_file: ""
offline_thumbnail: ""
uuid: a080e736-d944-48c6-9a64-38decfa868b7
updated: 1484310441
title: stood
categories:
    - Dictionary
---
stand
     n 1: a support or foundation; "the base of the lamp" [syn: {base},
           {pedestal}]
     2: the position where a thing or person stands
     3: a growth of similar plants (usually trees) in a particular
        area; "they cut down a stand of trees"
     4: a small table for holding articles of various kinds; "a
        bedside stand"
     5: a support for displaying various articles; "the newspapers
        were arranged on a rack" [syn: {rack}]
     6: an interruption of normal activity [syn: {standstill}, {tie-up}]
     7: a mental position from which things are viewed; "we should
        consider this problem from the viewpoint of the Russians";
        "teaching ...
