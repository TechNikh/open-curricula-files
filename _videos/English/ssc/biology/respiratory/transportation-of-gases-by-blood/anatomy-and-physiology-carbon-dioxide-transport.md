---
title: Anatomy and Physiology-Carbon Dioxide Transport
youtube_id: oJ_9OqJJzdI
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - transportation-of-gases-by-blood
---
