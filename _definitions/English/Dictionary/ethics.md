---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethics
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484431561
title: ethics
categories:
    - Dictionary
---
ethics
     n 1: motivation based on ideas of right and wrong [syn: {ethical
          motive}, {morals}, {morality}]
     2: the philosophical study of moral values and rules [syn: {moral
        philosophy}]
