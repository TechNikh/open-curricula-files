---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unstable
offline_file: ""
offline_thumbnail: ""
uuid: 0bacc1b8-47df-4290-a3cd-bbcb12ba757f
updated: 1484310571
title: unstable
categories:
    - Dictionary
---
unstable
     adj 1: lacking stability or fixity or firmness; "unstable political
            conditions"; "the tower proved to be unstable in the
            high wind"; "an unstable world economy" [ant: {stable}]
     2: highly or violently reactive; "sensitive and highly unstable
        compounds"
     3: affording no ease or reassurance; "a precarious truce" [syn:
         {precarious}]
     4: suffering from severe mental illness; "of unsound mind"
        [syn: {mentally ill}, {unsound}]
     5: disposed to psychological variability; "his rather unstable
        religious convictions"
     6: subject to change; variable; "a fluid situation fraught with
        uncertainty"; ...
