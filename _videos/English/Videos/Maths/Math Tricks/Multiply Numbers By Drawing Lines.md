---
version: 1
type: video
provider: YouTube
id: bbKjXKV9QNA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 988e1a7d-af60-4845-aaee-a8f4c199d1d1
updated: 1486069623
title: Multiply Numbers By Drawing Lines
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/bbKjXKV9QNA/default.jpg
    - https://i3.ytimg.com/vi/bbKjXKV9QNA/1.jpg
    - https://i3.ytimg.com/vi/bbKjXKV9QNA/2.jpg
    - https://i3.ytimg.com/vi/bbKjXKV9QNA/3.jpg
---
