---
version: 1
type: video
provider: YouTube
id: CROTY1kOW9A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a5c3c8c3-f0bf-4459-9d1a-534cb839c3de
updated: 1486069616
title: Adding hundreds, tens, and ones
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/CROTY1kOW9A/default.jpg
    - https://i3.ytimg.com/vi/CROTY1kOW9A/1.jpg
    - https://i3.ytimg.com/vi/CROTY1kOW9A/2.jpg
    - https://i3.ytimg.com/vi/CROTY1kOW9A/3.jpg
---
