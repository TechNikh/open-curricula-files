---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undernourished
offline_file: ""
offline_thumbnail: ""
uuid: 9cf73ae6-a802-4797-bdf0-85af7d8cfe9c
updated: 1484310537
title: undernourished
categories:
    - Dictionary
---
undernourished
     adj : not getting adequate food; "gaunt underfed children"; "badly
           undernourished" [syn: {ill-fed}, {underfed}]
