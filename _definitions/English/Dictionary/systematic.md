---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/systematic
offline_file: ""
offline_thumbnail: ""
uuid: 7f9e195c-4f70-426c-8475-1597b8c000b0
updated: 1484310371
title: systematic
categories:
    - Dictionary
---
systematic
     adj 1: characterized by order and planning; "the investigation was
            very systematic"; "a systematic administrator" [ant: {unsystematic}]
     2: not haphazard; "a series of orderly actions at regular
        hours" [syn: {orderly}]
