---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indiscriminately
offline_file: ""
offline_thumbnail: ""
uuid: a6eec3fc-ce1c-4269-a1d1-92c63f7b191b
updated: 1484310246
title: indiscriminately
categories:
    - Dictionary
---
indiscriminately
     adv 1: in a random manner; "the houses were randomly scattered";
            "bullets were fired into the crowd at random" [syn: {randomly},
             {haphazardly}, {willy-nilly}, {arbitrarily}, {at
            random}, {every which way}]
     2: in an indiscriminate manner; "she reads promiscuously" [syn:
         {promiscuously}]
