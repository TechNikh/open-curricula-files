---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flag_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: eb33e31e-74df-4d66-bf5a-e70f77d9c8ea
updated: 1484309251
title: Flag (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Polytope_Flag.PNG
categories:
    - Incidence geometry
---
More formally, a flag ψ of an n-polytope is a set {F−1, F0, ..., Fn} such that Fi ≤ Fi+1 (−1 ≤ i ≤ n − 1) and there is precisely one Fi in ψ for each i, (−1 ≤ i ≤ n). Since, however, the minimal face F−1 and the maximal face Fn must be in every flag, they are often omitted from the list of faces, as a shorthand. These latter two are called improper faces.
