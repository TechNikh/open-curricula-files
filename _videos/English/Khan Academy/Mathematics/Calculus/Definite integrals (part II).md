---
version: 1
type: video
provider: YouTube
id: 6PaFm_Je5A0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Definite%20integrals%20%28part%20II%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 234ad1c2-7306-437a-99ff-d3fc159e4193
updated: 1486069694
title: Definite integrals (part II)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/6PaFm_Je5A0/default.jpg
    - https://i3.ytimg.com/vi/6PaFm_Je5A0/1.jpg
    - https://i3.ytimg.com/vi/6PaFm_Je5A0/2.jpg
    - https://i3.ytimg.com/vi/6PaFm_Je5A0/3.jpg
---
