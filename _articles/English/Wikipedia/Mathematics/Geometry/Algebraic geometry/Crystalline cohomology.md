---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crystalline_cohomology
offline_file: ""
offline_thumbnail: ""
uuid: 650c2460-27d2-4572-bfe7-30ff624c699f
updated: 1484309142
title: Crystalline cohomology
tags:
    - Applications
    - de Rham cohomology
    - Coefficients
    - Motivation
    - Crystalline cohomology
    - Crystals
categories:
    - Algebraic geometry
---
In mathematics, crystalline cohomology is a Weil cohomology theory for schemes X over a base field k. Its values Hn(X/W) are modules over the ring W of Witt vectors over k. It was introduced by Alexander Grothendieck (1966, 1968) and developed by Pierre Berthelot (1974).
