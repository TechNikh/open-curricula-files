---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/player
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484392081
title: player
categories:
    - Dictionary
---
player
     n 1: a person who participates in or is skilled at some game
          [syn: {participant}]
     2: someone who plays a musical instrument (as a profession)
        [syn: {musician}, {instrumentalist}]
     3: a theatrical performer [syn: {actor}, {histrion}, {thespian},
         {role player}]
     4: an important participant (as in a business deal); "he was a
        major player in setting up the corporation"
