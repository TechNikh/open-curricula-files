---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recycle
offline_file: ""
offline_thumbnail: ""
uuid: 6a80f4cb-670e-4f8d-ae57-8616da060e44
updated: 1484310249
title: recycle
categories:
    - Dictionary
---
recycle
     v 1: cause to repeat a cycle
     2: use again after processing; "We must recycle the cardboard
        boxes" [syn: {reprocess}, {reuse}]
