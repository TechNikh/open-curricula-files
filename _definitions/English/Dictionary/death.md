---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/death
offline_file: ""
offline_thumbnail: ""
uuid: 05f4ca17-b6c2-4ed2-bf9d-ea302c04d70c
updated: 1484310264
title: death
categories:
    - Dictionary
---
death
     n 1: the event of dying or departure from life; "her death came
          as a terrible shock"; "upon your decease the capital
          will pass to your grandchildren" [syn: {decease}] [ant:
          {birth}]
     2: the permanent end of all life functions in an organism or
        part of an organism; "the animal died a painful death"
     3: the time at which life ends; continuing until dead; "she
        stayed until his death"; "a struggle to the last" [syn: {last}]
     4: the personification of death; "Death walked the streets of
        the plague-bound city"
     5: the absence of life or state of being dead; "he seemed more
        content in death than he had ever ...
