---
version: 1
type: video
provider: YouTube
id: L0k-enzoeOM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dd262e36-bc9a-46f7-81a1-ea4e18a3cf9b
updated: 1486069678
title: 'Mitosis- Splitting Up is Complicated - Crash Course Biology #12'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/L0k-enzoeOM/default.jpg
    - https://i3.ytimg.com/vi/L0k-enzoeOM/1.jpg
    - https://i3.ytimg.com/vi/L0k-enzoeOM/2.jpg
    - https://i3.ytimg.com/vi/L0k-enzoeOM/3.jpg
---
