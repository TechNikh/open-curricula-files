---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Www
offline_file: ""
offline_thumbnail: ""
uuid: c1c65eb5-24f1-4623-9306-12e36cd6446a
updated: 1484308440
title: World Wide Web
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Web_Index.svg.png
tags:
    - History
    - Function
    - Linking
    - Dynamic updates of web pages
    - WWW prefix
    - Scheme specifiers
    - Web security
    - Privacy
    - Standards
    - Accessibility
    - Internationalisation
    - Statistics
    - Speed issues
    - Web caching
categories:
    - Uncategorized
---
The World Wide Web (abbreviated WWW or the Web) is an information space where documents and other web resources are identified by Uniform Resource Locators (URLs), interlinked by hypertext links, and can be accessed via the Internet.[1] English scientist Tim Berners-Lee invented the World Wide Web in 1989. He wrote the first web browser computer programme in 1990 while employed at CERN in Switzerland.[2][3]
