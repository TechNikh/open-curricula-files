---
version: 1
type: video
provider: YouTube
id: j11oAqtl1EE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 169a76f2-dc76-4666-9784-7691b2476a14
updated: 1486069619
title: 'Example 1- Using the quadratic formula'
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/j11oAqtl1EE/default.jpg
    - https://i3.ytimg.com/vi/j11oAqtl1EE/1.jpg
    - https://i3.ytimg.com/vi/j11oAqtl1EE/2.jpg
    - https://i3.ytimg.com/vi/j11oAqtl1EE/3.jpg
---
