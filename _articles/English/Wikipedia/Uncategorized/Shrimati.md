---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Srimati
offline_file: ""
offline_thumbnail: ""
uuid: 5a1b6575-4669-4f10-a2b6-f58893e94edd
updated: 1484308331
title: Shrimati
categories:
    - Uncategorized
---
Shrimati or Shreemati (Sanskrit: श्रिमती), abbreviated Smt., is the standard Indian honorific (akin to Mrs. in English) used when referring to an adult in various Indian languages, including Sanskrit, Nepali, Bengali, Hindi, and Malayalam, Telugu and sometimes in English as well (in an Indian context). The equivalent title for men is Shree or Sri.
