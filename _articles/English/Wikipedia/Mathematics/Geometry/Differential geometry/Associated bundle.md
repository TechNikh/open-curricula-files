---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Associated_bundle
offline_file: ""
offline_thumbnail: ""
uuid: 7e97cd0b-88c1-4546-a61c-8a4270208550
updated: 1484309206
title: Associated bundle
tags:
    - An example
    - Construction
    - Associated bundles in general
    - Principal bundle associated to a fibre bundle
    - Fiber bundle associated to a principal bundle
    - Reduction of the structure group
    - Examples of reduction
    - Books
categories:
    - Differential geometry
---
In mathematics, the theory of fiber bundles with a structure group 
  
    
      
        G
      
    
    {\displaystyle G}
  
 (a topological group) allows an operation of creating an associated bundle, in which the typical fiber of a bundle changes from 
  
    
      
        
          F
          
            1
          
        
      
    
    {\displaystyle F_{1}}
  
 to 
  
    
      
        
          F
          
            2
          
        
      
    
    {\displaystyle F_{2}}
  
, which are both topological spaces with a group action of 
  
    
      
        G
      
    
    {\displaystyle G}
  
. For a fibre bundle F with structure group G, the transition ...
