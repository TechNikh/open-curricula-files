---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nomadic
offline_file: ""
offline_thumbnail: ""
uuid: a5007414-830c-4f3e-9f70-b2faef2d21ce
updated: 1484310475
title: nomadic
categories:
    - Dictionary
---
nomadic
     adj : (of groups of people) tending to travel and change
           settlements frequently; "a restless mobile society";
           "the nomadic habits of the Bedouins"; "believed the
           profession of a peregrine typist would have a happy
           future"; "wandering tribes" [syn: {mobile}, {peregrine},
            {roving}, {wandering}]
