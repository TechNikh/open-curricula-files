---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sixteen
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484332561
title: sixteen
categories:
    - Dictionary
---
sixteen
     adj : being one more than fifteen [syn: {16}, {xvi}]
     n : the cardinal number that is the sum of fifteen and one [syn:
          {16}, {XVI}]
