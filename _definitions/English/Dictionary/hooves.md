---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hooves
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484481661
title: hooves
categories:
    - Dictionary
---
hoof
     n 1: the foot of an ungulate mammal
     2: the horny covering of the end of the foot in hoofed mammals
     v 1: walk; "let's hoof it to the disco" [syn: {foot}, {leg it}, {hoof
          it}]
     2: dance in a professional capacity
     [also: {hooves} (pl)]
