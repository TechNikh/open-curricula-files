---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/somewhat
offline_file: ""
offline_thumbnail: ""
uuid: 58a458ac-d7cc-4fe8-a43b-b028324fe8d6
updated: 1484310484
title: somewhat
categories:
    - Dictionary
---
somewhat
     adv 1: to a small degree or extent; "his arguments were somewhat
            self-contradictory"; "the children argued because one
            slice of cake was slightly larger than the other"
            [syn: {slightly}]
     2: to a moderately sufficient extent or degree; "the shoes are
        priced reasonably"; "he is fairly clever with computers";
        "they lived comfortably within reason" [syn: {reasonably},
         {moderately}, {within reason}, {fairly}, {middling}, {passably}]
        [ant: {unreasonably}, {unreasonably}]
