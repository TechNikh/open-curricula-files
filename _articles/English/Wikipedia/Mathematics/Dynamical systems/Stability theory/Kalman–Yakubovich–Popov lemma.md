---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kalman%E2%80%93Yakubovich%E2%80%93Popov_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 1ef9aa99-c2a0-47fe-ab54-fbf31eaf597a
updated: 1484308825
title: Kalman–Yakubovich–Popov lemma
categories:
    - Stability theory
---
The Kalman–Yakubovich–Popov lemma is a result in system analysis and control theory which states: Given a number 
  
    
      
        γ
        >
        0
      
    
    {\displaystyle \gamma >0}
  
, two n-vectors B, C and an n x n Hurwitz matrix A, if the pair 
  
    
      
        (
        A
        ,
        B
        )
      
    
    {\displaystyle (A,B)}
  
 is completely controllable, then a symmetric matrix P and a vector Q satisfying
