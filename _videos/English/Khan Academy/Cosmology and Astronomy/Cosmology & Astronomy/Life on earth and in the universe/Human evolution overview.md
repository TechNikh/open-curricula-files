---
version: 1
type: video
provider: YouTube
id: frE1rjhH77Y
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Life%20on%20earth%20and%20in%20the%20universe/Human%20evolution%20overview.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f8eb545f-c653-4a5e-9871-2cb881bafbf9
updated: 1486069686
title: Human evolution overview
categories:
    - Life on earth and in the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/frE1rjhH77Y/default.jpg
    - https://i3.ytimg.com/vi/frE1rjhH77Y/1.jpg
    - https://i3.ytimg.com/vi/frE1rjhH77Y/2.jpg
    - https://i3.ytimg.com/vi/frE1rjhH77Y/3.jpg
---
