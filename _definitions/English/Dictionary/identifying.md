---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/identifying
offline_file: ""
offline_thumbnail: ""
uuid: 2a1062d4-08c3-40de-a42f-f96c07fdd3ca
updated: 1484310279
title: identifying
categories:
    - Dictionary
---
identifying
     adj : serving to distinguish or identify a species or group; "the
           distinguishing mark of the species is its plumage";
           "distinctive tribal tattoos"; "we were asked to
           describe any identifying marks or distinguishing
           features" [syn: {distinguishing}, {distinctive}, {identifying(a)}]
