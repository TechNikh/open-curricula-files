---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rich
offline_file: ""
offline_thumbnail: ""
uuid: e86611ad-9248-4c7c-a48c-ef08f1deaee7
updated: 1484310325
title: rich
categories:
    - Dictionary
---
rich
     adj 1: possessing material wealth; "her father is extremely rich";
            "many fond hopes are pinned on rich uncles" [ant: {poor}]
     2: having an abundant supply of desirable qualities or
        substances (especially natural resources); "blessed with a
        land rich in minerals"; "rich in ideas"; "rich with
        cultural interest" [ant: {poor}]
     3: of great worth or quality; "a rich collection of antiques"
     4: marked by great fruitfulness; "fertile farmland"; "a fat
        land"; "a productive vineyard"; "rich soil" [syn: {fat}, {fertile},
         {productive}]
     5: strong; intense; "deep purple"; "a rich red" [syn: {deep}]
     6: very productive; ...
