---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indicate
offline_file: ""
offline_thumbnail: ""
uuid: db50fe9a-9559-4239-a37f-f1bd0a9fc2f7
updated: 1484310284
title: indicate
categories:
    - Dictionary
---
indicate
     v 1: be a signal for or a symptom of; "These symptoms indicate a
          serious illness"; "Her behavior points to a severe
          neurosis"; "The economic indicators signal that the euro
          is undervalued" [syn: {bespeak}, {betoken}, {point}, {signal}]
     2: indicate a place, direction, person, or thing; either
        spatially or figuratively; "I showed the customer the
        glove section"; "He pointed to the empty parking space";
        "he indicated his opponents" [syn: {point}, {show}]
     3: to state or express briefly; "indicated his wishes in a
        letter" [ant: {contraindicate}]
     4: give evidence of; "The evidence argues for your claim"; ...
