---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chetaev_instability_theorem
offline_file: ""
offline_thumbnail: ""
uuid: eba0cf57-6460-4ab0-b662-0e5d893866cc
updated: 1484308696
title: Chetaev instability theorem
categories:
    - Theorems in dynamical systems
---
The Chetaev instability theorem for dynamical systems states that if there exists, for the system 
  
    
      
        
          
            
              
                x
              
              ˙
            
          
        
        =
        X
        (
        
          
            x
          
        
        )
      
    
    {\displaystyle {\dot {\textbf {x}}}=X({\textbf {x}})}
  
 with an equilibrium point at the origin, a continuously differentiable function V(x) such that
