---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scale
offline_file: ""
offline_thumbnail: ""
uuid: ebcfe500-45ac-406e-8dd6-19cf1498130e
updated: 1484310297
title: scale
categories:
    - Dictionary
---
scale
     n 1: an ordered reference standard; "judging on a scale of 1 to
          10" [syn: {scale of measurement}, {graduated table}, {ordered
          series}]
     2: relative magnitude; "they entertained on a grand scale"
     3: the ratio between the size of something and a representation
        of it; "the scale of the map"; "the scale of the model"
     4: an indicator having a graduated sequence of marks
     5: a specialized leaf or bract that protects a bud or catkin
        [syn: {scale leaf}]
     6: a thin flake of dead epidermis shed from the surface of the
        skin [syn: {scurf}, {exfoliation}]
     7: (music) a series of notes differing in pitch according to a
      ...
