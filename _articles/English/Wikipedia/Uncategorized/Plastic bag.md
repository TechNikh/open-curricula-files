---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plastic_bag
offline_file: ""
offline_thumbnail: ""
uuid: 18896c85-ad7c-4418-a795-89c5834e034b
updated: 1484308366
title: Plastic bag
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Trash_bin_in_Paris.jpg
tags:
    - Bags
    - Packages
    - Plastic shopping bags
    - Medical uses
    - Waste disposal bags
    - Flexible intermediate bulk container
    - History
    - Use of plastic bags internationally
    - Danger to children
    - Environmental issues
    - Uses
categories:
    - Uncategorized
---
A plastic bag, polybag, or pouch is a type of container made of thin, flexible, plastic film, nonwoven fabric, or plastic textile. Plastic bags are used for containing and transporting goods such as foods, produce, powders, ice, magazines, chemicals, and waste. It is a common form of packaging.
