---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nine-point_conic
offline_file: ""
offline_thumbnail: ""
uuid: 37c550de-ca69-4e7b-a014-80e6c1bbdabd
updated: 1484309123
title: Nine-point conic
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Nine_point_conic.svg.png
categories:
    - Theorems in geometry
---
In geometry, the nine-point conic of a complete quadrangle is a conic that passes through the three diagonal points and the six midpoints of sides of the complete quadrangle.
