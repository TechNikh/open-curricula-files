---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rose
offline_file: ""
offline_thumbnail: ""
uuid: 4759b0fb-66d6-4e54-9caa-a421594507ec
updated: 1484310275
title: rose
categories:
    - Dictionary
---
rose
     adj : having a dusty purplish pink color; "the roseate glow of
           dawn" [syn: {roseate}, {rosaceous}]
     n 1: any of many plants of the genus Rosa
     2: pinkish table wine from red grapes whose skins were removed
        after fermentation began [syn: {blush wine}, {pink wine},
        {rose wine}]
     3: a dusty pink color
