---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beta
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484310121
title: beta
categories:
    - Dictionary
---
beta
     adj 1: second in order of importance; "the candidate, considered a
            beta male, was perceived to be unable to lead his
            party to victory"
     2: preliminary or testing stage of a software or hardware
        product; "a beta version"; "beta software"
     n 1: the 2nd letter of the Greek alphabet
     2: beets [syn: {genus Beta}]
