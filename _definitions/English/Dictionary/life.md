---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/life
offline_file: ""
offline_thumbnail: ""
uuid: d5f21ffe-b9bd-4c7f-b4ed-f0c4e544db81
updated: 1484310361
title: life
categories:
    - Dictionary
---
life
     n 1: a characteristic state or mode of living; "social life";
          "city life"; "real life"
     2: the course of existence of an individual; the actions and
        events that occur in living; "he hoped for a new life in
        Australia"; "he wanted to live his own life without
        interference from others"
     3: the experience of living; the course of human events and
        activities; "he could no longer cope with the complexities
        of life" [syn: {living}]
     4: the condition of living or the state of being alive; "while
        there's life there's hope"; "life depends on many chemical
        and physical processes" [syn: {animation}, {living}, ...
