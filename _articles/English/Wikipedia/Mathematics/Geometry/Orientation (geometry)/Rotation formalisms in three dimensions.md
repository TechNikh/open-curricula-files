---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions
offline_file: ""
offline_thumbnail: ""
uuid: 727c44f3-a297-4076-ae5a-7bd6ea9857ee
updated: 1484309302
title: Rotation formalisms in three dimensions
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Euler_AxisAngle_0.png
tags:
    - Rotations and motions
    - Formalism alternatives
    - Rotation matrix
    - Euler axis and angle (rotation vector)
    - Euler rotations
    - Quaternions
    - Rodrigues parameters and Gibbs representation
    - Cayley–Klein parameters
    - Higher-dimensional analogues
    - Conversion formulae between formalisms
    - Rotation matrix ↔ Euler angles
    - Rotation matrix → Euler angles (z-x-z extrinsic)
    - Euler angles ( z-y’-x″ intrinsic) → Rotation matrix
    - Rotation matrix ↔ Euler axis/angle
    - Rotation matrix ↔ quaternion
    - Euler angles ↔ Quaternion
    - Euler angles (z-x-z extrinsic) → Quaternion
    - Euler angles (z-y’-x″ intrinsic) → Quaternion
    - Quaternion → Euler angles (z-x-z extrinsic)
    - Quaternion → Euler angles (z-y’-x″ intrinsic)
    - Euler axis/angle ↔ quaternion
    - Conversion formulae for derivatives
    - Rotation matrix ↔ angular velocities
    - Quaternion ↔ angular velocities
    - Rotors in a geometric algebra
categories:
    - Orientation (geometry)
---
In geometry, various formalisms exist to express a rotation in three dimensions as a mathematical transformation. In physics, this concept is applied to classical mechanics where rotational (or angular) kinematics is the science of quantitative description of a purely rotational motion. The orientation of an object at a given instant is described with the same tools, as it is defined as an imaginary rotation from a reference placement in space, rather than an actually observed rotation from a previous placement in space.
