---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polarization_ripples
offline_file: ""
offline_thumbnail: ""
uuid: 294d0904-d4fd-4dae-8e1b-9d314de3b67f
updated: 1484308822
title: Polarization ripples
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Scheme_of_near-wavelength_periodic_surface_structures_induced_by_ultrasort_laser_irradiation.png
categories:
    - Oscillation
---
Polarization ripples are parallel oscillations which have been observed since the 1960s [1] on the bottom of pulsed laser irradiation of semiconductors. They have the property to be very dependent to the orientation of the laser electric field.
