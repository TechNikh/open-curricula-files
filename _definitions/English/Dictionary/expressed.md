---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expressed
offline_file: ""
offline_thumbnail: ""
uuid: e75958b9-700a-4f35-9e2c-f67795eecaa2
updated: 1484310299
title: expressed
categories:
    - Dictionary
---
expressed
     adj 1: communicated in words; "frequently uttered sentiments" [syn:
             {uttered}, {verbalized}, {verbalised}]
     2: precisely and clearly expressed or readily observable;
        leaving nothing to implication; "explicit instructions";
        "she made her wishes explicit"; "explicit sexual scenes"
        [syn: {explicit}] [ant: {implicit}]
