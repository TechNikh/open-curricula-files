---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cia
offline_file: ""
offline_thumbnail: ""
uuid: de5efb4b-4bad-4bed-a75a-8da75686679f
updated: 1484310180
title: cia
categories:
    - Dictionary
---
CIA
     n : an independent agency of the United States government
         responsible for collecting and coordinating intelligence
         and counterintelligence activities abroad in the national
         interest; headed by the Director of Central Intelligence
         under the supervision of the President and National
         Security Council [syn: {Central Intelligence Agency}]
