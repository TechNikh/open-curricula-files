---
version: 1
type: video
provider: YouTube
id: UQHT5RktlVk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Gases%20and%20kinetic%20molecular%20theory/Introduction%20to%20partial%20pressure.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2ac4b035-a2f7-4bd8-a557-7fa0b9fb7520
updated: 1486069587
title: Introduction to partial pressure
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Gases and kinetic molecular theory
thumbnail_urls:
    - https://i3.ytimg.com/vi/UQHT5RktlVk/default.jpg
    - https://i3.ytimg.com/vi/UQHT5RktlVk/1.jpg
    - https://i3.ytimg.com/vi/UQHT5RktlVk/2.jpg
    - https://i3.ytimg.com/vi/UQHT5RktlVk/3.jpg
---
Definition of partial pressure and Dalton's law of partial pressure. Example of calculating partial pressure of a gas in a mixture from the total pressure and partial pressure of the other gases in the mixture.

Chemistry on Khan Academy:
Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: 
Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: 
http://www.youtube.com/subscription_center?add_user=khanacademy
