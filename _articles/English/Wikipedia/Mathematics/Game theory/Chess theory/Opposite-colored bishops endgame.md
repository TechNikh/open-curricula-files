---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Opposite-colored_bishops_endgame
offline_file: ""
offline_thumbnail: ""
uuid: 5581aa79-9a70-4f2d-bb4d-0b0cc0a5df4a
updated: 1484309020
title: Opposite-colored bishops endgame
tags:
    - General principles
    - Drawing tendency
    - Bishop and pawn versus bishop
    - Bishop and two pawns versus a bishop
    - Doubled pawns
    - Isolated pawns
    - Wrong rook pawn
    - Recap
    - Connected pawns
    - More pawns
    - Examples from master games
    - Berger vs. Kotlerman
    - Piskov vs. Nunn
    - Nunn
    - Sokolov vs. McShane
    - Lautier vs. Rublevsky
    - Kotov vs. Botvinnik
    - Fischer vs. Donner
    - Fischer vs. Polugaevsky
    - Vidmar vs. Maróczy
    - Advantageous with positional considerations
    - Against weak pawns
    - Positional advantages
    - Additional pieces
    - Knight
    - Rook
    - Example
    - Queen
    - History
    - Quotes
    - Notes
categories:
    - Chess theory
---
The opposite-colored bishops endgame is a chess endgame in which each side has a single bishop, but the bishops reside on opposite-colored squares on the chessboard, thus cannot attack or block each other. Without other pieces (but with pawns) these endings are notorious for their tendency to result in a draw. These are the most difficult endings in which to convert a small material advantage to a win. With additional pieces, the stronger side has more chances to win, but not as many as if the bishops were on the same color.
