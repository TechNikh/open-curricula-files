---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humans
offline_file: ""
offline_thumbnail: ""
uuid: bb78ec25-e6f5-4fab-a6e3-8f2ac1a75fa7
updated: 1484310279
title: humans
categories:
    - Dictionary
---
humans
     n : all of the inhabitants of the earth; "all the world loves a
         lover"; "she always used `humankind' because `mankind'
         seemed to slight the women" [syn: {world}, {human race},
         {humanity}, {humankind}, {human beings}, {mankind}, {man}]
