---
version: 1
type: video
provider: YouTube
id: 3D-f_nAYqHQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Visualizing%20division%20by%20fractions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d0a0ffb8-8d69-4794-a0b4-7cdb5b59f680
updated: 1486069656
title: Visualizing division by fractions
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/3D-f_nAYqHQ/default.jpg
    - https://i3.ytimg.com/vi/3D-f_nAYqHQ/1.jpg
    - https://i3.ytimg.com/vi/3D-f_nAYqHQ/2.jpg
    - https://i3.ytimg.com/vi/3D-f_nAYqHQ/3.jpg
---
