---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Matching_preclusion
offline_file: ""
offline_thumbnail: ""
uuid: e99ce8fe-a656-4a8b-a13f-c88515e52c96
updated: 1484309422
title: Matching preclusion
categories:
    - Graph invariants
---
In graph theory, a branch of mathematics, the matching preclusion number of a graph G (denoted mp(G)) is the minimum number of edges whose deletion results in the destruction of a perfect matching or near-perfect matching (a matching that covers all but one vertex in a graph with an odd number of vertices).[1] Matching preclusion measures the robustness of a graph as a communications network topology for distributed algorithms that require each node of the distributed system to be matched with a neighboring partner node.[2]
