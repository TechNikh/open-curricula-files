---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Li_Shanlan_identity
offline_file: ""
offline_thumbnail: ""
uuid: ff177231-ca9b-48da-a52a-43ccbf021e10
updated: 1484308580
title: Li Shanlan identity
tags:
    - The identity
    - Proofs of the identity
    - On Duoji bilei
categories:
    - Combinatorics
---
In mathematics, in combinatorics, Li Shanlan identity (also called Li Shanlan's summation formula) is a certain combinatorial identity attributed to the nineteenth century Chinese mathematician Li Shanlan.[1] Since Li Shanlan is also known as Li Renshu, this identity is also referred to as Li Renshu identity.[2] This identity appears in the third chapter of Duoji bilei (Summing finite series), a mathematical text authored by Li Shanlan and published in 1867 as part of his collected works. A Czech mathematician Josef Kaucky published an elementary proof of the identity along with a history of the identity in 1964.[3] Kaucky attributed the identity to a certain Li Jen-Shu. From the account of ...
