---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/provident
offline_file: ""
offline_thumbnail: ""
uuid: 5182380d-df13-4fea-99cb-88c717dd51a9
updated: 1484310457
title: provident
categories:
    - Dictionary
---
provident
     adj 1: providing carefully for the future; "wild squirrels are
            provident"; "a provident father plans for his
            children's education" [ant: {improvident}]
     2: careful in regard to your own interests; "the prudent use
        and development of resources"; "wild squirrels are
        provident"
