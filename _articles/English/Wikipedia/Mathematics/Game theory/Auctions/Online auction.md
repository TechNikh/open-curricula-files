---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Online_auction
offline_file: ""
offline_thumbnail: ""
uuid: e5844fd5-74ef-423c-8e42-a59f2635eaf8
updated: 1484309011
title: Online auction
tags:
    - History
    - Types of online auctions
    - English auctions
    - Dutch auctions
    - First-price sealed-bid
    - Vickrey auction
    - Reverse auction
    - Bidding fee auction
    - Legalities
    - Shill bidding
    - Fraud
    - Sale of stolen goods
    - Bidding techniques
    - Auction sniping
categories:
    - Auctions
---
An online auction is an auction which is held over the internet. Online auctions come in many different formats, but most popularly they are ascending English auctions, descending Dutch auctions, first-price sealed-bid, Vickrey auctions, or sometimes even a combination of multiple auctions, taking elements of one and forging them with another. The scope and reach of these auctions have been propelled by the Internet to a level beyond what the initial purveyors had anticipated.[1] This is mainly because online auctions break down and remove the physical limitations of traditional auctions such as geography, presence, time, space, and a small target audience.[1] This influx in reachability ...
