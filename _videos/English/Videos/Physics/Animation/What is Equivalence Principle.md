---
version: 1
type: video
provider: YouTube
id: 2bMK_32Brvo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ce3ae319-f6d6-4739-bd90-49bc890e2d65
updated: 1486069654
title: What is Equivalence Principle
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/2bMK_32Brvo/default.jpg
    - https://i3.ytimg.com/vi/2bMK_32Brvo/1.jpg
    - https://i3.ytimg.com/vi/2bMK_32Brvo/2.jpg
    - https://i3.ytimg.com/vi/2bMK_32Brvo/3.jpg
---
