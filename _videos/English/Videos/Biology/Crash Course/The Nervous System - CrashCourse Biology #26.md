---
version: 1
type: video
provider: YouTube
id: x4PPZCLnVkA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/The%20Nervous%20System%20-%20CrashCourse%20Biology%20%2326.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d196b9cd-6804-45b9-b810-6c4b22b1d85b
updated: 1486069677
title: 'The Nervous System - CrashCourse Biology #26'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/x4PPZCLnVkA/default.jpg
    - https://i3.ytimg.com/vi/x4PPZCLnVkA/1.jpg
    - https://i3.ytimg.com/vi/x4PPZCLnVkA/2.jpg
    - https://i3.ytimg.com/vi/x4PPZCLnVkA/3.jpg
---
