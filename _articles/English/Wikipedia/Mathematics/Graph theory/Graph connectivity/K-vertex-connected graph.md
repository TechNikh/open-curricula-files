---
version: 1
type: article
id: https://en.wikipedia.org/wiki/K-vertex-connected_graph
offline_file: ""
offline_thumbnail: ""
uuid: 69b77bad-24af-4595-8eab-e4336103be20
updated: 1484309400
title: K-vertex-connected graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/4-connected.jpg
tags:
    - Definitions
    - Applications
    - Polyhedral combinatorics
    - Computational complexity
    - Notes
categories:
    - Graph connectivity
---
In graph theory, a connected graph G is said to be k-vertex-connected (or k-connected) if it has more than k vertices and remains connected whenever fewer than k vertices are removed.
