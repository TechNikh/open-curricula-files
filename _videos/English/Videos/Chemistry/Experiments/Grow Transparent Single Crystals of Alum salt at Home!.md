---
version: 1
type: video
provider: YouTube
id: eIAkWaQi0AE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c83f54db-f71a-4c1c-8863-5c60ebca523c
updated: 1486069661
title: Grow Transparent Single Crystals of Alum salt at Home!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/eIAkWaQi0AE/default.jpg
    - https://i3.ytimg.com/vi/eIAkWaQi0AE/1.jpg
    - https://i3.ytimg.com/vi/eIAkWaQi0AE/2.jpg
    - https://i3.ytimg.com/vi/eIAkWaQi0AE/3.jpg
---
