---
version: 1
type: video
provider: YouTube
id: xnLcxdM8OD8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Simplifying%20expressions%20with%20exponents%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d546a65b-0201-4ebd-a834-9bf89b29c902
updated: 1486069561
title: Simplifying expressions with exponents 2
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/xnLcxdM8OD8/default.jpg
    - https://i3.ytimg.com/vi/xnLcxdM8OD8/1.jpg
    - https://i3.ytimg.com/vi/xnLcxdM8OD8/2.jpg
    - https://i3.ytimg.com/vi/xnLcxdM8OD8/3.jpg
---
