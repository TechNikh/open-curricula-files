---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slum
offline_file: ""
offline_thumbnail: ""
uuid: f5092fd6-3287-4cb6-8cd4-f28cffb09ec5
updated: 1484310475
title: slum
categories:
    - Dictionary
---
slum
     n : a district of a city marked by poverty and inferior living
         conditions [syn: {slum area}]
     v : visit slums for entertainment or out of curiosity
     [also: {slumming}, {slummed}]
