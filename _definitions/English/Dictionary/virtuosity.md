---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/virtuosity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484641861
title: virtuosity
categories:
    - Dictionary
---
virtuosity
     n : technical skill or fluency or style exhibited by a virtuoso
