---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bipartite_realization_problem
offline_file: ""
offline_thumbnail: ""
uuid: 8e709e7b-976d-48fe-b8e6-bb4295206517
updated: 1484309391
title: Bipartite realization problem
tags:
    - Solutions
    - Other notations
    - Related problems
categories:
    - Computational problems in graph theory
---
The bipartite realization problem is a classical decision problem in graph theory, a branch of combinatorics. Given two finite sequences 
  
    
      
        (
        
          a
          
            1
          
        
        ,
        …
        ,
        
          a
          
            n
          
        
        )
      
    
    {\displaystyle (a_{1},\dots ,a_{n})}
  
 and 
  
    
      
        (
        
          b
          
            1
          
        
        ,
        …
        ,
        
          b
          
            n
          
        
        )
      
    
    {\displaystyle (b_{1},\dots ,b_{n})}
  
 of natural numbers, the problem asks whether ...
