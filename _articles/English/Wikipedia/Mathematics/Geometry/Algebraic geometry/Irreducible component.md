---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Irreducible_component
offline_file: ""
offline_thumbnail: ""
uuid: 15d1f902-809c-4e5c-a2bd-01c37d09b235
updated: 1484309156
title: Irreducible component
categories:
    - Algebraic geometry
---
In mathematics, and specifically in algebraic geometry, the concept of irreducible component is used to make formal the idea that a set such as defined by the equation
