---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Generalized_minimum-distance_decoding
offline_file: ""
offline_thumbnail: ""
uuid: 5334f12f-d988-4d18-ad23-26b8f74bf1bb
updated: 1484308549
title: Generalized minimum-distance decoding
tags:
    - Setup
    - Randomized algorithm
    - Modified randomized algorithm
    - Deterministic algorithm
categories:
    - Coding theory
---
In coding theory, generalized minimum-distance (GMD) decoding provides an efficient algorithm for decoding concatenated codes, which is based on using an errors-and-erasures decoder for the outer code.
