---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/present
offline_file: ""
offline_thumbnail: ""
uuid: 2cf46604-d5c5-4ca4-88e8-84b8cd3bd8dc
updated: 1484310351
title: present
categories:
    - Dictionary
---
present
     adj 1: temporal sense; intermediate between past and future; now
            existing or happening or in consideration; "the
            present leader"; "articles for present use"; "the
            present topic"; "the present system"; "present
            observations" [syn: {present(a)}] [ant: {future}, {past}]
     2: spatial sense; being or existing in a specified place; "the
        murderer is present in this room"; "present at the
        wedding"; "present at the creation" [ant: {absent}]
     n 1: the period of time that is happening now; any continuous
          stretch of time including the moment of speech; "that is
          enough for the present"; "he lives in ...
