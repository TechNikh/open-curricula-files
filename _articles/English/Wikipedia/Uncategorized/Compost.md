---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Composting
offline_file: ""
offline_thumbnail: ""
uuid: e6e147e4-7159-41a9-b189-89bbac4b897b
updated: 1484308471
title: Compost
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Compost_site_germany.JPG
tags:
    - Terminology
    - Ingredients
    - Carbon, nitrogen, oxygen, water
    - Animal manure and bedding
    - Microorganisms
    - Phases of composting
    - Human waste
    - Humanure
    - Uses
    - Composting technologies
categories:
    - Uncategorized
---
Compost (/ˈkɒmpɒst/ or /ˈkɒmpoʊst/) is organic matter that has been decomposed and recycled as a fertilizer and soil amendment. Compost is a key ingredient in organic farming.
