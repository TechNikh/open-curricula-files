---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/depleted
offline_file: ""
offline_thumbnail: ""
uuid: 9012408e-16a0-47cd-8e08-288f354b95c2
updated: 1484310254
title: depleted
categories:
    - Dictionary
---
depleted
     adj 1: having resources completely depleted; "our depleted
            resources"
     2: no longer sufficient; "supplies are low"; "our funds are
        depleted" [syn: {low}]
