---
version: 1
type: video
provider: YouTube
id: y3fwWcV7t0I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e2c43308-abd4-48bb-ab10-20244ff55fd8
updated: 1486069651
title: >
    Exploration of the 4D mandelbrot set on the Z-plane
    (Buddhabrot)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/y3fwWcV7t0I/default.jpg
    - https://i3.ytimg.com/vi/y3fwWcV7t0I/1.jpg
    - https://i3.ytimg.com/vi/y3fwWcV7t0I/2.jpg
    - https://i3.ytimg.com/vi/y3fwWcV7t0I/3.jpg
---
