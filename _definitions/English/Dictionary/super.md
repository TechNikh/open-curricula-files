---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/super
offline_file: ""
offline_thumbnail: ""
uuid: eda9c3f2-f2ad-4eaf-b8cb-a40a0ea33f00
updated: 1484310395
title: super
categories:
    - Dictionary
---
super
     adj 1: of the highest quality; "an ace reporter"; "a crack shot";
            "a first-rate golfer"; "a super party"; "played
            top-notch tennis"; "an athlete in tiptop condition";
            "she is absolutely tops" [syn: {ace}, {A-one}, {crack},
             {first-rate}, {tiptop}, {topnotch}, {tops(p)}]
     2: including more than a specified category; "a super
        experiment"
     3: extremely large; "another super skyscraper"
     n : a caretaker for an apartment house; represents the owner as
         janitor and rent collector [syn: {superintendent}]
     adv : to an extreme degree; "the house was super clean for
           Mother's visit" [syn: {extremely}]
