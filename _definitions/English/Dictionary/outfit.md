---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outfit
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484449081
title: outfit
categories:
    - Dictionary
---
outfit
     n 1: any cohesive unit such as a military company
     2: a set of clothing (with accessories); "his getup was
        exceedingly elegant" [syn: {getup}, {rig}, {turnout}]
     3: gear consisting of a set of articles or tools for a
        specified purpose [syn: {kit}]
     v : provide with (something) usually for a specific purpose;
         "The expedition was equipped with proper clothing, food,
         and other necessities" [syn: {equip}, {fit}, {fit out}]
     [also: {outfitting}, {outfitted}]
