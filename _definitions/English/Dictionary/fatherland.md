---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fatherland
offline_file: ""
offline_thumbnail: ""
uuid: f20c03e8-6bd2-4736-a7ce-8b99136b6357
updated: 1484310559
title: fatherland
categories:
    - Dictionary
---
fatherland
     n : the country where you were born [syn: {homeland}, {motherland},
          {mother country}, {country of origin}, {native land}]
