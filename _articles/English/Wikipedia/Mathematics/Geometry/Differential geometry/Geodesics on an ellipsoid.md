---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geodesics_on_an_ellipsoid
offline_file: ""
offline_thumbnail: ""
uuid: aa72c9af-e98a-4e7c-990f-5af61b8a68c6
updated: 1484309221
title: Geodesics on an ellipsoid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Long_geodesic_on_an_oblate_ellipsoid.svg.png
tags:
    - Geodesics on an ellipsoid of revolution
    - Equations for a geodesic
    - Behavior of geodesics
    - Evaluation of the integrals
    - Solution of the direct problem
    - Solution of the inverse problem
    - Differential behavior of geodesics
    - Geodesic map projections
    - Envelope of geodesics
    - Area of a geodesic polygon
    - Software implementations
    - Geodesics on a triaxial ellipsoid
    - Triaxial coordinate systems
    - "Jacobi's solution"
    - Survey of triaxial geodesics
    - Applications
    - Notes
categories:
    - Differential geometry
---
The study of geodesics on an ellipsoid arose in connection with geodesy specifically with the solution of triangulation networks. The figure of the Earth is well approximated by an oblate ellipsoid, a slightly flattened sphere. A geodesic is the shortest path between two points on a curved surface, i.e., the analogue of a straight line on a plane surface. The solution of a triangulation network on an ellipsoid is therefore a set of exercises in spheroidal trigonometry (Euler 1755).
