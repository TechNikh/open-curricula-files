---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/iron
offline_file: ""
offline_thumbnail: ""
uuid: 94c5754d-0aa7-4dda-a2d4-bcfafc5ed8e3
updated: 1484310238
title: iron
categories:
    - Dictionary
---
iron
     adj : extremely robust; "an iron constitution" [syn: {cast-iron}]
     n 1: a heavy ductile magnetic metallic element; is silver-white
          in pure form but readily rusts; used in construction and
          tools and armament; plays a role in the transport of
          oxygen by the blood [syn: {Fe}, {atomic number 26}]
     2: a golf club that has a relatively narrow metal head
     3: metal shackles; for hands or legs [syn: {irons}, {chain}, {chains}]
     4: implement used to brand live stock [syn: {branding iron}]
     5: home appliance consisting of a flat metal base that is
        heated and used to smooth cloth [syn: {smoothing iron}]
     v : press and smooth with a ...
