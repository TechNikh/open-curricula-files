---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fat_object
offline_file: ""
offline_thumbnail: ""
uuid: 11408773-374e-49a5-bc18-ace910cabb72
updated: 1484309176
title: Fat object
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Two-cubes-slimness.png
tags:
    - Global fatness
    - Enclosing-fatness vs. enclosed-fatness
    - Balls vs. cubes
    - Local fatness
    - Global vs. local definitions
    - Examples
    - Fatness of a triangle
    - Enclosed-ball slimness
    - Enclosing-ball slimness
    - Two-balls slimness
    - Fatness of circles, ellipses and their parts
    - Lemmas
    - Generalizations
categories:
    - Computational geometry
---
In geometry, a fat object is an object in two or more dimensions, whose lengths in the different dimensions are similar. For example, a square is fat because its length and width are identical. A 2-by-1 rectangle is thinner than a square, but it is fat relative to a 10-by-1 rectangle. Similarly, a circle is fatter than a 1-by-10 ellipse and an equilateral triangle is fatter than a very obtuse triangle.
