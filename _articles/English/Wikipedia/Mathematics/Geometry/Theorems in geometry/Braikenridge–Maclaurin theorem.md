---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Braikenridge%E2%80%93Maclaurin_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 0351ccfd-2a6a-4a48-9330-23ac66fbc412
updated: 1484309114
title: Braikenridge–Maclaurin theorem
categories:
    - Theorems in geometry
---
In geometry, the Braikenridge–Maclaurin theorem, named for 18th century British mathematicians William Braikenridge and Colin Maclaurin (Mills 1984), is the converse to Pascal's theorem. It states that if the three intersection points of the three pairs of lines through opposite sides of a hexagon lie on a line L, then the six vertices of the hexagon lie on a conic C; the conic may be degenerate, as in Pappus's theorem. (Coxeter & Greitzer 1967, p. 76). The Braikenridge–Maclaurin theorem may be applied in the Braikenridge–Maclaurin construction, which is a synthetic construction of the conic defined by five points, by varying the sixth point. Namely, Pascal's theorem states that given ...
