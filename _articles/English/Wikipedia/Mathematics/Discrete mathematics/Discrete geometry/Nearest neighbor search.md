---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nearest_neighbor_search
offline_file: ""
offline_thumbnail: ""
uuid: 1ecc848b-27a8-4da9-9974-41008a7d9a67
updated: 1484308608
title: Nearest neighbor search
tags:
    - Applications
    - Methods
    - Linear search
    - Space partitioning
    - Locality sensitive hashing
    - >
        Nearest neighbor search in spaces with small intrinsic
        dimension
    - Projected radial search
    - Vector approximation files
    - Compression/clustering based search
    - Greedy walk search in small-world graphs
    - Variants
    - k-nearest neighbor
    - Approximate nearest neighbor
    - Nearest neighbor distance ratio
    - Fixed-radius near neighbors
    - All nearest neighbors
    - Notes
categories:
    - Discrete geometry
---
Nearest neighbor search (NNS), also known as proximity search, similarity search or closest point search, is an optimization problem for finding closest (or most similar) points. Closeness is typically expressed in terms of a dissimilarity function: the less similar the objects, the larger the function values. Formally, the nearest-neighbor (NN) search problem is defined as follows: given a set S of points in a space M and a query point q ∈ M, find the closest point in S to q. Donald Knuth in vol. 3 of The Art of Computer Programming (1973) called it the post-office problem, referring to an application of assigning to a residence the nearest post office. A direct generalization of this ...
