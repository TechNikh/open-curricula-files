---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bromine
offline_file: ""
offline_thumbnail: ""
uuid: 6fd2dab4-b8fc-4306-9e26-650ce6f99f0c
updated: 1484310375
title: bromine
categories:
    - Dictionary
---
bromine
     n : a nonmetallic largely pentavalent heavy volatile corrosive
         dark brown liquid element belonging to the halogens;
         found in sea water [syn: {Br}, {atomic number 35}]
