---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abscissa
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484337721
title: abscissa
categories:
    - Dictionary
---
abscissa
     n : the value of a coordinate on the horizontal axis
     [also: {abscissae} (pl)]
