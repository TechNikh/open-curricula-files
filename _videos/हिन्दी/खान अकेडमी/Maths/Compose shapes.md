---
version: 1
type: video
provider: YouTube
id: 1KLujUig1Eg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7bb9daa5-239c-4bca-8287-3a341f3329c1
updated: 1486069609
title: Compose shapes
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/1KLujUig1Eg/default.jpg
    - https://i3.ytimg.com/vi/1KLujUig1Eg/1.jpg
    - https://i3.ytimg.com/vi/1KLujUig1Eg/2.jpg
    - https://i3.ytimg.com/vi/1KLujUig1Eg/3.jpg
---
