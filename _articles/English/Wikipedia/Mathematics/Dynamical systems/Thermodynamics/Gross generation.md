---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gross_generation
offline_file: ""
offline_thumbnail: ""
uuid: c61339a0-b6f3-40cb-b4c0-d74e35765ce7
updated: 1484308845
title: Gross generation
categories:
    - Thermodynamics
---
Gross generation or gross electric output is the total generation of electricity produced by an electric power plant. It is measured at the plant terminal right before the power leaves the station and is measured in kilowatt-hours (kW·h), megawatt-hours (MW·h), or gigawatt-hours (GW·h). Thus
