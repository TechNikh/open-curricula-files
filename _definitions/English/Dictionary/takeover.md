---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/takeover
offline_file: ""
offline_thumbnail: ""
uuid: dd74ab76-981b-4219-a4df-b65ed3a29e98
updated: 1484310551
title: takeover
categories:
    - Dictionary
---
take over
     v 1: seize and take control without authority and possibly with
          force; take as one's right or possession; "He assumed to
          himself the right to fill all positions in the town";
          "he usurped my rights"; "She seized control of the
          throne after her husband died" [syn: {assume}, {usurp},
          {seize}, {arrogate}]
     2: take on titles, offices, duties, responsibilities; "When
        will the new President assume office?" [syn: {assume}, {adopt},
         {take on}]
     3: free someone temporarily from his or her obligations [syn: {relieve}]
     4: take on as one's own the expenses or debts of another
        person; "I'll accept the ...
