---
version: 1
type: video
provider: YouTube
id: TsEhZRT16LU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Perpendicular%20lines%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 66681966-8cad-4037-8f1f-5a25a26efa28
updated: 1486069563
title: Perpendicular lines 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/TsEhZRT16LU/default.jpg
    - https://i3.ytimg.com/vi/TsEhZRT16LU/1.jpg
    - https://i3.ytimg.com/vi/TsEhZRT16LU/2.jpg
    - https://i3.ytimg.com/vi/TsEhZRT16LU/3.jpg
---
