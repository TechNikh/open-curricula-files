---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/twentieth
offline_file: ""
offline_thumbnail: ""
uuid: 15375592-6dd9-4b6a-8eb7-07a442fcd9ab
updated: 1484310403
title: twentieth
categories:
    - Dictionary
---
twentieth
     adj : coming next after the nineteenth in position [syn: {20th}]
     n : position 20 in a countable series of things
