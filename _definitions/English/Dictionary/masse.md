---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/masse
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484526841
title: masse
categories:
    - Dictionary
---
masse
     n : a shot in billiards made by hitting the cue ball with the
         cue held nearly vertically; the cue ball spins around
         another ball before hitting the object ball [syn: {masse
         shot}]
