---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/P%C3%A1l_Tur%C3%A1n'
offline_file: ""
offline_thumbnail: ""
uuid: 2a0db086-f3bd-4c3d-9157-c5c91aed9bb3
updated: 1484309476
title: Pál Turán
tags:
    - Life and education
    - Work
    - Number theory
    - Analysis
    - Graph theory
    - Power sum method
    - Publications
    - Honors
    - Notes
categories:
    - Graph theorists
---
Paul (Pál) Turán (Hungarian: [ˈpaːl ˈturaːn]; 18 August 1910 – 26 September 1976)[1]:271[2] was a Hungarian mathematician who worked primarily in number theory. He had a long collaboration with fellow Hungarian mathematician Paul Erdős, lasting 46 years and resulting in 28 joint papers.[3]
