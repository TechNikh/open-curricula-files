---
version: 1
type: video
provider: YouTube
id: dMPXu6GF18M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Drew%20Berry%20-%20Astonishing%20Molecular%20Machines.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 92515463-031b-41ee-b838-66f237322c0d
updated: 1486069672
title: 'Drew Berry - Astonishing Molecular Machines'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/dMPXu6GF18M/default.jpg
    - https://i3.ytimg.com/vi/dMPXu6GF18M/1.jpg
    - https://i3.ytimg.com/vi/dMPXu6GF18M/2.jpg
    - https://i3.ytimg.com/vi/dMPXu6GF18M/3.jpg
---
