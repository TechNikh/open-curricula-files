---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/raise
offline_file: ""
offline_thumbnail: ""
uuid: 6c4b8757-41c5-423b-a74f-bbc2662b23e9
updated: 1484310234
title: raise
categories:
    - Dictionary
---
raise
     n 1: the amount a salary is increased; "he got a 3% raise"; "he
          got a wage hike" [syn: {rise}, {wage hike}, {hike}, {wage
          increase}, {salary increase}]
     2: an upward slope or grade (as in a road); "the car couldn't
        make it up the rise" [syn: {ascent}, {acclivity}, {rise},
        {climb}, {upgrade}] [ant: {descent}]
     3: increasing the size of a bet (as in poker); "I'll see your
        raise and double it"
     4: the act of raising something; "he responded with a lift of
        his eyebrow"; "fireman learn several different raises for
        getting ladders up" [syn: {lift}, {heave}]
     v 1: raise the level or amount of something; "raise ...
