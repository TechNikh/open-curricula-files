---
version: 1
type: video
provider: YouTube
id: 9sGlEEtXQCg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f3adf437-5b87-405a-8f6c-cce523f39392
updated: 1486069641
title: Fractal Setting Tripping on Mozart
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/9sGlEEtXQCg/default.jpg
    - https://i3.ytimg.com/vi/9sGlEEtXQCg/1.jpg
    - https://i3.ytimg.com/vi/9sGlEEtXQCg/2.jpg
    - https://i3.ytimg.com/vi/9sGlEEtXQCg/3.jpg
---
