---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/igneous
offline_file: ""
offline_thumbnail: ""
uuid: f04b29d9-6797-4c2c-b8b2-257e1726a1a1
updated: 1484310437
title: igneous
categories:
    - Dictionary
---
igneous
     adj 1: produced under conditions involving intense heat; "igneous
            rock is rock formed by solidification from a molten
            state; especially from molten magma"; "igneous fusion
            is fusion by heat alone"; "pyrogenic strata" [syn: {pyrogenic},
             {pyrogenous}]
     2: like or suggestive of fire; "the burning sand"; "a fiery
        desert wind"; "an igneous desert atmosphere" [syn: {fiery}]
