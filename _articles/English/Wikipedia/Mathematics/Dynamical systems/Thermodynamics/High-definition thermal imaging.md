---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/High-definition_thermal_imaging
offline_file: ""
offline_thumbnail: ""
uuid: 345a2123-a4d8-4070-a81b-d92d06b33cd3
updated: 1484308846
title: High-definition thermal imaging
categories:
    - Thermodynamics
---
High resolution thermal imaging refers to the fine detail and clarity of a thermal image. This means it contains a large number of pixels per unit of area. More pixels mean greater temperature measurement accuracy, particularly for small objects.
