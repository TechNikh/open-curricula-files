---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wiener_index
offline_file: ""
offline_thumbnail: ""
uuid: 06297211-c439-486e-8366-a5f780d1c126
updated: 1484309426
title: Wiener index
tags:
    - History
    - Example
    - Relation to chemical properties
    - Calculation in arbitrary graphs
    - Calculation in special types of graph
    - Inverse problem
categories:
    - Graph invariants
---
In chemical graph theory, the Wiener index (also Wiener number) is a topological index of a molecule, defined as the sum of the lengths of the shortest paths between all pairs of vertices in the chemical graph representing the non-hydrogen atoms in the molecule.[1]
