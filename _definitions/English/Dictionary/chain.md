---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chain
offline_file: ""
offline_thumbnail: ""
uuid: bfbb1dd7-d429-4ec1-9e2a-b13f133fed36
updated: 1484310275
title: chain
categories:
    - Dictionary
---
chain
     n 1: a series of things depending on each other as if linked
          together; "the chain of command"; "a complicated
          concatenation of circumstances" [syn: {concatenation}]
     2: (chemistry) a series of linked atoms (generally in an
        organic molecule) [syn: {chemical chain}]
     3: a series of (usually metal) rings or links fitted into one
        another to make a flexible ligament
     4: a number of similar establishments (stores or restaurants or
        banks or hotels or theaters) under one ownership
     5: anything that acts as a restraint
     6: a unit of length
     7: British biochemist (born in Germany) who isolated and
        purified ...
