---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bisection
offline_file: ""
offline_thumbnail: ""
uuid: 17a9d5b6-7028-48a1-a451-9ee2523878a0
updated: 1484308924
title: Bisection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bisectors.svg.png
tags:
    - Line segment bisector
    - Angle bisector
    - Triangle
    - Concurrencies and collinearities
    - Angle bisector theorem
    - Lengths
    - Integer triangles
    - Quadrilateral
    - Rhombus
    - Ex-tangential quadrilateral
    - Parabola
    - Bisectors of the sides of a polygon
    - Triangle
    - Medians
    - Perpendicular bisectors
    - Quadrilateral
    - Area bisectors and perimeter bisectors
    - Triangle
    - Parallelogram
    - Circle and ellipse
    - Bisectors of diagonals
    - Parallelogram
    - Quadrilateral
    - Volume bisectors
categories:
    - Elementary geometry
---
In geometry, bisection is the division of something into two equal or congruent parts, usually by a line, which is then called a bisector. The most often considered types of bisectors are the segment bisector (a line that passes through the midpoint of a given segment) and the angle bisector (a line that passes through the apex of an angle, that divides it into two equal angles).
