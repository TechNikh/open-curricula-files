---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fe
offline_file: ""
offline_thumbnail: ""
uuid: 4cebb4f6-7b9d-4d89-8495-b15fd25bb4f5
updated: 1484310305
title: fe
categories:
    - Dictionary
---
Fe
     n : a heavy ductile magnetic metallic element; is silver-white
         in pure form but readily rusts; used in construction and
         tools and armament; plays a role in the transport of
         oxygen by the blood [syn: {iron}, {atomic number 26}]
