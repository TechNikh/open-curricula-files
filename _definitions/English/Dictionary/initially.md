---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/initially
offline_file: ""
offline_thumbnail: ""
uuid: 9b30995d-d813-4ae9-a836-a4f38556acef
updated: 1484310295
title: initially
categories:
    - Dictionary
---
initially
     adv : at the beginning; "at first he didn't notice anything
           strange" [syn: {ab initio}, {at first}, {at the start}]
