---
version: 1
type: video
provider: YouTube
id: 6q7N8-Nh4pA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cd0b0545-0c00-4269-a08c-0348d9b466cd
updated: 1486069632
title: How to Make an Underwater Volcano | Science Projects
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/6q7N8-Nh4pA/default.jpg
    - https://i3.ytimg.com/vi/6q7N8-Nh4pA/1.jpg
    - https://i3.ytimg.com/vi/6q7N8-Nh4pA/2.jpg
    - https://i3.ytimg.com/vi/6q7N8-Nh4pA/3.jpg
---
