---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/malicious
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415061
title: malicious
categories:
    - Dictionary
---
malicious
     adj 1: having the nature of or resulting from malice; "malicious
            gossip"; "took malicious pleasure in...watching me
            wince"- Rudyard Kipling [ant: {unmalicious}]
     2: wishing or appearing to wish evil to others; arising from
        intense ill will or hatred; "a gossipy malevolent old
        woman"; "failure made him malevolent toward those who were
        successful" [syn: {malevolent}] [ant: {benevolent}]
