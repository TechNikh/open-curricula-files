---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stations
offline_file: ""
offline_thumbnail: ""
uuid: 612ebf65-cbd5-409b-a203-03279425e837
updated: 1484310435
title: stations
categories:
    - Dictionary
---
Stations
     n : (Roman Catholic Church) a devotion consisting of 14 prayers
         before the 14 stations of the cross (referring to Jesus'
         suffering and crucifixion) [syn: {Stations of the Cross}]
