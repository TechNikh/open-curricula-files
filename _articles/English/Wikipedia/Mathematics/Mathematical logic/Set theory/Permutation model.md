---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Permutation_model
offline_file: ""
offline_thumbnail: ""
uuid: 8eee47fa-eae5-402b-a703-1822721d7548
updated: 1484309510
title: Permutation model
categories:
    - Set theory
---
In mathematical set theory, a permutation model is a model of set theory with atoms (ZFA) constructed using a group of permutations of the atoms. A symmetric model is similar except that it is a model of ZF (without atoms) and is constructed using a group of permutations of a forcing poset. One application is to show the independence of the axiom of choice from the other axioms of ZFA or ZF. Permutation models were introduced by Fraenkel (1922) and developed further by Mostowski (1938). Symmetric models were introduced by Paul Cohen.
