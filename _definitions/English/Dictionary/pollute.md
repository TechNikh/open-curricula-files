---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pollute
offline_file: ""
offline_thumbnail: ""
uuid: 76a09e3c-4d6c-4295-90d8-1f2f0dd29683
updated: 1484310238
title: pollute
categories:
    - Dictionary
---
pollute
     v : make impure; "The industrial wastes polluted the lake" [syn:
          {foul}, {contaminate}]
