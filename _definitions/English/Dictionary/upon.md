---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upon
offline_file: ""
offline_thumbnail: ""
uuid: 409c9d52-f427-4d0e-9063-67dea94b8282
updated: 1484310321
title: upon
categories:
    - Dictionary
---
up on
     adj : being up to particular standard or level especially in being
           up to date in knowledge; "kept abreast of the latest
           developments"; "constant revision keeps the book au
           courant"; "always au fait on the latest events"; "up on
           the news" [syn: {abreast of(p)}, {au courant}, {au fait},
            {up on(p)}]
