---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evaporate
offline_file: ""
offline_thumbnail: ""
uuid: d5763bb5-a35b-4793-8ee9-5c1842a51e66
updated: 1484310224
title: evaporate
categories:
    - Dictionary
---
evaporate
     v 1: lose or cause to lose liquid by vaporization leaving a more
          concentrated residue; "evaporate milk" [syn: {vaporize},
           {vaporise}]
     2: cause to change into a vapor; "The chemist evaporated the
        water" [syn: {vaporise}]
     3: change into a vapor; "The water evaporated in front of our
        eyes" [syn: {vaporise}]
