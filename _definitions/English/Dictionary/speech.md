---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/speech
offline_file: ""
offline_thumbnail: ""
uuid: a6ea9bb4-a4fb-4384-8e7b-2ecb9aa615fd
updated: 1484310561
title: speech
categories:
    - Dictionary
---
speech
     n 1: the act of delivering a formal spoken communication to an
          audience; "he listened to an address on minor Roman
          poets" [syn: {address}]
     2: (language) communication by word of mouth; "his speech was
        garbled"; "he uttered harsh language"; "he recorded the
        spoken language of the streets" [syn: {speech
        communication}, {spoken communication}, {spoken language},
         {language}, {voice communication}, {oral communication}]
     3: something spoken; "he could hear them uttering merry
        speeches"
     4: the exchange of spoken words; "they were perfectly
        comfortable together without speech"
     5: your characteristic ...
