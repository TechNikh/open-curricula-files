---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imperialism
offline_file: ""
offline_thumbnail: ""
uuid: ccff75da-a4fc-46ab-86c9-58dafe328478
updated: 1484310553
title: imperialism
categories:
    - Dictionary
---
imperialism
     n 1: a policy of extending your rule over foreign countries
     2: a political orientation that advocates imperial interests
     3: any instance of aggressive extension of authority
