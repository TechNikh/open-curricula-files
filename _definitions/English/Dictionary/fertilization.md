---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fertilization
offline_file: ""
offline_thumbnail: ""
uuid: 20ef0866-799b-4452-9479-ebb8968ddf59
updated: 1484310303
title: fertilization
categories:
    - Dictionary
---
fertilization
     n 1: creation by the physical union of male and female gametes;
          of sperm and ova in an animal or pollen and ovule in a
          plant [syn: {fertilisation}, {fecundation}, {impregnation}]
     2: making fertile as by applying fertilizer or manure [syn: {fertilisation},
         {fecundation}, {dressing}]
