---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/produce
offline_file: ""
offline_thumbnail: ""
uuid: c6fa6812-d347-4e89-a801-3a52d416354a
updated: 1484310330
title: produce
categories:
    - Dictionary
---
produce
     n : fresh fruits and vegetable grown for the market [syn: {green
         goods}, {green groceries}, {garden truck}]
     v 1: bring forth or yield; "The tree would not produce fruit"
          [syn: {bring forth}]
     2: create or manufacture a man-made product; "We produce more
        cars than we can sell"; "The company has been making toys
        for two centuries" [syn: {make}, {create}]
     3: cause to occur or exist; "This procedure produces a curious
        effect"; "The new law gave rise to many complaints";
        "These chemicals produce a noxious vapor" [syn: {bring
        about}, {give rise}]
     4: bring out for display; "The proud father produced many
    ...
