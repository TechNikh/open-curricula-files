---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strategist
offline_file: ""
offline_thumbnail: ""
uuid: bb3ac5c8-c2e5-4166-a1e5-c75e77813375
updated: 1484309065
title: Strategist
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Enchoen27n3200.jpg
tags:
    - Types of strategists by field
    - Strategist as a personality type
    - Career paths
    - Notable Strategists
    - Sun-Tzu
    - Carl Von Clausewitz
    - Winston Churchill
    - Napoleon Bonaparte
categories:
    - Strategy
---
A strategist is a person with responsibility for the formulation and implementation of a strategy. Strategy generally involves setting goals, determining actions to achieve the goals, and mobilizing resources to execute the actions. A strategy describes how the ends (goals) will be achieved by the means (resources). The senior leadership of an organization is generally tasked with determining strategy. Strategy can be intended or can emerge as a pattern of activity as the organization adapts to its environment or competes. It involves activities such as strategic planning and strategic thinking.[1]
