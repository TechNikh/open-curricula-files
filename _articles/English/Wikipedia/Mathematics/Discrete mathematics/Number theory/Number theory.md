---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Number_theory
offline_file: ""
offline_thumbnail: ""
uuid: 032c7a11-fc5b-4205-874f-7f25c21a6436
updated: 1484308544
title: Number theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lehmer_sieve.jpg
tags:
    - History
    - Origins
    - Dawn of arithmetic
    - Classical Greece and the early Hellenistic period
    - Diophantus
    - Āryabhaṭa, Brahmagupta, Bhāskara
    - Arithmetic in the Islamic golden age
    - Western Europe in the Middle Ages
    - Early modern number theory
    - Fermat
    - Euler
    - Lagrange, Legendre and Gauss
    - Maturity and division into subfields
    - Main subdivisions
    - Elementary tools
    - Analytic number theory
    - Algebraic number theory
    - Diophantine geometry
    - Recent approaches and subfields
    - Probabilistic number theory
    - Arithmetic combinatorics
    - Computations in number theory
    - Applications
    - Literature
    - Prizes
    - Notes
    - Sources
categories:
    - Number theory
---
Number theory or, in older usage,[note 1] arithmetic is a branch of pure mathematics devoted primarily to the study of the integers. It is sometimes called "The Queen of Mathematics" because of its foundational place in the discipline.[1] Number theorists study prime numbers as well as the properties of objects made out of integers (e.g., rational numbers) or defined as generalizations of the integers (e.g., algebraic integers).
