---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defeated
offline_file: ""
offline_thumbnail: ""
uuid: 2d251906-9e74-4caf-b145-bb1c23eecffb
updated: 1484310559
title: defeated
categories:
    - Dictionary
---
defeated
     adj 1: beaten or overcome; not victorious; "the defeated enemy"
            [ant: {undefeated}]
     2: disappointingly unsuccessful; "disappointed expectations and
        thwarted ambitions"; "their foiled attempt to capture
        Calais"; "many frustrated poets end as pipe-smoking
        teachers"; "his best efforts were thwarted" [syn: {disappointed},
         {discomfited}, {foiled}, {frustrated}, {thwarted}]
     n : people who are defeated; "the Romans had no pity for the
         defeated" [syn: {discomfited}]
