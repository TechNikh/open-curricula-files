---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/variable
offline_file: ""
offline_thumbnail: ""
uuid: 5b7eff70-ef4e-425c-99b8-edccbb3d3048
updated: 1484310531
title: variable
categories:
    - Dictionary
---
variable
     adj 1: liable to or capable of change; "rainfall in the tropics is
            notoriously variable"; "variable winds"; "variable
            expenses" [ant: {invariable}]
     2: marked by diversity or difference; "the varying angles of
        roof slope"; "nature is infinitely variable" [syn: {varying}]
     3: (used of a device) designed so that a property (as e.g.
        light) can be varied; "a variable capacitor"; "variable
        filters in front of the mercury xenon lights"
     n 1: something that is likely to vary; something that is subject
          to variation; "the weather is one variable to be
          considered"
     2: a quantity that can assume any of a ...
