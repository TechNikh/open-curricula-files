---
version: 1
type: video
provider: YouTube
id: c7ByaI3T7Dc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Partial%20derivatives%20of%20vector-valued%20functions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 965cf1e1-f1ac-4363-8373-be1dce0a3240
updated: 1486069692
title: Partial derivatives of vector-valued functions
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/c7ByaI3T7Dc/default.jpg
    - https://i3.ytimg.com/vi/c7ByaI3T7Dc/1.jpg
    - https://i3.ytimg.com/vi/c7ByaI3T7Dc/2.jpg
    - https://i3.ytimg.com/vi/c7ByaI3T7Dc/3.jpg
---
