---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/groundnut
offline_file: ""
offline_thumbnail: ""
uuid: 4e1ef10f-2f9a-4eeb-8ae4-29c57348fb9f
updated: 1484310543
title: groundnut
categories:
    - Dictionary
---
groundnut
     n 1: a North American vine with fragrant blossoms and edible
          tubers; important food crop of Native Americans [syn: {groundnut
          vine}, {Indian potato}, {potato bean}, {wild bean}, {Apios
          americana}, {Apios tuberosa}]
     2: nutlike tuber; important food of Native Americans [syn: {potato
        bean}, {wild bean}]
     3: pod of the peanut vine containing usually 2 nuts or seeds;
        `groundnut' and `monkey nut' are British terms [syn: {peanut},
         {earthnut}, {goober}, {goober pea}, {monkey nut}]
