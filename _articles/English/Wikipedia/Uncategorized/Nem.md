---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nem
offline_file: ""
offline_thumbnail: ""
uuid: 4e53277a-2d8d-403e-90f7-b8ea82029b97
updated: 1484308391
title: Nem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Summer_roll.jpg
tags:
    - Types
    - Banh Trang Rolls
    - Barbecued
    - Cured/Fermented
categories:
    - Uncategorized
---
Nem (Vietnamese: món nem) is a Vietnamese sausage. Depending on the locality, nem may refer to a rolled sausage in rice paper called nem cuon or nem ran (fresh and fried rolls, respectively), barbecued sausage called nem nướng or cured sausage called nem chua.
