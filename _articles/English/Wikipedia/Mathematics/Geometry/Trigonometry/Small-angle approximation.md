---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Small-angle_approximation
offline_file: ""
offline_thumbnail: ""
uuid: d26062b8-912c-497f-890e-b201b5d9c5ec
updated: 1484309348
title: Small-angle approximation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Kleinwinkelnaeherungen.png
tags:
    - Justifications
    - Graphic
    - Geometric
    - Algebraic
    - Error of the approximations
    - Specific uses
    - Astronomy
    - Motion of a pendulum
    - Structural mechanics
    - Piloting
categories:
    - Trigonometry
---
The small-angle approximation is a useful simplification of the basic trigonometric functions which is approximately true in the limit where the angle approaches zero. They are truncations of the Taylor series for the basic trigonometric functions to a second-order approximation. This truncation gives:
