---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/twinkling
offline_file: ""
offline_thumbnail: ""
uuid: cb272b79-9130-416c-b517-85221f412e16
updated: 1484310208
title: twinkling
categories:
    - Dictionary
---
twinkling
     adj : shining intermittently with a sparkling light; "twinkling
           stars" [syn: {twinkling(a)}]
     n : a very short time (as the time it takes the eye blink or the
         heart to beat); "if I had the chance I'd do it in a
         flash" [syn: {blink of an eye}, {flash}, {heartbeat}, {instant},
          {jiffy}, {split second}, {trice}, {wink}, {New York
         minute}]
