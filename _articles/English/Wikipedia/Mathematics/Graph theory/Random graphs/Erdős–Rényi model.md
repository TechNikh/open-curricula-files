---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93R%C3%A9nyi_model'
offline_file: ""
offline_thumbnail: ""
uuid: 539733c4-3559-4e38-b0b8-042ed78d6060
updated: 1484309450
title: Erdős–Rényi model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Erdos_generated_network-p0.01.jpg
tags:
    - Definition
    - Comparison between the two models
    - Properties of G(n, p)
    - Relation to percolation
    - Caveats
    - History
categories:
    - Random graphs
---
In graph theory, the Erdős–Rényi model is either of two closely related models for generating random graphs. They are named after Paul Erdős and Alfréd Rényi, who first introduced one of the models in 1959;[1][2] the other model was introduced independently and contemporaneously by Edgar Gilbert.[3] In the model introduced by Erdős and Rényi, all graphs on a fixed vertex set with a fixed number of edges are equally likely; in the model introduced by Gilbert, each edge has a fixed probability of being present or absent, independently of the other edges. These models can be used in the probabilistic method to prove the existence of graphs satisfying various properties, or to provide ...
