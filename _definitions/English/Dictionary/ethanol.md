---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethanol
offline_file: ""
offline_thumbnail: ""
uuid: 8168a519-7c41-49ff-9bff-02a0ac2ca631
updated: 1484310423
title: ethanol
categories:
    - Dictionary
---
ethanol
     n : the intoxicating agent in fermented and distilled liquors;
         used pure or denatured as a solvent or in medicines and
         colognes and cleaning solutions and rocket fuel; proposed
         as a renewable clean-burning additive to gasoline [syn: {ethyl
         alcohol}, {fermentation alcohol}, {grain alcohol}]
