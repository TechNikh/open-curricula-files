---
version: 1
type: article
id: https://en.wikipedia.org/wiki/K-edge-connected_graph
offline_file: ""
offline_thumbnail: ""
uuid: 964ea4df-4094-4c11-aa9c-583c60a80960
updated: 1484309400
title: K-edge-connected graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/2-edge-connected.jpg
tags:
    - Formal definition
    - Related concepts
    - Computational aspects
categories:
    - Graph connectivity
---
The edge-connectivity of a graph is the largest k for which the graph is k-edge-connected.
