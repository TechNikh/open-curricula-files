---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lorry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484361721
title: lorry
categories:
    - Dictionary
---
lorry
     n 1: a large low horse-drawn wagon without sides
     2: a large truck designed to carry heavy loads; usually without
        sides [syn: {camion}]
