---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smelt
offline_file: ""
offline_thumbnail: ""
uuid: d1c1d3e8-7dfc-4f8d-b6aa-32ccc4084621
updated: 1484310377
title: smelt
categories:
    - Dictionary
---
smelt
     n 1: small cold-water silvery fish; migrate between salt and
          fresh water
     2: small trout-like silvery marine or freshwater food fishes of
        cold northern waters
     v : extract (metals) by heating
