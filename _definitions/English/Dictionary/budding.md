---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/budding
offline_file: ""
offline_thumbnail: ""
uuid: b7e43cdc-b894-4f03-ac16-08613dd86a8e
updated: 1484310160
title: budding
categories:
    - Dictionary
---
bud
     n 1: a partially opened flower
     2: a swelling on a plant stem consisting of overlapping
        immature leaves or petals
     v 1: develop buds; "The hibiscus is budding!"
     2: start to grow or develop; "a budding friendship"
     [also: {budding}, {budded}]
