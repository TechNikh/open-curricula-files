---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/promulgation
offline_file: ""
offline_thumbnail: ""
uuid: 27d114e3-65e7-4864-beb0-d86e8995dff2
updated: 1484310591
title: promulgation
categories:
    - Dictionary
---
promulgation
     n 1: a public statement about something that is happening or
          going to happen; "the announcement appeared in the local
          newspaper"; "the promulgation was written in English"
          [syn: {announcement}]
     2: the official announcement of a new law or ordinance whereby
        the law or ordinance is put into effect
     3: the formal act of proclaiming; giving public notice; "his
        promulgation of the policy proved to be premature" [syn: {proclamation}]
