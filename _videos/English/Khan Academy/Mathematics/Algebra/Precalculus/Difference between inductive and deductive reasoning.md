---
version: 1
type: video
provider: YouTube
id: GEId0GonOZM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Difference%20between%20inductive%20and%20deductive%20reasoning.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1b20c836-6325-4cba-8600-765a1ffb2fdd
updated: 1486069557
title: Difference between inductive and deductive reasoning
categories:
    - Precalculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/GEId0GonOZM/default.jpg
    - https://i3.ytimg.com/vi/GEId0GonOZM/1.jpg
    - https://i3.ytimg.com/vi/GEId0GonOZM/2.jpg
    - https://i3.ytimg.com/vi/GEId0GonOZM/3.jpg
---
