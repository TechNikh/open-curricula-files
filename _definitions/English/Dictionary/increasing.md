---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/increasing
offline_file: ""
offline_thumbnail: ""
uuid: 527f9594-8f66-4f42-9077-969d5710f0b4
updated: 1484310293
title: increasing
categories:
    - Dictionary
---
increasing
     adj 1: becoming greater or larger; "increasing prices" [ant: {decreasing}]
     2: music [ant: {decreasing}]
