---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exclamation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484465281
title: exclamation
categories:
    - Dictionary
---
exclamation
     n 1: an abrupt excited utterance; "she gave an exclamation of
          delight"; "there was much exclaiming over it" [syn: {exclaiming}]
     2: a loud complaint or protest or reproach
     3: an exclamatory rhetorical device; "O tempore! O mores" [syn:
         {ecphonesis}]
