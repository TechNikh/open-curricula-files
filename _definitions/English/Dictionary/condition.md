---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/condition
offline_file: ""
offline_thumbnail: ""
uuid: aeed1cd1-0d6b-4ba6-8eaf-786ada223ad4
updated: 1484310359
title: condition
categories:
    - Dictionary
---
condition
     n 1: a state at a particular time; "a condition (or state) of
          disrepair"; "the current status of the arms
          negotiations" [syn: {status}]
     2: a mode of being or form of existence of a person or thing;
        "the human condition"
     3: an assumption on which rests the validity or effect of
        something else [syn: {precondition}, {stipulation}]
     4: (usually plural) a statement of what is required as part of
        an agreement; "the contract set out the conditions of the
        lease"; "the terms of the treaty were generous" [syn: {term}]
     5: the state of (good) health (especially in the phrases `in
        condition' or `in shape' or ...
