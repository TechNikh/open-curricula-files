---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unintended_consequences
offline_file: ""
offline_thumbnail: ""
uuid: 4c60e71b-e6aa-404b-a358-9d8fb87e41d7
updated: 1484308773
title: Unintended consequences
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_6.png
tags:
    - History
    - Causes
    - Examples
    - Unexpected benefits
    - Unexpected drawbacks
    - Perverse results
    - Environmental intervention
    - Notes
categories:
    - Entropy
---
In the social sciences, unintended consequences (sometimes unanticipated consequences or unforeseen consequences) are outcomes that are not the ones foreseen and intended by a purposeful action. The term was popularised in the twentieth century by American sociologist Robert K. Merton.[1]
