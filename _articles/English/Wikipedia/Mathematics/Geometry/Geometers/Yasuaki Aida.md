---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yasuaki_Aida
offline_file: ""
offline_thumbnail: ""
uuid: 8b9a05b5-a502-4272-9967-1d98cb4ff591
updated: 1484309069
title: Yasuaki Aida
tags:
    - Selected works
    - Notes
categories:
    - Geometers
---
He made significant contributions to the fields of number theory and geometry, and furthered methods for simplifying continued fractions.
