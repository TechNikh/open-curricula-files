---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mysore
offline_file: ""
offline_thumbnail: ""
uuid: 366788d6-044f-4809-8364-b4d778850e1f
updated: 1484308412
title: Mysore
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mysore_Palace%252C_India_%2528photo_-_Jim_Ankan_Deka%2529.jpg'
tags:
    - Etymology
    - History
    - geography
    - Climate
    - Administration and utilities
    - Demographics
    - Economy
    - Education
    - culture
    - Transport
    - Road
    - Rail
    - air
    - Media
    - Sports
    - Tourism
    - Sister cities
    - Notes
categories:
    - Uncategorized
---
Mysore[3] (i/maɪˈsʊər/), officially renamed as Mysuru, is the second most populous and also the second largest city[4] in the state of Karnataka, India. Located at the base of the Chamundi Hills about 146 km (91 mi) southwest of the state capital Bangalore, it is spread across an area of 128.42 km2 (50 sq mi). According to the provisional results of the 2011 national census of India, the population is 887,446. Mysore City Corporation is responsible for the civic administration of the city, which is also the headquarters of the Mysore district and the Mysore division.
