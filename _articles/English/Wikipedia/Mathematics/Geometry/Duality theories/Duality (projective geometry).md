---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Duality_(projective_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 209f1982-a49c-4938-9a6b-3e5a803091e5
updated: 1484309239
title: Duality (projective geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Dualquads.svg.png
tags:
    - Principle of Duality
    - Dual theorems
    - Dual configurations
    - Duality as a mapping
    - Plane dualities
    - In general projective spaces
    - Classification of dualities
    - Homogeneous coordinate formulation
    - A fundamental example
    - Geometric interpretation in the real projective plane
    - Matrix form
    - Polarity
    - Polarities of general projective spaces
    - Polarities in finite projective planes
    - Poles and polars
    - Reciprocation in the Euclidean plane
    - Algebraic formulation
    - Synthetic approach
    - Properties
    - History
    - Notes
categories:
    - Duality theories
---
In geometry a striking feature of projective planes is the symmetry of the roles played by points and lines in the definitions and theorems, and (plane) duality is the formalization of this concept. There are two approaches to the subject of duality, one through language (§ Principle of Duality) and the other a more functional approach through special mappings. These are completely equivalent and either treatment has as its starting point the axiomatic version of the geometries under consideration. In the functional approach there is a map between related geometries that is called a duality. Such a map can be constructed in many ways. The concept of plane duality readily extends to space ...
