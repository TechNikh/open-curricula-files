---
version: 1
type: video
provider: YouTube
id: HpdMJaKaXXc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Ratios%2C%20proportions%2C%20units%2C%20and%20rates/Introduction%20to%20ratios.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a2deb64e-13f7-405d-8ebc-ceb3026f7dc2
updated: 1486069571
title: Introduction to ratios
categories:
    - Ratios, proportions, units, and rates
thumbnail_urls:
    - https://i3.ytimg.com/vi/HpdMJaKaXXc/default.jpg
    - https://i3.ytimg.com/vi/HpdMJaKaXXc/1.jpg
    - https://i3.ytimg.com/vi/HpdMJaKaXXc/2.jpg
    - https://i3.ytimg.com/vi/HpdMJaKaXXc/3.jpg
---
What a ratio is. Simple ratio problems.

Practice this lesson yourself on KhanAcademy.org right now: https://www.khanacademy.org/math/pre-algebra/rates-and-ratios/ratios_and_proportions/e/ratio_word_problems?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Watch the next lesson: https://www.khanacademy.org/math/pre-algebra/rates-and-ratios/ratios_and_proportions/v/ratios-as-fractions-in-simplest-form?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Missed the previous lesson? 
https://www.khanacademy.org/math/pre-algebra/fractions-pre-alg/number-sets-pre-alg/v/number-sets-3?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Pre-Algebra on Khan Academy: No way, this isn't your run of the mill arithmetic. This is Pre-algebra. You're about to play with the professionals. Think of pre-algebra as a runway. You're the airplane and algebra is your sunny vacation destination. Without the runway you're not going anywhere. Seriously, the foundation for all higher mathematics is laid with many of the concepts that we will introduce to you here: negative numbers, absolute value, factors, multiples, decimals, and fractions to name a few. So buckle up and move your seat into the upright position. We're about to take off!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy’s Pre-Algebra channel:: https://www.youtube.com/channel/UCIMlYkATtXOFswVoCZN7nAA?sub_confirmation=1
Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
