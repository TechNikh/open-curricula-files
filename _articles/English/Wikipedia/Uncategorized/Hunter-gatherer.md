---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hunter-gatherers
offline_file: ""
offline_thumbnail: ""
uuid: cfd5b6d0-510c-40ec-b03b-e82144f4c209
updated: 1484308447
title: Hunter-gatherer
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-San_tribesman_0.jpg
tags:
    - Archaeological evidence
    - Common characteristics
    - Habitat and population
    - Social and economic structure
    - Variability
    - Modern and revisionist perspectives
    - Americas
    - Modern hunter-gatherer groups
    - Social movements
categories:
    - Uncategorized
---
A hunter-gatherer is a human living in a society in which most or all food is obtained by foraging (collecting wild plants and pursuing wild animals), in contrast to agricultural societies, which rely mainly on domesticated species.
