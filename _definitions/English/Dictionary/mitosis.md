---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mitosis
offline_file: ""
offline_thumbnail: ""
uuid: ec45ac28-e460-40e4-92f2-871cd0b0d639
updated: 1484310286
title: mitosis
categories:
    - Dictionary
---
mitosis
     n : cell division in which the nucleus divides into nuclei
         containing the same number of chromosomes
