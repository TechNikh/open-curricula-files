---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eduardo_Torroja_Caballe
offline_file: ""
offline_thumbnail: ""
uuid: 47cfac52-4d85-4892-b3a6-b07ee72d53af
updated: 1484309105
title: Eduardo Torroja Caballe
categories:
    - Geometers
---
Eduardo Torroja Caballé (February 1, 1847 – June 1, 1918) was a Spanish mathematician born in the city of Tarragona, Spain. His father was Juan Torroja, a Professor of Geography and History. He continued his studies at Complutense University, where he obtained the degrees of Bachelor of Science (1864), Masters of Science (1866), Architect (1869) and Doctor of Science (1873) in Mathematics.
