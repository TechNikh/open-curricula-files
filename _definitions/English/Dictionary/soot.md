---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soot
offline_file: ""
offline_thumbnail: ""
uuid: 1b39473f-8955-4088-8dc1-e12da9b5d22e
updated: 1484310210
title: soot
categories:
    - Dictionary
---
soot
     n : a black colloidal substance consisting wholly or principally
         of amorphous carbon and used to make pigments and ink
         [syn: {carbon black}, {lampblack}, {smut}]
     v : coat with soot
