---
version: 1
type: video
provider: YouTube
id: iLFcSfzrlMk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Life%20on%20earth%20and%20in%20the%20universe/K-Ar%20dating%20calculation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 769dc3d1-8e8d-4d78-85db-14b270fae416
updated: 1486069686
title: K-Ar dating calculation
categories:
    - Life on earth and in the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/iLFcSfzrlMk/default.jpg
    - https://i3.ytimg.com/vi/iLFcSfzrlMk/1.jpg
    - https://i3.ytimg.com/vi/iLFcSfzrlMk/2.jpg
    - https://i3.ytimg.com/vi/iLFcSfzrlMk/3.jpg
---
