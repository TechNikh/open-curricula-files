---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/devastate
offline_file: ""
offline_thumbnail: ""
uuid: b3c9d0a9-3f15-4d27-ab82-11188dee82eb
updated: 1484310242
title: devastate
categories:
    - Dictionary
---
devastate
     v 1: devastate or ravage; "The enemy lay waste to the countryside
          after the invasion" [syn: {lay waste to}, {waste}, {desolate},
           {ravage}, {scourge}]
     2: overwhelm or overpower; "He was devastated by his grief when
        his son died"
