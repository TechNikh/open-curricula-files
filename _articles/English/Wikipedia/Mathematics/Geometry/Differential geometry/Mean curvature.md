---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mean_curvature
offline_file: ""
offline_thumbnail: ""
uuid: 9be0d576-25db-46bd-b776-a1806c07cf1d
updated: 1484309230
title: Mean curvature
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/175px-Costa_minimal_surface.jpg
tags:
    - Definition
    - Surfaces in 3D space
    - Implicit form of mean curvature
    - Mean curvature in fluid mechanics
    - Minimal surfaces
    - Notes
categories:
    - Differential geometry
---
In mathematics, the mean curvature 
  
    
      
        H
      
    
    {\displaystyle H}
  
 of a surface 
  
    
      
        S
      
    
    {\displaystyle S}
  
 is an extrinsic measure of curvature that comes from differential geometry and that locally describes the curvature of an embedded surface in some ambient space such as Euclidean space.
