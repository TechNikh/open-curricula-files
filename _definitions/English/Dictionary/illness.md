---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illness
offline_file: ""
offline_thumbnail: ""
uuid: 49dad507-f972-46ae-8bfb-e117ac76d6a5
updated: 1484310471
title: illness
categories:
    - Dictionary
---
illness
     n : impairment of normal physiological function affecting part
         or all of an organism [syn: {unwellness}, {malady}, {sickness}]
         [ant: {health}, {health}]
