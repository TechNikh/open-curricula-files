---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/integrity
offline_file: ""
offline_thumbnail: ""
uuid: 1cd2260a-4ce2-436a-8e35-dffad8eeac89
updated: 1484310591
title: integrity
categories:
    - Dictionary
---
integrity
     n 1: an unreduced or unbroken completeness or totality [syn: {unity},
           {wholeness}]
     2: moral soundness
