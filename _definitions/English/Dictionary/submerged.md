---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/submerged
offline_file: ""
offline_thumbnail: ""
uuid: a168d10b-bdf8-4825-8b62-4b565314d6ad
updated: 1484310437
title: submerged
categories:
    - Dictionary
---
submerged
     adj 1: under water; e.g. at the bottom of a body of water; "sunken
            treasure"; "a sunken ship" [syn: {sunken}] [ant: {afloat(p)},
             {aground(p)}]
     2: beneath the surface of the water; "submerged rocks" [syn: {submersed},
         {underwater}]
     3: growing or remaining under water; "viewing subaqueous fauna
        from a glass-bottomed boat"; "submerged leaves" [syn: {subaqueous},
         {subaquatic}, {submersed}, {underwater}]
