---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bee-keeper
offline_file: ""
offline_thumbnail: ""
uuid: 9d2a4020-a55d-4635-88a7-ef82fa19a4cb
updated: 1484308432
title: Beekeeper
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Beekeeper_keeping_bees.jpg
tags:
    - Definition
    - Classifications of beekeepers
    - Types of beekeepers
    - Notable beekeepers
categories:
    - Uncategorized
---
Honey bees produce commodities such as honey, beeswax, pollen, propolis, and royal jelly, while some beekeepers also raise queens and bees to sell to other farmers and to satisfy scientific curiosity. Beekeepers also use honeybees to provide pollination services to fruit and vegetable growers. Many people keep bees as a hobby. Others do it for income, either as a sideline to other work or as a commercial operator. These factors affect the number of colonies maintained by the beekeeper.
