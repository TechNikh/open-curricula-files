---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/passing
offline_file: ""
offline_thumbnail: ""
uuid: 582df9d4-d145-445d-9d84-31ca8100c5e1
updated: 1484310311
title: passing
categories:
    - Dictionary
---
passing
     adj 1: enduring a very short time; "the ephemeral joys of
            childhood"; "a passing fancy"; "youth's transient
            beauty"; "love is transitory but at is eternal";
            "fugacious blossoms" [syn: {ephemeral}, {short-lived},
             {transient}, {transitory}, {fugacious}]
     2: of advancing the ball by throwing it; "a team with a good
        passing attack"; "a pass play" [syn: {passing(a)}, {pass(a)}]
        [ant: {running(a)}]
     3: allowing you to pass (e.g., an examination or inspection)
        satisfactorily; "a passing grade" [syn: {passing(a)}]
     4: hasty and without attention to detail; not thorough; "a
        casual (or cursory) ...
