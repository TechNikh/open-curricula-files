---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cobalt
offline_file: ""
offline_thumbnail: ""
uuid: ab7a0322-fec7-42fb-9e06-a368e63fa9c4
updated: 1484310397
title: cobalt
categories:
    - Dictionary
---
cobalt
     n : a hard ferromagnetic silver-white bivalent or trivalent
         metallic element; a trace element in plant and animal
         nutrition [syn: {Co}, {atomic number 27}]
