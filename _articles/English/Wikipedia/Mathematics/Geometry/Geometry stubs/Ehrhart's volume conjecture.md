---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ehrhart%27s_volume_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 665b59e1-7f8d-4e1b-97e7-e7dd45ae64e2
updated: 1484309360
title: "Ehrhart's volume conjecture"
categories:
    - Geometry stubs
---
In the geometry of numbers, Ehrhart's volume conjecture gives an upper bound on the volume of a convex body containing only one lattice point in its interior. It is a kind of converse to Minkowski's theorem, which guarantees that a centrally symmetric convex body K must contain a lattice point as soon as its volume exceeds 
  
    
      
        
          2
          
            n
          
        
      
    
    {\displaystyle 2^{n}}
  
. The conjecture states that a convex body K containing only one lattice point in its interior can have volume no greater than 
  
    
      
        (
        n
        +
        1
        
          )
          
            n
          
        
   ...
