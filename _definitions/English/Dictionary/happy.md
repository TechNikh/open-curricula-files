---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/happy
offline_file: ""
offline_thumbnail: ""
uuid: aacf4c23-f28d-4ea7-8da6-f50c2d81dee5
updated: 1484310445
title: happy
categories:
    - Dictionary
---
happy
     adj 1: enjoying or showing or marked by joy or pleasure or good
            fortune; "a happy smile"; "spent many happy days on
            the beach"; "a happy marriage" [ant: {unhappy}]
     2: experiencing pleasure or joy; "happy you are here"; "pleased
        with the good news" [syn: {pleased}]
     3: marked by good fortune; "a felicitous life"; "a happy
        outcome" [syn: {felicitous}]
     4: satisfied; enjoying well-being and contentment; "felt
        content with her lot"; "quite happy to let things go on as
        they are"
     5: exaggerated feeling of well-being or elation [syn: {euphoric}]
        [ant: {dysphoric}]
     6: well expressed and to the point; ...
