---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/serving
offline_file: ""
offline_thumbnail: ""
uuid: d225c629-a6eb-43d8-bbed-5c3194d3f9c7
updated: 1484310429
title: serving
categories:
    - Dictionary
---
serving
     n 1: an individual quantity of food or drink taken as part of a
          meal; "the helpings were all small"; "his portion was
          larger than hers"; "there's enough for two servings
          each" [syn: {helping}, {portion}]
     2: the act of delivering a writ or summons upon someone; "he
        accepted service of the subpoena" [syn: {service}, {service
        of process}]
