---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Melvin_Dresher
offline_file: ""
offline_thumbnail: ""
uuid: 0f649078-3e57-41d1-b27e-7a52c152f649
updated: 1484308513
title: Melvin Dresher
categories:
    - Game theorists
---
Melvin Dresher (Dreszer) (March 13, 1911 – June 4, 1992) was a Polish-born American mathematician, notable for developing, with Merrill Flood, the game theoretical model of cooperation and conflict known as the Prisoner's Dilemma while at RAND in 1950 (Albert W. Tucker gave the game its prison-sentence interpretation, and thus the name by which it is known today).
