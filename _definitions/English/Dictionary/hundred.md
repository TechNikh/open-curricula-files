---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hundred
offline_file: ""
offline_thumbnail: ""
uuid: 797a37f5-7660-4f68-916b-923c24e99f43
updated: 1484310279
title: hundred
categories:
    - Dictionary
---
hundred
     adj : being ten more than ninety [syn: {a hundred}, {one hundred},
            {100}, {c}]
     n : ten 10s [syn: {100}, {C}, {century}, {one C}, {centred}]
