---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secrete
offline_file: ""
offline_thumbnail: ""
uuid: 04485c7e-7e8f-4ade-abd2-0fb66e3cae3c
updated: 1484310330
title: secrete
categories:
    - Dictionary
---
secrete
     v 1: generate and separate from cells or bodily fluids; "secrete
          digestive juices"; "release a hormone into the blood
          stream" [syn: {release}]
     2: place out of sight; keep secret; "The money was secreted
        from his children"
