---
version: 1
type: video
provider: YouTube
id: VWAfEbgf1Po
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Test%20taking%20probability%20and%20independent%20events.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 98d72e01-e267-4e11-8b4c-ec6ba82cfd34
updated: 1486069557
title: Test taking probability and independent events
categories:
    - Precalculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/VWAfEbgf1Po/default.jpg
    - https://i3.ytimg.com/vi/VWAfEbgf1Po/1.jpg
    - https://i3.ytimg.com/vi/VWAfEbgf1Po/2.jpg
    - https://i3.ytimg.com/vi/VWAfEbgf1Po/3.jpg
---
