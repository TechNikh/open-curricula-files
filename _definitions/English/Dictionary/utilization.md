---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/utilization
offline_file: ""
offline_thumbnail: ""
uuid: d31063a3-2196-436e-bbd6-fbaccd28d9d6
updated: 1484310355
title: utilization
categories:
    - Dictionary
---
utilization
     n 1: the act of using; "he warned against the use of narcotic
          drugs"; "skilled in the utilization of computers" [syn:
          {use}, {usage}, {utilisation}, {employment}, {exercise}]
     2: the state of having been made use of; "the rate of
        utilization"
