---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unquestioned
offline_file: ""
offline_thumbnail: ""
uuid: 110a3cfb-7de0-46d2-80f8-0eb15797178d
updated: 1484310547
title: unquestioned
categories:
    - Dictionary
---
unquestioned
     adj : accepted without question; "undoubted evidence" [syn: {unchallenged},
            {undisputed}, {undoubted}]
