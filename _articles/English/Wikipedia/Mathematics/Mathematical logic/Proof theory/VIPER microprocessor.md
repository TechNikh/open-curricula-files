---
version: 1
type: article
id: https://en.wikipedia.org/wiki/VIPER_microprocessor
offline_file: ""
offline_thumbnail: ""
uuid: 34f97d69-8ca4-419d-a6e9-4f2074424633
updated: 1484309499
title: VIPER microprocessor
categories:
    - Proof theory
---
VIPER is a 32-bit microprocessor design created by Royal Signals and Radar Establishment in the 1980s, intended for use in safety-critical systems such as avionics.[1] It was the first commercial microprocessor design to be formally proven correct, although there was some controversy surrounding this claim and the definition of proof.
