---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Principle_of_maximum_caliber
offline_file: ""
offline_thumbnail: ""
uuid: da7a5a91-1548-4a1b-8145-3a6f9410a2f1
updated: 1484308773
title: Principle of maximum caliber
tags:
    - History
    - Mathematical formulation
    - Maximum caliber and statistical mechanics
    - Notes
categories:
    - Entropy and information
---
The principle of maximum caliber (MaxCal) or maximum path entropy principle suggested by E. T. Jaynes,[1] can be considered as a generalization of the principle of maximum entropy, postulates that the most unbiased probability distribution of paths is the one that maximizes their Shannon entropy. This entropy of paths is sometimes called the "caliber" of the system, and is given by the path integral
