---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rugged
offline_file: ""
offline_thumbnail: ""
uuid: 93d22a15-2625-41d2-b726-c4612158c584
updated: 1484310435
title: rugged
categories:
    - Dictionary
---
rugged
     adj 1: sturdy and strong in constitution or construction; enduring;
            "a rugged trapper who spent months in the winderness";
            "those that survive are stalwart rugged men"; "with a
            house full of boys you have to have rugged furniture"
            [ant: {delicate}]
     2: topographically very uneven; "broken terrain"; "rugged
        ground" [syn: {broken}]
     3: rocky and steep [syn: {craggy}]
     4: very difficult; severely testing stamina or resolution; "a
        rugged competitive examination"; "the rugged conditions of
        frontier life"; "the competition was tough"; "it's a tough
        life"; "it was a tough job" [syn: {tough}]
