---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dissolve
offline_file: ""
offline_thumbnail: ""
uuid: 9afca36f-9ef4-4084-91ab-27ac909454e7
updated: 1484310351
title: dissolve
categories:
    - Dictionary
---
dissolve
     n : (film) a gradual transition from one scene to the next; the
         next scene is gradually superimposed as the former scene
         fades out
     v 1: cause to go into a solution; "The recipe says that we should
          dissolve a cup of sugar in two cups of water" [syn: {resolve},
           {break up}]
     2: pass into a solution; "The sugar quickly dissolved in the
        coffee"
     3: become weaker; "The sound faded out" [syn: {fade out}, {fade
        away}]
     4: come to an end; "Their marriage dissolved"; "The tobacco
        monopoly broke up" [syn: {break up}]
     5: stop functioning or cohering as a unit; "The political wing
        of the party ...
