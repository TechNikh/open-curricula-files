---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/virtually
offline_file: ""
offline_thumbnail: ""
uuid: 3bb8f28a-ab87-4ee1-bcb9-00ba95780cc2
updated: 1484310573
title: virtually
categories:
    - Dictionary
---
virtually
     adv 1: (intensifier before a figurative expression) without
            exaggeration; "our eyes were literally pinned to TV
            during the Gulf war" [syn: {literally}]
     2: in essence or effect but not in fact; "the strike virtually
        paralyzed the city"; "I'm virtually broke"
     3: (of actions or states) slightly short of or not quite
        accomplished; `near' is sometimes used informally for
        `nearly' and `most' is sometimes used informally for
        `almost'; "the job is (just) about done"; "the baby was
        almost asleep when the alarm sounded"; "we're almost
        finished"; "the car all but ran her down"; "he nearly
        fainted"; ...
