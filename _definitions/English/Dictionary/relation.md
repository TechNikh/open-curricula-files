---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relation
offline_file: ""
offline_thumbnail: ""
uuid: 9d0a4711-d2a0-4fd2-9f12-259efc2d5af3
updated: 1484310349
title: relation
categories:
    - Dictionary
---
relation
     n 1: an abstraction belonging to or characteristic of two
          entities or parts together
     2: the act of sexual procreation between a man and a woman; the
        man's penis is inserted into the woman's vagina and
        excited until orgasm and ejaculation occur [syn: {sexual
        intercourse}, {intercourse}, {sex act}, {copulation}, {coitus},
         {coition}, {sexual congress}, {congress}, {sexual
        relation}, {carnal knowledge}]
     3: a person related by blood or marriage; "police are searching
        for relatives of the deceased"; "he has distant relations
        back in New Jersey" [syn: {relative}]
     4: an act of narration; "he was the hero ...
