---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bride
offline_file: ""
offline_thumbnail: ""
uuid: 4232c96d-4a89-40c8-8d70-6e80f965eba6
updated: 1484310146
title: bride
categories:
    - Dictionary
---
bride
     n 1: a woman who has recently been married
     2: Irish abbess; a patron saint of Ireland (453-523) [syn: {Bridget},
         {Saint Bridget}, {St. Bridget}, {Brigid}, {Saint Brigid},
         {St. Brigid}, {Saint Bride}, {St. Bride}]
     3: a woman participant in her own marriage ceremony
