---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jeong_Han_Kim
offline_file: ""
offline_thumbnail: ""
uuid: ad6c8b83-ce63-40e7-bfde-e747ea2b76f1
updated: 1484309468
title: Jeong Han Kim
categories:
    - Graph theorists
---
Jeong Han Kim (Hangul: 김정한; born July 20, 1962) is a South Korean mathematician. He studied physics and mathematical physics at Yonsei University, and earned his Ph.D in mathematics at Rutgers University. He was a researcher at AT&T Bell Labs, Microsoft Research, and was Underwood Chair Professor of Mathematics at Yonsei University. He is currently a Professor of School of Computational Sciences at Korea Institute for Advanced Study.
