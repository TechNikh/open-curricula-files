---
version: 1
type: video
provider: YouTube
id: 2olwZDsDwtI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4e64200a-db75-4cf2-90dd-7b03f463c8c0
updated: 1486069660
title: Steve Spangler on The Ellen Show February 2008
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/2olwZDsDwtI/default.jpg
    - https://i3.ytimg.com/vi/2olwZDsDwtI/1.jpg
    - https://i3.ytimg.com/vi/2olwZDsDwtI/2.jpg
    - https://i3.ytimg.com/vi/2olwZDsDwtI/3.jpg
---
