---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nil
offline_file: ""
offline_thumbnail: ""
uuid: e4c6a6bc-7c58-4b99-97db-87f42a4c4e9f
updated: 1484310163
title: nil
categories:
    - Dictionary
---
nil
     n : a quantity of no importance; "it looked like nothing I had
         ever seen before"; "reduced to nil all the work we had
         done"; "we racked up a pathetic goose egg"; "it was all
         for naught"; "I didn't hear zilch about it" [syn: {nothing},
          {nix}, {nada}, {null}, {aught}, {cipher}, {cypher}, {goose
         egg}, {naught}, {zero}, {zilch}, {zip}]
