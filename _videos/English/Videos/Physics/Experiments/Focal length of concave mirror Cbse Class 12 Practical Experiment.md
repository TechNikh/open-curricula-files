---
version: 1
type: video
provider: YouTube
id: 8XnKXnc5v28
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 38efca2b-29ce-4655-9dae-ce08ee01271f
updated: 1486069636
title: >
    Focal length of concave mirror Cbse Class 12 Practical
    Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/8XnKXnc5v28/default.jpg
    - https://i3.ytimg.com/vi/8XnKXnc5v28/1.jpg
    - https://i3.ytimg.com/vi/8XnKXnc5v28/2.jpg
    - https://i3.ytimg.com/vi/8XnKXnc5v28/3.jpg
---
