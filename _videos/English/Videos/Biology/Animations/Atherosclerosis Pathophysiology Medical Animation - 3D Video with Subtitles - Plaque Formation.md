---
version: 1
type: video
provider: YouTube
id: vIKuijJ1sCk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4297c850-eef8-416b-bfee-53037c5c9df5
updated: 1486069668
title: 'Atherosclerosis Pathophysiology Medical Animation - 3D Video with Subtitles - Plaque Formation'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/vIKuijJ1sCk/default.jpg
    - https://i3.ytimg.com/vi/vIKuijJ1sCk/1.jpg
    - https://i3.ytimg.com/vi/vIKuijJ1sCk/2.jpg
    - https://i3.ytimg.com/vi/vIKuijJ1sCk/3.jpg
---
