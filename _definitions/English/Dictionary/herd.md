---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/herd
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484579401
title: herd
categories:
    - Dictionary
---
herd
     n 1: a group of cattle or sheep or other domestic mammals all of
          the same kind that are herded by humans
     2: a group of wild animals of one species that remain together:
        antelope or elephants or seals or whales or zebra
     3: a crowd especially of ordinary or undistinguished persons or
        things; "his brilliance raised him above the ruck"; "the
        children resembled a fairy herd" [syn: {ruck}]
     v 1: cause to herd, drive, or crowd together; "We herded the
          children into a spare classroom" [syn: {crowd}]
     2: move together, like a herd
     3: keep, move, or drive animals; "Who will be herding the
        cattle when the cowboy ...
