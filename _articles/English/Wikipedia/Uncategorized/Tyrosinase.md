---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tyrosinase
offline_file: ""
offline_thumbnail: ""
uuid: f5df26ff-44b9-4491-90a1-22724c560ea6
updated: 1484308313
title: Tyrosinase
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-PBB_GE_TYR_206630_at_tn.png
tags:
    - Significance
    - Clinical significance
    - Significance in food industry
    - Significance in insects
    - Catalyzed reaction
    - Structure
    - Human tyrosinase
    - Active site
    - Gene regulation
categories:
    - Uncategorized
---
Tyrosinase is an oxidase that is the rate-limiting enzyme for controlling the production of melanin. The enzyme is mainly involved in two distinct reactions of melanin synthesis; firstly, the hydroxylation of a monophenol and secondly, the conversion of an o-diphenol to the corresponding o-quinone. o-Quinone undergoes several reactions to eventually form melanin.[4] Tyrosinase is a copper-containing enzyme present in plant and animal tissues that catalyzes the production of melanin and other pigments from tyrosine by oxidation, as in the blackening of a peeled or sliced potato exposed to air.[5] It is found inside melanosomes which are synthesised in the skin melanocytes. In humans, the ...
