---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shark
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484411521
title: shark
categories:
    - Dictionary
---
shark
     n 1: any of numerous elongate mostly marine carnivorous fishes
          with heterocercal caudal fins and tough skin covered
          with small toothlike scales
     2: a person who is ruthless and greedy and dishonest
     3: a person who is unusually skilled in certain ways; "a card
        shark"
     v 1: play the shark; act with trickery
     2: hunt shark
