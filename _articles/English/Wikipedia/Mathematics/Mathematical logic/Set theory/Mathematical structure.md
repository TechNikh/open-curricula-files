---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mathematical_structure
offline_file: ""
offline_thumbnail: ""
uuid: 6e1ca5f0-de47-4b3e-811a-e6d43ef9f42a
updated: 1484309504
title: Mathematical structure
tags:
    - History
    - 'Example: the real numbers'
categories:
    - Set theory
---
In mathematics, a structure on a set is an additional mathematical object that, in some manner, attaches (or relates) to that set to endow it with some additional meaning or significance.
