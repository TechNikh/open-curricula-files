---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infant
offline_file: ""
offline_thumbnail: ""
uuid: 3a83677a-8f9d-4ba3-9552-50dac9243a14
updated: 1484310445
title: infant
categories:
    - Dictionary
---
infant
     n : a very young child (birth to 1 year) who has not yet begun
         to walk or talk; "isn't she too young to have a baby?"
         [syn: {baby}, {babe}]
