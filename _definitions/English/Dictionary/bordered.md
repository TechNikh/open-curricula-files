---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bordered
offline_file: ""
offline_thumbnail: ""
uuid: 9a12625d-4fe9-47ac-a21b-9c34b5542e63
updated: 1484310437
title: bordered
categories:
    - Dictionary
---
bordered
     adj : having a border especially of a specified kind; sometimes
           used as a combining term; "black-bordered handkerchief"
           [ant: {unbordered}]
