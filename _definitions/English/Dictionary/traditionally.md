---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/traditionally
offline_file: ""
offline_thumbnail: ""
uuid: a51b3899-c77f-478c-a39e-340a9dded605
updated: 1484310477
title: traditionally
categories:
    - Dictionary
---
traditionally
     adv : according to tradition; in a traditional manner;
           "traditionally, we eat fried foods on Hanukah"
