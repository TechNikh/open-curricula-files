---
version: 1
type: video
provider: YouTube
id: NlCoDbmwFVs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Genome%20editing%20with%20GeneArt%C2%AE%20Precision%20TALs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b29b2ec6-1abe-42da-849f-bfb7a0d4b2a2
updated: 1486069672
title: Genome editing with GeneArt® Precision TALs
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/NlCoDbmwFVs/default.jpg
    - https://i3.ytimg.com/vi/NlCoDbmwFVs/1.jpg
    - https://i3.ytimg.com/vi/NlCoDbmwFVs/2.jpg
    - https://i3.ytimg.com/vi/NlCoDbmwFVs/3.jpg
---
