---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moved
offline_file: ""
offline_thumbnail: ""
uuid: 74c3f144-b566-44ba-995f-efc0a1e6dcf1
updated: 1484310339
title: moved
categories:
    - Dictionary
---
moved
     adj : emotionally moved; "too moved to speak" [syn: {moved(p)}]
           [ant: {unmoved(p)}]
