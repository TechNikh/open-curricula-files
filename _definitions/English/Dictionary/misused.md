---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/misused
offline_file: ""
offline_thumbnail: ""
uuid: d44994c1-ca9c-4251-8cf1-6c1ac4433aa7
updated: 1484310254
title: misused
categories:
    - Dictionary
---
misused
     adj : used incorrectly or carelessly or for an improper purpose;
           "misused words are often laughable but one weeps for
           misused talents" [ant: {used}]
