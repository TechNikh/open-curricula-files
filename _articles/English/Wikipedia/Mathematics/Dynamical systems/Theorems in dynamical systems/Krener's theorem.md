---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Krener%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 15f2ba08-0d81-45bf-8c4a-075e1fe1baec
updated: 1484308696
title: "Krener's theorem"
categories:
    - Theorems in dynamical systems
---
In mathematics, Krener's theorem is a result attributed to Arthur J. Krener in geometric control theory about the topological properties of attainable sets of finite-dimensional control systems. It states that any attainable set of a bracket-generating system has nonempty interior or, equivalently, that any attainable set has nonempty interior in the topology of the corresponding orbit. Heuristically, Krener's theorem prohibits attainable sets from being hairy.
