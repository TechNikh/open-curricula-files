---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foster
offline_file: ""
offline_thumbnail: ""
uuid: decee95b-0a53-4c45-9013-2f755b5d0c0a
updated: 1484310605
title: foster
categories:
    - Dictionary
---
foster
     adj : providing or receiving nurture or parental care though not
           related by blood or legal ties; "foster parent";
           "foster child"; "foster home"; "surrogate father" [syn:
            {surrogate}]
     n : United States songwriter whose songs embody the sentiment of
         the South before the American Civil War (1826-1864) [syn:
          {Stephen Foster}, {Stephen Collins Foster}]
     v 1: promote the growth of; "Foster our children's well-being and
          education" [syn: {further}]
     2: bring up under fosterage; of children
     3: help develop, help grow; "nurture his talents" [syn: {nurture}]
