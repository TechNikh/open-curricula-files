---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rann
offline_file: ""
offline_thumbnail: ""
uuid: 75347129-6d0d-4620-a38f-ac4c41ba05dd
updated: 1484308422
title: Rann (fictional planet)
tags:
    - Natives of Rann
    - Noted Rannians
    - Rann technology
    - Rann and Thanagar
    - R.E.B.E.L.S.
    - Other versions
    - In other media
categories:
    - Uncategorized
---
Rann is a fictional planet in the Polaris star system (formerly the Alpha Centauri System) of the DC Comics Universe whose capital city is Ranagar. Rann is most famous for being the adopted planet of the Earth explorer and hero Adam Strange and for their teleportation device called the Zeta Beam. The planet Rann, along with her famous non-native son, first appeared in Showcase #17 (November–December 1958).
