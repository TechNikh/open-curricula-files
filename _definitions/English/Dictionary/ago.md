---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ago
offline_file: ""
offline_thumbnail: ""
uuid: 583f4274-4c1b-463e-8ca6-4e7f5d8a919e
updated: 1484310283
title: ago
categories:
    - Dictionary
---
ago
     adj : gone by; or in the past; "two years ago"; "`agone' is an
           archaic word for `ago'" [syn: {agone}]
     adv : in the past; "long ago"; "sixty years ago my grandfather
           came to the U.S."
