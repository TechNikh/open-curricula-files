---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crisp
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484582941
title: crisp
categories:
    - Dictionary
---
crisp
     adj 1: (of something seen or heard) clearly defined; "a sharp
            photographic image"; "the sharp crack of a twig"; "the
            crisp snap of dry leaves underfoot" [syn: {sharp}]
     2: tender and brittle; "crisp potato chips" [syn: {crispy}]
     3: pleasantly cold and invigorating; "crisp clear nights and
        frosty mornings"; "a nipping wind"; "a nippy fall day";
        "snappy weather"; (`parky' is a British term) [syn: {frosty},
         {nipping}, {nippy}, {snappy}, {parky}]
     4: pleasingly firm and fresh and making a crunching noise when
        chewed; "crisp carrot and celery sticks"; "a firm apple";
        "crunchy lettuce" [syn: {firm}, ...
