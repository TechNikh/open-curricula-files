---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sheikh
offline_file: ""
offline_thumbnail: ""
uuid: 615fb3c8-86a8-4348-94c7-de281863907d
updated: 1484310609
title: sheikh
categories:
    - Dictionary
---
sheikh
     n : the leader of an Arab village or family [syn: {sheik}, {tribal
         sheik}, {tribal sheikh}, {Arab chief}]
