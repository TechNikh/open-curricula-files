---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/star
offline_file: ""
offline_thumbnail: ""
uuid: f225614b-ef5b-4b70-a050-7821ce7fdfb6
updated: 1484310477
title: star
categories:
    - Dictionary
---
star
     adj : indicating the most important performer or role; "the
           leading man"; "prima ballerina"; "prima donna"; "a star
           figure skater"; "the starring role"; "a stellar role";
           "a stellar performance" [syn: {leading(p)}, {prima(p)},
            {star(p)}, {starring(p)}, {stellar(a)}]
     n 1: (astronomy) a celestial body of hot gases that radiates
          energy derived from thermonuclear reactions in the
          interior
     2: someone who is dazzlingly skilled in any field [syn: {ace},
        {adept}, {champion}, {sensation}, {maven}, {mavin}, {virtuoso},
         {genius}, {hotshot}, {superstar}, {whiz}, {whizz}, {wizard},
         {wiz}]
     ...
