---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sect
offline_file: ""
offline_thumbnail: ""
uuid: 4daf304a-7e42-4e11-9f9d-fd1e257085a2
updated: 1484310585
title: sect
categories:
    - Dictionary
---
sect
     n 1: a subdivision of a larger religious group [syn: {religious
          sect}, {religious order}]
     2: a dissenting clique [syn: {faction}]
