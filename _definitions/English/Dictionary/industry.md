---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/industry
offline_file: ""
offline_thumbnail: ""
uuid: 0008c40d-63f8-44ee-85ab-0e68ac10ed59
updated: 1484310387
title: industry
categories:
    - Dictionary
---
industry
     n 1: the people or companies engaged in a particular kind of
          commercial enterprise; "each industry has its own trade
          publications"
     2: the organized action of making of goods and services for
        sale; "American industry is making increased use of
        computers to control production" [syn: {manufacture}]
     3: persevering determination to perform a task; "his diligence
        won him quick promotions"; "frugality and industry are
        still regarded as virtues" [syn: {diligence}, {industriousness}]
