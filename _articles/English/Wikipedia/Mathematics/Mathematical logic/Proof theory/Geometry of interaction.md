---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometry_of_interaction
offline_file: ""
offline_thumbnail: ""
uuid: ff69220a-a8fb-43f9-beca-e94eb2b79266
updated: 1484309492
title: Geometry of interaction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Crystal_Clear_app_3d_0.png
categories:
    - Proof theory
---
The Geometry of Interaction (GoI) was introduced by Jean-Yves Girard shortly after his work on Linear logic. In linear logic, proofs can be seen as various kinds of networks as opposed to the flat tree structures of sequent calculus. To distinguish the real proof nets from all the possible networks, Girard devised a criterium involving trips in the network. Trips can in fact be seen as some kind of operator[clarification needed] acting on the proof. Drawing from this observation, Girard described directly this operator from the proof and has given a formula, the so-called execution formula, encoding the process of cut elimination at the level of operators.
