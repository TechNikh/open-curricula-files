---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paraconsistent_mathematics
offline_file: ""
offline_thumbnail: ""
uuid: 032bae35-bf2b-453b-a96b-2c0c87df8682
updated: 1484309497
title: Paraconsistent mathematics
categories:
    - Proof theory
---
Paraconsistent mathematics (sometimes called inconsistent mathematics) represents an attempt to develop the classical infrastructure of mathematics (e.g. analysis) based on a foundation of paraconsistent logic instead of classical logic. A number of reformulations of analysis can be developed, for example functions which both do and do not have a given value simultaneously.
