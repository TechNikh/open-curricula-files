---
version: 1
type: article
id: https://en.wikipedia.org/wiki/V
offline_file: ""
offline_thumbnail: ""
uuid: 401e0084-5844-4303-bb46-7eb0f59d812d
updated: 1484308384
title: V
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-V_cursiva.gif
tags:
    - History
    - Letter
    - Name in other languages
    - Use in writing systems
    - Other systems
    - Related characters
    - Descendants and related letters in the Latin alphabet
    - Ancestors and siblings in other alphabets
    - Ligatures and abbreviations
    - Computing codes
    - Other representations
categories:
    - Uncategorized
---
