---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sovereign
offline_file: ""
offline_thumbnail: ""
uuid: 68c1c2a8-9743-405d-b999-a57c659c6d71
updated: 1484310527
title: sovereign
categories:
    - Dictionary
---
sovereign
     adj 1: of political bodies; "an autonomous judiciary"; "a sovereign
            state" [syn: {autonomous}, {independent}, {self-governing}]
     2: greatest in status or authority or power; "a supreme
        tribunal" [syn: {supreme}]
     n : a nation's ruler or head of state usually by hereditary
         right [syn: {crowned head}, {monarch}]
