---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spectra
offline_file: ""
offline_thumbnail: ""
uuid: 5b6d8d3a-1ecf-466c-bf9f-6bdcabe42527
updated: 1484310391
title: spectra
categories:
    - Dictionary
---
spectrum
     n 1: an ordered array of the components of an emission or wave
     2: broad range of related values or qualities or ideas or
        activities
     [also: {spectra} (pl)]
