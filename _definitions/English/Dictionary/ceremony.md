---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ceremony
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484460121
title: ceremony
categories:
    - Dictionary
---
ceremony
     n 1: a formal event performed on a special occasion; "a ceremony
          commemorating Pearl Harbor" [syn: {ceremonial}, {ceremonial
          occasion}, {observance}]
     2: any activity that is performed in an especially solemn
        elaborate or formal way; "the ceremony of smelling the
        cork and tasting the wine"; "he makes a ceremony of
        addressing his golf ball"; "he disposed of it without
        ceremony"
     3: the proper or conventional behavior on some solemn occasion;
        "an inaugural ceremony"
