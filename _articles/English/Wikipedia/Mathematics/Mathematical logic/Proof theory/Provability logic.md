---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Provability_logic
offline_file: ""
offline_thumbnail: ""
uuid: 4257c61e-9372-4fbe-b2f3-96136399eb5e
updated: 1484309497
title: Provability logic
tags:
    - Examples
    - History
    - Generalizations
categories:
    - Proof theory
---
Provability logic is a modal logic, in which the box (or "necessity") operator is interpreted as 'it is provable that'. The point is to capture the notion of a proof predicate of a reasonably rich formal theory, such as Peano arithmetic.
