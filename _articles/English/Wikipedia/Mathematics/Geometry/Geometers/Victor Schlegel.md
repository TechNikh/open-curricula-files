---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Victor_Schlegel
offline_file: ""
offline_thumbnail: ""
uuid: 5101bb44-18a1-46ac-99e1-9df3a3e2e46a
updated: 1484309098
title: Victor Schlegel
categories:
    - Geometers
---
Victor Schlegel (1843–1905)[1] was a German mathematician. He is remembered for promoting the geometric algebra of Hermann Grassmann and for a method of visualizing polytopes called Schlegel diagrams.
