---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mohammad_Hajiaghayi
offline_file: ""
offline_thumbnail: ""
uuid: 947712bb-aa11-4b29-8268-08437babfe63
updated: 1484309463
title: Mohammad Hajiaghayi
tags:
    - Early life
    - Professional career
    - Honors and awards
categories:
    - Graph theorists
---
Mohammad Taghi Hajiaghayi (Persian: محمد تقی‌ حاجی آقائی‎‎; born in 1979) is a computer scientist known for his work in algorithms, game theory, social networks, network design, graph theory, and big data.[1][2][3] More specifically he has designed numerous algorithms and taught classes in the areas of approximation algorithms, fixed-parameter algorithms, algorithmic game theory, algorithmic graph theory, online algorithms, and streaming algorithms. He has over 200 publications with over 185 collaborators and 10 issued patents.[4][5]
