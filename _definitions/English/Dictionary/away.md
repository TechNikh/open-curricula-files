---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/away
offline_file: ""
offline_thumbnail: ""
uuid: f0568d70-25bb-4b8a-9826-3dcfe38c165c
updated: 1484310284
title: away
categories:
    - Dictionary
---
away
     adj 1: distant in either space or time; "the town is a mile away";
            "a country far away"; "the game is a week away" [syn:
            {away(p)}]
     2: not present; having left; "he's away right now"; "you must
        not allow a stranger into the house when your mother is
        away"; "everyone is gone now"; "the departed guests" [syn:
         {away(p)}, {gone(p)}, {departed(a)}]
     3: used of an opponent's ground; "an away game" [ant: {home(a)}]
     4: (of a baseball pitch) on the far side of home plate from the
        batter; "the pitch was away (or wide)"; "an outside pitch"
        [syn: {outside}]
     adv 1: from a particular thing or place or position ...
