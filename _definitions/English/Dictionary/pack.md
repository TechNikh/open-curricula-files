---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pack
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484393521
title: pack
categories:
    - Dictionary
---
pack
     n 1: a large indefinite number; "a battalion of ants"; "a
          multitude of TV antennas"; "a plurality of religions"
          [syn: {battalion}, {large number}, {multitude}, {plurality}]
     2: a complete collection of similar things
     3: a small parcel (as of cigarettes or film)
     4: an association of criminals; "police tried to break up the
        gang"; "a pack of thieves" [syn: {gang}, {ring}, {mob}]
     5: an exclusive circle of people with a common purpose [syn: {clique},
         {coterie}, {ingroup}, {inner circle}, {camp}]
     6: a group of hunting animals
     7: a cream that cleanses and tones the skin [syn: {face pack}]
     8: a sheet or blanket ...
