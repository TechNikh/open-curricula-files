---
version: 1
type: video
provider: YouTube
id: hNHxeCuu3cI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bb9673c3-0c8e-4fdb-9a21-1a2c11934ac3
updated: 1486069622
title: 'Shortcut Trick - How to check perfect square numbers - Vedic Maths'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/hNHxeCuu3cI/default.jpg
    - https://i3.ytimg.com/vi/hNHxeCuu3cI/1.jpg
    - https://i3.ytimg.com/vi/hNHxeCuu3cI/2.jpg
    - https://i3.ytimg.com/vi/hNHxeCuu3cI/3.jpg
---
