---
version: 1
type: video
provider: YouTube
id: JoL2pO3O0rg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Black%20holes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 22dc59b4-9261-49cb-bfce-6d29814d7eda
updated: 1486069686
title: Black holes
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/JoL2pO3O0rg/default.jpg
    - https://i3.ytimg.com/vi/JoL2pO3O0rg/1.jpg
    - https://i3.ytimg.com/vi/JoL2pO3O0rg/2.jpg
    - https://i3.ytimg.com/vi/JoL2pO3O0rg/3.jpg
---
