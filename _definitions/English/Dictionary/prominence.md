---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prominence
offline_file: ""
offline_thumbnail: ""
uuid: 5e0d1d9e-c02c-4102-9b47-04debada6018
updated: 1484310431
title: prominence
categories:
    - Dictionary
---
prominence
     n 1: the state of being prominent: widely known or eminent [ant:
          {obscurity}]
     2: relative importance
     3: something that bulges out or is protuberant or projects from
        a form [syn: {bulge}, {bump}, {hump}, {gibbosity}, {gibbousness},
         {jut}, {protuberance}, {protrusion}, {extrusion}, {excrescence}]
