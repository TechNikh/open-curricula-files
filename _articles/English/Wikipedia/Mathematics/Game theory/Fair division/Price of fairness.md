---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Price_of_fairness
offline_file: ""
offline_thumbnail: ""
uuid: cd7ae533-9837-4ef4-9a74-e8aca3353535
updated: 1484308524
title: Price of fairness
tags:
    - Numeric example
    - Upper bound
    - Lower bound
    - Combined
    - Cake-cutting with general pieces
    - Utilitarian price of proportionality
    - Utilitarian price of envy
    - Utilitarian price of equitability
    - Indivisible item assignment
    - Divisible chore division
    - Indivisible chore assignment
    - Cake-cutting with connected pieces
    - Utilitarian price of fairness
    - Egalitarian price of fairness
    - Price of welfare-maximization
    - Other results
categories:
    - Fair division
---
In the theory of fair division, the price of fairness (POF) is the ratio of the largest economic welfare attainable by a division to the economic welfare attained by a fair division. The POF is a quantitative measure of the loss of welfare that society has to take in order to guarantee fairness.
