---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/facilitate
offline_file: ""
offline_thumbnail: ""
uuid: 9484a6ce-312e-4331-b3cd-b47b54cab41c
updated: 1484310601
title: facilitate
categories:
    - Dictionary
---
facilitate
     v 1: make easier; "you could facilitate the process by sharing
          your knowledge" [syn: {ease}, {alleviate}]
     2: be of use; "This will help to prevent accidents" [syn: {help}]
     3: physiology: increase the likelihood of (a response); "The
        stimulus facilitates a delayed impulse"
