---
version: 1
type: video
provider: YouTube
id: Ciaml-vREuY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2d5cee87-5b19-4489-a8b3-f4230c698733
updated: 1486069607
title: Repeated addition example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ciaml-vREuY/default.jpg
    - https://i3.ytimg.com/vi/Ciaml-vREuY/1.jpg
    - https://i3.ytimg.com/vi/Ciaml-vREuY/2.jpg
    - https://i3.ytimg.com/vi/Ciaml-vREuY/3.jpg
---
