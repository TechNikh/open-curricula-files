---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suspended
offline_file: ""
offline_thumbnail: ""
uuid: 8cb80246-5113-470b-81a5-99190b97b0a5
updated: 1484310426
title: suspended
categories:
    - Dictionary
---
suspended
     adj 1: (of undissolved particles in a fluid) supported or kept from
            sinking or falling by buoyancy and without apparent
            attachment; "suspended matter such as silt or mud...";
            "dust particles suspended in the air"; "droplets in
            suspension in a gas"
     2: temporarily inactive [syn: {abeyant}, {inactive}, {in
        abeyance(p)}]
