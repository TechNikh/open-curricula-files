---
version: 1
type: video
provider: YouTube
id: 6YqPLgNjR4Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Transcription%20and%20Translation%20Overview.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d2733bec-7aba-48cf-abc0-d9fad1284bec
updated: 1486069673
title: Transcription and Translation Overview
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/6YqPLgNjR4Q/default.jpg
    - https://i3.ytimg.com/vi/6YqPLgNjR4Q/1.jpg
    - https://i3.ytimg.com/vi/6YqPLgNjR4Q/2.jpg
    - https://i3.ytimg.com/vi/6YqPLgNjR4Q/3.jpg
---
