---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infinitely
offline_file: ""
offline_thumbnail: ""
uuid: 00447289-e7d1-4389-b2cf-aac6781bb9f5
updated: 1484310142
title: infinitely
categories:
    - Dictionary
---
infinitely
     adv 1: without bounds; "he is infinitely wealthy" [syn: {boundlessly},
             {immeasurably}]
     2: continuing forever without end; "there are infinitely many
        possibilities" [syn: {endlessly}] [ant: {finitely}]
