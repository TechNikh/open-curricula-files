---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/publish
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484442781
title: publish
categories:
    - Dictionary
---
publish
     v 1: put into print; "The newspaper published the news of the
          royal couple's divorce"; "These news should not be
          printed" [syn: {print}]
     2: prepare and issue for public distribution or sale; "publish
        a magazine or newspaper" [syn: {bring out}, {put out}, {issue},
         {release}]
     3: have (one's written work) issued for publication; "How many
        books did Georges Simenon write?"; "She published 25 books
        during her long career" [syn: {write}]
