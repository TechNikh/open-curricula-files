---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/telegraph
offline_file: ""
offline_thumbnail: ""
uuid: 8df1bed2-d05c-48c6-87ea-a144b0275767
updated: 1484310527
title: telegraph
categories:
    - Dictionary
---
telegraph
     n : apparatus used to communicate at a distance over a wire
         (usually in Morse code) [syn: {telegraphy}]
     v : send cables, wires, or telegrams [syn: {cable}, {wire}]
