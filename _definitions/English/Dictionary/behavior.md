---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/behavior
offline_file: ""
offline_thumbnail: ""
uuid: ca0bf724-262d-4fb1-998b-12e7dd6eefa0
updated: 1484310344
title: behavior
categories:
    - Dictionary
---
behavior
     n 1: manner of acting or conducting yourself [syn: {behaviour}, {conduct},
           {doings}]
     2: the action or reaction of something (as a machine or
        substance) under specified circumstances; "the behavior of
        small particles can be studied in experiments" [syn: {behaviour}]
     3: (behavioral attributes) the way a person behaves toward
        other people [syn: {demeanor}, {demeanour}, {behaviour}, {conduct},
         {deportment}]
     4: (psychology) the aggregate of the responses or reactions or
        movements made by an organism in any situation [syn: {behaviour}]
