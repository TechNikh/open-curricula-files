---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nonintersecting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484353921
title: nonintersecting
categories:
    - Dictionary
---
nonintersecting
     adj : of lines, planes, or surfaces; never meeting or crossing
           [syn: {nonconvergent}]
