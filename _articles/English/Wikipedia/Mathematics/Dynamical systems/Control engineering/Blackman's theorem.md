---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Blackman%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5d3f0f4e-0de4-4d92-a150-9730829aa269
updated: 1484308732
title: "Blackman's theorem"
categories:
    - Control engineering
---
Blackman's theorem is a general procedure for calculating the change in an impedance due to feedback in a circuit. It was published by R.B. Blackman in 1943,[1] was connected to signal-flow analysis by John Choma, and was made popular in the extra element theorem by R. D. Middlebrook and the asymptotic gain model of Solomon Rosenstark.[2][3][4][5] Blackman's approach leads to the formula for the impedance Z between two selected terminals of a negative feedback amplifier as Blackman's formula:
