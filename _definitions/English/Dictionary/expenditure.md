---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expenditure
offline_file: ""
offline_thumbnail: ""
uuid: 49667938-7553-4a8a-880d-111f8b15a7b3
updated: 1484310258
title: expenditure
categories:
    - Dictionary
---
expenditure
     n 1: money paid out [syn: {outgo}, {outlay}] [ant: {income}]
     2: the act of spending money for goods or services [syn: {expending}]
     3: the act of consuming something [syn: {consumption}, {using
        up}]
