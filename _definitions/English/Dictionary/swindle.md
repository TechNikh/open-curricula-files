---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swindle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484566621
title: swindle
categories:
    - Dictionary
---
swindle
     n : the act of swindling by some fraudulent scheme; "that book
         is a fraud" [syn: {cheat}, {rig}]
     v : deprive of by deceit; "He swindled me out of my
         inheritance"; "She defrauded the customers who trusted
         her"; "the cashier gypped me when he gave me too little
         change" [syn: {victimize}, {rook}, {goldbrick}, {nobble},
          {diddle}, {bunco}, {defraud}, {scam}, {mulct}, {gyp}, {con}]
