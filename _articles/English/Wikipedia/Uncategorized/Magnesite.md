---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magnesite
offline_file: ""
offline_thumbnail: ""
uuid: bb8f4945-a2fa-4795-b395-265284902013
updated: 1484308389
title: Magnesite
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Magnesite.jpg
tags:
    - Occurrence
    - Formation
    - Uses
    - Occupational safety and health
    - United States
categories:
    - Uncategorized
---
Magnesite is a mineral with the chemical formula MgCO3 (magnesium carbonate). Mixed crystals of iron(II) carbonate and magnesite (mixed crystals known as ankerite) possess a layered structure: monolayers of carbonate groups alternate with magnesium monolayers as well as iron(II) carbonate monolayers.[5] Manganese, cobalt and nickel may also occur in small amounts.
