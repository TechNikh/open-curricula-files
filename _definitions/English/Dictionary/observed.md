---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/observed
offline_file: ""
offline_thumbnail: ""
uuid: 8b586081-534d-454d-bae7-92d4c557ae43
updated: 1484310343
title: observed
categories:
    - Dictionary
---
observed
     adj : discovered or determined by scientific observation;
           "variation in the ascertained flux depends on a number
           of factors"; "the discovered behavior norms";
           "discovered differences in achievement"; "no
           explanation for the observed phenomena" [syn: {ascertained},
            {discovered}]
