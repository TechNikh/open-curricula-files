---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mingle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484623021
title: mingle
categories:
    - Dictionary
---
mingle
     v 1: to bring or combine together or with something else;
          "resourcefully he mingled music and dance" [syn: {mix},
          {commix}, {unify}, {amalgamate}]
     2: get involved or mixed-up with; "He was about to mingle in an
        unpleasant affair"
     3: be all mixed up or jumbled together; "His words jumbled"
        [syn: {jumble}]
