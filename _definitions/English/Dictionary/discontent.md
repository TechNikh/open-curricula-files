---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discontent
offline_file: ""
offline_thumbnail: ""
uuid: 9f6a7be9-16f6-46bf-b53f-c88e25d3c3f9
updated: 1484310587
title: discontent
categories:
    - Dictionary
---
discontent
     adj : showing or experiencing dissatisfaction or restless longing;
           "saw many discontent faces in the room"; "was
           discontented with his position" [syn: {discontented}]
           [ant: {contented}]
     n : a longing for something better than the present situation
         [syn: {discontentment}, {discontentedness}] [ant: {contentment}]
     v : make dissatisfied [ant: {content}]
