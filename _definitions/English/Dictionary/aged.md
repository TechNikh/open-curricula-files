---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aged
offline_file: ""
offline_thumbnail: ""
uuid: 4a6f8fc2-5201-4750-b9d0-22748c0a4973
updated: 1484310469
title: aged
categories:
    - Dictionary
---
aged
     adj 1: advanced in years; (`aged' is pronounced as two syllables);
            "aged members of the society"; "elderly residents
            could remember the construction of the first
            skyscraper"; "senior citizen" [syn: {elderly}, {older},
             {senior}]
     2: at an advanced stage of erosion (pronounced as one
        syllable); "aged rocks"
     3: having attained a specific age; (`aged' is pronounced as one
        syllable); "aged ten"; "ten years of age" [syn: {aged(a)},
         {of age(p)}]
     4: of wines, fruit, cheeses; having reached a desired or final
        condition; (`aged' pronounced as one syllable); "mature
        well-aged cheeses" ...
