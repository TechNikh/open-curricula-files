---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/patch
offline_file: ""
offline_thumbnail: ""
uuid: 8d680f36-141d-4aef-87e3-b3ee7ce993e1
updated: 1484310480
title: patch
categories:
    - Dictionary
---
patch
     n 1: a small contrasting part of something; "a bald spot"; "a
          leopard's spots"; "a patch of clouds"; "patches of thin
          ice"; "a fleck of red" [syn: {spot}, {speckle}, {dapple},
           {fleck}, {maculation}]
     2: a small area of ground covered by specific vegetation; "a
        bean plot"; "a cabbage patch"; "a briar patch" [syn: {plot},
         {plot of ground}]
     3: a piece of cloth used as decoration or to mend or cover a
        hole
     4: a period of indeterminate length (usually short) marked by
        some action or condition; "he was here for a little
        while"; "I need to rest for a piece"; "a spell of good
        weather"; "a patch ...
