---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/operculum
offline_file: ""
offline_thumbnail: ""
uuid: d521fbfb-e567-45ce-8a7a-d27cbb4f085b
updated: 1484310158
title: operculum
categories:
    - Dictionary
---
operculum
     n : a hard flap serving as a cover for (a) the gill slits in
         fishes or (b) the opening of the shell in certain
         gastropods when the body is retracted
     [also: {opercula} (pl)]
