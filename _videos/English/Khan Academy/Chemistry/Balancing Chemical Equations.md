---
version: 1
type: video
provider: YouTube
id: RnGu3xO2h74
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Balancing%20Chemical%20Equations.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4b8269f6-d83f-4b38-8fbc-b80c252f274d
updated: 1486069587
title: Balancing Chemical Equations
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/RnGu3xO2h74/default.jpg
    - https://i3.ytimg.com/vi/RnGu3xO2h74/1.jpg
    - https://i3.ytimg.com/vi/RnGu3xO2h74/2.jpg
    - https://i3.ytimg.com/vi/RnGu3xO2h74/3.jpg
---
The art of balancing equations in chemistry!
More free lessons at: http://www.khanacademy.org/video?v=RnGu3xO2h74
