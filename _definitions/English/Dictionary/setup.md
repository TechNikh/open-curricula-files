---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/setup
offline_file: ""
offline_thumbnail: ""
uuid: f64093c9-2ced-4673-9169-413dac428c53
updated: 1484310206
title: setup
categories:
    - Dictionary
---
set up
     adj : well established and ready to function; "things I can do now
           that I'm set up"
     v 1: set up or found; "She set up a literacy program" [syn: {establish},
           {found}, {launch}] [ant: {abolish}]
     2: make by putting pieces together; "She pieced a quilt"; "He
        tacked together some verses" [syn: {assemble}, {piece}, {put
        together}, {tack}, {tack together}] [ant: {disassemble}]
     3: construct, build, or erect; "Raise a barn" [syn: {raise}, {erect},
         {rear}, {put up}] [ant: {level}]
     4: get ready for a particular purpose or event; "set up an
        experiment"; "set the table"; "lay out the tools for the
        surgery" ...
