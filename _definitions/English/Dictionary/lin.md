---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lin
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484377081
title: lin
categories:
    - Dictionary
---
Lin
     n : United States sculptor and architect whose public works
         include the memorial to veterans of the Vietnam War in
         Washington (born in 1959) [syn: {Maya Lin}]
