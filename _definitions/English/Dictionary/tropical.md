---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tropical
offline_file: ""
offline_thumbnail: ""
uuid: 5dcb709c-3ad5-4f9b-ab65-1e3942f05f5a
updated: 1484310275
title: tropical
categories:
    - Dictionary
---
tropical
     adj 1: relating to or situated in or characteristic of the tropics
            (the region on either side of the equator); "tropical
            islands"; "tropical fruit" [syn: {tropic}]
     2: of or relating to the tropics, or either tropic; "tropical
        year"
     3: characterized by or of the nature of a trope or tropes;
        changed from its literal sense
     4: of weather or climate; hot and humid as in the tropics;
        "tropical weather" [syn: {tropic}]
