---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appoint
offline_file: ""
offline_thumbnail: ""
uuid: 6bab555f-05ff-463b-a563-1ad837c05ef1
updated: 1484310451
title: appoint
categories:
    - Dictionary
---
appoint
     v 1: create and charge with a task or function; "nominate a
          committee" [syn: {name}, {nominate}, {constitute}]
     2: assign a duty, responsibility or obligation to; "He was
        appointed deputy manager"; "She was charged with
        supervising the creation of a concordance" [syn: {charge}]
     3: furnish; "a beautifully appointed house"
