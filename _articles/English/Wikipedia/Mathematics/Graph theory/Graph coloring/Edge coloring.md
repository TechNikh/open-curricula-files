---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edge_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 221383e2-6b6f-41d0-a4d4-fff28436190a
updated: 1484309376
title: Edge coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Desargues_graph_3color_edge.svg.png
tags:
    - Examples
    - Definitions
    - Relation to matching
    - Relation to degree
    - "Vizing's theorem"
    - Regular graphs
    - Multigraphs
    - Algorithms
    - Optimally coloring special classes of graphs
    - Algorithms that use more than the optimal number of colors
    - Exact algorithms
    - Additional properties
    - Other types of edge coloring
    - Applications
    - Open problems
    - Notes
categories:
    - Graph coloring
---
In graph theory, an edge coloring of a graph is an assignment of "colors" to the edges of the graph so that no two adjacent edges have the same color. For example, the figure to the right shows an edge coloring of a graph by the colors red, blue, and green. Edge colorings are one of several different types of graph coloring. The edge-coloring problem asks whether it is possible to color the edges of a given graph using at most k different colors, for a given value of k, or with the fewest possible colors. The minimum required number of colors for the edges of a given graph is called the chromatic index of the graph. For example, the edges of the graph in the illustration can be colored by ...
