---
version: 1
type: video
provider: YouTube
id: gyHp4wo8dOc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 456da7f6-5866-42f4-b2fa-b7e50411ff8f
updated: 1486069609
title: Rewriting a quadratic function to find roots and vertex
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/gyHp4wo8dOc/default.jpg
    - https://i3.ytimg.com/vi/gyHp4wo8dOc/1.jpg
    - https://i3.ytimg.com/vi/gyHp4wo8dOc/2.jpg
    - https://i3.ytimg.com/vi/gyHp4wo8dOc/3.jpg
---
