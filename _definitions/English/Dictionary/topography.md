---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/topography
offline_file: ""
offline_thumbnail: ""
uuid: e48830f4-5564-4894-a1e6-3d582840915e
updated: 1484310437
title: topography
categories:
    - Dictionary
---
topography
     n 1: the configuration of a surface and the relations among its
          man-made and natural features
     2: precise detailed study of the surface features of a region
