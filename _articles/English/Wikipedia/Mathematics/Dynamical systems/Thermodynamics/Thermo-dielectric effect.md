---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermo-dielectric_effect
offline_file: ""
offline_thumbnail: ""
uuid: 9155ee00-baab-4dad-aed1-6198f12e771d
updated: 1484308872
title: Thermo-dielectric effect
categories:
    - Thermodynamics
---
This interesting effect was discovered by Joaquim da Costa Ribeiro in 1944. The Brazilian physicist observed that solidification and melting of many dielectrics are accompanied by charge separation. A thermo-dielectric effect was demonstrated with carnauba wax, naphthalene and paraffin. Charge separation in ice was also expected. This effect was observed during water freezing period, electrical storm effects can be caused by this strange phenomenon. Effect was measured by many researches - Bernhard Gross, Armando Dias Tavares, Sergio Mascarenhas etc. César Lattes (co-discoverer of the pion) supposed that this was the only effect ever to be discovered entirely in Brasil.
