---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/customary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484576521
title: customary
categories:
    - Dictionary
---
customary
     adj 1: in accordance with convention or custom; "sealed the deal
            with the customary handshake"
     2: commonly used or practiced; usual; "his accustomed
        thoroughness"; "took his customary morning walk"; "his
        habitual comment"; "with her wonted candor" [syn: {accustomed},
         {habitual}, {wonted(a)}]
