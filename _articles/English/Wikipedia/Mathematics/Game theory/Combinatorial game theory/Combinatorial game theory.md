---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorial_game_theory
offline_file: ""
offline_thumbnail: ""
uuid: 24bf3ab4-51ba-4bd9-93a6-85db094e5b3a
updated: 1484309022
title: Combinatorial game theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mathematicians_playing_Konane.jpg
tags:
    - History
    - Examples
    - Overview
    - Game abbreviations
    - Numbers
    - Star
    - Up
    - Down
    - "Hot" games
    - Nimbers
    - Notes
categories:
    - Combinatorial game theory
---
Combinatorial game theory (CGT) is a branch of mathematics and theoretical computer science that typically studies sequential games with perfect information. Study has been largely confined to two-player games that have a position in which the players take turns changing in defined ways or moves to achieve a defined winning condition. CGT has not traditionally studied games of chance or those that use imperfect or incomplete information, favoring games that offer perfect information in which the state of the game and the set of available moves is always known by both players.[1] However, as mathematical techniques advance, the types of game that can be mathematically analyzed expands, thus ...
