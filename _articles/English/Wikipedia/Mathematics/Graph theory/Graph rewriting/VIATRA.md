---
version: 1
type: article
id: https://en.wikipedia.org/wiki/VIATRA
offline_file: ""
offline_thumbnail: ""
uuid: 60e3706d-1e4c-4b6a-80ce-794a841c6c88
updated: 1484309452
title: VIATRA
tags:
    - Target application domains
    - Approach
    - Conformance to related standards
    - Target audience and end users
categories:
    - Graph rewriting
---
The VIATRA (VIsual Automated model TRAnsformations) framework is the core of a transformation-based verification and validation environment for improving the quality of systems designed using the Unified Modeling Language by automatically checking consistency, completeness, and dependability requirements.
