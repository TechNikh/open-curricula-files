---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distinct
offline_file: ""
offline_thumbnail: ""
uuid: c4730c55-ccb3-41b9-90c2-0d30a8ec4964
updated: 1484310563
title: distinct
categories:
    - Dictionary
---
distinct
     adj 1: easy to perceive; especially clearly outlined; "a distinct
            flavor"; "a distinct odor of turpentine"; "a distinct
            outline"; "the ship appeared as a distinct
            silhouette"; "distinct fingerprints" [ant: {indistinct}]
     2: (often followed by `from') not alike; different in nature or
        quality; "plants of several distinct types"; "the word
        `nationalism' is used in at least two distinct senses";
        "gold is distinct from iron"; "a tree related to but quite
        distinct from the European beech"; "management had
        interests quite distinct from those of their employees"
        [syn: {distinguishable}]
     3: ...
