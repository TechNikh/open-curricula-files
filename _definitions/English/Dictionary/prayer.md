---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prayer
offline_file: ""
offline_thumbnail: ""
uuid: a626926a-b289-4d21-ba47-28c8c0a44363
updated: 1484310589
title: prayer
categories:
    - Dictionary
---
prayer
     n 1: the act of communicating with a deity (especially as a
          petition or in adoration or contrition or thanksgiving);
          "the priest sank to his knees in prayer" [syn: {supplication}]
     2: reverent petition to a deity [syn: {petition}, {orison}]
     3: earnest or urgent request; "an entreaty to stop the
        fighting"; "an appeal for help"; "an appeal to the public
        to keep calm" [syn: {entreaty}, {appeal}]
     4: a fixed text used in praying
     5: someone who prays to God [syn: {supplicant}]
