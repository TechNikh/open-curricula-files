---
version: 1
type: video
provider: YouTube
id: Ka82QC4QvGA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Perimeter%2C%20area%2C%20and%20volume/Quadrilateral%20overview.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7f0d8859-214e-4668-be23-0035410ae6c0
updated: 1486069577
title: Quadrilateral overview
categories:
    - Perimeter, area, and volume
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ka82QC4QvGA/default.jpg
    - https://i3.ytimg.com/vi/Ka82QC4QvGA/1.jpg
    - https://i3.ytimg.com/vi/Ka82QC4QvGA/2.jpg
    - https://i3.ytimg.com/vi/Ka82QC4QvGA/3.jpg
---
"Quad" means "four" and "lateral" means "line." A quadrilateral is literally a shape with four sides (lines) and four angles. Let's learn the difference between concave and convex quadrilaterals as well as trapezoids, parallelograms, and rhombi.

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/geometry/basic-geometry/area_non_standard/e/area_of_parallelograms?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Watch the next lesson: https://www.khanacademy.org/math/geometry/basic-geometry/area_non_standard/v/quadrilateral-properties?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Missed the previous lesson? 
https://www.khanacademy.org/math/geometry/basic-geometry/circum_area_circles/v/area-of-a-circle?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Geometry on Khan Academy: We are surrounded by space. And that space contains lots of things. And these things have shapes. In geometry we are concerned with the nature of these shapes, how we define them, and what they teach us about the world at large--from math to architecture to biology to astronomy (and everything in between). Learning geometry is about more than just taking your medicine ("It's good for you!"), it's at the core of everything that exists--including you. Having said all that, some of the specific topics we'll cover include angles, intersecting lines, right triangles, perimeter, area, volume, circles, triangles, quadrilaterals, analytic geometry, and geometric constructions. Wow. That's a lot. To summarize: it's difficult to imagine any area of math that is more widely used than geometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Geometry channel:
https://www.youtube.com/channel/UCD3OtKxPRUFw8kzYlhJXa1Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
