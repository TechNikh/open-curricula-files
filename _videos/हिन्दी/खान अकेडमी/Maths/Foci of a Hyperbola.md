---
version: 1
type: video
provider: YouTube
id: P3K6dHYZfTw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2210b5ad-ee7e-4c5b-95d7-ed59dfcc3a7d
updated: 1486069616
title: Foci of a Hyperbola
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/P3K6dHYZfTw/default.jpg
    - https://i3.ytimg.com/vi/P3K6dHYZfTw/1.jpg
    - https://i3.ytimg.com/vi/P3K6dHYZfTw/2.jpg
    - https://i3.ytimg.com/vi/P3K6dHYZfTw/3.jpg
---
