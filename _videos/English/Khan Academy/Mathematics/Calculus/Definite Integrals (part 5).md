---
version: 1
type: video
provider: YouTube
id: CmXmRNFrtFw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Definite%20Integrals%20%28part%205%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 834caa5f-a2f1-4094-bf18-7c9589c203e7
updated: 1486069696
title: Definite Integrals (part 5)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/CmXmRNFrtFw/default.jpg
    - https://i3.ytimg.com/vi/CmXmRNFrtFw/1.jpg
    - https://i3.ytimg.com/vi/CmXmRNFrtFw/2.jpg
    - https://i3.ytimg.com/vi/CmXmRNFrtFw/3.jpg
---
