---
version: 1
type: video
provider: YouTube
id: JSfskTCUKr4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 13e462a7-8d30-446a-9436-d04ab872696f
updated: 1486069621
title: Midpoint formula
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/JSfskTCUKr4/default.jpg
    - https://i3.ytimg.com/vi/JSfskTCUKr4/1.jpg
    - https://i3.ytimg.com/vi/JSfskTCUKr4/2.jpg
    - https://i3.ytimg.com/vi/JSfskTCUKr4/3.jpg
---
