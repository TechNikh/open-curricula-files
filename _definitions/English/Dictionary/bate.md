---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bate
offline_file: ""
offline_thumbnail: ""
uuid: 7388e897-79e2-4643-abbb-d6e06e9cceab
updated: 1484310541
title: bate
categories:
    - Dictionary
---
bate
     v 1: moderate or restrain; lessen the force of; "He bated his
          breath when talking about this affair"; "capable of
          bating his enthusiasm"
     2: flap the wings wildly or frantically; used of falcons
     3: soak in a special solution to soften and remove chemicals
        used in previous treatments; "bate hides and skins"
