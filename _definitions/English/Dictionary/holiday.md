---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/holiday
offline_file: ""
offline_thumbnail: ""
uuid: 459e8132-707a-40aa-ad40-2cd1d8cafb1b
updated: 1484310236
title: holiday
categories:
    - Dictionary
---
holiday
     n 1: leisure time away from work devoted to rest or pleasure; "we
          get two weeks of vacation every summer"; "we took a
          short holiday in Puerto Rico" [syn: {vacation}]
     2: a day on which work is suspended by law or custom; "no mail
        is delivered on federal holidays"; "it's a good thing that
        New Year's was a holiday because everyone had a hangover"
     v : spend or take a vacation [syn: {vacation}]
