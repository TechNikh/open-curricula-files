---
version: 1
type: video
provider: YouTube
id: Vh3-NHdjnyQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 49c2a819-c864-43b1-a301-17fb94859e8c
updated: 1486069672
title: 'NOVA scienceNOW - 8 - RNAi'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Vh3-NHdjnyQ/default.jpg
    - https://i3.ytimg.com/vi/Vh3-NHdjnyQ/1.jpg
    - https://i3.ytimg.com/vi/Vh3-NHdjnyQ/2.jpg
    - https://i3.ytimg.com/vi/Vh3-NHdjnyQ/3.jpg
---
