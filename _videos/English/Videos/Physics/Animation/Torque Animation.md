---
version: 1
type: video
provider: YouTube
id: bPLs5TbT7Bs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Torque%20Animation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f5d8ce22-e1b4-4c69-81cb-ed9fcb902e67
updated: 1486069654
title: Torque Animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/bPLs5TbT7Bs/default.jpg
    - https://i3.ytimg.com/vi/bPLs5TbT7Bs/1.jpg
    - https://i3.ytimg.com/vi/bPLs5TbT7Bs/2.jpg
    - https://i3.ytimg.com/vi/bPLs5TbT7Bs/3.jpg
---
