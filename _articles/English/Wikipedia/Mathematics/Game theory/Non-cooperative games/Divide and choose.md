---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Divide_and_choose
offline_file: ""
offline_thumbnail: ""
uuid: 15cfa42c-bcb1-4f17-b90b-9451a5202cec
updated: 1484309050
title: Divide and choose
tags:
    - History
    - Analysis
    - Efficiency issues
    - Alternatives
    - Notes and references
categories:
    - Non-cooperative games
---
Divide and choose (also Cut and choose or I cut, you choose) is a procedure for envy-free cake-cutting between two partners. It involves a heterogeneous good or resource ("the cake") and two partners which have different preferences over parts of the cake. The protocol proceeds as follows: one person ("the cutter") cuts the cake to two pieces; the other person ("the chooser") chooses one of the pieces; the cutter receives the remaining piece.
