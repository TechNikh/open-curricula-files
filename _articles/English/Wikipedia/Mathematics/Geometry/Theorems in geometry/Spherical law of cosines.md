---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spherical_law_of_cosines
offline_file: ""
offline_thumbnail: ""
uuid: b491b374-949f-4e74-9be7-b195c195723b
updated: 1484309123
title: Spherical law of cosines
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Law-of-haversines.svg.png
tags:
    - Proof
    - Proof without vectors
    - 'Planar limit: small angles'
    - Notes
categories:
    - Theorems in geometry
---
In spherical trigonometry, the law of cosines (also called the cosine rule for sides[1]) is a theorem relating the sides and angles of spherical triangles, analogous to the ordinary law of cosines from plane trigonometry.
