---
version: 1
type: video
provider: YouTube
id: cdaJNNSOvbY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cd09293b-f4a4-430c-8213-a7a403c8a256
updated: 1486069634
title: 'Polarization - Science experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/cdaJNNSOvbY/default.jpg
    - https://i3.ytimg.com/vi/cdaJNNSOvbY/1.jpg
    - https://i3.ytimg.com/vi/cdaJNNSOvbY/2.jpg
    - https://i3.ytimg.com/vi/cdaJNNSOvbY/3.jpg
---
