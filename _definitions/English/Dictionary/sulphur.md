---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sulphur
offline_file: ""
offline_thumbnail: ""
uuid: 3217b5ec-85b7-4164-9061-9a338114819a
updated: 1484310387
title: sulphur
categories:
    - Dictionary
---
sulphur
     n : an abundant tasteless odorless multivalent nonmetallic
         element; best known in yellow crystals; occurs in many
         sulphide and sulphate minerals and even in native form
         (especially in volcanic regions) [syn: {sulfur}, {S}, {atomic
         number 16}]
     v : treat with sulphur in order to preserve; "These dried fruits
         are sulphured" [syn: {sulfur}]
