---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/underground
offline_file: ""
offline_thumbnail: ""
uuid: 750710c6-89f8-413f-b2a7-770df8e2a8f5
updated: 1484310258
title: underground
categories:
    - Dictionary
---
underground
     adj 1: under the level of the ground; "belowground storage areas";
            "underground caverns" [syn: {belowground}]
     2: conducted with or marked by hidden aims or methods;
        "clandestine intelligence operations"; "cloak-and-dagger
        activities behind enemy lines"; "hole-and-corner
        intrigue"; "secret missions"; "a secret agent"; "secret
        sales of arms"; "surreptitious mobilization of troops";
        "an undercover investigation"; "underground resistance"
        [syn: {clandestine}, {cloak-and-dagger}, {hole-and-corner(a)},
         {hugger-mugger}, {hush-hush}, {on the quiet(p)}, {secret},
         {surreptitious}, {undercover}]
     3: ...
