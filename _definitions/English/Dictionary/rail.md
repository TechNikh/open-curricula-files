---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rail
offline_file: ""
offline_thumbnail: ""
uuid: ad6f78e7-7bd6-44e3-8616-4fd0ee69eeef
updated: 1484310571
title: rail
categories:
    - Dictionary
---
rail
     n 1: a barrier consisting of a horizontal bar and supports [syn:
          {railing}]
     2: short for railway; "he traveled by rail"; "he was concerned
        with rail safety"
     3: a bar or bars of rolled steel making a track along which
        vehicles can roll [syn: {track}, {rails}]
     4: a horizontal bar (usually of wood)
     5: any of numerous widely distributed small wading birds of the
        family Rallidae having short wings and very long toes for
        running on soft mud
     v 1: complain bitterly [syn: {inveigh}]
     2: enclose with rails; "rail in the old graves" [syn: {rail in}]
     3: provide with rails; "The yard was railed"
     4: separate with a ...
