---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/capability
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484605381
title: capability
categories:
    - Dictionary
---
capability
     n 1: the quality of being capable -- physically or intellectually
          or legally; "he worked to the limits of his capability"
          [syn: {capableness}] [ant: {incapability}, {incapability}]
     2: the susceptibility of something to a particular treatment;
        "the capability of a metal to be fused" [syn: {capacity}]
     3: an aptitude that may be developed [syn: {capableness}, {potentiality}]
        [ant: {incapability}]
