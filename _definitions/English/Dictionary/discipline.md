---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discipline
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484627941
title: discipline
categories:
    - Dictionary
---
discipline
     n 1: a branch of knowledge; "in what discipline is his
          doctorate?"; "teachers should be well trained in their
          subject"; "anthropology is the study of human beings"
          [syn: {subject}, {subject area}, {subject field}, {field},
           {field of study}, {study}, {bailiwick}, {branch of
          knowledge}]
     2: a system of rules of conduct or method of practice; "he
        quickly learned the discipline of prison routine" or "for
        such a plan to work requires discipline";
     3: the trait of being well behaved; "he insisted on discipline
        among the troops" [ant: {indiscipline}]
     4: training to improve strength or ...
