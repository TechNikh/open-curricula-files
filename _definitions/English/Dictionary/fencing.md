---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fencing
offline_file: ""
offline_thumbnail: ""
uuid: 73394ac9-9605-4774-9727-f6ebee3b26d8
updated: 1484310174
title: fencing
categories:
    - Dictionary
---
fencing
     n 1: a barrier that serves to enclose an area [syn: {fence}]
     2: material for building fences [syn: {fencing material}]
     3: the art or sport of fighting with swords (especially the use
        of foils or epees or sabres to score points under a set of
        rules)
