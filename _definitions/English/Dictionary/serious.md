---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/serious
offline_file: ""
offline_thumbnail: ""
uuid: 542aa421-85c2-45bd-96c1-73b18ea35aae
updated: 1484310200
title: serious
categories:
    - Dictionary
---
serious
     adj 1: concerned with work or important matters rather than play or
            trivialities; "a serious student of history"; "a
            serious attempt to learn to ski"; "gave me a serious
            look"; "a serious young man"; "are you serious or
            joking?"; "Don't be so serious!" [ant: {frivolous}]
     2: of great consequence; "marriage is a serious matter"
     3: causing fear or anxiety by threatening great harm; "a
        dangerous operation"; "a grave situation"; "a grave
        illness"; "grievous bodily harm"; "a serious wound"; "a
        serious turn of events"; "a severe case of pneumonia"; "a
        life-threatening disease" [syn: {dangerous}, ...
