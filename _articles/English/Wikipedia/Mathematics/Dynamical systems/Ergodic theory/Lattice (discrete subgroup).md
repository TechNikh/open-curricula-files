---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lattice_(discrete_subgroup)
offline_file: ""
offline_thumbnail: ""
uuid: 4696816a-d4aa-48ff-84a0-bdf5274d15d0
updated: 1484308782
title: Lattice (discrete subgroup)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-HeisenbergCayleyGraph.png
tags:
    - Generalities on lattices
    - Informal discussion
    - Definition
    - First examples
    - Which groups have lattices?
    - Lattices in solvable Lie groups
    - Nilpotent Lie groups
    - The general case
    - Lattices in semisimple Lie groups
    - Arithmetic groups and existence of lattices
    - Irreducibility
    - Rank 1 versus higher rank
    - "Kazhdan's property (T)"
    - Finiteness properties
    - Riemannian manifolds associated to lattices in Lie groups
    - Left-invariant metrics
    - Locally symmetric spaces
    - Lattices in p-adic Lie groups
    - S-arithmetic groups
    - Lattices in adelic groups
    - Rigidity
    - Rigidity results
    - Nonrigidity in low dimensions
    - Tree lattices
    - Definition
    - Tree lattices from algebraic groups
    - Tree lattices from Bass-Serre theory
    - Existence criterion
    - Notes
categories:
    - Ergodic theory
---
In Lie theory and related areas of mathematics, a lattice in a locally compact group is a discrete subgroup with the property that the quotient space has finite invariant measure. In the special case of subgroups of Rn, this amounts to the usual geometric notion of a lattice as a periodic subset of points, and both the algebraic structure of lattices and the geometry of the space of all lattices are relatively well understood.
