---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geographically
offline_file: ""
offline_thumbnail: ""
uuid: a79384ab-3886-4625-9156-09d3e0821f1f
updated: 1484310551
title: geographically
categories:
    - Dictionary
---
geographically
     adv : with respect to geography; "they are geographically closer
           to the center of town"
