---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gay
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484452921
title: gay
categories:
    - Dictionary
---
gay
     adj 1: bright and pleasant; promoting a feeling of cheer; "a cheery
            hello"; "a gay sunny room"; "a sunny smile" [syn: {cheery},
             {sunny}]
     2: full of or showing high-spirited merriment; "when hearts
        were young and gay"; "a poet could not but be gay, in such
        a jocund company"- Wordsworth; "the jolly crowd at the
        reunion"; "jolly old Saint Nick"; "a jovial old
        gentleman"; "have a merry Christmas"; "peals of merry
        laughter"; "a mirthful laugh" [syn: {jocund}, {jolly}, {jovial},
         {merry}, {mirthful}]
     3: given to social pleasures often including dissipation; "led
        a gay Bohemian life"; "a gay old ...
