---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rigid
offline_file: ""
offline_thumbnail: ""
uuid: 25315e67-785a-4e2e-8beb-fd04326743a0
updated: 1484310271
title: rigid
categories:
    - Dictionary
---
rigid
     adj 1: incapable of or resistant to bending; "a rigid strip of
            metal"; "a table made of rigid plastic"; "a palace
            guardsman stiff as a poker" [syn: {stiff}]
     2: incapable of compromise or flexibility [syn: {strict}]
     3: incapable of adapting or changing to meet circumstances; "a
        rigid disciplinarian"; "an inflexible law"; "an unbending
        will to dominate" [syn: {inflexible}, {unbending}]
     4: fixed and unmoving; "with eyes set in a fixed glassy stare";
        "his bearded face already has a set hollow look"- Connor
        Cruise O'Brien; "a face rigid with pain" [syn: {fixed}, {set}]
     5: designating an airship or dirigible ...
