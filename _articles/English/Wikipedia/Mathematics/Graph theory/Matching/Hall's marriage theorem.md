---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hall%27s_marriage_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 9cb38f17-3912-4c6b-9b63-053b0c478f83
updated: 1484309511
title: "Hall's marriage theorem"
tags:
    - Combinatorial formulation
    - Examples
    - Application to marriage
    - Graph theoretic formulation
    - Proof of the graph theoretic version
    - >
        Equivalence of the combinatorial formulation and the
        graph-theoretic formulation
    - Marshall Hall Jr. variant
    - Applications
    - Marriage condition does not extend
    - Logical equivalences
    - Notes
categories:
    - Matching
---
