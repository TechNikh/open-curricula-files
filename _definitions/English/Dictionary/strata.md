---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strata
offline_file: ""
offline_thumbnail: ""
uuid: 08831196-21b2-4758-9673-b96fe8899507
updated: 1484310462
title: strata
categories:
    - Dictionary
---
stratum
     n 1: one of several parallel layers of material arranged one on
          top of another (such as a layer of tissue or cells in an
          organism)
     2: an abstract place usually conceived as having depth; "a good
        actor communicates on several levels"; "a simile has at
        least two layers of meaning"; "the mind functions on many
        strata simultaneously" [syn: {level}, {layer}]
     [also: {strata} (pl)]
