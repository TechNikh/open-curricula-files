---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gage
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484503741
title: gage
categories:
    - Dictionary
---
gage
     n 1: street names for marijuana [syn: {pot}, {grass}, {green
          goddess}, {dope}, {weed}, {sess}, {sens}, {smoke}, {skunk},
           {locoweed}, {Mary Jane}]
     2: a measuring instrument for measuring and indicating a
        quantity such as the thickness of wire or the amount of
        rain etc. [syn: {gauge}]
     v : place a bet on; "Which horse are you backing?"; "I'm betting
         on the new horse" [syn: {bet on}, {back}, {stake}, {game},
          {punt}]
