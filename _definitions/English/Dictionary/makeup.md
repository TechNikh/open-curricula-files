---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/makeup
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484540101
title: makeup
categories:
    - Dictionary
---
make up
     v 1: form or compose; "This money is my only income"; "The stone
          wall was the backdrop for the performance"; "These
          constitute my entire belonging"; "The children made up
          the chorus"; "This sum represents my entire income for a
          year"; "These few men comprise his entire army" [syn: {constitute},
           {represent}, {comprise}, {be}]
     2: devise or compose; "This designer makes up our Spring
        collections"; "He designed women's shoes"
     3: do or give something to somebody in return; "Does she pay
        you for the work you are doing?" [syn: {pay}, {pay off}, {compensate}]
     4: make up work that was missed due to absence ...
