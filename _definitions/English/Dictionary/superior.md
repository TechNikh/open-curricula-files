---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/superior
offline_file: ""
offline_thumbnail: ""
uuid: cccfbbfd-e4e7-454b-b9f7-da2595b8bbe8
updated: 1484310154
title: superior
categories:
    - Dictionary
---
superior
     adj 1: of high or superior quality or performance; "superior wisdom
            derived from experience"; "superior math students"
            [ant: {inferior}]
     2: of or characteristic of high rank or importance; "a superior
        officer" [ant: {inferior}]
     3: (sometimes followed by `to') not subject to or influenced
        by; "overcome by a superior opponent"; "trust magnates who
        felt themselves superior to law"
     4: written or printed above and to one side of another
        character [syn: {superscript}] [ant: {subscript}, {adscript}]
     5: having an orbit farther from the sun than the Earth's orbit;
        "Mars and Jupiter are the closest in of ...
