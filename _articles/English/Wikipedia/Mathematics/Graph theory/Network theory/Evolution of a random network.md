---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Evolution_of_a_random_network
offline_file: ""
offline_thumbnail: ""
uuid: 90b8e54d-4e2e-4440-8793-c4e2890414a5
updated: 1484309432
title: Evolution of a random network
tags:
    - Conditions for emergence of a giant component
    - Regimes of evolution of a random network
    - Subcritical regime
    - Critical point
    - Supercritical regime
    - Connected Regime
    - Examples for occurrences in nature
    - Water-ice transition
    - Magnetic phase transition
    - Applications
    - Physics and chemistry
    - Biology and medicine
    - Network science, statistics and machine learning
categories:
    - Network theory
---
Evolution of a random network is a dynamical process, usually leading to emergence of giant component accompanied with striking consequences on the network topology. To quantify this process, there is a need of inspection on how the size of the largest connected cluster within the network, 
  
    
      
        
          
            N
            
              G
            
          
        
      
    
    {\displaystyle {N_{G}}}
  
, varies with 
  
    
      
        
          
            ⟨
            k
            ⟩
          
        
      
    
    {\displaystyle {\left\langle k\right\rangle }}
  
.[1] Networks change their topology as they evolve, undergoing phase ...
