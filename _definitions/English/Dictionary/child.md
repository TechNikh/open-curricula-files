---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/child
offline_file: ""
offline_thumbnail: ""
uuid: 78114051-acca-4050-86c4-01ab47c484e1
updated: 1484310295
title: child
categories:
    - Dictionary
---
child
     n 1: a young person of either sex; "she writes books for
          children"; "they're just kids"; "`tiddler' is a British
          term for youngsters" [syn: {kid}, {youngster}, {minor},
          {shaver}, {nipper}, {small fry}, {tiddler}, {tike}, {tyke},
           {fry}, {nestling}]
     2: a human offspring (son or daughter) of any age; "they had
        three children"; "they were able to send their kids to
        college" [syn: {kid}] [ant: {parent}]
     3: an immature childish person; "he remained a child in
        practical matters as long as he lived"; "stop being a
        baby!" [syn: {baby}]
     4: a member of a clan or tribe; "the children of Israel"
     ...
