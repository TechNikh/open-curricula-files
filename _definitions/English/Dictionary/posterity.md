---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/posterity
offline_file: ""
offline_thumbnail: ""
uuid: ca041b1c-bbf7-48ab-9922-63b30e7a1938
updated: 1484310591
title: posterity
categories:
    - Dictionary
---
posterity
     n 1: all of the offspring of a given progenitor; "we must secure
          the benefits of freedom for ourselves and our posterity"
          [syn: {descendants}]
     2: all future generations
