---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kotli
offline_file: ""
offline_thumbnail: ""
uuid: c942444a-aa99-45c0-850e-bd4d9c3920d5
updated: 1484308419
title: Kotli
tags:
    - History
    - Indo-Pakistan War of 1947–48
    - Modern Kotli
    - Communications
categories:
    - Uncategorized
---
Kotli (Urdu: کوٹلی‎) or Cotly, as known in Britain, is the chief town of Kotli District, in the Azad Kashmir. Kotli is linked with Mirpur by two metalled roads, one via Rajdhani, (90 km) and the other via Charhoi. It is also directly linked with Rawalakot via Trarkhal (82 km) and a double road which links Kotli with the rest of Pakistan via Sehnsa, another major town in Azad Kashmir. Kotli is roughly a three hours drive from Islamabad and Rawalpindi, at a distance of 117 km via Sehnsa.
