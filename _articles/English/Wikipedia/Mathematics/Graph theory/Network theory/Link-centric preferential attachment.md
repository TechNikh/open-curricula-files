---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Link-centric_preferential_attachment
offline_file: ""
offline_thumbnail: ""
uuid: c29b6e26-ec14-4124-a9ff-b4c9c727b73f
updated: 1484309435
title: Link-centric preferential attachment
tags:
    - Background
    - Examples
categories:
    - Network theory
---
In mathematical modeling of social networks, link-centric preferential attachment[1][2] describes a node's propensity to re-establish links to nodes it has previously been in contact with in time-varying networks.[3] This preferential attachment model relies on nodes keeping memory of previous neighbors up to the current time.[1][4]
