---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/skin
offline_file: ""
offline_thumbnail: ""
uuid: c02843a2-dbff-4503-b522-131bdf01398d
updated: 1484310321
title: skin
categories:
    - Dictionary
---
skin
     n 1: a natural protective covering of the body; site of the sense
          of touch; "your skin is the largest organ of your body"
          [syn: {tegument}, {cutis}]
     2: the tissue forming the hard outer layer (of e.g. a fruit)
        [syn: {rind}, {peel}]
     3: an outer surface (usually thin); "the skin of an airplane"
     4: body covering of a living animal [syn: {hide}, {pelt}]
     5: a person's skin regarded as their life; "he tried to save
        his skin"
     6: the rind of a fruit or vegetable [syn: {peel}]
     7: a bag serving as a container for liquids; it is made from
        the skin of an animal
     v 1: climb awkwardly, as if by scrambling [syn: ...
