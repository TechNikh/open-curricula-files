---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diagram
offline_file: ""
offline_thumbnail: ""
uuid: f49ed5ec-045c-443b-bf20-06a9031c0dd5
updated: 1484310335
title: diagram
categories:
    - Dictionary
---
diagram
     n : a drawing intended to explain how something works; a drawing
         showing the relation between the parts
     v : make a schematic or technical drawing of that shows how
         things work or how they are constructed [syn: {plot}]
     [also: {diagramming}, {diagrammed}]
