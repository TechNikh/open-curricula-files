---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nd
offline_file: ""
offline_thumbnail: ""
uuid: a1eb628b-c005-475c-9b9f-60624732808c
updated: 1484310305
title: nd
categories:
    - Dictionary
---
Nd
     n 1: a yellow trivalent metallic element of the rare earth group;
          occurs in monazite and bastnasite in association with
          cerium and lanthanum and praseodymium [syn: {neodymium},
           {atomic number 60}]
     2: a state of north central United States bordering on Canada
        [syn: {North Dakota}, {Peace Garden State}]
