---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_connection
offline_file: ""
offline_thumbnail: ""
uuid: aa0e83a9-6486-4c1f-9e6f-f8307a182839
updated: 1484309202
title: Affine connection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Parallel_transport_sphere.svg.png
tags:
    - Motivation and history
    - Motivation from surface theory
    - Motivation from tensor calculus
    - Approaches
    - Formal definition as a differential operator
    - Elementary properties
    - Parallel transport for affine connections
    - Formal definition on the frame bundle
    - Affine connections as Cartan connections
    - Explanations and historical intuition
    - Affine space as the flat model geometry
    - Definition of an affine space
    - Affine frames and the flat affine connection
    - 'General affine geometries: formal definitions'
    - Definition via absolute parallelism
    - Definition as a principal affine connection
    - Relation to the motivation
    - Further properties
    - Curvature and torsion
    - The Levi-Civita connection
    - Geodesics
    - Development
    - Surface theory revisited
    - 'Example: the unit sphere in Euclidean space'
    - Notes
    - Primary historical references
    - Secondary references
categories:
    - Differential geometry
---
In the branch of mathematics called differential geometry, an affine connection is a geometric object on a smooth manifold which connects nearby tangent spaces, and so permits tangent vector fields to be differentiated as if they were functions on the manifold with values in a fixed vector space. The notion of an affine connection has its roots in 19th-century geometry and tensor calculus, but was not fully developed until the early 1920s, by Élie Cartan (as part of his general theory of connections) and Hermann Weyl (who used the notion as a part of his foundations for general relativity). The terminology is due to Cartan and has its origins in the identification of tangent spaces in ...
