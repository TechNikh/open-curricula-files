---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_recreational_number_theory_topics
offline_file: ""
offline_thumbnail: ""
uuid: 089a955d-0c30-4d80-bc14-eef8d44ece56
updated: 1484308549
title: List of recreational number theory topics
tags:
    - Number sequences
    - Digits
    - Prime and related sequences
    - Magic squares, etc.
categories:
    - Number theory
---
This is a list of recreational number theory topics (see number theory, recreational mathematics). Listing here is not pejorative: many famous topics in number theory have origins in challenging problems posed purely for their own sake.
