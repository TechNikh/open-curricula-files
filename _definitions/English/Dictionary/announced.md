---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/announced
offline_file: ""
offline_thumbnail: ""
uuid: 430ff1bd-1396-4b8a-8201-3890784ff899
updated: 1484310477
title: announced
categories:
    - Dictionary
---
announced
     adj : declared publicly; made widely known; "their announced
           intentions"; "the newspaper's proclaimed adherence to
           the government's policy" [syn: {proclaimed}]
