---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Network_Science_CTA
offline_file: ""
offline_thumbnail: ""
uuid: 26364930-b4b6-4167-8396-8b6dde68c884
updated: 1484309438
title: Network Science CTA
tags:
    - Core Research Program
    - Communication Networks Academic Research Center (CNARC)
    - Information Networks Academic Research Center (INARC)
    - 'Social/Cognitive Networks Academic Research Center (SCNARC) [2]'
    - Interdisciplinary Research Center (IRC)
    - Cross-Cutting Research Issues (CCRI)
    - Trust in Distributed Decision Making
    - Evolving Dynamic Integrated Networks (EDIN)
    - Participating Institutions in the Alliance
    - The NS CTA Facility
    - Notes
categories:
    - Network theory
---
The Network Science Collaborative Technology Alliance (NS CTA) is a collaborative research alliance funded by the US Army Research Laboratory (ARL) and focused on fundamental research on the critical scientific and technical challenges that emerge from the close interdependence of several genres of networks such as social/cognitive, information, and communications networks. The primary goal of the NS CTA is to deeply understand the underlying commonalities among these intertwined networks, and, by understanding, improve our ability to analyze, predict, design, and influence complex systems interweaving many kinds of networks.[1]
