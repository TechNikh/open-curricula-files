---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vice-versa
offline_file: ""
offline_thumbnail: ""
uuid: a42655f4-422f-4d92-bcae-0e7797c2bc8d
updated: 1484310369
title: vice-versa
categories:
    - Dictionary
---
vice versa
     adv : with the order reversed; "she hates him and vice versa"
           [syn: {the other way around}, {contrariwise}]
