---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drum
offline_file: ""
offline_thumbnail: ""
uuid: 13d0e61e-a1e7-4691-8eb3-309093fc6c99
updated: 1484310389
title: drum
categories:
    - Dictionary
---
drum
     n 1: a musical percussion instrument; usually consists of a
          hollow cylinder with a membrane stretch across each end
          [syn: {membranophone}, {tympan}]
     2: the sound of a drum; "he could hear the drums before he
        heard the fifes"
     3: a bulging cylindrical shape; hollow with flat ends [syn: {barrel}]
     4: a cylindrical metal container used for shipping or storage
        of liquids [syn: {metal drum}]
     5: a hollow cast-iron cylinder attached to the wheel that forms
        part of the brakes [syn: {brake drum}]
     6: small to medium-sized bottom-dwelling food and game fishes
        of shallow coastal and fresh waters that make a drumming
   ...
