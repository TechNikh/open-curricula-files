---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/detecting
offline_file: ""
offline_thumbnail: ""
uuid: 167d2945-ff1d-4a86-ae81-1ed7e0dc8e4f
updated: 1484310424
title: detecting
categories:
    - Dictionary
---
detecting
     n : a police investigation to determine the perpetrator;
         "detection is hard on the feet" [syn: {detection}, {detective
         work}, {sleuthing}]
