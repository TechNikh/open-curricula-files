---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ram_Prakash_Bambah
offline_file: ""
offline_thumbnail: ""
uuid: 52ada90d-bf8b-4794-87b4-76c4449a1ebe
updated: 1484309073
title: Ram Prakash Bambah
categories:
    - Geometers
---
Bambah earned a bachelor's degree from Government College University, Lahore, and a master's degree from the University of the Punjab, Lahore.[2] He then went to England for his doctoral studies, earning his Ph.D. in 1950 from the University of Cambridge under the supervision of Louis J. Mordell.[2][3] Returning to India, he became a reader at Panjab University, Chandigarh, in 1952, and was promoted to professor there in 1957.[2] Maintaining his position at Panjab University, he also held a position as professor at Ohio State University in the US from 1964 to 1969.[2] He retired from Panjab University in 1993.[2]
