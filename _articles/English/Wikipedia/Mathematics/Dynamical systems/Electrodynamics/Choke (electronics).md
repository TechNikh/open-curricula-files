---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Choke_(electronics)
offline_file: ""
offline_thumbnail: ""
uuid: a28d7c86-5532-4c6c-8878-24311d3dffdf
updated: 1484308753
title: Choke (electronics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Common_mode_choke_2A_with_20mH_inductance.jpg
tags:
    - Types and construction
    - Audio frequency chokes
    - Radio frequency chokes
    - Common-mode chokes
categories:
    - Electrodynamics
---
The name comes from blocking—“choking”—high frequencies while passing low frequencies. It is a functional name; the name “choke” is used if an inductor is used for blocking or decoupling higher frequencies, but is simply called an “inductor” if used in electronic filters or tuned circuits. Inductors designed for use as chokes are usually distinguished by not having the low-loss construction (high Q factor) required in inductors used in tuned circuits and filtering applications.
