---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Visibility_graph_analysis
offline_file: ""
offline_thumbnail: ""
uuid: f067b556-0692-4057-b637-007db57b840f
updated: 1484309241
title: Visibility graph analysis
categories:
    - Geometric graph theory
---
In architecture, visibility graph analysis (VGA) is a method of analysing the inter-visibility connections within buildings or urban networks. Visibility graph analysis was developed from the architectural theory of space syntax by Turner et al. (2001), and is applied through construction of a visibility graph within the open space of a plan.
