---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coalition
offline_file: ""
offline_thumbnail: ""
uuid: d33fce4c-866a-4af1-9747-1fda7a544ff6
updated: 1484310585
title: coalition
categories:
    - Dictionary
---
coalition
     n 1: an organization of people (or countries) involved in a pact
          or treaty [syn: {alliance}, {alignment}, {alinement}]
          [ant: {nonalignment}]
     2: the state of being combined into one body [syn: {fusion}]
     3: the union of diverse things into one body or form or group;
        the growing together of parts [syn: {coalescence}, {coalescency},
         {concretion}, {conglutination}]
