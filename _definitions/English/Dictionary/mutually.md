---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mutually
offline_file: ""
offline_thumbnail: ""
uuid: 2b0b42bd-6638-402d-a4ac-a17d9311ee17
updated: 1484310403
title: mutually
categories:
    - Dictionary
---
mutually
     adv : in a mutual or shared manner; "the agreement was mutually
           satisfactory"; "the goals of the negotiators were not
           reciprocally exclusive" [syn: {reciprocally}]
