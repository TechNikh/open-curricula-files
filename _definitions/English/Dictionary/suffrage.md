---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suffrage
offline_file: ""
offline_thumbnail: ""
uuid: ef85f953-02c8-46c2-9778-37a2ac623f0d
updated: 1484310597
title: suffrage
categories:
    - Dictionary
---
suffrage
     n : a legal right guaranteed by the 15th amendment to the US
         constitution; guaranteed to women by the 19th amendment;
         "American women got the vote in 1920" [syn: {right to
         vote}, {vote}]
