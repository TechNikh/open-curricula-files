---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Samosa
offline_file: ""
offline_thumbnail: ""
uuid: 858df35c-9714-41e9-831a-4e9e86b9605a
updated: 1484308327
title: Samosa
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sweets_1.jpg
tags:
    - Name variation
    - Etymology
    - History
    - Regional varieties
    - South Asia
    - India
    - Bangladesh
    - Nepal
    - Pakistan
    - Maldives
    - Central Asia
    - Southeast Asia
    - Burma
    - Indonesia
    - Horn of Africa
    - Israel
    - Portuguese-speaking regions
    - English-speaking regions
categories:
    - Uncategorized
---
A samosa (/səˈmoʊsə/), or samoosa, is a fried or baked dish with a savoury filling, such as spiced potatoes, onions, peas, lentils, macaroni, noodles or minced meat (lamb, beef or chicken).[1] Pine nuts can also be added. Its size and consistency may vary, but typically it is distinctly triangular or tetrahedral in shape. Indian samosas are usually vegetarian, and often accompanied by a mint chutney.[2][3][unreliable source?] Samosas are a popular entrée, appetizer or snack in the local cuisines of Indian subcontinent, Southeast Asia, Central Asia, Southwest Asia, the Arabian Peninsula, the Mediterranean, the Horn of Africa, East Africa, North Africa and South Africa. Due to cultural ...
