---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/planting
offline_file: ""
offline_thumbnail: ""
uuid: 98314075-4447-4471-8f6d-c03a782e03fc
updated: 1484310254
title: planting
categories:
    - Dictionary
---
planting
     n 1: the act of fixing firmly in place; "he ordered the planting
          of policemen outside every doorway"
     2: a collection of plants (trees or shrubs or flowers) in a
        particular area; "the landscape architect suggested a
        small planting in the northwest corner"
     3: putting seeds or young plants in the ground to grow; "the
        planting of corn is hard work"
