---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484317201
title: mary
categories:
    - Dictionary
---
Mary
     n : the mother of Jesus; Christians refer to her as the Virgin
         Mary; she is especially honored by Roman Catholics [syn:
         {Virgin Mary}, {The Virgin}, {Blessed Virgin}, {Madonna}]
