---
version: 1
type: video
provider: YouTube
id: cLQJ8x7fB_s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 86d58729-74bb-4714-91e8-ec70d914dac1
updated: 1486069613
title: Complex Numbers part 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/cLQJ8x7fB_s/default.jpg
    - https://i3.ytimg.com/vi/cLQJ8x7fB_s/1.jpg
    - https://i3.ytimg.com/vi/cLQJ8x7fB_s/2.jpg
    - https://i3.ytimg.com/vi/cLQJ8x7fB_s/3.jpg
---
