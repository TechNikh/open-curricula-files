---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equitable_division
offline_file: ""
offline_thumbnail: ""
uuid: fd7351c0-8c06-473d-bf39-e24a15ee0a38
updated: 1484308520
title: Equitable division
tags:
    - Comparison to other criteria
    - Finding an equitable division for two partners
    - 'One cut - full revelation'
    - Proportional-equitability variant
    - 'Two cuts - moving knife'
    - 'Many cuts - full revelation'
    - Run-time
    - 'One cut - near-equitable division'
    - Finding an equitable division for three or more partners
    - Moving knife procedure
    - 'Connected pieces - full revelation'
    - Impossibility results
    - Pie cutting
    - Divisible goods
    - Summary table
categories:
    - Fair division
---
An equitable division (EQ) is a division of a heterogeneous object (e.g. a cake), in which the subjective value of all partners is the same, i.e., each partner is equally happy with his/her share. Mathematically, that means that for all partners i and j:
