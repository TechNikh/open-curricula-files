---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/endangered
offline_file: ""
offline_thumbnail: ""
uuid: 6c9d0ff1-ef8a-4f0e-8aac-d1831741b6e6
updated: 1484310240
title: endangered
categories:
    - Dictionary
---
endangered
     adj : (of flora or fauna) in imminent danger of extinction; "an
           endangered species"
