---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lightness
offline_file: ""
offline_thumbnail: ""
uuid: 7110bdca-d100-49cb-af21-02d5f7df4c97
updated: 1484310377
title: lightness
categories:
    - Dictionary
---
lightness
     n 1: the property of being comparatively small in weight; "the
          lightness of balsa wood" [syn: {weightlessness}] [ant: {heaviness}]
     2: the gracefulness of a person or animal that is quick and
        nimble [syn: {agility}, {legerity}, {lightsomeness}, {nimbleness}]
     3: having a light color [ant: {darkness}]
     4: the visual effect of illumination on objects or scenes as
        created in pictures; "he could paint the lightest light
        and the darkest dark" [syn: {light}]
