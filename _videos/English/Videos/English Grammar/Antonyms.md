---
version: 1
type: video
provider: YouTube
id: yaihuBb8Mco
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 39652607-2f44-4997-bd7c-42d96ad66a79
updated: 1486069632
title: Antonyms
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/yaihuBb8Mco/default.jpg
    - https://i3.ytimg.com/vi/yaihuBb8Mco/1.jpg
    - https://i3.ytimg.com/vi/yaihuBb8Mco/2.jpg
    - https://i3.ytimg.com/vi/yaihuBb8Mco/3.jpg
---
