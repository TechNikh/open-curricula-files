---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computer_Olympiad
offline_file: ""
offline_thumbnail: ""
uuid: 8b0deeaf-c212-49e8-b813-0cda1550d8e6
updated: 1484309002
title: Computer Olympiad
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Abalone_Boardgame.jpg
tags:
    - History
    - Games contested
    - 1st Computer Olympiad
    - 2nd Computer Olympiad
    - 3rd Computer Olympiad
    - 4th Computer Olympiad
    - 5th Computer Olympiad
    - 6th Computer Olympiad
    - 7th Computer Olympiad
    - 8th Computer Olympiad
    - 9th Computer Olympiad
    - 10th Computer Olympiad
    - 11th Computer Olympiad
    - 12th Computer Olympiad
    - 13th Computer Olympiad
    - 14th Computer Olympiad
    - 15th Computer Olympiad
    - 16th Computer Olympiad
    - 17th Computer Olympiad
    - 18th Computer Olympiad
    - 19th Computer Olympiad
    - Summary by game
    - Abalone
    - Amazons
    - Awari
    - Backgammon
    - Bridge
    - Chess
    - Chinese Chess
    - Chinese Dark Chess
    - Clobber
    - Connect Four
    - Connect6
    - Dominoes
    - Gin Rummy
    - GIPF
    - Octi
    - Poker
    - Pool
categories:
    - Game artificial intelligence
---
The Computer Olympiad is a multi-games event in which computer programs compete against each other. For many games, the Computer Olympiads are an opportunity to claim the "world's best computer player" title. First contested in 1989, the majority of the games are board games but other games such as Bridge take place as well. In 2010, several puzzles were included in the competition.
