---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Body_temperature
offline_file: ""
offline_thumbnail: ""
uuid: 6590a387-c5cd-40e0-a13d-35ac269e8c0d
updated: 1484308372
title: Thermoregulation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-BFAL_SOTE_shade.JPG
tags:
    - Classification of animals by thermal characteristics
    - Endothermy vs. ectothermy
    - Ectotherms
    - Ectothermic cooling
    - Ectothermic heating (or minimizing heat loss)
    - Endothermy
    - Homeothermy compared with poikilothermy
    - Vertebrates
    - Brain control
    - In birds and mammals
    - In humans
    - In plants
    - Behavioral temperature regulation
    - Hibernation, estivation and daily torpor
    - Variation in animals
    - Normal human temperature
    - Variations due to circadian rhythms
    - "Variations due to women's menstrual cycles"
    - Variations due to fever
    - Variations due to biofeedback
    - Low body temperature increases lifespan
    - Limits compatible with life
    - Arthropoda
categories:
    - Uncategorized
---
Thermoregulation is the ability of an organism to keep its body temperature within certain boundaries, even when the surrounding temperature is very different. A thermoconforming organism, by contrast, simply adopts the surrounding temperature as its own body temperature, thus avoiding the need for internal thermoregulation. The internal thermoregulation process is one aspect of homeostasis: a state of dynamic stability in an organism's internal conditions, maintained far from thermal equilibrium with its environment (the study of such processes in zoology has been called physiological ecology). If the body is unable to maintain a normal temperature and it increases significantly above ...
