---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gratitude
offline_file: ""
offline_thumbnail: ""
uuid: c2297051-0fc6-457b-9f2a-9d9a95e49a91
updated: 1484310234
title: gratitude
categories:
    - Dictionary
---
gratitude
     n : a feeling of thankfulness and appreciation; "he was
         overwhelmed with gratitude for their help" [ant: {ingratitude}]
