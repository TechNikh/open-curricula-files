---
version: 1
type: video
provider: YouTube
id: IrBWXoJ9NMQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 91349b97-1d21-48fa-9111-8e2185e2b562
updated: 1486069617
title: Chain rule definition and example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/IrBWXoJ9NMQ/default.jpg
    - https://i3.ytimg.com/vi/IrBWXoJ9NMQ/1.jpg
    - https://i3.ytimg.com/vi/IrBWXoJ9NMQ/2.jpg
    - https://i3.ytimg.com/vi/IrBWXoJ9NMQ/3.jpg
---
