---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/joke
offline_file: ""
offline_thumbnail: ""
uuid: 042460d4-a1fe-4712-819d-9210b3310396
updated: 1484310533
title: joke
categories:
    - Dictionary
---
joke
     n 1: a humorous anecdote or remark intended to provoke laughter;
          "he told a very funny joke"; "he knows a million gags";
          "thanks for the laugh"; "he laughed unpleasantly at
          hisown jest"; "even a schoolboy's jape is supposed to
          have some ascertainable point" [syn: {gag}, {laugh}, {jest},
           {jape}]
     2: activity characterized by good humor [syn: {jest}, {jocularity}]
     3: a ludicrous or grotesque act done for fun and amusement
        [syn: {antic}, {prank}, {trick}, {caper}, {put-on}]
     4: a triviality not to be taken seriously; "I regarded his
        campaign for mayor as a joke"
     v 1: tell a joke; speak humorously; ...
