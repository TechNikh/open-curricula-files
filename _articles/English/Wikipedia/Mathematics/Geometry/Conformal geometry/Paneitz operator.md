---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paneitz_operator
offline_file: ""
offline_thumbnail: ""
uuid: 5394919a-a1be-497b-87e6-ada95b1783d5
updated: 1484309185
title: Paneitz operator
categories:
    - Conformal geometry
---
In the mathematical field of differential geometry, the Paneitz operator is a fourth-order differential operator defined on a Riemannian manifold of dimension n. It is named after Stephen Paneitz, who discovered it in 1983, and whose preprint was later published posthumously in Paneitz 2008. In fact, the same operator was found earlier in the context of conformal supergravity by E. Fradkin and A. Tseytlin in 1982 (Phys Lett B 110 (1982) 117 and Nucl Phys B 1982 (1982) 157 ). It is given by the formula
