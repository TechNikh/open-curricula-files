---
version: 1
type: article
id: https://en.wikipedia.org/wiki/55_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 4c5e6a7a-42d3-477a-985d-04b788c09157
updated: 1484308967
title: 55 (number)
tags:
    - Mathematics
    - Science
    - Astronomy
    - Music
    - Transportation
    - Film
    - Years
    - Other uses
categories:
    - Integers
---
