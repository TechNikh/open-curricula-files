---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inspired
offline_file: ""
offline_thumbnail: ""
uuid: 1ac6e9a7-801d-4c3d-9205-ae8a38e76135
updated: 1484310416
title: inspired
categories:
    - Dictionary
---
inspired
     adj : of such surpassing excellence as to suggest divine
           inspiration; "her pies were simply divine"; "the divine
           Shakespeare"; "an elysian meal"; "an inspired
           performance" [syn: {divine}, {elysian}]
