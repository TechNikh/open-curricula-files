---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kuratowski%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 887eb92b-6365-4729-bfc0-343c6db5f438
updated: 1484309456
title: "Kuratowski's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-GP92-Kuratowski.svg.png
tags:
    - Statement of the theorem
    - Kuratowski subgraphs
    - Algorithmic implications
    - History
    - Related results
categories:
    - Theorems in graph theory
---
In graph theory, Kuratowski's theorem is a mathematical forbidden graph characterization of planar graphs, named after Kazimierz Kuratowski. It states that a finite graph is planar if and only if it does not contain a subgraph that is a subdivision of K5 (the complete graph on five vertices) or of K3,3 (complete bipartite graph on six vertices, three of which connect to each of the other three, also known as the utility graph).
