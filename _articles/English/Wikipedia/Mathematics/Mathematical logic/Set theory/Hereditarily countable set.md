---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hereditarily_countable_set
offline_file: ""
offline_thumbnail: ""
uuid: 050cea7e-6268-4bca-a5ce-1b54c4d9be7e
updated: 1484309504
title: Hereditarily countable set
categories:
    - Set theory
---
In set theory, a set is called hereditarily countable if it is a countable set of hereditarily countable sets. This inductive definition is in fact well-founded and can be expressed in the language of first-order set theory. A set is hereditarily countable if and only if it is countable, and every element of its transitive closure is countable. If the axiom of countable choice holds, then a set is hereditarily countable if and only if its transitive closure is countable.
