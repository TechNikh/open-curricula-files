---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mount
offline_file: ""
offline_thumbnail: ""
uuid: 892c9f30-3ad9-4cb4-8ff3-a04e13f60848
updated: 1484310178
title: mount
categories:
    - Dictionary
---
mount
     n 1: a lightweight horse kept for riding only [syn: {saddle horse},
           {riding horse}]
     2: the act of climbing something; "it was a difficult climb to
        the top" [syn: {climb}]
     3: a land mass that projects well above its surroundings;
        higher than a hill [syn: {mountain}]
     4: mounting consisting of a piece of metal (as in a ring or
        other jewelry) that holds a gem in place; "the diamond was
        in a plain gold mount" [syn: {setting}]
     5: something forming a back that is added for strengthening
        [syn: {backing}]
     v 1: attach to a support; "They mounted the aerator on a
          floating"
     2: go up or advance; "Sales ...
