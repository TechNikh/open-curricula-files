---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mangrove
offline_file: ""
offline_thumbnail: ""
uuid: d0cc7820-97eb-43f8-8694-10dc166bcd4c
updated: 1484310439
title: mangrove
categories:
    - Dictionary
---
mangrove
     n : a tropical tree or shrub bearing fruit that germinates while
         still on the tree and having numerous prop roots that
         eventually form an impenetrable mass and are important in
         land building [syn: {Rhizophora mangle}]
