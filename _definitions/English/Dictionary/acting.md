---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acting
offline_file: ""
offline_thumbnail: ""
uuid: 15f18ec1-277d-4e09-9c2a-c1788400f031
updated: 1484310202
title: acting
categories:
    - Dictionary
---
acting
     adj : serving temporarily especially as a substitute; "the acting
           president" [syn: {acting(a)}]
     n : the performance of a part or role in a drama [syn: {playing},
          {playacting}, {performing}]
