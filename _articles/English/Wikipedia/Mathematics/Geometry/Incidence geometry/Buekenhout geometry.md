---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Buekenhout_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 4ca212f8-42a2-4c5b-ba0c-07537433f6fa
updated: 1484309253
title: Buekenhout geometry
categories:
    - Incidence geometry
---
In mathematics, a Buekenhout geometry or diagram geometry is a generalization of projective space, Tits buildings, and several other geometric structures, introduced by Buekenhout (1979).
