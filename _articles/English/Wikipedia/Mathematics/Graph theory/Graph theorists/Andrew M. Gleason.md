---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Andrew_M._Gleason
offline_file: ""
offline_thumbnail: ""
uuid: bbcbe02e-7e32-45c1-bd2b-a35fe65ca74f
updated: 1484309466
title: Andrew M. Gleason
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-GleasonAndrewMattei_WithJean1958.jpg
tags:
    - Biography
    - Teaching and education reform
    - Cryptanalysis work
    - Mathematics research
    - "Hilbert's fifth problem"
    - Quantum mechanics
    - Ramsey theory
    - Coding theory
    - Other areas
    - Awards and honors
    - Selected publications
    - Notes
categories:
    - Graph theorists
---
Andrew Mattei Gleason (November 4, 1921 – October 17, 2008) was an American mathematician who as a young World War II naval officer broke German and Japanese military codes, then over the succeeding sixty years made fundamental contributions to widely varied areas of mathematics, including the solution of Hilbert's fifth problem, and was a leader in reform and innovation in math­e­mat­ics teaching at all levels.[4][5] Gleason's theorem in quantum logic and the Greenwood–Gleason graph, an important example in Ramsey theory, are named for him.
