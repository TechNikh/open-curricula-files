---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rejection
offline_file: ""
offline_thumbnail: ""
uuid: 5d081d2c-6730-43a9-89be-0e55d06dfc84
updated: 1484310172
title: rejection
categories:
    - Dictionary
---
rejection
     n 1: the act of rejecting something; "his proposals were met with
          rejection"
     2: the state of being rejected [ant: {acceptance}]
     3: (medicine) an immunological response that refuses to accept
        substances or organisms that are recognized as foreign;
        "rejection of the transplanted liver"
     4: the speech act of rejecting
