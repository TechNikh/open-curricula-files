---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Induced_path
offline_file: ""
offline_thumbnail: ""
uuid: 1cfe2381-d94a-4e3e-89e8-ea6492aef017
updated: 1484309445
title: Induced path
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Snakeinthebox.svg_0.png
tags:
    - Example
    - Characterization of graph families
    - Algorithms and Complexity
    - Atomic cycles
    - Notes
categories:
    - Graph theory objects
---
In the mathematical area of graph theory, an induced path in an undirected graph G is a path that is an induced subgraph of G. That is, it is a sequence of vertices in G such that each two adjacent vertices in the sequence are connected by an edge in G, and each two nonadjacent vertices in the sequence are not connected by any edge in G. An induced path is sometimes called a snake, and the problem of finding long induced paths in hypercube graphs is known as the snake-in-the-box problem.
