---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/masonry
offline_file: ""
offline_thumbnail: ""
uuid: 13df9371-9069-4a1f-909c-1dbde021cb31
updated: 1484310266
title: masonry
categories:
    - Dictionary
---
masonry
     n 1: structure built of stone or brick by a mason
     2: Freemasons collectively [syn: {Freemasonry}]
     3: the craft of a mason
