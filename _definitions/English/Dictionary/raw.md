---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/raw
offline_file: ""
offline_thumbnail: ""
uuid: aa6f7551-f88f-4a3a-ac4d-e56fe07c4828
updated: 1484310244
title: raw
categories:
    - Dictionary
---
raw
     adj 1: (used especially of commodities) in the natural unprocessed
            condition; "natural yogurt"; "natural produce"; "raw
            wool"; "raw sugar"; "bales of rude cotton" [syn: {natural},
             {raw(a)}, {rude(a)}]
     2: having the surface exposed and painful; "a raw wound"
     3: not treated with heat to prepare it for eating [ant: {cooked}]
     4: not processed or refined; "raw sewage"
     5: devoid of elaboration or diminution or concealment; bare and
        pure; "naked ambition"; "raw fury"; "you may kill someone
        someday with your raw power" [syn: {naked}]
     6: brutally unfair or harsh; "received raw treatment from his
        friends"; ...
