---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Toroidal_graph
offline_file: ""
offline_thumbnail: ""
uuid: e13813a4-f54a-44ac-b09b-7d932c722b6b
updated: 1484309481
title: Toroidal graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Toroidal_graph_sample.gif
tags:
    - Examples
    - Properties
    - Notes
categories:
    - Topological graph theory
---
In mathematics, a toroidal graph is a graph that can be embedded on a torus. In other words, the graph's vertices can be placed on a torus such that no edges cross.
