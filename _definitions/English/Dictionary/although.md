---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/although
offline_file: ""
offline_thumbnail: ""
uuid: 627bb28b-f490-4f36-82b8-fc105f44c14f
updated: 1484310327
title: although
categories:
    - Dictionary
---
although
     adv : despite the fact that; "even though she knew the answer, she
           did not respond" [syn: {though}, {tho'}]
