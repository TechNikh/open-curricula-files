---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terrorism
offline_file: ""
offline_thumbnail: ""
uuid: 403c8c03-be88-4a07-b49c-43cadc7e788f
updated: 1484310178
title: terrorism
categories:
    - Dictionary
---
terrorism
     n : the calculated use of violence (or threat of violence)
         against civilians in order to attain goals that are
         political or religious or ideological in nature; this is
         done through intimindation or coercion or instilling fear
         [syn: {act of terrorism}, {terrorist act}]
