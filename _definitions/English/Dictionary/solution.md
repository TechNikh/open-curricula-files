---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solution
offline_file: ""
offline_thumbnail: ""
uuid: f518cc55-89a8-45cb-8e7b-f63d5e231dfe
updated: 1484310344
title: solution
categories:
    - Dictionary
---
solution
     n 1: a homogeneous mixture of two or more substances; frequently
          (but not necessarily) a liquid solution; "he used a
          solution of peroxide and water"
     2: a statement that solves a problem or explains how to solve
        the problem; "they were trying to find a peaceful
        solution"; "the answers were in the back of the book"; "he
        computed the result to four decimal places" [syn: {answer},
         {result}, {resolution}, {solvent}]
     3: a method for solving a problem; "the easy solution is to
        look it up in the handbook"
     4: the set of values that give a true statement when
        substituted into an equation [syn: {root}]
   ...
