---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distorted
offline_file: ""
offline_thumbnail: ""
uuid: bbd7313a-2794-40ba-81cf-2227b82af369
updated: 1484310212
title: distorted
categories:
    - Dictionary
---
distorted
     adj 1: strained or wrenched out of normal shape; "old trees with
            contorted branches"; "scorched and distorted fragments
            of steel"; "trapped under twisted steel girders" [syn:
             {contorted}, {twisted}]
     2: so badly formed or out of shape as to be ugly; "deformed
        thalidomide babies"; "his poor distorted limbs"; "an
        ill-shapen vase"; "a limp caused by a malformed foot";
        "misshapen old fingers" [syn: {deformed}, {ill-shapen}, {malformed},
         {misshapen}]
     3: having an intended meaning altered or misrepresented; "many
        of the facts seemed twisted out of any semblance to
        reality"; "a perverted ...
