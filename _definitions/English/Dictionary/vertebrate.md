---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vertebrate
offline_file: ""
offline_thumbnail: ""
uuid: bc5f2d2f-b896-47d1-a466-e6bbc898d43d
updated: 1484310283
title: vertebrate
categories:
    - Dictionary
---
vertebrate
     adj : having a backbone or spinal column; "fishes and amphibians
           and reptiles and birds and mammals are verbetrate
           animals" [ant: {invertebrate}]
     n : animals having a bony or cartilaginous skeleton with a
         segmented spinal column and a large brain enclosed in a
         skull or cranium [syn: {craniate}]
