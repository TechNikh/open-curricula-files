---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calculated
offline_file: ""
offline_thumbnail: ""
uuid: b80de248-49c3-40a6-b8d7-4066b51cd2c4
updated: 1484310281
title: calculated
categories:
    - Dictionary
---
calculated
     adj 1: carefully thought out in advance; "a calculated insult";
            "with measured irony" [syn: {deliberate}, {measured}]
     2: determined by mathematical computation; "the calculated
        velocity of a bullet"; "a derived value" [syn: {derived}]
