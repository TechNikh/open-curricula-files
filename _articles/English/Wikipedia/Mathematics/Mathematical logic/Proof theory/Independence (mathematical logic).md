---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Independence_(mathematical_logic)
offline_file: ""
offline_thumbnail: ""
uuid: a13d619d-775e-4d51-b684-0b7a434f893b
updated: 1484309495
title: Independence (mathematical logic)
tags:
    - Usage note
    - Independence results in set theory
    - Applications in Physical Theory
categories:
    - Proof theory
---
A sentence σ is independent of a given first-order theory T if T neither proves nor refutes σ; that is, it is impossible to prove σ from T, and it is also impossible to prove from T that σ is false. Sometimes, σ is said (synonymously) to be undecidable from T; this is not the same meaning of "decidability" as in a decision problem.
