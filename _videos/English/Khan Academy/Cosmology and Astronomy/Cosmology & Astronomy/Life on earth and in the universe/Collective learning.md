---
version: 1
type: video
provider: YouTube
id: NqR_dtZu4Mo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Life%20on%20earth%20and%20in%20the%20universe/Collective%20learning.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 52008965-81f1-496b-93d7-d31c69fe1ae0
updated: 1486069686
title: Collective learning
categories:
    - Life on earth and in the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/NqR_dtZu4Mo/default.jpg
    - https://i3.ytimg.com/vi/NqR_dtZu4Mo/1.jpg
    - https://i3.ytimg.com/vi/NqR_dtZu4Mo/2.jpg
    - https://i3.ytimg.com/vi/NqR_dtZu4Mo/3.jpg
---
