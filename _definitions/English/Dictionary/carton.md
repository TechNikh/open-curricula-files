---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carton
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484394061
title: carton
categories:
    - Dictionary
---
carton
     n 1: the quantity contained in a carton [syn: {cartonful}]
     2: a box made of cardboard; opens by flaps on top
