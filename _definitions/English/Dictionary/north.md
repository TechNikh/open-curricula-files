---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/north
offline_file: ""
offline_thumbnail: ""
uuid: d7a83489-1744-4f7b-b63f-de4f89fd9dd2
updated: 1484310228
title: north
categories:
    - Dictionary
---
north
     adj : situated in or facing or moving toward or coming from the
           north; "artists like north light"; "the north portico"
           [ant: {south}]
     n 1: the region of the United States lying north of the
          Mason-Dixon Line
     2: the United States (especially the northern states during the
        American Civil War); "he has visited every state in the
        Union"; "Lee hoped to detach Maryland from the Union";
        "the North's superior resources turned the scale" [syn: {Union}]
     3: the cardinal compass point that is at 0 or 360 degrees [syn:
         {due north}, {N}]
     4: any region lying in or toward the north [syn: {northland}, ...
