---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sound
offline_file: ""
offline_thumbnail: ""
uuid: 0396b9f2-8af2-49c4-a9bd-61f07faa331a
updated: 1484310379
title: sound
categories:
    - Dictionary
---
sound
     adj 1: financially secure and safe; "sound investments"; "a sound
            economy" [ant: {unsound}]
     2: exercising or showing good judgment; "healthy scepticism";
        "a healthy fear of rattlesnakes"; "the healthy attitude of
        French laws"; "healthy relations between labor and
        management"; "an intelligent solution"; "a sound approach
        to the problem"; "sound advice"; "no sound explanation for
        his decision" [syn: {healthy}, {intelligent}, {levelheaded}]
     3: in good condition; free from defect or damage or decay; "a
        sound timber"; "the wall is sound"; "a sound foundation"
        [ant: {unsound}]
     4: in excellent physical ...
