---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/win
offline_file: ""
offline_thumbnail: ""
uuid: 660b9032-7fdf-4695-a913-9e8c8d692d80
updated: 1484310429
title: win
categories:
    - Dictionary
---
win
     n 1: a victory (as in a race or other competition); "he was happy
          to get the win"
     2: something won (especially money) [syn: {winnings}, {profits}]
        [ant: {losings}]
     v 1: be the winner in a contest or competition; be victorious;
          "He won the Gold Medal in skating"; "Our home team won";
          "Win the game" [ant: {lose}]
     2: win something through one's efforts; "I acquired a passing
        knowledge of Chinese"; "Gain an understanding of
        international finance" [syn: {acquire}, {gain}] [ant: {lose}]
     3: obtain advantages, such as points, etc.; "The home team was
        gaining ground"; "After defeating the Knicks, the Blazers
  ...
