---
version: 1
type: video
provider: YouTube
id: 1KwW1v__T_0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Equation%20of%20a%20tangent%20line.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f960cefb-f806-4bfa-8beb-0f0afe3271c1
updated: 1486069693
title: Equation of a tangent line
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/1KwW1v__T_0/default.jpg
    - https://i3.ytimg.com/vi/1KwW1v__T_0/1.jpg
    - https://i3.ytimg.com/vi/1KwW1v__T_0/2.jpg
    - https://i3.ytimg.com/vi/1KwW1v__T_0/3.jpg
---
