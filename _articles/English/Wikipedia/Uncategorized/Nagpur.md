---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nagpur
offline_file: ""
offline_thumbnail: ""
uuid: 57802b66-dc06-42ff-bdf5-9f5473340fba
updated: 1484308458
title: Nagpur
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Futala_lake.jpg
tags:
    - History
    - Etymology
    - Early and medieval history
    - Modern history
    - After Indian independence
    - geography
    - Topography
    - Climate
    - Administration
    - Second capital of Maharashtra
    - Local government
    - Utility services
    - Health care
    - Greater Nagpur Metropolitan Area
    - Military establishments
    - Demographics
    - population
    - Religion and language
    - Economy
    - Education
    - Tourism
    - Sports
    - culture
    - Cultural events and literature
    - Religious places and festivals
    - Arts and crafts
    - Cuisine
    - Media
    - Transport
    - Rail
    - Nagpur Metro Rail
    - Road
    - Air transport
    - Notable people
    - Twin towns and sister cities
    - Smart City Project
categories:
    - Uncategorized
---
Nagpur (Marathi नागपूर  pronunciation (help·info)) is the winter capital and the third largest city of the Indian state of Maharashtra[13] and largest city of central India. It has one of the highest literacy rate of 91.92% among all the urban agglomerations in India[14] and one of the proposed Smart Cities from Maharashtra.[15]
