---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nice
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484364061
title: nice
categories:
    - Dictionary
---
nice
     adj 1: pleasant or pleasing or agreeable in nature or appearance;
            "what a nice fellow you are and we all thought you so
            nasty"- George Meredith; "nice manners"; "a nice
            dress"; "a nice face"; "a nice day"; "had a nice time
            at the party"; "the corn and tomatoes are nice today"
            [ant: {nasty}]
     2: socially or conventionally correct; refined or virtuous;
        "from a decent family"; "a nice girl" [syn: {decent}]
     3: done with delicacy and skill; "a nice bit of craft"; "a job
        requiring nice measurements with a micrometer"; "a nice
        shot" [syn: {skillful}]
     4: excessively fastidious and easily ...
