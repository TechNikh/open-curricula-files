---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/technique
offline_file: ""
offline_thumbnail: ""
uuid: 41420ecc-03aa-417e-88e1-2b0b3d1ded1a
updated: 1484310218
title: technique
categories:
    - Dictionary
---
technique
     n 1: a practical method or art applied to some particular task
     2: skillfulness in the command of fundamentals deriving from
        practice and familiarity; "practice greatly improves
        proficiency" [syn: {proficiency}]
