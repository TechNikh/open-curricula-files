---
version: 1
type: video
provider: YouTube
id: 39ZiDWnkUeo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Carbon%20and%20Organic%20Chemistry%203D%20Animation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2b45e55f-e083-44a5-bef4-de636e110f16
updated: 1486069664
title: Carbon and Organic Chemistry 3D Animation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/39ZiDWnkUeo/default.jpg
    - https://i3.ytimg.com/vi/39ZiDWnkUeo/1.jpg
    - https://i3.ytimg.com/vi/39ZiDWnkUeo/2.jpg
    - https://i3.ytimg.com/vi/39ZiDWnkUeo/3.jpg
---
