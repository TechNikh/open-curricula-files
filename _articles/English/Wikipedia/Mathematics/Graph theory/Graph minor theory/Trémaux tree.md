---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tr%C3%A9maux_tree'
offline_file: ""
offline_thumbnail: ""
uuid: 385c49ba-b132-4e49-9ed6-b0053d6f1356
updated: 1484309414
title: Trémaux tree
tags:
    - Example
    - In finite graphs
    - Existence
    - Parallel construction
    - Logical expression
    - Related properties
    - In infinite graphs
    - Existence
    - Minors
    - Ends and metrizability
categories:
    - Graph minor theory
---
In graph theory, a Trémaux tree of an undirected graph G is a spanning tree of G, rooted at one of its vertices, with the property that every two adjacent vertices in G are related to each other as an ancestor and descendant in the tree. All depth-first search trees and all Hamiltonian paths are Trémaux trees. Trémaux trees are named after Charles Pierre Trémaux, a 19th-century French author who used a form of depth-first search as a strategy for solving mazes.[1][2] They have also been called normal spanning trees, especially in the context of infinite graphs.[3]
