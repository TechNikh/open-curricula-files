---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metasymplectic_space
offline_file: ""
offline_thumbnail: ""
uuid: cc5722ed-c0f6-43fc-b54a-917a6c825a3c
updated: 1484309253
title: Metasymplectic space
categories:
    - Incidence geometry
---
In mathematics, a metasymplectic space, introduced by Freudenthal (1959) and Tits (1974, 10.13), is a Tits building of type F4 (a specific generalized incidence structure). The four types of vertices are called points, lines, planes, and symplecta.
