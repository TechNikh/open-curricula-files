---
title: Erosion
article_ref: https://en.wikipedia.org/wiki/Erosion
tags:
    - Physical processes
    - Rainfall and surface runoff
    - Rivers and streams
    - Coastal erosion
    - Chemical erosion
    - Glaciers
    - Floods
    - Wind erosion
    - Mass movement
    - Factors affecting erosion rates
    - Climate
    - Vegetative cover
    - Topography
    - Tectonics
    - Erosion of Earth systems
    - Mountain ranges
    - Soils
categories:
    - soil
---
