---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alliterative
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484494081
title: alliterative
categories:
    - Dictionary
---
alliterative
     adj : having the same consonant at the beginning of each stressed
           syllable; "alliterative verse"
