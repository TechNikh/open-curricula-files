---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cone
offline_file: ""
offline_thumbnail: ""
uuid: 684d71a3-e968-4494-a087-722a58a1167c
updated: 1484310156
title: cone
categories:
    - Dictionary
---
cone
     n 1: any cone-shaped artifact
     2: a shape whose base is a circle and whose sides taper up to a
        point [syn: {conoid}, {cone shape}]
     3: cone-shaped mass of ovule- or spore-bearing scales or bracts
        [syn: {strobilus}, {strobile}]
     4: visual receptor cell sensitive to color [syn: {cone cell}, {retinal
        cone}]
     v : make cone-shaped; "cone a tire"
