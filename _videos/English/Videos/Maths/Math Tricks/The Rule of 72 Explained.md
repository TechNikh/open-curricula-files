---
version: 1
type: video
provider: YouTube
id: NhB3NVwpmF0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4f2c6190-8298-4eb6-8d0a-c5d5f6d5dd29
updated: 1486069623
title: The Rule of 72 Explained
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/NhB3NVwpmF0/default.jpg
    - https://i3.ytimg.com/vi/NhB3NVwpmF0/1.jpg
    - https://i3.ytimg.com/vi/NhB3NVwpmF0/2.jpg
    - https://i3.ytimg.com/vi/NhB3NVwpmF0/3.jpg
---
