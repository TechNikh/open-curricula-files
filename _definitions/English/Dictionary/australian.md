---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/australian
offline_file: ""
offline_thumbnail: ""
uuid: 8fdb824e-01cf-4c6f-a45b-8d4e3f1a1d0a
updated: 1484310597
title: australian
categories:
    - Dictionary
---
Australian
     adj : of or relating to or characteristic of Australia or its
           inhabitants; "Australian deserts"; "Australian
           aborigines"
     n 1: a native or inhabitant of Australia [syn: {Aussie}]
     2: the Austronesian languages spoken by Australian aborigines
        [syn: {Aboriginal Australian}]
