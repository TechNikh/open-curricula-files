---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sector
offline_file: ""
offline_thumbnail: ""
uuid: ba8f6163-0476-456f-992f-2ee79ad1496a
updated: 1484310252
title: sector
categories:
    - Dictionary
---
sector
     n 1: a plane figure bounded by two radii and the included arc of
          a circle
     2: a body of people who form part of society or economy; "the
        public sector"
     3: a particular aspect of life or activity; "he was helpless in
        an important sector of his life" [syn: {sphere}]
     4: the minimum track length that can be assigned to store
        information; unless otherwise specified a sector of data
        consists of 512 bytes
     5: a portion of a military position
     6: measuring instrument consisting of two graduated arms hinged
        at one end
