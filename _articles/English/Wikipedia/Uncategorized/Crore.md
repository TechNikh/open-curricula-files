---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crore
offline_file: ""
offline_thumbnail: ""
uuid: 70011675-28ae-4eb2-825d-8d52bd0f23fb
updated: 1484308456
title: Crore
tags:
    - money
    - Demographics
    - Etymology and regional variants
    - South Asian languages
    - In Other languages
categories:
    - Uncategorized
---
A crore (/ˈkrɔər/; abbreviated cr) denotes ten million (10,000,000 or 107 in scientific notation) and is equals to 100 lakhs in the Indian and Nepali Numbering system. It is widely used in South Asia, and is written in this region as 1,00,00,000 with the local style of digit group separators (a lakh is equal to one hundred thousand and is written as 1,00,000).[1]
