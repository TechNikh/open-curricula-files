---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Warwick_Tucker
offline_file: ""
offline_thumbnail: ""
uuid: 2b66a38d-3a6b-4b59-a9e5-ea4b13a0f86d
updated: 1484308692
title: Warwick Tucker
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Bluetank.png
categories:
    - Dynamical systems theorists
---
Warwick Tucker is an Australian mathematician at Uppsala University who works on dynamical systems, chaos theory and computational mathematics.[1] He is a recipient of the 2002 R. E. Moore Prize,[2] and the 2004 EMS Prize.[3]
