---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cristina_Bicchieri
offline_file: ""
offline_thumbnail: ""
uuid: 0b5e5d7e-41bf-4c73-8b97-ab12c84fe180
updated: 1484308513
title: Cristina Bicchieri
tags:
    - Biography
    - Research
    - Social norms
    - Epistemic foundations of game theory
    - Books
categories:
    - Game theorists
---
Cristina Bicchieri is the S.J.P. Harvie Professor of Social Thought and Comparative Ethics in the Philosophy and Psychology Departments at the University of Pennsylvania, and director of the Philosophy, Politics and Economics program.[1] She is also a Professor in the Legal Studies department of the Wharton School. She has worked on problems in the philosophy of social science, rational choice and game theory.[1] More recently, her work has focused on the nature and evolution of social norms, and the design of behavioral experiments to test under which conditions norms will be followed.[1] She is a leader in the field of behavioral ethics and is the director of the Behavioral Ethics Lab ...
