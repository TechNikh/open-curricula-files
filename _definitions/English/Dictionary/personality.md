---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/personality
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484405881
title: personality
categories:
    - Dictionary
---
personality
     n 1: the complex of all the attributes--behavioral,
          temperamental, emotional and mental--that characterize a
          unique individual; "their different reactions reflected
          their very different personalities"; "it is his nature
          to help others"
     2: a person of considerable prominence; "she is a Hollywood
        personality"
