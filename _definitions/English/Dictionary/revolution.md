---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revolution
offline_file: ""
offline_thumbnail: ""
uuid: 8f22a59c-fb76-40b5-96c5-5805cbf80080
updated: 1484310369
title: revolution
categories:
    - Dictionary
---
revolution
     n 1: a drastic and far-reaching change in ways of thinking and
          behaving; "the industrial revolution was also a cultural
          revolution"
     2: the overthrow of a government by those who are governed
     3: a single complete turn (axial or orbital); "the plane made
        three rotations before it crashed"; "the revolution of the
        earth about the sun takes one year" [syn: {rotation}, {gyration}]
