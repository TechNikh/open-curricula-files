---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/use
offline_file: ""
offline_thumbnail: ""
uuid: 37a0f8a1-fcd1-4307-bd9a-a7a58fc343b1
updated: 1484310343
title: use
categories:
    - Dictionary
---
use
     n 1: the act of using; "he warned against the use of narcotic
          drugs"; "skilled in the utilization of computers" [syn:
          {usage}, {utilization}, {utilisation}, {employment}, {exercise}]
     2: a particular service; "he put his knowledge to good use";
        "patrons have their uses"
     3: what something is used for; "the function of an auger is to
        bore holes"; "ballet is beautiful but what use is it?"
        [syn: {function}, {purpose}, {role}]
     4: (economics) the utilization of economic goods to satisfy
        needs or in manufacturing; "the consumption of energy has
        increased steadily" [syn: {consumption}, {economic
        consumption}, ...
