---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bogdanov%E2%80%93Takens_bifurcation'
offline_file: ""
offline_thumbnail: ""
uuid: 74313dd1-bf99-43d1-98c2-527c194e6a1f
updated: 1484308705
title: Bogdanov–Takens bifurcation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Bogdanov_takens_bifurcation.svg.png
categories:
    - Bifurcation theory
---
In bifurcation theory, a field within mathematics, a Bogdanov–Takens bifurcation is a well-studied example of a bifurcation with co-dimension two, meaning that two parameters must be varied for the bifurcation to occur. It is named after Rifkat Bogdanov and Floris Takens, who independently and simultaneously described this bifurcation.
