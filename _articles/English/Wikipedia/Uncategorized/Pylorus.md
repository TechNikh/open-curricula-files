---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pyloric
offline_file: ""
offline_thumbnail: ""
uuid: f30ef091-272b-4d57-9560-a5911977bf49
updated: 1484308380
title: Pylorus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray1050_0.png
tags:
    - Structure
    - Antrum
    - Canal
    - Sphincter
    - Histology
    - Function
    - Clinical significance
    - Stenosis
    - Other
    - Additional images
categories:
    - Uncategorized
---
The pylorus (/paɪˈlɔərəs/ or /pᵻˈlɔərəs/), or pyloric part, connects the stomach to the duodenum. The pylorus is considered as having two parts, the pyloric antrum (opening to the body of the stomach) and the pyloric canal (opening to the duodenum). The pyloric canal ends as the pyloric orifice, which marks the junction between the stomach and the duodenum. The orifice is surrounded by a sphincter, a band of muscle, called the pyloric sphincter. The word pylorus comes from Greek πυλωρός, via Latin. The word pylorus in Greek means "gatekeeper", related to "gate" (Greek: pyle) and is thus linguistically related to the word "pylon".[1]
