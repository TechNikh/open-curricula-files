---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prepare
offline_file: ""
offline_thumbnail: ""
uuid: 9cd341d2-0739-4806-9f56-d50d91cd484e
updated: 1484310315
title: prepare
categories:
    - Dictionary
---
prepare
     v 1: make ready or suitable or equip in advance for a particular
          purpose or for some use, event, etc; "Get the children
          ready for school!"; "prepare for war"; "I was fixing to
          leave town after I paid the hotel bill" [syn: {fix}, {set
          up}, {ready}, {gear up}, {set}]
     2: prepare for eating by applying heat; "Cook me dinner,
        please"; "can you make me an omelette?"; "fix breakfast
        for the guests, please" [syn: {cook}, {fix}, {ready}, {make}]
     3: to prepare verbally, either for written or spoken delivery;
        "prepare a report"; "prepare a speech"
     4: arrange by systematic planning and united effort; "machinate
 ...
