---
version: 1
type: video
provider: YouTube
id: EYYysBEwA6w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c17aed18-1d4a-45b4-a18f-cda9d19ccf10
updated: 1486069658
title: Free energy From Air Circuit part 2/24
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/EYYysBEwA6w/default.jpg
    - https://i3.ytimg.com/vi/EYYysBEwA6w/1.jpg
    - https://i3.ytimg.com/vi/EYYysBEwA6w/2.jpg
    - https://i3.ytimg.com/vi/EYYysBEwA6w/3.jpg
---
