---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proprism
offline_file: ""
offline_thumbnail: ""
uuid: 18dec6ff-32af-4d93-85c9-a2bd0850b17e
updated: 1484309289
title: Proprism
categories:
    - Multi-dimensional geometry
---
In geometry of 4 dimensions or higher, a proprism is a polytope resulting from the Cartesian product of two or more polytopes, each of two dimensions or higher. The term was coined by John Horton Conway for product prism. The dimension of the space of a proprism equals the sum of the dimensions of all its product elements.
