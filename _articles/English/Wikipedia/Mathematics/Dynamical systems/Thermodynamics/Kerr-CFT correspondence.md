---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kerr/CFT_correspondence
uuid: e19f2eb1-6054-4e66-9f64-6ee5a426a21e
updated: 1480455138
title: Kerr/CFT correspondence
categories:
    - Thermodynamics
---
The Kerr/CFT correspondence is an extension of the AdS/CFT correspondence or gauge-gravity duality to rotating black holes (which are described by the Kerr metric).[1]
