---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/romantic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484429701
title: romantic
categories:
    - Dictionary
---
romantic
     adj 1: belonging to or characteristic of romanticism or the
            Romantic movement in the arts; "romantic poetry" [syn:
             {romanticist}, {romanticistic}]
     2: expressive of or exciting sexual love or romance; "her
        amatory affairs"; "amorous glances"; "a romantic
        adventure"; "a romantic moonlight ride" [syn: {amatory}, {amorous}]
     3: not sensible about practical matters; unrealistic; "as
        quixotic as a restoration of medieval knighthood"; "a
        romantic disregard for money"; "a wild-eyed dream of a
        world state" [syn: {quixotic}, {wild-eyed}]
     n 1: a soulful or amorous idealist
     2: an artist of the romantic ...
