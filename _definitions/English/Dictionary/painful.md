---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/painful
offline_file: ""
offline_thumbnail: ""
uuid: 586298dc-1419-40aa-92df-0152d8da1116
updated: 1484310200
title: painful
categories:
    - Dictionary
---
painful
     adj 1: causing physical or psychological pain; "worked with painful
            slowness" [ant: {painless}]
     2: causing misery or pain or distress; "it was a sore trial to
        him"; "the painful process of growing up" [syn: {afflictive},
         {sore}]
     3: exceptionally bad or displeasing; "atrocious taste";
        "abominable workmanship"; "an awful voice"; "dreadful
        manners"; "a painful performance"; "terrible handwriting";
        "an unspeakable odor came sweeping into the room" [syn: {atrocious},
         {abominable}, {awful}, {dreadful}, {terrible}, {unspeakable}]
     4: causing physical discomfort; "bites of black flies are more
        than ...
