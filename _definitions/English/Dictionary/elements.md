---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elements
offline_file: ""
offline_thumbnail: ""
uuid: aeff4923-ac2e-40e2-bbdc-2aa017d921fe
updated: 1484310281
title: elements
categories:
    - Dictionary
---
elements
     n : violent or severe weather (viewed as caused by the action of
         the four elements); "they felt the full fury of the
         elements"
