---
version: 1
type: article
id: https://en.wikipedia.org/wiki/LOOP_(programming_language)
offline_file: ""
offline_thumbnail: ""
uuid: 41658baf-881a-4a51-a2b4-015ac342387b
updated: 1484309487
title: LOOP (programming language)
tags:
    - features
    - Formal definition
    - Syntax
    - Semantics
    - Example Programs
    - Addition
    - Multiplication
    - Notes and references
categories:
    - Computability theory
---
LOOP is a programming language designed by Uwe Schöning, along with GOTO and WHILE. The only operations supported in the language are assignment, addition and looping.
