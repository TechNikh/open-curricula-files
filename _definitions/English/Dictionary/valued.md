---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/valued
offline_file: ""
offline_thumbnail: ""
uuid: 8fbff773-cae0-4f27-a633-8f84b39c9b58
updated: 1484310531
title: valued
categories:
    - Dictionary
---
valued
     adj 1: (usually used in combination) having value of a specified
            kind; "triple-valued"
     2: held in great esteem for admirable qualities especially of
        an intrinsic nature; "a valued friend"
