---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pavol_Hell
offline_file: ""
offline_thumbnail: ""
uuid: 556dcdb9-501b-49d1-96d8-bcd33b7eba0e
updated: 1484309468
title: Pavol Hell
categories:
    - Graph theorists
---
Pavol Hell is a Canadian mathematician and computer scientist, born in Czechoslovakia. He is a professor of computing science at Simon Fraser University. Hell started his mathematical studies at Charles University in Prague, and moved to Canada in August 1968 after the Warsaw Pact invasion of Czechoslovakia. He obtained his MSc from McMaster University in Hamilton, under the joint supervision of Gert Sabidussi and Alex Rosa, and his PhD at the Universite de Montreal, with Gert Sabidussi. In his PhD research he pioneered, on the suggestion of Gert Sabidussi, the study of graph retracts. He describes his area of interest as "computational combinatorics", including algorithmic graph theory and ...
