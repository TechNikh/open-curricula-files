---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/directional
offline_file: ""
offline_thumbnail: ""
uuid: 0b1c26fa-0cb0-493c-a923-18eaf74a7233
updated: 1484310405
title: directional
categories:
    - Dictionary
---
directional
     adj 1: relating to or indicating directions in space; "a
            directional microphone" [ant: {omnidirectional}]
     2: relating to direction toward a (nonspatial) goal; "he tried
        to explain the directional trends of modern science"
     3: showing the way by conducting or leading; imposing direction
        on; "felt his mother's directing arm around him"; "the
        directional role of science on industrial progress" [syn:
        {directing}, {directive}, {guiding}]
