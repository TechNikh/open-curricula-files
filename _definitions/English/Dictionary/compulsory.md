---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compulsory
offline_file: ""
offline_thumbnail: ""
uuid: b8feb18a-9f30-4556-99dc-04f51c5d11dc
updated: 1484310158
title: compulsory
categories:
    - Dictionary
---
compulsory
     adj : required by rule; "in most schools physical education are
           compulsory"; "attendance is mandatory"; "required
           reading" [syn: {mandatory}, {required}]
