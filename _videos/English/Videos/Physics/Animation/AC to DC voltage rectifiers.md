---
version: 1
type: video
provider: YouTube
id: J8A6QUxfk8c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/AC%20to%20DC%20voltage%20rectifiers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4113cd8e-a77b-49cb-9dff-39718f020821
updated: 1486069654
title: AC to DC voltage rectifiers
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/J8A6QUxfk8c/default.jpg
    - https://i3.ytimg.com/vi/J8A6QUxfk8c/1.jpg
    - https://i3.ytimg.com/vi/J8A6QUxfk8c/2.jpg
    - https://i3.ytimg.com/vi/J8A6QUxfk8c/3.jpg
---
