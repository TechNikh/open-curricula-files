---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_B._Wilson
offline_file: ""
offline_thumbnail: ""
uuid: c0184689-94b0-4ba6-b1ab-40f15799bbc7
updated: 1484309029
title: Robert B. Wilson
tags:
    - Academic career
    - Research
    - Honors
categories:
    - Game theorists
---
Robert Butler "Bob" Wilson, Jr. (born May 16, 1937) is an American economist and the Adams Distinguished Professor of Management, Emeritus at Stanford University. He is known for his contributions to management science and business economics. His doctoral thesis introduced sequential quadratic programming, which became a leading iterative method for nonlinear programming. With other mathematical economists at the Stanford Business School, he helped to reformulate the economics of industrial organization and organization theory using non-cooperative game theory. His research on nonlinear pricing has influenced policies for large firms, particularly in the energy industry, especially ...
