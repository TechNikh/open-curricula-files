---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/allotting
offline_file: ""
offline_thumbnail: ""
uuid: b1286791-aaa3-42a5-96e4-2aba8b9bcea3
updated: 1484310416
title: allotting
categories:
    - Dictionary
---
allot
     v 1: give out or allot; "We were assigned new uniforms" [syn: {assign},
           {portion}]
     2: allow to have; "grant a privilege" [syn: {accord}, {grant}]
     3: administer or bestow, as in small portions; "administer
        critical remarks to everyone present"; "dole out some
        money"; "shell out pocket money for the children"; "deal a
        blow to someone" [syn: {distribute}, {administer}, {mete
        out}, {deal}, {parcel out}, {lot}, {dispense}, {shell out},
         {deal out}, {dish out}, {dole out}]
     [also: {allotting}, {allotted}]
