---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Maximum_common_edge_subgraph_problem
offline_file: ""
offline_thumbnail: ""
uuid: b732900d-1f99-4ede-ab6d-953b450aba57
updated: 1484309392
title: Maximum common edge subgraph problem
categories:
    - Computational problems in graph theory
---
Given two graphs 
  
    
      
        G
      
    
    {\displaystyle G}
  
 and 
  
    
      
        
          G
          ′
        
      
    
    {\displaystyle G'}
  
, the maximum common edge subgraph problem is the problem of finding a graph 
  
    
      
        H
      
    
    {\displaystyle H}
  
 with as many edges as possible which is isomorphic to both a subgraph of 
  
    
      
        G
      
    
    {\displaystyle G}
  
 and a subgraph of 
  
    
      
        
          G
          ′
        
      
    
    {\displaystyle G'}
  
.
