---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perfect_ruler
offline_file: ""
offline_thumbnail: ""
uuid: d4054d98-a086-41ee-8e17-b18c8a453f9f
updated: 1484308603
title: Perfect ruler
categories:
    - Combinatorics
---
A perfect ruler of length 
  
    
      
        n
      
    
    {\displaystyle n}
  
 is a ruler with a subset of the integer markings 
  
    
      
        {
        0
        ,
        
          a
          
            2
          
        
        ,
        ⋯
        ,
        
          a
          
            n
          
        
        }
        ⊂
        {
        0
        ,
        1
        ,
        2
        ,
        …
        ,
        n
        }
      
    
    {\displaystyle \{0,a_{2},\cdots ,a_{n}\}\subset \{0,1,2,\ldots ,n\}}
  
 that appear on a regular ruler. The defining criterion of this subset is that there exists an 
  
    
      
        m
      
 ...
