---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shaving
offline_file: ""
offline_thumbnail: ""
uuid: 63219fde-47ec-4188-a26d-8ac0d98b42ee
updated: 1484310216
title: shaving
categories:
    - Dictionary
---
shaving
     n 1: the act of removing hair with a razor [syn: {shave}]
     2: a thin fragment or slice (especially of wood) that has been
        shaved from something [syn: {paring}, {sliver}]
     3: the act of brushing against while passing [syn: {grazing}, {skimming}]
