---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hilbert_plane
offline_file: ""
offline_thumbnail: ""
uuid: 32a93283-089e-4f66-8255-148ccc8ecba4
updated: 1484309363
title: Hilbert plane
categories:
    - Geometry stubs
---
A Hilbert plane consists of a set of point with certain subsets called lines, and certain relations called betweenness for points on a line, congruence for line segments, and congruence for angles. The have to satisfy some (but not all) of Hilbert's axioms.
