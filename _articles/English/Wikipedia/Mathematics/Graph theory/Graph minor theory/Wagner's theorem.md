---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Wagner%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: bc844414-30af-4067-bc5a-a37b40585c6d
updated: 1484309414
title: "Wagner's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Petersen_Wagner_minors.svg.png
tags:
    - Definitions and theorem statement
    - "History and relation to Kuratowski's theorem"
    - Implications
categories:
    - Graph minor theory
---
In graph theory, Wagner's theorem is a mathematical forbidden graph characterization of planar graphs, named after Klaus Wagner, stating that a finite graph is planar if and only if its minors include neither K5 (the complete graph on five vertices) nor K3,3 (the utility graph, a complete bipartite graph on six vertices). This was one of the earliest results in the theory of graph minors and can be seen as a forerunner of the Robertson–Seymour theorem.
