---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hilbert_system
offline_file: ""
offline_thumbnail: ""
uuid: f41f5810-1af8-4093-9048-4d390e74b755
updated: 1484309495
title: Hilbert system
tags:
    - Formal deductions
    - Logical axioms
    - Conservative extensions
    - Existential quantification
    - Conjunction and Disjunction
    - Metatheorems
    - Alternative axiomatizations
    - Further connections
    - Notes
categories:
    - Proof theory
---
In logic, especially mathematical logic, a Hilbert system, sometimes called Hilbert calculus, Hilbert-style deductive system or Hilbert–Ackermann system, is a type of system of formal deduction attributed to Gottlob Frege[1] and David Hilbert. These deductive systems are most often studied for first-order logic, but are of interest for other logics as well.
