---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/herbivorous
offline_file: ""
offline_thumbnail: ""
uuid: 2c962e80-36b6-4fca-9dcd-38972e2cf754
updated: 1484310266
title: herbivorous
categories:
    - Dictionary
---
herbivorous
     adj : feeding only on plants [ant: {omnivorous}, {insectivorous},
           {carnivorous}]
