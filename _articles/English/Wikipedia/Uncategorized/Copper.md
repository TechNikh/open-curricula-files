---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cupric
offline_file: ""
offline_thumbnail: ""
uuid: f4fa8541-6a9b-441d-bcc5-4e41f4ff78d9
updated: 1484308307
title: Copper
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-NatCopper.jpg
tags:
    - characteristics
    - Physical
    - Chemical
    - Isotopes
    - Occurrence
    - Production
    - Reserves
    - Methods
    - Recycling
    - Alloys
    - Compounds
    - Binary compounds
    - Coordination chemistry
    - Organocopper chemistry
    - Copper(III) and copper(IV)
    - History
    - Copper Age
    - Bronze Age
    - Antiquity and Middle Ages
    - Modern period
    - Applications
    - Wire and cable
    - Electronics and related devices
    - electric motors
    - Architecture
    - Antibiofouling applications
    - Antimicrobial applications
    - Folk medicine
    - Compression clothing
    - Other uses
    - Degradation
    - Biological role
    - Dietary needs
    - Dietary reference intake
    - Copper-based disorders
    - Occupational exposure
    - Notes
categories:
    - Uncategorized
---
Copper is a chemical element with symbol Cu (from Latin: cuprum) and atomic number 29. It is a soft, malleable and ductile metal with very high thermal and electrical conductivity. A freshly exposed surface of pure copper has a reddish-orange color. It is used as a conductor of heat and electricity, as a building material and as a constituent of various metal alloys, such as sterling silver used in jewelry, cupronickel used to make marine hardware and coins and constantan used in strain gauges and thermocouples for temperature measurement.
