---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/persian
offline_file: ""
offline_thumbnail: ""
uuid: 359f5dce-64cf-49b9-8291-04fe98066088
updated: 1484310517
title: persian
categories:
    - Dictionary
---
Persian
     adj : of or relating to Iran or its people or language or culture;
           "Iranian mountains"; "Iranian security police" [syn: {Iranian}]
     n 1: a native or inhabitant of Iran; "the majority of Irani are
          Persian Shiite Muslims" [syn: {Irani}, {Iranian}]
     2: the language of Persia (Iran) in any of its ancient forms
        [syn: {Farsi}]
