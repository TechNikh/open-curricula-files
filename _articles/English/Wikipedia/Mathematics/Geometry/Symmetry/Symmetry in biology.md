---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_in_biology
offline_file: ""
offline_thumbnail: ""
uuid: 7a62e6b0-b047-4bd8-bd77-c53b84b66a98
updated: 1484309319
title: Symmetry in biology
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-SymmetryOfLifeFormsOnEarth.jpg
tags:
    - Radial symmetry
    - Special forms of radial symmetry
    - Spherical symmetry
    - Bilateral symmetry
    - Biradial symmetry
    - Asymmetry
    - Sources
categories:
    - Symmetry
---
Symmetry in biology is the balanced distribution of duplicate body parts or shapes within the body of an organism. In nature and biology, symmetry is always approximate: for example plant leaves, while considered symmetrical, rarely match up exactly when folded in half. Symmetry creates a class of patterns in nature, where the near-repetition of the pattern element is by reflection or rotation. The body plans of most multicellular organisms exhibit some form of symmetry, whether radial, bilateral, or spherical. A small minority, notably among the sponges, exhibit no symmetry (i.e., are asymmetric).
