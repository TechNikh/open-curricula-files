---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conditional_mutual_information
offline_file: ""
offline_thumbnail: ""
uuid: 3526c13f-a5cd-4414-819f-71b4a88dd67f
updated: 1484308764
title: Conditional mutual information
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/256px-VennInfo3Var.svg.png
tags:
    - Definition
    - More general definition
    - Note on notation
    - Multivariate mutual information
categories:
    - Entropy and information
---
In probability theory, and in particular, information theory, the conditional mutual information[1][2] is, in its most basic form, the expected value of the mutual information of two random variables given the value of a third.
