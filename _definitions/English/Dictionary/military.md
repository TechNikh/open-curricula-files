---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/military
offline_file: ""
offline_thumbnail: ""
uuid: 618b77ae-6a67-420c-8f3d-f573799d440e
updated: 1484310477
title: military
categories:
    - Dictionary
---
military
     adj 1: of or relating to the study of the principles of warfare;
            "military law"
     2: characteristic of or associated with soldiers or the
        military; "military uniforms" [ant: {unmilitary}]
     3: associated with or performed by armed services as contrasted
        with civilians; "military police" [ant: {civilian}]
     n : the military forces of a nation; "their military is the
         largest in the region"; "the military machine is the same
         one we faced in 1991 but now it is weaker" [syn: {armed
         forces}, {armed services}, {military machine}, {war
         machine}]
