---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sweeping
offline_file: ""
offline_thumbnail: ""
uuid: 5f61b210-ed28-48e8-8d8c-731582450e3e
updated: 1484310531
title: sweeping
categories:
    - Dictionary
---
sweeping
     adj 1: taking in or moving over (or as if over) a wide area; often
            used in combination; "a sweeping glance"; "a
            wide-sweeping view of the river"
     2: ignoring distinctions; "sweeping generalizations";
        "wholesale destruction" [syn: {wholesale}]
     3: having broad range or effect; "had extensive press
        coverage"; "far-reaching changes in the social structure";
        "sweeping reforms" [syn: {extensive}, {far-reaching}]
     n : the act of cleaning with a broom
