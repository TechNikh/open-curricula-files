---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unclean
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484584021
title: unclean
categories:
    - Dictionary
---
unclean
     adj 1: soiled or likely to soil with dirt or grime; "dirty unswept
            sidewalks"; "a child in dirty overalls"; "dirty
            slums"; "piles of dirty dishes"; "put his dirty feet
            on the clean sheet"; "wore an unclean shirt"; "mining
            is a dirty job"; "Cinderella did the dirty work while
            her sisters preened themselves" [syn: {dirty}, {soiled}]
            [ant: {clean}]
     2: ritually unclean or impure; "and the swine...is unclean to
        you"-Leviticus 11:3 [syn: {impure}] [ant: {clean}]
