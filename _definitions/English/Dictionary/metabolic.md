---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/metabolic
offline_file: ""
offline_thumbnail: ""
uuid: bddde263-d30b-454a-bf2d-2e23e737c347
updated: 1484310389
title: metabolic
categories:
    - Dictionary
---
metabolic
     adj 1: of or relating to metabolism; "metabolic rate"
     2: undergoing metamorphosis [syn: {metabolous}] [ant: {ametabolic}]
