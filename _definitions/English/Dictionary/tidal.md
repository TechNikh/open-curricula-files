---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tidal
offline_file: ""
offline_thumbnail: ""
uuid: 4dc8022c-fb5b-42e9-a444-c8828dd35d81
updated: 1484310244
title: tidal
categories:
    - Dictionary
---
tidal
     adj : of or relating to or caused by tides; "tidal wave"
