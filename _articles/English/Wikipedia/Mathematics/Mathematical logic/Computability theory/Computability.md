---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computability
offline_file: ""
offline_thumbnail: ""
uuid: 16910afc-b1a8-4526-8f89-065edf81532c
updated: 1484309481
title: Computability
tags:
    - Problems
    - Formal models of computation
    - Power of automata
    - Power of finite state machines
    - Power of pushdown automata
    - Power of Turing machines
    - The halting problem
    - Beyond recursively enumerable languages
    - Concurrency-based models
    - Stronger models of computation
    - Infinite execution
    - Oracle machines
    - Limits of hyper-computation
categories:
    - Computability theory
---
Computability is the ability to solve a problem in an effective manner. It is a key topic of the field of computability theory within mathematical logic and the theory of computation within computer science. The computability of a problem is closely linked to the existence of an algorithm to solve the problem.
