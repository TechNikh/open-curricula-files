---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alternated_octagonal_tiling
offline_file: ""
offline_thumbnail: ""
uuid: 389927a8-186b-4a78-97a7-f2ff3ec99d84
updated: 1484309352
title: Alternated octagonal tiling
tags:
    - Geometry
    - Dual tiling
    - In art
    - Related polyhedra and tiling
categories:
    - Geometry stubs
---
In geometry, the tritetragonal tiling or alternated octagonal tiling is a uniform tiling of the hyperbolic plane. It has Schläfli symbols of {(4,3,3)} or h{8,3}.
