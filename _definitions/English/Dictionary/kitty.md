---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kitty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484422141
title: kitty
categories:
    - Dictionary
---
kitty
     n 1: the combined stakes of the betters [syn: {pool}]
     2: the cumulative amount involved in a game (such as poker)
        [syn: {pot}, {jackpot}]
     3: young domestic cat [syn: {kitten}]
     4: informal terms referring to a domestic cat [syn: {kitty-cat},
         {puss}, {pussy}, {pussycat}]
