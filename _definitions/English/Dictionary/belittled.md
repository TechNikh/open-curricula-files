---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/belittled
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484487541
title: belittled
categories:
    - Dictionary
---
belittled
     adj : made to seem smaller or less (especially in worth); "her
           comments made me feel small" [syn: {diminished}, {small}]
