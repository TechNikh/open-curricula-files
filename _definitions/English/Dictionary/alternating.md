---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alternating
offline_file: ""
offline_thumbnail: ""
uuid: fc378ebd-5fcd-4ba4-a672-58934291d290
updated: 1484310244
title: alternating
categories:
    - Dictionary
---
alternating
     adj 1: of a current that reverses direction at regular intervals;
            "alternating current" [ant: {direct}]
     2: occurring by turns; first one and then the other;
        "alternating feelings of love and hate" [syn: {alternate(a)},
         {alternating(a)}]
