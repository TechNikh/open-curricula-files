---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/position
offline_file: ""
offline_thumbnail: ""
uuid: a6f5f5cd-6d85-4d87-9738-4183c2e28f49
updated: 1484310330
title: position
categories:
    - Dictionary
---
position
     n 1: the particular portion of space occupied by a physical
          object; "he put the lamp back in its place" [syn: {place}]
     2: a point occupied by troops for tactical reasons [syn: {military
        position}]
     3: a way of regarding situations or topics etc.; "consider what
        follows from the positivist view" [syn: {view}, {perspective}]
     4: position or arrangement of the body and its limbs; "he
        assumed an attitude of surrender" [syn: {posture}, {attitude}]
     5: the relative position or standing of things or especially
        persons in a society; "he had the status of a minor"; "the
        novel attained the status of a classic"; "atheists ...
