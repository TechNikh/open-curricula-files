---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jumping
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484493421
title: jumping
categories:
    - Dictionary
---
jumping
     n 1: the act of participating in an athletic competition in which
          you must jump
     2: the act of jumping; propelling yourself off the ground; "he
        advanced in a series of jumps"; "the jumping was
        unexpected" [syn: {jump}]
