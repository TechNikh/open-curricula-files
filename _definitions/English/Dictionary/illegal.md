---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illegal
offline_file: ""
offline_thumbnail: ""
uuid: 7ede87d9-193b-498f-ad74-c4a4742cb5ab
updated: 1484310174
title: illegal
categories:
    - Dictionary
---
illegal
     adj : prohibited by law or by official or accepted rules; "an
           illegal chess move" [ant: {legal}]
