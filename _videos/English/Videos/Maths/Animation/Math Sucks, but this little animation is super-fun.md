---
version: 1
type: video
provider: YouTube
id: G2IFTNXJrnI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 60517e98-b7bf-4aac-b564-1c5703d96abb
updated: 1486069651
title: Math Sucks, but this little animation is super-fun
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/G2IFTNXJrnI/default.jpg
    - https://i3.ytimg.com/vi/G2IFTNXJrnI/1.jpg
    - https://i3.ytimg.com/vi/G2IFTNXJrnI/2.jpg
    - https://i3.ytimg.com/vi/G2IFTNXJrnI/3.jpg
---
