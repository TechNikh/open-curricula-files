---
version: 1
type: video
provider: YouTube
id: 4nDwfznKxaU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Anti%20Gravity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fa1fb61c-12b1-4ea7-b494-53fc42e108bf
updated: 1486069654
title: Anti Gravity
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/4nDwfznKxaU/default.jpg
    - https://i3.ytimg.com/vi/4nDwfznKxaU/1.jpg
    - https://i3.ytimg.com/vi/4nDwfznKxaU/2.jpg
    - https://i3.ytimg.com/vi/4nDwfznKxaU/3.jpg
---
