---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Euclid%27s_Elements'
offline_file: ""
offline_thumbnail: ""
uuid: b2dc34fc-9555-4b2f-ab63-cde0361fba2a
updated: 1484309244
title: "Euclid's Elements"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-P._Oxy._I_29.jpg
tags:
    - History
    - Basis in earlier work
    - Transmission of the text
    - Influence
    - Outline of Elements
    - Contents of the books
    - "Euclid's method and style of presentation"
    - Criticism
    - Apocrypha
    - Editions
    - Translations
    - Currently in print
    - Free versions
    - Notes
categories:
    - History of geometry
---
Euclid's Elements (Ancient Greek: Στοιχεῖα Stoicheia) is a mathematical and geometric treatise consisting of 13 books attributed to the ancient Greek mathematician Euclid in Alexandria, Ptolemaic Egypt circa 300 BC. It is a collection of definitions, postulates (axioms), propositions (theorems and constructions), and mathematical proofs of the propositions. The books cover Euclidean geometry and the ancient Greek version of elementary number theory. The work also includes an algebraic system that has become known as geometric algebra, which is powerful enough to solve many algebraic problems,[1] including the problem of finding the square root of a number.[2] Elements is the ...
