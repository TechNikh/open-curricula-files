---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suspend
offline_file: ""
offline_thumbnail: ""
uuid: 36afcc06-14e7-42f3-9c83-882b6b029194
updated: 1484310591
title: suspend
categories:
    - Dictionary
---
suspend
     v 1: hang freely; "The secret police suspended their victims from
          the ceiling and beat them"
     2: cause to be held in suspension in a fluid; "suspend the
        particles"
     3: bar temporarily; from school, office, etc. [syn: {debar}]
     4: stop a process or a habit by imposing a freeze on it;
        "Suspend the aid to the war-torn country" [syn: {freeze}]
     5: make inoperative or stop; "suspend payments on the loan"
        [syn: {set aside}]
     6: as of a prison sentence
