---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emphasizing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484603341
title: emphasizing
categories:
    - Dictionary
---
emphasizing
     n : the act of giving special importance or significance to
         something [syn: {accenting}, {accentuation}]
