---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evolutionary
offline_file: ""
offline_thumbnail: ""
uuid: c5004492-b1af-4813-88ec-407d6adf0b28
updated: 1484310286
title: evolutionary
categories:
    - Dictionary
---
evolutionary
     adj : of or relating to or produced by evolution; "evolutionary
           biology"
