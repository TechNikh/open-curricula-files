---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Proofs_of_Fermat%27s_little_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 66081e32-6727-4f76-bb82-7ac189806e07
updated: 1484308639
title: "Proofs of Fermat's little theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/166px-Proofs-of-Fermats-Little-Theorem-bracelet1.svg.png
tags:
    - Simplifications
    - Combinatorial proofs
    - Proof by counting necklaces
    - Necklaces
    - Completing the proof
    - Proof using dynamical systems
    - Multinomial proofs
    - Proof using the binomial theorem
    - Proof using the multinomial expansion
    - Proof using power product expansions
    - Proof using modular arithmetic
    - An example
    - The cancellation law
    - The rearrangement property
    - "Applications to Euler's theorem"
    - Proofs using group theory
    - Standard proof
    - "Euler's proof"
    - Notes
categories:
    - Number theory
---
for every prime number p and every integer a (see modular arithmetic).
