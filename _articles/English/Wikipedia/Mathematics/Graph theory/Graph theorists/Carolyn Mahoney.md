---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carolyn_Mahoney
offline_file: ""
offline_thumbnail: ""
uuid: 15d6151c-4c19-4c2e-9929-a8f59bd68f91
updated: 1484309470
title: Carolyn Mahoney
tags:
    - Early life and education
    - Career
    - Contributions
    - Awards and honors
categories:
    - Graph theorists
---
Carolyn Ray Boone Mahoney (born 1946) is an American mathematician who served as president of Lincoln University of Missouri.[1] Her research interests include combinatorics, graph theory, and matroids.[2]
