---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accustomed
offline_file: ""
offline_thumbnail: ""
uuid: 974daca2-1d84-4fc6-bba7-99b37dff4a15
updated: 1484310599
title: accustomed
categories:
    - Dictionary
---
accustomed
     adj 1: (often followed by `to') in the habit of or adapted to;
            "accustomed to doing her own work"; "I've grown
            accustomed to her face" [ant: {unaccustomed}]
     2: commonly used or practiced; usual; "his accustomed
        thoroughness"; "took his customary morning walk"; "his
        habitual comment"; "with her wonted candor" [syn: {customary},
         {habitual}, {wonted(a)}]
