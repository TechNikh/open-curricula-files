---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ladakh
offline_file: ""
offline_thumbnail: ""
uuid: 2f18f0d7-6a43-41b4-a7a0-79d8d584ebbd
updated: 1484308447
title: Ladakh
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-thumbnail.jpg
tags:
    - History
    - geography
    - Panorama
    - Flora and Fauna
    - Government and politics
    - Economy
    - Astronomy
    - Transport
    - Demographics
    - culture
    - Cuisine
    - Music and dance
    - Sports
    - Social status of women
    - Traditional medicine
    - Festivals of Ladakh
    - Organizations
    - Education
    - Media
    - In popular culture
    - Notes
    - Citations
    - Sources
categories:
    - Uncategorized
---
Ladakh ("land of high passes") (Ladakhi: ལ་དྭགས la'dwags; Hindi: लद्दाख़ laddākh; Urdu: لَدّاخ‎) is a region in Indian state of Jammu and Kashmir that currently extends from the Kunlun mountain range[3] to the main Great Himalayas to the south, inhabited by people of Indo-Aryan and Tibetan descent.[4][5] It is one of the most sparsely populated regions in Jammu and Kashmir and its culture and history are closely related to that of Tibet.
