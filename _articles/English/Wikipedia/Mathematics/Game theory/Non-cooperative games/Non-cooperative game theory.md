---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-cooperative_game_theory
offline_file: ""
offline_thumbnail: ""
uuid: b9c7bb5f-1683-4318-af1c-9fdc37d6c719
updated: 1484309050
title: Non-cooperative game theory
categories:
    - Non-cooperative games
---
In game theory, a non-cooperative game is a game with competition between individual players and in which only self-enforcing (e.g. through credible threats) alliances (or competition between groups of players, called "coalitions") are possible due to the absence of external means to enforce cooperative behavior (e.g. contract law), as opposed to cooperative games.
