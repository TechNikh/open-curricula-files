---
version: 1
type: video
provider: YouTube
id: sG1ZAdYi13A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Thermodynamics/Gibbs%20free%20energy%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b2e7a1c5-2991-41ee-8823-7935073a42fd
updated: 1486069587
title: Gibbs free energy example
categories:
    - Thermodynamics
thumbnail_urls:
    - https://i3.ytimg.com/vi/sG1ZAdYi13A/default.jpg
    - https://i3.ytimg.com/vi/sG1ZAdYi13A/1.jpg
    - https://i3.ytimg.com/vi/sG1ZAdYi13A/2.jpg
    - https://i3.ytimg.com/vi/sG1ZAdYi13A/3.jpg
---
Determining if a reaction is spontaneous by calculating the change in Gibbs free energy. Also calculates the change in entropy using table of standard entropies. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/thermodynamics-chemistry/gibbs-free-energy/v/more-rigorous-gibbs-free-energy-spontaneity-relationship?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/thermodynamics-chemistry/gibbs-free-energy/v/gibbs-free-energy-and-spontaneity?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
