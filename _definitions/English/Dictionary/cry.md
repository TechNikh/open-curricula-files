---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484514241
title: cry
categories:
    - Dictionary
---
cry
     n 1: a loud utterance; often in protest or opposition; "the
          speaker was interrupted by loud cries from the rear of
          the audience" [syn: {outcry}, {call}, {yell}, {shout}, {vociferation}]
     2: a loud utterance of emotion (especially when inarticulate);
        "a cry of rage"; "a yell of pain" [syn: {yell}]
     3: a slogan used to rally support for a cause; "a cry to arms";
        "our watchword will be `democracy'" [syn: {war cry}, {rallying
        cry}, {battle cry}, {watchword}]
     4: a fit of weeping; "had a good cry"
     5: the characteristic utterance of an animal; "animal cries
        filled the night"
     v 1: utter a sudden loud cry; "she cried ...
