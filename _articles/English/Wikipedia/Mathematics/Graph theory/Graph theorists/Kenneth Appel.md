---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kenneth_Appel
offline_file: ""
offline_thumbnail: ""
uuid: a9f7785d-30e4-4fc1-8e8f-e93d818294f4
updated: 1484309457
title: Kenneth Appel
tags:
    - Biography
    - Schooling and teaching
    - Contributions to mathematics
    - The four color theorem
    - Group theory
categories:
    - Graph theorists
---
Kenneth Ira Appel (October 8, 1932 – April 19, 2013) was an American mathematician who in 1976, with colleague Wolfgang Haken at the University of Illinois at Urbana-Champaign, solved one of the most famous problems in mathematics, the four-color theorem. They proved that any two-dimensional map, with certain limitations, can be filled in with four colors without any adjacent "countries" sharing the same color.
