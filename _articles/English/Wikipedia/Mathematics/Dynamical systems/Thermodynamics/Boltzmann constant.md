---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Boltzmann_constant
offline_file: ""
offline_thumbnail: ""
uuid: 63dcbafe-ed94-48b7-b925-e4c8791e52e7
updated: 1484308834
title: Boltzmann constant
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Zentralfriedhof_Vienna_-_Boltzmann.JPG
tags:
    - Bridge from macroscopic to microscopic physics
    - Role in the equipartition of energy
    - Application to simple gas thermodynamics
    - Role in Boltzmann factors
    - Role in the statistical definition of entropy
    - 'Role in semiconductor physics: the thermal voltage'
    - History
    - Value in different units
    - Planck units
categories:
    - Thermodynamics
---
The Boltzmann constant (kB or k), named after Ludwig Boltzmann, is a physical constant relating the average kinetic energy of particles in a gas with the temperature of the gas.[2] It is the gas constant R divided by the Avogadro constant NA:
