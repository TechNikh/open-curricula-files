---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Handshaking_lemma
offline_file: ""
offline_thumbnail: ""
uuid: 875039f4-4d9c-4ff2-850c-92bd382e0709
updated: 1484308611
title: Handshaking lemma
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-6n-graf.svg_0.png
tags:
    - Proof
    - Regular graphs
    - Infinite graphs
    - Exchange graphs
    - Computational complexity
    - Other applications
    - Notes
categories:
    - Graph theory
---
In graph theory, a branch of mathematics, the handshaking lemma is the statement that every finite undirected graph has an even number of vertices with odd degree (the number of edges touching the vertex). In more colloquial terms, in a party of people some of whom shake hands, an even number of people must have shaken an odd number of other people's hands.
