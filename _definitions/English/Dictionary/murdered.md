---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/murdered
offline_file: ""
offline_thumbnail: ""
uuid: ca67af08-6153-469c-89a8-d9219c6d21d3
updated: 1484310188
title: murdered
categories:
    - Dictionary
---
murdered
     adj : killed unlawfully; "the murdered woman"; "lay a wreath on
           murdered Lincoln's bier"
