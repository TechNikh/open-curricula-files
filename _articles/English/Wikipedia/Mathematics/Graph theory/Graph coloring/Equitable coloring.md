---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equitable_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 88bfaf78-8e62-49bb-b695-66067eab9756
updated: 1484309382
title: Equitable coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Equitable-K15.svg.png
tags:
    - Examples
    - Hajnal–Szemerédi theorem
    - Special classes of graphs
    - Computational complexity
    - Applications
    - Notes
categories:
    - Graph coloring
---
That is, the partition of vertices among the different colors is as uniform as possible. For instance, giving each vertex a distinct color would be equitable, but would typically use many more colors than are necessary in an optimal equitable coloring. An equivalent way of defining an equitable coloring is that it is an embedding of the given graph as a subgraph of a Turán graph. There are two kinds of chromatic number associated with equitable coloring.[1] The equitable chromatic number of a graph G is the smallest number k such that G has an equitable coloring with k colors. But G might not have equitable colorings for some larger numbers of colors; the equitable chromatic threshold of G ...
