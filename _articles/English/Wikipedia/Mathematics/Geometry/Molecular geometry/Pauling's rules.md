---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pauling%27s_rules'
offline_file: ""
offline_thumbnail: ""
uuid: 7c69c2df-c272-465c-ac56-4058382dcf05
updated: 1484309279
title: "Pauling's rules"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Criticalradiusratio.png
tags:
    - 'First rule: the radius ratio rule'
    - 'Second rule: the electrostatic valence rule'
    - 'Third rule: sharing of polyhedron corners, edges and faces'
    - 'Fourth rule: crystals containing different cations'
    - 'Fifth rule: the rule of parsimony'
categories:
    - Molecular geometry
---
