---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sporulation
offline_file: ""
offline_thumbnail: ""
uuid: c84559a7-48a2-4ef7-acb9-3f084690b8b5
updated: 1484310160
title: sporulation
categories:
    - Dictionary
---
sporulation
     n : asexual reproduction by the production and release of spores
         [syn: {monogenesis}]
