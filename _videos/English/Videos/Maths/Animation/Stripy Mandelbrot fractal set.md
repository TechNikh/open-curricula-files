---
version: 1
type: video
provider: YouTube
id: anUKUMptvpk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 21df4f54-efa4-4130-86a4-3d7c4dc23287
updated: 1486069648
title: Stripy Mandelbrot fractal set
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/anUKUMptvpk/default.jpg
    - https://i3.ytimg.com/vi/anUKUMptvpk/1.jpg
    - https://i3.ytimg.com/vi/anUKUMptvpk/2.jpg
    - https://i3.ytimg.com/vi/anUKUMptvpk/3.jpg
---
