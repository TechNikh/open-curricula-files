---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Banach%E2%80%93Mazur_compactum'
offline_file: ""
offline_thumbnail: ""
uuid: e4ad5dd9-d3ab-4701-a02b-8df1d7520b45
updated: 1484309261
title: Banach–Mazur compactum
categories:
    - Metric geometry
---
In the mathematical study of functional analysis, the Banach–Mazur distance is a way to define a distance on the set Q(n) of n-dimensional normed spaces. If X and Y are two finite-dimensional normed spaces with the same dimension, let GL(X,Y) denote the collection of all linear isomorphisms T : X → Y. The Banach–Mazur distance between X and Y is defined by
