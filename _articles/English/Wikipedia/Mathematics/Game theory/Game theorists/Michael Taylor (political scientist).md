---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Michael_Taylor_(political_scientist)
offline_file: ""
offline_thumbnail: ""
uuid: 6d071b97-5016-4363-be49-768e662460ec
updated: 1484309026
title: Michael Taylor (political scientist)
categories:
    - Game theorists
---
Taylor completed his PhD at the University of Essex in the United Kingdom. He has taught at Essex and at Yale University and was previously Visiting Professor or Fellow has held visiting positions at the Center for Advanced Study at Stanford University, the Netherlands Institute of Advanced Studies, the Institute of Advanced Studies in Vienna, the European University Institute in Florence, and at the Australian National University in Canberra.
