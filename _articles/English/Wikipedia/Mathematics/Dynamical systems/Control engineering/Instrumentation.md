---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Instrumentation
uuid: b7f6784f-93ae-4066-9b3a-cc90f08b52c9
updated: 1480455208
title: Instrumentation
tags:
    - History and development
    - Pre-industrial
    - Early industrial
    - Automatic process control
    - Large integrated computer-based systems
    - Applications
    - Household
    - Automotive
    - Aircraft
    - Laboratory instrumentation
    - Measurement Parameters
    - Instrumentation engineering
    - Typical Industrial Transmitter Signal Types
    - Analysis of development
categories:
    - Control engineering
---
The term instrumentation may refer to something as simple as direct reading thermometers or, when using many sensors, may become part of a complex Industrial control system in such as manufacturing industry, vehicles and transportation. Instrumentation can be found in the household as well; a smoke detector or a heating thermostat are examples.
