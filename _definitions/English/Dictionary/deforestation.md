---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deforestation
offline_file: ""
offline_thumbnail: ""
uuid: da11da49-cb63-4a55-ba72-6a1d45b2c9d5
updated: 1484310249
title: deforestation
categories:
    - Dictionary
---
deforestation
     n 1: the state of being clear of trees
     2: the removal of trees [syn: {disforestation}]
