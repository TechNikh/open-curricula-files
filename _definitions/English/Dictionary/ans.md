---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ans
offline_file: ""
offline_thumbnail: ""
uuid: d69708ef-d026-4de7-80df-1c19794b2d56
updated: 1484310316
title: ans
categories:
    - Dictionary
---
ANS
     n : the part of the nervous system of vertebrates that controls
         involuntary actions of the smooth muscles and heart and
         glands [syn: {autonomic nervous system}]
