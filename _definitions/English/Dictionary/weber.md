---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weber
offline_file: ""
offline_thumbnail: ""
uuid: 02b9aff3-6671-44e4-a8dd-e309ad368cb3
updated: 1484310194
title: weber
categories:
    - Dictionary
---
weber
     n 1: a unit of magnetic flux equal to 100,000,000 maxwells [syn:
          {Wb}]
     2: German physicist and brother of E. H. Weber; noted for his
        studies of terrestrial magnetism (1804-1891) [syn: {Wilhelm
        Eduard Weber}]
     3: United States abstract painter (born in Russia) (1881-1961)
        [syn: {Max Weber}]
     4: German sociologist and pioneer of the analytic method in
        sociology (1864-1920) [syn: {Max Weber}]
     5: German conductor and composer of Romantic operas (1786-1826)
        [syn: {Carl Maria von Weber}, {Baron Karl Maria Friedrich
        Ernst von Weber}]
     6: German physiologist who studied sensory responses to stimuli
        ...
