---
version: 1
type: article
id: https://en.wikipedia.org/wiki/De_Bruijn_torus
offline_file: ""
offline_thumbnail: ""
uuid: 1097b671-bb97-4df4-b834-96b9919d39d5
updated: 1484308565
title: De Bruijn torus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-2-2-4-4-de-Bruijn-torus.svg.png
tags:
    - B2
    - 'Larger example: B4'
    - 'Practical consideration for construction of de Bruijn tori B6 & B8'
categories:
    - Combinatorics
---
In combinatorial mathematics, a De Bruijn torus, named after Nicolaas Govert de Bruijn, is an array of symbols from an alphabet (often just 0 and 1) that contains every m-by-n matrix exactly once. It is a torus because the edges are considered wraparound for the purpose of finding matrices. Its name comes from the De Bruijn sequence, which can be considered a special case where n is 1 (one dimension).
