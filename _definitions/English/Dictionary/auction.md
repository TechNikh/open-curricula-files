---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/auction
offline_file: ""
offline_thumbnail: ""
uuid: 9f1ef035-d1eb-4ab6-97eb-f23886d22eb9
updated: 1484310168
title: auction
categories:
    - Dictionary
---
auction
     n 1: a variety of bridge in which tricks made in excess of the
          contract are scored toward game; now generally
          superseded by contract bridge [syn: {auction bridge}]
     2: the public sale of something to the highest bidder [syn: {auction
        sale}, {vendue}]
     v : sell at an auction [syn: {auction off}, {auctioneer}]
