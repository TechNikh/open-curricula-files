---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/separated
offline_file: ""
offline_thumbnail: ""
uuid: 5730ce0b-f8ae-4496-9c90-71fa676018c1
updated: 1484310289
title: separated
categories:
    - Dictionary
---
separated
     adj 1: being or feeling set or kept apart from others; "she felt
            detached from the group"; "could not remain the
            isolated figure he had been"- Sherwood Anderson;
            "thought of herself as alone and separated from the
            others"; "had a set-apart feeling" [syn: {detached}, {isolated},
             {set-apart}]
     2: spaced apart [syn: {spaced}]
     3: not living together as man and wife; "decided to live
        apart"; "maintaining separate households"; "they are
        separated" [syn: {apart(p)}, {separate}]
     4: separated at the joint; "a dislocated knee"; "a separated
        shoulder" [syn: {disjointed}, {dislocated}]
     ...
