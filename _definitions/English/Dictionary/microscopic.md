---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/microscopic
offline_file: ""
offline_thumbnail: ""
uuid: b5d4eb04-3154-4e06-8418-23795db4ea0d
updated: 1484310264
title: microscopic
categories:
    - Dictionary
---
microscopic
     adj 1: of or relating to or used in microscopy; "microscopic
            analysis"; "microscopical examination" [syn: {microscopical}]
     2: too small to be seen except under a microscope [syn: {microscopical},
         {small}] [ant: {macroscopic}]
     3: extremely precise with great attention to details; "examined
        it with microscopic care"
     4: infinitely or immeasurably small; "two minute whiplike
        threads of protoplasm"; "reduced to a microscopic scale"
        [syn: {infinitesimal}, {minute}]
