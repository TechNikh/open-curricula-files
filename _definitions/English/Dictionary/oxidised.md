---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oxidised
offline_file: ""
offline_thumbnail: ""
uuid: b87eebd5-3856-43a9-b8a2-583e3d70f517
updated: 1484310323
title: oxidised
categories:
    - Dictionary
---
oxidised
     adj : combined with or having undergone a chemical reaction with
           oxygen; "the oxidized form of iodine" [syn: {oxidized}]
