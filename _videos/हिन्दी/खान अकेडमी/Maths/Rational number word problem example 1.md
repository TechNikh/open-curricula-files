---
version: 1
type: video
provider: YouTube
id: u_2tRyRy6-U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b5be5927-a268-4117-b623-7e2c70da0295
updated: 1486069611
title: Rational number word problem example 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/u_2tRyRy6-U/default.jpg
    - https://i3.ytimg.com/vi/u_2tRyRy6-U/1.jpg
    - https://i3.ytimg.com/vi/u_2tRyRy6-U/2.jpg
    - https://i3.ytimg.com/vi/u_2tRyRy6-U/3.jpg
---
