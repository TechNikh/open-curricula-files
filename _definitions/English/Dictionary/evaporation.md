---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evaporation
offline_file: ""
offline_thumbnail: ""
uuid: 9b88aa64-4f26-47ae-81d6-540a804db968
updated: 1484310260
title: evaporation
categories:
    - Dictionary
---
evaporation
     n 1: the process of becoming a vapor [syn: {vaporization}, {vaporisation},
           {vapor}, {vapour}]
     2: the process of extracting moisture [syn: {dehydration}, {desiccation},
         {drying up}]
