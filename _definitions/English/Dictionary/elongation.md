---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elongation
offline_file: ""
offline_thumbnail: ""
uuid: 34d4483a-8dbb-47aa-8926-05b2cc279d90
updated: 1484310293
title: elongation
categories:
    - Dictionary
---
elongation
     n 1: the quality of being elongated
     2: an addition to the length of something [syn: {extension}]
     3: the act of lengthening something
