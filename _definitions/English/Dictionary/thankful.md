---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thankful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484410501
title: thankful
categories:
    - Dictionary
---
thankful
     adj : feeling or showing gratitude; "a grateful heart"; "grateful
           for the tree's shade"; "a thankful smile" [syn: {grateful}]
           [ant: {ungrateful}]
