---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/copyeditor
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484492581
title: copyeditor
categories:
    - Dictionary
---
copy editor
     n : an editor who prepares text for publication [syn: {copyreader},
          {text editor}]
