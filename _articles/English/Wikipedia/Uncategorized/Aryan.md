---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aryan
offline_file: ""
offline_thumbnail: ""
uuid: 790c3e06-b390-42c8-a770-f26ff312097e
updated: 1484308482
title: Aryan
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Darius_I_the_Great%2527s_inscription.jpg'
tags:
    - Etymology
    - Origins
    - Sanskrit and Avestan
    - Proto-Indo-Iranian
    - Pre-Proto-Indo-Iranians
    - Usage
    - Scholarly usage
    - Contemporary usage
    - Indian and Iranian nationalism
    - Nazism and White Supremacy
categories:
    - Uncategorized
---
"Aryan" (/ˈɛəriən, ˈɛərjən, ˈær-/)[1] is a term meaning "noble" which was used as a self-designation by Indo-Iranian people. The word was used by the Indic people of the Vedic period in India as an ethnic label for themselves, as well as to refer to the noble class and geographic location known as Āryāvarta where Indo-Aryan culture was based.[2][3] The closely related Iranian people also used the term as an ethnic label for themselves in the Avesta scriptures, and the word forms the etymological source of the country Iran.[4][5][6][7] It was believed in the 19th century that it was also a self-designation used by all Proto-Indo-Europeans, a theory that has now been abandoned.[8] ...
