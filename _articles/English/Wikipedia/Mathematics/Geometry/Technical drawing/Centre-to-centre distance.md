---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Centre-to-centre_distance
offline_file: ""
offline_thumbnail: ""
uuid: ea7d6e57-831b-4e8c-a5f8-915d0107152e
updated: 1484309324
title: Centre-to-centre distance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-H_op_h.PNG
categories:
    - Technical drawing
---
It is the distance between the centre (the heart) of a column or pillar and the centre (the heart) of another column or pillar. By expressing a distance in c.t.c., one can measure distances between pillar and columns with different diameters without confusion.
