---
version: 1
type: video
provider: YouTube
id: IUCySS0sqSM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d9c334f4-d44a-45ac-a75e-13ce4a4c411b
updated: 1486069643
title: 'OurAutobiography [Lights] & Fractal Kaleidoscope Mix'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/IUCySS0sqSM/default.jpg
    - https://i3.ytimg.com/vi/IUCySS0sqSM/1.jpg
    - https://i3.ytimg.com/vi/IUCySS0sqSM/2.jpg
    - https://i3.ytimg.com/vi/IUCySS0sqSM/3.jpg
---
