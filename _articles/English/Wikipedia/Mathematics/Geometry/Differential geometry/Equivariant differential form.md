---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equivariant_differential_form
offline_file: ""
offline_thumbnail: ""
uuid: 26acbcca-7ba1-4d87-a829-2f6b7e9e1416
updated: 1484309218
title: Equivariant differential form
categories:
    - Differential geometry
---
from the Lie algebra 
  
    
      
        
          
            g
          
        
        =
        Lie
        ⁡
        (
        G
        )
      
    
    {\displaystyle {\mathfrak {g}}=\operatorname {Lie} (G)}
  
 to the space of differential forms on M that are equivariant; i.e.,
