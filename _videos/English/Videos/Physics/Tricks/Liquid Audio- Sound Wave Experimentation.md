---
version: 1
type: video
provider: YouTube
id: jwMq8mmqgQ4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 557695d2-b3f3-4a24-9fda-3720ee3f2ad2
updated: 1486069660
title: 'Liquid Audio- Sound Wave Experimentation'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/jwMq8mmqgQ4/default.jpg
    - https://i3.ytimg.com/vi/jwMq8mmqgQ4/1.jpg
    - https://i3.ytimg.com/vi/jwMq8mmqgQ4/2.jpg
    - https://i3.ytimg.com/vi/jwMq8mmqgQ4/3.jpg
---
