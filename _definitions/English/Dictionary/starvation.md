---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/starvation
offline_file: ""
offline_thumbnail: ""
uuid: e12d5186-7ede-49ad-a117-112b6ed705f7
updated: 1484310293
title: starvation
categories:
    - Dictionary
---
starvation
     n 1: a state of extreme hunger resulting from lack of essential
          nutrients over a prolonged period [syn: {famishment}]
     2: the act of depriving of food or subjecting to famine; "the
        beseigers used starvation to induce surrender"; "they were
        charged with the starvation of children in their care"
        [syn: {starving}]
