---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concealed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484567941
title: concealed
categories:
    - Dictionary
---
concealed
     adj 1: not accessible to view; "concealed (or hidden) damage"; "in
            stormy weather the stars are out of sight" [syn: {hidden},
             {out of sight}]
     2: concealed or hidden on any grounds for any motive; "a
        concealed weapon"; "a concealed compartment in his
        briefcase" [ant: {unconcealed}]
