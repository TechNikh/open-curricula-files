---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Manin_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: e702bf36-da0f-4031-930c-3ff3aae51dc9
updated: 1484308649
title: Manin conjecture
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rational_points_of_bounded_height_outside_the_27_lines_on_Clebsch%2527s_diagonal_cubic_surface.png'
categories:
    - Number theory
---
In mathematics, the Manin conjecture describes the conjectural distribution of rational points on an algebraic variety relative to a suitable height function. It was proposed by Yuri I. Manin and his collaborators[1] in 1989 when they initiated a program with the aim of describing the distribution of rational points on suitable algebraic varieties.
