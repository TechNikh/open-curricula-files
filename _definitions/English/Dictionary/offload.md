---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/offload
offline_file: ""
offline_thumbnail: ""
uuid: 90a92e27-1ee2-45ce-a500-9e517c934bd9
updated: 1484310486
title: offload
categories:
    - Dictionary
---
offload
     v 1: transfer to a peripheral device, of computer data
     2: take the load off (a container or vehicle); "unload the
        truck"; "offload the van" [syn: {unload}]
