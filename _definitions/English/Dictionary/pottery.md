---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pottery
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484322181
title: pottery
categories:
    - Dictionary
---
pottery
     n 1: ceramic ware made from clay and baked in a kiln [syn: {clayware}]
     2: the craft of making earthenware
     3: a workshop where clayware is made
