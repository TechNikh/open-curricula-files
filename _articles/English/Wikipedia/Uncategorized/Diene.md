---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diene
offline_file: ""
offline_thumbnail: ""
uuid: d1019df7-69c3-4c25-a884-4f2f30ae4216
updated: 1484308403
title: Diene
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/150px-1%252C3-butadiene.svg.png'
tags:
    - Classes
    - Synthesis of dienes
    - Reactivity and uses
    - Polymerization
    - Cycloadditions
    - Other addition reactions
    - Metathesis reactions
    - Acidity
    - As ligands
categories:
    - Uncategorized
---
In organic chemistry a diene (/ˈdaɪ.iːn/ DY-een) or diolefin (/daɪˈoʊləfᵻn/ dy-OH-lə-fin) is a hydrocarbon that contains two carbon double bonds. Dienes occur occasionally in nature. Conjugated dienes are widely used as monomers in the polymer industry.
