---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/militarism
offline_file: ""
offline_thumbnail: ""
uuid: 97bec360-4509-4c11-8f07-23388a60a06c
updated: 1484310553
title: militarism
categories:
    - Dictionary
---
militarism
     n : a political orientation of a people or a government to
         maintain a strong military force and to be prepared to
         use it aggresively to defend or promote national
         interests
