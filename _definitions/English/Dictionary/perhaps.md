---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perhaps
offline_file: ""
offline_thumbnail: ""
uuid: d8cae67b-739e-49b5-89d4-4a84cc3e37d3
updated: 1484310254
title: perhaps
categories:
    - Dictionary
---
perhaps
     adv : by chance; "perhaps she will call tomorrow"; "we may
           possibly run into them at the concert"; "it may
           peradventure be thought that there never was such a
           time" [syn: {possibly}, {perchance}, {maybe}, {mayhap},
            {peradventure}]
