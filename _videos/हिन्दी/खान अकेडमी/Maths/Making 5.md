---
version: 1
type: video
provider: YouTube
id: tsy8bC84q3U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 60c0d083-682d-426e-bf61-d8702369e9e2
updated: 1486069614
title: Making 5
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/tsy8bC84q3U/default.jpg
    - https://i3.ytimg.com/vi/tsy8bC84q3U/1.jpg
    - https://i3.ytimg.com/vi/tsy8bC84q3U/2.jpg
    - https://i3.ytimg.com/vi/tsy8bC84q3U/3.jpg
---
