---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deserts
offline_file: ""
offline_thumbnail: ""
uuid: 628e2a9c-77c0-4911-8a70-9d72896790f8
updated: 1484310275
title: deserts
categories:
    - Dictionary
---
deserts
     n : an outcome (good or bad) that is well deserved [syn: {comeuppance},
          {comeupance}]
