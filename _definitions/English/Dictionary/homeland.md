---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/homeland
offline_file: ""
offline_thumbnail: ""
uuid: f7f344ee-d743-45b2-b7f3-81d2c1bab488
updated: 1484310178
title: homeland
categories:
    - Dictionary
---
homeland
     n : the country where you were born [syn: {fatherland}, {motherland},
          {mother country}, {country of origin}, {native land}]
