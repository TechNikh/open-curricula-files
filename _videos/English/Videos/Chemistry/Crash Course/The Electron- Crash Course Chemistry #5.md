---
version: 1
type: video
provider: YouTube
id: rcKilE9CdaA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/The%20Electron-%20Crash%20Course%20Chemistry%20%235.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9a14926b-4c26-4b5e-b46d-569ea2659e78
updated: 1486069680
title: 'The Electron- Crash Course Chemistry #5'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/rcKilE9CdaA/default.jpg
    - https://i3.ytimg.com/vi/rcKilE9CdaA/1.jpg
    - https://i3.ytimg.com/vi/rcKilE9CdaA/2.jpg
    - https://i3.ytimg.com/vi/rcKilE9CdaA/3.jpg
---
