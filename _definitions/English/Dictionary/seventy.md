---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seventy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484453101
title: seventy
categories:
    - Dictionary
---
seventy
     adj : being ten more than sixty [syn: {70}, {lxx}]
     n : the cardinal number that is the product of ten and seven
         [syn: {70}, {LXX}]
