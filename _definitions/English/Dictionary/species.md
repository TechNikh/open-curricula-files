---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/species
offline_file: ""
offline_thumbnail: ""
uuid: 25254a68-cce2-4ac9-928f-1953936abb94
updated: 1484310299
title: species
categories:
    - Dictionary
---
species
     n 1: (biology) taxonomic group whose members can interbreed
     2: a specific kind of something; "a species of molecule"; "a
        species of villainy"
