---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wax
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484367241
title: wax
categories:
    - Dictionary
---
wax
     n : any of various substances of either mineral origin or plant
         or animal origin; they are solid at normal temperatures
         and insoluble in water
     v 1: cover with wax; "wax the car"
     2: go up or advance; "Sales were climbing after prices were
        lowered" [syn: {mount}, {climb}, {rise}] [ant: {wane}]
     3: increase in phase; "the moon is waxing" [syn: {full}] [ant:
        {wane}]
