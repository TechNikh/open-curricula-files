---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/election
offline_file: ""
offline_thumbnail: ""
uuid: e2e38ceb-0212-438f-a997-7130ead6ba71
updated: 1484310587
title: election
categories:
    - Dictionary
---
election
     n 1: a vote to select the winner of a position or political
          office; "the results of the election will be announced
          tonight"
     2: the act of selecting someone or something; the exercise of
        deliberate choice; "her election of medicine as a
        profession"
     3: the status or fact of being elected; "they celebrated his
        election"
     4: the predestination of some individuals as objects of divine
        mercy (especially as conceived by Calvinists)
