---
version: 1
type: video
provider: YouTube
id: yXM3chrKFw8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6e07cb68-5e8b-4f1c-9124-98072da5aae0
updated: 1486069607
title: 'Order by length  Measurement and data'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/yXM3chrKFw8/default.jpg
    - https://i3.ytimg.com/vi/yXM3chrKFw8/1.jpg
    - https://i3.ytimg.com/vi/yXM3chrKFw8/2.jpg
    - https://i3.ytimg.com/vi/yXM3chrKFw8/3.jpg
---
