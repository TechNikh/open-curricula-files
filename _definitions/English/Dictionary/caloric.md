---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/caloric
offline_file: ""
offline_thumbnail: ""
uuid: 369cb7de-f752-4bd7-bc3d-366cd9637e04
updated: 1484310535
title: caloric
categories:
    - Dictionary
---
caloric
     adj 1: relating to or associated with heat; "thermal movements of
            molecules"; "thermal capacity"; "thermic energy"; "the
            caloric effect of sunlight" [syn: {thermal}, {thermic}]
            [ant: {nonthermal}]
     2: of or relating to calories in food; "comparison of foods on
        a caloric basis"; "the caloric content of foods"
