---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Normal_direction
offline_file: ""
offline_thumbnail: ""
uuid: 04608518-9943-4505-9807-c97afb1856bf
updated: 1484308371
title: Normal (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Normal_vectors2.svg.png
tags:
    - Normal to surfaces in 3D space
    - Calculating a surface normal
    - Uniqueness of the normal
    - Transforming normals
    - Hypersurfaces in n-dimensional space
    - >
        Varieties defined by implicit equations in n-dimensional
        space
    - Example
    - Uses
    - Normal in geometric optics
categories:
    - Uncategorized
---
In geometry, a normal is an object such as a line or vector that is perpendicular to a given object. For example, in the two-dimensional case, the normal line to a curve at a given point is the line perpendicular to the tangent line to the curve at the point.
