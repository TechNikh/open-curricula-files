---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immortal
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484521621
title: immortal
categories:
    - Dictionary
---
immortal
     adj : not subject to death [ant: {mortal}]
     n 1: a person (such as an author) of enduring fame; "Shakespeare
          is one of the immortals"
     2: any supernatural being worshipped as controlling some part
        of the world or some aspect of life or who is the
        personification of a force [syn: {deity}, {divinity}, {god}]
