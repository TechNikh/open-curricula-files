---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stereo_cameras
offline_file: ""
offline_thumbnail: ""
uuid: d825b227-093f-41cf-8426-cfb1a48ba434
updated: 1484309184
title: Stereo cameras
categories:
    - Geometry in computer vision
---
The stereo cameras approach is a method of distilling a noisy video signal into a coherent data set that a computer can begin to process into actionable symbolic objects, or abstractions. Stereo cameras is one of many approaches used in the broader fields of computer vision and machine vision.
