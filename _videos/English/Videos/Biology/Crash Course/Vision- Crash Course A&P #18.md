---
version: 1
type: video
provider: YouTube
id: o0DYP-u1rNM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f0a57be0-3e34-4be6-8492-ad5b7f65210a
updated: 1486069681
title: 'Vision- Crash Course A&P #18'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/o0DYP-u1rNM/default.jpg
    - https://i3.ytimg.com/vi/o0DYP-u1rNM/1.jpg
    - https://i3.ytimg.com/vi/o0DYP-u1rNM/2.jpg
    - https://i3.ytimg.com/vi/o0DYP-u1rNM/3.jpg
---
