---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Josephson_vortex
offline_file: ""
offline_thumbnail: ""
uuid: ee673bca-e98f-4487-aaaf-d01b108e7540
updated: 1484308875
title: Josephson vortex
categories:
    - Vortices
---
In superconductivity, a Josepshon vortex (after Brian Josephson from Cambridge University) is a quantum vortex of supercurrents in a Josephson junction (see Josephson effect) The supercurrents circulate around the vortex center which is situated inside the Josephson barrier, unlike Abrikosov vortices in type-II superconductors, which are located in the superconducting condensate.
