---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evidence
offline_file: ""
offline_thumbnail: ""
uuid: 06b25029-f259-44cf-8e9b-5bc80796653d
updated: 1484310313
title: evidence
categories:
    - Dictionary
---
evidence
     n 1: your basis for belief or disbelief; knowledge on which to
          base belief; "the evidence that smoking causes lung
          cancer is very compelling" [syn: {grounds}]
     2: an indication that makes something evident; "his trembling
        was evidence of his fear"
     3: (law) all the means by which any alleged matter of fact
        whose truth is investigated at judicial trial is
        established or disproved
     v 1: provide evidence for; stand as proof of; show by one's
          behavior, attitude, or external attributes; "His high
          fever attested to his illness"; "The buildings in Rome
          manifest a high level of architectural ...
