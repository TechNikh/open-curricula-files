---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Proportional_cake-cutting_with_different_entitlements
offline_file: ""
offline_thumbnail: ""
uuid: 4e5eb6d9-d7d5-48ac-ab3f-23745976241d
updated: 1484308520
title: Proportional cake-cutting with different entitlements
tags:
    - Cloning
    - Ramsey partitions
    - Cut-near-halves
    - Exact divisions
    - Impossibility result
categories:
    - Fair division
---
In the fair cake-cutting problem, the partners often have different entitlements. For example, the resource may belong to two shareholders such that Alice holds 8/13 and George holds 5/13. This leads to the criterion of weighted proportionality (WPR): there are several weights 
  
    
      
        
          w
          
            i
          
        
      
    
    {\displaystyle w_{i}}
  
 that sum up to 1, and every partner 
  
    
      
        i
      
    
    {\displaystyle i}
  
 should receive at least a fraction 
  
    
      
        
          w
          
            i
          
        
      
    
    {\displaystyle w_{i}}
  
 of the resource by their own valuation.
