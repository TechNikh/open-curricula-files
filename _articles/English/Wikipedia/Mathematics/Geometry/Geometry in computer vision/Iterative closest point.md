---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Iterative_closest_point
offline_file: ""
offline_thumbnail: ""
uuid: cdf62395-c186-4a1b-92a9-022e997507ec
updated: 1484309181
title: Iterative closest point
tags:
    - Overview
    - Implementations
categories:
    - Geometry in computer vision
---
Iterative Closest Point (ICP) [1][2][3] is an algorithm employed to minimize the difference between two clouds of points. ICP is often used to reconstruct 2D or 3D surfaces from different scans, to localize robots and achieve optimal path planning (especially when wheel odometry is unreliable due to slippery terrain), to co-register bone models, etc.
