---
version: 1
type: video
provider: YouTube
id: Qw_CftytVQQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e713e93b-298c-4683-8280-66ae73c80419
updated: 1486069622
title: Multiply Numbers Like A Jedi
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Qw_CftytVQQ/default.jpg
    - https://i3.ytimg.com/vi/Qw_CftytVQQ/1.jpg
    - https://i3.ytimg.com/vi/Qw_CftytVQQ/2.jpg
    - https://i3.ytimg.com/vi/Qw_CftytVQQ/3.jpg
---
