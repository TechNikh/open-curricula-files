---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chop-chop
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495341
title: chop-chop
categories:
    - Dictionary
---
chop-chop
     adv : with rapid movements; "he works quickly" [syn: {quickly}, {rapidly},
            {speedily}, {apace}] [ant: {slowly}]
