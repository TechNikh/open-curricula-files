---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/optimism
offline_file: ""
offline_thumbnail: ""
uuid: 36932803-2493-4b96-a50b-c0dab0cbdfaf
updated: 1484310451
title: optimism
categories:
    - Dictionary
---
optimism
     n 1: the optimistic feeling that all is going to turn out well
          [ant: {pessimism}]
     2: a general disposition to expect the best in all things [ant:
         {pessimism}]
