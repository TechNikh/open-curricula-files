---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/divergent
offline_file: ""
offline_thumbnail: ""
uuid: d96b8ffd-82b6-4434-bf6a-068303e516be
updated: 1484310283
title: divergent
categories:
    - Dictionary
---
divergent
     adj 1: diverging from another or from a standard; "a divergent
            opinion"
     2: tending to move apart in different directions [syn: {diverging}]
        [ant: {convergent}]
