---
version: 1
type: video
provider: YouTube
id: bvPM6sfidY4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Transportation%20in%20Plants.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1ee896bb-fa26-441e-94fe-28caf09db3b4
updated: 1486069666
title: Transportation in Plants
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/bvPM6sfidY4/default.jpg
    - https://i3.ytimg.com/vi/bvPM6sfidY4/1.jpg
    - https://i3.ytimg.com/vi/bvPM6sfidY4/2.jpg
    - https://i3.ytimg.com/vi/bvPM6sfidY4/3.jpg
---
