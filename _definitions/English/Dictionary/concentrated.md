---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concentrated
offline_file: ""
offline_thumbnail: ""
uuid: f68a5242-8595-4faa-bd4c-343b7f2c306b
updated: 1484310381
title: concentrated
categories:
    - Dictionary
---
concentrated
     adj 1: gathered together or made less diffuse; "their concentrated
            efforts"; "his concentrated attention"; "concentrated
            study"; "a narrow thread of concentrated ore" [ant: {distributed}]
     2: of or relating to a solution whose dilution has been reduced
     3: intensely focused; "her concentrated passion held them at
        bay"
     4: being the most concentrated solution possible at a given
        temperature; unable to dissolve still more of a substance;
        "a saturated solution" [syn: {saturated}] [ant: {unsaturated}]
