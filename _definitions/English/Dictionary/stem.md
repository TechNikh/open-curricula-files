---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stem
offline_file: ""
offline_thumbnail: ""
uuid: 199f9f97-cdae-4c4d-9ab7-be00c6d05bba
updated: 1484310339
title: stem
categories:
    - Dictionary
---
stem
     n 1: (linguistics) the form of a word after all affixes are
          removed; "thematic vowels are part of the stem" [syn: {root},
           {root word}, {base}, {theme}, {radical}]
     2: a slender or elongated structure that supports a plant or
        fungus or a plant part or plant organ [syn: {stalk}]
     3: cylinder forming a long narrow part of something [syn: {shank}]
     4: the tube of a tobacco pipe
     5: front part of a vessel or aircraft; "he pointed the bow of
        the boat toward the finish line" [syn: {bow}, {fore}, {prow}]
     6: a turn made in skiing; the back of one ski is forced outward
        and the other ski is brought parallel to it [syn: {stem
  ...
