---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/touching
offline_file: ""
offline_thumbnail: ""
uuid: 36ba22b0-ae2a-4072-9898-f3f3dffd34a5
updated: 1484310221
title: touching
categories:
    - Dictionary
---
touching
     adj : arousing affect; "the homecoming of the released hostages
           was an affecting scene"; "poignant grief cannot endure
           forever"; "his gratitude was simple and touching" [syn:
            {affecting}, {poignant}]
     n 1: the event of something coming in contact with the body; "he
          longed for the touch of her hand"; "the cooling touch of
          the night air" [syn: {touch}]
     2: the act of putting two things together with no space between
        them; "at his touch the room filled with lights" [syn: {touch}]
