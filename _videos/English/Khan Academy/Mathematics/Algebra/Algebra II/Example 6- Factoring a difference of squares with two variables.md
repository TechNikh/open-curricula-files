---
version: 1
type: video
provider: YouTube
id: YahJQvY396o
offline_file: ""
offline_thumbnail: ""
uuid: 4e9f1bf2-5533-4cc3-8005-087ab25d59d5
updated: 1484040398
title: 'Example 6- Factoring a difference of squares with two variables'
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/YahJQvY396o/default.jpg
    - https://i3.ytimg.com/vi/YahJQvY396o/1.jpg
    - https://i3.ytimg.com/vi/YahJQvY396o/2.jpg
    - https://i3.ytimg.com/vi/YahJQvY396o/3.jpg
---
