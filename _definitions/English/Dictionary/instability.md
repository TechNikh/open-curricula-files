---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instability
offline_file: ""
offline_thumbnail: ""
uuid: 4dc8dad9-e10e-48da-ac2d-bc1567e2bd4e
updated: 1484310563
title: instability
categories:
    - Dictionary
---
instability
     n 1: an unstable order [ant: {stability}]
     2: unreliability attributable to being unstable
     3: a state of disequilibrium (as may occur in cases of inner
        ear disease) [syn: {imbalance}, {unbalance}] [ant: {balance}]
     4: the quality or attribute of being unstable and irresolute
        [syn: {unstableness}] [ant: {stability}, {stability}]
