---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grassmannian
offline_file: ""
offline_thumbnail: ""
uuid: 923f95c8-7375-4304-99ef-47c39250083e
updated: 1484309156
title: Grassmannian
tags:
    - Motivation
    - Low dimensions
    - The Grassmannian as a set
    - The Grassmannian as a homogeneous space
    - The Grassmannian as a scheme
    - Representable functor
    - Universal family
    - The Plücker embedding
    - The Grassmannian as a real affine algebraic variety
    - Duality
    - Schubert cells
    - Cohomology ring of the complex Grassmannian
    - Associated measure
    - Oriented Grassmannian
    - Applications
    - Notes
categories:
    - Algebraic geometry
---
In mathematics, the Grassmannian Gr(r, V) is a space which parameterizes all linear subspaces of a vector space V of given dimension r. For example, the Grassmannian Gr(1, V) is the space of lines through the origin in V, so it is the same as the projective space of one dimension lower than V.
