---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/literate
offline_file: ""
offline_thumbnail: ""
uuid: 3eb0f8c9-72e8-4161-8388-e284713c5c21
updated: 1484310445
title: literate
categories:
    - Dictionary
---
literate
     adj 1: able to read and write [ant: {illiterate}]
     2: able to read and write
     n : a person who can read and write [syn: {literate person}]
