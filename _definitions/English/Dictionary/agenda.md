---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agenda
offline_file: ""
offline_thumbnail: ""
uuid: 97717386-8ab4-49cc-98f2-e9fabf944acb
updated: 1484310571
title: agenda
categories:
    - Dictionary
---
agenda
     n 1: a temporally organized plan for matters to be attended to
          [syn: {docket}, {schedule}]
     2: a list of matters to be taken up (as at a meeting) [syn: {agendum},
         {order of business}]
