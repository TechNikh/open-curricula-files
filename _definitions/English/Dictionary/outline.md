---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outline
offline_file: ""
offline_thumbnail: ""
uuid: afcf2587-e02a-4052-b313-c91ebd5bcd26
updated: 1484310439
title: outline
categories:
    - Dictionary
---
outline
     n 1: the line that appears to bound an object [syn: {lineation}]
     2: a sketchy summary of the main points of an argument or
        theory [syn: {synopsis}, {abstract}, {precis}]
     3: a schematic or preliminary plan [syn: {schema}, {scheme}]
     v 1: describe roughly or briefly or give the main points or
          summary of; "sketch the outline of the book"; "outline
          his ideas" [syn: {sketch}, {adumbrate}]
     2: draw up an outline or sketch for something; "draft a speech"
        [syn: {draft}]
     3: trace the shape of [syn: {delineate}, {limn}]
