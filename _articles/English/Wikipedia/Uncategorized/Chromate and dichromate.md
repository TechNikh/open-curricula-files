---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dichromate
offline_file: ""
offline_thumbnail: ""
uuid: 1c570076-ace3-48ee-9518-fe366c190e1d
updated: 1484308403
title: Chromate and dichromate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Predominance_diagram_Cr.png
tags:
    - Chemical properties
    - Acid–base properties
    - Oxidation–reduction properties
    - Applications
    - Natural occurrence and production
    - Safety
    - Notes
categories:
    - Uncategorized
---
Chromate salts contain the chromate anion, CrO2−
4. Dichromate salts contain the dichromate anion, Cr
2O2−
7. They are oxoanions of chromium in the oxidation state +6. They are moderately strong oxidizing agents. In an aqueous solution, chromate and dichromate ions can be interconvertible.
