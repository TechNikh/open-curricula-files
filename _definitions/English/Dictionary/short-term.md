---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/short-term
offline_file: ""
offline_thumbnail: ""
uuid: 12e4365c-489d-4f6b-a299-ca489f432918
updated: 1484310522
title: short-term
categories:
    - Dictionary
---
short-term
     adj : relating to or extending over a limited period; "short-run
           planning"; "a short-term lease"; "short-term credit"
           [syn: {short-run}]
