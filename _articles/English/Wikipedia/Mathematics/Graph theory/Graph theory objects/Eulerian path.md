---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eulerian_path
offline_file: ""
offline_thumbnail: ""
uuid: a8465649-b356-4cf3-9988-614a35ee6785
updated: 1484309443
title: Eulerian path
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/165px-K%25C3%25B6nigsberg_graph.svg.png'
tags:
    - Definition
    - Properties
    - Constructing Eulerian trails and circuits
    - "Fleury's algorithm"
    - "Hierholzer's algorithm"
    - Counting Eulerian circuits
    - Complexity issues
    - Special cases
    - Applications
    - Notes
categories:
    - Graph theory objects
---
In graph theory, an Eulerian trail (or Eulerian path) is a trail in a graph which visits every edge exactly once. Similarly, an Eulerian circuit or Eulerian cycle is an Eulerian trail which starts and ends on the same vertex. They were first discussed by Leonhard Euler while solving the famous Seven Bridges of Königsberg problem in 1736. The problem can be stated mathematically like this:
