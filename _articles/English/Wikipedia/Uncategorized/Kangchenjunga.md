---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kanchenjunga
offline_file: ""
offline_thumbnail: ""
uuid: 9e139318-f9c9-40e9-bab5-f6db2bca4489
updated: 1484308426
title: Kangchenjunga
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-KANGCHENJUNGA_MAP_by_GARWOOD%252C_1903.jpg'
tags:
    - Names
    - Protected areas
    - geography
    - Climbing history
    - Early reconnaissances and attempts
    - First ascent
    - Other notable ascents
    - Tourism
    - In myth
    - In literature
categories:
    - Uncategorized
---
Kangchenjunga (Nepali: कञ्चनजङ्घा; Hindi: कंचनजंघा; Sikkimese: ཁང་ཅེན་ཛོཾག་), also spelled Kanchenjunga, is the third highest mountain in the world, and lies partly in Nepal and partly in Sikkim, India.[3] It rises with an elevation of 8,586 m (28,169 ft) in a section of the Himalayas called Kangchenjunga Himal that is limited in the west by the Tamur River, in the north by the Lhonak Chu and Jongsang La, and in the east by the Teesta River.[1]
