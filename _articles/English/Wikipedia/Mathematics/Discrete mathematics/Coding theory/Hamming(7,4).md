---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamming(7,4)
offline_file: ""
offline_thumbnail: ""
uuid: 8bdccc1c-a203-479a-9f24-7d14ee402284
updated: 1484308542
title: Hamming(7,4)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/160px-Hamming%25287%252C4%2529.svg.png'
tags:
    - Goal
    - Hamming matrices
    - Channel coding
    - Parity check
    - Error correction
    - Decoding
    - Multiple bit errors
    - All codewords
categories:
    - Coding theory
---
In coding theory, Hamming(7,4) is a linear error-correcting code that encodes four bits of data into seven bits by adding three parity bits. It is a member of a larger family of Hamming codes, but the term Hamming code often refers to this specific code that Richard W. Hamming introduced in 1950. At the time, Hamming worked at Bell Telephone Laboratories and was frustrated with the error-prone punched card reader, which is why he started working on error-correcting codes.[1]
