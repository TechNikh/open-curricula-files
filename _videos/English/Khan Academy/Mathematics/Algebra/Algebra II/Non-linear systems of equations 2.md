---
version: 1
type: video
provider: YouTube
id: XPf8LMu7QSw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Non-linear%20systems%20of%20equations%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ea1d26cf-8433-4308-ba6f-798799e908bf
updated: 1486069563
title: Non-linear systems of equations 2
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/XPf8LMu7QSw/default.jpg
    - https://i3.ytimg.com/vi/XPf8LMu7QSw/1.jpg
    - https://i3.ytimg.com/vi/XPf8LMu7QSw/2.jpg
    - https://i3.ytimg.com/vi/XPf8LMu7QSw/3.jpg
---
