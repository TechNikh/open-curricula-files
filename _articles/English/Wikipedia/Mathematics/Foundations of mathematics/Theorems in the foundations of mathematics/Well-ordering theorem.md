---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Well-ordering_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 22a0aa6f-d641-4e4c-aa90-e99fa82b1826
updated: 1484308999
title: Well-ordering theorem
tags:
    - History
    - Statement and proof
    - Proof
    - Notes
categories:
    - Theorems in the foundations of mathematics
---
In mathematics, the well-ordering theorem states that every set can be well-ordered. A set X is well-ordered by a strict total order if every non-empty subset of X has a least element under the ordering. This is also known as Zermelo's theorem and is equivalent to the Axiom of Choice.[1][2] Ernst Zermelo introduced the Axiom of Choice as an "unobjectionable logical principle" to prove the well-ordering theorem. This is important because it makes every set susceptible to the powerful technique of transfinite induction. The well-ordering theorem has consequences that may seem paradoxical, such as the Banach–Tarski paradox.
