---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tolerate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484628781
title: tolerate
categories:
    - Dictionary
---
tolerate
     v 1: put up with something or somebody unpleasant; "I cannot bear
          his constant criticism"; "The new secretary had to
          endure a lot of unprofessional remarks"; "he learned to
          tolerate the heat"; "She stuck out two years in a
          miserable marriage" [syn: {digest}, {endure}, {stick out},
           {stomach}, {bear}, {stand}, {support}, {brook}, {abide},
           {suffer}, {put up}]
     2: recognize and respect (rights and beliefs of others); "We
        must tolerate the religions of others"
     3: have a tolerance for a poison or strong drug or pathogen;
        "The patient does not tolerate the anti-inflammatory drugs
        we gave ...
