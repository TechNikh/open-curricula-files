---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Arnold%E2%80%93Beltrami%E2%80%93Childress_flow'
offline_file: ""
offline_thumbnail: ""
uuid: 53561943-96f3-415f-9246-f599535f5cc5
updated: 1484308699
title: Arnold–Beltrami–Childress flow
categories:
    - Chaos theory
---
The Arnold–Beltrami–Childress (ABC) flow is a three-dimensional incompressible velocity field which is an exact solution of Euler's equation. Its representation in Cartesian coordinates is the following:[1][2]
