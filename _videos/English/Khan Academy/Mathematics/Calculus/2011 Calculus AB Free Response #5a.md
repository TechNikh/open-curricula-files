---
version: 1
type: video
provider: YouTube
id: n9UvuzI2fq0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%235a.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d8f6c257-8563-4ac1-9011-1da270877eee
updated: 1486069692
title: '2011 Calculus AB Free Response #5a'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/n9UvuzI2fq0/default.jpg
    - https://i3.ytimg.com/vi/n9UvuzI2fq0/1.jpg
    - https://i3.ytimg.com/vi/n9UvuzI2fq0/2.jpg
    - https://i3.ytimg.com/vi/n9UvuzI2fq0/3.jpg
---
