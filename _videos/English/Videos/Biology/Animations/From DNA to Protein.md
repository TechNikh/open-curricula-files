---
version: 1
type: video
provider: YouTube
id: D3fOXt4MrOM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/From%20DNA%20to%20Protein.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6f1838a9-406d-410c-b2eb-5212b58d8dda
updated: 1486069672
title: From DNA to Protein
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/D3fOXt4MrOM/default.jpg
    - https://i3.ytimg.com/vi/D3fOXt4MrOM/1.jpg
    - https://i3.ytimg.com/vi/D3fOXt4MrOM/2.jpg
    - https://i3.ytimg.com/vi/D3fOXt4MrOM/3.jpg
---
