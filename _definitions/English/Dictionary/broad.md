---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/broad
offline_file: ""
offline_thumbnail: ""
uuid: 40ab8830-c86a-4b65-90de-1ad5c9e55359
updated: 1484310254
title: broad
categories:
    - Dictionary
---
broad
     adj 1: having great (or a certain) extent from one side to the
            other; "wide roads"; "a wide necktie"; "wide margins";
            "three feet wide"; "a river two miles broad"; "broad
            shoulders"; "a broad river" [syn: {wide}] [ant: {narrow}]
     2: broad in scope or content; "across-the-board pay increases";
        "an all-embracing definition"; "blanket sanctions against
        human-rights violators"; "an invention with broad
        applications"; "a panoptic study of Soviet nationality"-
        T.G.Winner; "granted him wide powers" [syn: {across-the-board},
         {all-embracing}, {all-encompassing}, {all-inclusive}, {blanket(a)},
         ...
