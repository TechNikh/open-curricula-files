---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/opening
offline_file: ""
offline_thumbnail: ""
uuid: d0f47699-a53c-4dff-a834-cb6abce5a15a
updated: 1484310346
title: opening
categories:
    - Dictionary
---
opening
     adj : first or beginning; "the memorable opening bars of
           Beethoven's Fifth"; "the play's opening scene" [ant: {closing}]
     n 1: an open or empty space in or between things; "there was a
          small opening between the trees"; "the explosion made a
          gap in the wall" [syn: {gap}]
     2: a ceremony accompanying the start of some enterprise
     3: becoming open or being made open; "the opening of his arms
        was the sign I was waiting for"
     4: the first performance (as of a theatrical production); "the
        opening received good critical reviews" [syn: {opening
        night}, {curtain raising}]
     5: the act of opening something; "the ray ...
