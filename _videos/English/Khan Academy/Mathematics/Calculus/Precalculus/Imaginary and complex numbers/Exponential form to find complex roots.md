---
version: 1
type: video
provider: YouTube
id: N0Y8ia57C24
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Imaginary%20and%20complex%20numbers/Exponential%20form%20to%20find%20complex%20roots.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 613b66a0-bef5-4b97-abf1-7ece099f2c8b
updated: 1486069696
title: Exponential form to find complex roots
categories:
    - Imaginary and complex numbers
thumbnail_urls:
    - https://i3.ytimg.com/vi/N0Y8ia57C24/default.jpg
    - https://i3.ytimg.com/vi/N0Y8ia57C24/1.jpg
    - https://i3.ytimg.com/vi/N0Y8ia57C24/2.jpg
    - https://i3.ytimg.com/vi/N0Y8ia57C24/3.jpg
---
