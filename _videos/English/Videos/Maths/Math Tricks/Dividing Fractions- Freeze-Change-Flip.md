---
version: 1
type: video
provider: YouTube
id: gwjV7qWF2A0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ba1f5549-e3f4-4a1a-a6d4-cc8848448046
updated: 1486069623
title: 'Dividing Fractions- Freeze/Change/Flip'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/gwjV7qWF2A0/default.jpg
    - https://i3.ytimg.com/vi/gwjV7qWF2A0/1.jpg
    - https://i3.ytimg.com/vi/gwjV7qWF2A0/2.jpg
    - https://i3.ytimg.com/vi/gwjV7qWF2A0/3.jpg
---
