---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/margin
offline_file: ""
offline_thumbnail: ""
uuid: 606b8b33-508a-47f0-8a5b-6d4097b76e0d
updated: 1484310273
title: margin
categories:
    - Dictionary
---
margin
     n 1: the boundary line or the area immediately inside the
          boundary [syn: {border}, {perimeter}]
     2: a permissible difference; allowing some freedom to move
        within limits [syn: {allowance}, {leeway}, {tolerance}]
     3: the amount of collateral a customer deposits with a broker
        when borrowing from the broker to buy securities [syn: {security
        deposit}]
     4: (finance) the net sales minus the cost of goods and services
        sold [syn: {gross profit}, {gross profit margin}]
     5: the blank space that surrounds the text on a page
     6: a strip near the boundary of an object; "he jotted a note on
        the margin of the page" [syn: ...
