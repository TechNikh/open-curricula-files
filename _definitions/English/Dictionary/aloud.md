---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aloud
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484624101
title: aloud
categories:
    - Dictionary
---
aloud
     adv 1: using the voice; not silently; "please read the passage
            aloud"; "he laughed out loud" [syn: {out loud}]
     2: with relatively high volume; "the band played loudly"; "she
        spoke loudly and angrily"; "he spoke loud enough for those
        at the back of the room to hear him"; "cried aloud for
        help" [syn: {loudly}, {loud}] [ant: {softly}]
