---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Even_code
offline_file: ""
offline_thumbnail: ""
uuid: ab4a9f56-b722-48da-bae9-d3d25fb7a715
updated: 1484308551
title: Even code
categories:
    - Coding theory
---
A binary code is called an even code if the Hamming weight of each of its codewords is even. An even code should have a generator polynomial that include (1+x) minimal polynomial as a product. Furthermore, a binary code is called doubly even if the Hamming weight of all its codewords is divisible by 4. An even code which is not doubly even is said to be strictly even.
