---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_regular_polytopes_and_compounds
offline_file: ""
offline_thumbnail: ""
uuid: 143585e3-667c-4a0b-8381-c5950d9fa5ec
updated: 1484309287
title: List of regular polytopes and compounds
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Pure_3-dimensional_apeirohedra_chart.png
tags:
    - Overview
    - One dimension
    - Two dimensions (polygons)
    - Convex
    - Spherical
    - Stars
    - Skew polygons
    - Three dimensions (polyhedra)
    - Convex
    - Spherical
    - Stars
    - Skew polyhedra
    - Four dimensions
    - Convex
    - Spherical
    - Stars
    - Five and more dimensions
    - Convex
    - 5 dimensions
    - 6 dimensions
    - 7 dimensions
    - 8 dimensions
    - 9 dimensions
    - 10 dimensions
    - Non-convex
    - Regular projective polytopes
    - Regular projective polyhedra
    - Regular projective 4-polytopes
    - Regular projective 5-polytopes
    - Apeirotopes
    - One dimension (apeirogons)
    - Skew apeirogons
    - Two dimensions (apeirohedra)
    - Euclidean tilings
    - Euclidean star-tilings
    - Hyperbolic tilings
    - Hyperbolic star-tilings
    - Skew apeirohedra in Euclidean 3-space
    - Skew apeirohedra in hyperbolic 3-space
    - Three dimensions (4-apeirotopes)
    - Tessellations of Euclidean 3-space
    - Improper tessellations of Euclidean 3-space
    - Tessellations of hyperbolic 3-space
    - Four dimensions (5-apeirotopes)
    - Tessellations of Euclidean 4-space
    - Tessellations of hyperbolic 4-space
    - Star tessellations of hyperbolic 4-space
    - Five dimensions (6-apeirotopes)
    - Tessellations of Euclidean 5-space
    - Tessellations of hyperbolic 5-space
    - 6 dimensions and higher (7-apeirotopes+)
    - Tessellations of hyperbolic 6-space and higher
    - Compound polytopes
    - Two dimensional compounds
    - Three dimensional compounds
    - Euclidean and hyperbolic plane compounds
    - Four dimensional compounds
    - Euclidean 3-space compounds
    - Five dimensions and higher compounds
    - Euclidean honeycomb compounds
    - Abstract polytopes
    - Notes
categories:
    - Multi-dimensional geometry
---
The Schläfli symbol describes every regular tessellation of an n-sphere, Euclidean and hyperbolic spaces. A Schläfli symbol describing an n-polytope equivalently describes a tessellation of a (n-1)-sphere. In addition, the symmetry of a regular polytope or tessellation is expressed as a Coxeter group, which Coxeter expressed identically to the Schläfli symbol, except delimiting by square brackets, a notation that is called Coxeter notation. Another related symbol is the Coxeter-Dynkin diagram which represents a symmetry group with no rings, and the represents regular polytope or tessellation with a ring on the first node. For example, the cube has Schläfli symbol {4,3}, and with its ...
