---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pythagoras
offline_file: ""
offline_thumbnail: ""
uuid: 85b7453f-c331-484b-abba-05dedd0e965c
updated: 1484309098
title: Pythagoras
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pythagoras_Bust_Vatican_Museum.jpg
tags:
    - Biographical sources
    - life
    - family
    - Influence
    - Views
    - Croton
    - Writings
    - Mathematics
    - Pythagorean theorem
    - Musical theories and investigations
    - Tetractys
    - Religion and science
    - Lore
    - Pythagoreanism
    - Pythagorean school
    - Influence
    - Influence on Plato
    - Politics and science
    - Influence on Greek art
    - Influence on other groups
    - Sources
    - Classical secondary sources
    - Modern secondary sources
categories:
    - Geometers
---
Pythagoras of Samos (US /pɪˈθæɡərəs/;[1] UK /paɪˈθæɡərəs/;[2] Greek: Πυθαγόρας ὁ Σάμιος Pythagóras ho Sámios "Pythagoras the Samian", or simply Πυθαγόρας; Πυθαγόρης in Ionian Greek; c. 570 – c. 495 BC)[3][4] was an Ionian Greek philosopher, mathematician, and the putative founder of the movement called Pythagoreanism. Most of the information about Pythagoras was written down centuries after he lived, so very little reliable information is known about him. He was born on the island of Samos, and travelled, visiting Egypt and Greece, and maybe India.[5] Around 530 BC, he moved to Croton, in Magna Graecia, and there established some kind ...
