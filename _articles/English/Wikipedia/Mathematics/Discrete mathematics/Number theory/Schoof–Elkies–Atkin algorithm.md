---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Schoof%E2%80%93Elkies%E2%80%93Atkin_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 31b6e93b-4a0e-47b9-b0f9-558f5b687506
updated: 1484308654
title: Schoof–Elkies–Atkin algorithm
categories:
    - Number theory
---
The Schoof–Elkies–Atkin algorithm (SEA) is an algorithm used for finding the order of or calculating the number of points on an elliptic curve over a finite field. Its primary application is in elliptic curve cryptography. The algorithm is an extension of Schoof's algorithm by Noam Elkies and A. O. L. Atkin to significantly improve its efficiency (under heuristic assumptions).
