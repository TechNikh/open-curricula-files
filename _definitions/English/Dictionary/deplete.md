---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deplete
offline_file: ""
offline_thumbnail: ""
uuid: 698a34e2-09b3-43af-a0ed-97394d2f468c
updated: 1484310242
title: deplete
categories:
    - Dictionary
---
deplete
     v : use up (resources or materials); "this car consumes a lot of
         gas"; "We exhausted our savings"; "They run through 20
         bottles of wine a week" [syn: {consume}, {eat up}, {use
         up}, {eat}, {exhaust}, {run through}, {wipe out}]
