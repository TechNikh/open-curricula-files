---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solder
offline_file: ""
offline_thumbnail: ""
uuid: a1543232-a72b-41ce-b9be-ee1a3cf5ecef
updated: 1484310200
title: solder
categories:
    - Dictionary
---
solder
     n : an alloy (usually of lead and tin) used when melted to join
         two metal surfaces
     v : join or fuse with solder; "solder these two pipes together"
