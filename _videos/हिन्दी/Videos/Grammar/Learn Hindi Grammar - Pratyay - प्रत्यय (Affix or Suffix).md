---
version: 1
type: video
provider: YouTube
id: iysSIphnqGA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 881ac825-2649-455e-bef2-dc1465a23277
updated: 1486069629
title: 'Learn Hindi Grammar - Pratyay - प्रत्यय (Affix or Suffix)'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/iysSIphnqGA/default.jpg
    - https://i3.ytimg.com/vi/iysSIphnqGA/1.jpg
    - https://i3.ytimg.com/vi/iysSIphnqGA/2.jpg
    - https://i3.ytimg.com/vi/iysSIphnqGA/3.jpg
---
