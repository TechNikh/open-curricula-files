---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recessive
offline_file: ""
offline_thumbnail: ""
uuid: 73ea8574-60b5-469c-a19a-7f67f99c46da
updated: 1484310305
title: recessive
categories:
    - Dictionary
---
recessive
     adj 1: of or pertaining to a recession [syn: {recessionary}]
     2: of genes; producing its characteristic phenotype only when
        its allele is identical [ant: {dominant}]
