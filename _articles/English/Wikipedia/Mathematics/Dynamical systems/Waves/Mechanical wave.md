---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mechanical_wave
offline_file: ""
offline_thumbnail: ""
uuid: 5259eccc-6cf2-4e46-bed3-4948dc353fc9
updated: 1484308887
title: Mechanical wave
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Espejo_%25283207185886%2529.jpg'
tags:
    - Transverse wave
    - Longitudinal wave
    - Surface waves
    - Examples
categories:
    - Waves
---
A mechanical wave is a wave that is an oscillation of matter, and therefore transfers energy through a medium.[1] While waves can move over long distances, the movement of the medium of transmission—the material—is limited. Therefore, oscillating material does not move far from its initial equilibrium position. Mechanical waves transport energy. This energy propagates in the same direction as the wave. Any kind of wave (mechanical or electromagnetic) has a certain energy. Mechanical waves can be produced only in media which possess elasticity and inertia.
