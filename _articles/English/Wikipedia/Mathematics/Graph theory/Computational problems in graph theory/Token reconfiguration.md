---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Token_reconfiguration
offline_file: ""
offline_thumbnail: ""
uuid: e9b4fcc7-7ae7-4ff4-81bf-92a88d3caaa1
updated: 1484309398
title: Token reconfiguration
tags:
    - Motivation
    - Complexity
    - Optimization
    - Approximation
categories:
    - Computational problems in graph theory
---
In computational complexity theory and combinatorics, the token reconfiguration problem is an optimization problem on a graph with both an initial and desired state for tokens.
