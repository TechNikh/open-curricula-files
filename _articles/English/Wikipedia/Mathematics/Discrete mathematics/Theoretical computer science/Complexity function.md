---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complexity_function
offline_file: ""
offline_thumbnail: ""
uuid: 48cc5d88-12eb-46d3-8773-450d953be715
updated: 1484308669
title: Complexity function
tags:
    - Complexity function of a word
    - Complexity function of a language
    - Related concepts
categories:
    - Theoretical computer science
---
In computer science, the complexity function of a string, a finite or infinite sequence of letters from some alphabet, is the function that counts the number of distinct factors (substrings of consecutive symbols) from that string. More generally, the complexity function of a language, a set of finite words over an alphabet, counts the number of distinct words of given length.
