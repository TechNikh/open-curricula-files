---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Theoretical_computer_science
offline_file: ""
offline_thumbnail: ""
uuid: f9017478-c19b-4675-9c5b-c4075e43f530
updated: 1484308662
title: Theoretical computer science
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Maquina.png
tags:
    - History
    - Topics
    - Algorithms
    - Data structures
    - Computational complexity theory
    - Distributed computation
    - Parallel computation
    - Very-large-scale integration
    - Machine learning
    - Computational biology
    - Computational geometry
    - Information theory
    - Cryptography
    - Quantum computation
    - Information-based complexity
    - Computational number theory
    - Symbolic computation
    - Program semantics
    - Formal methods
    - Automata theory
    - Coding theory
    - Computational learning theory
    - Organizations
    - Journals and newsletters
    - Conferences
    - Notes
categories:
    - Theoretical computer science
---
Theoretical computer science is a division or subset of general computer science and mathematics that focuses on more abstract or mathematical aspects of computing and includes the theory of computation.
