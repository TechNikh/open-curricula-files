---
version: 1
type: video
provider: YouTube
id: ZBfy-MNgtoY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Parabolic%20motion%20-%20velocity%20vector.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b435e40f-efaf-49e5-9498-e61e15b62bbe
updated: 1486069651
title: 'Parabolic motion - velocity vector'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZBfy-MNgtoY/default.jpg
    - https://i3.ytimg.com/vi/ZBfy-MNgtoY/1.jpg
    - https://i3.ytimg.com/vi/ZBfy-MNgtoY/2.jpg
    - https://i3.ytimg.com/vi/ZBfy-MNgtoY/3.jpg
---
