---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/precipitate
offline_file: ""
offline_thumbnail: ""
uuid: a62883de-b5ed-4ac5-9654-a5a4cd8783dd
updated: 1484310371
title: precipitate
categories:
    - Dictionary
---
precipitate
     adj : done with very great haste and without due deliberation;
           "hasty marriage seldom proveth well"- Shakespeare;
           "hasty makeshifts take the place of planning"- Arthur
           Geddes; "rejected what was regarded as an overhasty
           plan for reconversion"; "wondered whether they had been
           rather precipitate in deposing the king" [syn: {hasty},
            {overhasty}, {precipitant}, {precipitous}]
     n : a precipitated solid substance in suspension or after
         settling or filtering
     v 1: separate as a fine suspension of solid particles
     2: bring about abruptly; "The crisis precipitated by Russia's
        revolution"
 ...
