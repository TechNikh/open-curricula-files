---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalized_complex_structure
offline_file: ""
offline_thumbnail: ""
uuid: ac71a372-334b-4fca-96a2-07c5c8dbded7
updated: 1484309219
title: Generalized complex structure
tags:
    - Definition
    - The generalized tangent bundle
    - Courant bracket
    - The definition
    - Maximal isotropic subbundles
    - Classification
    - type
    - Real index
    - Canonical bundle
    - Generalized almost complex structures
    - Integrability and other structures
    - Local classification
    - Canonical bundle
    - Regular point
    - "Darboux's theorem"
    - Local holomorphicity
    - Examples
    - Complex manifolds
    - Symplectic manifolds
    - Relation to G-structures
    - Calabi versus Calabi-Yau metric
categories:
    - Differential geometry
---
In the field of mathematics known as differential geometry, a generalized complex structure is a property of a differential manifold that includes as special cases a complex structure and a symplectic structure. Generalized complex structures were introduced by Nigel Hitchin in 2002 and further developed by his students Marco Gualtieri and Gil Cavalcanti.
