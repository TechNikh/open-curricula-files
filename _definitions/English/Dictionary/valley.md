---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/valley
offline_file: ""
offline_thumbnail: ""
uuid: 488acf39-21aa-4f66-8a99-0cd5320d0201
updated: 1484310435
title: valley
categories:
    - Dictionary
---
valley
     n : a long depression in the surface of the land that usually
         contains a river [syn: {vale}]
