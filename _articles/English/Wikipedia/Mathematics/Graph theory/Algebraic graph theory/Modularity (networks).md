---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Modularity_(networks)
offline_file: ""
offline_thumbnail: ""
uuid: 56724908-9d1a-4a5b-8114-090978661837
updated: 1484309374
title: Modularity (networks)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sample_Network.jpg
tags:
    - Motivation
    - Definition
    - Example of multiple community detection
    - Matrix formulation
    - Resolution limit
    - Multiresolution methods
categories:
    - Algebraic graph theory
---
Modularity is one measure of the structure of networks or graphs. It was designed to measure the strength of division of a network into modules (also called groups, clusters or communities). Networks with high modularity have dense connections between the nodes within modules but sparse connections between nodes in different modules. Modularity is often used in optimization methods for detecting community structure in networks. However, it has been shown that modularity suffers a resolution limit and, therefore, it is unable to detect small communities. Biological networks, including animal brains, exhibit a high degree of modularity.
