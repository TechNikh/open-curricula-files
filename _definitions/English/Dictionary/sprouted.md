---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sprouted
offline_file: ""
offline_thumbnail: ""
uuid: fe54247a-d8e9-45e8-92f4-f1bd01a2e788
updated: 1484310424
title: sprouted
categories:
    - Dictionary
---
sprouted
     adj : (of growing vegetation) having just emerged from the ground;
           "the corn is sprouted"
