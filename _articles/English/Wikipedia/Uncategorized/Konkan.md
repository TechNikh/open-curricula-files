---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Konkan
offline_file: ""
offline_thumbnail: ""
uuid: 95485cab-3933-4a30-80f9-9e8f8f5cdbb5
updated: 1484308422
title: Konkan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Konkan_Districts.png
tags:
    - Etymology
    - Boundaries
    - Famous Temple
    - The Konkan division
    - geography
    - Ethnology
categories:
    - Uncategorized
---
Konkan, also known as the Konkan Coast or Kokan, is a rugged section of the western coastline of India. It is a 720 km long coastline. It consists of the coastal districts of western Indian states of Karnataka, Goa, and Maharashtra. The ancient Saptakonkana is a slightly larger region.
