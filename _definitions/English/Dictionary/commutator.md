---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commutator
offline_file: ""
offline_thumbnail: ""
uuid: fc99dc3a-9cec-4650-91b7-5d4bbaf0d74b
updated: 1484310369
title: commutator
categories:
    - Dictionary
---
commutator
     n : switch for reversing the direction of an electric current
