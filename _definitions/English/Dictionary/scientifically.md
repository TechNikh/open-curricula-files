---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scientifically
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484604961
title: scientifically
categories:
    - Dictionary
---
scientifically
     adv : with respect to science; in a scientific way; "this is
           scientifically interesting"
