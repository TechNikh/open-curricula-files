---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hot_game
offline_file: ""
offline_thumbnail: ""
uuid: 5cde1a96-eda5-4ec1-8863-ce70539e23e7
updated: 1484308533
title: Hot game
categories:
    - Combinatorial game theory
---
By contrast, a cold game is one where each player can only worsen their position by making the next move. Cold games have values in the surreal numbers and so can be ordered by value, while hot games can have other values.[1]
