---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/automobile
offline_file: ""
offline_thumbnail: ""
uuid: 3c9f6d68-cd0e-4e7d-aafd-3352cf0d26f6
updated: 1484310212
title: automobile
categories:
    - Dictionary
---
automobile
     n : 4-wheeled motor vehicle; usually propelled by an internal
         combustion engine; "he needs a car to get to work" [syn:
         {car}, {auto}, {machine}, {motorcar}]
     v : travel in an automobile
