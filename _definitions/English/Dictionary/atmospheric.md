---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/atmospheric
offline_file: ""
offline_thumbnail: ""
uuid: e7055396-9416-4d43-9c84-a76fb9f395f6
updated: 1484310224
title: atmospheric
categories:
    - Dictionary
---
atmospheric
     adj : relating to or located in the atmosphere; "atmospheric
           tests" [syn: {atmospherical}]
