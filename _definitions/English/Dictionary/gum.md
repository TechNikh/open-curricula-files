---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gum
offline_file: ""
offline_thumbnail: ""
uuid: 3a5f24c2-da1b-4e64-b491-d959c64b6caf
updated: 1484310327
title: gum
categories:
    - Dictionary
---
gum
     n 1: a preparation (usually made of sweetened chicle) for chewing
          [syn: {chewing gum}]
     2: the tissue (covered by mucous membrane) of the jaws that
        surrounds the bases of the teeth [syn: {gingiva}]
     3: any of various substances (soluble in water) that exude from
        certain plants; they are gelatinous when moist but harden
        on drying
     4: cement consisting of a sticky substance that is used as an
        adhesive [syn: {glue}, {mucilage}]
     5: wood or lumber from any of various gum trees especially the
        sweet gum [syn: {gumwood}]
     6: any of various trees of the genera Eucalyptus or Liquidambar
        or Nyssa that are sources ...
