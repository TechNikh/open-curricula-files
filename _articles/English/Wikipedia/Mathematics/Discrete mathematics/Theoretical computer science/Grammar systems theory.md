---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grammar_systems_theory
offline_file: ""
offline_thumbnail: ""
uuid: 94539021-8191-4e0f-91b8-6f5319981334
updated: 1484308674
title: Grammar systems theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/450px-Grammar_system_diagram.svg.png
categories:
    - Theoretical computer science
---
Grammar systems theory is a field of theoretical computer science that studies systems of finite collections of formal grammars generating a formal language. Each grammar works on a string, a so-called sequential form that represents an environment. Grammar systems can thus be used as a formalization of decentralized or distributed systems of agents in artificial intelligence.[1]
