---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minimized
offline_file: ""
offline_thumbnail: ""
uuid: 7c3dbd34-fa86-47bb-afd6-0a213c639d4b
updated: 1484310377
title: minimized
categories:
    - Dictionary
---
minimized
     adj : reduced to the smallest possible size or amount or degree
