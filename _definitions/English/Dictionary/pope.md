---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pope
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484420101
title: pope
categories:
    - Dictionary
---
Pope
     n 1: the head of the Roman Catholic Church [syn: {Catholic Pope},
           {Roman Catholic Pope}, {Pontiff}, {Holy Father}, {Vicar
          of Christ}, {Bishop of Rome}]
     2: English poet and satirist (1688-1744) [syn: {Alexander Pope}]
