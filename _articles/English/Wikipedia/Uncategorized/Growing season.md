---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Growing_season
offline_file: ""
offline_thumbnail: ""
uuid: a1f1c492-4454-481b-91f8-d590aef8c6d9
updated: 1484308363
title: Growing season
tags:
    - geography
    - Season extension
    - Locations
    - North America
    - Europe
    - Tropics and deserts
categories:
    - Uncategorized
---
The growing season is the part of the year during which local weather conditions (i.e. rainfall and temperature) permit normal plant growth. While each plant or crop has a specific growing season that depends on its genetic adaptation, growing seasons can generally be grouped into macro-environmental classes.
