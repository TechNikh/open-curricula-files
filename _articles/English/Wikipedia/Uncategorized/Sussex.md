---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sussex
offline_file: ""
offline_thumbnail: ""
uuid: 8c10de5d-b4d9-4783-aa3c-7f6ac91dbcb4
updated: 1484308392
title: Sussex
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/160px-Sussex_shield.svg.png
tags:
    - Toponymy
    - Symbols
    - geography
    - Landscape
    - Climate
    - Conurbations
    - population
    - History
    - Beginnings
    - Roman Canton
    - Saxon Kingdom
    - Norman Sussex
    - Sussex under the Plantagenets
    - Early modern Sussex
    - Late modern and contemporary Sussex
    - Governance
    - Politics
    - Law
    - Administrative divisions
    - Historic sub-divisions
    - Modern local authority areas
    - Monarchy and peerage
    - Economy
    - Education
    - Healthcare
    - culture
    - Architecture
    - Dialect
    - Literature
    - Music
    - religion
    - Science
    - Sport
    - Cuisine
    - Visual arts
    - Footnotes
categories:
    - Uncategorized
---
Sussex (/ˈsʌsᵻks/; abbreviated Sx),[6] from the Old English Sūþsēaxe (South Saxons), is a historic county in South East England corresponding roughly in area to the ancient Kingdom of Sussex. It is bounded to the west by Hampshire, north by Surrey, north-east by Kent, south by the English Channel, and divided for local government into West Sussex and East Sussex and the city of Brighton and Hove. Brighton and Hove was created as a unitary authority in 1997, and granted City status in 2000. Until then, Chichester was Sussex's only city.
