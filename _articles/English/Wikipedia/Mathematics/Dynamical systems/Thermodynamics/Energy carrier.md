---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Energy_carrier
offline_file: ""
offline_thumbnail: ""
uuid: b06ae3ea-259f-4eff-b552-da1ce6bd9054
updated: 1484308506
title: Energy carrier
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/130px-Crystal_energy.svg.png
tags:
    - Definition according to ISO 13600
    - Definition within the field of energetics
categories:
    - Thermodynamics
---
An energy carrier is a substance (energy form) or sometimes a phenomenon (energy system) that contains energy that can be later converted to other forms such as mechanical work or heat or to operate chemical or physical processes. Such carriers include springs, electrical batteries, capacitors, pressurized air, dammed water, hydrogen, petroleum, coal, wood, and natural gas. An energy carrier does not produce energy; it simply contains energy imbued by another system.
