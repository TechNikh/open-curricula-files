---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laser
offline_file: ""
offline_thumbnail: ""
uuid: 232b26ac-e486-491e-96a2-bbcd182dda5a
updated: 1484310214
title: laser
categories:
    - Dictionary
---
laser
     n : an acronym for light amplification by stimulated emission of
         radiation; an optical device that produces an intense
         monochromatic beam of coherent light [syn: {optical maser}]
