---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/volcanic
offline_file: ""
offline_thumbnail: ""
uuid: a3014d2a-04f1-47d7-91a6-35493f887ac0
updated: 1484310437
title: volcanic
categories:
    - Dictionary
---
volcanic
     adj 1: relating to or produced by or consisting of volcanoes;
            "volcanic steam"; "volcanic islands such as Iceland";
            "a volcanic cone is a conical mountain or hill built
            up of material from volcanic eruptions"
     2: explosively unstable; "a volcanic temper"
     3: igneous rock produced by eruption and solidified on or near
        the earth's surface; rhyolite or andesite or basalt;
        "volcanic rock includes the volcanic glass obsidian" [syn:
         {eruptive}]
