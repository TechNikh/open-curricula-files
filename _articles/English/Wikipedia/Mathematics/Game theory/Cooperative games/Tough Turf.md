---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tough_Turf
offline_file: ""
offline_thumbnail: ""
uuid: 33596cdd-c8e1-4dd6-b0f6-62e33e564ebf
updated: 1484308528
title: Tough Turf
tags:
    - Gameplay
    - Reception
categories:
    - Cooperative games
---
Tough Turf (タフターフ?) is a 1989 2D beat 'em up arcade game developed by Sunsoft and Sega. It was published by Sega in Japan and by Sunsoft of America in North America.
