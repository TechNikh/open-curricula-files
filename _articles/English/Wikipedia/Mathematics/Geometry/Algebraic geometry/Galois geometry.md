---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Galois_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 8b47eccc-5b81-4400-b626-87c184fd54e4
updated: 1484309147
title: Galois geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fano_plane.svg_0.png
tags:
    - Notes
categories:
    - Algebraic geometry
---
Galois geometry (so named after the 19th century French Mathematician Évariste Galois) is the branch of finite geometry that is concerned with algebraic and analytic geometry over a finite field (or Galois field).[1] More narrowly, a Galois geometry may be defined as a projective space over a finite field.[2]
