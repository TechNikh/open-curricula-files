---
version: 1
type: video
provider: YouTube
id: Z2zA9nPFN5A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Gamma-Ray%20Bursts-%20Crash%20Course%20Astronomy%20%2340.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 270d3ef4-533e-4fa0-8d9e-f8d6e8de962c
updated: 1486069685
title: 'Gamma-Ray Bursts- Crash Course Astronomy #40'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/Z2zA9nPFN5A/default.jpg
    - https://i3.ytimg.com/vi/Z2zA9nPFN5A/1.jpg
    - https://i3.ytimg.com/vi/Z2zA9nPFN5A/2.jpg
    - https://i3.ytimg.com/vi/Z2zA9nPFN5A/3.jpg
---
