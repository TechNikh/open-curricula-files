---
version: 1
type: video
provider: YouTube
id: nfnWBvZo50c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 59369fc6-6d79-4ecf-a4aa-91d12a3fbc0f
updated: 1486069648
title: '[HD 1080p] 世界一美しい日本の花火大会 fireworks Japan beautiful in the world/'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/nfnWBvZo50c/default.jpg
    - https://i3.ytimg.com/vi/nfnWBvZo50c/1.jpg
    - https://i3.ytimg.com/vi/nfnWBvZo50c/2.jpg
    - https://i3.ytimg.com/vi/nfnWBvZo50c/3.jpg
---
