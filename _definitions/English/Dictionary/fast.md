---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fast
offline_file: ""
offline_thumbnail: ""
uuid: b0c0a831-6b63-48e2-8e85-e0d997557515
updated: 1484310311
title: fast
categories:
    - Dictionary
---
fast
     adj 1: acting or moving or capable of acting or moving quickly;
            "fast film"; "on the fast track in school"; "set a
            fast pace"; "a fast car" [ant: {slow}]
     2: (used of timepieces) indicating a time ahead of or later
        than the correct time; "my watch is fast" [ant: {slow}]
     3: at a rapid tempo; "the band played a fast fox trot" [ant: {slow}]
     4: (of surfaces) conducive to rapid speeds; "a fast road";
        "grass courts are faster than clay"
     5: firmly fastened or secured against opening; "windows and
        doors were all fast"; "a locked closet"; "left the house
        properly secured" [syn: {barred}, {bolted}, {latched}, ...
