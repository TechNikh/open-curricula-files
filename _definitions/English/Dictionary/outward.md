---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outward
offline_file: ""
offline_thumbnail: ""
uuid: b9ed882d-b264-4a54-8707-8ba3084bc9ed
updated: 1484310365
title: outward
categories:
    - Dictionary
---
outward
     adj 1: relating to physical reality rather than with thoughts or
            the mind; "a concern with outward beauty rather than
            with inward reflections" [ant: {inward}]
     2: that is going out or leaving; "the departing train"; "an
        outward journey"; "outward-bound ships" [syn: {departing(a)},
         {outbound}, {outward-bound}]
     adv : toward the outside; "move the needle further outward!" [syn:
            {outwards}] [ant: {inward}]
