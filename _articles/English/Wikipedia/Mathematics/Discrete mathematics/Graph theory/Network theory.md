---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Network_theory
offline_file: ""
offline_thumbnail: ""
uuid: 47224628-7453-4e2d-8002-0c0d61ef02fd
updated: 1484308620
title: Network theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Small_Network.png
tags:
    - Network optimization
    - Network analysis
    - Social network analysis
    - Biological network analysis
    - Narrative network analysis
    - Link analysis
    - Network robustness
    - Web link analysis
    - Centrality measures
    - Assortative and disassortative mixing
    - Recurrence networks
    - Spread
    - Interdependent networks
    - Implementations
    - Books
categories:
    - Graph theory
---
In computer and network science, network theory is the study of graphs as a representation of either symmetric relations or, more generally, of asymmetric relations between discrete objects. Network theory is a part of graph theory: a network can be defined as a graph in which nodes and/or edges have attributes (e.g. names).
