---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assess
offline_file: ""
offline_thumbnail: ""
uuid: a969abdc-7857-400d-8dd2-04bfeb6c26b0
updated: 1484310480
title: assess
categories:
    - Dictionary
---
assess
     v 1: place a value on; judge the worth of something; "I will have
          the family jewels appraised by a professional" [syn: {measure},
           {evaluate}, {valuate}, {appraise}, {value}]
     2: charge (a person or a property) with a payment, such as a
        tax or a fine
     3: set or determine the amount of (a payment such as a fine)
        [syn: {tax}]
     4: estimate the value of (property) for taxation; "Our house
        hasn't been assessed in years"
