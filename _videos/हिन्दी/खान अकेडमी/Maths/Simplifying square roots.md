---
version: 1
type: video
provider: YouTube
id: 95V3ucw626s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a73ca3ee-ae44-4770-95c8-3a26844a78aa
updated: 1486069611
title: Simplifying square roots
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/95V3ucw626s/default.jpg
    - https://i3.ytimg.com/vi/95V3ucw626s/1.jpg
    - https://i3.ytimg.com/vi/95V3ucw626s/2.jpg
    - https://i3.ytimg.com/vi/95V3ucw626s/3.jpg
---
