---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/package
offline_file: ""
offline_thumbnail: ""
uuid: 78cfff12-befd-4ce0-82d9-3b0231591d6d
updated: 1484310567
title: package
categories:
    - Dictionary
---
package
     n 1: a collection of things wrapped or boxed together [syn: {bundle},
           {packet}, {parcel}]
     2: a wrapped container [syn: {parcel}]
     3: (computer science) written programs or procedures or rules
        and associated documentation pertaining to the operation
        of a computer system and that are stored in read/write
        memory; "the market for software is expected to expand"
        [syn: {software}, {software system}, {software package}]
        [ant: {hardware}]
     v : put into a box; "box the gift, please" [syn: {box}] [ant: {unbox}]
