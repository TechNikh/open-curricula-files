---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_books_in_computational_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 7d1a94d7-b0d1-440d-acef-429f0b5fbf08
updated: 1484309170
title: List of books in computational geometry
tags:
    - Combinatorial computational geometry
    - General-purpose textbooks
    - Specialized textbooks and monographs
    - >
        Numerical computational geometry (geometric modelling,
        computer-aided geometric design)
    - Monographs
    - Other
    - Conferences
    - Paper collections
categories:
    - Computational geometry
---
