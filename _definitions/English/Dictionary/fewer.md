---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fewer
offline_file: ""
offline_thumbnail: ""
uuid: 79fe4c2c-6344-4aeb-b14d-f29bf8516abf
updated: 1484310268
title: fewer
categories:
    - Dictionary
---
fewer
     adj : (comparative of `few' used with count nouns) quantifier
           meaning a smaller number of; "fewer birds came this
           year"; "the birds are fewer this year"; "fewer trains
           were late" [ant: {more(a)}]
