---
version: 1
type: video
provider: YouTube
id: KlWpFLfLFBI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Tides-%20Crash%20Course%20Astronomy%20%238.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 18ab5b54-7baf-4b47-80c7-b14cf5df74f5
updated: 1486069684
title: 'Tides- Crash Course Astronomy #8'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/KlWpFLfLFBI/default.jpg
    - https://i3.ytimg.com/vi/KlWpFLfLFBI/1.jpg
    - https://i3.ytimg.com/vi/KlWpFLfLFBI/2.jpg
    - https://i3.ytimg.com/vi/KlWpFLfLFBI/3.jpg
---
