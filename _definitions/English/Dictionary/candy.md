---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/candy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484393401
title: candy
categories:
    - Dictionary
---
candy
     n : a rich sweet made of flavored sugar and often combined with
         fruit or nuts
     v : coat with something sweet, such as a hard sugar glaze [syn:
         {sugarcoat}, {glaze}]
     [also: {candied}]
