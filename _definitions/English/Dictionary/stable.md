---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stable
offline_file: ""
offline_thumbnail: ""
uuid: ea8f333f-62b8-43cb-882c-e04591b7d298
updated: 1484310234
title: stable
categories:
    - Dictionary
---
stable
     adj 1: resistant to change of position or condition; "a stable
            ladder"; "a stable peace"; "a stable relationship";
            "stable prices" [ant: {unstable}]
     2: firm and dependable; subject to little fluctuation; "the
        economy is stable"
     3: not taking part readily in chemical change
     4: maintaining equilibrium
     5: showing little if any change; "a static population" [syn: {static},
         {unchanging}]
     n : a farm building for housing horses or other livestock [syn:
         {stalls}, {horse barn}]
     v : shelter in a stable; "stable horses"
