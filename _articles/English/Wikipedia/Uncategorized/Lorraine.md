---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lorraine
offline_file: ""
offline_thumbnail: ""
uuid: 04d7c518-c70b-46a0-857b-1145a21853b1
updated: 1484308476
title: Lorraine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nancy_Porte_Here_BW_2015-07-18_13-45-28.jpg
tags:
    - History
    - Development of the borders of Lorraine in modern history
    - geography
    - Language and culture
    - Cross of Lorraine
    - Cuisine
    - Beverages
    - Traditions
    - Housing
    - Economy
categories:
    - Uncategorized
---
Lorraine (French pronunciation: ​[lɔʁɛn]; Lorrain: Louréne; Lorraine Franconian: Lottringe; German:  Lothringen (help·info); Luxembourgish: Loutrengen, Polish: Lotaryngia) is a cultural and historical region in north-eastern France, now located in the administrative region of Grand Est. Lorraine's name stems from the medieval kingdom of Lotharingia, which in turn was named for either Emperor Lothair I or King Lothair II. It became later the Duchy of Lorraine before it was annexed to France in 1766.
