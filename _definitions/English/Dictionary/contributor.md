---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contributor
offline_file: ""
offline_thumbnail: ""
uuid: 7a09761a-33fc-4fb2-a810-dbc034423e5a
updated: 1484310453
title: contributor
categories:
    - Dictionary
---
contributor
     n 1: someone who contributes (or promises to contribute) a sum of
          money [syn: {subscriber}]
     2: a writer whose work is published in a newspaper or magazine
        or as part of a book
