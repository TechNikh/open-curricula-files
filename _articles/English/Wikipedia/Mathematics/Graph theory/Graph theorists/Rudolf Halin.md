---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rudolf_Halin
offline_file: ""
offline_thumbnail: ""
uuid: ea88be49-8f74-487b-aa30-fc58c4199dd6
updated: 1484309463
title: Rudolf Halin
tags:
    - Selected publications
    - Research papers
    - Textbooks
categories:
    - Graph theorists
---
Rudolf Halin (February 3, 1934, Uerdingen – November 14, 2014, Mölln)[1] was a German graph theorist, known for defining the ends of infinite graphs,[2] for Halin's grid theorem,[3][4] for extending Menger's theorem to infinite graphs,[5] and for his early research on treewidth and tree decomposition.[6] He is also the namesake of Halin graphs, a class of planar graphs constructed from trees by adding a cycle through the leaves of the given tree; earlier researchers had studied the subclass of cubic Halin graphs but Halin was the first to study this class of graphs in full generality.[7]
