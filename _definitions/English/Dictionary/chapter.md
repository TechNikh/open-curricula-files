---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chapter
offline_file: ""
offline_thumbnail: ""
uuid: 653b71ff-c240-48b1-9f23-f73b4108664b
updated: 1484310309
title: chapter
categories:
    - Dictionary
---
chapter
     n 1: a subdivision of a written work; usually numbered and
          titled; "he read a chapter every night before falling
          asleep"
     2: any distinct period in history or in a person's life; "the
        industrial revolution opened a new chapter in British
        history"; "the divorce was an ugly chapter in their
        relationship"
     3: a local branch of some fraternity or association; "he joined
        the Atlanta chapter"
     4: an ecclesiastical assembly of the monks in a monastery or
        even of the canons of a church
     5: a series of related events forming an episode; "a chapter of
        disasters"
