---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rainwater
offline_file: ""
offline_thumbnail: ""
uuid: 625d499c-4569-4b99-a3d0-91fa86c49bf7
updated: 1484310256
title: rainwater
categories:
    - Dictionary
---
rainwater
     n : drops of fresh water that fall as precipitation from clouds
         [syn: {rain}]
