---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rectilinear_propagation
offline_file: ""
offline_thumbnail: ""
uuid: b31cfaac-0e75-497c-a2da-d1ce9108138c
updated: 1484308894
title: Rectilinear propagation
categories:
    - Waves
---
Electromagnetic waves (light). Even though a wave front may be bent (e.g. the waves created by a rock hitting a pond) the individual waves are moving in straight lines. In the sense of the scattering of waves by an inhomogeneous medium, this situation corresponds to the case n ≠ 1, where n is the refractive index of the material.
