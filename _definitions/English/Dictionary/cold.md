---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cold
offline_file: ""
offline_thumbnail: ""
uuid: 89a36180-8430-4861-a5b0-31764e1c78b8
updated: 1484310353
title: cold
categories:
    - Dictionary
---
cold
     adj 1: used of physical coldness; having a low or inadequate
            temperature or feeling a sensation of coldness or
            having been made cold by e.g. ice or refrigeration; "a
            cold climate"; "a cold room"; "dinner has gotten
            cold"; "cold fingers"; "if you are cold, turn up the
            heat"; "a cold beer" [ant: {hot}]
     2: extended meanings; especially of psychological coldness;
        without human warmth or emotion; "a cold unfriendly nod";
        "a cold and unaffectionate person"; "a cold impersonal
        manner"; "cold logic"; "the concert left me cold" [ant: {hot}]
     3: having lost freshness through passage of time; "a ...
