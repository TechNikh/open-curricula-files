---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484560981
title: spate
categories:
    - Dictionary
---
spate
     n 1: (often followed by `of') a large number or amount or extent;
          "a batch of letters"; "a deal of trouble"; "a lot of
          money"; "he made a mint on the stock market"; "it must
          have cost plenty" [syn: {batch}, {deal}, {flock}, {good
          deal}, {great deal}, {hatful}, {heap}, {lot}, {mass}, {mess},
           {mickle}, {mint}, {muckle}, {peck}, {pile}, {plenty}, {pot},
           {quite a little}, {raft}, {sight}, {slew}, {stack}, {tidy
          sum}, {wad}, {whole lot}, {whole slew}]
     2: a sudden forceful flow [syn: {rush}, {surge}, {upsurge}]
     3: the occurrence of a water flow resulting from sudden rain or
        melting snow [syn: ...
