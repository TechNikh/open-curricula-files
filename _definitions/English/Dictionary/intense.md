---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intense
offline_file: ""
offline_thumbnail: ""
uuid: a424f949-19d0-47a8-8622-dc6f8ee87257
updated: 1484310305
title: intense
categories:
    - Dictionary
---
intense
     adj 1: in an extreme degree; "intense heat"; "intense anxiety";
            "intense desire"; "intense emotion"; "the skunk's
            intense acrid odor"; "intense pain"; "enemy fire was
            intense" [ant: {mild}]
     2: extremely sharp or intense; "acute pain"; "felt acute
        annoyance"; "intense itching and burning" [syn: {acute}]
     3: (of color) having the highest saturation; "vivid green";
        "intense blue" [syn: {vivid}]
