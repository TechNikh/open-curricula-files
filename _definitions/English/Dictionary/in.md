---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/in
offline_file: ""
offline_thumbnail: ""
uuid: 2f9beddf-b691-467b-9956-e72dd632a86c
updated: 1484310359
title: in
categories:
    - Dictionary
---
in
     adj 1: holding office; "the in party" [syn: {in(p)}]
     2: directed or bound inward; "took the in bus"; "the in basket"
        [syn: {in(a)}]
     3: currently fashionable; "the in thing to do"; "large shoulder
        pads are in"
     n 1: a unit of length equal to one twelfth of a foot [syn: {inch}]
     2: a rare soft silvery metallic element; occurs in small
        quantities in sphalerite [syn: {indium}, {atomic number 49}]
     3: a state in midwestern United States [syn: {Indiana}, {Hoosier
        State}]
     adv 1: to or toward the inside of; "come in"; "smash in the door"
            [syn: {inwards}, {inward}]
     2: inside an enclosed space [ant: {out}]
