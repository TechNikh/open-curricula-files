---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Brownian_motor
offline_file: ""
offline_thumbnail: ""
uuid: 9fec28cf-0397-4267-a0ae-d1fabdc12dd3
updated: 1484308837
title: Brownian motor
categories:
    - Thermodynamics
---
Brownian motors are nano-scale or molecular devices by which thermally activated processes (chemical reactions) are controlled and used to generate directed motion in space and to do mechanical or electrical work. These tiny engines operate in an environment where viscosity dominates inertia, and where thermal noise makes moving in a specific direction as difficult as walking in a hurricane: the forces impelling these motors in the desired direction are minuscule in comparison with the random forces exerted by the environment. Because this type of motor is so strongly dependent on random thermal noise, Brownian motors are feasible only at the nanometer scale.
