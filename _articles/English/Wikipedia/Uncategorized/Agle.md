---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Agle
offline_file: ""
offline_thumbnail: ""
uuid: 3fcd208d-e2df-484e-970d-2de4b2c422e9
updated: 1484308323
title: Agle
categories:
    - Uncategorized
---
Agle is a village in the municipality of Snåsa in Nord-Trøndelag county, Norway. It is located along the Nordland Line about 8 kilometres (5.0 mi) north of the municipal center of Snåsa. The mountain Andorfjellet and the lake Andorsjøen lie to the east of the village.[2]
