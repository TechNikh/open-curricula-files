---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marking
offline_file: ""
offline_thumbnail: ""
uuid: 792b21b8-df89-4352-b1b3-e9e0c915b551
updated: 1484310309
title: marking
categories:
    - Dictionary
---
marking
     n 1: a distinguishing symbol; "the owner's mark was on all the
          sheep" [syn: {marker}, {mark}]
     2: a pattern of marks
     3: evaluation of performance by assigning a grade or score;
        "what he disliked about teaching was all the grading he
        had to do" [syn: {grading}, {scoring}]
     4: the act of making a visible mark on a surface
