---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tolerant
offline_file: ""
offline_thumbnail: ""
uuid: c475a6ab-81a3-4cb9-8543-f8619f3ff68d
updated: 1484310595
title: tolerant
categories:
    - Dictionary
---
tolerant
     adj 1: showing respect for the rights or opinions or practices of
            others [ant: {intolerant}]
     2: tolerant and forgiving under provocation; "our neighbor was
        very kind about the window our son broke" [syn: {kind}]
     3: showing or characterized by broad-mindedness; "a broad
        political stance"; "generous and broad sympathies"; "a
        liberal newspaper"; "tolerant of his opponent's opinions"
        [syn: {broad}, {large-minded}, {liberal}]
     4: showing the capacity for endurance; "injustice can make us
        tolerant and forgiving"; "a man patient of distractions"
        [syn: {patient of}]
