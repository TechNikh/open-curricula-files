---
version: 1
type: video
provider: YouTube
id: xznMJLV3u58
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Yes%20and%20No%20Questions%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 95d62781-c2aa-444f-b18b-4dfe0fb50f0c
updated: 1486069632
title: Yes and No Questions!
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/xznMJLV3u58/default.jpg
    - https://i3.ytimg.com/vi/xznMJLV3u58/1.jpg
    - https://i3.ytimg.com/vi/xznMJLV3u58/2.jpg
    - https://i3.ytimg.com/vi/xznMJLV3u58/3.jpg
---
