---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/camera
offline_file: ""
offline_thumbnail: ""
uuid: fbb87c00-9dec-418a-be65-a2be339d97ce
updated: 1484310221
title: camera
categories:
    - Dictionary
---
camera
     n 1: equipment for taking photographs (usually consisting of a
          lightproof box with a lens at one end and
          light-sensitive film at the other) [syn: {photographic
          camera}]
     2: television equipment consisting of a lens system that
        focuses an image on a photosensitive mosaic that is
        scanned by an electron beam [syn: {television camera}, {tv
        camera}]
     [also: {camerae} (pl)]
