---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paradoxes_of_set_theory
offline_file: ""
offline_thumbnail: ""
uuid: da6610ac-449d-4fd8-91bd-9a5fdc83114b
updated: 1484309507
title: Paradoxes of set theory
tags:
    - Basics
    - Cardinal numbers
    - Ordinal numbers
    - Power sets
    - Paradoxes of the infinite set
    - Paradoxes of enumeration
    - Je le vois, mais je ne crois pas
    - Paradoxes of well-ordering
    - Paradoxes of the Supertask
    - The diary of Tristram Shandy
    - The Ross-Littlewood paradox
    - Paradoxes of proof and definability
    - 'Early paradoxes: the set of all sets'
    - Paradoxes by change of language
    - "König's paradox"
    - "Richard's paradox"
    - Paradox of Löwenheim and Skolem
    - Notes
categories:
    - Set theory
---
This article contains a discussion of paradoxes of set theory. As with most mathematical paradoxes, they generally reveal surprising and counter-intuitive mathematical results, rather than actual logical contradictions within modern axiomatic set theory.
