---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shipped
offline_file: ""
offline_thumbnail: ""
uuid: 0a7b7384-ca30-4f3d-bf71-7cde5ada5395
updated: 1484310525
title: shipped
categories:
    - Dictionary
---
ship
     n : a vessel that carries passengers or freight
     v 1: transport commercially [syn: {transport}, {send}]
     2: hire for work on a ship
     3: go on board [syn: {embark}] [ant: {disembark}]
     4: travel by ship
     5: place on board a ship; "ship the cargo in the hold of the
        vessel"
     [also: {shipping}, {shipped}]
