---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unshaded
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484358781
title: unshaded
categories:
    - Dictionary
---
unshaded
     adj 1: (of pictures) not having shadow represented; "unshaded
            drawings resembling cartoons" [ant: {shaded}]
     2: not darkened or dimmed by shade; "an unshaded meadow"; "a
        bright and unshaded lane" [ant: {shaded}]
