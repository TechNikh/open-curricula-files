---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pyroelectricity
offline_file: ""
offline_thumbnail: ""
uuid: fa1e0db1-39aa-4510-a142-d1fa8e243d84
updated: 1484308857
title: Pyroelectricity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pyrosensor.jpg
tags:
    - Explanation
    - History
    - The pyroelectric crystal classes and piezoelectricity
    - Recent developments
    - Mathematical description
    - Power generation
categories:
    - Thermodynamics
---
Pyroelectricity (from the Greek pyr, fire, and electricity) is the property of certain crystals which are naturally electrically polarized, and as a result contain large electric fields.[1] Due to historical reasons, it is interpreted sometimes as the ability of certain materials to generate a temporary voltage when they are heated or cooled.[2] The change in temperature modifies the positions of the atoms slightly within the crystal structure, such that the polarization of the material changes. This polarization change gives rise to a voltage across the crystal. If the temperature stays constant at its new value, the pyroelectric voltage gradually disappears due to leakage current (the ...
