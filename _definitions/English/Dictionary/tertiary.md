---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tertiary
offline_file: ""
offline_thumbnail: ""
uuid: 63fa0350-2804-4179-8d42-08941b6fab2a
updated: 1484310275
title: tertiary
categories:
    - Dictionary
---
tertiary
     adj : coming next after the second and just before the fourth in
           position [syn: {third}, {3rd}]
     n : from 63 million to 2 million years ago [syn: {Tertiary
         period}]
