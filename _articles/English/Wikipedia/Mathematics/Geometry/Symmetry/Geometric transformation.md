---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometric_transformation
offline_file: ""
offline_thumbnail: ""
uuid: 8136787b-f01f-4d41-8b47-eca7332c4f7b
updated: 1484309311
title: Geometric transformation
categories:
    - Symmetry
---
A geometric transformation is any bijection of a set having some geometric structure to itself or another such set. Specifically, "A geometric transformation is a function whose domain and range are sets of points. Most often the domain and range of a geometric transformation are both R2 or both R3. Often geometric transformations are required to be 1-1 functions, so that they have inverses." [1] The study of geometry may be approached via the study of these transformations.[2]
