---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/treated
offline_file: ""
offline_thumbnail: ""
uuid: d69afc0b-b3eb-4a22-bf1a-7fceb4f049ec
updated: 1484310315
title: treated
categories:
    - Dictionary
---
treated
     adj 1: subjected to a physical (or chemical) treatment or action or
            agent; "the sludge of treated sewage can be used as
            fertilizer"; "treated timbers resist rot"; "treated
            fabrics resist wrinkling" [ant: {untreated}]
     2: (of a specimen for study under a microscope) treated with a
        reagent or dye that colors only certain structures
     3: given medical care or treatment; "a treated cold is usually
        gone in 14 days; if left untreated it lasts two weeks"
        [ant: {untreated}]
     4: made hard or flexible or resilient especially by heat
        treatment; "a sword of tempered steel"; "tempered glass"
        [syn: ...
