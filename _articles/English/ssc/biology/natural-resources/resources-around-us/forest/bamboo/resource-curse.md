---
title: Resource curse
article_ref: https://en.wikipedia.org/wiki/Resource_curse
tags:
    - Resource curse thesis
    - Economic effects
    - Dutch disease
    - Revenue volatility
    - Enclave effects
    - Human resources
    - Incomes and employment
    - Political effects
    - Armed conflict
    - Authoritarian rule
    - Gender inequality
    - International cooperation
    - Criticisms
categories:
    - Bamboo
---
