---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bunch
offline_file: ""
offline_thumbnail: ""
uuid: 8c9a6cc9-2a5f-4fed-b2db-67e4ca5bc000
updated: 1484310210
title: bunch
categories:
    - Dictionary
---
bunch
     n 1: a grouping of a number of similar things; "a bunch of
          trees"; "a cluster of admirers" [syn: {clump}, {cluster},
           {clustering}]
     2: an informal body of friends; "he still hangs out with the
        same crowd" [syn: {crowd}, {crew}, {gang}]
     3: any collection in its entirety; "she bought the whole
        caboodle" [syn: {lot}, {caboodle}]
     v 1: form into a bunch; "The frightened children bunched together
          in the corner of the classroom" [syn: {bunch together},
          {bunch up}]
     2: gather or cause to gather into a cluster; "She bunched her
        fingers into a fist"; "The students bunched up at the
        registration desk" ...
