---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Double_auction
offline_file: ""
offline_thumbnail: ""
uuid: c83fe281-70a1-46a3-bc15-beafc2f3cc83
updated: 1484309011
title: Double auction
tags:
    - Economic analysis
    - Natural ordering
    - Game-theoretic analysis
    - Mechanism design
    - Average mechanism
    - VCG mechanism
    - Trade reduction mechanism
    - "McAfee's mechanism"
    - Probabilistic reduction mechanisms
    - Comparison
    - Double auctions in a supply chain
    - Modular approach
    - Notes
categories:
    - Auctions
---
A double auction is a process of buying and selling goods when potential buyers submit their bids and potential sellers simultaneously submit their ask prices to an auctioneer, and then an auctioneer chooses some price p that clears the market: all the sellers who asked less than p sell and all buyers who bid more than p buy at this price p.
