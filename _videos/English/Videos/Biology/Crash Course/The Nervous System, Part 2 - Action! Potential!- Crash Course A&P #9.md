---
version: 1
type: video
provider: YouTube
id: OZG8M_ldA1M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c71aa37b-3e19-4889-aa5d-ff131e51c999
updated: 1486069681
title: 'The Nervous System, Part 2 - Action! Potential!- Crash Course A&P #9'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/OZG8M_ldA1M/default.jpg
    - https://i3.ytimg.com/vi/OZG8M_ldA1M/1.jpg
    - https://i3.ytimg.com/vi/OZG8M_ldA1M/2.jpg
    - https://i3.ytimg.com/vi/OZG8M_ldA1M/3.jpg
---
