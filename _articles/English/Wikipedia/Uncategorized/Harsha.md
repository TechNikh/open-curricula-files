---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Harsha
offline_file: ""
offline_thumbnail: ""
uuid: 3d11e6ed-b3b4-4042-8444-b24796f6bac9
updated: 1484308321
title: Harsha
tags:
    - Origins
    - Ascension
    - Reign
    - Author
categories:
    - Uncategorized
---
Harsha (c. 590–648), also known as Harshavardhana, was an Indian emperor who ruled North India from 606 to 648 from his capital Kannauj. He belonged to Pushyabhuti dynasty. He was the son of Prabhakarvardhana who defeated the Huna invaders[1] and the younger brother of Rajyavardhana, a king of Thanesar, Haryana. He was the founder and ruler of the Empire of Harsha and at the height of his power his empire spanned the Punjab, Rajasthan, Gujarat, Bengal, Odisha and the entire Indo-Gangetic plain north of the Narmada River. Harsha was defeated by the south Indian Emperor Pulakeshin II of the Chalukya dynasty when Harsha tried to expand his Empire into the southern peninsula of India.[2] ...
