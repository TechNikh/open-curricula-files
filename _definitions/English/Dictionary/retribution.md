---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retribution
offline_file: ""
offline_thumbnail: ""
uuid: 08e8e1d4-83b7-462c-ac8c-cd609282304d
updated: 1484310567
title: retribution
categories:
    - Dictionary
---
retribution
     n 1: a justly deserved penalty [syn: {requital}]
     2: the act of correcting for your wrongdoing
     3: the act of taking revenge (harming someone in retaliation
        for something harmful that they have done) especially in
        the next life; "Vengeance is mine; I will repay, saith the
        Lord"--Romans 12:19; "For vengeance I would do nothing.
        This nation is too great to look for mere revenge"--James
        Garfield; "he swore vengeance on the man who betrayed
        him"; "the swiftness of divine retribution" [syn: {vengeance},
         {payback}]
