---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/luckily
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484410201
title: luckily
categories:
    - Dictionary
---
luckily
     adv : by good fortune; "fortunately the weather was good" [syn: {fortunately},
            {fortuitously}, {as luck would have it}] [ant: {unfortunately},
            {unfortunately}]
