---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ehud_Kalai
offline_file: ""
offline_thumbnail: ""
uuid: 65db8782-5da8-4c9c-ad5f-20a9f13035a8
updated: 1484308513
title: Ehud Kalai
tags:
    - Biography
    - Selected Contributions
    - Selected publications
categories:
    - Game theorists
---
Ehud Kalai is a prominent American game theorist and mathematical economist known for his contributions to the field of game theory and its interface with economics, social choice, computer science and operations research. He is the James J. O’Connor Distinguished Professor of Decision and Game Sciences at Northwestern University, where he has taught since 1975.
