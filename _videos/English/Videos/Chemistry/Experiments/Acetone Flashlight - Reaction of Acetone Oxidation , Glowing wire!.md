---
version: 1
type: video
provider: YouTube
id: ZLcvM0aAWus
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Acetone%20Flashlight%20-%20Reaction%20of%20Acetone%20Oxidation%20%2C%20Glowing%20wire%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 32ae2ea2-6b20-4ee5-9e3f-a3ac91aadead
updated: 1486069661
title: 'Acetone Flashlight - Reaction of Acetone Oxidation , Glowing wire!'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZLcvM0aAWus/default.jpg
    - https://i3.ytimg.com/vi/ZLcvM0aAWus/1.jpg
    - https://i3.ytimg.com/vi/ZLcvM0aAWus/2.jpg
    - https://i3.ytimg.com/vi/ZLcvM0aAWus/3.jpg
---
