---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Firehose_instability
offline_file: ""
offline_thumbnail: ""
uuid: 40b50f5b-b167-4b81-9293-cf37192b388b
updated: 1484308823
title: Firehose instability
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Large_bonfire.jpg
tags:
    - 'Stability analysis: sheets and wires'
    - 'Stability analysis: finite-thickness galaxies'
    - Importance
categories:
    - Stability theory
---
The firehose instability (or hose-pipe instability) is a dynamical instability of thin or elongated galaxies. The instability causes the galaxy to buckle or bend in a direction perpendicular to its long axis. After the instability has run its course, the galaxy is less elongated (i.e. rounder) than before. Any sufficiently thin stellar system, in which some component of the internal velocity is in the form of random or counter-streaming motions (as opposed to rotation), is subject to the instability.
