---
version: 1
type: video
provider: YouTube
id: nFV9H6H3kKc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ea84e586-0d57-474b-bec2-ef74adfded4d
updated: 1486069609
title: Evaluating expressions with unknown variables
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/nFV9H6H3kKc/default.jpg
    - https://i3.ytimg.com/vi/nFV9H6H3kKc/1.jpg
    - https://i3.ytimg.com/vi/nFV9H6H3kKc/2.jpg
    - https://i3.ytimg.com/vi/nFV9H6H3kKc/3.jpg
---
