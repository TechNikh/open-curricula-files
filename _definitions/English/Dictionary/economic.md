---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/economic
offline_file: ""
offline_thumbnail: ""
uuid: eeaf759a-9724-4887-8159-069e771d35c2
updated: 1484310451
title: economic
categories:
    - Dictionary
---
economic
     adj 1: of or relating to an economy, the system of production and
            management of material wealth; "economic growth";
            "aspects of social, political, and economical life"
            [syn: {economical}]
     2: of or relating to the science of economics; "economic
        theory"
     3: concerned with worldly necessities of life (especially
        money); "he wrote the book primarily for economic
        reasons"; "gave up the large house for economic reasons";
        "in economic terms they are very privileged"
     4: financially rewarding; "it was no longer economic to keep
        the factory open"; "have to keep prices high enough to
        make ...
