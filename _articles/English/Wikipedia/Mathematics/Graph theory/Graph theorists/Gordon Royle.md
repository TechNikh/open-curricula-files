---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gordon_Royle
offline_file: ""
offline_thumbnail: ""
uuid: 65c60420-e197-414f-933e-ad9c81956728
updated: 1484309473
title: Gordon Royle
categories:
    - Graph theorists
---
Royle is the co-author (with Chris Godsil) of the book Algebraic Graph Theory (Springer Verlag, 2001, ISBN 0-387-95220-9). Royle is also known for his research into the mathematics of Sudoku and his search for the Sudoku puzzle with the smallest number of entries that has a unique solution.[2]
