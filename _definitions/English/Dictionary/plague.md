---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plague
offline_file: ""
offline_thumbnail: ""
uuid: c4938b21-689c-4646-a91a-5acb2a4443e3
updated: 1484310473
title: plague
categories:
    - Dictionary
---
plague
     n 1: a serious (sometimes fatal) infection of rodents caused by
          Yersinia pestis and accidentally transmitted to humans
          by the bite of an infected rat flea (especially bubonic
          plague)
     2: any epidemic disease with a high death rate [syn: {pestilence}]
     3: a swarm of insects that attack plants; "a plague of
        grasshoppers" [syn: {infestation}]
     4: any large scale calamity (especially when thought to be sent
        by God)
     5: an annoyance; "those children are a damn plague"
     v 1: cause to suffer a blight; "Too much rain may blight the
          garden with mold" [syn: {blight}]
     2: annoy continually or chronically; "He ...
