---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kayles
offline_file: ""
offline_thumbnail: ""
uuid: 0579f095-4070-48b4-90de-6dd5131c9967
updated: 1484308535
title: Kayles
tags:
    - Rules
    - History
    - Analysis
    - Applications
    - Computational complexity
categories:
    - Combinatorial game theory
---
