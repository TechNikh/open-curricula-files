---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mixer
offline_file: ""
offline_thumbnail: ""
uuid: 2e89ef19-87eb-4a87-a820-40d7befeac5a
updated: 1484310333
title: mixer
categories:
    - Dictionary
---
mixer
     n 1: a party of people assembled to promote sociability and
          communal activity [syn: {sociable}, {social}]
     2: club soda or fruit juice used to mix with alcohol
     3: electronic equipment that mixes two or more input signals to
        give a single output signal
     4: a kitchen utensil that is used for mixing foods
