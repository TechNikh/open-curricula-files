---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Perfect_digit-to-digit_invariant
offline_file: ""
offline_thumbnail: ""
uuid: 4a7956db-77d8-4478-965c-bbeac8b1bfbc
updated: 1484308657
title: Perfect digit-to-digit invariant
categories:
    - Number theory
---
A perfect digit-to-digit invariant (PDDI) (also known as a Munchausen number[1]) is a natural number that is equal to the sum of its digits each raised to a power equal to the digit.
