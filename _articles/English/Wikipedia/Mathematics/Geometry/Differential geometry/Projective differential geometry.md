---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Projective_differential_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 0df7a383-17a8-4250-8023-767415142f78
updated: 1484309202
title: Projective differential geometry
categories:
    - Differential geometry
---
In mathematics, projective differential geometry is the study of differential geometry, from the point of view of properties of mathematical objects such as functions, diffeomorphisms, and submanifolds, that are invariant under transformations of the projective group. This is a mixture of the approaches from Riemannian geometry of studying invariances, and of the Erlangen program of characterizing geometries according to their group symmetries.
