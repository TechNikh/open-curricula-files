---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stirling_permutation
offline_file: ""
offline_thumbnail: ""
uuid: 36a50db8-2249-4c52-b615-efd5c2fdf78b
updated: 1484308592
title: Stirling permutation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Stirling_permutation_Euler_tour.svg.png
categories:
    - Combinatorics
---
In combinatorial mathematics, a Stirling permutation of order k is a permutation of the multiset 1, 1, 2, 2, ..., k, k (with two copies of each value from 1 to k) with the additional property that, for each value i appearing in the permutation, the values between the two copies of i are larger than i. For instance, the 15 Stirling permutations of order three are
