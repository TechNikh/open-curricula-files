---
version: 1
type: video
provider: YouTube
id: JbqtqCunYzA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Partial%20Pressures%20%26%20Vapor%20Pressure-%20Crash%20Course%20Chemistry%20%2315.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a2af388d-3c11-450b-8bbc-f66ee48a3c37
updated: 1486069680
title: 'Partial Pressures & Vapor Pressure- Crash Course Chemistry #15'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/JbqtqCunYzA/default.jpg
    - https://i3.ytimg.com/vi/JbqtqCunYzA/1.jpg
    - https://i3.ytimg.com/vi/JbqtqCunYzA/2.jpg
    - https://i3.ytimg.com/vi/JbqtqCunYzA/3.jpg
---
