---
version: 1
type: video
provider: YouTube
id: Yidp95_4bQo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fc663262-643b-4280-b24e-e807a2d25dbe
updated: 1486069629
title: Learn Hindi Grammar -Vachya (वाच्य) Voice
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/Yidp95_4bQo/default.jpg
    - https://i3.ytimg.com/vi/Yidp95_4bQo/1.jpg
    - https://i3.ytimg.com/vi/Yidp95_4bQo/2.jpg
    - https://i3.ytimg.com/vi/Yidp95_4bQo/3.jpg
---
