---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/empty
offline_file: ""
offline_thumbnail: ""
uuid: 1b3f77ae-68a9-4603-827b-1eb709e11d0e
updated: 1484310353
title: empty
categories:
    - Dictionary
---
empty
     adj 1: holding or containing nothing; "an empty glass"; "an empty
            room"; "full of empty seats"; "empty hours" [ant: {full}]
     2: devoid of significance or point; "empty promises"; "a hollow
        victory"; "vacuous comments" [syn: {hollow}, {vacuous}]
     3: having nothing inside; "an empty sphere"
     4: needing nourishment; "after skipped lunch the men were empty
        by suppertime"; "empty-bellied children" [syn: {empty-bellied}]
     5: emptied of emotion; "after the violent argument he felt
        empty"
     n : a container that has been emptied; "return all empties to
         the store"
     v 1: make void or empty of contents; "Empty the box"; "The ...
