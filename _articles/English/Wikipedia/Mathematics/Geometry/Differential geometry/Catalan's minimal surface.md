---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Catalan%27s_minimal_surface'
offline_file: ""
offline_thumbnail: ""
uuid: 7f326f40-7322-4a4c-b023-6adab937395d
updated: 1484309209
title: "Catalan's minimal surface"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Catalan%2527s_Minimal_Surface_0.png'
categories:
    - Differential geometry
---
It has the special property of being the minimal surface that contains a cycloid as a geodesic. It is also swept out by a family of parabolae.[2]
