---
version: 1
type: video
provider: YouTube
id: 87Gha8rUNFE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7b4c1b25-934a-4789-9e33-e23f07a0a3e9
updated: 1486069621
title: 'Example 3- Using the quadratic formula'
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/87Gha8rUNFE/default.jpg
    - https://i3.ytimg.com/vi/87Gha8rUNFE/1.jpg
    - https://i3.ytimg.com/vi/87Gha8rUNFE/2.jpg
    - https://i3.ytimg.com/vi/87Gha8rUNFE/3.jpg
---
