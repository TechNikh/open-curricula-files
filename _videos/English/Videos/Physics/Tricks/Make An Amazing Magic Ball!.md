---
version: 1
type: video
provider: YouTube
id: 7yZwj111f_4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 68640eaa-9ec7-4785-b37b-97c04342c5e5
updated: 1486069661
title: Make An Amazing Magic Ball!
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/7yZwj111f_4/default.jpg
    - https://i3.ytimg.com/vi/7yZwj111f_4/1.jpg
    - https://i3.ytimg.com/vi/7yZwj111f_4/2.jpg
    - https://i3.ytimg.com/vi/7yZwj111f_4/3.jpg
---
