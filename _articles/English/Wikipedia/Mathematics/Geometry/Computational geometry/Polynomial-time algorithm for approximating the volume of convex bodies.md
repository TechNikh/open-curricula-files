---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Polynomial-time_algorithm_for_approximating_the_volume_of_convex_bodies
offline_file: ""
offline_thumbnail: ""
uuid: 4cb1cec5-1132-4bc2-97b4-ded1c44ac84f
updated: 1484309176
title: >
    Polynomial-time algorithm for approximating the volume of
    convex bodies
categories:
    - Computational geometry
---
The main result of the paper is a randomized algorithm for finding an 
  
    
      
        ϵ
      
    
    {\displaystyle \epsilon }
  
 approximation to the volume of a convex body 
  
    
      
        K
      
    
    {\displaystyle K}
  
 in 
  
    
      
        n
      
    
    {\displaystyle n}
  
-dimensional Euclidean space by assuming the existence of a membership oracle. The algorithm takes time bounded by a polynomial in 
  
    
      
        n
      
    
    {\displaystyle n}
  
, the dimension of 
  
    
      
        K
      
    
    {\displaystyle K}
  
 and 
  
    
      
        1
        
          /
        
        ϵ
      
    
    {\displaystyle ...
