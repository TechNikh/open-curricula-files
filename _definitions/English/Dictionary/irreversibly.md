---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irreversibly
offline_file: ""
offline_thumbnail: ""
uuid: acb76425-31da-4556-a7d9-a65770d5f1f6
updated: 1484310541
title: irreversibly
categories:
    - Dictionary
---
irreversibly
     adv : in an irreversible manner; "this old tradition is
           irreversibly disappearing"
