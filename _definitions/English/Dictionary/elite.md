---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elite
offline_file: ""
offline_thumbnail: ""
uuid: a8fab404-18c4-4ec4-b393-4f01687660c5
updated: 1484310571
title: elite
categories:
    - Dictionary
---
elite
     adj : selected as the best; "an elect circle of artists"; "elite
           colleges" [syn: {elect}]
     n : a group or class of persons enjoying superior intellectual
         or social or economic status [syn: {elite group}]
