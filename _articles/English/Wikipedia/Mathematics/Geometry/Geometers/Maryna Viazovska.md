---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maryna_Viazovska
offline_file: ""
offline_thumbnail: ""
uuid: 6e2f272a-38fb-4f00-9bfc-7e7c445bbc44
updated: 1484309110
title: Maryna Viazovska
categories:
    - Geometers
---
Maryna Sergiivna Viazovska[1] (Ukrainian: Марина Сергіївна В'язовська;[2] born 1984)[3] is a Ukrainian mathematician who in 2016 solved the sphere-packing problem in dimension 8[4][5][6] and, in collaboration with others, in dimension 24.[7][8] Previously, the problem had been solved only for three or fewer dimensions, and the proof of the three-dimensional version (the Kepler conjecture) involved long computer calculations. In contrast, Viazovska's proof for 8 and 24 dimensions is "stunningly simple".[8]
