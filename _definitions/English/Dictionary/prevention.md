---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prevention
offline_file: ""
offline_thumbnail: ""
uuid: 793776f4-43ad-46e6-8d91-e095b57de93d
updated: 1484310234
title: prevention
categories:
    - Dictionary
---
prevention
     n : the act of preventing; "there was no bar against leaving";
         "money was allocated to study the cause and prevention of
         influenza" [syn: {bar}]
