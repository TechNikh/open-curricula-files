---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/musical
offline_file: ""
offline_thumbnail: ""
uuid: fa49e657-6dc1-470a-8301-1e11fab74a01
updated: 1484310397
title: musical
categories:
    - Dictionary
---
musical
     adj 1: characterized by or capable of producing music; "a musical
            evening"; "musical instruments"
     2: talented in or devoted to music; "comes from a very musical
        family" [ant: {unmusical}]
     3: characteristic of or resembling or accompanied by music; "a
        musical speaking voice"; "a musical comedy" [ant: {unmusical}]
     4: containing or constituting or characterized by pleasing
        melody; "the melodious song of a meadowlark" [syn: {melodious},
         {melodic}] [ant: {unmelodious}]
     n : a play or film whose action and dialogue is interspersed
         with singing and dancing [syn: {musical comedy}, {musical
         theater}]
