---
version: 1
type: video
provider: YouTube
id: 0RnPJ1JBk8k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 32a3d742-f8cf-422d-b345-7bf536ece2db
updated: 1486069651
title: Solar Eclipses II
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/0RnPJ1JBk8k/default.jpg
    - https://i3.ytimg.com/vi/0RnPJ1JBk8k/1.jpg
    - https://i3.ytimg.com/vi/0RnPJ1JBk8k/2.jpg
    - https://i3.ytimg.com/vi/0RnPJ1JBk8k/3.jpg
---
