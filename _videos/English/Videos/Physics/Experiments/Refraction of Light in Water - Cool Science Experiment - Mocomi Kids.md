---
version: 1
type: video
provider: YouTube
id: o08jgkut7e8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ee902af1-a1f0-4e7c-9313-de879b2146a5
updated: 1486069639
title: 'Refraction of Light in Water - Cool Science Experiment | Mocomi Kids'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/o08jgkut7e8/default.jpg
    - https://i3.ytimg.com/vi/o08jgkut7e8/1.jpg
    - https://i3.ytimg.com/vi/o08jgkut7e8/2.jpg
    - https://i3.ytimg.com/vi/o08jgkut7e8/3.jpg
---
