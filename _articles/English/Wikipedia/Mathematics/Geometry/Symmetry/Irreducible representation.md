---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Irreducible_representation
offline_file: ""
offline_thumbnail: ""
uuid: b8c9cb43-af82-49de-a192-52185c7b3ce6
updated: 1484309311
title: Irreducible representation
tags:
    - History
    - Overview
    - Notation and terminology of group representations
    - Decomposable and Indecomposable representations
    - Applications in theoretical physics and chemistry
    - Lie groups
    - Lorentz group
    - Associative algebras
    - Lie groups
    - Books
    - Papers
categories:
    - Symmetry
---
In mathematics, specifically in the representation theory of groups and algebras, an irreducible representation 
  
    
      
        (
        ρ
        ,
        V
        )
      
    
    {\displaystyle (\rho ,V)}
  
 or irrep of an algebraic structure 
  
    
      
        A
      
    
    {\displaystyle A}
  
 is a nonzero representation that has no proper subrepresentation 
  
    
      
        (
        ρ
        
          
            |
          
          
            W
          
        
        ,
        W
        )
        ,
        W
        ⊂
        V
      
    
    {\displaystyle (\rho |_{W},W),W\subset V}
  
 closed under the action of 
  
    
      
       ...
