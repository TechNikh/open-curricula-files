---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illusion
offline_file: ""
offline_thumbnail: ""
uuid: 5be21ffe-64d5-4000-bc38-d2a15252e0b4
updated: 1484310210
title: illusion
categories:
    - Dictionary
---
illusion
     n 1: an erroneous mental representation [syn: {semblance}]
     2: something many people believe that is false; "they have the
        illusion that I am very wealthy" [syn: {fantasy}, {phantasy},
         {fancy}]
     3: the act of deluding; deception by creating illusory ideas
        [syn: {delusion}, {head game}]
     4: an illusory feat; considered magical by naive observers
        [syn: {magic trick}, {conjuring trick}, {trick}, {magic},
        {legerdemain}, {conjuration}, {deception}]
