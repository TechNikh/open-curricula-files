---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arithmetic_variety
offline_file: ""
offline_thumbnail: ""
uuid: 10014632-9dee-4f73-8feb-d446c93412b4
updated: 1484308542
title: Arithmetic variety
tags:
    - "Kazhdan's theorem"
categories:
    - Number theory
---
In mathematics, an arithmetic variety is the quotient space of a Hermitian symmetric space by an arithmetic subgroup of the associated algebraic Lie group.
