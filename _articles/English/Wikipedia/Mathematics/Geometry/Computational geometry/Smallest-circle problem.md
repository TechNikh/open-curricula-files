---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Smallest-circle_problem
offline_file: ""
offline_thumbnail: ""
uuid: d7238f7b-0b4c-4a9a-91b5-d25a2c053cfd
updated: 1484309176
title: Smallest-circle problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Smallest_circle_problem.svg.png
tags:
    - Characterization
    - Linear-time solutions
    - Other algorithms
    - Weighted variants of the problem
categories:
    - Computational geometry
---
The smallest-circle problem or minimum covering circle problem is a mathematical problem of computing the smallest circle that contains all of a given set of points in the Euclidean plane. The corresponding problem in n-dimensional space, the smallest bounding-sphere problem, is to compute the smallest n-sphere that contains all of a given set of points.[1] The smallest-circle problem was initially proposed by the English mathematician James Joseph Sylvester in 1857.[2]
