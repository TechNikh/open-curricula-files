---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attempt
offline_file: ""
offline_thumbnail: ""
uuid: f0052dbf-e77e-4d83-b2f5-089f412e92f6
updated: 1484310389
title: attempt
categories:
    - Dictionary
---
attempt
     n 1: earnest and conscientious activity intended to do or
          accomplish something; "made an effort to cover all the
          reading material"; "wished him luck in his endeavor";
          "she gave it a good try" [syn: {effort}, {endeavor}, {endeavour},
           {try}]
     2: the act of attacking; "attacks on women increased last
        year"; "they made an attempt on his life" [syn: {attack}]
     v 1: make an effort or attempt; "He tried to shake off his
          fears"; "The infant had essayed a few wobbly steps";
          "The police attempted to stop the thief"; "He sought to
          improve himself"; "She always seeks to do good in the
          world" ...
