---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Law_of_cotangents
offline_file: ""
offline_thumbnail: ""
uuid: 73fbbf79-e5fc-4939-bc76-74a17d6d27f3
updated: 1484309343
title: Law of cotangents
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/512px-Johnny-automatic-scales-of-justice.svg_0.png
tags:
    - Statement
    - Proof
    - Some proofs using the law of cotangents
categories:
    - Trigonometry
---
In trigonometry, the law of cotangents[1] is a relationship among the lengths of the sides of a triangle and the cotangents of the halves of the three angles.
