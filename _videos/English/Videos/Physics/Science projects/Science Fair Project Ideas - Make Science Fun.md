---
version: 1
type: video
provider: YouTube
id: 3fddzWNy6Lo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d47af22a-a595-4698-a554-94a87038057c
updated: 1486069634
title: 'Science Fair Project Ideas - Make Science Fun'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/3fddzWNy6Lo/default.jpg
    - https://i3.ytimg.com/vi/3fddzWNy6Lo/1.jpg
    - https://i3.ytimg.com/vi/3fddzWNy6Lo/2.jpg
    - https://i3.ytimg.com/vi/3fddzWNy6Lo/3.jpg
---
