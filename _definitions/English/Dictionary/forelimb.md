---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forelimb
offline_file: ""
offline_thumbnail: ""
uuid: 018e53ea-77a9-4f71-8d5c-336850e1e5d5
updated: 1484310289
title: forelimb
categories:
    - Dictionary
---
forelimb
     n : the front limb (or homologous structure in other animals
         such as a flipper or wing)
