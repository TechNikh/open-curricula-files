---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fast-growing_hierarchy
offline_file: ""
offline_thumbnail: ""
uuid: 8fe07460-379f-47fe-bfef-e084a642b0ff
updated: 1484309485
title: Fast-growing hierarchy
tags:
    - Definition
    - The Wainer hierarchy
    - Points of interest
    - Functions in fast-growing hierarchies
categories:
    - Computability theory
---
In computability theory, computational complexity theory and proof theory, a fast-growing hierarchy (also called an extended Grzegorczyk hierarchy) is an ordinal-indexed family of rapidly increasing functions fα: N → N (where N is the set of natural numbers {0, 1, ...}, and α ranges up to some large countable ordinal). A primary example is the Wainer hierarchy, or Löb–Wainer hierarchy, which is an extension to all α < ε0. Such hierarchies provide a natural way to classify computable functions according to rate-of-growth and computational complexity.
