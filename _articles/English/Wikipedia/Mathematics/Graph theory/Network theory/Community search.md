---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Community_search
offline_file: ""
offline_thumbnail: ""
uuid: e2277ff4-fbaa-4248-ae2a-34b5e4dfed15
updated: 1484309432
title: Community search
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/P_globe_blue.png
categories:
    - Network theory
---
Discovering communities in a network, known as community detection/discovery, is a fundamental problem in network science, which attracted much attention in the past several decades. In recent years, with the tremendous studies on big data, there is another related but different problem, called community search, which aims to find the most likely community that contains the query node, has attracted great attention from both academic and industry areas. It is a query-dependent variant of the community detection problem. In recent years, there has been many interestings studies focusing on this novel research problem.[1][2][3][4][5][6][7][8][9]
