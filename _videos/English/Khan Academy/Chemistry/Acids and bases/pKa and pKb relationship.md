---
version: 1
type: video
provider: YouTube
id: 3Gm4nAAc3zc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Acids%20and%20bases/pKa%20and%20pKb%20relationship.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6d8b11d9-c94f-4210-a1da-63d612329c20
updated: 1486069587
title: pKa and pKb relationship
categories:
    - Acids and bases
thumbnail_urls:
    - https://i3.ytimg.com/vi/3Gm4nAAc3zc/default.jpg
    - https://i3.ytimg.com/vi/3Gm4nAAc3zc/1.jpg
    - https://i3.ytimg.com/vi/3Gm4nAAc3zc/2.jpg
    - https://i3.ytimg.com/vi/3Gm4nAAc3zc/3.jpg
---
The Ka and Kb relationship and pKa and pKb relationship between conjugate acids and bases. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/acids-and-bases-topic/copy-of-acid-base-equilibria/v/relationship-between-ka-and-kb?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/acids-and-bases-topic/copy-of-acid-base-equilibria/v/conjugate-acids-and-bases?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
