---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manufacturer
offline_file: ""
offline_thumbnail: ""
uuid: 2f1a6175-fa8b-41b4-8b35-efe017cd37a6
updated: 1484310525
title: manufacturer
categories:
    - Dictionary
---
manufacturer
     n 1: a business engaged in manufacturing some product [syn: {maker},
           {manufacturing business}]
     2: someone who manufactures something [syn: {producer}]
