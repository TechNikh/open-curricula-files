---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seasonal
offline_file: ""
offline_thumbnail: ""
uuid: cb641988-390f-42ee-ae76-84f9d0107ff3
updated: 1484310435
title: seasonal
categories:
    - Dictionary
---
seasonal
     adj : occurring at or dependent on a particular season; "seasonal
           labor"; "a seasonal rise in unemployment" [ant: {year-round}]
