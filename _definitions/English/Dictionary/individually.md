---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/individually
offline_file: ""
offline_thumbnail: ""
uuid: b88d3136-cb9d-4bb0-8385-2f346f089829
updated: 1484310234
title: individually
categories:
    - Dictionary
---
individually
     adv : apart from others; "taken individually, the rooms were, in
           fact, square"; "the fine points are treated singly"
           [syn: {separately}, {singly}, {severally}, {one by one},
            {on an individual basis}]
