---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inordinate
offline_file: ""
offline_thumbnail: ""
uuid: 5278f7a4-a7d0-4030-a82b-d2e2ed8586f5
updated: 1484310174
title: inordinate
categories:
    - Dictionary
---
inordinate
     adj : beyond normal limits; "excessive charges"; "a book of
           inordinate length"; "his dress stops just short of
           undue elegance"; "unreasonable demands" [syn: {excessive},
            {undue}, {unreasonable}]
