---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/negotiate
offline_file: ""
offline_thumbnail: ""
uuid: 860a1fd7-52ea-488e-98b2-549abc9203c9
updated: 1484310529
title: negotiate
categories:
    - Dictionary
---
negotiate
     v : discuss the terms of an arrangement; "They negotiated the
         sale of the house" [syn: {negociate}, {talk terms}]
