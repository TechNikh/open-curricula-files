---
version: 1
type: video
provider: YouTube
id: sw2p2tUIFpc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Integrals-%20Trig%20Substitution%203%20%28long%20problem%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f5de806b-d845-4368-b558-18f00abb2ac2
updated: 1486069696
title: 'Integrals- Trig Substitution 3 (long problem)'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/sw2p2tUIFpc/default.jpg
    - https://i3.ytimg.com/vi/sw2p2tUIFpc/1.jpg
    - https://i3.ytimg.com/vi/sw2p2tUIFpc/2.jpg
    - https://i3.ytimg.com/vi/sw2p2tUIFpc/3.jpg
---
