---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beer
offline_file: ""
offline_thumbnail: ""
uuid: a34860e5-4a56-445e-893d-f64042e17d7c
updated: 1484310384
title: beer
categories:
    - Dictionary
---
beer
     n : a general name for alcoholic beverages made by fermenting a
         cereal (or mixture of cereals) flavored with hops
