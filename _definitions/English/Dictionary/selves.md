---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/selves
offline_file: ""
offline_thumbnail: ""
uuid: 023a4da9-9ced-4569-814f-76141512ecc5
updated: 1484310595
title: selves
categories:
    - Dictionary
---
self
     adj 1: combining form; oneself or itself; "self-control"
     2: used as a combining form; relating to--of or by or to or
        from or for--the self; "self-knowledge";
        "self-proclaimed"; "self-induced"
     n 1: your consciousness of your own identity [syn: {ego}]
     2: a person considered as a unique individual; "one's own self"
     [also: {selves} (pl)]
