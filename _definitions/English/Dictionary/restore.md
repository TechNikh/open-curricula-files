---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/restore
offline_file: ""
offline_thumbnail: ""
uuid: acc6dfd6-7782-4310-903b-f8ca2f069476
updated: 1484310256
title: restore
categories:
    - Dictionary
---
restore
     v 1: return to its original or usable and functioning condition;
          "restore the forest to its original pristine condition"
          [syn: {reconstruct}]
     2: return to life; get or give new life or energy; "The week at
        the spa restored me" [syn: {regenerate}, {rejuvenate}]
     3: give or bring back; "Restore the stolen painting to its
        rightful owner" [syn: {restitute}]
     4: restore by replacing a part or putting together what is torn
        or broken; "She repaired her TV set"; "Repair my shoes
        please" [syn: {repair}, {mend}, {fix}, {bushel}, {doctor},
         {furbish up}, {touch on}] [ant: {break}]
     5: bring back into original ...
