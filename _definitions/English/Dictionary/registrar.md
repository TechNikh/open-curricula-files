---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/registrar
offline_file: ""
offline_thumbnail: ""
uuid: 14cfb186-f6e7-4c9a-b503-7222c1f45cdf
updated: 1484310469
title: registrar
categories:
    - Dictionary
---
registrar
     n 1: a person employed to keep a record of the owners of stocks
          and bonds issued by the company
     2: the administrator responsible for student records
     3: someone responsible for keeping records [syn: {record-keeper},
         {recorder}]
