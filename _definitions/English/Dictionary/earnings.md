---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/earnings
offline_file: ""
offline_thumbnail: ""
uuid: 14962593-1055-4915-a68a-3a4262f6c05a
updated: 1484310455
title: earnings
categories:
    - Dictionary
---
earnings
     n 1: the excess of revenues over outlays in a given period of
          time (including depreciation and other non-cash
          expenses) [syn: {net income}, {net}, {net profit}, {lucre},
           {profit}, {profits}]
     2: something that remunerates; "wages were paid by check"; "he
        wasted his pay on drink"; "they saved a quarter of all
        their earnings" [syn: {wage}, {pay}, {remuneration}, {salary}]
