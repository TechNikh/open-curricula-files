---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nasser
offline_file: ""
offline_thumbnail: ""
uuid: 72788155-40fb-424d-b533-6c911ac1dabb
updated: 1484310178
title: nasser
categories:
    - Dictionary
---
Nasser
     n 1: Egyptian statesman who nationalized the Suez Canal
          (1918-1870) [syn: {Gamal Abdel Nasser}]
     2: lake in Egypt formed by dams built on the Nile River at
        Aswan [syn: {Lake Nasser}]
