---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concurrent
offline_file: ""
offline_thumbnail: ""
uuid: 4a3a0cf4-e1ea-442b-8d70-772512b0122a
updated: 1484310599
title: concurrent
categories:
    - Dictionary
---
concurrent
     adj : occurring or operating at the same time; "a series of
           coincident events" [syn: {coincident}, {coincidental},
           {coinciding}, {cooccurring}, {simultaneous}]
