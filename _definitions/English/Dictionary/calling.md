---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calling
offline_file: ""
offline_thumbnail: ""
uuid: 1c03f60d-5b4d-4923-aca5-4a9baa554e30
updated: 1484310194
title: calling
categories:
    - Dictionary
---
calling
     n : the particular occupation for which you are trained [syn: {career},
          {vocation}]
