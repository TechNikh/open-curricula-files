---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yurta
offline_file: ""
offline_thumbnail: ""
uuid: 8b80f249-8046-4e27-b9d3-31645b2c1794
updated: 1484308471
title: Yurt
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Syr_Darya_Oblast._Kyrgyz_Yurt_WDL10968.png
tags:
    - Etymology and synonyms
    - History
    - Construction
    - Decoration and symbolism
    - In Central Asia
    - Buddhist symbolism in the Mongolian Gers
    - Western yurts
categories:
    - Uncategorized
---
A traditional yurt (from the Turkic languages) or ger (Mongolian) is a portable, round tent covered with skins or felt and used as a dwelling by nomads in the steppes of Central Asia. The structure comprises an angled assembly or latticework of pieces of wood or bamboo for walls, a door frame, ribs (poles, rafters), and a wheel (crown, compression ring) possibly steam-bent. The roof structure is often self-supporting, but large yurts may have interior posts supporting the crown. The top of the wall of self-supporting yurts is prevented from spreading by means of a tension band which opposes the force of the roof ribs. Modern yurts may be permanently built on a wooden platform; they may use ...
