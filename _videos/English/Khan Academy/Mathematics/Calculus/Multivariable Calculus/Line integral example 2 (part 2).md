---
version: 1
type: video
provider: YouTube
id: Qqanbd3gLhw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Line%20integral%20example%202%20%28part%202%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1567573c-6a66-49fd-a9dc-81aa265165ac
updated: 1486069696
title: Line integral example 2 (part 2)
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/Qqanbd3gLhw/default.jpg
    - https://i3.ytimg.com/vi/Qqanbd3gLhw/1.jpg
    - https://i3.ytimg.com/vi/Qqanbd3gLhw/2.jpg
    - https://i3.ytimg.com/vi/Qqanbd3gLhw/3.jpg
---
