---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grand_potential
offline_file: ""
offline_thumbnail: ""
uuid: f915fff3-59c5-49fd-8ef6-73870313a90e
updated: 1484308843
title: Grand potential
tags:
    - Definition
    - Landau free energy
    - >
        Grand potential for homogeneous systems (vs. inhomogeneous
        systems)
    - Ideal gas
categories:
    - Thermodynamics
---
The grand potential is a quantity used in statistical mechanics, especially for irreversible processes in open systems. The grand potential is the characteristic state function for the grand canonical ensemble.
