---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coloured
offline_file: ""
offline_thumbnail: ""
uuid: 2cdfe4b7-cd2f-4c25-9012-22e11c00820f
updated: 1484310303
title: coloured
categories:
    - Dictionary
---
coloured
     adj 1: having color or a certain color; sometimes used in
            combination; "colored crepe paper"; "the film was in
            color"; "amber-colored heads of grain" [syn: {colored},
             {colorful}] [ant: {uncolored}]
     2: favoring one person or side over another; "a biased account
        of the trial"; "a decision that was partial to the
        defendant" [syn: {biased}, {colored}, {one-sided}, {slanted}]
     3: (used of color) artificially produced; not natural; "a
        bleached blonde" [syn: {bleached}, {colored}, {dyed}]
     4: having skin rich in melanin pigments; "National Association
        for the Advancement of Colored People"; "the dark ...
