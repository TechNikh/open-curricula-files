---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tray
offline_file: ""
offline_thumbnail: ""
uuid: 9365d34a-9fcd-446d-ad97-900ff48ee7e4
updated: 1484310208
title: tray
categories:
    - Dictionary
---
tray
     n : an open receptacle for holding or displaying or serving
         articles or food
