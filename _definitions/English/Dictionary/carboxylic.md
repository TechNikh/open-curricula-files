---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carboxylic
offline_file: ""
offline_thumbnail: ""
uuid: 08e87660-7621-4514-9ef9-9be778f13482
updated: 1484310419
title: carboxylic
categories:
    - Dictionary
---
carboxylic
     adj : relating to or containing the carboxyl group or carboxyl
           radical [syn: {carboxyl}]
