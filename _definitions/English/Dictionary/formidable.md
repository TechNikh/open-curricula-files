---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formidable
offline_file: ""
offline_thumbnail: ""
uuid: ebed95a8-d9d7-42e5-a41d-19a1a264e07c
updated: 1484310595
title: formidable
categories:
    - Dictionary
---
formidable
     adj 1: extremely impressive in strength or excellence; "a
            formidable opponent"; "the challenge was formidable";
            "had a formidable array of compositions to his
            credit"; "the formidable army of brains at the Prime
            Minister's disposal"
     2: inspiring fear; "the formidable prospect of major surgery";
        "a tougher and more redoubtable adversary than the
        heel-clicking, jackbooted fanatic"- G.H.Johnston;
        "something unnerving and prisonlike about high gray wall"
        [syn: {redoubtable}, {unnerving}]
