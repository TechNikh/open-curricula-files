---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aviation
offline_file: ""
offline_thumbnail: ""
uuid: ac656567-7eda-4dc1-986e-78b8e616d0aa
updated: 1484310424
title: aviation
categories:
    - Dictionary
---
aviation
     n 1: the aggregation of a country's military aircraft [syn: {air
          power}]
     2: the operation of aircraft to provide transportation
     3: the art of operating aircraft [syn: {airmanship}]
     4: travel via aircraft; "air travel involves too much waiting
        in airports"; "if you've time to spare go by air" [syn: {air
        travel}, {air}]
