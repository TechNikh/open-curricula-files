---
version: 1
type: article
id: https://en.wikipedia.org/wiki/LCP_theory
offline_file: ""
offline_thumbnail: ""
uuid: 60bae299-5b52-419d-b7f2-18262282d4d3
updated: 1484309277
title: LCP theory
tags:
    - Ligand radius
    - Treatment of lone pairs
    - LCP and VSEPR
categories:
    - Molecular geometry
---
In chemistry, ligand close packing theory (LCP theory), sometimes called the ligand close packing model describes how ligand – ligand repulsions affect the geometry around a central atom.[1] It has been developed by R. J. Gillespie and others from 1997 onwards [2] and is said to sit alongside VSEPR[1] which was originally developed by R. J. Gillespie and R Nyholm.[3] The inter-ligand distances in a wide range of molecules have been determined. The example below shows a series of related molecules:[4]
