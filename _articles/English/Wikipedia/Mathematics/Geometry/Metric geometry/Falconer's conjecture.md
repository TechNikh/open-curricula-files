---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Falconer%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 74d05476-b488-4269-9cc9-0f0ec6c07559
updated: 1484309268
title: "Falconer's conjecture"
categories:
    - Metric geometry
---
In geometric measure theory, Falconer's conjecture, named after Kenneth Falconer, is an unsolved problem concerning the sets of Euclidean distances between points in compact d-dimensional spaces. Intuitively, it states that a set of points that is large in its Hausdorff dimension must determine a set of distances that is large in measure. More precisely, if S is a compact set of points in d-dimensional Euclidean space whose Hausdorff dimension is strictly greater than d/2, then the conjecture states that the set of distances between pairs of points in S must have nonzero Lebesgue measure.
