---
version: 1
type: video
provider: YouTube
id: 8XffLj2zvf4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra-%20Slope%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5f854e08-0ae3-4661-8483-deca43dae057
updated: 1486069573
title: 'Algebra- Slope 3'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/8XffLj2zvf4/default.jpg
    - https://i3.ytimg.com/vi/8XffLj2zvf4/1.jpg
    - https://i3.ytimg.com/vi/8XffLj2zvf4/2.jpg
    - https://i3.ytimg.com/vi/8XffLj2zvf4/3.jpg
---
Part 3 of slope
