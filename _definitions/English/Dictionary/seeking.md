---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seeking
offline_file: ""
offline_thumbnail: ""
uuid: 03e23dbb-8d8e-4c8f-a908-817d789237ef
updated: 1484310443
title: seeking
categories:
    - Dictionary
---
seeking
     adj : trying to obtain; "profit-seeking producers"; "people
           seeking happiness"
     n 1: the act of searching for something; "a quest for diamonds"
          [syn: {quest}]
     2: an attempt to acquire or gain something
