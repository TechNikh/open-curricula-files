---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liberal
offline_file: ""
offline_thumbnail: ""
uuid: 63b19df1-8e6f-4f69-8fd9-5aebad72e1d6
updated: 1484310527
title: liberal
categories:
    - Dictionary
---
liberal
     adj 1: showing or characterized by broad-mindedness; "a broad
            political stance"; "generous and broad sympathies"; "a
            liberal newspaper"; "tolerant of his opponent's
            opinions" [syn: {broad}, {large-minded}, {tolerant}]
     2: having political or social views favoring reform and
        progress
     3: tolerant of change; not bound by authoritarianism,
        orthodoxy, or tradition [ant: {conservative}]
     4: given or giving freely; "was a big tipper"; "the bounteous
        goodness of God"; "bountiful compliments"; "a freehanded
        host"; "a handsome allowance"; "Saturday's child is loving
        and giving"; "a liberal backer of ...
