---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/circuit
offline_file: ""
offline_thumbnail: ""
uuid: 6fb02020-61ec-4e1d-9afe-4c603b0badc5
updated: 1484310206
title: circuit
categories:
    - Dictionary
---
circuit
     n 1: an electrical device that provides a path for electrical
          current to flow [syn: {electrical circuit}, {electric
          circuit}]
     2: a journey or route all the way around a particular place or
        area; "they took an extended tour of Europe"; "we took a
        quick circuit of the park"; "a ten-day coach circuit of
        the island" [syn: {tour}]
     3: an established itinerary of venues or events that a
        particular group of people travel to; "she's a familiar
        name on the club circuit"; "on the lecture circuit"; "the
        judge makes a circuit of the courts in his district"; "the
        international tennis circuit"
     4: (law) ...
