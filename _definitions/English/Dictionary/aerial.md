---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aerial
offline_file: ""
offline_thumbnail: ""
uuid: 578487b0-5734-44c2-9221-d358de379025
updated: 1484310597
title: aerial
categories:
    - Dictionary
---
aerial
     adj 1: in or belonging to the air or operating (for or by means of
            aircraft or elevated cables) in the air; "aerial
            particles"; "small aerial creatures such as
            butterflies"; "aerial warfare"; "aerial photography";
            "aerial cable cars"
     2: growing in air; "aerial roots of a philodendron"
     3: characterized by lightness and insubstantiality; as
        impalpable or intangible as air; "figures light and
        aeriform come unlooked for and melt away"- Thomas Carlyle;
        "aerial fancies"; "an airy apparition"; "physical rather
        than ethereal forms" [syn: {aeriform}, {airy}, {aery}, {ethereal}]
     n 1: a pass to a ...
