---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anhydride
offline_file: ""
offline_thumbnail: ""
uuid: 8ed9c071-dec8-46bf-8bdc-b93ca9ee9db5
updated: 1484310421
title: anhydride
categories:
    - Dictionary
---
anhydride
     n : a compound formed from one or more other compounds in a
         reaction resulting in removal of water
