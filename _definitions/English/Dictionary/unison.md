---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unison
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484653921
title: unison
categories:
    - Dictionary
---
unison
     n 1: corresponding exactly; "marching in unison"
     2: occurring together or simultaneously; "the two spoke in
        unison"
     3: (music) two or more sounds or tones at the same pitch or in
        octaves; "singing in unison"
