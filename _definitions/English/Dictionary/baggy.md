---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/baggy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484502302
title: baggy
categories:
    - Dictionary
---
baggy
     adj : not fitting closely; hanging loosely; "baggy trousers"; "a
           loose-fitting blouse is comfortable in hot weather"
           [syn: {loose-fitting}]
     [also: {baggiest}, {baggier}]
