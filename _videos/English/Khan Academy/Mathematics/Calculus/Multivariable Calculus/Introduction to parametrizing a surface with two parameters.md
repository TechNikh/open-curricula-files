---
version: 1
type: video
provider: YouTube
id: owKAHXf1y1A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Introduction%20to%20parametrizing%20a%20surface%20with%20two%20parameters.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 43834b39-617f-4020-8f9e-80575d96816f
updated: 1486069690
title: Introduction to parametrizing a surface with two parameters
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/owKAHXf1y1A/default.jpg
    - https://i3.ytimg.com/vi/owKAHXf1y1A/1.jpg
    - https://i3.ytimg.com/vi/owKAHXf1y1A/2.jpg
    - https://i3.ytimg.com/vi/owKAHXf1y1A/3.jpg
---
