---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/verify
offline_file: ""
offline_thumbnail: ""
uuid: 6efbb419-bb49-4738-8dc1-9d4cf8af982e
updated: 1484310220
title: verify
categories:
    - Dictionary
---
verify
     v 1: confirm the truth of; "Please verify that the doors are
          closed"; "verify a claim"
     2: verify or regulate by conducting a parallel experiment or
        comparing with another standard, of scientific
        experiments; "Are you controlling for the temperature?"
        [syn: {control}]
     3: attach or append a legal verification to (a pleading or
        petition)
     4: to declare or affirm solemnly and formally as true; "Before
        God I swear I am innocent" [syn: {affirm}, {assert}, {avow},
         {aver}, {swan}, {swear}]
     [also: {verified}]
