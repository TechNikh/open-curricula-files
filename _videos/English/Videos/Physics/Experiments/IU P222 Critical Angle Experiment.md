---
version: 1
type: video
provider: YouTube
id: D74SDyaHMxA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bed418c4-6fea-405c-b168-d7cf4588a78e
updated: 1486069640
title: IU P222 Critical Angle Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/D74SDyaHMxA/default.jpg
    - https://i3.ytimg.com/vi/D74SDyaHMxA/1.jpg
    - https://i3.ytimg.com/vi/D74SDyaHMxA/2.jpg
    - https://i3.ytimg.com/vi/D74SDyaHMxA/3.jpg
---
