---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Megacities
offline_file: ""
offline_thumbnail: ""
uuid: 1820af3b-be4b-454c-9e6f-fb43618eed67
updated: 1484308451
title: Megacity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-TrafficBangkok.JPG
tags:
    - Largest cities
    - History
    - growth
    - Challenges
    - Slums
    - Crime
    - Homelessness
    - Traffic congestion
    - Urban sprawl
    - Gentrification
    - Air pollution
    - Energy and material resources
    - In fiction
categories:
    - Uncategorized
---
A megacity is usually defined as a metropolitan area with a total population in excess of ten million people.[1] A megacity can be a single metropolitan area or two or more metropolitan areas that converge. The terms conurbation, metropolis and metroplex are also applied to the latter.
