---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Liouville%E2%80%93Arnold_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: f782ff0e-fc16-4773-bbcb-58b153ff7507
updated: 1484308702
title: Liouville–Arnold theorem
categories:
    - Theorems in dynamical systems
---
In dynamical systems theory, the Liouville–Arnold theorem states that if, in a Hamiltonian dynamical system with n degrees of freedom, there are also known n first integrals of motion that are independent and in involution, then there exists a canonical transformation to action-angle coordinates in which the transformed Hamiltonian is dependent only upon the action coordinates and the angle coordinates evolve linearly in time. Thus the equations of motion for the system can be solved in quadratures if the canonical transform is explicitly known. The theorem is named after Joseph Liouville and Vladimir Arnold.[1][2][3][4](pp270–272)
