---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unclear
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484440201
title: unclear
categories:
    - Dictionary
---
unclear
     adj 1: poorly stated or described; "he confuses the reader with
            ill-defined terms and concepts" [syn: {ill-defined}]
            [ant: {well-defined}]
     2: not clear to the mind; "the law itself was unclear on that
        point"; "the reason for their actions is unclear to this
        day" [ant: {clear}]
     3: not easily deciphered; "indecipherable handwriting" [syn: {indecipherable},
         {undecipherable}, {unreadable}]
