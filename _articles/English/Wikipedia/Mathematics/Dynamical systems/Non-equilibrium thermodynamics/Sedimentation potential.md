---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sedimentation_potential
offline_file: ""
offline_thumbnail: ""
uuid: 7946750c-c5ff-42e8-bd09-dd78e5044fce
updated: 1484308801
title: Sedimentation potential
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sedimentation_Potential_1n.jpg
tags:
    - Surface Energy
    - Background Related to Phenomenon
    - History of Sedimentation Potential Models
    - Generation of a Potential
    - Testing
    - Measuring Sedementation Potential
    - Real World Applications
    - >
        Applications of sedimentation field flow fractionation
        (SFFF)
    - >
        Particle size analysis by sedimentation field flow
        fractionation
categories:
    - Non-equilibrium thermodynamics
---
Sedimentation Potential occurs when dispersed particles move under the influence of either gravity or centrifugation in a medium. This motion disrupts the equilibrium symmetry of the particle's double layer. While the particle moves, the ions in the electric double layer lag behind due to the liquid flow. This causes a slight displacement between the surface charge and the electric charge of the diffuse layer. As a result, the moving particle creates a dipole moment. The sum of all of the dipoles generates an electric field which is called sedimentation potential. It can be measured with an open electrical circuit, which is also called sedimentation current.
