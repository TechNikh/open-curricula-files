---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concentrate
offline_file: ""
offline_thumbnail: ""
uuid: 2ea8d3dc-d8cb-4ca8-99fb-9de0f6a243a6
updated: 1484310429
title: concentrate
categories:
    - Dictionary
---
concentrate
     n 1: the desired mineral that is left after impurities have been
          removed from mined ore [syn: {dressed ore}]
     2: a concentrated form of a foodstuff; the bulk is reduced by
        removing water
     3: a concentrated example; "the concentrate of contemporary
        despair"
     v 1: make (the solvent of a solution) dense or denser
     2: direct one's attention on something; "Please focus on your
        studies and not on your hobbies" [syn: {focus}, {center},
        {centre}, {pore}, {rivet}]
     3: make central; "The Russian government centralized the
        distribution of food" [syn: {centralize}, {centralise}]
        [ant: {decentralize}, ...
