---
version: 1
type: video
provider: YouTube
id: iicN3Jak5vc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 351f9915-cd6b-41b8-8ecc-5688d7a69a43
updated: 1486069618
title: Complex conjugates
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/iicN3Jak5vc/default.jpg
    - https://i3.ytimg.com/vi/iicN3Jak5vc/1.jpg
    - https://i3.ytimg.com/vi/iicN3Jak5vc/2.jpg
    - https://i3.ytimg.com/vi/iicN3Jak5vc/3.jpg
---
