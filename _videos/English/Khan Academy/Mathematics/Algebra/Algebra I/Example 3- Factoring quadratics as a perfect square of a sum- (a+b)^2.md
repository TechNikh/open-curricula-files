---
version: 1
type: video
provider: YouTube
id: XuwldEyWjH0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Example%203-%20Factoring%20quadratics%20as%20a%20perfect%20square%20of%20a%20sum-%20%28a%2Bb%29%5E2.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0df5b38b-3af0-47b6-a8fd-130031bc3216
updated: 1486069559
title: 'Example 3- Factoring quadratics as a perfect square of a sum- (a+b)^2'
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/XuwldEyWjH0/default.jpg
    - https://i3.ytimg.com/vi/XuwldEyWjH0/1.jpg
    - https://i3.ytimg.com/vi/XuwldEyWjH0/2.jpg
    - https://i3.ytimg.com/vi/XuwldEyWjH0/3.jpg
---
