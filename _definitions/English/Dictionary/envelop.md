---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/envelop
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484628301
title: envelop
categories:
    - Dictionary
---
envelop
     v : enclose or enfold completely with or as if with a covering;
         "Fog enveloped the house" [syn: {enfold}, {enwrap}, {wrap},
          {enclose}]
