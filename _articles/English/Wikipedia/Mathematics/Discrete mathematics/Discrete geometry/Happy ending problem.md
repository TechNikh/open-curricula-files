---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Happy_ending_problem
offline_file: ""
offline_thumbnail: ""
uuid: 94370285-36f4-41ac-850e-694f389c7221
updated: 1484308616
title: Happy ending problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Happy-End-problem.svg.png
tags:
    - Larger polygons
    - Empty convex polygons
    - Related problems
    - Notes
categories:
    - Discrete geometry
---
This was one of the original results that led to the development of Ramsey theory.
