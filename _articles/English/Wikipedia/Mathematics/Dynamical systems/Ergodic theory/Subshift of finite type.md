---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Subshift_of_finite_type
offline_file: ""
offline_thumbnail: ""
uuid: 45bfd767-66c9-4fc2-a17b-9b01755b6f8d
updated: 1484308782
title: Subshift of finite type
tags:
    - Definition
    - Terminology
    - Generalizations
    - Topology
    - Metric
    - Measure
    - Zeta function
    - Notes
categories:
    - Ergodic theory
---
In mathematics, subshifts of finite type are used to model dynamical systems, and in particular are the objects of study in symbolic dynamics and ergodic theory. They also describe the set of all possible sequences executed by a finite state machine. The most widely studied shift spaces are the subshifts of finite type.
