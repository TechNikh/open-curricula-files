---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tumbler
offline_file: ""
offline_thumbnail: ""
uuid: e3a9d793-907e-4cf3-9b2d-18ff69d9bb8c
updated: 1484310226
title: tumbler
categories:
    - Dictionary
---
tumbler
     n 1: a gymnast who performs rolls and somersaults and twists etc.
     2: a glass with a flat bottom but no handle or stem; originally
        had a round bottom
     3: a movable obstruction in a lock that must be adjusted to a
        given position (as by a key) before the bolt can be thrown
     4: pigeon that executes backward somersaults in flight or on
        the ground [syn: {roller}, {tumbler pigeon}]
