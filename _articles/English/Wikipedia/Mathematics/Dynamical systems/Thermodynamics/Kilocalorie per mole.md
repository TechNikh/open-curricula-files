---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kilocalorie_per_mole
offline_file: ""
offline_thumbnail: ""
uuid: 8514fe61-420b-4c07-bb79-3d0b5fcced5a
updated: 1484308846
title: Kilocalorie per mole
categories:
    - Thermodynamics
---
The kilocalorie per mole is a unit to measure an amount of energy per number of molecules, atoms, or other similar particles. It is defined as one kilocalorie of energy (1000 thermochemical gram calories) per one mole of substance, that is, per Avogadro’s number of particles. It is abbreviated "kcal/mol" or "kcal mol−1". As typically measured, one kcal/mol represents a temperature increase of one degree Celsius in one liter of water (with a mass of 1kg) resulting from the reaction of one mole of reagents.
