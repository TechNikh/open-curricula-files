---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laminar-turbulent_transition
offline_file: ""
offline_thumbnail: ""
uuid: 00c06b5f-9c92-4d74-a585-f4772c99f1d5
updated: 1484308709
title: Laminar-turbulent transition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Laminar-turbulent_transition.jpg
tags:
    - History
    - Transition stages in a boundary layer
    - Receptivity
    - Primary mode growth
    - Secondary instabilities
categories:
    - Chaos theory
---
The process of a laminar flow becoming turbulent is known as laminar-turbulent transition. This is an extraordinarily complicated process which at present is not fully understood. However, as the result of many decades of intensive research, certain features have become gradually clear, and it is known that the process proceeds through a series of stages. "Transitional flow" can refer to transition in either direction, that is laminar-turbulent transitional or turbulent-laminar transitional flow.
