---
version: 1
type: article
id: https://en.wikipedia.org/wiki/State_postulate
offline_file: ""
offline_thumbnail: ""
uuid: 2a9501af-d082-49b3-a03b-2b0a5ac5e47b
updated: 1484308866
title: State postulate
categories:
    - Thermodynamics
---
The state postulate is a term used in thermodynamics that defines the given number of properties to a thermodynamic system in a state of equilibrium. The state postulate allows a finite number of properties to be specified in order to fully describe a state of thermodynamic equilibrium. Once the state postulate is given the other unspecified properties must assume certain values.
