---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uranium
offline_file: ""
offline_thumbnail: ""
uuid: e8a2a299-3e68-4d85-8061-9edb66d0af55
updated: 1484310283
title: uranium
categories:
    - Dictionary
---
uranium
     n : a heavy toxic silvery-white radioactive metallic element;
         occurs in many isotopes; used for nuclear fuels and
         nuclear weapons [syn: {U}, {atomic number 92}]
