---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crown
offline_file: ""
offline_thumbnail: ""
uuid: d4614d4a-8e5a-40f2-9d8a-0ad38fa3cceb
updated: 1484310212
title: crown
categories:
    - Dictionary
---
Crown
     n 1: the Crown (or the reigning monarch) as the symbol of the
          power and authority of a monarchy; "the colonies
          revolted against the Crown"
     2: the enamel covered part of a tooth above the gum
     3: a wreath or garland worn on the head to signify victory
     4: an ornamental jewelled headdress signifying sovereignty
        [syn: {diadem}]
     5: the part of a hat (the vertex) covering the crown of the
        head
     6: an English coin worth 5 shillings
     7: the upper branches and leaves of a tree [syn: {capitulum}, {treetop}]
     8: the top point of a mountain or hill; "the view from the peak
        was magnificent"; "they clambered to the ...
