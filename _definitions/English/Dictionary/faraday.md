---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faraday
offline_file: ""
offline_thumbnail: ""
uuid: 86180f07-7c53-49d0-8f32-71bcf9697f32
updated: 1484310366
title: faraday
categories:
    - Dictionary
---
Faraday
     n : the English physicist nd chemist who discovered
         electromagnetic induction (1791-1867) [syn: {Michael
         Faraday}]
