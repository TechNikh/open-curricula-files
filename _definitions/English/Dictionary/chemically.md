---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chemically
offline_file: ""
offline_thumbnail: ""
uuid: 37b44bb5-f0bf-4a52-9211-ceb0ad000b78
updated: 1484310375
title: chemically
categories:
    - Dictionary
---
chemically
     adv 1: by the use of chemicals; "chemically fertilized" [syn: {with
            chemicals}]
     2: with respect to chemistry; "chemically different substances"
