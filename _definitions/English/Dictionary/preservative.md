---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preservative
offline_file: ""
offline_thumbnail: ""
uuid: be6e4c44-d497-4d2d-80f6-275405212d00
updated: 1484310424
title: preservative
categories:
    - Dictionary
---
preservative
     adj : tending or having the power to preserve; "the timbers should
           be treated with a preservative"
     n : a chemical compound that is added to protect against decay
         or decomposition
