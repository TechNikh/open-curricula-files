---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fourth_power
offline_file: ""
offline_thumbnail: ""
uuid: e16c8a7b-c883-4f50-b90e-ff88b131536f
updated: 1484308644
title: Fourth power
categories:
    - Number theory
---
Fourth powers are also formed by multiplying a number by its cube. Furthermore, they are squares of squares.
