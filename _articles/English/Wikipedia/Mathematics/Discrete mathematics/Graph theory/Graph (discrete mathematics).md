---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 75ed0789-f6b3-4514-aaa1-e8fb8513f4aa
updated: 1484308619
title: Graph (discrete mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-6n-graf.svg_1.png
tags:
    - Definitions
    - Graph
    - Adjacency relation
    - Types of graphs
    - Distinction in terms of the main definition
    - Undirected graph
    - Directed graph
    - Oriented graph
    - Mixed graph
    - Multigraph
    - Simple graph
    - Quiver
    - Weighted graph
    - Half-edges, loose edges
    - Important classes of graph
    - Regular graph
    - Complete graph
    - Finite graph
    - Connected graph
    - Bipartite graph
    - Path graph
    - Planar graph
    - Cycle graph
    - Tree
    - Advanced classes
    - Properties of graphs
    - Examples
    - Graph operations
    - Generalizations
    - Notes
categories:
    - Graph theory
---
In mathematics, and more specifically in graph theory, a graph is a structure amounting to a set of objects in which some pairs of the objects are in some sense "related." The objects correspond to mathematical abstractions called vertices (also called nodes or points) and each of the related pairs of vertices is called an edge (also called an arc or line).[1] Typically, a graph is depicted in diagrammatic form as a set of dots for the vertices, joined by lines or curves for the edges. Graphs are one of the objects of study in discrete mathematics.
