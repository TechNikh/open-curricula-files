---
version: 1
type: video
provider: YouTube
id: N1ALx5q6jO4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1d406d45-0a58-41fb-97c2-e282322a0bf2
updated: 1486069623
title: 'Multiplication by Fours & Sixes, Learning the Times Table Stars'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/N1ALx5q6jO4/default.jpg
    - https://i3.ytimg.com/vi/N1ALx5q6jO4/1.jpg
    - https://i3.ytimg.com/vi/N1ALx5q6jO4/2.jpg
    - https://i3.ytimg.com/vi/N1ALx5q6jO4/3.jpg
---
