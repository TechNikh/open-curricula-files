---
title: Diaphragm Anatomy & Structures that Perforate Diaphragm
youtube_id: lPiSWjINNJ8
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - mechanism-of-respiration
---
