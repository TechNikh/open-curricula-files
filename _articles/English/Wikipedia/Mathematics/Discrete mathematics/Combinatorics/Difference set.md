---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Difference_set
offline_file: ""
offline_thumbnail: ""
uuid: 1e112723-d20f-4e78-b93b-4877c2c0c57c
updated: 1484308582
title: Difference set
tags:
    - Basic facts
    - Equivalent and isomorphic difference sets
    - Multipliers
    - Parameters
    - Known difference sets
    - History
    - Application
    - Generalisations
    - Notes
categories:
    - Combinatorics
---
In combinatorics, a 
  
    
      
        (
        v
        ,
        k
        ,
        λ
        )
      
    
    {\displaystyle (v,k,\lambda )}
  
 difference set is a subset 
  
    
      
        D
      
    
    {\displaystyle D}
  
 of size 
  
    
      
        k
      
    
    {\displaystyle k}
  
 of a group 
  
    
      
        G
      
    
    {\displaystyle G}
  
 of order 
  
    
      
        v
      
    
    {\displaystyle v}
  
 such that every nonidentity element of 
  
    
      
        G
      
    
    {\displaystyle G}
  
 can be expressed as a product 
  
    
      
        
          d
          
            1
          
        
        
       ...
