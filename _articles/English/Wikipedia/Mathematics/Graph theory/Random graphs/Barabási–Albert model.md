---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Barab%C3%A1si%E2%80%93Albert_model'
offline_file: ""
offline_thumbnail: ""
uuid: b156a4a5-31e7-4fb8-8a81-2fb764576216
updated: 1484309448
title: Barabási–Albert model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Barabasi_Albert_model.gif
tags:
    - concepts
    - Algorithm
    - Properties
    - Degree distribution
    - Hirsch index distribution
    - Average path length
    - Node degree correlations
    - Clustering coefficient
    - Spectral properties
    - Limiting cases
    - Model A
    - Model B
    - History
categories:
    - Random graphs
---
The Barabási–Albert (BA) model is an algorithm for generating random scale-free networks using a preferential attachment mechanism. Scale-free networks are widely observed in natural and human-made systems, including the Internet, the world wide web, citation networks, and some social networks. The algorithm is named for its inventors Albert-László Barabási and Réka Albert.[1]
