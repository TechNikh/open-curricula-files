---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bicone
offline_file: ""
offline_thumbnail: ""
uuid: 0b2dff69-7aad-45dc-9f48-026a748de3f2
updated: 1484308921
title: Bicone
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Bicone.svg.png
tags:
    - Geometry
    - Related polyhedra
categories:
    - Elementary geometry
---
A bicone or dicone (bi- comes from Latin, di- from Greek) is the three-dimensional surface of revolution of a rhombus around one of its axes of symmetry. Equivalently, a bicone is the surface created by joining two congruent right circular cones base-to-base.
