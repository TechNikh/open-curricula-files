---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Holonomic_basis
offline_file: ""
offline_thumbnail: ""
uuid: be22e7a9-fdb0-4a70-a8fa-8c316c6b5cfe
updated: 1484309223
title: Holonomic basis
categories:
    - Differential geometry
---
In mathematics and mathematical physics, a holonomic basis or coordinate basis for a differentiable manifold is a set of basis vector fields {ek} such that some coordinate system {xk} exists for which
