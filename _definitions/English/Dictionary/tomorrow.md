---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tomorrow
offline_file: ""
offline_thumbnail: ""
uuid: 2b7b8b03-0020-465a-8147-82798459fd6f
updated: 1484310150
title: tomorrow
categories:
    - Dictionary
---
tomorrow
     n 1: the day after today; "what are our tasks for tomorrow?"
     2: the near future; "tomorrow's world"; "everyone hopes for a
        better tomorrow"
     adv : the next day, the day after, following the present day
