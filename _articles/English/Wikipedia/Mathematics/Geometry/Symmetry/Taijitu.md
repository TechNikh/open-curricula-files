---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Taijitu
offline_file: ""
offline_thumbnail: ""
uuid: 911b5efc-947d-4092-8d8b-c9a57d1b185a
updated: 1484309321
title: Taijitu
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/ZhoushiTaijitu.png
tags:
    - Structure
    - History
    - Song era
    - Ming era
    - Modern yin-yang symbol
    - Comparison with prehistoric symbols
categories:
    - Symmetry
---
A taijitu (Traditional Chinese: 太極圖; Simplified Chinese: 太极图; Pinyin: tàijítú; Wade-Giles: t'ai⁴chi²t'u²) is a symbol or diagram (图 tú) in Chinese philosophy representing Taiji (太极 tàijí "great pole" or "supreme ultimate") representing both its monist (wuji) and its dualist (yin and yang) aspects. Such a diagram was first introduced by Song Dynasty philosopher Zhou Dunyi (周敦頤 1017–1073) in his Taijitu shuo 太極圖說.
