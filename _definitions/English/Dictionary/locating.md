---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/locating
offline_file: ""
offline_thumbnail: ""
uuid: 550fc9ac-7f47-469a-8a4d-91e81573ce99
updated: 1484310206
title: locating
categories:
    - Dictionary
---
locating
     n 1: the act of putting something in a certain place or location
          [syn: {placement}, {location}, {position}, {positioning},
           {emplacement}]
     2: a determination of the location of something; "he got a good
        fix on the target" [syn: {localization}, {localisation}, {location},
         {fix}]
