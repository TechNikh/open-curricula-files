---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conservativity_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 42d48c6d-5a9f-43e6-8b62-a0cd146bbc4b
updated: 1484308991
title: Conservativity theorem
categories:
    - Theorems in the foundations of mathematics
---
is a theorem of a first-order theory 
  
    
      
        T
      
    
    {\displaystyle T}
  
. Let 
  
    
      
        
          T
          
            1
          
        
      
    
    {\displaystyle T_{1}}
  
 be a theory obtained from 
  
    
      
        T
      
    
    {\displaystyle T}
  
 by extending its language with new constants
