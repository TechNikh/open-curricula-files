---
version: 1
type: video
provider: YouTube
id: Vz0XeWg37II
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f6a40e07-be15-48cd-87e4-2432f8653fc2
updated: 1486069623
title: "Mental Math Question Asked To The World's Hottest Math Teacher"
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Vz0XeWg37II/default.jpg
    - https://i3.ytimg.com/vi/Vz0XeWg37II/1.jpg
    - https://i3.ytimg.com/vi/Vz0XeWg37II/2.jpg
    - https://i3.ytimg.com/vi/Vz0XeWg37II/3.jpg
---
