---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alternant_code
offline_file: ""
offline_thumbnail: ""
uuid: 291ada60-3125-47cd-975b-b7bb46325708
updated: 1484308549
title: Alternant code
categories:
    - Coding theory
---
An alternant code over GF(q) of length n is defined by a parity check matrix H of alternant form Hi,j = αjiyi, where the αj are distinct elements of the extension GF(qm), the yi are further non-zero parameters again in the extension GF(qm) and the indices range as i from 0 to δ − 1, j from 1 to n.
