---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Directional_derivative
offline_file: ""
offline_thumbnail: ""
uuid: fdc7d881-5acc-45de-a505-a48b048e5d63
updated: 1484309216
title: Directional derivative
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/275px-Directional_derivative_contour_plot.svg.png
tags:
    - Definition
    - Variation using only direction of vector for Euclidean space
    - Notation
    - Properties
    - In differential geometry
    - The Lie derivative
    - The Riemann tensor
    - In group theory
    - Translations
    - Rotations
    - Normal derivative
    - In the continuum mechanics of solids
    - Derivatives of scalar valued functions of vectors
    - Derivatives of vector valued functions of vectors
    - >
        Derivatives of scalar valued functions of second-order
        tensors
    - >
        Derivatives of tensor valued functions of second-order
        tensors
    - Notes
categories:
    - Differential geometry
---
In mathematics, the directional derivative of a multivariate differentiable function along a given vector v at a given point x intuitively represents the instantaneous rate of change of the function, moving through x with a velocity specified by v. It therefore generalizes the notion of a partial derivative, in which the rate of change is taken along one of the curvilinear coordinate curves, all other coordinates being constant.
