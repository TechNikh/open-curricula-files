---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/willingness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484602321
title: willingness
categories:
    - Dictionary
---
willingness
     n : cheerful compliance; "he expressed his willingness to help"
         [ant: {unwillingness}]
