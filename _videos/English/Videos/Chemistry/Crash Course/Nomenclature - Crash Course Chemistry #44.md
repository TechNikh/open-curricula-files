---
version: 1
type: video
provider: YouTube
id: U7wavimfNFE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Nomenclature%20-%20Crash%20Course%20Chemistry%20%2344.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 335054df-08b2-462f-a041-29d145869b50
updated: 1486069680
title: 'Nomenclature - Crash Course Chemistry #44'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/U7wavimfNFE/default.jpg
    - https://i3.ytimg.com/vi/U7wavimfNFE/1.jpg
    - https://i3.ytimg.com/vi/U7wavimfNFE/2.jpg
    - https://i3.ytimg.com/vi/U7wavimfNFE/3.jpg
---
