---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equality_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 798a6391-8fb4-4e56-9e79-fe17c86a5535
updated: 1484308542
title: Equality (mathematics)
tags:
    - Etymology
    - Equality in mathematical logic
    - Logical formulations
    - Some basic logical properties of equality
    - Equalities as predicates
    - Equality in set theory
    - Set equality based on first-order logic with equality
    - Set equality based on first-order logic without equality
    - Equality in algebra and analysis
    - Identities
    - Equations
    - Congruences
    - Approximate equality
    - Relation with equivalence and isomorphism
    - Notes
categories:
    - Elementary arithmetic
---
In mathematics, equality is a relationship between two quantities or, more generally two mathematical expressions, asserting that the quantities have the same value, or that the expressions represent the same mathematical object. The equality between A and B is written A = B, and pronounced A equals B. The symbol "=" is called an "equals sign". Thus there are three kinds of equality, which are formalized in different ways.
