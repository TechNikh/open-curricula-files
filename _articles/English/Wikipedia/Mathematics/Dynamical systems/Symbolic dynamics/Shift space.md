---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shift_space
offline_file: ""
offline_thumbnail: ""
uuid: 46105c7e-0579-4feb-9935-74ab5d87e75f
updated: 1484308837
title: Shift space
tags:
    - Notation
    - Definition
    - Characterization and sofic subshifts
    - Examples
categories:
    - Symbolic dynamics
---
In symbolic dynamics and related branches of mathematics, a shift space or subshift is a set of infinite words that represent the evolution of a discrete system. In fact, shift spaces and symbolic dynamical systems are often considered synonyms.
