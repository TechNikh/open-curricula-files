---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Quaternion-K%C3%A4hler_symmetric_space'
offline_file: ""
offline_thumbnail: ""
uuid: ac56a110-2ba6-4a22-b26d-303848ce5782
updated: 1484309249
title: Quaternion-Kähler symmetric space
categories:
    - Homogeneous spaces
---
In differential geometry, a quaternion-Kähler symmetric space or Wolf space is a quaternion-Kähler manifold which, as a Riemannian manifold, is a Riemannian symmetric space. Any quaternion-Kähler symmetric space with positive Ricci curvature is compact and simply connected, and is a Riemannian product of quaternion-Kähler symmetric spaces associated to compact simple Lie groups.
