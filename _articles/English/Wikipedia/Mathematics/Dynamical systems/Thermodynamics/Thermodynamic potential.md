---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermodynamic_potential
offline_file: ""
offline_thumbnail: ""
uuid: 32867742-fdd0-4b76-8703-e36b0a339951
updated: 1484308872
title: Thermodynamic potential
tags:
    - Description and interpretation
    - Natural variables
    - The fundamental equations
    - The equations of state
    - The Maxwell relations
    - Euler integrals
    - The Gibbs–Duhem relation
    - Chemical reactions
    - Notes
categories:
    - Thermodynamics
---
A thermodynamic potential is a scalar quantity used to represent the thermodynamic state of a system. The concept of thermodynamic potentials was introduced by Pierre Duhem in 1886. Josiah Willard Gibbs in his papers used the term fundamental functions. One main thermodynamic potential that has a physical interpretation is the internal energy U. It is the energy of configuration of a given system of conservative forces (that is why it is a potential) and only has meaning with respect to a defined set of references (or data). Expressions for all other thermodynamic energy potentials are derivable via Legendre transforms from an expression for U. In thermodynamics, certain forces, such as ...
