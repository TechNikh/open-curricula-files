---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Directional_symmetry_(time_series)
offline_file: ""
offline_thumbnail: ""
uuid: 654ff30f-4750-4fe8-9571-e5c0cfa96e0c
updated: 1484309307
title: Directional symmetry (time series)
tags:
    - Definition
    - Interpretation
    - Notes and references
categories:
    - Symmetry
---
In statistical analysis of time series and in signal processing, directional symmetry is a statistical measure of a model's performance in predicting the direction of change, positive or negative, of a time series from one time period to the next.
