---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%C3%89tienne_Ghys'
offline_file: ""
offline_thumbnail: ""
uuid: af4bb98c-de8b-4155-8679-524cac3b60df
updated: 1484308692
title: Étienne Ghys
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-%25C3%2589tienne_Ghys.jpeg'
categories:
    - Dynamical systems theorists
---
Étienne Ghys (born 29 December 1954) is a French mathematician. His research focuses mainly on geometry and dynamical systems, though his mathematical interests are broad. He also expresses much interest in the historical development of mathematical ideas, especially the contributions of Henri Poincaré.
