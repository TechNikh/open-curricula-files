---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rails
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484313961
title: rails
categories:
    - Dictionary
---
rails
     n : a bar or bars of rolled steel making a track along which
         vehicles can roll [syn: {track}, {rail}]
