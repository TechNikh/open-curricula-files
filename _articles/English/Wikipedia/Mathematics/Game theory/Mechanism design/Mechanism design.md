---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mechanism_design
offline_file: ""
offline_thumbnail: ""
uuid: b61c62b3-a5bd-4292-9a15-94f86c84cae7
updated: 1484309045
title: Mechanism design
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Stanley_Reiter_MDdiagram.png
tags:
    - Intuition
    - Foundations
    - Mechanism
    - Revelation principle
    - Implementability
    - Necessity
    - Sufficiency
    - Highlighted results
    - Revenue equivalence theorem
    - Vickrey–Clarke–Groves mechanisms
    - Gibbard–Satterthwaite theorem
    - Myerson–Satterthwaite theorem
    - Examples
    - Price discrimination
    - Myerson ironing
    - Proof
    - Notes
categories:
    - Mechanism design
---
Mechanism design is a field in economics and game theory that takes an engineering approach to designing economic mechanisms or incentives, toward desired objectives, in strategic settings, where players act rationally. Because it starts at the end of the game, then goes backwards, it is also called reverse game theory. It has broad applications, from economics and politics (markets, auctions, voting procedures) to networked-systems (internet interdomain routing, sponsored search auctions).
