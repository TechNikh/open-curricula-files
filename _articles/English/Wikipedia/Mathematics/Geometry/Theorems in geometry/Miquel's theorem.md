---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Miquel%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 112060bb-1e25-49a7-af2a-e13fbc8ea2ed
updated: 1484309114
title: "Miquel's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Miquel_Circles.svg.png
tags:
    - Pivot theorem
    - Trilinear coordinates of the Miquel point
    - "A converse of Miquel's theorem"
    - Similar inscribed triangle
    - "Miquel and Steiner's quadrilateral theorem"
    - "Miquel's pentagon theorem"
    - "Miquel's six circle theorem"
    - "Three dimensional version of Miquel's theorem"
    - Notes
categories:
    - Theorems in geometry
---
Miquel's theorem is a result in geometry, named after Auguste Miquel,[1] concerning the intersection of three circles, each drawn through one vertex of a triangle and two points on its adjacent sides. It is one of several results concerning circles in Euclidean geometry due to Miquel, whose work was published in Liouville's newly founded journal Journal de mathématiques pures et appliquées.
