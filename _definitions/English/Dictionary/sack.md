---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sack
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484481841
title: sack
categories:
    - Dictionary
---
sack
     n 1: a bag made of paper or plastic for holding customer's
          purchases [syn: {poke}, {paper bag}, {carrier bag}]
     2: an enclosed space; "the trapped miners found a pocket of
        air" [syn: {pouch}, {sac}, {pocket}]
     3: the quantity contained in a sack [syn: {sackful}]
     4: any of various light dry strong white wine from Spain and
        Canary Islands (including sherry)
     5: a woman's full loose hiplength jacket [syn: {sacque}]
     6: a hanging bed of canvas or rope netting (usually suspended
        between two trees); swing easily [syn: {hammock}]
     7: a loose-fitting dress hanging straight from the shoulders
        without a waist [syn: ...
