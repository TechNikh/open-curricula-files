---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Christian_Wiener
offline_file: ""
offline_thumbnail: ""
uuid: b00b791c-43a6-40c8-9ae6-08400c0a4f4c
updated: 1484309107
title: Christian Wiener
categories:
    - Geometers
---
Ludwig Christian Wiener (7 December 1826 Darmstadt – 31 July 1896 Karlsruhe) was a German mathematician who specialized in descriptive geometry. Wiener was also a physicist and philosopher. In 1863, he was the first person to identify qualitatively the internal molecular cause of Brownian motion.
