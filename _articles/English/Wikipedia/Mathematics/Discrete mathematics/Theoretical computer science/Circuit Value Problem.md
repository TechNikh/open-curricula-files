---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circuit_Value_Problem
offline_file: ""
offline_thumbnail: ""
uuid: 9c35ebb2-f95b-40e9-8845-97bf1805f526
updated: 1484308667
title: Circuit Value Problem
categories:
    - Theoretical computer science
---
The Circuit Value Problem (aka. the Circuit Evaluation Problem) is the computational problem of computing the output of a given Boolean circuit on a given input.
