---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/howling
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484580001
title: howling
categories:
    - Dictionary
---
howling
     adj 1: noisy with or as if with loud cries and shouts; "a crying
            mass of rioters"; "a howling wind"; "shouting fans";
            "the yelling fiend" [syn: {crying}, {yelling}, {shouting}]
     2: extraordinarily good; used especially as intensifiers; "a
        fantastic trip to the Orient"; "the film was fantastic!";
        "a howling success"; "a marvelous collection of rare
        books"; "had a rattling conversation about politics"; "a
        tremendous achievement" [syn: {fantastic}, {howling(a)}, {marvelous},
         {marvellous}, {rattling(a)}, {terrific}, {tremendous}, {wonderful},
         {wondrous}]
     n : a long loud emotional utterance; "he gave ...
