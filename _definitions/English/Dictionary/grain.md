---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grain
offline_file: ""
offline_thumbnail: ""
uuid: d0d44366-aa3c-4b97-956e-f235d1a2bc0d
updated: 1484310234
title: grain
categories:
    - Dictionary
---
grain
     n 1: a small hard particle; "a grain of sand"
     2: foodstuff prepared from the starchy grains of cereal grasses
        [syn: {food grain}, {cereal}]
     3: used for pearls or diamonds: 50 mg or 1/4 carat [syn: {metric
        grain}]
     4: 1/60 dram; equals an avoirdupois grain or 64.799 milligrams
     5: 1/7000 pound; equals a troy grain or 64.799 milligrams
     6: dry seedlike fruit produced by the cereal grasses: e.g.
        wheat, barley, Indian corn [syn: {caryopsis}]
     7: the direction or texture of fibers found in wood or leather
        or stone or in a woven fabric; "saw the board across the
        grain"
     v 1: thoroughly work in; "His hands were ...
