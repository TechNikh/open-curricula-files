---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/term
offline_file: ""
offline_thumbnail: ""
uuid: c26c814e-0dfe-4cf9-b70e-b202dbf9dabb
updated: 1484310268
title: term
categories:
    - Dictionary
---
term
     n 1: a word or expression used for some particular thing; "he
          learned many medical terms"
     2: a limited period of time; "a prison term"; "he left school
        before the end of term"
     3: (usually plural) a statement of what is required as part of
        an agreement; "the contract set out the conditions of the
        lease"; "the terms of the treaty were generous" [syn: {condition}]
     4: any distinct quantity contained in a polynomial; "the
        general term of an algebraic equation of the n-th degree"
     5: one of the substantive phrases in a logical proposition;
        "the major term of a syllogism must occur twice"
     6: the end of gestation or ...
