---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blende
offline_file: ""
offline_thumbnail: ""
uuid: 1a5494f3-4e82-4331-ac62-a63ea4e61665
updated: 1484310405
title: blende
categories:
    - Dictionary
---
blende
     n : an ore that is the chief source of zinc; consists largely of
         zinc sulfide in crystalline form [syn: {zinc blende}, {sphalerite}]
