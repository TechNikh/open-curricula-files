---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brief
offline_file: ""
offline_thumbnail: ""
uuid: 869d198a-483b-4ef1-9813-7a96f45720cd
updated: 1484310316
title: brief
categories:
    - Dictionary
---
brief
     adj 1: of short duration or distance; "a brief stay in the
            country"; "in a little while"; "it's a little way
            away" [syn: {little}]
     2: concise and succinct; "covered the matter in a brief
        statement"
     3: (of clothing) very short; "an abbreviated swimsuit"; "a
        brief bikini" [syn: {abbreviated}]
     n 1: a document stating the facts and points of law of a client's
          case [syn: {legal brief}]
     2: a condensed written summary or abstract
     v : give essential information to someone; "The reporters were
         briefed about the President's plan to invade"
