---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laid
offline_file: ""
offline_thumbnail: ""
uuid: 63c64e66-65e4-4037-99fc-f4b6fd58f84f
updated: 1484310260
title: laid
categories:
    - Dictionary
---
laid
     adj : set down according to a plan:"a carefully laid table with
           places set for four people"; "stones laid in a pattern"
           [syn: {set}]
