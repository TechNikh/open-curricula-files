---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/isosceles
offline_file: ""
offline_thumbnail: ""
uuid: 271a08ad-90f9-4ca2-bf82-a96a36b0a224
updated: 1484310214
title: isosceles
categories:
    - Dictionary
---
isosceles
     adj : (of a triangle) having two sides of equal length
