---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flawed
offline_file: ""
offline_thumbnail: ""
uuid: 2ef5bb07-ce8c-41cd-9191-d93c4407e559
updated: 1484310467
title: flawed
categories:
    - Dictionary
---
flawed
     adj : having a blemish or flaw; "a flawed diamond" [syn: {blemished}]
