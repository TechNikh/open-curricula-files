---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hitchin_system
offline_file: ""
offline_thumbnail: ""
uuid: 423a3ad6-72a7-48cb-9d2d-6b36b0e0d4a7
updated: 1484308787
title: Hitchin system
categories:
    - Hamiltonian mechanics
---
In mathematics, the Hitchin integrable system is an integrable system depending on the choice of a complex reductive group and a compact Riemann surface, introduced by Nigel Hitchin in 1987. It lies on the crossroads of the algebraic geometry, theory of Lie algebras and integrable system theory. It also plays an important role in geometric Langlands correspondence over the field of complex numbers; related to conformal field theory. A genus zero analogue of the Hitchin system arises as a certain limit of the Knizhnik–Zamolodchikov equations. Almost all integrable systems of classical mechanics can be obtained as particular cases of the Hitchin system (or its meromorphic generalization or ...
