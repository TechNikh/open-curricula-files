---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elementary
offline_file: ""
offline_thumbnail: ""
uuid: 1689ddaf-eb95-4f4d-9544-8780d59aee20
updated: 1484310486
title: elementary
categories:
    - Dictionary
---
elementary
     adj 1: easy and not involved or complicated; "an elementary problem
            in statistics"; "elementary, my dear Watson"; "a
            simple game"; "found an uncomplicated solution to the
            problem" [syn: {simple}, {uncomplicated}, {unproblematic}]
     2: of or being the essential or basic part; "an elementary need
        for love and nurturing" [syn: {primary}]
