---
version: 1
type: video
provider: YouTube
id: FmQoSenbtnU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/More%20on%20orbitals%20and%20electron%20configuration.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0925b877-268b-4a66-8a76-23ee64431324
updated: 1486069585
title: More on orbitals and electron configuration
tags:
    - dotsub
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/FmQoSenbtnU/default.jpg
    - https://i3.ytimg.com/vi/FmQoSenbtnU/1.jpg
    - https://i3.ytimg.com/vi/FmQoSenbtnU/2.jpg
    - https://i3.ytimg.com/vi/FmQoSenbtnU/3.jpg
---
How electron configuration is related to orbitals and the structure of the periodic table. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/electronic-structure-of-atoms/electron-configurations-jay-sal/v/noble-gas-configuration-electronic-structure-of-atoms-chemistry-khan-academy?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/electronic-structure-of-atoms/electron-configurations-jay-sal/v/orbitals?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
