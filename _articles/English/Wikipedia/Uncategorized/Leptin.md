---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Leptin
offline_file: ""
offline_thumbnail: ""
uuid: c1a13620-09d3-4208-a746-7940dd30b950
updated: 1484308378
title: Leptin
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Leptin.png
tags:
    - Identification of the gene
    - Recognition of scientific advances
    - Location of gene and structure of hormone
    - Mutations
    - Nonsense
    - Frameshift
    - Polymorphisms
    - Transversion
    - Sites of synthesis
    - Blood levels
    - Physiologic variation
    - In specific conditions
    - Mutant leptins
    - Effects
    - Central (hypothalamic)
    - Peripheral (non-hypothalamic)
    - Circulatory system
    - Fetal lung
    - Reproductive system
    - Ovulatory cycle
    - Pregnancy
    - Lactation
    - Puberty
    - Bone
    - Brain
    - Immune system
    - Role in obesity and weight loss
    - Obesity
    - Response to weight loss
    - Therapeutic use
    - Leptin
    - Analog metreleptin
    - Notes
categories:
    - Uncategorized
---
Leptin (from Greek λεπτός leptos, "thin"), the "satiety hormone",[a] is a hormone made by adipose cells that helps to regulate energy balance by inhibiting hunger. Leptin is opposed by the actions of the hormone ghrelin, the "hunger hormone". Both hormones act on receptors in the arcuate nucleus of the hypothalamus to regulate appetite to achieve energy homeostasis.[4] In obesity, a decreased sensitivity to leptin occurs, resulting in an inability to detect satiety despite high energy stores.[5]
