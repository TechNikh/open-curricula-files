---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/twice
offline_file: ""
offline_thumbnail: ""
uuid: 7524c45c-1d81-402b-b6fa-b9638f0d12f4
updated: 1484310216
title: twice
categories:
    - Dictionary
---
twice
     adv 1: two times; "I called her twice"
     2: to double the degree; "she was doubly rewarded"; "his eyes
        were double bright" [syn: {doubly}, {double}]
