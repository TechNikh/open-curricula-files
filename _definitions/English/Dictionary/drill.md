---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drill
offline_file: ""
offline_thumbnail: ""
uuid: a56283c3-8231-4bf4-a032-0899f36694d2
updated: 1484310373
title: drill
categories:
    - Dictionary
---
drill
     n 1: a tool with a sharp point and cutting edges for making holes
          in hard materials (usually rotating rapidly or by
          repeated blows)
     2: similar to the mandrill but smaller and less brightly
        colored [syn: {Mandrillus leucophaeus}]
     3: systematic training by multiple repetitions; "practice makes
        perfect" [syn: {exercise}, {practice}, {practice session},
         {recitation}]
     4: (military) the training of soldiers to march (as in
        ceremonial parades) or to perform the manual of arms
     v 1: make a hole with a pointed power or hand tool; "don't drill
          here, there's a gas pipe"; "drill a hole into the wall";
          ...
