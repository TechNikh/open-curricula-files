---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/idiom
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484453521
title: idiom
categories:
    - Dictionary
---
idiom
     n 1: a manner of speaking that is natural to native speakers of a
          language [syn: {parlance}]
     2: the usage or vocabulary that is characteristic of a specific
        group of people; "the immigrants spoke an odd dialect of
        English"; "he has a strong German accent" [syn: {dialect},
         {accent}]
     3: the style of a particular artist or school or movement; "an
        imaginative orchestral idiom" [syn: {artistic style}]
     4: an expression whose meanings cannot be inferred from the
        meanings of the words that make it up [syn: {idiomatic
        expression}, {phrasal idiom}, {set phrase}, {phrase}]
