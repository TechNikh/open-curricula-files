---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arithmetic_Fuchsian_group
offline_file: ""
offline_thumbnail: ""
uuid: 9f45d17b-0c84-4dde-a464-83b6dfebd7ac
updated: 1484308506
title: Arithmetic Fuchsian group
tags:
    - Definition and examples
    - Quaternion algebras
    - Arithmetic Fuchsian groups
    - Examples
    - Maximal subgroups
    - Congruence subgroups
    - Construction via quadratic forms
    - Arithmetic Kleinian groups
    - Trace fields of arithmetic Fuchsian groups
    - Geometry of arithmetic hyperbolic surfaces
    - Volume formula and finiteness
    - Minimal volume
    - Closed geodesics and injectivity radii
    - Spectra of arithmetic hyperbolic surfaces
    - Laplace eigenvalues and eigenfunctions
    - Selberg conjecture
    - Relations with geometry
    - Quantum ergodicity
    - Isospectral surfaces
    - Notes
categories:
    - Number theory
---
Arithmetic Fuchsian groups are a special class of Fuchsian groups constructed using orders in quaternion algebras. They are particular instances of arithmetic groups. The prototypical example of an arithmetic Fuchsian group is the modular group 
  
    
      
        
          
            P
            S
            L
          
          
            2
          
        
        (
        
          Z
        
        )
      
    
    {\displaystyle \mathrm {PSL} _{2}(\mathbb {Z} )}
  
. They, and the hyperbolic surface associated to their action on the hyperbolic plane often exhibit particularly regular behaviour among Fuchsian groups and hyperbolic surfaces.
