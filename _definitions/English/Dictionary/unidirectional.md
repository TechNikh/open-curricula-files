---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unidirectional
offline_file: ""
offline_thumbnail: ""
uuid: c7eb17da-f0ae-4c64-80e0-1f55ab2d27cc
updated: 1484310158
title: unidirectional
categories:
    - Dictionary
---
unidirectional
     adj : operating or moving or allowing movement in one direction
           only; "a unidirectional flow"; "a unidirectional
           antenna"; "a unidirectional approach to a problem"
           [ant: {bidirectional}]
