---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pipeline
offline_file: ""
offline_thumbnail: ""
uuid: 176d3064-a040-400a-b983-01cb4e1679a6
updated: 1484310258
title: pipeline
categories:
    - Dictionary
---
pipeline
     n 1: gossip spread by spoken communication; "the news of their
          affair was spread by word of mouth" [syn: {grapevine}, {word
          of mouth}]
     2: a pipe used to transport liquids or gases; "a pipeline runs
        from the wells to the seaport" [syn: {line}]
