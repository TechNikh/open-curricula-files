---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Toilet_paper_orientation
offline_file: ""
offline_thumbnail: ""
uuid: e5873a41-807e-43b7-9132-0d81b1dd18f6
updated: 1484309300
title: Toilet paper orientation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Toilet_paper_in_Hotel_Monasterio.jpg
tags:
    - Context and relevance
    - Arguments for over or under
    - Preliminaries
    - Preferences
    - Survey results
    - Themes
    - Sex and age
    - Class and politics
    - Character
    - Noted preferences
    - Social consequences
    - Solutions
    - Mechanical
    - Behavioral
    - Notes and references
    - Notes
categories:
    - Orientation (geometry)
---
Toilet paper when used with a toilet roll holder with a horizontal axle parallel to the floor and also parallel to the wall has two possible orientations: the toilet paper may hang over (in front of) or under (behind) the roll; if perpendicular to the wall, the two orientations are right-left or near-away. The choice is largely a matter of personal preference, dictated by habit. In surveys of US consumers and of bath and kitchen specialists, 60–70 percent of respondents prefer over.[1]
