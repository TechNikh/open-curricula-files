---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alan_D._Taylor
offline_file: ""
offline_thumbnail: ""
uuid: 1305202b-193b-4941-8c4f-69c6f615536d
updated: 1484308513
title: Alan D. Taylor
categories:
    - Fair division
---
Alan Dana Taylor (born October 27, 1947) is an American mathematician who, with Steven Brams, solved the problem of envy-free cake-cutting for an arbitrary number of people with the Brams–Taylor procedure.
