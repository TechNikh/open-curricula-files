---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Howard_Raiffa
offline_file: ""
offline_thumbnail: ""
uuid: 870b4884-c444-4446-8426-6b623becdcba
updated: 1484309025
title: Howard Raiffa
tags:
    - Early life
    - Career
    - Bibliography
categories:
    - Game theorists
---
Howard Raiffa (/ˈreɪfə/; January 24, 1924 – July 8, 2016) was an American academic who was the Frank P. Ramsey Professor (Emeritus) of Managerial Economics, a joint chair held by the Business School and the Kennedy School of Government at Harvard University.[1] He was an influential Bayesian decision theorist and pioneer in the field of decision analysis, with works in statistical decision theory, game theory, behavioral decision theory, risk analysis, and negotiation analysis.[2] He helped found and was the first director of the International Institute for Applied Systems Analysis.[3][4]
