---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/design
offline_file: ""
offline_thumbnail: ""
uuid: d7b6c18d-c2d0-48f1-8afd-95d3ba7da9f1
updated: 1484310475
title: design
categories:
    - Dictionary
---
design
     n 1: the act of working out the form of something (as by making a
          sketch or outline or plan); "he contributed to the
          design of a new instrument" [syn: {designing}]
     2: an arrangement scheme; "the awkward design of the keyboard
        made operation difficult"; "it was an excellent design for
        living"; "a plan for seating guests" [syn: {plan}]
     3: something intended as a guide for making something else; "a
        blueprint for a house"; "a pattern for a skirt" [syn: {blueprint},
         {pattern}]
     4: a decorative or artistic work; "the coach had a design on
        the doors" [syn: {pattern}, {figure}]
     5: an anticipated outcome that ...
