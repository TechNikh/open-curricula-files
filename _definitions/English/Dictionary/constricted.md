---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/constricted
offline_file: ""
offline_thumbnail: ""
uuid: 36c659ed-19aa-4597-867c-37c794d5ff8e
updated: 1484310307
title: constricted
categories:
    - Dictionary
---
constricted
     adj 1: especially tense; especially in some dialects
     2: drawn together or squeezed physically or by extension
        psychologically; "a constricted blood vessel"; "a
        constricted view of life" [ant: {unconstricted}]
     3: inhibited; "when modern man feels socially constricted his
        first impulse is to move"
