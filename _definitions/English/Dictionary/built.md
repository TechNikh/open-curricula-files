---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/built
offline_file: ""
offline_thumbnail: ""
uuid: 80db1a3f-a536-4ae9-aa7d-274ed65a9f84
updated: 1484310258
title: built
categories:
    - Dictionary
---
build
     n 1: constitution of the human body [syn: {physique}, {body-build},
           {habitus}]
     2: alternative names for the body of a human being; "Leonardo
        studied the human body"; "he has a strong physique"; "the
        spirit is willing but the flesh is weak" [syn: {human body},
         {physical body}, {material body}, {soma}, {figure}, {physique},
         {anatomy}, {shape}, {bod}, {chassis}, {frame}, {form}, {flesh}]
     v 1: make by combining materials and parts; "this little pig made
          his house out of straw"; "Some eccentric constructed an
          electric brassiere warmer" [syn: {construct}, {make}]
     2: form or accumulate steadily; "Resistance ...
