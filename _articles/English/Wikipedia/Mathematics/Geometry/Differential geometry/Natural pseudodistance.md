---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Natural_pseudodistance
offline_file: ""
offline_thumbnail: ""
uuid: d91d84a8-7561-4123-8db0-3f577f799a36
updated: 1484309228
title: Natural pseudodistance
categories:
    - Differential geometry
---
In size theory, the natural pseudodistance between two size pairs 
  
    
      
        (
        M
        ,
        φ
        :
        M
        →
        
          R
        
        )
         
      
    
    {\displaystyle (M,\varphi :M\to \mathbb {R} )\ }
  
, 
  
    
      
        (
        N
        ,
        ψ
        :
        N
        →
        
          R
        
        )
         
      
    
    {\displaystyle (N,\psi :N\to \mathbb {R} )\ }
  
 is the value 
  
    
      
        
          inf
          
            h
          
        
        ∥
        φ
        −
        ψ
        ∘
        h
        
          ∥
          
            ∞
   ...
