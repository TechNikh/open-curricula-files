---
version: 1
type: video
provider: YouTube
id: bA6GXNNd1IA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5380a723-20e1-4125-b41f-7d82c96ceebe
updated: 1486069614
title: Math patterns example 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/bA6GXNNd1IA/default.jpg
    - https://i3.ytimg.com/vi/bA6GXNNd1IA/1.jpg
    - https://i3.ytimg.com/vi/bA6GXNNd1IA/2.jpg
    - https://i3.ytimg.com/vi/bA6GXNNd1IA/3.jpg
---
