---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/severity
offline_file: ""
offline_thumbnail: ""
uuid: b761195f-8ea6-486e-8d72-1f61637bb08b
updated: 1484310575
title: severity
categories:
    - Dictionary
---
severity
     n 1: used of the degree of something undesirable e.g. pain or
          weather [syn: {badness}]
     2: something hard to endure; "the asperity of northern winters"
        [syn: {asperity}, {grimness}, {hardship}, {rigor}, {rigour},
         {rigorousness}]
     3: excessive sternness; "severity of character"; "the harshness
        of his punishment was inhuman"; "the rigors of boot camp"
        [syn: {harshness}, {rigor}, {rigour}, {inclemency}, {hardness},
         {stiffness}]
