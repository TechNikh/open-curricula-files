---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Remainder
offline_file: ""
offline_thumbnail: ""
uuid: 70775ac4-1971-4f31-b927-f962630b8145
updated: 1484308664
title: Remainder
tags:
    - Integer division
    - Examples
    - For floating-point numbers
    - In programming languages
    - Polynomial division
    - Notes
categories:
    - Number theory
---
In mathematics, the remainder is the amount "left over" after performing some computation. In arithmetic, the remainder is the integer "left over" after dividing one integer by another to produce an integer quotient (integer division). In algebra, the remainder is the polynomial "left over" after dividing one polynomial by another. The modulo operation is the operation that produces such a remainder when given a dividend and divisor.
