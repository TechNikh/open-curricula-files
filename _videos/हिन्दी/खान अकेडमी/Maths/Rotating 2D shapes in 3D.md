---
version: 1
type: video
provider: YouTube
id: 8YVSFu1DVQY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 64a8ef5d-653f-4b56-b7c5-d0cbbad90773
updated: 1486069613
title: Rotating 2D shapes in 3D
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/8YVSFu1DVQY/default.jpg
    - https://i3.ytimg.com/vi/8YVSFu1DVQY/1.jpg
    - https://i3.ytimg.com/vi/8YVSFu1DVQY/2.jpg
    - https://i3.ytimg.com/vi/8YVSFu1DVQY/3.jpg
---
