---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_plane
offline_file: ""
offline_thumbnail: ""
uuid: a148e4e8-1cfb-45ca-80ba-52e79eb9a7b6
updated: 1484309352
title: Affine plane
categories:
    - Geometry stubs
---
All the affine planes defined over a field are isomorphic. More precisely, the choice of an affine coordinate system (or, in the real case, a Cartesian coordinate system) for an affine plane P over a field F induces an isomorphism of affine planes between P and F2.
