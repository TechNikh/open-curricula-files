---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dual_graph
offline_file: ""
offline_thumbnail: ""
uuid: 748169ef-baab-406f-902d-931aea219d78
updated: 1484309232
title: Dual graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Duals_graphs.svg.png
tags:
    - Examples
    - Cycles and dipoles
    - Dual polyhedra
    - Self-dual graphs
    - Properties
    - Simple graphs versus multigraphs
    - Uniqueness
    - Cuts and cycles
    - Spanning trees
    - Additional properties
    - Variations
    - Directed graphs
    - Weak dual
    - Infinite graphs and tessellations
    - Nonplanar embeddings
    - Matroids and algebraic duals
    - Applications
    - History
    - Notes
categories:
    - Duality theories
---
In the mathematical discipline of graph theory, the dual graph of a plane graph G is a graph that has a vertex for each face of G. The dual graph has an edge whenever two faces of G are separated from each other by an edge, and a self-loop when the same face appears on both sides of an edge. Thus, each edge e of G has a corresponding dual edge, whose endpoints are the dual vertices corresponding to the faces on either side of e. The definition of the dual depends on the choice of embedding of the graph G, so it is a property of plane graphs (graphs that are already embedded in the plane) rather than planar graphs (graphs that may be embedded but for which the embedding is not yet known). ...
