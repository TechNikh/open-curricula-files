---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/speechless
offline_file: ""
offline_thumbnail: ""
uuid: 7d471801-315d-4d0d-877c-e0df1d07750f
updated: 1484310144
title: speechless
categories:
    - Dictionary
---
speechless
     adj : unable to speak temporarily; "struck dumb"; "speechless with
           shock" [syn: {dumb}]
