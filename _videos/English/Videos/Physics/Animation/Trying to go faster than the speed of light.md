---
version: 1
type: video
provider: YouTube
id: qhVgIW4_-AQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Trying%20to%20go%20faster%20than%20the%20speed%20of%20light.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3743c323-968b-4720-9d14-494997e1a963
updated: 1486069656
title: Trying to go faster than the speed of light
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/qhVgIW4_-AQ/default.jpg
    - https://i3.ytimg.com/vi/qhVgIW4_-AQ/1.jpg
    - https://i3.ytimg.com/vi/qhVgIW4_-AQ/2.jpg
    - https://i3.ytimg.com/vi/qhVgIW4_-AQ/3.jpg
---
