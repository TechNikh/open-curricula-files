---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thyroxin
offline_file: ""
offline_thumbnail: ""
uuid: 5cc31405-bc9f-474d-9583-a5450579a159
updated: 1484310160
title: thyroxin
categories:
    - Dictionary
---
thyroxin
     n : hormone produced by the thyroid glands to regulate
         metabolism by controlling the rate of oxidation in cells;
         "thyroxine is 65% iodine" [syn: {thyroxine}, {tetraiodothyronine},
          {T}]
