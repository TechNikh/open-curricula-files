---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calibre
offline_file: ""
offline_thumbnail: ""
uuid: 758e2118-739d-42bd-8df2-0b4f8ca9d2cd
updated: 1484310597
title: calibre
categories:
    - Dictionary
---
calibre
     n 1: a degree or grade of excellence or worth; "the quality of
          students has risen"; "an executive of low caliber" [syn:
           {quality}, {caliber}]
     2: diameter of a tube or gun barrel [syn: {bore}, {gauge}, {caliber}]
