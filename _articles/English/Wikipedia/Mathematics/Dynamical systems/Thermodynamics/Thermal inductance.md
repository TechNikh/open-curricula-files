---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_inductance
offline_file: ""
offline_thumbnail: ""
uuid: a217ac0d-5a7e-4269-b924-b1270eaa9370
updated: 1484308870
title: Thermal inductance
categories:
    - Thermodynamics
---
Thermal inductance refers to the phenomenon wherein a thermal change of an object surrounded by a fluid will induce a change in convection currents within that fluid, thus inducing a change in the kinetic energy of the fluid.[1] It is considered the thermal analogue to electrical inductance in system equivalence modeling; its unit is the thermal henry.[1]
