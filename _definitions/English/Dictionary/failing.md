---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/failing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484424061
title: failing
categories:
    - Dictionary
---
failing
     adj 1: unable to meet financial obligations; "a failing business
            venture" [syn: {failed}]
     2: below acceptable in performance; "received failing grades"
     n 1: a flaw or weak point; "he was quick to point out his wife's
          failings" [syn: {weakness}]
     2: failure to reach a minimum required performance; "his
        failing the course led to his disqualification" [ant: {passing}]
