---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/footage
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484547901
title: footage
categories:
    - Dictionary
---
footage
     n 1: the amount of film that has been shot
     2: a rate of charging by the linear foot of work done
