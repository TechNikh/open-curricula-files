---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vindicate
offline_file: ""
offline_thumbnail: ""
uuid: a98da97b-d8d6-451e-9caf-87549c27abe9
updated: 1484310589
title: vindicate
categories:
    - Dictionary
---
vindicate
     v 1: show to be right by providing justification or proof;
          "vindicate a claim" [syn: {justify}]
     2: maintain, uphold, or defend; "vindicate the rights of the
        citizens"
     3: clear of accusation, blame, suspicion, or doubt with
        supporting proof; "You must vindicate yourself and fight
        this libel"
