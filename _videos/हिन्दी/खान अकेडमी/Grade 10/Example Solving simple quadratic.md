---
version: 1
type: video
provider: YouTube
id: vAw3_CZ7sSY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 14b669bc-2585-4105-91d3-1816294b640a
updated: 1486069619
title: Example Solving simple quadratic
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/vAw3_CZ7sSY/default.jpg
    - https://i3.ytimg.com/vi/vAw3_CZ7sSY/1.jpg
    - https://i3.ytimg.com/vi/vAw3_CZ7sSY/2.jpg
    - https://i3.ytimg.com/vi/vAw3_CZ7sSY/3.jpg
---
