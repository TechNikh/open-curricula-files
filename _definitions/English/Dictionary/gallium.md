---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gallium
offline_file: ""
offline_thumbnail: ""
uuid: 5063293a-046e-4893-b953-6c1ed604d0fd
updated: 1484310397
title: gallium
categories:
    - Dictionary
---
gallium
     n : a rare silvery (usually trivalent) metallic element; brittle
         at low temperatures but liquid above room temperature;
         occurs in trace amounts in bauxite and zinc ores [syn: {Ga},
          {atomic number 31}]
