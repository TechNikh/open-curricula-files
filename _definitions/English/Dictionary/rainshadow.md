---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rainshadow
offline_file: ""
offline_thumbnail: ""
uuid: 3306f440-9570-4f69-9d94-c63aff1d5211
updated: 1484310464
title: rainshadow
categories:
    - Dictionary
---
rain shadow
     n : an area that has little precipitation because some barrier
         causes the winds to lose their moisture before reaching
         it
