---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Pythagorean_trigonometric_identity
offline_file: ""
offline_thumbnail: ""
uuid: 84566ae2-3a65-43ec-b1c3-ffad851f0e33
updated: 1484309347
title: Pythagorean trigonometric identity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Trig_functions.svg.png
tags:
    - Proofs and their relationships to the Pythagorean theorem
    - Proof based on right-angle triangles
    - Related identities
    - Proof using the unit circle
    - Proof using power series
    - Proof using the differential equation
    - In-line notes and references
categories:
    - Trigonometry
---
The Pythagorean trigonometric identity is a trigonometric identity expressing the Pythagorean theorem in terms of trigonometric functions. Along with the sum-of-angles formulae, it is one of the basic relations between the sine and cosine functions.
