---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gasp
offline_file: ""
offline_thumbnail: ""
uuid: f2143498-dc33-4abc-8254-4d8d95b69f26
updated: 1484310152
title: gasp
categories:
    - Dictionary
---
gasp
     n : a short labored intake of breath with the mouth open; "she
         gave a gasp and fainted" [syn: {pant}]
     v : breathe noisily, as when one is exhausted; "The runners
         reached the finish line, panting heavily" [syn: {pant}, {puff},
          {heave}]
