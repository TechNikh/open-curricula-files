---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tough
offline_file: ""
offline_thumbnail: ""
uuid: 245765f8-81f5-423e-a478-6800de351d2d
updated: 1484310522
title: tough
categories:
    - Dictionary
---
tough
     adj 1: not given to gentleness or sentimentality; "a tough
            character" [ant: {tender}]
     2: very difficult; severely testing stamina or resolution; "a
        rugged competitive examination"; "the rugged conditions of
        frontier life"; "the competition was tough"; "it's a tough
        life"; "it was a tough job" [syn: {rugged}]
     3: physically toughened; "the tough bottoms of his feet" [syn:
        {toughened}] [ant: {tender}]
     4: substantially made or constructed; "sturdy steel shelves";
        "sturdy canvas"; "a tough all-weather fabric"; "some
        plastics are as tough as metal" [syn: {sturdy}]
     5: violent and lawless; "the more ruffianly ...
