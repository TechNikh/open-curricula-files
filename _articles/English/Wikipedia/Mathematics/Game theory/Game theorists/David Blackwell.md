---
version: 1
type: article
id: https://en.wikipedia.org/wiki/David_Blackwell
offline_file: ""
offline_thumbnail: ""
uuid: 5486b5ff-0071-46c8-babb-b520711331e4
updated: 1484308512
title: David Blackwell
tags:
    - Career
    - Quotation
    - Honors and awards
categories:
    - Game theorists
---
David Harold Blackwell (April 24, 1919 – July 8, 2010) was Professor Emeritus of Statistics at the University of California, Berkeley, and is one of the eponyms of the Rao–Blackwell theorem.[2] Born in Centralia, Illinois, he was the first African American inducted into the National Academy of Sciences, and the first black tenured faculty member at UC Berkeley.[1][3]
