---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/desperation
offline_file: ""
offline_thumbnail: ""
uuid: 782311c3-8673-4299-8295-306ac5661ded
updated: 1484310484
title: desperation
categories:
    - Dictionary
---
desperation
     n 1: a state in which everything seems wrong and will turn out
          badly; "they were rescued from despair at the last
          minute" [syn: {despair}]
     2: desperate recklessness; "it was a policy of desperation"
