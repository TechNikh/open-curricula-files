---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blast
offline_file: ""
offline_thumbnail: ""
uuid: 0dfee1dd-d3d9-4871-9f43-685af82ee82c
updated: 1484310411
title: blast
categories:
    - Dictionary
---
blast
     n 1: a long and hard-hit fly ball
     2: a sudden very loud noise [syn: {bang}, {clap}, {eruption}, {loud
        noise}]
     3: a strong current of air; "the tree was bent almost double by
        the gust" [syn: {gust}, {blow}]
     4: an explosion (as of dynamite)
     5: a highly pleasurable or exciting experience; "we had a good
        time at the party"; "celebrating after the game was a
        blast" [syn: {good time}]
     6: intense adverse criticism; "Clinton directed his fire at the
        Republican Party"; "the government has come under attack";
        "don't give me any flak" [syn: {fire}, {attack}, {flak}, {flack}]
     v 1: make a strident sound; "She tended ...
