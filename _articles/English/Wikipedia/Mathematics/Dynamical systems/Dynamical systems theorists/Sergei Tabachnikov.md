---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sergei_Tabachnikov
offline_file: ""
offline_thumbnail: ""
uuid: e68521a6-65c1-4cae-901f-f3053e779518
updated: 1484308690
title: Sergei Tabachnikov
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tabachnikov_Dmitry_Fuchs.jpg
categories:
    - Dynamical systems theorists
---
Sergei Tabachnikov, also spelled Serge, (in Russian: Сергей Львович Табачников; born in 1956) is a Russian mathematician who works in geometry and dynamical systems.
