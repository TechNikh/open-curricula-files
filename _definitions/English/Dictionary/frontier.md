---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frontier
offline_file: ""
offline_thumbnail: ""
uuid: 11a166c2-31b3-4c17-b180-8e7a7cc06746
updated: 1484310163
title: frontier
categories:
    - Dictionary
---
frontier
     n 1: a wilderness at the edge of a settled area of a country;
          "the individualism of the frontier in Andrew Jackson's
          day"
     2: an international boundary or the area (often fortified)
        immediately inside the boundary
     3: an undeveloped field of study; a topic inviting research and
        development; "he worked at the frontier of brain science"
