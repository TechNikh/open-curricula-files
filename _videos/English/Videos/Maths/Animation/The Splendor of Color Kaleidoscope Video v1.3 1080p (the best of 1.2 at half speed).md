---
version: 1
type: video
provider: YouTube
id: gxxqdrrpgZc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 23dbe209-e424-4f10-ba93-a275038e1ea1
updated: 1486069641
title: >
    The Splendor of Color Kaleidoscope Video v1.3 1080p (the
    best of 1.2 at half speed)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/gxxqdrrpgZc/default.jpg
    - https://i3.ytimg.com/vi/gxxqdrrpgZc/1.jpg
    - https://i3.ytimg.com/vi/gxxqdrrpgZc/2.jpg
    - https://i3.ytimg.com/vi/gxxqdrrpgZc/3.jpg
---
