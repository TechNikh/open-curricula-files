---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crescent
offline_file: ""
offline_thumbnail: ""
uuid: 371885f2-0808-4ad9-b74a-de3d90194877
updated: 1484310587
title: crescent
categories:
    - Dictionary
---
crescent
     adj : resembling the new moon in shape [syn: {crescent(a)}, {crescent-shaped},
            {semilunar}, {lunate}]
     n : any shape resembling the curved shape of the moon in its
         first or last quarters
