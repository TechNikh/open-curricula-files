---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polyhedral_terrain
offline_file: ""
offline_thumbnail: ""
uuid: 81c189e1-cd4c-489d-bc58-c38c484bd8a5
updated: 1484309179
title: Polyhedral terrain
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Piecewise_linear_function2D.svg.png
categories:
    - Computational geometry
---
In computational geometry, a polyhedral terrain in three-dimensional Euclidean space is a polyhedral surface that intersects every line parallel to some particular line in a connected set (i.e., a point or a line segment) or the empty set.[1] Without loss of generality, we may assume that the line in question is the z-axis of the Cartesian coordinate system. Then a polyhedral terrain is the image of a piecewise-linear function in x and y variables.[2]
