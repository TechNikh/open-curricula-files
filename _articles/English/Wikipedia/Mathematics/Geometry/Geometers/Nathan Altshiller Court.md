---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nathan_Altshiller_Court
offline_file: ""
offline_thumbnail: ""
uuid: 89da2cc5-ea93-419f-869c-0a5685969bf6
updated: 1484309075
title: Nathan Altshiller Court
tags:
    - Biography
    - Major works
categories:
    - Geometers
---
Nathan Altshiller Court (1881-1968) was a Polish-American mathematician, a geometer in particular and author of the famous book College Geometry - An Introduction to the Modern Geometry of the Triangle and the Circle.
