---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consciousness
offline_file: ""
offline_thumbnail: ""
uuid: ab4e9740-c5db-4193-bf3e-ef88482890a8
updated: 1484310547
title: consciousness
categories:
    - Dictionary
---
consciousness
     n 1: an alert cognitive state in which you are aware of yourself
          and your situation; "he lost consciousness" [ant: {unconsciousness}]
     2: having knowledge of; "he had no awareness of his mistakes";
        "his sudden consciousness of the problem he faced"; "their
        intelligence and general knowingness was impressive" [syn:
         {awareness}, {cognizance}, {cognisance}, {knowingness}]
