---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ivory
offline_file: ""
offline_thumbnail: ""
uuid: c33dd805-de03-4d78-9e45-50205be7f86b
updated: 1484310441
title: ivory
categories:
    - Dictionary
---
ivory
     n 1: a hard smooth ivory colored dentine that makes up most of
          the tusks of elephants and walruses [syn: {tusk}]
     2: a shade of white the color of bleached bones [syn: {bone}, {pearl},
         {off-white}]
