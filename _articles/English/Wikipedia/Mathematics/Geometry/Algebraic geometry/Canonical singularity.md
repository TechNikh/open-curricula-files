---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Canonical_singularity
offline_file: ""
offline_thumbnail: ""
uuid: 71eb461f-a710-444b-8676-076c2f46249b
updated: 1484309134
title: Canonical singularity
tags:
    - Definition
    - Properties
    - Classification in small dimensions
    - Pairs
categories:
    - Algebraic geometry
---
In mathematics, canonical singularities appear as singularities of the canonical model of a projective variety, and terminal singularities are special cases that appear as singularities of minimal models. They were introduced by Reid (1980). Terminal singularities are important in the minimal model program because smooth minimal models do not always exist, and thus one must allow certain singularities, namely the terminal singularities.
