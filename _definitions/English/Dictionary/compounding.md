---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compounding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484492161
title: compounding
categories:
    - Dictionary
---
compounding
     n : the act of combining things to form a new whole [syn: {combination},
          {combining}]
