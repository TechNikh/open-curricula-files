---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/population
offline_file: ""
offline_thumbnail: ""
uuid: 24b8627d-7568-4446-824e-df978835214e
updated: 1484310307
title: population
categories:
    - Dictionary
---
population
     n 1: the people who inhabit a territory or state; "the population
          seemed to be well fed and clothed"
     2: a group of organisms of the same species populating a given
        area; "they hired hunters to keep down the deer
        population"
     3: (statistics) the entire aggregation of items from which
        samples can be drawn; "it is an estimate of the mean of
        the population" [syn: {universe}]
     4: the number of inhabitants (either the total number or the
        number of a particular race or class) in a given place
        (country or city etc.); "people come and go, but the
        population of this town has remained approximately
        ...
