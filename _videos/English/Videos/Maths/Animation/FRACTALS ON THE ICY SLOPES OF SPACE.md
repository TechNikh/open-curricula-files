---
version: 1
type: video
provider: YouTube
id: xMMLVSj5o5I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 83ea4fa8-3e62-4673-aaa4-8e495078aead
updated: 1486069647
title: FRACTALS ON THE ICY SLOPES OF SPACE
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/xMMLVSj5o5I/default.jpg
    - https://i3.ytimg.com/vi/xMMLVSj5o5I/1.jpg
    - https://i3.ytimg.com/vi/xMMLVSj5o5I/2.jpg
    - https://i3.ytimg.com/vi/xMMLVSj5o5I/3.jpg
---
