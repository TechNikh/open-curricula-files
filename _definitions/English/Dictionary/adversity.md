---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adversity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484410381
title: adversity
categories:
    - Dictionary
---
adversity
     n 1: a state of misfortune or affliction; "debt-ridden farmers
          struggling with adversity"; "a life of hardship" [syn: {hardship},
           {hard knocks}]
     2: a stroke of ill fortune; a calamitous event; "a period
        marked by adversities"
