---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Promise_theory
offline_file: ""
offline_thumbnail: ""
uuid: 70b41a60-d95f-452f-8810-971cb1d2aaff
updated: 1484308675
title: Promise theory
tags:
    - History
    - Autonomy
    - Multi-agent systems and commitments
    - Economics
    - CFEngine
    - Emergent behaviour
    - Agency as a model of systems in space and time
categories:
    - Theoretical computer science
---
Promise Theory, in the context of information science, is a model of voluntary cooperation between individual, autonomous actors or agents who publish their intentions to one another in the form of promises. It is a form of labelled graph theory, describing discrete networks of agents joined by the unilateral promises they make.
