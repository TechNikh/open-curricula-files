---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Non-Linear_Preferential_Attachment
offline_file: ""
offline_thumbnail: ""
uuid: dd447c42-f59e-4a14-8b25-e86e68f7a953
updated: 1484309438
title: Non-Linear Preferential Attachment
tags:
    - Types of preferential attachment
    - Sub-linear preferential attachment
    - Super-linear preferential attachment
categories:
    - Network theory
---
In network science preferential attachment means that nodes tend to connect to those nodes which have more links. If the network is growing and new nodes tend to connect to existing ones with linear probability in the degree of the existing nodes then preferential attachment leads to a scale-free network. If this probability is sub-linear then the network’s degree distribution is stretched exponential and hubs are much smaller than in a scale-free network. If this probability is super-linear then almost all nodes are connected to a few hubs. According to Kunegis, Blattner and Moser several online networks follow non-linear preferential attachment model. Communication networks and online ...
