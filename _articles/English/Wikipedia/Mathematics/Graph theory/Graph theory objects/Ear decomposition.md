---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ear_decomposition
offline_file: ""
offline_thumbnail: ""
uuid: 43992c9e-59d9-47ff-82e6-847fcfc995fa
updated: 1484309443
title: Ear decomposition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ear_decomposition_0.png
tags:
    - Characterizing graph classes
    - Graph connectivity
    - Strong connectivity of directed graphs
    - Factor-critical graphs
    - Series-parallel graphs
    - Matroids
    - Algorithms
categories:
    - Graph theory objects
---
In graph theory, an ear of an undirected graph G is a path P where the two endpoints of the path may coincide, but where otherwise no repetition of edges or vertices is allowed, so every internal vertex of P has degree two in P. An ear decomposition of an undirected graph G is a partition of its set of edges into a sequence of ears, such that the one or two endpoints of each ear belong to earlier ears in the sequence and such that the internal vertices of each ear do not belong to any earlier ear. Additionally, in most cases the first ear in the sequence must be a cycle. An open ear decomposition or a proper ear decomposition is an ear decomposition in which the two endpoints of each ear ...
