---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ovoid_(polar_space)
offline_file: ""
offline_thumbnail: ""
uuid: bd3e3006-af6e-4400-adf5-fc73a14371ac
updated: 1484309253
title: Ovoid (polar space)
tags:
    - Cases
    - Symplectic polar space
    - Hermitian polar space
    - Hyperbolic quadrics
    - Parabolic quadrics
    - Elliptic quadrics
categories:
    - Incidence geometry
---
In mathematics, an ovoid O of a (finite) polar space of rank r is a set of points, such that every subspace of rank 
  
    
      
        r
        −
        1
      
    
    {\displaystyle r-1}
  
 intersects O in exactly one point.[1]
