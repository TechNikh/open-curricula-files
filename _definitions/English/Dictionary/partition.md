---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/partition
offline_file: ""
offline_thumbnail: ""
uuid: df59e82a-ca65-47ad-944c-33a7afd2913b
updated: 1484310435
title: partition
categories:
    - Dictionary
---
partition
     n 1: a vertical structure that divides or separates (as a wall
          divides one room from another) [syn: {divider}]
     2: the act of dividing or partitioning; separation by the
        creation of a boundary that divides or keeps apart [syn: {division},
         {partitioning}, {segmentation}, {sectionalization}, {sectionalisation}]
     3: (computer science) the part of a hard disk that is dedicated
        to a particular operating system or application and
        accessed as a single unit
     v 1: divide into parts, pieces, or sections; "The Arab peninsula
          was partitioned by the British" [syn: {partition off}]
     2: separate or apportion into sections; ...
