---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Time-Sensitive_Networking
offline_file: ""
offline_thumbnail: ""
uuid: a811d29e-e62b-4aa0-983b-422a8e30b926
updated: 1484308742
title: Time-Sensitive Networking
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/688px-MontreGousset001_0.jpg
tags:
    - Current status
categories:
    - Control engineering
---
Time-Sensitive Networking (TSN) is a set of standards under development by the Time-Sensitive Networking task group of the IEEE 802.1 working group.[1] The TSN task group was formed at November 2012 by renaming the existing Audio / Video Bridging Task Group[2] and continuing its work. The name changed as a result of extension of the working area of the standardization group. The standards define mechanisms for the time-sensitive transmission of data over Ethernet networks.
