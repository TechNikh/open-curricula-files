---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/umbrella
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484498341
title: umbrella
categories:
    - Dictionary
---
umbrella
     adj : covering or applying simultaneously to a number of similar
           items or elements or groups; "an umbrella
           organization"; "umbrella insurance coverage"
     n 1: a lightweight handheld collapsible canopy
     2: a formation of military planes maintained over ground
        operations or targets; "an air umbrella over England"
     3: having the function of uniting a group of similar things;
        "the Democratic Party is an umbrella for many liberal
        groups"; "under the umbrella of capitalism"
