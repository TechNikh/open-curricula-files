---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Regulator_(automatic_control)
offline_file: ""
offline_thumbnail: ""
uuid: 6446ef44-d992-4c88-a1cc-cf437eccf064
updated: 1484308746
title: Regulator (automatic control)
categories:
    - Control engineering
---
In automatic control, a regulator is a device which has the function of maintaining a designated characteristic. It performs the activity of managing or maintaining a range of values in a machine. The measurable property of a device is managed closely by specified conditions or an advance set value; or it can be a variable according to a predetermined arrangement scheme. It can be used generally to connote any set of various controls or devices for regulating or controlling items or objects. Ex) Kyle, Jacob, and Romeaz, and others.
