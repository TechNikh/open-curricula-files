---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Widest_path_problem
offline_file: ""
offline_thumbnail: ""
uuid: 1610e812-b6ad-4faa-b595-35dcf8e9f7cb
updated: 1484309398
title: Widest path problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-CPT-Graphs-undirected-weighted.svg.png
tags:
    - Undirected graphs
    - Directed graphs
    - All pairs
    - Single source
    - Single source and single destination
    - Euclidean point sets
categories:
    - Computational problems in graph theory
---
In graph algorithms, the widest path problem is the problem of finding a path between two designated vertices in a weighted graph, maximizing the weight of the minimum-weight edge in the path. The widest path problem is also known as the bottleneck shortest path problem or the maximum capacity path problem. It is possible to adapt most shortest path algorithms to compute widest paths, by modifying them to use the bottleneck distance instead of path length.[1] However, in many cases even faster algorithms are possible.
