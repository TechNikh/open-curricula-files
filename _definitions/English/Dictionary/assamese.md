---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assamese
offline_file: ""
offline_thumbnail: ""
uuid: 32f731ea-aca9-46ba-8fb4-c605e909b60a
updated: 1484310189
title: assamese
categories:
    - Dictionary
---
Assamese
     adj : of or relating to or characteristic of Assam or its people
           or culture
     n 1: native or inhabitant of the state of Assam in northeastern
          India
     2: the Magadhan language spoken by the Assamese people; closely
        related to Bengali [syn: {Asamiya}]
