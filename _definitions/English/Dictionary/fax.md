---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fax
offline_file: ""
offline_thumbnail: ""
uuid: d7d22804-eb87-4e59-a248-e74f70fdd0fa
updated: 1484310527
title: fax
categories:
    - Dictionary
---
fax
     n : duplicator that transmits the copy by wire or radio [syn: {facsimile},
          {facsimile machine}]
     v : send something via a facsimile machine; "Can you fax me the
         report right away?" [syn: {telefax}, {facsimile}]
