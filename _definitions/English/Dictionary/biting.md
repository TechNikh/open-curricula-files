---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biting
offline_file: ""
offline_thumbnail: ""
uuid: 65a9ee00-2c01-4b61-9e9e-5c1614cc479c
updated: 1484310343
title: biting
categories:
    - Dictionary
---
biting
     adj 1: capable of wounding; "a barbed compliment"; "a biting
            aphorism"; "pungent satire" [syn: {barbed}, {nipping},
             {pungent}, {mordacious}]
     2: causing a sharply painful or stinging sensation; used
        especially of cold; "bitter cold"; "a biting wind" [syn: {bitter}]
