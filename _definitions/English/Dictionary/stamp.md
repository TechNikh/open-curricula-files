---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stamp
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484341741
title: stamp
categories:
    - Dictionary
---
stamp
     n 1: a token that postal fees have been paid [syn: {postage}, {postage
          stamp}]
     2: the distinctive form in which a thing is made; "pottery of
        this cast was found throughout the region" [syn: {cast}, {mold}]
     3: a type or class; "more men of his stamp are needed"
     4: a symbol that is the result of printing; "he put his stamp
        on the envelope" [syn: {impression}]
     5: machine consisting of a heavy bar that moves vertically for
        pounding or crushing ores [syn: {pestle}]
     6: a block or die used to imprint a mark or design
     7: a device incised to make an impression; used to secure a
        closing or to authenticate documents ...
