---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Narrative_network
offline_file: ""
offline_thumbnail: ""
uuid: e066b0d8-006c-4c22-a382-9524f7f98e22
updated: 1484309435
title: Narrative network
tags:
    - 'Overview: Narrative as a structure of a story in time'
    - Background
    - Current Studies
    - Citations
categories:
    - Network theory
---
A narrative network is a system which represents complex event sequences or characters’ interactions as depicted by a narrative text. Network science methodology offers an alternative way of analysing the patterns of relationships, composition and activities of events and actors studied in their own context. Network theory can contribute to the understanding of the structural properties of a text and the data contained in it. The meaning of the individual and the community in a narrative is conditional on their position in a system of social relationships reported by the author. Hence, a central problem when dealing with narratives is framing and organising the author’s perspective of ...
