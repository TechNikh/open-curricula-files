---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expelled
offline_file: ""
offline_thumbnail: ""
uuid: 19c867bd-fb6a-4408-87e3-cb2e695fc27f
updated: 1484310325
title: expelled
categories:
    - Dictionary
---
expel
     v 1: force to leave or move out; "He was expelled from his native
          country" [syn: {throw out}, {kick out}]
     2: put out or expel from a place; "The child was expelled from
        the classroom" [syn: {eject}, {chuck out}, {exclude}, {throw
        out}, {kick out}, {turf out}, {boot out}, {turn out}]
     3: remove from a position or office; "The chairman was ousted
        after he misappropriated funds" [syn: {oust}, {throw out},
         {drum out}, {boot out}, {kick out}]
     4: cause to flee; "rout out the fighters from their caves"
        [syn: {rout}, {rout out}]
     5: eliminate (substances) from the body [syn: {discharge}, {eject},
         {release}]
    ...
