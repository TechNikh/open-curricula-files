---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Claw-free_graph
offline_file: ""
offline_thumbnail: ""
uuid: 8d68b85b-c86e-4758-acc8-6f4a87f88d64
updated: 1484309423
title: Claw-free graph
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Complete_bipartite_graph_K3%252C1.svg.png'
tags:
    - Examples
    - Recognition
    - Enumeration
    - Matchings
    - Independent sets
    - Coloring, cliques, and domination
    - Structure
    - Notes
categories:
    - Matching
---
A claw is another name for the complete bipartite graph K1,3 (that is, a star graph with three edges, three leaves, and one central vertex). A claw-free graph is a graph in which no induced subgraph is a claw; i.e., any subset of four vertices has other than only three edges connecting them in this pattern. Equivalently, a claw-free graph is a graph in which the neighborhood of any vertex is the complement of a triangle-free graph.
