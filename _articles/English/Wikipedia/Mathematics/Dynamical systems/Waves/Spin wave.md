---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spin_wave
offline_file: ""
offline_thumbnail: ""
uuid: 6c16e380-adae-4d43-b3fb-6b1af4b54ead
updated: 1484308900
title: Spin wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Precession2.png
tags:
    - Theory
    - Experimental observation
    - Practical significance
categories:
    - Waves
---
Spin waves are propagating disturbances in the ordering of magnetic materials. These low-lying collective excitations occur in magnetic lattices with continuous symmetry. From the equivalent quasiparticle point of view, spin waves are known as magnons, which are boson modes of the spin lattice that correspond roughly to the phonon excitations of the nuclear lattice. As temperature is increased, the thermal excitation of spin waves reduces a ferromagnet's spontaneous magnetization. The energies of spin waves are typically only μeV in keeping with typical Curie points at room temperature and below. The discussion of spin waves in antiferromagnets is beyond the scope of this article.
