---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diversification
offline_file: ""
offline_thumbnail: ""
uuid: 3ead9d2b-9aff-4a70-8025-650905d173cd
updated: 1484310533
title: diversification
categories:
    - Dictionary
---
diversification
     n 1: the act of introducing variety (especially in investments or
          in the variety of goods and services offered); "my
          broker recommended a greater diversification of my
          investments"; "he limited his losses by diversification
          of his product line" [syn: {variegation}]
     2: the condition of being varied; "that restaurant's menu lacks
        diversification; every day it is the same"
