---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feathery
offline_file: ""
offline_thumbnail: ""
uuid: 47262e93-b05b-41b5-9be4-da3a767a6972
updated: 1484310283
title: feathery
categories:
    - Dictionary
---
feathery
     adj 1: suggestive of feathers in lightness; "feathery snowflakes"
            [syn: {featherlike}]
     2: resembling a feather or feathers; "feathery palm trees"
        [syn: {featherlike}]
     3: characterized by a covering of feathers; "the feathery
        congregaton of jays"
     4: adorned with feathers or plumes [syn: {feathered}, {plumy}]
