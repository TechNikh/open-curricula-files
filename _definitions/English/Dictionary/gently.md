---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gently
offline_file: ""
offline_thumbnail: ""
uuid: 88ae93d9-830f-4327-88bf-926175248ae6
updated: 1484310316
title: gently
categories:
    - Dictionary
---
gently
     adv 1: in a gradual manner; "a gently sloping terrain"
     2: in a gentle manner; "he talked gently to the injured animal"
        [syn: {mildly}]
     3: with little weight or force; "she kissed him lightly on the
        forehead" [syn: {lightly}, {softly}]
