---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inversion_in_a_sphere
offline_file: ""
offline_thumbnail: ""
uuid: c467c314-2251-494e-97e4-6d9fa9b99892
updated: 1484309258
title: Inversion in a sphere
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Sphere_inversion.png
tags:
    - Definition
    - Properties
    - Proofs
    - Definition
    - Construction
    - Inversion of a pair of points
    - Inverse of a line
    - Inverse of a plane
    - Inverse of a Sphere
    - Inverse of a circle
    - Results of inversion in a sphere
categories:
    - Inversive geometry
---
In geometry, inversion in a sphere is a transformation of Euclidean space that fixes the points of a sphere while sending the points inside of the sphere to the outside of the sphere, and vice versa. Intuitively, it "swaps the inside and outside" of the sphere while leaving the points on the sphere unchanged. Inversion is a conformal transformation, and is the basic operation of inversive geometry.
