---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flow_process
offline_file: ""
offline_thumbnail: ""
uuid: 15b2e5c5-5e1f-4ebd-aff5-fa44abf143a0
updated: 1484308843
title: Flow process
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-First_law_open_system.svg.png
categories:
    - Thermodynamics
---
The region of space enclosed by open system boundaries is usually called a control volume. It may or may not correspond to physical walls. It is convenient to define the shape of the control volume so that all flow of matter, in or out, occurs perpendicular to its surface. One may consider a process in which the matter flowing into and out of the system is chemically homogeneous.[1] Then the inflowing matter performs work as if it were driving a piston of fluid into the system. Also, the system performs work as if it were driving out a piston of fluid. Through the system walls that do not pass matter, heat (δQ) and work (δW) transfers may be defined, including shaft work.
