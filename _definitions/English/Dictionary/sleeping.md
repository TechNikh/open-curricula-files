---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sleeping
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484479921
title: sleeping
categories:
    - Dictionary
---
sleeping
     adj : lying with head on paws as if sleeping [syn: {dormant(ip)}]
     n 1: the state of being asleep [ant: {waking}]
     2: quiet and inactive restfulness [syn: {quiescence}, {quiescency},
         {dormancy}]
     3: the suspension of consciousness and decrease in metabolic
        rate
