---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preserved
offline_file: ""
offline_thumbnail: ""
uuid: 0127e13b-ed8c-4db4-82dc-e42406155c85
updated: 1484310284
title: preserved
categories:
    - Dictionary
---
preserved
     adj 1: prevented from decaying or spoiling and prepared for future
            use [ant: {fresh}]
     2: kept intact or in a particular condition [ant: {destroyed}]
