---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Topological_graph
offline_file: ""
offline_thumbnail: ""
uuid: 4522abeb-44c1-4229-a0b7-5f58d0b62497
updated: 1484309483
title: Topological graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Oddpair.jpg
tags:
    - Extremal problems for topological graphs
    - Quasi-planar graphs
    - Crossing numbers
    - Ramsey-type problems for geometric graphs
    - Topological hypergraphs
categories:
    - Topological graph theory
---
In mathematics, a topological graph is a representation of a graph in the plane, where the vertices of the graph are represented by distinct points and the edges by Jordan arcs (connected pieces of Jordan curves) joining the corresponding pairs of points. The points representing the vertices of a graph and the arcs representing its edges are called the vertices and the edges of the topological graph. It is usually assumed that any two edges of a topological graph cross a finite number of times, no edge passes through a vertex different from its endpoints, and no two edges touch each other (without crossing). A topological graph is also called a drawing of a graph.
