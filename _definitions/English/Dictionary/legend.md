---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/legend
offline_file: ""
offline_thumbnail: ""
uuid: 588683a1-2b1a-4d7a-98dc-73c2e97980f9
updated: 1484310146
title: legend
categories:
    - Dictionary
---
legend
     n 1: a story about mythical or supernatural beings or events
          [syn: {fable}]
     2: brief description accompanying an illustration [syn: {caption}]
