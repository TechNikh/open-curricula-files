---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lev_Pitaevskii
offline_file: ""
offline_thumbnail: ""
uuid: 08e363bc-46a2-408b-bc91-22f5b3025209
updated: 1484308762
title: Lev Pitaevskii
tags:
    - Education
    - Research
    - Honors and awards
    - Works
    - Books and monographs
    - Papers
categories:
    - Electrodynamics
---
Lev Petrovich Pitaevskii (Russian: Лев Петро́вич Пита́евский [ˈlʲɛf pʲɪˈtrovʲɪtɕ pʲɪˈtajɪfskʲɪj]) is a Soviet theoretical physicist, who made contributions to the theory of quantum mechanics, electrodynamics, low-temperature physics, plasma physics, and condensed matter physics. Together with Evgeny Lifshitz and Vladimir Berestetskii, Lev Pitaevskii has also been the co-author of a few volumes of the influential Landau–Lifschitz Course of Theoretical Physics series. His academic status is professor.
