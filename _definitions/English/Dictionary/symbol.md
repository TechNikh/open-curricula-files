---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/symbol
offline_file: ""
offline_thumbnail: ""
uuid: 3f38b6e1-6c30-4fc8-9d6c-9fbff8cd1a52
updated: 1484310297
title: symbol
categories:
    - Dictionary
---
symbol
     n 1: an arbitrary sign (written or printed) that has acquired a
          conventional significance
     2: something visible that by association or convention
        represents something else that is invisible; "the eagle is
        a symbol of the United States" [syn: {symbolization}, {symbolisation},
         {symbolic representation}]
     [also: {symbolling}, {symbolled}]
