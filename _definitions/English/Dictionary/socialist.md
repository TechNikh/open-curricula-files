---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/socialist
offline_file: ""
offline_thumbnail: ""
uuid: f9f35c66-7525-4a0c-af84-18b7f35fa6c8
updated: 1484310549
title: socialist
categories:
    - Dictionary
---
socialist
     adj 1: of or relating to or promoting or practicing socialism;
            "socialist theory"; "socialist realism"; "asocialist
            party" [syn: {socialistic}]
     2: advocating or following the socialist principles;
        "socialistic government" [syn: {socialistic}] [ant: {capitalistic}]
     n : a political advocate of socialism
