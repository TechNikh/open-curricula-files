---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sharing
offline_file: ""
offline_thumbnail: ""
uuid: f61ad5a8-aea3-4d28-870a-4baffc1be302
updated: 1484310258
title: sharing
categories:
    - Dictionary
---
sharing
     adj 1: sharing equally with another or others
     2: unselfishly willing to share with others; "a warm and
        sharing friend"
     n 1: having in common; "the sharing of electrons creates
          molecules"
     2: using or enjoying something jointly with others
     3: sharing thoughts and feelings [syn: {communion}]
     4: a distribution in shares [syn: {share-out}]
