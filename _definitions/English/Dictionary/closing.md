---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/closing
offline_file: ""
offline_thumbnail: ""
uuid: 85f76a35-6459-4c8f-9e7d-553552b4f0ca
updated: 1484310323
title: closing
categories:
    - Dictionary
---
closing
     adj : final or ending; "the closing stages of the election"; "the
           closing weeks of the year"; "the closing scene of the
           film"; "closing remarks" [ant: {opening}]
     n 1: the act of closing something [syn: {shutting}] [ant: {opening}]
     2: the last section of a communication; "in conclusion I want
        to say..." [syn: {conclusion}, {end}, {close}, {ending}]
     3: approaching a particular destination; a coming closer; a
        narrowing of a gap; "the ship's rapid rate of closing gave
        them little time to avoid a collision" [syn: {closure}]
     4: termination of operations; "they regretted the closure of
        the day care center" [syn: ...
