---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fallopian_tube
offline_file: ""
offline_thumbnail: ""
uuid: c6ccb02b-3a35-402b-9aa2-aced52529fa5
updated: 1484308366
title: Fallopian tube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Scheme_female_reproductive_system-en.svg.png
tags:
    - Structure
    - Histology
    - Development
    - Function
    - Fertilization
    - Clinical significance
    - Patency testing
    - Inflammation
    - Cancer
    - Surgery
    - History
    - Additional images
categories:
    - Uncategorized
---
The Fallopian tubes, also known as, uterine tubes, and salpinges (singular salpinx), are two very fine tubes lined with ciliated epithelia, leading from the ovaries of female mammals into the uterus, via the uterotubal junction. In non-mammalian vertebrates, the equivalent structures are called oviducts.
