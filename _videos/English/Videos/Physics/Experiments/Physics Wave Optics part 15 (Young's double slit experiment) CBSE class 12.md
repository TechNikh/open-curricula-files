---
version: 1
type: video
provider: YouTube
id: p-vepMIWFKM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7ddf7c91-4e29-43ed-8c8b-2c532a05ac94
updated: 1486069635
title: "Physics Wave Optics part 15 (Young's double slit experiment) CBSE class 12"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/p-vepMIWFKM/default.jpg
    - https://i3.ytimg.com/vi/p-vepMIWFKM/1.jpg
    - https://i3.ytimg.com/vi/p-vepMIWFKM/2.jpg
    - https://i3.ytimg.com/vi/p-vepMIWFKM/3.jpg
---
