---
version: 1
type: video
provider: YouTube
id: lgijJ7cvG3I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7e6a98aa-8c64-4b62-a319-902f3d2744eb
updated: 1486069643
title: '95% confidence interval in 30 seconds'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/lgijJ7cvG3I/default.jpg
    - https://i3.ytimg.com/vi/lgijJ7cvG3I/1.jpg
    - https://i3.ytimg.com/vi/lgijJ7cvG3I/2.jpg
    - https://i3.ytimg.com/vi/lgijJ7cvG3I/3.jpg
---
