---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hearts
offline_file: ""
offline_thumbnail: ""
uuid: 4ab2e348-77af-4f59-91f9-f0ed21964cab
updated: 1484310561
title: hearts
categories:
    - Dictionary
---
hearts
     n : a form of whist in which players avoid winning tricks
         containing hearts of the queen of spades [syn: {Black
         Maria}]
