---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Joel_Spencer
offline_file: ""
offline_thumbnail: ""
uuid: a54d07e6-d6dc-4a2c-bbec-94c794d4debb
updated: 1484309476
title: Joel Spencer
tags:
    - Selected publications
categories:
    - Graph theorists
---
Joel Spencer (born April 20, 1946) is an American mathematician. He is a combinatorialist who has worked on probabilistic methods in combinatorics and on Ramsey theory. He received his doctorate from Harvard University in 1970, under the supervision of Andrew Gleason.[1] He is currently (as of 2015[update]) a professor at the Courant Institute of Mathematical Sciences of New York University.
