---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Directed_graph
offline_file: ""
offline_thumbnail: ""
uuid: a860f56e-a130-4a66-bff5-66a35c5af508
updated: 1484308630
title: Directed graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/125px-Directed.svg_0.png
tags:
    - Definition
    - Types of directed graphs
    - Subclasses
    - Digraphs with supplementary properties
    - Basic terminology
    - Indegree and outdegree
    - Degree sequence
    - Directed graph connectivity
    - Notes
categories:
    - Graph theory
---
In mathematics, and more specifically in graph theory, a directed graph (or digraph) is a graph (that is a set of vertices connected by edges), where the edges have a direction associated with them.
