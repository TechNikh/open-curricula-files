---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thesis
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464201
title: thesis
categories:
    - Dictionary
---
thesis
     n 1: an unproved statement put forward as a premise in an
          argument
     2: a treatise advancing a new point of view resulting from
        research; usually a requirement for an advanced academic
        degree [syn: {dissertation}]
     [also: {theses} (pl)]
