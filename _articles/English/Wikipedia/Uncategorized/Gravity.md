---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gravitational_force
offline_file: ""
offline_thumbnail: ""
uuid: 1692944b-12ad-4b78-8234-0fca1310da8b
updated: 1484308366
title: Gravity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-GPB_circling_earth.jpg
tags:
    - History of gravitational theory
    - Earlier Concepts of Gravity
    - Scientific revolution
    - "Newton's theory of gravitation"
    - Equivalence principle
    - General relativity
    - Solutions
    - Tests
    - Gravity and quantum mechanics
    - Specifics
    - "Earth's gravity"
    - Equations for a falling body near the surface of the Earth
    - Gravity and astronomy
    - Gravitational radiation
    - Speed of gravity
    - Anomalies and discrepancies
    - Alternative theories
    - Historical alternative theories
    - Modern alternative theories
    - Footnotes
categories:
    - Uncategorized
---
Gravity, or gravitation, is a natural phenomenon by which all things with mass are brought toward (or gravitate toward) one another, including planets, stars and galaxies. Since energy and mass are equivalent, all forms of energy, including light, also cause gravitation and are under the influence of it. On Earth, gravity gives weight to physical objects and causes the ocean tides. The gravitational attraction of the original gaseous matter present in the Universe caused it to begin coalescing, forming stars — and the stars to group together into galaxies — so gravity is responsible for many of the large scale structures in the Universe. Gravity has an infinite range, although its ...
