---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solved
offline_file: ""
offline_thumbnail: ""
uuid: 23d5c20e-e0e0-4aa9-a87e-ac7815c8d5c3
updated: 1484310601
title: solved
categories:
    - Dictionary
---
solved
     adj : explained or answered; "mysteries solved and unsolved;
           problems resolved and unresolved" [syn: {resolved}]
           [ant: {unsolved}]
