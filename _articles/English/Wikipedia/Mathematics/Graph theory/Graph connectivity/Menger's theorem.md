---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Menger%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 041273ad-445c-4c1a-96c7-e47211af96bb
updated: 1484309400
title: "Menger's theorem"
tags:
    - Edge connectivity
    - Vertex connectivity
    - Reductions to the directed and to the bipartite cases
    - Infinite graphs
categories:
    - Graph connectivity
---
In the mathematical discipline of graph theory and related areas, Menger's theorem is a characterization of the connectivity in finite undirected graphs in terms of the minimum number of disjoint paths that can be found between any pair of vertices. It was proved for edge-connectivity and vertex-connectivity by Karl Menger in 1927. The edge-connectivity version of Menger's theorem was later generalized by the max-flow min-cut theorem.
