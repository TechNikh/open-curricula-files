---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zaslavskii_map
offline_file: ""
offline_thumbnail: ""
uuid: 96f5e80b-3549-4b3f-a5ec-ce1ae7313a5c
updated: 1484308719
title: Zaslavskii map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Zaslavskii_map.png
categories:
    - Chaotic maps
---
The Zaslavskii map is a discrete-time dynamical system introduced by George M. Zaslavsky. It is an example of a dynamical system that exhibits chaotic behavior. The Zaslavskii map takes a point (
  
    
      
        
          x
          
            n
          
        
        ,
        
          y
          
            n
          
        
      
    
    {\displaystyle x_{n},y_{n}}
  
) in the plane and maps it to a new point:
