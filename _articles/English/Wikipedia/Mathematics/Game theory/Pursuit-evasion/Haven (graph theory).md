---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Haven_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: f08b1def-10a6-4a25-a2bc-79cbc2e4a41c
updated: 1484309063
title: Haven (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-3x3_grid_graph_haven.svg.png
tags:
    - Definition
    - Example
    - Pursuit-evasion
    - Connections to treewidth, separators, and minors
    - In infinite graphs
categories:
    - Pursuit-evasion
---
In graph theory, a haven is a certain type of function on sets of vertices in an undirected graph. If a haven exists, it can be used by an evader to win a pursuit-evasion game on the graph, by consulting the function at each step of the game to determine a safe set of vertices to move into. Havens were first introduced by Seymour & Thomas (1993) as a tool for characterizing the treewidth of graphs.[1] Their other applications include proving the existence of small separators on minor-closed families of graphs,[2] and characterizing the ends and clique minors of infinite graphs.[3][4]
