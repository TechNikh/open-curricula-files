---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/damn
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484457481
title: damn
categories:
    - Dictionary
---
damn
     adj 1: used as expletives; "oh, damn (or goddamn)!" [syn: {goddamn}]
     2: expletives used informally as intensifiers; "he's a blasted
        idiot"; "it's a blamed shame"; "a blame cold winter"; "not
        a blessed dime"; "I'll be damned (or blessed or darned or
        goddamned) if I'll do any such thing"; "he's a damn (or
        goddam or goddamned) fool"; "a deuced idiot"; "tired or
        his everlasting whimpering"; "an infernal nuisance" [syn:
        {blasted}, {blame}, {blamed}, {blessed}, {damned}, {darned},
         {deuced}, {everlasting}, {goddam}, {goddamn}, {goddamned},
         {infernal}]
     n : something of little value; "his promise is not worth a
    ...
