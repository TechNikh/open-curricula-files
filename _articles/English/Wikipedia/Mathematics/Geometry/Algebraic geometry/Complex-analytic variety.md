---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex-analytic_variety
offline_file: ""
offline_thumbnail: ""
uuid: 4d312a72-8bfd-480f-8677-ea6d26accf9d
updated: 1484309138
title: Complex-analytic variety
categories:
    - Algebraic geometry
---
In mathematics, specifically complex geometry, a complex-analytic variety is defined locally as the set of common zeros of finitely many analytic functions. It is analogous to the included concept of complex algebraic variety, and every complex manifold is an analytic variety. Since analytic varieties may have singular points, not all analytic varieties are complex manifolds. A further generalization of analytic varieties is provided by the notion of complex analytic space. An analytic variety is also called a (real or complex) analytic set.
