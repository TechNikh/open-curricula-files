---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/winnowing
offline_file: ""
offline_thumbnail: ""
uuid: 1e5fddbb-a7eb-4b07-9d04-7ecf08b82e6f
updated: 1484310521
title: winnowing
categories:
    - Dictionary
---
winnowing
     n : the act of separating grain from chaff; "the winnowing was
         done by women" [syn: {winnow}, {sifting}]
