---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terrace
offline_file: ""
offline_thumbnail: ""
uuid: 440d08b7-a6a2-4482-a67e-98064446342d
updated: 1484310435
title: terrace
categories:
    - Dictionary
---
terrace
     n 1: usually paved outdoor area adjoining a residence [syn: {patio}]
     2: a level shelf of land interrupting a declivity (with steep
        slopes above and below) [syn: {bench}]
     3: a row of houses built in a similar style and having common
        dividing walls (or the street on which they face);
        "Grosvenor Terrace"
     v 1: provide (a house) with a terrace; "We terrassed the country
          house" [syn: {terrasse}]
     2: make into terraces as for cultivation; "The Incas terraced
        their mountainous land"
