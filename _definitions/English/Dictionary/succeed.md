---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/succeed
offline_file: ""
offline_thumbnail: ""
uuid: 48c9a6bf-eaef-4c6e-8da1-7e0259caf032
updated: 1484310168
title: succeed
categories:
    - Dictionary
---
succeed
     v 1: attain success or reach a desired goal; "The enterprise
          succeeded"; "We succeeded in getting tickets to the
          show"; "she struggled to overcome her handicap and won"
          [syn: {win}, {come through}, {bring home the bacon}, {deliver
          the goods}] [ant: {fail}]
     2: be the successor (of); "Carter followed Ford"; "Will Charles
        succeed to the throne?" [syn: {come after}, {follow}]
        [ant: {precede}]
