---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Pentagrammic-order_600-cell_honeycomb
offline_file: ""
offline_thumbnail: ""
uuid: 4a94106d-e45c-4a81-8a4a-9be5a8a888bd
updated: 1484309369
title: Pentagrammic-order 600-cell honeycomb
categories:
    - Geometry stubs
---
In the geometry of hyperbolic 4-space, the pentagrammic-order 600-cell honeycomb is one of four regular star-honeycombs. With Schläfli symbol {3,3,5,5/2}, it has five 600-cells around each face in a pentagrammic arrangement. It is dual to the small stellated 120-cell honeycomb. It can be considered the higher-dimensional analogue of the 4-dimensional icosahedral 120-cell and the 3-dimensional great dodecahedron. It is related to the order-5 icosahedral 120-cell honeycomb and great 120-cell honeycomb: the icosahedral 120-cells and great 120-cells in each honeycomb are replaced by the 600-cells that are their convex hulls, thus forming the pentagrammic-order 600-cell honeycomb.
