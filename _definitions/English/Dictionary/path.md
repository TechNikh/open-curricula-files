---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/path
offline_file: ""
offline_thumbnail: ""
uuid: fbf1b28a-ad31-4893-8aa3-28d421fc409f
updated: 1484310323
title: path
categories:
    - Dictionary
---
path
     n 1: a course of conduct; "the path of virtue"; "we went our
          separate ways"; "our paths in life led us apart";
          "genius usually follows a revolutionary path" [syn: {way},
           {way of life}]
     2: a way especially designed for a particular use
     3: an established line of travel or access [syn: {route}, {itinerary}]
     4: a line or route along which something travels or moves; "the
        hurricane demolished houses in its path"; "the track of an
        animal"; "the course of the river" [syn: {track}, {course}]
