---
version: 1
type: video
provider: YouTube
id: uxqTvfbTi4k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ce826e89-008e-42ce-939f-5d8780e8ff43
updated: 1486069637
title: 'Light refraction on plan parallel glass - physics experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/uxqTvfbTi4k/default.jpg
    - https://i3.ytimg.com/vi/uxqTvfbTi4k/1.jpg
    - https://i3.ytimg.com/vi/uxqTvfbTi4k/2.jpg
    - https://i3.ytimg.com/vi/uxqTvfbTi4k/3.jpg
---
