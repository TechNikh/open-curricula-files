---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Large_set_(combinatorics)
offline_file: ""
offline_thumbnail: ""
uuid: 219dab8b-74b6-40bf-b4bc-e9a7f44965fd
updated: 1484308573
title: Large set (combinatorics)
tags:
    - Examples
    - Properties
    - Open problems involving large sets
    - Notes
categories:
    - Combinatorics
---
is one such that the infinite sum of the reciprocals
