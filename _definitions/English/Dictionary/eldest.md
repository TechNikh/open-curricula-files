---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eldest
offline_file: ""
offline_thumbnail: ""
uuid: aa960677-cd7d-4c7b-aff6-21001525d194
updated: 1484310473
title: eldest
categories:
    - Dictionary
---
eldest
     adj : first in order of birth; "the firstborn child" [syn: {firstborn}]
     n : the offspring who came first in the order of birth [syn: {firstborn}]
