---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Summer_School_Marktoberdorf
offline_file: ""
offline_thumbnail: ""
uuid: 3ba95644-f5d9-4d03-ac79-b0880d8c7beb
updated: 1484308683
title: Summer School Marktoberdorf
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Manfred_Broy_2004_1.jpeg
tags:
    - Status
    - Directors
categories:
    - Theoretical computer science
---
The International Summer School Marktoberdorf is an annual two-week summer school for international computer science and mathematics postgraduate students and other young researchers, held annually since 1970 in Marktoberdorf, near Munich in southern Germany.[1] Students are accommodated in the boarding house of a local high school, Gymnasium Marktoberdorf.[2] Proceedings are published when appropriate.[3]
