---
version: 1
type: video
provider: YouTube
id: SUdAEGXLO-8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Reproductive%20System%2C%20part%203%20-%20Sex%20%26%20Fertilization-%20Crash%20Course%20A%26P%20%2342.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e41883bb-8c23-4c15-9cd9-92646891f7f3
updated: 1486069680
title: 'Reproductive System, part 3 - Sex & Fertilization- Crash Course A&P #42'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/SUdAEGXLO-8/default.jpg
    - https://i3.ytimg.com/vi/SUdAEGXLO-8/1.jpg
    - https://i3.ytimg.com/vi/SUdAEGXLO-8/2.jpg
    - https://i3.ytimg.com/vi/SUdAEGXLO-8/3.jpg
---
