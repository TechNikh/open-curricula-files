---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sport
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484412061
title: sport
categories:
    - Dictionary
---
sport
     n 1: an active diversion requiring physical exertion and
          competition [syn: {athletics}]
     2: the occupation of athletes who compete for pay
     3: someone who engages in sports [syn: {sportsman}, {sportswoman}]
     4: (biology) an organism that has characteristics resulting
        from chromosomal alteration [syn: {mutant}, {mutation}, {variation}]
     5: (Maine colloquial) temporary summer resident of inland Maine
     6: verbal wit (often at another's expense but not to be taken
        seriously); "he became a figure of fun" [syn: {fun}, {play}]
     v 1: wear or display in an ostentatious or proud manner; "she was
          sporting a new hat" [syn: ...
