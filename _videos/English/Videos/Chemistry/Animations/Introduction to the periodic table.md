---
version: 1
type: video
provider: YouTube
id: 8migjQOY3hM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Introduction%20to%20the%20periodic%20table.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3edc22d0-4c11-4570-8faf-d6f118324e6c
updated: 1486069661
title: Introduction to the periodic table
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/8migjQOY3hM/default.jpg
    - https://i3.ytimg.com/vi/8migjQOY3hM/1.jpg
    - https://i3.ytimg.com/vi/8migjQOY3hM/2.jpg
    - https://i3.ytimg.com/vi/8migjQOY3hM/3.jpg
---
