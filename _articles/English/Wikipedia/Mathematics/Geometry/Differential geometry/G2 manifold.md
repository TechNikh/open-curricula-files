---
version: 1
type: article
id: https://en.wikipedia.org/wiki/G2_manifold
offline_file: ""
offline_thumbnail: ""
uuid: c60ee6ac-cb6c-4258-b638-f8ead0cb907b
updated: 1484309221
title: G2 manifold
tags:
    - Properties
    - History
    - Connections to physics
categories:
    - Differential geometry
---
In differential geometry, a G2 manifold is a seven-dimensional Riemannian manifold with holonomy group contained in G2. The group 
  
    
      
        
          G
          
            2
          
        
      
    
    {\displaystyle G_{2}}
  
 is one of the five exceptional simple Lie groups. It can be described as the automorphism group of the octonions, or equivalently, as a proper subgroup of special orthogonal group SO(7) that preserves a spinor in the eight-dimensional spinor representation or lastly as the subgroup of the general linear group GL(7) which preserves the non-degenerate 3-form 
  
    
      
        ϕ
      
    
    {\displaystyle \phi }
  
, the associative ...
