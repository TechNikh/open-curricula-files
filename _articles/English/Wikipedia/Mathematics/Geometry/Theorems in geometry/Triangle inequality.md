---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Triangle_inequality
offline_file: ""
offline_thumbnail: ""
uuid: 0200fdb7-14c0-4401-ac82-6dbf899ae86b
updated: 1484309127
title: Triangle inequality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-TriangleInequality.svg.png
tags:
    - Euclidean geometry
    - >
        Mathematical expression of the constraint on the sides of a
        triangle
    - Right triangle
    - >
        Some practical examples of the use of the triangle
        inequality
    - Generalization of the inequality to any polygon
    - >
        Example of the generalized polygon inequality for a
        quadrilateral
    - Relationship with shortest paths
    - Converse
    - Generalization of the inequality to higher dimensions
    - Normed vector space
    - Example norms
    - Metric space
    - Reverse triangle inequality
    - Reversal in Minkowski space
    - Notes
categories:
    - Theorems in geometry
---
In mathematics, the triangle inequality states that for any triangle, the sum of the lengths of any two sides must be greater than or equal to the length of the remaining side.[1][2] If x, y, and z are the lengths of the sides of the triangle, with no side being greater than z, then the triangle inequality states that
