---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incite
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484635441
title: incite
categories:
    - Dictionary
---
incite
     v 1: give an incentive for action; "This moved me to sacrifice my
          career" [syn: {motivate}, {actuate}, {propel}, {move}, {prompt}]
     2: provoke or stir up; "incite a riot"; "set off great unrest
        among the people" [syn: {instigate}, {set off}, {stir up}]
     3: urge on; cause to act; "They other children egged the boy
        on, but he did not want to throw the stone through the
        window" [syn: {prod}, {egg on}]
