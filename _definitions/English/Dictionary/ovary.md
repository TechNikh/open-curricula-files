---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ovary
offline_file: ""
offline_thumbnail: ""
uuid: b3c08428-1c83-40e2-8e1d-8d2dd8c92f9a
updated: 1484310162
title: ovary
categories:
    - Dictionary
---
ovary
     n 1: the organ that bears the ovules of a flower
     2: (vertebrates) one of usually two organs that produce ova and
        secrete estrogen and progesterone
