---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Catenary
offline_file: ""
offline_thumbnail: ""
uuid: 44959416-cc5b-4ba2-9c8b-c6c7f38ec382
updated: 1484309160
title: Catenary
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Kette_Kettenkurve_Catenary_2008_PD.JPG
tags:
    - History
    - Inverted catenary arch
    - Catenary bridges
    - Anchoring of marine objects
    - Mathematical description
    - Equation
    - Relation to other curves
    - Geometrical properties
    - Science
    - Analysis
    - Model of chains and arches
    - Derivation of equations for the curve
    - Alternative derivation
    - Determining parameters
    - Generalizations with vertical force
    - Nonuniform chains
    - Suspension bridge curve
    - Catenary of equal strength
    - Elastic catenary
    - Other generalizations
    - Chain under a general force
    - Notes
    - Bibliography
categories:
    - Analytic geometry
---
In physics and geometry, a catenary (US /ˈkætənˌɛri/, UK /kəˈtiːnəri/) is the curve that an idealized hanging chain or cable assumes under its own weight when supported only at its ends. The curve has a U-like shape, superficially similar in appearance to a parabola, but it is not a parabola: it is a (scaled, rotated) graph of the hyperbolic cosine. The curve appears in the design of certain types of arches and as a cross section of the catenoid—the shape assumed by a soap film bounded by two parallel circular rings.
