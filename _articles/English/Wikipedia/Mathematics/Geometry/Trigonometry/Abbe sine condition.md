---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abbe_sine_condition
offline_file: ""
offline_thumbnail: ""
uuid: b501a058-cca5-4cd0-b840-a52ed81b95ec
updated: 1484309338
title: Abbe sine condition
categories:
    - Trigonometry
---
The Abbe sine condition is a condition that must be fulfilled by a lens or other optical system in order for it to produce sharp images of off-axis as well as on-axis objects. It was formulated by Ernst Abbe in the context of microscopes.
