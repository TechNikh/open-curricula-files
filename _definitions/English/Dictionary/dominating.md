---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dominating
offline_file: ""
offline_thumbnail: ""
uuid: 6216e90b-78dc-449a-b80d-ba13883657d0
updated: 1484310299
title: dominating
categories:
    - Dictionary
---
dominating
     adj 1: most powerful or important or influential; "the economically
            ascendant class"; "D-day is considered the dominating
            event of the war in Europe" [syn: {ascendant}, {ascendent}]
     2: used of a height or viewpoint; "a commanding view of the
        ocean"; "looked up at the castle dominating the
        countryside"; "the balcony overlooking the ballroom" [syn:
         {commanding}, {overlooking}]
     3: offensively self-assured or given to exercising usually
        unwarranted power; "an autocratic person"; "autocratic
        behavior"; "a bossy way of ordering others around"; "a
        rather aggressive and dominating character"; "managed ...
