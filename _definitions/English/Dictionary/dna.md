---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dna
offline_file: ""
offline_thumbnail: ""
uuid: e5663cd2-2244-4ea5-b48c-9733914802f8
updated: 1484310297
title: dna
categories:
    - Dictionary
---
DNA
     n : (biochemistry) a long linear polymer found in the nucleus of
         a cell and formed from nucleotides and shaped like a
         double helix; associated with the transmission of genetic
         information; "DNA is the king of molecules" [syn: {deoxyribonucleic
         acid}, {desoxyribonucleic acid}]
