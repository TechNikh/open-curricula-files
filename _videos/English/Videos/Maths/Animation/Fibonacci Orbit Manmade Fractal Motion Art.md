---
version: 1
type: video
provider: YouTube
id: jS0ag9EKmVc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d473d79b-7610-4e1c-b8bf-3113c636c0ac
updated: 1486069645
title: Fibonacci Orbit Manmade Fractal Motion Art
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/jS0ag9EKmVc/default.jpg
    - https://i3.ytimg.com/vi/jS0ag9EKmVc/1.jpg
    - https://i3.ytimg.com/vi/jS0ag9EKmVc/2.jpg
    - https://i3.ytimg.com/vi/jS0ag9EKmVc/3.jpg
---
