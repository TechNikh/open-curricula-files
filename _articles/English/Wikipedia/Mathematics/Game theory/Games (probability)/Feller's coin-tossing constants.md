---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Feller%27s_coin-tossing_constants'
offline_file: ""
offline_thumbnail: ""
uuid: ec91432f-675d-4d54-b694-00b5d52e0093
updated: 1484309040
title: "Feller's coin-tossing constants"
tags:
    - Values of the constants
    - Example
categories:
    - Games (probability)
---
Feller's coin-tossing constants are a set of numerical constants which describe asymptotic probabilities that in n independent tosses of a fair coin, no run of k consecutive heads (or, equally, tails) appears.
