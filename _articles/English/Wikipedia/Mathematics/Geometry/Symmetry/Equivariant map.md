---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equivariant_map
offline_file: ""
offline_thumbnail: ""
uuid: 21ef01ca-9c54-481a-89c0-f25c138a5360
updated: 1484309307
title: Equivariant map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Triangle.Centroid.svg.png
tags:
    - Examples
    - Elementary geometry
    - Statistics
    - Representation theory
    - Formalization
    - Generalization
categories:
    - Symmetry
---
In mathematics, equivariance is a form of symmetry for functions from one symmetric space to another. A function is said to be an equivariant map when its domain and codomain are acted on by the same symmetry group, and when the function commutes with the action of the group. That is, applying a symmetry transformation and then computing the function produces the same result as computing the function and then applying the transformation.
