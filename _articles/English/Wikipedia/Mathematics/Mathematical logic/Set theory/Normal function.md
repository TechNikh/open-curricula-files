---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Normal_function
offline_file: ""
offline_thumbnail: ""
uuid: da2c663a-3141-4c8e-9705-8fa92112c2f8
updated: 1484309509
title: Normal function
tags:
    - Examples
    - Properties
    - Notes
categories:
    - Set theory
---
In axiomatic set theory, a function f : Ord → Ord is called normal (or a normal function) iff it is continuous (with respect to the order topology) and strictly monotonically increasing. This is equivalent to the following two conditions:
