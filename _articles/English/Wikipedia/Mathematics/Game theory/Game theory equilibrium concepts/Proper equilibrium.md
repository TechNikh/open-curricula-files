---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proper_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: c2435615-9597-43ab-aad6-4d0625fc2801
updated: 1484309029
title: Proper equilibrium
tags:
    - Definition
    - Example
    - Proper equilibria of extensive games
categories:
    - Game theory equilibrium concepts
---
Proper equilibrium is a refinement of Nash Equilibrium due to Roger B. Myerson. Proper equilibrium further refines Reinhard Selten's notion of a trembling hand perfect equilibrium by assuming that more costly trembles are made with significantly smaller probability than less costly ones.
