---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oskar_Bolza
offline_file: ""
offline_thumbnail: ""
uuid: 3f577299-e6c8-4c0b-886d-8f53ffde5810
updated: 1484309071
title: Oskar Bolza
tags:
    - life
    - Academic career
    - Work
    - Research activity
    - Teaching activity
    - Selected publications
    - Notes
    - Biographical references
categories:
    - Geometers
---
Oskar Bolza (12 May 1857 – 5 July 1942) was a German mathematician, and student of Felix Klein. He was born in Bad Bergzabern, Palatinate, then a district of Bavaria, known for his research in the calculus of variations, particularly influenced by Karl Weierstrass' 1879 lectures on the subject.[2]
