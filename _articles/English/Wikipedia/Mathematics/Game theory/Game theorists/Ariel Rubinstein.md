---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ariel_Rubinstein
offline_file: ""
offline_thumbnail: ""
uuid: 60d3943f-9524-4621-87d5-c9c88da262d7
updated: 1484309025
title: Ariel Rubinstein
tags:
    - Biography
    - Honours and awards
    - Published works
categories:
    - Game theorists
---
Ariel Rubinstein (Hebrew: אריאל רובינשטיין) (born April 13, 1951) is an Israeli economist who works in Economic Theory, Game Theory and Bounded Rationality .
