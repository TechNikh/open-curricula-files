---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Howard_Eves
offline_file: ""
offline_thumbnail: ""
uuid: 5e8a23d6-8dc4-4637-a314-923992d3384f
updated: 1484309080
title: Howard Eves
categories:
    - Geometers
---
Howard Whitley Eves (10 January 1911, New Jersey – 6 June 2004) was an American mathematician, known for his work in geometry and the history of mathematics.
