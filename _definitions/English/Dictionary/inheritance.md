---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inheritance
offline_file: ""
offline_thumbnail: ""
uuid: a9566af1-7eba-45d5-a90d-fcaa78d470d8
updated: 1484310303
title: inheritance
categories:
    - Dictionary
---
inheritance
     n 1: hereditary succession to a title or an office or property
          [syn: {heritage}]
     2: that which is inherited; a title or property or estate that
        passes by law to the heir on the death of the owner [syn:
        {heritage}]
     3: (genetics) attributes acquired via biological heredity from
        the parents [syn: {hereditary pattern}]
     4: any attribute or immaterial possession that is inherited
        from ancestors; "my only inheritance was my mother's
        blessing"; "the world's heritage of knowledge" [syn: {heritage}]
