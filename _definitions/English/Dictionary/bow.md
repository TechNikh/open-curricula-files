---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bow
offline_file: ""
offline_thumbnail: ""
uuid: 771686c1-5b6b-4c94-8c32-bb6a0de39da3
updated: 1484310186
title: bow
categories:
    - Dictionary
---
bow
     n 1: a knot with two loops and loose ends; used to tie shoelaces
          [syn: {bowknot}]
     2: a slightly curved piece of resilient wood with taut
        horsehair strands, used in playing certain stringed
        instrument
     3: front part of a vessel or aircraft; "he pointed the bow of
        the boat toward the finish line" [syn: {fore}, {prow}, {stem}]
     4: curved piece of resilient wood with taut cord to propel
        arrows
     5: something curved in shape [syn: {arc}]
     6: bending the head or body or knee as a sign of reverence or
        submission or shame [syn: {bowing}, {obeisance}]
     7: an appearance by actors or performers at the end of the
        ...
