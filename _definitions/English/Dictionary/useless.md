---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/useless
offline_file: ""
offline_thumbnail: ""
uuid: c50fb473-43fe-482b-a6c4-64960f3933d6
updated: 1484310242
title: useless
categories:
    - Dictionary
---
useless
     adj 1: having no beneficial use or incapable of functioning
            usefully; "a kitchen full of useless gadgets"; "she is
            useless in an emergency" [ant: {useful}]
     2: not useful; "a curiously unhelpful and useless manual"
