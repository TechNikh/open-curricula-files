---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/esp
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484643301
title: esp
categories:
    - Dictionary
---
E.S.P.
     n : apparent power to perceive things that are not present to
         the senses [syn: {clairvoyance}, {second sight}, {extrasensory
         perception}, {ESP}]
