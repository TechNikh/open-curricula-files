---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hypothesis
offline_file: ""
offline_thumbnail: ""
uuid: 9e058519-6952-4aaf-8324-a2628d0dff82
updated: 1484310303
title: hypothesis
categories:
    - Dictionary
---
hypothesis
     n 1: a proposal intended to explain certain facts or observations
     2: a tentative theory about the natural world; a concept that
        is not yet verified but that if true would explain certain
        facts or phenomena; "a scientific hypothesis that survives
        experimental testing becomes a scientific theory"; "he
        proposed a fresh theory of alkalis that later was accepted
        in chemical practices" [syn: {possibility}, {theory}]
     3: a message expressing an opinion based on incomplete evidence
        [syn: {guess}, {conjecture}, {supposition}, {surmise}, {surmisal},
         {speculation}]
     [also: {hypotheses} (pl)]
