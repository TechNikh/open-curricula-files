---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalized_map
offline_file: ""
offline_thumbnail: ""
uuid: 9d5db7ce-f057-4cae-b1fe-cb87613a6d96
updated: 1484309481
title: Generalized map
categories:
    - Topological graph theory
---
In mathematics, a generalized map is a topological model which allows one to represent and to handle subdivided objects. This model was defined starting from combinatorial maps in order to represent non-orientable and open subdivisions, which is not possible with combinatorial maps. The main advantage of generalized map is the homogeneity of one-to-one mappings in any dimensions, which simplifies definitions and algorithms comparing to combinatorial maps. For this reason, generalized maps are sometimes used instead of combinatorial maps, even to represent orientable closed partitions.
