---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Common_knowledge_(logic)
offline_file: ""
offline_thumbnail: ""
uuid: 52ea2fca-c7fe-4c6c-a5ef-095de2d105be
updated: 1484308782
title: Common knowledge (logic)
tags:
    - Example
    - Puzzle
    - Solution
    - Proof
    - Formalization
    - Modal logic (syntactic characterization)
    - Set theoretic (semantic characterization)
    - Applications
    - Notes
categories:
    - Fixed points (mathematics)
---
Common knowledge is a special kind of knowledge for a group of agents. There is common knowledge of p in a group of agents G when all the agents in G know p, they all know that they know p, they all know that they all know that they know p, and so on ad infinitum.[1]
