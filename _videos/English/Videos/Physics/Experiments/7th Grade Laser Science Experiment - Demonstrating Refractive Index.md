---
version: 1
type: video
provider: YouTube
id: j0yLHAfI9IQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9116938f-bd66-40bc-bc55-961b26eb25c9
updated: 1486069634
title: '7th Grade Laser Science Experiment - Demonstrating Refractive Index'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/j0yLHAfI9IQ/default.jpg
    - https://i3.ytimg.com/vi/j0yLHAfI9IQ/1.jpg
    - https://i3.ytimg.com/vi/j0yLHAfI9IQ/2.jpg
    - https://i3.ytimg.com/vi/j0yLHAfI9IQ/3.jpg
---
