---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/searching
offline_file: ""
offline_thumbnail: ""
uuid: 119473ec-0d31-4372-a1be-87391ff59cee
updated: 1484310486
title: searching
categories:
    - Dictionary
---
searching
     adj 1: diligent and thorough in inquiry or investigation; "a
            probing inquiry"; "a searching investigation of their
            past dealings" [syn: {inquisitory}, {probing}]
     2: having keenness and forcefulness and penetration in thought,
        expression, or intellect; "searching insights"; "trenchant
        criticism" [syn: {trenchant}]
     3: exploring thoroughly
