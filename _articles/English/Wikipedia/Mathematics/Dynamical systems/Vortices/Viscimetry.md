---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Viscimetry
offline_file: ""
offline_thumbnail: ""
uuid: 945c1dfc-c15a-4cc3-949b-19eb3f65f89f
updated: 1484308879
title: Viscimetry
categories:
    - Vortices
---
Viscimation is the turbulence when liquids of different viscosities mix, particularly the formation of vortices (called "viscimetric whorls") and visible separate threads of the different liquids.
