---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revenge
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484579581
title: revenge
categories:
    - Dictionary
---
revenge
     n : action taken in return for an injury or offense [syn: {retaliation}]
     v : take revenge for a perceived wrong; "He wants to avenge the
         murder of his brother" [syn: {avenge}, {retaliate}]
