---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Completeness_of_atomic_initial_sequents
offline_file: ""
offline_thumbnail: ""
uuid: fa0e411b-168f-4353-a794-265f1d8ddee3
updated: 1484308992
title: Completeness of atomic initial sequents
categories:
    - Theorems in the foundations of mathematics
---
In sequent calculus, the completeness of atomic initial sequents states that initial sequents A ⊢ A (where A is an arbitrary formula) can be derived from only atomic initial sequents p ⊢ p (where p is an atomic formula). This theorem plays a role analogous to eta expansion in lambda calculus, and dual to cut-elimination and beta reduction. Typically it can be established by induction on the structure of A, much more easily than cut-elimination.
