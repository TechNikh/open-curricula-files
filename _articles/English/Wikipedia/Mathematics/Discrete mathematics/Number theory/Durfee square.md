---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Durfee_square
offline_file: ""
offline_thumbnail: ""
uuid: 7ce8e27a-c822-4892-89e0-0b364bc8ff51
updated: 1484308639
title: Durfee square
tags:
    - Examples
    - History
    - Properties
categories:
    - Number theory
---
In number theory, a Durfee square is an attribute of an integer partition. A partition of n has a Durfee square of side s if s is the largest number such that the partition contains at least s parts with values ≥ s.[1] An equivalent, but more visual, definition is that the Durfee square is the largest square that is contained within a partition's Ferrers diagram.[2] The side-length of the Durfee square is known as the rank of the partition.[3]
