---
version: 1
type: article
id: https://en.wikipedia.org/wiki/GXL
offline_file: ""
offline_thumbnail: ""
uuid: f3ecfc04-18c7-4d14-b3e0-44a6224f7038
updated: 1484309409
title: GXL
tags:
    - Overview
    - Presentations of former GXL versions
    - GXL Partners
categories:
    - Graph description languages
---
GXL (Graph eXchange Language) is designed to be a standard exchange format for graphs. GXL is an extensible markup language (XML) sublanguage and the syntax is given by an XML document type definition (DTD). This exchange format offers an adaptable and flexible means to support interoperability between graph-based tools.
