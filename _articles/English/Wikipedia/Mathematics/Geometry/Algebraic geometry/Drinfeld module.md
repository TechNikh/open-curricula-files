---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Drinfeld_module
offline_file: ""
offline_thumbnail: ""
uuid: c0f44589-4de2-49fe-b222-452e1afae9ec
updated: 1484309142
title: Drinfeld module
tags:
    - Drinfeld modules
    - The ring of additive polynomials
    - Definition of Drinfeld modules
    - Examples of Drinfeld modules
    - Shtukas
    - Applications
    - Drinfeld modules
    - Shtukas
categories:
    - Algebraic geometry
---
In mathematics, a Drinfeld module (or elliptic module) is roughly a special kind of module over a ring of functions on a curve over a finite field, generalizing the Carlitz module. Loosely speaking, they provide a function field analogue of complex multiplication theory. A shtuka (also called F-sheaf or chtouca) is a sort of generalization of a Drinfeld module, consisting roughly of a vector bundle over a curve, together with some extra structure identifying a "Frobenius twist" of the bundle with a "modification" of it.
