---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mechanically_isolated_system
uuid: 6403ee29-8abb-4271-ad53-d2b3bcc60f12
updated: 1480455138
title: Mechanically isolated system
categories:
    - Thermodynamics
---
In thermodynamics, a mechanically isolated system is a system that is mechanically constraint to disallow deformations, so that it cannot perform any work on its environment. It may however, exchange heat across the system boundary.
