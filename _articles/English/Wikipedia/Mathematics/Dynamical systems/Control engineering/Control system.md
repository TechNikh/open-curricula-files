---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Control_system
offline_file: ""
offline_thumbnail: ""
uuid: 334b823b-a25f-4352-8df6-b21f5633aeab
updated: 1484308739
title: Control system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Amerongen_hydrocentrale01_0.jpg
tags:
    - Overview
    - Open-loop and closed-loop control
    - Logic control
    - On–off control
    - Linear control
    - Proportional control
    - Under-damped furnace example
    - Over-damped furnace example
    - PID control
    - Derivative action
    - Integral action
    - 'Ramp UP % per minute'
    - Other techniques
    - Fuzzy logic
    - Physical implementations
categories:
    - Control engineering
---
A control system is a device, or set of devices, that manages, commands, directs or regulates the behaviour of other devices or systems. They can range from a home heating controller using a thermostat controlling a boiler to large Industrial control systems which are used for controlling processes or machines.
