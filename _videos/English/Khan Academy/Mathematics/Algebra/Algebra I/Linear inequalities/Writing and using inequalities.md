---
version: 1
type: video
provider: YouTube
id: RHe9X2HDEjA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20inequalities/Writing%20and%20using%20inequalities.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fb3facd9-5512-48b9-b71c-1b8ae0fd8621
updated: 1486069565
title: Writing and using inequalities
categories:
    - Linear inequalities
thumbnail_urls:
    - https://i3.ytimg.com/vi/RHe9X2HDEjA/default.jpg
    - https://i3.ytimg.com/vi/RHe9X2HDEjA/1.jpg
    - https://i3.ytimg.com/vi/RHe9X2HDEjA/2.jpg
    - https://i3.ytimg.com/vi/RHe9X2HDEjA/3.jpg
---
