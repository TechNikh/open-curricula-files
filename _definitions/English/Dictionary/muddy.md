---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/muddy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484645341
title: muddy
categories:
    - Dictionary
---
muddy
     adj 1: (of soil) soft and watery; "the ground was boggy under
            foot"; "a marshy coastline"; "miry roads"; "wet mucky
            lowland"; "muddy barnyard"; "quaggy terrain"; "the
            sloughy edge of the pond"; "swampy bayous" [syn: {boggy},
             {marshy}, {miry}, {mucky}, {quaggy}, {sloughy}, {swampy}]
     2: dirty and messy; covered with mud or muck; "muddy boots"; "a
        mucky stable" [syn: {mucky}]
     3: (of color) discolored by impurities; not bright and clear;
        "dirty" is often used in combination; "a dirty (or dingy)
        white"; "the muddied gray of the sea"; "muddy colors";
        "dirty-green walls"; "dirty-blonde hair" [syn: ...
