---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Normal_number
offline_file: ""
offline_thumbnail: ""
uuid: fc21c09b-9662-4f5f-aed6-ed5d82e3cd2b
updated: 1484308652
title: Normal number
tags:
    - Definitions
    - Properties and examples
    - Non-normal numbers
    - Properties
    - Connection to finite-state machines
    - Connection to equidistributed sequences
    - Notes
categories:
    - Number theory
---
In mathematics, a normal number is a real number whose infinite sequence of digits in every base b[1] is distributed uniformly in the sense that each of the b digit values has the same natural density 1/b, also all possible b2 pairs of digits are equally likely with density b−2, all b3 triplets of digits equally likely with density b−3, etc.
