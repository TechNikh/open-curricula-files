---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vortex_ring
offline_file: ""
offline_thumbnail: ""
uuid: 8c072245-58c8-4af2-a2e4-8eb15a649324
updated: 1484308878
title: Vortex ring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Vortex_Ring_Gun_Schlierin.jpg
tags:
    - Structure
    - Formation
    - Other examples
    - Vortex ring state in helicopters
    - Vortex rings in the human heart
    - Bubble rings
    - Theory
    - Historical studies
    - Spherical vortices
    - Instabilities
categories:
    - Vortices
---
A vortex ring, also called a toroidal vortex, is a torus-shaped vortex in a fluid or gas; that is, a region where the fluid mostly spins around an imaginary axis line that forms a closed loop. The dominant flow in a vortex ring is said to be toroidal, more precisely poloidal.
