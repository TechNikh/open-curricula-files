---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flour
offline_file: ""
offline_thumbnail: ""
uuid: c4c696a7-c790-46d4-aa51-7fbdbac4f09f
updated: 1484310339
title: flour
categories:
    - Dictionary
---
flour
     n : fine powdery foodstuff obtained by grinding and sifting the
         meal of a cereal grain
     v 1: cover with flour; "flour fish or meat before frying it"
     2: convert grain into flour
