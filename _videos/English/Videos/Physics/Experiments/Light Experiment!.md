---
version: 1
type: video
provider: YouTube
id: BOeRRHxuKrI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ecbaadae-fae7-43d7-a92e-d85b9036a7a3
updated: 1486069639
title: Light Experiment!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/BOeRRHxuKrI/default.jpg
    - https://i3.ytimg.com/vi/BOeRRHxuKrI/1.jpg
    - https://i3.ytimg.com/vi/BOeRRHxuKrI/2.jpg
    - https://i3.ytimg.com/vi/BOeRRHxuKrI/3.jpg
---
