---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/micelle
offline_file: ""
offline_thumbnail: ""
uuid: fd657734-f488-40e3-bc99-7df92fc9eee6
updated: 1484310426
title: micelle
categories:
    - Dictionary
---
micelle
     n : an electrically charged particle built up from polymeric
         molecules or ions and occurring in certain colloidal
         electrolytic solutions like soaps and detergents
