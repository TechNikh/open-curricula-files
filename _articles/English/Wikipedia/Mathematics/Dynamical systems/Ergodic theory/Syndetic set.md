---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Syndetic_set
offline_file: ""
offline_thumbnail: ""
uuid: 6d3ecae1-d7e7-4ddf-8f7e-f32c513333d0
updated: 1484308775
title: Syndetic set
categories:
    - Ergodic theory
---
In mathematics, a syndetic set is a subset of the natural numbers, having the property of "bounded gaps": that the sizes of the gaps in the sequence of natural numbers is bounded.
