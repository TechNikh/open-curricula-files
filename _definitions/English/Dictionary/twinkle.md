---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/twinkle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484453221
title: twinkle
categories:
    - Dictionary
---
twinkle
     n : a rapid change in brightness; a brief spark or flash [syn: {scintillation},
          {sparkling}]
     v 1: gleam or glow intermittently; "The lights were flashing"
          [syn: {flash}, {blink}, {wink}, {winkle}]
     2: emit or reflect light in a flickering manner; "Does a
        constellation twinkle more brightly than a single star?"
        [syn: {winkle}, {scintillate}]
