---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wave_Motion_(journal)
offline_file: ""
offline_thumbnail: ""
uuid: b8b73fd7-6ad0-4117-8950-8608edf750f6
updated: 1484308903
title: Wave Motion (journal)
categories:
    - Waves
---
Wave Motion is a peer-reviewed scientific journal publishing papers on the physics of waves – with emphasis on the areas of acoustics, optics, geophysics, seismology, electromagnetic theory, solid and fluid mechanics. Original research articles on analytical, numerical and experimental aspects of wave motion are covered.
