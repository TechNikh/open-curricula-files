---
version: 1
type: article
id: https://en.wikipedia.org/wiki/GADDAG
offline_file: ""
offline_thumbnail: ""
uuid: fb2941cd-69c3-46d8-be55-6ba8718e8352
updated: 1484309006
title: GADDAG
tags:
    - Description
    - Definition
    - Use in move generation
categories:
    - Game artificial intelligence
---
A GADDAG is a data structure presented by Steven Gordon in 1994, for use in generating moves for Scrabble and other word-generation games where such moves require words that "hook into" existing words. It is often in contrast to move-generation algorithms using a directed acyclic word graph (DAWG) such as the one used by Maven. It is generally twice as fast as the traditional DAWG algorithms, but take about 5 times as much space for regulation Scrabble dictionaries.[1]
