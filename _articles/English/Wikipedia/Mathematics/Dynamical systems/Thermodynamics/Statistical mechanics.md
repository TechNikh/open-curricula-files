---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Statistical_mechanics
offline_file: ""
offline_thumbnail: ""
uuid: f76437aa-0699-4f8b-a563-0191ea7208e3
updated: 1484308870
title: Statistical mechanics
tags:
    - 'Principles: mechanics and ensembles'
    - Statistical thermodynamics
    - Fundamental postulate
    - Three thermodynamic ensembles
    - Calculation methods
    - Exact
    - Monte Carlo
    - Other
    - Non-equilibrium statistical mechanics
    - Stochastic methods
    - Near-equilibrium methods
    - Hybrid methods
    - Applications outside thermodynamics
    - History
    - Notes
categories:
    - Thermodynamics
---
Statistical mechanics is a branch of theoretical physics that studies, using probability theory, the average behaviour of a mechanical system where the state of the system is uncertain.[1][2][3][note 1]
