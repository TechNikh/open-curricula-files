---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/halves
offline_file: ""
offline_thumbnail: ""
uuid: a3489fdd-b3d7-4c7c-81e3-60fa0ca82d5b
updated: 1484310344
title: halves
categories:
    - Dictionary
---
half
     adj 1: consisting of one of two equivalent parts in value or
            quantity; "a half chicken"; "lasted a half hour" [syn:
             {half(a)}]
     2: partial; "gave me a half smile"; "he did only a half job"
        [syn: {half(a)}]
     3: (of siblings) related through one parent only; "a half
        brother"; "half sister" [ant: {whole}]
     n 1: one of two equal parts of a divisible whole; "half a loaf";
          "half an hour"; "a century and one half" [syn: {one-half}]
     2: in various games or performances: either of two periods of
        play separated by an interval
     adv : partially or to the extent of a half; "he was half hidden by
           the ...
