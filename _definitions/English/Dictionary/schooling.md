---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/schooling
offline_file: ""
offline_thumbnail: ""
uuid: 58cbe5d1-a400-4401-932d-577809a059c5
updated: 1484310447
title: schooling
categories:
    - Dictionary
---
schooling
     n 1: the act of teaching at school
     2: the process of being formally educated at a school; "what
        will you do when you finish school?" [syn: {school}]
     3: the training of an animal (especially the training of a
        horse for dressage)
