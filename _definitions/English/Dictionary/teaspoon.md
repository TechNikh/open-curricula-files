---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/teaspoon
offline_file: ""
offline_thumbnail: ""
uuid: a0b1404d-ac0e-4d26-918a-12fcde1c313b
updated: 1484310333
title: teaspoon
categories:
    - Dictionary
---
teaspoon
     n 1: a small spoon used for stirring tea or coffee; holds about
          one fluid dram
     2: as much as a teaspoon will hold [syn: {teaspoonful}]
