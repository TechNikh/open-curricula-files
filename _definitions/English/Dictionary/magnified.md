---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnified
offline_file: ""
offline_thumbnail: ""
uuid: 8c88e1f2-0a9b-4757-96cd-f1a4d8055dd9
updated: 1484310208
title: magnified
categories:
    - Dictionary
---
magnified
     adj : enlarged to an abnormal degree; "thick lenses exaggerated
           the size of her eyes" [syn: {exaggerated}, {enlarged}]
