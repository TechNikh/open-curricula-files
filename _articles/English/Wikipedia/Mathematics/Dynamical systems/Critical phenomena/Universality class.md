---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Universality_class
offline_file: ""
offline_thumbnail: ""
uuid: 261407b8-ef69-4c0b-a011-1786d1afbeb4
updated: 1484308746
title: Universality class
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_5.png
categories:
    - Critical phenomena
---
In statistical mechanics, a universality class is a collection of mathematical models which share a single scale invariant limit under the process of renormalization group flow. While the models within a class may differ dramatically at finite scales, their behavior will become increasingly similar as the limit scale is approached. In particular, asymptotic phenomena such as critical exponents will be the same for all models in the class.
