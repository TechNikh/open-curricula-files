---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ideal_(set_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 3731e956-9a3e-42b5-8b57-9455d8545d96
updated: 1484309507
title: Ideal (set theory)
tags:
    - Terminology
    - Examples of ideals
    - General examples
    - Ideals on the natural numbers
    - Ideals on the real numbers
    - Ideals on other sets
    - Operations on ideals
    - Relationships among ideals
categories:
    - Set theory
---
In the mathematical field of set theory, an ideal is a collection of sets that are considered to be "small" or "negligible". Every subset of an element of the ideal must also be in the ideal (this codifies the idea that an ideal is a notion of smallness), and the union of any two elements of the ideal must also be in the ideal.
