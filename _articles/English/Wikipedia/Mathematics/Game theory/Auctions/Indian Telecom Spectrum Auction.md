---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Indian_Telecom_Spectrum_Auction
offline_file: ""
offline_thumbnail: ""
uuid: 4431685a-55b6-4b66-925b-fb6837b52579
updated: 1484309013
title: Indian Telecom Spectrum Auction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_India.svg.png
tags:
    - Overview
    - 1994 auction
    - 1995 auction
    - 1997 and 2000 auctions
    - 2001 auction
    - 2010 Spectrum Auction
    - 2012 Spectrum Auction
    - 2013 Spectrum Auction
    - 2014 Spectrum Auction
    - 2015 spectrum auction
    - 2016 auction
categories:
    - Auctions
---
In India, the Department of Telecommunications (DoT) conducts auctions of licenses for electromagnetic spectrum. India was among the early adopters of spectrum auctions beginning auctions in 1994.
