---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_production_game
offline_file: ""
offline_thumbnail: ""
uuid: 5de3ad84-5881-4f9a-b7af-c49c34d363cf
updated: 1484308528
title: Linear production game
categories:
    - Cooperative games
---
Linear Production Game (LP Game) is a N-person game in which the value of a coalition can be obtained by solving a Linear Programming problem. It is widely used in the context of resource allocation and payoff distribution. Mathematically, there are m types of resources and n products can be produced out of them. Product j requires 
  
    
      
        
          a
          
            k
          
          
            j
          
        
      
    
    {\displaystyle a_{k}^{j}}
  
 amount of the kth resource. The products can be sold at a given market price 
  
    
      
        
          
            
              c
              →
            
          
        
      
  ...
