---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/petition
offline_file: ""
offline_thumbnail: ""
uuid: 52833d38-c5b4-4279-aba1-f145babf60ab
updated: 1484310170
title: petition
categories:
    - Dictionary
---
petition
     n 1: a formal message requesting something that is submitted to
          an authority [syn: {request}, {postulation}]
     2: reverent petition to a deity [syn: {prayer}, {orison}]
     v : write a petition for something to somebody; request formally
         and in writing
