---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boxing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484653681
title: boxing
categories:
    - Dictionary
---
boxing
     n 1: fighting with the fists [syn: {pugilism}, {fisticuffs}]
     2: the enclosure of something in a package or box [syn: {packing}]
