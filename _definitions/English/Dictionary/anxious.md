---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anxious
offline_file: ""
offline_thumbnail: ""
uuid: a40f4f94-5309-4a67-a9be-9676c17af56e
updated: 1484310140
title: anxious
categories:
    - Dictionary
---
anxious
     adj 1: mentally upset over possible misfortune or danger etc;
            worried; "anxious parents"; "anxious about her job";
            "not used to a city and anxious about small things";
            "felt apprehensive about the consequences" [syn: {apprehensive}]
     2: eagerly desirous; "anxious to see the new show at the
        museum"; "dying to hear who won" [syn: {anxious(p)}, {dying(p)}]
     3: causing or fraught with or showing anxiety; "spent an
        anxious night waiting for the test results"; "cast anxious
        glances behind her"; "those nervous moments before
        takeoff"; "an unquiet mind" [syn: {nervous}, {uneasy}, {unquiet}]
