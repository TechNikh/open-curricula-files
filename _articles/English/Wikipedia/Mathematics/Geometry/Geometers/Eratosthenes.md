---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eratosthenes
offline_file: ""
offline_thumbnail: ""
uuid: 3d7a5b76-a2f7-42e4-8eee-db4afa7968e2
updated: 1484309078
title: Eratosthenes
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Eratosthenes_measure_of_Earth_circumference.svg.png
tags:
    - life
    - "Measurement of the Earth's circumference"
    - "Father of geography"
    - Achievements
    - Prime numbers
    - Works
    - Titles
    - Eponyms
categories:
    - Geometers
---
Eratosthenes of Cyrene (/ɛrəˈtɒsθəniːz/; Greek: Ἐρατοσθένης, IPA: [eratostʰénɛːs]; c. 276 BC[1] – c. 195/194 BC[2]) was a Greek mathematician, geographer, poet, astronomer, and music theorist. He was a man of learning, becoming the chief librarian at the Library of Alexandria. He invented the discipline of geography, including the terminology used today.[3]
