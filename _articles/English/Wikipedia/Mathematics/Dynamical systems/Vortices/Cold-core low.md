---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cold-core_low
offline_file: ""
offline_thumbnail: ""
uuid: 2821e2ab-ec87-430d-aad9-1a812cdc4700
updated: 1484308870
title: Cold-core low
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-TUTTcellWestPac2007071012WV.jpg
tags:
    - characteristics
    - >
        Importance to cyclones within the subtropics and
        mid-latitudes
    - Importance to tropical cyclones
categories:
    - Vortices
---
A cold-core low, also known as an upper level low or cold-core cyclone, is a cyclone aloft which has an associated cold pool of air residing at high altitude within the Earth's troposphere. It is a low pressure system which strengthens with height in accordance with the thermal wind relationship. These systems can be referred to as upper level lows. If a weak surface circulation forms in response to such a feature at subtropical latitudes of the eastern north Pacific or north Indian oceans, it is called a subtropical cyclone. Cloud cover and rainfall mainly occurs with these systems during the day. Severe weather, such as tornadoes, can occur near the center of cold-core lows. Cold lows can ...
