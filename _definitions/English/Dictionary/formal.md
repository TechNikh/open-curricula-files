---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formal
offline_file: ""
offline_thumbnail: ""
uuid: 65c8f859-1d3e-4637-be6c-0b3e442582c8
updated: 1484310244
title: formal
categories:
    - Dictionary
---
formal
     adj 1: being in accord with established forms and conventions and
            requirements (as e.g. of formal dress); "pay one's
            formal respects"; "formal dress"; "a formal ball";
            "the requirement was only formal and often ignored";
            "a formal education" [ant: {informal}]
     2: characteristic of or befitting a person in authority;
        "formal duties"; "an official banquet"
     3: (of spoken and written language) adhering to traditional
        standards of correctness and without casual, contracted,
        and colloquial forms; "the paper was written in formal
        English" [ant: {informal}]
     4: represented in simplified or ...
