---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/act
offline_file: ""
offline_thumbnail: ""
uuid: c5556de3-636e-4f04-a626-1e6afd111bb7
updated: 1484310335
title: act
categories:
    - Dictionary
---
act
     n 1: a legal document codifying the result of deliberations of a
          committee or society or legislative body [syn: {enactment}]
     2: something that people do or cause to happen [syn: {human
        action}, {human activity}]
     3: a subdivision of a play or opera or ballet
     4: a short theatrical performance that is part of a longer
        program; "he did his act three times every evening"; "she
        had a catchy little routine"; "it was one of the best
        numbers he ever did" [syn: {routine}, {number}, {turn}, {bit}]
     5: a manifestation of insincerity; "he put on quite an act for
        her benefit"
     v 1: perform an action, or work out or perform ...
