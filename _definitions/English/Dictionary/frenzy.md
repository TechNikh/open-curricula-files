---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frenzy
offline_file: ""
offline_thumbnail: ""
uuid: e33644ee-e5a7-48de-ae81-c8e10fc36ce9
updated: 1484310589
title: frenzy
categories:
    - Dictionary
---
frenzy
     n : state of violent mental agitation [syn: {craze}, {delirium},
          {fury}, {hysteria}]
     [also: {frenzied}]
