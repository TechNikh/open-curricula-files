---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/excited
offline_file: ""
offline_thumbnail: ""
uuid: f37b52af-1e58-4c19-a765-7354bcfc4e6a
updated: 1484310391
title: excited
categories:
    - Dictionary
---
excited
     adj 1: in an aroused state [ant: {unexcited}]
     2: of persons; excessively affected by emotion; "he would
        become emotional over nothing at all"; "she was worked up
        about all the noise" [syn: {aroused}, {emotional}, {worked
        up}]
     3: marked by uncontrolled excitement or emotion; "a crowd of
        delirious baseball fans"; "something frantic in their
        gaiety"; "a mad whirl of pleasure" [syn: {delirious}, {frantic},
         {mad}, {unrestrained}]
     4: of e.g. a molecule; made reactive or more reactive [syn: {activated}]
