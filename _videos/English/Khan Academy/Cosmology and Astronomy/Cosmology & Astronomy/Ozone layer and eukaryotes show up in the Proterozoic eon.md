---
version: 1
type: video
provider: YouTube
id: E1P79uFLCMc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Ozone%20layer%20and%20eukaryotes%20show%20up%20in%20the%20Proterozoic%20eon.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 377043b8-2b97-4ada-bfe2-d21f6c1776e9
updated: 1486069685
title: Ozone layer and eukaryotes show up in the Proterozoic eon
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/E1P79uFLCMc/default.jpg
    - https://i3.ytimg.com/vi/E1P79uFLCMc/1.jpg
    - https://i3.ytimg.com/vi/E1P79uFLCMc/2.jpg
    - https://i3.ytimg.com/vi/E1P79uFLCMc/3.jpg
---
