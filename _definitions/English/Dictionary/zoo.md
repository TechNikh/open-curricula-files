---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/zoo
offline_file: ""
offline_thumbnail: ""
uuid: ef8a7a32-605e-475f-94aa-7878390c2cad
updated: 1484310266
title: zoo
categories:
    - Dictionary
---
zoo
     n : the facility where wild animals are housed for exhibition
         [syn: {menagerie}, {zoological garden}]
