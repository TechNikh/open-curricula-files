---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conformal_geometric_algebra
offline_file: ""
offline_thumbnail: ""
uuid: a2e169b7-bea4-4cb0-ba34-3e6a615a2607
updated: 1484309175
title: Conformal geometric algebra
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Conformal_Embedding.svg.png
tags:
    - Construction of CGA
    - Notation and terminology
    - Base and representation spaces
    - Mapping between the base space and the representation space
    - Inverse mapping
    - Origin and point at infinity
    - Geometrical objects
    - As the solution of a pair of equations
    - As derived from points of the object
    - odds
    - g(x) . A = 0
    - Transformations
    - Notes
    - Bibliography
    - Books
    - Online resources
categories:
    - Computational geometry
---
In mathematics, with application in computational geometry, conformal geometric algebra (CGA) is the geometric algebra constructed over the resultant space of a projective map from an n-dimensional pseudo-Euclidean (including Euclidean) base space Ep,q into ℝp+1,q+1. This allows operations on the n-dimensional space, including rotations, translations and reflections to be represented using versors of the geometric algebra; and it is found that points, lines, planes, circles and spheres gain particularly natural and computationally amenable representations.
