---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plumule
offline_file: ""
offline_thumbnail: ""
uuid: 3cc4a291-b370-4437-a8e4-4ccfcfa6c430
updated: 1484310160
title: plumule
categories:
    - Dictionary
---
plumule
     n : down feather of young birds; persists in some adult birds
