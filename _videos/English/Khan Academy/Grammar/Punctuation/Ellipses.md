---
version: 1
type: video
provider: YouTube
id: JmRMfFVw6NE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/Ellipses.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b4662835-d1a4-4deb-b4db-222d30479341
updated: 1486069599
title: Ellipses
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Punctuation
thumbnail_urls:
    - https://i3.ytimg.com/vi/JmRMfFVw6NE/default.jpg
    - https://i3.ytimg.com/vi/JmRMfFVw6NE/1.jpg
    - https://i3.ytimg.com/vi/JmRMfFVw6NE/2.jpg
    - https://i3.ytimg.com/vi/JmRMfFVw6NE/3.jpg
---
Ellipses are three little dots that we use to show a pause or that a portion of a quote has been removed. Learn how to use them in this video.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/miscellaneous-punctuation/e/introduction-to-the-ellipsis

Missed the previous lesson? Watch here: https://www.khanacademy.org/miscellaneous-punctuation/v/hyphens

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
