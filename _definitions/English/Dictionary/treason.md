---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/treason
offline_file: ""
offline_thumbnail: ""
uuid: 3f79e23b-0072-4cbc-9fdc-bd6ef2779110
updated: 1484310587
title: treason
categories:
    - Dictionary
---
treason
     n 1: a crime that undermines the offender's government [syn: {high
          treason}, {lese majesty}]
     2: disloyalty by virtue of subversive behavior [syn: {subversiveness},
         {traitorousness}]
     3: an act of deliberate betrayal [syn: {treachery}, {betrayal},
         {perfidy}]
