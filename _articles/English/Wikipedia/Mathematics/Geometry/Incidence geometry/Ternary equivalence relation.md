---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ternary_equivalence_relation
offline_file: ""
offline_thumbnail: ""
uuid: a0a31040-f18d-43e6-abc8-2a2d3f4e558c
updated: 1484309255
title: Ternary equivalence relation
categories:
    - Incidence geometry
---
In mathematics, a ternary equivalence relation is a kind of ternary relation analogous to a binary equivalence relation. A ternary equivalence relation is symmetric, reflexive, and transitive. The classic example is the relation of collinearity among three points in Euclidean space. In an abstract set, a ternary equivalence relation determines a collection of equivalence classes or pencils that form a linear space in the sense of incidence geometry. In the same way, a binary equivalence relation on a set determines a partition.
