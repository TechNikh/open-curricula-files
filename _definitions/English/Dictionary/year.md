---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/year
offline_file: ""
offline_thumbnail: ""
uuid: fd804dcd-2406-4d68-b011-de231b973a80
updated: 1484310315
title: year
categories:
    - Dictionary
---
year
     n 1: a period of time containing 365 (or 366) days; "she is 4
          years old"; "in the year 1920" [syn: {twelvemonth}, {yr}]
     2: a period of time occupying a regular part of a calendar year
        that is used for some particular activity; "a school year"
     3: the period of time that it takes for a planet (as, e.g.,
        Earth or Mars) to make a complete revolution around the
        sun; "a Martian year takes 687 of our days"
     4: a body of students who graduate together; "the class of
        '97"; "she was in my year at Hoehandle High" [syn: {class}]
