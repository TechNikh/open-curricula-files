---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roll
offline_file: ""
offline_thumbnail: ""
uuid: 58f86eb1-3c1a-4a19-9cd5-4bb97a024ce7
updated: 1484310611
title: roll
categories:
    - Dictionary
---
roll
     n 1: rotary motion of an object around its own axis; "wheels in
          axial rotation" [syn: {axial rotation}, {axial motion}]
     2: a list of names; "his name was struck off the rolls" [syn: {roster}]
     3: a long heavy sea wave as it advances towards the shore [syn:
         {roller}, {rolling wave}]
     4: photographic film rolled up inside a container to protect it
        from light
     5: a round shape formed by a series of concentric circles [syn:
         {coil}, {whorl}, {curl}, {curlicue}, {ringlet}, {gyre}, {scroll}]
     6: a roll of currency notes (often taken as the resources of a
        person or business etc.); "he shot his roll on a
        bob-tailed ...
