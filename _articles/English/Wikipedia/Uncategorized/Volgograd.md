---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stalingrad
offline_file: ""
offline_thumbnail: ""
uuid: abffde1c-ed36-4997-a61d-56d642d5e6d6
updated: 1484308485
title: Volgograd
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gogolya_str.%252C_Tsaritsyn_%25281914%2529.jpg'
tags:
    - History
    - Tsaritsyn
    - Stalingrad
    - Volgograd
    - Terrorist attacks
    - Politics
    - Administrative and municipal status
    - Economy
    - Transportation
    - City name
categories:
    - Uncategorized
---
Volgograd (Russian: Волгогра́д; IPA: [vəlɡɐˈɡrat] ( listen)), formerly Tsaritsyn (Russian:  Цари́цын​ (help·info)), 1589–1925, and Stalingrad (Russian:  Сталингра́д​ (help·info)), 1925–1961, is an important industrial city and the administrative center of Volgograd Oblast, Russia. It is 80 kilometers (50 mi) long,[11] north to south and is situated on the western bank of the Volga River, after which the city was named. Population: 1,021,215 (2010 Census);[7] 1,011,417 (2002 Census);[12] 1,022,578 (1989 Census).[13]
