---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert
offline_file: ""
offline_thumbnail: ""
uuid: 10f239da-3e8f-4207-b693-105d8e68e5f6
updated: 1484308306
title: Robert
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Robert_magnificent_statue_in_falaise.JPG
tags:
    - 'Variations[4]'
    - trivia
    - Notable people
    - Fictional characters
    - In different languages
    - Surname
categories:
    - Uncategorized
---
The name Robert is a Germanic given name, from Proto-Germanic *χrōþi- "fame" and *berχta- "bright".[1] Compare Old Dutch Robrecht and Old High German Hrodebert (a compound of hruod "fame, glory" and berht "bright"). It is also in use as a surname.[2][3]
