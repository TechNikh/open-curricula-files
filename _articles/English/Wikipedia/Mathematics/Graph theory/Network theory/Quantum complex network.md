---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_complex_network
offline_file: ""
offline_thumbnail: ""
uuid: 7789b711-133b-4c7a-8aa8-698bfdd9fcbb
updated: 1484309438
title: Quantum complex network
tags:
    - Motivation
    - Important concepts
    - Qubits
    - Entanglement
    - Bell measurement
    - Entanglement swapping
    - Network structure
    - Notation
    - Models
    - Quantum random networks
    - Entanglement Percolation
categories:
    - Network theory
---
Being part of network science the study of quantum complex networks aims to explore the impact of complexity science and network architectures in quantum systems.[1][2][3] According to quantum information theory it is possible to improve communication security and data transfer rates by taking advantage of quantum mechanics.[4][5] In this context the study of quantum complex networks is motivated by the possibility of quantum communications being used on a massive scale in the future.[2] In such case it is likely that quantum communication networks will acquire non trivial features as is common in existing communication networks today.[3][6]
