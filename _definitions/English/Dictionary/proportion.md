---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proportion
offline_file: ""
offline_thumbnail: ""
uuid: e1ff10ea-4db4-4a2c-826f-5dfbffa948c4
updated: 1484310273
title: proportion
categories:
    - Dictionary
---
proportion
     n 1: the quotient obtained when the magnitude of a part is
          divided by the magnitude of the whole [syn: {proportionality}]
     2: magnitude or extent; "a building of vast proportions" [syn:
        {dimension}]
     3: balance among the parts of something [syn: {symmetry}] [ant:
         {disproportion}]
     4: harmonious arrangement or relation of parts or elements
        within a whole (as in a design); "in all perfectly
        beautiful objects there is found the opposition of one
        part to another and a reciprocal balance"- John Ruskin
        [syn: {balance}]
     v 1: give pleasant proportions to; "harmonize a building with
          those ...
