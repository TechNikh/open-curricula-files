---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Structural_proof_theory
offline_file: ""
offline_thumbnail: ""
uuid: 1a8090b8-d158-4eb9-b08e-0afc05007a81
updated: 1484309497
title: Structural proof theory
tags:
    - Analytic proof
    - Structures and connectives
    - Cut-elimination in the sequent calculus
    - Natural deduction and the formulae-as-types correspondence
    - Logical duality and harmony
    - Display logic
    - Calculus of structures
categories:
    - Proof theory
---
In mathematical logic, structural proof theory is the subdiscipline of proof theory that studies proof calculi that support a notion of analytic proof.
