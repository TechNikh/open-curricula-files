---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attain
offline_file: ""
offline_thumbnail: ""
uuid: 651c4729-a302-46b0-ad68-47d0bdfc9414
updated: 1484310240
title: attain
categories:
    - Dictionary
---
attain
     v 1: to gain with effort; "she achieved her goal despite
          setbacks" [syn: {achieve}, {accomplish}, {reach}]
     2: reach a point in time, or a certain state or level; "The
        thermometer hit 100 degrees"; "This car can reach a speed
        of 140 miles per hour" [syn: {reach}, {hit}]
     3: find unexpectedly; "the archeologists chanced upon an old
        tomb"; "she struck a goldmine"; "The hikers finally struck
        the main path to the lake" [syn: {fall upon}, {strike}, {come
        upon}, {light upon}, {chance upon}, {come across}, {chance
        on}, {happen upon}, {discover}]
     4: reach a destination, either real or abstract; "We hit
        ...
