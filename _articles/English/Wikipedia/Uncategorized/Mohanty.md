---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mohanty
offline_file: ""
offline_thumbnail: ""
uuid: 56789d08-f268-48e6-80ed-a12cdc35a540
updated: 1484308451
title: Mohanty
categories:
    - Uncategorized
---
The Mohanty surname derives from the Sanskrit word "mahan" meaning "great". The title was awarded by the king to scribes and successful administrative members within the king's court. The surname belongs to the Kayastha warrior caste and is common to the Indian state of Orissa.
