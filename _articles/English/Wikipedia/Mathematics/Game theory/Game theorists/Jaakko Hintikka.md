---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jaakko_Hintikka
offline_file: ""
offline_thumbnail: ""
uuid: 5be1f4bf-27b3-49de-9286-ec95386f7ae2
updated: 1484308517
title: Jaakko Hintikka
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Jaakko_Hintikka.jpg
tags:
    - Biography
    - Selected books
    - Notes
categories:
    - Game theorists
---
