---
version: 1
type: video
provider: YouTube
id: VLmd5_HfV0s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 93e6cf17-3cfd-479b-8ffe-116e996bb21e
updated: 1486069634
title: an experiment on frictional force
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/VLmd5_HfV0s/default.jpg
    - https://i3.ytimg.com/vi/VLmd5_HfV0s/1.jpg
    - https://i3.ytimg.com/vi/VLmd5_HfV0s/2.jpg
    - https://i3.ytimg.com/vi/VLmd5_HfV0s/3.jpg
---
