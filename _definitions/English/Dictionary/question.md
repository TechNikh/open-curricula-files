---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/question
offline_file: ""
offline_thumbnail: ""
uuid: 4688abda-739d-433b-818e-76f79d8f1c4a
updated: 1484310277
title: question
categories:
    - Dictionary
---
question
     n 1: an instance of questioning; "there was a question about my
          training"; "we made inquiries of all those who were
          present" [syn: {inquiry}, {enquiry}, {query}, {interrogation}]
          [ant: {answer}]
     2: the subject matter at issue; "the question of disease merits
        serious discussion"; "under the head of minor Roman poets"
        [syn: {head}]
     3: a sentence of inquiry that asks for a reply; "he asked a
        direct question"; "he had trouble phrasing his
        interrogations" [syn: {interrogation}, {interrogative}, {interrogative
        sentence}]
     4: uncertainty about the truth or factuality of existence of
        something; ...
