---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yamuna
offline_file: ""
offline_thumbnail: ""
uuid: 9a499811-0d60-404a-bf0a-67da4f5c44ca
updated: 1484308420
title: Yamuna
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-DoabUnitedProvincesIGI1908.jpg
tags:
    - Course
    - Important tributaries
    - History
    - Religious significance
    - Management
    - Irrigation
    - Western Yamuna Canal
    - Delhi Branch
    - Sirsa Branch
    - Barwala Branch
    - Hansi Branch
    - Butana Branch
    - Sunder Branch
    - Jind Branch
    - Rohtak Branch
    - Bhiwani Branch
    - Bhalaut Branch
    - Jhajjar Branch
    - The Sutlej–Yamuna Link
    - Conservation zone
    - pollution
    - Causes
    - Gallery
    - Quotes on Yamuna
categories:
    - Uncategorized
---
The Yamuna (/jəmʊnaː/; Sanskrit and Hindi: यमुना), sometimes called Jamuna (Hindi: जमुना; /d͡ʒəmʊna:/) is the longest and the second largest tributary river of the Ganges (Ganga) in northern India. Originating from the Yamunotri Glacier at a height of 6,387 metres on the south western slopes of Banderpooch peaks in the uppermost region of the Lower Himalayas in Uttarakhand, it travels a total length of 1,376 kilometres (855 mi) and has a drainage system of 366,223 square kilometres (141,399 sq mi), 40.2% of the entire Ganges Basin, before merging with the Ganges at Triveni Sangam, Allahabad, the site for the Kumbha Mela every twelve years. It is the longest ...
