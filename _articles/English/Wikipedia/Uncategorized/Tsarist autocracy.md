---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tsarist
offline_file: ""
offline_thumbnail: ""
uuid: fdf4746c-f39c-426d-902d-c2085228120d
updated: 1484308476
title: Tsarist autocracy
tags:
    - Alternative names
    - History
    - features
    - Influences
    - Notes
categories:
    - Uncategorized
---
Tsarist autocracy[a] (Russian: царское самодержавие, transcr. tsarskoye samoderzhaviye) refers to a form of autocracy (later absolute monarchy) specific to the Grand Duchy of Moscow, which later became Tsardom of Russia and the Russian Empire.[b] In it, all power and wealth is controlled (and distributed) by the tsar. They had more power than constitutional monarchs, who are usually rested by law and counterbalanced by a legislative authority; they even had more authority on religious issues compared to Western monarchs. In Russia, it originated during the time of Ivan III (1440−1505), and was abolished after the Russian Revolution of 1917.
