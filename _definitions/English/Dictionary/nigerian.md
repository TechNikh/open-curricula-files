---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nigerian
offline_file: ""
offline_thumbnail: ""
uuid: 0550a4f4-2dc2-48be-b321-9e8f0878a480
updated: 1484310579
title: nigerian
categories:
    - Dictionary
---
Nigerian
     adj 1: of or relating to Nigeria; "the Nigerian capital used to be
            Lagos"
     2: of or relating to the people of Nigeria; "a Nigerian
        novelist won the Noble Prize for literature this year"
        [syn: {Nigerien}]
     n : a native or inhabitant of Nigeria
