---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reichstag
offline_file: ""
offline_thumbnail: ""
uuid: 0e518f03-f679-49ee-87f3-e0751f4a9dc6
updated: 1484308482
title: Reichstag
tags:
    - Buildings
    - Institutions
    - Historic events
categories:
    - Uncategorized
---
Reichstag is a German word generally meaning parliament, more directly translated as Diet of the Realm or National Diet or Imperial Diet. It may refer to:
