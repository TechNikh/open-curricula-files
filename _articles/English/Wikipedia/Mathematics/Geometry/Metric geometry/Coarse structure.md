---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coarse_structure
offline_file: ""
offline_thumbnail: ""
uuid: ce5c5947-5a2c-4613-911e-0837d1f64353
updated: 1484309261
title: Coarse structure
tags:
    - Definition
    - Intuition
    - Examples
categories:
    - Metric geometry
---
In the mathematical fields of geometry and topology, a coarse structure on a set X is a collection of subsets of the cartesian product X × X with certain properties which allow the large-scale structure of metric spaces and topological spaces to be defined.
