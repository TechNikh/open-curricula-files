---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bathing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484473981
title: bathing
categories:
    - Dictionary
---
bathing
     n 1: immersing the body in water or sunshine
     2: the act of washing yourself (or another person) [syn: {washup}]
