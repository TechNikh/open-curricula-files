---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pose
offline_file: ""
offline_thumbnail: ""
uuid: 4adde478-de16-4bb8-9e6a-be4c60960fbd
updated: 1484310188
title: pose
categories:
    - Dictionary
---
pose
     n 1: affected manners intended to impress others; "don't put on
          airs with me" [syn: {airs}]
     2: a posture assumed by models for photographic or artistic
        purposes
     3: a deliberate pretense or exaggerated display [syn: {affectation},
         {mannerism}, {affectedness}]
     v 1: introduce; "This poses an interesting question" [syn: {present}]
     2: assume a posture as for artistic purposes; "We don't know
        the woman who posed for Leonardo so often" [syn: {model},
        {sit}, {posture}]
     3: pretend to be someone you are not; sometimes with fraudulent
        intentions; "She posed as the Czar's daughter" [syn: {impersonate},
         ...
