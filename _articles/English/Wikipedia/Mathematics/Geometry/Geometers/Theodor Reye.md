---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Theodor_Reye
offline_file: ""
offline_thumbnail: ""
uuid: 8fc640d3-334b-4bb0-b834-a739e8f612a3
updated: 1484309098
title: Theodor Reye
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Theodor_Reye.jpeg
tags:
    - life
    - Mathematical work
categories:
    - Geometers
---
Karl Theodor Reye (born 20 June 1838 in Ritzebüttel, Germany and died 2 July 1919 in Würzburg, Germany) was a German mathematician. He contributed to geometry, particularly projective geometry and synthetic geometry. He is best known for his introduction of configurations in the second edition of his book, Geometrie der Lage (Geometry of Position, 1876).[1] The Reye configuration of 12 points, 12 planes, and 16 lines is named after him.
