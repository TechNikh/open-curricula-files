---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/berlin
offline_file: ""
offline_thumbnail: ""
uuid: 3883bd8d-d3f6-4e37-be5d-5b849c0512b1
updated: 1484310567
title: berlin
categories:
    - Dictionary
---
Berlin
     n 1: capital of Germany located in eastern Germany [syn: {German
          capital}]
     2: United States songwriter (born in Russia) who wrote more
        than 1500 songs and several musical comedies (1888-1989)
        [syn: {Irving Berlin}, {Israel Baline}]
     3: a limousine with a glass partition between the front and
        back seats
