---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/straight
offline_file: ""
offline_thumbnail: ""
uuid: 32e8303d-040c-4bd2-be9e-e843a16a4077
updated: 1484310220
title: straight
categories:
    - Dictionary
---
straight
     adj 1: successive (without a break); "sick for five straight days"
            [syn: {consecutive}]
     2: having no deviations; "straight lines"; "straight roads
        across the desert"; "straight teeth"; "straight shoulders"
        [ant: {crooked}]
     3: (of hair) having no waves or curls; "her naturally straight
        hair hung long and silky" [ant: {curly}]
     4: erect in posture; "behind him sat old man Arthur; he was
        straight with something angry in his attitude"; "stood
        defiantly with unbowed back" [syn: {unbent}, {unbowed}, {upright}]
     5: right; in keeping with the facts; "set the record straight";
        "made sure the facts were ...
