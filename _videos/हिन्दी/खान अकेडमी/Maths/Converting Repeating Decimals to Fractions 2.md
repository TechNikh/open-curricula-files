---
version: 1
type: video
provider: YouTube
id: Q-BKiAdCC7c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5cc4c73d-e7f8-41c9-9fa4-72eb16444fa5
updated: 1486069617
title: Converting Repeating Decimals to Fractions 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Q-BKiAdCC7c/default.jpg
    - https://i3.ytimg.com/vi/Q-BKiAdCC7c/1.jpg
    - https://i3.ytimg.com/vi/Q-BKiAdCC7c/2.jpg
    - https://i3.ytimg.com/vi/Q-BKiAdCC7c/3.jpg
---
