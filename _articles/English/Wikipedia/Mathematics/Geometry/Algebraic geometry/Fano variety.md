---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fano_variety
offline_file: ""
offline_thumbnail: ""
uuid: f5f78ba2-75f7-493d-ba93-902528c47a3a
updated: 1484309147
title: Fano variety
tags:
    - Examples
    - Some properties
    - Classification in small dimensions
    - Notes
categories:
    - Algebraic geometry
---
In algebraic geometry, a Fano variety, introduced in (Fano 1934, 1942), is a complete variety X whose anticanonical bundle KX* is ample. In this definition, one could assume that X is smooth over a field, but the minimal model program has also led to the study of Fano varieties with various types of singularities, such as terminal or klt singularities.
