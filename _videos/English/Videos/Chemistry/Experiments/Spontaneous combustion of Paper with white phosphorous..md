---
version: 1
type: video
provider: YouTube
id: BhAIMK--do0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Spontaneous%20combustion%20of%20Paper%20with%20white%20phosphorous..webm"
offline_file: ""
offline_thumbnail: ""
uuid: 70c4c983-ff30-4b7e-88e6-8f3493bf7526
updated: 1486069661
title: Spontaneous combustion of Paper with white phosphorous.
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/BhAIMK--do0/default.jpg
    - https://i3.ytimg.com/vi/BhAIMK--do0/1.jpg
    - https://i3.ytimg.com/vi/BhAIMK--do0/2.jpg
    - https://i3.ytimg.com/vi/BhAIMK--do0/3.jpg
---
