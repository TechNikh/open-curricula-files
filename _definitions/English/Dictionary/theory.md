---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/theory
offline_file: ""
offline_thumbnail: ""
uuid: eb1dc8cf-e45f-4e7c-8398-c0a2a1977248
updated: 1484310290
title: theory
categories:
    - Dictionary
---
theory
     n 1: a well-substantiated explanation of some aspect of the
          natural world; an organized system of accepted knowledge
          that applies in a variety of circumstances to explain a
          specific set of phenomena; "theories can incorporate
          facts and laws and tested hypotheses"; "true in fact and
          theory"
     2: a tentative theory about the natural world; a concept that
        is not yet verified but that if true would explain certain
        facts or phenomena; "a scientific hypothesis that survives
        experimental testing becomes a scientific theory"; "he
        proposed a fresh theory of alkalis that later was accepted
        in ...
