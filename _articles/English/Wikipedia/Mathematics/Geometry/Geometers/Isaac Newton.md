---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isaac_Newton
offline_file: ""
offline_thumbnail: ""
uuid: b8776996-4191-41a3-aa83-f5e47978708e
updated: 1484309096
title: Isaac Newton
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Sir_Isaac_Newton_by_Sir_Godfrey_Kneller%252C_Bt.jpg'
tags:
    - life
    - Early life
    - Middle years
    - Mathematics
    - Optics
    - Mechanics and gravitation
    - Classification of cubics
    - Later life
    - Personal relations
    - After death
    - Fame
    - Commemorations
    - Religious views
    - Effect on religious thought
    - Occult
    - Alchemy
    - Enlightenment philosophers
    - Apple incident
    - Works
    - Published in his lifetime
    - Published posthumously
    - Primary sources
    - Bibliography
categories:
    - Geometers
---
Sir Isaac Newton FRS (/ˈnjuːtən/;[6] 25 December 1642 – 20 March 1726/27[1]) was an English physicist and mathematician (described in his own day as a "natural philosopher") who is widely recognised as one of the most influential scientists of all time and a key figure in the scientific revolution. His book Philosophiæ Naturalis Principia Mathematica ("Mathematical Principles of Natural Philosophy"), first published in 1687, laid the foundations for classical mechanics. Newton made seminal contributions to optics, and he shares credit with Gottfried Wilhelm Leibniz for the development of calculus.
