---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oestrogen
offline_file: ""
offline_thumbnail: ""
uuid: 30bf21ae-f0d6-44cf-8787-9190dde4d3b5
updated: 1484310160
title: oestrogen
categories:
    - Dictionary
---
oestrogen
     n : a general term for female steroid sex hormones that are
         secreted by the ovary and responsible for typical female
         sexual characteristics [syn: {estrogen}]
