---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Control_communications
offline_file: ""
offline_thumbnail: ""
uuid: 5e28eeb4-f6e3-4671-b778-a212646b47ae
updated: 1484308742
title: Control communications
categories:
    - Control engineering
---
In telecommunication, control communications is the branch of technology devoted to the design, development, and application of communications facilities used specifically for control purposes, such as for controlling (a) industrial processes, (b) movement of resources, (c) electric power generation, distribution, and utilization, (d) communications networks, and (e) transportation systems.
