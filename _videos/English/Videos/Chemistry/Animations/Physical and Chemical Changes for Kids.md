---
version: 1
type: video
provider: YouTube
id: BgM3e8YZxuc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Physical%20and%20Chemical%20Changes%20for%20Kids.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8f1231dc-e5fc-4180-abe0-ca6e6b679e79
updated: 1486069661
title: Physical and Chemical Changes for Kids
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/BgM3e8YZxuc/default.jpg
    - https://i3.ytimg.com/vi/BgM3e8YZxuc/1.jpg
    - https://i3.ytimg.com/vi/BgM3e8YZxuc/2.jpg
    - https://i3.ytimg.com/vi/BgM3e8YZxuc/3.jpg
---
