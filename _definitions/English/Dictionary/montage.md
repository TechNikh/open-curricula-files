---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/montage
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484547961
title: montage
categories:
    - Dictionary
---
montage
     n : a paste-up made by sticking together pieces of paper or
         photographs to form an artistic image; "he used his
         computer to make a collage of pictures superimposed on a
         map" [syn: {collage}]
