---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/previous
offline_file: ""
offline_thumbnail: ""
uuid: 97fa6470-2a44-414b-918d-05e25ec9bfde
updated: 1484310311
title: previous
categories:
    - Dictionary
---
previous
     adj 1: (used especially of persons) of the immediate past; "the
            former president"; "our late President is still very
            active"; "the previous occupant of the White House"
            [syn: {former(a)}, {late(a)}, {previous(a)}]
     2: too soon or too hasty; "our condemnation of him was a bit
        previous"; "a premature judgment" [syn: {previous(p)}, {premature}]
     3: just preceding something else in time or order; "the
        previous owner"; "my old house was larger" [syn: {previous(a)},
         {old}]
