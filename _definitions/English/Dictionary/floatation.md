---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/floatation
offline_file: ""
offline_thumbnail: ""
uuid: 3cb76b8b-e780-463d-8db2-f189a726ee63
updated: 1484310409
title: floatation
categories:
    - Dictionary
---
floatation
     n 1: the phenomenon of floating (remaining on the surface of a
          liquid without sinking) [syn: {flotation}]
     2: financing a commercial enterprise by bond or stock shares
        [syn: {flotation}]
