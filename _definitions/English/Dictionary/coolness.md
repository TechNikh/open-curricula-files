---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coolness
offline_file: ""
offline_thumbnail: ""
uuid: 4c7b11e2-7f44-4431-a9df-8e7527ef6858
updated: 1484310228
title: coolness
categories:
    - Dictionary
---
coolness
     n 1: the property of being moderately cold; "the chilliness of
          early morning" [syn: {chilliness}]
     2: calm and unruffled self-assurance; "he performed with all
        the coolness of a veteran" [syn: {imperturbability}, {imperturbableness}]
     3: fearless self-possession in the face of danger [syn: {nervelessness}]
     4: a lack of affection or enthusiasm [syn: {coldness}, {frigidity}]
