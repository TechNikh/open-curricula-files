---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Konrad_Osterwalder
offline_file: ""
offline_thumbnail: ""
uuid: 1bf82c71-6b9c-4103-9891-16d45dea059e
updated: 1484309093
title: Konrad Osterwalder
tags:
    - United Nations University
    - Bologna Process
    - Life and career
    - Osterwalder–Schrader theorem
    - Career achievements
    - Awards and prizes
    - Publications
categories:
    - Geometers
---
Konrad Osterwalder (born June 3, 1942) is a Swiss mathematician and physicist, former Undersecretary-General of the United Nations, former Rector of the United Nations University (UNU),[1] and Rector Emeritus of the Swiss Federal Institute of Technology Zurich (ETH Zurich).
