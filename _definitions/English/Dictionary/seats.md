---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seats
offline_file: ""
offline_thumbnail: ""
uuid: db6b9cc9-2de8-436c-bdf1-d5b4ff13206b
updated: 1484310581
title: seats
categories:
    - Dictionary
---
seats
     n : an area that includes seats for several people; "there is
         seating for 40 students in this classroom" [syn: {seating},
          {seating room}, {seating area}]
