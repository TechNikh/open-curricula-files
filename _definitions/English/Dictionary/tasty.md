---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tasty
offline_file: ""
offline_thumbnail: ""
uuid: df84d544-de28-492f-822c-3c6867ff2a20
updated: 1484310353
title: tasty
categories:
    - Dictionary
---
tasty
     adj 1: especially pleasing to the taste; "a dainty dish to set
            before a kind"; "a tasty morsel" [syn: {dainty}]
     2: pleasing to the sense of taste [syn: {mouth-watering}, {savory},
         {savoury}]
     [also: {tastiest}, {tastier}]
