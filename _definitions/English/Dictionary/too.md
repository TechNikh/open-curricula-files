---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/too
offline_file: ""
offline_thumbnail: ""
uuid: c593a2f3-9323-4d40-97a7-a29ca5bafcba
updated: 1484310325
title: too
categories:
    - Dictionary
---
too
     adv 1: to an excessive degree; "too big" [syn: {excessively}, {overly},
             {to a fault}]
     2: in addition; "he has a Mercedes, too" [syn: {besides}, {also},
         {likewise}, {as well}]
