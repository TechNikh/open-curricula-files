---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incoming
offline_file: ""
offline_thumbnail: ""
uuid: 7990d68f-8d8e-488c-a9e2-466977e2df11
updated: 1484310218
title: incoming
categories:
    - Dictionary
---
incoming
     adj : coming in or succeeding or of the future; "incoming class";
           "incoming mail"; "the incoming president" [ant: {outgoing}]
     n : the act of entering; "she made a grand entrance" [syn: {entrance},
          {entering}, {entry}, {ingress}]
