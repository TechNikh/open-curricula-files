---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thrown
offline_file: ""
offline_thumbnail: ""
uuid: 8867de10-0d63-4716-9404-c0441837e924
updated: 1484310154
title: thrown
categories:
    - Dictionary
---
throw
     n 1: the act of throwing (propelling something through the air
          with a rapid movement of the arm and wrist); "the
          catcher made a good throw to second base"
     2: a single chance or instance; "he couldn't afford $50 a
        throw"
     3: the maximum movement available to a pivoted or reciprocating
        piece by a cam [syn: {stroke}, {cam stroke}]
     4: the distance that something can be thrown; "it is just a
        stone's throw from here"
     5: bedclothes consisting of a lightweight cloth covering (an
        afghan or bedspread) that is casually thrown over
        something
     6: the throwing of an object in order to determine an outcome
       ...
