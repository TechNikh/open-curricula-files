---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crazy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484452441
title: crazy
categories:
    - Dictionary
---
crazy
     adj 1: affected with madness or insanity; "a man who had gone mad"
            [syn: {brainsick}, {demented}, {distracted}, {disturbed},
             {mad}, {sick}, {unbalanced}, {unhinged}]
     2: foolish; totally unsound; "an impractical solution"; "a
        crazy scheme"; "half-baked ideas"; "a screwball proposal
        without a prayer of working" [syn: {half-baked}, {screwball},
         {softheaded}]
     3: marked by foolish or unreasoning fondness; "she was crazy
        about him"; "gaga over the rock group's new album"; "he
        was infatuated with her" [syn: {dotty}, {gaga}, {enamored},
         {infatuated}, {in love}, {smitten}, {soft on(p)}, {taken
        ...
