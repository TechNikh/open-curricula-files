---
version: 1
type: video
provider: YouTube
id: Fh45mhVsZrU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Run-ons%20and%20comma%20splices.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0125ece0-e156-4899-84e9-fae9de17feed
updated: 1486069601
title: Run-ons and comma splices
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/Fh45mhVsZrU/default.jpg
    - https://i3.ytimg.com/vi/Fh45mhVsZrU/1.jpg
    - https://i3.ytimg.com/vi/Fh45mhVsZrU/2.jpg
    - https://i3.ytimg.com/vi/Fh45mhVsZrU/3.jpg
---
A run-on sentence doesn’t separate any of its independent clauses with the punctuation that it needs, and a comma splice incorrectly separates two independent clauses with a comma, instead of a comma-and-coordinating-conjunction.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/syntax/e/recognizing-run-ons-and-comma-splices 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/subjects-and-predicates-syntax-khan-academy

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/syntax/v/recognizing-fragments-syntax-khan-academy 

Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
