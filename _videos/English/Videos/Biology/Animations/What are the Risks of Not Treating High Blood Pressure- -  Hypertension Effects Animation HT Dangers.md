---
version: 1
type: video
provider: YouTube
id: pD1MP4YjunQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/What%20are%20the%20Risks%20of%20Not%20Treating%20High%20Blood%20Pressure-%20-%20%20Hypertension%20Effects%20Animation%20HT%20Dangers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 00e2a86f-0462-4041-a19a-4ae141c80f84
updated: 1486069669
title: 'What are the Risks of Not Treating High Blood Pressure? -  Hypertension Effects Animation HT Dangers'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/pD1MP4YjunQ/default.jpg
    - https://i3.ytimg.com/vi/pD1MP4YjunQ/1.jpg
    - https://i3.ytimg.com/vi/pD1MP4YjunQ/2.jpg
    - https://i3.ytimg.com/vi/pD1MP4YjunQ/3.jpg
---
