---
version: 1
type: article
id: https://en.wikipedia.org/wiki/System_information_modelling
offline_file: ""
offline_thumbnail: ""
uuid: 20088393-5e8f-44f5-88f7-ed35af68ad02
updated: 1484308739
title: System information modelling
tags:
    - Origins
    - Definition
    - Throughout the life-cycle
    - Design
    - Procurement and construction
    - Asset management
    - Software
    - International development
    - Australia
    - China
    - Saudi Arabia
    - SIM and BIM
    - Extended applications
categories:
    - Control engineering
---
System information modelling (SIM) is a generic term used to describe the process of modelling complex connected systems. System information models are digital representations of connected systems, such as electrical instrumentation and control, power and communication systems. The objects modelled in a SIM have a 1:1 relationship with the objects in the physical system. Components, connections and functions are defined and linked as they would be in the real world.
