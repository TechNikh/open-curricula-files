---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dehradun
offline_file: ""
offline_thumbnail: ""
uuid: 19b46304-0f9f-4eed-b52d-5c96872a28d9
updated: 1484308415
title: Dehradun
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Flag_of_Tehri_Garhwal.svg.png
tags:
    - Etymology
    - History
    - geography
    - Climate
    - Demographics
    - Economy
    - Tourism
    - Transport
    - Education
    - Higher educational institutions
    - culture
    - Sport
    - Grounds
    - Architecture
    - Shopping and entertainment
    - Images
categories:
    - Uncategorized
---
Dehradun /ˌdɛrəˈduːn/ or Dehra Dun is the capital city of the state of Uttarakhand in the northern part of India. Located in the Garhwal region, it lies 236 kilometres (147 mi) north of India's capital New Delhi and is one of the "Counter Magnets" of the National Capital Region (NCR) being developed as an alternative centre of growth to help ease the migration and population explosion in the Delhi metropolitan area.[3]
