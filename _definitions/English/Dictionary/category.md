---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/category
offline_file: ""
offline_thumbnail: ""
uuid: 2c1a0123-84ea-4904-a3a1-a11216c9bda2
updated: 1484310439
title: category
categories:
    - Dictionary
---
category
     n 1: a collection of things sharing a common attribute; "there
          are two classes of detergents" [syn: {class}, {family}]
     2: a general concept that marks divisions or coordinations in a
        conceptual scheme
