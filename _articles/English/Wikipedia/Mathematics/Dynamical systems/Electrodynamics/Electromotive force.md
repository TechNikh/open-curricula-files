---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electromotive_force
offline_file: ""
offline_thumbnail: ""
uuid: b241e742-e695-43c0-8148-f562c2a8fbf9
updated: 1484308753
title: Electromotive force
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/380px-Reaction_path.JPG
tags:
    - Notation and units of measurement
    - Formal definitions
    - In thermodynamics
    - Voltage difference
    - Generation
    - Chemical sources
    - Voltaic cells
    - Of cells
    - Electromagnetic induction
    - Contact potentials
    - Solar cell
categories:
    - Electrodynamics
---
Electromotive force, also called emf (denoted 
  
    
      
        
          
            E
          
        
      
    
    {\displaystyle {\mathcal {E}}}
  
 and measured in volts),[1] is the voltage developed by any source of electrical energy such as a battery or dynamo. It is generally defined as the electrical potential for a source in a circuit.[2] A device that supplies electrical energy is called electromotive force or emf. Emfs convert chemical, mechanical, and other forms of energy into electrical energy.[3] The product of such a device is also known as emf.
