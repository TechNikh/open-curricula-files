---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trodden
offline_file: ""
offline_thumbnail: ""
uuid: 39c122d8-3640-4ab5-b848-b55bc63b86f8
updated: 1484310609
title: trodden
categories:
    - Dictionary
---
tread
     n 1: a step in walking or running [syn: {pace}, {stride}]
     2: the grooved surface of a pneumatic tire
     3: the part (as of a wheel or shoe) that makes contact with the
        ground
     4: structural member consisting of the horizontal part of a
        stair or step
     v 1: put down or press the foot, place the foot; "For fools rush
          in where angels fear to tread"; "step on the brake"
          [syn: {step}]
     2: tread or stomp heavily or roughly; "The soldiers trampled
        across the fields" [syn: {trample}]
     3: crush as if by treading on; "tread grapes to make wine"
     4: brace (an archer's bow) by pressing the foot against the
        center
  ...
