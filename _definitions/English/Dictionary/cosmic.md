---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cosmic
offline_file: ""
offline_thumbnail: ""
uuid: 46ccf56e-02ac-4c3d-a0a0-2597086b183e
updated: 1484310391
title: cosmic
categories:
    - Dictionary
---
cosmic
     adj 1: of or from or pertaining to or characteristic of the cosmos
            or universe; "cosmic laws"; "cosmic catastrophe";
            "cosmic rays"
     2: inconceivably extended in space or time
