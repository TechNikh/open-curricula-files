---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Asymmetry_Principle
offline_file: ""
offline_thumbnail: ""
uuid: 3c1a9d7b-34f9-4466-9b4b-d804577846b8
updated: 1484309304
title: Asymmetry Principle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Figure_10_1.jpg
tags:
    - Introduction
    - Multiples
    - Wasted Resources
    - Inefficient Power Generation
    - Energy Loses
    - Alternative Energy Source of the Future
    - Presentation
categories:
    - Symmetry
---
The Asymmetry Principle was developed by economist Peter Tertzakian, and was first presented in his book The End of Energy Obesity published in 2009. Historically, each energy crisis has brought society to a solutions crossroad: find more or use less. The latter path through conservation efforts has seldom triggered any excitement, so energy shocks have always triggered calls for more supply. With six billion people from the developing world seeking the same living standards as those in the affluent OECD countries the earth’s resources and environment are being put under great strain again. Tertzakian terms this problem the modern world's "energy obesity." In pursuit of a solution, the ...
