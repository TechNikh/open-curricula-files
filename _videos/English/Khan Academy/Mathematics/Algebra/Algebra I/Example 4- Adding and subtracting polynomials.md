---
version: 1
type: video
provider: YouTube
id: DMyhUb1pZT0
offline_file: ""
offline_thumbnail: ""
uuid: 97f29be2-91b3-4178-ac7c-14aab37a03ea
updated: 1484040398
title: 'Example 4- Adding and subtracting polynomials'
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/DMyhUb1pZT0/default.jpg
    - https://i3.ytimg.com/vi/DMyhUb1pZT0/1.jpg
    - https://i3.ytimg.com/vi/DMyhUb1pZT0/2.jpg
    - https://i3.ytimg.com/vi/DMyhUb1pZT0/3.jpg
---
