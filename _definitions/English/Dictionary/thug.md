---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thug
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484574841
title: thug
categories:
    - Dictionary
---
thug
     n : an aggressive and violent young criminal [syn: {hood}, {hoodlum},
          {goon}, {punk}, {tough}, {toughie}, {strong-armer}]
