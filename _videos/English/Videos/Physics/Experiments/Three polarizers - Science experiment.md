---
version: 1
type: video
provider: YouTube
id: -Yh-U8Ro-P0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2b0f05f3-9968-41fc-988f-bd7853c3253c
updated: 1486069635
title: 'Three polarizers - Science experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/-Yh-U8Ro-P0/default.jpg
    - https://i3.ytimg.com/vi/-Yh-U8Ro-P0/1.jpg
    - https://i3.ytimg.com/vi/-Yh-U8Ro-P0/2.jpg
    - https://i3.ytimg.com/vi/-Yh-U8Ro-P0/3.jpg
---
