---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperstability
offline_file: ""
offline_thumbnail: ""
uuid: d53fd726-e322-4b13-aae1-4c53cb840c50
updated: 1484308832
title: Hyperstability
categories:
    - Stability theory
---
In stability theory, hyperstability is a property of a system that requires the state vector to remain bounded if the inputs are restricted to belonging to a subset of the set of all possible inputs.[1]
