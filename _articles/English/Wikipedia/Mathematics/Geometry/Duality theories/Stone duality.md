---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stone_duality
offline_file: ""
offline_thumbnail: ""
uuid: 491ee897-d171-4ed4-a2f9-e1fb888168cd
updated: 1484309239
title: Stone duality
tags:
    - Overview of Stone-type dualities
    - Duality of sober spaces and spatial locales
    - The lattice of open sets
    - Points of a locale
    - The functor pt
    - The adjunction of Top and Loc
    - The duality theorem
categories:
    - Duality theories
---
In mathematics, there is an ample supply of categorical dualities between certain categories of topological spaces and categories of partially ordered sets. Today, these dualities are usually collected under the label Stone duality, since they form a natural generalization of Stone's representation theorem for Boolean algebras. These concepts are named in honor of Marshall Stone. Stone-type dualities also provide the foundation for pointless topology and are exploited in theoretical computer science for the study of formal semantics.
