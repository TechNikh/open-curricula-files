---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/directory
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484403121
title: directory
categories:
    - Dictionary
---
directory
     n 1: an alphabetical list of names and addresses
     2: (computer science) a listing of the files stored in memory
        (usually on a hard disk)
