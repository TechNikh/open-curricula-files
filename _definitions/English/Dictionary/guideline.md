---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guideline
offline_file: ""
offline_thumbnail: ""
uuid: d31aed5e-e995-4576-9c5d-2bc932e73163
updated: 1484310192
title: guideline
categories:
    - Dictionary
---
guideline
     n 1: a light line that is used in lettering to help align the
          letters
     2: a detailed plan or explanation to guide you in setting
        standards or determining a course of action; "the
        president said he had a road map for normalizing relations
        with Vietnam" [syn: {road map}]
     3: a rule or principle that provides guidance to appropriate
        behavior [syn: {guidepost}, {rule of thumb}]
