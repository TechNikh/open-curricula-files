---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/key
offline_file: ""
offline_thumbnail: ""
uuid: 663be52b-c014-4ca6-ae0d-d14942b30f4d
updated: 1484310325
title: key
categories:
    - Dictionary
---
key
     adj 1: serving as an essential component; "a cardinal rule"; "the
            central cause of the problem"; "an example that was
            fundamental to the argument"; "computers are
            fundamental to modern industrial structure" [syn: {cardinal},
             {central}, {fundamental}, {primal}]
     2: effective; producing a desired effect; "the operative word"
        [syn: {operative}]
     n 1: metal device shaped in such a way that when it is inserted
          into the appropriate lock the lock's mechanism can be
          rotated
     2: something crucial for explaining; "the key to development is
        economic integration"
     3: pitch of the voice; "he ...
