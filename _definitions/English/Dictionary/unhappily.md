---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unhappily
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484652001
title: unhappily
categories:
    - Dictionary
---
unhappily
     adv 1: in an unpleasant way; "they were unhappily married" [ant: {happily}]
     2: in an unfortunate way; "sadly he died before he could see
        his grandchild" [syn: {sadly}] [ant: {happily}]
