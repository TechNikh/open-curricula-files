---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maglev
offline_file: ""
offline_thumbnail: ""
uuid: 4db9f28e-c944-420c-a9e1-dc168883b01b
updated: 1484308759
title: Maglev
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Series_L0.JPG
tags:
    - Development
    - History
    - First maglev patent
    - New York, United States, 1913
    - New York, United States, 1968
    - Hamburg, Germany, 1979
    - Birmingham, United Kingdom, 1984–95
    - Emsland, Germany, 1984–2012
    - Japan, 1969–present
    - Vancouver, Canada and Hamburg, Germany, 1986–88
    - Berlin, Germany, 1989–91
    - South Korea, 1993–present
    - Technology
    - Electromagnetic suspension
    - Electrodynamic suspension (EDS)
    - Tracks
    - Evaluation
    - Propulsion
    - Stability
    - Guidance system
    - Evacuated tubes
    - Energy use
    - Comparison with conventional trains
    - Comparison with aircraft
    - Economics
    - Records
    - History of maglev speed records
    - Systems
    - Test tracks
    - San Diego, USA
    - SCMaglev, Japan
    - "FTA's UMTD program"
    - Southwest Jiaotong University, China
    - Operational systems
    - Shanghai Maglev
    - Linimo (Tobu Kyuryo Line, Japan)
    - Incheon Airport Maglev
    - Changsha Maglev
    - Maglevs under construction
    - AMT test track – Powder Springs, Georgia
    - Beijing S1 line
    - Tokyo – Nagoya – Osaka
    - SkyTran – Tel Aviv (Israel)
    - Proposed maglev systems
    - Australia
    - Italy
    - United Kingdom
    - United States
    - Puerto Rico
    - Germany
    - Switzerland
    - China
    - India
    - Malaysia
    - Iran
    - Taiwan
    - Hong Kong
    - Incidents
    - Notes
categories:
    - Electrodynamics
---
Maglev (derived from magnetic levitation) is a transport method that uses magnetic levitation to move vehicles without making contact with the ground. With maglev, a vehicle travels along a guideway using magnets to create both lift and propulsion, thereby reducing friction by a great extent and allowing very high speeds.
