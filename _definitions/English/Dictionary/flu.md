---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flu
offline_file: ""
offline_thumbnail: ""
uuid: c2a022d6-6311-401d-9faa-bcc3a50dfdcf
updated: 1484310479
title: flu
categories:
    - Dictionary
---
flu
     n : an acute febrile highly contagious viral disease [syn: {influenza},
          {grippe}]
