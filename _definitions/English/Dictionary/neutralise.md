---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neutralise
offline_file: ""
offline_thumbnail: ""
uuid: c1825bb8-39c2-4c79-80fb-4ad6ac8cb5df
updated: 1484310413
title: neutralise
categories:
    - Dictionary
---
neutralise
     v 1: get rid of (someone who may be a threat) by killing; "The
          mafia liquidated the informer"; "the double agent was
          neutralized" [syn: {neutralize}, {liquidate}, {waste}, {knock
          off}, {do in}]
     2: make incapable of military action [syn: {neutralize}]
     3: make ineffective by counterbalancing the effect of; "Her
        optimism neutralizes his gloom"; "This action will negate
        the effect of my efforts" [syn: {neutralize}, {nullify}, {negate}]
     4: make chemically neutral; "She neutralized the solution"
        [syn: {neutralize}]
