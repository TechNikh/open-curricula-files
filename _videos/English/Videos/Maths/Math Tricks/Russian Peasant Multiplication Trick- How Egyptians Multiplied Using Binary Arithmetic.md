---
version: 1
type: video
provider: YouTube
id: qHXsKyVSPOU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 31cc3212-a822-4dc7-9b73-41647d3aff4b
updated: 1486069623
title: 'Russian Peasant Multiplication Trick- How Egyptians Multiplied Using Binary Arithmetic'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/qHXsKyVSPOU/default.jpg
    - https://i3.ytimg.com/vi/qHXsKyVSPOU/1.jpg
    - https://i3.ytimg.com/vi/qHXsKyVSPOU/2.jpg
    - https://i3.ytimg.com/vi/qHXsKyVSPOU/3.jpg
---
