---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Least_fixed_point
offline_file: ""
offline_thumbnail: ""
uuid: 16ea4696-2c45-4974-bcab-32b1917fb899
updated: 1484308780
title: Least fixed point
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Puntos_fijos.svg.png
categories:
    - Fixed points (mathematics)
---
In order theory, a branch of mathematics, the least fixed point (lfp or LFP, sometimes also smallest fixed point) of a function from a partially ordered set to itself is the fixed point which is less than each other fixed point, according to the set's order. A function need not have a least fixed point, and cannot have more than one.
