---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Morass_(set_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 868c7280-f172-4497-89dc-2468d279b091
updated: 1484309509
title: Morass (set theory)
categories:
    - Set theory
---
In axiomatic set theory, a mathematical discipline, a morass is an infinite combinatorial structure, used to create "large" structures from a "small" number of "small" approximations. They were invented by Ronald Jensen in his proof that cardinal transfer theorems hold under the axiom of constructibility.
