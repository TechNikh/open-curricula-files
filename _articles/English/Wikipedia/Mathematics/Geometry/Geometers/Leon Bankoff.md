---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Leon_Bankoff
offline_file: ""
offline_thumbnail: ""
uuid: dc0a33a1-82c0-44f6-9d34-5c7c59e29ec3
updated: 1484309073
title: Leon Bankoff
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bankoff_Circle.svg.png
categories:
    - Geometers
---
After a visit to the City College of New York, Bankoff studied dentistry at New York University. Later, he moved to Los Angeles, California, where he taught at the University of Southern California; while there, he completed his studies. He practiced over 60 years as a dentist in Beverly Hills. Many of his patients were celebrities.[1]
