---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-enforcing_agreement
offline_file: ""
offline_thumbnail: ""
uuid: 0881b0c5-497d-4f5f-850c-22a2553b4221
updated: 1484309032
title: Self-enforcing agreement
categories:
    - Game theory equilibrium concepts
---
A self-enforcing agreement is an agreement or contract between two parties that is enforced only by those two parties; a third party cannot enforce or interfere with the agreement. This agreement stands so long the parties believe the agreement is mutually beneficial and the agreement is not breached by either party.[1]
