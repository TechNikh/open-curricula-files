---
version: 1
type: video
provider: YouTube
id: IZ8W-h764Cc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Solid%20of%20Revolution%20%28part%207%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a2445fb1-8cd9-45cc-b243-0b28282e6c0e
updated: 1486069694
title: Solid of Revolution (part 7)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/IZ8W-h764Cc/default.jpg
    - https://i3.ytimg.com/vi/IZ8W-h764Cc/1.jpg
    - https://i3.ytimg.com/vi/IZ8W-h764Cc/2.jpg
    - https://i3.ytimg.com/vi/IZ8W-h764Cc/3.jpg
---
