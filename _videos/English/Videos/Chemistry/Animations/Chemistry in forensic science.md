---
version: 1
type: video
provider: YouTube
id: i503z_GsZ7A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Chemistry%20in%20forensic%20science.webm"
offline_file: ""
offline_thumbnail: ""
uuid: deb6b311-4caf-4fcc-96c9-e17fdb176e0a
updated: 1486069661
title: Chemistry in forensic science
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/i503z_GsZ7A/default.jpg
    - https://i3.ytimg.com/vi/i503z_GsZ7A/1.jpg
    - https://i3.ytimg.com/vi/i503z_GsZ7A/2.jpg
    - https://i3.ytimg.com/vi/i503z_GsZ7A/3.jpg
---
