---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transverse_wave
offline_file: ""
offline_thumbnail: ""
uuid: 3718d67e-12c0-4c11-8c6b-7ff5500ecff0
updated: 1484308898
title: Transverse wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Onde_cisaillement_impulsion_1d_30_petit_0.gif
tags:
    - Explanation
    - "'Polarized' waves"
    - Electromagnetic waves
categories:
    - Waves
---
A transverse wave is a moving wave that consists of oscillations occurring perpendicular (or right angled) to the direction of energy transfer. If a transverse wave is moving in the positive x-direction, its oscillations are in up and down directions that lie in the y–z plane. Light is an example of a transverse wave. With regard to transverse waves in matter, the displacement of the medium is perpendicular to the direction of propagation of the wave. A ripple in a pond and a wave on a string are easily visualized as transverse waves.
