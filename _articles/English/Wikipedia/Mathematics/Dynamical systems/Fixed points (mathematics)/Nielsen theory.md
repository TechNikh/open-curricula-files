---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nielsen_theory
offline_file: ""
offline_thumbnail: ""
uuid: cb39f3bb-946a-46fa-ae19-8887e8aa3c90
updated: 1484308784
title: Nielsen theory
categories:
    - Fixed points (mathematics)
---
Nielsen theory is a branch of mathematical research with its origins in topological fixed point theory. Its central ideas were developed by Danish mathematician Jakob Nielsen, and bear his name.
