---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/melting
offline_file: ""
offline_thumbnail: ""
uuid: 2e63d719-3db6-4cd7-b4e0-0b278e802a3e
updated: 1484310224
title: melting
categories:
    - Dictionary
---
melting
     adj : becoming liquid [syn: {liquescent}]
     n : the process whereby heat changes something from a solid to a
         liquid; "the power failure caused a refrigerator melt
         that was a disaster"; "the thawing of a frozen turkey
         takes several hours" [syn: {thaw}, {melt}, {thawing}]
