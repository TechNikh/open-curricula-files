---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regime
offline_file: ""
offline_thumbnail: ""
uuid: 9d361701-6943-4b59-8997-c53c75683305
updated: 1484310557
title: regime
categories:
    - Dictionary
---
regime
     n 1: the organization that is the governing authority of a
          political unit; "the government reduced taxes"; "the
          matter was referred to higher authorities" [syn: {government},
           {authorities}]
     2: (medicine) a systematic plan for therapy (often including
        diet) [syn: {regimen}]
