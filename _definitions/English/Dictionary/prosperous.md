---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prosperous
offline_file: ""
offline_thumbnail: ""
uuid: acecdfc2-79a7-4d37-9e45-d7d2d39fe5e4
updated: 1484310441
title: prosperous
categories:
    - Dictionary
---
prosperous
     adj 1: in fortunate circumstances financially; moderately rich;
            "they were comfortable or even wealthy by some
            standards"; "easy living"; "a prosperous family"; "his
            family is well-situated financially"; "well-to-do
            members of the community" [syn: {comfortable}, {easy},
             {well-fixed}, {well-heeled}, {well-off}, {well-situated},
             {well-to-do}]
     2: very lively and profitable; "flourishing businesses"; "a
        palmy time for stockbrokers"; "a prosperous new business";
        "doing a roaring trade"; "a thriving tourist center"; "did
        a thriving business in orchids" [syn: {booming}, ...
