---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cholera
offline_file: ""
offline_thumbnail: ""
uuid: 2a138d5f-4875-4a6b-910a-6651cb257e56
updated: 1484310473
title: cholera
categories:
    - Dictionary
---
cholera
     n : an acute intestinal infection caused by ingestion of
         contaminated water or food [syn: {Asiatic cholera}, {Indian
         cholera}, {epidemic cholera}]
