---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entertainment
offline_file: ""
offline_thumbnail: ""
uuid: bffa06dc-3590-4aad-8ba7-64515cf0db94
updated: 1484310479
title: entertainment
categories:
    - Dictionary
---
entertainment
     n : a diversion that holds the attention [syn: {amusement}]
