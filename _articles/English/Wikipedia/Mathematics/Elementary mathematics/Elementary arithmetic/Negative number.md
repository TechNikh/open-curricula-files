---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Negative_number
offline_file: ""
offline_thumbnail: ""
uuid: e2d9c7d7-2574-4cea-9d94-8d5a0cfca4fe
updated: 1484308918
title: Negative number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-thumbnail_0.jpg
tags:
    - Introduction
    - As the result of subtraction
    - The number line
    - Signed numbers
    - Everyday uses of negative numbers
    - Sport
    - Science
    - Finance
    - Other
    - Arithmetic involving negative numbers
    - Addition
    - Subtraction
    - Multiplication
    - Division
    - Negation
    - Formal construction of negative integers
    - Uniqueness
    - History
    - Citations
    - Bibliography
categories:
    - Elementary arithmetic
---
In mathematics, a negative number is a real number that is less than zero. Negative numbers represent opposites. If positive represents movement to the right, negative represents movement to the left. If positive represents above sea level, then negative represents below level. If positive represents a deposit, negative represents a withdrawal. They are often used to represent the magnitude of a loss or deficiency. A debt that is owed may be thought of as a negative asset, a decrease in some quantity may be thought of as a negative increase. If a quantity may have either of two opposite senses, then one may choose to distinguish between those senses—perhaps arbitrarily—as positive and ...
