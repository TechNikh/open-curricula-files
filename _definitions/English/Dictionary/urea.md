---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urea
offline_file: ""
offline_thumbnail: ""
uuid: f266ee9a-9ace-415c-ba71-b069092b9e87
updated: 1484310325
title: urea
categories:
    - Dictionary
---
urea
     n : the chief solid component of mammalian urine; synthesized
         from ammonia and carbon dioxide and used as fertilizer
         and in animal feed and in plastics [syn: {carbamide}]
