---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_bandwidth
offline_file: ""
offline_thumbnail: ""
uuid: 6efdc23b-1a7e-4714-b04d-54e686c60866
updated: 1484309422
title: Graph bandwidth
tags:
    - Bandwidth formulas for some graphs
    - Bounds
    - Computing the bandwidth
    - Applications
categories:
    - Graph invariants
---
In graph theory, the graph bandwidth problem is to label the n vertices vi of a graph G with distinct integers f(vi) so that the quantity 
  
    
      
        max
        {
        
        
          |
        
        f
        (
        
          v
          
            i
          
        
        )
        −
        f
        (
        
          v
          
            j
          
        
        )
        
          |
        
        :
        
          v
          
            i
          
        
        
          v
          
            j
          
        
        ∈
        E
        
        }
      
    
    {\displaystyle ...
