---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trapezia
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484338201
title: trapezia
categories:
    - Dictionary
---
trapezium
     n 1: a quadrilateral with no parallel sides [ant: {parallelogram}]
     2: a multiple star in the constellation of Orion [syn: {the
        Trapezium}]
     3: the wrist bone on the thumb side of the hand that
        articulates with the 1st and 2nd metacarpals [syn: {trapezium
        bone}, {os trapezium}]
     [also: {trapezia} (pl)]
