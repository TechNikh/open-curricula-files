---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/target
offline_file: ""
offline_thumbnail: ""
uuid: 17ee2c5b-5f40-433b-9a08-90aa8fedde35
updated: 1484310414
title: target
categories:
    - Dictionary
---
target
     n 1: a reference point to shoot at; "his arrow hit the mark"
          [syn: {mark}]
     2: a person who is the aim of an attack (especially a victim of
        ridicule or exploitation) by some hostile person or
        influence; "he fell prey to muggers"; "everyone was fair
        game"; "the target of a manhunt" [syn: {prey}, {quarry}, {fair
        game}]
     3: the location of the target that is to be hit [syn: {target
        area}]
     4: sports equipment consisting of an object set up for a
        marksman or archer to aim at [syn: {butt}]
     5: the goal intended to be attained (and which is believed to
        be attainable); "the sole object of her trip was to ...
