---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Matthew_O._Jackson
offline_file: ""
offline_thumbnail: ""
uuid: 529c06f6-e0c5-466f-937f-19cea3e897f8
updated: 1484308513
title: Matthew O. Jackson
categories:
    - Game theorists
---
Matthew O. Jackson is the William D. Eberle Professor of Economics at Stanford University, an external faculty member of the Santa Fe Institute, and a fellow of CIFAR.[1] He received his Ph.D. from Stanford University in 1988. Jackson has been honored with the Social Choice and Welfare Prize,[2] the B.E.Press Arrow Prize for Senior Economists,[3] and a Guggenheim Fellowship.[4] He has served as co-editor of Games and Economic Behavior,[5] the Review of Economic Design,[6] and Econometrica.[7] Jackson co-teaches a popular game theory course on Coursera.org,[8] along with Kevin Leyton-Brown and Yoav Shoham.
