---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resigned
offline_file: ""
offline_thumbnail: ""
uuid: 0dd1914b-b5f2-495e-ac51-32a4aa38f2ab
updated: 1484310583
title: resigned
categories:
    - Dictionary
---
resigned
     adj 1: (followed by `to') having come to accept; "resigned to his
            fate" [syn: {resigned(p)}]
     2: showing utter resignation or hopelessness; "abject
        surrender" [syn: {abject}, {unhopeful}]
