---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curl
offline_file: ""
offline_thumbnail: ""
uuid: 9964b253-bc11-4269-a068-bf9e4bf7988d
updated: 1484310365
title: curl
categories:
    - Dictionary
---
curl
     n 1: a round shape formed by a series of concentric circles [syn:
           {coil}, {whorl}, {roll}, {curlicue}, {ringlet}, {gyre},
           {scroll}]
     2: American chemist who with Richard Smalley and Harold Kroto
        discovered fullerenes and opened a new branch of chemistry
        (born in 1933) [syn: {Robert Curl}, {Robert F. Curl}, {Robert
        Floyd Curl Jr.}]
     3: a strand or cluster of hair [syn: {lock}, {ringlet}, {whorl}]
     v 1: form a curl, curve, or kink; "the cigar smoke curled up at
          the ceiling" [syn: {curve}, {kink}]
     2: shape one's body into a curl; "She curled farther down under
        the covers"; "She fell and drew in" [syn: ...
