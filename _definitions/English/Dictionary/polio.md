---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polio
offline_file: ""
offline_thumbnail: ""
uuid: 8de1eb69-945a-47fb-937b-fb224f73a881
updated: 1484310160
title: polio
categories:
    - Dictionary
---
polio
     n : an acute viral disease marked by inflammation of nerve cells
         of the brain stem and spinal cord [syn: {poliomyelitis},
         {infantile paralysis}, {acute anterior poliomyelitis}]
