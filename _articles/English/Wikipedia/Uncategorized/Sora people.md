---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Saora
offline_file: ""
offline_thumbnail: ""
uuid: fafe921e-1576-4fe3-a771-6d9b76fd8e13
updated: 1484308456
title: Sora people
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Saura_Lady.JPG
tags:
    - Occupation
    - Religious beliefs
    - Social life
categories:
    - Uncategorized
---
The Sora (alternative names and spellings include Saora, Saura, Savara and Sabara) are a tribe from Southern Odisha, north coastal Andhra Pradesh in India. They are also found in the hills of Jharkhand, Madhya Pradesh and Maharashtra.
