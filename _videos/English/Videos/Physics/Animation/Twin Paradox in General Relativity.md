---
version: 1
type: video
provider: YouTube
id: bjHLboK2M1g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 441c9851-6d8f-4e71-9367-5b0761b8f213
updated: 1486069654
title: Twin Paradox in General Relativity
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/bjHLboK2M1g/default.jpg
    - https://i3.ytimg.com/vi/bjHLboK2M1g/1.jpg
    - https://i3.ytimg.com/vi/bjHLboK2M1g/2.jpg
    - https://i3.ytimg.com/vi/bjHLboK2M1g/3.jpg
---
