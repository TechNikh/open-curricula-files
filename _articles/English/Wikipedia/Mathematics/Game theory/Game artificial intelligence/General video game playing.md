---
version: 1
type: article
id: https://en.wikipedia.org/wiki/General_video_game_playing
offline_file: ""
offline_thumbnail: ""
uuid: 26d096ec-3f79-4520-b7a1-e11dcfda2e2a
updated: 1484309002
title: General video game playing
categories:
    - Game artificial intelligence
---
General video game playing (GVGP)[1] is the design of artificial intelligence programs to be able to play more than one video game successfully. In recent years, some progress have been made in this area, including programs that can learn to play Atari 2600 games[2][3][4][5] as well as a program that can learn to play NES games.[6][7][8]
