---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sense
offline_file: ""
offline_thumbnail: ""
uuid: 3b408527-dbe2-48fa-bd9c-9c00686b9019
updated: 1484310353
title: sense
categories:
    - Dictionary
---
sense
     n 1: a general conscious awareness; "a sense of security"; "a
          sense of happiness"; "a sense of danger"; "a sense of
          self"
     2: the meaning of a word or expression; the way in which a word
        or expression or situation can be interpreted; "the
        dictionary gave several senses for the word"; "in the best
        sense charity is really a duty"; "the signifier is linked
        to the signified" [syn: {signified}]
     3: the faculty through which the external world is apprehended;
        "in the dark he had to depend on touch and on his senses
        of smell and hearing" [syn: {sensation}, {sentience}, {sentiency},
         {sensory faculty}]
   ...
