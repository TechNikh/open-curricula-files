---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/newly
offline_file: ""
offline_thumbnail: ""
uuid: c235d0ff-566c-4d5c-8d50-ab6496d2dcc6
updated: 1484310413
title: newly
categories:
    - Dictionary
---
newly
     adv : very recently; "they are newly married"; "newly raised
           objections"; "a newly arranged hairdo"; "grass new
           washed by the rain"; "a freshly cleaned floor"; "we are
           fresh out of tomatoes" [syn: {recently}, {freshly}, {fresh},
            {new}]
