---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compressed
offline_file: ""
offline_thumbnail: ""
uuid: 4f5de476-8681-49a5-b19b-b1bb5f61c01f
updated: 1484310409
title: compressed
categories:
    - Dictionary
---
compressed
     adj 1: pressed tightly together; "with lips compressed" [syn: {tight}]
     2: reduced in volume by pressure; "compressed air"
     3: flattened laterally along the whole length (e.g., certain
        leafstalks or flatfishes) [syn: {flat}]
