---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yelled
offline_file: ""
offline_thumbnail: ""
uuid: 037bf9d4-959c-42e2-a8e1-aa418036e512
updated: 1484310144
title: yelled
categories:
    - Dictionary
---
yelled
     adj : in a vehement outcry; "his shouted words of encouragement
           could be heard over the crowd noises" [syn: {shouted}]
