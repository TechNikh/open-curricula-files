---
version: 1
type: video
provider: YouTube
id: SxZUFA2SGX8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Adding%20Decimals%20%28Old%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 62f9ef7c-8a2e-461d-9032-ce8f11d8b0ea
updated: 1486069571
title: Adding Decimals (Old)
tags:
    - Khan
    - Academy
    - math
    - Decimal
    - Addition
    - CC_5_NBT_7
    - CC_6_NS_3
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/SxZUFA2SGX8/default.jpg
    - https://i3.ytimg.com/vi/SxZUFA2SGX8/1.jpg
    - https://i3.ytimg.com/vi/SxZUFA2SGX8/2.jpg
    - https://i3.ytimg.com/vi/SxZUFA2SGX8/3.jpg
---
Adding decimals
