---
title: Alternative energy
article_ref: https://en.wikipedia.org/wiki/Alternative_energy
tags:
    - Definitions
    - History
    - Coal as an alternative to wood
    - Petroleum as an alternative to whale oil
    - ethanol as an alternative to fossil fuels
    - Coal gasification as an alternative to petroleum
    - Common types of alternative energy
    - Enabling technologies
    - Renewable energy vs non-renewable energy
    - Ecologically friendly alternatives
    - Relatively new concepts for alternative energy
    - Carbon-neutral and negative fuels
    - Algae fuel
    - Biomass briquettes
    - Biogas digestion
    - Biological hydrogen production
    - Hydroelectricity
    - Offshore wind
    - Marine and hydrokinetic energy
    - Thorium
    - Investing in alternative energy
    - Alternative energy in transportation
    - Making alternative energy mainstream
    - Research
    - Solar
    - Wind
    - Biomass
    - Ethanol biofuels
    - Other biofuels
    - geothermal
    - hydrogen
    - Disadvantages
categories:
    - Fossil Fuels
---
