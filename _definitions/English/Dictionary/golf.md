---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/golf
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484368741
title: golf
categories:
    - Dictionary
---
golf
     n : a game played on a large open course with 9 or 18 holes; the
         object is use as few strokes as possible in playing all
         the holes [syn: {golf game}]
     v : play golf
