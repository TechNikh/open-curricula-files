---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/representative
offline_file: ""
offline_thumbnail: ""
uuid: 31c68494-e696-402d-94ae-71d28f9c0e47
updated: 1484310299
title: representative
categories:
    - Dictionary
---
representative
     adj 1: serving to represent or typify; "representative moviegoers";
            "a representative modern play"
     2: standing for something else; "the bald eagle is
        representative of the United States" [ant: {nonrepresentative}]
     3: being or characteristic of government by representation in
        which citizens exercise power through elected officers and
        representatives; "representative government as defined by
        Abraham Lincoln is government of the people, by the
        people, for the people"
     n 1: a person who represents others
     2: an advocate who represents someone else's policy or purpose;
        "the meeting was attended by ...
