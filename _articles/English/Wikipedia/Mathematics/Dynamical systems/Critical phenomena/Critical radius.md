---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Critical_radius
offline_file: ""
offline_thumbnail: ""
uuid: 1f1b7530-7ed9-4ede-accc-db5da1b71439
updated: 1484308746
title: Critical radius
categories:
    - Critical phenomena
---
Critical radius is the minimum size that must be formed by atoms or molecules clustering together (in a gas, liquid or solid matrix) before a new-phase inclusion (a bubble, a droplet, or a solid particle) is stable and begins to grow. Formation of such stable "nuclei" is called nucleation.
