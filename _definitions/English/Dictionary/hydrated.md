---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydrated
offline_file: ""
offline_thumbnail: ""
uuid: 9b0d1f2c-6924-424e-b000-48c56a4bc34d
updated: 1484310381
title: hydrated
categories:
    - Dictionary
---
hydrated
     adj : containing combined water (especially water of
           crystallization as in a hydrate) [syn: {hydrous}] [ant:
            {anhydrous}]
