---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bent
offline_file: ""
offline_thumbnail: ""
uuid: 88056bbc-f2ff-4590-a72f-a331f2b90702
updated: 1484310220
title: bent
categories:
    - Dictionary
---
bend
     n 1: a circular segment of a curve; "a bend in the road"; "a
          crook in the path" [syn: {crook}, {turn}]
     2: movement that causes the formation of a curve [syn: {bending}]
     3: curved segment (of a road or river or railroad track etc.)
        [syn: {curve}]
     4: an angular or rounded shape made by folding; "a fold in the
        napkin"; "a crease in his trousers"; "a plication on her
        blouse"; "a flexure of the colon"; "a bend of his elbow"
        [syn: {fold}, {crease}, {plication}, {flexure}, {crimp}]
     5: a town in central Oregon at the eastern foot of the Cascade
        Range
     6: diagonal line traversing a shield from the upper right
        ...
