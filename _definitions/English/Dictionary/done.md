---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/done
offline_file: ""
offline_thumbnail: ""
uuid: 6ee6af39-758c-49d6-87b3-2d46efeb91fc
updated: 1484310313
title: done
categories:
    - Dictionary
---
done
     adj 1: having finished or arrived at completion; "certain to make
            history before he's done"; "it's a done deed"; "after
            the treatment, the patient is through except for
            follow-up"; "almost through with his studies" [syn: {through},
             {through with(p)}]
     2: cooked until ready to serve
