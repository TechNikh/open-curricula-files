---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plus
offline_file: ""
offline_thumbnail: ""
uuid: 88cb2eb0-0760-417b-ba1f-380c1aa59852
updated: 1484310228
title: plus
categories:
    - Dictionary
---
plus
     adj 1: on the positive side or higher end of a scale; "a plus
            value"; "temperature of plus 5 degrees"; "a grade of C
            plus" [ant: {minus}]
     2: involving advantage or good; "a plus (or positive) factor"
        [syn: {positive}]
     n 1: a useful or valuable quality [syn: {asset}] [ant: {liability}]
     2: the arithmetic operation of summing; calculating the sum of
        two or more numbers; "the summation of four and three
        gives seven"; "four plus three equals seven" [syn: {summation},
         {addition}]
