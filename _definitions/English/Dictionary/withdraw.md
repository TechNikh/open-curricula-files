---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/withdraw
offline_file: ""
offline_thumbnail: ""
uuid: b5830f20-bc6a-4809-b193-62fba6e726cc
updated: 1484310577
title: withdraw
categories:
    - Dictionary
---
withdraw
     v 1: pull back or move away or backward; "The enemy withdrew";
          "The limo pulled away from the curb" [syn: {retreat}, {pull
          away}, {draw back}, {recede}, {pull back}, {retire}, {move
          back}]
     2: withdraw from active participation; "He retired from chess"
        [syn: {retire}]
     3: release from something that holds fast, connects, or
        entangles; "I want to disengage myself from his
        influence"; "disengage the gears" [syn: {disengage}] [ant:
         {engage}]
     4: cause to be returned; "recall the defective auto tires";
        "The manufacturer tried to call back the spoilt yoghurt"
        [syn: {recall}, {call in}, {call ...
