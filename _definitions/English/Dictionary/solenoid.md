---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solenoid
offline_file: ""
offline_thumbnail: ""
uuid: b46e25fa-b845-461f-b591-b5cb152ae006
updated: 1484310365
title: solenoid
categories:
    - Dictionary
---
solenoid
     n : a coil of wire around an iron core; becomes a magnet when
         current passes through the coil
