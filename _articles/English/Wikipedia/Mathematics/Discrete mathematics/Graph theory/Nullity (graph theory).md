---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nullity_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: d4d1ae0b-db99-4cfb-a61a-e208094ca34a
updated: 1484308630
title: Nullity (graph theory)
categories:
    - Graph theory
---
The nullity of a graph in the mathematical subject of graph theory can mean either of two unrelated numbers. If the graph has n vertices and m edges, then the nullity can be:
