---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anhydrous
offline_file: ""
offline_thumbnail: ""
uuid: b2baec06-a5cc-47e3-9430-f3a359049548
updated: 1484310411
title: anhydrous
categories:
    - Dictionary
---
anhydrous
     adj : without water; especially without water of crystallization
           [ant: {hydrous}]
