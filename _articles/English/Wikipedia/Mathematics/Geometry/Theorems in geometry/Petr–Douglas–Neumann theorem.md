---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Petr%E2%80%93Douglas%E2%80%93Neumann_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 4fae5922-8ac8-4956-8f40-40f97aa59c86
updated: 1484309119
title: Petr–Douglas–Neumann theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-PDNTheoremForTriangle.svg.png
tags:
    - Statement of the theorem
    - Specialisation to triangles
    - Specialisation to quadrilaterals
    - >
        Construct A1 using apex angle π/2 and then A2 with apex
        angle π.
    - >
        Construct A1 using apex angle π and then A2 with apex angle
        π/2.
    - >
        Images illustrating application of the theorem to
        quadrilaterals
    - Specialisation to pentagons
    - Proof of the theorem
categories:
    - Theorems in geometry
---
In geometry, the Petr–Douglas–Neumann theorem (or the PDN-theorem) is a result concerning arbitrary planar polygons. The theorem asserts that a certain procedure when applied to an arbitrary polygon always yields a regular polygon having the same number of sides as the initial polygon. The theorem was first published by Karel Petr (1868–1950) of Prague in 1908.[1][2] The theorem was independently rediscovered by Jesse Douglas (1897–1965) in 1940[3] and also by B H Neumann (1909–2002) in 1941.[2][4] The naming of the theorem as Petr–Douglas–Neumann theorem, or as the PDN-theorem for short, is due to Stephen B Gray.[2] This theorem has also been called Douglas’s theorem, the ...
