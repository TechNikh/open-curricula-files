---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/command
offline_file: ""
offline_thumbnail: ""
uuid: 666a8510-ef48-457e-8627-cb97a90d5f58
updated: 1484310589
title: command
categories:
    - Dictionary
---
command
     n 1: an authoritative direction or instruction to do something
          [syn: {bid}, {bidding}, {dictation}]
     2: a military unit or region under the control of a single
        officer
     3: the power or authority to command; "an admiral in command"
     4: availability for use; "the materials at the command of the
        potters grew"
     5: a position of highest authority; "the corporation has just
        undergone a change in command"
     6: great skillfulness and knowledge of some subject or
        activity; "a good command of French" [syn: {control}, {mastery}]
     7: (computer science) a line of code written as part of a
        computer program [syn: ...
