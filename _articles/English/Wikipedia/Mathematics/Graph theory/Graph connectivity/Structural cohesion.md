---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Structural_cohesion
offline_file: ""
offline_thumbnail: ""
uuid: 6dde8344-a00b-4402-99d0-2bac534427c1
updated: 1484309404
title: Structural cohesion
tags:
    - Software
    - Examples
    - Perceived cohesion
categories:
    - Graph connectivity
---
Structural cohesion is the sociological conception[1][2] of a useful formal definition and measure of cohesion in social groups. It is defined as the minimal number of actors in a social network that need to be removed to disconnect the group. It is thus identical to the question of the node connectivity of a given graph. The vertex-cut version of Menger's theorem also proves that the disconnection number is equivalent to a maximally sized group with a network in which every pair of persons has at least this number of separate paths between them. It is also useful to know that k-cohesive graphs (or k-components) are always a subgraph of a k-core, although a k-core is not always k-cohesive. ...
