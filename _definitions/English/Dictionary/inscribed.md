---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inscribed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484364361
title: inscribed
categories:
    - Dictionary
---
inscribed
     adj 1: written (by handwriting, printing, engraving, or carving) on
            or in a surface
     2: cut or impressed into a surface; "an incised design";
        "engraved invitations" [syn: {engraved}, {etched}, {graven},
         {incised}]
