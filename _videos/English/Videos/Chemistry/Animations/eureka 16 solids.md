---
version: 1
type: video
provider: YouTube
id: 4TPV3V39PMI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/eureka%2016%20solids.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ea9c3f6f-7ca6-4fbb-9cea-307a10e529e0
updated: 1486069664
title: eureka 16 solids
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/4TPV3V39PMI/default.jpg
    - https://i3.ytimg.com/vi/4TPV3V39PMI/1.jpg
    - https://i3.ytimg.com/vi/4TPV3V39PMI/2.jpg
    - https://i3.ytimg.com/vi/4TPV3V39PMI/3.jpg
---
