---
version: 1
type: article
id: https://en.wikipedia.org/wiki/David_Eppstein
offline_file: ""
offline_thumbnail: ""
uuid: 9130040b-40a1-4888-88d0-855c33d8aec1
updated: 1484309461
title: David Eppstein
tags:
    - Biography
    - Research Interests
    - Other interests
    - Awards
    - Selected publications
    - Books
categories:
    - Graph theorists
---
David Arthur Eppstein (born 1963) is an American computer scientist and mathematician. He is a Chancellor's Professor of computer science at University of California, Irvine.[2] He is known for his work in computational geometry, graph algorithms, and recreational mathematics. In 2011, he was named an ACM Fellow.
