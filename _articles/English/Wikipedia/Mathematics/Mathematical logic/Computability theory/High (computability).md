---
version: 1
type: article
id: https://en.wikipedia.org/wiki/High_(computability)
offline_file: ""
offline_thumbnail: ""
uuid: 9162aed4-e517-4290-8767-b13a0609140a
updated: 1484309487
title: High (computability)
categories:
    - Computability theory
---
In computability theory, a Turing degree [X] is high if it is computable in 0′, and the Turing jump [X′] is 0′′, which is the greatest possible degree in terms of Turing reducibility for the jump of a set which is computable in 0′ (Soare 1987:71).
