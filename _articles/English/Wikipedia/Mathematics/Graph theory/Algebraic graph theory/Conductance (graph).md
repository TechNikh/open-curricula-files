---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conductance_(graph)
offline_file: ""
offline_thumbnail: ""
uuid: 1241913f-00a5-4fb2-a95c-e797d992f0e2
updated: 1484309371
title: Conductance (graph)
tags:
    - Generalizations and applications
    - Markov chains
categories:
    - Algebraic graph theory
---
In graph theory the conductance of a graph G=(V,E) measures how "well-knit" the graph is: it controls how fast a random walk on G converges to a uniform distribution. The conductance of a graph is often called the Cheeger constant of a graph as the analog of its counterpart in spectral geometry.[citation needed] Since electrical networks are intimately related to random walks with a long history in the usage of the term "conductance", this alternative name helps avoid possible confusion.
