---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/showing
offline_file: ""
offline_thumbnail: ""
uuid: 49cadf10-7bfe-4f3a-af6d-1e5d1ad31318
updated: 1484310319
title: showing
categories:
    - Dictionary
---
showing
     n 1: the display of a motion picture [syn: {screening}, {viewing}]
     2: something shown to the public; "the museum had many exhibits
        of oriental art" [syn: {display}, {exhibit}]
