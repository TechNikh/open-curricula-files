---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vena_cava
offline_file: ""
offline_thumbnail: ""
uuid: ad29dbed-c661-42ea-add3-30abe439e30a
updated: 1484308375
title: Venae cavae
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Relations_of_the_aorta%252C_trachea%252C_esophagus_and_other_heart_structures.png'
categories:
    - Uncategorized
---
The venae cavae (from the Latin for "hollow veins", singular "vena cava") are large veins (venous trunks) that return deoxygenated blood from the body into the heart. In humans they are called the superior vena cava and the inferior vena cava, and both empty into the right atrium.[1] They are located slightly off-center, toward the right side of the body.
