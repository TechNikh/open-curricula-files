---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Star_height_problem
offline_file: ""
offline_thumbnail: ""
uuid: 1c25b11a-3c79-4ed7-ac5c-248f91e279de
updated: 1484308662
title: Star height problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/He1523a_2.jpg
tags:
    - Families of regular languages with unbounded star height
    - Computing the star height of regular languages
categories:
    - Theorems in discrete mathematics
---
The star height problem in formal language theory is the question whether all regular languages can be expressed using regular expressions of limited star height, i.e. with a limited nesting depth of Kleene stars. Specifically, is a nesting depth of one always sufficient? If not, is there an algorithm to determine how many are required? The problem was raised by Eggan (1963).
