---
version: 1
type: article
id: https://en.wikipedia.org/wiki/George_R._Price
offline_file: ""
offline_thumbnail: ""
uuid: 111d31dc-25b2-48cc-88b0-a752d2bd9b63
updated: 1484309025
title: George R. Price
tags:
    - Early life
    - Career
    - Early career
    - 'To Britain[clarification needed]'
    - Conversion to Christianity
    - Other work in evolutionary theory
    - Helping the homeless
    - Death
    - Recognition
    - In media
    - Bibliography
categories:
    - Game theorists
---
Originally a physical chemist and later a science journalist, he moved to London in 1967, where he worked in theoretical biology at the Galton Laboratory, making three important contributions: first, rederiving W.D. Hamilton's work on kin selection with a new Price equation; second, introducing (with John Maynard Smith) the concept of the evolutionarily stable strategy (ESS), a central concept in game theory; and third, formalising Fisher's fundamental theorem of natural selection.
