---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/injustice
offline_file: ""
offline_thumbnail: ""
uuid: c996e013-05df-44d5-8530-9283bdf00087
updated: 1484310565
title: injustice
categories:
    - Dictionary
---
injustice
     n 1: an unjust act [syn: {unfairness}, {iniquity}]
     2: the practice of being unjust or unfair [syn: {unjustness}]
        [ant: {justice}]
