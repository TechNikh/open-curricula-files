---
version: 1
type: article
id: https://en.wikipedia.org/wiki/How_To_Pay_For_The_War
offline_file: ""
offline_thumbnail: ""
uuid: 5b75a204-ff5d-4867-b51c-06e9b63610b8
updated: 1484309063
title: How To Pay For The War
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-How_To_Pay_For_The_War%252C_book.JPG'
categories:
    - Strategy
---
How to Pay for the War: A radical plan for the Chancellor of the Exchequer, is a book by John Maynard Keynes, published in 1940 by Macmillan and Co., Ltd.. It is an application of Keynesian thinking and principles to a practical economic problem and a relatively late text. Keynes died in 1946.
