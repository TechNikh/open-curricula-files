---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Morning_glory
offline_file: ""
offline_thumbnail: ""
uuid: 0ced38e8-1989-43ae-baef-f538c2f53e49
updated: 1484308372
title: Morning glory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Morningglory-1.jpg
tags:
    - Habit
    - Cultivation
    - History
    - Culinary uses
    - Ethnobotany
    - Gallery
categories:
    - Uncategorized
---
Morning glory (also written as morning-glory[1]) is the common name for over 1,000 species of flowering plants in the family Convolvulaceae, whose current taxonomy and systematics are in flux. Morning glory species belong to many genera, some of which are:
