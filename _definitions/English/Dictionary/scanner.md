---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scanner
offline_file: ""
offline_thumbnail: ""
uuid: ac9b36c4-fb7a-487e-a632-2df225214a70
updated: 1484310369
title: scanner
categories:
    - Dictionary
---
scanner
     n 1: someone who scans verse to determine the number and prosodic
          value of the syllables
     2: an electronic device that generates a digital representation
        of an image for data input to a computer [syn: {digital
        scanner}, {image scanner}]
     3: a radar dish that rotates or oscillates in order to scan a
        broad area
     4: a radio receiver that that moves automatically across some
        selected range of frequencies looking for for some signal
        or condition; "they used scanners to monitor police radio
        channels" [syn: {electronic scanner}]
