---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mode
offline_file: ""
offline_thumbnail: ""
uuid: 8df74afe-ceb2-4e34-9b2d-2b5bb515f506
updated: 1484310273
title: mode
categories:
    - Dictionary
---
mode
     n 1: how something is done or how it happens; "her dignified
          manner"; "his rapid manner of talking"; "their nomadic
          mode of existence"; "in the characteristic New York
          style"; "a lonely way of life"; "in an abrasive fashion"
          [syn: {manner}, {style}, {way}, {fashion}]
     2: a particular functioning condition or arrangement; "switched
        from keyboard to voice mode"
     3: a classification of propositions on the basis of whether
        they claim necessity or possibility or impossibility [syn:
         {modality}]
     4: verb inflections that express how the action or state is
        conceived by the speaker [syn: {mood}, ...
