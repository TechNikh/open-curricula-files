---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/launch
offline_file: ""
offline_thumbnail: ""
uuid: a6430db4-b6ea-4273-a819-09b5eb20fd2c
updated: 1484310585
title: launch
categories:
    - Dictionary
---
launch
     n 1: a motorboat with an open deck or a half deck
     2: the act of propelling with force [syn: {launching}]
     v 1: set up or found; "She set up a literacy program" [syn: {establish},
           {set up}, {found}] [ant: {abolish}]
     2: propel with force; "launch the space shuttle"; "Launch a
        ship"
     3: launch for the first time; launch on a maiden voyage;
        "launch a ship"
     4: begin with vigor; "He launched into a long diatribe"; "She
        plunged into a dangerous adventure" [syn: {plunge}]
     5: get going; give impetus to; "launch a career"; "Her actions
        set in motion a complicated judicial process" [syn: {set
        in motion}]
     6: ...
