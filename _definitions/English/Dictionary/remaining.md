---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remaining
offline_file: ""
offline_thumbnail: ""
uuid: 304fc6f4-9929-424e-9fc4-f6a77681dc28
updated: 1484310319
title: remaining
categories:
    - Dictionary
---
remaining
     adj 1: being the remaining one or ones of several; "tried to sell
            the remaining books"
     2: not used up; "leftover meatloaf"; "she had a little money
        left over so she went to a movie"; "some odd dollars
        left"; "saved the remaining sandwiches for supper";
        "unexpended provisions" [syn: {leftover}, {left over(p)},
        {left(p)}, {odd}, {unexpended}]
