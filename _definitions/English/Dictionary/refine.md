---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refine
offline_file: ""
offline_thumbnail: ""
uuid: 3b2821fb-8d4f-4fcf-93bc-12e2c92600fd
updated: 1484310411
title: refine
categories:
    - Dictionary
---
refine
     v 1: improve or perfect by pruning or polishing; "refine one's
          style of writing" [syn: {polish}, {fine-tune}, {down}]
     2: make more complex, intricate, or richer; "refine a design or
        pattern" [syn: {complicate}, {rarify}, {elaborate}]
     3: treat or prepare so as to put in a usable condition; "refine
        paper stock"; "refine pig iron"; "refine oil"
     4: reduce to a fine, unmixed, or pure state; separate from
        extraneous matter or cleanse from impurities; "refine
        sugar" [syn: {rectify}]
     5: attenuate or reduce in vigor, strength, or validity by
        polishing or purifying; "many valuable nutrients are
        refined out of ...
