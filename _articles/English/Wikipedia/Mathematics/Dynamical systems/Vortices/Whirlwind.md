---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Whirlwind
offline_file: ""
offline_thumbnail: ""
uuid: e857ee54-2615-4397-a72e-24525bc696e6
updated: 1484308883
title: Whirlwind
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Whirlwind-001.jpg
tags:
    - Types
    - Formation
    - Duration
    - Associated weather
    - Similar phenomena
categories:
    - Vortices
---
A whirlwind is a weather phenomenon in which a vortex of wind (a vertically oriented rotating column of air) forms due to instabilities and turbulence created by heating and flow (current) gradients. Whirlwinds occur all over the world and in any season.
