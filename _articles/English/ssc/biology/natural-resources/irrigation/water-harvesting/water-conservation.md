---
title: Water conservation
article_ref: https://en.wikipedia.org/wiki/Water_conservation
tags:
    - Strategies
    - Social solutions
    - Household applications
    - Commercial applications
    - Agricultural applications
categories:
    - Water harvesting
---
