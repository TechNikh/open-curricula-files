---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/endeavour
offline_file: ""
offline_thumbnail: ""
uuid: bddac471-65f2-4d18-8564-d4fa2007e447
updated: 1484310176
title: endeavour
categories:
    - Dictionary
---
endeavour
     n 1: a purposeful or industrious undertaking (especially one that
          requires effort or boldness); "he had doubts about the
          whole enterprise" [syn: {enterprise}, {endeavor}]
     2: earnest and conscientious activity intended to do or
        accomplish something; "made an effort to cover all the
        reading material"; "wished him luck in his endeavor"; "she
        gave it a good try" [syn: {attempt}, {effort}, {endeavor},
         {try}]
     v : attempt by employing effort; "we endeavor to make our
         customers happy" [syn: {endeavor}, {strive}]
