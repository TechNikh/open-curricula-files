---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proboscis
offline_file: ""
offline_thumbnail: ""
uuid: d45a9fd3-4d0c-4fe7-826c-8c4a1dec62b6
updated: 1484310158
title: proboscis
categories:
    - Dictionary
---
proboscis
     n 1: the human nose (especially when it is large)
     2: a long flexible snout as of an elephant [syn: {trunk}]
     [also: {proboscides} (pl)]
