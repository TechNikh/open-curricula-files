---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/virgin
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484407561
title: virgin
categories:
    - Dictionary
---
virgin
     adj 1: being used or worked for the first time; "virgin wool"
     2: in a state of sexual virginity; "pure and vestal modesty";
        "a spinster or virgin lady"; "men have decreed that their
        women must be pure and virginal" [syn: {pure}, {vestal}, {virginal},
         {virtuous}]
     n 1: a person who has never had sex
     2: (astrology) a person who is born while the sun is in Virgo
        [syn: {Virgo}]
     3: the sixth sign of the zodiac; the sun is in this sign from
        about August 23 to September 22 [syn: {Virgo}, {Virgo the
        Virgin}]
