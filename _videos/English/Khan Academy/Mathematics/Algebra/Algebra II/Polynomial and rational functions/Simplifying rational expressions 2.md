---
version: 1
type: video
provider: YouTube
id: dstNU7It-Ro
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Polynomial%20and%20rational%20functions/Simplifying%20rational%20expressions%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3f779085-9f0c-4da5-a88c-30c2b1535b9f
updated: 1486069563
title: Simplifying rational expressions 2
categories:
    - Polynomial and rational functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/dstNU7It-Ro/default.jpg
    - https://i3.ytimg.com/vi/dstNU7It-Ro/1.jpg
    - https://i3.ytimg.com/vi/dstNU7It-Ro/2.jpg
    - https://i3.ytimg.com/vi/dstNU7It-Ro/3.jpg
---
