---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gasoline
offline_file: ""
offline_thumbnail: ""
uuid: a0281873-6f1a-4218-b028-8e629eac1aab
updated: 1484310424
title: gasoline
categories:
    - Dictionary
---
gasoline
     n : a volatile flammable mixture of hydrocarbons (hexane and
         heptane and octane etc.) derived from petroleum; used
         mainly as a fuel in internal-combustion engines [syn: {gasolene},
          {gas}, {petrol}]
