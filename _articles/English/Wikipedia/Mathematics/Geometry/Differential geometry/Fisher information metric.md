---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fisher_information_metric
offline_file: ""
offline_thumbnail: ""
uuid: ff4510f3-b024-4fd8-9a2f-076c02efc71a
updated: 1484309218
title: Fisher information metric
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Antennarius_striatus_1.jpg
tags:
    - Definition
    - Relation to the Kullback–Leibler divergence
    - Relation to Ruppeiner geometry
    - Change in entropy
    - Relation to the Jensen–Shannon divergence
    - As Euclidean metric
    - As Fubini–Study metric
    - Continuously-valued probabilities
categories:
    - Differential geometry
---
In information geometry, the Fisher information metric is a particular Riemannian metric which can be defined on a smooth statistical manifold, i.e., a smooth manifold whose points are probability measures defined on a common probability space. It can be used to calculate the informational difference between measurements.
