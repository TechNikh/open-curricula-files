---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Beed
offline_file: ""
offline_thumbnail: ""
uuid: 7570a5fe-e1ea-41c3-aa94-60bda12c75b7
updated: 1484308455
title: Beed
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kapildhara_in_Balaghat_range.jpg
tags:
    - Topography
    - Location
    - Climate
    - History
    - Historical accounts
    - Foundation and name
    - In mythology
    - Early history
    - 1600 to 1858
    - 1858 to Present
    - Demographics
    - Administration
    - culture
    - Economy
    - Education
    - Health
    - Sports
    - Media and communication
    - Issues and challenges in the 21st century
    - 'Historical buildings[33]'
    - Kankaleshwar Temple
    - Khandoba Temple
    - Jama Masjid (Grand Mosque)
    - Shahinshah Wali tomb
    - Mansur Shah tomb
    - Historic Gates
    - Khazana Well (Kajana Bavdi)
    - Chronology
categories:
    - Uncategorized
---
Beed (Marathi:बीड) is a city in central region of Maharashtra state in India. It is the administrative headquarters and the largest city with a population of 146,709 in Beed district.[2] Nearly 36% of the district’s urban population lives in the city alone. It has witnessed 6.1% population growth during 2001 – 2011 decade.
