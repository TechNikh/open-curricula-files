---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hold
offline_file: ""
offline_thumbnail: ""
uuid: dbd59ff3-f51e-4606-9c99-9edf221c46fc
updated: 1484310218
title: hold
categories:
    - Dictionary
---
hold
     n 1: the act of grasping; "he released his clasp on my arm"; "he
          has a strong grip for an old man"; "she kept a firm hold
          on the railing" [syn: {clasp}, {clench}, {clutch}, {clutches},
           {grasp}, {grip}]
     2: understanding of the nature or meaning or quality or
        magnitude of something; "he has a good grasp of accounting
        practices" [syn: {appreciation}, {grasp}]
     3: power by which something or someone is affected or
        dominated; "he has a hold over them"
     4: time during which some action is awaited; "instant replay
        caused too long a delay"; "he ordered a hold in the
        action" [syn: {delay}, {time lag}, ...
