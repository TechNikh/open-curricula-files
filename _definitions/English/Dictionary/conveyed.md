---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conveyed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484492041
title: conveyed
categories:
    - Dictionary
---
conveyed
     adj : sent or carried from one place to another; "during battle,
           messages conveyed by carrier pigeon got through more
           often than those sent by plane"
