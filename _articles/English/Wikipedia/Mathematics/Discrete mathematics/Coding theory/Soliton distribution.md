---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Soliton_distribution
offline_file: ""
offline_thumbnail: ""
uuid: 6c3872a8-a882-4979-b2ea-8cd88ad33fa0
updated: 1484308553
title: Soliton distribution
tags:
    - Ideal distribution
    - Robust distribution
categories:
    - Coding theory
---
A soliton distribution is a type of discrete probability distribution that arises in the theory of erasure correcting codes. A paper by Luby[1] introduced two forms of such distributions, the ideal soliton distribution and the robust soliton distribution.
