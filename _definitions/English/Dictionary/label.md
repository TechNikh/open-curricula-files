---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/label
offline_file: ""
offline_thumbnail: ""
uuid: bc2d60b8-13c4-4809-95a6-5e66abf5f511
updated: 1484310379
title: label
categories:
    - Dictionary
---
label
     n 1: a brief description given for purposes of identification;
          "the label Modern is applied to many different kinds of
          architecture"
     2: trade name of a company that produces musical recordings;
        "the artists and repertoire department of a recording
        label is responsible for finding new talent" [syn: {recording
        label}]
     3: a radioactive isotope that is used in a compound in order to
        trace the mechanism of a chemical reaction
     4: an identifying or descriptive marker that is attached to an
        object
     v 1: assign a label to; designate with a label; "These students
          were labelled `learning disabled'"
     ...
