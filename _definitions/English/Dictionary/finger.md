---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finger
offline_file: ""
offline_thumbnail: ""
uuid: 2de81c6c-c617-4376-81fe-f50989fa1285
updated: 1484310323
title: finger
categories:
    - Dictionary
---
finger
     n 1: any of the terminal members of the hand (sometimes excepting
          the thumb); "her fingers were long and thin"
     2: the length of breadth of a finger used as a linear measure
        [syn: {fingerbreadth}, {finger's breadth}, {digit}]
     3: the part of a glove that provides a covering for one of the
        fingers
     v 1: feel or handle with the fingers; "finger the binding of the
          book" [syn: {thumb}]
     2: examine by touch; "Feel this soft cloth!"; "The customer
        fingered the sweater" [syn: {feel}]
     3: search for on the computer; "I fingered my boss and found
        that he is not logged on in the afternoons"
     4: indicate the ...
