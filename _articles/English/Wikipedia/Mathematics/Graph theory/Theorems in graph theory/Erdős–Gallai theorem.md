---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Gallai_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 4930fcc7-8e05-4bd4-86ef-3503a514d544
updated: 1484309454
title: Erdős–Gallai theorem
tags:
    - Theorem statement
    - Proofs
    - Relation to integer partitions
    - Graphic sequences for other types of graphs
    - Stronger version
    - Generalization
categories:
    - Theorems in graph theory
---
The Erdős–Gallai theorem is a result in graph theory, a branch of combinatorial mathematics. It provides one of two known approaches solving the graph realization problem, i.e. it gives a necessary and sufficient condition for a finite sequence of natural numbers to be the degree sequence of a simple graph. A sequence obeying these conditions is called "graphic". The theorem was published in 1960 by Paul Erdős and Tibor Gallai, after whom it is named.
