---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/described
offline_file: ""
offline_thumbnail: ""
uuid: 83e91033-9172-46d8-a6f2-c063d6a07af6
updated: 1484310271
title: described
categories:
    - Dictionary
---
described
     adj : represented in words especially with sharpness and detail;
           "the vividly described wars"
