---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Emporer
offline_file: ""
offline_thumbnail: ""
uuid: dd58e358-db41-423e-8e0d-f642c289f138
updated: 1484308482
title: Emperor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Rimini083.jpg
tags:
    - Roman tradition
    - Roman Empire and Byzantine emperors
    - Classical antiquity
    - Byzantine period
    - Before the 4th Crusade
    - Latin emperors
    - After the 4th Crusade
    - Ottoman Empire
    - Holy Roman Empire
    - Austrian Empire
categories:
    - Uncategorized
---
An emperor (through Old French empereor from Latin: 'imperator'[1]) is a monarch, usually the sovereign ruler of an empire or another type of imperial realm. Empress, the female equivalent, may indicate an emperor's wife (empress consort), mother (empress dowager), or a woman who rules in her own right (empress regnant). Emperors are generally recognized to be of a higher honour and rank than kings. In Europe the title of Emperor was, since the Middle Ages, considered equal or almost equal in dignity to that of Pope, due to the latter's position as visible head of the Church and spiritual leader of Western Europe. The Emperor of Japan is the only currently reigning monarch whose title is ...
