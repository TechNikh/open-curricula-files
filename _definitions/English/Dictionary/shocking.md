---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shocking
offline_file: ""
offline_thumbnail: ""
uuid: 205bb28f-ad54-43e3-ae1a-52b311a633cd
updated: 1484310533
title: shocking
categories:
    - Dictionary
---
shocking
     adj 1: glaringly vivid and graphic; marked by sensationalism;
            "lurid details of the accident" [syn: {lurid}]
     2: giving offense to moral sensibilities and injurious to
        reputation; "scandalous behavior"; "the wicked rascally
        shameful conduct of the bankrupt"- Thackeray; "the most
        shocking book of its time" [syn: {disgraceful}, {scandalous},
         {shameful}]
