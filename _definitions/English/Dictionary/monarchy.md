---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monarchy
offline_file: ""
offline_thumbnail: ""
uuid: 27a05e36-7001-4bc5-a4b1-fb759772813a
updated: 1484310557
title: monarchy
categories:
    - Dictionary
---
monarchy
     n : an autocracy governed by a monarch who usually inherits the
         authority
