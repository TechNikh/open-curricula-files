---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oval_(projective_plane)
offline_file: ""
offline_thumbnail: ""
uuid: f35ea559-f4bd-44db-b2ff-49f444276693
updated: 1484309251
title: Oval (projective plane)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Oval-definition-en.svg.png
tags:
    - Definition of an oval
    - Examples
    - Conic sections
    - Ovals, which are not conics
    - Criteria for an oval to be a conic
    - Further results on ovals in finite planes
    - 'Desarguesian Case: PG(2,2h)'
    - Known Hyperovals in PG(2,2h)
    - Hyperovals in PG(2, q), q even, q ≤ 64
    - PG(2,16)
    - PG(2,32)
    - PG(2,64)
    - Abstract ovals
    - Notes
categories:
    - Incidence geometry
---
In projective geometry an oval is a circle-like pointset (curve) in a plane that is defined by incidence propertes. The standard examples are the nondegenerate conics. However, a conic is only defined in a pappian plane, whereas an oval may exist in any type of projective plane. In the literature, there are many criteria which imply that an oval is a conic, but there are many examples, both infinite and finite, of ovals in pappian planes which are not conics.
