---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Septentrional
offline_file: ""
offline_thumbnail: ""
uuid: 121b999c-1b48-4887-8e43-05f4c6397e44
updated: 1484309302
title: Septentrional
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Mallet_miniature_map_France_1687.jpg
tags:
    - Etymology
    - Usage
    - Notes
categories:
    - Orientation (geometry)
---
Septentrional, meaning "of the north", is a word rarely used in English, but is commonly used in Latin and in the Romance languages. The term septentrional usually is found on maps, mostly those made before 1700. Early maps of North America often refer to the northern- and northwestern-most unexplored areas of the continent as at the "Septentrional" and as "America Septentrionalis", sometimes with slightly varying spellings.[note 1]
