---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cramer%27s_theorem_(algebraic_curves)'
offline_file: ""
offline_thumbnail: ""
uuid: 61e72e46-288c-45cd-b6e0-071da0489397
updated: 1484309160
title: "Cramer's theorem (algebraic curves)"
tags:
    - Derivation of the formula
    - Degenerate cases
    - Restricted cases
categories:
    - Analytic geometry
---
In mathematics, Cramer's theorem on algebraic curves gives the necessary and sufficient number of points in the real plane falling on an algebraic curve to uniquely determine the curve in non-degenerate cases. This number is
