---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attraction
offline_file: ""
offline_thumbnail: ""
uuid: d7ba8e96-88ae-41c6-b0de-53fe24773ff5
updated: 1484310401
title: attraction
categories:
    - Dictionary
---
attraction
     n 1: the force by which one object attracts another [syn: {attractive
          force}] [ant: {repulsion}]
     2: an entertainment that is offered to the public
     3: the quality of arousing interest; being attractive or
        something that attracts; "her personality held a strange
        attraction for him" [syn: {attractiveness}]
     4: a characteristic that provides pleasure and attracts;
        "flowers are an attractor for bees" [syn: {attractor}, {attracter},
         {attractive feature}, {magnet}]
     5: an entertainer who attracts large audiences; "he was the
        biggest drawing card they had" [syn: {drawing card}, {draw},
         {attractor}, ...
