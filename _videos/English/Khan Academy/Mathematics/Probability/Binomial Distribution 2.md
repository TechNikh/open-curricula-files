---
version: 1
type: video
provider: YouTube
id: FI8xtVaI068
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Binomial%20Distribution%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3305aaea-af0c-438e-9d23-d54c32d4b33e
updated: 1486069688
title: Binomial Distribution 2
categories:
    - Probability
thumbnail_urls:
    - https://i3.ytimg.com/vi/FI8xtVaI068/default.jpg
    - https://i3.ytimg.com/vi/FI8xtVaI068/1.jpg
    - https://i3.ytimg.com/vi/FI8xtVaI068/2.jpg
    - https://i3.ytimg.com/vi/FI8xtVaI068/3.jpg
---
