---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cardinality_of_the_continuum
offline_file: ""
offline_thumbnail: ""
uuid: 54b70fa9-7edd-4c3b-97d8-95cbeb745d8c
updated: 1484309501
title: Cardinality of the continuum
tags:
    - Properties
    - Uncountability
    - Cardinal equalities
    - |
        Alternative explanation for
        
        
        
        
        
        c
        
        
        =
        
        2
        
        
        ℵ
        
        0
    - Beth numbers
    - The continuum hypothesis
    - Sets with cardinality of the continuum
    - Sets with greater cardinality
categories:
    - Set theory
---
In set theory, the cardinality of the continuum is the cardinality or “size” of the set of real numbers 
  
    
      
        
          R
        
      
    
    {\displaystyle \mathbb {R} }
  
, sometimes called the continuum. It is an infinite cardinal number and is denoted by 
  
    
      
        
          |
        
        
          R
        
        
          |
        
      
    
    {\displaystyle |\mathbb {R} |}
  
 or 
  
    
      
        
          
            c
          
        
      
    
    {\displaystyle {\mathfrak {c}}}
  
 (a lowercase fraktur script "c").
