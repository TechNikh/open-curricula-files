---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-satisfied
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484454301
title: self-satisfied
categories:
    - Dictionary
---
self-satisfied
     adj 1: marked by excessive complacency or self-satisfaction; "a
            smug glow of self-congratulation" [syn: {smug}]
     2: contented to a fault; "he had become complacent after years
        of success"; "his self-satisfied dignity" [syn: {complacent}]
