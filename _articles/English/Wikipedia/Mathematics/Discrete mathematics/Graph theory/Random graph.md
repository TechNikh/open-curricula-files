---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Random_graph
offline_file: ""
offline_thumbnail: ""
uuid: 9dfaec09-d1bc-4bcc-a5f7-441cf7f83eda
updated: 1484308627
title: Random graph
tags:
    - Random graph models
    - Terminology
    - Properties of random graphs
    - Coloring of Random Graphs
    - Random trees
    - Conditionally uniform random graphs
    - Interdependent graphs
    - History
categories:
    - Graph theory
---
In mathematics, random graph is the general term to refer to probability distributions over graphs. Random graphs may be described simply by a probability distribution, or by a random process which generates them.[1] The theory of random graphs lies at the intersection between graph theory and probability theory. From a mathematical perspective, random graphs are used to answer questions about the properties of typical graphs. Its practical applications are found in all areas in which complex networks need to be modeled – a large number of random graph models are thus known, mirroring the diverse types of complex networks encountered in different areas. In a mathematical context, random ...
