---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thymine
offline_file: ""
offline_thumbnail: ""
uuid: 6aa3a922-a4af-4ec2-a6e3-494778c0f105
updated: 1484308349
title: Thymine
tags:
    - Derivation
    - Theoretical aspects
categories:
    - Uncategorized
---
Thymine /ˈθaɪmᵻn/ (T, Thy) is one of the four nucleobases in the nucleic acid of DNA that are represented by the letters G–C–A–T. The others are adenine, guanine, and cytosine. Thymine is also known as 5-methyluracil, a pyrimidine nucleobase. In RNA, thymine is replaced by the nucleobase uracil. Thymine was first isolated (from calves' thymus glands) in 1893 by Albrecht Kossel and Albert Neumann.[1]
