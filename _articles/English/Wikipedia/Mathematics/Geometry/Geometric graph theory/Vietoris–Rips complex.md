---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Vietoris%E2%80%93Rips_complex'
offline_file: ""
offline_thumbnail: ""
uuid: bf9b5156-ecec-4167-b537-6165238cd317
updated: 1484309241
title: Vietoris–Rips complex
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-VR_complex.svg.png
tags:
    - History
    - Relation to Čech complex
    - Relation to unit disk graphs and clique complexes
    - Other results
    - Applications
    - Notes
categories:
    - Geometric graph theory
---
In topology, the Vietoris–Rips complex, also called the Vietoris complex or Rips complex, is an abstract simplicial complex that can be defined from any metric space M and distance δ by forming a simplex for every finite set of points that has diameter at most δ. That is, it is a family of finite subsets of M, in which we think of a subset of k points as forming a (k − 1)-dimensional simplex (an edge for two points, a triangle for three points, a tetrahedron for four points, etc.); if a finite set S has the property that the distance between every pair of points in S is at most δ, then we include S as a simplex in the complex.
