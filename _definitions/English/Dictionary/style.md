---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/style
offline_file: ""
offline_thumbnail: ""
uuid: a80d2dc9-275a-4c07-a6fd-f60c989d175f
updated: 1484310369
title: style
categories:
    - Dictionary
---
style
     n 1: a particular kind (as to appearance); "this style of shoe is
          in demand"
     2: how something is done or how it happens; "her dignified
        manner"; "his rapid manner of talking"; "their nomadic
        mode of existence"; "in the characteristic New York
        style"; "a lonely way of life"; "in an abrasive fashion"
        [syn: {manner}, {mode}, {way}, {fashion}]
     3: a way of expressing something (in language or art or music
        etc.) that is characteristic of a particular person or
        group of people or period; "all the reporters were
        expected to adopt the style of the newspaper" [syn: {expressive
        style}]
     4: distinctive ...
