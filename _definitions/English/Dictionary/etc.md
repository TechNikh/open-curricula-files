---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/etc
offline_file: ""
offline_thumbnail: ""
uuid: b8db4626-de7c-4c16-8f6a-6430cedabb9b
updated: 1484310355
title: etc
categories:
    - Dictionary
---
etc.
     adv : continuing in the same way [syn: {and so forth}, {and so on},
            {etcetera}]
