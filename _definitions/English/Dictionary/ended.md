---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ended
offline_file: ""
offline_thumbnail: ""
uuid: a3ce1869-9dd2-4f0c-8841-a424c68a3fb0
updated: 1484310553
title: ended
categories:
    - Dictionary
---
ended
     adj : having come or been brought to a conclusion; "the harvesting
           was complete"; "the affair is over, ended, finished";
           "the abruptly terminated interview" [syn: {complete}, {concluded},
            {over(p)}, {all over}, {terminated}]
