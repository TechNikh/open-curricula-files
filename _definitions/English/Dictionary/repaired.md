---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repaired
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484570521
title: repaired
categories:
    - Dictionary
---
repaired
     adj 1: mended or put in working order; "a reconditioned sewing
            machine"; "a repaired vacuum cleaner"; "the broken
            lock is now fixed" [syn: {reconditioned}, {fixed}]
     2: made ready for service [syn: {maintained}, {serviced}]
