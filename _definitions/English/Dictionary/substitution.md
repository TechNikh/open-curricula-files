---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/substitution
offline_file: ""
offline_thumbnail: ""
uuid: 2242b672-2184-4f5e-8c14-47158af586d6
updated: 1484310423
title: substitution
categories:
    - Dictionary
---
substitution
     n 1: an event in which one thing is substituted for another; "the
          replacement of lost blood by a transfusion of donor
          blood" [syn: {permutation}, {transposition}, {replacement},
           {switch}]
     2: the act of putting one thing or person in the place of
        another: "he sent Smith in for Jones but the substitution
        came too late to help" [syn: {exchange}, {commutation}]
