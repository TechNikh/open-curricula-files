---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dissociation_number
offline_file: ""
offline_thumbnail: ""
uuid: 1686e83d-ecda-43f5-b3fd-0b67d2fe05b8
updated: 1484309420
title: Dissociation number
categories:
    - Graph invariants
---
In the mathematical discipline of graph theory, a subset of vertices in a graph G is called dissociation if it induces a subgraph with maximum degree 1. The number of vertices in a maximum cardinality dissociation set in G is called the dissociation number of G, denoted by diss(G). The problem of computing diss(G) (dissociation number problem) was firstly studied by Yannakakis.[1][2] The problem is NP-hard even in the class of bipartite and planar graphs.[3]
