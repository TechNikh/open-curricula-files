---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quicklime
offline_file: ""
offline_thumbnail: ""
uuid: 02babca4-3d98-42de-9200-7e19af7eebf8
updated: 1484310369
title: quicklime
categories:
    - Dictionary
---
quicklime
     n : a white crystalline oxide used in the production of calcium
         hydroxide [syn: {calcium oxide}, {lime}, {calx}, {calcined
         lime}, {fluxing lime}, {unslaked lime}, {burnt lime}]
