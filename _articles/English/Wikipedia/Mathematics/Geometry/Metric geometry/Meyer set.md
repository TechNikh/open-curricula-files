---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Meyer_set
offline_file: ""
offline_thumbnail: ""
uuid: 39aabdc9-9966-4972-95e6-f12838530670
updated: 1484309269
title: Meyer set
categories:
    - Metric geometry
---
In mathematics, a Meyer set or almost lattice is a set relatively dense X of points in the Euclidean plane or a higher-dimensional Euclidean space such that its Minkowski difference with itself is uniformly discrete. Meyer sets have several equivalent characterizations; they are named after Yves Meyer, who studied them as a mathematical model for quasicrystals.[1][2]
