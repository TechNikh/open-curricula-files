---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/erect
offline_file: ""
offline_thumbnail: ""
uuid: 3ef0ab73-bc87-494f-8d77-5ba3c57f7969
updated: 1484310218
title: erect
categories:
    - Dictionary
---
erect
     adj 1: upright in position or posture; "an erect stature"; "erect
            flower stalks"; "for a dog, an erect tail indicates
            aggression"; "a column still vertical amid the ruins";
            "he sat bolt upright" [syn: {vertical}, {upright}]
            [ant: {unerect}]
     2: of sexual organs; stiff and rigid [syn: {tumid}]
     v 1: construct, build, or erect; "Raise a barn" [syn: {raise}, {rear},
           {set up}, {put up}] [ant: {level}]
     2: cause to rise up [syn: {rear}]
