---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/useful
offline_file: ""
offline_thumbnail: ""
uuid: d4ea7f79-bb96-43b3-8a03-16024fb84e2c
updated: 1484310307
title: useful
categories:
    - Dictionary
---
useful
     adj 1: being of use or service; "the girl felt motherly and
            useful"; "a useful job"; "a useful member of society"
            [syn: {utile}] [ant: {useless}]
     2: of great importance or use or service; "useful information";
        "valuable advice" [syn: {valuable}, {of value}]
     3: capable of being turned to use or account; "useful
        applications of calculus"
     4: having a useful function; "utilitarian steel tables" [syn: {utilitarian}]
