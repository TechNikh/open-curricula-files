---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tears
offline_file: ""
offline_thumbnail: ""
uuid: 8236312f-13c6-48f5-afb4-f7dd8ad644fa
updated: 1484310539
title: tears
categories:
    - Dictionary
---
tears
     n : the process of shedding tears (usually accompanied by sobs
         or other inarticulate sounds); "I hate to hear the crying
         of a child"; "she was in tears" [syn: {crying}, {weeping}]
