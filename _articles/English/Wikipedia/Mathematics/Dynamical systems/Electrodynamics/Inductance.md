---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inductance
offline_file: ""
offline_thumbnail: ""
uuid: cd8a2391-9ada-413c-86ab-07c04f1fe040
updated: 1484308760
title: Inductance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Mutually_inducting_inductors.PNG
tags:
    - Circuit analysis
    - "Derivation from Faraday's law of inductance"
    - Inductance and magnetic field energy
    - Coupled inductors and mutual inductance
    - Matrix representation
    - Equivalent circuit
    - Tuned transformer
    - Ideal transformers
    - Calculation techniques
    - Mutual inductance of two wire loops
    - Self-inductance of a wire loop
    - Method of images
    - Relation between inductance and capacitance
    - Self-inductance of simple electrical circuits in air
    - Inductance with physical symmetry
    - Inductance of a solenoid
    - Inductance of a coaxial line
    - Phasor circuit analysis and impedance
    - Nonlinear inductance
    - General references
categories:
    - Electrodynamics
---
In electromagnetism and electronics, inductance is the property of an electrical conductor by which a change in current through it induces an electromotive force in both the conductor itself[1] and in any nearby conductors by mutual inductance.[1]
