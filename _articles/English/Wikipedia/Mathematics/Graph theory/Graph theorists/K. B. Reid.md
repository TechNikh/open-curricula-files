---
version: 1
type: article
id: https://en.wikipedia.org/wiki/K._B._Reid
offline_file: ""
offline_thumbnail: ""
uuid: b22bc53c-6e5c-4646-973b-b04c00334f03
updated: 1484309473
title: K. B. Reid
categories:
    - Graph theorists
---
Kenneth Brooks Reid, Jr. is a graph theorist and the founder faculty (Head 1989) professor at California State University, San Marcos. He specializes in combinatorial mathematics. He is known for his work in tournaments, frequency partitions and aspects of voting theory. He is known (with E. T. Parker) on a disproof of a conjecture on tournaments by Erdős and Moser
