---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Slope
offline_file: ""
offline_thumbnail: ""
uuid: 95332907-d7e8-46cd-a252-2ba79c08c56d
updated: 1484309167
title: Slope
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Wiki_slope_in_2d.svg.png
tags:
    - Definition
    - Examples
    - Algebra and geometry
    - Examples
    - Slope of a road or railway
    - calculus
    - Other generalizations
categories:
    - Analytic geometry
---
In mathematics, the slope or gradient of a line is a number that describes both the direction and the steepness of the line.[1] Slope is often denoted by the letter m; there is no clear answer to the question why the letter m is used for slope, but it might be from the "m for multiple" in the equation of a straight line "y = mx + c".[2]
