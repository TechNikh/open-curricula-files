---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Andr%C3%A9_Sainte-Lagu%C3%AB'
offline_file: ""
offline_thumbnail: ""
uuid: 828f4cc8-cfa5-401d-8249-069fe0594e7b
updated: 1484309476
title: André Sainte-Laguë
categories:
    - Graph theorists
---
André Sainte-Laguë (French pronunciation: ​[ɑ̃.dʀe sɛ̃t.la.ɡy], 20 April 1882 – 18 January 1950) was a French mathematician who was a pioneer in the area of graph theory. His research on seat allocation methods (published in 1910) led to one being named after him, the Sainte-Laguë method. Also named after him is the Sainte-Laguë Index for measuring the proportionality of an electoral outcome.
