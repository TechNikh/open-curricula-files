---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conditional_entropy
offline_file: ""
offline_thumbnail: ""
uuid: 93ffc929-4894-4b54-9a83-d7d640cc02d1
updated: 1484308764
title: Conditional entropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/256px-Entropy-mutual-information-relative-entropy-relation-diagram.svg.png
tags:
    - Definition
    - Chain rule
    - "Bayes' rule"
    - Generalization to quantum theory
    - Other properties
categories:
    - Entropy and information
---
In information theory, the conditional entropy (or equivocation) quantifies the amount of information needed to describe the outcome of a random variable 
  
    
      
        Y
      
    
    {\displaystyle Y}
  
 given that the value of another random variable 
  
    
      
        X
      
    
    {\displaystyle X}
  
 is known. Here, information is measured in shannons, nats, or hartleys. The entropy of 
  
    
      
        Y
      
    
    {\displaystyle Y}
  
 conditioned on 
  
    
      
        X
      
    
    {\displaystyle X}
  
 is written as 
  
    
      
        H
        (
        Y
        
          |
        
        X
        )
      
    
    ...
