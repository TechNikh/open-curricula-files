---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/well-connected
offline_file: ""
offline_thumbnail: ""
uuid: 9890a92f-ae84-4695-a068-cbac7a508817
updated: 1484310518
title: well-connected
categories:
    - Dictionary
---
well-connected
     adj : connected by blood or close acquaintance with people of
           wealth or social position; "a well-connected Edinburgh
           family" [syn: {socially connected}]
