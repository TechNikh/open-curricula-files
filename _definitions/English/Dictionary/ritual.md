---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ritual
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484626682
title: ritual
categories:
    - Dictionary
---
ritual
     adj 1: of or relating to or characteristic of religious rituals;
            "ritual killing"
     2: of or relating to or employed in social rites or rituals; "a
        ritual dance of Haiti"; "sedate little colonial tribe with
        its ritual tea parties"- Nadine Gordimer
     n 1: any customary observance or practice [syn: {rite}]
     2: the prescribed procedure for conducting religious ceremonies
     3: stereotyped behavior
