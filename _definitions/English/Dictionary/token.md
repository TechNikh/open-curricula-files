---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/token
offline_file: ""
offline_thumbnail: ""
uuid: 4babddce-bab2-40f8-83d6-25f161ee005b
updated: 1484310154
title: token
categories:
    - Dictionary
---
token
     adj : insignificantly small; a matter of form only (`tokenish' is
           informal); "the fee was nominal"; "a token gesture of
           resistance"; "a tokenish gesture" [syn: {nominal}, {token(a)},
            {tokenish}]
     n 1: an individual instance of a type of symbol; "the word`error'
          contains three tokens of `r'" [syn: {item}]
     2: a metal or plastic disk that can be used (as a substitute
        for coins) in slot machines
     3: something of sentimental value [syn: {keepsake}, {souvenir},
         {relic}]
