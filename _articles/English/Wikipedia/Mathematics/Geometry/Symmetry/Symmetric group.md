---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_group
offline_file: ""
offline_thumbnail: ""
uuid: 89b69bbc-43af-4573-8ca9-4cd05b76c635
updated: 1484309317
title: Symmetric group
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Symmetric_group_4%253B_Cayley_graph_4%252C9.svg.png'
tags:
    - Definition and first properties
    - Applications
    - Elements
    - Multiplication
    - Verification of group axioms
    - Transpositions
    - Cycles
    - Special elements
    - Conjugacy classes
    - Low degree groups
    - Maps between symmetric groups
    - Properties
    - Relation with alternating group
    - Generators and relations
    - Subgroup structure
    - Normal subgroups
    - Maximal subgroups
    - Sylow subgroups
    - Transitive subgroups
    - Automorphism group
    - Homology
    - Representation theory
categories:
    - Symmetry
---
In abstract algebra, the symmetric group Sn on a finite set of n symbols is the group whose elements are all the permutation operations that can be performed on n distinct symbols, and whose group operation is the composition of such permutation operations, which are defined as bijective functions from the set of symbols to itself.[1] Since there are n! (n factorial) possible permutation operations that can be performed on a tuple composed of n symbols, it follows that the order (the number of elements) of the symmetric group Sn is n!.
