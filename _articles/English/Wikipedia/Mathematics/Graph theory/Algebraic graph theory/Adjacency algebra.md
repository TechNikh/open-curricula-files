---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adjacency_algebra
offline_file: ""
offline_thumbnail: ""
uuid: 329b7c8c-0497-4c1e-b290-b7521e079630
updated: 1484309371
title: Adjacency algebra
categories:
    - Algebraic graph theory
---
In algebraic graph theory, the adjacency algebra of a graph G is the algebra of polynomials in the adjacency matrix A(G) of the graph. It is an example of a matrix algebra and is the set of the linear combinations of powers of A.[1]
