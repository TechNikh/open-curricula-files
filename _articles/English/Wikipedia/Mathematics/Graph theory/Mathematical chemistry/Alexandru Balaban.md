---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alexandru_Balaban
offline_file: ""
offline_thumbnail: ""
uuid: 9bc0f856-0c29-405e-b43f-86c6333c90ce
updated: 1484309431
title: Alexandru Balaban
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Alexandru_T._Balaban.jpg
tags:
    - Early life and education
    - Career
    - Professional experience (full-time jobs)
    - Invited positions (not full-time jobs)
    - Research
    - Experimental organic chemistry
    - Theoretical chemistry
    - Graph theory
    - Scientific Publications
    - Memberships
    - Honors and awards
categories:
    - Mathematical chemistry
---
Alexandru T. Balaban (April 2, 1931 − ) is a chemist who significantly contributed to the fields of organic chemistry, theoretical chemistry, mathematical chemistry, and chemical graph theory.
