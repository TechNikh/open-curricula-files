---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supervised
offline_file: ""
offline_thumbnail: ""
uuid: ae272366-2fdb-4857-bd8b-8bcb61ec10f7
updated: 1484310429
title: supervised
categories:
    - Dictionary
---
supervised
     adj : under observation or under the direction of a superintendent
           or overseer; "supervised play" [ant: {unsupervised}]
