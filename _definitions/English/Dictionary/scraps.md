---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scraps
offline_file: ""
offline_thumbnail: ""
uuid: 168b50d8-d157-4064-ab7a-168d4816642c
updated: 1484310537
title: scraps
categories:
    - Dictionary
---
scraps
     n : food that is discarded (as from a kitchen) [syn: {garbage},
         {refuse}, {food waste}]
