---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Domino_tiling
offline_file: ""
offline_thumbnail: ""
uuid: 02a4fb37-d255-4826-b8a3-cb74c5542b0e
updated: 1484308583
title: Domino tiling
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pavage_domino.svg.png
tags:
    - Height functions
    - "Thurston's height condition"
    - Counting tilings of regions
    - Tatami
categories:
    - Combinatorics
---
In geometry, a domino tiling of a region in the Euclidean plane is a tessellation of the region by dominos, shapes formed by the union of two unit squares meeting edge-to-edge. Equivalently, it is a perfect matching in the grid graph formed by placing a vertex at the center of each square of the region and connecting two vertices when they correspond to adjacent squares.
