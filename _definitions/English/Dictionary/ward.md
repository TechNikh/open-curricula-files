---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ward
offline_file: ""
offline_thumbnail: ""
uuid: 76db17f6-63da-4bb9-ad84-c80483d27c9d
updated: 1484310166
title: ward
categories:
    - Dictionary
---
ward
     n 1: a person who is under the protection or in the custody of
          another
     2: a district into which a city or town is divided for the
        purpose of administration and elections
     3: block forming a division of a hospital (or a suite of rooms)
        shared by patients who need a similar kind of care; "they
        put her in a 4-bed ward" [syn: {hospital ward}]
     4: English economist and conservationist (1914-1981) [syn: {Barbara
        Ward}, {Baroness Jackson of Lodsworth}]
     5: English writer of novels who was an active opponent of the
        women's suffrage movement (1851-1920) [syn: {Mrs. Humphrey
        Ward}, {Mary Augusta Arnold Ward}]
     6: ...
