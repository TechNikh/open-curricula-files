---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tempest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484466181
title: tempest
categories:
    - Dictionary
---
tempest
     n 1: a violent commotion or disturbance; "the storms that had
          characterized their relationship had died away"; "it was
          only a tempest in a teapot" [syn: {storm}]
     2: (literary) a violent wind; "a tempest swept over the island"
