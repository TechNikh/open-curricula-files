---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/well-off
offline_file: ""
offline_thumbnail: ""
uuid: 8b7ddb49-bd77-48a3-b6e0-c26adf9f6bf9
updated: 1484310529
title: well-off
categories:
    - Dictionary
---
well-off
     adj 1: in fortunate circumstances financially; moderately rich;
            "they were comfortable or even wealthy by some
            standards"; "easy living"; "a prosperous family"; "his
            family is well-situated financially"; "well-to-do
            members of the community" [syn: {comfortable}, {easy},
             {prosperous}, {well-fixed}, {well-heeled}, {well-situated},
             {well-to-do}]
     2: fortunately situated; "doesn't know when he's well-off"
