---
version: 1
type: video
provider: YouTube
id: PddbEVNMgTY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Three%20pointer%20vs%20free%20throwing%20probability.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 64215a5e-514d-424b-bbc8-d27780988b6f
updated: 1486069688
title: Three pointer vs free throwing probability
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/PddbEVNMgTY/default.jpg
    - https://i3.ytimg.com/vi/PddbEVNMgTY/1.jpg
    - https://i3.ytimg.com/vi/PddbEVNMgTY/2.jpg
    - https://i3.ytimg.com/vi/PddbEVNMgTY/3.jpg
---
