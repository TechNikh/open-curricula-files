---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/safeguard
offline_file: ""
offline_thumbnail: ""
uuid: b9aec944-6793-462f-b752-d11ecda63650
updated: 1484310486
title: safeguard
categories:
    - Dictionary
---
safeguard
     n 1: a precautionary measure warding off impending danger or
          damage or injury etc.; "he put an ice pack on the injury
          as a precaution"; "an insurance policy is a good
          safeguard"; "we let our guard down" [syn: {precaution},
          {guard}]
     2: a document or escort providing safe passage through a region
        especially in time of war [syn: {safe-conduct}]
     v 1: make safe
     2: escort safely
