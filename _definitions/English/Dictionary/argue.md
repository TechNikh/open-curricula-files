---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/argue
offline_file: ""
offline_thumbnail: ""
uuid: 767181d3-373a-4656-9e4c-b8a98cad6aff
updated: 1484310529
title: argue
categories:
    - Dictionary
---
argue
     v 1: present reasons and arguments [syn: {reason}]
     2: have an argument about something [syn: {contend}, {debate},
        {fence}]
     3: give evidence of; "The evidence argues for your claim"; "The
        results indicate the need for more work" [syn: {indicate}]
