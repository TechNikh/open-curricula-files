---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tank_blanketing
offline_file: ""
offline_thumbnail: ""
uuid: f7226e71-4370-4fe3-ae17-60aa812173e8
updated: 1484308743
title: Tank blanketing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/AMISOM_T-55.jpg
tags:
    - Methods
    - Common practices
    - External sources
categories:
    - Control engineering
---
Tank blanketing, also referred to as tank padding, is the process of applying a gas to the empty space in a storage container. The term storage container here refers to any container that is used to store products, regardless of its size. Though tank blanketing is used for a variety of reasons, it typically involves using a buffer gas to protect products inside the storage container. A few of the benefits of blanketing include a longer life of the product in the container, reduced hazards, and longer equipment life cycles.
