---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cayleyan
offline_file: ""
offline_thumbnail: ""
uuid: 382292e8-cdfc-4544-927c-e2ebf767bd5f
updated: 1484309138
title: Cayleyan
categories:
    - Algebraic geometry
---
In algebraic geometry, the Cayleyan is a variety associated to a hypersurface by Cayley (1844), who named it the pippian in (Cayley 1857) and also called it the Steiner–Hessian.
