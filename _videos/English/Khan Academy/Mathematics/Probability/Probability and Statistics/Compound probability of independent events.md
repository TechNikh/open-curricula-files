---
version: 1
type: video
provider: YouTube
id: xSc4oLA9e8o
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Compound%20probability%20of%20independent%20events.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 966a8d18-8527-4f82-90cb-ae565f64a5d2
updated: 1486069688
title: Compound probability of independent events
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/xSc4oLA9e8o/default.jpg
    - https://i3.ytimg.com/vi/xSc4oLA9e8o/1.jpg
    - https://i3.ytimg.com/vi/xSc4oLA9e8o/2.jpg
    - https://i3.ytimg.com/vi/xSc4oLA9e8o/3.jpg
---
