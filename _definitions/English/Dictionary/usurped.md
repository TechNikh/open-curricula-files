---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/usurped
offline_file: ""
offline_thumbnail: ""
uuid: 92badf42-e348-4b2d-92fd-77a0b622ecbb
updated: 1484310571
title: usurped
categories:
    - Dictionary
---
usurped
     adj : (used especially of the rights of another) seized and held
           by force; "the usurped crown sat heavy on his head"
