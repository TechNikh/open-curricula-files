---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indirectly
offline_file: ""
offline_thumbnail: ""
uuid: 5664700e-07ad-44c9-b4ed-4bd9b2bdd67e
updated: 1484310401
title: indirectly
categories:
    - Dictionary
---
indirectly
     adv : not in a forthright manner; "he answered very indirectly"
           [ant: {directly}]
