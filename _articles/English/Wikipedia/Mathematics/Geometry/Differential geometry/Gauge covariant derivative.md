---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gauge_covariant_derivative
offline_file: ""
offline_thumbnail: ""
uuid: 486b296d-32b0-4872-8347-ccd94cc764d7
updated: 1484309219
title: Gauge covariant derivative
tags:
    - Introduction
    - Fluid dynamics
    - Gauge theory
    - >
        Construction of the covariant derivative through the
        comparator
    - >
        Construction of the covariant derivative through Gauge
        covariance requirement
    - Quantum electrodynamics
    - Quantum chromodynamics
    - Standard Model
    - General relativity
categories:
    - Differential geometry
---
The gauge covariant derivative is a variation of the covariant derivative used in general relativity. If a theory has gauge transformations, it means that some physical properties of certain equations are preserved under those transformations. Likewise, the gauge covariant derivative is the ordinary derivative modified in such a way as to make it behave like a true vector operator, so that equations written using the covariant derivative preserve their physical properties under gauge transformations.
