---
version: 1
type: video
provider: YouTube
id: GNeidvTGiY8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1c6e43f1-3f4b-4133-a381-3f5ac95fdf21
updated: 1486069643
title: 'Communication Craze in a Morphing World - Mandelbulb 3d fractal animation HD'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/GNeidvTGiY8/default.jpg
    - https://i3.ytimg.com/vi/GNeidvTGiY8/1.jpg
    - https://i3.ytimg.com/vi/GNeidvTGiY8/2.jpg
    - https://i3.ytimg.com/vi/GNeidvTGiY8/3.jpg
---
