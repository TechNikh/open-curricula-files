---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contact
offline_file: ""
offline_thumbnail: ""
uuid: b876d29a-fcb1-4d82-8aa8-9e73c9495708
updated: 1484310230
title: contact
categories:
    - Dictionary
---
contact
     n 1: close interaction; "they kept in daily contact"; "they
          claimed that they had been in contact with
          extraterrestrial beings"
     2: the state or condition of touching or of being in immediate
        proximity; "litmus paper turns red on contact with an
        acid"
     3: the act of touching physically; "her fingers came in contact
        with the light switch"
     4: the physical coming together of two or more things; "contact
        with the pier scraped paint from the hull" [syn: {impinging},
         {striking}]
     5: a person who is in a position to give you special
        assistance; "he used his business contacts to get an
        ...
