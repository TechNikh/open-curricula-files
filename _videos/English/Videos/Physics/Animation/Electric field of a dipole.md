---
version: 1
type: video
provider: YouTube
id: bG9XSY8i_q8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Electric%20field%20of%20a%20dipole.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6bbefef6-c5ba-4d26-8ea1-0c47a4d60ef0
updated: 1486069653
title: Electric field of a dipole
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/bG9XSY8i_q8/default.jpg
    - https://i3.ytimg.com/vi/bG9XSY8i_q8/1.jpg
    - https://i3.ytimg.com/vi/bG9XSY8i_q8/2.jpg
    - https://i3.ytimg.com/vi/bG9XSY8i_q8/3.jpg
---
