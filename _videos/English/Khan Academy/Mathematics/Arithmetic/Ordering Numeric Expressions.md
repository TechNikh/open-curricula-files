---
version: 1
type: video
provider: YouTube
id: w7k1YEcAaaE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Ordering%20Numeric%20Expressions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f393d621-5b90-4bf7-a041-b4395113ac63
updated: 1486069569
title: Ordering Numeric Expressions
tags:
    - Arithmetic
    - Ordering
    - numeric
    - expressions
    - CC_6_NS_7
    - CC_6_NS_7_b
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/w7k1YEcAaaE/default.jpg
    - https://i3.ytimg.com/vi/w7k1YEcAaaE/1.jpg
    - https://i3.ytimg.com/vi/w7k1YEcAaaE/2.jpg
    - https://i3.ytimg.com/vi/w7k1YEcAaaE/3.jpg
---
Converting percentages and fractions to decimals to order numbers
