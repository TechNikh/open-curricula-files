---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Projected_tolerance_zone
offline_file: ""
offline_thumbnail: ""
uuid: 60e7ad83-1bb0-45f5-b860-3e94f94f9a17
updated: 1484309331
title: Projected tolerance zone
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Gd%2526t_projectedtolerancezone.png'
categories:
    - Technical drawing
---
In geometric dimensioning and tolerancing, a projected tolerance zone is defined to predict the final dimensions and locations of features on a component or assembly subject to tolerance stack-up.
