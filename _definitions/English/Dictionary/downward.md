---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/downward
offline_file: ""
offline_thumbnail: ""
uuid: 851ab11b-1178-4b32-87b5-c093b92220c9
updated: 1484310365
title: downward
categories:
    - Dictionary
---
downward
     adj 1: on or toward a surface regarded as a base; "he lay face
            downward"; "the downward pull of gravity" [syn: {downward(ip)}]
     2: extending or moving from a higher to a lower place; "the
        down staircase"; "the downward course of the stream" [syn:
         {down(a)}, {downward(a)}]
     adv : spatially or metaphorically from a higher to a lower level
           or position; "don't fall down"; "rode the lift up and
           skied down"; "prices plunged downward" [syn: {down}, {downwards},
            {downwardly}] [ant: {up}, {up}, {up}, {up}]
