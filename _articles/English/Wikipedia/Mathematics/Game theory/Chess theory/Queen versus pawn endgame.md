---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Queen_versus_pawn_endgame
offline_file: ""
offline_thumbnail: ""
uuid: 79ad4469-2c1d-4d31-9794-fcac3a83e131
updated: 1484309022
title: Queen versus pawn endgame
tags:
    - Queen versus a pawn on the sixth rank
    - Exceptions
    - Example
    - Example from game
    - Queen versus a pawn on the seventh rank
    - Central pawn or knight pawn
    - Rook pawn
    - Bishop pawn
    - Example from game
    - Petrosian versus Fischer
    - Traps
    - Extra pawn for the defense
    - Notes
categories:
    - Chess theory
---
The chess endgame of a queen versus pawn (with both sides having no other pieces except the kings) is usually an easy win for the side with the queen. However, if the pawn has advanced to its seventh rank it has possibilities of reaching a draw, and there are some drawn positions with the pawn on the sixth rank. This endgame arises most often from a race of pawns to promote.
