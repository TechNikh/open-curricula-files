---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dine
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484623261
title: dine
categories:
    - Dictionary
---
dine
     v 1: have supper; eat dinner; "We often dine with friends in this
          restaurant"
     2: give dinner to; host for dinner; "I'm wining and dining my
        friends"
