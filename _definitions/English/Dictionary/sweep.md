---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sweep
offline_file: ""
offline_thumbnail: ""
uuid: d3c55bff-7ac6-4eb1-becf-31d227c76951
updated: 1484310192
title: sweep
categories:
    - Dictionary
---
sweep
     n 1: a wide scope; "the sweep of the plains" [syn: {expanse}]
     2: someone who cleans soot from chimneys [syn: {chimneysweeper},
         {chimneysweep}]
     3: winning all or all but one of the tricks in bridge [syn: {slam}]
     4: a long oar used in an open boat [syn: {sweep oar}]
     5: (American football) an attempt to advance the ball by
        running around the end of the line [syn: {end run}]
     6: a movement in an arc; "a sweep of his arm"
     v 1: sweep across or over; "Her long skirt brushed the floor"; "A
          gasp swept cross the audience" [syn: {brush}]
     2: move with sweeping, effortless, gliding motions; "The diva
        swept into the room"; ...
