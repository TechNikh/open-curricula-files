---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/internally
offline_file: ""
offline_thumbnail: ""
uuid: 04ee373b-55fb-479c-8056-fa6086a1eb79
updated: 1484310605
title: internally
categories:
    - Dictionary
---
internally
     adv : on or from the inside; "an internally controlled
           environment" [ant: {externally}]
