---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Iodine_solution
offline_file: ""
offline_thumbnail: ""
uuid: 937a44cd-b32d-4216-a9cc-02028dfa5ae7
updated: 1484308375
title: Tincture of iodine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Epidural.JPG
tags:
    - USP formulas
    - Usage
categories:
    - Uncategorized
---
Tincture of iodine or iodine tincture is an antiseptic, it is also called weak iodine solution. It is usually 2–7% elemental iodine, along with potassium iodide or sodium iodide, dissolved in a mixture of ethanol and water. Tincture solutions are characterized by the presence of alcohol. It was used from 1908 in pre-operative skin preparation by surgeon Antonio Grossich.[1]
