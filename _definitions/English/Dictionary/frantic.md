---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frantic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484579461
title: frantic
categories:
    - Dictionary
---
frantic
     adj 1: excessively agitated; transported with rage or other violent
            emotion; "frantic with anger and frustration";
            "frenetic screams followed the accident"; "a frenzied
            look in his eye" [syn: {frenetic}, {phrenetic}, {frenzied}]
     2: marked by uncontrolled excitement or emotion; "a crowd of
        delirious baseball fans"; "something frantic in their
        gaiety"; "a mad whirl of pleasure" [syn: {delirious}, {excited},
         {mad}, {unrestrained}]
