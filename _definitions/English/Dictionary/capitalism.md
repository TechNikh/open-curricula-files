---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/capitalism
offline_file: ""
offline_thumbnail: ""
uuid: 8f218cd2-8056-41fe-aead-18eb49a92a7f
updated: 1484310549
title: capitalism
categories:
    - Dictionary
---
capitalism
     n : an economic system based on private ownership of capital
         [syn: {capitalist economy}] [ant: {socialism}]
