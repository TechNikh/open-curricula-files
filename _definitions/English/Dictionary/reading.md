---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reading
offline_file: ""
offline_thumbnail: ""
uuid: 5de3389d-8cb9-489c-8b40-0bd85bd8a91c
updated: 1484310230
title: reading
categories:
    - Dictionary
---
reading
     n 1: the cognitive process of understanding a written linguistic
          message; "he enjoys reading books"
     2: a datum about some physical state that is presented to a
        user by a meter or similar instrument; "he could not
        believe the meter reading"; "the barometer gave clear
        indications of an approaching storm" [syn: {meter reading},
         {indication}]
     3: a particular interpretation or performance; "on that reading
        it was an insult"; "he was famous for his reading of
        Mozart"
     4: written material intended to be read; "the teacher assigned
        new readings"; "he bought some reading material at the
        airport" ...
