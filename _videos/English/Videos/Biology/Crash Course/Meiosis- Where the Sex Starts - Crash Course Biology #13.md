---
version: 1
type: video
provider: YouTube
id: qCLmR9-YY7o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ee6d9b25-cd81-40ef-b274-b8f27e190e63
updated: 1486069678
title: 'Meiosis- Where the Sex Starts - Crash Course Biology #13'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/qCLmR9-YY7o/default.jpg
    - https://i3.ytimg.com/vi/qCLmR9-YY7o/1.jpg
    - https://i3.ytimg.com/vi/qCLmR9-YY7o/2.jpg
    - https://i3.ytimg.com/vi/qCLmR9-YY7o/3.jpg
---
