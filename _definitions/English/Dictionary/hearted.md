---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hearted
offline_file: ""
offline_thumbnail: ""
uuid: 9a8b1de3-e35d-4eaf-b98b-7831048bd961
updated: 1484310607
title: hearted
categories:
    - Dictionary
---
hearted
     adj : (used only in combination) having a heart as specified;
           "gave pleasure to lighter-hearted members of the staff"
           [ant: {heartless}]
