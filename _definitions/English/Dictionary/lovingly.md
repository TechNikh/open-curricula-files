---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lovingly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484632981
title: lovingly
categories:
    - Dictionary
---
lovingly
     adv : with fondness; with love; "she spoke to her children fondly"
           [syn: {fondly}]
