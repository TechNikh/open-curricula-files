---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/broke
offline_file: ""
offline_thumbnail: ""
uuid: 19e1a7bf-3cac-478a-a7d5-2d02cc9a2518
updated: 1484310391
title: broke
categories:
    - Dictionary
---
broke
     adj : lacking funds; "`skint' is a British slang term" [syn: {bust},
            {skint}, {stone-broke}, {stony-broke}]
