---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/afternoon
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484479081
title: afternoon
categories:
    - Dictionary
---
afternoon
     n 1: the part of the day between noon and evening; "he spent a
          quiet afternoon in the park"
     2: a conventional expression of greeting or farewell [syn: {good
        afternoon}]
