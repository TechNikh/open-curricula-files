---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contention
offline_file: ""
offline_thumbnail: ""
uuid: 92d3995c-008b-4f18-98e0-c6b91b93e66a
updated: 1484310178
title: contention
categories:
    - Dictionary
---
contention
     n 1: a point asserted as part of an argument
     2: a contentious speech act; a dispute where there is strong
        disagreement; "they were involved in a violent argument"
        [syn: {controversy}, {contestation}, {disputation}, {disceptation},
         {tilt}, {argument}, {arguing}]
     3: the act of competing as for profit or a prize; "the teams
        were in fierce contention for first place" [syn: {competition},
         {rivalry}] [ant: {cooperation}]
