---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/landscape
offline_file: ""
offline_thumbnail: ""
uuid: 4ee669de-bd0d-4e20-a191-cf631d407a42
updated: 1484310242
title: landscape
categories:
    - Dictionary
---
landscape
     n 1: an expanse of scenery that can be seen in a single view
     2: painting depicting an expanse of natural scenery
     3: a genre of art dealing with the depiction of natural scenery
        [syn: {landscape painting}]
     4: an extensive mental viewpoint; "the political landscape
        looks bleak without a change of administration"; "we
        changed the landscape for solving the proble of payroll
        inequity"
     v 1: embellish with plants; "Let's landscape the yard"
     2: do landscape gardening; "My sons landscapes for corporations
        and earns a good living"
