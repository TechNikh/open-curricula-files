---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/occasional
offline_file: ""
offline_thumbnail: ""
uuid: 6a5fe50e-e881-459e-8e8a-84d4d81c01a8
updated: 1484310327
title: occasional
categories:
    - Dictionary
---
occasional
     adj 1: occurring from time to time; "took an occasional glass of
            wine" [syn: {occasional(a)}]
     2: occurring or appearing at usually irregular intervals;
        "episodic in his affections"; "occasional headaches" [syn:
         {episodic}]
     3: employed in a specified capacity from time to time; "casual
        employment"; "a casual correspondence with a former
        teacher"; "an occasional worker" [syn: {casual}]
