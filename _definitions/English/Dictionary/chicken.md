---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chicken
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484407021
title: chicken
categories:
    - Dictionary
---
chicken
     adj : easily frightened [syn: {chickenhearted}, {lily-livered}, {white-livered},
            {yellow}, {yellow-bellied}]
     n 1: the flesh of a chicken used for food [syn: {poulet}, {volaille}]
     2: a domestic fowl bred for flesh or eggs; believed to have
        been developed from the red jungle fowl [syn: {Gallus
        gallus}]
     3: a person who lacks confidence, is irresolute and wishy-washy
        [syn: {wimp}, {crybaby}]
     4: a foolhardy competition; a dangerous activity that is
        continued until one competitor becomes afraid and stops
