---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/robert
offline_file: ""
offline_thumbnail: ""
uuid: bdd68140-70c5-4f60-9154-6f26123f32bd
updated: 1484310393
title: robert
categories:
    - Dictionary
---
Robert
     n : United States parliamentary authority and author (in 1876)
         of Robert's Rules of Order (1837-1923) [syn: {Henry M.
         Robert}, {Henry Martyn Robert}]
