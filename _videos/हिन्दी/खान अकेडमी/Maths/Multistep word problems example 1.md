---
version: 1
type: video
provider: YouTube
id: 63i6YVYk7L8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 453038d6-4b46-4f11-95ed-d47c7f325c24
updated: 1486069614
title: Multistep word problems example 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/63i6YVYk7L8/default.jpg
    - https://i3.ytimg.com/vi/63i6YVYk7L8/1.jpg
    - https://i3.ytimg.com/vi/63i6YVYk7L8/2.jpg
    - https://i3.ytimg.com/vi/63i6YVYk7L8/3.jpg
---
