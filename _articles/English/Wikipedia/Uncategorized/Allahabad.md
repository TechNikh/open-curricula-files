---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Allahabad
offline_file: ""
offline_thumbnail: ""
uuid: 52997aab-d066-4527-8781-e7bcec8762c1
updated: 1484308413
title: Allahabad
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Yamuna_river%252C_Allahabad.jpg'
tags:
    - History
    - geography
    - Cityscape
    - Topography
    - Climate
    - biodiversity
    - Demographics
    - Civic administration
    - Economy
    - culture
    - Literature
    - Entertainment and recreation
    - Media
    - Sports
    - Transportation and utilities
    - Education
categories:
    - Uncategorized
---
Allahabad (i/əˈlɑːhəbɑːd/), also known as Prayag (/prəˈjɑːɡ/) is a city in the Indian state of Uttar Pradesh and the administrative headquarters of Allahabad District, the most populous district in the state. As of 2011, Allahabad is the seventh most populous city in the state, twelfth in the Northern India and thirty-sixth in India, with an estimated population of 1.11 million in the city and 1.21 million in its metropolitan region.[7][8] In 2011 it was ranked the world's 130th fastest-growing city.[9][10] Allahabad, in 2013, was ranked the third most liveable city in the state (after Noida and Lucknow) and twenty-ninth in the country.[11]
