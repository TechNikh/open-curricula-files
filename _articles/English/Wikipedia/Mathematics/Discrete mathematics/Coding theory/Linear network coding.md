---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_network_coding
offline_file: ""
offline_thumbnail: ""
uuid: 07bb8e24-121d-42e8-a560-edca77cbbfb6
updated: 1484308547
title: Linear network coding
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Butterfly_network.svg.png
tags:
    - Encoding and decoding
    - A brief history
    - The butterfly network example
    - Random Linear Network Coding
    - Open issues
    - Wireless Network Coding
    - Applications
categories:
    - Coding theory
---
Linear network coding is a technique which can be used to improve a network's throughput, efficiency and scalability, as well as resilience to attacks and eavesdropping. Instead of simply relaying the packets of information they receive, the nodes of a network take several packets and combine them together for transmission. This can be used to attain the maximum possible information flow in a network.
