---
version: 1
type: video
provider: YouTube
id: j_kSmmEpvQk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Professional%20Antigen%20Presenting%20Cells%20%28APC%29%20and%20MHC%20II%20complexes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0c1f7275-d894-479a-953d-b4358da07bbc
updated: 1486069579
title: >
    Professional Antigen Presenting Cells (APC) and MHC II
    complexes
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/j_kSmmEpvQk/default.jpg
    - https://i3.ytimg.com/vi/j_kSmmEpvQk/1.jpg
    - https://i3.ytimg.com/vi/j_kSmmEpvQk/2.jpg
    - https://i3.ytimg.com/vi/j_kSmmEpvQk/3.jpg
---
How professional antigen presenting cells present parts of engulfed pathogens on MHC II complexes (major histocompatibility complexes).
More free lessons at: http://www.khanacademy.org/video?v=j_kSmmEpvQk
