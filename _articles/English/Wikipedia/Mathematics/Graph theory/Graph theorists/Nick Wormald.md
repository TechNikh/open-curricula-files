---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nick_Wormald
offline_file: ""
offline_thumbnail: ""
uuid: b4a53ca3-e969-46ef-b3c8-aad7acad7bb8
updated: 1484309481
title: Nick Wormald
categories:
    - Graph theorists
---
He specializes in probabilistic combinatorics, graph theory, graph algorithms, Steiner trees, web graphs, mine optimization, and other areas in combinatorics.[1]
