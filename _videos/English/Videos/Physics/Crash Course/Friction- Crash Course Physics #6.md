---
version: 1
type: video
provider: YouTube
id: fo_pmp5rtzo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 896fb142-0fa2-430d-893c-eec7262ca647
updated: 1486069682
title: 'Friction- Crash Course Physics #6'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/fo_pmp5rtzo/default.jpg
    - https://i3.ytimg.com/vi/fo_pmp5rtzo/1.jpg
    - https://i3.ytimg.com/vi/fo_pmp5rtzo/2.jpg
    - https://i3.ytimg.com/vi/fo_pmp5rtzo/3.jpg
---
