---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/www
offline_file: ""
offline_thumbnail: ""
uuid: acb94fdc-662c-4dbc-bfe1-ac296202517e
updated: 1484310467
title: www
categories:
    - Dictionary
---
WWW
     n : computer network consisting of a collection of internet
         sites that offer text and graphics and sound and
         animation resources through the hypertext transfer
         protocol [syn: {World Wide Web}, {web}]
