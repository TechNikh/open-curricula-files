---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/telephone
offline_file: ""
offline_thumbnail: ""
uuid: 82c21b2e-1211-42e9-bfbf-671b1e1f24be
updated: 1484310212
title: telephone
categories:
    - Dictionary
---
telephone
     n 1: electronic equipment that converts sound into electrical
          signals that can be transmitted over distances and then
          converts received signals back into sounds; "I talked to
          him on the telephone" [syn: {phone}, {telephone set}]
     2: transmitting speech at a distance [syn: {telephony}]
     v : get or try to get into communication (with someone) by
         telephone; "I tried to call you all night"; "Take two
         aspirin and call me in the morning" [syn: {call}, {call
         up}, {phone}, {ring}]
