---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/profits
offline_file: ""
offline_thumbnail: ""
uuid: 610ea936-fbd7-4e63-81b9-167d4e45f126
updated: 1484310527
title: profits
categories:
    - Dictionary
---
profits
     n 1: the excess of revenues over outlays in a given period of
          time (including depreciation and other non-cash
          expenses) [syn: {net income}, {net}, {net profit}, {lucre},
           {profit}, {earnings}]
     2: something won (especially money) [syn: {winnings}, {win}]
        [ant: {losings}]
