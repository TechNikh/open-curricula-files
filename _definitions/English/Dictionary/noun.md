---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/noun
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464081
title: noun
categories:
    - Dictionary
---
noun
     n 1: a word that can be used to refer to a person or place or
          thing
     2: a word that can serve as the subject or object of a verb
