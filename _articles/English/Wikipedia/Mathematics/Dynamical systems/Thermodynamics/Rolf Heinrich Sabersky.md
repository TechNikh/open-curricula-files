---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rolf_Heinrich_Sabersky
offline_file: ""
offline_thumbnail: ""
uuid: 11efc1c4-4afa-4a0f-8171-23b746e2f314
updated: 1484308864
title: Rolf Heinrich Sabersky
tags:
    - Life and Times
    - Education
    - Aerojet
    - Aerobee
    - California Institute of Technology
    - Research Interests
    - Awards and honors
    - Publications
categories:
    - Thermodynamics
---
Rolf Heinrich Sabersky (born 20 October 1920) professor emeritus in mechanical engineering at Caltech. He worked with luminaries throughout his distinguished career. These included Apollo M. O. Smith and Theodore von Kármán at Aerojet. James Van Allan sought his expertise for the development of the Ajax and Bumblebee rocket programs.
