---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Autosomes
offline_file: ""
offline_thumbnail: ""
uuid: a8fd534b-5b71-46df-8297-30e8146bf6af
updated: 1484308347
title: Autosome
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Autosomal_recessive_inheritance.gif
categories:
    - Uncategorized
---
An autosome is a chromosome that is not an allosome (a sex chromosome).[1] Autosomes appear in pairs whose members have the same form but differ from other pairs in a diploid cell, whereas members of an allosome pair may differ from one another and thereby determine sex. The DNA in autosomes is collectively known as atDNA or auDNA.[2]
