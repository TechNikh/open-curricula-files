---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Takeuti%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 2d61e6b2-82dc-4424-ade7-5ffc5fc5dd48
updated: 1484309499
title: "Takeuti's conjecture"
categories:
    - Proof theory
---
In mathematics, Takeuti's conjecture is the conjecture of Gaisi Takeuti that a sequent formalisation of second-order logic has cut-elimination (Takeuti 1953). It was settled positively:
