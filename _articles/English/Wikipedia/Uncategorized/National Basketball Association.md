---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nba
offline_file: ""
offline_thumbnail: ""
uuid: 55b64107-99ca-4246-9bd7-7aeef859b5ce
updated: 1484308321
title: National Basketball Association
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-NBALogo.svg.png
tags:
    - History
    - Creation and merger
    - "Celtics' dominance, league expansion and competition"
    - Surging popularity
    - Modern era
    - International Influence
    - Other developments
    - Teams
    - Regular season
    - Playoffs
    - League championships
    - Media coverage
    - International competitions
    - Ticket prices and viewership demographics
    - Viewership demographics
    - Notable people
    - Presidents and commissioners
    - Players
    - Foreign players
    - International Influence
    - Coaches
categories:
    - Uncategorized
---
The National Basketball Association (NBA) is the pre-eminent men's professional basketball league in North America, and is widely considered to be the premier men's professional basketball league in the world. It has 30 teams (29 in the United States and 1 in Canada), and is an active member of USA Basketball (USAB),[2] which is recognized by FIBA (also known as the International Basketball Federation) as the national governing body for basketball in the United States. The NBA is one of the four major professional sports leagues in the United States and Canada. NBA players are the world's best paid sportsmen, by average annual salary per player.[3][4]
