---
version: 1
type: video
provider: YouTube
id: CB019AxAoKk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9087cfeb-1d3f-4acc-bfde-f1f18e20e4d0
updated: 1486069619
title: Finding dimensions of triangle from area
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/CB019AxAoKk/default.jpg
    - https://i3.ytimg.com/vi/CB019AxAoKk/1.jpg
    - https://i3.ytimg.com/vi/CB019AxAoKk/2.jpg
    - https://i3.ytimg.com/vi/CB019AxAoKk/3.jpg
---
