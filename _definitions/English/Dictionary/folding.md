---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/folding
offline_file: ""
offline_thumbnail: ""
uuid: c9823222-8737-4e37-9a84-1d7eedb23a39
updated: 1484310433
title: folding
categories:
    - Dictionary
---
folding
     adj : capable of being folded up and stored; "a foldaway bed"
           [syn: {foldable}, {foldaway}, {folding(a)}]
     n 1: the process whereby a protein molecule assumes its intricate
          three-dimensional shape; "understanding protein folding
          is the next step in deciphering the genetic code" [syn:
          {protein folding}]
     2: the act of folding; "he gave the napkins a double fold"
        [syn: {fold}]
