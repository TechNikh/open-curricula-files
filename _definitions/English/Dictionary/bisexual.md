---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bisexual
offline_file: ""
offline_thumbnail: ""
uuid: bba03d50-5c32-43eb-b0df-8e4e3b0a13fa
updated: 1484310301
title: bisexual
categories:
    - Dictionary
---
bisexual
     adj 1: sexually attracted to both sexes [ant: {heterosexual}, {homosexual}]
     2: having an ambiguous sexual identity [syn: {epicene}]
     n : a person who is sexually attracted to both sexes [syn: {bisexual
         person}]
