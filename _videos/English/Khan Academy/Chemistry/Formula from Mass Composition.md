---
version: 1
type: video
provider: YouTube
id: xatVrAh2U0E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Formula%20from%20Mass%20Composition.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 20149ff8-2778-43b2-a2bd-0fd2a2962a91
updated: 1486069585
title: Formula from Mass Composition
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/xatVrAh2U0E/default.jpg
    - https://i3.ytimg.com/vi/xatVrAh2U0E/1.jpg
    - https://i3.ytimg.com/vi/xatVrAh2U0E/2.jpg
    - https://i3.ytimg.com/vi/xatVrAh2U0E/3.jpg
---
Figuring out the empirical formula from a molecules mass composition
