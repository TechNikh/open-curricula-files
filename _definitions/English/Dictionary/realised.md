---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/realised
offline_file: ""
offline_thumbnail: ""
uuid: 7e195282-527f-4b15-816d-9a7353830331
updated: 1484310397
title: realised
categories:
    - Dictionary
---
realised
     adj : successfully completed or brought to an end; "his mission
           accomplished he took a vacation"; "the completed
           project"; "the joy of a realized ambition overcame him"
           [syn: {accomplished}, {completed}, {realized}]
