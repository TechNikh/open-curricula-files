---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pixel_connectivity
offline_file: ""
offline_thumbnail: ""
uuid: d6d10723-f05e-492c-a200-f73eba2c2eb0
updated: 1484309402
title: Pixel connectivity
tags:
    - Types of connectivity
    - 2-Dimensional
    - 4-connected
    - 6-connected
    - 8-connected
    - 3-Dimensional
    - 6-connected
    - 18-connected
    - 26-connected
categories:
    - Graph connectivity
---
In image processing and image recognition, pixel connectivity is the way in which pixels in 2-dimensional (or voxels in 3-dimensional) images relate to their neighbors.
