---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/will
offline_file: ""
offline_thumbnail: ""
uuid: 2ace02b4-523d-4d77-96c1-1a701bf12b6b
updated: 1484310351
title: will
categories:
    - Dictionary
---
will
     n 1: the capability of conscious choice and decision and
          intention; "the exercise of their volition we construe
          as revolt"- George Meredith [syn: {volition}]
     2: a fixed and persistent intent or purpose; "where there's a
        will there's a way"
     3: a legal document declaring a person's wishes regarding the
        disposal of their property when they die [syn: {testament}]
     v 1: decree or ordain; "God wills our existence"
     2: have in mind; "I will take the exam tomorrow" [syn: {wish}]
     3: determine by choice; "This action was willed and intended"
     4: leave or give by will after one's death; "My aunt bequeathed
        me all her ...
