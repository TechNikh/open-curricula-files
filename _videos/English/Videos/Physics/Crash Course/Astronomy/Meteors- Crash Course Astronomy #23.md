---
version: 1
type: video
provider: YouTube
id: TuDfZ2Md5x8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f6851f03-54c6-4b24-a45b-3b242640e1ee
updated: 1486069684
title: 'Meteors- Crash Course Astronomy #23'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/TuDfZ2Md5x8/default.jpg
    - https://i3.ytimg.com/vi/TuDfZ2Md5x8/1.jpg
    - https://i3.ytimg.com/vi/TuDfZ2Md5x8/2.jpg
    - https://i3.ytimg.com/vi/TuDfZ2Md5x8/3.jpg
---
