---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dimension_function
offline_file: ""
offline_thumbnail: ""
uuid: 794fbe52-c82b-4a10-9e77-175e8fbd3da6
updated: 1484309262
title: Dimension function
tags:
    - 'Motivation: s-dimensional Hausdorff measure'
    - Definition
    - Packing dimension
    - Example
categories:
    - Metric geometry
---
In mathematics, the notion of an (exact) dimension function (also known as a gauge function) is a tool in the study of fractals and other subsets of metric spaces. Dimension functions are a generalisation of the simple "diameter to the dimension" power law used in the construction of s-dimensional Hausdorff measure.
