---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cro
offline_file: ""
offline_thumbnail: ""
uuid: d39f3bc8-9fc5-4da3-bec6-d0aa8c7005d5
updated: 1484308312
title: Cro
tags:
    - Premise
    - Characters
    - Cavepeople
    - Mammoths
    - Villains
    - Other characters
    - Episodes
    - Season 1 (1993–1994)
    - Season 2 (1994–1995)
    - videos
categories:
    - Uncategorized
---
Cro is an American animated television series produced by the Children's Television Workshop (now known as Sesame Workshop) and Film Roman. It debuted on September 18, 1993 as part of the Saturday morning line-up for fall 1993 on ABC. Cro lasted 1½ seasons and ran in reruns through summer 1995. The show had an educational theme (this was before federal educational/informational mandates took effect in 1996), introducing basic concepts of physics, mechanical engineering, and technology. The premise of using woolly mammoths as a teaching tool for the principles of technology was inspired by David Macaulay's The Way Things Work; Macaulay is credited as writer on the show.[1] The last new ...
