---
version: 1
type: video
provider: YouTube
id: 9Ek61w1LxSc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/How%20to%20solve%20equations%20of%20the%20form%20ax%20%3D%20b.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8f4425b9-2fe7-486a-a5a0-b6ddd8a388d2
updated: 1486069569
title: How to solve equations of the form ax = b
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/9Ek61w1LxSc/default.jpg
    - https://i3.ytimg.com/vi/9Ek61w1LxSc/1.jpg
    - https://i3.ytimg.com/vi/9Ek61w1LxSc/2.jpg
    - https://i3.ytimg.com/vi/9Ek61w1LxSc/3.jpg
---
Let's ease into this, shall we? Here's an introduction to basic algebraic equations of the form ax=b. Remember that you can check to see if you have the right answer by substituting it for the variable!

Practice this lesson yourself on KhanAcademy.org right now:
https://www.khanacademy.org/e/linear_equations_1?utm_source=YTdescription&utm_medium=YTdescription&utm_campaign=YTdescription

Watch the next lesson: https://www.khanacademy.org/math/algebra/solving-linear-equations-and-inequalities/equations_beginner/v/solving-one-step-equations-2?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraI

Missed the previous lesson? 
https://www.khanacademy.org/math/algebra/solving-linear-equations-and-inequalities/super-yoga/v/super-yoga-plans-solving-systems-by-elimination?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraI

Algebra I on Khan Academy: Algebra is the language through which we describe patterns. Think of it as a shorthand, of sorts. As opposed to having to do something over and over again, algebra gives you a simple way to express that repetitive process. It's also seen as a "gatekeeper" subject. Once you achieve an understanding of algebra, the higher-level math subjects become accessible to you. Without it, it's impossible to move forward. It's used by people with lots of different jobs, like carpentry, engineering, and fashion design. In these tutorials, we'll cover a lot of ground. Some of the topics include linear equations, linear inequalities, linear functions, systems of equations, factoring expressions, quadratic expressions, exponents, functions, and ratios.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Algebra channel:
https://www.youtube.com/channel/UCYZrCV8PNENpJt36V0kd-4Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
