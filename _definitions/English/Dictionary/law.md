---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/law
offline_file: ""
offline_thumbnail: ""
uuid: 9f3757d3-f56a-410d-8fd7-b20b80b73710
updated: 1484310299
title: law
categories:
    - Dictionary
---
law
     n 1: legal document setting forth rules governing a particular
          kind of activity; "there is a law against kidnapping"
     2: the collection of rules imposed by authority; "civilization
        presupposes respect for the law"; "the great problem for
        jurisprudence to allow freedom while enforcing order"
        [syn: {jurisprudence}]
     3: a generalization that describes recurring facts or events in
        nature; "the laws of thermodynamics" [syn: {law of nature}]
     4: a rule or body of rules of conduct inherent in human nature
        and essential to or binding upon human society [syn: {natural
        law}]
     5: the learned profession that is mastered ...
