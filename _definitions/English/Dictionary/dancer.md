---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dancer
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484642161
title: dancer
categories:
    - Dictionary
---
dancer
     n 1: a performer who dances [syn: {professional dancer}]
     2: a person who participates in a social gathering arranged for
        dancing (as a ball) [syn: {social dancer}]
