---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biography
offline_file: ""
offline_thumbnail: ""
uuid: c5b60c00-61ed-428f-bdfc-6197aa2ed709
updated: 1484310144
title: biography
categories:
    - Dictionary
---
biography
     n : an account of the series of events making up a person's life
         [syn: {life}, {life story}, {life history}]
