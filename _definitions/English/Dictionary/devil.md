---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/devil
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484456101
title: devil
categories:
    - Dictionary
---
Devil
     n 1: (Judeo-Christian and Islamic religions) chief spirit of evil
          and adversary of God; tempter of mankind; master of Hell
          [syn: {Satan}, {Old Nick}, {the Devil}, {Lucifer}, {Beelzebub},
           {the Tempter}, {Prince of Darkness}]
     2: one of the evil spirits of traditional Jewish and Christian
        belief [syn: {fiend}, {demon}, {daemon}, {daimon}]
     3: a word used in exclamations of confusion; "what the devil";
        "the deuce with it"; "the dickens you say" [syn: {deuce},
        {dickens}]
     4: a rowdy or mischievous person (usually a young man); "he
        chased the young hellions out of his yard" [syn: {hellion},
         {heller}]
  ...
