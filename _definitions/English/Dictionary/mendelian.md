---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mendelian
offline_file: ""
offline_thumbnail: ""
uuid: aab53f79-4e88-4ad5-96bd-7c38e88683da
updated: 1484310297
title: mendelian
categories:
    - Dictionary
---
Mendelian
     adj : of or relating to Gregor Mendel or in accord with Mendel's
           laws; "Mendelian inheritance"
