---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pancreas
offline_file: ""
offline_thumbnail: ""
uuid: cadf8a78-a68e-4bd2-9891-1361adb97278
updated: 1484310328
title: pancreas
categories:
    - Dictionary
---
pancreas
     n : a large elongated exocrine gland located behind the stomach;
         secretes pancreatic juice and insulin
