---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anniversary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484442661
title: anniversary
categories:
    - Dictionary
---
anniversary
     n : the date on which an event occurred in some previous year
         (or the celebration of it) [syn: {day of remembrance}]
