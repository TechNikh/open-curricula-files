---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_physics
offline_file: ""
offline_thumbnail: ""
uuid: 4d3fe7a3-6c30-4bc6-8a51-1f03159a0cf6
updated: 1484308866
title: Thermal physics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Bose_Einstein_condensate.png
tags:
    - Overview
categories:
    - Thermodynamics
---
Thermal physics is the combined study of thermodynamics, statistical mechanics, and kinetic theory. This umbrella-subject is typically designed for physics students and functions to provide a general introduction to each of three core heat-related subjects. Other authors, however, define thermal physics loosely as a summation of only thermodynamics and statistical mechanics.[1]
