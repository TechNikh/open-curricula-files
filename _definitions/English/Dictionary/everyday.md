---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/everyday
offline_file: ""
offline_thumbnail: ""
uuid: 48667931-b84d-4b45-8307-a667094bcd4a
updated: 1484310377
title: everyday
categories:
    - Dictionary
---
every day
     adj : occurring or done each day; "a daily record"; "day-by-day
           labors of thousands of men and women"- H.S.Truman; "her
           day-after-day behavior"; "an every day occurrence"
           [syn: {daily}, {day-to-day}, {day-after-day}]
     adv : without missing a day; "he stops by daily" [syn: {daily}, {each
           day}]
