---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Total_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 91682653-6912-4fb2-adde-47316da626a2
updated: 1484309387
title: Total coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Total_coloring_foster_cage.svg.png
categories:
    - Graph coloring
---
In graph theory, total coloring is a type of graph coloring on the vertices and edges of a graph. When used without any qualification, a total coloring is always assumed to be proper in the sense that no adjacent edges and no edge and its endvertices are assigned the same color. The total chromatic number χ″(G) of a graph G is the least number of colors needed in any total coloring of G.
