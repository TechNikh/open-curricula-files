---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lie_groupoid
offline_file: ""
offline_thumbnail: ""
uuid: 77f26093-849d-43ee-9618-332dcd424f30
updated: 1484309228
title: Lie groupoid
tags:
    - Examples
    - Morita Morphisms and Smooth Stacks
    - Examples
categories:
    - Differential geometry
---
In mathematics, a Lie groupoid is a groupoid where the set 
  
    
      
        O
        b
      
    
    {\displaystyle Ob}
  
 of objects and the set 
  
    
      
        M
        o
        r
      
    
    {\displaystyle Mor}
  
 of morphisms are both manifolds, the source and target operations
