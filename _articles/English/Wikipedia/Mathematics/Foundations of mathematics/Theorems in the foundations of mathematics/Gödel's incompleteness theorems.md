---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/G%C3%B6del%27s_incompleteness_theorems'
offline_file: ""
offline_thumbnail: ""
uuid: fc52a85a-aa45-45d5-b6e7-12137b50a396
updated: 1484308997
title: "Gödel's incompleteness theorems"
tags:
    - 'Formal systems: completeness, consistency, and effective axiomatization'
    - Effective axiomatization
    - Completeness
    - Consistency
    - Systems which contain arithmetic
    - Conflicting goals
    - First incompleteness theorem
    - Syntactic form of the Gödel sentence
    - Truth of the Gödel sentence
    - Relationship with the liar paradox
    - "Extensions of Gödel's original result"
    - Second incompleteness theorem
    - Expressing consistency
    - The Hilbert–Bernays conditions
    - Implications for consistency proofs
    - Examples of undecidable statements
    - Undecidable statements provable in larger systems
    - Relationship with computability
    - Proof sketch for the first theorem
    - Arithmetization of syntax
    - Construction of a statement about "provability"
    - Diagonalization
    - "Proof via Berry's paradox"
    - Computer verified proofs
    - Proof sketch for the second theorem
    - Discussion and implications
    - "Consequences for logicism and Hilbert's second problem"
    - Minds and machines
    - Paraconsistent logic
    - Appeals to the incompleteness theorems in other fields
    - History
    - Announcement
    - Generalization and acceptance
    - Criticisms
    - Finsler
    - Zermelo
    - Wittgenstein
    - Notes
    - Articles by Gödel
    - "Translations, during his lifetime, of Gödel's paper into English"
    - Articles by others
    - Books about the theorems
    - Miscellaneous references
categories:
    - Theorems in the foundations of mathematics
---
Gödel's incompleteness theorems are two theorems of mathematical logic that demonstrate the inherent limitations of every formal axiomatic system containing basic arithmetic.[1] These results, published by Kurt Gödel in 1931, are important both in mathematical logic and in the philosophy of mathematics. The theorems are widely, but not universally, interpreted as showing that Hilbert's program to find a complete and consistent set of axioms for all mathematics is impossible.
