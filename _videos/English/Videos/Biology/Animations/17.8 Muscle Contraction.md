---
version: 1
type: video
provider: YouTube
id: 7UurFO-kndI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/17.8%20Muscle%20Contraction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 14ca4e8f-ebd9-4d09-bac0-8940556e8908
updated: 1486069677
title: 17.8 Muscle Contraction
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/7UurFO-kndI/default.jpg
    - https://i3.ytimg.com/vi/7UurFO-kndI/1.jpg
    - https://i3.ytimg.com/vi/7UurFO-kndI/2.jpg
    - https://i3.ytimg.com/vi/7UurFO-kndI/3.jpg
---
