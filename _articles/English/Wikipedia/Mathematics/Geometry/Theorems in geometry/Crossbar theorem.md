---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crossbar_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 4e4d45ce-cc39-43f2-a592-54e40fe52125
updated: 1484309110
title: Crossbar theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Crossbar_theorem_diagram.svg.png
categories:
    - Theorems in geometry
---
This result is one of the deeper results in axiomatic plane geometry.[2] It is often used in proofs to justify the statement that a line through a vertex of a triangle lying inside the triangle meets the side of the triangle opposite that vertex. This property was often used by Euclid in his proofs without explicit justification.[3]
