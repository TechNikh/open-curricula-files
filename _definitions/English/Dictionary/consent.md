---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consent
offline_file: ""
offline_thumbnail: ""
uuid: e02a8d3f-eef0-4222-9aff-e79ae1f4ac37
updated: 1484310585
title: consent
categories:
    - Dictionary
---
consent
     n : permission to do something; "he indicated his consent"
     v : give an affirmative reply to; respond favorably to; "I
         cannot accept your invitation"; "I go for this
         resolution" [syn: {accept}, {go for}] [ant: {refuse}]
