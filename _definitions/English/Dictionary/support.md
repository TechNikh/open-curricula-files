---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/support
offline_file: ""
offline_thumbnail: ""
uuid: 8c8bc7cc-1ed8-4830-ace6-3533e13d95a1
updated: 1484310365
title: support
categories:
    - Dictionary
---
support
     n 1: the activity of providing for or maintaining by supplying
          with money or necessities; "his support kept the family
          together"; "they gave him emotional support during
          difficult times"
     2: aiding the cause or policy or interests of; "the president
        no longer had the support of his own party"; "they
        developed a scheme of mutual support"
     3: something providing immaterial support or assistance to a
        person or cause or interest; "the policy found little
        public support"; "his faith was all the support he
        needed"; "the team enjoyed the support of their fans"
     4: a military operation (often involving ...
