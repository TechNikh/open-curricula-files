---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leukaemia
offline_file: ""
offline_thumbnail: ""
uuid: fa5aba86-ec92-40c4-be28-44d4fe5c2a2f
updated: 1484310551
title: leukaemia
categories:
    - Dictionary
---
leukaemia
     n : malignant neoplasm of blood-forming tissues; characterized
         by abnormal proliferation of leukocytes; one of the four
         major types of cancer [syn: {leukemia}, {leucaemia}, {cancer
         of the blood}]
