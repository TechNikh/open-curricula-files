---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Peixoto%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: a817a761-a388-4865-ba8e-da050b614811
updated: 1484308696
title: "Peixoto's theorem"
categories:
    - Theorems in dynamical systems
---
In the theory of dynamical systems, Peixoto theorem, proved by Maurício Peixoto, states that among all smooth flows on surfaces, i.e. compact two-dimensional manifolds, structurally stable systems may be characterized by the following properties:
