---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nobel
offline_file: ""
offline_thumbnail: ""
uuid: 53591566-f30b-45a3-90a5-46ab34bcaebf
updated: 1484310297
title: nobel
categories:
    - Dictionary
---
Nobel
     n : Swedish chemist remembered for his invention of dynamite and
         for the bequest that created the Nobel prizes (1833-1896)
         [syn: {Alfred Nobel}, {Alfred Bernhard Nobel}]
