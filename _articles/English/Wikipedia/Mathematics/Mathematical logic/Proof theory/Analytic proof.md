---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Analytic_proof
offline_file: ""
offline_thumbnail: ""
uuid: 86a3d8db-f21c-4747-aabd-47687af8838a
updated: 1484309490
title: Analytic proof
categories:
    - Proof theory
---
In mathematics, an analytic proof is a proof of a theorem in analysis that only makes use of methods from analysis, and which does not predominantly make use of algebraic or geometrical methods. The term was first used by Bernard Bolzano, who first provided a non-analytic proof of his intermediate value theorem and then, several years later provided a proof of the theorem which was free from intuitions concerning lines crossing each other at a point, and so he felt happy calling it analytic (Bolzano 1817).
