---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kingdom
offline_file: ""
offline_thumbnail: ""
uuid: 9c30703f-c0a0-435a-8a52-6b000230c7da
updated: 1484310515
title: kingdom
categories:
    - Dictionary
---
kingdom
     n 1: a domain in which something is dominant; "the untroubled
          kingdom of reason"; "a land of make-believe"; "the rise
          of the realm of cotton in the south" [syn: {land}, {realm}]
     2: a country with a king as head of state
     3: the domain ruled by a king or queen [syn: {realm}]
     4: a monarchy with a king or queen as head of state
     5: one of seven biological categories: Monera or Protoctista or
        Plantae or Fungi or Animalia
     6: a basic group of natural objects
