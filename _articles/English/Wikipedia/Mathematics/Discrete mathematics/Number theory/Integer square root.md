---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Integer_square_root
offline_file: ""
offline_thumbnail: ""
uuid: 88592b27-825f-40aa-b84e-5e999c9855fc
updated: 1484308649
title: Integer square root
tags:
    - Algorithm
    - Using only integer division
    - Domain of computation
    - Stopping criterion
categories:
    - Number theory
---
In number theory, the integer square root (isqrt) of a positive integer n is the positive integer m which is the greatest integer less than or equal to the square root of n,
