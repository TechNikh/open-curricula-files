---
version: 1
type: video
provider: YouTube
id: TvUCe6Bomy4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Absolute%20value%20inequalities%20example%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 778209b3-608d-43b2-a87a-e7ae384100af
updated: 1486069557
title: Absolute value inequalities example 1
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/TvUCe6Bomy4/default.jpg
    - https://i3.ytimg.com/vi/TvUCe6Bomy4/1.jpg
    - https://i3.ytimg.com/vi/TvUCe6Bomy4/2.jpg
    - https://i3.ytimg.com/vi/TvUCe6Bomy4/3.jpg
---
