---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fluctuation_theorem
offline_file: ""
offline_thumbnail: ""
uuid: b2fc4d21-14bf-4af5-8ad4-2dab2182eae6
updated: 1484308801
title: Fluctuation theorem
tags:
    - Statement of the fluctuation theorem
    - Second law inequality
    - Nonequilibrium partition identity
    - Dissipation function
    - "The fluctuation theorem and Loschmidt's paradox"
    - Summary
    - Notes
categories:
    - Non-equilibrium thermodynamics
---
The fluctuation theorem (FT), which originated from statistical mechanics, deals with the relative probability that the entropy of a system which is currently away from thermodynamic equilibrium (i.e., maximum entropy) will increase or decrease over a given amount of time. While the second law of thermodynamics predicts that the entropy of an isolated system should tend to increase until it reaches equilibrium, it became apparent after the discovery of statistical mechanics that the second law is only a statistical one, suggesting that there should always be some nonzero probability that the entropy of an isolated system might spontaneously decrease; the fluctuation theorem precisely ...
