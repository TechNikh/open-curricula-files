---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faulty
offline_file: ""
offline_thumbnail: ""
uuid: 229d8469-6192-4954-aeb7-8ba09182c1da
updated: 1484310170
title: faulty
categories:
    - Dictionary
---
faulty
     adj 1: characterized by errors; "he submitted a faulty report"
     2: having a defect; "I returned the appliance because it was
        defective" [syn: {defective}]
     [also: {faultiest}, {faultier}]
