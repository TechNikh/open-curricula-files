---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Synchronization_networks
offline_file: ""
offline_thumbnail: ""
uuid: bf980484-befe-4621-bd9a-3c8f9a026baf
updated: 1484308830
title: Synchronization networks
tags:
    - Definition
    - Kuramoto Model
    - Network Topology
    - History
categories:
    - Oscillation
---
Synchronization networks are also often known as "networks of coupled dynamical systems". Both of these refer to networks connecting oscillators, where oscillators are nodes that emit a signal with somewhat regular (possibly variable) frequency, and are also capable of receiving a signal.
