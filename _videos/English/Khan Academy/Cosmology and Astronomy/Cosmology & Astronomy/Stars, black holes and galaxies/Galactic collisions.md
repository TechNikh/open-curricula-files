---
version: 1
type: video
provider: YouTube
id: QXYbGZ3T3_k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Galactic%20collisions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: eb5a795d-f8d0-4778-9d58-0abef91fe3d3
updated: 1486069686
title: Galactic collisions
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/QXYbGZ3T3_k/default.jpg
    - https://i3.ytimg.com/vi/QXYbGZ3T3_k/1.jpg
    - https://i3.ytimg.com/vi/QXYbGZ3T3_k/2.jpg
    - https://i3.ytimg.com/vi/QXYbGZ3T3_k/3.jpg
---
