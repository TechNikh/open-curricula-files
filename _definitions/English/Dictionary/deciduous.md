---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deciduous
offline_file: ""
offline_thumbnail: ""
uuid: cb562bf4-afa7-464a-a436-a79a99c907ed
updated: 1484310268
title: deciduous
categories:
    - Dictionary
---
deciduous
     adj 1: (of plants and shrubs) shedding foliage at the end of the
            growing season [ant: {evergreen}]
     2: (of teeth, antlers, etc.) being shed at the end of a period
        of growth; "deciduous teeth"
