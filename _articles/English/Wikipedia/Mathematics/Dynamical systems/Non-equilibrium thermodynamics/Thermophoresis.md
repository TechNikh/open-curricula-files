---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermophoresis
offline_file: ""
offline_thumbnail: ""
uuid: 69c733b6-99c9-4e2c-9e21-0ef88b1c08ad
updated: 1484308813
title: Thermophoresis
tags:
    - Thermophoretic force
    - Applications
    - History
categories:
    - Non-equilibrium thermodynamics
---
Thermophoresis (also thermomigration, thermodiffusion, the Soret effect, or the Ludwig-Soret effect) is a phenomenon observed in mixtures of mobile particles where the different particle types exhibit different responses to the force of a temperature gradient. The term thermophoresis most often applies to aerosol mixtures, but may also commonly refer to the phenomenon in all phases of matter. The term Soret effect normally applies to liquid mixtures, which behave according to different, less well-understood mechanisms than gaseous mixtures. Thermophoresis may not apply to thermomigration in solids, especially multi-phase alloys.[citation needed]
