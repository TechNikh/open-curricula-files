---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 5f5dc86c-a2b8-42f0-9b9f-cfc1cda5af38
updated: 1484309130
title: Algebraic geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_2.png
tags:
    - Basic notions
    - Zeros of simultaneous polynomials
    - Affine varieties
    - Regular functions
    - Morphism of affine varieties
    - Rational function and birational equivalence
    - Projective variety
    - Real algebraic geometry
    - Computational algebraic geometry
    - Gröbner basis
    - Cylindrical algebraic decomposition (CAD)
    - Asymptotic complexity vs. practical efficiency
    - Abstract modern viewpoint
    - History
    - 'Prehistory: before the 16th century'
    - Renaissance
    - 19th and early 20th century
    - 20th century
    - Analytic geometry
    - Applications
    - Notes
categories:
    - Algebraic geometry
---
Algebraic geometry is a branch of mathematics, classically studying zeros of multivariate polynomials. Modern algebraic geometry is based on the use of abstract algebraic techniques, mainly from commutative algebra, for solving geometrical problems about these sets of zeros.
