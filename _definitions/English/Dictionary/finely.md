---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finely
offline_file: ""
offline_thumbnail: ""
uuid: ddfa500f-9ba4-4c9c-9dd2-15547d937004
updated: 1484310381
title: finely
categories:
    - Dictionary
---
finely
     adv 1: in tiny pieces; "the surfaces were finely granular" [ant: {coarsely}]
     2: in an elegant manner; "finely costumed actors"
     3: in a superior and skilled manner; "the soldiers were
        fighting finely" [syn: {fine}]
     4: in a delicate manner; "finely shaped features"; "her fine
        drawn body" [syn: {fine}, {delicately}, {exquisitely}]
