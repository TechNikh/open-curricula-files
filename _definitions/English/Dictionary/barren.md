---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/barren
offline_file: ""
offline_thumbnail: ""
uuid: 2132adf0-41b1-4a55-ad5b-d8b82b1dc53b
updated: 1484310439
title: barren
categories:
    - Dictionary
---
barren
     adj 1: providing no shelter or sustenance; "bare rocky hills";
            "barren lands"; "the bleak treeless regions of the
            high Andes"; "the desolate surface of the moon"; "a
            stark landscape" [syn: {bare}, {bleak}, {desolate}, {stark}]
     2: not bearing offspring; "a barren woman"; "learned early in
        his marriage that he was sterile"
     3: incapable of sustaining life; "the dead and barren Moon"
     n : an uninhabited wilderness that is worthless for cultivation;
         "the barrens of central Africa"; "the trackless wastes of
         the desert" [syn: {waste}, {wasteland}]
