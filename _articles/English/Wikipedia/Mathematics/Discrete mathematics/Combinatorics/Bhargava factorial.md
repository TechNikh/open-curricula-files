---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bhargava_factorial
offline_file: ""
offline_thumbnail: ""
uuid: 04cc5b0d-3c30-4b2a-828c-364da3f87ae6
updated: 1484308573
title: Bhargava factorial
tags:
    - Motivation for the generalization
    - The generalisation
    - 'Example: Factorials using set of prime numbers'
    - 'Example: Factorials using the set of natural numbers'
    - 'Examples: Some general expressions'
    - Properties
categories:
    - Combinatorics
---
In mathematics, Bhargava's factorial function, or simply Bhargava factorial, is a certain generalization of the factorial function developed by the Fields Medal winning mathematician Manjul Bhargava as part of his thesis in Harvard University in 1996. The Bhargava factorial has the property that many number-theoretic results involving the ordinary factorials remain true even when the factorials are replaced by the Bhargava factorials. Using an arbitrary infinite subset S of the set Z of integers, Bhargava associated a positive integer with every positive integer k, which he denoted by k !S, with the property that if we take S = Z itself, then the integer associated with k, that is k !Z, ...
