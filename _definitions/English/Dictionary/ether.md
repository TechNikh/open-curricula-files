---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ether
offline_file: ""
offline_thumbnail: ""
uuid: 7b566f4c-4515-4330-a843-793d9d11e5a9
updated: 1484310419
title: ether
categories:
    - Dictionary
---
ether
     n 1: a colorless volatile highly inflammable liquid formerly used
          as an inhalation anesthetic [syn: {ethoxyethane}, {divinyl
          ether}, {vinyl ether}, {diethyl ether}, {ethyl ether}]
     2: the fifth and highest element after air and earth and fire
        and water; was believed to be the substance composing all
        heavenly bodies [syn: {quintessence}]
     3: any of a class of organic compounds that have two
        hydrocarbon groups linked by an oxygen atom
     4: a medium that was once supposed to fill all space and to
        support the propagation of electromagnetic waves [syn: {aether}]
