---
version: 1
type: video
provider: YouTube
id: diACdEtQ-bY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a6bafadb-e962-4988-be1c-3f123b6ed844
updated: 1486069637
title: "Wave Optics- Young's Double Slit Experiment - 09/35"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/diACdEtQ-bY/default.jpg
    - https://i3.ytimg.com/vi/diACdEtQ-bY/1.jpg
    - https://i3.ytimg.com/vi/diACdEtQ-bY/2.jpg
    - https://i3.ytimg.com/vi/diACdEtQ-bY/3.jpg
---
