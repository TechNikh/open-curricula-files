---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subordination
offline_file: ""
offline_thumbnail: ""
uuid: 6d53e63b-76ec-4e0e-a3f2-e65a52975311
updated: 1484310569
title: subordination
categories:
    - Dictionary
---
subordination
     n 1: the state of being subordinate to something
     2: the semantic relation of being subordinate or belonging to a
        lower rank or class [syn: {hyponymy}]
     3: the grammatical relation of a modifying word or phrase to
        its head
     4: the quality of obedient submissiveness [ant: {insubordination}]
     5: the act of mastering or subordinating someone [syn: {mastery}]
