---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Modern_technology
offline_file: ""
offline_thumbnail: ""
uuid: d39d5e86-9602-40c6-8b03-55cbf2804486
updated: 1484308378
title: Technology
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/500px-Telecom-icon.svg.png
tags:
    - Definition and usage
    - Science, engineering and technology
    - History
    - Paleolithic (2.5 Ma – 10 ka)
    - Stone tools
    - Fire
    - Clothing and shelter
    - Neolithic through classical antiquity (10 ka – 300 CE)
    - Metal tools
    - Energy and transport
    - Medieval and modern history (300 CE – present)
    - Philosophy
    - Technicism
    - Optimism
    - Skepticism and critics
    - Appropriate technology
    - Optimism and skepticism in the 21st century
    - Complex technological systems
    - Competitiveness
    - Project Socrates
    - Other animal species
    - Future technology
categories:
    - Uncategorized
---
Technology ("science of craft", from Greek τέχνη, techne, "art, skill, cunning of hand"; and -λογία, -logia[2]) is the collection of techniques, skills, methods and processes used in the production of goods or services or in the accomplishment of objectives, such as scientific investigation. Technology can be the knowledge of techniques, processes, and the like, or it can be embedded in machines which can be operated without detailed knowledge of their workings.
