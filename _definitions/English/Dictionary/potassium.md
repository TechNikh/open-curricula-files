---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/potassium
offline_file: ""
offline_thumbnail: ""
uuid: 8616736b-40a5-45de-96a1-c67c121e44f3
updated: 1484310284
title: potassium
categories:
    - Dictionary
---
potassium
     n : a light soft silver-white metallic element of the alkali
         metal group; oxidizes rapidly in air and reacts violently
         with water; is abundant in nature in combined forms
         occurring in sea water and in carnallite and kainite and
         sylvite [syn: {K}, {atomic number 19}]
