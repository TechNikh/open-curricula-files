---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ultraparallel_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 9a94f220-f7fa-436f-ac1a-e8c0ea3da400
updated: 1484309130
title: Ultraparallel theorem
tags:
    - "Hilbert's construction"
    - Proof in the Poincaré half-plane model
    - Proof in the Beltrami-Klein model
categories:
    - Theorems in geometry
---
In hyperbolic geometry, the ultraparallel theorem states that every pair of ultraparallel lines (lines that are not intersecting and not limiting parallel) has a unique common perpendicular hyperbolic line.
