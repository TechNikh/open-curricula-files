---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fitness_model_(network_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 08cef4bc-616a-49a9-ae47-fee3efd15ea7
updated: 1484309435
title: Fitness model (network theory)
tags:
    - Description of the model
    - Fitness model and the evolution of the Web
categories:
    - Network theory
---
In complex network theory, the fitness model is a model of the evolution of a network: how the links between nodes change over time depends on the fitness of nodes. Fitter nodes attract more links at the expense of less fit nodes.
