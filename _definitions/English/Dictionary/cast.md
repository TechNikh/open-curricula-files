---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cast
offline_file: ""
offline_thumbnail: ""
uuid: 9bcaf923-558e-47bb-8203-ff7d26cbf94e
updated: 1484310585
title: cast
categories:
    - Dictionary
---
cast
     adj : (of molten metal or glass) formed by pouring or pressing
           into a mold
     n 1: the actors in a play [syn: {cast of characters}, {dramatis
          personae}]
     2: container into which liquid is poured to create a given
        shape when it hardens [syn: {mold}, {mould}]
     3: the distinctive form in which a thing is made; "pottery of
        this cast was found throughout the region" [syn: {mold}, {stamp}]
     4: the visual appearance of something or someone; "the delicate
        cast of his features" [syn: {form}, {shape}]
     5: bandage consisting of a firm covering (often made of plaster
        of Paris) that immobilizes broken bones while they heal
 ...
