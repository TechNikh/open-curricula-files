---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expensive
offline_file: ""
offline_thumbnail: ""
uuid: 463e3980-d3c4-40cc-bea1-454bf76203ef
updated: 1484310238
title: expensive
categories:
    - Dictionary
---
expensive
     adj : high in price or charging high prices; "expensive clothes";
           "an expensive shop" [ant: {cheap}]
