---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electronegativity
offline_file: ""
offline_thumbnail: ""
uuid: eb45fd8d-9b18-48c5-8a67-43a7fb2b2249
updated: 1484308386
title: Electronegativity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Electrostatic_Potential.jpg
tags:
    - Electronegativities of the elements
    - Methods of calculation
    - Pauling electronegativity
    - Mulliken electronegativity
    - Allred–Rochow electronegativity
    - Sanderson electronegativity equalization
    - Allen electronegativity
    - Correlation of electronegativity with other properties
    - Trends in electronegativity
    - Periodic trends
    - Variation of electronegativity with oxidation number
    - Group electronegativity
    - Electropositivity
    - Bibliography
categories:
    - Uncategorized
---
Electronegativity, symbol χ, is a chemical property that describes the tendency of an atom or a functional group to attract electrons (or electron density) towards itself.[1] An atom's electronegativity is affected by both its atomic number and the distance at which its valence electrons reside from the charged nucleus. The higher the associated electronegativity number, the more an element or compound attracts electrons towards it.
