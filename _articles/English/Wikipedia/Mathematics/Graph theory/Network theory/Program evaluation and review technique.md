---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Program_evaluation_and_review_technique
offline_file: ""
offline_thumbnail: ""
uuid: 90815529-2103-4700-b278-4b995e9af833
updated: 1484309435
title: Program evaluation and review technique
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/309px-Pert_chart_colored.svg.png
tags:
    - History
    - Overview
    - Terminology
    - Implementation
    - Advantages
    - Disadvantages
    - Uncertainty in project scheduling
categories:
    - Network theory
---
The program (or project) evaluation and review technique, commonly abbreviated PERT, is a statistical tool, used in project management, which was designed to analyze and represent the tasks involved in completing a given project. First developed by the United States Navy in the 1950s, it is commonly used in conjunction with the critical path method (CPM).
