---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/buying
offline_file: ""
offline_thumbnail: ""
uuid: a3a9219e-a1dc-4459-983e-fcddcd066555
updated: 1484310451
title: buying
categories:
    - Dictionary
---
buying
     n : the act of buying; "buying and selling fill their days";
         "shrewd purchasing requires considerable knowledge" [syn:
          {purchasing}]
