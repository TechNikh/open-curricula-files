---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collision
offline_file: ""
offline_thumbnail: ""
uuid: 88378c78-4194-480e-90e0-6a622892368a
updated: 1484310228
title: collision
categories:
    - Dictionary
---
collision
     n 1: (physics) an brief event in which two or more bodies come
          together; "the collision of the particles resulted in an
          exchange of energy and a change of direction" [syn: {hit}]
     2: an accident resulting from violent impact of a moving
        object; "three passengers were killed in the collision";
        "the collision of the two ships resulted in a serious oil
        spill"
     3: a conflict of opposed ideas or attitudes or goals; "a
        collision of interests"
