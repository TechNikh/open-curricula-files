---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hindi
offline_file: ""
offline_thumbnail: ""
uuid: 3c7410bb-7302-4547-bce9-f540b754826e
updated: 1484310541
title: hindi
categories:
    - Dictionary
---
Hindi
     adj : of or relating to or supporting Hinduism; "the Hindu faith"
           [syn: {Hindu}, {Hindoo}]
     n : the most widely spoken of modern Indic vernaculars; spoken
         mostly in the north of India; along with English it is
         the official language of India; usually written in
         Devanagari script
