---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moisture
offline_file: ""
offline_thumbnail: ""
uuid: 3ba4e118-0bb5-4510-8e24-41a9bcf7a48e
updated: 1484310260
title: moisture
categories:
    - Dictionary
---
moisture
     n : wetness caused by water; "drops of wet gleamed on the
         window" [syn: {wet}]
