---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/momentum
offline_file: ""
offline_thumbnail: ""
uuid: 4bd81135-5bc2-4fc1-8a3e-1b0d583a96bd
updated: 1484310393
title: momentum
categories:
    - Dictionary
---
momentum
     n 1: an impelling force or strength; "the car's momentum carried
          it off the road" [syn: {impulse}]
     2: the product of a body's mass and its velocity; "the momentum
        of the particles was deduced from meteoritic velocities"
     [also: {momenta} (pl)]
