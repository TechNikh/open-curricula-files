---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distilled
offline_file: ""
offline_thumbnail: ""
uuid: 96d4ccdd-457e-42c6-ba3e-ed426f2ecb20
updated: 1484310375
title: distilled
categories:
    - Dictionary
---
distil
     v 1: undergo condensation; change from a gaseous to a liquid
          state and fall in drops; "water condenses"; "The acid
          distills at a specific temperature" [syn: {condense}, {distill}]
     2: extract by the process of distillation; "distill the essence
        of this compound" [syn: {distill}, {extract}]
     3: undergo the process of distillation [syn: {distill}]
     4: give off (a liquid); "The doctor distilled a few drops of
        disinfectant onto the wound" [syn: {distill}]
     [also: {distilling}, {distilled}]
