---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forehead
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484487061
title: forehead
categories:
    - Dictionary
---
forehead
     n 1: the part of the face above the eyes [syn: {brow}]
     2: the large cranial bone forming the front part of the
        cranium: the forehead and the upper part of the orbits
        [syn: {frontal bone}, {os frontale}]
