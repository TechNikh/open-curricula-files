---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_map
offline_file: ""
offline_thumbnail: ""
uuid: 9c9d428f-bbec-47fe-b6a3-934bd0a874f3
updated: 1484309334
title: Linear map
tags:
    - Definition and first consequences
    - Examples
    - Matrices
    - Examples of linear transformation matrices
    - Forming new linear maps from given ones
    - Endomorphisms and automorphisms
    - Kernel, image and the rank–nullity theorem
    - Cokernel
    - Index
    - Algebraic classifications of linear transformations
    - Change of basis
    - Continuity
    - Applications
    - Notes
categories:
    - Transformation (function)
---
In mathematics, a linear map (also called a linear mapping, linear transformation or, in some contexts, linear function) is a mapping V → W between two modules (including vector spaces) that preserves (in the sense defined below) the operations of addition and scalar multiplication.
