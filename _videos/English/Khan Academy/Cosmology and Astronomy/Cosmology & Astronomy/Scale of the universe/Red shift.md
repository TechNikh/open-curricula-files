---
version: 1
type: video
provider: YouTube
id: mx2M_ZKXM_c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Red%20shift.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 662c18c0-7a88-4383-9a35-0eda682aa4b8
updated: 1486069686
title: Red shift
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/mx2M_ZKXM_c/default.jpg
    - https://i3.ytimg.com/vi/mx2M_ZKXM_c/1.jpg
    - https://i3.ytimg.com/vi/mx2M_ZKXM_c/2.jpg
    - https://i3.ytimg.com/vi/mx2M_ZKXM_c/3.jpg
---
