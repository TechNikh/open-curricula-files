---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hyperk%C3%A4hler_manifold'
offline_file: ""
offline_thumbnail: ""
uuid: e5492d17-58ac-4ff2-b9c9-02647c84aa4b
updated: 1484309153
title: Hyperkähler manifold
tags:
    - Quaternionic structure
    - Holomorphic symplectic form
    - Examples
categories:
    - Algebraic geometry
---
In differential geometry, a hyperkähler manifold is a Riemannian manifold of dimension 4k and holonomy group contained in Sp(k) (here Sp(k) denotes a compact form of a symplectic group, identified with the group of quaternionic-linear unitary endomorphisms of a 
  
    
      
        k
      
    
    {\displaystyle k}
  
-dimensional quaternionic Hermitian space). Hyperkähler manifolds are special classes of Kähler manifolds. They can be thought of as quaternionic analogues of Kähler manifolds. All hyperkähler manifolds are Ricci-flat and are thus Calabi–Yau manifolds (this can be easily seen by noting that Sp(k) is a subgroup of SU(2k)).
