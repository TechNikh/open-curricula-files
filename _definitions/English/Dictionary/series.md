---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/series
offline_file: ""
offline_thumbnail: ""
uuid: c0bbcd43-b2de-43ab-886c-32c57a371ec9
updated: 1484310359
title: series
categories:
    - Dictionary
---
series
     n 1: similar things placed in order or happening one after
          another; "they were investigating a series of bank
          robberies"
     2: a serialized set of programs; "a comedy series"; "the
        Masterworks concert series" [syn: {serial}]
     3: a periodical that appears at scheduled times [syn: {serial},
         {serial publication}]
     4: (sports) several contests played successively by the same
        teams; "the visiting team swept the series"
     5: a group of postage stamps having a common theme or a group
        of coins or currency selected as a group for study or
        collection; "the Post Office issued a series commemorating
        famous ...
