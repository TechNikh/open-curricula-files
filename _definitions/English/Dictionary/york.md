---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/york
offline_file: ""
offline_thumbnail: ""
uuid: 9f89be07-6928-4bff-8a50-ccebbc56c0de
updated: 1484310181
title: york
categories:
    - Dictionary
---
York
     n : the English royal house (a branch of the Plantagenet line)
         that reigned from 1461 to 1485; its emblem was a white
         rose [syn: {House of York}]
