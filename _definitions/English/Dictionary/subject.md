---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subject
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484385721
title: subject
categories:
    - Dictionary
---
subject
     adj 1: not exempt from tax; "the gift will be subject to taxation"
            [syn: {subject(p)}]
     2:  possibly accepting or permitting; "a passage capable of
        misinterpretation"; "open to interpretation"; "an issue
        open to question"; "the time is fixed by the director and
        players and therefore subject to much variation" [syn: {capable},
         {open}]
     3: being under the power or sovereignty of another or others;
        "subject peoples"; "a dependent prince" [syn: {dependent}]
     n 1: the subject matter of a conversation or discussion; "he
          didn't want to discuss that subject"; "it was a very
          sensitive topic"; "his ...
