---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arithmetic
offline_file: ""
offline_thumbnail: ""
uuid: dbab2bd1-4a66-4d09-91b8-5896fbd73cbc
updated: 1484310289
title: arithmetic
categories:
    - Dictionary
---
arithmetic
     adj : relating to or involving arithmetic; "arithmetical
           computations" [syn: {arithmetical}]
     n : the branch of pure mathematics dealing with the theory of
         numerical calculations
