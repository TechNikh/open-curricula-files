---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/failed
offline_file: ""
offline_thumbnail: ""
uuid: e845b999-8862-4633-b790-11ac6b7c99f1
updated: 1484310389
title: failed
categories:
    - Dictionary
---
failed
     adj : unable to meet financial obligations; "a failing business
           venture" [syn: {failing}]
