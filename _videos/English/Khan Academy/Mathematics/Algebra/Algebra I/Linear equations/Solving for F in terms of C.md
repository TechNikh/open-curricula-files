---
version: 1
type: video
provider: YouTube
id: BR5yFOt0zao
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Solving%20for%20F%20in%20terms%20of%20C.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 14008f3b-b318-4e89-8643-6336e4dfc9f9
updated: 1486069559
title: Solving for F in terms of C
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/BR5yFOt0zao/default.jpg
    - https://i3.ytimg.com/vi/BR5yFOt0zao/1.jpg
    - https://i3.ytimg.com/vi/BR5yFOt0zao/2.jpg
    - https://i3.ytimg.com/vi/BR5yFOt0zao/3.jpg
---
