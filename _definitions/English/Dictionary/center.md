---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/center
offline_file: ""
offline_thumbnail: ""
uuid: 281bccc1-76f7-4a4f-864d-d6b6d4fedd2a
updated: 1484310337
title: center
categories:
    - Dictionary
---
center
     adj 1: equally distant from the extremes [syn: {center(a)}, {halfway},
             {middle(a)}, {midway}]
     2: of or belonging to neither the right nor the left
        politically or intellectually [ant: {right}, {left}]
     n 1: an area that is approximately central within some larger
          region; "it is in the center of town"; "they ran forward
          into the heart of the struggle"; "they were in the eye
          of the storm" [syn: {centre}, {middle}, {heart}, {eye}]
     2: the piece of ground in the outfield directly ahead of the
        catcher; "he hit the ball to deep center" [syn: {center
        field}]
     3: a building dedicated to a particular ...
