---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Elitzur%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 51cb1cdf-ea56-4bcb-b6a6-82965bbdbc8b
updated: 1484309309
title: "Elitzur's theorem"
tags:
    - Notes
categories:
    - Symmetry
---
Elitzur's theorem is a theorem in quantum and statistical field theory stating that local gauge symmetries cannot be spontaneously broken. The theorem was proposed in 1975 by Shmuel Elitzur, who proved it for Abelian gauge fields on a lattice.[1] It is nonetheless possible to spontaneously break a global symmetry within a theory that has a local gauge symmetry, as in the Higgs mechanism.[citation needed]
