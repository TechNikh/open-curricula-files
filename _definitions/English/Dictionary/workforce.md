---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/workforce
offline_file: ""
offline_thumbnail: ""
uuid: 46b5f340-9092-4e35-aa67-e49c350ef6ad
updated: 1484310539
title: workforce
categories:
    - Dictionary
---
work force
     n : the force of workers available [syn: {workforce}, {manpower},
          {hands}, {men}]
