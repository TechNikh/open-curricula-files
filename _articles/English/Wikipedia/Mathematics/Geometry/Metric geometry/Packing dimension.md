---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Packing_dimension
offline_file: ""
offline_thumbnail: ""
uuid: ba5e14a6-ba4c-4a47-a0a0-d9fb1947ce35
updated: 1484309270
title: Packing dimension
tags:
    - Definitions
    - An example
    - Generalizations
    - Properties
categories:
    - Metric geometry
---
In mathematics, the packing dimension is one of a number of concepts that can be used to define the dimension of a subset of a metric space. Packing dimension is in some sense dual to Hausdorff dimension, since packing dimension is constructed by "packing" small open balls inside the given subset, whereas Hausdorff dimension is constructed by covering the given subset by such small open balls. The packing dimension was introduced by C. Tricot Jr. in 1982.
