---
version: 1
type: video
provider: YouTube
id: ZEml96_kyN4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Absolute%20value%20equations.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7b0d9512-b47f-4be4-898f-974c2bb8cf27
updated: 1486069563
title: Absolute value equations
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZEml96_kyN4/default.jpg
    - https://i3.ytimg.com/vi/ZEml96_kyN4/1.jpg
    - https://i3.ytimg.com/vi/ZEml96_kyN4/2.jpg
    - https://i3.ytimg.com/vi/ZEml96_kyN4/3.jpg
---
