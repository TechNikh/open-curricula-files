---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assigned
offline_file: ""
offline_thumbnail: ""
uuid: 32aa5139-c659-44b5-a7d2-51149ae5964f
updated: 1484310395
title: assigned
categories:
    - Dictionary
---
assigned
     adj : appointed to a post or duty; "assigned personnel"; "assigned
           duties" [ant: {unassigned}]
