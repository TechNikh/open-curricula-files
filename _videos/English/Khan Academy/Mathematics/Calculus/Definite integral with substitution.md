---
version: 1
type: video
provider: YouTube
id: CbUx0S8BCtA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Definite%20integral%20with%20substitution.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 924af42f-8ad7-4174-a3e6-72b2f2943f79
updated: 1486069696
title: Definite integral with substitution
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/CbUx0S8BCtA/default.jpg
    - https://i3.ytimg.com/vi/CbUx0S8BCtA/1.jpg
    - https://i3.ytimg.com/vi/CbUx0S8BCtA/2.jpg
    - https://i3.ytimg.com/vi/CbUx0S8BCtA/3.jpg
---
