---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forgiveness
offline_file: ""
offline_thumbnail: ""
uuid: 0db19e39-7e10-4ac2-91fc-d5664fbd20a2
updated: 1484310583
title: forgiveness
categories:
    - Dictionary
---
forgiveness
     n 1: compassionate feelings that support a willingness to forgive
     2: the act of excusing a mistake or offense [syn: {pardon}]
