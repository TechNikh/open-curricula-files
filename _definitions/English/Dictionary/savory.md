---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/savory
offline_file: ""
offline_thumbnail: ""
uuid: 6dcf304d-f9e4-4918-b86e-fd9391018908
updated: 1484310353
title: savory
categories:
    - Dictionary
---
savory
     adj 1: morally respectable or inoffensive; "a past that was
            scarcely savory" [syn: {savoury}, {inoffensive}] [ant:
             {unsavory}]
     2: having an agreeably pungent taste [syn: {piquant}, {savoury},
         {spicy}, {zesty}]
     3: pleasing to the sense of taste [syn: {mouth-watering}, {savoury},
         {tasty}]
     n 1: any of several aromatic herbs or subshrubs of the genus
          Satureja having spikes of flowers attractive to bees
     2: dwarf aromatic shrub of Mediterranean regions [syn: {Micromeria
        juliana}]
     3: either of two aromatic herbs of the mint family [syn: {savoury}]
     4: an aromatic or spicy dish served at the end of ...
