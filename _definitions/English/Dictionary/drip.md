---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drip
offline_file: ""
offline_thumbnail: ""
uuid: 887c8395-90ec-44d5-929a-a681588c383b
updated: 1484310258
title: drip
categories:
    - Dictionary
---
drip
     n 1: flowing in drops; the formation and falling of drops of
          liquid; "there's a drip through the roof" [syn: {trickle},
           {dribble}]
     2: the sound of a liquid falling drop by drop; "the constant
        sound of dripping irritated him" [syn: {dripping}]
     3: (architecture) a projection from a cornice or sill designed
        to protect the area below from rainwater (as over a window
        or doorway) [syn: {drip mold}, {drip mould}]
     v 1: fall in drops; "Water is dripping from the faucet"
     2: let or cause to fall in drops; "dribble oil into the
        mixture" [syn: {dribble}, {drop}]
     [also: {dripping}, {dripped}]
