---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cis_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 23f5c066-d54e-4174-a3fe-35cfac6708a6
updated: 1484309341
title: Cis (mathematics)
tags:
    - Overview
    - Relation to the complex exponential function
    - Mathematical identities
    - Derivative
    - Integral
    - Other properties
    - History
categories:
    - Trigonometry
---
cis is a rarely used mathematical notation in which cis(x) = cos(x) + i sin(x),[1][2][3][4][5] where cos is the cosine function, i is the imaginary unit and sin is the sine. The notation is redundant, as Euler's formula offers an even shorter and more general short-hand notation for cos(x) + i sin(x).
