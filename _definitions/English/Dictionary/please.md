---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/please
offline_file: ""
offline_thumbnail: ""
uuid: 93dda2b5-b300-4b98-9b51-04acea879759
updated: 1484310174
title: please
categories:
    - Dictionary
---
please
     adv : used in polite request; "please pay attention"
     v 1: give pleasure to or be pleasing to; "These colors please the
          senses"; "a pleasing sensation" [syn: {delight}] [ant: {displease}]
     2: be the will of or have the will (to); "he could do many
        things if he pleased"
     3: give satisfaction; "The waiters around her aim to please"
