---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/publisher
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484317321
title: publisher
categories:
    - Dictionary
---
publisher
     n 1: a firm in the publishing business [syn: {publishing house},
          {publishing firm}, {publishing company}]
     2: a person engaged in publishing periodicals or books or music
     3: the proprietor of a newspaper [syn: {newspaper publisher}]
