---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/staggering
offline_file: ""
offline_thumbnail: ""
uuid: e6c31026-bfef-4e7e-b391-cf30c64cf930
updated: 1484310579
title: staggering
categories:
    - Dictionary
---
staggering
     adj 1: walking unsteadily; "a stqaggering gait" [syn: {lurching}, {stumbling},
             {weaving}]
     2: so surprisingly impressive as to stun or overwhelm; "such an
        enormous response was astonishing"; "an astounding
        achievement"; "the amount of money required was
        staggering"; "suffered a staggering defeat"; "the figure
        inside the boucle dress was stupefying" [syn: {astonishing},
         {astounding}, {stupefying}]
