---
version: 1
type: video
provider: YouTube
id: tv205iQZR7c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Subject%20and%20Object%20English%20Grammar.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 032267af-8f22-4862-bda3-550ad498bc68
updated: 1486069632
title: Subject and Object English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/tv205iQZR7c/default.jpg
    - https://i3.ytimg.com/vi/tv205iQZR7c/1.jpg
    - https://i3.ytimg.com/vi/tv205iQZR7c/2.jpg
    - https://i3.ytimg.com/vi/tv205iQZR7c/3.jpg
---
