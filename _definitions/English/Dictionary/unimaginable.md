---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unimaginable
offline_file: ""
offline_thumbnail: ""
uuid: bffa4591-383c-4e0e-8650-db4a98891f65
updated: 1484310587
title: unimaginable
categories:
    - Dictionary
---
unimaginable
     adj : totally unlikely [syn: {impossible}, {inconceivable}, {out
           of the question}]
