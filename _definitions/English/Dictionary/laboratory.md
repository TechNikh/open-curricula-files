---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laboratory
offline_file: ""
offline_thumbnail: ""
uuid: b61241aa-ce4d-4f2f-b88b-64fd7baad994
updated: 1484310305
title: laboratory
categories:
    - Dictionary
---
laboratory
     n 1: a workplace for the conduct of scientific research [syn: {lab},
           {research lab}, {research laboratory}, {science lab}, {science
          laboratory}]
     2: a region resembling a laboratory inasmuch as it offers
        opportunities for observation and practice and
        experimentation; "the new nation is a testing ground for
        socioeconomic theories"; "Pakistan is a laboratory for
        studying the use of American troops to combat terrorism"
        [syn: {testing ground}]
