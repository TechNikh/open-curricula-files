---
version: 1
type: video
provider: YouTube
id: UL1jmJaUkaQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Stoichiometry-%20Chemistry%20for%20Massive%20Creatures%20-%20Crash%20Course%20Chemistry%20%236.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d4e6ac76-40f6-4286-a436-5e690a378943
updated: 1486069664
title: 'Stoichiometry- Chemistry for Massive Creatures - Crash Course Chemistry #6'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/UL1jmJaUkaQ/default.jpg
    - https://i3.ytimg.com/vi/UL1jmJaUkaQ/1.jpg
    - https://i3.ytimg.com/vi/UL1jmJaUkaQ/2.jpg
    - https://i3.ytimg.com/vi/UL1jmJaUkaQ/3.jpg
---
