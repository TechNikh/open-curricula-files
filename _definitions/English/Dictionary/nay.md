---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nay
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484540881
title: nay
categories:
    - Dictionary
---
nay
     n : a negative; "the nays have it" [ant: {yea}]
     adv : not this merely but also; not only so but; "each of us is
           peculiar, nay, in a sense unique"
