---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nomenclature
offline_file: ""
offline_thumbnail: ""
uuid: b53f84c8-8abe-45d6-a22a-b9c4caea617e
updated: 1484310421
title: nomenclature
categories:
    - Dictionary
---
nomenclature
     n : a system of words used in a particular discipline; "legal
         terminology"; "the language of sociology" [syn: {terminology},
          {language}]
