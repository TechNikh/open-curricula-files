---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nanometers
offline_file: ""
offline_thumbnail: ""
uuid: 9369b660-c3b6-407f-917d-d73bf20682b0
updated: 1484308392
title: Nanometre
tags:
    - use
    - History
categories:
    - Uncategorized
---
The nanometre (International spelling as used by the International Bureau of Weights and Measures; SI symbol: nm) or nanometer (American spelling) is a unit of length in the metric system, equal to one billionth of a metre (6991100000000000000♠0.000000001 m). The name combines the SI prefix nano- (from the Ancient Greek νάνος, nanos, "dwarf") with the parent unit name metre (from Greek μέτρον, metrοn, "unit of measurement"). It can be written in scientific notation as 6991100000000000000♠1×10−9 m, in engineering notation as 1 E−9 m, and is simply 1/7009100000000000000♠1000000000 metres. One nanometre equals ten ångströms. Every second your nail grows 1 nanometer
