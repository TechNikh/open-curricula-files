---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beetle
offline_file: ""
offline_thumbnail: ""
uuid: a814c65b-190d-41d7-9ffe-357d2acb688e
updated: 1484310295
title: beetle
categories:
    - Dictionary
---
beetle
     adj : jutting or overhanging; "beetle brows" [syn: {beetling}]
     n 1: insect having biting mouthparts and front wings modified to
          form horny covers overlying the membranous rear wings
     2: a tool resembling a hammer but with a large head (usually
        wooden); used to drive wedges or ram down paving stones or
        for crushing or beating or flattening or smoothing [syn: {mallet}]
     v 1: be suspended over or hang over; "This huge rock beetles over
          the edge of the town" [syn: {overhang}]
     2: fly or go in a manner resembling a beetle; "He beetled up
        the staircase"; "They beetled off home"
     3: beat with a beetle
