---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fight
offline_file: ""
offline_thumbnail: ""
uuid: 0734b48b-1fdf-497c-8996-4ae5e8750df0
updated: 1484310477
title: fight
categories:
    - Dictionary
---
fight
     n 1: the act of fighting; any contest or struggle; "a fight broke
          out at the hockey game"; "there was fighting in the
          streets"; "the unhappy couple got into a terrible scrap"
          [syn: {fighting}, {combat}, {scrap}]
     2: an intense verbal dispute; "a violent fight over the bill is
        expected in the Senate"
     3: a boxing match; "the fight was on television last night"
        [syn: {bout}]
     4: a hostile meeting of opposing military forces in the course
        of a war; "Grant won a decisive victory in the battle of
        Chickamauga"; "he lost his romantic ideas about war when
        he got into a real engagement" [syn: {battle}, ...
