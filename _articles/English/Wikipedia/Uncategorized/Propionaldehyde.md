---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Propanal
offline_file: ""
offline_thumbnail: ""
uuid: 8f430f13-2881-40d7-a61f-0b9673691ebd
updated: 1484308401
title: Propionaldehyde
tags:
    - Production
    - Laboratory preparation
    - Uses
    - Extraterrestrial occurrence
categories:
    - Uncategorized
---
Propionaldehyde or propanal is the organic compound with the formula CH3CH2CHO. It is a saturated 3-carbon aldehyde and is a structural isomer of acetone. It is a colorless liquid with a slightly irritating, fruity odor.
