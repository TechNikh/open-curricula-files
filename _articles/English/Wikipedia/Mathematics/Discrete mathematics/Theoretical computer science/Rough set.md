---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rough_set
offline_file: ""
offline_thumbnail: ""
uuid: fe7c90a5-2429-4794-b065-167be9d1b793
updated: 1484308678
title: Rough set
tags:
    - Definitions
    - Information system framework
    - 'Example: equivalence-class structure'
    - Definition of a rough set
    - Lower approximation and positive region
    - Upper approximation and negative region
    - Boundary region
    - The rough set
    - Objective analysis
    - Definability
    - Reduct and core
    - Attribute dependency
    - Rule extraction
    - Decision matrices
    - LERS rule induction system
    - Incomplete data
    - Applications
    - History
    - Extensions and generalizations
    - Rough membership
    - Other generalizations
categories:
    - Theoretical computer science
---
In computer science, a rough set, first described by Polish computer scientist Zdzisław I. Pawlak, is a formal approximation of a crisp set (i.e., conventional set) in terms of a pair of sets which give the lower and the upper approximation of the original set. In the standard version of rough set theory (Pawlak 1991), the lower- and upper-approximation sets are crisp sets, but in other variations, the approximating sets may be fuzzy sets.
