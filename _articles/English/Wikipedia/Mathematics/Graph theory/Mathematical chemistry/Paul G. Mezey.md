---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paul_G._Mezey
offline_file: ""
offline_thumbnail: ""
uuid: 2075e07a-90a4-4eba-b74b-0fd4fd548c06
updated: 1484309431
title: Paul G. Mezey
tags:
    - Biography
    - Awards and honors
categories:
    - Mathematical chemistry
---
Paul G. Mezey is a Hungarian-Canadian mathematical chemist, the Canada Research Chair in Scientific Modeling and Simulation in the Department of Chemistry at the Memorial University of Newfoundland.[1]
