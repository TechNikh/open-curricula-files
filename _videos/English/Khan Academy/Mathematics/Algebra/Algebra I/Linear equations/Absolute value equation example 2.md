---
version: 1
type: video
provider: YouTube
id: jaizi_1IB5c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Absolute%20value%20equation%20example%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7ad7a1bd-8781-4949-87eb-c96c47d79e0e
updated: 1486069557
title: Absolute value equation example 2
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/jaizi_1IB5c/default.jpg
    - https://i3.ytimg.com/vi/jaizi_1IB5c/1.jpg
    - https://i3.ytimg.com/vi/jaizi_1IB5c/2.jpg
    - https://i3.ytimg.com/vi/jaizi_1IB5c/3.jpg
---
