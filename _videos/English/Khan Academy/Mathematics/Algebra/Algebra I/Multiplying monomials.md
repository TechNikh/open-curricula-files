---
version: 1
type: video
provider: YouTube
id: iHnzLETGz2I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Multiplying%20monomials.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a9200550-a271-4cca-b703-aa273e6c2b54
updated: 1486069565
title: Multiplying monomials
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/iHnzLETGz2I/default.jpg
    - https://i3.ytimg.com/vi/iHnzLETGz2I/1.jpg
    - https://i3.ytimg.com/vi/iHnzLETGz2I/2.jpg
    - https://i3.ytimg.com/vi/iHnzLETGz2I/3.jpg
---
