---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nato
offline_file: ""
offline_thumbnail: ""
uuid: a2baa281-f01e-409c-a511-2cf092851531
updated: 1484310180
title: nato
categories:
    - Dictionary
---
NATO
     n : an international organization created in 1949 by the North
         Atlantic Treaty for purposes of collective security [syn:
          {North Atlantic Treaty Organization}]
