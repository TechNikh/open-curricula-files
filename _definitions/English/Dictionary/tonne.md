---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tonne
offline_file: ""
offline_thumbnail: ""
uuid: 75c7cf3f-2d85-4981-bcf1-e1ecbc33c78f
updated: 1484310533
title: tonne
categories:
    - Dictionary
---
tonne
     n : a unit of weight equivalent to 1000 kilograms [syn: {metric
         ton}, {MT}, {t}]
