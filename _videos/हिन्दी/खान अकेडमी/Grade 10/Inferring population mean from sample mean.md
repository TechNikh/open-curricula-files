---
version: 1
type: video
provider: YouTube
id: tfdf80wLCBo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bb14ba56-3ad7-4210-ae3a-d2d76f63cb04
updated: 1486069618
title: Inferring population mean from sample mean
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/tfdf80wLCBo/default.jpg
    - https://i3.ytimg.com/vi/tfdf80wLCBo/1.jpg
    - https://i3.ytimg.com/vi/tfdf80wLCBo/2.jpg
    - https://i3.ytimg.com/vi/tfdf80wLCBo/3.jpg
---
