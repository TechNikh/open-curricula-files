---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unrelenting
offline_file: ""
offline_thumbnail: ""
uuid: 48e5d5ae-7e4e-4e80-aaca-f54428a1d94c
updated: 1484310589
title: unrelenting
categories:
    - Dictionary
---
unrelenting
     adj 1: not to be placated or appeased or moved by entreaty; "grim
            determination"; "grim necessity"; "Russia's final
            hour, it seemed, approached with inexorable
            certainty"; "relentless persecution"; "the stern
            demands of parenthood" [syn: {grim}, {inexorable}, {relentless},
             {stern}, {unappeasable}, {unforgiving}]
     2: never-ceasing; "the relentless beat of the drums" [syn: {persistent},
         {relentless}]
