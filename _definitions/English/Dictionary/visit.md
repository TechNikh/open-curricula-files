---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/visit
offline_file: ""
offline_thumbnail: ""
uuid: 148348a5-6506-4184-b8fe-83aecf0a61e7
updated: 1484310469
title: visit
categories:
    - Dictionary
---
visit
     n 1: the act of going to see some person or place or thing for a
          short time; "he dropped by for a visit"
     2: a meeting arranged by the visitor to see someone (such as a
        doctor or lawyer) for treatment or advice; "he scheduled a
        visit to the dentist"
     3: the act of visiting in an official capacity (as for an
        inspection)
     4: the act of going to see some person in a professional
        capacity; "a visit to the dentist"
     5: a temporary stay (e.g., as a guest) [syn: {sojourn}]
     v 1: visit a place, as for entertainment; "We went to see the
          Eiffel Tower in the morning" [syn: {see}]
     2: go to certain places as for ...
