---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ill-treatment
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484633521
title: ill-treatment
categories:
    - Dictionary
---
ill-treatment
     n : cruel or inhumane treatment [syn: {maltreatment}, {ill-usage},
          {abuse}]
