---
version: 1
type: video
provider: YouTube
id: yX3JpmEgNHo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%234d.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7cdd608f-c1a6-401e-9c8b-434fd7c0db43
updated: 1486069689
title: '2011 Calculus AB Free Response #4d'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/yX3JpmEgNHo/default.jpg
    - https://i3.ytimg.com/vi/yX3JpmEgNHo/1.jpg
    - https://i3.ytimg.com/vi/yX3JpmEgNHo/2.jpg
    - https://i3.ytimg.com/vi/yX3JpmEgNHo/3.jpg
---
