---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/night
offline_file: ""
offline_thumbnail: ""
uuid: 6985a185-99c7-4f8e-8e9a-c805bef84174
updated: 1484310601
title: night
categories:
    - Dictionary
---
night
     n 1: the time after sunset and before sunrise while it is dark
          outside [syn: {nighttime}, {dark}] [ant: {day}]
     2: the time between sunset and midnight; "he watched television
        every night"
     3: the period spent sleeping; "I had a restless night"
     4: the dark part of the diurnal cycle considered a time unit;
        "three nights later he collapsed"
     5: darkness; "it vanished into the night"
     6: a shortening of nightfall; "they worked from morning to
        night"
     7: a period of ignorance or backwardness or gloom
     8: Roman goddess of night; daughter of Erebus; counterpart of
        Greek Nyx [syn: {Nox}]
