---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rationing
offline_file: ""
offline_thumbnail: ""
uuid: 4bcf20ba-0a92-43cb-8133-5d5e698827da
updated: 1484310587
title: rationing
categories:
    - Dictionary
---
rationing
     n : the act of rationing; "during the war the government imposed
         rationing of food and gasoline"
