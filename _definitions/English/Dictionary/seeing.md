---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seeing
offline_file: ""
offline_thumbnail: ""
uuid: a1e777c4-257b-4d28-88d2-3e2bb436616c
updated: 1484310321
title: seeing
categories:
    - Dictionary
---
seeing
     adj : having vision, not blind
     n 1: perception by means of the eyes [syn: {visual perception}, {beholding}]
     2: normal use of the faculty of vision [syn: {eyesight}, {sightedness}]
