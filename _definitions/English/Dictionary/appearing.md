---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appearing
offline_file: ""
offline_thumbnail: ""
uuid: f9b4b142-b721-4839-ac71-0b946edc3b34
updated: 1484310156
title: appearing
categories:
    - Dictionary
---
appearing
     n : formal attendance (in court or at a hearing) of a party in
         an action [syn: {appearance}, {coming into court}]
