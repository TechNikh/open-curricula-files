---
version: 1
type: video
provider: YouTube
id: 2Moq13tZ684
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a752b492-17db-438e-9b83-55a5857c127d
updated: 1486069634
title: Water Dam Project Working Science Craft Model
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/2Moq13tZ684/default.jpg
    - https://i3.ytimg.com/vi/2Moq13tZ684/1.jpg
    - https://i3.ytimg.com/vi/2Moq13tZ684/2.jpg
    - https://i3.ytimg.com/vi/2Moq13tZ684/3.jpg
---
