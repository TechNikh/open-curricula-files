---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sprinkling
offline_file: ""
offline_thumbnail: ""
uuid: 6cd074a2-f0fc-4970-aba1-e8bc8dac38a1
updated: 1484310366
title: sprinkling
categories:
    - Dictionary
---
sprinkling
     n 1: a small number dispersed haphazardly; "the first scatterings
          of green" [syn: {scattering}]
     2: a light shower that falls in some locations and not others
        nearby [syn: {scattering}, {sprinkle}]
     3: the act of sprinkling water in baptism (rare) [syn: {aspersion}]
     4: the act of sprinkling or splashing water; "baptized with a
        sprinkling of holy water"; "a sparge of warm water over
        the malt" [syn: {sprinkle}, {sparge}]
