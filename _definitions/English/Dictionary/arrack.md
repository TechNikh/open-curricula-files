---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arrack
offline_file: ""
offline_thumbnail: ""
uuid: 22f31c21-5c3d-4005-9fe8-35fcfab6eaa7
updated: 1484310168
title: arrack
categories:
    - Dictionary
---
arrack
     n : any of various strong liquors distilled from the fermented
         sap of toddy palms or from fermented molasses [syn: {arak}]
