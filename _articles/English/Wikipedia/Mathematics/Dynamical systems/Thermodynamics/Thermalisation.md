---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermalisation
offline_file: ""
offline_thumbnail: ""
uuid: fa7265c8-a143-40de-be4b-82f502aab96e
updated: 1484308870
title: Thermalisation
categories:
    - Thermodynamics
---
In physics, thermalisation (in American English thermalization) is the process of physical bodies reaching thermal equilibrium through mutual interaction. In general the natural tendency of a system is towards a state of equipartition of energy or uniform temperature, maximising the system's entropy.
