---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equidistribution_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 6700a5ba-81f0-436c-b115-86b3e326e72b
updated: 1484308777
title: Equidistribution theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-Equidistribution_theorem.svg.png
tags:
    - History
    - Historical references
    - Modern references
categories:
    - Ergodic theory
---
is uniformly distributed on the circle 
  
    
      
        
          R
        
        
          /
        
        
          Z
        
      
    
    {\displaystyle \mathbb {R} /\mathbb {Z} }
  
, when a is an irrational number. It is a special case of the ergodic theorem where one takes the normalized angle measure 
  
    
      
        μ
        =
        
          
            
              d
              θ
            
            
              2
              π
            
          
        
      
    
    {\displaystyle \mu ={\frac {d\theta }{2\pi }}}
  
.
