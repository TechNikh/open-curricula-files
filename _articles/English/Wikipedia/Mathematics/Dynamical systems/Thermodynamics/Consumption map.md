---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Consumption_map
offline_file: ""
offline_thumbnail: ""
uuid: 65ad1ccb-c69a-4d9f-9057-ff8ea40bcf04
updated: 1484308843
title: Consumption map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Brake_specific_fuel_consumption.svg.png
categories:
    - Thermodynamics
---
The consumption map or efficiency map[1] shows the brake specific fuel consumption in g per kWh over mean effective pressure per rotational speed of an internal combustion engine.
