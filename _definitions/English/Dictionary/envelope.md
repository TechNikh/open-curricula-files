---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/envelope
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484513221
title: envelope
categories:
    - Dictionary
---
envelope
     n 1: a flat rectangular paper container for papers
     2: any wrapper or covering
     3: a curve that is tangent to each of a family of curves
     4: a natural covering (as by a fluid); "the spacecraft detected
        an envelope of gas around the comet"
     5: the maximum operating capability of a system; "test pilots
        try to push the envelope"
     6: the bag containing the gas in a balloon [syn: {gasbag}]
