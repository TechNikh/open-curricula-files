---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/production
offline_file: ""
offline_thumbnail: ""
uuid: 116587da-789b-4222-af50-8ba2df6f8d7d
updated: 1484310357
title: production
categories:
    - Dictionary
---
production
     n 1: (economics) manufacturing or mining or growing something
          (usually in large quantities) for sale; "he introduced
          more efficient methods of production"
     2: a presentation for the stage or screen or radio or
        television; "have you seen the new production of Hamlet?"
     3: the act or process of producing something; "Shakespeare's
        production of poetry was enormous"; "the production of
        white blood cells"
     4: an artifact that has been created by someone or some
        process; "they improve their product every year"; "they
        export most of their agricultural production" [syn: {product}]
     5: (law) the act of ...
