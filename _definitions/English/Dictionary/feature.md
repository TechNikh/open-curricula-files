---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feature
offline_file: ""
offline_thumbnail: ""
uuid: 53e7ecbf-b398-48db-97ab-43a8a51bda67
updated: 1484310284
title: feature
categories:
    - Dictionary
---
feature
     n 1: a prominent aspect of something; "the map showed roads and
          other features"; "generosity is one of his best
          characteristics" [syn: {characteristic}]
     2: the characteristic parts of a person's face: eyes and nose
        and mouth and chin; "an expression of pleasure crossed his
        features"; "his lineaments were very regular" [syn: {lineament}]
     3: the principal (full-length) film in a program at a movie
        theater; "the feature tonight is `Casablanca'" [syn: {feature
        film}]
     4: a special or prominent article in a newspaper or magazine;
        "they ran a feature on retirement planning" [syn: {feature
        article}]
     ...
