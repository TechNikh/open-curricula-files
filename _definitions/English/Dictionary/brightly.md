---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brightly
offline_file: ""
offline_thumbnail: ""
uuid: a46e3ca2-9f4b-41c3-af1a-7ef3b8133fd2
updated: 1484310381
title: brightly
categories:
    - Dictionary
---
brightly
     adv : with brightness; "the stars shone brilliantly"; "the windows
           glowed jewel bright" [syn: {brilliantly}, {bright}]
