---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Network_Description_Language
offline_file: ""
offline_thumbnail: ""
uuid: 809c34dd-96f8-4192-bf82-8bb76885c447
updated: 1484309438
title: Network Description Language
categories:
    - Network theory
---
Network Description Language (NDL) is a tool to reduce the complexity as networks evolve into the future. NDL enables both humans and machines to have a better grasp on today’s highly evolved networks to ease time consuming and tedious tasks being performed by humans. Through the use of Resource Description Framework (RDF), researchers have been able to create an ontology for complex networks, thus creating a clear view of any network.
