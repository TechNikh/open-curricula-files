---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Acceleration_(differential_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 8aa453d4-3255-4032-a744-9817d7f07dbf
updated: 1484309200
title: Acceleration (differential geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/370px-ACC_Map_crop_2014_1.png
tags:
    - Formal definition
    - Notes
categories:
    - Differential geometry
---
In mathematics and physics, acceleration is the rate of change of velocity of a curve with respect to a given linear connection. This operation provides us with a measure of the rate and direction of the "bend".[1][2]
