---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maharashtra
offline_file: ""
offline_thumbnail: ""
uuid: 453e6b33-6ba4-4ab9-8dcf-5ee9c81c03dd
updated: 1484308422
title: Maharashtra
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Maharashtra_Divisions_Eng.svg.png
tags:
    - Etymology
    - History
    - Geography and Climate
    - biodiversity
    - Regions, divisions and districts
    - Demographics
    - Government and administration
    - Economy
    - Transport
    - Education and social development
    - Infrastructure
    - Healthcare
    - Energy
    - culture
    - Cuisine
    - Attire
    - Music and dance
    - Literature
    - Films
    - Theatre
    - Media
    - Sports
categories:
    - Uncategorized
---
Maharashtra (/mɑːhəˈrɑːʃtrə/; Marathi: महाराष्ट्र pronunciation: locally: [məharaːʂʈrə] ( listen), abbr. MH) is a state in the western region of India and is India's third-largest state by area and is also the world's second-most populous sub-national entity. It has over 112 million inhabitants and its capital, Mumbai, has a population of approximately 18 million. Nagpur is Maharashtra's second capital as well as its winter capital.[5] Maharashtra's business opportunities along with its potential to offer a higher standard of living attract migrants from all over India.
