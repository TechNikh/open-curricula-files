---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heat_engine
offline_file: ""
offline_thumbnail: ""
uuid: 039b13b3-7d0d-4c96-9e0e-350378df34e8
updated: 1484308846
title: Heat engine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Heat_engine.png
tags:
    - Overview
    - Power
    - Everyday examples
    - Examples of heat engines
    - "Earth's heat engine"
    - Phase-change cycles
    - Gas-only cycles
    - Liquid only cycle
    - Electron cycles
    - Magnetic cycles
    - Cycles used for refrigeration
    - Evaporative heat engines
    - Mesoscopic heat engines
    - Efficiency
    - Endoreversible heat engines
    - History
    - Heat engine enhancements
    - Heat engine processes
categories:
    - Thermodynamics
---
In thermodynamics, a heat engine is a system that converts heat or thermal energy—and chemical energy—to mechanical energy, which can then be used to do mechanical work.[1][2] It does this by bringing a working substance from a higher state temperature to a lower state temperature. A heat "source" generates thermal energy that brings the working substance to the high temperature state. The working substance generates work in the "working body" of the engine while transferring heat to the colder "sink" until it reaches a low temperature state. During this process some of the thermal energy is converted into work by exploiting the properties of the working substance. The working substance ...
