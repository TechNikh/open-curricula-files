---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/journalist
offline_file: ""
offline_thumbnail: ""
uuid: c71ef5fe-0420-4ebf-8be4-2639284d7bee
updated: 1484310561
title: journalist
categories:
    - Dictionary
---
journalist
     n 1: a writer for newspapers and magazines
     2: someone who keeps a diary or journal [syn: {diarist}, {diary
        keeper}]
