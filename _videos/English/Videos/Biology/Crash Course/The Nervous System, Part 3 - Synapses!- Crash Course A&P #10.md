---
version: 1
type: video
provider: YouTube
id: VitFvNvRIIY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/The%20Nervous%20System%2C%20Part%203%20-%20Synapses%21-%20Crash%20Course%20A%26P%20%2310.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c37f5138-2b45-4fc3-a7c3-ce0227142658
updated: 1486069681
title: 'The Nervous System, Part 3 - Synapses!- Crash Course A&P #10'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/VitFvNvRIIY/default.jpg
    - https://i3.ytimg.com/vi/VitFvNvRIIY/1.jpg
    - https://i3.ytimg.com/vi/VitFvNvRIIY/2.jpg
    - https://i3.ytimg.com/vi/VitFvNvRIIY/3.jpg
---
