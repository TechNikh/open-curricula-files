---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hindustan
offline_file: ""
offline_thumbnail: ""
uuid: e20e0207-9a25-4f27-bd44-21e1005c6d4d
updated: 1484308456
title: Hindustan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Indo-Gangetic_Plain.hu.png
tags:
    - Etymology
    - Current usage
    - Geographic area
    - people
    - language
    - General sources
categories:
    - Uncategorized
---
Hindustan pronunciation (help·info) (ہندوستان or हिंदुस्तान) is a common geographic term for the northern/northwestern Indian subcontinent.[1][2] The terms Hind and Hindustān were current in Persian and Arabic at the time of the 11th century Turkic conquests.
