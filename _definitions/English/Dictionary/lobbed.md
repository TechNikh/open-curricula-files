---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lobbed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484591281
title: lobbed
categories:
    - Dictionary
---
lob
     n 1: an easy return of a tennis ball in a high arc
     2: the act of propelling something (as a ball or shell etc.) in
        a high arc
     v : propel in a high arc; "lob the tennis ball"
     [also: {lobbing}, {lobbed}]
