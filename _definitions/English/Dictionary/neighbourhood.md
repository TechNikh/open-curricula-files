---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neighbourhood
offline_file: ""
offline_thumbnail: ""
uuid: 18911b7c-fe2a-414c-8543-e6791d17f833
updated: 1484310447
title: neighbourhood
categories:
    - Dictionary
---
neighbourhood
     n 1: a surrounding or nearby region; "the plane crashed in the
          vicinity of Asheville"; "it is a rugged locality"; "he
          always blames someone else in the immediate
          neighborhood"; "I will drop in on you the next time I am
          in this neck of the woods" [syn: {vicinity}, {locality},
           {neighborhood}, {neck of the woods}]
     2: people living near one another; "it is a friendly
        neighborhood"; "my neighborhood voted for Bush" [syn: {neighborhood}]
