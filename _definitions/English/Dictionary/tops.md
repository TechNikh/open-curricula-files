---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tops
offline_file: ""
offline_thumbnail: ""
uuid: 7bf52a52-1312-44c7-a58e-01924708954d
updated: 1484310154
title: tops
categories:
    - Dictionary
---
tops
     adj : of the highest quality; "an ace reporter"; "a crack shot";
           "a first-rate golfer"; "a super party"; "played
           top-notch tennis"; "an athlete in tiptop condition";
           "she is absolutely tops" [syn: {ace}, {A-one}, {crack},
            {first-rate}, {super}, {tiptop}, {topnotch}, {tops(p)}]
