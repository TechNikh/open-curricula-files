---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/device
offline_file: ""
offline_thumbnail: ""
uuid: 1e404a6d-94c0-48c2-8611-65d28ff42750
updated: 1484310202
title: device
categories:
    - Dictionary
---
device
     n 1: an instrumentality invented for a particular purpose; "the
          device is small enough to wear on your wrist"; "a device
          intended to conserve water"
     2: something in an artistic work designed to achieve a
        particular effect
     3: any clever (deceptive) maneuver; "he would stoop to any
        device to win a point" [syn: {gimmick}, {twist}]
     4: any ornamental pattern or design (as in embroidery)
     5: an emblematic design (especially in heraldry); "he was
        recognized by the device on his shield"
