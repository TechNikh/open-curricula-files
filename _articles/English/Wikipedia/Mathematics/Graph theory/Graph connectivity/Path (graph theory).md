---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Path_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: a8c80727-217d-46af-bb77-59d7beaba127
updated: 1484309400
title: Path (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Snake-in-the-box_and_Hamiltonian_path.svg.png
tags:
    - Definitions
    - Examples
    - Finding paths
categories:
    - Graph connectivity
---
In graph theory, a path in a graph is a finite or infinite sequence of edges which connect a sequence of vertices which, by most definitions, are all distinct from one another. In a directed graph, a directed path (sometimes called dipath[1]) is again a sequence of edges (or arcs) which connect a sequence of vertices, but with the added restriction that the edges all be directed in the same direction.
