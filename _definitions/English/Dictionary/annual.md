---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/annual
offline_file: ""
offline_thumbnail: ""
uuid: 7935f6a3-67d9-4da1-9fb2-4a974da255e0
updated: 1484310305
title: annual
categories:
    - Dictionary
---
annual
     adj 1: occurring or payable every year; "an annual trip to Paris";
            "yearly medical examinations"; "annual (or yearly)
            income" [syn: {yearly}]
     2: completing its life cycle within a year; "a border of annual
        flowering plants" [syn: {one-year}] [ant: {biennial}, {perennial}]
     n 1: a plant that completes its entire life cycle within the
          space of a year
     2: a reference book that is published regularly once every year
        [syn: {yearbook}]
