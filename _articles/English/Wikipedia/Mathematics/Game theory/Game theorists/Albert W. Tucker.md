---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Albert_W._Tucker
offline_file: ""
offline_thumbnail: ""
uuid: 3608a6b1-a6e9-4fc4-a1b9-1612da2dac96
updated: 1484309029
title: Albert W. Tucker
categories:
    - Game theorists
---
Albert William Tucker (28 November 1905 – 25 January 1995) was a Canadian mathematician who made important contributions in topology, game theory, and non-linear programming.[1]
