---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slope
offline_file: ""
offline_thumbnail: ""
uuid: e45da2de-3e66-4d2c-aaab-6212b0196ca2
updated: 1484310244
title: slope
categories:
    - Dictionary
---
slope
     n 1: an elevated geological formation; "he climbed the steep
          slope"; "the house was built on the side of the
          mountain" [syn: {incline}, {side}]
     2: the property possessed by a line or surface that departs
        from the horizontal; "a five-degree gradient" [syn: {gradient}]
     v : be at an angle; "The terrain sloped down" [syn: {incline}, {pitch}]
