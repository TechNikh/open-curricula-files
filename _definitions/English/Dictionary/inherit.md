---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inherit
offline_file: ""
offline_thumbnail: ""
uuid: 1207fd9b-85ef-4aac-9703-cf8c148f79ae
updated: 1484310289
title: inherit
categories:
    - Dictionary
---
inherit
     v 1: obtain from someone after their death; "I inherited a castle
          from my French grandparents" [syn: {come into}]
     2: receive from a predecessor; "The new chairman inherited many
        problems from the previous chair"
     3: receive by genetic transmission; "I inherited my good
        eyesight from my mother"
