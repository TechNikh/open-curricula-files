---
version: 1
type: video
provider: YouTube
id: vX_WLrcgikc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4508d6a3-c4af-4b3e-a339-beb7e1b278a5
updated: 1486069656
title: 'Entropy is not disorder- micro-state vs macro-state'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/vX_WLrcgikc/default.jpg
    - https://i3.ytimg.com/vi/vX_WLrcgikc/1.jpg
    - https://i3.ytimg.com/vi/vX_WLrcgikc/2.jpg
    - https://i3.ytimg.com/vi/vX_WLrcgikc/3.jpg
---
