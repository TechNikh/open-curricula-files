---
version: 1
type: video
provider: YouTube
id: LS67vS10O5Y
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/pH%20and%20pOH-%20Crash%20Course%20Chemistry%20%2330.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3b59204b-2d6a-4650-b9e2-95ba775ed813
updated: 1486069680
title: 'pH and pOH- Crash Course Chemistry #30'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/LS67vS10O5Y/default.jpg
    - https://i3.ytimg.com/vi/LS67vS10O5Y/1.jpg
    - https://i3.ytimg.com/vi/LS67vS10O5Y/2.jpg
    - https://i3.ytimg.com/vi/LS67vS10O5Y/3.jpg
---
