---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pumped
offline_file: ""
offline_thumbnail: ""
uuid: 6ed0da71-f21e-449b-99bf-3ff2d9f7d30f
updated: 1484310260
title: pumped
categories:
    - Dictionary
---
pumped
     adj : tense with excitement and enthusiasm as from a rush of
           adrenaline; "we were really pumped up for the race";
           "he was so pumped he couldn't sleep" [syn: {pumped-up(a)},
            {pumped up(p)}, {pumped(p)}, {wired}]
