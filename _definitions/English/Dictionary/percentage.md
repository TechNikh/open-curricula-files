---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/percentage
offline_file: ""
offline_thumbnail: ""
uuid: 7e985694-e7ed-44f0-94e4-eac38adb8d36
updated: 1484310330
title: percentage
categories:
    - Dictionary
---
percentage
     n 1: a proportion multiplied by 100 [syn: {percent}, {per centum},
           {pct}]
     2: assets belonging to or due to or contributed by an
        individual person or group; "he wanted his share in cash"
        [syn: {share}, {portion}, {part}]
