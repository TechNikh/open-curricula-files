---
version: 1
type: video
provider: YouTube
id: QbcBpuDuUms
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Tricks/Amazing%20Magnetic%20Device%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fd7fbbec-6eb2-4338-8c3c-e4ce29c3d4a2
updated: 1486069661
title: Amazing Magnetic Device!
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/QbcBpuDuUms/default.jpg
    - https://i3.ytimg.com/vi/QbcBpuDuUms/1.jpg
    - https://i3.ytimg.com/vi/QbcBpuDuUms/2.jpg
    - https://i3.ytimg.com/vi/QbcBpuDuUms/3.jpg
---
