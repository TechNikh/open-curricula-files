---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surround
offline_file: ""
offline_thumbnail: ""
uuid: 4fbf342d-b388-4ccf-ad86-45db09870e2b
updated: 1484310405
title: surround
categories:
    - Dictionary
---
surround
     n : the area in which something exists or lives; "the
         country--the flat agricultural surround" [syn: {environment},
          {environs}, {surroundings}]
     v 1: be around; "Developments surround the town"; "The river
          encircles the village" [syn: {environ}, {encircle}, {circle},
           {round}, {ring}]
     2: extend on all sides of simultaneously; encircle; "The forest
        surrounds my property" [syn: {skirt}, {border}]
     3: envelop completely; "smother the meat in gravy" [syn: {smother}]
     4: surround so as to force to give up; "The Turks besieged
        Vienna" [syn: {besiege}, {beleaguer}, {hem in}, {circumvent}]
     5: surround with a ...
