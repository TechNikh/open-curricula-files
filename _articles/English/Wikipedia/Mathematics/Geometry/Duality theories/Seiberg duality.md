---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Seiberg_duality
offline_file: ""
offline_thumbnail: ""
uuid: 3380d193-6818-4d17-a031-ce13bce242cd
updated: 1484309237
title: Seiberg duality
tags:
    - The statement of Seiberg duality
    - Relations between the original and dual theories
    - Evidence for Seiberg duality
    - Generalizations
categories:
    - Duality theories
---
In quantum field theory, Seiberg duality, conjectured by Nathan Seiberg, is an S-duality relating two different supersymmetric QCDs. The two theories are not identical, but they agree at low energies. More precisely under a renormalization group flow they flow to the same IR fixed point, and so are in the same universality class.
