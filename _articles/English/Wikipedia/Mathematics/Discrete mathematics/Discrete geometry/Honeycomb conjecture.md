---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Honeycomb_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: bb4229e0-45e2-4650-9fa1-a05839ef7d00
updated: 1484308611
title: Honeycomb conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hexagons.jpg
tags:
    - Statement
    - History
categories:
    - Discrete geometry
---
The honeycomb conjecture states that a regular hexagonal grid or honeycomb is the best way to divide a surface into regions of equal area with the least total perimeter. The conjecture was proven in 1999 by mathematician Thomas C. Hales.
