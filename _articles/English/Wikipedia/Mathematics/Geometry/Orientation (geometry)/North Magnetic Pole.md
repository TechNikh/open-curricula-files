---
version: 1
type: article
id: https://en.wikipedia.org/wiki/North_Magnetic_Pole
offline_file: ""
offline_thumbnail: ""
uuid: fa45148b-0771-4320-ac61-4eb7112be957
updated: 1484309297
title: North Magnetic Pole
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-North_Magnetic_Poles.svg.png
tags:
    - Polarity
    - History
    - Expeditions and measurements
    - Early
    - Project Polaris
    - Modern (post-1996)
    - Magnetic north and magnetic declination
    - North Geomagnetic Pole
    - Geomagnetic reversal
    - Notes and references
categories:
    - Orientation (geometry)
---
The North Magnetic Pole is the point on the surface of Earth's Northern Hemisphere at which the planet's magnetic field points vertically downwards (in other words, if a magnetic compass needle is allowed to rotate about a horizontal axis, it will point straight down). There is only one location where this occurs, near (but distinct from) the Geographic North Pole and the Geomagnetic North Pole.
