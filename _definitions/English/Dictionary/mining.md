---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mining
offline_file: ""
offline_thumbnail: ""
uuid: 52c6922b-a617-43df-bc30-45bfb462a458
updated: 1484310240
title: mining
categories:
    - Dictionary
---
mining
     n 1: the act of extracting ores or coal etc from the earth [syn:
          {excavation}]
     2: laying explosive mines in concealed places to destroy enemy
        personnel and equipment [syn: {minelaying}]
