---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/asian
offline_file: ""
offline_thumbnail: ""
uuid: bf5451fb-4906-4f82-b010-60e59389fcc2
updated: 1484310518
title: asian
categories:
    - Dictionary
---
Asian
     adj 1: of or relating to or characteristic of Asia or the peoples
            of Asia or their languages or culture; "Asian
            countries" [syn: {Asiatic}]
     2: denoting or characteristic of the biogeographic region
        including southern Asia and the Malay Archipelago as far
        as the Philippines and Borneo and Java; "Oriental
        politeness"; "for people of South and East Asian ancestry
        the term `Asian' is preferred to `Oriental'"; "Asian
        ancestry" [syn: {oriental}]
     n : a native or inhabitant of Asia [syn: {Asiatic}]
