---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Two_ears_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 193336c0-22ac-430f-82af-0879d382b8be
updated: 1484309127
title: Two ears theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Triangulation_3-coloring.svg.png
tags:
    - Statement of the theorem
    - Ears from triangulations
    - Related types of vertex
    - History and proof
categories:
    - Theorems in geometry
---
In geometry, the two ears theorem states that every simple polygon with more than three vertices has at least two ears, vertices that can be removed from the polygon without introducing any crossings. The two ears theorem is equivalent to the existence of polygon triangulations. It is frequently attributed to Gary H. Meisters, but was proved earlier by Max Dehn.
