---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Myerson%E2%80%93Satterthwaite_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: b714ab70-e0f4-4696-aa10-759f1d02c753
updated: 1484309046
title: Myerson–Satterthwaite theorem
tags:
    - Notation
    - Requirements
    - Statement
    - Extensions
categories:
    - Mechanism design
---
The Myerson–Satterthwaite theorem is an important result in mechanism design and the economics of asymmetric information, due to Roger Myerson and Mark Satterthwaite. Informally, the result says that there is no efficient way for two parties to trade a good when they each have secret and probabilistically varying valuations for it, without the risk of forcing one party to trade at a loss.
