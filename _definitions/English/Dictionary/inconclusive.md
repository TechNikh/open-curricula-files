---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inconclusive
offline_file: ""
offline_thumbnail: ""
uuid: 0274db8b-5b9d-4291-b5af-d06272b824c9
updated: 1484310589
title: inconclusive
categories:
    - Dictionary
---
inconclusive
     adj 1: not conclusive; not putting an end to doubt or question; "an
            inconclusive reply"; "inconclusive evidence"; "the
            inconclusive committee vote" [ant: {conclusive}]
     2: leading to no final results or outcome; "an inconclusive
        experiment"
