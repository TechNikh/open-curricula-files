---
version: 1
type: annotation
id: 'http://eschool2go.org/sites/default/files/chapter_8%20People%20and%20Migration.pdf'
uuid: 937568c6-e396-4f40-a20c-c9f39796e605
updated: "1483818481"
annotations:
    - 
        quote: ramaiah
        id: https://en.wikipedia.org/wiki/Ramaiah
        uuid: 2dbf0d88-6925-4d9c-b087-9973e5af6334
        text: Ramaiah
        comment: S. Ramaiah
    - 
        quote: mandal
        id: https://en.wikipedia.org/wiki/Mandal
        uuid: 13dc8dbb-ccc8-4ef6-af7f-e1abf987fb9f
        text: Tehsil
        comment: >
            A tehsil (also known as a mandal or taluka) is an
            administrative division of some countries of South Asia. It
            is an area of land with a city or town that serves as its
            administrative centre, with possible additional towns, and
            usually a number of villages. The terms in India have
            replaced earlier geographical terms, such as pargana,
            pergunnah and thannah, used under the Delhi Sultanate and
            the British Raj.
    - 
        quote: chityal
        id: https://en.wikipedia.org/wiki/Chityal
        uuid: d618c12c-72e7-4f28-ac14-a19466ed653e
        text: Chityal
        comment: 'Chityal may refer to:'
    - 
        quote: choutuppal
        id: https://en.wikipedia.org/wiki/Choutuppal
        uuid: 66ef3c98-1bb9-4f1d-af4e-9d802996596a
        text: Choutuppal
        comment: 'Choutuppal is a census town in Yadadri Bhuvanagiri district of the Indian state of Telangana. It is located in Choutuppal mandal of Choutuppal division.[1] Its part of Hyderabad Metropolitan Development Authority.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-City_Bus_Hyd.jpg
            uuid: e6b815ff-a48a-4bbd-b803-6de00785c937
            comment: >
                A local bus that commutes between Dilsukhnagar and
                Choutuppal
    - 
        quote: rangareddy
        id: https://en.wikipedia.org/wiki/Rangareddy
        uuid: c5ba5f07-9e7d-4279-bef5-d1be65aea706
        text: Ranga Reddy district
        comment: 'Ranga Reddy District (formerly: K. V. Ranga Reddy District and Hyderabad (Rural) District), is a district in the Indian state of Telangana. The district headquarters are located at Shamshabad.[3] The district headquarters are located at Shamshabad.[4] It was named after the former deputy chief minister of the united state of Andhra Pradesh, K.V.Ranga Reddy.[5]'
    - 
        quote: sindhu
        id: https://en.wikipedia.org/wiki/Sindhu
        uuid: fff99655-5e43-4a21-9bbd-3a8905b9ca5e
        text: Indus River
        comment: >
            The Indus River, also called the Sindhū River or Abāsīn,
            is a major south-flowing river in South Asia. The total
            length of the river is 3,180 km (1,980 mi) which makes it
            one of longest rivers in Asia. Originating in the western
            part of Tibet in the vicinity of Mount Kailash and Lake
            Manasarovar, the river runs a course through Ladakh,
            Gilgit-Baltistan, and Khyber Pakhtunkhwa, and then flows
            along the entire length of Punjab to merge into the Arabian
            Sea near the port city of Karachi in Sindh. It is the
            longest river of Pakistan.
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/280px-River_Sindh_0.jpg
            uuid: 205a91ba-b3ec-42f5-b9cf-80028eb45739
            comment: Indus River in Kharmang District, Pakistan.
    - 
        quote: rajahmundry
        id: https://en.wikipedia.org/wiki/Rajahmundry
        uuid: d4f3d1e1-e60d-4e51-ade9-3e7eebbdb110
        text: Rajahmundry
        comment: 'Rajahmundry (officially: Rajamahendravaram) is one of the major cities in the Indian state of Andhra Pradesh. It is located on the banks of the Godavari River, in East Godavari district of the state. The city is the mandal headquarters to both Rajahmundry (rural) and Rajahmundry (urban) mandals. It is also the divisional headquarters of Rajahmundry revenue division and one of the two municipal corporations in the district, alongside Kakinada and the biggest city in both the godavari districts waiting for a greater status.[4] As of 2011[update] census, it is the sixth most populous city in the state, with a population of 341,831.[5]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Portrait_of_Nannayya.JPG
            uuid: 99881700-97e9-44f9-b27e-351802c0029c
            comment: Portrait of Nannayya
    - 
        quote: godavari
        id: https://en.wikipedia.org/wiki/Godavari
        uuid: ec704707-cf1a-4379-83cb-3e1b5bcfcdc6
        text: Godavari River
        comment: 'The Godavari is the second longest river in India after the river Ganges having its source at Tryambakeshwar, Maharashtra.[4] It starts in Maharashtra and flows east for 1,465 kilometres (910 mi) emptying into Bay of Bengal draining the Indian states Maharashtra (48.6%), Telangana( 18.8%), Andhra Pradesh (4.5%), Chhattisgarh (10.9%), Madhya Pradesh (10.0%), Odisha (5.7%) and Karnataka (1.4%)[5] through its extensive network of tributaries. Measuring up to 312,812 km2 (120,777 sq mi), it forms one of the largest river basins in the Indian subcontinent, with only the Ganges and Indus rivers having a drainage basin larger than it in India.[6] In terms of length, catchment area and ...'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/245px-Godavari-_river_basin.gif
            uuid: 5b152246-add8-4e9a-a91a-a9916d3e39ca
            comment: Godavari river Basin
    - 
        quote: oviya
        id: https://en.wikipedia.org/wiki/Oviya
        uuid: 13b6b25d-ea15-4270-89bb-672c234bd9e1
        text: Oviya
        comment: 'Oviya (born Helen Nelson; 29 April 1991) is an Indian model and film actress, who mainly works in the Tamil film industry. She rose to fame with the 2010 comedy film Kalavani.[2]'
    - 
        quote: vijayawada
        id: https://en.wikipedia.org/wiki/Vijayawada
        uuid: 9a8ffacf-ca78-489b-9ee5-89bcecea6127
        text: Vijayawada
        comment: 'Vijayawada is a city on the banks of the Krishna River, in the Indian state of Andhra Pradesh. It is a municipal corporation and the headquarters of Vijayawada (urban) mandal in Krishna district of the state. The city forms a part of Andhra Pradesh Capital Region and the headquarters of Andhra Pradesh Capital Region Development Authority is located in the city.[7] The city is one of the major trading and business centres of the state and hence, it is also known as "The Business Capital of Andhra Pradesh".[8][9] The city is one of the two metropolis in the state, with the other being Visakhapatnam.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Exterior_View_of_Mall.jpg
            uuid: 39436e35-abee-47cc-bcc0-1c75981e5d98
            comment: A view of PVP Mall on M.G.Road
    - 
        quote: ali
        id: https://en.wikipedia.org/wiki/Ali
        uuid: 65747f1d-03fd-4cb7-a00c-89284a8c2aab
        text: Ali
        comment: 'Ali ibn Abi Talib (/ˈɑːli, ɑːˈliː/;[6] Arabic: علي بن أبي طالب‎, translit. ʿAlī bin Abī Ṭālib‎, Arabic pronunciation: [ʕaliː bɪn ʔabiː t̪ˤaːlɪb]; 13 Rajab, 21 BH – 21 Ramadan, 40 AH; 15 September 601[3] – 29 January 661)[2][3] was the cousin and son-in-law of the Islamic prophet Muhammad, ruling over the Islamic caliphate from 656 to 661.[7]'
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Levha_%2528panel%2529_in_honor_of_Imam_%2527Ali.jpg'
            uuid: eeda5603-d0f7-483b-bd29-7d28d596d3a6
            comment: >
                Arabic calligraphy which means "There is no brave youth
                except Ali and there is no sword which renders service
                except Zulfiqar."
    - 
        quote: nri
        id: https://en.wikipedia.org/wiki/Nri
        uuid: 72b858bd-3fa6-4639-9786-2f3850e5dc4a
        text: NRI
        comment: 'NRI may refer to:'
    - 
        quote: thimmapuram
        id: https://en.wikipedia.org/wiki/Thimmapuram
        uuid: a466879d-bf76-4cbb-9470-ed8dc6b8084d
        text: Thimmapuram
        comment: >
            Thimmapuram is a village in Tuni Mandal in East Godavari
            District of Andhra Pradesh State, India. Its nearest town is
            Tuni which is 14 km far.Mostly people depend on farming and
            the nearby villages sell their produce using the railway
            station present.It is 1 km away from the village Tetagunta
            where it has better educational and other facilities.
            Kakinada and Rajahmundry are the nearby Cities to
            Thimmapuram.
    - 
        quote: swathi
        id: https://en.wikipedia.org/wiki/Swathi
        uuid: b16b7d52-25c4-4e23-9a9f-48edb251f7d2
        text: Swathi
        comment: 'Swathi may refer to:'
    - 
        quote: madhya
        id: https://en.wikipedia.org/wiki/Madhya
        uuid: 405455d4-b7ee-47a9-95ac-d752771c6d52
        text: Madhya Pradesh
        comment: >
            Madhya Pradesh (MP) (/ˈmɑːdjə prəˈdɛʃ/,
              (help·info), meaning Central Province) is a state in
            central India. Its capital is Bhopal and the largest city is
            Indore. Nicknamed the "heart of India" due to its
            geographical location in India, Madhya Pradesh is the
            second-largest state in the country by area. With over 75
            million inhabitants, it is the fifth-largest state in India
            by population. It borders the states of Uttar Pradesh to the
            northeast, Chhattisgarh to the southeast, Maharashtra to the
            south, Gujarat to the west, and Rajasthan to the northwest.
            Its total area is 308,245 km². Before 2000, When
            Chattisgarh was a part of Madhya Pradesh, Madhya Pradesh was
            the largest ...
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bateswar_%25286%2529.JPG'
            uuid: 3459e85f-fc9d-4f2f-bd20-e64bdc50f6b3
            comment: Bateswar Group of Temples, Padavli, Morena
    - 
        quote: chhattisgarh
        id: https://en.wikipedia.org/wiki/Chhattisgarh
        uuid: 07a7a4eb-dd8d-4a04-ba8e-0b04e67a6343
        text: Chhattisgarh
        comment: "Chhattisgarh (Chatīsgaṛh, literally 'Thirty-Six Forts') is a state in central India. It is the 10th largest state in India, with an area of 135,194 km2 (52,199 sq mi). With a population of 28 million, Chhattisgarh is the 17th most-populated state of the nation. It is a source of electricity and steel for India, accounting for 15% of the total steel produced in the country.[1] Chhattisgarh is one of the fastest-developing states in India.[2]"
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Shorea_robusta_in_Chhattisgarh.jpg
            uuid: 28fb8a73-87b8-4a1e-8251-64e36fa73cc5
            comment: 'Sal- The State Tree of Chhattisgarh'
    - 
        quote: punjab
        id: https://en.wikipedia.org/wiki/Punjab
        uuid: 25bc1406-5a7d-4a05-9bac-41905ae7a6db
        text: Punjab (region)
        comment: 'The Punjab (i/pʌnˈdʒɑːb/, /ˈpʌndʒɑːb/, /pʌnˈdʒæb/, /ˈpʌndʒæb/), also spelled Panjab, panj-āb, land of "five rivers"[1] (Punjabi: پنجاب (Shahmukhi); ਪੰਜਾਬ (Gurumukhi)), is a geographical and cultural region in the northern part of South Asia, comprising areas of eastern Pakistan and northern India. Not being a political unit, the extent of the region is the subject of debate and focuses on historical events to determine its boundaries.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Pope1880Panjab3.jpg
            uuid: 07785854-d72c-4f73-87fc-a8004a56805f
            comment: The Panjab 1880
    - 
        quote: lakh
        id: https://en.wikipedia.org/wiki/Lakh
        uuid: bd420775-c094-488e-a50b-8c6531657878
        text: Lakh
        comment: 'A lakh (/ˈlæk/ or /ˈlɑːk/; abbreviated L; sometimes written Lac[1] or Lacs) is a unit in the Indian numbering system equal to one hundred thousand (100,000; scientific notation: 105). [2][1][3] In the Indian convention of digit grouping, it is written as 1,00,000. For example, in India 150,000 rupees becomes 1.5 lakh rupees, written as ₹1,50,000 or INR 1,50,000.'
    - 
        quote: aren
        id: https://en.wikipedia.org/wiki/Aren
        uuid: 88020ad8-f98a-49f3-9320-8f484275c837
        text: Aren
        comment: 'The inhabitants of the commune are known as Arenais or Arenaises.[1]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Aren_Wayside_Cross.JPG
            uuid: ff6f92ab-7738-47b5-bed8-feebef407407
            comment: A Wayside Cross in Aren
    - 
        quote: kopi
        id: https://en.wikipedia.org/wiki/Kopi
        uuid: 7a7642aa-f404-4987-964a-3577bfbdff97
        text: Kopi
        comment: ""
    - 
        quote: kopis
        id: https://en.wikipedia.org/wiki/Kopis
        uuid: f8589d0e-1caa-4d4b-9265-7d5ecb91b9f4
        text: Kopis
        comment: 'The term kopis (from Greek κοπίς, plural kopides[1] from κόπτω - koptō, "to cut, to strike";[2] alternatively a derivation from the Ancient Egyptian term khopesh for a cutting sword has been postulated[3]) in Ancient Greece could describe a heavy knife with a forward-curving blade, primarily used as a tool for cutting meat, for ritual slaughter and animal sacrifice,[citation needed] or refer to a single edged cutting or "cut and thrust" sword with a similarly shaped blade.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Greek-Persian_duel.jpg
            uuid: 30fc99c8-e60c-49d1-9cdd-99c26cba9bfa
            comment: >
                Greek hoplite (standing) fighting against a Persian archer.
                Both are using a kopis. Depiction in ancient kylix, 5th
                century BC, National Archaeological Museum of Athens.
    - 
        quote: gadi
        id: https://en.wikipedia.org/wiki/Gadi
        uuid: 6a2f904e-86d1-47de-8183-710f733cf47f
        text: Gadi
        comment: 'Gadi may refer to:'
    - 
        quote: offload
        id: https://en.wikipedia.org/wiki/Offload
        uuid: 2707c97c-ea89-4955-b5ce-3320730f9206
        text: Offload
        comment: ""
    - 
        quote: nasik
        id: https://en.wikipedia.org/wiki/Nasik
        uuid: 0a2f738d-8da3-4754-86c2-9ce56289a2cd
        text: Nashik
        comment: 'Nashik (pron:ˈnʌʃɪk) ( pronunciation (help·info))[3] is a city in the northwest region of Maharashtra in India, and is the administrative headquarter of the Nashik District and Nashik Division.'
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rama%252C_Lakshman_and_Sita_at_the_Kalaram_Temple%252C_Nashik..jpg'
            uuid: 03308a13-53a9-48ee-a9c0-a515973aac7a
            comment: Ram, Lakshman, Sita at a Nashik Temple.
    - 
        quote: ahmadnagar
        id: https://en.wikipedia.org/wiki/Ahmadnagar
        uuid: b54ffba6-645e-456c-866a-81902fe7aa7f
        text: Ahmadnagar
        comment: 'Coordinates: 32°10′N 73°50′E﻿ / ﻿32.167°N 73.833°E﻿ / 32.167; 73.833'
    - 
        quote: pune
        id: https://en.wikipedia.org/wiki/Pune
        uuid: dcb779b7-203b-464c-850b-72e36358c194
        text: Pune
        comment: 'Pune (IPA: [puɳe] English pronunciation: /ˈpuːnə/;[6][7][8][9] spelt Poona during British rule) is the 9th most populous city in India and the second largest in the state of Maharashtra after the state capital Mumbai. Pune is also the 101st most populous city in the world.[10]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bajirao_Peshwa.jpg
            uuid: bbba1fee-f412-4151-85ea-445a976d62a5
            comment: 'An equestrian statue of Baji Rao I (श्रीमंत बाजीराव पेशवे.) outside the Shaniwar Wada, who is credited with successful expansion of Maratha power in North India (circa 1730 CE)[23][24]'
    - 
        quote: satara
        id: https://en.wikipedia.org/wiki/Satara
        uuid: bf6cad31-8ba2-4aa2-a30b-e4c25ed8064e
        text: Satara
        comment: 'Satara may refer to:'
    - 
        quote: sangli
        id: https://en.wikipedia.org/wiki/Sangli
        uuid: 7d9b6389-ea04-4ecc-9fd5-73febf47df92
        text: Sangli
        comment: 'Sangli ( pronunciation (help·info)) is a city and the district headquarters of Sangli District in the state of Maharashtra, in western India. It is known as the Turmeric City of Maharashtra due to its production and trade of the spice.[3] Sangli is situated on the banks of river Krishna and houses many sugar factories. The Ganesha Temple of Sangli is a historical landmark of the city and is visited by thousands of pilgrims.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sangli-ganpati-02.jpg
            uuid: 3934532a-0383-45a3-93b4-2965eef824fa
            comment: Ganpati Temple, Heart of Sangli City
    - 
        quote: dalits
        id: https://en.wikipedia.org/wiki/Dalits
        uuid: c80d4152-015b-4904-b900-d34faf415204
        text: Dalit
        comment: 'Dalit, meaning "oppressed" in Sanskrit is the self-chosen political name of castes in India which are "untouchable".[1][2] Though the name Dalit has been in existence since the nineteenth century, the economist and reformer B. R. Ambedkar (1891–1956) popularised the term.[3] Dalits were excluded from the four-fold Varna system and formed the unmentioned fifth varna; they were also called Panchama.[4]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-A_school_of_untouchables_near_Bangalore_by_Lady_Ottoline_Morrell_2.jpg
            uuid: 0e20c4eb-4a36-4754-9537-4c433c3c7405
            comment: >
                A school of untouchables near Bangalore, by Lady Ottoline
                Morrell.
    - 
        quote: adivasis
        id: https://en.wikipedia.org/wiki/Adivasis
        uuid: 6d4b811e-bf23-4b3e-9aa5-c9db07fc0789
        text: Adivasi
        comment: "Adivasi (Hindi: आदिवासी, IPA: [aːd̪ɪˈʋaːsi]) are the tribal groups considered the Indigenous people population of South Asia.[1][2][3] Adivasi make up 8.6% of India's population or 104 million, according to the 2011 census, and a large percentage of the Nepalese population.[4][5][6] They comprise a substantial indigenous minority of the population of India and Nepal. The same term Adivasi is used for the ethnic minorities of Bangladesh, Pakistan, the native Tharu people of Nepal, and also the native Vedda people of Sri Lanka (Sinhalese: ආදී වාස).[7][8] The word is also used in the same sense in Nepal, as is another word, janajati (Nepali: जनजाति; ..."
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Women_in_tribal_village%252C_Umaria_district%252C_India_1.jpg'
            uuid: ad032ebf-2010-414f-a10e-6dec080b682c
            comment: >
                Women in a tribal Gond adivasi village, Umaria district,
                Madhya Pradesh, India
    - 
        quote: kolhapur
        id: https://en.wikipedia.org/wiki/Kolhapur
        uuid: 94f656d1-6544-479f-9c59-a0e7b47f8f73
        text: Kolhapur
        comment: 'Kolhapur, ( Kolhapur.ogg (help·info)) is a city in the Panchganga River Basin in the western Indian state of Maharashtra.[4] It is the district headquarters of Kolhapur district. Kolhapur comes under the administration of Pune division.[5] Prior to Indian Independence, Kolhapur was a nineteen gun salute, princely state ruled by the Bhosale Chhatrapati (Bhosale royal clan) of the Maratha Empire.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Mahalaxmi_of_Kolhapur.jpg
            uuid: 5eee2839-8e70-4a6f-bd54-2c0e046a63fb
            comment: Mahalakshmi, Hindu goddess
    - 
        quote: sholapur
        id: https://en.wikipedia.org/wiki/Sholapur
        uuid: a3379269-ec56-4378-8f34-5d70fd8fa386
        text: Solapur
        comment: 'Solapur (IPA: [Sōlāpūr]) ( pronunciation (help·info)) is a city located in the south-eastern region of the Indian state of Maharashtra.[6][7] Solapur is located on major road and rail routes between Mumbai and Hyderabad, with a branch line to the cities of Bijapur and Gadag in the neighbouring state of Karnataka.[8] It is classified as a 2 Tier and B-2 class city by House Rent Allowance (HRA) classification by the Government of India.[9] It is 49th-most-populous city in India and 43rd-largest urban agglomeration.[10]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Siddheshwar_Temple.JPG
            uuid: c09a33a7-93d2-4725-8711-9ac30f87b0cc
            comment: Shri.Siddheshwar Temple During the sankranthi
    - 
        quote: surat
        id: https://en.wikipedia.org/wiki/Surat
        uuid: 14fcc2b7-ad19-4ffd-86b6-00b5b9a24762
        text: Surat
        comment: 'Surat, is a port city previously known as Suryapur. It is the economical capital and former princely state in the Indian state of Gujarat. It is the eighth largest city and ninth largest urban agglomeration (after Pune) in India. Surat is the 3rd "cleanest city of India",[8] and 4th fastest growing city of the world.[9] Surat is famous for its food.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Baghdadi_Jews_Cemetery_2.jpg
            uuid: e6baad63-60c3-4b14-93a4-ed5500c8b103
            comment: Baghdadi Jews Cemetery
    - 
        quote: belgaum
        id: https://en.wikipedia.org/wiki/Belgaum
        uuid: c3d967aa-f29f-471a-be50-174ef3a76d3c
        text: Belgaum
        comment: 'Belgaum (officially known as Belagavi, and earlier known "Venugrama" or the "Bamboo Village",[4]) is a city in the Indian state of Karnataka. It is the administrative headquarters of the eponymous Belgaum division and Belgaum district. The Government of Karnataka has proposed making Belgaum the second capital of Karnataka, hence a second state administrative building Suvarna Vidhana Soudha was inaugurated on 11 October 2012.[5]'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Belgaum_1896_map.jpg
            uuid: c926b4f1-79d4-4a99-a879-6f61acdd1218
            comment: Belgaum 1896 map
    - 
        quote: marathwada
        id: https://en.wikipedia.org/wiki/Marathwada
        uuid: 57f3a1b0-e695-4476-9530-743f7562f40f
        text: Marathwada
        comment: >
            Marathwada (IPA:Marāṭhvāḍā) is one of the five
            regions in Indian state of Maharashtra. The region coincides
            with the Aurangabad Division of Maharashtra.
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Mir_osman_ali_khan.JPG
            uuid: c7459290-958f-4c12-a22b-a1d11a4b8cf9
            comment: >
                Mir Osman Ali Khan was last Nizam of Hyderabad
                State(1911-1948), On 22 February 1937 a cover story by TIME
                called him as the wealthiest man in the world.
    - 
        quote: beed
        id: https://en.wikipedia.org/wiki/Beed
        uuid: 7570a5fe-e1ea-41c3-aa94-60bda12c75b7
        text: Beed
        comment: 'Beed (Marathi:बीड) is a city in central region of Maharashtra state in India. It is the administrative headquarters and the largest city with a population of 146,709 in Beed district.[2] Nearly 36% of the district’s urban population lives in the city alone. It has witnessed 6.1% population growth during 2001 – 2011 decade.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kapildhara_in_Balaghat_range.jpg
            uuid: e55894e8-99db-45f0-8d77-49281e3a3faa
            comment: >
                The Kapildhar fall in Balaghat range about 18 km south of
                Beed city.
    - 
        quote: jalgaon
        id: https://en.wikipedia.org/wiki/Jalgaon
        uuid: 1983c1ed-863b-4f37-b400-3a5d28991407
        text: Jalgaon
        comment: >
            Jalgaon ( pronunciation (help·info)) is a city in western
            India, north of Maharashtra in Jalgaon district and in the
            agricultural region of Khandesh. Jalgaon has a municipal
            corporation, and had 460,468 residents at the 2011 census.
            It is 59 km (37 mi) from the Ajanta Caves. Jalgaon, in a
            Central Railway zone, can be reached by National Highway 6
            and has an airport.
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Khandesh_Central.jpg
            uuid: c1321742-8259-49db-b334-5d2bdfd6751c
            comment: Khandesh Central mall
    - 
        quote: jalna
        id: https://en.wikipedia.org/wiki/Jalna
        uuid: 1422bab3-9f01-4331-8328-43fb8ace9530
        text: Jalna
        comment: 'Jalna may refer to:'
    - 
        quote: tribals
        id: https://en.wikipedia.org/wiki/Tribals
        uuid: e6c16c08-5a28-4925-9653-b25b931ad343
        text: Adivasi
        comment: "Adivasi (Hindi: आदिवासी, IPA: [aːd̪ɪˈʋaːsi]) are the tribal groups considered the Indigenous people population of South Asia.[1][2][3] Adivasi make up 8.6% of India's population or 104 million, according to the 2011 census, and a large percentage of the Nepalese population.[4][5][6] They comprise a substantial indigenous minority of the population of India and Nepal. The same term Adivasi is used for the ethnic minorities of Bangladesh, Pakistan, the native Tharu people of Nepal, and also the native Vedda people of Sri Lanka (Sinhalese: ආදී වාස).[7][8] The word is also used in the same sense in Nepal, as is another word, janajati (Nepali: जनजाति; ..."
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Women_in_tribal_village%252C_Umaria_district%252C_India_0.jpg'
            uuid: f726e97a-b67f-4eea-887e-5430b93c40be
            comment: >
                Women in a tribal Gond adivasi village, Umaria district,
                Madhya Pradesh, India
    - 
        quote: bardhaman
        id: https://en.wikipedia.org/wiki/Bardhaman
        uuid: 3bb11c12-f8d6-41f7-91b9-b25d62c98e43
        text: Bardhaman
        comment: 'Bardhaman (Pron: ˈbɑ:dəˌmən) (Bengali): বর্ধমান, (Hindi): बर्दवान is a city of West Bengal state in eastern India. It is the headquarters of Bardhaman district, having become a district capital during the period of British rule. Burdwan, an alternative name for the city, has remained in use since that period.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/181px-Sarbamangala_temple.jpg
            uuid: c61b7c7e-18e1-4617-bc6a-fa3cb7a42538
            comment: Sarbamangala Temple
    - 
        quote: saora
        id: https://en.wikipedia.org/wiki/Saora
        uuid: fafe921e-1576-4fe3-a771-6d9b76fd8e13
        text: Sora people
        comment: >
            The Sora (alternative names and spellings include Saora,
            Saura, Savara and Sabara) are a tribe from Southern Odisha,
            north coastal Andhra Pradesh in India. They are also found
            in the hills of Jharkhand, Madhya Pradesh and Maharashtra.
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Saura_Lady.JPG
            uuid: c5aa0226-4883-4df5-b4bb-30d9e2349b2a
            comment: A Sora Woman
    - 
        quote: munda
        id: https://en.wikipedia.org/wiki/Munda
        uuid: d69234e2-7903-40d5-a589-9b4989c57338
        text: Munda
        comment: 'Munda may refer to:'
    - 
        quote: santhal
        id: https://en.wikipedia.org/wiki/Santhal
        uuid: bce013ba-554b-4b0e-94f0-5864120b7196
        text: Santhal
        comment: 'Santhal may refer to :'
    - 
        quote: che
        id: https://en.wikipedia.org/wiki/Che
        uuid: f668564f-63a8-4bab-b505-3af126941d87
        text: Che
        comment: (Ч, ч), a letter of the Cyrillic alphabet
    - 
        quote: one-third
        id: https://en.wikipedia.org/wiki/One-third
        uuid: eea06669-8355-4e74-aa40-7d684e0ba915
        text: 1/3
        comment: '1/3 may refer to:'
    - 
        quote: usa
        id: https://en.wikipedia.org/wiki/Usa
        uuid: ff7e091e-f06b-4a44-a117-080a6f39c794
        text: United States
        comment: 'The United States of America (USA), commonly referred to as the United States (U.S.) or America, is a federal republic composed of 50 states, a federal district, five major self-governing territories, and various possessions.[fn 1] Forty-eight of the fifty states and the federal district are contiguous and located in North America between Canada and Mexico. The state of Alaska is in the far northwestern corner of North America, with a land border to the east with Canada and separated by the Bering Strait from Russia. The state of Hawaii is an archipelago in the mid-Pacific. The territories of the U.S are scattered about the Pacific Ocean and the Caribbean Sea. Nine time zones are covered. ...'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Chromesun_kincaid_site_01.jpg
            uuid: 9e8e9862-ad7c-4653-b12a-febdcdb00478
            comment: >
                An artistic recreation of The Kincaid Site from the
                prehistoric Mississippian culture as it may have looked at
                its peak 1050-1400 AD
    - 
        quote: undp
        id: https://en.wikipedia.org/wiki/Undp
        uuid: 98588715-b46c-47da-a6c8-20e8f0ab8e57
        text: United Nations Development Programme
        comment: >
            Headquartered in New York City, UNDP advocates for change
            and connects countries to knowledge, experience and
            resources to help people build a better life. It provides
            expert advice, training, and grants support to developing
            countries, with increasing emphasis on assistance to the
            least developed countries.
    - 
        quote: uae
        id: https://en.wikipedia.org/wiki/Uae
        uuid: a9c2f6b8-f505-4d29-8b0e-30b7bd1f186c
        text: United Arab Emirates
        comment: "The United Arab Emirates (i/juːˌnaɪtᵻd ˌærəb ˈɛmɪrᵻts/; Arabic: دولة الإمارات العربية المتحدة‎‎ Dawlat al-Imārāt al-'Arabīyah al-Muttaḥidah), sometimes simply called the Emirates (Arabic: الامارات‎‎ al-Imārāt) or the UAE, is a federal absolute monarchy in Western Asia, at the southeast end of the Arabian Peninsula and bordering seas in the Gulf of Oman, occupying the Persian Gulf. It borders with countries including Oman to the east and Saudi Arabia to the south, although the United Arab Emirates shares maritime borders with Qatar in the west and Iran in the north, and sharing sea borders with Iraq, Kuwait, and Bahrain. In 2013, ..."
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Map_of_expansion_of_Caliphate.svg.png
            uuid: babcba8e-1e15-4abe-9b34-29f06aec31f3
            comment: 'The land comprising the UAE, was under the Umayyads:   Expansion under Muhammad, 622–632   Expansion during the Rashidun Caliphate, 632–661   Expansion during the Umayyad Caliphate, 661–750'
    - 
        quote: three-fifths
        id: https://en.wikipedia.org/wiki/Three-fifths
        uuid: 8c3bc6af-9cd7-43d4-9de1-e4c2217aa585
        text: Three-Fifths Compromise
        comment: "The Three-Fifths Compromise was a compromise reached between delegates from southern states and those from northern states during the 1787 United States Constitutional Convention. The debate was over whether, and if so, how, slaves would be counted when determining a state's total population for legislative representation and taxing purposes. The issue was important, as this population number would then be used to determine the number of seats that the state would have in the United States House of Representatives for the next ten years. The effect was to give the southern states a third more seats in Congress and a third more electoral votes than if slaves had been ignored, but fewer than ..."
    - 
        quote: crore
        id: https://en.wikipedia.org/wiki/Crore
        uuid: 70011675-28ae-4eb2-825d-8d52bd0f23fb
        text: Crore
        comment: 'A crore (/ˈkrɔər/; abbreviated cr) denotes ten million (10,000,000 or 107 in scientific notation) and is equals to 100 lakhs in the Indian and Nepali Numbering system. It is widely used in South Asia, and is written in this region as 1,00,00,000 with the local style of digit group separators (a lakh is equal to one hundred thousand and is written as 1,00,000).[1]'
    - 
        quote: keralites
        id: https://en.wikipedia.org/wiki/Keralites
        uuid: e5293d85-1438-49fc-bbc3-cf258629b5d1
        text: Malayali
        comment: 'The Malayali or Malayalee (Malayalam: മലയാളി) are an ethno-linguistic group native to the South Indian state of Kerala and the Union Territory of Lakshadweep and have the Malayalam language as their mother tongue. The Malayali community in India has a history of immigrants to the region from various parts of the world, as well as a unique sub-culture owing to the tropical environment of the state.[13][14]'
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-British_Residency_in_Asramam%252C_Kollam.jpg'
            uuid: 5e91161b-3718-4353-a4e8-ab25d3427080
            comment: 'British Residency in Kollam - It is a splendid two-storeyed Palace built by Col. John Munro between 1811 and 1819. It is a blend of European-Indian-Tuscan architectural styles'
    - 
        quote: gdp
        id: https://en.wikipedia.org/wiki/Gdp
        uuid: 697959ab-c85a-4767-90e2-57aa6158c342
        text: Gross domestic product
        comment: >
            Gross domestic product (GDP) is a monetary measure of the
            market value of all final goods and services produced in a
            period (quarterly or yearly). Nominal GDP estimates are
            commonly used to determine the economic performance of a
            whole country or region, and to make international
            comparisons. Nominal GDP per capita does not, however,
            reflect differences in the cost of living and the inflation
            rates of the countries; therefore using a GDP PPP per capita
            basis is arguably more useful when comparing differences in
            living standards between nations.
        image:
            id: 'http://eschool2go.org/sites/default/files/articles/thumbnails/370px-Countries_by_GDP_%2528Nominal%2529_in_2014.svg.png'
            uuid: d06efee4-d476-48f7-826a-ee507a412b7c
            comment: 'A map of world economies by size of GDP (nominal) in USD, World Bank, 2014.[1]'
    - 
        quote: onam
        id: https://en.wikipedia.org/wiki/Onam
        uuid: eb1ac922-8e32-473e-b894-bdfa720b15f1
        text: Onam
        comment: 'Onam (Malayalam: ഓണം) is the most important seasonal festival celebrated in the state of Kerala in India.[1] It is also the State festival of Kerala with State holidays on 4 days starting from Onam Eve (Uthradom) to the 3rd Onam Day. Onam is a major cultural festival celebrated by people cutting across socio-economic and religious distinctions.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Blathur_Onam_celebration.jpg
            uuid: ead3052a-5ef8-4b23-bf29-33dcd5ef5342
            comment: Onam is a community festival
    - 
        quote: thiruvananthapuram
        id: https://en.wikipedia.org/wiki/Thiruvananthapuram
        uuid: c762c6de-34ed-4494-9d32-16cb20a7c77c
        text: Thiruvananthapuram
        comment: 'Thiruvananthapuram (Tiruvaṉantapuram, IPA: [t̪iruʋənən̪t̪əpurəm] ( listen)), formerly known as Trivandrum, is the capital and the largest city of the Indian state of Kerala. It is located on the west coast of India near the extreme south of the mainland. Referred to by Mahatma Gandhi as the "Evergreen city of India",[3][4] it is classified as a Tier-II city by the Government of India.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Aruvikkara_Dam.jpg
            uuid: 2d7a10df-418a-4eb5-adbd-657e1eae7e7a
            comment: Aruvikkara Dam
    - 
        quote: hindustan
        id: https://en.wikipedia.org/wiki/Hindustan
        uuid: e20e0207-9a25-4f27-bd44-21e1005c6d4d
        text: Hindustan
        comment: 'Hindustan pronunciation (help·info) (ہندوستان or हिंदुस्तान) is a common geographic term for the northern/northwestern Indian subcontinent.[1][2] The terms Hind and Hindustān were current in Persian and Arabic at the time of the 11th century Turkic conquests.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Indo-Gangetic_Plain.hu.png
            uuid: 234828e0-c194-47ad-9aef-a3d2a3ec9a11
            comment: Indo-Gangetic Plain
    - 
        quote: uts
        id: https://en.wikipedia.org/wiki/Uts
        uuid: e961c99d-056c-432a-8b46-ed29b071a4b2
        text: UTS
        comment: ""
---
