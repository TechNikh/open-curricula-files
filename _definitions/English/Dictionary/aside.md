---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aside
offline_file: ""
offline_thumbnail: ""
uuid: 4b6a0d16-76ff-4119-9f21-767061aa0dee
updated: 1484310236
title: aside
categories:
    - Dictionary
---
aside
     n 1: a line spoken by an actor to the audience but not intended
          for others on the stage
     2: a message that departs from the main subject [syn: {digression},
         {excursus}, {divagation}, {parenthesis}]
     adv 1: on or to one side; "step aside"; "stood aside to let him
            pass"; "threw the book aside"; "put her sewing aside
            when he entered"
     2: out of the way (especially away from one's thoughts); "brush
        the objections aside"; "pushed all doubts away" [syn: {away}]
     3: not taken into account or excluded from consideration;
        "these problems apart, the country is doing well"; "all
        joking aside, I think you're ...
