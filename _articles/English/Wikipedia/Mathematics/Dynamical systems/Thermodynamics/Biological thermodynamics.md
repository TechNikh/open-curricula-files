---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biological_thermodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 1b38c025-3665-4479-9523-59d0a0f17ba4
updated: 1484308828
title: Biological thermodynamics
tags:
    - History
    - The focus of thermodynamics in biology
    - Energy Transformation in Biological Systems
    - Examples
    - First Law of Thermodynamics
    - Second law of thermodynamics
    - Gibbs Free Energy
categories:
    - Thermodynamics
---
Biological thermodynamics is the quantitative study of the energy transductions that occur in and between living organisms, structures, and cells and of the nature and function of the chemical processes underlying these transductions. Biological thermodynamics may address the question of whether the benefit associated with any particular phenotypic trait is worth the energy investment it requires.
