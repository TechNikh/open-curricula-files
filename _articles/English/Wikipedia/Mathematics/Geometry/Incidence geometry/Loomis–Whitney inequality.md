---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Loomis%E2%80%93Whitney_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: 7b7b7e22-4d4d-4759-a94e-cc5278e4b814
updated: 1484309251
title: Loomis–Whitney inequality
tags:
    - Statement of the inequality
    - A special case
    - Generalizations
categories:
    - Incidence geometry
---
In mathematics, the Loomis–Whitney inequality is a result in geometry, which in its simplest form, allows one to estimate the "size" of a d-dimensional set by the sizes of its (d – 1)-dimensional projections. The inequality has applications in incidence geometry, the study of so-called "lattice animals", and other areas.
