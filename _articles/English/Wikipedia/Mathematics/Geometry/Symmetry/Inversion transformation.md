---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inversion_transformation
offline_file: ""
offline_thumbnail: ""
uuid: a53e6ce9-374a-465a-8ca2-e6d06b57f828
updated: 1484309312
title: Inversion transformation
tags:
    - Early use
    - Transformation on coordinates
    - Invariants
    - Physical evidence
categories:
    - Symmetry
---
In mathematical physics, inversion transformations are a natural extension of Poincaré transformations to include all conformal one-to-one transformations on coordinate space-time. They are less studied in physics because unlike the rotations and translations of Poincaré symmetry an object cannot be physically transformed by the inversion symmetry. Some physical theories are invariant under this symmetry, in these cases it is what is known as a 'hidden symmetry'. Other hidden symmetries of physics include gauge symmetry and general covariance.
