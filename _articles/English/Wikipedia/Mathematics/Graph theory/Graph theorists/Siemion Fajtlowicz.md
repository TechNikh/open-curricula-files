---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Siemion_Fajtlowicz
offline_file: ""
offline_thumbnail: ""
uuid: 877e46e5-8b7b-437c-ab3b-02174b550de7
updated: 1484309461
title: Siemion Fajtlowicz
categories:
    - Graph theorists
---
Siemion Fajtlowicz is a Polish mathematician, currently a professor at the University of Houston. He is known for developing the conjecture-making computer program Graffiti.[1][2]
