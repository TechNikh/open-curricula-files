---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/honestly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484564101
title: honestly
categories:
    - Dictionary
---
honestly
     adv 1: (used as intensives reflecting the speaker's attitude) it is
            sincerely the case that; "honestly, I don't believe
            it"; "candidly, I think she doesn't have a
            conscience"; "frankly, my dear, I don't give a damn"
            [syn: {candidly}, {frankly}]
     2: in an honest manner; "in he can't get it honestly, he is
        willing to steal it"; "was known for dealing aboveboard in
        everything" [syn: {aboveboard}] [ant: {dishonestly}]
