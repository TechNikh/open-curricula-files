---
version: 1
type: article
id: https://en.wikipedia.org/wiki/PGO_waves
offline_file: ""
offline_thumbnail: ""
uuid: f53e902c-50e0-4eb4-b73c-1600004852a8
updated: 1484308898
title: PGO waves
tags:
    - discovery
    - Detection
    - Mechanism for generation and propagation
    - Executive neurons
    - Triggering neurons
    - Transfer neurons
    - Modulatory neurons
    - Aminergic neurons
    - Cholinergic neurons
    - Nitroxergic neurons
    - GABA-ergic neurons
    - Vestibular nuclei
    - Amygdala
    - Suprachiasmatic nuclei
    - Auditory stimulation
    - Basal ganglia
    - REM sleep
    - Future research
    - Additional images
categories:
    - Waves
---
Ponto-geniculo-occipital waves or PGO waves are phasic field potentials. These waves can be recorded from the pons, the lateral geniculate nucleus (LGN), and the occipital cortex regions of the brain, where these waveforms originate. The waves begin as electrical pulses from the pons, then move to the lateral geniculate nucleus residing in the thalamus, and then finally end up in the primary visual cortex of the occipital lobe. The appearances of these waves are most prominent in the period right before rapid eye movement sleep (or REM sleep), and are theorized to be intricately involved with eye movement of both wake and sleep cycles in many different animals.
