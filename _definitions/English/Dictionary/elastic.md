---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elastic
offline_file: ""
offline_thumbnail: ""
uuid: 5b8c4537-f444-4daf-9db4-1d43e8e0cc90
updated: 1484310335
title: elastic
categories:
    - Dictionary
---
elastic
     adj 1: capable of resuming original shape after stretching or
            compression; springy; "an elastic band"; "a youthful
            and elastic walk" [ant: {inelastic}]
     2: able to adjust readily to different conditions; "an
        adaptable person"; "a flexible personality"; "an elastic
        clause in a contract" [syn: {flexible}, {pliable}, {pliant}]
     n 1: a narrow band of elastic rubber used to hold things (such as
          papers) together [syn: {rubber band}, {elastic band}]
     2: an elastic fabric made of yarns containing an elastic
        material
