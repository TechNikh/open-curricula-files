---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vividly
offline_file: ""
offline_thumbnail: ""
uuid: ea141c68-785b-40e7-afdd-a36f013b4672
updated: 1484310303
title: vividly
categories:
    - Dictionary
---
vividly
     adv : in a vivid manner; "he described his adventures vividly"
