---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Idli
offline_file: ""
offline_thumbnail: ""
uuid: e84b6271-0559-41cd-a350-bcfe28bf209a
updated: 1484308428
title: Idli
tags:
    - History
    - Preparation
    - Serving
    - Variations
    - Bibliography
categories:
    - Uncategorized
---
Idli /ɪdliː/) is a traditional breakfast in South Indian households. Idli is a savoury cake that is popular throughout India and neighbouring countries like Sri Lanka. The cakes are made by steaming a batter consisting of fermented black lentils (de-husked) and rice. The fermentation process breaks down the starches so that they are more readily metabolized by the body.
