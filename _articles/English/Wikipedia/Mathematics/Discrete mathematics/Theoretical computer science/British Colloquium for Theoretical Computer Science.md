---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/British_Colloquium_for_Theoretical_Computer_Science
offline_file: ""
offline_thumbnail: ""
uuid: 19e406b1-d088-45ed-bf6e-c4ee93131b7a
updated: 1484308678
title: British Colloquium for Theoretical Computer Science
categories:
    - Theoretical computer science
---
The British Colloquium for Theoretical Computer Science (BCTCS) is an organisation that hosts an annual event for UK-based researchers in theoretical computer science. A central aspect of BCTCS is the training of PhD students.
