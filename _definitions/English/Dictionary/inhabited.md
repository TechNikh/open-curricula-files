---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inhabited
offline_file: ""
offline_thumbnail: ""
uuid: 756812c2-b7bb-4780-8d22-ea0aa63b3a18
updated: 1484310577
title: inhabited
categories:
    - Dictionary
---
inhabited
     adj : having inhabitants; lived in; "the inhabited regions of the
           earth" [ant: {uninhabited}]
