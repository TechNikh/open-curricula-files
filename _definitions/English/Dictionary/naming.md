---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/naming
offline_file: ""
offline_thumbnail: ""
uuid: bd9bd880-597e-4c20-8742-409caecc1e28
updated: 1484310421
title: naming
categories:
    - Dictionary
---
naming
     adj : inclined to or serving for the giving of names; "the
           appellative faculty of children"; "the appellative
           function of some primitive rites" [syn: {appellative},
           {naming(a)}]
     n 1: the verbal act of naming; "the part he failed was the naming
          of state capitals"
     2: the act of putting a person into a non-elective position;
        "the appointment had to be approved by the whole
        committee" [syn: {appointment}, {assignment}, {designation}]
