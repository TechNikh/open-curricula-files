---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lebesgue%27s_universal_covering_problem'
offline_file: ""
offline_thumbnail: ""
uuid: e6d0feaa-aabb-4c8d-9a69-28ab81e5b516
updated: 1484308599
title: "Lebesgue's universal covering problem"
categories:
    - Discrete geometry
---
Lebesgue's universal covering problem is an unsolved problem in geometry that asks for the convex shape of smallest area that can cover any planar set of diameter one. The diameter of a set by definition is the least upper bound of the distances between all pairs of points in the set. A shape covers a set if it contains a congruent subset. In other words the set may be rotated, translated or reflected to fit inside the shape.
