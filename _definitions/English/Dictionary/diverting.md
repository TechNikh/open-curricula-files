---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diverting
offline_file: ""
offline_thumbnail: ""
uuid: 25f82c07-0c35-421d-b7fc-b9771c90f90f
updated: 1484310148
title: diverting
categories:
    - Dictionary
---
diverting
     adj : providing enjoyment; pleasantly entertaining; "an amusing
           speaker"; "a diverting story"; "a fun thing to do"
           [syn: {amusing}, {amusive}, {fun(a)}]
