---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/float
offline_file: ""
offline_thumbnail: ""
uuid: 38e16af9-ef19-4571-82b5-f09b99ab8731
updated: 1484310411
title: float
categories:
    - Dictionary
---
float
     n 1: the time interval between the deposit of a check in a bank
          and its payment
     2: the number of shares outstanding and available for trading
        by the public
     3: a drink with ice cream floating in it [syn: {ice-cream soda},
         {ice-cream float}]
     4: an elaborate display mounted on a platform carried by a
        truck (or pulled by a truck) in a procession or parade
     5: a hand tool with a flat face used for smoothing and
        finishing the surface of plaster or cement or stucco [syn:
         {plasterer's float}]
     6: something that remains on the surface of a liquid
     v 1: be in motion due to some air or water current; "The leaves
 ...
