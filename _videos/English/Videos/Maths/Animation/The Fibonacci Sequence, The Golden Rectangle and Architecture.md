---
version: 1
type: video
provider: YouTube
id: GmZlAVRKx8I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a02b8b73-0ce5-4952-af9b-8a1eb37f3536
updated: 1486069641
title: >
    The Fibonacci Sequence, The Golden Rectangle and
    Architecture
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/GmZlAVRKx8I/default.jpg
    - https://i3.ytimg.com/vi/GmZlAVRKx8I/1.jpg
    - https://i3.ytimg.com/vi/GmZlAVRKx8I/2.jpg
    - https://i3.ytimg.com/vi/GmZlAVRKx8I/3.jpg
---
