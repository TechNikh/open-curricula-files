---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bottom
offline_file: ""
offline_thumbnail: ""
uuid: be4ed855-5b10-424e-8504-002bdd6e3505
updated: 1484310268
title: bottom
categories:
    - Dictionary
---
bottom
     adj 1: situated at the bottom or lowest position; "the bottom
            drawer" [syn: {bottom(a)}] [ant: {side(a)}, {top(a)}]
     2: at the bottom; lowest or last; "the bottom price" [syn: {lowest}]
     3: the lowest rank; "bottom member of the class" [syn: {poorest}]
     n 1: the lower side of anything [syn: {underside}, {undersurface}]
     2: the lowest part of anything; "they started at the bottom of
        the hill"
     3: the fleshy part of the human body that you sit on; "he
        deserves a good kick in the butt"; "are you going to sit
        on your fanny and do nothing?" [syn: {buttocks}, {nates},
        {arse}, {butt}, {backside}, {bum}, {buns}, {can}, ...
