---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/valuable
offline_file: ""
offline_thumbnail: ""
uuid: 5bbe6179-f9dc-4cd0-90b8-ba132878bac0
updated: 1484310377
title: valuable
categories:
    - Dictionary
---
valuable
     adj 1: having great material or monetary value especially for use
            or exchange; "another human being equally valuable in
            the sight of God"; "a valuable diamond" [ant: {worthless}]
     2: having worth or merit or value; "a valuable friend"; "a good
        and worthful man" [syn: {worthful}]
     3: of great importance or use or service; "useful information";
        "valuable advice" [syn: {useful}, {of value}]
     n : something of value; "all our valuables were stolen"
