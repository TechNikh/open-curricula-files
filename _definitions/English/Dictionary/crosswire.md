---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crosswire
offline_file: ""
offline_thumbnail: ""
uuid: 100d78a2-d2c1-457b-8c5e-558985c28030
updated: 1484310366
title: crosswire
categories:
    - Dictionary
---
cross wire
     n : either of two fine mutually perpendicular lines that cross
         in the focus plane of an optical instrument and are use
         for sighting or calibration; "he had the target in his
         cross hairs" [syn: {cross hair}]
