---
version: 1
type: video
provider: YouTube
id: tVtg_YJ0Bbo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f503a2db-cc2c-4fdb-980b-1fe436b3d281
updated: 1486069637
title: Amazing Water Refraction
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/tVtg_YJ0Bbo/default.jpg
    - https://i3.ytimg.com/vi/tVtg_YJ0Bbo/1.jpg
    - https://i3.ytimg.com/vi/tVtg_YJ0Bbo/2.jpg
    - https://i3.ytimg.com/vi/tVtg_YJ0Bbo/3.jpg
---
