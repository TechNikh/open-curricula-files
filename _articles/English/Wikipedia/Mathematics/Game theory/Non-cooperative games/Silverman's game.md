---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Silverman%27s_game'
offline_file: ""
offline_thumbnail: ""
uuid: 9c127b61-3b72-4255-9a8f-b7895499a6da
updated: 1484309057
title: "Silverman's game"
categories:
    - Non-cooperative games
---
It is played by two players on a given set S of positive real numbers. Before play starts, a threshold T and penalty ν are chosen with 1 < T < ∞ and 0 < ν < ∞. For example, consider S to be the set of integers from 1 to n, T = 3 and ν = 2.
