---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phenotype
offline_file: ""
offline_thumbnail: ""
uuid: 337aa8db-541b-41ed-b1f3-725c2e9f7eb8
updated: 1484310299
title: phenotype
categories:
    - Dictionary
---
phenotype
     n : what an organism looks like as a consequence of its
         genotype; two organisms with the same phenotype can have
         different genotypes
