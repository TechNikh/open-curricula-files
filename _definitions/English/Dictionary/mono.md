---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mono
offline_file: ""
offline_thumbnail: ""
uuid: 173bb745-53dd-4d34-af5e-9081aae3f9fb
updated: 1484310421
title: mono
categories:
    - Dictionary
---
mono
     adj : designating sound transmission or recording or reproduction
           over a single channel [syn: {monophonic}, {single-channel}]
     n : an acute disease characterized by fever and swollen lymph
         nodes and an abnormal increase of mononuclear leucocytes
         or monocytes in the bloodstream; not highly contagious;
         some believe it can be transmitted by kissing [syn: {infectious
         mononucleosis}, {mononucleosis}, {glandular fever}, {kissing
         disease}]
