---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Peace_war_game
offline_file: ""
offline_thumbnail: ""
uuid: 5d41ed08-f79f-4f6f-b36b-f1cd7bc393cd
updated: 1484309055
title: Peace war game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Mongol_Empire_map.gif
categories:
    - Non-cooperative games
---
Peace war game is an iterated game originally played in academic groups and by computer simulation for years to study possible strategies of cooperation and aggression.[1] As peace makers became richer over time it became clear that making war had greater costs than initially anticipated. The only strategy that acquired wealth more rapidly was a "Genghis Khan", a constant aggressor making war continually to gain resources. This led to the development of the "provokable nice guy" strategy, a peace-maker until attacked.[2] Multiple players continue to gain wealth cooperating with each other while bleeding the constant aggressor.
