---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perturbed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484623441
title: perturbed
categories:
    - Dictionary
---
perturbed
     adj : thrown into a state of agitated confusion; (`rattled' is an
           informal term) [syn: {flustered}, {hot and bothered(p)},
            {rattled}]
