---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weakly_additive
offline_file: ""
offline_thumbnail: ""
uuid: 4b3d483b-c847-4732-a7b4-bf5cb826c575
updated: 1484308518
title: Weakly additive
categories:
    - Fair division
---
Every additive utility function is weakly-additive. However, additivity is applicable only to cardinal utility functions, while weak additivity is applicable to ordinal utility functions.
