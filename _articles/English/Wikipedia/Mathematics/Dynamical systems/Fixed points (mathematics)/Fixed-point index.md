---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fixed-point_index
offline_file: ""
offline_thumbnail: ""
uuid: 028b3e49-79fd-453b-98d0-ac0cf19af339
updated: 1484308786
title: Fixed-point index
categories:
    - Fixed points (mathematics)
---
In mathematics, the fixed-point index is a concept in topological fixed-point theory, and in particular Nielsen theory. The fixed-point index can be thought of as a multiplicity measurement for fixed points.
