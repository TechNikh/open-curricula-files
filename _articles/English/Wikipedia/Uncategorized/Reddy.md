---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reddy
offline_file: ""
offline_thumbnail: ""
uuid: 02e07216-99db-414e-b9c4-09a2893d3d6d
updated: 1484308331
title: Reddy
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kondavid-drug._Signed_%2527W.R.%2527.jpg'
tags:
    - Origin theories
    - Varna status
    - History
    - Medieval history
    - Kakatiya period
    - Reddy dynasty
    - Vijayanagara period
    - Modern history
    - Golkonda period
    - British period
    - Zamindaris
    - Modern politics
    - Notes and references
categories:
    - Uncategorized
---
Reddy (also transliterated as Raddi, Reddi, Reddiar, Reddappa, Reddy) is a caste that originated in India, predominantly settled in Andhra Pradesh and Telangana. They are classified as a forward caste.
