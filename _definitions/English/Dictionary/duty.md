---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/duty
offline_file: ""
offline_thumbnail: ""
uuid: 035f0bff-d814-4169-bdff-8a6779e60493
updated: 1484310467
title: duty
categories:
    - Dictionary
---
duty
     n 1: work that you are obliged to perform for moral or legal
          reasons; "the duties of the job"
     2: the social force that binds you to your obligations and the
        courses of action demanded by that force; "we must instill
        a sense of duty in our children"; "every right implies a
        responsibility; every opportunity, an obligation; every
        possession, a duty"- John D.Rockefeller Jr [syn: {responsibility},
         {obligation}]
     3: a government tax on imports or exports; "they signed a
        treaty to lower duties on trade between their countries"
        [syn: {tariff}]
