---
version: 1
type: article
id: https://en.wikipedia.org/wiki/AA_postulate
offline_file: ""
offline_thumbnail: ""
uuid: 9882f039-6f26-41e3-92c6-9833a8c85dfe
updated: 1484308924
title: AA postulate
categories:
    - Elementary geometry
---
The AA postulate states that the interior angles of a triangle are always equal to 180°. By knowing two angles, such as 32° and 64° degrees, we know that the next angle is 84°, because 180-(32+64)=84. (This is sometimes referred to as the AAA Postulate—which is true in all respects, but two angles are entirely sufficient.)
