---
title: Sustainable Development Goals
article_ref: https://en.wikipedia.org/wiki/Sustainable_Development_Goals
tags:
    - Background
    - The goals
    - Post-2015 development agenda process
    - Critique
    - Education
    - Intersectoral linkages
    - Water, sanitation, and hygiene
    - Climate change
categories:
    - Bamboo
---
