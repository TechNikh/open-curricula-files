---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Montonen%E2%80%93Olive_duality'
offline_file: ""
offline_thumbnail: ""
uuid: d6b9b327-566d-498b-bad7-056f64085e84
updated: 1484309236
title: Montonen–Olive duality
categories:
    - Duality theories
---
In theoretical physics, Montonen–Olive duality is the oldest known example of S-duality or a strong-weak duality. It generalizes the electro-magnetic symmetry of Maxwell's equations. It is named after Finnish Claus Montonen and British David Olive.
