---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Radiative_cooling
offline_file: ""
offline_thumbnail: ""
uuid: 52407a37-1263-475b-9b47-6a1e3faeaca8
updated: 1484308859
title: Radiative cooling
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Erbe.gif
tags:
    - Terrestrial radiative cooling
    - "Earth's energy budget"
    - "Radiative cooling on Earth's surface at night"
    - "Kelvin's estimate of the age of the Earth"
    - Applications
    - Architecture
    - Nocturnal ice making
    - Spacecraft
categories:
    - Thermodynamics
---
