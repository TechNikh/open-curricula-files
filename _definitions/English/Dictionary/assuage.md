---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assuage
offline_file: ""
offline_thumbnail: ""
uuid: 684fdedb-62fd-4aa1-9433-c34efbbf4689
updated: 1484310605
title: assuage
categories:
    - Dictionary
---
assuage
     v 1: cause to be more favorably inclined; gain the good will of;
          "She managed to mollify the angry customer" [syn: {pacify},
           {lenify}, {conciliate}, {appease}, {mollify}, {placate},
           {gentle}, {gruntle}]
     2: satisfy (thirst); "The cold water quenched his thirst" [syn:
         {quench}, {slake}, {allay}]
     3: provide physical relief, as from pain; "This pill will
        relieve your headaches" [syn: {relieve}, {alleviate}, {palliate}]
