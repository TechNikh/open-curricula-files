---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kosaraju%27s_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: f9597284-bd5c-4879-8b56-312de2dbc0cc
updated: 1484309402
title: "Kosaraju's algorithm"
tags:
    - The algorithm
    - Complexity
categories:
    - Graph connectivity
---
In computer science, Kosaraju's algorithm (also known as the Kosaraju–Sharir algorithm) is a linear time algorithm to find the strongly connected components of a directed graph. Aho, Hopcroft and Ullman credit it to an unpublished paper from 1978 by S. Rao Kosaraju. The same algorithm was independently discovered by Micha Sharir and published by him in 1981. It makes use of the fact that the transpose graph (the same graph with the direction of every edge reversed) has exactly the same strongly connected components as the original graph.
