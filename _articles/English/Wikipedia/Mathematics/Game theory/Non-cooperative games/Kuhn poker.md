---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kuhn_poker
offline_file: ""
offline_thumbnail: ""
uuid: 29117339-ed56-4ffb-854c-bffc913b6f21
updated: 1484309059
title: Kuhn poker
tags:
    - Game description
    - Optimal strategy
categories:
    - Non-cooperative games
---
Kuhn poker is an extremely simplified form of poker developed by Harold W. Kuhn as a simple model zero-sum two-player imperfect-information game, amenable to a complete game-theoretic analysis. In Kuhn poker, the deck includes only three playing cards, for example a King, Queen, and Jack. One card is dealt to each player, which may place bets similarly to a standard poker. If both players bet or both players pass, the player with the higher card wins, otherwise, the betting player wins.
