---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electronics
offline_file: ""
offline_thumbnail: ""
uuid: e443102d-dadc-4706-a389-141824bb6125
updated: 1484310244
title: electronics
categories:
    - Dictionary
---
electronics
     n : the branch of physics that deals with the emission and
         effects of electrons and with the use of electronic
         devices
