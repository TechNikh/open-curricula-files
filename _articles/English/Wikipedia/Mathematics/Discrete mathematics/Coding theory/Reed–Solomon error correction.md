---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Reed%E2%80%93Solomon_error_correction'
offline_file: ""
offline_thumbnail: ""
uuid: 30248804-97dd-4e33-bfb5-a38e7bf099a1
updated: 1484308551
title: Reed–Solomon error correction
tags:
    - History
    - Applications
    - Data storage
    - Bar code
    - Data transmission
    - Space transmission
    - Constructions
    - "Reed & Solomon's original view: The codeword as a sequence of values"
    - 'Simple encoding procedure: The message as a sequence of coefficients'
    - 'Systematic encoding procedure: The message as an initial sequence of values'
    - Theoretical decoding procedure
    - 'The BCH view: The codeword as a sequence of coefficients'
    - Systematic encoding procedure
    - 'Duality of the two views - discrete Fourier transform'
    - Remarks
    - Properties
    - Error correction algorithms
    - Peterson–Gorenstein–Zierler decoder
    - Syndrome decoding
    - Error locators and error values
    - Error locator polynomial
    - Obtain the error locators from the error locator polynomial
    - Calculate the error locations
    - Calculate the error values
    - Fix the errors
    - Berlekamp–Massey decoder
    - Example
    - Euclidean decoder
    - Example
    - Decoder using discrete Fourier transform
    - Decoding beyond the error-correction bound
    - Soft-decoding
    - Matlab Example
    - Encoder
    - Decoder
    - Notes
    - Information and Tutorials
    - Code
categories:
    - Coding theory
---
Reed–Solomon codes are a group of error-correcting codes that were introduced by Irving S. Reed and Gustave Solomon in 1960.[1] They have many applications, the most prominent of which include consumer technologies such as CDs, DVDs, Blu-ray Discs, QR Codes, data transmission technologies such as DSL and WiMAX, broadcast systems such as DVB and ATSC, and storage systems such as RAID 6. They are also used in satellite communication.
