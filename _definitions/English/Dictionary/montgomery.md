---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/montgomery
offline_file: ""
offline_thumbnail: ""
uuid: dc01c699-2b0b-4216-b957-c8396b5aa575
updated: 1484310172
title: montgomery
categories:
    - Dictionary
---
Montgomery
     n 1: Canadian novelist (1874-1942) [syn: {L. M. Montgomery}, {Lucy
          Maud Montgomery}]
     2: English general during World War II; won victories over
        Rommel in North Africa and led British ground forces in
        the invasion of Normandy (1887-1976) [syn: {Bernard Law
        Montgomery}, {Sir Bernard Law Montgomery}, {1st Viscount
        Montgomery of Alamein}]
     3: the state capital of Alabama on the Mobile River [syn: {capital
        of Alabama}]
