---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tommy_Bonnesen
offline_file: ""
offline_thumbnail: ""
uuid: 896eb3e0-5aa2-43a9-bd5d-393e94504edc
updated: 1484309075
title: Tommy Bonnesen
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tommy_Bonnesen.jpg
categories:
    - Geometers
---
Bonnesen studied at the University of Copenhagen, where in 1902 he received his Ph.D. (promotion) with thesis Analytiske studier over ikke-euklidisk geometri (Analytic studies of non-Euclidean geometry).[1] He was the Professor for Descriptive Geometry at the Polytekniske Læreanstalt.
