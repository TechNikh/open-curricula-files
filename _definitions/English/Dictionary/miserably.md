---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/miserably
offline_file: ""
offline_thumbnail: ""
uuid: 56ccd074-1f77-4dfa-bebf-ed7893146146
updated: 1484310152
title: miserably
categories:
    - Dictionary
---
miserably
     adv : in a miserable manner; "I bit my lip miserably and nodded"
