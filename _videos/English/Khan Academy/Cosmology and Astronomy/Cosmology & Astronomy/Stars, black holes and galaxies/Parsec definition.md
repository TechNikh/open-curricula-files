---
version: 1
type: video
provider: YouTube
id: 6zV3JEjLoyE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Parsec%20definition.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e334d4f7-0112-4e27-b8ea-479c8446d023
updated: 1486069688
title: Parsec definition
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/6zV3JEjLoyE/default.jpg
    - https://i3.ytimg.com/vi/6zV3JEjLoyE/1.jpg
    - https://i3.ytimg.com/vi/6zV3JEjLoyE/2.jpg
    - https://i3.ytimg.com/vi/6zV3JEjLoyE/3.jpg
---
