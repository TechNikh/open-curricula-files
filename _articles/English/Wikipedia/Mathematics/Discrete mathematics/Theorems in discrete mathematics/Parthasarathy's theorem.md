---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Parthasarathy%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: b2b7cfd2-ce05-40d0-b89a-11dac483f0a3
updated: 1484308662
title: "Parthasarathy's theorem"
categories:
    - Theorems in discrete mathematics
---
In mathematics and in particular the study of games on the unit square, Parthasarathy's theorem is a generalization of Von Neumann's minimax theorem. It states that a particular class of games has a mixed value, provided that at least one of the players has a strategy that is restricted to absolutely continuous distributions with respect to the Lebesgue measure (in other words, one of the players is forbidden to use a pure strategy).
