---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Three-dimensional_space
offline_file: ""
offline_thumbnail: ""
uuid: 4659514a-dd65-47b6-85ec-874b7ed0cb31
updated: 1484309168
title: Three-dimensional space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Coord_planes_color.svg.png
tags:
    - In euclidean geometry
    - Coordinate systems
    - Lines and planes
    - Spheres and balls
    - Polytopes
    - Surfaces of revolution
    - Quadric surfaces
    - In linear algebra
    - Dot product, angle, and length
    - Cross product
    - In calculus
    - Gradient, divergence and curl
    - Line integrals, surface integrals, and volume integrals
    - Fundamental theorem of line integrals
    - "Stokes' theorem"
    - Divergence theorem
    - In topology
    - Notes
categories:
    - Analytic geometry
---
Three-dimensional space (also: 3-space or, rarely, tri-dimensional space) is a geometric setting in which three values (called parameters) are required to determine the position of an element (i.e., point). This is the informal meaning of the term dimension.
