---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faceless
offline_file: ""
offline_thumbnail: ""
uuid: f5302360-0ed4-4566-ae87-73c9af922438
updated: 1484310537
title: faceless
categories:
    - Dictionary
---
faceless
     adj : without a face or identity; "a faceless apparition"; "the
           faceless accusers of the police state" [ant: {faced}]
