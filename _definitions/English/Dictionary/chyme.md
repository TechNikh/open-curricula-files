---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chyme
offline_file: ""
offline_thumbnail: ""
uuid: 38be7489-657c-4d11-84f5-1d070fbe4d7a
updated: 1484310330
title: chyme
categories:
    - Dictionary
---
chyme
     n : a semiliquid mass of partially digested food that passes
         from the stomach through the pyloric sphincter into the
         duodenum
