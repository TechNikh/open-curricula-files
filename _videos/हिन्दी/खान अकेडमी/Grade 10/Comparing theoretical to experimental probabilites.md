---
version: 1
type: video
provider: YouTube
id: 5132eQxLLXk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ae7dca9a-7395-4d6b-a918-b98f2a831c16
updated: 1486069621
title: Comparing theoretical to experimental probabilites
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/5132eQxLLXk/default.jpg
    - https://i3.ytimg.com/vi/5132eQxLLXk/1.jpg
    - https://i3.ytimg.com/vi/5132eQxLLXk/2.jpg
    - https://i3.ytimg.com/vi/5132eQxLLXk/3.jpg
---
