---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unilaterally
offline_file: ""
offline_thumbnail: ""
uuid: 64a043c3-ff49-4dcb-9a2c-a3a1c714002a
updated: 1484310186
title: unilaterally
categories:
    - Dictionary
---
unilaterally
     adv : in a unilateral manner; "they worked out an agreement
           unilaterally" [ant: {bilaterally}]
