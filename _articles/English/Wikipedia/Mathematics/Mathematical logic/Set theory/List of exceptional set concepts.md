---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_exceptional_set_concepts
offline_file: ""
offline_thumbnail: ""
uuid: dcff2a03-b419-4ea5-99c0-09075b433a23
updated: 1484309506
title: List of exceptional set concepts
categories:
    - Set theory
---
This is a list of exceptional set concepts. In mathematics, and in particular in mathematical analysis, it is very useful to be able to characterise subsets of a given set X as 'small', in some definite sense, or 'large' if their complement in X is small. There are numerous concepts that have been introduced to study 'small' or 'exceptional' subsets. In the case of sets of natural numbers, it is possible to define more than one concept of 'density', for example. See also list of properties of sets of reals.
