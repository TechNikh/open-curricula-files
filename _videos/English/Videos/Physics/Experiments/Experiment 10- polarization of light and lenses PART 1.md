---
version: 1
type: video
provider: YouTube
id: sdzUVpT4s_I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c375b2ca-e7b4-4dbb-b9ed-33cd327d6d13
updated: 1486069640
title: 'Experiment 10- polarization of light and lenses PART 1'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/sdzUVpT4s_I/default.jpg
    - https://i3.ytimg.com/vi/sdzUVpT4s_I/1.jpg
    - https://i3.ytimg.com/vi/sdzUVpT4s_I/2.jpg
    - https://i3.ytimg.com/vi/sdzUVpT4s_I/3.jpg
---
