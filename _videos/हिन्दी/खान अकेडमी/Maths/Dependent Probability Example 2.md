---
version: 1
type: video
provider: YouTube
id: b0CelhmrHdY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1c8fbe05-6324-489e-a05e-f15f540b4171
updated: 1486069611
title: Dependent Probability Example 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/b0CelhmrHdY/default.jpg
    - https://i3.ytimg.com/vi/b0CelhmrHdY/1.jpg
    - https://i3.ytimg.com/vi/b0CelhmrHdY/2.jpg
    - https://i3.ytimg.com/vi/b0CelhmrHdY/3.jpg
---
