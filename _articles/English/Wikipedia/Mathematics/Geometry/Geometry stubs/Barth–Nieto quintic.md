---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Barth%E2%80%93Nieto_quintic'
offline_file: ""
offline_thumbnail: ""
uuid: feb353c2-8fe4-400e-8e6e-cfc6312d94b1
updated: 1484309352
title: Barth–Nieto quintic
categories:
    - Geometry stubs
---
In algebraic geometry, the Barth–Nieto quintic is a quintic 3-fold in 4 (or sometimes 5) dimensional projective space studied by Barth & Nieto (1994) that is the Hessian of the Segre cubic. The Barth–Nieto quintic is the closure of the set of points (x0:x1:x2:x3:x4:x5) of P5 satisfying the equations
