---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Medial_graph
offline_file: ""
offline_thumbnail: ""
uuid: a7569cc2-9fb3-4823-9ca5-5bdac4dac112
updated: 1484309450
title: Medial graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/55px-Icon_Camera.svg_2.png
tags:
    - Formal definition
    - Properties
    - Applications
    - Directed medial graph
categories:
    - Graph operations
---
In the mathematical discipline of graph theory, the medial graph of plane graph G is another graph M(G) that represents the adjacencies between edges in the faces of G. Medial graphs were introduced in 1922 by Ernst Steinitz to study combinatorial properties of convex polyhedra,[1] although the inverse construction was already used by Peter Tait in 1877 in his foundational study of knots and links.[2][3]
