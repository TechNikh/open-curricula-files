---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nursing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484470021
title: nursing
categories:
    - Dictionary
---
nursing
     n 1: the work of caring for the sick or injured or infirm
     2: the profession of a nurse
     3: nourishing at the breast [syn: {breast feeding}]
