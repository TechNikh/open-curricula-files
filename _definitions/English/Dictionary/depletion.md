---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/depletion
offline_file: ""
offline_thumbnail: ""
uuid: c870c65b-ae6e-47d0-aca0-bc3df5c203b5
updated: 1484310262
title: depletion
categories:
    - Dictionary
---
depletion
     n 1: the act of decreasing something markedly
     2: the state of being depleted
