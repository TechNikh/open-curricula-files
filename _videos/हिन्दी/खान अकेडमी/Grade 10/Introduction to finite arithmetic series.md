---
version: 1
type: video
provider: YouTube
id: GI6bvnhjUqM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e17084b2-8353-4f35-82f6-3974dc14216b
updated: 1486069619
title: Introduction to finite arithmetic series
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/GI6bvnhjUqM/default.jpg
    - https://i3.ytimg.com/vi/GI6bvnhjUqM/1.jpg
    - https://i3.ytimg.com/vi/GI6bvnhjUqM/2.jpg
    - https://i3.ytimg.com/vi/GI6bvnhjUqM/3.jpg
---
