---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vegetable
offline_file: ""
offline_thumbnail: ""
uuid: a7ee5435-43fc-44ef-838e-9fd6a8097920
updated: 1484310377
title: vegetable
categories:
    - Dictionary
---
vegetable
     adj : of the nature of or characteristic of or derived from
           plants; "decaying vegetable matter" [ant: {mineral}, {animal}]
     n 1: edible seeds or roots or stems or leaves or bulbs or tubers
          or nonsweet fruits of any of numerous herbaceous plant
          [syn: {veggie}]
     2: any of various herbaceous plants cultivated for an edible
        part such as the fruit or the root of the beet or the leaf
        of spinach or the seeds of bean plants or the flower buds
        of broccoli or cauliflower
