---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sterility
offline_file: ""
offline_thumbnail: ""
uuid: d00d3591-40cf-40b4-9613-bfb2835e91fa
updated: 1484310158
title: sterility
categories:
    - Dictionary
---
sterility
     n 1: (of non-living objects) the state of being free of
          pathogenic organisms [syn: {asepsis}, {antisepsis}, {sterileness}]
     2: the state of being unable to produce offspring; in a woman
        it is an inability to conceive; in a man it is an
        inability to impregnate [syn: {infertility}] [ant: {fertility}]
