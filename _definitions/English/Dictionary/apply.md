---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apply
offline_file: ""
offline_thumbnail: ""
uuid: 890786f5-b6f5-42bd-951b-ced8c3626a4b
updated: 1484310212
title: apply
categories:
    - Dictionary
---
apply
     v 1: put into service; make work or employ (something) for a
          particular purpose or for its inherent or natural
          purpose; "use your head!"; "we only use Spanish at
          home"; "I can't make use of this tool"; "Apply a
          magnetic field here"; "This thinking was applied to many
          projects"; "How do you utilize this tool?"; "I apply
          this rule to get good results"; "use the plastic bags to
          store the food"; "He doesn't know how to use a computer"
          [syn: {use}, {utilize}, {utilise}, {employ}]
     2: be pertinent or relevant or applicable; "The same laws apply
        to you!"; "This theory holds for all irrational ...
