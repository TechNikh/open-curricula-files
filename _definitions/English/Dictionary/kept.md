---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kept
offline_file: ""
offline_thumbnail: ""
uuid: 48450061-7770-479d-a6ee-d324a7008f42
updated: 1484310339
title: kept
categories:
    - Dictionary
---
kept
     adj : (especially of promises or contracts) not violated or
           disregarded; "unbroken promises"; "promises kept" [syn:
            {unbroken}] [ant: {broken}]
