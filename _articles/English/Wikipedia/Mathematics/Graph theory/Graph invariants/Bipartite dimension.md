---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bipartite_dimension
offline_file: ""
offline_thumbnail: ""
uuid: 3846600c-f7d4-4511-8685-48743e3c3645
updated: 1484309414
title: Bipartite dimension
tags:
    - Example
    - Bipartite dimension formulas for some graphs
    - Computing the bipartite dimension
    - Applications
categories:
    - Graph invariants
---
In the mathematical fields of graph theory and combinatorial optimization, the bipartite dimension or biclique cover number of a graph G = (V, E) is the minimum number of bicliques (that is complete bipartite subgraphs), needed to cover all edges in E. A collection of bicliques covering all edges in G is called a biclique edge cover, or sometimes biclique cover. The bipartite dimension of G is often denoted by the symbol d(G).
