---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/warfare
offline_file: ""
offline_thumbnail: ""
uuid: e71f8c16-40c2-4570-9f7e-cdd3734363cc
updated: 1484310573
title: warfare
categories:
    - Dictionary
---
warfare
     n 1: the waging of armed conflict against an enemy; "thousands of
          people were killed in the war" [syn: {war}]
     2: an active struggle between competing entities; "a price
        war"; "a war of wits"; "diplomatic warfare" [syn: {war}]
