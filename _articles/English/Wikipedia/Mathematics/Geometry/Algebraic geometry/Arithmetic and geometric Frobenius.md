---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Arithmetic_and_geometric_Frobenius
offline_file: ""
offline_thumbnail: ""
uuid: 922597aa-4736-42a3-bd6f-6e41e7344930
updated: 1484309134
title: Arithmetic and geometric Frobenius
categories:
    - Algebraic geometry
---
In mathematics, the Frobenius endomorphism is defined in any commutative ring R that has characteristic p, where p is a prime number. Namely, the mapping φ that takes r in R to rp is a ring endomorphism of R.
