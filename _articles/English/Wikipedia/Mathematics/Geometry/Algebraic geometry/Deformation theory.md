---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deformation_theory
offline_file: ""
offline_thumbnail: ""
uuid: fb5e7388-5d29-41b9-a5f5-6e12d9bb9ee0
updated: 1484309138
title: Deformation theory
tags:
    - Deformations of complex manifolds
    - Relationship to string theory
categories:
    - Algebraic geometry
---
In mathematics, deformation theory is the study of infinitesimal conditions associated with varying a solution P of a problem to slightly different solutions Pε, where ε is a small number, or vector of small quantities. The infinitesimal conditions are therefore the result of applying the approach of differential calculus to solving a problem with constraints. One might think, in analogy, of a structure that is not completely rigid, and that deforms slightly to accommodate forces applied from the outside; this explains the name.
