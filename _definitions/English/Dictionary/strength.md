---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strength
offline_file: ""
offline_thumbnail: ""
uuid: 18ee06b3-ba0e-4288-9368-41b1457373dc
updated: 1484310194
title: strength
categories:
    - Dictionary
---
strength
     n 1: the property of being physically or mentally strong;
          "fatigue sapped his strength" [ant: {weakness}]
     2: capability in terms of personnel and materiel that affect
        the capacity to fight a war; "we faced an army of great
        strength"; "politicians have neglected our military
        posture" [syn: {military capability}, {military strength},
         {military posture}, {posture}]
     3: physical energy or intensity; "he hit with all the force he
        could muster"; "it was destroyed by the strength of the
        gale"; "a government has not the vitality and forcefulness
        of a living man" [syn: {force}, {forcefulness}]
     4: an asset ...
