---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Defective_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 50895d89-232e-41c4-9711-e207b0cbb1b5
updated: 1484309379
title: Defective coloring
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Example_of_defective_colouring_of_a_cycle_on_five_vertices_when_d_%253D_0%252C_1%252C_2.PNG'
tags:
    - History
    - Definitions and terminology
    - Defective coloring
    - d-Defective chromatic number
    - Impropriety of a vertex
    - Impropriety of a vertex-coloring
    - Example
    - Properties
    - Few Results
    - Every outerplanar graph is (2,2)-colorable
    - Graphs excluding a complete minor
    - Applications
    - Notes
categories:
    - Graph coloring
---
In graph theory, a mathematical discipline, coloring refers to an assignment of colours or labels to vertices, edges and faces of a graph. Defective coloring is a variant of proper vertex coloring. In a proper vertex coloring, the vertices are coloured such that no adjacent vertices have the same colour. In defective coloring, on the other hand, vertices are allowed to have neighbours of the same colour to a certain extent. (See here for Glossary of graph theory)
