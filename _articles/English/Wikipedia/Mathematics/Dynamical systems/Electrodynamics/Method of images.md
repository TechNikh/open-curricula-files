---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Method_of_images
offline_file: ""
offline_thumbnail: ""
uuid: 0320f170-66bd-4441-86f4-af2e982ed221
updated: 1484308764
title: Method of images
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-VFPt_imagecharge_plane_horizontal_plusminus.svg.png
categories:
    - Electrodynamics
---
The method of images (or method of mirror images) is a mathematical tool for solving differential equations, in which the domain of the sought function is extended by the addition of its mirror image with respect to a symmetry hyperplane. As a result, certain boundary conditions are satisfied automatically by the presence of a mirror image, greatly facilitating the solution of the original problem.
