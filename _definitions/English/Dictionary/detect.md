---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/detect
offline_file: ""
offline_thumbnail: ""
uuid: 42bccd9f-0ac4-4863-bb9b-cf256e876725
updated: 1484310379
title: detect
categories:
    - Dictionary
---
detect
     v : discover or determine the existence, presence, or fact of;
         "She detected high levels of lead in her drinking water";
         "We found traces of lead in the paint" [syn: {observe}, {find},
          {discover}, {notice}]
