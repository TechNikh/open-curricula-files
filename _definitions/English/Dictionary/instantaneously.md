---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instantaneously
offline_file: ""
offline_thumbnail: ""
uuid: 583e1163-b24c-42b1-863a-9deef7e30373
updated: 1484310202
title: instantaneously
categories:
    - Dictionary
---
instantaneously
     adv : without any delay; "he was killed outright" [syn: {outright},
            {instantly}, {in a flash}]
