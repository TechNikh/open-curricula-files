---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tiller
offline_file: ""
offline_thumbnail: ""
uuid: 4a7a030c-0594-4181-a7d1-04fb2604a4ef
updated: 1484310603
title: tiller
categories:
    - Dictionary
---
tiller
     n 1: a shoot that sprouts from the base of a grass
     2: someone who tills land (prepares the soil for the planting
        of crops)
     3: lever used to turn the rudder on a boat
     4: a farm implement used to break up the surface of the soil
        (for aeration and weed control and conservation of
        moisture) [syn: {cultivator}]
     v : grow shoots in the form of stools or tillers [syn: {stool}]
