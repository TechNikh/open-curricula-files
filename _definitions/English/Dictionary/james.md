---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/james
offline_file: ""
offline_thumbnail: ""
uuid: 7a98d247-eb71-473b-85cc-19d9a5d035fc
updated: 1484310297
title: james
categories:
    - Dictionary
---
James
     n 1: United States outlaw who fought as a Confederate soldier and
          later led a band of outlaws that robbed trains and banks
          in the West until he was murdered by a member of his own
          gang (1847-1882) [syn: {Jesse James}]
     2: United States pragmatic philosopher and psychologist
        (1842-1910) [syn: {William James}]
     3: writer who was born in the United States but lived in
        England (1843-1916) [syn: {Henry James}]
     4: (New Testament) disciple of Jesus; brother of John; author
        of the Epistle of James in the New Testament [syn: {Saint
        James}, {St. James}, {Saint James the Apostle}, {St. James
        the Apostle}]
    ...
