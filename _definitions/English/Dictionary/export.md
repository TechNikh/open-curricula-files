---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/export
offline_file: ""
offline_thumbnail: ""
uuid: 732d4958-1779-42aa-94cb-30dd471dc3d6
updated: 1484310477
title: export
categories:
    - Dictionary
---
export
     n : commodities (goods or services) sold to a foreign country
         [syn: {exportation}] [ant: {import}]
     v 1: sell or transfer abroad; "we export less than we import and
          have a negative trade balance" [ant: {import}]
     2: cause to spread in another part of the world; "The Russians
        exported Marxism to Africa"
