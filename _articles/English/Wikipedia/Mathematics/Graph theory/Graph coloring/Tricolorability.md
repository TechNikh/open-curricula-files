---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tricolorability
offline_file: ""
offline_thumbnail: ""
uuid: 4f037a4e-5fee-451b-b812-b1d8fc095afe
updated: 1484309387
title: Tricolorability
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tricoloring.png
tags:
    - Rules of tricolorability
    - Examples
    - Example of a tricolorable knot
    - Example of a non-tricolorable knot
    - Isotopy invariant
    - Properties
    - In torus knots
    - Sources
categories:
    - Graph coloring
---
In the mathematical field of knot theory, the tricolorability of a knot is the ability of a knot to be colored with three colors subject to certain rules. Tricolorability is an isotopy invariant, and hence can be used to distinguish between two different (non-isotopic) knots. In particular, since the unknot is not tricolorable, any tricolorable knot is necessarily nontrivial.
