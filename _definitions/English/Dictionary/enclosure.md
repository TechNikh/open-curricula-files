---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enclosure
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484319841
title: enclosure
categories:
    - Dictionary
---
enclosure
     n 1: artifact consisting of a space that has been enclosed for
          some purpose
     2: the act of enclosing something inside something else [syn: {enclosing},
         {envelopment}, {inclosure}]
     3: a naturally enclosed space [syn: {natural enclosure}]
     4: something (usually a supporting document) that is enclosed
        in an envelope with a covering letter [syn: {inclosure}]
