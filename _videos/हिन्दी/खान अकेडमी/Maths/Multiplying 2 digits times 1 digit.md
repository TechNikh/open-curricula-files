---
version: 1
type: video
provider: YouTube
id: LL92Pq6-iMU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 01f05a53-30ad-4237-8416-40a22123e990
updated: 1486069614
title: Multiplying 2 digits times 1 digit
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/LL92Pq6-iMU/default.jpg
    - https://i3.ytimg.com/vi/LL92Pq6-iMU/1.jpg
    - https://i3.ytimg.com/vi/LL92Pq6-iMU/2.jpg
    - https://i3.ytimg.com/vi/LL92Pq6-iMU/3.jpg
---
