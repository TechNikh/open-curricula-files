---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Adjacent-vertex-distinguishing-total_coloring
offline_file: ""
offline_thumbnail: ""
uuid: c43c929e-d8b1-410b-8849-9977e429563b
updated: 1484309379
title: Adjacent-vertex-distinguishing-total coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Avd-total-coloring-of-complete-graph-K4.svg.png
categories:
    - Graph coloring
---
(1). no adjacent vertices have the same color;
