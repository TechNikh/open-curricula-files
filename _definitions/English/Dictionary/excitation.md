---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/excitation
offline_file: ""
offline_thumbnail: ""
uuid: 14e019c5-a934-4f40-9e0a-363b1ebffd79
updated: 1484310407
title: excitation
categories:
    - Dictionary
---
excitation
     n 1: the state of being emotionally aroused and worked up; "his
          face was flushed with excitement and his hands
          trembled"; "he tried to calm those who were in a state
          of extreme inflammation" [syn: {excitement}, {inflammation},
           {fervor}, {fervour}]
     2: the neural or electrical arousal of an organ or muscle or
        gland [syn: {innervation}, {irritation}]
     3: something that agitates and arouses; "he looked forward to
        the excitements of the day" [syn: {excitement}]
