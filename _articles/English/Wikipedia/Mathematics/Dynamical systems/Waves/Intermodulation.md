---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Intermodulation
offline_file: ""
offline_thumbnail: ""
uuid: 81698961-4f50-4fcc-a57d-a0a02379c987
updated: 1484308887
title: Intermodulation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/288px-RF_Intermodulation_at_280_MHz.jpg
tags:
    - Causes of intermodulation
    - Intermodulation order
    - Passive intermodulation (PIM)
    - Sources of PIM
    - PIM Testing
    - Intermodulation in electronic circuits
    - Measurement
categories:
    - Waves
---
Intermodulation (IM) or intermodulation distortion (IMD) is the amplitude modulation of signals containing two or more different frequencies, caused by nonlinearities in a system. The intermodulation between each frequency component will form additional signals at frequencies that are not just at harmonic frequencies (integer multiples) of either, like harmonic distortion, but also at the sum and difference frequencies of the original frequencies and at multiples of those sum and difference frequencies.
