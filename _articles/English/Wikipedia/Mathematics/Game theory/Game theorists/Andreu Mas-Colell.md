---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Andreu_Mas-Colell
offline_file: ""
offline_thumbnail: ""
uuid: cc89d5d3-b5a5-42e4-8cf6-dd57384eb48f
updated: 1484308512
title: Andreu Mas-Colell
tags:
    - Biography
    - Research
    - Books
    - Awards and honors
categories:
    - Game theorists
---
Andreu Mas-Colell (Catalan: [ənˈdɾew ˈmas kuˈɫeʎ], born 29 June 1944) is a Spanish economist, an expert in microeconomics and one of the world's leading mathematical economists.[1] He is the founder of the Barcelona Graduate School of Economics and a professor in the department of economics at Pompeu Fabra University in Barcelona, Catalonia, Spain. He has also served several times in the cabinet of the Catalan government.[2][3][4][5] Summarizing his and others' research in general equilibrium theory, his monograph gave a thorough exposition of research using differential topology.[6][7] His textbook on microeconomics, co-authored with Michael Whinston and Jerry Green, is the most ...
