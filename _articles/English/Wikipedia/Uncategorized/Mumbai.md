---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mumbai
offline_file: ""
offline_thumbnail: ""
uuid: 19042a5d-c1ec-4db1-96b1-52dc39038138
updated: 1484308409
title: Mumbai
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mumbai_03-2016_11_Haji_Ali_Dargah.jpg
tags:
    - Etymology
    - History
    - Early history
    - Portuguese and British rule
    - Independent India
    - geography
    - Climate
    - Economy
    - Civic administration
    - Politics
    - Transport
    - Public transport
    - Rail
    - Bus
    - Water
    - Road
    - air
    - Sea
    - Utility services
    - Architecture
    - Demographics
    - Ethnic groups and religions
    - culture
    - Media
    - Education
    - schools
    - Higher education
    - Sports
    - Notes
categories:
    - Uncategorized
---
Mumbai (/mʊmˈbaɪ/; also known as Bombay, the official name until 1995) is the capital city of the Indian state of Maharashtra. It is the most populous city in India and the ninth most populous agglomeration in the world, with an estimated city population of 18.4 million. Along with the neighbouring regions of the Mumbai Metropolitan Region, it is one of the most populous urban regions in the world and the second most populous metropolitan area in India, with a population of 20.7 million as of 2011[update].[11][12] Mumbai lies on the west coast of India and has a deep natural harbour. In 2009, Mumbai was named an alpha world city.[13] It is also the wealthiest city in India,[14] and has ...
