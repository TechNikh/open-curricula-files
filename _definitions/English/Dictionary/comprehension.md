---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comprehension
offline_file: ""
offline_thumbnail: ""
uuid: efc3c988-9034-4199-932e-8d9f9f2ee79f
updated: 1484310154
title: comprehension
categories:
    - Dictionary
---
comprehension
     n 1: an ability to understand the meaning or importance of
          something (or the knowledge acquired as a result); "how
          you can do that is beyond my comprehension"; "he was
          famous for his comprehension of American literature"
          [ant: {incomprehension}]
     2: the relation of comprising something; "he admired the
        inclusion of so many ideas in such a short work" [syn: {inclusion}]
