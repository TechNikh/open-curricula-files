---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carved
offline_file: ""
offline_thumbnail: ""
uuid: 73a3207a-ad78-44e9-97cb-fe1534f32bce
updated: 1484310461
title: carved
categories:
    - Dictionary
---
carved
     adj : made for or formed by carving (`carven' is archaic or
           literary); "the carved fretwork"; "an intricately
           carved door"; "stood as if carven from stone" [syn: {carven}]
           [ant: {uncarved}]
