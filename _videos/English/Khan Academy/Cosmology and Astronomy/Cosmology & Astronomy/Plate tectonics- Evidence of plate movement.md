---
version: 1
type: video
provider: YouTube
id: 6EdsBabSZ4g
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Plate%20tectonics-%20Evidence%20of%20plate%20movement.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1517c781-88e2-4a4f-98d8-6e4d743fdf28
updated: 1486069685
title: 'Plate tectonics- Evidence of plate movement'
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/6EdsBabSZ4g/default.jpg
    - https://i3.ytimg.com/vi/6EdsBabSZ4g/1.jpg
    - https://i3.ytimg.com/vi/6EdsBabSZ4g/2.jpg
    - https://i3.ytimg.com/vi/6EdsBabSZ4g/3.jpg
---
