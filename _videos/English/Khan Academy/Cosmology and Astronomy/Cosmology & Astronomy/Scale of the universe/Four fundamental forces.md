---
version: 1
type: video
provider: YouTube
id: FEF6PxWOvsk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Four%20fundamental%20forces.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2897a059-0a59-468a-a97e-b1b7541af655
updated: 1486069686
title: Four fundamental forces
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/FEF6PxWOvsk/default.jpg
    - https://i3.ytimg.com/vi/FEF6PxWOvsk/1.jpg
    - https://i3.ytimg.com/vi/FEF6PxWOvsk/2.jpg
    - https://i3.ytimg.com/vi/FEF6PxWOvsk/3.jpg
---
