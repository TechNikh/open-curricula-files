---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/restricted
offline_file: ""
offline_thumbnail: ""
uuid: 3f1d5d48-4522-4925-b900-db06c802eaf6
updated: 1484310196
title: restricted
categories:
    - Dictionary
---
restricted
     adj 1: subject to restriction or subjected to restriction; "of
            restricted importance" [ant: {unrestricted}]
     2: restricted in meaning; (as e.g. `man' in `a tall man') [syn:
         {qualified}]
     3: curbed or regulated; "controlled emotions" [syn: {controlled}]
     4: the lowest level of official classification for documents
