---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Post-office
offline_file: ""
offline_thumbnail: ""
uuid: 98e54640-b056-418b-ad36-2fdf114a7cc2
updated: 1484308432
title: Post office
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-%25E7%259C%25BA%25E6%259C%259B%25E4%25B8%258A%25E6%25B5%25B7%25E5%25B8%2582%25E9%2582%25AE%25E6%2594%25BF%25E5%25B1%2580.jpg'
tags:
    - Name
    - History
    - Unstaffed postal facilities
    - Notable post offices
    - Operational
    - Former
    - Historic
categories:
    - Uncategorized
---
A post office is a customer service facility forming part of a national postal system.[1] Post offices offer mail-related services such as acceptance of letters and parcels; provision of post office boxes; and sale of postage stamps, packaging, and stationery. In addition, many post offices offer additional services: providing and accepting government forms (such as passport applications), processing government services and fees (such as road tax), and banking services (such as savings accounts and money orders).[2] The chief administrator of a post office is a postmaster.
