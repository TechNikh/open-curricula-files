---
version: 1
type: video
provider: YouTube
id: ADN9Rph96NE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e24d0da4-f15e-4006-b572-fc445e817286
updated: 1486069637
title: Determining Refractive Index Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/ADN9Rph96NE/default.jpg
    - https://i3.ytimg.com/vi/ADN9Rph96NE/1.jpg
    - https://i3.ytimg.com/vi/ADN9Rph96NE/2.jpg
    - https://i3.ytimg.com/vi/ADN9Rph96NE/3.jpg
---
