---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calamity
offline_file: ""
offline_thumbnail: ""
uuid: 87af4b05-26d2-409b-957c-0cf2893ccc8d
updated: 1484310537
title: calamity
categories:
    - Dictionary
---
calamity
     n : an event resulting in great loss and misfortune; "the whole
         city was affected by the irremediable calamity"; "the
         earthquake was a disaster" [syn: {catastrophe}, {disaster},
          {tragedy}, {cataclysm}]
