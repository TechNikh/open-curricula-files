---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rainfed
offline_file: ""
offline_thumbnail: ""
uuid: c1ee2a3a-5714-42bf-9f04-08bf91e960fd
updated: 1484308425
title: Rainfed agriculture
categories:
    - Uncategorized
---
The term rainfed agriculture is used to describe farming practises that rely on rainfall for water. It provides much of the food consumed by poor communities in developing countries. For example, rainfed agriculture accounts for more than 95% of farmed land in sub-Saharan Africa, 90% in Latin America, 75% in the Near East and North Africa; 65% in East Asia and 60% in South Asia.[1]
