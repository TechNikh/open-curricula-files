---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entropy_of_entanglement
offline_file: ""
offline_thumbnail: ""
uuid: c068f24b-c98f-4393-8d13-4f5cb2eaab8b
updated: 1484308760
title: Entropy of entanglement
tags:
    - Bipartite entanglement entropy
    - Von Neumann entanglement entropy
    - Renyi entanglement entropies
    - Area law of bipartite entanglement entropy
    - References/sources
categories:
    - Entropy
---
