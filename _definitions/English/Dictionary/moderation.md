---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moderation
offline_file: ""
offline_thumbnail: ""
uuid: aeb0463e-0f20-4a7a-886d-f9a149507799
updated: 1484310181
title: moderation
categories:
    - Dictionary
---
moderation
     n 1: quality of being moderate and avoiding extremes [syn: {moderateness}]
          [ant: {immoderation}]
     2: a change for the better [syn: {easing}, {relief}]
     3: the trait of avoiding excesses [syn: {temperance}] [ant: {intemperance}]
     4: the action of lessening in severity or intensity; "the
        object being control or moderation of economic
        depressions"
