---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thue_number
offline_file: ""
offline_thumbnail: ""
uuid: f9424495-e0ca-47d9-8f36-a82a3b8d263c
updated: 1484309386
title: Thue number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Thue-number.svg.png
tags:
    - Example
    - Results
    - Computational complexity
categories:
    - Graph coloring
---
In the mathematical area of graph theory, the Thue number of a graph is a variation of the chromatic index, defined by Alon et al. (2002) and named after mathematician Axel Thue, who studied the squarefree words used to define this number.
