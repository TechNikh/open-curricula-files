---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expand
offline_file: ""
offline_thumbnail: ""
uuid: c2689006-7be2-4ba7-b241-3534521577bb
updated: 1484310448
title: expand
categories:
    - Dictionary
---
expand
     v 1: extend in one or more directions; "The dough expands" [syn:
          {spread out}] [ant: {shrink}]
     2: become larger in size or volume or quantity; "his business
        expanded rapidly"
     3: make bigger or wider in size, volume, or quantity; "expand
        the house by adding another wing"
     4: grow stronger; "The economy was booming" [syn: {boom}, {prosper},
         {thrive}, {get ahead}, {flourish}]
     5: exaggerate or make bigger; "The charges were inflated" [syn:
         {inflate}, {blow up}, {amplify}]
     6: add details, as to an account or idea; clarify the meaning
        of and discourse in a learned way, usually in writing;
        "She ...
