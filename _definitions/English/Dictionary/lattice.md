---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lattice
offline_file: ""
offline_thumbnail: ""
uuid: 5bcc78e2-b3ca-4b75-a998-17cea36e87ff
updated: 1484310202
title: lattice
categories:
    - Dictionary
---
lattice
     n 1: an arrangement of points or particles or objects in a
          regular periodic pattern in 2 or 3 dimensions
     2: small opening (like a window in a door) through which
        business can be transacted [syn: {wicket}, {grille}]
     3: framework consisting of an ornamental design made of strips
        of wood or metal [syn: {latticework}, {fretwork}]
