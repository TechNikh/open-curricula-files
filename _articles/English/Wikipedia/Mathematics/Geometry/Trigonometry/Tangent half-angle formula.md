---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tangent_half-angle_formula
offline_file: ""
offline_thumbnail: ""
uuid: 895e6065-7216-4811-9bca-b9647ab5c791
updated: 1484309350
title: Tangent half-angle formula
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Weierstrass_substitution.svg.png
tags:
    - Proofs
    - Algebraic proofs
    - Geometric proofs
    - The tangent half-angle substitution in integral calculus
    - Hyperbolic identities
    - The Gudermannian function
    - Pythagorean triples
categories:
    - Trigonometry
---
In trigonometry, tangent half-angle formulas relate the tangent of half of an angle to trigonometric functions of the entire angle. Among these are the following
