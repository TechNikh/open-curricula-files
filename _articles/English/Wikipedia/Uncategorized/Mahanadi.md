---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mahanadi
offline_file: ""
offline_thumbnail: ""
uuid: e54d2a69-c4e4-48b3-963c-5be092dfd3d5
updated: 1484308422
title: Mahanadi
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Mahanadi-river-delta.jpg
tags:
    - Sources
    - Middle course
    - Mouth
    - Navigation
    - Trade and agriculture
    - Water
    - Floods
categories:
    - Uncategorized
---
The Mahanadi is a major river in East Central India. It drains an area of around 141,600 square kilometres (54,700 sq mi) and has a total course of 858 kilometres (533 mi).[1] The river flows through the states of Chhattisgarh and Odisha.The word Mahanadi literally comes from two odia words 'maha' and nadi' meaning 'The Great River'.
