---
version: 1
type: video
provider: YouTube
id: IPJAi5zWu9U
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Officer%20on%20Horseback.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 05dcbcc7-5725-4898-ae44-b8237f065f02
updated: 1486069575
title: Officer on Horseback
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/IPJAi5zWu9U/default.jpg
    - https://i3.ytimg.com/vi/IPJAi5zWu9U/1.jpg
    - https://i3.ytimg.com/vi/IPJAi5zWu9U/2.jpg
    - https://i3.ytimg.com/vi/IPJAi5zWu9U/3.jpg
---
Not quite a brain teaser but fun nonetheless!
More free lessons at: http://www.khanacademy.org/video?v=IPJAi5zWu9U
