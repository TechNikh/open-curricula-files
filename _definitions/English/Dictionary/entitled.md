---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entitled
offline_file: ""
offline_thumbnail: ""
uuid: 7e3e584e-21e8-4387-9b48-a8eea7753c54
updated: 1484310535
title: entitled
categories:
    - Dictionary
---
entitled
     adj 1: qualified for by right according to law; "we are all
            entitled to equal protection under the law"
     2: given a title or identifying name; "the book entitled `A
        Tale of Two Cities'"
