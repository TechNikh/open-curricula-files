---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prevalent
offline_file: ""
offline_thumbnail: ""
uuid: 2559f23c-a10a-45ad-9701-eedfbe3c307b
updated: 1484310518
title: prevalent
categories:
    - Dictionary
---
prevalent
     adj : encountered generally especially at the present time; "the
           prevailing opinion was that a trade war could be
           averted"; "the most prevalent religion in our area";
           "speculation concerning the books author was rife"
           [syn: {prevailing}, {rife}]
