---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Philippines
offline_file: ""
offline_thumbnail: ""
uuid: 833dccea-7030-45c9-befe-05d90751912e
updated: 1484308342
title: Philippines
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Banaue_Rice_Terraces_by_Nonoyborbun.jpg
tags:
    - Etymology
    - History
    - Prehistory
    - Precolonial epoch
    - Colonial era
    - Postcolonial period
    - Contemporary history
    - Politics
    - Foreign relations
    - Military
    - Administrative divisions
    - Regions of the Philippines
    - geography
    - wildlife
    - Climate
    - Economy
    - Transportation
    - science and technology
    - Communications
    - Tourism
    - hydrology
    - Demographics
    - Cities
    - Ethnic groups
    - Languages
    - religion
    - Health
    - Education
    - culture
    - Cosmopolitanism
    - Music
    - Visual art
    - Values
    - Dance
    - Cuisine
    - Literature
    - Media
    - Sports
    - Games
    - Notes
    - Citations
    - Bibliography
categories:
    - Uncategorized
---
The Philippines (i/ˈfɪlᵻpiːnz/; Filipino: Pilipinas [ˌpɪlɪˈpinɐs]), officially the Republic of the Philippines (Filipino: Republika ng Pilipinas), is a sovereign island country in Southeast Asia situated in the western Pacific Ocean. It consists of about 7,641 islands[16] that are categorized broadly under three main geographical divisions from north to south: Luzon, Visayas, and Mindanao. The capital city of the Philippines is Manila and the most populous city is Quezon City, both part of Metro Manila.[17] Bounded by the South China Sea on the west, the Philippine Sea on the east and the Celebes Sea on the southwest, the Philippines shares maritime borders with Taiwan to the ...
