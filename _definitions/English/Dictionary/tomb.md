---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tomb
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441521
title: tomb
categories:
    - Dictionary
---
tomb
     n : a place for the burial of a corpse (especially beneath the
         ground and marked by a tombstone); "he put flowers on his
         mother's grave" [syn: {grave}]
