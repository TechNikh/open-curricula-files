---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/involve
offline_file: ""
offline_thumbnail: ""
uuid: f7b11ed0-88bd-4d76-8f07-07c67371df01
updated: 1484310268
title: involve
categories:
    - Dictionary
---
involve
     v 1: connect closely and often incriminatingly; "This new ruling
          affects your business" [syn: {affect}, {regard}]
     2: engage as a participant; "Don't involve me in your family
        affairs!"
     3: have as a necessary feature or consequence; entail; "This
        decision involves many changes" [syn: {imply}]
     4: require as useful, just, or proper; "It takes nerve to do
        what she did"; "success usually requires hard work"; "This
        job asks a lot of patience and skill"; "This position
        demands a lot of personal sacrifice"; "This dinner calls
        for a spectacular dessert"; "This intervention does not
        postulates a patient's ...
