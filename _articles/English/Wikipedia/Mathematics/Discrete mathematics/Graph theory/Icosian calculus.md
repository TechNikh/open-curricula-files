---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Icosian_calculus
offline_file: ""
offline_thumbnail: ""
uuid: 56daabb0-d557-43e8-8375-7ddcc63b11e6
updated: 1484308619
title: Icosian calculus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Icosian_grid_small_with_labels2.svg.png
categories:
    - Graph theory
---
The icosian calculus is a non-commutative algebraic structure discovered by the Irish mathematician William Rowan Hamilton in 1856.[1][2] In modern terms, he gave a group presentation of the icosahedral rotation group by generators and relations.
