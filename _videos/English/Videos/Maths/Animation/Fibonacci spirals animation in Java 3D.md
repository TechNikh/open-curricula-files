---
version: 1
type: video
provider: YouTube
id: ie-6tcX5jKg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bc08187d-cd08-4a33-b523-69096c7adc7d
updated: 1486069649
title: Fibonacci spirals animation in Java 3D
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/ie-6tcX5jKg/default.jpg
    - https://i3.ytimg.com/vi/ie-6tcX5jKg/1.jpg
    - https://i3.ytimg.com/vi/ie-6tcX5jKg/2.jpg
    - https://i3.ytimg.com/vi/ie-6tcX5jKg/3.jpg
---
