---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Absolutely_convex_set
offline_file: ""
offline_thumbnail: ""
uuid: 7a7449a4-5ae8-4a14-b15e-5cec04c8ffb6
updated: 1484309189
title: Absolutely convex set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Absolute_convex_hull.svg.png
tags:
    - Properties
    - Absolutely convex hull
categories:
    - Convex geometry
---
A set C in a real or complex vector space is said to be absolutely convex or disked if it is convex and balanced (circled), in which case it is called a disk.
