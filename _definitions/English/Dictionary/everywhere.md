---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/everywhere
offline_file: ""
offline_thumbnail: ""
uuid: 3eec5a36-c63d-4fc3-bb9c-cc031ab153b4
updated: 1484310366
title: everywhere
categories:
    - Dictionary
---
everywhere
     adv : to or in any or all places; "You find fast food stores
           everywhere"; "people everywhere are becoming aware of
           the problem"; "he carried a gun everywhere he went";
           "looked all over for a suitable gift"; (`everyplace' is
           used informally for `everywhere') [syn: {everyplace}, {all
           over}]
