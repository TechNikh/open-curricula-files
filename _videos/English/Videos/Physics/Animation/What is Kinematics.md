---
version: 1
type: video
provider: YouTube
id: XMnM2KGCyLc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20Kinematics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d59eb7b0-7ff1-4964-bfc0-946b1e7fa85e
updated: 1486069654
title: What is Kinematics
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/XMnM2KGCyLc/default.jpg
    - https://i3.ytimg.com/vi/XMnM2KGCyLc/1.jpg
    - https://i3.ytimg.com/vi/XMnM2KGCyLc/2.jpg
    - https://i3.ytimg.com/vi/XMnM2KGCyLc/3.jpg
---
