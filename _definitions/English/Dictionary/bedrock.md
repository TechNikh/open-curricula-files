---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bedrock
offline_file: ""
offline_thumbnail: ""
uuid: da73b01c-2ec3-4b79-a9dc-a1df04af9e4f
updated: 1484310256
title: bedrock
categories:
    - Dictionary
---
bedrock
     n 1: solid unweathered rock lying beneath surface deposits of
          soil
     2: principles from which other truths can be derived; "first
        you must learn the fundamentals"; "let's get down to
        basics" [syn: {fundamentals}, {basics}, {fundamental
        principle}, {basic principle}]
