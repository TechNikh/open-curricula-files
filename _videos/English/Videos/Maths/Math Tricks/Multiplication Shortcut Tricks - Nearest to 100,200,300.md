---
version: 1
type: video
provider: YouTube
id: dShUkUp3_hQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a5f97dda-7edd-4084-9fcd-7ba50e8c52d9
updated: 1486069621
title: 'Multiplication Shortcut Tricks - Nearest to 100,200,300'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/dShUkUp3_hQ/default.jpg
    - https://i3.ytimg.com/vi/dShUkUp3_hQ/1.jpg
    - https://i3.ytimg.com/vi/dShUkUp3_hQ/2.jpg
    - https://i3.ytimg.com/vi/dShUkUp3_hQ/3.jpg
---
