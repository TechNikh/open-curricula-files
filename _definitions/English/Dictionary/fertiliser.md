---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fertiliser
offline_file: ""
offline_thumbnail: ""
uuid: aab02d7d-ae8c-4a28-a30e-e7f2b07bcd3e
updated: 1484310457
title: fertiliser
categories:
    - Dictionary
---
fertiliser
     n : any substance such as manure or a mixture of nitrates used
         to make soil more fertile [syn: {fertilizer}, {plant food}]
