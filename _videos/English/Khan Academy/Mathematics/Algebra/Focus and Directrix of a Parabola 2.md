---
version: 1
type: video
provider: YouTube
id: 1Jm9rREA-uA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Focus%20and%20Directrix%20of%20a%20Parabola%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c61f99e3-ab53-444e-be46-3d99c2736ba9
updated: 1486069571
title: Focus and Directrix of a Parabola 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/1Jm9rREA-uA/default.jpg
    - https://i3.ytimg.com/vi/1Jm9rREA-uA/1.jpg
    - https://i3.ytimg.com/vi/1Jm9rREA-uA/2.jpg
    - https://i3.ytimg.com/vi/1Jm9rREA-uA/3.jpg
---
Finding the focus and directrix of a parabola
More free lessons at: http://www.khanacademy.org/video?v=1Jm9rREA-uA
