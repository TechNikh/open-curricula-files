---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_curvature
offline_file: ""
offline_thumbnail: ""
uuid: 6dbc3767-8f7c-4099-8bb8-2369ded3a8f7
updated: 1484309202
title: Affine curvature
tags:
    - Formal definition
    - Special affine arclength
    - Special affine curvature
    - Affine curvature
    - Conics
    - Characterization up to affine congruence
    - Derivation of the curvature by affine invariance
    - Human Motor System
categories:
    - Differential geometry
---
Special affine curvature, also known as the equi-affine curvature or affine curvature, is a particular type of curvature that is defined on a plane curve that remains unchanged under a special affine transformation (an affine transformation that preserves area). The curves of constant equi-affine curvature k are precisely all non-singular plane conics. Those with k > 0 are ellipses, those with k = 0 are parabolas, and those with k < 0 are hyperbolas.
