---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/organized
offline_file: ""
offline_thumbnail: ""
uuid: f02f0b30-d78e-4e87-97b4-067320b46468
updated: 1484310277
title: organized
categories:
    - Dictionary
---
organized
     adj 1: formed into a structured or coherent whole; "organized
            religion"; "organized crime"; "an organized tour"
            [ant: {unorganized}]
     2: methodical and efficient in arrangement or function; "how
        well organized she is"; "his life was almost too
        organized" [ant: {disorganized}]
     3: being a member of or formed into a labor union; "organized
        labor"; "unionized workers"; "a unionized shop" [syn: {organised},
         {unionized}, {unionised}]
