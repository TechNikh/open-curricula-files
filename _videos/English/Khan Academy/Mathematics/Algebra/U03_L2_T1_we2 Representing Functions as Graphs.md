---
version: 1
type: video
provider: YouTube
id: 12w3qsF4xmE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/U03_L2_T1_we2%20Representing%20Functions%20as%20Graphs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fdf6540b-60da-4b13-a0dd-39963643a507
updated: 1486069561
title: U03_L2_T1_we2 Representing Functions as Graphs
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/12w3qsF4xmE/default.jpg
    - https://i3.ytimg.com/vi/12w3qsF4xmE/1.jpg
    - https://i3.ytimg.com/vi/12w3qsF4xmE/2.jpg
    - https://i3.ytimg.com/vi/12w3qsF4xmE/3.jpg
---
