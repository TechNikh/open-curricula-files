---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/organic
offline_file: ""
offline_thumbnail: ""
uuid: 7c9083f4-78c8-4be7-a621-aca6809b9eaf
updated: 1484310266
title: organic
categories:
    - Dictionary
---
organic
     adj 1: relating or belonging to the class of chemical compounds
            having a carbon basis; "hydrocarbons are organic
            compounds" [ant: {inorganic}]
     2: of or relating to or derived from living organisms; "organic
        soil"
     3: being or relating to or derived from or having properties
        characteristic of living organisms; "organic life";
        "organic growth"; "organic remains found in rock" [ant: {inorganic}]
     4: involving or affecting physiology or bodily organs; "an
        organic disease" [ant: {functional}]
     5: of or relating to foodstuff grown or raised without
        synthetic fertilizers or pesticides or hormones; ...
