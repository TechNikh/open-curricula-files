---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mao
offline_file: ""
offline_thumbnail: ""
uuid: d1dcc4cf-7cab-4b05-a007-f75d685456a8
updated: 1484310571
title: mao
categories:
    - Dictionary
---
MAO
     n 1: an enzyme that catalyzes the oxidation of many body
          compounds (e.g., epinephrine and norepinephrine and
          serotonin) [syn: {monoamine oxidase}]
     2: Chinese communist leader (1893-1976) [syn: {Mao Zedong}, {Mao
        Tsetung}]
