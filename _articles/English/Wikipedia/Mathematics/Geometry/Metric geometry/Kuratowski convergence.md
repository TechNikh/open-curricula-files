---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kuratowski_convergence
offline_file: ""
offline_thumbnail: ""
uuid: 664465ad-cbc3-485e-aaa4-0ed135215790
updated: 1484309269
title: Kuratowski convergence
tags:
    - Definitions
    - Properties
    - Related concepts
    - Examples
categories:
    - Metric geometry
---
In mathematics, Kuratowski convergence is a notion of convergence for sequences (or, more generally, nets) of compact subsets of metric spaces, named after Kazimierz Kuratowski. Intuitively, the Kuratowski limit of a sequence of sets is where the sets "accumulate".
