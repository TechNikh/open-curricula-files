---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nilmanifold
offline_file: ""
offline_thumbnail: ""
uuid: c8037de6-d538-4dc0-8a75-dc7bc743fd89
updated: 1484309247
title: Nilmanifold
tags:
    - Compact nilmanifolds
    - Complex nilmanifolds
    - Properties
    - Examples
    - Nilpotent Lie groups
    - Abelian Lie groups
    - Generalizations
categories:
    - Homogeneous spaces
---
In mathematics, a nilmanifold is a differentiable manifold which has a transitive nilpotent group of diffeomorphisms acting on it. As such, a nilmanifold is an example of a homogeneous space and is diffeomorphic to the quotient space 
  
    
      
        N
        
          /
        
        H
      
    
    {\displaystyle N/H}
  
, the quotient of a nilpotent Lie group N modulo a closed subgroup H. This notion was introduced by A. Mal'cev in 1951.
