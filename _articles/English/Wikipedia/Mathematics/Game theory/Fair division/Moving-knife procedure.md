---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moving-knife_procedure
offline_file: ""
offline_thumbnail: ""
uuid: b41d0c5d-afe4-45e3-8911-af55d6595187
updated: 1484308520
title: Moving-knife procedure
categories:
    - Fair division
---
In the mathematics of social science, and especially game theory, a moving-knife procedure is a type of solution to the fair division problem. The canonical example is the division of a cake using a knife.[1]
