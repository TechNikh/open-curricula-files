---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Orbital_stability
offline_file: ""
offline_thumbnail: ""
uuid: 49c38c0b-5520-4725-8593-1dae34a8a1b4
updated: 1484308832
title: Orbital stability
tags:
    - Formal definition
    - Example
categories:
    - Stability theory
---
In mathematical physics or theory of partial differential equations, the solitary wave solution of the form 
  
    
      
        u
        (
        x
        ,
        t
        )
        =
        
          e
          
            −
            i
            ω
            t
          
        
        ϕ
        (
        x
        )
        
      
    
    {\displaystyle u(x,t)=e^{-i\omega t}\phi (x)\,}
  
 is said to be orbitally stable if any solution with the initial data sufficiently close to 
  
    
      
        ϕ
        (
        x
        )
        
      
    
    {\displaystyle \phi (x)\,}
  
 forever remains in a given small neighborhood of the trajectory of 
  
  ...
