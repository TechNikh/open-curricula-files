---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ordinal_definable_set
offline_file: ""
offline_thumbnail: ""
uuid: df6d1133-ef43-4d3a-9895-4285c703ff96
updated: 1484309509
title: Ordinal definable set
categories:
    - Set theory
---
In mathematical set theory, a set S is said to be ordinal definable if, informally, it can be defined in terms of a finite number of ordinals by a first order formula. Ordinal definable sets were introduced by Gödel (1965).
