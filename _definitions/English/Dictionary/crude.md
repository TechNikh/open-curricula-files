---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crude
offline_file: ""
offline_thumbnail: ""
uuid: a544b712-2ace-4f47-a47a-16527c08b6fa
updated: 1484310409
title: crude
categories:
    - Dictionary
---
crude
     adj 1: not carefully or expertly made; "managed to make a crude
            splint"; "a crude cabin of logs with bark still on
            them"; "rough carpentry" [syn: {rough}]
     2: conspicuously and tastelessly indecent; "coarse language";
        "a crude joke"; "crude behavior"; "an earthy sense of
        humor"; "a revoltingly gross expletive"; "a vulgar
        gesture"; "full of language so vulgar it should have been
        edited" [syn: {coarse}, {earthy}, {gross}, {vulgar}]
     3: not refined or processed; "unrefined ore"; "crude oil" [syn:
         {unrefined}, {unprocessed}] [ant: {refined}]
     4: belonging to an early stage of technical development;
        ...
