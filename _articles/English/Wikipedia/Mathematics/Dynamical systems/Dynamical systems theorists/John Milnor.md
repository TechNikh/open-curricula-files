---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_Milnor
offline_file: ""
offline_thumbnail: ""
uuid: 02a55952-ee2a-4662-afd3-8bb56a064a65
updated: 1484308690
title: John Milnor
tags:
    - Early life and career
    - Research
    - Awards and honors
    - Publications
categories:
    - Dynamical systems theorists
---
John Willard Milnor (born February 20, 1931) is an American mathematician known for his work in differential topology, K-theory and dynamical systems. Milnor is a distinguished professor at Stony Brook University and one of the four mathematicians to have won the Fields Medal, the Wolf Prize, and the Abel Prize (along with Pierre Deligne, Jean-Pierre Serre, and John G. Thompson).
