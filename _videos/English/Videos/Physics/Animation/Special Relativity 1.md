---
version: 1
type: video
provider: YouTube
id: uNpOygVuGQs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5042778a-d236-4d3f-a5d4-3579814bbdf1
updated: 1486069649
title: Special Relativity 1
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/uNpOygVuGQs/default.jpg
    - https://i3.ytimg.com/vi/uNpOygVuGQs/1.jpg
    - https://i3.ytimg.com/vi/uNpOygVuGQs/2.jpg
    - https://i3.ytimg.com/vi/uNpOygVuGQs/3.jpg
---
