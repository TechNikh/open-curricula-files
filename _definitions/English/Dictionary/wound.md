---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wound
offline_file: ""
offline_thumbnail: ""
uuid: 7eb1c66b-4c00-4db1-a28c-f8c3837086bc
updated: 1484310315
title: wound
categories:
    - Dictionary
---
wind
     n 1: air moving (sometimes with considerable force) from an area
          of high pressure to an area of low pressure; "trees bent
          under the fierce winds"; "when there is no wind, row";
          "the radioactivity was being swept upwards by the air
          current and out into the atmosphere" [syn: {air current},
           {current of air}]
     2: a tendency or force that influences events; "the winds of
        change"
     3: breath; "the collision knocked the wind out of him"
     4: empty rhetoric or insincere or exaggerated talk; "that's a
        lot of wind"; "don't give me any of that jazz" [syn: {idle
        words}, {jazz}, {nothingness}]
     5: an ...
