---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compost
offline_file: ""
offline_thumbnail: ""
uuid: bb7e85fa-92b3-4554-84fc-7c748a42352d
updated: 1484310379
title: compost
categories:
    - Dictionary
---
compost
     n : a mixture of decaying vegetation and manure; used as a
         fertilizer
     v : convert to compost; "compost organic debris"
