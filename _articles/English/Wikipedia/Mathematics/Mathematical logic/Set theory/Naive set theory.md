---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Naive_set_theory
offline_file: ""
offline_thumbnail: ""
uuid: 0d026887-40fa-44e5-b6bf-532df8fd35fc
updated: 1484309507
title: Naive set theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Passage_with_the_set_definition_of_Georg_Cantor.png
tags:
    - Method
    - Paradoxes
    - "Cantor's theory"
    - Axiomatic theories
    - Consistency
    - Utility
    - Sets, membership and equality
    - Note on consistency
    - Membership
    - Equality
    - Empty set
    - Specifying sets
    - Subsets
    - Universal sets and absolute complements
    - Unions, intersections, and relative complements
    - Ordered pairs and Cartesian products
    - Some important sets
    - Paradoxes in early set theory
    - Notes
categories:
    - Set theory
---
Naive set theory is one of several theories of sets used in the discussion of the foundations of mathematics.[1] Unlike axiomatic set theories, which are defined using a formal logic, naive set theory is defined informally, in natural language. It describes the aspects of mathematical sets familiar in discrete mathematics (for example Venn diagrams and symbolic reasoning about their Boolean algebra), and suffices for the everyday usage of set theory concepts in contemporary mathematics.[2]
