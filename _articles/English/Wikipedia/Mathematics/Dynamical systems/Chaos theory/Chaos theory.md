---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaos_theory
offline_file: ""
offline_thumbnail: ""
uuid: a2e69d82-4fed-4c90-94aa-20db84aab397
updated: 1484308713
title: Chaos theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lorenz_attractor_yb.svg_0.png
tags:
    - Introduction
    - Chaotic dynamics
    - Sensitivity to initial conditions
    - Topological mixing
    - Density of periodic orbits
    - Strange attractors
    - Minimum complexity of a chaotic system
    - Jerk systems
    - Exactly-solvable chaotic systems
    - Spontaneous order
    - History
    - Distinguishing random from chaotic data
    - Applications
    - Computer science
    - Biology
    - Other areas
    - Scientific literature
    - Articles
    - Textbooks
    - Semitechnical and popular works
categories:
    - Chaos theory
---
Chaos theory is the field of study in mathematics that studies the behavior of dynamical systems that are highly sensitive to initial conditions—a response popularly referred to as the butterfly effect.[1] Small differences in initial conditions (such as those due to rounding errors in numerical computation) yield widely diverging outcomes for such dynamical systems, rendering long-term prediction impossible in general.[2] This happens even though these systems are deterministic, meaning that their future behavior is fully determined by their initial conditions, with no random elements involved.[3] In other words, the deterministic nature of these systems does not make them ...
