---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Instant_Insanity
offline_file: ""
offline_thumbnail: ""
uuid: 5be078df-fe90-49f5-9c1b-98ddf5be0afd
updated: 1484309392
title: Instant Insanity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-InstantInsanity.jpg
categories:
    - Computational problems in graph theory
---
The "Instant Insanity" puzzle consists of four cubes with faces colored with four colors (commonly red, blue, green, and white). The objective of the puzzle is to stack these cubes in a column so that each side (front, back, left, and right) of the stack shows each of the four colors. The distribution of colors on each cube is unique.
