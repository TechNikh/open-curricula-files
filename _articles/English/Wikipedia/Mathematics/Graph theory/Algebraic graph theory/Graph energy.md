---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_energy
offline_file: ""
offline_thumbnail: ""
uuid: e4f41e4a-6a8e-4e36-a778-2773458db6df
updated: 1484309373
title: Graph energy
categories:
    - Algebraic graph theory
---
In mathematics, the energy of a graph is the sum of the absolute values of the eigenvalues of the adjacency matrix of the graph. This quantity is studied in the context of spectral graph theory.
