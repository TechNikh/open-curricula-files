---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Planimeter
offline_file: ""
offline_thumbnail: ""
uuid: 012f78e2-a76a-4ec5-8126-87319bd4d406
updated: 1484309332
title: Planimeter
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Planimeter.png
tags:
    - Construction
    - Principle of the linear planimeter
    - Mathematical Derivation
    - Polar coordinates
categories:
    - Technical drawing
---
