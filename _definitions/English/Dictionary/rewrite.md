---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rewrite
offline_file: ""
offline_thumbnail: ""
uuid: e57614f4-e9aa-4ad2-af83-6a92d905f24d
updated: 1484310186
title: rewrite
categories:
    - Dictionary
---
rewrite
     n : something that has been written again; "the rewrite was much
         better" [syn: {revision}, {rescript}]
     v 1: write differently; alter the writing of; "The student
          rewrote his thesis"
     2: rewrite so as to make fit to suit a new or different
        purpose; "re-write a play for use in schools"
     [also: {rewrote}, {rewritten}]
