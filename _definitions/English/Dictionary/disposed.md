---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disposed
offline_file: ""
offline_thumbnail: ""
uuid: 5366e9ad-2af9-4067-80cf-542e0a4f6a1e
updated: 1484310480
title: disposed
categories:
    - Dictionary
---
disposed
     adj 1: having made preparations; "prepared to take risks" [syn: {disposed(p)},
             {fain}, {inclined(p)}, {prepared}]
     2: (usually followed by `to') naturally disposed toward; "he is
        apt to ignore matters he considers unimportant"; "I am not
        minded to answer any questions" [syn: {apt(p)}, {disposed(p)},
         {given(p)}, {minded(p)}, {tending(p)}]
