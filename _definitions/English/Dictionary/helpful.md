---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/helpful
offline_file: ""
offline_thumbnail: ""
uuid: f958c7d2-2855-4cef-b11c-1785ead7ab41
updated: 1484310319
title: helpful
categories:
    - Dictionary
---
helpful
     adj 1: providing assistance or serving a useful function [ant: {unhelpful}]
     2: of service or assistance; "a child who is helpful around the
        house can save the mother many steps"
     3: showing a willingness to cooperate; "a helpful cooperative
        patient"; "parents hope to raise children who are
        considerate and helpful to others"
