---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dec
offline_file: ""
offline_thumbnail: ""
uuid: 41cb214d-b0ba-4186-af48-b26fe5f2c970
updated: 1484310421
title: dec
categories:
    - Dictionary
---
Dec
     n 1: the last (12th) month of the year [syn: {December}]
     2: (astronomy) the angular distance to a point on a celestial
        object measured north or south from the celestial equator;
        expressed in degrees; used with right ascension to specify
        positions on the celestial sphere [syn: {declination}, {celestial
        latitude}]
