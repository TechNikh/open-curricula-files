---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/war
offline_file: ""
offline_thumbnail: ""
uuid: c5f272e3-d248-4e4b-9569-2220d9f4cdda
updated: 1484310383
title: war
categories:
    - Dictionary
---
war
     n 1: the waging of armed conflict against an enemy; "thousands of
          people were killed in the war" [syn: {warfare}]
     2: a legal state created by a declaration of war and ended by
        official declaration during which the international rules
        of war apply; "war was declared in November but actual
        fighting did not begin until the following spring" [syn: {state
        of war}] [ant: {peace}]
     3: an active struggle between competing entities; "a price
        war"; "a war of wits"; "diplomatic warfare" [syn: {warfare}]
     4: a concerted campaign to end something that is injurious;
        "the war on poverty"; "the war against crime"
     v : make ...
