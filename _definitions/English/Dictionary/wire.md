---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wire
offline_file: ""
offline_thumbnail: ""
uuid: 93bab3cc-c642-4e43-98a8-a221b5f735be
updated: 1484310204
title: wire
categories:
    - Dictionary
---
wire
     n 1: ligament made of metal and used to fasten things or make
          cages or fences etc
     2: a metal conductor that carries electricity over a distance
        [syn: {conducting wire}]
     3: the finishing line on a racetrack
     4: a message transmitted by telegraph [syn: {telegram}]
     v 1: provide with electrical circuits; "wire the addition to the
          house"
     2: send cables, wires, or telegrams [syn: {cable}, {telegraph}]
     3: fasten with wire; "The columns were wired to the beams for
        support" [ant: {unwire}]
     4: string on a wire; "wire beads"
     5: equip for use with electricity; "electrify an appliance"
        [syn: {electrify}]
