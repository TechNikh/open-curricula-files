---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%CE%95-net_(computational_geometry)'
offline_file: ""
offline_thumbnail: ""
uuid: cc11ea27-1043-455f-8d42-0f3ae1499ff8
updated: 1484309170
title: Ε-net (computational geometry)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Unit_square_%25C9%259B-net.svg.png'
categories:
    - Computational geometry
---
An ε-net (pronounced epsilon-net) is any of several related concepts in mathematics, and in particular in computational geometry, where it relates to the approximation of a general set by a collection of simpler subsets. It has a particular meaning in probability theory where it is used to describe the approximation of one probability distribution by another.
