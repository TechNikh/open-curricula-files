---
version: 1
type: video
provider: YouTube
id: d2WythLLYqo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 88380d69-4ad8-47bb-83ca-b208d4312312
updated: 1486069618
title: Similarity postulates
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/d2WythLLYqo/default.jpg
    - https://i3.ytimg.com/vi/d2WythLLYqo/1.jpg
    - https://i3.ytimg.com/vi/d2WythLLYqo/2.jpg
    - https://i3.ytimg.com/vi/d2WythLLYqo/3.jpg
---
