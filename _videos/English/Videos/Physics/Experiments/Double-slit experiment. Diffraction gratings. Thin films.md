---
version: 1
type: video
provider: YouTube
id: IP2THfy4JpE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 26437bb3-dec3-4d87-84de-13e9808e5d66
updated: 1486069636
title: Double-slit experiment. Diffraction gratings. Thin films
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/IP2THfy4JpE/default.jpg
    - https://i3.ytimg.com/vi/IP2THfy4JpE/1.jpg
    - https://i3.ytimg.com/vi/IP2THfy4JpE/2.jpg
    - https://i3.ytimg.com/vi/IP2THfy4JpE/3.jpg
---
