---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/task
offline_file: ""
offline_thumbnail: ""
uuid: ef930b37-8bb2-4fa6-9d2d-b67ba7ad1bc9
updated: 1484310221
title: task
categories:
    - Dictionary
---
task
     n 1: any piece of work that is undertaken or attempted; "he
          prepared for great undertakings" [syn: {undertaking}, {project},
           {labor}]
     2: a specific piece of work required to be done as a duty or
        for a specific fee; "estimates of the city's loss on that
        job ranged as high as a million dollars"; "the job of
        repairing the engine took several hours"; "the endless
        task of classifying the samples"; "the farmer's morning
        chores" [syn: {job}, {chore}]
     v 1: assign a task to; "I tasked him with looking after the
          children"
     2: use to the limit; "you are taxing my patience" [syn: {tax}]
