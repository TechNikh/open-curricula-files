---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disaster
offline_file: ""
offline_thumbnail: ""
uuid: 2539af7b-3302-4ef0-8138-25fe914ea1c7
updated: 1484310533
title: disaster
categories:
    - Dictionary
---
disaster
     n 1: a state of extreme (usually irremediable) ruin and
          misfortune; "lack of funds has resulted in a catastrophe
          for our school system"; "his policies were a disaster"
          [syn: {catastrophe}]
     2: an event resulting in great loss and misfortune; "the whole
        city was affected by the irremediable calamity"; "the
        earthquake was a disaster" [syn: {calamity}, {catastrophe},
         {tragedy}, {cataclysm}]
     3: an act that has disastrous consequences
