---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/epitaph
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441761
title: epitaph
categories:
    - Dictionary
---
epitaph
     n 1: an inscription on a tombstone or monument in memory of the
          person buried there
     2: a summary statement of commemoration for a dead person
