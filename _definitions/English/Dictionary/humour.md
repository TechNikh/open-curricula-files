---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humour
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484443981
title: humour
categories:
    - Dictionary
---
humour
     n 1: a characteristic (habitual or relatively temporary) state of
          feeling; "whether he praised or cursed me depended on
          his temper at the time"; "he was in a bad humor" [syn: {temper},
           {mood}, {humor}]
     2: a message whose ingenuity or verbal skill or incongruity has
        the power to evoke laughter [syn: {wit}, {humor}, {witticism},
         {wittiness}]
     3: (Middle Ages) one of the four fluids in the body whose
        balance was believed to determine your emotional and
        physical state; "the humors are blood and phlegm and
        yellow and black bile" [syn: {humor}]
     4: the liquid parts of the body [syn: {liquid body ...
