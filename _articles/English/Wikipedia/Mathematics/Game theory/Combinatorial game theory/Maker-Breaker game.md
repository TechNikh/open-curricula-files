---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maker-Breaker_game
offline_file: ""
offline_thumbnail: ""
uuid: 8009c4e9-704f-4d09-9ded-108fa3b05e54
updated: 1484308533
title: Maker-Breaker game
categories:
    - Combinatorial game theory
---
It is a two-person game with complete information played on a hypergraph (V,H) where V is an arbitrary set (called the board of the game) and H is a family of subsets of V, called the winning sets. The two players alternately occupy previously unoccupied elements of V.
