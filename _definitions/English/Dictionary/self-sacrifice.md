---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-sacrifice
offline_file: ""
offline_thumbnail: ""
uuid: b1b208c2-9d1f-48c9-86e3-e1feb32c5702
updated: 1484310563
title: self-sacrifice
categories:
    - Dictionary
---
self-sacrifice
     n : acting with less concern for yourself than for the success
         of the joint activity [syn: {selflessness}]
