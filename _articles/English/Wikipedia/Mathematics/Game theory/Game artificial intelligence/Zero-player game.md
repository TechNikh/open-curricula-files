---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zero-player_game
offline_file: ""
offline_thumbnail: ""
uuid: 8abc7fb2-6daf-4b7f-a22e-ebdbdaa8b4a0
updated: 1484309006
title: Zero-player game
categories:
    - Game artificial intelligence
---
In computer games, the term refers to programs that use artificial intelligence rather than human players.[1]
