---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pedestal
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484381041
title: pedestal
categories:
    - Dictionary
---
pedestal
     n 1: a support or foundation; "the base of the lamp" [syn: {base},
           {stand}]
     2: a position of great esteem (and supposed superiority); "they
        put him on a pedestal"
     3: an architectural support or base (as for a column or statue)
        [syn: {plinth}, {footstall}]
