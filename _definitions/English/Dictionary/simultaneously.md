---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/simultaneously
offline_file: ""
offline_thumbnail: ""
uuid: 85314591-5fa9-4ad3-9ba9-5d1f7bc66464
updated: 1484310333
title: simultaneously
categories:
    - Dictionary
---
simultaneously
     adv : at the same instant; "they spoke simultaneously" [syn: {at
           the same time}]
