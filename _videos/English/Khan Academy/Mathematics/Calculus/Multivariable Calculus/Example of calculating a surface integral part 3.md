---
version: 1
type: video
provider: YouTube
id: s2_NTiISZl4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Example%20of%20calculating%20a%20surface%20integral%20part%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d505e463-c0f8-49af-bc0c-d170a681fbcc
updated: 1486069692
title: Example of calculating a surface integral part 3
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/s2_NTiISZl4/default.jpg
    - https://i3.ytimg.com/vi/s2_NTiISZl4/1.jpg
    - https://i3.ytimg.com/vi/s2_NTiISZl4/2.jpg
    - https://i3.ytimg.com/vi/s2_NTiISZl4/3.jpg
---
