---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Resource_slack
offline_file: ""
offline_thumbnail: ""
uuid: f78a1836-e4b6-48ec-805c-bec0562b94b2
updated: 1484309069
title: Resource slack
categories:
    - Strategy
---
Resource slack, in the business and management literature, is the level of availability of a resource. Resource slack can be considered as the opposite of resource scarcity or resource constraints.
