---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pompeiu_problem
offline_file: ""
offline_thumbnail: ""
uuid: 0dcd11c6-a739-46b2-a70a-1fd96f11de03
updated: 1484309255
title: Pompeiu problem
categories:
    - Integral geometry
---
In mathematics, the Pompeiu problem is a conjecture in integral geometry, named for Dimitrie Pompeiu, who posed the problem in 1929, as follows. Suppose f is a nonzero continuous function defined on a Euclidean space, and K is a simply connected Lipschitz domain, so that the integral of f vanishes on every congruent copy of K. Then the domain is a ball.
