---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meek
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484565961
title: meek
categories:
    - Dictionary
---
meek
     adj 1: humble in spirit or manner; suggesting retiring mildness or
            even cowed submissiveness; "meek and self-effacing"
            [syn: {mild}, {modest}]
     2: very docile; "tame obedience"; "meek as a mouse"- Langston
        Hughes [syn: {tame}]
     3: evidencing little spirit or courage; overly submissive or
        compliant; "compliant and anxious to suit his opinions of
        those of others"; "a fine fiery blast against meek
        conformity"- Orville Prescott; "she looked meek but had
        the heart of a lion"; "was submissive and subservient"
        [syn: {compliant}, {spiritless}]
