---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vitalism
offline_file: ""
offline_thumbnail: ""
uuid: 420058e6-5c4d-4a9b-9b7c-593efbca6c4f
updated: 1484310416
title: vitalism
categories:
    - Dictionary
---
vitalism
     n : (philosophy) a doctrine that life is a vital principle
         distinct from physics and chemistry
