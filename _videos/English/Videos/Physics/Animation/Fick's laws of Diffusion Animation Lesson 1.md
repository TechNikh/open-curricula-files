---
version: 1
type: video
provider: YouTube
id: nTrI5IRfBPE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b90b34ef-c700-4c02-bc98-63963ba60ce6
updated: 1486069653
title: "Fick's laws of Diffusion Animation Lesson 1"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/nTrI5IRfBPE/default.jpg
    - https://i3.ytimg.com/vi/nTrI5IRfBPE/1.jpg
    - https://i3.ytimg.com/vi/nTrI5IRfBPE/2.jpg
    - https://i3.ytimg.com/vi/nTrI5IRfBPE/3.jpg
---
