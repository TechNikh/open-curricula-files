---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chief
offline_file: ""
offline_thumbnail: ""
uuid: 0a8fdf61-da04-4692-8bd1-90e47c6de86b
updated: 1484310236
title: chief
categories:
    - Dictionary
---
chief
     adj : most important element; "the chief aim of living"; "the main
           doors were of solid glass"; "the principal rivers of
           America"; "the principal example"; "policemen were
           primary targets" [syn: {chief(a)}, {main(a)}, {primary(a)},
            {principal(a)}]
     n 1: a person who is in charge; "the head of the whole operation"
          [syn: {head}, {top dog}]
     2: a person who exercises control over workers; "if you want to
        leave early you have to ask the foreman" [syn: {foreman},
        {gaffer}, {honcho}, {boss}]
