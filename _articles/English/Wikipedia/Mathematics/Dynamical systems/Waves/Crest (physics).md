---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crest_(physics)
offline_file: ""
offline_thumbnail: ""
uuid: 69d613e2-112a-44ca-9b10-99770f851236
updated: 1484308885
title: Crest (physics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Crest_trough.svg.png
tags:
    - Interference
    - Notes
categories:
    - Waves
---
A crest is the point on a wave with the maximum value or upward displacement within a cycle. A crest is a point on the wave where the displacement of the medium is at a maximum. A trough is the opposite of a crest, so the minimum or lowest point in a cycle. A point on the wave is a trough if the displacement of the medium at that point is at a minimum[1]
