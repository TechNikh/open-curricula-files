---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kronheimer%E2%80%93Mrowka_basic_class'
offline_file: ""
offline_thumbnail: ""
uuid: d4325536-7a6e-4028-a5c5-b49fca1b8edc
updated: 1484309226
title: Kronheimer–Mrowka basic class
categories:
    - Differential geometry
---
In mathematics, the Kronheimer–Mrowka basic classes are elements of the second cohomology H2(X) of a simple smooth 4-manifold X that determine its Donaldson polynomials. They were introduced by P. B. Kronheimer and Tomasz S. Mrowka (1994, 1995).
