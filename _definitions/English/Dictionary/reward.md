---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reward
offline_file: ""
offline_thumbnail: ""
uuid: 9ea4fcea-6f52-4bbf-9c93-3e017cdf523c
updated: 1484310146
title: reward
categories:
    - Dictionary
---
reward
     n 1: a recompense for worthy acts or retribution for wrongdoing;
          "the wages of sin is death"; "virtue is its own reward"
          [syn: {wages}, {payoff}]
     2: payment made in return for a service rendered
     3: an act performed to strengthen approved behavior [syn: {reinforcement}]
     4: the offer of money for helping to find a criminal or for
        returning lost property
     5: benefit resulting from some event or action; "it turned out
        to my advantage"; "reaping the rewards of generosity"
        [syn: {advantage}] [ant: {penalty}]
     v 1: bestow honor or rewards upon; "Today we honor our soldiers";
          "The scout was rewarded for ...
