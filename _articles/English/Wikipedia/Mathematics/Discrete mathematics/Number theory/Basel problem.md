---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Basel_problem
offline_file: ""
offline_thumbnail: ""
uuid: 6c48155a-ec93-43bf-bbdb-326693bfc812
updated: 1484308629
title: Basel problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Limit_circle_FbN.jpeg
tags:
    - "Euler's approach"
    - The Riemann zeta function
    - A rigorous proof using Fourier series
    - A rigorous elementary proof
    - History of this proof
    - The proof
    - Notes
categories:
    - Number theory
---
The Basel problem is a problem in mathematical analysis with relevance to number theory, first posed by Pietro Mengoli in 1644 and solved by Leonhard Euler in 1734[1] and read on 5 December 1735 in The Saint Petersburg Academy of Sciences (Russian: Петербургская Академия наук).[2] Since the problem had withstood the attacks of the leading mathematicians of the day, Euler's solution brought him immediate fame when he was twenty-eight. Euler generalised the problem considerably, and his ideas were taken up years later by Bernhard Riemann in his seminal 1859 paper On the Number of Primes Less Than a Given Magnitude, in which he defined his zeta function and proved its ...
