---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faraway
offline_file: ""
offline_thumbnail: ""
uuid: 76e440c1-8d88-4a23-b475-45906e23b697
updated: 1484310522
title: faraway
categories:
    - Dictionary
---
faraway
     adj 1: very far away in space or time; "faraway mountains"; "the
            faraway future"; "troops landing on far-off shores";
            "far-off happier times" [syn: {far-off}]
     2: far removed mentally; "a faraway (or distant) look in her
        eyes"
