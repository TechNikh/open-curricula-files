---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blue
offline_file: ""
offline_thumbnail: ""
uuid: 988b2426-2c02-434b-995d-89ab949f40f7
updated: 1484310339
title: blue
categories:
    - Dictionary
---
blue
     adj 1: having a color similar to that of a clear unclouded sky;
            "October's bright blue weather"- Helen Hunt Jackson;
            "a blue flame"; "blue haze of tobacco smoke" [syn: {bluish},
             {blueish}, {light-blue}, {dark-blue}, {blue-black}]
     2: used to signify the Union forces in the Civil War (who wore
        blue uniforms); "a ragged blue line"
     3: low in spirits; "lonely and blue in a strange city";
        "depressed by the loss of his job"; "a dispirited and
        resigned expression on her face"; "downcast after his
        defeat"; "feeling discouraged and downhearted" [syn: {depressed},
         {dispirited}, {down(p)}, {downcast}, ...
