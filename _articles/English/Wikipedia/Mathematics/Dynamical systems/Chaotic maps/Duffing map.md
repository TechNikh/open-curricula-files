---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Duffing_map
offline_file: ""
offline_thumbnail: ""
uuid: 0bbcb458-322a-4afb-958e-8b9688add552
updated: 1484308728
title: Duffing map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-DuffingMap.png
categories:
    - Chaotic maps
---
The Duffing map (also called as 'Holmes map') is a discrete-time dynamical system. It is an example of a dynamical system that exhibits chaotic behavior. The Duffing map takes a point (xn, yn) in the plane and maps it to a new point given by
