---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/account
offline_file: ""
offline_thumbnail: ""
uuid: 94905a28-7469-4688-ba8a-9e9796e70142
updated: 1484310309
title: account
categories:
    - Dictionary
---
account
     n 1: a formal contractual relationship established to provide for
          regular banking or brokerage or business services; "he
          asked to see the executive who handled his account"
          [syn: {business relationship}]
     2: the act of informing by verbal report; "he heard reports
        that they were causing trouble"; "by all accounts they
        were a happy couple" [syn: {report}]
     3: a record or narrative description of past events; "a history
        of France"; "he gave an inaccurate account of the plot to
        kill the president"; "the story of exposure to lead" [syn:
         {history}, {chronicle}, {story}]
     4: a short account of the ...
