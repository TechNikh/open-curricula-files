---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frobenius_splitting
offline_file: ""
offline_thumbnail: ""
uuid: 75b4cd43-1599-46d2-a55d-a1d81f2379fc
updated: 1484309147
title: Frobenius splitting
categories:
    - Algebraic geometry
---
In mathematics, a Frobenius splitting, introduced by Mehta and Ramanthan (1985), is a splitting of the injective morphism OX→F*OX from a structure sheaf OX of a characteristic p > 0 variety X to its image F*OX under the Frobenius endomorphism F*.
