---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dollar_auction
offline_file: ""
offline_thumbnail: ""
uuid: dba394eb-e410-452f-a1d0-c7b7be183a06
updated: 1484309052
title: Dollar auction
tags:
    - Play
    - Analysis
    - Notes
categories:
    - Non-cooperative games
---
The dollar auction is a non-zero sum sequential game designed by economist Martin Shubik to illustrate a paradox brought about by traditional rational choice theory in which players with perfect information in the game are compelled to make an ultimately irrational decision based completely on a sequence of apparently rational choices made throughout the game.[1]
