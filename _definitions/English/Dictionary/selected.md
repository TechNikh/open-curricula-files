---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/selected
offline_file: ""
offline_thumbnail: ""
uuid: 5021c4bf-bfb3-466a-bf58-2022a8aa8b39
updated: 1484310301
title: selected
categories:
    - Dictionary
---
selected
     adj : chosen in preference to another [ant: {unselected}]
