---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breakdown
offline_file: ""
offline_thumbnail: ""
uuid: 7bda1f43-d834-4d1b-83fa-698023b9660a
updated: 1484310361
title: breakdown
categories:
    - Dictionary
---
break down
     v 1: make a mathematical, chemical, or grammatical analysis of;
          break down into components or essential features;
          "analyze a specimen"; "analyze a sentence"; "analyze a
          chemical compound" [syn: {analyze}, {analyse}, {dissect},
           {take apart}] [ant: {synthesize}]
     2: make ineffective; "Martin Luther King tried to break down
        racial discrimination" [syn: {crush}]
     3: lose control of one's emotions; "When she heard that she had
        not passed the exam, she lost it completely"; "When her
        baby died, she snapped" [syn: {lose it}, {snap}]
     4: stop operating or functioning; "The engine finally went";
        "The ...
