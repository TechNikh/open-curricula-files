---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cm
offline_file: ""
offline_thumbnail: ""
uuid: 71366206-2754-41ac-9400-7886fab8bd72
updated: 1484310328
title: cm
categories:
    - Dictionary
---
cm
     n 1: a metric unit of length equal to one hundredth of a meter
          [syn: {centimeter}, {centimetre}]
     2: a radioactive transuranic metallic element; produced by
        bombarding plutonium with helium nuclei [syn: {curium}, {atomic
        number 96}]
