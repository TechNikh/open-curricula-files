---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Path-based_strong_component_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: 22cc5989-0e99-4087-a3ce-fad837fef873
updated: 1484309400
title: Path-based strong component algorithm
tags:
    - Description
    - Related algorithms
    - Notes
categories:
    - Graph connectivity
---
In graph theory, the strongly connected components of a directed graph may be found using an algorithm that uses depth-first search in combination with two stacks, one to keep track of the vertices in the current component and the second to keep track of the current search path.[1] Versions of this algorithm have been proposed by Purdom (1970), Munro (1971), Dijkstra (1976), Cheriyan & Mehlhorn (1996), and Gabow (2000); of these, Dijkstra's version was the first to achieve linear time.[2]
