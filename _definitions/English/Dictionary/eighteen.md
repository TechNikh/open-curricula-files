---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eighteen
offline_file: ""
offline_thumbnail: ""
uuid: cd917fbc-318b-479b-bbc8-a11d5ac23012
updated: 1484310399
title: eighteen
categories:
    - Dictionary
---
eighteen
     adj : being one more than seventeen [syn: {18}, {xviii}]
     n : the cardinal number that is the sum of seventeen and one
         [syn: {18}, {XVIII}]
