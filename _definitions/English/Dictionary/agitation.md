---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agitation
offline_file: ""
offline_thumbnail: ""
uuid: 269bb327-e853-469b-aef5-9ec12d7f7a66
updated: 1484310234
title: agitation
categories:
    - Dictionary
---
agitation
     n 1: a mental state of extreme emotional disturbance
     2: a state of agitation or turbulent change or development;
        "the political ferment produced a new leadership"; "social
        unrest" [syn: {ferment}, {fermentation}, {unrest}]
     3: the feeling of being agitated; not calm [ant: {calmness}]
     4: disturbance usually in protest [syn: {excitement}, {turmoil},
         {upheaval}, {hullabaloo}]
     5: the act of agitating something; causing it to move around
        (usually vigorously)
