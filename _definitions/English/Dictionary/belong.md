---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/belong
offline_file: ""
offline_thumbnail: ""
uuid: 0bfe7e9e-154c-4023-8e43-a3069f74cba9
updated: 1484310266
title: belong
categories:
    - Dictionary
---
belong
     v 1: be owned by; be in the possession of; "This book belongs to
          me"
     2: originate (in); "The problems dwell in the social injustices
        in this country" [syn: {dwell}, {consist}, {lie}, {lie in}]
     3: be suitable or acceptable; "This student somehow doesn't
        belong"
     4: be in the right place or situation; "Where do these books
        belong?"; "Let's put health care where it belongs--under
        the control of the government"; "Where do these books go?"
        [syn: {go}]
     5: be classified with; "The whales belong among the mammals"
