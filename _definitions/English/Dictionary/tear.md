---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tear
offline_file: ""
offline_thumbnail: ""
uuid: 9ecd65eb-caad-42eb-8ded-cbafc5564a3a
updated: 1484310146
title: tear
categories:
    - Dictionary
---
tear
     n 1: a drop of the clear salty saline solution secreted by the
          lacrimal glands; "his story brought tears to her eyes"
          [syn: {teardrop}]
     2: an opening made forcibly as by pulling apart; "there was a
        rip in his pants"; "she had snags in her stockings" [syn:
        {rip}, {rent}, {snag}, {split}]
     3: an occasion for excessive eating or drinking; "they went on
        a bust that lasted three days" [syn: {bust}, {binge}, {bout}]
     4: the act of tearing; "he took the manuscript in both hands
        and gave it a mighty tear"
     v 1: separate or cause to separate abruptly; "The rope snapped";
          "tear the paper" [syn: {rupture}, {snap}, ...
