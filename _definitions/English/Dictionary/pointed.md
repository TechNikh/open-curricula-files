---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pointed
offline_file: ""
offline_thumbnail: ""
uuid: 523a7e4d-fe64-4ac9-a79c-d419f5f514eb
updated: 1484310343
title: pointed
categories:
    - Dictionary
---
pointed
     adj 1: having a point [ant: {pointless}]
     2: direct and obvious in meaning or reference; often
        unpleasant; "a pointed critique"; "a pointed allusion to
        what was going on"; "another pointed look in their
        direction"
