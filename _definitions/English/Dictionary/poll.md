---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poll
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484537881
title: poll
categories:
    - Dictionary
---
poll
     n 1: an inquiry into public opinion conducted by interviewing a
          random sample of people [syn: {opinion poll}, {public
          opinion poll}, {canvass}]
     2: the top of the head [syn: {pate}, {crown}]
     3: the part of the head between the ears
     4: a tame parrot [syn: {poll parrot}]
     5: the counting of votes (as in an election)
     v 1: get the opinions (of people) by asking specific questions
          [syn: {canvass}, {canvas}]
     2: vote in an election at a polling station
     3: get the votes of
     4: convert into a pollard; "pollard trees" [syn: {pollard}]
