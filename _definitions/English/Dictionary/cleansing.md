---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cleansing
offline_file: ""
offline_thumbnail: ""
uuid: 35b2ff1c-d919-4fe5-8b03-9bc48638baa8
updated: 1484310426
title: cleansing
categories:
    - Dictionary
---
cleansing
     adj 1: cleansing the body by washing; especially ritual washing of
            e.g. hands; "ablutionary rituals" [syn: {ablutionary}]
     2: acting like an antiseptic [syn: {purifying}]
     n : the act of making something clean; "he gave his shoes a good
         cleaning" [syn: {cleaning}, {cleanup}]
