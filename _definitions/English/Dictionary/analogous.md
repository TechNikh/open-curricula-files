---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/analogous
offline_file: ""
offline_thumbnail: ""
uuid: 47c02542-ec2b-4726-8f79-5dae50d3b071
updated: 1484310289
title: analogous
categories:
    - Dictionary
---
analogous
     adj 1: similar or correspondent in some respects though otherwise
            dissimilar; "brains and computers are often considered
            analogous"; "surimi is marketed as analogous to
            crabmeat" [syn: {correspondent}]
     2: corresponding in function but not in evolutionary origin;
        "the wings of a bee and those of a hummingbird are
        analogous" [ant: {homologous}, {heterologous}]
