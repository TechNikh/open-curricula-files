---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Diophantine_graph'
offline_file: ""
offline_thumbnail: ""
uuid: 4ec8196e-b467-4ec3-9fba-cdcdb2326528
updated: 1484308595
title: Erdős–Diophantine graph
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-5-point_Erd%25C5%2591s-Diophantine_graph.svg.png'
categories:
    - Discrete geometry
---
An Erdős–Diophantine graph is an object in the mathematical subject of Diophantine equations consisting of a set of integer points at integer distances in the plane that cannot be extended by any additional points. Equivalently, it can be described as a complete graph with vertices located on the integer square grid 
  
    
      
        
          
            
              Z
            
            
              2
            
          
        
      
    
    {\displaystyle \scriptstyle \mathbb {Z} ^{2}}
  
 such that all mutual distances between the vertices are integers, while all other grid points have a non-integer distance to at least one vertex.
