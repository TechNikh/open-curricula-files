---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phenomenal
offline_file: ""
offline_thumbnail: ""
uuid: 5059c4f4-fb49-4401-bdf1-4183730aa6ec
updated: 1484310148
title: phenomenal
categories:
    - Dictionary
---
phenomenal
     adj 1: of or relating to a phenomenon; "phenomenal science"
     2: exceedingly or unbelievably great; "the bomb did fantastic
        damage"; "Samson is supposed to have had fantastic
        strength"; "phenomenal feats of memory" [syn: {fantastic}]
