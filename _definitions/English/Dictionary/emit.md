---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emit
offline_file: ""
offline_thumbnail: ""
uuid: 3ead625b-8414-4453-b444-1d051863ffc4
updated: 1484310389
title: emit
categories:
    - Dictionary
---
emit
     v 1: expel (gases or odors) [syn: {breathe}, {pass off}]
     2: give off, send forth, or discharge; as of light, heat, or
        radiation, vapor, etc.; "The ozone layer blocks some
        harmful rays which the sun emits" [syn: {give out}, {give
        off}] [ant: {absorb}]
     3: express audibly; utter sounds (not necessarily words); "She
        let out a big heavy sigh"; "He uttered strange sounds that
        nobody could understand" [syn: {utter}, {let out}, {let
        loose}]
     [also: {emitting}, {emitted}]
