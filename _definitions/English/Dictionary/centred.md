---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/centred
offline_file: ""
offline_thumbnail: ""
uuid: c8386480-5029-4155-b2ea-a53fa76eb59f
updated: 1484310405
title: centred
categories:
    - Dictionary
---
centred
     adj : concentrated on or clustered around a central point or
           purpose [syn: {centered}, {centralized}, {centralised},
            {focused}]
     n : ten 10s [syn: {hundred}, {100}, {C}, {century}, {one C}]
