---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cumulative
offline_file: ""
offline_thumbnail: ""
uuid: b37fe456-f9d0-448f-bf8d-b157252f2937
updated: 1484310154
title: cumulative
categories:
    - Dictionary
---
cumulative
     adj : increasing by successive addition; "the benefits are
           cumulative"; "the eventual accumulative effect of these
           substances" [syn: {accumulative}]
