---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Richard
offline_file: ""
offline_thumbnail: ""
uuid: 7327f343-9e2e-498b-9cdd-2dd23f94ecb6
updated: 1484308394
title: Richard
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Richard_I_of_England.png
tags:
    - People named Richard
    - Rulers and heads of state
    - In politics and government
    - Aristocrats and non-ruling royals
    - Business people
    - Religious figures
    - In music
    - Composers
    - Singers and musicians
    - Actors
    - Explorers
    - Military figures
    - In sports
    - Scientists
    - Other
    - Cognates/transliterations
    - In Altaic languages
    - In Indo-European languages
    - Germanic
    - Romance
    - Celtic
    - Slavic
    - Other
    - In Afroasiatic languages
    - In Uralic languages
    - In Other languages
    - Short forms
    - Pet forms
categories:
    - Uncategorized
---
The Germanic first or given name Richard derives from German, French, and English "ric" (ruler, leader, king) and "hard" (strong, brave), and it therefore means "powerful leader".[citation needed] Nicknames include "Dick", "Dickie", "Rich", "Richie", "Rick", "Ricky", "Rickey", and others.[citation needed]
