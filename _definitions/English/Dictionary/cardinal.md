---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cardinal
offline_file: ""
offline_thumbnail: ""
uuid: d1fae0b0-9470-4fa8-87a7-883cf946142f
updated: 1484310138
title: cardinal
categories:
    - Dictionary
---
cardinal
     adj 1: serving as an essential component; "a cardinal rule"; "the
            central cause of the problem"; "an example that was
            fundamental to the argument"; "computers are
            fundamental to modern industrial structure" [syn: {central},
             {fundamental}, {key}, {primal}]
     2: being or denoting a numerical quantity but not order;
        "cardinal numbers" [ant: {ordinal}]
     n 1: (Roman Catholic Church) one of a group of more than 100
          prominent bishops in the Sacred College who advise the
          Pope and elect new Popes
     2: the number of elements in a mathematical set; denotes a
        quantity but not the order [syn: ...
