---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/constitute
offline_file: ""
offline_thumbnail: ""
uuid: 574c071d-8bf7-44d0-8bd3-88eece9626b2
updated: 1484310403
title: constitute
categories:
    - Dictionary
---
constitute
     v 1: form or compose; "This money is my only income"; "The stone
          wall was the backdrop for the performance"; "These
          constitute my entire belonging"; "The children made up
          the chorus"; "This sum represents my entire income for a
          year"; "These few men comprise his entire army" [syn: {represent},
           {make up}, {comprise}, {be}]
     2: create and charge with a task or function; "nominate a
        committee" [syn: {appoint}, {name}, {nominate}]
     3: to compose or represent:"This wall forms the background of
        the stage setting"; "The branches made a roof"; "This
        makes a fine introduction" [syn: {form}, {make}]
    ...
