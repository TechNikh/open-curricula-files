---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/avid
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484434261
title: avid
categories:
    - Dictionary
---
avid
     adj 1: (often followed by `for') ardently or excessively desirous;
            "avid for adventure"; "an avid ambition to succeed";
            "fierce devouring affection"; "the esurient eyes of an
            avid curiosity"; "greedy for fame" [syn: {devouring(a)},
             {esurient}, {greedy}]
     2: marked by active interest and enthusiasm; "an avid sports
        fan"; "a great walker"; "an eager beaver" [syn: {great}, {eager},
         {zealous}]
