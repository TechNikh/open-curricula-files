---
version: 1
type: video
provider: YouTube
id: cxMPqb_kxCc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 22eea23e-d423-4a5f-b449-939e344dbc72
updated: 1486069619
title: 'Example- Using trig to solve for missing information'
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/cxMPqb_kxCc/default.jpg
    - https://i3.ytimg.com/vi/cxMPqb_kxCc/1.jpg
    - https://i3.ytimg.com/vi/cxMPqb_kxCc/2.jpg
    - https://i3.ytimg.com/vi/cxMPqb_kxCc/3.jpg
---
