---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Abouabdillah%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 2e8fcd6e-92fd-4f76-9aad-576527553696
updated: 1484309107
title: "Abouabdillah's theorem"
tags:
    - Geometry
    - Number theory
categories:
    - Theorems in geometry
---
Abouabdillah's theorem refers to two distinct theorems in mathematics[citation needed], proven by Moroccan mathematician Driss Abouabdillah: one in geometry and one in number theory.
