---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Imaginary_line_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 2c98123a-52dc-4560-ba76-278eca3bc7d9
updated: 1484309153
title: Imaginary line (mathematics)
categories:
    - Algebraic geometry
---
In complex geometry, an imaginary line is a straight line that only contains one real point. It can be proven that this point is the intersection point with the conjugated line.[1]
