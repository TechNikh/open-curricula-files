---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Barwise_compactness_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 1c194e0c-a693-4503-9182-f681573b9d06
updated: 1484308992
title: Barwise compactness theorem
categories:
    - Theorems in the foundations of mathematics
---
In mathematical logic, the Barwise compactness theorem, named after Jon Barwise, is a generalization of the usual compactness theorem for first-order logic to a certain class of infinitary languages. It was stated and proved by Barwise in 1967.
