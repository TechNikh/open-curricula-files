---
version: 1
type: article
id: https://en.wikipedia.org/wiki/The_Paradoxes_of_the_Infinite
offline_file: ""
offline_thumbnail: ""
uuid: 0126748e-26df-4d59-849d-33a19c1ac390
updated: 1484309509
title: The Paradoxes of the Infinite
categories:
    - Set theory
---
The Paradoxes of the Infinite (German title: Paradoxien des Unendlichen) is a mathematical work by Bernard Bolzano on the theory of sets. It was published by a friend in 1851, three years after Bolzano's death. The work contained many interesting results in set theory. Bolzano expanded on the theme of Galileo's paradox, giving more examples of correspondences between the elements of an infinite set and proper subsets of infinite sets. In the work he also explained the term Menge, rendered in English as "set", which he had coined and used in several works since the 1830s.
