---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contrasting
offline_file: ""
offline_thumbnail: ""
uuid: ee6171ab-cb74-46bf-af26-1a74bb33c2e2
updated: 1484310307
title: contrasting
categories:
    - Dictionary
---
contrasting
     adj : strikingly different; tending to contrast; "contrasting (or
           contrastive) colors" [syn: {contrastive}]
