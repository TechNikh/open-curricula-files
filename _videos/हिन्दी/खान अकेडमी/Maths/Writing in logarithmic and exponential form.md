---
version: 1
type: video
provider: YouTube
id: 5UnwFBIvgU4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3d863e4d-966d-453b-94a1-f68e11f5d5a2
updated: 1486069614
title: Writing in logarithmic and exponential form
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/5UnwFBIvgU4/default.jpg
    - https://i3.ytimg.com/vi/5UnwFBIvgU4/1.jpg
    - https://i3.ytimg.com/vi/5UnwFBIvgU4/2.jpg
    - https://i3.ytimg.com/vi/5UnwFBIvgU4/3.jpg
---
