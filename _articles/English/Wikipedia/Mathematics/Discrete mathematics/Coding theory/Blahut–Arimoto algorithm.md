---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Blahut%E2%80%93Arimoto_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 91f4280f-f782-4f1e-a826-a8a3c18f5688
updated: 1484308542
title: Blahut–Arimoto algorithm
categories:
    - Coding theory
---
The Blahut–Arimoto algorithm, is often used to refer to a class of algorithms for computing numerically either the information theoretic capacity of a channel, or the rate-distortion function of a source. They are iterative algorithms that eventually converge to the optimal solution of the convex optimization problem that is associated with these information theoretic concepts.
