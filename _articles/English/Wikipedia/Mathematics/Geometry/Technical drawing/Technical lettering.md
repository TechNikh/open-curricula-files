---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Technical_lettering
offline_file: ""
offline_thumbnail: ""
uuid: 8704009b-5f2a-4256-995f-678e32ac2ef3
updated: 1484309334
title: Technical lettering
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Technical_Lettering.jpg
tags:
    - Methods of forming letters
    - Freehand lettering
    - Mechanical lettering
    - Dimensions of letters
    - Lettering A
    - Lettering B
categories:
    - Technical drawing
---
Technical lettering is the process of forming letters, numerals, and other characters in technical drawing. It is used to describe, or provide detailed specifications for, an object. With the goals of legibility and uniformity, styles are standardized and lettering ability has little relationship to normal writing ability. Engineering drawings use a Gothic sans-serif script, formed by a series of short strokes. Lower case letters are rare in most drawings of machines.
