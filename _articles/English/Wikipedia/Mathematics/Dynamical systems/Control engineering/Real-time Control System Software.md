---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Real-time_Control_System_Software
offline_file: ""
offline_thumbnail: ""
uuid: 923828e3-d2fc-4db0-9c16-4676b917af4d
updated: 1484308739
title: Real-time Control System Software
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Real-Time_Control_Systems_Library_2.png
tags:
    - Introduction
    - RCS applications
categories:
    - Control engineering
---
The Real-time Control System (RCS) is a software system developed by NIST based on the Real-time Control System Reference Model Architecture, that implements a generic Hierarchical control system. The RCS Software Library is an archive of free C++, Java and Ada code, scripts, tools, makefiles, and documentation developed to aid programmers of software to be used in real-time control systems (especially those using the Reference Model Architecture for Intelligent Systems Design).[1]
