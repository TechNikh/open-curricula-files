---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sniff
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484450581
title: sniff
categories:
    - Dictionary
---
sniff
     n : sensing an odor by inhaling through the nose [syn: {snuff}]
     v 1: perceive by inhaling through the nose; "sniff the perfume"
          [syn: {whiff}]
     2: inhale audibly through the nose; "the sick student was
        sniffling in the back row" [syn: {sniffle}]
