---
version: 1
type: video
provider: YouTube
id: 4H46hGmQE-s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/LMNS%2C%20Ep.05%2C%20Adventure%20and%20educational%20cartoon%20-%20THE%20PERIODIC%20TABLE%20.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0475b6fe-296d-43dd-972d-4955d01410d4
updated: 1486069664
title: 'LMNS, Ep.05, Adventure and educational cartoon - THE PERIODIC TABLE -'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/4H46hGmQE-s/default.jpg
    - https://i3.ytimg.com/vi/4H46hGmQE-s/1.jpg
    - https://i3.ytimg.com/vi/4H46hGmQE-s/2.jpg
    - https://i3.ytimg.com/vi/4H46hGmQE-s/3.jpg
---
