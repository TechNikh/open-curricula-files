---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Brian_Alspach
offline_file: ""
offline_thumbnail: ""
uuid: afd91b5d-29b6-4570-8cc6-7fec76736fa4
updated: 1484309461
title: Brian Alspach
tags:
    - Biography
    - Research
categories:
    - Graph theorists
---
Brian Roger Alspach is a mathematician whose main research interest is in graph theory. Alspach has also studied the mathematics behind poker, and writes for Poker Digest and Canadian Poker Player magazines.
