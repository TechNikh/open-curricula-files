---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attractiveness
offline_file: ""
offline_thumbnail: ""
uuid: cc00363a-6ac0-4a5a-b8b4-4bba9ef7210d
updated: 1484310527
title: attractiveness
categories:
    - Dictionary
---
attractiveness
     n 1: the quality of arousing interest; being attractive or
          something that attracts; "her personality held a strange
          attraction for him" [syn: {attraction}]
     2: a beauty that appeals to the senses [ant: {unattractiveness}]
