---
version: 1
type: video
provider: YouTube
id: 1c5HY3z4k8M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Example%202-%20Variables%20on%20both%20sides.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1931b158-fc23-4e87-a8fa-37518434c36c
updated: 1486069561
title: 'Example 2- Variables on both sides'
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/1c5HY3z4k8M/default.jpg
    - https://i3.ytimg.com/vi/1c5HY3z4k8M/1.jpg
    - https://i3.ytimg.com/vi/1c5HY3z4k8M/2.jpg
    - https://i3.ytimg.com/vi/1c5HY3z4k8M/3.jpg
---
