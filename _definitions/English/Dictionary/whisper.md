---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/whisper
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484410861
title: whisper
categories:
    - Dictionary
---
whisper
     n 1: speaking softly without vibration of the vocal cords [syn: {whispering},
           {susurration}]
     2: the light noise like the noise of silk clothing or leaves
        blowing in the wind [syn: {rustle}, {rustling}, {whispering}]
     v : speak softly; in a low voice [ant: {shout}]
