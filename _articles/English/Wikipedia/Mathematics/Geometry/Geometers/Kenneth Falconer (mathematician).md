---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Kenneth_Falconer_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 87af0a72-56e0-4938-9e6f-5761da537aea
updated: 1484309077
title: Kenneth Falconer (mathematician)
categories:
    - Geometers
---
Kenneth John Falconer FRSE (born 25 January 1952) is a mathematician working in mathematical analysis and in particular on fractal geometry .[1][2] He is Professor of Pure Mathematics in the School of Mathematics and Statistics at the University of St Andrews.
