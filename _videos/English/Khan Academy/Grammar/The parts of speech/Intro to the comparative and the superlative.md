---
version: 1
type: video
provider: YouTube
id: jROzZfJbplU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Intro%20to%20the%20comparative%20and%20the%20superlative.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1c3befd5-a6e6-4b16-8885-f8f932d21f50
updated: 1486069599
title: Intro to the comparative and the superlative
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/jROzZfJbplU/default.jpg
    - https://i3.ytimg.com/vi/jROzZfJbplU/1.jpg
    - https://i3.ytimg.com/vi/jROzZfJbplU/2.jpg
    - https://i3.ytimg.com/vi/jROzZfJbplU/3.jpg
---
Find out how to be "happier" or even "the happiest!"

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-modifier/v/forming-the-comparative-and-superlative?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/the-modifier/v/adjective-order?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
