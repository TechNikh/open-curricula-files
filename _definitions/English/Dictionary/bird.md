---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bird
offline_file: ""
offline_thumbnail: ""
uuid: bfc17e4b-c725-4bea-84a8-b39ef7e44533
updated: 1484310279
title: bird
categories:
    - Dictionary
---
bird
     n 1: warm-blooded egg-laying vertebrates characterized by
          feathers and forelimbs modified as wings
     2: the flesh of a bird or fowl (wild or domestic) used as food
        [syn: {fowl}]
     3: informal terms for a (young) woman [syn: {dame}, {doll}, {wench},
         {skirt}, {chick}]
     4: a cry or noise made to express displeasure or contempt [syn:
         {boo}, {hoot}, {Bronx cheer}, {hiss}, {raspberry}, {razzing},
         {snort}]
     5: badminton equipment consisting of a ball of cork or rubber
        with a crown of feathers [syn: {shuttlecock}, {birdie}, {shuttle}]
     v : watch and study birds in their natural habitat [syn: {birdwatch}]
