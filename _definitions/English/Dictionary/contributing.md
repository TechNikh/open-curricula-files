---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contributing
offline_file: ""
offline_thumbnail: ""
uuid: dd9a0c4b-a34c-4532-b457-a0f881465eab
updated: 1484310246
title: contributing
categories:
    - Dictionary
---
contributing
     adj : tending to bring about; being partly responsible for;
           "working conditions are not conducive to productivity";
           "the seaport was a contributing factor in the growth of
           the city"; "a contributory factor" [syn: {conducive}, {contributing(a)},
            {contributive}, {contributory}, {tributary}]
