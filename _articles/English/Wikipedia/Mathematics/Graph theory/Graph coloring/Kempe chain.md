---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kempe_chain
offline_file: ""
offline_thumbnail: ""
uuid: 69603202-eb14-4558-8016-1aea47388b51
updated: 1484309382
title: Kempe chain
tags:
    - History
    - Formal definition
    - In terms of maps
    - Application to the four colour theorem
    - Other applications
categories:
    - Graph coloring
---
