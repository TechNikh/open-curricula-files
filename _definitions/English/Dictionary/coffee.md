---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coffee
offline_file: ""
offline_thumbnail: ""
uuid: a249a18c-306c-4906-922b-e047d2cb6c31
updated: 1484310381
title: coffee
categories:
    - Dictionary
---
coffee
     n 1: a beverage consisting of an infusion of ground coffee beans;
          "he ordered a cup of coffee" [syn: {java}]
     2: any of several small trees and shrubs native to the tropical
        Old World yielding coffee beans [syn: {coffee tree}]
     3: a seed of the coffee tree; ground to make coffee [syn: {coffee
        bean}, {coffee berry}]
     4: a medium to dark brown color [syn: {chocolate}, {deep brown},
         {umber}, {burnt umber}]
