---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Georg_Cantor%27s_first_set_theory_article'
offline_file: ""
offline_thumbnail: ""
uuid: ebcbb7fe-0762-4b16-93a6-f37a8fc209d1
updated: 1484309501
title: "Georg Cantor's first set theory article"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Georg_Cantor3.jpg
tags:
    - The article
    - The proofs
    - First theorem
    - Second theorem
    - Dense sequences
    - "Example of Cantor's construction"
    - "The development of Cantor's ideas"
    - "The disagreement about Cantor's existence proof"
    - "The influence of Weierstrass and Kronecker on Cantor's article"
    - "Dedekind's contributions to Cantor's article"
    - "The legacy of Cantor's article"
    - Notes
    - Bibliography
categories:
    - Set theory
---
Georg Cantor's first set theory article was published in 1874 and contains the first theorems of transfinite set theory, which studies infinite sets and their properties.[1] One of these theorems is "Cantor's revolutionary discovery" that the set of all real numbers is uncountably, rather than countably, infinite.[2] This theorem is proved using Cantor's first uncountability proof, which differs from the more familiar proof using his diagonal argument. The title of the article, "On a Property of the Collection of All Real Algebraic Numbers," refers to its first theorem: the set of real algebraic numbers is countable.
