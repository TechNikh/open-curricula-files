---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pharynx
offline_file: ""
offline_thumbnail: ""
uuid: b3652e0d-fd12-4b7b-9950-55f14ac8c7ae
updated: 1484310335
title: pharynx
categories:
    - Dictionary
---
pharynx
     n : the passage to the stomach and lungs; in the front part of
         the neck below the chin and above the collarbone [syn: {throat}]
     [also: {pharynges} (pl)]
