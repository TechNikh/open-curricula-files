---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thousand
offline_file: ""
offline_thumbnail: ""
uuid: 210f37f5-5476-46be-af33-95f8e4405e77
updated: 1484310279
title: thousand
categories:
    - Dictionary
---
thousand
     adj : denoting a quantity consisting of 1,000 items or units [syn:
            {a thousand}, {one thousand}, {1000}, {m}, {k}]
     n : the cardinal number that is the product of 10 and 100 [syn:
         {one thousand}, {1000}, {M}, {K}, {chiliad}, {G}, {grand},
          {thou}, {yard}]
