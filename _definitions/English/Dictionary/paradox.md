---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paradox
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484537101
title: paradox
categories:
    - Dictionary
---
paradox
     n : (logic) a self-contradiction; "`I always lie' is a paradox
         because if it is true it must be false"
