---
version: 1
type: video
provider: YouTube
id: qKIOka1PFq0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 04e58648-8a49-4f37-9b05-e27173398f81
updated: 1486069621
title: Finding area using similarity and congruence
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/qKIOka1PFq0/default.jpg
    - https://i3.ytimg.com/vi/qKIOka1PFq0/1.jpg
    - https://i3.ytimg.com/vi/qKIOka1PFq0/2.jpg
    - https://i3.ytimg.com/vi/qKIOka1PFq0/3.jpg
---
