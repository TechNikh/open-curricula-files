---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Batchelor_vortex
offline_file: ""
offline_thumbnail: ""
uuid: 8d111487-2191-4a2d-bbf8-857bdc40ff03
updated: 1484308874
title: Batchelor vortex
categories:
    - Vortices
---
In fluid dynamics, Batchelor vortices, first described by George Batchelor in a 1964 article, have been found useful in analyses of airplane vortex wake hazard problems.[1]
