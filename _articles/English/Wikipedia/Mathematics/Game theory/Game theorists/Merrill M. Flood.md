---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Merrill_M._Flood
offline_file: ""
offline_thumbnail: ""
uuid: e0f8555a-528a-4847-a029-a9a9ee0c30b1
updated: 1484308517
title: Merrill M. Flood
tags:
    - Biography
    - Work
    - Traveling salesman problem
    - Hitchcock transportation problem
    - Publications
categories:
    - Game theorists
---
Merrill Meeks Flood (1908 – 1991[1]) was an American mathematician, notable for developing, with Melvin Dresher, the basis of the game theoretical Prisoner's dilemma model of cooperation and conflict while being at RAND in 1950 (Albert W. Tucker gave the game its prison-sentence interpretation, and thus the name by which it is known today).[2]
