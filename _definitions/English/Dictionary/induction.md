---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/induction
offline_file: ""
offline_thumbnail: ""
uuid: 6b07cc44-becb-4d26-965a-51f4bf226426
updated: 1484310194
title: induction
categories:
    - Dictionary
---
induction
     n 1: a formal entry into an organization or position or office;
          "his initiation into the club"; "he was ordered to
          report for induction into the army"; "he gave a speech
          as part of his installation into the hall of fame" [syn:
           {initiation}, {installation}]
     2: an electrical phenomenon whereby an electromotive force
        (EMF) is generated in a closed circuit by a change in the
        flow of current
     3: reasoning from detailed facts to general principles [syn: {generalization},
         {generalisation}, {inductive reasoning}]
     4: the process whereby changes in the current flow in a circuit
        produce magnetism or ...
