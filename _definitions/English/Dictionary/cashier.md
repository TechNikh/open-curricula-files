---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cashier
offline_file: ""
offline_thumbnail: ""
uuid: 59d52ae5-2ed2-4787-abdf-07319aa8049d
updated: 1484310156
title: cashier
categories:
    - Dictionary
---
cashier
     n 1: an employee of a bank who receives and pays out money [syn:
          {teller}, {bank clerk}]
     2: a person responsible for receiving payments for goods and
        services (as in a shop or restaurant)
     v 1: discard or do away with; "cashier the literal sense of this
          word"
     2: discharge with dishonor, as in the army
