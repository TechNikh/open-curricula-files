---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/snug
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484450281
title: snug
categories:
    - Dictionary
---
snug
     adj 1: offering safety; well protected or concealed; "a snug
            harbor"; "a snug hideout"
     2: fitting closely but comfortably; "a close fit" [syn: {close},
         {close-fitting}]
     3: well and tightly constructed; "a snug house"; "a snug little
        sailboat"
     4: enjoying or affording comforting warmth and shelter
        especially in a small space; "a cozy nook near the fire";
        "snug in bed"; "a snug little apartment" [syn: {cozy}, {cosy}]
     n : a small secluded room [syn: {cubby}, {cubbyhole}, {snuggery}]
     [also: {snugging}, {snugged}, {snuggest}, {snugger}]
