---
version: 1
type: video
provider: YouTube
id: JBi3PWrsKlg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6ccce11f-dd97-4c94-af21-2b70615514bf
updated: 1486069640
title: '!!APPEAR UPSIDE DOWN IN A MIRROR AND RIGHTSIDE UP IN A SPOON!! (CONCAVE AND CONVEX MIRRORS) SCIENCE'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/JBi3PWrsKlg/default.jpg
    - https://i3.ytimg.com/vi/JBi3PWrsKlg/1.jpg
    - https://i3.ytimg.com/vi/JBi3PWrsKlg/2.jpg
    - https://i3.ytimg.com/vi/JBi3PWrsKlg/3.jpg
---
