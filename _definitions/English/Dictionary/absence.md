---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/absence
offline_file: ""
offline_thumbnail: ""
uuid: 4381e0c1-b85b-46a9-adee-c816f53e39a9
updated: 1484310339
title: absence
categories:
    - Dictionary
---
absence
     n 1: the state of being absent; "he was surprised by the absence
          of any explanation" [ant: {presence}]
     2: failure to be present [ant: {presence}]
     3: the time interval during which something or somebody is
        away; "he visited during my absence"
     4: epilepsy characterized by paroxysmal attacks of brief
        clouding of consciousness (a possible other abnormalities)
        [syn: {petit mal epilepsy}]
