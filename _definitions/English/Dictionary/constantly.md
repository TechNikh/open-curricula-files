---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/constantly
offline_file: ""
offline_thumbnail: ""
uuid: c20fd92f-e4e0-43fd-a986-c8a2b6b29e8f
updated: 1484310305
title: constantly
categories:
    - Dictionary
---
constantly
     adv 1: seemingly uninterrupted; "constantly bullied by his big
            brother"; "was perpetually answering the doorbell"
            [syn: {perpetually}]
     2: without variation or change; "constantly kind and gracious"
     3: without interruption; "the world is constantly changing"
