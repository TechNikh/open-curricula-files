---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/horrific
offline_file: ""
offline_thumbnail: ""
uuid: 31ee797a-5fb9-4f76-8120-eec55fe8f6ba
updated: 1484310579
title: horrific
categories:
    - Dictionary
---
horrific
     adj 1: grossly offensive to decency or morality; causing horror;
            "subjected to outrageous cruelty"; "a hideous pattern
            of injustice"; "horrific conditions in the mining
            industry" [syn: {hideous}, {horrid}, {outrageous}]
     2: causing fear or dread or terror; "the awful war"; "an awful
        risk"; "dire news"; "a career or vengeance so direful that
        London was shocked"; "the dread presence of the
        headmaster"; "polio is no longer the dreaded disease it
        once was"; "a dreadful storm"; "a fearful howling";
        "horrendous explosions shook the city"; "a terrible curse"
        [syn: {awful}, {dire}, {direful}, ...
