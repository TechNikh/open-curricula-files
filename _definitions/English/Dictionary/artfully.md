---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/artfully
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484566261
title: artfully
categories:
    - Dictionary
---
artfully
     adv 1: in an artful manner; "her foot pointed artfully toward
            tapering toes"
     2: in a disingenuous manner; "disingenuously, he asked leading
        questions abut his opponent's work" [syn: {disingenuously}]
     3: in an artful manner; "he craftily arranged to be there when
        the decision was announced"; "had ever circumstances
        conspired so cunningly?" [syn: {craftily}, {cunningly}, {foxily},
         {knavishly}, {slyly}, {trickily}]
