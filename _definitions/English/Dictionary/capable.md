---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/capable
offline_file: ""
offline_thumbnail: ""
uuid: 5d46230e-fe20-4eba-aab0-b5878f989bcf
updated: 1484310311
title: capable
categories:
    - Dictionary
---
capable
     adj 1: (usually followed by `of') having capacity or ability;
            "capable of winning"; "capable of hard work"; "capable
            of walking on two feet" [ant: {incapable}]
     2:  possibly accepting or permitting; "a passage capable of
        misinterpretation"; "open to interpretation"; "an issue
        open to question"; "the time is fixed by the director and
        players and therefore subject to much variation" [syn: {open},
         {subject}]
     3: (followed by `of') having the temperament or inclination
        for; "no one believed her capable of murder" [ant: {incapable}]
     4: having the requisite qualities for; "equal to the task";
        "the ...
