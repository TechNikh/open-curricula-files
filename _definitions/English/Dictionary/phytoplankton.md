---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phytoplankton
offline_file: ""
offline_thumbnail: ""
uuid: 25bbaea6-6457-4dd9-9784-9ffd67e8563e
updated: 1484310271
title: phytoplankton
categories:
    - Dictionary
---
phytoplankton
     n : photosynthetic or plant constituent of plankton; mainly
         unicellular algae
