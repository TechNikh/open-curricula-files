---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fisher%27s_principle'
offline_file: ""
offline_thumbnail: ""
uuid: 7ca1b6bf-159c-424d-918e-c01e9fa32ace
updated: 1484308524
title: "Fisher's principle"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Antennarius_striatus_0.jpg
tags:
    - Basic explanation
    - Parental expenditure
    - Development of the argument
    - "Fisher's sources"
categories:
    - Evolutionary game theory
---
Fisher's principle is an evolutionary model that explains why the sex ratio of most species that produce offspring through sexual reproduction is approximately 1:1 between males and females. It was famously outlined by Ronald Fisher in his 1930 book The Genetical Theory of Natural Selection[1] (but incorrectly attributed to Fisher as original[2]). Nevertheless, A. W. F. Edwards has remarked that it is "probably the most celebrated argument in evolutionary biology".[2] Specifically, Fisher couched his argument in terms of parental expenditure, and predicted that parental expenditure on both sexes should be equal. Sex ratios that are 1:1 are hence known as "Fisherian", and those that are not ...
