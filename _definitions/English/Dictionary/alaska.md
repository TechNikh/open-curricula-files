---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alaska
offline_file: ""
offline_thumbnail: ""
uuid: 9857b6ac-b0c6-4dee-862d-f3cf72bde0b5
updated: 1484310170
title: alaska
categories:
    - Dictionary
---
Alaska
     n : a state in northwestern North America; the 49th state
         admitted to the union; "Alaska is the largest state in
         the United States" [syn: {Last Frontier}, {AK}]
