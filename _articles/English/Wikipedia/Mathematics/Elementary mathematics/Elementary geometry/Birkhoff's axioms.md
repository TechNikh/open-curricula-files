---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Birkhoff%27s_axioms'
offline_file: ""
offline_thumbnail: ""
uuid: 751847ee-6790-4dfc-b85e-19ce06f55151
updated: 1484308927
title: "Birkhoff's axioms"
categories:
    - Elementary geometry
---
In 1932, G. D. Birkhoff created a set of four postulates of Euclidean geometry sometimes referred to as Birkhoff's axioms.[1] These postulates are all based on basic geometry that can be confirmed experimentally with a scale and protractor. Since the postulates build upon the real numbers, the approach is similar to a model-based introduction to Euclidean geometry.
