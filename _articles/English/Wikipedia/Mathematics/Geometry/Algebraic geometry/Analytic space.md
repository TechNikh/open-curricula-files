---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Analytic_space
offline_file: ""
offline_thumbnail: ""
uuid: ac2d34bf-197e-434d-bb5a-5bafe0edfbee
updated: 1484309130
title: Analytic space
tags:
    - Definition
    - Basic Results
    - Smoothness
    - Coherent sheaves
    - Generalizations
categories:
    - Algebraic geometry
---
An analytic space is a generalization of an analytic manifold that allows singularities. An analytic space is a space that is locally the same as an analytic variety. They are prominent in the study of several complex variables, but they also appear in other contexts.
