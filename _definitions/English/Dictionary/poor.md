---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poor
offline_file: ""
offline_thumbnail: ""
uuid: a4e1d1a3-1fe9-42fa-b7d2-f3332d2d86aa
updated: 1484310252
title: poor
categories:
    - Dictionary
---
poor
     adj 1: moderate to inferior in quality; "they improved the quality
            from mediocre to above average"; "he would make a poor
            spy" [syn: {mediocre}, {second-rate}]
     2: deserving or inciting pity; "a hapless victim"; "miserable
        victims of war"; "the shabby room struck her as
        extraordinarily pathetic"- Galsworthy; "piteous appeals
        for help"; "pitiable homeless children"; "a pitiful fate";
        "Oh, you poor thing"; "his poor distorted limbs"; "a
        wretched life" [syn: {hapless}, {miserable}, {misfortunate},
         {pathetic}, {piteous}, {pitiable}, {pitiful}, {wretched}]
     3: having little money or few possessions; ...
