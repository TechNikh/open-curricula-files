---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wine_auction
offline_file: ""
offline_thumbnail: ""
uuid: 0587a677-3274-49c1-9ba2-c2ddd821ad0f
updated: 1484309017
title: Wine auction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Glass_of_wine.png
tags:
    - First hand auctions
    - Second hand auctions
categories:
    - Auctions
---
A wine auction is an auction devoted to wine, sometimes in combination with other alcoholic beverages. There are two basic types of wine auctions: first hand wine auctions, where wineries sell their own wines, and second hand wine auctions, arranged by auction houses or other auctioneers to make it possible for any owners of wine to trade it. In most cases, the wines traded at wine auctions are collectible wine, so-called "fine wine". These are wines which are typically suitable for extended cellaring, and where some of the buyers are looking for mature wines which are no longer available through first-hand retail channels.
