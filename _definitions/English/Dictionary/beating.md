---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beating
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415601
title: beating
categories:
    - Dictionary
---
beating
     adj : expanding and contracting rhythmically as to the beating of
           the heart; "felt the pulsating artery"; "oh my beating
           heart" [syn: {pulsating}, {pulsing}]
     n 1: the act of overcoming or outdoing [syn: {whipping}]
     2: the act of inflicting corporal punishment with repeated
        blows [syn: {thrashing}, {licking}, {drubbing}, {lacing},
        {trouncing}, {whacking}]
