---
version: 1
type: video
provider: YouTube
id: _3TALBPpXyo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: eb1bc706-994d-4a07-b2d7-f9eee8704808
updated: 1486069609
title: New operator definitions
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/_3TALBPpXyo/default.jpg
    - https://i3.ytimg.com/vi/_3TALBPpXyo/1.jpg
    - https://i3.ytimg.com/vi/_3TALBPpXyo/2.jpg
    - https://i3.ytimg.com/vi/_3TALBPpXyo/3.jpg
---
