---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dexter_and_sinister
offline_file: ""
offline_thumbnail: ""
uuid: a3d5ab34-c5e9-4bab-b5c3-4050286efd26
updated: 1484309295
title: Dexter and sinister
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-DexterAndSinister_HeraldicEscutcheon.png
categories:
    - Orientation (geometry)
---
Dexter and sinister are terms used in heraldry to refer to specific locations in an escutcheon bearing a coat of arms, and to the other elements of an achievement. "Dexter" (Latin for "right")[1] means to the right from the viewpoint of the bearer of the shield, i.e. the bearer's proper right, to the left from that of the viewer. "Sinister" (Latin for "left")[2] means to the left from the viewpoint of the bearer, the bearer's proper left, to the right from that of the viewer.
