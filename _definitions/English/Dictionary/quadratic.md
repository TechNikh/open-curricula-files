---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quadratic
offline_file: ""
offline_thumbnail: ""
uuid: 03769bdb-b54e-4bba-9dd2-343932afb249
updated: 1484310156
title: quadratic
categories:
    - Dictionary
---
quadratic
     adj 1: of or relating to or resembling a square; "quadratic shapes"
     2: of or relating to the second power; "quadratic equation"
     n 1: an equation in which the highest power of an unknown
          quantity is a square [syn: {quadratic equation}]
     2: a polynomial of the second degree [syn: {quadratic
        polynomial}]
