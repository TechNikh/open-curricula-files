---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nature
offline_file: ""
offline_thumbnail: ""
uuid: b5b37b21-b398-4949-9d9d-8de33275577c
updated: 1484310328
title: nature
categories:
    - Dictionary
---
nature
     n 1: the essential qualities or characteristics by which
          something is recognized; "it is the nature of fire to
          burn"; "the true nature of jealousy"
     2: a causal agent creating and controlling things in the
        universe; "the laws of nature"; "nature has seen to it
        that men are stronger than women"
     3: the natural physical world including plants and animals and
        landscapes etc.; "they tried to preserve nature as they
        found it"
     4: the complex of emotional and intellectual attributes that
        determine a person's characteristic actions and reactions;
        "it is his nature to help others"
     5: a particular type ...
