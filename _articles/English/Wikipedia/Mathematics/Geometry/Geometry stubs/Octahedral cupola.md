---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Octahedral_cupola
offline_file: ""
offline_thumbnail: ""
uuid: a94522d7-b4e6-4e0b-ad58-955913733c1d
updated: 1484309365
title: Octahedral cupola
tags:
    - Related polytopes
categories:
    - Geometry stubs
---
In 4-dimensional geometry, the octahedral cupola is a 4-polytope bounded by one octahedron and a parallel rhombicuboctahedron, connected by 20 triangular prisms, and 6 square pyramids.[1]
