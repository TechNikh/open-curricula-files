---
version: 1
type: video
provider: YouTube
id: 3AF_rN-yN-Y
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Concrete%20and%20abstract%20nouns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7f8ac34c-b192-4842-80a4-21badafa688c
updated: 1486069599
title: Concrete and abstract nouns
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/3AF_rN-yN-Y/default.jpg
    - https://i3.ytimg.com/vi/3AF_rN-yN-Y/1.jpg
    - https://i3.ytimg.com/vi/3AF_rN-yN-Y/2.jpg
    - https://i3.ytimg.com/vi/3AF_rN-yN-Y/3.jpg
---
The difference between concrete and abstract nouns can be a little fuzzy. Let's start by focusing on the word origins of "concrete" and "abstract", and see if that gives us a way in.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/e/abstract-and-concrete-nouns?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/v/introduction-to-verbs-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/v/bonus-video-origin-of-the-mutant-plural-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
