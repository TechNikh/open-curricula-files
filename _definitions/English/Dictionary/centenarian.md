---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/centenarian
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484439601
title: centenarian
categories:
    - Dictionary
---
centenarian
     adj : being at least 100 years old
     n : someone who is at least 100 years old
