---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stew_pan
offline_file: ""
offline_thumbnail: ""
uuid: 27aca511-e340-4c9d-93c7-865441c66c3b
updated: 1484308375
title: Casserole
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Makaronilaatikko.jpg
tags:
    - History
    - Use of the term in the U.S. and Canada
categories:
    - Uncategorized
---
A casserole (French: diminutive of casse, from Provençal cassa "pan"[1]) is a large, deep dish used both in the oven and as a serving vessel. The word is also used for the food cooked and served in such a vessel, with the cookware itself called a casserole dish or casserole pan.
