---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nim
offline_file: ""
offline_thumbnail: ""
uuid: f8876238-577c-4785-b4cf-cc36f8d3b5a5
updated: 1484308533
title: Nim
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nim_qc.jpg
tags:
    - Game play and illustration
    - Winning positions
    - Mathematical theory
    - Proof of the winning formula
    - Variations
    - Dividing natural number
    - The subtraction game S(1, 2, . . ., k)
    - The 21 game
    - The 100 game
    - A multiple-heap rule
    - Circular Nim
    - "Grundy's game"
    - Greedy Nim
    - Index-k Nim
    - Building Nim
    - Additional reading
categories:
    - Combinatorial game theory
---
Nim is a mathematical game of strategy in which two players take turns removing objects from distinct heaps. On each turn, a player must remove at least one object, and may remove any number of objects provided they all come from the same heap. The goal of the game is to be the player to remove the last object.
