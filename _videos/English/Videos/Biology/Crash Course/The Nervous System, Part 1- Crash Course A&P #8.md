---
version: 1
type: video
provider: YouTube
id: qPix_X-9t7E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/The%20Nervous%20System%2C%20Part%201-%20Crash%20Course%20A%26P%20%238.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a2617d38-c319-46b8-8c1c-0ae2dacfa96b
updated: 1486069681
title: 'The Nervous System, Part 1- Crash Course A&P #8'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/qPix_X-9t7E/default.jpg
    - https://i3.ytimg.com/vi/qPix_X-9t7E/1.jpg
    - https://i3.ytimg.com/vi/qPix_X-9t7E/2.jpg
    - https://i3.ytimg.com/vi/qPix_X-9t7E/3.jpg
---
