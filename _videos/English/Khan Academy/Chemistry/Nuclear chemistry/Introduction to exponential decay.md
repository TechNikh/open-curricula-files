---
version: 1
type: video
provider: YouTube
id: HTDop6eEsaA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Nuclear%20chemistry/Introduction%20to%20exponential%20decay.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f2f22307-d1e8-4d08-8cf7-48a126942263
updated: 1486069583
title: Introduction to exponential decay
categories:
    - Nuclear chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/HTDop6eEsaA/default.jpg
    - https://i3.ytimg.com/vi/HTDop6eEsaA/1.jpg
    - https://i3.ytimg.com/vi/HTDop6eEsaA/2.jpg
    - https://i3.ytimg.com/vi/HTDop6eEsaA/3.jpg
---
Introduction to Exponential Decay. Using the exponential decay formula to calculate k, calculating the mass of carbon-14 remaining after a given time, and calculating the time it takes to have a specific mass remaining .

Watch the next lesson: https://www.khanacademy.org/science/chemistry/nuclear-chemistry/radioactive-decay/v/more-exponential-decay-examples?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/nuclear-chemistry/radioactive-decay/v/exponential-decay-formula-proof-can-skip-involves-calculus?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
