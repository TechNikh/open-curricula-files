---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swimming
offline_file: ""
offline_thumbnail: ""
uuid: 4c040725-3510-4ebb-baa1-1ad63e62a487
updated: 1484310277
title: swimming
categories:
    - Dictionary
---
swim
     n : the act of swimming [syn: {swimming}]
     v 1: travel through water; "We had to swim for 20 minutes to
          reach the shore"; "a big fish was swimming in the tank"
     2: be afloat; stay on a liquid surface; not sink [syn: {float}]
        [ant: {sink}]
     [also: {swum}, {swimming}, {swam}]
