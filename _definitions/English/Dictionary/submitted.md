---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/submitted
offline_file: ""
offline_thumbnail: ""
uuid: c8a09a80-e165-4da5-9abb-71874dc97e70
updated: 1484310166
title: submitted
categories:
    - Dictionary
---
submit
     v 1: refer for judgment or consideration; "She submitted a
          proposal to the agency" [syn: {subject}]
     2: put before; "I submit to you that the accused is guilty"
        [syn: {state}, {put forward}, {posit}]
     3: yield to the control of another
     4: hand over formally [syn: {present}]
     5: refer to another person for decision or judgment; "She likes
        to relegate difficult questions to her colleagues" [syn: {relegate},
         {pass on}]
     6: submit or yield to another's wish or opinion; "The
        government bowed to the military pressure" [syn: {bow}, {defer},
         {accede}, {give in}]
     7: accept or undergo, often unwillingly; "We ...
