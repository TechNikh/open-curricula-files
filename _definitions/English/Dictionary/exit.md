---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exit
offline_file: ""
offline_thumbnail: ""
uuid: 02021378-6ab2-4fa0-91d1-0a4cd1369e1e
updated: 1484310319
title: exit
categories:
    - Dictionary
---
exit
     n 1: an opening that permits escape or release; "he blocked the
          way out"; "the canyon had only one issue" [syn: {issue},
           {outlet}, {way out}]
     2: euphemistic expressions for death; "thousands mourned his
        passing" [syn: {passing}, {loss}, {departure}, {expiration},
         {going}, {release}]
     3: the act of going out
     v 1: move out of or depart from; "leave the room"; "the fugitive
          has left the country" [syn: {go out}, {get out}, {leave}]
          [ant: {enter}]
     2: lose the lead
     3: pass from physical life and lose all all bodily attributes
        and functions necessary to sustain life; "She died from
        cancer"; ...
