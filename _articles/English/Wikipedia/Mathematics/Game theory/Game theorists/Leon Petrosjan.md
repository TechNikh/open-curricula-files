---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Leon_Petrosjan
offline_file: ""
offline_thumbnail: ""
uuid: e02e4e5b-a355-4793-9b14-4330918709ee
updated: 1484309022
title: Leon Petrosjan
tags:
    - Fields of research
    - Academic activities
    - Education
    - Selected publications
categories:
    - Game theorists
---
Leon Petrosjan (Russian: Леон Аганесович Петросян) (born December 18, 1940) is a professor of Applied Mathematics and the Head of the Department of Mathematical Game theory and Statistical Decision Theory at the St. Petersburg University, Russia.[1]
