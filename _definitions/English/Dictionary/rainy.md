---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rainy
offline_file: ""
offline_thumbnail: ""
uuid: 5e064de4-1132-448b-9069-498327cd83a1
updated: 1484310236
title: rainy
categories:
    - Dictionary
---
rainy
     adj 1: (of weather) wet by periods of rain; "showery weather";
            "rainy days" [syn: {showery}]
     2: marked by rain; "their vacation turned out to be a series of
        rainy days" [syn: {pluvial}, {pluvious}]
     [also: {rainiest}, {rainier}]
