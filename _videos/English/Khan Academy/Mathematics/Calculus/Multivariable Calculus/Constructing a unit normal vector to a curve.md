---
version: 1
type: video
provider: YouTube
id: _9x2cqO7-Ig
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Constructing%20a%20unit%20normal%20vector%20to%20a%20curve.webm"
offline_file: ""
offline_thumbnail: ""
uuid: aebe3491-1b08-4560-b473-ae3afc7a31bb
updated: 1486069690
title: Constructing a unit normal vector to a curve
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/_9x2cqO7-Ig/default.jpg
    - https://i3.ytimg.com/vi/_9x2cqO7-Ig/1.jpg
    - https://i3.ytimg.com/vi/_9x2cqO7-Ig/2.jpg
    - https://i3.ytimg.com/vi/_9x2cqO7-Ig/3.jpg
---
