---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/princely
offline_file: ""
offline_thumbnail: ""
uuid: 24b3d802-c2a6-4d39-8067-e74ba14234f9
updated: 1484310591
title: princely
categories:
    - Dictionary
---
princely
     adj 1: rich and superior in quality; "a princely sum"; "gilded
            dining rooms" [syn: {deluxe}, {gilded}, {luxurious}, {opulent},
             {sumptuous}]
     2: having the rank of or befitting a prince; "a princely
        bearing"; "princely manner"
     [also: {princeliest}, {princelier}]
