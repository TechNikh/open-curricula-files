---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marshy
offline_file: ""
offline_thumbnail: ""
uuid: 077324a5-2cd0-4ac8-98cb-d51069fc7918
updated: 1484310437
title: marshy
categories:
    - Dictionary
---
marshy
     adj : (of soil) soft and watery; "the ground was boggy under
           foot"; "a marshy coastline"; "miry roads"; "wet mucky
           lowland"; "muddy barnyard"; "quaggy terrain"; "the
           sloughy edge of the pond"; "swampy bayous" [syn: {boggy},
            {miry}, {mucky}, {muddy}, {quaggy}, {sloughy}, {swampy}]
     [also: {marshiest}, {marshier}]
