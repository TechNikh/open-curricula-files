---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swelled
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484567881
title: swelled
categories:
    - Dictionary
---
swelled
     adj : feeling self-importance; "too big for his britches"; "had a
           swelled head"; "he was swelled with pride" [syn: {big},
            {vainglorious}]
