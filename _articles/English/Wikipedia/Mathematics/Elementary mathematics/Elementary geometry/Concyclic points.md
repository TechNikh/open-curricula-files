---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Concyclic_points
offline_file: ""
offline_thumbnail: ""
uuid: 0a3682ed-659a-4632-860d-e719f4590899
updated: 1484308930
title: Concyclic points
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Concyclic.svg.png
tags:
    - Bisectors
    - Cyclic polygons
    - Triangles
    - Quadrilaterals
    - n-gons
    - Variations
    - Other properties
    - Examples
    - Triangles
    - Other polygons
categories:
    - Elementary geometry
---
In geometry, a set of points are said to be concyclic (or cocyclic) if they lie on a common circle. All concyclic points are the same distance from the center of the circle. Three points in the plane that do not all fall on a straight line are concyclic, but four or more such points in the plane are not necessarily concyclic.
