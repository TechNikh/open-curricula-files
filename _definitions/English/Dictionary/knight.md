---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/knight
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484333521
title: knight
categories:
    - Dictionary
---
knight
     n 1: originally a person of noble birth trained to arms and
          chivalry; today in Great Britain a person honored by the
          sovereign for personal merit
     2: a chessman in the shape of a horse's head; can move two
        squares horizontally and one vertically (or vice versa)
        [syn: {horse}]
     v : raise (someone) to knighthood; "The Beatles were knighted"
         [syn: {dub}]
