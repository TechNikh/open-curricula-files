---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/serial
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484549821
title: serial
categories:
    - Dictionary
---
serial
     adj 1: in regular succession without gaps; "serial concerts" [syn:
            {consecutive}, {sequent}, {sequential}, {successive}]
     2: pertaining to or composed in serial technique; "serial
        music"
     3: pertaining to or occurring in or producing a series; "serial
        monogamy"; "serial killing"; "a serial killer"; "serial
        publication"
     4: of or relating to the sequential performance of multiple
        operations; "serial processing" [syn: {in series(p)}, {nonparallel}]
     n 1: a serialized set of programs; "a comedy series"; "the
          Masterworks concert series" [syn: {series}]
     2: a periodical that appears at scheduled times [syn: ...
