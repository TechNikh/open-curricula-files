---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biholomorphism
offline_file: ""
offline_thumbnail: ""
uuid: c17749b1-3e58-4719-846b-9e02114bab79
updated: 1484309134
title: Biholomorphism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Biholomorphism_illustration.svg.png
tags:
    - Formal definition
    - Riemann mapping theorem and generalizations
    - Alternative definitions
categories:
    - Algebraic geometry
---
In the mathematical theory of functions of one or more complex variables, and also in complex algebraic geometry, a biholomorphism or biholomorphic function is a bijective holomorphic function whose inverse is also holomorphic.
