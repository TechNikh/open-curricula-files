---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Animal_behavior
offline_file: ""
offline_thumbnail: ""
uuid: a1f2fc52-9547-46b3-b671-2ae05895da78
updated: 1484308361
title: Ethology
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Ethology_diversity.jpg
tags:
    - Etymology
    - Relationship with comparative psychology
    - History
    - "Scala naturae and Lamarck's theories"
    - >
        Theory of evolution by natural selection and the beginnings
        of ethology
    - Social ethology and recent developments
    - Growth of the field
    - Instinct
    - Fixed action patterns
    - learning
    - Habituation
    - Associative learning
    - Imprinting
    - Cultural learning
    - Observational learning
    - Imitation
    - Stimulus and local enhancement
    - Social transmission
    - Teaching
    - Mating and the fight for supremacy
    - Living in groups
    - Benefits and costs of group living
    - Group size
    - "Tinbergen's four questions for ethologists"
    - List of ethologists
    - Gallery of videos
categories:
    - Uncategorized
---
Ethology is the scientific and objective study of animal behaviour, usually with a focus on behaviour under natural conditions, and viewing behaviour as an evolutionarily adaptive trait.[1] Behaviourism is a term that also describes the scientific and objective study of animal behaviour, usually referring to measured responses to stimuli or trained behavioural responses in a laboratory context, without a particular emphasis on evolutionary adaptivity.[2] Many naturalists have studied aspects of animal behaviour throughout history. Ethology has its scientific roots in the work of Charles Darwin and of American and German ornithologists of the late 19th and early 20th century, including ...
