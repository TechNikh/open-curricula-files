---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rubber
offline_file: ""
offline_thumbnail: ""
uuid: cf3f88a2-989d-469c-bd81-fbf3067e4008
updated: 1484310242
title: rubber
categories:
    - Dictionary
---
rubber
     adj 1: made of rubber and therefore water-repellent; "rubber boots"
            [syn: {rubberized}, {rubberised}]
     2: returned for lack of funds; "a rubber check"; "a no-good
        check" [syn: {no-good}]
     n 1: latex from trees (especially trees of the genera Hevea and
          Ficus) [syn: {India rubber}, {gum elastic}, {caoutchouc}]
     2: an eraser made of rubber (or of a synthetic material with
        properties similar to rubber); commonly mounted at one end
        of a pencil [syn: {rubber eraser}, {pencil eraser}]
     3: contraceptive device consisting of a thin rubber or latex
        sheath worn over the penis during intercourse [syn: {condom},
         ...
