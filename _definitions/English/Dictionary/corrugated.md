---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corrugated
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484614681
title: corrugated
categories:
    - Dictionary
---
corrugated
     adj : shaped into alternating parallel grooves and ridges; "the
           surface of the ocean was rippled and corrugated"
