---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-control
offline_file: ""
offline_thumbnail: ""
uuid: 9931bd98-14b3-4226-bf3c-136c4e96f4c2
updated: 1484310181
title: self-control
categories:
    - Dictionary
---
self-control
     n 1: the act of denying yourself; controlling your impulses [syn:
           {self-denial}, {self-discipline}]
     2: the trait of resolutely controlling your own behavior [syn:
        {self-possession}, {possession}, {willpower}, {self-command},
         {self-will}]
