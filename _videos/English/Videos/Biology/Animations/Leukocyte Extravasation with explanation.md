---
version: 1
type: video
provider: YouTube
id: 297HcgDxb7k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Leukocyte%20Extravasation%20with%20explanation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6d137d92-4e93-4ef7-aa07-3b590b23f964
updated: 1486069669
title: Leukocyte Extravasation with explanation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/297HcgDxb7k/default.jpg
    - https://i3.ytimg.com/vi/297HcgDxb7k/1.jpg
    - https://i3.ytimg.com/vi/297HcgDxb7k/2.jpg
    - https://i3.ytimg.com/vi/297HcgDxb7k/3.jpg
---
