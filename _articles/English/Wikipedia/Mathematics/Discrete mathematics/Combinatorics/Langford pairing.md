---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Langford_pairing
offline_file: ""
offline_thumbnail: ""
uuid: 40bcb823-0ed2-47dd-9163-926cf9de30ce
updated: 1484308590
title: Langford pairing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Langford_pairing.svg.png
tags:
    - Example
    - Properties
    - Applications
    - Notes
categories:
    - Combinatorics
---
In combinatorial mathematics, a Langford pairing, also called a Langford sequence, is a permutation of the sequence of 2n numbers 1, 1, 2, 2, ..., n, n in which the two ones are one unit apart, the two twos are two units apart, and more generally the two copies of each number k are k units apart. Langford pairings are named after C. Dudley Langford, who posed the problem of constructing them in 1958.
