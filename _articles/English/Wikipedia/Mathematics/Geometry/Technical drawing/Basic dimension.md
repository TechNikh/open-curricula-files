---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Basic_dimension
offline_file: ""
offline_thumbnail: ""
uuid: 72afe924-379e-4fa5-af00-8b802dac9b6b
updated: 1484309322
title: Basic dimension
categories:
    - Technical drawing
---
In technical drawing, a basic dimension is a theoretically exact dimension, given from a datum to a feature of interest. In Geometric dimensioning and tolerancing, basic dimensions are defined as a numerical value used to describe the theoretically exact size, profile, orientation or location of a feature or datum target.[1] Allowable variations from the theoretically exact geometry are indicated by feature control frames, notes, and tolerances on other non-basic dimensions.[1]
