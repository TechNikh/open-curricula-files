---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/picometre
offline_file: ""
offline_thumbnail: ""
uuid: 34c6bc6b-8db9-4b38-8195-e3c741ac9cb7
updated: 1484310405
title: picometre
categories:
    - Dictionary
---
picometre
     n : a metric unit of length equal to one trillionth of a meter
         [syn: {picometer}, {micromicron}]
