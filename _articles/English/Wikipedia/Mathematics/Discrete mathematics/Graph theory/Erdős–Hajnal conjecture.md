---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Hajnal_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 683bbc8d-7706-4452-8763-ffcb33b515b4
updated: 1484308619
title: Erdős–Hajnal conjecture
tags:
    - Graphs without large cliques or independent sets
    - Partial results
categories:
    - Graph theory
---
In graph theory, a branch of mathematics, the Erdős–Hajnal conjecture states that families of graphs defined by forbidden induced subgraphs have either large cliques or large independent sets. More precisely, for an arbitrary undirected graph 
  
    
      
        H
      
    
    {\displaystyle H}
  
, let 
  
    
      
        
          
            
              F
            
          
          
            H
          
        
      
    
    {\displaystyle {\mathcal {F}}_{H}}
  
 be the family of graphs that do not have 
  
    
      
        H
      
    
    {\displaystyle H}
  
 as an induced subgraph. Then, according to the conjecture, there exists a constant 
  
    ...
