---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/baptist
offline_file: ""
offline_thumbnail: ""
uuid: 71417fa3-9d01-45ea-a7dc-d2df08bf2ed7
updated: 1484310293
title: baptist
categories:
    - Dictionary
---
Baptist
     adj : of or pertaining to or characteristic of the Baptist church;
           "Baptist baptismal practices"; "a Baptist minister"
           [syn: {Baptistic}]
     n : follower of Baptistic doctrines
