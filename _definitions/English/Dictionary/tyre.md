---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tyre
offline_file: ""
offline_thumbnail: ""
uuid: b9ceaba6-1c56-412f-ae48-1021e777f008
updated: 1484310486
title: tyre
categories:
    - Dictionary
---
Tyre
     n 1: a port in southern Lebanon on the Mediterranean Sea;
          formerly a major Phoenician seaport famous for silks
          [syn: {Sur}]
     2: hoop that covers a wheel; "automobile tires are usually made
        of rubber and filled with compressed air" [syn: {tire}]
