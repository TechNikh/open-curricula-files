---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamiltonian_coloring
offline_file: ""
offline_thumbnail: ""
uuid: b998659b-42db-4ffa-bd75-8d5345628e76
updated: 1484309384
title: Hamiltonian coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Detour_distance_in_C5.png
categories:
    - Graph coloring
---
Hamiltonian coloring is a type of graph coloring. Hamiltonian coloring uses a concept called detour distance between two vertices of the graph.[1] It has many applications in different areas of science and technology.
