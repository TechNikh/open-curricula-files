---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shortening
offline_file: ""
offline_thumbnail: ""
uuid: d20dd57d-b929-48ac-afed-df86bfb6c8bf
updated: 1484310333
title: shortening
categories:
    - Dictionary
---
shortening
     n 1: fat such as butter or lard used in baked goods
     2: act of decreasing in length; "the dress needs shortening"
