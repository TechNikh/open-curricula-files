---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preference
offline_file: ""
offline_thumbnail: ""
uuid: 2a272da5-4e36-4b3d-be05-7da36dcc28ff
updated: 1484310411
title: preference
categories:
    - Dictionary
---
preference
     n 1: a strong liking; "my own preference is for good literature";
          "the Irish have a penchant for blarney" [syn: {penchant},
           {predilection}, {taste}]
     2: a predisposition in favor of something; "a predilection for
        expensive cars"; "his sexual preferences"; "showed a
        Marxist orientation" [syn: {predilection}, {orientation}]
     3: the right or chance to choose; "given my druthers, I'd eat
        cake" [syn: {druthers}]
     4: grant of favor or advantage to one over another (especially
        to a country or countries in matters of international
        trade, such as levying duties)
