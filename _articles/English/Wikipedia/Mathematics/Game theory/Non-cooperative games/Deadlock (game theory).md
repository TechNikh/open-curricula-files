---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deadlock_(game_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 72abf860-5f55-4c17-bc4d-f5447d44d344
updated: 1484309055
title: Deadlock (game theory)
categories:
    - Non-cooperative games
---
In game theory, Deadlock is a game where the action that is mutually most beneficial is also dominant. This provides a contrast to the Prisoner's Dilemma where the mutually most beneficial action is dominated. This makes Deadlock of rather less interest, since there is no conflict between self-interest and mutual benefit. The game provides some interest, however, since one has some motivation to encourage one's opponent to play a dominated strategy.
