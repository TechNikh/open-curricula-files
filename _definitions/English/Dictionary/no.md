---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/no
offline_file: ""
offline_thumbnail: ""
uuid: 7aa93892-a8e7-48a1-b390-b4fcb7322683
updated: 1484310355
title: 'no'
categories:
    - Dictionary
---
no
     adj : quantifier; used with either mass nouns or plural count
           nouns for indicating a complete or almost complete lack
           or zero quantity of; "we have no bananas"; "no eggs
           left and no money to buy any"; "have you no decency?";
           "did it with no help"; "I'll get you there in no time"
           [syn: {no(a)}] [ant: {all(a)}, {some(a)}]
     n 1: a negative; "his no was loud and clear" [ant: {yes}]
     2: a radioactive transuranic element synthesized by bombarding
        curium with carbon ions; 7 isotopes are known [syn: {nobelium},
         {atomic number 102}]
     adv 1: referring to the degree to which a certain quality is
            ...
