---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/masses
offline_file: ""
offline_thumbnail: ""
uuid: 31823375-612c-4739-8de8-9918883b2d6c
updated: 1484310230
title: masses
categories:
    - Dictionary
---
mass
     adj 1: occurring widely (as to many people); "mass destruction"
            [syn: {large-scale}]
     2: gathered or tending to gather into a mass or whole;
        "aggregate expenses include expenses of all divisions
        combined for the entire year"; "the aggregated amount of
        indebtedness" [syn: {aggregate}, {aggregated}, {aggregative}]
     n 1: the property of a body that causes it to have weight in a
          gravitational field
     2: (often followed by `of') a large number or amount or extent;
        "a batch of letters"; "a deal of trouble"; "a lot of
        money"; "he made a mint on the stock market"; "it must
        have cost plenty" [syn: {batch}, ...
