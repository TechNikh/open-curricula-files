---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/communal
offline_file: ""
offline_thumbnail: ""
uuid: 4002ff7b-b19b-47f3-b87c-50616fcb5826
updated: 1484310575
title: communal
categories:
    - Dictionary
---
communal
     adj 1: for or by a group rather than individuals; "dipping each his
            bread into a communal dish of stew"- Paul Roche; "a
            communal settlement in which all earnings and food
            were shared"; "a group effort"
     2: relating to a small administrative district or community;
        "communal elections in several European countries"
