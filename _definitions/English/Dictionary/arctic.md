---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arctic
offline_file: ""
offline_thumbnail: ""
uuid: b5a6c905-c696-44d3-8b50-c07a8713870a
updated: 1484310433
title: arctic
categories:
    - Dictionary
---
Arctic
     adj 1: at or near the north pole [syn: {north-polar}]
     2: of or relating to the Arctic; "Arctic circle"
     3: extremely cold; "an arctic climate";  "a frigid day"; "gelid
        waters of the North Atlantic"; "glacial winds"; "icy
        hands"; "polar weather" [syn: {frigid}, {gelid}, {glacial},
         {icy}, {polar}]
     n 1: the regions north of the Arctic Circle centered on the North
          Pole [syn: {Arctic Zone}, {North Frigid Zone}]
     2: a waterproof overshoe that protects shoes from water or snow
        [syn: {galosh}, {golosh}, {rubber}, {gumshoe}]
