---
version: 1
type: video
provider: YouTube
id: 6MyjFHd04R8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 580a0b92-2dfc-4e2e-a754-236b08f23f54
updated: 1486069645
title: 'II. Reflection of Truth - Amanda Darling (HD VISUALIZER)'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/6MyjFHd04R8/default.jpg
    - https://i3.ytimg.com/vi/6MyjFHd04R8/1.jpg
    - https://i3.ytimg.com/vi/6MyjFHd04R8/2.jpg
    - https://i3.ytimg.com/vi/6MyjFHd04R8/3.jpg
---
