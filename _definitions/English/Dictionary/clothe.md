---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clothe
offline_file: ""
offline_thumbnail: ""
uuid: 71e283af-dd3b-4f4c-ac2a-d70ed124f5c1
updated: 1484310146
title: clothe
categories:
    - Dictionary
---
clothe
     v 1: provide with clothes or put clothes on; "Parents must feed
          and dress their child" [syn: {dress}, {enclothe}, {garb},
           {raiment}, {tog}, {garment}, {habilitate}, {fit out}, {apparel}]
          [ant: {undress}]
     2: furnish with power or authority; of kings or emperors [syn:
        {invest}, {adorn}]
     [also: {clad}]
