---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tremendous
offline_file: ""
offline_thumbnail: ""
uuid: d14d9b66-22a1-4ad4-b096-dc4661df79ba
updated: 1484310453
title: tremendous
categories:
    - Dictionary
---
tremendous
     adj 1: extraordinarily large in size or extent or amount or power
            or degree; "an enormous boulder"; "enormous expenses";
            "tremendous sweeping plains"; "a tremendous fact in
            human experience; that a whole civilization should be
            dependent on technology"- Walter Lippman; "a plane
            took off with a tremendous noise" [syn: {enormous}]
     2: extraordinarily good; used especially as intensifiers; "a
        fantastic trip to the Orient"; "the film was fantastic!";
        "a howling success"; "a marvelous collection of rare
        books"; "had a rattling conversation about politics"; "a
        tremendous achievement" ...
