---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Moser%27s_worm_problem'
offline_file: ""
offline_thumbnail: ""
uuid: e7684d63-dd8e-4957-b35b-dac8ddca10e9
updated: 1484308611
title: "Moser's worm problem"
tags:
    - Examples
    - Solution properties
    - Known bounds
    - Notes
categories:
    - Discrete geometry
---
Moser's worm problem is an unsolved problem in geometry formulated by the Austrian-Canadian mathematician Leo Moser in 1966. The problem asks for the region of smallest area that can accommodate every plane curve of length 1. Here "accommodate" means that the curve may be rotated and translated to fit inside the region. In some variations of the problem, the region is restricted to be convex.
