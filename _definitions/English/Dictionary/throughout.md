---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/throughout
offline_file: ""
offline_thumbnail: ""
uuid: a8d309b9-ad8b-40a8-b815-883088f805cb
updated: 1484310323
title: throughout
categories:
    - Dictionary
---
throughout
     adv 1: from first to last; "the play was excellent end-to-end"
            [syn: {end-to-end}]
     2: used to refer to cited works [syn: {passim}]
