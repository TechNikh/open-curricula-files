---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mg
offline_file: ""
offline_thumbnail: ""
uuid: 83d1642d-bb25-4ab3-8c85-55268512ecbf
updated: 1484310303
title: mg
categories:
    - Dictionary
---
mg
     n 1: one thousandth (1/1,000) gram [syn: {milligram}]
     2: a light silver-white ductile bivalent metallic element; in
        pure form it burns with brilliant white flame; occurs
        naturally only in combination (as in magnesite and
        dolomite and carnallite and spinel and olivine) [syn: {magnesium},
         {atomic number 12}]
