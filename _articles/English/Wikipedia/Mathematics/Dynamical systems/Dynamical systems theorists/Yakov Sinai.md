---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yakov_Sinai
offline_file: ""
offline_thumbnail: ""
uuid: 43cb743c-468d-4a9e-bbce-f36bb4aa917e
updated: 1484308690
title: Yakov Sinai
tags:
    - Biography
    - Selected works
categories:
    - Dynamical systems theorists
---
Yakov Grigorevich Sinai (Russian: Я́ков Григо́рьевич Сина́й; born September 21, 1935) is a mathematician known for his work on dynamical systems. He contributed to the modern metric theory of dynamical systems and connected the world of deterministic (dynamical) systems with the world of probabilistic (stochastic) systems.[1] He has also worked on mathematical physics and probability theory.[2] His efforts have provided the groundwork for advances in the physical sciences.[1]
