---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Horrocks_bundle
offline_file: ""
offline_thumbnail: ""
uuid: 8e30ae25-c726-4809-a1ac-358911390441
updated: 1484309153
title: Horrocks bundle
categories:
    - Algebraic geometry
---
In algebraic geometry, Horrocks bundles are certain indecomposable rank 3 vector bundles (locally free sheaves) on 5-dimensional projective space, found by Horrocks (1978).
