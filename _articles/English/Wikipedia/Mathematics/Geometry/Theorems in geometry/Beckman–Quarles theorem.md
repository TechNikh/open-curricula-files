---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Beckman%E2%80%93Quarles_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 7037339a-47c5-4bc7-803f-9dd268005d4a
updated: 1484309105
title: Beckman–Quarles theorem
tags:
    - Formal statement
    - Counterexamples for other spaces
    - Related results
categories:
    - Theorems in geometry
---
In geometry, the Beckman–Quarles theorem, named after F. S. Beckman and D. A. Quarles, Jr., states that if a transformation of the Euclidean plane or a higher-dimensional Euclidean space preserves unit distances, then it preserves all distances. Equivalently, every automorphism of the unit distance graph of the plane must be an isometry of the plane. Beckman and Quarles published this result in 1953;[1] it was later rediscovered by other authors.[2][3]
