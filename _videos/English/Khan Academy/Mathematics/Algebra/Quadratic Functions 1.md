---
version: 1
type: video
provider: YouTube
id: CuPgmA7ytWA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Quadratic%20Functions%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3a53eb19-90dd-449f-9218-fe1cadf56906
updated: 1486069563
title: Quadratic Functions 1
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/CuPgmA7ytWA/default.jpg
    - https://i3.ytimg.com/vi/CuPgmA7ytWA/1.jpg
    - https://i3.ytimg.com/vi/CuPgmA7ytWA/2.jpg
    - https://i3.ytimg.com/vi/CuPgmA7ytWA/3.jpg
---
