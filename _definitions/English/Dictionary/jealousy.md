---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jealousy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484437441
title: jealousy
categories:
    - Dictionary
---
jealousy
     n 1: a feeling of jealous envy (especially of a rival) [syn: {green-eyed
          monster}]
     2: zealous vigilance; "cherish their official political freedom
        with fierce jealousy"-Paul Blanshard
