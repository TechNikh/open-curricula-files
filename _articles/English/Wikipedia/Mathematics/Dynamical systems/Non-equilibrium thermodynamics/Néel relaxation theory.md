---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/N%C3%A9el_relaxation_theory'
offline_file: ""
offline_thumbnail: ""
uuid: c3c9c3f9-aa7b-47d8-90ba-b578101d5a92
updated: 1484308802
title: Néel relaxation theory
tags:
    - Superparamagnetism
    - Mean transition time
    - Blocking temperature
categories:
    - Non-equilibrium thermodynamics
---
Néel relaxation theory is a theory developed by Louis Néel in 1949[1] to explain time-dependent magnetic phenomena known as magnetic viscosity. It is also called Néel-Arrhenius theory, after the Arrhenius equation, and Néel-Brown theory after a more rigorous derivation by William Fuller Brown, Jr.[2] Néel used his theory to develop a model of thermoremanent magnetization in single-domain ferromagnetic minerals that explained how these minerals could reliably record the geomagnetic field. He also modeled frequency-dependent susceptibility and alternating field demagnetization.
