---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/latest
offline_file: ""
offline_thumbnail: ""
uuid: 0c78bd2c-c561-42ff-9840-9c14ea5393ec
updated: 1484310240
title: latest
categories:
    - Dictionary
---
latest
     adj 1: up to the immediate present; most recent or most up-to-date;
            "the news is up-to-the-minute"; "the very latest
            scientific discoveries" [syn: {up-to-the-minute}]
     2: in accord with the most fashionable ideas or style; "wears
        only the latest style"; "the last thing in swimwear";
        "knows the newest dances"; "cutting-edge technology"; "a
        with-it boutique" [syn: {last}, {newest}, {up-to-date}, {cutting-edge},
         {with-it}]
