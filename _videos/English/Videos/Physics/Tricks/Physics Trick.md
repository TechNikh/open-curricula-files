---
version: 1
type: video
provider: YouTube
id: gzwdxnYoTTU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0ec172b1-afaf-44df-a46e-e16ca35480e5
updated: 1486069658
title: Physics Trick
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/gzwdxnYoTTU/default.jpg
    - https://i3.ytimg.com/vi/gzwdxnYoTTU/1.jpg
    - https://i3.ytimg.com/vi/gzwdxnYoTTU/2.jpg
    - https://i3.ytimg.com/vi/gzwdxnYoTTU/3.jpg
---
