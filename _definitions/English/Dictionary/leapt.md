---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leapt
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484482201
title: leapt
categories:
    - Dictionary
---
leap
     n 1: a light springing movement upwards or forwards [syn: {leaping},
           {spring}, {saltation}, {bound}, {bounce}]
     2: an abrupt transition; "a successful leap from college to the
        major leagues" [syn: {jump}, {saltation}]
     3: a sudden and decisive increase; "a jump in attendance" [syn:
         {jump}]
     4: the distance leaped (or to be leaped); "a leap of 10 feet"
     v 1: move forward by leaps and bounds; "The horse bounded across
          the meadow"; "The child leapt across the puddle"; "Can
          you jump over the fence?" [syn: {jump}, {bound}, {spring}]
     2: pass abruptly from one state or topic to another; "leap into
        fame"; "jump ...
