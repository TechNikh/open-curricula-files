---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mess
offline_file: ""
offline_thumbnail: ""
uuid: 9d11e7d0-2681-47f5-bb00-11b5092debd5
updated: 1484310547
title: mess
categories:
    - Dictionary
---
mess
     n 1: a state of confusion and disorderliness; "the house was a
          mess"; "she smoothed the mussiness of the bed" [syn: {messiness},
           {muss}, {mussiness}]
     2: informal terms for a difficult situation; "he got into a
        terrible fix"; "he made a muddle of his marriage" [syn: {fix},
         {hole}, {jam}, {muddle}, {pickle}, {kettle of fish}]
     3: soft semiliquid food; "a mess of porridge"
     4: a meal eaten by service personnel
     5: a (large) military dining room where service personnel eat
        or relax [syn: {mess hall}]
     6: (often followed by `of') a large number or amount or extent;
        "a batch of letters"; "a deal of trouble"; "a ...
