---
version: 1
type: video
provider: YouTube
id: rvnCClhs7sk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Light%20from%20Physics%20Perspective.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 43a3e11a-d42c-44fa-8d92-860ccd60fb78
updated: 1486069654
title: Light from Physics Perspective
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/rvnCClhs7sk/default.jpg
    - https://i3.ytimg.com/vi/rvnCClhs7sk/1.jpg
    - https://i3.ytimg.com/vi/rvnCClhs7sk/2.jpg
    - https://i3.ytimg.com/vi/rvnCClhs7sk/3.jpg
---
