---
version: 1
type: article
id: https://en.wikipedia.org/wiki/GEUP
offline_file: ""
offline_thumbnail: ""
uuid: a9ed0153-423f-4b07-ad18-bdfde2e53b95
updated: 1484309363
title: GEUP
categories:
    - Geometry stubs
---
GEUP is a commercial interactive geometry software program, similar to Cabri Geometry. Originally using the Spanish language, it was programmed by Ramón Alvarez Galván.[1] Recent versions include support for three-dimensional geometry.[2]
