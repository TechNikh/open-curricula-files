---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/questionnaire
offline_file: ""
offline_thumbnail: ""
uuid: 3589061c-8005-4972-8f9b-d11c44a0462f
updated: 1484310316
title: questionnaire
categories:
    - Dictionary
---
questionnaire
     n : a form containing a set of questions; submitted to people to
         gain statistical information
