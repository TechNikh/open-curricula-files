---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prey
offline_file: ""
offline_thumbnail: ""
uuid: 054861ab-e802-460c-b610-58f6cf5aea46
updated: 1484310273
title: prey
categories:
    - Dictionary
---
prey
     n 1: a person who is the aim of an attack (especially a victim of
          ridicule or exploitation) by some hostile person or
          influence; "he fell prey to muggers"; "everyone was fair
          game"; "the target of a manhunt" [syn: {quarry}, {target},
           {fair game}]
     2: animal hunted or caught for food [syn: {quarry}]
     v 1: profit from in an exploitatory manner; "He feeds on her
          insecurity" [syn: {feed}]
     2: prey on or hunt for; "These mammals predate certain eggs"
        [syn: {raven}, {predate}]
