---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polyhedral_symbol
offline_file: ""
offline_thumbnail: ""
uuid: 1c400bbf-b34a-4a4d-b2a3-dc7c1c476186
updated: 1484309279
title: Polyhedral symbol
tags:
    - 'Polyhedral symbols[1]'
    - 'Configuration index[1]'
    - T shaped (TS-3)
    - 'Seesaw (SS- 4)'
    - Square planar (SP-4)
    - Octahedral (OC-6)
    - Square pyramidal (SPY-4)
    - Square pyramidal (SPY-5)
    - Trigonal bipyramidal (TBPY-5)
    - Other bipyramidal structures (PBPY-7, HBPY-8 and HBPY-9)
categories:
    - Molecular geometry
---
The polyhedral symbol is sometimes used in coordination chemistry to indicate the approximate geometry of the coordinating atoms around the central atom. One or more italicised letters indicate the geometry, e.g. TP-3 which is followed by a number that gives the coordination number of the central atom.[1] The polyhedral symbol can be used in naming of compounds, in which case it is followed by the configuration index.[1]
