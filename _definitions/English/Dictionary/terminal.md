---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terminal
offline_file: ""
offline_thumbnail: ""
uuid: 2491dd83-b6c8-4a65-adb6-4f8d2f9f7541
updated: 1484310305
title: terminal
categories:
    - Dictionary
---
terminal
     adj 1: being or situated at an end; "the endmost pillar"; "terminal
            buds on a branch"; "a terminal station"; "the terminal
            syllable" [syn: {endmost}]
     2: of or relating to or situated at the ends of a delivery
        route; "freight pickup is a terminal service"; "terminal
        charges"
     3: relating to or occurring in a term or fixed period of time;
        "terminal examinations"; "terminal payments"
     4: occurring at or forming an end or termination; "his
        concluding words came as a surprise"; "the final chapter";
        "the last days of the dinosaurs"; "terminal leave" [syn: {concluding},
         {final}, {last}]
     5: ...
