---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irony
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484433001
title: irony
categories:
    - Dictionary
---
irony
     n 1: witty language used to convey insults or scorn; "he used
          sarcasm to upset his opponent"; "irony is wasted on the
          stupid"; "Satire is a sort of glass, wherein beholders
          do generally discover everybody's face but their
          own"--Johathan Swift [syn: {sarcasm}, {satire}, {caustic
          remark}]
     2: incongruity between what might be expected and what actually
        occurs; "the irony of Ireland's copying the nation she
        most hated"
     3: a trope that involves incongruity between what is expected
        and what occurs
