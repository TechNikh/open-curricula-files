---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/considerably
offline_file: ""
offline_thumbnail: ""
uuid: e0f8639c-cd15-4fd1-b7eb-8eb555cf80c5
updated: 1484310464
title: considerably
categories:
    - Dictionary
---
considerably
     adv : to a great extent or degree; "I'm afraid the film was well
           over budget"; "painting the room white made it seem
           considerably (or substantially) larger"; "the house has
           fallen considerably in value"; "the price went up
           substantially" [syn: {well}, {substantially}]
