---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peripheral
offline_file: ""
offline_thumbnail: ""
uuid: 83185e7a-3135-4dac-a799-76b15a3f17f9
updated: 1484310325
title: peripheral
categories:
    - Dictionary
---
peripheral
     adj 1: on or near an edge or constituting an outer boundary; the
            outer area; "Russia's peripheral provinces";
            "peripheral suburbs" [ant: {central}]
     2: related to the key issue but not of central importance; "a
        peripheral interest"; "energy is far from a peripheral
        issue in the economy"; "peripheral issues"
     n : (computer science) electronic equipment connected by cable
         to the CPU of a computer; "disk drives and printers are
         important peripherals" [syn: {computer peripheral}, {peripheral
         device}]
