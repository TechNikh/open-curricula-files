---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/operator
offline_file: ""
offline_thumbnail: ""
uuid: 6bb98bbf-973d-4bcd-b082-a714be531a03
updated: 1484310521
title: operator
categories:
    - Dictionary
---
operator
     n 1: (mathematics) a symbol that represents a function from
          functions to functions; "the integral operator"
     2: an agent that operates some apparatus or machine; "the
        operator of the switchboard" [syn: {manipulator}]
     3: someone who owns or operates a business; "who is the
        operator of this franchise?"
     4: a shrewd or unscrupulous person who knows how to circumvent
        difficulties [syn: {hustler}, {wheeler dealer}]
     5: a speculator who trades aggressively on stock or commodity
        markets
