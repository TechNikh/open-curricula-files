---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consequently
offline_file: ""
offline_thumbnail: ""
uuid: ea7a4c63-1a90-4c10-8725-ce3f668cfb72
updated: 1484310469
title: consequently
categories:
    - Dictionary
---
consequently
     adv 1: (sentence connectors) because of the reason given;
            "consequently, he didn't do it"; "continued to have
            severe headaches and accordingly returned to the
            doctor" [syn: {accordingly}]
     2: as a consequence; "he had good reason to be grateful for the
        opportunities which they had made available to him and
        which consequently led to the good position he now held"
        [syn: {therefore}]
