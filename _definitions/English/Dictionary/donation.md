---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/donation
offline_file: ""
offline_thumbnail: ""
uuid: 0f3d4eaf-b1c6-4de2-ac62-b5dc95422ba1
updated: 1484310158
title: donation
categories:
    - Dictionary
---
donation
     n 1: a voluntary gift (as of money or service or ideas) made to
          some worthwhile cause [syn: {contribution}]
     2: act of giving in common with others for a common purpose
        especially to a charity [syn: {contribution}]
