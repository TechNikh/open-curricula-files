---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/freed
offline_file: ""
offline_thumbnail: ""
uuid: 0764b2a5-de54-4f3f-90df-070efe53dc18
updated: 1484310589
title: freed
categories:
    - Dictionary
---
freed
     adj 1: freed from bondage [syn: {emancipated}, {liberated}]
     2: having become freed from entanglement; disengaged [syn: {disentangled},
         {extricated}]
