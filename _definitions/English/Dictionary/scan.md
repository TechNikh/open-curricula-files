---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scan
offline_file: ""
offline_thumbnail: ""
uuid: 3e37e6f8-8e7a-425d-b3df-71cd003780fd
updated: 1484310479
title: scan
categories:
    - Dictionary
---
scan
     n 1: the act of scanning; systematic examination of a prescribed
          region; "he made a thorough scan of the beach with his
          binoculars"
     2: an image produced by scanning; "he analyzed the brain scan";
        "you could see the tumor in the CAT scan" [syn: {CAT scan}]
     v 1: examine minutely or intensely; "the surgeon scanned the
          X-ray"
     2: examine hastily; "She scanned the newspaper headlines while
        waiting for the taxi" [syn: {skim}, {rake}, {glance over},
         {run down}]
     3: make a wide, sweeping search of; "The beams scanned the
        night sky"
     4: conform to a metrical pattern
     5: move a light beam over; in ...
