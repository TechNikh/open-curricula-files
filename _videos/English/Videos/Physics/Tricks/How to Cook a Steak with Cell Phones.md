---
version: 1
type: video
provider: YouTube
id: LsJuZ-kn74o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f10273ea-af77-436a-913c-4f6114fa120c
updated: 1486069658
title: How to Cook a Steak with Cell Phones
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/LsJuZ-kn74o/default.jpg
    - https://i3.ytimg.com/vi/LsJuZ-kn74o/1.jpg
    - https://i3.ytimg.com/vi/LsJuZ-kn74o/2.jpg
    - https://i3.ytimg.com/vi/LsJuZ-kn74o/3.jpg
---
