---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hunted
offline_file: ""
offline_thumbnail: ""
uuid: 771e4180-cf6d-40b2-be1f-fc7e1f0ab975
updated: 1484310475
title: hunted
categories:
    - Dictionary
---
hunted
     adj : reflecting the fear or terror of one who is hunted; "the
           hopeless hunted look on the prisoner's face"; "a
           glitter of apprehension in her hunted eyes"
