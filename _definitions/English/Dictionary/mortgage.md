---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mortgage
offline_file: ""
offline_thumbnail: ""
uuid: b04c5a87-6969-438b-b723-8a2eb2563246
updated: 1484310541
title: mortgage
categories:
    - Dictionary
---
mortgage
     n : a conditional conveyance of property as security for the
         repayment of a loan
     v : put up as security or collateral
