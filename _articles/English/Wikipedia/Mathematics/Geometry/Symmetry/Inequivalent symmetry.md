---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inequivalent_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 75bf49d3-50e1-4263-9b56-a5a8544e0fa4
updated: 1484309311
title: Inequivalent symmetry
categories:
    - Symmetry
---
Two symmetrical patterns are considered to be equivalent if they have exactly the same types of symmetry. As recently as 1891, it was finally proved that there are only 17 inequivalent symmetry patterns in the plane. However, the Moors, who lived in Spain from the eighth to the fifteenth centuries, were aware of all 17 types of symmetry patterns. Examples of the patterns were used to decorate the Alhambra, a Moorish fortress in Granada.
