---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chemists
offline_file: ""
offline_thumbnail: ""
uuid: 3912d571-3105-4eb1-8a61-1d0991d8531e
updated: 1484310393
title: chemists
categories:
    - Dictionary
---
chemist's
     n : a retail shop where medicine and other articles are sold
         [syn: {drugstore}, {apothecary's shop}, {chemist's shop},
          {pharmacy}]
