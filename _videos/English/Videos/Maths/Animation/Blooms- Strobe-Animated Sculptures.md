---
version: 1
type: video
provider: YouTube
id: nom7NiTLrFg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b01dec15-0594-4352-b5ab-d23b6bf207a8
updated: 1486069644
title: 'Blooms- Strobe-Animated Sculptures'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/nom7NiTLrFg/default.jpg
    - https://i3.ytimg.com/vi/nom7NiTLrFg/1.jpg
    - https://i3.ytimg.com/vi/nom7NiTLrFg/2.jpg
    - https://i3.ytimg.com/vi/nom7NiTLrFg/3.jpg
---
