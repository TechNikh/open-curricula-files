---
version: 1
type: video
provider: YouTube
id: ETzngG8N3AU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Introduction%20to%20singular%20and%20plural%20nouns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 49b097ac-41ce-4794-9e61-9bc54eadab8b
updated: 1486069601
title: Introduction to singular and plural nouns
tags:
    - Education
    - online learning
    - learning
    - lessons
    - grammar
    - nouns
    - plural
    - regular plural
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/ETzngG8N3AU/default.jpg
    - https://i3.ytimg.com/vi/ETzngG8N3AU/1.jpg
    - https://i3.ytimg.com/vi/ETzngG8N3AU/2.jpg
    - https://i3.ytimg.com/vi/ETzngG8N3AU/3.jpg
---
Learn the difference between singular and plural nouns. 

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/e/plural-and-singular-nouns?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/v/common-and-proper-nouns?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/v/introduction-to-nouns-the-parts-of-speech-grammar-khan-academy?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
