---
version: 1
type: video
provider: YouTube
id: T0IOrRETWhI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Geometry/Right%20triangles%20and%20trigonometry/Pythagorean%20theorem%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a5b4e985-a87e-4b48-bbc7-28814212d89a
updated: 1486069559
title: Pythagorean theorem 3
categories:
    - Right triangles and trigonometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/T0IOrRETWhI/default.jpg
    - https://i3.ytimg.com/vi/T0IOrRETWhI/1.jpg
    - https://i3.ytimg.com/vi/T0IOrRETWhI/2.jpg
    - https://i3.ytimg.com/vi/T0IOrRETWhI/3.jpg
---
