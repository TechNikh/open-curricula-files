---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/doubling
offline_file: ""
offline_thumbnail: ""
uuid: 5acde36e-1275-4999-b2e5-5b80a316ec64
updated: 1484310146
title: doubling
categories:
    - Dictionary
---
doubling
     n 1: increase by a factor of two; "doubling with a computer took
          no time at all"
     2: raising the stakes in a card game by a factor of 2; "I
        decided his double was a bluff" [syn: {double}]
