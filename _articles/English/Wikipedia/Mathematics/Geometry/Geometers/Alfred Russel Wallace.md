---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alfred_Russel_Wallace
offline_file: ""
offline_thumbnail: ""
uuid: 18376bf4-fb41-4e7a-beea-1818072903f7
updated: 1484309107
title: Alfred Russel Wallace
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Wallace_map_archipelago.jpg
tags:
    - Biography
    - Early life
    - Exploration and study of the natural world
    - Return to England, marriage and children
    - Financial struggles
    - Social activism
    - Further scientific work
    - Death
    - Theory of evolution
    - Early evolutionary thinking
    - Natural selection and Darwin
    - Defence of Darwin and his ideas
    - "Differences between Darwin's and Wallace's ideas on natural selection"
    - Warning colouration and sexual selection
    - Wallace effect
    - >
        Application of theory to humans, and role of teleology in
        evolution
    - "Assessment of Wallace's role in history of evolutionary theory"
    - Other scientific contributions
    - Biogeography and ecology
    - Environmental issues
    - Astrobiology
    - Controversies
    - Spiritualism
    - Flat Earth wager
    - Anti-vaccination campaign
    - Legacy and historical perception
    - Awards, honours, and memorials
    - Writings by Wallace
    - Selected books
    - Selected papers
    - Bird specimens collected by Wallace
categories:
    - Geometers
---
Alfred Russel Wallace OM FRS (8 January 1823 – 7 November 1913) was a British naturalist, explorer, geographer, anthropologist, and biologist. He is best known for independently conceiving the theory of evolution through natural selection; his paper on the subject was jointly published with some of Charles Darwin's writings in 1858.[2] This prompted Darwin to publish his own ideas in On the Origin of Species. Wallace did extensive fieldwork, first in the Amazon River basin and then in the Malay Archipelago, where he identified the faunal divide now termed the Wallace Line, which separates the Indonesian archipelago into two distinct parts: a western portion in which the animals are ...
