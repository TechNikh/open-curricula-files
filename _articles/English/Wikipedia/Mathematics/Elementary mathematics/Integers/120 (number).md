---
version: 1
type: article
id: https://en.wikipedia.org/wiki/120_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 16a644c3-790d-444b-b557-7fca792b4b37
updated: 1484308977
title: 120 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/246px-Schlegel_wireframe_120-cell.png
tags:
    - In mathematics
    - In science
    - In religion
    - In sports
    - In other fields
categories:
    - Integers
---
In English and other Germanic languages, it was also formerly known as "one hundred". This "hundred" of six score is now obsolete, but is described as the long hundred or great hundred in historical contexts.
