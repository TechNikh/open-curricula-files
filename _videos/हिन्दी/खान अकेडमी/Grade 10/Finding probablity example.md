---
version: 1
type: video
provider: YouTube
id: npuzzP_BpGA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9b7ae101-9fab-403f-85e5-4fcfa904925f
updated: 1486069621
title: Finding probablity example
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/npuzzP_BpGA/default.jpg
    - https://i3.ytimg.com/vi/npuzzP_BpGA/1.jpg
    - https://i3.ytimg.com/vi/npuzzP_BpGA/2.jpg
    - https://i3.ytimg.com/vi/npuzzP_BpGA/3.jpg
---
