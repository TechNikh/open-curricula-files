---
version: 1
type: video
provider: YouTube
id: 8kK2zwjRV0M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/DNA%20Structure%20and%20Replication-%20Crash%20Course%20Biology%20%2310.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b8a58b09-8b95-4d49-8f7f-e42cba4245c0
updated: 1486069678
title: 'DNA Structure and Replication- Crash Course Biology #10'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/8kK2zwjRV0M/default.jpg
    - https://i3.ytimg.com/vi/8kK2zwjRV0M/1.jpg
    - https://i3.ytimg.com/vi/8kK2zwjRV0M/2.jpg
    - https://i3.ytimg.com/vi/8kK2zwjRV0M/3.jpg
---
