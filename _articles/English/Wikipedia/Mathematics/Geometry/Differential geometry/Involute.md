---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Involute
offline_file: ""
offline_thumbnail: ""
uuid: eb17882f-18fe-4138-994c-4379a261ea0e
updated: 1484309224
title: Involute
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Animated_involute_of_circle.gif
tags:
    - Involute of a Parametrically Defined Curve
    - Examples
    - Involute of a circle
    - Curve length
    - Application
    - Involute of a catenary
    - Involute of a cycloid
    - Application
categories:
    - Differential geometry
---
In the differential geometry of curves, an involute (also known as evolvent) is a curve obtained from another given curve by attaching an imaginary taut string to the given curve and tracing its free end as it is wound onto that given curve; or in reverse, unwound. It is a roulette wherein the rolling curve is a straight line containing the generating point. For example, an involute approximates the path followed by a tetherball as the connecting tether is wound around the center pole. If the center pole has a circular cross-section, then the curve is an involute of a circle.
