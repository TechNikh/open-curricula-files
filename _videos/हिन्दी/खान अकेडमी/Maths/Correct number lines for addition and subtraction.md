---
version: 1
type: video
provider: YouTube
id: wYu9sXp6CGw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c3ee9b25-2565-4f64-bb31-2a8ac82955e1
updated: 1486069616
title: Correct number lines for addition and subtraction
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/wYu9sXp6CGw/default.jpg
    - https://i3.ytimg.com/vi/wYu9sXp6CGw/1.jpg
    - https://i3.ytimg.com/vi/wYu9sXp6CGw/2.jpg
    - https://i3.ytimg.com/vi/wYu9sXp6CGw/3.jpg
---
