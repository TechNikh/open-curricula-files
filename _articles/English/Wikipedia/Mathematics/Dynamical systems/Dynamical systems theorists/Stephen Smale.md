---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stephen_Smale
offline_file: ""
offline_thumbnail: ""
uuid: 3e1af36f-c034-4725-979e-3042e217f75d
updated: 1484308692
title: Stephen Smale
tags:
    - Education and career
    - Important publications
categories:
    - Dynamical systems theorists
---
Stephen Smale (born July 15, 1930) is an American mathematician from Flint, Michigan. His research concerns topology, dynamical systems and mathematical economics. He was awarded the Fields Medal in 1966[2] and spent more than three decades on the mathematics faculty of the University of California, Berkeley (1960–1961 and 1964–1995).
