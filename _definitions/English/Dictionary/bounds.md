---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bounds
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484546762
title: bounds
categories:
    - Dictionary
---
bounds
     n 1: the line or plane indicating the limit or extent of
          something [syn: {boundary}, {bound}]
     2: the greatest possible degree of something; "what he did was
        beyond the bounds of acceptable behavior"; "to the limit
        of his ability" [syn: {limit}, {boundary}]
