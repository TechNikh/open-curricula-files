---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/watery
offline_file: ""
offline_thumbnail: ""
uuid: 114e4ffe-60b0-4960-b754-d1b3cf8cfa3e
updated: 1484310344
title: watery
categories:
    - Dictionary
---
watery
     adj 1: filled with water; "watery soil"
     2: wet with secreted or exuded moisture such as sweat or blood
        or tears; "wiped his reeking neck" [syn: {reeking}, {dripping}]
     3: filled or brimming with tears; "swimming eyes"; "watery
        eyes"; "sorrow made the eyes of many grow liquid" [syn: {liquid},
         {swimming}]
     4: relating to or resembling or consisting of water; "a watery
        grave"
     5: overly diluted; thin and insipid; "washy coffee"; "watery
        milk"; "weak tea" [syn: {washy}, {weak}]
