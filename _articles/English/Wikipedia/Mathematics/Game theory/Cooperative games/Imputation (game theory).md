---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Imputation_(game_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 7d67d1ca-a60e-4bd8-a683-898b3e7be63c
updated: 1484308524
title: Imputation (game theory)
tags:
    - Example
    - Properties
    - Time consistency in dynamic games
categories:
    - Cooperative games
---
In fully cooperative games players act efficiently when they form a single coalition, the grand coalition. The focus of the game is to find acceptable distributions of the payoff of the grand coalition. Distributions where a player receives less than it could obtain on its own, without cooperating with anyone else, are unacceptable - a condition known as individual rationality. Imputations are distributions that are efficient and are individually rational.
