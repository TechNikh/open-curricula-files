---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pater
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484522041
title: pater
categories:
    - Dictionary
---
pater
     n : an informal term for a father; probably derived from baby
         talk [syn: {dad}, {dada}, {daddy}, {pa}, {papa}, {pappa},
          {pop}]
