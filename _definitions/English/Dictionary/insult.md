---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insult
offline_file: ""
offline_thumbnail: ""
uuid: 88c06cbc-a2bc-4ef4-8f07-7aa27081c379
updated: 1484310192
title: insult
categories:
    - Dictionary
---
insult
     n 1: a rude expression intended to offend or hurt; "when a
          student made a stupid mistake he spared them no abuse";
          "they yelled insults at the visiting team" [syn: {abuse},
           {revilement}, {contumely}, {vilification}]
     2: a deliberately offensive act or something producing the
        effect of an affront; "turning his back on me was a
        deliberate insult" [syn: {affront}]
     v : treat, mention, or speak to rudely; "He insulted her with
         his rude remarks"; "the student who had betrayed his
         classmate was dissed by everyone" [syn: {diss}, {affront}]
