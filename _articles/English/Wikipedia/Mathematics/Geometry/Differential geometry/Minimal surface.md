---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minimal_surface
offline_file: ""
offline_thumbnail: ""
uuid: 3e8ab597-8c8f-4695-880e-e684be004ab5
updated: 1484309230
title: Minimal surface
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Bulle_de_savon_h%25C3%25A9lico%25C3%25AFde.PNG'
tags:
    - Definitions
    - History
    - Examples
    - Generalisations and links to other fields
categories:
    - Differential geometry
---
In mathematics, a minimal surface is a surface that locally minimizes its area. This is equivalent to (see definitions below) having a mean curvature of zero.
