---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/haunted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484517421
title: haunted
categories:
    - Dictionary
---
haunted
     adj 1: having or showing excessive or compulsive concern with
            something; "became more and more haunted by the stupid
            riddle"; "was absolutely obsessed with the girl"; "got
            no help from his wife who was preoccupied with the
            children"; "he was taken up in worry for the old
            woman" [syn: {obsessed}, {preoccupied}, {taken up(p)}]
     2: showing emotional affliction or disquiet; "her expression
        became progressively more haunted"
     3: inhabited by or as if by apparitions; "a haunted house"
