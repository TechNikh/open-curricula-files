---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shaker
offline_file: ""
offline_thumbnail: ""
uuid: acc37d29-69b2-4aba-ba6d-a20f4cd7dc58
updated: 1484310403
title: shaker
categories:
    - Dictionary
---
shaker
     n 1: a person who wields power and influence; "a shaker of
          traditional beliefs"; "movers and shakers in the
          business world" [syn: {mover and shaker}]
     2: a member of Christian group practicing celibacy and communal
        living and common possession of property and separation
        from the world
     3: a container in which something can be shaken
