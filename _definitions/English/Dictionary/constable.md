---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/constable
offline_file: ""
offline_thumbnail: ""
uuid: 14f622c1-9d6f-45bd-a88b-5f5bbb5eba2a
updated: 1484310459
title: constable
categories:
    - Dictionary
---
constable
     n 1: a lawman with less authority and jurisdiction than a sheriff
     2: English landscape painter (1776-1837) [syn: {John Constable}]
     3: a police officer of the lowest rank [syn: {police constable}]
