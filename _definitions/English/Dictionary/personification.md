---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/personification
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484506261
title: personification
categories:
    - Dictionary
---
personification
     n 1: a person who represents an abstract quality; "she is the
          personification of optimism"
     2: representing an abstract quality or idea as a person or
        creature [syn: {prosopopoeia}]
     3: the act of attributing human characteristics to abstract
        ideas etc. [syn: {incarnation}]
