---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boyle
offline_file: ""
offline_thumbnail: ""
uuid: d4160f10-95bb-45c1-b98a-f1eb4b7c5a4c
updated: 1484310393
title: boyle
categories:
    - Dictionary
---
Boyle
     n 1: Irish chemist who established that air has weight and whose
          definitions of chemical elements and chemical reactions
          helped to dissociate chemistry from alchemy (1627-1691)
          [syn: {Robert Boyle}]
     2: United States writer (1902-1992) [syn: {Kay Boyle}]
