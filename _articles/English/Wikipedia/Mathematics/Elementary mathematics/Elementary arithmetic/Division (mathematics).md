---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Division_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 4aba7a55-6253-40b7-8be2-aa0538c6341c
updated: 1484308913
title: Division (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Divide20by4.svg.png
tags:
    - Notation
    - Computing
    - Manual methods
    - By computer or with computer assistance
    - Properties
    - Euclidean division
    - Of integers
    - Of rational numbers
    - Of real numbers
    - By zero
    - Of complex numbers
    - Of polynomials
    - Of matrices
    - Left and right division
    - Pseudoinverse
    - In abstract algebra
    - calculus
    - Notes
categories:
    - Elementary arithmetic
---
Division is one of the four basic operations of arithmetic, the others being addition, subtraction, and multiplication. The division of two natural numbers is the process of calculating the number of times one number is contained within one another.[1]:7 For example, in the picture on the right, the 20 apples are divided into groups of five apples, and there exist four groups, meaning that five can be contained within 20 four times, or 20 ÷ 5 = 4. Division can also be thought of as the process of evaluating a fraction, and fractional notation (a/b and  a⁄b) is commonly used to represent division.[2]
