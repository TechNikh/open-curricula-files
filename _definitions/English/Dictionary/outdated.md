---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outdated
offline_file: ""
offline_thumbnail: ""
uuid: d8234ddb-8e1f-4449-88c7-4a4e4d83878a
updated: 1484310464
title: outdated
categories:
    - Dictionary
---
outdated
     adj : old; no longer in use or valid or fashionable; "obsolete
           words"; "an obsolete locomotive"; "outdated equipment";
           "superannuated laws"; "out-of-date ideas" [syn: {obsolete},
            {out-of-date}, {superannuated}]
