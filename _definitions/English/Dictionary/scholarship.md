---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scholarship
offline_file: ""
offline_thumbnail: ""
uuid: b36e7db4-d6db-4ce2-8a72-00356c60df85
updated: 1484310431
title: scholarship
categories:
    - Dictionary
---
scholarship
     n 1: financial aid provided to a student on the basis of academic
          merit
     2: profound scholarly knowledge [syn: {eruditeness}, {erudition},
         {learnedness}, {learning}, {encyclopedism}, {encyclopaedism}]
