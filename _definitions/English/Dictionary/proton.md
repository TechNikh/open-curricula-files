---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proton
offline_file: ""
offline_thumbnail: ""
uuid: 6a0a08e8-ac7b-43a3-bf72-6d858b114d4c
updated: 1484310401
title: proton
categories:
    - Dictionary
---
proton
     n : a stable particle with positive charge equal to the negative
         charge of an electron
