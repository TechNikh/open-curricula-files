---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/socialising
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415661
title: socialising
categories:
    - Dictionary
---
socialising
     n : the act of meeting for social purposes; "there was too much
         socialization with the enlisted men" [syn: {socialization},
          {socialisation}, {socializing}]
