---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graphic_communication
offline_file: ""
offline_thumbnail: ""
uuid: d28647e1-ca49-4868-b644-86c3ee182331
updated: 1484309327
title: Graphic communication
tags:
    - Overview
    - History
    - Graphic communication topics
    - Graphics
    - Communication
    - Visual communication
    - Communication design
    - Graphic design
    - Graphical Representation
categories:
    - Technical drawing
---
Graphic communication as the name suggests is communication using graphic elements. These elements include symbols such as glyphs and icons, images such as drawings and photographs, and can include the passive contributions of substrate, color and surroundings. It is the process of creating, producing, and distributing material incorporating words and images to convey data, concepts, and emotions.[1]
