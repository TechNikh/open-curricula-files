---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/burma
offline_file: ""
offline_thumbnail: ""
uuid: 1e640dfd-1131-40a7-a3d3-db57f6b2d7a0
updated: 1484310587
title: burma
categories:
    - Dictionary
---
Burma
     n : a mountainous republic in southeastern Asia on the Bay of
         Bengal; "much opium is grown in Myanmar" [syn: {Myanmar},
          {Union of Burma}]
