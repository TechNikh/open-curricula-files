---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Apeirogonal_hosohedron
offline_file: ""
offline_thumbnail: ""
uuid: 8e6820c6-866b-43b4-b170-71680a5d360a
updated: 1484309352
title: Apeirogonal hosohedron
tags:
    - Related tilings and polyhedra
    - Notes
categories:
    - Geometry stubs
---
In geometry, an apeirogonal hosohedron or infinite hosohedron[1]is a tiling of the plane consisting of two vertices at infinity. It may be considered an improper regular tiling of the Euclidean plane, with Schläfli symbol {2,∞}.
