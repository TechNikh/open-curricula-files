---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crooks_fluctuation_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 5bf865bb-5c51-4c15-995b-8cb54f0c5f53
updated: 1484308801
title: Crooks fluctuation theorem
categories:
    - Non-equilibrium thermodynamics
---
The Crooks Fluctuation Theorem (CFT), sometimes known as the Crooks Equation,[1] is an equation in statistical mechanics that relates the work done on a system during a non-equilibrium transformation to the free energy difference between the final and the initial state of the transformation. During the non-equilibrium transformation the system is at constant volume and in contact with a heat reservoir. The CFT is named after the chemist Gavin E. Crooks (then at University of California) who discovered it in 1998.
