---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phonovoltaic
offline_file: ""
offline_thumbnail: ""
uuid: 7ca15fa5-cb15-40ea-ae39-ec00c970a79b
updated: 1484308853
title: Phonovoltaic
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Phonovoltaic_Cell_and_Energy_Diagram_0.png
tags:
    - Satisfying the laws of thermodynamics
    - >
        Non-equilibrium optical phonon population and the nanoscale
        requirement
    - Entropy generation and efficiency
    - The electron-phonon coupling
    - The phonon-phonon coupling
    - pV materials
categories:
    - Thermodynamics
---
A phonovoltaic (pV) cell converts vibrational (phonon) energy into a direct current much like the photovoltaic effect in a photovoltaic (PV) cell converts light (photon) into power. That is, it uses a p-n junction to separate the electrons and holes generated as valence electrons absorb optical phonons more energetic than the band gap, and then collects them in the metallic contacts for use in a circuit.[1] The pV cell is an application of heat transfer physics[2] and competes with other thermal energy harvesting devices like the thermoelectric generator.
