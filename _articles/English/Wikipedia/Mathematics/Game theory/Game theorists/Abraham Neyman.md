---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abraham_Neyman
offline_file: ""
offline_thumbnail: ""
uuid: dcf2b201-5d00-4769-b827-93b121412288
updated: 1484308506
title: Abraham Neyman
tags:
    - Biography
    - Awards and honors
    - Research contributions
    - Stochastic games
    - Repeated Games
    - The Shapley value
    - Other
    - Business involvements
categories:
    - Game theorists
---
Abraham Neyman (born June 14, 1949, Israel) is an Israeli mathematician and game theorist, Professor of Mathematics at the Federmann Center for the Study of Rationality[1] and the Einstein Institute of Mathematics[2] at the Hebrew University of Jerusalem in Israel. He is currently the president of the Israeli Chapter of the Game Theory Society (2014–2016).[3]
