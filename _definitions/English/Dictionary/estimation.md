---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/estimation
offline_file: ""
offline_thumbnail: ""
uuid: e4ee06a9-b8b5-4d1f-90d6-b7147161617b
updated: 1484310220
title: estimation
categories:
    - Dictionary
---
estimation
     n 1: an approximate calculation of quantity or degree or worth;
          "an estimate of what it would cost"; "a rough idea how
          long it would take" [syn: {estimate}, {approximation}, {idea}]
     2: a document appraising the value of something (as for
        insurance or taxation) [syn: {appraisal}, {estimate}]
     3: the respect with which a person is held; "they had a high
        estimation of his ability" [syn: {estimate}]
     4: a judgment of the qualities of something or somebody; "many
        factors are involved in any estimate of human life"; "in
        my estimation the boy is innocent" [syn: {estimate}]
