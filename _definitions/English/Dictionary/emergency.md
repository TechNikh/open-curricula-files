---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emergency
offline_file: ""
offline_thumbnail: ""
uuid: 38e390c9-f878-41e7-8e34-e13390c306a2
updated: 1484310609
title: emergency
categories:
    - Dictionary
---
emergency
     n 1: a sudden unforeseen crisis (usually involving danger) that
          requires immediate action; "he never knew what to do in
          an emergency" [syn: {exigency}, {pinch}]
     2: a state in which martial law applies; "the governor declared
        a state of emergency"
     3: a brake operated by hand; usually operates by mechanical
        linkage [syn: {hand brake}, {emergency brake}, {parking
        brake}]
