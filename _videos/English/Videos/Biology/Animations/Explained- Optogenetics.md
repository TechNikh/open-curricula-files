---
version: 1
type: video
provider: YouTube
id: Nb07TLkJ3Ww
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Explained-%20Optogenetics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a936a92c-2627-4438-83e3-738095181b25
updated: 1486069672
title: 'Explained- Optogenetics'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Nb07TLkJ3Ww/default.jpg
    - https://i3.ytimg.com/vi/Nb07TLkJ3Ww/1.jpg
    - https://i3.ytimg.com/vi/Nb07TLkJ3Ww/2.jpg
    - https://i3.ytimg.com/vi/Nb07TLkJ3Ww/3.jpg
---
