---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/natural
offline_file: ""
offline_thumbnail: ""
uuid: 9dc988aa-39ba-4978-8da7-808232e76993
updated: 1484310313
title: natural
categories:
    - Dictionary
---
natural
     adj 1: in accordance with nature; relating to or concerning nature;
            "a very natural development"; "our natural
            environment"; "natural science"; "natural resources";
            "natural cliffs"; "natural phenomena" [ant: {unnatural}]
     2: existing in or produced by nature; not artificial or
        imitation; "a natural pearl"; "natural gas"; "natural
        silk"; "natural blonde hair"; "a natural sweetener";
        "natural fertilizers" [ant: {artificial}]
     3: existing in or in conformity with nature or the observable
        world; neither supernatural nor magical; "a perfectly
        natural explanation" [ant: {supernatural}]
     4: ...
