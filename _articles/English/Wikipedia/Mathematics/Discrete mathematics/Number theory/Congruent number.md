---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Congruent_number
offline_file: ""
offline_thumbnail: ""
uuid: a987b53e-6f02-4b4c-a8a0-22a8b7d96b4f
updated: 1484308630
title: Congruent number
tags:
    - Congruent number problem
    - Relation to elliptic curves
    - Current progress
categories:
    - Number theory
---
In mathematics, a congruent number is a positive integer that is the area of a right triangle with three rational number sides.[1] A more general definition includes all positive rational numbers with this property.[2]
