---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Property_testing
offline_file: ""
offline_thumbnail: ""
uuid: a5b6592f-e104-40e7-915a-8d581014a490
updated: 1484308688
title: Property testing
categories:
    - Theoretical computer science
---
In computer science, a property testing algorithm for a decision problem is an algorithm whose query complexity to its input is much smaller than the instance size of the problem. Typically property testing algorithms are used to decide if some mathematical object (such as a graph or a boolean function) has a "global" property, or is "far" from having this property, using only a small number of "local" queries to the object.
