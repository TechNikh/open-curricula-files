---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cost-sharing_mechanism
offline_file: ""
offline_thumbnail: ""
uuid: 651ac0c1-2d48-451e-af60-7f5d36c39482
updated: 1484309046
title: Cost-sharing mechanism
tags:
    - Definitions
    - Budget-balanced mechanisms
    - Socially-efficient mechanisms
categories:
    - Mechanism design
---
In economics and mechanism design, a cost-sharing mechanism is a process by which several agents decide on the scope of a public service, and how much each agent should pay for the service.
