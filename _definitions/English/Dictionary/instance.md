---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instance
offline_file: ""
offline_thumbnail: ""
uuid: d9c5cc03-0a08-4560-b156-8409b0280826
updated: 1484310273
title: instance
categories:
    - Dictionary
---
instance
     n 1: an occurrence of something; "it was a case of bad judgment";
          "another instance occurred yesterday"; "but there is
          always the famous example of the Smiths" [syn: {case}, {example}]
     2: an item of information that is representative of a type;
        "this patient provides a typical example of the syndrome";
        "there is an example on page 10" [syn: {example}, {illustration},
         {representative}]
     v : clarify by giving an example of [syn: {exemplify}, {illustrate}]
