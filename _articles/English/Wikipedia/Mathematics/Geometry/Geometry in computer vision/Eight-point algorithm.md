---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eight-point_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: 6dc67864-c10e-40a8-b7e6-c27756c59e1f
updated: 1484309181
title: Eight-point algorithm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Epipolar_Geometry1.svg.png
tags:
    - Coplanarity constraint
    - The basic algorithm
    - 'Step 1: Formulating a homogeneous linear equation'
    - 'Step 2: Solving the equation'
    - 'Step 3: Enforcing the internal constraint'
    - Determining R and t from E
    - The normalized eight-point algorithm
    - The problem
    - "What's causing the problem"
    - How it can be solved
    - Using fewer than eight points
categories:
    - Geometry in computer vision
---
The eight-point algorithm is an algorithm used in computer vision to estimate the essential matrix or the fundamental matrix related to a stereo camera pair from a set of corresponding image points. It was introduced by Christopher Longuet-Higgins in 1981 for the case of the essential matrix. In theory, this algorithm can be used also for the fundamental matrix, but in practice the normalized eight-point algorithm, described by Richard Hartley in 1997, is better suited for this case.
