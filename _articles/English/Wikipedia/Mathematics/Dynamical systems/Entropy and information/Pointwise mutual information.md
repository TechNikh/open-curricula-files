---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pointwise_mutual_information
offline_file: ""
offline_thumbnail: ""
uuid: 7ee7d975-cb89-48cc-8ba3-d8d7ca090814
updated: 1484308773
title: Pointwise mutual information
tags:
    - Definition
    - Similarities to mutual information
    - Normalized pointwise mutual information (npmi)
    - PMI Variants
    - Chain-rule for pmi
    - Applications
categories:
    - Entropy and information
---
Pointwise mutual information (PMI),[1] or point mutual information, is a measure of association used in information theory and statistics. In contrast to mutual information (MI) which builds upon PMI, it refers to single events, whereas MI refers to the average of all possible events.
