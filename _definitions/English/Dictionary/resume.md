---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resume
offline_file: ""
offline_thumbnail: ""
uuid: 6400a6db-75c1-403f-a24c-5ea1173b7eb6
updated: 1484310176
title: resume
categories:
    - Dictionary
---
resume
     n 1: short descriptive summary (of events) [syn: {sketch}, {survey}]
     2: a summary of your academic and work history [syn: {curriculum
        vitae}, {CV}]
     v 1: take up or begin anew; "We resumed the negotiations" [syn: {restart}]
     2: return to a previous location or condition; "The painting
        resumed its old condition when we restored it" [syn: {take
        up}]
     3: assume anew; "resume a title"; "resume an office"; "resume
        one's duties"
     4: give a summary (of); "he summed up his results"; "I will now
        summarize" [syn: {sum up}, {summarize}, {summarise}]
