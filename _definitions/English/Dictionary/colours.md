---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/colours
offline_file: ""
offline_thumbnail: ""
uuid: c9e5cf09-26c1-4a5f-bd6d-865ca7172cc9
updated: 1484310373
title: colours
categories:
    - Dictionary
---
colours
     n 1: a distinguishing emblem; "his tie proclaimed his school
          colors" [syn: {colors}]
     2: a flag that shows its nationality [syn: {colors}]
