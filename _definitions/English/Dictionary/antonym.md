---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/antonym
offline_file: ""
offline_thumbnail: ""
uuid: 21dab4ad-2e3a-4441-a3e9-6bb1fb0b497c
updated: 1484310148
title: antonym
categories:
    - Dictionary
---
antonym
     n : two words that express opposing concepts; "to him the
         opposite of gay was depressed" [syn: {opposite word}, {opposite}]
