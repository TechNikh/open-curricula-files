---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyclotomic_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: 5a4e0838-3144-4640-bc9b-cb2d683073b4
updated: 1484308635
title: Cyclotomic polynomial
tags:
    - Examples
    - Properties
    - Fundamental tools
    - Easy cases for computation
    - Integers appearing as coefficients
    - "Gauss's formula"
    - "Lucas's formula"
    - Cyclotomic polynomials over Zp
    - Polynomial values
    - Applications
    - Notes
categories:
    - Number theory
---
In mathematics, more specifically in algebra, the nth cyclotomic polynomial, for any positive integer n, is the unique irreducible polynomial with integer coefficients, which is a divisor of 
  
    
      
        
          x
          
            n
          
        
        −
        1
      
    
    {\displaystyle x^{n}-1}
  
 and is not a divisor of 
  
    
      
        
          x
          
            k
          
        
        −
        1
      
    
    {\displaystyle x^{k}-1}
  
 for any k < n. Its roots are the nth primitive roots of unity 
  
    
      
        
          e
          
            2
            i
            π
            
              
        ...
