---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revive
offline_file: ""
offline_thumbnail: ""
uuid: 7a577dcf-50a6-48b6-8db0-856c4669f797
updated: 1484310246
title: revive
categories:
    - Dictionary
---
revive
     v 1: cause to regain consciousness; "The doctors revived the
          comatose man" [syn: {resuscitate}]
     2: give new life or energy to; "A hot soup will revive me";
        "This will renovate my spirits"; "This treatment repaired
        my health" [syn: {animate}, {recreate}, {reanimate}, {renovate},
         {repair}, {quicken}, {vivify}, {revivify}]
     3: be brought back to life, consciousness, or strength;
        "Interest in ESP revived"
     4: restore from a depressed, inactive, or unused state; "He
        revived this style of opera"; "He resurrected the tango in
        this remote part of Argentina" [syn: {resurrect}]
     5: return to consciousness; "The ...
