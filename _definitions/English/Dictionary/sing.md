---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sing
offline_file: ""
offline_thumbnail: ""
uuid: 5eeb65ce-d017-477b-a6b7-fb73e3b08fc9
updated: 1484310543
title: sing
categories:
    - Dictionary
---
sing
     v 1: deliver by singing; "Sing Christmas carols"
     2: produce tones with the voice; "She was singing while she was
        cooking"; "My brother sings very well"
     3: to make melodious sounds; "The nightingale was singing"
     4: make a whining, ringing, or whistling sound; "the kettle was
        singing"; "the bullet sang past his ear" [syn: {whistle}]
     5: divulge confidential information or secrets;  "Be
        careful--his secretary talks" [syn: {spill the beans}, {let
        the cat out of the bag}, {talk}, {tattle}, {blab}, {peach},
         {babble}, {babble out}, {blab out}] [ant: {keep quiet}]
     [also: {sung}, {singing}, {sang}]
