---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proof_mining
offline_file: ""
offline_thumbnail: ""
uuid: 8557302e-f7e3-4a1d-8ca1-2d828ec89f7b
updated: 1484309497
title: Proof mining
categories:
    - Proof theory
---
In proof theory, a branch of mathematical logic, proof mining (or unwinding) is a research program that analyzes formalized proofs, especially in analysis, to obtain explicit bounds or rates of convergence from proofs that, when expressed in natural language, appear to be nonconstructive.[1] This research has led to improved results in analysis obtained from the analysis of classical proofs.
