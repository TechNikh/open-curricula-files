---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Connection_form
offline_file: ""
offline_thumbnail: ""
uuid: b0f14b37-5b3b-4027-a973-8e67655a8ba4
updated: 1484309212
title: Connection form
tags:
    - Vector bundles
    - Preliminaries
    - Frames on a vector bundle
    - Exterior connections
    - Connection forms
    - Change of frame
    - Global connection forms
    - Curvature
    - Soldering and torsion
    - Bianchi identities
    - 'Example: The Levi-Civita connection'
    - Curvature
    - Torsion
    - Structure groups
    - Compatible connections
    - Change of frame
    - Principal bundles
    - The principal connection for a connection form
    - Connection forms associated to a principal connection
    - Notes
categories:
    - Differential geometry
---
In mathematics, and specifically differential geometry, a connection form is a manner of organizing the data of a connection using the language of moving frames and differential forms.
