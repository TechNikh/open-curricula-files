---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subsidiary
offline_file: ""
offline_thumbnail: ""
uuid: 86b526b5-a680-4324-8dd8-c2a74ea8ccf2
updated: 1484310595
title: subsidiary
categories:
    - Dictionary
---
subsidiary
     adj 1: relating to something that is added but is not essential;
            "an ancillary pump"; "an adjuvant discipline to forms
            of mysticism"; "The mind and emotions are auxilliary
            to each other" [syn: {accessory}, {adjunct}, {ancillary},
             {adjuvant}, {appurtenant}, {auxiliary}]
     2: functioning in a subsidiary or supporting capacity; "the
        main library and its auxiliary branches" [syn: {auxiliary},
         {supplemental}, {supplementary}]
     n 1: an assistant subject to the authority or control of another
          [syn: {subordinate}, {underling}, {foot soldier}]
     2: a company that is completely controlled by another ...
