---
version: 1
type: video
provider: YouTube
id: ywqg9PorTAw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Periodic%20Table%20Trends-%20Ionization%20Energy.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5ab95126-9457-4d4d-b36f-7baa82188997
updated: 1486069585
title: 'Periodic Table Trends- Ionization Energy'
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/ywqg9PorTAw/default.jpg
    - https://i3.ytimg.com/vi/ywqg9PorTAw/1.jpg
    - https://i3.ytimg.com/vi/ywqg9PorTAw/2.jpg
    - https://i3.ytimg.com/vi/ywqg9PorTAw/3.jpg
---
What an ion is.  Using the periodic table to understand how difficult it is to ionize an atom.
More free lessons at: http://www.khanacademy.org/video?v=ywqg9PorTAw
