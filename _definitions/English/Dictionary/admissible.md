---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/admissible
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484330881
title: admissible
categories:
    - Dictionary
---
admissible
     adj : deserving to be admitted; "admissible evidence" [ant: {inadmissible}]
