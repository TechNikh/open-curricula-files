---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/improvement
offline_file: ""
offline_thumbnail: ""
uuid: 7285ed76-e873-4420-b20e-be2b652f3541
updated: 1484310462
title: improvement
categories:
    - Dictionary
---
improvement
     n 1: the act of improving something; "their improvements
          increased the value of the property"
     2: a change for the better; progress in development [syn: {betterment},
         {advance}]
     3: a condition superior to an earlier condition; "the new
        school represents a great improvement" [syn: {melioration}]
        [ant: {decline}]
