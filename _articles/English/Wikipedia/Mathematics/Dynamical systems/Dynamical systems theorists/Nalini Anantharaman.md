---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nalini_Anantharaman
offline_file: ""
offline_thumbnail: ""
uuid: 7b055206-70d2-44df-bc4f-f2af9b2552b8
updated: 1484308690
title: Nalini Anantharaman
categories:
    - Dynamical systems theorists
---
Nalini Florence Anantharaman was born in Paris in 1976 to two mathematicians. Her father and her mother are Professors at the University of Orléans. She entered Ecole Normale Supérieure in 1994. She completed her Ph.D in Paris under the supervision of François Ledrappier in 2000 at Université Pierre et Marie Curie (Paris 6).[1] She served as associate professor at Ecole Normale Supérieure de Lyon, and Ecole Polytechnique (Palaiseau).[2]
