---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ibrahim_ibn_Sinan
offline_file: ""
offline_thumbnail: ""
uuid: a0bb7a27-4ddb-4ef9-8cf2-24b3ed7bf9ad
updated: 1484309105
title: Ibrahim ibn Sinan
categories:
    - Geometers
---
Ibrahim ibn Sinan ibn Thābit ibn Qurra (born : 295-296 A.H/908 A.D in Baghdad, died : 334-335 A.H/ 946 A.D in Baghdad, aged 38) was a Syriac speaking Muslim from Harran in northern Mesopotamia/Assyria, the grandson of Thābit ibn Qurra.[1][2] He was mathematician and astronomer who studied geometry and in particular tangents to circles.[1][2] He also made advances in the quadrature of the parabola and the theory of integration, generalizing the work of Archimedes, which was unavailable at the time.[1][2] He is often referenced as one of the most important mathematicians of his time.[1]
