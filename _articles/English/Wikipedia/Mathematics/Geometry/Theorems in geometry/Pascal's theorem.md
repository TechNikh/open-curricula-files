---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pascal%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 6e6c2fd7-f2dd-4e61-99a9-7d7c4c07cb6c
updated: 1484309123
title: "Pascal's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Pascaltheoremgenericwithlabels.svg.png
tags:
    - Euclidean variants
    - Related results
    - Hexagrammum Mysticum
    - Proofs
    - Proof using cubic curves
    - "Proof using Bézout's theorem"
    - "A Property of Pascal's Hexagon"
    - "Degenerations of Pascals's theorem"
    - Notes
categories:
    - Theorems in geometry
---
In projective geometry, Pascal's theorem (also known as the Hexagrammum Mysticum Theorem) states that if six arbitrary points are chosen on a conic (i.e., ellipse, parabola or hyperbola) and joined by line segments in any order to form a hexagon, then the three pairs of opposite sides of the hexagon (extended if necessary) meet in three points which lie on a straight line, called the Pascal line of the hexagon.
