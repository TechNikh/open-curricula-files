---
version: 1
type: article
id: https://en.wikipedia.org/wiki/High_contrast_grating
offline_file: ""
offline_thumbnail: ""
uuid: e5774608-5e5d-4ddb-8cee-4542c7051abb
updated: 1484308889
title: High contrast grating
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-HighContrastGrating.png
tags:
    - History
    - Principle of operation
    - Applications
categories:
    - Waves
---
In physics, a high contrast grating is a single layer near-wavelength grating physical structure where the grating material has a large contrast in index of refraction with its surroundings. The term near-wavelength refers to the grating period, which has a value between one optical wavelength in the grating material and that in its surrounding materials.
