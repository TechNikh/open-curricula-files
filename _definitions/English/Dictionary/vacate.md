---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vacate
offline_file: ""
offline_thumbnail: ""
uuid: 30c98129-ca5b-4b0f-8cce-5ccc47c25fdc
updated: 1484310188
title: vacate
categories:
    - Dictionary
---
vacate
     v 1: leave (a job, post, post, or position) voluntarily; "She
          vacated the position when she got pregnant"; "The
          chairman resigned when he was found to have
          misappropriated funds" [syn: {resign}, {renounce}, {give
          up}]
     2: leave behind empty; move out of; "You must vacate your
        office by tonight" [syn: {empty}, {abandon}]
     3: annul by recalling or rescinding; "He revoked the ban on
        smoking"; "lift an embargo"; "vacate a death sentence"
        [syn: {revoke}, {annul}, {lift}, {countermand}, {reverse},
         {repeal}, {overturn}, {rescind}]
