---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foot
offline_file: ""
offline_thumbnail: ""
uuid: c52fb2bd-bd6e-4274-8533-5da694541470
updated: 1484310281
title: foot
categories:
    - Dictionary
---
foot
     n 1: a linear unit of length equal to 12 inches or a third of a
          yard; "he is six feet tall" [syn: {ft}]
     2: the foot of a human being; "his bare feet projected from his
        trousers"; "armored from head to foot" [syn: {human foot},
         {pes}]
     3: the lower part of anything; "curled up on the foot of the
        bed"; "the foot of the page"; "the foot of the list"; "the
        foot of the mountain" [ant: {head}]
     4: travel by foot; "he followed on foot"; "the swiftest of
        foot"
     5: a foot of a vertebrate other than a human being [syn: {animal
        foot}]
     6: a support resembling a pedal extremity; "one foot of the
        chair was ...
