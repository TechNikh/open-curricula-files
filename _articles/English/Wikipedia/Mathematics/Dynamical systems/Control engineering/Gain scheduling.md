---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gain_scheduling
offline_file: ""
offline_thumbnail: ""
uuid: defd76a1-55b8-4d33-9365-d579c3a418d4
updated: 1484308738
title: Gain scheduling
categories:
    - Control engineering
---
In control theory, gain scheduling is an approach to control of non-linear systems that uses a family of linear controllers, each of which provides satisfactory control for a different operating point of the system.
