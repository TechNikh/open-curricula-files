---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/later
offline_file: ""
offline_thumbnail: ""
uuid: 26dd25fb-09ce-4f17-8524-e55ff1fdf0f2
updated: 1484310325
title: later
categories:
    - Dictionary
---
later
     adj 1: coming at a subsequent time or stage; "the future president
            entered college at the age of 16"; "awaiting future
            actions on the bill"; "later developments"; "without
            ulterior argument" [syn: {future(a)}, {later(a)}, {ulterior}]
     2: at or toward an end or late period or stage of development;
        "the late phase of feudalism"; "a later symptom of the
        disease"; "later medical science could have saved the
        child" [syn: {late}, {later(a)}] [ant: {early}]
     adv 1: happening at a time subsequent to a reference time; "he
            apologized subsequently"; "he's going to the store but
            he'll be back here ...
