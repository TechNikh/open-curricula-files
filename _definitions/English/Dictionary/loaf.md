---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loaf
offline_file: ""
offline_thumbnail: ""
uuid: 0ae5df1d-0eb3-4944-884c-4b8e00f2cd72
updated: 1484310561
title: loaf
categories:
    - Dictionary
---
loaf
     n : a shaped mass of baked bread [syn: {loaf of bread}]
     v 1: be lazy or idle; "Her son is just bumming around all day"
          [syn: {bum}, {bum around}, {bum about}, {arse around}, {arse
          about}, {fuck off}, {frig around}, {waste one's time}, {lounge
          around}, {loll}, {loll around}, {lounge about}]
     2: be about; "The high school students like to loiter in the
        Central Square"; "Who is this man that is hanging around
        the department?" [syn: {loiter}, {lounge}, {footle}, {lollygag},
         {lallygag}, {hang around}, {mess about}, {tarry}, {linger},
         {lurk}, {mill about}, {mill around}]
     [also: {loaves} (pl)]
