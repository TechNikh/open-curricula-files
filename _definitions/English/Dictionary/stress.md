---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stress
offline_file: ""
offline_thumbnail: ""
uuid: ac1669b9-7699-4084-b4af-70dc1daf0fd9
updated: 1484310480
title: stress
categories:
    - Dictionary
---
stress
     n 1: the relative prominence of a syllable or musical note
          (especially with regard to stress or pitch); "he put the
          stress on the wrong syllable" [syn: {emphasis}, {accent}]
     2: (psychology) a state of mental or emotional strain or
        suspense; "he suffered from fatigue and emotional
        tension"; "stress is a vasoconstrictor" [syn: {tension}, {tenseness}]
     3: (physics) force that produces strain on a physical body;
        "the intensity of stress is expressed in units of force
        divided by units of area"
     4: special emphasis attached to something; "the stress was more
        on accuracy than on speed" [syn: {focus}]
     5: ...
