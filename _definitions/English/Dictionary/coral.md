---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coral
offline_file: ""
offline_thumbnail: ""
uuid: 6f0cfde7-eacf-4b26-bd8f-51e65f21c259
updated: 1484310439
title: coral
categories:
    - Dictionary
---
coral
     adj : of a strong pink to yellowish-pink color
     n 1: a variable color averaging a deep pink
     2: the hard stony skeleton of a Mediterranean coral that has a
        delicate red or pink color and is used for jewelry [syn: {red
        coral}, {precious coral}]
     3: unfertilized lobster roe; reddens in cooking; used as
        garnish or to color sauces
     4: marine colonial polyp characterized by a calcareous
        skeleton; masses in a variety of shapes often forming
        reefs
