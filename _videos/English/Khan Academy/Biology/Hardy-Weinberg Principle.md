---
version: 1
type: video
provider: YouTube
id: 4Kbruik_LOo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Hardy-Weinberg%20Principle.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6070fbbe-dac5-4eb5-bbf1-90031c8cd9f9
updated: 1486069583
title: Hardy-Weinberg Principle
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/4Kbruik_LOo/default.jpg
    - https://i3.ytimg.com/vi/4Kbruik_LOo/1.jpg
    - https://i3.ytimg.com/vi/4Kbruik_LOo/2.jpg
    - https://i3.ytimg.com/vi/4Kbruik_LOo/3.jpg
---
Understanding allele and genotype frequency in population in Hardy-Weinberg Equilibrium.
