---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/multiple
offline_file: ""
offline_thumbnail: ""
uuid: b5ef1587-7466-48e7-ab13-f1f7c1314482
updated: 1484310221
title: multiple
categories:
    - Dictionary
---
multiple
     adj : having or involving or consisting of more than one part or
           entity or individual; "multiple birth"; "multiple
           ownership"; "made multiple copies of the speech"; "his
           multiple achievements in public life"; "her multiple
           personalities"; "a pineapple is a multiple fruit" [ant:
            {single(a)}]
     n : the product of a quantity by an integer; "36 is a multiple
         of 9"
