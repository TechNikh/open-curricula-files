---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Telephone_number_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: bdb7f05c-499f-4f5c-a204-8805bfcc6520
updated: 1484309431
title: Telephone number (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-K4_matchings.svg_0.png
tags:
    - Applications
    - Mathematical properties
    - Recurrence
    - Summation formula and approximation
    - Generating function
    - Prime factors
categories:
    - Matching
---
In mathematics, the telephone numbers or involution numbers are a sequence of integers that count the number of connection patterns in a telephone system with n subscribers, where connections are made between pairs of subscribers. These numbers also describe the number of matchings (the Hosoya index) of a complete graph on n vertices, the number of permutations on n elements that are involutions, the sum of absolute values of coefficients of the Hermite polynomials, the number of standard Young tableaux with n cells, and the sum of the degrees of the irreducible representations of the symmetric group. Involution numbers were first studied in 1800 by Heinrich August Rothe, who gave a ...
