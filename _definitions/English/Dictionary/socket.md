---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/socket
offline_file: ""
offline_thumbnail: ""
uuid: 1f0d1a35-495f-4e41-a160-675cef26ccbb
updated: 1484310198
title: socket
categories:
    - Dictionary
---
socket
     n 1: a bony hollow into which a structure fits
     2: receptacle where something (a pipe or probe or end of a
        bone) is inserted
     3: a receptacle into which an electric device can be inserted
