---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalized_Riemann_hypothesis
offline_file: ""
offline_thumbnail: ""
uuid: c8dc9876-8610-4eb4-ac26-b18e5c3e6d83
updated: 1484309153
title: Generalized Riemann hypothesis
tags:
    - Generalized Riemann hypothesis (GRH)
    - Consequences of GRH
    - Extended Riemann hypothesis (ERH)
    - Notes
categories:
    - Algebraic geometry
---
The Riemann hypothesis is one of the most important conjectures in mathematics. It is a statement about the zeros of the Riemann zeta function. Various geometrical and arithmetical objects can be described by so-called global L-functions, which are formally similar to the Riemann zeta-function. One can then ask the same question about the zeros of these L-functions, yielding various generalizations of the Riemann hypothesis. Many mathematicians believe these generalizations of the Riemann hypothesis to be true. The only cases of these conjectures which have been proven occur in the function field case (not the number field case).
