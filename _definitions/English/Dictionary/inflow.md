---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inflow
offline_file: ""
offline_thumbnail: ""
uuid: 0079bc39-d263-4dab-b743-464dc8db1768
updated: 1484310461
title: inflow
categories:
    - Dictionary
---
inflow
     n : the process of flowing in [syn: {influx}] [ant: {outflow}, {outflow}]
