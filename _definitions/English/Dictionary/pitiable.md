---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pitiable
offline_file: ""
offline_thumbnail: ""
uuid: 1f2619ae-ee5d-49d8-ae8d-fc71a5e5dd60
updated: 1484310258
title: pitiable
categories:
    - Dictionary
---
pitiable
     adj 1: inspiring mixed contempt and pity; "their efforts were
            pathetic"; "pitiable lack of character"; "pitiful
            exhibition of cowardice" [syn: {pathetic}, {pitiful}]
     2: deserving or inciting pity; "a hapless victim"; "miserable
        victims of war"; "the shabby room struck her as
        extraordinarily pathetic"- Galsworthy; "piteous appeals
        for help"; "pitiable homeless children"; "a pitiful fate";
        "Oh, you poor thing"; "his poor distorted limbs"; "a
        wretched life" [syn: {hapless}, {miserable}, {misfortunate},
         {pathetic}, {piteous}, {pitiful}, {poor}, {wretched}]
