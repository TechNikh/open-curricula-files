---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_Axelrod
offline_file: ""
offline_thumbnail: ""
uuid: 2ef2e56e-592a-410a-b84b-28cf6e13ab19
updated: 1484308518
title: Robert Axelrod
tags:
    - Biography
    - Bibliography
    - Books
    - Journal articles
categories:
    - Game theorists
---
Robert Marshall Axelrod (born May 27, 1943) is an American political scientist. He is Professor of Political Science and Public Policy at the University of Michigan where he has been since 1974. He is best known for his interdisciplinary work on the evolution of cooperation, which has been cited in numerous articles. His current research interests include complexity theory (especially agent-based modeling), international security, and cyber security. Axelrod is a member of the Council on Foreign Relations.[1]
