---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breeding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484523601
title: breeding
categories:
    - Dictionary
---
breeding
     adj : producing offspring or set aside especially for producing
           offspring; "the breeding population"; "retained a few
           bulls for breeding purposes"
     n 1: elegance by virtue of fineness of manner and expression
          [syn: {genteelness}, {gentility}]
     2: the result of good upbringing (especially knowledge of
        correct social behavior); "a woman of breeding and
        refinement" [syn: {education}, {training}]
     3: raising someone to be an accepted member of the community;
        "they debated whether nature or nurture was more
        important" [syn: {bringing up}, {fostering}, {fosterage},
        {nurture}, {raising}, {rearing}, ...
