---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grehlin
offline_file: ""
offline_thumbnail: ""
uuid: 8e0baf21-14da-42cf-a51e-fa18948fa475
updated: 1484308380
title: Ghrelin
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Preproghrelin_1P7X.png
tags:
    - History and name
    - Gene, transcription products, and structure
    - Ghrelin cells
    - Alternative names
    - Location
    - features
    - Function and mechanism of action
    - Blood levels
    - Ghrelin receptor
    - Locations of action
    - Gastrointestinal tract
    - Pancreas
    - Glucose metabolism
    - Nervous system
    - Learning and memory
    - Depression
    - Sleep duration
    - Stress-induced fear
    - Substantia nigra function
    - Reproductive system
    - Fetus and neonate
    - Immune system
    - Anorexia and obesity
    - Disease management
    - Gastric bypass surgery
    - Medical management of obesity
    - Aging
    - Future clinical uses
    - Notes
categories:
    - Uncategorized
---
Ghrelin (pronounced /ˈɡrɛlɪn/), the "hunger hormone", also known as lenomorelin (INN), is a peptide hormone produced by ghrelinergic cells in the gastrointestinal tract[3][4] which functions as a neuropeptide in the central nervous system.[5] Besides regulating appetite, ghrelin also plays a significant role in regulating the distribution and rate of use of energy.[6]
