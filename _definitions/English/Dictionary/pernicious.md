---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pernicious
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484593081
title: pernicious
categories:
    - Dictionary
---
pernicious
     adj 1: exceedingly harmful [syn: {baneful}, {deadly}, {pestilent}]
     2: working or spreading in a hidden and usually injurious way;
        "glaucoma is an insidious disease"; "a subtle poison"
        [syn: {insidious}, {subtle}]
