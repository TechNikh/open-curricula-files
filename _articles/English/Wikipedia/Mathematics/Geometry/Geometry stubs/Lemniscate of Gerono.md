---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lemniscate_of_Gerono
offline_file: ""
offline_thumbnail: ""
uuid: b76b36ae-319c-4d37-a41f-da104a4a4bca
updated: 1484309365
title: Lemniscate of Gerono
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lemniscate-of-Gerono.svg_0.png
categories:
    - Geometry stubs
---
In algebraic geometry, the lemniscate of Gerono, or lemniscate of Huygens, or figure-eight curve, is a plane algebraic curve of degree four and genus zero and is a lemniscate curve shaped like an 
  
    
      
        ∞
      
    
    {\displaystyle \infty }
  
 symbol, or figure eight. It has equation
