---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contradict
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484491501
title: contradict
categories:
    - Dictionary
---
contradict
     v 1: be in contradiction with [syn: {belie}, {negate}]
     2: deny the truth of [syn: {negate}, {contravene}]
     3: be resistant to; "The board opposed his motion" [syn: {oppose},
         {controvert}]
     4: prove negative; show to be false [syn: {negate}] [ant: {confirm}]
