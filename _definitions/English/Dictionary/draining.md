---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/draining
offline_file: ""
offline_thumbnail: ""
uuid: 3d90dbb4-fa27-48ca-81ab-e3250c388393
updated: 1484310571
title: draining
categories:
    - Dictionary
---
draining
     adj : having a debilitating effect; "an exhausting job in the hot
           sun" [syn: {exhausting}]
