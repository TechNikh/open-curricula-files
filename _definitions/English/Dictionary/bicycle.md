---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bicycle
offline_file: ""
offline_thumbnail: ""
uuid: 975c45b2-031f-4bbb-8eaf-b50dcea262cf
updated: 1484310244
title: bicycle
categories:
    - Dictionary
---
bicycle
     n : a wheeled vehicle that has two wheels and is moved by foot
         pedals [syn: {bike}, {wheel}, {cycle}]
     v : ride a bicycle [syn: {cycle}, {bike}, {pedal}, {wheel}]
