---
version: 1
type: video
provider: YouTube
id: Gc-oWWqgQ_4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Total%20Flux%20Through%20a%20Surface%20%20Containing%20a%20Charge.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1524ce42-d036-416f-ae17-3127118e3e8c
updated: 1486069652
title: 'Total Flux Through a Surface  Containing a Charge'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Gc-oWWqgQ_4/default.jpg
    - https://i3.ytimg.com/vi/Gc-oWWqgQ_4/1.jpg
    - https://i3.ytimg.com/vi/Gc-oWWqgQ_4/2.jpg
    - https://i3.ytimg.com/vi/Gc-oWWqgQ_4/3.jpg
---
