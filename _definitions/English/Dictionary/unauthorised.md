---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unauthorised
offline_file: ""
offline_thumbnail: ""
uuid: 0cb314e8-df11-4781-a1c9-bf930686f772
updated: 1484310475
title: unauthorised
categories:
    - Dictionary
---
unauthorised
     adj 1: not endowed with authority [syn: {unauthorized}] [ant: {authorized}]
     2: without official authorization; "an unauthorized strike";
        "wildcat work stoppage" [syn: {unauthorized}, {wildcat}]
