---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electoral
offline_file: ""
offline_thumbnail: ""
uuid: b3849f11-f440-42db-97ee-a0a2eb435e39
updated: 1484310601
title: electoral
categories:
    - Dictionary
---
electoral
     adj 1: of or relating to elections; "electoral process"
     2: relating to or composed of electors; "electoral college"
