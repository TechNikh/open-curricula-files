---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stark
offline_file: ""
offline_thumbnail: ""
uuid: 3415603c-3037-4f6c-8fd6-0b2d9e228eff
updated: 1484310547
title: stark
categories:
    - Dictionary
---
stark
     adj 1: devoid of any qualifications or disguise or adornment; "the
            blunt truth"; "the crude facts"; "facing the stark
            reality of the deadline" [syn: {blunt}, {crude(a)}, {stark(a)}]
     2: severely simple; "a stark interior" [syn: {austere}, {severe}]
     3: complete or extreme; "stark poverty"; "a stark contrast"
     4: without qualification; used informally as (often pejorative)
        intensifiers; "an arrant fool"; "a complete coward"; "a
        consummate fool"; "a double-dyed villain"; "gross
        negligence"; "a perfect idiot"; "pure folly"; "what a
        sodding mess"; "stark staring mad"; "a thoroughgoing
        villain"; "utter ...
