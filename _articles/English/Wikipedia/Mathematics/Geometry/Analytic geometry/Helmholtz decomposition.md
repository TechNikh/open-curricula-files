---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Helmholtz_decomposition
offline_file: ""
offline_thumbnail: ""
uuid: a6c0b9f6-4fd1-46ff-8efd-a710c145082c
updated: 1484309165
title: Helmholtz decomposition
tags:
    - Statement of the theorem
    - Derivation
    - Another derivation from the Fourier transform
    - Fields with prescribed divergence and curl
    - Differential forms
    - Weak formulation
    - Longitudinal and transverse fields
    - Notes
    - General references
    - References for the weak formulation
categories:
    - Analytic geometry
---
In physics and mathematics, in the area of vector calculus, Helmholtz's theorem,[1][2] also known as the fundamental theorem of vector calculus,[3][4][5][6][7][8][9] states that any sufficiently smooth, rapidly decaying vector field in three dimensions can be resolved into the sum of an irrotational (curl-free) vector field and a solenoidal (divergence-free) vector field; this is known as the Helmholtz decomposition. It is named after Hermann von Helmholtz.[10]
