---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Small-world_routing
offline_file: ""
offline_thumbnail: ""
uuid: a88e09f5-6212-4308-8246-2fb453b4a2b8
updated: 1484309440
title: Small-world routing
tags:
    - Greedy routing
    - Constructing a reference base
    - The Kleinberg model
    - Greedy routing in the Kleinberg model
categories:
    - Network theory
---
In network theory, small-world routing refers to routing methods for small-world networks. Networks of this type are peculiar in that relatively short paths exist between any two nodes. Determining these paths, however, can be a difficult problem from the perspective of an individual routing node in the network if no further information is known about the network as a whole.
