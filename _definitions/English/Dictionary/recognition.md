---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recognition
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484490361
title: recognition
categories:
    - Dictionary
---
recognition
     n 1: the state or quality of being recognized or acknowledged;
          "the partners were delighted with the recognition of
          their work"; "she seems to avoid much in the way of
          recognition or acknowledgement of feminist work prior to
          her own" [syn: {acknowledgment}, {acknowledgement}]
     2: the process of recognizing something or someone by
        remembering; "a politician whose recall of names was as
        remarkable as his recognition of faces"; "experimental
        psychologists measure the elapsed time from the onset of
        the stimulus to its recognition by the observer" [syn: {identification}]
     3: approval; "give her ...
