---
version: 1
type: video
provider: YouTube
id: FofPjj7v414
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 606dc234-3804-44b8-852e-35ed5fa1855c
updated: 1486069634
title: Amazing chemical reactions!
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/FofPjj7v414/default.jpg
    - https://i3.ytimg.com/vi/FofPjj7v414/1.jpg
    - https://i3.ytimg.com/vi/FofPjj7v414/2.jpg
    - https://i3.ytimg.com/vi/FofPjj7v414/3.jpg
---
