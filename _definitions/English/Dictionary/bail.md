---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bail
offline_file: ""
offline_thumbnail: ""
uuid: eb19be0d-7631-416d-805e-fc45c1e8246b
updated: 1484310563
title: bail
categories:
    - Dictionary
---
bail
     n 1: (criminal law) money that must be forfeited by the bondsman
          if an accused person fails to appear in court for trial;
          "the judge set bail at $10,000"; "a $10,000 bond was
          furnished by an alderman" [syn: {bail bond}, {bond}]
     2: the legal system that allows an accused person to be
        temporarily released from custody (usually on condition
        that a sum of money guarantees their appearance at trial);
        "he is out on bail"
     v 1: release after a security has been paid
     2: deliver something in trust to somebody for a special purpose
        and for a limited period
     3: secure the release of (someone) by providing ...
