---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retain
offline_file: ""
offline_thumbnail: ""
uuid: efd2d55c-a952-4387-8c1e-4a36be131bac
updated: 1484310284
title: retain
categories:
    - Dictionary
---
retain
     v 1: hold within; "This soil retains water"; "I retain this drug
          for a long time"
     2: allow to remain in a place or position; "We cannot continue
        several servants any longer"; "She retains a lawyer"; "The
        family's fortune waned and they could not keep their
        household staff"; "Our grant has run out and we cannot
        keep you on"; "We kept the work going as long as we could"
        [syn: {continue}, {keep}, {keep on}, {keep going}]
     3: secure and keep for possible future use or application; "The
        landlord retained the security deposit"; "I reserve the
        right to disagree" [syn: {hold}, {keep back}, {hold back}]
     4: ...
