---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Backcover
offline_file: ""
offline_thumbnail: ""
uuid: 871b6dfe-2369-4501-819b-7607ee458efd
updated: 1484308458
title: Book design
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Design_for_a_metalwork_book_cover%252C_by_Hans_Holbein_the_Younger.jpg'
tags:
    - Book structure
    - Front matter
    - Body matter
    - Back matter (end matter)
    - Front cover, spine, and back cover of the dust-jacket
    - Binding
    - Other items
    - Page spread
    - Print space
categories:
    - Uncategorized
---
In the words of Jan Tschichold, book design, "though largely forgotten today, methods and rules upon which it is impossible to improve, have been developed over centuries. To produce perfect books, these rules have to be brought back to life and applied."[1] Richard Hendel describes book design as "an arcane subject", and refers to the need for a context to understand what that means.[2]
