---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spin
offline_file: ""
offline_thumbnail: ""
uuid: 93e9391d-498e-4c69-838a-3dec3301f7d3
updated: 1484310391
title: spin
categories:
    - Dictionary
---
spin
     n 1: a swift whirling motion (usually of a missile)
     2: the act of rotating rapidly; "he gave the crank a spin"; "it
        broke off after much twisting" [syn: {twirl}, {twist}, {twisting},
         {whirl}]
     3: a short drive in a car; "he took the new car for a spin"
     4: rapid descent of an aircraft in a steep spiral [syn: {tailspin}]
     5: a distinctive interpretation (especially as used by
        politicians to sway public opinion); "the campaign put a
        favorable spin on the story"
     v 1: revolve quickly and repeatedly around one's own axis; "The
          dervishes whirl around and around without getting dizzy"
          [syn: {spin around}, {whirl}, ...
