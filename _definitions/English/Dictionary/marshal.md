---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marshal
offline_file: ""
offline_thumbnail: ""
uuid: c7511597-6719-4793-ba19-6c1a68c73e57
updated: 1484310163
title: marshal
categories:
    - Dictionary
---
marshal
     n 1: a law officer having duties similar to those of a sheriff in
          carrying out the judgments of a court of law [syn: {marshall}]
     2: (in some countries) a military officer of highest rank [syn:
         {marshall}]
     v 1: place in proper rank; "marshal the troops"
     2: arrange in logical order; "marshal facts or arguments"
     3: make ready for action or use; "marshal resources" [syn: {mobilize},
         {mobilise}, {summon}]
     4: lead ceremoniously, as in a procession
     [also: {marshalling}, {marshalled}]
