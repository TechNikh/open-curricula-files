---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/democratic
offline_file: ""
offline_thumbnail: ""
uuid: 43be2275-8b58-4b72-a9cb-df06514ad43d
updated: 1484310547
title: democratic
categories:
    - Dictionary
---
democratic
     adj 1: characterized by or advocating or based upon the principles
            of democracy or social equality; "democratic
            government"; "a democratic country"; "a democratic
            scorn for bloated dukes and lords"- George du Maurier
            [ant: {undemocratic}]
     2: belong to or relating to the Democratic Party; "Democratic
        senator"
     3: representing or appealing to or adapted for the benefit of
        the people at large; "democratic art forms"; "a democratic
        or popular movement"; "popular thought"; "popular
        science"; "popular fiction" [syn: {popular}]
