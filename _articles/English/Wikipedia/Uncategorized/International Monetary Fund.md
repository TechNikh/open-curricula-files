---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Imf
offline_file: ""
offline_thumbnail: ""
uuid: 3a698747-4db7-4122-8383-f19cab1e6350
updated: 1484308463
title: International Monetary Fund
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-IMF_DDS.svg.png
tags:
    - Functions
    - Surveillance of the global economy
    - Conditionality of loans
    - Structural adjustment
    - Benefits
    - History
    - Since 2000
    - Member countries
    - Qualifications
    - Benefits
categories:
    - Uncategorized
---
The International Monetary Fund (IMF) is an international organization headquartered in Washington, D.C., of "189 countries working to foster global monetary cooperation, secure financial stability, facilitate international trade, promote high employment and sustainable economic growth, and reduce poverty around the world."[1] Formed in 1944 at the Bretton Woods Conference primarily by the ideas of Harry Dexter White and John Maynard Keynes,[4] it came into formal existence in 1945 with 29 member countries and the goal of reconstructing the international payment system. It now plays a central role in the management of balance of payments difficulties and international financial crises.[5] ...
