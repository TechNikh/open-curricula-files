---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accepting
offline_file: ""
offline_thumbnail: ""
uuid: e186cd72-43a0-4458-bb9c-12a7595b62a2
updated: 1484310192
title: accepting
categories:
    - Dictionary
---
accepting
     adj : tolerating without protest; "always more accepting of
           coaching suggestion than her teammates"; "the
           atmosphere was judged to be more supporting and
           accepting"
