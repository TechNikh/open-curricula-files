---
version: 1
type: video
provider: YouTube
id: jFd-6EPfnec
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Pre-Algebra/Factors%20and%20multiples/Greatest%20common%20factor%20explained.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8bf9f499-5288-46ce-bff9-41091777bbdf
updated: 1486069569
title: Greatest common factor explained
categories:
    - Factors and multiples
thumbnail_urls:
    - https://i3.ytimg.com/vi/jFd-6EPfnec/default.jpg
    - https://i3.ytimg.com/vi/jFd-6EPfnec/1.jpg
    - https://i3.ytimg.com/vi/jFd-6EPfnec/2.jpg
    - https://i3.ytimg.com/vi/jFd-6EPfnec/3.jpg
---
Here's a nice explanation of least common factor (or least common divisor) along with a few practice example exercises. Let's roll.

Practice this lesson yourself on KhanAcademy.org right now: https://www.khanacademy.org/math/pre-algebra/factors-multiples/greatest_common_divisor/e/greatest_common_divisor?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Watch the next lesson: https://www.khanacademy.org/math/pre-algebra/factors-multiples/greatest_common_divisor/v/lcm-and-gcf-greatest-common-factor-word-problems?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Missed the previous lesson? 
https://www.khanacademy.org/math/pre-algebra/factors-multiples/greatest_common_divisor/v/greatest-common-divisor-factor-exercise?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Pre-Algebra on Khan Academy: No way, this isn't your run of the mill arithmetic. This is Pre-algebra. You're about to play with the professionals. Think of pre-algebra as a runway. You're the airplane and algebra is your sunny vacation destination. Without the runway you're not going anywhere. Seriously, the foundation for all higher mathematics is laid with many of the concepts that we will introduce to you here: negative numbers, absolute value, factors, multiples, decimals, and fractions to name a few. So buckle up and move your seat into the upright position. We're about to take off!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy’s Pre-Algebra channel:: https://www.youtube.com/channel/UCIMlYkATtXOFswVoCZN7nAA?sub_confirmation=1
Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
