---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anther
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484346601
title: anther
categories:
    - Dictionary
---
anther
     n : the part of the stamen that contains pollen; usually borne
         on a stalk
