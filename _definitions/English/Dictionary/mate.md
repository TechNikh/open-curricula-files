---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mate
offline_file: ""
offline_thumbnail: ""
uuid: 751d0b16-13fb-4a14-bae3-b889a696d29c
updated: 1484310284
title: mate
categories:
    - Dictionary
---
mate
     n 1: the officer below the master on a commercial ship [syn: {first
          mate}]
     2: a fellow member of a team; "it was his first start against
        his former teammates" [syn: {teammate}]
     3: the partner of an animal (especially a sexual partner); "he
        loved the mare and all her mates"; "camels hate leaving
        their mates"
     4: a person's partner in marriage [syn: {spouse}, {partner}, {married
        person}, {better half}]
     5: an exact duplicate; "when a match is found an entry is made
        in the notebook" [syn: {match}]
     6: South American holly; leaves used in making a drink like tea
        [syn: {Paraguay tea}, {Ilex ...
