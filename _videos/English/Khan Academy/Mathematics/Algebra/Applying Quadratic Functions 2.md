---
version: 1
type: video
provider: YouTube
id: HtN86WyZ6zY
offline_file: ""
offline_thumbnail: ""
uuid: e046158d-46d3-4df2-9942-b6b9a87f247f
updated: 1484040398
title: Applying Quadratic Functions 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/HtN86WyZ6zY/default.jpg
    - https://i3.ytimg.com/vi/HtN86WyZ6zY/1.jpg
    - https://i3.ytimg.com/vi/HtN86WyZ6zY/2.jpg
    - https://i3.ytimg.com/vi/HtN86WyZ6zY/3.jpg
---
