---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carlos_Matheus
offline_file: ""
offline_thumbnail: ""
uuid: c737a347-42b8-4725-92cf-87267cca5556
updated: 1484308688
title: Carlos Matheus
tags:
    - Selected publications
categories:
    - Dynamical systems theorists
---
Carlos Matheus Silva Santos (born May 1, 1984 in Aracaju) is a Brazilian mathematician working in dynamical systems, analysis and geometry. He currently works at the CNRS, in Paris.[1]
