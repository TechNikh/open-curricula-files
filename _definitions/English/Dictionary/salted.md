---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/salted
offline_file: ""
offline_thumbnail: ""
uuid: 8890b94d-74f7-47c2-a796-8d1386cdcf13
updated: 1484310311
title: salted
categories:
    - Dictionary
---
salted
     adj : (used especially of meats) preserved in salt [syn: {salt-cured}]
