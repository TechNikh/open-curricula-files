---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crossing
offline_file: ""
offline_thumbnail: ""
uuid: 4ced153c-5683-4034-b995-5e964647eab9
updated: 1484310275
title: crossing
categories:
    - Dictionary
---
crossing
     n 1: traveling across
     2: a shallow area in a stream that can be forded [syn: {ford}]
     3: a point where two lines (paths or arcs etc.) intersect
     4: a junction where one street or road crosses another [syn: {intersection},
         {crossroad}, {crossway}, {carrefour}]
     5: a path (often marked) where something (as a street or
        railroad) can be crossed to get from one side to the other
        [syn: {crosswalk}, {crossover}]
     6: (genetics) the act of mixing different species or varieties
        of animals or plants and thus to produce hybrids [syn: {hybridization},
         {hybridisation}, {crossbreeding}, {cross}, {interbreeding},
         ...
