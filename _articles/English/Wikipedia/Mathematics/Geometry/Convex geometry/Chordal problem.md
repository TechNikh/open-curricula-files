---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chordal_problem
offline_file: ""
offline_thumbnail: ""
uuid: 033e95ff-3033-40b0-9e00-ff3893a5ade5
updated: 1484309189
title: Chordal problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Curve_transp.png
tags:
    - Curves with one equichordal point
    - An example
    - Special cases
    - The status of various special cases
    - The equichordal point problem (α = 1)
    - The equireciprocal point problem (α = −1)
    - Other cases
categories:
    - Convex geometry
---
where 
  
    
      
        c
      
    
    {\displaystyle c}
  
 is a constant not depending on the chord. In this article we will call a point 
  
    
      
        O
      
    
    {\displaystyle O}
  
 satisfying equation[2] a chordal point, or 
  
    
      
        α
      
    
    {\displaystyle \alpha }
  
-chordal point.
