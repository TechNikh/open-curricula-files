---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clique_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: babdc069-482d-4ff3-a460-bdcc1ba78b87
updated: 1484309442
title: Clique (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-VR_complex.svg_0.png
tags:
    - Definitions
    - Mathematics
    - Computer science
    - Applications
    - Notes
categories:
    - Graph theory objects
---
In the mathematical area of graph theory, a clique (/ˈkliːk/ or /ˈklɪk/) is a subset of vertices of an undirected graph such that its induced subgraph is complete; that is, every two distinct vertices in the clique are adjacent. Cliques are one of the basic concepts of graph theory and are used in many other mathematical problems and constructions on graphs. Cliques have also been studied in computer science: the task of finding whether there is a clique of a given size in a graph (the clique problem) is NP-complete, but despite this hardness result, many algorithms for finding cliques have been studied.
