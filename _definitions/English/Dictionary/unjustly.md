---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unjustly
offline_file: ""
offline_thumbnail: ""
uuid: 7e5a20ce-3948-459b-87df-8f7c7ca018d1
updated: 1484310152
title: unjustly
categories:
    - Dictionary
---
unjustly
     adv : in an unjust manner; "he was unjustly singled out for
           punishment" [ant: {rightly}]
