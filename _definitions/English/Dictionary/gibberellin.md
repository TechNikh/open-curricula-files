---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gibberellin
offline_file: ""
offline_thumbnail: ""
uuid: 5328dad9-bb2b-437e-8d18-2f1b004506fe
updated: 1484310162
title: gibberellin
categories:
    - Dictionary
---
gibberellin
     n : a plant hormone isolated from a fungus; used in promoting
         plant growth
