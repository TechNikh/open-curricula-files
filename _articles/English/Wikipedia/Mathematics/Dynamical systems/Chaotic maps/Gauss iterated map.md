---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gauss_iterated_map
offline_file: ""
offline_thumbnail: ""
uuid: a9872c9c-32d1-4a8a-8416-9606d5019953
updated: 1484308719
title: Gauss iterated map
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Gauss_alpha%253D4.9_beta%253D-0.58_cobweb.png'
categories:
    - Chaotic maps
---
In mathematics, the Gauss map (also known as Gaussian map[1] or mouse map), is a nonlinear iterated map of the reals into a real interval given by the Gaussian function:
