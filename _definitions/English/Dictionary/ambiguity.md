---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ambiguity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484439481
title: ambiguity
categories:
    - Dictionary
---
ambiguity
     n 1: an expression whose meaning cannot be determined from its
          context
     2: unclearness by virtue of having more than one meaning [syn:
        {equivocalness}] [ant: {unambiguity}, {unambiguity}]
