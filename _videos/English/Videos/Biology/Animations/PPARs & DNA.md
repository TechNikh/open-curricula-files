---
version: 1
type: video
provider: YouTube
id: QrS3rNvh4J4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f5f9f190-2940-4dab-bb97-431b88badbc8
updated: 1486069672
title: 'PPARs & DNA'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/QrS3rNvh4J4/default.jpg
    - https://i3.ytimg.com/vi/QrS3rNvh4J4/1.jpg
    - https://i3.ytimg.com/vi/QrS3rNvh4J4/2.jpg
    - https://i3.ytimg.com/vi/QrS3rNvh4J4/3.jpg
---
