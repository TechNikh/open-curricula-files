---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Patagium
offline_file: ""
offline_thumbnail: ""
uuid: 4e98092a-334d-41da-a990-0af0094dde45
updated: 1484308344
title: Patagium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Patagium-flying_squirrel-psf.png
tags:
    - Bats
    - Pterosaurs
    - Gliding mammals
    - Other
categories:
    - Uncategorized
---
The patagium is a membranous structure that assists an animal in gliding or flight. The structure is found in bats, birds, some dromaeosaurs, pterosaurs, and gliding mammals.
