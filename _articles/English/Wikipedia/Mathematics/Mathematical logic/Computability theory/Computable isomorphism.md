---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computable_isomorphism
offline_file: ""
offline_thumbnail: ""
uuid: 48f6bcae-ead7-4460-982e-445b24d5d080
updated: 1484309485
title: Computable isomorphism
categories:
    - Computability theory
---
In computability theory two sets 
  
    
      
        A
        ;
        B
        ⊆
        
          N
        
      
    
    {\displaystyle A;B\subseteq \mathbb {N} }
  
 of natural numbers are computably isomorphic or recursively isomorphic if there exists a total bijective computable function 
  
    
      
        f
        :
        
          N
        
        →
        
          N
        
      
    
    {\displaystyle f\colon \mathbb {N} \to \mathbb {N} }
  
 with 
  
    
      
        f
        (
        A
        )
        =
        B
      
    
    {\displaystyle f(A)=B}
  
. By the theorem of Myhill,[1] the relation of computable isomorphism coincides with ...
