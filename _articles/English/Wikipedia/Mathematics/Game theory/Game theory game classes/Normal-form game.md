---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Normal-form_game
offline_file: ""
offline_thumbnail: ""
uuid: 23b0a5f9-17d9-4d87-b001-e99526d2e1b7
updated: 1484309035
title: Normal-form game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-SGPNEandPlainNE_explainingexample.svg.png
tags:
    - An example
    - Other representations
    - Uses of normal form
    - Dominated strategies
    - Sequential games in normal form
    - General formulation
categories:
    - Game theory game classes
---
In game theory, normal form is a description of a game. Unlike extensive form, normal-form representations are not graphical per se, but rather represent the game by way of a matrix. While this approach can be of greater use in identifying strictly dominated strategies and Nash equilibria, some information is lost as compared to extensive-form representations. The normal-form representation of a game includes all perceptible and conceivable strategies, and their corresponding payoffs, for each player.
