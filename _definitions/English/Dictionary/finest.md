---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finest
offline_file: ""
offline_thumbnail: ""
uuid: 805db8b6-da84-4112-b7a7-b2a392e70aa1
updated: 1484310146
title: finest
categories:
    - Dictionary
---
finest
     adj : surpassing in quality; "top-grade ore" [syn: {high-grade}, {top-quality},
            {top-grade}]
