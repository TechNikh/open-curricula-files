---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dressing
offline_file: ""
offline_thumbnail: ""
uuid: 973cfbb0-b8d3-4eff-8fe4-7c5926ee305e
updated: 1484310409
title: dressing
categories:
    - Dictionary
---
dressing
     n 1: savory dressings for salads; basically of two kinds: either
          the thin French or vinaigrette type or the creamy
          mayonnaise type [syn: {salad dressing}]
     2: a mixture of seasoned ingredients used to stuff meats and
        vegetables [syn: {stuffing}]
     3: making fertile as by applying fertilizer or manure [syn: {fertilization},
         {fertilisation}, {fecundation}]
     4: a cloth covering for a wound or sore [syn: {medical dressing}]
     5: processes in the conversion of rough hides into leather
     6: the activity of getting dressed; putting on clothes [syn: {grooming}]
     7: the act of applying a bandage [syn: {bandaging}, {binding}]
