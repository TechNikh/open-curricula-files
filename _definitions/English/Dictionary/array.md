---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/array
offline_file: ""
offline_thumbnail: ""
uuid: 20aa2526-aebd-4c4e-9bdb-9ac765167be5
updated: 1484310218
title: array
categories:
    - Dictionary
---
array
     n 1: an orderly arrangement; "an array of troops in battle order"
     2: an impressive display; "it was a bewildering array of
        books"; "his tools were in an orderly array on the
        basement wall"
     3: especially fine or decorative clothing [syn: {raiment}, {regalia}]
     4: an arrangement of aerials spaced to give desired directional
        characteristics
     v 1: lay out in a line [syn: {range}, {lay out}, {set out}]
     2: align oneself with a group or a way of thinking [syn: {align}]
