---
version: 1
type: video
provider: YouTube
id: KMkCMYTh-HM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20Condensed%20Matter%20Physics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 78535842-28ac-4bac-9d74-2b952f792603
updated: 1486069653
title: What is Condensed Matter Physics
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/KMkCMYTh-HM/default.jpg
    - https://i3.ytimg.com/vi/KMkCMYTh-HM/1.jpg
    - https://i3.ytimg.com/vi/KMkCMYTh-HM/2.jpg
    - https://i3.ytimg.com/vi/KMkCMYTh-HM/3.jpg
---
