---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/somebody
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484431621
title: somebody
categories:
    - Dictionary
---
somebody
     n : a human being; "there was too much for one person to do"
         [syn: {person}, {individual}, {someone}, {mortal}, {human},
          {soul}]
