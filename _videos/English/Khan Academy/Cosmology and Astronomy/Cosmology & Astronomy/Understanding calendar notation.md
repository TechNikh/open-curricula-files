---
version: 1
type: video
provider: YouTube
id: EKKe7DBZVhI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Understanding%20calendar%20notation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 104203e8-6a1e-4bd7-aa9b-8f70faf0ddbc
updated: 1486069685
title: Understanding calendar notation
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/EKKe7DBZVhI/default.jpg
    - https://i3.ytimg.com/vi/EKKe7DBZVhI/1.jpg
    - https://i3.ytimg.com/vi/EKKe7DBZVhI/2.jpg
    - https://i3.ytimg.com/vi/EKKe7DBZVhI/3.jpg
---
