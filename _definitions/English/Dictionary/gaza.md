---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gaza
offline_file: ""
offline_thumbnail: ""
uuid: aa0092ff-6de0-4e59-af24-8c17944009df
updated: 1484310178
title: gaza
categories:
    - Dictionary
---
Gaza
     n : a coastal region at the southeastern corner of the
         Mediterranean bordering Israel and Egypt; "he is a
         Palestinian from Gaza" [syn: {Gaza Strip}]
