---
version: 1
type: video
provider: YouTube
id: FSyAehMdpyI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9a92e44d-23d2-4aa7-b032-53d4f9440159
updated: 1486069680
title: 'The Nucleus- Crash Course Chemistry #1'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/FSyAehMdpyI/default.jpg
    - https://i3.ytimg.com/vi/FSyAehMdpyI/1.jpg
    - https://i3.ytimg.com/vi/FSyAehMdpyI/2.jpg
    - https://i3.ytimg.com/vi/FSyAehMdpyI/3.jpg
---
