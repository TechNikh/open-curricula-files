---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rotation_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 24cad4e8-2bbf-422e-90a6-11c668d1b45c
updated: 1484309338
title: Rotation matrix
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Counterclockwise_rotation.png
tags:
    - In two dimensions
    - Non-standard orientation of the coordinate system
    - Common rotations
    - In three dimensions
    - Basic rotations
    - General rotations
    - Conversion from and to axis-angle
    - Determining the axis
    - Determining the angle
    - Rotation matrix from axis and angle
    - Properties of a rotation matrix
    - Examples
    - Geometry
    - Multiplication
    - Ambiguities
    - Decompositions
    - Independent planes
    - Sequential angles
    - Nested dimensions
    - "Skew parameters via Cayley's formula"
    - Decomposition into shears
    - Group theory
    - Lie group
    - Lie algebra
    - Exponential map
    - Baker–Campbell–Hausdorff formula
    - Spin group
    - Infinitesimal rotations
    - Conversions
    - Quaternion
    - Polar decomposition
    - Axis and angle
    - Euler angles
    - Uniform random rotation matrices
    - Remarks
    - Notes
categories:
    - Transformation (function)
---
rotates points in the xy-Cartesian plane counter-clockwise through an angle θ about the origin of the Cartesian coordinate system. To perform the rotation using a rotation matrix R, the position of each point must be represented by a column vector v, containing the coordinates of the point. A rotated vector is obtained by using the matrix multiplication Rv.
