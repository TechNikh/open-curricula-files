---
version: 1
type: video
provider: YouTube
id: 9IEPlYQPH1U
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Tricks/How%20to%20Make%20SLIME%20%28Cool%20Chemical%20Reaction%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e432a852-10e2-4f59-afbd-e2391fabfd14
updated: 1486069660
title: How to Make SLIME (Cool Chemical Reaction)
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/9IEPlYQPH1U/default.jpg
    - https://i3.ytimg.com/vi/9IEPlYQPH1U/1.jpg
    - https://i3.ytimg.com/vi/9IEPlYQPH1U/2.jpg
    - https://i3.ytimg.com/vi/9IEPlYQPH1U/3.jpg
---
