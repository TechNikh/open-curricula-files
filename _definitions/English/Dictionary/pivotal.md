---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pivotal
offline_file: ""
offline_thumbnail: ""
uuid: c70029b9-0a36-489a-a61e-433c168429f0
updated: 1484310469
title: pivotal
categories:
    - Dictionary
---
pivotal
     adj : being of crucial importance; "a pivotal event"; "Its pivotal
           location has also exposed it to periodic invasions"-
           Henry Kissinger; "the polar events of this study"; "a
           polar principal" [syn: {polar}]
