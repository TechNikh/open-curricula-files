---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demise
offline_file: ""
offline_thumbnail: ""
uuid: 320ba95a-003a-4309-8603-ba68af10d1b2
updated: 1484310529
title: demise
categories:
    - Dictionary
---
demise
     n : the time when something ends; "it was the death of all his
         plans"; "a dying of old hopes" [syn: {death}, {dying}]
         [ant: {birth}]
