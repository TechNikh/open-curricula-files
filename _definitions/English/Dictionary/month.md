---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/month
offline_file: ""
offline_thumbnail: ""
uuid: ea550553-7597-455f-a4d7-5a9a8d4f5ffa
updated: 1484310196
title: month
categories:
    - Dictionary
---
month
     n 1: one of the twelve divisions of the calendar year; "he paid
          the bill last month" [syn: {calendar month}]
     2: a time unit of 30 days; "he was given a month to pay the
        bill"
