---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Single-parameter_utility
offline_file: ""
offline_thumbnail: ""
uuid: 8a85f8fd-74d6-40fa-a956-8aeaaadd018c
updated: 1484309046
title: Single-parameter utility
tags:
    - Notation
    - Monotonicity
    - Critical value
    - Deterministic implementation
    - Randomized implementation
    - Single-parameter vs. multi-parameter domains
categories:
    - Mechanism design
---
In mechanism design, an agent is said to have single-parameter utility if his valuation of the possible outcomes can be represented by a single number. For example, in an auction for a single item, the utilities of all agents are single-parametric, since they can be represented by their monetary evaluation of the item. In contrast, in a combinatorial auction for two or more related items, the utilities are usually not single-parametric, since they are usually represented by their evaluations to all possible bundles of items.
