---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tsa
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484363881
title: tsa
categories:
    - Dictionary
---
TSA
     n : an agency established in 2001 to safeguard United States
         transportation systems and insure safe air travel [syn: {Transportation
         Safety Administration}]
