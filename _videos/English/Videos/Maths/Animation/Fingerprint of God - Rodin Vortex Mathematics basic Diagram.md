---
version: 1
type: video
provider: YouTube
id: w7dwRhSCV1k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2222c2fc-8ff7-4027-89a0-58dea53b6ade
updated: 1486069648
title: 'Fingerprint of God - Rodin Vortex Mathematics basic Diagram'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/w7dwRhSCV1k/default.jpg
    - https://i3.ytimg.com/vi/w7dwRhSCV1k/1.jpg
    - https://i3.ytimg.com/vi/w7dwRhSCV1k/2.jpg
    - https://i3.ytimg.com/vi/w7dwRhSCV1k/3.jpg
---
