---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/philippines
offline_file: ""
offline_thumbnail: ""
uuid: d6ee42a7-15ef-4641-84ed-da3589645473
updated: 1484310281
title: philippines
categories:
    - Dictionary
---
Philippines
     n 1: a republic on the Philippine Islands; achieved independence
          from the United States in 1946 [syn: {Republic of the
          Philippines}]
     2: an archipelago in the southwestern Pacific including some
        7000 islands [syn: {Philippine Islands}]
