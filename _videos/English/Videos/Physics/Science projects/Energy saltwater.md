---
version: 1
type: video
provider: YouTube
id: VMdS65_E_X4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2a209860-c2f0-428b-b649-afdceb3a2061
updated: 1486069634
title: Energy saltwater
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/VMdS65_E_X4/default.jpg
    - https://i3.ytimg.com/vi/VMdS65_E_X4/1.jpg
    - https://i3.ytimg.com/vi/VMdS65_E_X4/2.jpg
    - https://i3.ytimg.com/vi/VMdS65_E_X4/3.jpg
---
