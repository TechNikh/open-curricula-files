---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bewildered
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484636641
title: bewildered
categories:
    - Dictionary
---
bewildered
     adj : perplexed by many conflicting situations or statements;
           filled with bewilderment; "obviously bemused by his
           questions"; "bewildered and confused"; "a cloudy and
           confounded philosopher"; "just a mixed-up kid"; "she
           felt lost on the first day of school" [syn: {baffled},
           {befuddled}, {bemused}, {confounded}, {confused}, {lost},
            {mazed}, {mixed-up}, {at sea}]
