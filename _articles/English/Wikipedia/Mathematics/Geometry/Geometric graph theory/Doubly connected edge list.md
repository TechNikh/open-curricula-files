---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Doubly_connected_edge_list
offline_file: ""
offline_thumbnail: ""
uuid: 22ea7f38-f830-44de-9d83-27dd5342edd9
updated: 1484309241
title: Doubly connected edge list
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dcel-halfedge-connectivity.svg.png
categories:
    - Geometric graph theory
---
The doubly connected edge list (DCEL), also known as half-edge data structure, is a data structure to represent an embedding of a planar graph in the plane, and polytopes in 3D. This data structure provides efficient manipulation of the topological information associated with the objects in question (vertices, edges, faces). It is used in many algorithms of computational geometry to handle polygonal subdivisions of the plane, commonly called planar straight-line graphs (PSLG).[1] For example, a Voronoi diagram is commonly represented by a DCEL inside a bounding box.
