---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ammann%E2%80%93Beenker_tiling'
offline_file: ""
offline_thumbnail: ""
uuid: ec9d1319-8aca-4e83-b9e1-9ed002350e58
updated: 1484308592
title: Ammann–Beenker tiling
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Ammann-Beenker_A5_in_blue_and_pink.svg.png
tags:
    - Description of the tiles
    - Pell and silver ratio features
    - Cut-and-project construction
    - References and notes
categories:
    - Discrete geometry
---
In geometry, an Ammann–Beenker tiling is a nonperiodic tiling which can be generated either by an aperiodic set of prototiles as done by Robert Ammann in the 1970s, or by the cut-and-project method as done independently by F. P. M. Beenker. Because all tilings obtained with the tiles are non-periodic, Ammann–Beenker tilings are considered aperiodic tilings. They are one of the five sets of tilings discovered by Ammann and described in Tilings and Patterns.[1]
