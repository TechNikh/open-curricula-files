---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/warn
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484583841
title: warn
categories:
    - Dictionary
---
warn
     v 1: notify of danger, potential harm, or risk; "The director
          warned him that he might be fired"; "The doctor warned
          me about the dangers of smoking"
     2: admonish or counsel in terms of someone's behavior; "I
        warned him not to go too far"; "I warn you against false
        assumptions"; "She warned him to be quiet" [syn: {discourage},
         {admonish}, {monish}]
     3: ask to go away; "The old man warned the children off his
        property"
     4: notify, usually in advance; "I warned you that I would ask
        some difficult questions"
