---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indelible
offline_file: ""
offline_thumbnail: ""
uuid: 75cd5578-dcbe-4e4a-acaf-52c47294c6a4
updated: 1484310601
title: indelible
categories:
    - Dictionary
---
indelible
     adj : cannot be removed, washed away or erased; "an indelible
           stain" [syn: {unerasable}]
