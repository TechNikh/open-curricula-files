---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ion
offline_file: ""
offline_thumbnail: ""
uuid: 1c03fae9-2794-4ec3-9a91-8c8c30e8411b
updated: 1484310204
title: ion
categories:
    - Dictionary
---
ion
     n : a particle that is electrically charged (positive or
         negative); an atom or molecule or group that has lost or
         gained one or more electrons
