---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/naked
offline_file: ""
offline_thumbnail: ""
uuid: 6e92aac9-abc0-4acf-9ec5-fed6ea2d4b36
updated: 1484310210
title: naked
categories:
    - Dictionary
---
naked
     adj 1: completely unclothed; "bare bodies"; "naked from the waist
            up"; "a nude model" [syn: {bare}, {au naturel(p)}, {nude}]
     2: having no protecting or concealing cover; "naked to mine
        enemies"- Shakespeare [syn: {defenseless}]
     3: (of the eye or ear e.g.) without the aid of an optical or
        acoustical device or instrument; "visible to the naked
        eye"; "clearly audible to the unaided ear" [syn: {unaided}]
     4: devoid of elaboration or diminution or concealment; bare and
        pure; "naked ambition"; "raw fury"; "you may kill someone
        someday with your raw power" [syn: {raw}]
