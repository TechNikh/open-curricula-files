---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weil_conjectures
offline_file: ""
offline_thumbnail: ""
uuid: fac342d3-ee2d-424d-85d5-4dbcdf7ecef6
updated: 1484308790
title: Weil conjectures
tags:
    - Background and history
    - Statement of the Weil conjectures
    - Examples
    - The projective line
    - Projective space
    - Elliptic curves
    - Weil cohomology
    - "Grothendieck's formula for the zeta function"
    - "Deligne's first proof"
    - Use of Lefschetz pencils
    - The key estimate
    - Completion of the proof
    - "Deligne's second proof"
    - Applications
categories:
    - Fixed points (mathematics)
---
In mathematics, the Weil conjectures were some highly influential proposals by André Weil (1949) on the generating functions (known as local zeta-functions) derived from counting the number of points on algebraic varieties over finite fields.
