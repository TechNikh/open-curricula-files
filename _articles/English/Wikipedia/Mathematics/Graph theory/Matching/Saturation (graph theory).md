---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Saturation_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 302453bf-0b71-4cd2-ba4e-b998f33859a8
updated: 1484309423
title: Saturation (graph theory)
categories:
    - Matching
---
Let 
  
    
      
        G
        (
        V
        ,
        E
        )
      
    
    {\displaystyle G(V,E)}
  
 be a graph and 
  
    
      
        M
      
    
    {\displaystyle M}
  
 a matching in 
  
    
      
        G
      
    
    {\displaystyle G}
  
. A vertex 
  
    
      
        v
        ∈
        V
        (
        G
        )
      
    
    {\displaystyle v\in V(G)}
  
 is said to be saturated by 
  
    
      
        M
      
    
    {\displaystyle M}
  
 if there is an edge in 
  
    
      
        M
      
    
    {\displaystyle M}
  
 incident to 
  
    
      
        v
      
    
    {\displaystyle v}
  
. A vertex 
  
    
      
      ...
