---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guarantee
offline_file: ""
offline_thumbnail: ""
uuid: a762b7f4-3188-4b80-ad69-31bb6c84629c
updated: 1484310589
title: guarantee
categories:
    - Dictionary
---
guarantee
     n 1: a written assurance that some product or service will be
          provided or will meet certain specifications [syn: {warrant},
           {warrantee}, {warranty}]
     2: a pledge that something will happen or that something is
        true; "there is no guarantee that they are not lying"
     3: a collateral agreement to answer for the debt of another in
        case that person defaults [syn: {guaranty}]
     v 1: give surety or assume responsibility; "I vouch for the
          quality of my products" [syn: {vouch}]
     2: make certain of; "This nest egg will ensure a nice
        retirement for us"; "Preparation will guarantee success!"
        [syn: {ensure}, ...
