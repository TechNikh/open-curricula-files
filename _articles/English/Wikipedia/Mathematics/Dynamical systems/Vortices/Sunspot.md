---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sunspot
offline_file: ""
offline_thumbnail: ""
uuid: 83d08c9b-471e-450f-9211-a25404c21773
updated: 1484308885
title: Sunspot
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Sunspot_butterfly_graph.gif
tags:
    - History
    - Physics
    - Lifecycle
    - Period
    - Solar cycle
    - Modern observation
    - Application
    - Starspot
    - Gallery
    - videos
    - Sunspot data
categories:
    - Vortices
---
Sunspots are temporary phenomena on the photosphere of the Sun that appear as dark spots compared to surrounding regions. They are areas of reduced surface temperature caused by concentrations of magnetic field flux that inhibit convection. Sunspots usually appear in pairs of opposite magnetic polarity.[2] Their number varies according to the approximately 11-year solar cycle.
