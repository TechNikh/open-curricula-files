---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/endpoint
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484354581
title: endpoint
categories:
    - Dictionary
---
end point
     n 1: a place where something ends or is complete [syn: {endpoint},
           {termination}, {terminus}]
     2: the final point in a process [syn: {resultant}]
