---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cheerfully
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484454481
title: cheerfully
categories:
    - Dictionary
---
cheerfully
     adv : in a cheerful manner; "he cheerfully agreed to do it" [ant:
           {cheerlessly}]
