---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commissioned
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484377561
title: commissioned
categories:
    - Dictionary
---
commissioned
     adj 1: of military officers; holding by virtue of a commission a
            rank of second lieutenant or ensign or above [ant: {noncommissioned}]
     2: given official approval to act; "an accredited college";
        "commissioned broker"; "licensed pharmacist"; "authorized
        representative" [syn: {accredited}, {licensed}, {licenced}]
