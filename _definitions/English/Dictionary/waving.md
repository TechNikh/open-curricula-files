---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/waving
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484437201
title: waving
categories:
    - Dictionary
---
waving
     adj : streaming or flapping or spreading wide as if in a current
           of air; "ran quickly, her flaring coat behind her";
           "flying banners"; "flags waving in the breeze" [syn: {aflare},
            {flaring}, {flying}]
     n : the act of signaling by a movement of the hand [syn: {wave},
          {wafture}]
