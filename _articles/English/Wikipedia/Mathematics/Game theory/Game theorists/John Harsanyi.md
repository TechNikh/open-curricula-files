---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_Harsanyi
offline_file: ""
offline_thumbnail: ""
uuid: 51c4f7e9-5d30-4847-b902-5ebda9886211
updated: 1484308513
title: John Harsanyi
tags:
    - Early life
    - Postwar
    - Australia
    - Later years
    - Publications
categories:
    - Game theorists
---
John Charles Harsanyi (Hungarian: Harsányi János Károly; May 29, 1920 – August 9, 2000) was a Hungarian-American economist and Nobel Memorial Prize in Economic Sciences winner.
