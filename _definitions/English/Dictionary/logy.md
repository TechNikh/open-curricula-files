---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/logy
offline_file: ""
offline_thumbnail: ""
uuid: bc952e70-0379-455a-95e5-c40440179999
updated: 1484310140
title: logy
categories:
    - Dictionary
---
logy
     adj : stunned or confused and slow to react (as from blows or
           drunkenness or exhaustion) [syn: {dazed}, {foggy}, {groggy},
            {stuporous}]
     [also: {logiest}, {logier}]
