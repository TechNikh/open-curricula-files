---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hush
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484459101
title: hush
categories:
    - Dictionary
---
hush
     n : (poetic) tranquil silence; "the still of the night" [syn: {stillness},
          {still}]
     v 1: become quiet or still; fall silent; "hush my babay!"
     2: cause to be quiet or not talk; "Please silence the children
        in the church!" [syn: {quieten}, {silence}, {still}, {shut
        up}, {hush up}] [ant: {louden}]
     3: become quiet or quieter; "The audience fell silent when the
        speaker entered" [syn: {quieten}, {quiet}, {quiesce}, {quiet
        down}, {pipe down}] [ant: {louden}]
     4: wash by removing particles; "Wash ores"
     5: run water over the ground to erode (soil), revealing the
        underlying strata and valuable minerals
