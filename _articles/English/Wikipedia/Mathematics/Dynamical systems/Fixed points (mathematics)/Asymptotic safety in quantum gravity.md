---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Asymptotic_safety_in_quantum_gravity
offline_file: ""
offline_thumbnail: ""
uuid: e33b1a61-c14e-43b7-8e95-4c6c47d048b9
updated: 1484308786
title: Asymptotic safety in quantum gravity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/380px-UVCriticalSurfaceInTheorySpace.png
tags:
    - Motivation
    - History of asymptotic safety
    - 'Asymptotic safety: The main idea'
    - Theory space
    - Renormalization group flow
    - Taking the UV limit
    - Gaussian and non-Gaussian fixed points
    - Quantum Einstein Gravity (QEG)
    - Implementation via the effective average action
    - Exact functional renormalization group equation
    - Truncations of the theory space
    - Evidence for asymptotic safety from truncated flow equations
    - The Einstein–Hilbert truncation
    - Extended truncations
    - The microscopic structure of spacetime
    - Physics applications of asymptotically safe gravity
categories:
    - Fixed points (mathematics)
---
Asymptotic safety (sometimes also referred to as nonperturbative renormalizability) is a concept in quantum field theory which aims at finding a consistent and predictive quantum theory of the gravitational field. Its key ingredient is a nontrivial fixed point of the theory's renormalization group flow which controls the behavior of the coupling constants in the ultraviolet (UV) regime and renders physical quantities safe from divergences. Although originally proposed by Steven Weinberg to find a theory of quantum gravity, the idea of a nontrivial fixed point providing a possible UV completion can be applied also to other field theories, in particular to perturbatively nonrenormalizable ...
