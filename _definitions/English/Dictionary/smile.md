---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smile
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484434861
title: smile
categories:
    - Dictionary
---
smile
     n : a facial expression characterized by turning up the corners
         of the mouth; usually shows pleasure or amusement [syn: {smiling},
          {grin}, {grinning}]
     v 1: change one's facial expression by spreading the lips, often
          to signal pleasure
     2: express with a smile; "She smiled her thanks"
