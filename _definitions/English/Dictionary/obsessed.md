---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obsessed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484524021
title: obsessed
categories:
    - Dictionary
---
obsessed
     adj 1: having or showing excessive or compulsive concern with
            something; "became more and more haunted by the stupid
            riddle"; "was absolutely obsessed with the girl"; "got
            no help from his wife who was preoccupied with the
            children"; "he was taken up in worry for the old
            woman" [syn: {haunted}, {preoccupied}, {taken up(p)}]
     2: influenced or controlled by a powerful force such as a
        strong emotion; "by love possessed" [syn: {possessed(p)}]
