---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ernst
offline_file: ""
offline_thumbnail: ""
uuid: d87de306-2a23-4f0e-9c36-8c21537311c9
updated: 1484310391
title: ernst
categories:
    - Dictionary
---
Ernst
     n : painter (born in Germany, resident of France and the United
         States) who was a cofounder of Dadaism; developed the
         technique of collage (1891-1976) [syn: {Max Ernst}]
