---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nonmetal
offline_file: ""
offline_thumbnail: ""
uuid: a867d30f-7831-4d76-840d-ed1763e02dfa
updated: 1484310377
title: nonmetal
categories:
    - Dictionary
---
nonmetal
     adj : not containing or resembling or characteristic of a metal;
           "nonmetallic elements" [syn: {nonmetallic}] [ant: {metallic}]
     n : a chemical element lacking typical metallic properties
