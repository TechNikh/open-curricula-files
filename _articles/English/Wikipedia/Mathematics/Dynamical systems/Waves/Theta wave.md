---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Theta_wave
offline_file: ""
offline_thumbnail: ""
uuid: 82435115-3e8e-4b75-9251-999d57d28ac4
updated: 1484308896
title: Theta wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Eeg_theta.svg.png
tags:
    - History
    - Terminology
    - Hippocampal
    - Type 1 and type 2
    - Relationship with behavior
    - Mechanisms
    - Generators
    - Research findings
    - Humans and other primates
    - Brain waves
categories:
    - Waves
---
Theta waves generate the theta rhythm a neural oscillatory pattern in electroencephalography (EEG) signals, recorded either from inside the brain or from electrodes glued to the scalp. Two types of theta rhythm have been described. The "hippocampal theta rhythm" is a strong oscillation that can be observed in the hippocampus and other brain structures in numerous species of mammals including rodents, rabbits, dogs, cats, bats, and marsupials. "Cortical theta rhythms" are low-frequency components of scalp EEG, usually recorded from humans. Theta rhythms can be quantified using Quantitative electroencephalography (qEEG) using freely available toolboxes, such as, EEGLAB or the ...
