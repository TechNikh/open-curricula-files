---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/channel
offline_file: ""
offline_thumbnail: ""
uuid: 4c550bc4-c6e1-429d-b019-6574ecb44fbb
updated: 1484310459
title: channel
categories:
    - Dictionary
---
channel
     n 1: a path over which electrical signals can pass; "a channel is
          typically what you rent from a telephone company" [syn:
          {transmission channel}]
     2: a passage for water (or other fluids) to flow through; "the
        fields were crossed with irrigation channels"; "gutters
        carried off the rainwater into a series of channels under
        the street"
     3: a long narrow furrow cut either by a natural process (such
        as erosion) or by a tool (as e.g. a groove in a phonograph
        record) [syn: {groove}]
     4: a deep and relatively narrow body of water (as in a river or
        a harbor or a strait linking two larger bodies) that
       ...
