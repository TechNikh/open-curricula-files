---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/toxic
offline_file: ""
offline_thumbnail: ""
uuid: 6d749e68-2b0d-4f64-8f06-79e740233a7f
updated: 1484310238
title: toxic
categories:
    - Dictionary
---
toxic
     adj : of or relating to or caused by a toxin or poison; "suffering
           from exposure to toxic substances" [ant: {nontoxic}]
