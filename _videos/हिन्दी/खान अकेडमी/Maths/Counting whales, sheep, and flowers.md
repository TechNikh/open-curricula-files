---
version: 1
type: video
provider: YouTube
id: ppbQmsrOQaU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6398c25b-14e9-4981-989c-9d70e70abfc2
updated: 1486069605
title: Counting whales, sheep, and flowers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ppbQmsrOQaU/default.jpg
    - https://i3.ytimg.com/vi/ppbQmsrOQaU/1.jpg
    - https://i3.ytimg.com/vi/ppbQmsrOQaU/2.jpg
    - https://i3.ytimg.com/vi/ppbQmsrOQaU/3.jpg
---
