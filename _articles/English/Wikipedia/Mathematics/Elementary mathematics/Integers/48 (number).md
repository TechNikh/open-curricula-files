---
version: 1
type: article
id: https://en.wikipedia.org/wiki/48_(number)
offline_file: ""
offline_thumbnail: ""
uuid: deb0d37b-e451-45bc-a20e-e83e05f07248
updated: 1484308958
title: 48 (number)
tags:
    - In mathematics
    - In science
    - Astronomy
    - In religion
    - In music
    - In sports
    - In other fields
categories:
    - Integers
---
