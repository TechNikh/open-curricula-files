---
version: 1
type: video
provider: YouTube
id: R8JMfbYW2p4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0f0719dc-9605-43ac-b6c2-7586572742c5
updated: 1486069670
title: Platelet Activation and Factors for Clot Formation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/R8JMfbYW2p4/default.jpg
    - https://i3.ytimg.com/vi/R8JMfbYW2p4/1.jpg
    - https://i3.ytimg.com/vi/R8JMfbYW2p4/2.jpg
    - https://i3.ytimg.com/vi/R8JMfbYW2p4/3.jpg
---
