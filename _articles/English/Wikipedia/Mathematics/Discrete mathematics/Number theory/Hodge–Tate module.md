---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hodge%E2%80%93Tate_module'
offline_file: ""
offline_thumbnail: ""
uuid: d99ece2a-6b47-476e-9cd8-fd056ab3708a
updated: 1484308645
title: Hodge–Tate module
categories:
    - Number theory
---
In mathematics, a Hodge–Tate module is an analogue of a Hodge structure over p-adic fields. Serre (1967) introduced and named Hodge–Tate structures using the results of Tate (1967) on p-divisible groups.
