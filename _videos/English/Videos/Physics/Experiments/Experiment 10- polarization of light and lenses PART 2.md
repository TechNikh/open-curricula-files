---
version: 1
type: video
provider: YouTube
id: Xj80LsdwwTw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4904c74d-97cd-4111-8f35-29f5cc5f4810
updated: 1486069639
title: 'Experiment 10- polarization of light and lenses PART 2'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/Xj80LsdwwTw/default.jpg
    - https://i3.ytimg.com/vi/Xj80LsdwwTw/1.jpg
    - https://i3.ytimg.com/vi/Xj80LsdwwTw/2.jpg
    - https://i3.ytimg.com/vi/Xj80LsdwwTw/3.jpg
---
