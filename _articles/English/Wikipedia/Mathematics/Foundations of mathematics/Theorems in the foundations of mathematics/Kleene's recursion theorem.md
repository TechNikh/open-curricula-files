---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kleene%27s_recursion_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: f8e3b6d5-bbbf-4f9d-b901-55cf49093537
updated: 1484308997
title: "Kleene's recursion theorem"
tags:
    - Notation
    - "Rogers' fixed-point theorem"
    - Proof of the fixed-point theorem
    - Fixed-point free functions
    - "Kleene's second recursion theorem"
    - "Comparison to Rogers' theorem"
    - Application to elimination of recursion
    - Application to quines
    - Reflexive programming
    - The first recursion theorem
    - Example
    - Proof sketch for the first recursion theorem
    - Comparison to the second recursion theorem
    - Generalized theorem by A.I. Maltsev
    - Notes
categories:
    - Theorems in the foundations of mathematics
---
In computability theory, Kleene's recursion theorems are a pair of fundamental results about the application of computable functions to their own descriptions. The theorems were first proved by Stephen Kleene in 1938 and appear in his 1952 book Introduction to Metamathematics.
