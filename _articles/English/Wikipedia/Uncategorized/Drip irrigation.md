---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Micro-irrigation
offline_file: ""
offline_thumbnail: ""
uuid: ae9ba4fd-84ba-41ee-99d3-236e1539d074
updated: 1484308331
title: Drip irrigation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/213px-Dripperwithdrop.png
tags:
    - History
    - Components and operation
    - Advantages and disadvantages
    - Uses
categories:
    - Uncategorized
---
Drip irrigation is a form of irrigation that saves water and fertilizer by allowing water to drip slowly to the roots of many different plants, either onto the soil surface or directly onto the root zone, through a network of valves, pipes, tubing, and emitters. It is done through narrow tubes that deliver water directly to the base of the plant. It is chosen instead of surface irrigation for various reasons, often including concern about minimizing evaporation.
