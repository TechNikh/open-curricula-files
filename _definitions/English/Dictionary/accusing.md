---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accusing
offline_file: ""
offline_thumbnail: ""
uuid: 3ad2d8c8-33cc-413d-82df-75bbb586e040
updated: 1484310174
title: accusing
categories:
    - Dictionary
---
accusing
     adj : containing or expressing accusation; "an accusitive
           forefinger"; "black accusatory looks"; "accusive shoes
           and telltale trousers"- O.Henry; "his accusing glare"
           [syn: {accusative}, {accusatory}, {accusive}]
