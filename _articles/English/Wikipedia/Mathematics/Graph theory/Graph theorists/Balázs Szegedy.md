---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bal%C3%A1zs_Szegedy'
offline_file: ""
offline_thumbnail: ""
uuid: 283cd528-7356-4471-86cf-9b0963a9b4c7
updated: 1484309476
title: Balázs Szegedy
categories:
    - Graph theorists
---
Szegedy earned a master's degree in 1998 and a Ph.D. in 2003 from Eötvös Loránd University in Budapest.[1] His dissertation, supervised by Péter Pál Pálfy, was about group theory and was entitled "On the Sylow and Borel subgroups of classical groups".[2] After temporary positions at the Alfréd Rényi Institute of Mathematics, Microsoft Research, and the Institute for Advanced Study, he joined the faculty of the University of Toronto Scarborough in 2006. He returned to the Rényi Institute in 2013.[1]
