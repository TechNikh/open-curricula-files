---
version: 1
type: video
provider: YouTube
id: eyuRLmCphHc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Fourth%20Dimension%20rotation%20of%204D%20spheres%2C%20tetrahedrons%2C%20and%20cubes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 15b17a1f-2844-46b8-af01-07ac3b2e5c0c
updated: 1486069654
title: >
    Fourth Dimension rotation of 4D spheres, tetrahedrons, and
    cubes
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/eyuRLmCphHc/default.jpg
    - https://i3.ytimg.com/vi/eyuRLmCphHc/1.jpg
    - https://i3.ytimg.com/vi/eyuRLmCphHc/2.jpg
    - https://i3.ytimg.com/vi/eyuRLmCphHc/3.jpg
---
