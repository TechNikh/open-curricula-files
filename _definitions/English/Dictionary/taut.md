---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/taut
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484351101
title: taut
categories:
    - Dictionary
---
taut
     adj 1: pulled or drawn tight; "taut sails"; "a tight drumhead"; "a
            tight rope" [syn: {tight}]
     2: subjected to great tension; stretched tight; "the skin of
        his face looked drawn and tight"; "her nerves were taut as
        the strings of a bow" [syn: {drawn}]
