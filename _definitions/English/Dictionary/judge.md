---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/judge
offline_file: ""
offline_thumbnail: ""
uuid: fcaf2f2c-a582-4aa4-92c0-de13798e2d91
updated: 1484310220
title: judge
categories:
    - Dictionary
---
judge
     n 1: a public official authorized to decide questions bought
          before a court of justice [syn: {justice}, {jurist}, {magistrate}]
     2: an authority who is able to estimate worth or quality [syn:
        {evaluator}]
     v 1: determine the result of (a competition)
     2: form an opinion of or pass judgment on; "I cannot judge some
        works of modern art"
     3: judge tentatively or form an estimate of (quantities or
        time); "I estimate this chicken to weigh three pounds"
        [syn: {estimate}, {gauge}, {approximate}, {guess}]
     4: pronounce judgment on; "They labeled him unfit to work here"
        [syn: {pronounce}, {label}]
     5: put on trial ...
