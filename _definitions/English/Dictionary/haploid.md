---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/haploid
offline_file: ""
offline_thumbnail: ""
uuid: fc19a51f-1d01-47f2-b7d1-94c3be022d0c
updated: 1484310160
title: haploid
categories:
    - Dictionary
---
haploid
     adj : of a cell or organism having a single set of chromosomes
           [syn: {haploidic}, {monoploid}] [ant: {diploid}, {polyploid}]
     n : (genetics) an organism or cell having only one complete set
         of chromosomes
