---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flushed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448241
title: flushed
categories:
    - Dictionary
---
flushed
     adj 1: having the pinkish flush of health [syn: {rose-cheeked}, {rosy},
             {rosy-cheeked}]
     2: (especially of the face) reddened or suffused with or as if
        with blood from emotion or exertion; "crimson with fury";
        "turned red from exertion"; "with puffy reddened eyes";
        "red-faced and violent"; "flushed (or crimson) with
        embarrassment" [syn: {crimson}, {red}, {reddened}, {red-faced}]
