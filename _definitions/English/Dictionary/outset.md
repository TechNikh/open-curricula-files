---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outset
offline_file: ""
offline_thumbnail: ""
uuid: c4db91fd-4d39-4dd7-92a2-8aed1733337c
updated: 1484310581
title: outset
categories:
    - Dictionary
---
outset
     n : the time at which something is supposed to begin; "they got
         an early start"; "she knew from the get-go that he was
         the man for her" [syn: {beginning}, {commencement}, {first},
          {get-go}, {start}, {kickoff}, {starting time}, {showtime},
          {offset}] [ant: {middle}, {end}]
