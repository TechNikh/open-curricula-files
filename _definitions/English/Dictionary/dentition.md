---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dentition
offline_file: ""
offline_thumbnail: ""
uuid: b4608e83-b7b9-49c7-be4d-919260cc591b
updated: 1484310337
title: dentition
categories:
    - Dictionary
---
dentition
     n 1: the eruption through the gums of baby teeth [syn: {teething},
           {odontiasis}]
     2: the kind and number and arrangement of teeth (collectively)
        in a person or animal [syn: {teeth}]
