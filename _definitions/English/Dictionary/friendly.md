---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/friendly
offline_file: ""
offline_thumbnail: ""
uuid: f889291e-d4ff-42df-b435-8f0cb0f311cb
updated: 1484310529
title: friendly
categories:
    - Dictionary
---
friendly
     adj 1: characteristic of or befitting a friend; "friendly advice";
            "a friendly neighborhood"; "the only friendly person
            here"; "a friendly host and hostess" [ant: {unfriendly}]
     2: favorably disposed; not antagonistic or hostile; "a
        government friendly to our interests"; "an amicable
        agreement"
     3: easy to understand or use; "user-friendly computers"; "a
        consumer-friendly policy"; "a reader-friendly novel" [ant:
         {unfriendly}]
     4: of or belonging to your own country's forces or those of an
        ally; "in friendly territory"; "he was accidentally killed
        by friendly fire" [ant: {hostile}]
     [also: ...
