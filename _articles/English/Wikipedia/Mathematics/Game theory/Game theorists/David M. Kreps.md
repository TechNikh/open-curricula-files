---
version: 1
type: article
id: https://en.wikipedia.org/wiki/David_M._Kreps
offline_file: ""
offline_thumbnail: ""
uuid: 1aed2cf0-3936-452b-88f4-31a01c85e509
updated: 1484308513
title: David M. Kreps
categories:
    - Game theorists
---
David Marc "Dave" Kreps (born 1950 in New York) is a game theorist and economist and professor at the Graduate School of Business at Stanford University. He is known for his analysis of dynamic choice models and non-cooperative game theory, particularly the idea of sequential equilibrium, which he developed with Stanford Business School colleague Robert B. Wilson.
