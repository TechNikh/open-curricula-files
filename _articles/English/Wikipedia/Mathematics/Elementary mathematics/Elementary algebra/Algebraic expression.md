---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_expression
offline_file: ""
offline_thumbnail: ""
uuid: fdba9c9b-c20d-454e-85d1-bde979989d11
updated: 1484308907
title: Algebraic expression
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg.png
tags:
    - Terminology
    - In roots of polynomials
    - Conventions
    - Variables
    - Exponents
    - Algebraic vs. other mathematical expressions
    - Notes
categories:
    - Elementary algebra
---
In mathematics, an algebraic expression is an expression built up from integer constants, variables, and the algebraic operations (addition, subtraction, multiplication, division and exponentiation by an exponent that is a rational number).[1] For example, 
  
    
      
        3
        
          x
          
            2
          
        
        −
        2
        x
        y
        +
        c
      
    
    {\displaystyle 3x^{2}-2xy+c}
  
 is an algebraic expression. Since taking the square root is the same as raising to the power 
  
    
      
        
          
            
              1
              2
            
          
        
      
    
    {\displaystyle ...
