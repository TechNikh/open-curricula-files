---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/miserly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484603641
title: miserly
categories:
    - Dictionary
---
miserly
     adj : used of persons or behavior; characterized by or indicative
           of lack of generosity; "a mean person"; "he left a
           miserly tip" [syn: {mean}, {mingy}, {tight}]
