---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fraternity
offline_file: ""
offline_thumbnail: ""
uuid: 0f345895-3560-40a3-ada6-6de05806484f
updated: 1484310547
title: fraternity
categories:
    - Dictionary
---
fraternity
     n 1: a social club for male undergraduates [syn: {frat}]
     2: people engaged in a particular occupation; "the medical
        fraternity" [syn: {brotherhood}, {sodality}]
