---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Holland%27s_schema_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 34fc140a-1284-408b-b75a-1b8f76a27532
updated: 1484308661
title: "Holland's schema theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bimodal-bivariate-small.png
categories:
    - Theorems in discrete mathematics
---
Holland's schema theorem, also called the fundamental theorem of genetic algorithms,[1] is widely taken to be the foundation for explanations of the power of genetic algorithms. It says that short, low-order schemata with above-average fitness increase exponentially in successive generations. The theorem was proposed by John Holland in the 1970s.
