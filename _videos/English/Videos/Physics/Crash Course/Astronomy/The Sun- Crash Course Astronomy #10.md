---
version: 1
type: video
provider: YouTube
id: b22HKFMIfWo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/The%20Sun-%20Crash%20Course%20Astronomy%20%2310.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 74d63f52-8096-4324-bfc9-924638d0bbb6
updated: 1486069684
title: 'The Sun- Crash Course Astronomy #10'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/b22HKFMIfWo/default.jpg
    - https://i3.ytimg.com/vi/b22HKFMIfWo/1.jpg
    - https://i3.ytimg.com/vi/b22HKFMIfWo/2.jpg
    - https://i3.ytimg.com/vi/b22HKFMIfWo/3.jpg
---
