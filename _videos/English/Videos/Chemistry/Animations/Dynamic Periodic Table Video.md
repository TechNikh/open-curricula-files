---
version: 1
type: video
provider: YouTube
id: 9IOYmPCbIkU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 600944f6-d766-4240-b304-deb09ccf5dbd
updated: 1486069662
title: Dynamic Periodic Table Video
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/9IOYmPCbIkU/default.jpg
    - https://i3.ytimg.com/vi/9IOYmPCbIkU/1.jpg
    - https://i3.ytimg.com/vi/9IOYmPCbIkU/2.jpg
    - https://i3.ytimg.com/vi/9IOYmPCbIkU/3.jpg
---
