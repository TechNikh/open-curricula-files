---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unnerved
offline_file: ""
offline_thumbnail: ""
uuid: 7d3e7fba-0494-42e2-ae2c-b201ca14d5ce
updated: 1484310557
title: unnerved
categories:
    - Dictionary
---
unnerved
     adj : deprived of courage and strength; "the steeplejack,
           exhausted and unnerved, couldn't hold on to his
           dangerous perch much longer"
