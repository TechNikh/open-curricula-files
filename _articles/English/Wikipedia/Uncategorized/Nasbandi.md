---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nasbandi
offline_file: ""
offline_thumbnail: ""
uuid: 60d67576-afad-4ba1-8f2b-ad928699f961
updated: 1484308438
title: Nasbandi
tags:
    - Cast
    - Soundtracks
categories:
    - Uncategorized
---
Nasbandi is a 1978 Bollywood satirical comedy film directed by I. S. Johar, it starred duplicates of all the popular heroes of the time.[2] It is a satire on the sterilization drive of the government of India during Indira Gandhi ruling period. Each of the characters in the film is trying to find sterilization cases.
