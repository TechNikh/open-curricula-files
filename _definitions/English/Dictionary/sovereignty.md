---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sovereignty
offline_file: ""
offline_thumbnail: ""
uuid: dc7ffcf8-e723-4610-b714-38382bc3d97e
updated: 1484310591
title: sovereignty
categories:
    - Dictionary
---
sovereignty
     n 1: government free from external control
     2: royal authority; the dominion of a monarch [syn: {reign}]
