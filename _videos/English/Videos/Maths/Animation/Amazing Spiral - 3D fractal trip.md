---
version: 1
type: video
provider: YouTube
id: A7Od1umtdYs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 119f8349-1cdd-46d3-895f-89711319525b
updated: 1486069651
title: 'Amazing Spiral - 3D fractal trip'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/A7Od1umtdYs/default.jpg
    - https://i3.ytimg.com/vi/A7Od1umtdYs/1.jpg
    - https://i3.ytimg.com/vi/A7Od1umtdYs/2.jpg
    - https://i3.ytimg.com/vi/A7Od1umtdYs/3.jpg
---
