---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_coloring
offline_file: ""
offline_thumbnail: ""
uuid: ebfbc4bb-4569-469e-8e43-2ffd46594a3d
updated: 1484309386
title: List coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-List-coloring-K-3-27.svg.png
tags:
    - Definition
    - Examples
    - Properties
    - Computing choosability and (a, b)-choosability
    - Applications
categories:
    - Graph coloring
---
In graph theory, a branch of mathematics, list coloring is a type of graph coloring where each vertex can be restricted to a list of allowed colors. It was first studied in the 1976 Vizing.[1]
