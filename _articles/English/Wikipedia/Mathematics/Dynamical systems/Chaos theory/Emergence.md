---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Emergence
offline_file: ""
offline_thumbnail: ""
uuid: ed6cc1e1-364a-473b-a6fd-a3ad683991d4
updated: 1484308715
title: Emergence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-SnowflakesWilsonBentley.jpg
tags:
    - In philosophy
    - Definitions
    - Strong and weak emergence
    - Objective or subjective quality
    - In religion, art and humanities
    - Emergent properties and processes
    - Emergent structures in nature
    - Non-living, physical systems
    - Living, biological systems
    - Emergence and evolution
    - Organization of life
    - In humanity
    - Spontaneous order
    - Economics
    - World Wide Web and the Internet
    - Architecture and cities
    - Computer AI
    - language
    - Emergent change processes
    - Bibliography
categories:
    - Chaos theory
---
In philosophy, systems theory, science, and art, emergence is a process whereby larger entities arise through interactions among smaller or simpler entities such that the larger entities exhibit properties the smaller/simpler entities do not exhibit.
