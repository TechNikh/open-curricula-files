---
version: 1
type: article
id: https://en.wikipedia.org/wiki/17_(number)
offline_file: ""
offline_thumbnail: ""
uuid: cad05895-2035-4995-8951-270a77a6aebc
updated: 1484308948
title: 17 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Alitalia-17.jpg
tags:
    - In mathematics
    - In science
    - Age 17
    - In culture
    - Music
    - Bands
    - Albums
    - Songs
    - Other
    - Film
    - Anime and manga
    - Games
    - Print
    - religion
    - In sports
    - In other fields
categories:
    - Integers
---
In spoken English, the numbers 17 and 70 are sometimes confused because they sound similar. When carefully enunciated, they differ in which syllable is stressed: 17 /sɛvənˈtiːn/ vs 70 /ˈsɛvənti/. However, in dates such as 1789 or when contrasting numbers in the teens, such as 16, 17, 18, the stress shifts to the first syllable: 17 /ˈsɛvəntiːn/.
