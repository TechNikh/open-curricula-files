---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Mis%C3%A8re'
offline_file: ""
offline_thumbnail: ""
uuid: 982cb34f-1a9f-4dcc-83b5-9b90420ae571
updated: 1484308533
title: Misère
tags:
    - Misère game
    - Other uses
categories:
    - Combinatorial game theory
---
Misere or misère (French for "destitution"; equivalent terms in other languages include bettel, contrabola, devole, null, pobre) is a bid in various card games, and the player who bids misere undertakes to win no tricks or as few as possible, usually at no trump, in the round to be played. This does not allow sufficient variety to constitute a game in its own right, but it is the basis of such trick-avoidance games as Hearts, and provides an optional contract for most games involving an auction.[1]
