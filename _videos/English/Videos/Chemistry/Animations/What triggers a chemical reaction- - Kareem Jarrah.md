---
version: 1
type: video
provider: YouTube
id: 8m6RtOpqvtU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 02beb620-cf16-4fea-bcd7-722fc8d39a20
updated: 1486069662
title: 'What triggers a chemical reaction? - Kareem Jarrah'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/8m6RtOpqvtU/default.jpg
    - https://i3.ytimg.com/vi/8m6RtOpqvtU/1.jpg
    - https://i3.ytimg.com/vi/8m6RtOpqvtU/2.jpg
    - https://i3.ytimg.com/vi/8m6RtOpqvtU/3.jpg
---
