---
version: 1
type: video
provider: YouTube
id: xRGjQdHToNA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/The%20Apostrophe/Apostrophes%20and%20plurals.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c1015262-9ef3-431e-9a76-6c54ad586e51
updated: 1486069603
title: Apostrophes and plurals
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The Apostrophe
thumbnail_urls:
    - https://i3.ytimg.com/vi/xRGjQdHToNA/default.jpg
    - https://i3.ytimg.com/vi/xRGjQdHToNA/1.jpg
    - https://i3.ytimg.com/vi/xRGjQdHToNA/2.jpg
    - https://i3.ytimg.com/vi/xRGjQdHToNA/3.jpg
---
There is *one* extremely rare case in which we use apostrophes to make things plural. David and Paige, KA's resident grammarians, discuss this unusual case.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/e/apostrophes-and-plurals

Watch the next lesson:
https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/choosing-between-its-and-its-the-apostrophe-punctuation-khan-academy


Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/advanced-plural-possession-the-apostrophe-punctuation-khan-academy

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
