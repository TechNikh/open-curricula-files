---
version: 1
type: article
id: https://en.wikipedia.org/wiki/David_Francis_Barrow
offline_file: ""
offline_thumbnail: ""
uuid: 3267787e-d0c7-49b9-8901-b5418d1bde2f
updated: 1484309071
title: David Francis Barrow
categories:
    - Geometers
---
Barrow's father, David Crenshaw Barrow, Jr., was also a mathematician, and served as chancellor of the University of Georgia from 1906 to 1925. His son, David F. Barrow, did his undergraduate studies at the University of Georgia and then studied at Harvard University, where he earned his Ph.D. in 1913. After a year abroad, he taught for two years at the University of Texas, and then at the Sheffield Scientific School. After a brief stint in the U.S. armed services, he joined the faculty of his father's university in 1920. He became a full professor in 1923, and chaired the mathematics department in 1944–1945.[1]
