---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Network_science
offline_file: ""
offline_thumbnail: ""
uuid: 4e8afe9b-addd-4a59-bcd4-efb1643e4095
updated: 1484309438
title: Network science
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Moreno_Sociogram_1st_Grade.png
tags:
    - Background and history
    - Department of Defense initiatives
    - Network properties
    - density
    - Size
    - Average degree
    - Average path length (or Characteristic path length)
    - Diameter of a network
    - Clustering coefficient
    - Connectedness
    - Node centrality
    - Node influence
    - Network models
    - Erdős–Rényi random graph model
    - Watts-Strogatz small world model
    - Barabási–Albert (BA) preferential attachment model
    - Fitness model
    - Network analysis
    - Social network analysis
    - Dynamic network analysis
    - Biological network analysis
    - Link analysis
    - Network robustness
    - Pandemic analysis
    - Susceptible to infected
    - Infected to recovered
    - Infectious period
    - Web link analysis
    - PageRank
    - Random jumping
    - Centrality measures
    - Spread of content in networks
    - The SIR model
    - The master equation approach
    - Interdependent networks
    - Multilayer networks
    - Network optimization
    - Network science research centers
    - Network analysis and visualization tools
    - Notes
categories:
    - Network theory
---
Network science is an academic field which studies complex networks such as telecommunication networks, computer networks, biological networks, cognitive and semantic networks, and social networks, considering distinct elements or actors represented by nodes (or vertices) and the connections between the elements or actors as links (or edges). The field draws on theories and methods including graph theory from mathematics, statistical mechanics from physics, data mining and information visualization from computer science, inferential modeling from statistics, and social structure from sociology. The United States National Research Council defines network science as "the study of network ...
