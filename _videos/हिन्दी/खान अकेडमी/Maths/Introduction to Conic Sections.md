---
version: 1
type: video
provider: YouTube
id: ikzoI7K5q3o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9946dc90-49bf-4536-9ece-440370538656
updated: 1486069609
title: Introduction to Conic Sections
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ikzoI7K5q3o/default.jpg
    - https://i3.ytimg.com/vi/ikzoI7K5q3o/1.jpg
    - https://i3.ytimg.com/vi/ikzoI7K5q3o/2.jpg
    - https://i3.ytimg.com/vi/ikzoI7K5q3o/3.jpg
---
