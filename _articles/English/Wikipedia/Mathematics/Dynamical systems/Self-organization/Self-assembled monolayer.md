---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-assembled_monolayer
offline_file: ""
offline_thumbnail: ""
uuid: 03c9936a-5557-48b9-8ca0-58f2a8278aac
updated: 1484308822
title: Self-assembled monolayer
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/231px-SAM_schematic.jpeg
tags:
    - Types
    - Preparation
    - Characterization
    - Defects
    - Nanoparticle properties
    - Kinetics
    - Patterning
    - 1. Locally attract
    - 2. Locally remove
    - 3. Modify tail groups
    - Applications
    - Thin-film SAMs
    - Patterned SAMs
    - Metal organic superlattices
categories:
    - Self-organization
---
Self-assembled monolayers (SAM) of organic molecules are molecular assemblies formed spontaneously on surfaces by adsorption and are organized into more or less large ordered domains.[1][2] In some cases molecules that form the monolayer do not interact strongly with the substrate. This is the case for instance of the two-dimensional supramolecular networks[3] of e.g. Perylene-tetracarboxylicacid-dianhydride (PTCDA) on gold[4] or of e.g. porphyrins on highly oriented pyrolitic graphite (HOPG).[5] In other cases the molecules possess a head group that has a strong affinity to the substrate and anchors the molecule to it.[1] Such a SAM consisting of a head group, tail and functional end group ...
