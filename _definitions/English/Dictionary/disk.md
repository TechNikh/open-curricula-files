---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disk
offline_file: ""
offline_thumbnail: ""
uuid: fdfa89f2-33a8-40b8-9990-d6faa8c4d280
updated: 1484310214
title: disk
categories:
    - Dictionary
---
disk
     n 1: something with a round shape like a flat circular plate
          [syn: {disc}, {saucer}]
     2: a flat circular plate [syn: {disc}]
     3: sound recording consisting of a disc with continuous
        grooves; formerly used to reproduce music by rotating
        while a phonograph needle tracked in the grooves [syn: {phonograph
        record}, {phonograph recording}, {record}, {disc}, {platter}]
     4: (computer science) a memory device consisting of a flat disk
        covered with a magnetic coating on which information is
        stored [syn: {magnetic disk}, {magnetic disc}, {disc}]
     v : draw a harrow over (land) [syn: {harrow}]
