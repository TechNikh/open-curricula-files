---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gardiner
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484499001
title: gardiner
categories:
    - Dictionary
---
Gardiner
     n : British historian remembered for his ten-volume history of
         England (1829-1902) [syn: {Samuel Rawson Gardiner}]
