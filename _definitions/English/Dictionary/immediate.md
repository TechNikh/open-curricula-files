---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immediate
offline_file: ""
offline_thumbnail: ""
uuid: 15e1e754-918a-4a35-9e43-fb08ab0875ea
updated: 1484310549
title: immediate
categories:
    - Dictionary
---
immediate
     adj 1: very close or connected in space or time; "contiguous
            events"; "immediate contact"; "the immediate
            vicinity"; "the immediate past" [syn: {contiguous}]
     2: having no intervening medium; "an immediate influence" [ant:
         {mediate}]
     3: immediately before or after as in a chain of cause and
        effect; "the immediate result"; "the immediate cause of
        the trouble"
     4: of the present time and place; "the immediate revisions"
     5: performed with little or no delay; "an immediate reply to my
        letter"; "prompt obedience"; "was quick to respond"; "a
        straightaway denial" [syn: {prompt}, {quick}, ...
