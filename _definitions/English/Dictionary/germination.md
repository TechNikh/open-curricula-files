---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/germination
offline_file: ""
offline_thumbnail: ""
uuid: b4bfa28a-6953-4625-8c55-2ec9068625e5
updated: 1484310158
title: germination
categories:
    - Dictionary
---
germination
     n 1: the process whereby seeds or spores sprout and begin to grow
          [syn: {sprouting}]
     2: the origin of some development; "the germination of their
        discontent"
