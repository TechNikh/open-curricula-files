---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multi-stage_game
uuid: 7a8c4217-1188-420f-82b7-7a000aedfee2
updated: 1480455025
title: Multi-stage game
categories:
    - Game theory game classes
---
This is a generalization of a repeated game: a repeated game is a special case of a multi-stage game, in which the stage games are identical.
