---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/institutional
offline_file: ""
offline_thumbnail: ""
uuid: d93d02b8-2a87-4493-aef1-ad1d9e394bd1
updated: 1484310467
title: institutional
categories:
    - Dictionary
---
institutional
     adj 1: relating to or constituting or involving an institution;
            "institutional policy"
     2: organized as or forming an institution; "institutional
        religion" [ant: {noninstitutional}]
