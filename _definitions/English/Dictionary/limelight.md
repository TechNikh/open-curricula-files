---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/limelight
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484442241
title: limelight
categories:
    - Dictionary
---
limelight
     n 1: a focus of public attention; "he enjoyed being in the
          limelight"; "when Congress investigates it brings the
          full glare of publicity to the agency" [syn: {spotlight},
           {glare}, {public eye}]
     2: a lamp consisting of a flame directed at a cylinder of lime
        with a lens to concentrate the light; formerly used for
        stage lighting [syn: {calcium light}]
