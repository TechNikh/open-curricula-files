---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/isi
offline_file: ""
offline_thumbnail: ""
uuid: 44337e7d-f21e-47a8-9344-1a0d954aa246
updated: 1484310140
title: isi
categories:
    - Dictionary
---
ISI
     n : the Pakistan intelligence agency; a powerful and almost
         autonomous political and military force; has procured
         nuclear technology and delivery capabilities; has had
         strong ties with the Taliban and other militant Islamic
         groups [syn: {Directorate for Inter-Services Intelligence},
          {Inter-Services Intelligence}]
