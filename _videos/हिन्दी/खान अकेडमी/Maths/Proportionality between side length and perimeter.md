---
version: 1
type: video
provider: YouTube
id: r7z8pBXag3w
offline_file: ""
offline_thumbnail: ""
uuid: 0e8b9ff8-5486-4ea2-ac5b-bb8a94e2ea27
updated: 1484040352
title: Proportionality between side length and perimeter
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/r7z8pBXag3w/default.jpg
    - https://i3.ytimg.com/vi/r7z8pBXag3w/1.jpg
    - https://i3.ytimg.com/vi/r7z8pBXag3w/2.jpg
    - https://i3.ytimg.com/vi/r7z8pBXag3w/3.jpg
---
