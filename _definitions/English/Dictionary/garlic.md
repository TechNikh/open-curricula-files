---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/garlic
offline_file: ""
offline_thumbnail: ""
uuid: 7c971527-911f-4279-a278-7ba23e5881f5
updated: 1484310349
title: garlic
categories:
    - Dictionary
---
garlic
     n 1: bulbous herb of southern Europe widely naturalized; bulb
          breaks up into separate strong-flavored cloves [syn: {Allium
          sativum}]
     2: aromatic bulb used as seasoning [syn: {ail}]
