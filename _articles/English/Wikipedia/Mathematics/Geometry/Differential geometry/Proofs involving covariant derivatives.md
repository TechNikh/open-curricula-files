---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Proofs_involving_covariant_derivatives
offline_file: ""
offline_thumbnail: ""
uuid: 51e8b7f0-07eb-4ee3-85fc-69a1bca51496
updated: 1484309207
title: Proofs involving covariant derivatives
tags:
    - Contracted Bianchi identities
    - Proof
    - The covariant divergence of the Einstein tensor vanishes
    - Proof
    - Books
categories:
    - Differential geometry
---
