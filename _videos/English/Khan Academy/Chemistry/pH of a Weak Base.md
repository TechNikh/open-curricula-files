---
version: 1
type: video
provider: YouTube
id: gDJtOIxDu78
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/pH%20of%20a%20Weak%20Base.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1824d7ae-189d-45e6-a11a-0fdd53b3b0b6
updated: 1486069589
title: pH of a Weak Base
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/gDJtOIxDu78/default.jpg
    - https://i3.ytimg.com/vi/gDJtOIxDu78/1.jpg
    - https://i3.ytimg.com/vi/gDJtOIxDu78/2.jpg
    - https://i3.ytimg.com/vi/gDJtOIxDu78/3.jpg
---
pH of .2 M of NH3 (weak base).
More free lessons at: http://www.khanacademy.org/video?v=gDJtOIxDu78
