---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hodge_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: a7803322-8e19-47ee-9bd6-708a553f548b
updated: 1484309153
title: Hodge conjecture
tags:
    - Motivation
    - Statement of the Hodge conjecture
    - Reformulation in terms of algebraic cycles
    - Known cases of the Hodge conjecture
    - Low dimension and codimension
    - Hypersurfaces
    - Abelian varieties
    - Generalizations
    - The integral Hodge conjecture
    - The Hodge conjecture for Kähler varieties
    - The generalized Hodge conjecture
    - Algebraicity of Hodge loci
categories:
    - Algebraic geometry
---
In mathematics, the Hodge conjecture is a major unsolved problem in the field of algebraic geometry that relates the algebraic topology of a non-singular complex algebraic variety and the subvarieties of that variety. More specifically, the conjecture says that certain de Rham cohomology classes are algebraic, that is, they are sums of Poincaré duals of the homology classes of subvarieties. It was formulated by the Scottish mathematician William Vallance Douglas Hodge as a result of a work in between 1930 and 1940 to enrich the description of de Rham cohomology to include extra structure that is present in the case of complex algebraic varieties. It received little attention before Hodge ...
