---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unfolded
offline_file: ""
offline_thumbnail: ""
uuid: d54fbce9-dc06-4e3e-975b-39778b39aca3
updated: 1484310525
title: unfolded
categories:
    - Dictionary
---
unfolded
     adj : spread or opened out; "an unfolded newspaper"; "unfolded
           wings" [ant: {folded}]
