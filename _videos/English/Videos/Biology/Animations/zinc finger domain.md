---
version: 1
type: video
provider: YouTube
id: WyU2v7HT6bw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/zinc%20finger%20domain.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d0458939-e98f-41d9-99bd-12ef68625935
updated: 1486069672
title: zinc finger domain
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/WyU2v7HT6bw/default.jpg
    - https://i3.ytimg.com/vi/WyU2v7HT6bw/1.jpg
    - https://i3.ytimg.com/vi/WyU2v7HT6bw/2.jpg
    - https://i3.ytimg.com/vi/WyU2v7HT6bw/3.jpg
---
