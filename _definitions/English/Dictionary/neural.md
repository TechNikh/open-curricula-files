---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neural
offline_file: ""
offline_thumbnail: ""
uuid: aa737c68-a90a-4eb1-abee-181bb3a9f354
updated: 1484310355
title: neural
categories:
    - Dictionary
---
neural
     adj 1: of or relating to the nervous system; "nervous disease";
            "neural disorder" [syn: {nervous}]
     2: of or relating to neurons; "neural network" [syn: {neuronal},
         {neuronic}]
