---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/falling
offline_file: ""
offline_thumbnail: ""
uuid: 2f14249f-112e-4276-8504-590c1aee3bb7
updated: 1484310220
title: falling
categories:
    - Dictionary
---
falling
     adj 1: suddenly losing an upright position; "they ran from the
            falling tree"; "a falling wall crushed the car" [ant:
            {standing}]
     2: decreasing in amount or degree; "falling temperature"
     3: becoming lower or less in degree or value; "a falling
        market"; "falling incomes" [ant: {rising}]
     4: coming down freely under the influence of gravity; "the
        eerie whistle of dropping bombs"; "falling rain" [syn: {dropping}]
