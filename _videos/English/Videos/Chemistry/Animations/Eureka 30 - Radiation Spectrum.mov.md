---
version: 1
type: video
provider: YouTube
id: XrNGWcdZ6GY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Eureka%2030%20-%20Radiation%20Spectrum.mov.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e7a7a00b-d49a-4ad6-a6f8-89b5d8de316f
updated: 1486069665
title: 'Eureka 30 - Radiation Spectrum.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/XrNGWcdZ6GY/default.jpg
    - https://i3.ytimg.com/vi/XrNGWcdZ6GY/1.jpg
    - https://i3.ytimg.com/vi/XrNGWcdZ6GY/2.jpg
    - https://i3.ytimg.com/vi/XrNGWcdZ6GY/3.jpg
---
