---
version: 1
type: video
provider: YouTube
id: VvuuRpJbbHE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Quadratic%20equations/Example%201-%20Completing%20the%20square.webm"
offline_file: ""
offline_thumbnail: ""
uuid: df21a930-5fc8-4db3-b043-727eb1143b7f
updated: 1486069561
title: 'Example 1- Completing the square'
categories:
    - Quadratic equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/VvuuRpJbbHE/default.jpg
    - https://i3.ytimg.com/vi/VvuuRpJbbHE/1.jpg
    - https://i3.ytimg.com/vi/VvuuRpJbbHE/2.jpg
    - https://i3.ytimg.com/vi/VvuuRpJbbHE/3.jpg
---
