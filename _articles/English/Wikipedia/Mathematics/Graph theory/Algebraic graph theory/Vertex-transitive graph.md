---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vertex-transitive_graph
offline_file: ""
offline_thumbnail: ""
uuid: e00c0ef0-7ccc-43e3-ad0a-1fad88d7f0db
updated: 1484309376
title: Vertex-transitive graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tuncated_tetrahedral_graph.png
tags:
    - Finite examples
    - Properties
    - Infinite examples
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory, a vertex-transitive graph is a graph G such that, given any two vertices v1 and v2 of G, there is some automorphism
