---
version: 1
type: video
provider: YouTube
id: 5cK86VKoBPw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Functions%20and%20their%20graphs/Recognize%20functions%20from%20tables.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 93e4e6c6-cc52-494a-923c-5e251df0db40
updated: 1486069561
title: Recognize functions from tables
categories:
    - Functions and their graphs
thumbnail_urls:
    - https://i3.ytimg.com/vi/5cK86VKoBPw/default.jpg
    - https://i3.ytimg.com/vi/5cK86VKoBPw/1.jpg
    - https://i3.ytimg.com/vi/5cK86VKoBPw/2.jpg
    - https://i3.ytimg.com/vi/5cK86VKoBPw/3.jpg
---
