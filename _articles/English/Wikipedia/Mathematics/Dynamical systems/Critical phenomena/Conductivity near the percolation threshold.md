---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Conductivity_near_the_percolation_threshold
offline_file: ""
offline_thumbnail: ""
uuid: 420a6218-e820-40ce-8f9f-dc391e7df58a
updated: 1484308749
title: Conductivity near the percolation threshold
tags:
    - Geometrical percolation
    - Electrical percolation
    - Conductor-insulator mixture
    - Superconductor–conductor mixture
    - Conductivity near the percolation threshold
    - Values for the critical exponents
    - Dielectric constant
    - The R-C model
categories:
    - Critical phenomena
---
In a mixture between a dielectric and a metallic component, the conductivity 
  
    
      
        σ
      
    
    {\displaystyle \sigma }
  
 and the dielectric constant 
  
    
      
        ϵ
      
    
    {\displaystyle \epsilon }
  
 of this mixture show a critical behavior if the fraction of the metallic component reaches the percolation threshold.[1] The behavior of the conductivity near this percolation threshold will show a smooth change over from the conductivity of the dielectric component to the conductivity of the metallic component and can be described using two critical exponents s and t, whereas the dielectric constant will diverge if the threshold is approached ...
