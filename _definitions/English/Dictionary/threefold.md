---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/threefold
offline_file: ""
offline_thumbnail: ""
uuid: 3445dcd4-46b9-40b4-9072-68bb09cae5c2
updated: 1484310571
title: threefold
categories:
    - Dictionary
---
threefold
     adj 1: three times as great or many; "a claim for treble (or
            triple) damages"; "a threefold increase" [syn: {treble},
             {triple}]
     2: having more than one decidedly dissimilar aspects or
        qualities; "a double (or dual) role for an actor"; "the
        office of a clergyman is twofold; public preaching and
        private influence"- R.W.Emerson; "every episode has its
        double and treble meaning"-Frederick Harrison [syn: {double},
         {dual}, {twofold}, {treble}]
     adv : by a factor of three; "our rent increased threefold in the
           past five years" [syn: {three times}]
