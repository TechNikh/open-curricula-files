---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Victor_Bangert
offline_file: ""
offline_thumbnail: ""
uuid: 76950e79-89be-441c-a371-1eca5784f6cc
updated: 1484308684
title: Victor Bangert
categories:
    - Dynamical systems theorists
---
Victor Bangert (born 28 November 1950, Osnabrück) is Professor of Mathematics at the Mathematisches Institut in Freiburg, Germany. His main interests are differential geometry and dynamical systems theory. In particular, he made important contributions to the study of closed geodesics in Riemannian manifolds, and to Aubry–Mather theory.
