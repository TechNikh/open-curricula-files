---
version: 1
type: video
provider: YouTube
id: F5iMsjwFLX8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Simplifying%20Radical%20Expressions%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ab6c13a5-51fb-4891-aa07-9c6e052a54db
updated: 1486069563
title: Simplifying Radical Expressions 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/F5iMsjwFLX8/default.jpg
    - https://i3.ytimg.com/vi/F5iMsjwFLX8/1.jpg
    - https://i3.ytimg.com/vi/F5iMsjwFLX8/2.jpg
    - https://i3.ytimg.com/vi/F5iMsjwFLX8/3.jpg
---
