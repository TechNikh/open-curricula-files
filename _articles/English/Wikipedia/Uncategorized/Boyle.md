---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Boyle
offline_file: ""
offline_thumbnail: ""
uuid: 0dd72f15-fb55-44b6-bd4f-96e83db4d2f4
updated: 1484308307
title: Boyle
tags:
    - Disambiguation
    - Arts and media
    - Military
    - Politics, government, and law
    - religion
    - Sciences, math, and medicine
    - Sports
    - Writers
    - Other
    - Members of Boyle family headed by the Earl of Cork
    - Members of Clan Boyle headed by the Earl of Glasgow
categories:
    - Uncategorized
---
Boyle is an Irish and Scottish surname of Gaelic or Norman origin. In the northwest of Ireland it is one of the most common family names. Notable people with the surname include:
