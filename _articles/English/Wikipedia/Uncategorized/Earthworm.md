---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Earth_worm
offline_file: ""
offline_thumbnail: ""
uuid: b5c9ca74-2412-4670-aaff-c592fc0f0a60
updated: 1484308368
title: Earthworm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/450px-Earthworm_head.svg.png
tags:
    - Anatomy
    - Form and function
    - Nervous system
    - Central nervous system
    - Peripheral nervous system
    - Sympathetic nervous system
    - Movement
    - Senses
    - Photosensitivity
    - Digestive system
    - Circulatory system
    - Excretory system
    - Respiration
    - Reproduction
    - Regeneration
    - Locomotion and importance to soil
    - Benefits
    - As an invasive species
    - Special habitats
    - Ecology
    - Economic impact
    - Taxonomy and distribution
    - Referenced works
    - General
    - Academic
    - Agriculture and ecology
    - Worm farming
    - For children
categories:
    - Uncategorized
---
An earthworm is a tube-shaped, segmented worm found in the phylum Annelida. Earthworms are commonly found living in soil, feeding on live and dead organic matter. An earthworm's digestive system runs through the length of its body. It conducts respiration through its skin. It has a double transport system composed of coelomic fluid that moves within the fluid-filled coelom and a simple, closed blood circulatory system. It has a central and a peripheral nervous system. The central nervous system consists of two ganglia above the mouth, one on either side, connected to a nerve cord running back along its length to motor neurons and sensory cells in each segment. Large numbers of ...
