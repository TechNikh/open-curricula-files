---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ploughing
offline_file: ""
offline_thumbnail: ""
uuid: 67a23ef3-7607-4efa-9790-fd75c3beb4d9
updated: 1484310521
title: ploughing
categories:
    - Dictionary
---
ploughing
     n : tilling the land with a plow; "he hired someone to do the
         plowing for him" [syn: {plowing}]
