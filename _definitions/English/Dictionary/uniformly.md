---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uniformly
offline_file: ""
offline_thumbnail: ""
uuid: 3095318e-85b0-4f0b-81b5-6ffcf7b71fa1
updated: 1484310327
title: uniformly
categories:
    - Dictionary
---
uniformly
     adv : in a uniform manner; "a uniformly bright surface"
