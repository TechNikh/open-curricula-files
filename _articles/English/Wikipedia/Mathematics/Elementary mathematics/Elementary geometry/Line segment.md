---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Line_segment
offline_file: ""
offline_thumbnail: ""
uuid: 779322ad-e844-4b15-b8ec-a3a0242cdc3d
updated: 1484308932
title: Line segment
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Segment_definition.svg.png
tags:
    - In real or complex vector spaces
    - Properties
    - In proofs
    - As a degenerate ellipse
    - In other geometric shapes
    - Triangles
    - Quadrilaterals
    - Circles and ellipses
    - Directed line segment
    - Generalizations
categories:
    - Elementary geometry
---
In geometry, a line segment is a part of a line that is bounded by two distinct end points, and contains every point on the line between its endpoints. A closed line segment includes both endpoints, while an open line segment excludes both endpoints; a half-open line segment includes exactly one of the endpoints.
