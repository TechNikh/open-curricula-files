---
version: 1
type: video
provider: YouTube
id: 0IDgBlCHVsA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Sympathetic%20Nervous%20System-%20Crash%20Course%20A%26P%20%2314.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6c21a033-1c79-47a0-a29f-d5aad0cea9cb
updated: 1486069681
title: 'Sympathetic Nervous System- Crash Course A&P #14'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/0IDgBlCHVsA/default.jpg
    - https://i3.ytimg.com/vi/0IDgBlCHVsA/1.jpg
    - https://i3.ytimg.com/vi/0IDgBlCHVsA/2.jpg
    - https://i3.ytimg.com/vi/0IDgBlCHVsA/3.jpg
---
