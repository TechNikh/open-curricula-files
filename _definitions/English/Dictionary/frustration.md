---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frustration
offline_file: ""
offline_thumbnail: ""
uuid: ca962526-0511-43fc-bb2c-3c29419886ab
updated: 1484310607
title: frustration
categories:
    - Dictionary
---
frustration
     n 1: the feeling that accompanies an experience of being thwarted
          in attaining your goals [syn: {defeat}]
     2: an act of hindering someone's plans or efforts [syn: {thwarting},
         {foiling}]
     3: a feeling of annoyance at being hindered or criticized; "her
        constant complaints were the main source of his
        frustration"
