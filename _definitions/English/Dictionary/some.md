---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/some
offline_file: ""
offline_thumbnail: ""
uuid: 3bd19b33-c26d-4321-97bd-fc37172d25bc
updated: 1484310351
title: some
categories:
    - Dictionary
---
some
     adj 1: quantifier; used with either mass nouns or plural count
            nouns to indicate an unspecified number or quantity;
            "have some milk"; "some roses were still blooming";
            "having some friends over"; "some apples"; "some
            paper" [syn: {some(a)}] [ant: {no(a)}, {all(a)}]
     2: unknown or unspecified; "some lunatic drove into my car";
        "some man telephoned while you were out"; "some day my
        prince will come"; "some enchanted evening" [syn: {some(a)}]
     3: relatively many but unspecified in number; "they were here
        for some weeks"; "we did not meet again for some years"
        [syn: {some(a)}]
     4: remarkable; ...
