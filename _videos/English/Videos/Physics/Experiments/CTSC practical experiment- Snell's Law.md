---
version: 1
type: video
provider: YouTube
id: itt9iyUC1Cc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 618d6119-8d45-4480-9bf7-3f24356b3179
updated: 1486069639
title: "CTSC practical experiment- Snell's Law"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/itt9iyUC1Cc/default.jpg
    - https://i3.ytimg.com/vi/itt9iyUC1Cc/1.jpg
    - https://i3.ytimg.com/vi/itt9iyUC1Cc/2.jpg
    - https://i3.ytimg.com/vi/itt9iyUC1Cc/3.jpg
---
