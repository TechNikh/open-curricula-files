---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jarzynski_equality
offline_file: ""
offline_thumbnail: ""
uuid: 3037a9b4-aea2-4ad7-8f4c-34a55501659f
updated: 1484308802
title: Jarzynski equality
tags:
    - History
    - Bibliography
categories:
    - Non-equilibrium thermodynamics
---
The Jarzynski equality (JE) is an equation in statistical mechanics that relates free energy differences between two states and the irreversible work along an ensemble of trajectories joining the same states. It is named after the physicist Christopher Jarzynski (then at the University of Washington, currently at the University of Maryland) who derived it in 1997.[1][2]
