---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Terai
offline_file: ""
offline_thumbnail: ""
uuid: 9051ed06-ded9-4a5f-9a2a-70e702591a31
updated: 1484308419
title: Terai
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Terai_nepal.jpg
tags:
    - Etymology
    - Geology
    - Climate
    - Terai in Nepal
    - Inner Terai
    - Outer Terai
    - History
    - Ethnic groups
    - Economy
    - Transport
    - Tourism
    - Agriculture
    - Terai in India
categories:
    - Uncategorized
---
The Terai ( Nepali: तराई Hindi: तराइ ) is a plain region of Nepal and the plain land region in Bangladesh, Bhutan and India that lies in south of the outer foothills of the Himalaya, the Siwalik Hills, and north of the Indo-Gangetic Plain of the Ganges, Brahmaputra and their tributaries. This lowland belt is characterised by tall grasslands, scrub savannah, sal forests and clay rich swamps. In northern India, the Terai spreads eastward from the Yamuna River across Himachal Pradesh, Haryana, Uttarakhand, Uttar Pradesh and Bihar. The Terai is part the Terai-Duar savanna and grasslands ecoregion. Corresponding parts in West Bengal, Bangladesh, Bhutan and Assam east to the ...
