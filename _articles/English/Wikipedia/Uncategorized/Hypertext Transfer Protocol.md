---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Http
offline_file: ""
offline_thumbnail: ""
uuid: 44476071-92d0-42a8-b65c-cbf1494b2f19
updated: 1484308438
title: Hypertext Transfer Protocol
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Internet1.jpg
tags:
    - Technical overview
    - History
    - HTTP session
    - HTTP Authentication
    - Authentication Realms
    - Request methods
    - Safe methods
    - Idempotent methods and web applications
    - Security
    - Summary table
    - Status codes
    - Persistent connections
    - HTTP session state
    - Encrypted connections
    - Message Format
    - Request message
    - Response message
    - Example session
    - Client request
    - Server response
    - Similar protocols
    - Notes
categories:
    - Uncategorized
---
The Hypertext Transfer Protocol (HTTP) is an application protocol for distributed, collaborative, hypermedia information systems.[1] HTTP is the foundation of data communication for the World Wide Web.
