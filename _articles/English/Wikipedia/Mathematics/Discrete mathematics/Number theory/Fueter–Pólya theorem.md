---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fueter%E2%80%93P%C3%B3lya_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 36c205be-6853-461c-8cd4-7e731e957a7e
updated: 1484308639
title: Fueter–Pólya theorem
tags:
    - Introduction
    - Statement
    - Proof
    - Fueter–Pólya conjecture
    - Higher dimensions
categories:
    - Number theory
---
The Fueter–Pólya theorem, first proved by Rudolf Fueter and George Pólya, states that the only quadratic pairing functions are the Cantor polynomials.
