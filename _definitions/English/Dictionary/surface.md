---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surface
offline_file: ""
offline_thumbnail: ""
uuid: f3bb6f8b-b4ec-4b16-bb09-f2b45129b196
updated: 1484310341
title: surface
categories:
    - Dictionary
---
surface
     adj 1: on the surface; "surface materials of the moon" [ant: {subsurface},
             {overhead}]
     2: involving a surface only; "her beauty is only skin-deep";
        "superficial bruising"; "a surface wound" [syn: {skin-deep},
         {superficial}, {surface(a)}]
     n 1: the outer boundary of an artifact or a material layer
          constituting or resembling such a boundary; "there is a
          special cleaner for these surfaces"; "the cloth had a
          pattern of red dots on a white surface"
     2: the extended two-dimensional outer boundary of a
        three-dimensional object; "they skimmed over the surface
        of the water"; "a brush small enough to ...
