---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/camp
offline_file: ""
offline_thumbnail: ""
uuid: ff36a613-91e4-4e02-8455-858e1a049144
updated: 1484310206
title: camp
categories:
    - Dictionary
---
camp
     adj : providing sophisticated amusement by virtue of having
           artificially (and vulgarly) mannered or banal or
           sentimental qualities; "they played up the silliness of
           their roles for camp effect"; "campy Hollywood musicals
           of the 1940's" [syn: {campy}]
     n 1: temporary living quarters specially built by the army for
          soldiers; "wherever he went in the camp the men were
          grumbling" [syn: {encampment}, {cantonment}, {bivouac}]
     2: a group of people living together in a camp; "the whole camp
        laughed at his mistake"
     3: temporary lodgings in the country for travelers or
        vacationers; "level ground is ...
