---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meter
offline_file: ""
offline_thumbnail: ""
uuid: a2602886-2e59-4a78-a8a1-43573a28eba4
updated: 1484310216
title: meter
categories:
    - Dictionary
---
meter
     n 1: the basic unit of length adopted under the Systeme
          International d'Unites (approximately 1.094 yards) [syn:
           {metre}, {m}]
     2: any of various measuring instruments for measuring a
        quantity
     3: (prosody) the accent in a metrical foot of verse [syn: {metre},
         {measure}, {beat}, {cadence}]
     4: rhythm as given by division into parts of equal time [syn: {metre},
         {time}]
     v 1: measure with a meter; "meter the flow of water"
     2: stamp with a meter indicating the postage; "meter the mail"
