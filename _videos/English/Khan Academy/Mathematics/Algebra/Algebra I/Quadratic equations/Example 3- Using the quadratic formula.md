---
version: 1
type: video
provider: YouTube
id: XUvKjC21fYU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Quadratic%20equations/Example%203-%20Using%20the%20quadratic%20formula.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 97ac1360-03b6-467e-90fd-16ea0044e45a
updated: 1486069567
title: 'Example 3- Using the quadratic formula'
categories:
    - Quadratic equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/XUvKjC21fYU/default.jpg
    - https://i3.ytimg.com/vi/XUvKjC21fYU/1.jpg
    - https://i3.ytimg.com/vi/XUvKjC21fYU/2.jpg
    - https://i3.ytimg.com/vi/XUvKjC21fYU/3.jpg
---
