---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/custom
offline_file: ""
offline_thumbnail: ""
uuid: 1818f9ac-658c-4191-9105-6adb6d9105fb
updated: 1484310148
title: custom
categories:
    - Dictionary
---
custom
     adj : made according to the specifications of an individual [syn:
           {custom-made}, {customized}, {customised}] [ant: {ready-made}]
     n 1: accepted or habitual practice [syn: {usage}, {usance}]
     2: a specific practice of long standing [syn: {tradition}]
     3: money collected under a tariff [syn: {customs}, {customs
        duty}, {impost}]
     4: habitual patronage; "I have given this tailor my custom for
        many years"
