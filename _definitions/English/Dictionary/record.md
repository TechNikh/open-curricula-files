---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/record
offline_file: ""
offline_thumbnail: ""
uuid: 97d8a59c-a69b-4e8c-80b7-bf0e6b4d1dae
updated: 1484310346
title: record
categories:
    - Dictionary
---
record
     n 1: anything (such as a document or a phonograph record or a
          photograph) providing permanent evidence of or
          information about past events; "the film provided a
          valuable record of stage techniques"
     2: the number of wins versus losses and ties a team has had;
        "at 9-0 they have the best record in their league"
     3: an extreme attainment; the best (or worst) performance ever
        attested (as in a sport); "he tied the Olympic record";
        "coffee production last year broke all previous records";
        "Chicago set the homicide record"
     4: sound recording consisting of a disc with continuous
        grooves; formerly used to ...
