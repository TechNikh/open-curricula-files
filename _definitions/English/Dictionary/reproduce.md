---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reproduce
offline_file: ""
offline_thumbnail: ""
uuid: afa32349-bb94-47d8-b185-427f0b8d4212
updated: 1484310290
title: reproduce
categories:
    - Dictionary
---
reproduce
     v 1: make a copy or equivalent of; "reproduce the painting"
     2: have offspring or young; "The deer in our neighborhood
        reproduce madly"; "The Catholic Church tells people to
        procreate, no matter what their economic situation may be"
        [syn: {procreate}, {multiply}]
     3: recreate an idea, mood, atmosphere, etc. as by artistic
        means; "He reproduced the feeling of sadness in the
        portrait"
     4: repeat after memorization; "For the exam, you must be able
        to regurgitate the information" [syn: {regurgitate}]
