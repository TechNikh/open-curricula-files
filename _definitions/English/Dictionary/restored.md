---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/restored
offline_file: ""
offline_thumbnail: ""
uuid: 6b9f4fa6-90c8-41e1-9698-6c5ec1127904
updated: 1484310377
title: restored
categories:
    - Dictionary
---
restored
     adj : brought back to original condition; "a restored painting";
           "felt a restored faith in human beings" [ant: {unrestored}]
