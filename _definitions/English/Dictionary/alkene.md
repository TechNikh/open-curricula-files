---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alkene
offline_file: ""
offline_thumbnail: ""
uuid: f4b9a35d-cc52-4b45-8ff8-af5767af0052
updated: 1484310429
title: alkene
categories:
    - Dictionary
---
alkene
     n : any unsaturated aliphatic hydrocarbon [syn: {olefine}, {olefin}]
