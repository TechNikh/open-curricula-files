---
version: 1
type: video
provider: YouTube
id: VhokQhjl5t0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Introduction%20to%20functions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6a454dcb-0bb9-4dfd-8a07-8f0cc6a385f6
updated: 1486069573
title: Introduction to functions
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/VhokQhjl5t0/default.jpg
    - https://i3.ytimg.com/vi/VhokQhjl5t0/1.jpg
    - https://i3.ytimg.com/vi/VhokQhjl5t0/2.jpg
    - https://i3.ytimg.com/vi/VhokQhjl5t0/3.jpg
---
An introduction to functions.
More free lessons at: http://www.khanacademy.org/video?v=VhokQhjl5t0
