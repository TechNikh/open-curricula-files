---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rope
offline_file: ""
offline_thumbnail: ""
uuid: f0e75b7d-d836-4cbd-99d8-b3292ca0687a
updated: 1484310148
title: rope
categories:
    - Dictionary
---
rope
     n 1: a strong line
     2: street names for flunitrazepan [syn: {R-2}, {Mexican valium},
         {rophy}, {roofy}, {roach}, {forget me drug}, {circle}]
     v 1: catch with a lasso; "rope cows" [syn: {lasso}]
     2: fasten with a rope; "rope the bag securely" [syn: {leash}]
