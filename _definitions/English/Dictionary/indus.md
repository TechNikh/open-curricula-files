---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indus
offline_file: ""
offline_thumbnail: ""
uuid: beea2835-60a2-4410-95eb-80a438d8c166
updated: 1484310433
title: indus
categories:
    - Dictionary
---
Indus
     n 1: a faint constellation in the southern hemisphere near
          Telescopium and Tucana
     2: an Asian river; flows into the Arabian Sea [syn: {Indus
        River}]
