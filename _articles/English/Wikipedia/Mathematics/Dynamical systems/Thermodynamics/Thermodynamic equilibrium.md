---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermodynamic_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 2d241eaa-e2cf-463f-9fa1-832bb72ce650
updated: 1484308868
title: Thermodynamic equilibrium
tags:
    - Overview
    - Conditions for thermodynamic equilibrium
    - Relation of exchange equilibrium between systems
    - Thermodynamic state of internal equilibrium of a system
    - Multiple contact equilibrium
    - Local and global equilibrium
    - Reservations
    - Definitions
    - >
        Characteristics of a state of internal thermodynamic
        equilibrium
    - Homogeneity in the absence of external forces
    - Uniform temperature
    - Number of real variables needed for specification
    - Stability against small perturbations
    - >
        Approach to thermodynamic equilibrium within an isolated
        system
    - >
        Fluctuations within an isolated system in its own internal
        thermodynamic equilibrium
    - Thermal equilibrium
    - Non-equilibrium
    - General references
    - Cited bibliography
categories:
    - Thermodynamics
---
Thermodynamic equilibrium is an axiomatic concept of thermodynamics. It is an internal state of a single thermodynamic system, or a relation between several thermodynamic systems connected by more or less permeable or impermeable walls. In thermodynamic equilibrium there are no net macroscopic flows of matter or of energy, either within a system or between systems. In a system in its own state of internal thermodynamic equilibrium, no macroscopic change occurs. Systems in mutual thermodynamic equilibrium are simultaneously in mutual thermal, mechanical, chemical, and radiative equilibria. Systems can be in one kind of mutual equilibrium, though not in others. In thermodynamic equilibrium, ...
