---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Embryo_drawing
offline_file: ""
offline_thumbnail: ""
uuid: dceb2d3f-51b3-4aae-b385-c710456be291
updated: 1484309324
title: Embryo drawing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Gray41.png
tags:
    - >
        Use of embryo drawings and photography in contemporary
        biology
    - Controversy
    - Famous embryo illustrators
    - Ernst Haeckel (1834-1919)
    - Karl Ernst von Baer (1792-1876)
    - Wilhelm His (1831-1904)
    - Opposition to Haeckel
    - 'Early opponents: Ludwig Rutimeyer, Theodor Bischoff and Rudolf Virchow'
    - 'Contemporary criticism of Haeckel: Michael Richardson and Stephen Jay Gould'
    - "Haeckel's proponents (past and present)"
    - "The survival and reproduction of Haeckel's embryo drawings"
    - Notes
categories:
    - Technical drawing
---
Embryo drawing is the illustration of embryos in their developmental sequence. In plants and animals, an embryo develops from a zygote, the single cell that results when an egg and sperm fuse during fertilization. In animals, the zygote divides repeatedly to form a ball of cells, which then forms a set of tissue layers that migrate and fold to form an early embryo. Images of embryos provide a means of comparing embryos of different ages, and species. To this day, embryo drawings are made in undergraduate developmental biology lessons.
