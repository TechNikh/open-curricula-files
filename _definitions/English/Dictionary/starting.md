---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/starting
offline_file: ""
offline_thumbnail: ""
uuid: 5c74eb8f-341a-4c44-8582-0302fd9e96ac
updated: 1484310297
title: starting
categories:
    - Dictionary
---
starting
     adj 1: (especially of eyes) bulging or protruding as with fear;
            "with eyes starting from their sockets"
     2: appropriate to the beginning or start of an event; "the
        starting point"; "hands in the starting position"
     n : a turn to be a starter (in a game at the beginning); "he got
         his start because one of the regular pitchers was in the
         hospital"; "his starting meant that the coach thought he
         was one of their best linemen" [syn: {start}]
