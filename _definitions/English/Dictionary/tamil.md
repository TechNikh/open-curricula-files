---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tamil
offline_file: ""
offline_thumbnail: ""
uuid: 4ee27867-4b88-4999-aa3a-6cf09be9897a
updated: 1484310439
title: tamil
categories:
    - Dictionary
---
Tamil
     adj : of or relating to a speaker of the Tamil language or the
           language itself; "the Tamil Tigers are fighting the
           Sinhalese in Sri Lanka"; "Tamil agglutinative phrases"
     n 1: a member of the mixed Dravidian and Caucasoid people of
          southern India and Sri Lanka
     2: the Dravidian language spoken since prehistoric times by the
        Tamil people in southern India and Sri Lanka
