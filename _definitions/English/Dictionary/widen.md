---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/widen
offline_file: ""
offline_thumbnail: ""
uuid: a619bfc2-ba3f-4126-b2fb-b8d80c3ca9ba
updated: 1484310333
title: widen
categories:
    - Dictionary
---
widen
     v 1: become broader or wider or more extensive; "The road
          widened" [ant: {narrow}]
     2: make (clothes) larger; "Let out that dress--I gained a lot
        of weight" [syn: {let out}] [ant: {take in}]
     3: make wider; "widen the road"
     4: extend in scope or range or area; "The law was extended to
        all citizens"; "widen the range of applications"; "broaden
        your horizon"; "Extend your backyard" [syn: {broaden}, {extend}]
