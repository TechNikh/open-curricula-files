---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anatoly_Mikhailovich_Stepin
offline_file: ""
offline_thumbnail: ""
uuid: df06c70b-c89e-4dfd-868c-b38e33de48f2
updated: 1484308684
title: Anatoly Mikhailovich Stepin
tags:
    - Education and career
    - Awards
categories:
    - Dynamical systems theorists
---
Anatoly Mikhailovich Stepin (Анатолий Михайлович Степин, born 20 July 1940 in Moscow) is a Soviet-Russian mathematician, specializing in dynamical systems and ergodic theory.
