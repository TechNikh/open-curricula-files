---
version: 1
type: video
provider: YouTube
id: 9KdxaqH-6GE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 18e8a609-66bf-4d67-b19e-a82efcf378f4
updated: 1486069611
title: Breaking apart two digit addition problems
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/9KdxaqH-6GE/default.jpg
    - https://i3.ytimg.com/vi/9KdxaqH-6GE/1.jpg
    - https://i3.ytimg.com/vi/9KdxaqH-6GE/2.jpg
    - https://i3.ytimg.com/vi/9KdxaqH-6GE/3.jpg
---
