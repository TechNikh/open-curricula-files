---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Humphry
offline_file: ""
offline_thumbnail: ""
uuid: 2536eb3f-cca1-413d-a69c-f5afcd916b42
updated: 1484308384
title: Humphry
tags:
    - people
    - First name
    - Surname
    - Fiction
categories:
    - Uncategorized
---
Humphry is a masculine given name and surname. It comes from the Old Germanic name Hunfrid, which means "friend of the hun". The name may refer to:
