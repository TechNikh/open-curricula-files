---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rejoice
offline_file: ""
offline_thumbnail: ""
uuid: 99842c88-0ec2-4992-a3c9-c6f8c22458d3
updated: 1484310561
title: rejoice
categories:
    - Dictionary
---
rejoice
     v 1: feel happiness or joy [syn: {joy}]
     2: to express great joy; "Who cannot exult in Spring?" [syn: {exult},
         {triumph}, {jubilate}]
     3: be ecstatic with joy [syn: {wallow}, {triumph}]
