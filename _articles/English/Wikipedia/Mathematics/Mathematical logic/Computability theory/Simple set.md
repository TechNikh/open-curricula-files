---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Simple_set
offline_file: ""
offline_thumbnail: ""
uuid: 97cd3738-4bf3-4790-a1bc-6d7673b43ecf
updated: 1484309490
title: Simple set
tags:
    - "Relation to Post's problem"
    - Formal definitions and some properties
    - Notes
categories:
    - Computability theory
---
In recursion theory a subset of the natural numbers is called a simple set if it is co-infinite and recursively enumerable, but every infinite subset of its complement fails to be enumerated recursively. Simple sets are examples of recursively enumerable sets that are not recursive.
