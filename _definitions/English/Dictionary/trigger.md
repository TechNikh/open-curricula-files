---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trigger
offline_file: ""
offline_thumbnail: ""
uuid: a4bbb152-95b5-430b-b1d5-d33186eb97a3
updated: 1484310351
title: trigger
categories:
    - Dictionary
---
trigger
     n 1: lever that activates the firing mechanism of a gun [syn: {gun
          trigger}]
     2: a device that activates or releases or causes something to
        happen
     3: an act that sets in motion some course of events [syn: {induction},
         {initiation}]
     v 1: put in motion or move to act; "trigger a reaction"; "actuate
          the circuits" [syn: {trip}, {actuate}, {activate}, {set
          off}, {spark off}, {spark}, {trigger off}, {touch off}]
     2: release or pull the trigger on; "Trigger a gun"
