---
version: 1
type: video
provider: YouTube
id: sPTuCE5zd3s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/AP%20Calculus%20BC%20Exams-%202008%201%20d.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9e67dbe9-f6be-4a08-b3ec-09955b7e3fbb
updated: 1486069690
title: 'AP Calculus BC Exams- 2008 1 d'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/sPTuCE5zd3s/default.jpg
    - https://i3.ytimg.com/vi/sPTuCE5zd3s/1.jpg
    - https://i3.ytimg.com/vi/sPTuCE5zd3s/2.jpg
    - https://i3.ytimg.com/vi/sPTuCE5zd3s/3.jpg
---
