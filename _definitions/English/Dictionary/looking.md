---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/looking
offline_file: ""
offline_thumbnail: ""
uuid: 289322d4-9488-4b87-b825-b05f931b2f61
updated: 1484310343
title: looking
categories:
    - Dictionary
---
looking
     adj : appearing to be as specified; usually used as combining
           forms; "left their clothes dirty looking"; "a most
           disagreeable looking character"; "angry-looking";
           "liquid-looking"; "severe-looking policemen on noble
           horses"; "fine-sounding phrases"; "taken in by
           high-sounding talk" [syn: {sounding}]
     n 1: the act of directing the eyes toward something and
          perceiving it visually; "he went out to have a look";
          "his look was fixed on her eyes"; "he gave it a good
          looking at"; "his camera does his looking for him" [syn:
           {look}, {looking at}]
     2: the act of searching visually ...
