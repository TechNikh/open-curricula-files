---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liquor
offline_file: ""
offline_thumbnail: ""
uuid: 9faaeb0e-b142-4f2a-87f0-ffceade6f601
updated: 1484310464
title: liquor
categories:
    - Dictionary
---
liquor
     n 1: distilled rather than fermented [syn: {spirits}, {booze}, {hard
          drink}, {hard liquor}, {John Barleycorn}, {strong drink}]
     2: a liquid substance that is a solution (or emulsion or
        suspension) used or obtained in an industrial process;
        "waste liquors"
     3: the liquid in which vegetables or meat have be cooked [syn:
        {pot liquor}, {pot likker}]
