---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flooded
offline_file: ""
offline_thumbnail: ""
uuid: ad1288a2-0579-496f-94fd-d4b2a2cb47ca
updated: 1484310541
title: flooded
categories:
    - Dictionary
---
flooded
     adj 1: covered with water; "the main deck was afloat (or awash)";
            "the monsoon left the whole place awash"; "a flooded
            bathroom"; "inundated farmlands"; "an overflowing tub"
            [syn: {afloat(p)}, {awash(p)}, {inundated}, {overflowing}]
     2: rendered powerless especially by an excessive amount or
        profusion of something; "a desk flooded with
        applications"; "felt inundated with work"; "too much
        overcome to notice"; "a man engulfed by fear"; "swamped by
        work" [syn: {inundated}, {overcome}, {overpowered}, {overwhelmed},
         {swamped}, {engulfed}]
