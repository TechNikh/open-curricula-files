---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/learned
offline_file: ""
offline_thumbnail: ""
uuid: f8891afc-2b61-4b6c-b382-6241050c592b
updated: 1484310204
title: learned
categories:
    - Dictionary
---
learned
     adj 1: having or showing profound knowledge; "a learned jurist";
            "an erudite professor" [syn: {erudite}]
     2: highly educated; having extensive information or
        understanding; "an enlightened public"; "knowing
        instructors"; "a knowledgeable critic"; "a knowledgeable
        audience" [syn: {enlightened}, {knowing}, {knowledgeable},
         {lettered}, {well-educated}, {well-read}]
     3: established by conditioning or learning; "a conditioned
        response" [syn: {conditioned}] [ant: {unconditioned}]
     4: acquired by learning; "learned skills"
