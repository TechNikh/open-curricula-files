---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hausdorff_dimension
offline_file: ""
offline_thumbnail: ""
uuid: 9a345794-9050-417c-8a94-85a1ae3f67d5
updated: 1484309268
title: Hausdorff dimension
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-KochFlake.svg.png
tags:
    - Intuition
    - Formal definitions
    - Hausdorff content
    - Hausdorff dimension
    - Examples
    - Properties of Hausdorff dimension
    - Hausdorff dimension and inductive dimension
    - Hausdorff dimension and Minkowski dimension
    - Hausdorff dimensions and Frostman measures
    - Behaviour under unions and products
    - The Hausdorff Dimension Theorem
    - Self-similar sets
    - The open set condition
categories:
    - Metric geometry
---
Hausdorff dimension is a concept in mathematics introduced in 1918 by mathematician Felix Hausdorff, and it serves as a measure of the local size of a set of numbers (i.e., a "space"), taking into account the distance between each of its members (i.e., the "points" in the "space"). Applying its mathematical formalisms provides that the Hausdorff dimension of a single point is zero, of a line is 1, and of a square is 2, of a cube is 3. That is, for sets of points that define a smooth shape or a shape that has a small number of corners—the shapes of traditional geometry and science—the Hausdorff dimension is a counting number (integer) agreeing with a dimension corresponding to its ...
