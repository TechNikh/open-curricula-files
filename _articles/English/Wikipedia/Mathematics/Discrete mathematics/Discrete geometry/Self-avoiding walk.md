---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-avoiding_walk
offline_file: ""
offline_thumbnail: ""
uuid: c605f579-1447-47e0-ac54-259f60039532
updated: 1484308612
title: Self-avoiding walk
tags:
    - Universality
    - Self-avoiding walks on networks
    - Limits
categories:
    - Discrete geometry
---
In mathematics, a self-avoiding walk (SAW) is a sequence of moves on a lattice (a lattice path) that does not visit the same point more than once. This is a special case of the graph theoretical notion of a path. A self-avoiding polygon (SAP) is a closed self-avoiding walk on a lattice. SAWs were first introduced by the chemist Paul Flory[1][dubious – discuss] in order to model the real-life behavior of chain-like entities such as solvents and polymers, whose physical volume prohibits multiple occupation of the same spatial point. Very little is known rigorously about the self-avoiding walk from a mathematical perspective, although physicists have provided numerous conjectures that are ...
