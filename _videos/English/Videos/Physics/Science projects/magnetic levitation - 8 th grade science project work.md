---
version: 1
type: video
provider: YouTube
id: G7qLXxujqnQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8fe2659e-5844-42a3-8f5c-58b1c27fb6fd
updated: 1486069634
title: 'magnetic levitation - 8 th grade science project work'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/G7qLXxujqnQ/default.jpg
    - https://i3.ytimg.com/vi/G7qLXxujqnQ/1.jpg
    - https://i3.ytimg.com/vi/G7qLXxujqnQ/2.jpg
    - https://i3.ytimg.com/vi/G7qLXxujqnQ/3.jpg
---
