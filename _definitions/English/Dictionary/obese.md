---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obese
offline_file: ""
offline_thumbnail: ""
uuid: b8dfdf49-02b7-4505-9517-555c099caf51
updated: 1484310537
title: obese
categories:
    - Dictionary
---
obese
     adj : excessively fat; "a weighty man" [syn: {corpulent}, {weighty},
            {rotund}]
