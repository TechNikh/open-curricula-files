---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/controversy
offline_file: ""
offline_thumbnail: ""
uuid: e0b07240-64ff-4bc3-8adf-1ac192a7258c
updated: 1484310441
title: controversy
categories:
    - Dictionary
---
controversy
     n : a contentious speech act; a dispute where there is strong
         disagreement; "they were involved in a violent argument"
         [syn: {contention}, {contestation}, {disputation}, {disceptation},
          {tilt}, {argument}, {arguing}]
