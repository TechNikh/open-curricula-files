---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wreath
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484592541
title: wreath
categories:
    - Dictionary
---
wreath
     n : flower arrangement consisting of a circular band of foliage
         or flowers for ornamental purposes [syn: {garland}, {coronal},
          {chaplet}, {lei}]
     v : encircle with or as if with a wreath; "Her face was wreathed
         with blossoms" [syn: {wreathe}]
