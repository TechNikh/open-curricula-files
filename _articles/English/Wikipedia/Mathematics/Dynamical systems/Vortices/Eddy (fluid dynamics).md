---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eddy_(fluid_dynamics)
offline_file: ""
offline_thumbnail: ""
uuid: 2623efa4-a0bd-49b6-b073-dd5a376b2b9e
updated: 1484308874
title: Eddy (fluid dynamics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Canary_A2002186_1155_250m.jpg
tags:
    - Mesoscale ocean eddies
    - Swirl and eddies in engineering
categories:
    - Vortices
---
In fluid dynamics, an eddy is the swirling of a fluid and the reverse current created when the fluid flows past an obstacle. The moving fluid creates a space devoid of downstream-flowing fluid on the downstream side of the object. Fluid behind the obstacle flows into the void creating a swirl of fluid on each edge of the obstacle, followed by a short reverse flow of fluid behind the obstacle flowing upstream, toward the back of the obstacle. This phenomenon is most visible behind large emergent rocks in swift-flowing rivers.
