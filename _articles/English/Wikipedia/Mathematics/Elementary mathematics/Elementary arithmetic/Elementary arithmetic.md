---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Elementary_arithmetic
offline_file: ""
offline_thumbnail: ""
uuid: 4b1e40ef-db50-4c65-9694-e93c45da7009
updated: 1484308918
title: Elementary arithmetic
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Arithmetic_symbols.svg.png
tags:
    - The digits
    - Addition
    - What does it mean to add two natural numbers?
    - Addition algorithm
    - Example
    - Successorship and size
    - Counting
    - Subtraction
    - Multiplication
    - What does it mean to multiply two natural numbers?
    - Multiplication algorithm for a single-digit factor
    - Example
    - Multiplication algorithm for multi-digit factors
    - Example
    - Division
    - Division notation
    - Educational standards
    - Tools
categories:
    - Elementary arithmetic
---
Elementary arithmetic is the simplified portion of arithmetic that includes the operations of addition, subtraction, multiplication, and division. It should not be confused with elementary function arithmetic.
