---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Center_of_curvature
offline_file: ""
offline_thumbnail: ""
uuid: 7d753137-7702-4867-be23-ec8124a16e42
updated: 1484309207
title: Center of curvature
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Concave_mirror_qwertyxp2000.png
categories:
    - Differential geometry
---
In geometry, the center of curvature of a curve is found at a point that is at a distance from the curve equal to the radius of curvature lying on the normal vector. It is the point at infinity if the curvature is zero. The osculating circle to the curve is centered at the centre of curvature. Cauchy defined the center of curvature C as the intersection point of two infinitely close normal lines to the curve.[1] The locus of centers of curvature for each point on the curve comprise the evolute of the curve.
