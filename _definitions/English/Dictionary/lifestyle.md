---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lifestyle
offline_file: ""
offline_thumbnail: ""
uuid: e741d2f1-a283-4d88-bb1c-40ea1667990a
updated: 1484310480
title: lifestyle
categories:
    - Dictionary
---
life style
     n : a manner of living that reflects the person's values and
         attitudes [syn: {life-style}, {lifestyle}, {modus vivendi}]
