---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hartman%E2%80%93Grobman_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: a02beefb-6106-47a5-921c-29e34c049809
updated: 1484308696
title: Hartman–Grobman theorem
tags:
    - Main theorem
    - Example
categories:
    - Theorems in dynamical systems
---
In mathematics, in the study of dynamical systems, the Hartman–Grobman theorem or linearization theorem is a theorem about the local behavior of dynamical systems in the neighbourhood of a hyperbolic equilibrium point. It asserts that linearization—our first resort in applications—is unreasonably effective in predicting qualitative patterns of behavior.
