---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ordinal_arithmetic
offline_file: ""
offline_thumbnail: ""
uuid: 78e02155-38c7-4a29-b32f-6b97ced4bd1b
updated: 1484309509
title: Ordinal arithmetic
tags:
    - Addition
    - Multiplication
    - Exponentiation
    - Cantor normal form
    - Factorization into primes
    - Large countable ordinals
    - Natural operations
    - Nimber arithmetic
    - Notes
categories:
    - Set theory
---
In the mathematical field of set theory, ordinal arithmetic describes the three usual operations on ordinal numbers: addition, multiplication, and exponentiation. Each can be defined in essentially two different ways: either by constructing an explicit well-ordered set which represents the operation or by using transfinite recursion. Cantor normal form provides a standardized way of writing ordinals. The so-called "natural" arithmetical operations retain commutativity at the expense of continuity. Interpreted as nimbers, ordinals are also subject to nimber arithmetic operations.
