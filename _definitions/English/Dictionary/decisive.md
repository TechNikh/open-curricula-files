---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decisive
offline_file: ""
offline_thumbnail: ""
uuid: c5528e25-a04e-4bd0-a96a-d51520ea0d62
updated: 1484310587
title: decisive
categories:
    - Dictionary
---
decisive
     adj 1: determining or having the power to determine an outcome;
            "cast the decisive vote"; "two factors had a decisive
            influence" [ant: {indecisive}]
     2: unmistakable; "had a decisive lead in the polls"
     3: characterized by decision and firmness; "an able an decisive
        young woman"; "we needed decisive leadership"; "she gave
        him a decisive answer" [ant: {indecisive}]
     4: forming or having the nature of a turning point or crisis;
        "a critical point in the campaign"; "the critical test"
        [syn: {critical}]
