---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Blotto_games
offline_file: ""
offline_thumbnail: ""
uuid: 9488c0bd-6404-4c4a-8bc1-7364382805a7
updated: 1484309050
title: Blotto games
tags:
    - Example
    - Application
categories:
    - Non-cooperative games
---
Blotto games (or Colonel Blotto games, or "Divide a Dollar" games) constitute a class of two-person zero-sum games in which the players are tasked to simultaneously distribute limited resources over several objects (or battlefields). In the classic version of the game, the player devoting the most resources to a battlefield wins that battlefield, and the gain (or payoff) is then equal to the total number of battlefields won.
