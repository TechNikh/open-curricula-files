---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adiabatic_invariant
offline_file: ""
offline_thumbnail: ""
uuid: 18fb94d5-db5e-472f-b291-142206d4eb35
updated: 1484308834
title: Adiabatic invariant
tags:
    - Thermodynamics
    - Adiabatic expansion of an ideal gas
    - "Wien's law – adiabatic expansion of a box of light"
    - Classical mechanics – action variables
    - Old quantum theory
    - Plasma physics
    - The first adiabatic invariant, μ
    - The second adiabatic invariant, J
    - The third adiabatic invariant, Φ
categories:
    - Thermodynamics
---
In thermodynamics, an adiabatic process is a change that occurs without heat flow, and slowly compared to the time to reach equilibrium. In an adiabatic process, the system is in equilibrium at all stages. Under these conditions, the entropy is constant.
