---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Partition_function_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: ade75d0e-c6b8-4658-9b1f-1d3dadc4d031
updated: 1484308775
title: Partition function (mathematics)
tags:
    - Definition
    - The parameter β
    - Symmetry
    - As a measure
    - Normalization
    - Expectation values
    - Information geometry
    - Correlation functions
    - General properties
categories:
    - Entropy and information
---
The partition function or configuration integral, as used in probability theory, information theory and dynamical systems, is a generalization of the definition of a partition function in statistical mechanics. It is a special case of a normalizing constant in probability theory, for the Boltzmann distribution. The partition function occurs in many problems of probability theory because, in situations where there is a natural symmetry, its associated probability measure, the Gibbs measure, has the Markov property. This means that the partition function occurs not only in physical systems with translation symmetry, but also in such varied settings as neural networks (the Hopfield network), ...
