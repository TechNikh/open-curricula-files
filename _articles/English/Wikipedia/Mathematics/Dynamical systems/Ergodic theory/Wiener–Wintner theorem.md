---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Wiener%E2%80%93Wintner_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 97a72353-a394-4ecf-aae9-ebacc9906026
updated: 1484308775
title: Wiener–Wintner theorem
categories:
    - Ergodic theory
---
In mathematics, the Wiener–Wintner theorem, named after Norbert Wiener and Aurel Wintner, is a strengthening of the ergodic theorem, proved by Wiener and Wintner (1941)
