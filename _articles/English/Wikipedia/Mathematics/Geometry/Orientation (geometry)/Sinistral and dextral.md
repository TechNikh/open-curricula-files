---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sinistral_and_dextral
offline_file: ""
offline_thumbnail: ""
uuid: 6a90274e-9c7b-41a4-bbb2-fed903f6549f
updated: 1484309300
title: Sinistral and dextral
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/218px-Strike_slip_fault.png
tags:
    - Geology
    - Biology
    - Gastropods
    - Flatfish
    - Notes
categories:
    - Orientation (geometry)
---
The terms are derived from the Latin words for “left” (sinister) and “right” (dexter).
