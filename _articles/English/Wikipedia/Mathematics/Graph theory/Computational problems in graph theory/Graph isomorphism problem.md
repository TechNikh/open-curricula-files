---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_isomorphism_problem
offline_file: ""
offline_thumbnail: ""
uuid: 3145e87c-fdc8-4c8b-9880-21fd715511d0
updated: 1484309391
title: Graph isomorphism problem
tags:
    - State of the art
    - Solved special cases
    - Complexity class GI
    - GI-complete and GI-hard problems
    - Isomorphism of other objects
    - GI-complete classes of graphs
    - Other GI-complete problems
    - GI-hard problems
    - Program checking
    - Applications
    - Notes
    - Surveys and monographs
    - Software
categories:
    - Computational problems in graph theory
---
The problem is neither known to be solvable in polynomial time nor NP-complete, and therefore may be in the computational complexity class NP-intermediate. It is known that the graph isomorphism problem is in the low hierarchy of class NP, which implies that it is not NP-complete unless the polynomial time hierarchy collapses to its second level.[1] At the same time, isomorphism for many special classes of graphs can be solved in polynomial time, and in practice graph isomorphism can often be solved efficiently.[2]
