---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chicken_wire_(chemistry)
offline_file: ""
offline_thumbnail: ""
uuid: 73f25b33-625c-4398-926b-5ca46a4cda56
updated: 1484309275
title: Chicken wire (chemistry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fullerene-C60.png
tags:
    - Examples
    - Polycyclic aromatic hydrocarbons
    - Hexagonal molecular structures
    - Additional information
    - Bond line notation
    - Placeholder for organic compounds
    - Chemical joke
    - Surface plots
categories:
    - Molecular geometry
---
In chemistry the term chicken wire is used in different contexts. Most of them relate to the similarity of the regular hexagonal (honeycomb-like) patterns found in certain chemical compounds to the mesh structure commonly seen in real chicken wire.
