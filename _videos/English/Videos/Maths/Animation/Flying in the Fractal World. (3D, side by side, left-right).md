---
version: 1
type: video
provider: YouTube
id: jVZoD-z_gaY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6685bb7b-e34b-4146-a652-967fcdc2bb71
updated: 1486069641
title: Flying in the Fractal World. (3D, side by side, left-right)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/jVZoD-z_gaY/default.jpg
    - https://i3.ytimg.com/vi/jVZoD-z_gaY/1.jpg
    - https://i3.ytimg.com/vi/jVZoD-z_gaY/2.jpg
    - https://i3.ytimg.com/vi/jVZoD-z_gaY/3.jpg
---
