---
version: 1
type: video
provider: YouTube
id: m6oMgtGsAa4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20BC%20Free%20Response%20%236d.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e22e1e91-99c4-4f5b-97ed-7b291bd87f8c
updated: 1486069690
title: '2011 Calculus BC Free Response #6d'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/m6oMgtGsAa4/default.jpg
    - https://i3.ytimg.com/vi/m6oMgtGsAa4/1.jpg
    - https://i3.ytimg.com/vi/m6oMgtGsAa4/2.jpg
    - https://i3.ytimg.com/vi/m6oMgtGsAa4/3.jpg
---
