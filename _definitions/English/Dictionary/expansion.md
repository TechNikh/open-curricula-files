---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expansion
offline_file: ""
offline_thumbnail: ""
uuid: 78e7277e-4efe-47ed-915d-b2e5e9a24ad6
updated: 1484310475
title: expansion
categories:
    - Dictionary
---
expansion
     n 1: the act of increasing (something) in size or volume or
          quantity or scope [syn: {enlargement}] [ant: {contraction}]
     2: a discussion that provides additional information [syn: {enlargement},
         {elaboration}]
     3: adding information or detail [syn: {expanding upon}]
