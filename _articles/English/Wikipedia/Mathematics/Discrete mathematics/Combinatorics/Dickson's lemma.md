---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Dickson%27s_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 01fb41bb-9fe0-47c4-808f-021dbfc250ce
updated: 1484308562
title: "Dickson's lemma"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Dickson_hyperbola9.svg.png
tags:
    - Example
    - Formal statement
    - Generalizations and applications
categories:
    - Combinatorics
---
In mathematics, Dickson's lemma states that every set of 
  
    
      
        n
      
    
    {\displaystyle n}
  
-tuples of natural numbers has finitely many minimal elements. This simple fact from combinatorics has become attributed to the American algebraist L. E. Dickson, who used it to prove a result in number theory about perfect numbers.[1] However, the lemma was certainly known earlier, for example to Paul Gordan in his research on invariant theory.[2]
