---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stands
offline_file: ""
offline_thumbnail: ""
uuid: a57e42e0-0f79-4bfc-b04b-e1855e401bd6
updated: 1484310277
title: stands
categories:
    - Dictionary
---
stands
     n : tiered seats consisting of a structure (often made of wood)
         where people can sit to watch an event (game or parade)
         [syn: {stand}]
