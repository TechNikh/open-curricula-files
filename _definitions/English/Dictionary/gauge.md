---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gauge
offline_file: ""
offline_thumbnail: ""
uuid: 2c58ca6c-3811-4338-b4c7-bb112ae2d294
updated: 1484310196
title: gauge
categories:
    - Dictionary
---
gauge
     n 1: a measuring instrument for measuring and indicating a
          quantity such as the thickness of wire or the amount of
          rain etc. [syn: {gage}]
     2: accepted or approved instance or example of a quantity or
        quality against which others are judged or measured or
        compared [syn: {standard of measurement}]
     3: the distance between the rails of a railway or between the
        wheels of a train
     4: the thickness of wire
     5: diameter of a tube or gun barrel [syn: {bore}, {caliber}, {calibre}]
     v 1: judge tentatively or form an estimate of (quantities or
          time); "I estimate this chicken to weigh three pounds"
          [syn: ...
