---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/investing
offline_file: ""
offline_thumbnail: ""
uuid: 0e449125-9462-4c76-bb68-49ece54fcbc3
updated: 1484310531
title: investing
categories:
    - Dictionary
---
investing
     n : the act of investing; laying out money or capital in an
         enterprise with the expectation of profit [syn: {investment}]
