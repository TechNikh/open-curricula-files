---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pollution
offline_file: ""
offline_thumbnail: ""
uuid: c72194a5-6a3f-4700-a469-ef338192626b
updated: 1484310268
title: pollution
categories:
    - Dictionary
---
pollution
     n 1: undesirable state of the natural environment being
          contaminated with harmful substances as a consequence of
          human activities
     2: the state of being polluted [syn: {befoulment}, {defilement}]
     3: the act of contaminating or polluting; including (either
        intentionally or accidentally) unwanted substances or
        factors [syn: {contamination}] [ant: {decontamination}]
