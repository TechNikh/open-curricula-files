---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/redox
offline_file: ""
offline_thumbnail: ""
uuid: cf39a1f2-d299-446e-b900-fdef2a2eda2f
updated: 1484310375
title: redox
categories:
    - Dictionary
---
redox
     n : a reversible chemical reaction in which one reaction is an
         oxidation and the reverse is a reduction [syn: {oxidation-reduction},
          {oxidoreduction}]
