---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hume%27s_principle'
offline_file: ""
offline_thumbnail: ""
uuid: ed251bca-2132-429c-bfcb-271a606fe831
updated: 1484309506
title: "Hume's principle"
tags:
    - Origins
    - Influence on set theory
categories:
    - Set theory
---
Hume's principle or HP—the terms were coined by George Boolos—says that the number of Fs is equal to the number of Gs if and only if there is a one-to-one correspondence (a bijection) between the Fs and the Gs. HP can be stated formally in systems of second-order logic. Hume's principle is named for the Scottish philosopher David Hume.
