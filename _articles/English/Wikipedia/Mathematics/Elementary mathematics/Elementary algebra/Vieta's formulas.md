---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Vieta%27s_formulas'
offline_file: ""
offline_thumbnail: ""
uuid: 65e91954-d69c-4eda-839b-b1ae0356bfa7
updated: 1484308915
title: "Vieta's formulas"
tags:
    - The Laws
    - Basic formulas
    - Generalization to rings
    - Example
    - Proof
    - History
categories:
    - Elementary algebra
---
In mathematics, Vieta's formulas are formulas that relate the coefficients of a polynomial to sums and products of its roots. Named after François Viète (more commonly referred to by the Latinised form of his name, Franciscus Vieta), the formulas are used specifically in algebra.
