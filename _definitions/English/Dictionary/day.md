---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/day
offline_file: ""
offline_thumbnail: ""
uuid: 28ce7e02-4f1a-432a-b2af-68a98db17c79
updated: 1484310357
title: day
categories:
    - Dictionary
---
day
     n 1: time for Earth to make a complete rotation on its axis; "two
          days later they left"; "they put on two performances
          every day"; "there are 30,000 passengers per day" [syn:
          {twenty-four hours}, {solar day}, {mean solar day}]
     2: some point or period in time; "it should arrive any day
        now"; "after that day she never trusted him again"; "those
        were the days"; "these days it is not unusual"
     3: the time after sunrise and before sunset while it is light
        outside; "the dawn turned night into day"; "it is easier
        to make the repairs in the daytime" [syn: {daytime}, {daylight}]
        [ant: {night}]
     4: a day ...
