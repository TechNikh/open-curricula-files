---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Transportation_theory_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: aaa82321-c41e-484e-9037-a79edb23b85c
updated: 1484309431
title: Transportation theory (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Nuvola_apps_ksysv.png
tags:
    - Motivation
    - Mines and factories
    - 'Moving books: the importance of the cost function'
    - Abstract formulation of the problem
    - Monge and Kantorovich formulations
    - Duality formula
    - Solution of the problem
    - Optimal transportation on the real line
    - Separable Hilbert spaces
categories:
    - Matching
---
The problem was formalized by the French mathematician Gaspard Monge in 1781.[1]
