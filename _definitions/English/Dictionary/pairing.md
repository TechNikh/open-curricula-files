---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pairing
offline_file: ""
offline_thumbnail: ""
uuid: 1cec773b-412a-49ea-8b8a-854383daa2d8
updated: 1484310395
title: pairing
categories:
    - Dictionary
---
pairing
     n 1: the act of pairing a male and female for reproductive
          purposes; "the casual couplings of adolescents"; "the
          mating of some species occurs only in the spring" [syn:
          {coupling}, {mating}, {conjugation}, {union}, {sexual
          union}]
     2: the act of grouping things or people in pairs
