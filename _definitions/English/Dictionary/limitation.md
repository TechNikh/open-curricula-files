---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/limitation
offline_file: ""
offline_thumbnail: ""
uuid: 9e0a87cd-2157-4540-ae4b-6eef52c52106
updated: 1484310170
title: limitation
categories:
    - Dictionary
---
limitation
     n 1: a principle that limits the extent of something; "I am
          willing to accept certain restrictions on my movements"
          [syn: {restriction}]
     2: the quality of being limited or restricted; "it is a good
        plan but it has serious limitations"
     3: the greatest amount of something that is possible or
        allowed; "there are limits on the amount you can bet"; "it
        is growing rapidly with no limitation in sight" [syn: {limit}]
     4: (law) a time period after which suits cannot be brought;
        "statute of limitations"
     5: an act of limiting or restricting (as by regulation) [syn: {restriction}]
