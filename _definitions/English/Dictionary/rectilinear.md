---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rectilinear
offline_file: ""
offline_thumbnail: ""
uuid: bdad80ce-00bd-41f5-961a-6f968b2df386
updated: 1484310221
title: rectilinear
categories:
    - Dictionary
---
rectilinear
     adj : characterized by a straight line or lines; "rectilinear
           patterns in wallpaper"; "the rectilinear propagation of
           light" [syn: {rectilineal}]
