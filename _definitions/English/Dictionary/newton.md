---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/newton
offline_file: ""
offline_thumbnail: ""
uuid: 5b984ea5-c2b0-4ea1-8675-bdbcf2c58a47
updated: 1484310196
title: newton
categories:
    - Dictionary
---
Newton
     n 1: English mathematician and physicist; remembered for
          developing the calculus and for his law of gravitation
          and his three laws of motion (1642-1727) [syn: {Isaac
          Newton}, {Sir Isaac Newton}]
     2: a unit of force equal to the force that imparts an
        acceleration of 1 m/sec/sec to a mass of 1 kilogram; equal
        to 100,000 dynes [syn: {N}]
