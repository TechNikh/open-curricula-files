---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/probable
offline_file: ""
offline_thumbnail: ""
uuid: ade4b01c-b6dc-4801-8d58-1531c02e82c3
updated: 1484310303
title: probable
categories:
    - Dictionary
---
probable
     adj 1: likely but not certain to be or become true or real; "a
            likely result"; "he foresaw a probable loss" [syn: {likely},
             {plausible}] [ant: {improbable}]
     2: apparently destined; "the probable consequences of going
        ahead with the scheme"
     n : an applicant likely to be chosen
