---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proof_theory
offline_file: ""
offline_thumbnail: ""
uuid: e09880aa-38c4-4795-9124-587be46e5131
updated: 1484309492
title: Proof theory
tags:
    - History
    - Structural proof theory
    - Ordinal analysis
    - Provability logic
    - Reverse mathematics
    - Functional interpretations
    - Formal and informal proof
    - Proof-theoretic semantics
    - Notes
categories:
    - Proof theory
---
Proof theory is a major branch[1] of mathematical logic that represents proofs as formal mathematical objects, facilitating their analysis by mathematical techniques. Proofs are typically presented as inductively-defined data structures such as plain lists, boxed lists, or trees, which are constructed according to the axioms and rules of inference of the logical system. As such, proof theory is syntactic in nature, in contrast to model theory, which is semantic in nature.
