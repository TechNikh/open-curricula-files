---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apparel
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484636401
title: apparel
categories:
    - Dictionary
---
apparel
     n : clothing in general; "she was refined in her choice of
         apparel"; "he always bought his clothes at the same
         store"; "fastidious about his dress" [syn: {wearing
         apparel}, {dress}, {clothes}]
     v : provide with clothes or put clothes on; "Parents must feed
         and dress their child" [syn: {dress}, {clothe}, {enclothe},
          {garb}, {raiment}, {tog}, {garment}, {habilitate}, {fit
         out}] [ant: {undress}]
