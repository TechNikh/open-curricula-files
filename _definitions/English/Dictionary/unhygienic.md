---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unhygienic
offline_file: ""
offline_thumbnail: ""
uuid: 13d85d56-6525-4bd6-8273-c9f8d8dcd21f
updated: 1484310515
title: unhygienic
categories:
    - Dictionary
---
unhygienic
     adj : so unclean as to be a likely cause of disease; "pathetic
           dogs kept in small unhygienic cages"
