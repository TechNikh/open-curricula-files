---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/viewer
offline_file: ""
offline_thumbnail: ""
uuid: 5baaed13-6dc5-46bf-889d-795944bb802f
updated: 1484310208
title: viewer
categories:
    - Dictionary
---
viewer
     n 1: a close observer; someone who looks at something (such as an
          exhibition of some kind); "the spectators applauded the
          performance"; "television viewers"; "sky watchers
          discovered a new star" [syn: {spectator}, {witness}, {watcher},
           {looker}]
     2: an optical device for viewing photographic transparencies
