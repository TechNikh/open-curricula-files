---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cruise
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484631121
title: cruise
categories:
    - Dictionary
---
cruise
     n : an ocean trip taken for pleasure [syn: {sail}]
     v 1: drive around aimlessly but ostentatiously and at leisure;
          "She cruised the neighborhood in her new convertible"
     2: travel at a moderate speed; "Please keep your seat belt
        fastened while the plane is reaching cruising altitude"
     3: look for a sexual partner in a public place; "The men were
        cruising the park"
     4: sail or travel about for pleasure, relaxation, or
        sightseeing; "We were cruising in the Caribbean"
