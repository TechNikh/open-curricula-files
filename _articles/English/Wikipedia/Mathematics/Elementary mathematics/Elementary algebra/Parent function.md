---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parent_function
offline_file: ""
offline_thumbnail: ""
uuid: dece9a3f-fc84-41b0-bbbe-142bd8183951
updated: 1484308913
title: Parent function
categories:
    - Elementary algebra
---
In mathematics, a parent function is the simplest function of a family of functions that preserves the definition (or shape) of the entire family. For example, for the family of quadratic functions having the general form
