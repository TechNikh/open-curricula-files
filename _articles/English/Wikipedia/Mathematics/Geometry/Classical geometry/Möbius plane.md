---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/M%C3%B6bius_plane'
offline_file: ""
offline_thumbnail: ""
uuid: 64ddd856-5263-4a56-8502-cef84dfdb74e
updated: 1484309167
title: Möbius plane
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Moebius-touching-circles.svg.png
tags:
    - Relation to affine planes
    - Classical real Möbius plane
    - Axioms of a Möbius plane
    - Miquelian Möbius planes
    - Finite Möbius planes and block designs
categories:
    - Classical geometry
---
In mathematics, a Möbius plane (named after August Ferdinand Möbius) is one of the Benz planes: Möbius plane, Laguerre plane and Minkowski plane. The classical example is based on the geometry of lines and circles in the real affine plane.
