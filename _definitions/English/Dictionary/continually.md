---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/continually
offline_file: ""
offline_thumbnail: ""
uuid: 550d40b2-6e2b-4e36-8560-275a61d39575
updated: 1484310321
title: continually
categories:
    - Dictionary
---
continually
     adv : seemingly without interruption; "complained continually that
           there wasn't enough money"
