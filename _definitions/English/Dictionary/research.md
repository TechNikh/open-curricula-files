---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/research
offline_file: ""
offline_thumbnail: ""
uuid: ee493669-13d5-4886-9cdd-c7326879a120
updated: 1484310323
title: research
categories:
    - Dictionary
---
research
     n 1: systematic investigation to establish facts
     2: a search for knowledge; "their pottery deserves more
        research than it has received" [syn: {inquiry}, {enquiry}]
     v 1: inquire into [syn: {search}, {explore}]
     2: attempt to find out in a systematically and scientific
        manner; "The student researched the history of that word"
