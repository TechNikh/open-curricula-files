---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metric_signature
offline_file: ""
offline_thumbnail: ""
uuid: 7f3b83d2-8765-431e-8322-445b1b561e15
updated: 1484309228
title: Metric signature
tags:
    - Definition
    - Properties
    - Signature and dimension
    - "Sylvester's law of inertia: independence of basis choice and existence of orthonormal basis"
    - Geometrical interpretation of the indices
    - Examples
    - Matrices
    - Scalar products
    - How to compute the signature
    - Signature in physics
    - Signature change
    - Notes
categories:
    - Differential geometry
---
The signature (p, q, r) of a metric tensor g (or equivalently, a real quadratic form thought of as a real symmetric bilinear form on a finite-dimensional vector space) is the number (counted with multiplicity) of positive, negative and zero eigenvalues of the real symmetric matrix gab of the metric tensor with respect to a basis. Alternatively, it can be defined as the dimensions of a maximal positive, negative and null subspace. By Sylvester's law of inertia these numbers do not depend on the choice of basis. The signature thus classifies the metric up to a choice of basis. The signature is often denoted by a pair of integers (p, q) implying r = 0 or as an explicit list of signs of ...
