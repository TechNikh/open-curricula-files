---
version: 1
type: video
provider: YouTube
id: -xyTz0WZ1W4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Alternate%20Solution%20to%20Ratio%20Problem%20%28HD%20Version%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 01c789bf-87c7-4347-acd1-ca77b52731b3
updated: 1486069569
title: Alternate Solution to Ratio Problem (HD Version)
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/-xyTz0WZ1W4/default.jpg
    - https://i3.ytimg.com/vi/-xyTz0WZ1W4/1.jpg
    - https://i3.ytimg.com/vi/-xyTz0WZ1W4/2.jpg
    - https://i3.ytimg.com/vi/-xyTz0WZ1W4/3.jpg
---
An alternate solution to the advanced ratio problem in the last video.
More free lessons at: http://www.khanacademy.org/video?v=-xyTz0WZ1W4
