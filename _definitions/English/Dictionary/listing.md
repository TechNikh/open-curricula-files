---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/listing
offline_file: ""
offline_thumbnail: ""
uuid: b1d5271e-b3f5-42f1-89c0-4b39112b484a
updated: 1484310599
title: listing
categories:
    - Dictionary
---
listing
     n 1: a database containing an ordered array of items (names or
          topics) [syn: {list}]
     2: the act of making a list of items [syn: {itemization}, {itemisation}]
