---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weary
offline_file: ""
offline_thumbnail: ""
uuid: 03c51c5c-73a4-45c5-9bab-03ef184adb28
updated: 1484310148
title: weary
categories:
    - Dictionary
---
weary
     adj : physically and mentally fatigued; "`aweary' is archaic"
           [syn: {aweary}]
     v 1: exhaust or tire through overuse or great strain or stress;
          "We wore ourselves out on this hike" [syn: {tire}, {wear
          upon}, {tire out}, {wear}, {jade}, {wear out}, {outwear},
           {wear down}, {fag out}, {fag}, {fatigue}] [ant: {refresh}]
     2: get tired of something or somebody [syn: {tire}, {pall}, {fatigue},
         {jade}]
     [also: {wearied}, {weariest}, {wearier}]
