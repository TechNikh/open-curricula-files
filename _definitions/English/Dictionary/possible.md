---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/possible
offline_file: ""
offline_thumbnail: ""
uuid: b92a368c-fc37-437d-bf37-5e55f6f20e22
updated: 1484310323
title: possible
categories:
    - Dictionary
---
possible
     adj 1: capable of happening or existing; "a breakthrough may be
            possible next year"; "anything is possible"; "warned
            of possible consequences" [ant: {impossible}]
     2: existing in possibility; "a potential problem"; "possible
        uses of nuclear power" [syn: {potential}] [ant: {actual}]
     3: possible to conceive or imagine; "that is one possible
        answer" [syn: {conceivable}, {imaginable}]
     n 1: something that can be done; "politics is the art of the
          possible"
     2: an applicant who might be suitable
