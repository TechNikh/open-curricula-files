---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/federal
offline_file: ""
offline_thumbnail: ""
uuid: 6cb10161-36b2-4498-b58c-809a8291bd5f
updated: 1484310567
title: federal
categories:
    - Dictionary
---
federal
     adj 1: national; especially in reference to the government of the
            United States as distinct from that of its member
            units; "the Federal Bureau of Investigation"; "federal
            courts"; "the federal highway program"; "federal
            property"
     2: of or relating to the central government of a federation; "a
        federal district is one set aside as the seat of the
        national government"
     3: being of or having to do with the northern United States and
        those loyal to the Union during the Civil War; "Union
        soldiers"; "Federal forces"; "a Federal infantryman" [syn:
         {Union}]
     4: characterized by or ...
