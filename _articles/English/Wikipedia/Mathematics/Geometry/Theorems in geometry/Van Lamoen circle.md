---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Van_Lamoen_circle
offline_file: ""
offline_thumbnail: ""
uuid: 43a38340-afbe-42c5-bfa9-f9207c5cf520
updated: 1484309126
title: Van Lamoen circle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Van_Lamoen_circle.svg.png
tags:
    - History
    - Properties
categories:
    - Theorems in geometry
---
In Euclidean plane geometry, the van Lamoen circle is a special circle associated with any given triangle 
  
    
      
        T
      
    
    {\displaystyle T}
  
. It contains the circumcenters of the six triangles that are defined inside 
  
    
      
        T
      
    
    {\displaystyle T}
  
 by its three medians.[1][2]
