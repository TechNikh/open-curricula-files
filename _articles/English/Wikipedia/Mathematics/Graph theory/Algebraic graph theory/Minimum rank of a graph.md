---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minimum_rank_of_a_graph
offline_file: ""
offline_thumbnail: ""
uuid: 57f0835f-42b6-41ea-9407-63e66f546070
updated: 1484309374
title: Minimum rank of a graph
tags:
    - Definition
    - Properties
    - Characterization of known graph families
    - Notes
categories:
    - Algebraic graph theory
---
In mathematics, the minimum rank is a graph parameter 
  
    
      
        mr
        ⁡
        (
        G
        )
      
    
    {\displaystyle \operatorname {mr} (G)}
  
 for any graph G. It was motivated by the Colin de Verdière's invariant.
