---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kashmir
offline_file: ""
offline_thumbnail: ""
uuid: 337de89e-ea7b-4a36-85a2-fc808d72c67f
updated: 1484310435
title: kashmir
categories:
    - Dictionary
---
Kashmir
     n : an area in southwestern Asia whose sovereignty is disputed
         between Pakistan and India [syn: {Cashmere}, {Jammu and
         Kashmir}]
