---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/operations
offline_file: ""
offline_thumbnail: ""
uuid: acb0c0c5-c8b6-477b-aa00-2820e6919daa
updated: 1484310256
title: operations
categories:
    - Dictionary
---
operations
     n : financial transactions at a brokerage; having to do with the
         execution of trades and keeping customer records [syn: {trading
         operations}]
