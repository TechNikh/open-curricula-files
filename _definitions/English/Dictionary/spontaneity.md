---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spontaneity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484556661
title: spontaneity
categories:
    - Dictionary
---
spontaneity
     n : the quality of being spontaneous and coming from natural
         feelings without constraint; "the spontaneity of his
         laughter" [syn: {spontaneousness}]
