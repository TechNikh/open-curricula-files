---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/justice
offline_file: ""
offline_thumbnail: ""
uuid: 80052d85-a29f-4ca9-abe7-4a946205d83c
updated: 1484310545
title: justice
categories:
    - Dictionary
---
justice
     n 1: the quality of being just or fair [syn: {justness}] [ant: {injustice}]
     2: the administration of law; the act of determining rights and
        assigning rewards or punishments; "justice deferred is
        justice denied" [syn: {judicature}]
     3: a public official authorized to decide questions bought
        before a court of justice [syn: {judge}, {jurist}, {magistrate}]
     4: the United States federal department responsible for
        enforcing federal laws (including the enforcement of all
        civil rights legislation); created in 1870 [syn: {Department
        of Justice}, {Justice Department}, {DoJ}]
