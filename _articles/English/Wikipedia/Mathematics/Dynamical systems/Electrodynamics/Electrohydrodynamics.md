---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electrohydrodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 580b53a0-5e18-463d-ad5f-cc99c6ffa603
updated: 1484308753
title: Electrohydrodynamics
tags:
    - Electrokinesis
    - Water electrokinetics
    - Electrokinetic instabilities
    - Misc
categories:
    - Electrodynamics
---
Electrohydrodynamics (EHD), also known as electro-fluid-dynamics (EFD) or electrokinetics, is the study of the dynamics of electrically charged fluids.[1] It is the study of the motions of ionised particles or molecules and their interactions with electric fields and the surrounding fluid. The term may be considered to be synonymous with the rather elaborate electrostrictive hydrodynamics. EHD covers the following types of particle and fluid transport mechanisms: electrophoresis, electrokinesis, dielectrophoresis, electro-osmosis, and electrorotation. In general, the phenomena relate to the direct conversion of electrical energy into kinetic energy, and vice versa.
