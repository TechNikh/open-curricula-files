---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Support_function
offline_file: ""
offline_thumbnail: ""
uuid: 9685cd86-8bbb-4d94-a991-b859f5125965
updated: 1484309197
title: Support function
tags:
    - Definition
    - Examples
    - Properties
    - As a function of x
    - As a function of A
    - Variants
categories:
    - Convex geometry
---
In mathematics, the support function hA of a non-empty closed convex set A in 
  
    
      
        
          
            R
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {R} ^{n}}
  
 describes the (signed) distances of supporting hyperplanes of A from the origin. The support function is a convex function on 
  
    
      
        
          
            R
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {R} ^{n}}
  
. Any non-empty closed convex set A is uniquely determined by hA. Furthermore, the support function, as a function of the set A is compatible with many natural geometric ...
