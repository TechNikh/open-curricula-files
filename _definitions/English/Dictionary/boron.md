---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boron
offline_file: ""
offline_thumbnail: ""
uuid: b8db2d21-7dde-4672-a842-0ab4f9925402
updated: 1484310399
title: boron
categories:
    - Dictionary
---
boron
     n : a trivalent metalloid element; occurs both in a hard black
         crystal and in the form of a yellow or brown powder [syn:
          {B}, {atomic number 5}]
