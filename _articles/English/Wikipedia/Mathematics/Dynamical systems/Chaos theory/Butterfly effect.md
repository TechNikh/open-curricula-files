---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Butterfly_effect
offline_file: ""
offline_thumbnail: ""
uuid: 43a2888e-5a25-4a8f-be29-7b723aae47d5
updated: 1484308696
title: Butterfly effect
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Lorenz_attractor_yb.svg.png
tags:
    - History
    - Illustration
    - Theory and mathematical definition
    - In physical systems
    - In weather
    - In quantum mechanics
    - In popular culture
categories:
    - Chaos theory
---
The butterfly effect is the concept that small causes can have large effects. Initially, it was used with weather prediction but later the term became a metaphor used in and out of science.[1]
