---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/limitless
offline_file: ""
offline_thumbnail: ""
uuid: e3a81eba-a832-483b-aac2-a4343e676990
updated: 1484310252
title: limitless
categories:
    - Dictionary
---
limitless
     adj 1: without limits in extent or size or quantity; "immeasurable
            vastness of our solar system" [syn: {illimitable}, {measureless},
             {unmeasured}]
     2: having no limits in range or scope; "to start with a theory
        of unlimited freedom is to end up with unlimited
        despotism"- Philip Rahv; "the limitless reaches of outer
        space" [syn: {unlimited}] [ant: {limited}]
     3: seemingly boundless in amount, number, degree, or especially
        extent; "unbounded enthusiasm"; "children with boundless
        energy"; "a limitless supply of money" [syn: {boundless},
        {unbounded}]
