---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alkyl
offline_file: ""
offline_thumbnail: ""
uuid: 789acc16-60ad-48d1-893f-e634eca1ce13
updated: 1484310419
title: alkyl
categories:
    - Dictionary
---
alkyl
     n : any of a series of univalent groups of the general formula
         CnH2n+1 derived from aliphatic hydrocarbons [syn: {alkyl
         group}, {alkyl radical}]
