---
version: 1
type: video
provider: YouTube
id: 6BHbJ_gBOk0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a9aace79-dd3a-4fe0-b884-9443ed467ee4
updated: 1486069682
title: 'Temperature- Crash Course Physics #20'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/6BHbJ_gBOk0/default.jpg
    - https://i3.ytimg.com/vi/6BHbJ_gBOk0/1.jpg
    - https://i3.ytimg.com/vi/6BHbJ_gBOk0/2.jpg
    - https://i3.ytimg.com/vi/6BHbJ_gBOk0/3.jpg
---
