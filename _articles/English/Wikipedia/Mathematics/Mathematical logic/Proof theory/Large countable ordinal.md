---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Large_countable_ordinal
offline_file: ""
offline_thumbnail: ""
uuid: 01dff027-69a1-45ed-857e-d3bca2639ba1
updated: 1484309495
title: Large countable ordinal
tags:
    - Generalities on recursive ordinals
    - Ordinal notations
    - Relationship to systems of arithmetic
    - Specific recursive ordinals
    - Predicative definitions and the Veblen hierarchy
    - The Feferman–Schütte ordinal and beyond
    - Impredicative ordinals
    - “Unrecursable” recursive ordinals
    - Beyond recursive ordinals
    - The Church–Kleene ordinal
    - Admissible ordinals
    - Beyond admissible ordinals
    - “Unprovable” ordinals
    - A pseudo-well-ordering
    - On recursive ordinals
    - Beyond recursive ordinals
    - Both recursive and nonrecursive ordinals
    - Inline references
categories:
    - Proof theory
---
In the mathematical discipline of set theory, there are many ways of describing specific countable ordinals. The smallest ones can be usefully and non-circularly expressed in terms of their Cantor normal forms. Beyond that, many ordinals of relevance to proof theory still have computable ordinal notations. However, it is not possible to decide effectively whether a given putative ordinal notation is a notation or not (for reasons somewhat analogous to the unsolvability of the halting problem); various more-concrete ways of defining ordinals that definitely have notations are available.
