---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/either
offline_file: ""
offline_thumbnail: ""
uuid: c4d083d7-5d60-4ffb-81d5-967d94763946
updated: 1484310325
title: either
categories:
    - Dictionary
---
either
     adv : after a negative statement used as an intensive meaning
           something like `likewise' or `also'; "he isn't stupid,
           but he isn't exactly a genius either"; "I don't know
           either"; "if you don't order dessert I won't either"
