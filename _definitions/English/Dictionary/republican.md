---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/republican
offline_file: ""
offline_thumbnail: ""
uuid: 027a1447-5cdf-495c-8adb-df35d5ce8d85
updated: 1484310569
title: republican
categories:
    - Dictionary
---
republican
     adj 1: relating to or belonging to the Republican Party; "a
            Republican senator"; "Republican party politics"
     2: having the supreme power lying in the body of citizens
        entitled to vote for officers and representatives
        responsible to them or characteristic of such government;
        "the United States shall guarantee to every state in this
        union a republican form of government"- United States
        Constitution; "a very republican notion"; "so little
        republican and so much aristocratic sentiment"- Philip
        Marsh; "our republican and artistic simplicity"-Nathaniel
        Hawthorne
     n 1: a member of the Republican ...
