---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/barrier
offline_file: ""
offline_thumbnail: ""
uuid: fb166ac7-983f-4d56-9874-7caa04290c2e
updated: 1484310258
title: barrier
categories:
    - Dictionary
---
barrier
     n 1: a structure or object that impedes free movement
     2: any condition that makes it difficult to make progress or to
        achieve an objective; "intolerance is a barrier to
        understanding" [syn: {roadblock}]
     3: anything serving to maintain separation by obstructing
        vision or access
