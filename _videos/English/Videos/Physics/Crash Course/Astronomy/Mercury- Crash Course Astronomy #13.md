---
version: 1
type: video
provider: YouTube
id: P3GkZe3nRQ0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Mercury-%20Crash%20Course%20Astronomy%20%2313.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e451547f-c3e6-4f29-938d-077d00eb6743
updated: 1486069684
title: 'Mercury- Crash Course Astronomy #13'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/P3GkZe3nRQ0/default.jpg
    - https://i3.ytimg.com/vi/P3GkZe3nRQ0/1.jpg
    - https://i3.ytimg.com/vi/P3GkZe3nRQ0/2.jpg
    - https://i3.ytimg.com/vi/P3GkZe3nRQ0/3.jpg
---
