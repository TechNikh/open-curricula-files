---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Atiyah_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 4363110e-1c05-4433-a6cd-da583abb393c
updated: 1484309206
title: Atiyah conjecture
categories:
    - Differential geometry
---
In Mathematics, the Atiyah conjecture is a collective term for a number of statements about restrictions on possible values of 
  
    
      
        
          l
          
            2
          
        
      
    
    {\displaystyle l^{2}}
  
-Betti numbers.
