---
version: 1
type: video
provider: YouTube
id: vShCnTY1-T0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Acid%20Base%20Introduction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c533ef36-0b8f-4dde-a65b-3cc62aaf0fea
updated: 1486069587
title: Acid Base Introduction
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/vShCnTY1-T0/default.jpg
    - https://i3.ytimg.com/vi/vShCnTY1-T0/1.jpg
    - https://i3.ytimg.com/vi/vShCnTY1-T0/2.jpg
    - https://i3.ytimg.com/vi/vShCnTY1-T0/3.jpg
---
Arrhenius, Bronsted Lowry, and Lewis Acids and Bases.
More free lessons at: http://www.khanacademy.org/video?v=vShCnTY1-T0
