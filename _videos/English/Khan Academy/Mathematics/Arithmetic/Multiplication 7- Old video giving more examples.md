---
version: 1
type: video
provider: YouTube
id: _k3aWF6_b4w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Multiplication%207-%20Old%20video%20giving%20more%20examples.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 67a331c2-2ca6-4063-9465-4e3ddaefbeca
updated: 1486069571
title: 'Multiplication 7- Old video giving more examples'
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/_k3aWF6_b4w/default.jpg
    - https://i3.ytimg.com/vi/_k3aWF6_b4w/1.jpg
    - https://i3.ytimg.com/vi/_k3aWF6_b4w/2.jpg
    - https://i3.ytimg.com/vi/_k3aWF6_b4w/3.jpg
---
2 examples of multiplying a 3 digit number times a 2 digit number. 1 example of multiplying a 3 digit number times a 3 digit number.

Watch the next lesson: https://www.khanacademy.org/math/arithmetic/multiplication-division/long_division/v/division-2?utm_source=YT&utm_medium=Desc&utm_campaign=Arithmetic

Missed the previous lesson? 
https://www.khanacademy.org/math/arithmetic/multiplication-division/multi_digit_multiplication/v/multiplying-whole-numbers-and-applications-4?utm_source=YT&utm_medium=Desc&utm_campaign=Arithmetic

Arithmetic on Khan Academy: So you're ready to have some arithmetic fun? You've come to the right spot! It's the first "official" math topic and chalked full of fun exercises and great videos which help you start your journey towards math mastery. We'll cover the big ones: addition, subtraction, multiplication, and division, of course. But we don't stop there. We'll get into negative numbers, absolute value, decimals, and fractions, too. Learning math should be fun, and we plan on having some with you. Ready to get started?

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy’s Arithmetic channel: https://www.youtube.com/channel/UCFlJkgTNeC-FYYIQnyE5yzQ?sub_confirmation=1
Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
