---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oriya
offline_file: ""
offline_thumbnail: ""
uuid: 89099191-983f-4e27-8539-6e52a7fef849
updated: 1484310603
title: oriya
categories:
    - Dictionary
---
Oriya
     n 1: a member of a people in India living in Orissa and
          neighboring areas
     2: a Magadhan language that is spoken by the Oriya people and
        is the official language of the Indian state of Orissa
