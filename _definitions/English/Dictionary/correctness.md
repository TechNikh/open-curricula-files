---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/correctness
offline_file: ""
offline_thumbnail: ""
uuid: 92a3c784-cc8e-41a0-8c8c-d933683f439f
updated: 1484310373
title: correctness
categories:
    - Dictionary
---
correctness
     n 1: conformity to fact or truth [syn: {rightness}] [ant: {incorrectness},
           {incorrectness}]
     2: conformity to social expectations [ant: {incorrectness}]
