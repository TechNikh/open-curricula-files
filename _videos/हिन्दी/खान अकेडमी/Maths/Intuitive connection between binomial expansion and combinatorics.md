---
version: 1
type: video
provider: YouTube
id: eVwye7P4aU0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4f629c42-e52b-473f-851c-ffe27678a7b8
updated: 1486069613
title: >
    Intuitive connection between binomial expansion and
    combinatorics
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/eVwye7P4aU0/default.jpg
    - https://i3.ytimg.com/vi/eVwye7P4aU0/1.jpg
    - https://i3.ytimg.com/vi/eVwye7P4aU0/2.jpg
    - https://i3.ytimg.com/vi/eVwye7P4aU0/3.jpg
---
