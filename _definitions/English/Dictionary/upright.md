---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upright
offline_file: ""
offline_thumbnail: ""
uuid: 34853d11-85d9-4fd0-a8d1-a4ecff4e3570
updated: 1484310268
title: upright
categories:
    - Dictionary
---
upright
     adj 1: in a vertical position; not sloping; "an upright post" [syn:
             {unsloped}]
     2: of moral excellence; "a genuinely good person"; "a just
        cause"; "an upright and respectable man"; "the life of the
        nation is secure only while the nation is honest,
        truthful, and virtuous"- Frederick Douglass [syn: {good},
        {just}, {virtuous}]
     3: erect in posture; "behind him sat old man Arthur; he was
        straight with something angry in his attitude"; "stood
        defiantly with unbowed back" [syn: {straight}, {unbent}, {unbowed}]
     4: maintaining an erect position; "standing timber"; "many
        buildings were still standing" ...
