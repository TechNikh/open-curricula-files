---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bubble_ring
offline_file: ""
offline_thumbnail: ""
uuid: 884c8b61-dda5-4a85-8d7a-918f4d5fc7ae
updated: 1484308874
title: Bubble ring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Bubble_Ring_in_Sunlight.JPG
tags:
    - Physics
    - Buoyancy induced toroidal bubbles
    - Cavitation bubbles
    - Cetaceans
    - Human divers
    - Other uses of the term
    - Further references
categories:
    - Vortices
---
A bubble ring, or toroidal bubble, is an underwater vortex ring where an air bubble occupies the core of the vortex, forming a ring shape. The ring of air as well as the nearby water spins poloidally as it travels through the water, much like a flexible bracelet might spin when it is rolled on to a person's arm. The faster the bubble ring spins, the more stable it becomes.[1] Bubble rings and smoke rings are both examples of vortex rings—the physics of which is still under active study in fluid dynamics.[2][3] Devices have been invented which generate bubble vortex rings.[4][5]
