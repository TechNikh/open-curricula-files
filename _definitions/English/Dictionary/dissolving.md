---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dissolving
offline_file: ""
offline_thumbnail: ""
uuid: e21ffb3d-5443-4e88-9f37-674e1bbfacab
updated: 1484310381
title: dissolving
categories:
    - Dictionary
---
dissolving
     adj : causing to dissolve; "the dissolving medium is called the
           solvent"
     n : the process of going into solution; "the dissolving of salt
         in water" [syn: {dissolution}]
