---
version: 1
type: video
provider: YouTube
id: Ft48D-1mOEY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 94821496-dfcb-4f98-8f3c-125b8bb06828
updated: 1486069630
title: 'Learn the Use of the Ninth Vowel- ऐ'
categories:
    - Class 2
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ft48D-1mOEY/default.jpg
    - https://i3.ytimg.com/vi/Ft48D-1mOEY/1.jpg
    - https://i3.ytimg.com/vi/Ft48D-1mOEY/2.jpg
    - https://i3.ytimg.com/vi/Ft48D-1mOEY/3.jpg
---
