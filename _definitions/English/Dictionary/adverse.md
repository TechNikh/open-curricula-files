---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adverse
offline_file: ""
offline_thumbnail: ""
uuid: 4022e69a-2cb2-4a10-abae-cf921916ac1e
updated: 1484310539
title: adverse
categories:
    - Dictionary
---
adverse
     adj 1: contrary to your interests or welfare; "adverse
            circumstances"; "made a place for themselves under the
            most untoward conditions" [syn: {harmful}, {inauspicious},
             {untoward}]
     2: in an opposing direction; "adverse currents"; "a contrary
        wind" [syn: {contrary}]
