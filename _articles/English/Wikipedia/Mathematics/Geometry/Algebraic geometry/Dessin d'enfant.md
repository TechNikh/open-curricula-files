---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Dessin_d%27enfant'
offline_file: ""
offline_thumbnail: ""
uuid: e4dc2348-8a94-4ea4-9d15-3057321b9d78
updated: 1484309145
title: "Dessin d'enfant"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Dessin-LZ109.svg.png
tags:
    - History
    - 19th century
    - 20th century
    - Riemann surfaces and Belyi pairs
    - Maps and hypermaps
    - Regular maps and triangle groups
    - Trees and Shabat polynomials
    - The absolute Galois group and its invariants
    - Notes
categories:
    - Algebraic geometry
---
In mathematics, a dessin d'enfant is a type of graph embedding used to study Riemann surfaces and to provide combinatorial invariants for the action of the absolute Galois group of the rational numbers. The name of these embeddings is French for a "child's drawing"; its plural is either dessins d'enfant, "child's drawings", or dessins d'enfants, "children's drawings".
