---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Intravenous_injection
offline_file: ""
offline_thumbnail: ""
uuid: c4241731-7659-4818-a1f4-a1e47fc88e1f
updated: 1484308368
title: Intravenous therapy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-ICU_IV_1.jpg
tags:
    - Medical uses
    - Volume expanders
    - Blood-based products
    - Blood substitutes
    - Buffer solutions
    - Other medications
    - Other
    - Adverse effects
    - Pain
    - Infection
    - Phlebitis
    - Infiltration / extravasation
    - Fluid overload
    - Hypothermia
    - Electrolyte imbalance
    - Embolism
    - Glucose
    - Intravenous access devices
    - Hypodermic needle
    - Peripheral cannula
    - Complications
    - Central lines
    - Peripherally inserted central catheter
    - Central venous lines
    - Tunnelled lines
    - Implantable ports
    - Other equipment
    - Intermittent infusion
    - History
    - Education
categories:
    - Uncategorized
---
Intravenous therapy is the infusion of liquid substances directly into a vein. Intravenous simply means "within vein". Therapies administered intravenously are often included in the designation of specialty drugs. Intravenous infusions are commonly referred to as drips because many systems of administration employ a drip chamber, which prevents air from entering the blood stream (air embolism), and allows an estimation of flow rate.
