---
title: Deforestation
article_ref: https://en.wikipedia.org/wiki/Deforestation
tags:
    - Causes
    - Environmental problems
    - Atmospheric
    - Hydrological
    - Soil
    - biodiversity
    - Economic impact
    - Forest transition theory
    - Historical causes
    - Prehistory
    - Pre-industrial history
    - Industrial era
    - Rates of deforestation
    - Regions
    - Control
    - Reducing emissions
    - Payments for conserving forests
    - Farming
    - Monitoring deforestation
    - Forest management
    - Sustainable practices
    - Reforestation
    - Forest plantations
    - Military context
    - Public Health Context
categories:
    - Deforestation
---
