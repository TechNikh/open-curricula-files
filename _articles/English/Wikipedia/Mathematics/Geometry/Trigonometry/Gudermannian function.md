---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gudermannian_function
offline_file: ""
offline_thumbnail: ""
uuid: 6590a836-152c-427c-a5ec-6a67195e4e41
updated: 1484309343
title: Gudermannian function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Mplwp_gudermann_piaxis.svg.png
tags:
    - Properties
    - Alternative definitions
    - Some identities
    - Inverse
    - Some identities
    - Derivatives
    - History
    - Applications
categories:
    - Trigonometry
---
The Gudermannian function, named after Christoph Gudermann (1798–1852), relates the circular functions and hyperbolic functions without explicitly using complex numbers.
