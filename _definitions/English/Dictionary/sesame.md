---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sesame
offline_file: ""
offline_thumbnail: ""
uuid: 1a3939b5-1b27-4de5-ba76-be0ced9dfc33
updated: 1484310541
title: sesame
categories:
    - Dictionary
---
sesame
     n : East Indian annual erect herb; source of sesame seed or
         benniseed and sesame oil [syn: {benne}, {benni}, {benny},
          {Sesamum indicum}]
