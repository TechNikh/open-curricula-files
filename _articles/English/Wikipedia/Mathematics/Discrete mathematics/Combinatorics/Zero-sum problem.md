---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zero-sum_problem
offline_file: ""
offline_thumbnail: ""
uuid: 0e9c37a0-ff24-43ae-817a-39a4b8ddcee0
updated: 1484308605
title: Zero-sum problem
categories:
    - Combinatorics
---
In number theory, zero-sum problems are certain kinds of combinatorial problems about the structure of a finite abelian group. Concretely, given a finite abelian group G and a positive integer n, one asks for the smallest value of k such that every sequence of elements of G of size k contains n terms that sum to 0.
