---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dangerously
offline_file: ""
offline_thumbnail: ""
uuid: 68a41bf6-3bdb-4d7a-a591-b9f24dff4f08
updated: 1484310521
title: dangerously
categories:
    - Dictionary
---
dangerously
     adv : in a dangerous manner; "he came dangerously close to falling
           off the ledge" [syn: {perilously}, {hazardously}]
