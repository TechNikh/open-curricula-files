---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/threatened
offline_file: ""
offline_thumbnail: ""
uuid: 622161c0-5e27-4b0b-bb83-31ecde7509e5
updated: 1484310236
title: threatened
categories:
    - Dictionary
---
threatened
     adj : (of flora or fauna) likely in the near future to become
           endangered; "the spotted owl is a threatened species,
           not yet an endangered one"
