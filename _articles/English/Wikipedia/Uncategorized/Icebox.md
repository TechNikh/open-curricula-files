---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ice_box
offline_file: ""
offline_thumbnail: ""
uuid: e8ba5034-27e5-43b4-9fb5-e27a16000213
updated: 1484308375
title: Icebox
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Icebox_1.jpg
tags:
    - Design
    - use
categories:
    - Uncategorized
---
An icebox (also called a cold closet) is a compact non-mechanical refrigerator which was a common kitchen appliance before the development of safe powered refrigeration devices.
