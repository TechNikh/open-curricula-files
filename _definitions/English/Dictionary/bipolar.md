---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bipolar
offline_file: ""
offline_thumbnail: ""
uuid: e8eca449-e964-40cc-91bc-688f662da0e2
updated: 1484310178
title: bipolar
categories:
    - Dictionary
---
bipolar
     adj 1: of or relating to manic depressive illness
     2: of, pertaining to, or occurring in both polar regions; "the
        bipolar distribution of certain species"
     3: having two poles [ant: {unipolar}]
