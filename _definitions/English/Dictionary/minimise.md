---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minimise
offline_file: ""
offline_thumbnail: ""
uuid: 14d0a623-bb39-4709-bf36-12f1ed1d6040
updated: 1484310236
title: minimise
categories:
    - Dictionary
---
minimise
     v 1: represent as less significant or important [syn: {understate},
           {minimize}, {downplay}] [ant: {overstate}]
     2: make small or insignificant; "Let's minimize the risk" [syn:
         {minimize}] [ant: {maximize}, {maximize}]
