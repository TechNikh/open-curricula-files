---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/academy
offline_file: ""
offline_thumbnail: ""
uuid: 731a1e79-f222-4001-ba22-741aab9c4d91
updated: 1484310194
title: academy
categories:
    - Dictionary
---
academy
     n 1: a secondary school (usually private)
     2: an institution for the advancement of art or science or
        literature [syn: {honorary society}]
     3: a school for special training
     4: a learned establishment for the advancement of knowledge
