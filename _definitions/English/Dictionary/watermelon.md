---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/watermelon
offline_file: ""
offline_thumbnail: ""
uuid: 771645f1-282f-4dbc-a87f-369146de42c2
updated: 1484310221
title: watermelon
categories:
    - Dictionary
---
watermelon
     n 1: an African melon [syn: {watermelon vine}, {Citrullus
          vulgaris}]
     2: large oblong or roundish melon with a hard green rind and
        sweet watery red or occasionally yellowish pulp
