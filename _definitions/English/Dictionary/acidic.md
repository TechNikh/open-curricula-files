---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acidic
offline_file: ""
offline_thumbnail: ""
uuid: 7551919c-23bc-4c35-9254-5d476149409b
updated: 1484310325
title: acidic
categories:
    - Dictionary
---
acidic
     adj 1: being or containing an acid; of a solution having an excess
            of hydrogen atoms (having a pH of less than 7) [ant: {alkaline},
             {amphoteric}]
     2: tasting sour like acid [syn: {acidulent}, {acidulous}]
