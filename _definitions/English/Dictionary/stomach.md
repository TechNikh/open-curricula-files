---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stomach
offline_file: ""
offline_thumbnail: ""
uuid: e1a184b2-33c3-49fe-890b-8930d802d503
updated: 1484310355
title: stomach
categories:
    - Dictionary
---
stomach
     n 1: an enlarged and muscular saclike organ of the alimentary
          canal; the principal organ of digestion [syn: {tummy}, {tum},
           {breadbasket}]
     2: the region of the body of a vertebrate between the thorax
        and the pelvis [syn: {abdomen}, {venter}, {belly}]
     3: an inclination or liking for things involving conflict or
        difficulty or unpleasantness; "he had no stomach for a
        fight"
     4: an appetite for food; "exercise gave him a good stomach for
        dinner"
     v 1: bear to eat; "He cannot stomach raw fish"
     2: put up with something or somebody unpleasant; "I cannot bear
        his constant criticism"; "The new secretary ...
