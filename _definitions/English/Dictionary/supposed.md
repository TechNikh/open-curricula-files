---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supposed
offline_file: ""
offline_thumbnail: ""
uuid: 135f026c-e4d7-499c-b6e2-e04fa967a676
updated: 1484310457
title: supposed
categories:
    - Dictionary
---
supposed
     adj 1: firmly believed; "the way things are supposed to be" [syn: {supposed(p)}]
     2: mistakenly believed; "the supposed existence of ghosts"
        [syn: {supposed(a)}]
     3: commonly put forth or accepted as true on inconclusive
        grounds; "the foundling's putative father"; "the reputed
        (or purported) author of the book"; "the supposed date of
        birth" [syn: {putative(a)}, {purported(a)}, {reputed(a)},
        {supposed(a)}]
     4: designed to; "medication that is supposed to relieve pain";
        "what's that gadget supposed to do?" [syn: {supposed(a)}]
     5: doubtful or suspect; "these so-called experts are no help"
        [syn: {alleged(a)}, ...
