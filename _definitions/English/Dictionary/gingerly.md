---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gingerly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484455921
title: gingerly
categories:
    - Dictionary
---
gingerly
     adj : with extreme care or delicacy; "they proceeded with gingerly
           footwork over the jagged stones"; "the issue was
           handled only in a gingerly way"- W.S.White
     adv : in a gingerly manner; "gingerly I raised the edge of the
           blanket"
