---
version: 1
type: video
provider: YouTube
id: 2q4vSKwaBtw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Introduction%20to%20pH%2C%20pOH%2C%20and%20pKw.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c0493d6f-53d3-416a-aa29-e2298eea088c
updated: 1486069583
title: Introduction to pH, pOH, and pKw
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/2q4vSKwaBtw/default.jpg
    - https://i3.ytimg.com/vi/2q4vSKwaBtw/1.jpg
    - https://i3.ytimg.com/vi/2q4vSKwaBtw/2.jpg
    - https://i3.ytimg.com/vi/2q4vSKwaBtw/3.jpg
---
Autoionization of water into hydronium and hydroxide ions.  pH, pOH, and pKa.
More free lessons at: http://www.khanacademy.org/video?v=2q4vSKwaBtw
