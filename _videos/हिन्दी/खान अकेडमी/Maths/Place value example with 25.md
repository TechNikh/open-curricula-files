---
version: 1
type: video
provider: YouTube
id: DO3R9WWWH3c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b5ee3a88-c90f-4ea4-ab13-39112c3870a0
updated: 1486069609
title: Place value example with 25
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/DO3R9WWWH3c/default.jpg
    - https://i3.ytimg.com/vi/DO3R9WWWH3c/1.jpg
    - https://i3.ytimg.com/vi/DO3R9WWWH3c/2.jpg
    - https://i3.ytimg.com/vi/DO3R9WWWH3c/3.jpg
---
