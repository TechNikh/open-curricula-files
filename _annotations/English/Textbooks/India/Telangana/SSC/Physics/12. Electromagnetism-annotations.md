---
version: 1
type: annotation
id: 'http://eschool2go.org/sites/default/files/chapter_12%20Electromagnetism.pdf'
uuid: 97b91d18-b63f-4fc2-bac0-ab1cdc3075f8
updated: "1483297081"
annotations:
    - 
        quote: electromagnets
        id: https://en.wikipedia.org/wiki/Electromagnets
        uuid: 0abd76c6-4959-4407-80ca-f28e1c46e884
        text: Electromagnet
        comment: >
            An electromagnet is a type of magnet in which the magnetic
            field is produced by an electric current. The magnetic field
            disappears when the current is turned off. Electromagnets
            usually consist of a large number of closely spaced turns of
            wire that create the magnetic field. The wire turns are
            often wound around a magnetic core made from a ferromagnetic
            or ferrimagnetic material such as iron; the magnetic core
            concentrates the magnetic flux and makes a more powerful
            magnet.
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Simple_electromagnet2.gif
            uuid: 60119dc5-5ac6-4500-a888-2802ed24727a
            comment: 'A simple electromagnet consisting of a coil of insulated wire wrapped around an iron core. A core of ferromagnetic material like iron serves to increase the magnetic field created.[1] The strength of magnetic field generated is proportional to the ...'
    - 
        quote: hans
        id: https://en.wikipedia.org/wiki/Hans
        uuid: a3424778-36c5-47bd-998d-06a04353aeba
        text: Hans
        comment: 'Hans may refer to:'
    - 
        quote: oersted
        id: https://en.wikipedia.org/wiki/Oersted
        uuid: a677e60d-d694-4d47-b032-fa8f1192033f
        text: Oersted
        comment: ""
    - 
        quote: thermocole
        id: https://en.wikipedia.org/wiki/Thermocole
        uuid: a71f5a3b-2308-4748-898d-b64eeca88b39
        text: Polystyrene
        comment: 'Polystyrene (PS) /ˌpɒliˈstaɪriːn/ is a synthetic aromatic polymer made from the monomer styrene. Polystyrene can be solid or foamed. General-purpose polystyrene is clear, hard, and rather brittle. It is an inexpensive resin per unit weight. It is a rather poor barrier to oxygen and water vapor and has a relatively low melting point.[4] Polystyrene is one of the most widely used plastics, the scale of its production being several million tonnes per year.[5] Polystyrene can be naturally transparent, but can be colored with colorants. Uses include protective packaging (such as packing peanuts and CD and DVD cases), containers (such as "clamshells"), lids, bottles, trays, tumblers, and ...'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Polystyrene_stick_trp.png
            uuid: 67880db1-958a-4240-9931-15dfdaa5385c
            comment: Stick model of polystyrene
    - 
        quote: weber
        id: https://en.wikipedia.org/wiki/Weber
        uuid: a95c3f2c-0144-4c83-ab88-5b88a5fd2615
        text: Weber
        comment: |
            Weber (/ˈwɛbər/, /ˈwiːbər/ or /ˈweɪbər/; German: [ˈveːbɐ]) is a surname of German origin, derived from the noun meaning "weaver". In some cases, following migration to English-speaking countries, it has been anglicised to the English surname 'Webber' or even 'Weaver'.
    - 
        quote: tesla
        id: https://en.wikipedia.org/wiki/Tesla
        uuid: 9ecb18b4-894e-4c77-9bb1-569041fb8d64
        text: Tesla
        comment: 'Tesla may refer to:'
    - 
        quote: cos
        id: https://en.wikipedia.org/wiki/Cos
        uuid: 07583880-4503-4ca5-8ea5-b001878f9da0
        text: Cos
        comment: ""
    - 
        quote: crt
        id: https://en.wikipedia.org/wiki/Crt
        uuid: 200d0954-b753-4cf5-91b7-cfd25e36bca8
        text: CRT
        comment: 'CRT may refer to:'
    - 
        quote: fore-finger
        id: https://en.wikipedia.org/wiki/Fore-finger
        uuid: 8f39ecc1-6cf7-4f80-9a01-713d5408dd76
        text: Index finger
        comment: >
            The index finger, (also referred to as forefinger, pointer
            finger, trigger finger, digitus secundus, digitus II, and
            many other terms), is the first finger and the second digit
            of a human hand. It is located between the first and third
            digits, between the thumb and the middle finger. It is
            usually the most dextrous and sensitive finger of the hand,
            though not the longest – it is shorter than the middle
            finger, and may be shorter or longer than the ring finger
            – see digit ratio.
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Wijsvinger.jpg
            uuid: 5708197a-a32b-41f2-a25b-6ceedf736260
            comment: Left human hand with index finger extended
    - 
        quote: emf
        id: https://en.wikipedia.org/wiki/Emf
        uuid: 1642e75d-dfff-4582-8d2b-33ab0f28684c
        text: EMF
        comment: 'EMF may stand for:'
    - 
        quote: phi
        id: https://en.wikipedia.org/wiki/Phi
        uuid: f3031580-8c92-45bb-a374-0dadad9bd0e2
        text: Phi
        comment: 'Phi (uppercase Φ, lowercase  or ; Ancient Greek: ϕεῖ, pheî, [pʰé͜e]; modern Greek: φι, fi, [fi]; English: /faɪ/[1]) is the 21st letter of the Greek alphabet. In Ancient Greek, it represented an aspirated voiceless bilabial plosive ([pʰ]), which was the origin of its usual romanization as "ph". In modern Greek, it represents a voiceless labiodental fricative ([f]) and is correspondingly romanized as "f". Its origin is uncertain but it may be that phi originated as the letter qoppa and initially represented the sound /kʷʰ/ before shifting to Classical Greek [pʰ].[2] In traditional Greek numerals, phi has a value of 500 (φʹ) or 500 000 (͵φ). The Cyrillic letter Ef (Ф, ...'
    - 
        quote: lenz
        id: https://en.wikipedia.org/wiki/Lenz
        uuid: 200a2bf1-7e43-4c03-838c-1ff064401422
        text: Lenz
        comment: 'Lenz is a German surname, and may refer to:'
    - 
        quote: vice-versa
        id: https://en.wikipedia.org/wiki/Vice-versa
        uuid: ede12459-178c-494e-9550-493fb1fb2fcf
        text: List of Latin phrases (V)
        comment: >
            This page lists English translations of notable Latin
            phrases, such as veni vidi vici and et cetera. Some of the
            phrases are themselves translations of Greek phrases, as
            Greek rhetoric and literature reached its peak centuries
            before the rise of ancient Rome.
    - 
        quote: rms
        id: https://en.wikipedia.org/wiki/Rms
        uuid: 25d13978-2ae2-4d0d-8873-ca905812922e
        text: RMS
        comment: 'RMS may refer to:'
    - 
        quote: magnetic field
        id: https://en.wikipedia.org/wiki/Magnetic_field
        uuid: 5c3bf8a9-26f9-499a-99d3-f1024e3bbf20
        text: Magnetic field
        comment: 'A magnetic field is the magnetic effect of electric currents and magnetic materials. The magnetic field at any given point is specified by both a direction and a magnitude (or strength); as such it is a vector field.[nb 1] The term is used for two distinct but closely related fields denoted by the symbols B and H, where H is measured in units of amperes per meter (symbol: A⋅m−1 or A/m) in the SI. B is measured in teslas (symbol: T) and newtons per meter per ampere (symbol: N⋅m−1⋅A−1 or N/(m⋅A)) in the SI. B is most commonly defined in terms of the Lorentz force it exerts on moving electric charges.'
        image:
            id: >
                http://eschool2go.org/sites/default/files/articles/thumbnails/250px-VFPt_cylindrical_magnet_thumb.svg.png
            uuid: 3efa55f3-11af-425e-9041-5c1fdfbd2a87
            comment: >
                Magnetic field of an ideal cylindrical magnet with its axis
                of symmetry inside the image plane. The magnetic field is
                represented by magnetic field lines, which show the
                direction of the field at different points.
    - 
        quote: field
        id: https://en.wikipedia.org/wiki/Field
        uuid: c121693e-ebd2-4cd1-8c56-d996829197ed
        text: Field
        comment: 'Field may refer to:'
---
