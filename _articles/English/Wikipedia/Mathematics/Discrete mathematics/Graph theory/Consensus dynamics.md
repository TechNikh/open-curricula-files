---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Consensus_dynamics
offline_file: ""
offline_thumbnail: ""
uuid: 571a838b-fa4f-4778-9ea1-4e793e2bdd8f
updated: 1484308601
title: Consensus dynamics
categories:
    - Graph theory
---
Consensus dynamics or agreement dynamics is an area of research lying at the intersection of systems theory and graph theory. A major topic of investigation is the agreement or consensus problem[1] in multi-agent systems that concerns processes by which a collection of interacting agents achieve a common goal. Networks of agents that exchange information to reach consensus include: physiological systems, gene networks, large-scale energy systems and fleets of vehicles on land, in the air or in space. The agreement protocol or consensus protocol is an unforced dynamical system that is governed by the interconnection topology and the initial condition for each agent. Other problems are the ...
