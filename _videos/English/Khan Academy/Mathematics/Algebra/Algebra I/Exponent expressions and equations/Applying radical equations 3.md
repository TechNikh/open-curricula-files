---
version: 1
type: video
provider: YouTube
id: npUtXLjTnxg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Exponent%20expressions%20and%20equations/Applying%20radical%20equations%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2bc946d8-9ec7-43c2-b6c6-f9f8c54f018e
updated: 1486069565
title: Applying radical equations 3
categories:
    - Exponent expressions and equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/npUtXLjTnxg/default.jpg
    - https://i3.ytimg.com/vi/npUtXLjTnxg/1.jpg
    - https://i3.ytimg.com/vi/npUtXLjTnxg/2.jpg
    - https://i3.ytimg.com/vi/npUtXLjTnxg/3.jpg
---
