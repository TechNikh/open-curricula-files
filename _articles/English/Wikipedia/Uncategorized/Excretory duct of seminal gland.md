---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Seminal_ducts
offline_file: ""
offline_thumbnail: ""
uuid: a036d2db-dd50-4e5f-8f84-4fab9268d7f6
updated: 1484308366
title: Excretory duct of seminal gland
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray1160.png
categories:
    - Uncategorized
---
Each seminal vesicle consists of a single tube, coiled upon itself, and giving off several irregular cecal diverticula; the separate coils, as well as the diverticula, are connected together by fibrous tissue. When uncoiled, the tube is about the diameter of a quill, and varies in length from 10 to 15 cm.; it ends posteriorly in a cul-de-sac; its anterior extremity becomes constricted into a narrow straight duct called the excretory duct of seminal gland (or duct of the seminal vesicle), which joins with the corresponding ductus deferens to form the ejaculatory duct.
