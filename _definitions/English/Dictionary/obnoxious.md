---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obnoxious
offline_file: ""
offline_thumbnail: ""
uuid: 1ac53ae1-90c5-465c-bd60-42896e6feac2
updated: 1484310609
title: obnoxious
categories:
    - Dictionary
---
obnoxious
     adj : causing disapproval or protest; "a vulgar and objectionable
           person" [syn: {objectionable}, {unpleasant}]
