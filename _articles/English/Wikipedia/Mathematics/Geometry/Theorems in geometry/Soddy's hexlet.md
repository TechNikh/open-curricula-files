---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Soddy%27s_hexlet'
offline_file: ""
offline_thumbnail: ""
uuid: 04847321-7e54-4d88-9573-f8d2ebf764c2
updated: 1484309127
title: "Soddy's hexlet"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Rotating_hexlet_equator_opt.gif
tags:
    - Definition
    - Annular hexlet
    - Solution by inversion
    - Dupin cyclide
    - Relation to Steiner chains
    - Parabolic and hyperbolic hexlets
    - Sangaku tablets
    - Notes
categories:
    - Theorems in geometry
---
In geometry, Soddy's hexlet is a chain of six spheres (shown in grey in Figure 1), each of which is tangent to both of its neighbors and also to three mutually tangent given spheres. In Figure 1, these three spheres are shown as a central sphere (red), and two spheres (not shown) above and below the plane the centers of the hexlet spheres lie on. In addition, the hexlet spheres are tangent to a fourth sphere (blue in Figure 1), which is not tangent to the three others.
