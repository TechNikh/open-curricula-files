---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reasonable
offline_file: ""
offline_thumbnail: ""
uuid: f4674af7-3cd0-40c2-bf88-33b88c0507bb
updated: 1484310459
title: reasonable
categories:
    - Dictionary
---
reasonable
     adj 1: showing reason or sound judgment; "a sensible choice"; "a
            sensible person" [syn: {sensible}] [ant: {unreasonable}]
     2: not excessive or extreme; "a fairish income"; "reasonable
        prices" [syn: {fair}, {fairish}]
     3: marked by sound judgment; "sane nuclear policy" [syn: {sane}]
