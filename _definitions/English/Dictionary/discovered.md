---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discovered
offline_file: ""
offline_thumbnail: ""
uuid: dd20adf4-38d1-4169-b4df-90983e9b1671
updated: 1484310313
title: discovered
categories:
    - Dictionary
---
discovered
     adj 1: no longer concealed; uncovered as by opening a curtain;
            `discovered' is archaic and primarily a theater term;
            "the scene disclosed was of a moonlit forest" [syn: {disclosed},
             {revealed}]
     2: discovered or determined by scientific observation;
        "variation in the ascertained flux depends on a number of
        factors"; "the discovered behavior norms"; "discovered
        differences in achievement"; "no explanation for the
        observed phenomena" [syn: {ascertained}, {observed}]
