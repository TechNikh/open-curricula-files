---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leadership
offline_file: ""
offline_thumbnail: ""
uuid: 429e9690-3fd9-49f1-b468-5a99589f778d
updated: 1484310559
title: leadership
categories:
    - Dictionary
---
leadership
     n 1: the activity of leading; "his leadership inspired the team"
          [syn: {leading}]
     2: the body of people who lead a group; "the national
        leadership adopted his plan" [syn: {leaders}]
     3: the status of a leader; "they challenged his leadership of
        the union"
     4: the ability to lead; "he believed that leadership can be
        taught"
