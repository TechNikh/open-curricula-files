---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shabby
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484443261
title: shabby
categories:
    - Dictionary
---
shabby
     adj 1: showing signs of wear and tear; "a ratty old overcoat";
            "shabby furniture"; "an old house with dirty windows
            and tatty curtains" [syn: {moth-eaten}, {ratty}, {tatty}]
     2: mean and unworthy and despicable; "shabby treatment"
     [also: {shabbiest}, {shabbier}]
