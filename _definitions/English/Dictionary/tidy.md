---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tidy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484443321
title: tidy
categories:
    - Dictionary
---
tidy
     adj 1: marked by good order and cleanliness in appearance or
            habits; "a tidy person"; "a tidy house"; "a tidy mind"
            [ant: {untidy}]
     2: (used of hair) neat and tidy; "a nicely kempt beard" [syn: {kempt},
         {trim}]
     3: large in amount or extent or degree; "it cost a considerable
        amount"; "a goodly amount"; "received a hefty bonus"; "a
        respectable sum"; "a tidy sum of money"; "a sizable
        fortune" [syn: {goodly}, {goodish}, {hefty}, {respectable},
         {sizable}, {sizeable}]
     n : receptacle that holds odds and ends (as sewing materials)
     v : put (things or places) in order; "Tidy up your room!" [syn:
         ...
