---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parallel_coordinates
offline_file: ""
offline_thumbnail: ""
uuid: 3fab08f7-1b65-4e11-ba6b-1dbfd282decb
updated: 1484309289
title: Parallel coordinates
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Parallel_coordinates-sample.png
tags:
    - History
    - Higher dimensions
    - Statistical considerations
    - reading
    - Limitations
    - Software
categories:
    - Multi-dimensional geometry
---
To show a set of points in an n-dimensional space, a backdrop is drawn consisting of n parallel lines, typically vertical and equally spaced. A point in n-dimensional space is represented as a polyline with vertices on the parallel axes; the position of the vertex on the i-th axis corresponds to the i-th coordinate of the point.
