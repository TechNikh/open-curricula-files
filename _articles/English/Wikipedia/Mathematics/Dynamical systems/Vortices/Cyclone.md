---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyclone
offline_file: ""
offline_thumbnail: ""
uuid: 296e20f6-8eca-4eef-be7f-fa8da5341b6e
updated: 1484308872
title: Cyclone
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Low_pressure_system_over_Iceland.jpg
tags:
    - Structure
    - Formation
    - Synoptic scale
    - Surface-based types
    - Extratropical cyclone
    - Polar low
    - Subtropical
    - Tropical
    - Upper level types
    - Polar cyclone
    - TUTT cell
    - Mesoscale
    - Mesocyclone
    - Tornado
    - Dust devil
    - Waterspout
    - Steam devil
    - Fire whirl
    - Climate change
    - Other planets
categories:
    - Vortices
---
In meteorology, a cyclone is a large scale air mass that rotates around a strong center of low atmospheric pressure.[1][2] They are usually characterized by inward spiraling winds that rotate counterclockwise in the Northern Hemisphere and clockwise in the Southern Hemisphere. All large-scale cyclones are centered on low-pressure areas.[3][4] The largest low-pressure systems are polar vortices and extratropical cyclones of the largest scale (synoptic scale). According to the National Hurricane Center glossary, warm-core cyclones such as tropical cyclones and subtropical cyclones also lie within the synoptic scale.[5] Mesocyclones, tornadoes and dust devils lie within the smaller ...
