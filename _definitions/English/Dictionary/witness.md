---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/witness
offline_file: ""
offline_thumbnail: ""
uuid: af28e1aa-c3ff-4c42-bec6-f8a22416574d
updated: 1484310210
title: witness
categories:
    - Dictionary
---
witness
     n 1: someone who sees an event and reports what happened [syn: {witnesser},
           {informant}]
     2: a close observer; someone who looks at something (such as an
        exhibition of some kind); "the spectators applauded the
        performance"; "television viewers"; "sky watchers
        discovered a new star" [syn: {spectator}, {viewer}, {watcher},
         {looker}]
     3: testimony by word or deed to your religious faith
     4: (law) a person who attests to the genuineness of a document
        or signature by adding their own signature [syn: {attestant},
         {attestor}, {attestator}]
     5: (law) a person who testifies under oath in a court of law
     v ...
