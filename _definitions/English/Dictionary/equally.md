---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equally
offline_file: ""
offline_thumbnail: ""
uuid: 83f9c78b-eb8a-4531-b9d0-3209bd301d5a
updated: 1484310299
title: equally
categories:
    - Dictionary
---
equally
     adv 1: to the same degree (often followed by `as'); "they were
            equally beautiful"; "birds were singing and the child
            sang as sweetly"; "sang as sweetly as a nightingale";
            "he is every bit as mean as she is" [syn: {as}, {every
            bit}]
     2: in equal amounts or shares; in a balanced or impartial way;
        "a class evenly divided between girls and boys"; "they
        split their winnings equally"; "deal equally with rich and
        poor" [syn: {evenly}] [ant: {unevenly}, {unevenly}]
