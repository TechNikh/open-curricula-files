---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/voltmeter
offline_file: ""
offline_thumbnail: ""
uuid: bff54f88-22b6-43c7-b36b-27cd850a4e8a
updated: 1484310198
title: voltmeter
categories:
    - Dictionary
---
voltmeter
     n : meter that measures the potential difference between two
         points
