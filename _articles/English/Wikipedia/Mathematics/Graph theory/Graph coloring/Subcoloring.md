---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Subcoloring
offline_file: ""
offline_thumbnail: ""
uuid: 8ceafaa2-fa5d-485d-90e3-42c0cfb7d889
updated: 1484309387
title: Subcoloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Subcoloring_graph.svg.png
categories:
    - Graph coloring
---
In graph theory, a subcoloring is an assignment of colors to a graph's vertices such that each color class induces a vertex disjoint union of cliques. That is, each color class should form a cluster graph.
