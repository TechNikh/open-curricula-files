---
version: 1
type: video
provider: YouTube
id: xu-MRuAeYuU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4b7e5f13-5173-4b23-a2dd-f48c70139c1e
updated: 1486069630
title: Combining Letters Tutorial for Class 3 Kids
categories:
    - Class 3
thumbnail_urls:
    - https://i3.ytimg.com/vi/xu-MRuAeYuU/default.jpg
    - https://i3.ytimg.com/vi/xu-MRuAeYuU/1.jpg
    - https://i3.ytimg.com/vi/xu-MRuAeYuU/2.jpg
    - https://i3.ytimg.com/vi/xu-MRuAeYuU/3.jpg
---
