---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/combine
offline_file: ""
offline_thumbnail: ""
uuid: d5f023aa-de06-4169-ab07-aa5bc180689d
updated: 1484310373
title: combine
categories:
    - Dictionary
---
combine
     n 1: harvester that heads and threshes and cleans grain while
          moving across the field
     2: a consortium of independent organizations formed to limit
        competition by controlling the production and distribution
        of a product or service; "they set up the trust in the
        hope of gaining a monopoly" [syn: {trust}, {corporate
        trust}, {cartel}]
     3: an occurrence that results in things being united [syn: {combining}]
     v 1: put or add together; "combine resources" [syn: {compound}]
     2: have or possess in combination; "she unites charm with a
        good business sense" [syn: {unite}]
     3: combine so as to form a whole; mix; ...
