---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/noteworthy
offline_file: ""
offline_thumbnail: ""
uuid: bfba66a7-b7a9-4fe4-80b5-afdea037acae
updated: 1484310525
title: noteworthy
categories:
    - Dictionary
---
noteworthy
     adj 1: worthy of notice; "a noteworthy advance in cancer research"
            [syn: {notable}]
     2: worthy of notice; "a noteworthy fact is that her students
        rarely complain"; "a remarkable achievement" [syn: {remarkable}]
