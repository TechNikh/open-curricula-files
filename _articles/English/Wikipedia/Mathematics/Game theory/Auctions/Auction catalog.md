---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Auction_catalog
offline_file: ""
offline_thumbnail: ""
uuid: 49f34b96-f222-4ab1-ba73-040bdeb2e55f
updated: 1484309010
title: Auction catalog
categories:
    - Auctions
---
An auction catalog (US spelling) or auction catalogue (British spelling) is a catalogue that lists items to be sold at an auction. It is made available some time before the auction date. Auction catalogs for rare and expensive items, such as some art, books, jewelry, postage stamps, and antique furniture, are of interest in themselves: they will often include detailed descriptions of the items, their provenance, historical significance, photographs, and so forth.
