---
version: 1
type: video
provider: YouTube
id: stX0In3zKnI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 14fa3d84-b0e5-4319-ad85-76f57dcb9964
updated: 1486069640
title: 'Fun With Mirrors - Science Experiment- Grade 5'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/stX0In3zKnI/default.jpg
    - https://i3.ytimg.com/vi/stX0In3zKnI/1.jpg
    - https://i3.ytimg.com/vi/stX0In3zKnI/2.jpg
    - https://i3.ytimg.com/vi/stX0In3zKnI/3.jpg
---
