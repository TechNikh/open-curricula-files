---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ohm
offline_file: ""
offline_thumbnail: ""
uuid: 46b5634f-0da8-46f8-b531-74626ac07705
updated: 1484310200
title: ohm
categories:
    - Dictionary
---
ohm
     n 1: a unit of electrical resistance equal to the resistance
          between two points on a conductor when a potential
          difference of one volt between them produces a current
          of one ampere
     2: German physicist who formulated Ohm's Law (1787-1854) [syn:
        {Georg Simon Ohm}]
