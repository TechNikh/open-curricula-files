---
version: 1
type: video
provider: YouTube
id: LfxZNcDjuxA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: df770be7-fb6a-451c-914f-b2699128c20c
updated: 1486069645
title: 'Endless escape - 3D fractal zoom'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/LfxZNcDjuxA/default.jpg
    - https://i3.ytimg.com/vi/LfxZNcDjuxA/1.jpg
    - https://i3.ytimg.com/vi/LfxZNcDjuxA/2.jpg
    - https://i3.ytimg.com/vi/LfxZNcDjuxA/3.jpg
---
