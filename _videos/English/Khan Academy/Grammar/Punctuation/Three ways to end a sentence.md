---
version: 1
type: video
provider: YouTube
id: B9bJaoIHRp4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/Three%20ways%20to%20end%20a%20sentence.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 947393ac-fe99-4a26-885e-ab6bf2c37160
updated: 1486069601
title: Three ways to end a sentence
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Punctuation
thumbnail_urls:
    - https://i3.ytimg.com/vi/B9bJaoIHRp4/default.jpg
    - https://i3.ytimg.com/vi/B9bJaoIHRp4/1.jpg
    - https://i3.ytimg.com/vi/B9bJaoIHRp4/2.jpg
    - https://i3.ytimg.com/vi/B9bJaoIHRp4/3.jpg
---
David and Paige, KA’s resident grammarians, introduce the three ways to end a sentence: the period, the exclamation point, and the question mark.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/the-comma/e/three-ways-to-end-a-sentence 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/v/meet-the-comma-the-comma-punctuation-khan-academy

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
