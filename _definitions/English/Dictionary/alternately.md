---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alternately
offline_file: ""
offline_thumbnail: ""
uuid: 896c9603-e96d-4595-8685-9db97437f58e
updated: 1484310447
title: alternately
categories:
    - Dictionary
---
alternately
     adv : in an alternating sequence or position; "They were
           deglycerolized by alternately centrifuging and mixing";
           "he planted fir and pine trees alternately"
