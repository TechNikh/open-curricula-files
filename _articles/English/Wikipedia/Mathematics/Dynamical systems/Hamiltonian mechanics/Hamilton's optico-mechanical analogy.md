---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hamilton%27s_optico-mechanical_analogy'
offline_file: ""
offline_thumbnail: ""
uuid: 4a4e97a6-0fbe-4778-9105-b567ec0cd65a
updated: 1484308787
title: "Hamilton's optico-mechanical analogy"
tags:
    - "Huygens' principle"
    - "Extended Huygens' principle"
    - Classical limit of the Schrödinger equation
    - History
    - Bibliography of cited sources
categories:
    - Hamiltonian mechanics
---
Hamilton's optico-mechanical analogy is a concept of classical physics enunciated by William Rowan Hamilton.[1] It may be viewed as linking Huygens' principle of optics with Jacobi's Principle of mechanics.[2][3][4][5][6]
