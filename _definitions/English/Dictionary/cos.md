---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cos
offline_file: ""
offline_thumbnail: ""
uuid: 45e2c661-9af4-4fbe-8112-30013312d382
updated: 1484310196
title: cos
categories:
    - Dictionary
---
cos
     n 1: ratio of the adjacent side to the hypotenuse of a
          right-angled triangle [syn: {cosine}]
     2: lettuce with long dark-green leaves in a loosely packed
        elongated head [syn: {cos lettuce}, {romaine}, {romaine
        lettuce}]
