---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Debraj_Ray
offline_file: ""
offline_thumbnail: ""
uuid: 88bf9a9b-2ae1-47c0-9b6b-fa2fa06271fd
updated: 1484309029
title: Debraj Ray
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/DebrajRayPhotograph.jpg
tags:
    - Education
    - Teaching
    - Professional affiliations and awards
    - Books
    - Notes
categories:
    - Game theorists
---
Debraj Ray[1] (born 3 September 1957) is an Indian-American economist whose focus is development economics and game theory. Ray is currently Julius Silver Professor in the Faculty of Arts and Science, and Professor of Economics at New York University. He is Co-editor of the American Economic Review.
