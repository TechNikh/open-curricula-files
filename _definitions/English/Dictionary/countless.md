---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/countless
offline_file: ""
offline_thumbnail: ""
uuid: e5340b55-a9a3-4acb-a7f5-88b4e2344c0a
updated: 1484310541
title: countless
categories:
    - Dictionary
---
countless
     adj : too numerous to be counted; "incalculable riches";
           "countless hours"; "an infinite number of reasons";
           "innumerable difficulties"; "the multitudinous seas";
           "myriad stars"; "untold thousands" [syn: {infinite}, {innumerable},
            {innumerous}, {myriad(a)}, {multitudinous}, {numberless},
            {uncounted}, {unnumberable}, {unnumbered}, {unnumerable}]
