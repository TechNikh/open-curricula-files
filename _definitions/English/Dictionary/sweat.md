---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sweat
offline_file: ""
offline_thumbnail: ""
uuid: f83bcc9f-b75f-4d8e-9568-29e2b3742eea
updated: 1484310226
title: sweat
categories:
    - Dictionary
---
sweat
     n 1: salty fluid secreted by sweat glands; "sweat poured off his
          brow" [syn: {perspiration}, {sudor}]
     2: agitation resulting from active worry; "don't get in a
        stew"; "he's in a sweat about exams" [syn: {fret}, {stew},
         {lather}, {swither}]
     3: condensation of moisture on a cold surface; "the cold
        glasses were streaked with sweat"
     4: use of physical or mental energy; hard work; "he got an A
        for effort"; "they managed only with great exertion" [syn:
         {effort}, {elbow grease}, {exertion}, {travail}]
     v : excrete perspiration through the pores in the skin;
         "Exercise makes one sweat" [syn: {sudate}, ...
