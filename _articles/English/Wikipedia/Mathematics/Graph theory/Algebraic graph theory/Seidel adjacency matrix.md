---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Seidel_adjacency_matrix
offline_file: ""
offline_thumbnail: ""
uuid: b12f4078-b9d2-4c46-93e1-d4d91bffb850
updated: 1484309376
title: Seidel adjacency matrix
categories:
    - Algebraic graph theory
---
In mathematics, in graph theory, the Seidel adjacency matrix of a simple undirected graph G is a symmetric matrix with a row and column for each vertex, having 0 on the diagonal, −1 for positions whose rows and columns correspond to adjacent vertices, and +1 for positions corresponding to non-adjacent vertices. It is also called the Seidel matrix or—its original name—the (−1,1,0)-adjacency matrix. It can also be interpreted as the result of subtracting the adjacency matrix of G from the adjacency matrix of the complement graph of G
