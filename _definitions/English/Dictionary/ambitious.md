---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ambitious
offline_file: ""
offline_thumbnail: ""
uuid: 2e6e2a3d-f40e-4e45-9167-b30f12e2ec5a
updated: 1484310429
title: ambitious
categories:
    - Dictionary
---
ambitious
     adj 1: having a strong desire for success or achievement [ant: {unambitious}]
     2: requiring full use of your abilities or resources;
        "ambitious schedule"; "performed the most challenging task
        without a mistake" [syn: {challenging}]
