---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trilogy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484516041
title: trilogy
categories:
    - Dictionary
---
trilogy
     n : a set of three literary or dramatic works related in subject
         or theme
