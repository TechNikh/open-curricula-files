---
version: 1
type: video
provider: YouTube
id: PpUWpxAb39w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Surface%20integral%20ex2%20part%201-%20Parameterizing%20the%20surface.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4810f8a5-9172-44b3-819e-63063b114db2
updated: 1486069692
title: 'Surface integral ex2 part 1- Parameterizing the surface'
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/PpUWpxAb39w/default.jpg
    - https://i3.ytimg.com/vi/PpUWpxAb39w/1.jpg
    - https://i3.ytimg.com/vi/PpUWpxAb39w/2.jpg
    - https://i3.ytimg.com/vi/PpUWpxAb39w/3.jpg
---
