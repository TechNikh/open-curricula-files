---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soapy
offline_file: ""
offline_thumbnail: ""
uuid: 043518af-5e1c-4119-a250-c6fa8220f82a
updated: 1484310381
title: soapy
categories:
    - Dictionary
---
soapy
     adj : having the qualities of soap (and liable to slip away) [syn:
            {saponaceous}]
     [also: {soapiest}, {soapier}]
