---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foam
offline_file: ""
offline_thumbnail: ""
uuid: 0dd65db1-d42d-495f-85bf-2c21b358d9e4
updated: 1484310220
title: foam
categories:
    - Dictionary
---
foam
     n 1: a mass of small bubbles formed in or on a liquid [syn: {froth}]
     2: a lightweight material in cellular form; made by introducing
        gas bubbles during manufacture
     v : form bubbles; "The boiling soup was frothing"; "The river
         was foaming"; "Sparkling water" [syn: {froth}, {fizz}, {effervesce},
          {sparkle}]
