---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/industrialization
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484613001
title: industrialization
categories:
    - Dictionary
---
industrialization
     n : the development of industry on an extensive scale [syn: {industrialisation},
          {industrial enterprise}]
