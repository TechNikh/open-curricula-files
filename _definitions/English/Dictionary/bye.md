---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bye
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484493961
title: bye
categories:
    - Dictionary
---
bye
     n 1: you advance to the next round in a tournament without
          playing an opponent; "he had a bye in the first round"
          [syn: {pass}]
     2: a farewell remark; "they said their good-byes" [syn: {adieu},
         {adios}, {arrivederci}, {auf wiedersehen}, {au revoir}, {bye-bye},
         {cheerio}, {good-by}, {goodby}, {good-bye}, {goodbye}, {good
        day}, {sayonara}, {so long}]
