---
version: 1
type: video
provider: YouTube
id: 4fMipjAnlRk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Subject-verb%20agreement.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fddfab95-4e4e-4bed-b225-c0e0cc7e2fdc
updated: 1486069599
title: Subject-verb agreement
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/4fMipjAnlRk/default.jpg
    - https://i3.ytimg.com/vi/4fMipjAnlRk/1.jpg
    - https://i3.ytimg.com/vi/4fMipjAnlRk/2.jpg
    - https://i3.ytimg.com/vi/4fMipjAnlRk/3.jpg
---
Agreement is the art of making sure that sentence parts agree with one another; you want to make sure that your subjects and verbs match up.  



Practice this yourself on Khan Academy right now: https://www.khanacademy.org/syntax/e/subject-verb-agreement 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/pronoun-antecedent-agreement-syntax-khan-academy

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/syntax/v/compound-complex-sentences-syntax-khan-academy 


Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
