---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brow
offline_file: ""
offline_thumbnail: ""
uuid: 42691386-eb4a-4b66-9abd-6adb1caabc3e
updated: 1484310146
title: brow
categories:
    - Dictionary
---
brow
     n 1: the part of the face above the eyes [syn: {forehead}]
     2: the arch of hair above each eye [syn: {eyebrow}, {supercilium}]
     3: the peak of a hill; "the sun set behind the brow of distant
        hills" [syn: {hilltop}]
