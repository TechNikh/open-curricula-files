---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unfinished
offline_file: ""
offline_thumbnail: ""
uuid: ede70b44-d637-4a9e-9383-38bae2aef51a
updated: 1484310183
title: unfinished
categories:
    - Dictionary
---
unfinished
     adj 1: not brought to the desired final state [ant: {finished}]
     2: not brought to an end or conclusion; "unfinished business";
        "the building is still unfinished" [ant: {finished}]
     3: lacking a surface finish such as paint; "bare wood";
        "unfinished furniture" [syn: {bare}]
