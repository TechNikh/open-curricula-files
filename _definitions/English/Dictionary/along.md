---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/along
offline_file: ""
offline_thumbnail: ""
uuid: a0b3807a-d184-4d81-afda-762b62db5e26
updated: 1484310323
title: along
categories:
    - Dictionary
---
along
     adv 1: with a forward motion; "we drove along admiring the view";
            "the horse trotted along at a steady pace"; "the
            circus traveled on to the next city"; "move along";
            "march on" [syn: {on}]
     2: in accompaniment or as a companion; "his little sister came
        along to the movies"; "I brought my camera along";
        "working along with his father"
     3: to a more advanced state; "the work is moving along"; "well
        along in their research"; "hurrying their education
        along"; "getting along in years"
     4: in addition (usually followed by `with'); "we sent them food
        and some clothing went along in the package"; ...
