---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remark
offline_file: ""
offline_thumbnail: ""
uuid: cae857a6-34d8-4282-ab10-2998b209eb0b
updated: 1484310138
title: remark
categories:
    - Dictionary
---
remark
     n 1: a statement that expresses a personal opinion or belief;
          "from time to time she contributed a personal comment on
          his account" [syn: {comment}]
     2: explicit notice; "it passed without remark"
     v 1: make mention of; "She observed that his presentation took up
          too much time"; "They noted that it was a fine day to go
          sailing" [syn: {note}, {observe}, {mention}]
     2: make or write a comment on; "he commented the paper of his
        colleague" [syn: {comment}, {notice}, {point out}]
