---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Medical_illustration
offline_file: ""
offline_thumbnail: ""
uuid: 1259376e-9d8b-4a5b-bbb0-d0aa1c279eee
updated: 1484309329
title: Medical illustration
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Wikipedia_medical_illustration_thoracic_outlet_syndrome_brachial_plexus_anatomy_with_labels.jpg
tags:
    - History
    - Profession
    - Education
    - Technique
    - Additional images
    - Notes
categories:
    - Technical drawing
---
