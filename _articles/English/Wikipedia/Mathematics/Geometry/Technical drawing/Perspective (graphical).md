---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perspective_(graphical)
offline_file: ""
offline_thumbnail: ""
uuid: e5159d57-b2b5-4e9e-adfe-ac9d23dbe0a6
updated: 1484309329
title: Perspective (graphical)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/210px-%25C3%259Altima_Cena_-_Da_Vinci_5.jpg'
tags:
    - Overview
    - Early history
    - History
    - 'Renaissance: Mathematical basis'
    - 'Present: Computer graphics'
    - Types of perspective
    - One-point perspective
    - Two-point perspective
    - Three-point perspective
    - Four-point perspective
    - Zero-point perspective
    - Foreshortening
    - Methods of construction
    - Example
    - Limitations
    - Notes
categories:
    - Technical drawing
---
Perspective (from Latin: perspicere to see through) in the graphic arts is an approximate representation, on a flat surface (such as paper), of an image as it is seen by the eye. The two most characteristic features of perspective are that objects are smaller as their distance from the observer increases; and that they are subject to foreshortening, meaning that an object's dimensions along the line of sight are shorter than its dimensions across the line of sight.
