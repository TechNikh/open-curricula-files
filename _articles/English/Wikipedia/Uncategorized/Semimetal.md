---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Semi-metals
offline_file: ""
offline_thumbnail: ""
uuid: 22666e9f-95f7-4e46-99da-3e0e661e799b
updated: 1484308384
title: Semimetal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Band_filling_diagram.svg.png
tags:
    - Temperature dependency
    - Classification
    - Schematic
    - Physical properties
    - Classic semimetals
categories:
    - Uncategorized
---
A semimetal is a material with a very small overlap between the bottom of the conduction band and the top of the valence band. According to electronic band theory, solids can be classified as insulators, semiconductors, semimetals, or metals. In insulators and semiconductors the filled valence band is separated from an empty conduction band by a band gap. For insulators, the magnitude of the band gap is larger (e.g. > 4 eV) than that of a semiconductor (e.g. < 4 eV). Metals have a partially filled conduction band. A semimetal is a material with a very small overlap between the bottom of the conduction band and the top of the valence band. A semimetal thus has no band gap and a negligible ...
