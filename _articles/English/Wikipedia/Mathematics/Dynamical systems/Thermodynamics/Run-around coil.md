---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Run-around_coil
offline_file: ""
offline_thumbnail: ""
uuid: 49bee24b-44f6-41eb-9501-3cd3de8f2565
updated: 1484308860
title: Run-around coil
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-HVAC_run_around_coil.jpg
tags:
    - Description
    - Energy transfer process
    - Other types of air-to-air heat exchangers
categories:
    - Thermodynamics
---
A run-around coil is a type of energy recovery heat exchanger most often positioned within the supply and exhaust air streams of an air handling system, or in the exhaust gases of an industrial process, to recover the heat energy. Generally, it refers to any intermediate stream used to transfer heat between two streams that are not directly connected for reasons of safety or practicality. It may also be referred to as a run-around loop, a pump-around coil or a liquid coupled heat exchanger.[1]
