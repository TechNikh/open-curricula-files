---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Distributed_algorithmic_mechanism_design
offline_file: ""
offline_thumbnail: ""
uuid: e5d61dfe-4a46-4ee9-84ce-36dba10a9459
updated: 1484309045
title: Distributed algorithmic mechanism design
tags:
    - Game Theoretic Model
    - Nash equilibrium
    - Solution Preference
    - Truthfulness
    - Classic distributed computing problems
    - >
        Leader Election(Completely connected network, synchronous
        case)
categories:
    - Mechanism design
---
DAMD differs from Algorithmic mechanism design since the algorithm is computed in a distributed manner rather than by a central authority. This greatly improves computation time since the burden is shared by all agents within a network
