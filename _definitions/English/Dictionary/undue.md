---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undue
offline_file: ""
offline_thumbnail: ""
uuid: 0421c1b7-3351-4189-9356-f4443dc353e0
updated: 1484310531
title: undue
categories:
    - Dictionary
---
undue
     adj 1: not yet payable; "an undue loan" [syn: {not due}] [ant: {due}]
     2: not appropriate or proper (or even legal) in the
        circumstances; "undue influence"; "I didn't want to show
        undue excitement"; "accused of using undue force" [ant: {due}]
     3: lacking justification or authorization; "unreasonable
        searches and seizures"; "desire for undue private profit";
        "unwarranted limitations of personal freedom" [syn: {unjustified},
         {unwarranted}]
     4: beyond normal limits; "excessive charges"; "a book of
        inordinate length"; "his dress stops just short of undue
        elegance"; "unreasonable demands" [syn: {excessive}, ...
