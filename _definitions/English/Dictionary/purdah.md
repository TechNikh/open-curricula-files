---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purdah
offline_file: ""
offline_thumbnail: ""
uuid: 87ec8e97-e205-4a8f-9811-97e8c70eea91
updated: 1484310601
title: purdah
categories:
    - Dictionary
---
purdah
     n 1: a state of social isolation [syn: {solitude}]
     2: the traditional Hindu or Muslim system of keeping women
        secluded [syn: {sex segregation}]
     3: a screen used in India to separate women from men or
        strangers
