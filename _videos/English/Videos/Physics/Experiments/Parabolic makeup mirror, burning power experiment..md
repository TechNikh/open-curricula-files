---
version: 1
type: video
provider: YouTube
id: ouDfY_15MfI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7dd3b8e6-31b4-4312-a334-ed18cdcdb21f
updated: 1486069637
title: Parabolic makeup mirror, burning power experiment.
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/ouDfY_15MfI/default.jpg
    - https://i3.ytimg.com/vi/ouDfY_15MfI/1.jpg
    - https://i3.ytimg.com/vi/ouDfY_15MfI/2.jpg
    - https://i3.ytimg.com/vi/ouDfY_15MfI/3.jpg
---
