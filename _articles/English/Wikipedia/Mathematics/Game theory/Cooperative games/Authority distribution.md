---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Authority_distribution
offline_file: ""
offline_thumbnail: ""
uuid: ace42be0-dd44-438c-8d89-32e803337916
updated: 1484308533
title: Authority distribution
tags:
    - Definition
    - Applications
categories:
    - Cooperative games
---
The solution concept authority distribution was formulated by Lloyd Shapley and his student X. Hu in 2003 to measure the authority power of players in a well-contracted organization.[1] The index generates the Shapley-Shubik power index and can be used in ranking, planning and organizational choice.
