---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bucket
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484331121
title: bucket
categories:
    - Dictionary
---
bucket
     n 1: a roughly cylindrical that is vessel open at the top [syn: {pail}]
     2: the quantity contained in a bucket [syn: {bucketful}]
     v 1: put into a bucket
     2: carry in a bucket
