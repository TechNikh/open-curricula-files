---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-confirming_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 71a2b5d0-af25-4803-beba-1bdceba3dc1f
updated: 1484309032
title: Self-confirming equilibrium
categories:
    - Game theory equilibrium concepts
---
In game theory, self-confirming equilibrium is a generalization of Nash equilibrium for extensive form games, in which players correctly predict the moves their opponents actually make, but may have misconceptions about what their opponents would do at information sets that are never reached when the equilibrium is played. Informally, self-confirming equilibrium is motivated by the idea that if a game is played repeatedly, the players will revise their beliefs about their opponents' play if and only if they observe these beliefs to be wrong.
