---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Binary_erasure_channel
offline_file: ""
offline_thumbnail: ""
uuid: 89fc5c63-ffa4-4915-87a6-c21e63ae7f5e
updated: 1484308544
title: Binary erasure channel
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Binaryerasurechannel.png
tags:
    - Description
    - Definition
    - Capacity of the BEC
    - Deletion channel
categories:
    - Coding theory
---
A binary erasure channel (or BEC) is a common communications channel model used in coding theory and information theory. In this model, a transmitter sends a bit (a zero or a one), and the receiver either receives the bit or it receives a message that the bit was not received ("erased"). This channel is used frequently in information theory because it is one of the simplest channels to analyze. The BEC was introduced by Peter Elias of MIT in 1955 as a toy example.
