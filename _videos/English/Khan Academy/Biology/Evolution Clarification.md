---
version: 1
type: video
provider: YouTube
id: nh1R-gyY7es
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Evolution%20Clarification.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c1958e89-c4c6-4a71-b423-09d3efa91db9
updated: 1486069583
title: Evolution Clarification
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/nh1R-gyY7es/default.jpg
    - https://i3.ytimg.com/vi/nh1R-gyY7es/1.jpg
    - https://i3.ytimg.com/vi/nh1R-gyY7es/2.jpg
    - https://i3.ytimg.com/vi/nh1R-gyY7es/3.jpg
---
Clarifying some points on evolution and intelligent design
More free lessons at: http://www.khanacademy.org/video?v=nh1R-gyY7es
