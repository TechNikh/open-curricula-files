---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/courtesy
offline_file: ""
offline_thumbnail: ""
uuid: 48da06fd-d1f1-4def-862a-c965f6e5c8e6
updated: 1484310234
title: courtesy
categories:
    - Dictionary
---
courtesy
     n 1: a courteous or respectful or considerate act
     2: a courteous or respectful or considerate remark
     3: a courteous manner [syn: {good manners}] [ant: {discourtesy}]
