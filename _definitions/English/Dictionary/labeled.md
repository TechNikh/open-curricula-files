---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/labeled
offline_file: ""
offline_thumbnail: ""
uuid: e610310e-5463-40bb-bda3-8bdfb35cd203
updated: 1484310163
title: labeled
categories:
    - Dictionary
---
labeled
     adj : bearing or marked with a label or tag; "properly labeled
           luggage" [syn: {labelled}, {tagged}] [ant: {unlabeled}]
