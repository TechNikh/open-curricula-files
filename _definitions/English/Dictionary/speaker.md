---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/speaker
offline_file: ""
offline_thumbnail: ""
uuid: 0180880a-d002-484e-832f-2f2287ec82ca
updated: 1484310563
title: speaker
categories:
    - Dictionary
---
speaker
     n 1: someone who expresses in language; someone who talks
          (especially someone who delivers a public speech or
          someone especially garrulous); "the speaker at
          commencement"; "an utterer of useful maxims" [syn: {talker},
           {utterer}, {verbalizer}, {verbaliser}]
     2: electro-acoustic transducer that converts electrical signals
        into sounds loud enough to be heard at a distance [syn: {loudspeaker},
         {speaker unit}, {loudspeaker system}, {speaker system}]
     3: the presiding officer of a deliberative assembly; "the
        leader of the majority party is the Speaker of the House
        of Representatives"
