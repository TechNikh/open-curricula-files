---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trigonometric_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: 298c4bb1-9117-4bc0-a733-87ea57da78ec
updated: 1484309348
title: Trigonometric polynomial
categories:
    - Trigonometry
---
In the mathematical subfields of numerical analysis and mathematical analysis, a trigonometric polynomial is a finite linear combination of functions sin(nx) and cos(nx) with n taking on the values of one or more natural numbers. The coefficients may be taken as real numbers, for real-valued functions. For complex coefficients, there is no difference between such a function and a finite Fourier series.
