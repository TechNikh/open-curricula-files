---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slot
offline_file: ""
offline_thumbnail: ""
uuid: 6ea43364-3af6-490c-91c8-9c17991516f8
updated: 1484310395
title: slot
categories:
    - Dictionary
---
slot
     n 1: a position in a grammatical linguistic construction in which
          a variety of alternative units are interchangeable; "he
          developed a version of slot grammar"
     2: a small slit (as for inserting a coin or depositing mail);
        "he put a quarter in the slot"
     3: a time assigned on a schedule or agenda; "the TV program has
        a new time slot"; "an aircraft landing slot" [syn: {time
        slot}]
     4: a position in a hierarchy or organization; "Bob Dylan
        occupied the top slot for several weeks"; "she beat some
        tough competition for the number one slot"
     5: the trail of an animal (especially a deer); "he followed the
        ...
