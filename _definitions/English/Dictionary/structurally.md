---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/structurally
offline_file: ""
offline_thumbnail: ""
uuid: e128a9c0-eaea-4365-9d2f-3e85f3dde9a1
updated: 1484310283
title: structurally
categories:
    - Dictionary
---
structurally
     adv : with respect to structure; "structurally, the organization
           is healthy"
