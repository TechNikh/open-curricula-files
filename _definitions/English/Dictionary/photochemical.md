---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/photochemical
offline_file: ""
offline_thumbnail: ""
uuid: 0aa2b721-a42b-4fad-bfcf-1f1c8ab321c8
updated: 1484310377
title: photochemical
categories:
    - Dictionary
---
photochemical
     adj : of or relating to or produced by the effects of light on
           chemical systems
