---
version: 1
type: video
provider: YouTube
id: _KyyVhlUDNU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Oxidation%20and%20reduction%20in%20cellular%20respiration.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b20de820-0c4a-4c66-8d04-395cf269d740
updated: 1486069583
title: Oxidation and reduction in cellular respiration
tags:
    - dotsub
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/_KyyVhlUDNU/default.jpg
    - https://i3.ytimg.com/vi/_KyyVhlUDNU/1.jpg
    - https://i3.ytimg.com/vi/_KyyVhlUDNU/2.jpg
    - https://i3.ytimg.com/vi/_KyyVhlUDNU/3.jpg
---
Oxidation and reduction in cellular respiration. Reconciling the biology and chemistry definitions of oxidation and reduction.

Watch the next lesson: https://www.khanacademy.org/science/biology/cellular-respiration-and-fermentation/overview-of-cellular-respiration-steps/v/overview-of-cellular-respiration?utm_source=YT&utm_medium=Desc&utm_campaign=biology

Missed the previous lesson? https://www.khanacademy.org/science/biology/cellular-respiration-and-fermentation/intro-to-cellular-respiration/v/oxidation-and-reduction-from-biological-view?utm_source=YT&utm_medium=Desc&utm_campaign=biology

Biology on Khan Academy: Life is beautiful! From atoms to cells, from genes to proteins, from populations to ecosystems, biology is the study of the fascinating and intricate systems that make life possible. Dive in to learn more about the many branches of biology and why they are exciting and important. Covers topics seen in a high school or first-year college biology course.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy's Biology channel: https://www.youtube.com/channel/UC82qE46vcTn7lP4tK_RHhdg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
