---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Derek_Corneil
offline_file: ""
offline_thumbnail: ""
uuid: 379d52ae-384a-4931-ba4d-82243b8218d7
updated: 1484309461
title: Derek Corneil
tags:
    - life
    - Work
    - Awards
categories:
    - Graph theorists
---
Derek Gordon Corneil is a Canadian mathematician and computer scientist, a professor emeritus of Computer Science at the University of Toronto, and an expert in graph algorithms and Graph Theory.
