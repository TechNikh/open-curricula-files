---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maya
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484537701
title: maya
categories:
    - Dictionary
---
Maya
     n 1: a member of an American Indian people of Yucatan and Belize
          and Guatemala who had a culture (which reached its peak
          between AD 300 and 900) characterized by outstanding
          architecture and pottery and astronomy; "Mayans had a
          system of writing and an accurate calendar" [syn: {Mayan}]
     2: an ethnic minority speaking Mayan languages and living in
        Yucatan and adjacent areas
     3: a family of American Indian languages spoken by Mayan
        peoples [syn: {Mayan}, {Mayan language}]
