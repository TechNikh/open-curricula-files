---
version: 1
type: video
provider: YouTube
id: ry1hpm1GXVI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Binary%20Numbers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0df94ccd-2a3a-4f8e-a2f9-5a3eeb520c86
updated: 1486069567
title: Binary Numbers
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/ry1hpm1GXVI/default.jpg
    - https://i3.ytimg.com/vi/ry1hpm1GXVI/1.jpg
    - https://i3.ytimg.com/vi/ry1hpm1GXVI/2.jpg
    - https://i3.ytimg.com/vi/ry1hpm1GXVI/3.jpg
---
Understanding how numbers are represented.  Introduction to binary numbers
