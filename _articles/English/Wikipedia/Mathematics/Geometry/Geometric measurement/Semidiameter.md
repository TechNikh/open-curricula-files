---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Semidiameter
offline_file: ""
offline_thumbnail: ""
uuid: 7153f3f6-7f78-4981-9a7a-76d2e18b31d2
updated: 1484309244
title: Semidiameter
categories:
    - Geometric measurement
---
In geometry, the semidiameter or semi-diameter of a set of points may be one half of its diameter; or, sometimes, one half of its extent along a particular direction.
