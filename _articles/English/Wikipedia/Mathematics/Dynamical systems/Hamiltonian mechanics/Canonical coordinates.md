---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Canonical_coordinates
offline_file: ""
offline_thumbnail: ""
uuid: 79b4bd33-7f7a-4aac-b428-076dc079da14
updated: 1484308786
title: Canonical coordinates
tags:
    - Definition, in classical mechanics
    - Definition, on cotangent bundles
    - Formal development
    - Generalized coordinates
categories:
    - Hamiltonian mechanics
---
In mathematics and classical mechanics, canonical coordinates are sets of coordinates which can be used to describe a physical system at any given point in time (locating the system within phase space). Canonical coordinates are used in the Hamiltonian formulation of classical mechanics. A closely related concept also appears in quantum mechanics; see the Stone–von Neumann theorem and canonical commutation relations for details.
