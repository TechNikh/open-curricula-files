---
version: 1
type: video
provider: YouTube
id: 2gT1eAcK7T8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/mRNA%20Splicing%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f0f2005b-9d2e-49cd-a589-77c5b63a3106
updated: 1486069668
title: mRNA Splicing 3
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/2gT1eAcK7T8/default.jpg
    - https://i3.ytimg.com/vi/2gT1eAcK7T8/1.jpg
    - https://i3.ytimg.com/vi/2gT1eAcK7T8/2.jpg
    - https://i3.ytimg.com/vi/2gT1eAcK7T8/3.jpg
---
