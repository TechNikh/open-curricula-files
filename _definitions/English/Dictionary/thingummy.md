---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thingummy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484514361
title: thingummy
categories:
    - Dictionary
---
thingummy
     n : something whose name is either forgotten or not known [syn:
         {dohickey}, {dojigger}, {doodad}, {doohickey}, {gimmick},
          {hickey}, {gizmo}, {gismo}, {gubbins}, {thingamabob}, {thingumabob},
          {thingmabob}, {thingamajig}, {thingumajig}, {thingmajig}]
