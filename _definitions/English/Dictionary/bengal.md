---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bengal
offline_file: ""
offline_thumbnail: ""
uuid: 62342fac-42b1-4830-aa71-d904b86a30f7
updated: 1484310437
title: bengal
categories:
    - Dictionary
---
Bengal
     n : a region whose eastern part is now Bangladesh and whose
         western part is included in India
