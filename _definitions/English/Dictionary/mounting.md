---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mounting
offline_file: ""
offline_thumbnail: ""
uuid: f1c91b17-d77b-4295-a1cd-780b80999c84
updated: 1484310593
title: mounting
categories:
    - Dictionary
---
mounting
     n 1: an event that involves rising to a higher point (as in
          altitude or temperature or intensity etc.) [syn: {climb},
           {climbing}]
     2: framework used for support or display
