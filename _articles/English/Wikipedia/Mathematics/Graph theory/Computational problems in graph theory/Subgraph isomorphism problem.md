---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Subgraph_isomorphism_problem
offline_file: ""
offline_thumbnail: ""
uuid: 3f722bb5-6a7e-4f05-b014-6a44d8ef9c34
updated: 1484309398
title: Subgraph isomorphism problem
tags:
    - Decision problem and computational complexity
    - Algorithms
    - Applications
    - Notes
categories:
    - Computational problems in graph theory
---
In theoretical computer science, the subgraph isomorphism problem is a computational task in which two graphs G and H are given as input, and one must determine whether G contains a subgraph that is isomorphic to H. Subgraph isomorphism is a generalization of both the maximum clique problem and the problem of testing whether a graph contains a Hamiltonian cycle, and is therefore NP-complete.[1] However certain other cases of subgraph isomorphism may be solved in polynomial time.[2]
