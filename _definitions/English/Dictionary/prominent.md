---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prominent
offline_file: ""
offline_thumbnail: ""
uuid: c22fc116-85cf-4101-ab97-e987d7274776
updated: 1484310433
title: prominent
categories:
    - Dictionary
---
prominent
     adj 1: having a quality that thrusts itself into attention; "an
            outstanding fact of our time is that nations poisoned
            by anti semitism proved less fortunate in regard to
            their own freedom"; "a new theory is the most
            prominent feature of the book"; "salient traits"; "a
            spectacular rise in prices"; "a striking thing about
            Picadilly Circus is the statue of Eros in the center";
            "a striking resemblance between parent and child"
            [syn: {outstanding}, {salient}, {spectacular}, {striking}]
     2: conspicuous in position or importance; "a big figure in the
        movement"; "big man on ...
