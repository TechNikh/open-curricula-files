---
version: 1
type: video
provider: YouTube
id: mCzchPx3yF8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 715c0daf-857d-4ff0-92ee-ab530f004668
updated: 1486069684
title: 'The Moon- Crash Course Astronomy #12'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/mCzchPx3yF8/default.jpg
    - https://i3.ytimg.com/vi/mCzchPx3yF8/1.jpg
    - https://i3.ytimg.com/vi/mCzchPx3yF8/2.jpg
    - https://i3.ytimg.com/vi/mCzchPx3yF8/3.jpg
---
