---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-pollinating
offline_file: ""
offline_thumbnail: ""
uuid: ba7428ca-80b2-4f3c-a600-89a2c0f34a3b
updated: 1484310299
title: self-pollinating
categories:
    - Dictionary
---
self-pollinating
     adj : of or relating to or characteristic of self-pollination
