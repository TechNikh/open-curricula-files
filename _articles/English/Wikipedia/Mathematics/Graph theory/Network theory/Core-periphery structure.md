---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Core-periphery_structure
offline_file: ""
offline_thumbnail: ""
uuid: 76940eaa-b861-42a3-9a56-26fa2cd054bd
updated: 1484309434
title: Core-periphery structure
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Core-Periphery_Network.png
tags:
    - Models of Core-Periphery Structures
    - Discrete model
    - Continuous model
    - Discussion
    - Uses in Economics
categories:
    - Network theory
---
Core-periphery structures are commonly found in economic and social networks. They consist of a dense cohesive core and a sparse, loosely connected periphery. (Zhang, Martin, & Newman, n.d.) Networks can be described from various macro, micro and meso scales. Identifying these structures allows for the comparison between complex structures. (Rombach, Porter, Fowler, & Mucha, 2014)
