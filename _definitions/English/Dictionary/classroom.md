---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/classroom
offline_file: ""
offline_thumbnail: ""
uuid: d071c831-5f0d-44c9-9e4d-41e17ba5e38c
updated: 1484310522
title: classroom
categories:
    - Dictionary
---
classroom
     n : a room in a school where lessons take place [syn: {schoolroom}]
