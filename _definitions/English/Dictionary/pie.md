---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pie
offline_file: ""
offline_thumbnail: ""
uuid: b188d1f4-f596-47d0-9940-4a2f50805049
updated: 1484310453
title: pie
categories:
    - Dictionary
---
pie
     n 1: dish baked in pastry-lined pan often with a pastry top
     2: a prehistoric unrecorded language that was the ancestor of
        all Indo-European languages [syn: {Proto-Indo European}]
