---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spawn
offline_file: ""
offline_thumbnail: ""
uuid: 5b70799d-739f-4cc7-ab02-46444640ba62
updated: 1484310158
title: spawn
categories:
    - Dictionary
---
spawn
     n : the mass of eggs deposited by fish or amphibians or molluscs
     v 1: call forth [syn: {engender}, {breed}]
     2: lay spawn; "The salmon swims upstream to spawn"
