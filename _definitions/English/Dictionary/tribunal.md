---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tribunal
offline_file: ""
offline_thumbnail: ""
uuid: afe934cd-6bd4-4d93-9565-9e069b1299b8
updated: 1484310569
title: tribunal
categories:
    - Dictionary
---
tribunal
     n : an assembly (including one or more judges) to conduct
         judicial business [syn: {court}, {judicature}]
