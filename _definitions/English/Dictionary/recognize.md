---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recognize
offline_file: ""
offline_thumbnail: ""
uuid: 19955fbe-9c79-4aea-8fb1-9d17d292fb9c
updated: 1484310351
title: recognize
categories:
    - Dictionary
---
recognize
     v 1: accept (someone) to be what is claimed or accept his power
          and authority; "The Crown Prince was acknowledged as the
          true heir to the throne"; "We do not recognize your
          gods" [syn: {acknowledge}, {recognise}, {know}]
     2: be fully aware or cognizant of [syn: {recognise}, {realize},
         {realise}, {agnize}, {agnise}]
     3: detect with the senses; "The fleeing convicts were picked
        out of the darkness by the watchful prison guards"; "I
        can't make out the faces in this photograph" [syn: {recognise},
         {distinguish}, {discern}, {pick out}, {make out}, {tell
        apart}]
     4: perceive to be the same [syn: ...
