---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ellis%E2%80%93Numakura_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 0ecf09d9-cf72-4b81-8747-d9d85ece8d0d
updated: 1484308775
title: Ellis–Numakura lemma
tags:
    - Applications
    - Proof
categories:
    - Ergodic theory
---
In mathematics, the Ellis–Numakura lemma states that if S is a non-empty semigroup with a topology such that S is compact and the product is semi-continuous, then S has an idempotent element p, (that is, with pp = p). The lemma is named after Robert Ellis and Katsui Numakura.
