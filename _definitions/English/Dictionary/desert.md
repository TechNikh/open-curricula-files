---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/desert
offline_file: ""
offline_thumbnail: ""
uuid: 19f55c27-db82-4f70-b910-6de54789559e
updated: 1484310433
title: desert
categories:
    - Dictionary
---
desert
     adj : located in a dismal or remote area; desolate; "a desert
           island"; "a godforsaken wilderness crossroads"; "a wild
           stretch of land"; "waste places" [syn: {godforsaken}, {waste},
            {wild}]
     n : an arid region with little or no vegetation
     v 1: leave someone who needs or counts on you; leave in the
          lurch; "The mother deserted her children" [syn: {abandon},
           {forsake}, {desolate}]
     2: desert (a cause, a country or an army), often in order to
        join the opposing cause, country, or army; "If soldiers
        deserted Hitler's army, they were shot" [syn: {defect}]
