---
version: 1
type: video
provider: YouTube
id: 5mD4cW-71sQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f8f67b7d-52d6-404a-adfd-85005d05127d
updated: 1486069666
title: 'Stomach Ulcer Animation - Peptic Ulcer Disease Causes, Symptoms, Treatment - Gastric Anatomy'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/5mD4cW-71sQ/default.jpg
    - https://i3.ytimg.com/vi/5mD4cW-71sQ/1.jpg
    - https://i3.ytimg.com/vi/5mD4cW-71sQ/2.jpg
    - https://i3.ytimg.com/vi/5mD4cW-71sQ/3.jpg
---
