---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/celluloid
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484559901
title: celluloid
categories:
    - Dictionary
---
celluloid
     adj : artificial as if portrayed in a film; "a novel with flat
           celluloid characters" [syn: {synthetic}]
     n 1: highly flammable substance made from cellulose nitrate and
          camphor; used in e.g. motion-picture and X-ray film; its
          use has decreased with the development of nonflammable
          thermoplastics
     2: a medium that disseminates moving pictures; "theater pieces
        transferred to celluloid"; "this story would be good
        cinema"; "film coverage of sporting events" [syn: {film},
        {cinema}]
