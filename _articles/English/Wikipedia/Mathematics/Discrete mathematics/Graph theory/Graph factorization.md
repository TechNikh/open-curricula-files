---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_factorization
offline_file: ""
offline_thumbnail: ""
uuid: c9e1de27-02dd-40cc-869b-042609f75e47
updated: 1484308619
title: Graph factorization
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Desargues_graph_3color_edge.svg.png
tags:
    - 1-factorization
    - Complete graphs
    - 1-factorization conjecture
    - Perfect 1-factorization
    - 2-factorization
    - Notes
categories:
    - Graph theory
---
In graph theory, a factor of a graph G is a spanning subgraph, i.e., a subgraph that has the same vertex set as G. A k-factor of a graph is a spanning k-regular subgraph, and a k-factorization partitions the edges of the graph into disjoint k-factors. A graph G is said to be k-factorable if it admits a k-factorization. In particular, a 1-factor is a perfect matching, and a 1-factorization of a k-regular graph is an edge coloring with k colors. A 2-factor is a collection of cycles that spans all vertices of the graph.
