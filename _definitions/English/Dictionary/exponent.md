---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exponent
offline_file: ""
offline_thumbnail: ""
uuid: d6ff6cb2-c514-4c76-9983-f9de4ac24553
updated: 1484310599
title: exponent
categories:
    - Dictionary
---
exponent
     n 1: a person who pleads for a cause or propounds an idea [syn: {advocate},
           {advocator}, {proponent}]
     2: someone who expounds and interprets or explains
     3: a mathematical notation indicating the number of times a
        quantity is multiplied by itself [syn: {power}, {index}]
