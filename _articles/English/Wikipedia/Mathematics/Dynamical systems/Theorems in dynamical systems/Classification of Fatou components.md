---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Classification_of_Fatou_components
offline_file: ""
offline_thumbnail: ""
uuid: 0cbf97fb-bb20-49ae-9a0b-1a7a73a4caee
updated: 1484308692
title: Classification of Fatou components
tags:
    - Rational case
    - Examples
    - Attracting periodic point
    - Herman ring
    - Transcendental case
    - Baker domain
    - Wandering domain
categories:
    - Theorems in dynamical systems
---
In mathematics, Fatou components are components of the Fatou set.
