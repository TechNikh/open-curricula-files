---
version: 1
type: article
id: https://en.wikipedia.org/wiki/E-function
offline_file: ""
offline_thumbnail: ""
uuid: c5e6a1dc-4b62-455f-bf1e-d459d5d3b2f0
updated: 1484308636
title: E-function
tags:
    - Definition
    - Uses
    - The Siegel–Shidlovsky theorem
    - Examples
categories:
    - Number theory
---
In mathematics, E-functions are a type of power series that satisfy particular arithmetic conditions on the coefficients. They are of interest in transcendental number theory, and are more special than G-functions.
