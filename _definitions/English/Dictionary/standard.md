---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/standard
offline_file: ""
offline_thumbnail: ""
uuid: 26ef6872-d86a-4bdb-ac8e-e7d491d4b541
updated: 1484310373
title: standard
categories:
    - Dictionary
---
standard
     adj 1: conforming to or constituting a standard of measurement or
            value; or of the usual or regularized or accepted
            kind; "windows of standard width"; "standard sizes";
            "the standard fixtures"; "standard brands"; "standard
            operating procedure" [ant: {nonstandard}]
     2: commonly used or supplied; "standard procedure"; "standard
        car equipment"
     3: established or widely recognized as a model of authority or
        excellence; "a standard reference work" [ant: {nonstandard}]
     4: conforming to the established language usage of educated
        native speakers; "standard English" (American); "received
        ...
