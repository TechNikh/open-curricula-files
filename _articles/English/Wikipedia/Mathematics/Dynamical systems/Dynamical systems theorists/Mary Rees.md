---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mary_Rees
offline_file: ""
offline_thumbnail: ""
uuid: c461d793-4e98-4589-8e92-a754ad89c7e9
updated: 1484308688
title: Mary Rees
tags:
    - FRS
    - family
    - Works
categories:
    - Dynamical systems theorists
---
Susan Mary Rees, FRS (born Cambridge, 31 July 1953[1]) is a British mathematician and a Professor of Mathematics at Liverpool University since 2002, specialising in research in complex dynamical systems.[2][3]
