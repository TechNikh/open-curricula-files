---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Functional_unit
offline_file: ""
offline_thumbnail: ""
uuid: 1333ca98-3c1f-4496-9492-ae5c6423c2a6
updated: 1484308358
title: Execution unit
categories:
    - Uncategorized
---
In computer engineering, an execution unit (also called a functional unit) is a part of the central processing unit (CPU) that performs the operations and calculations as instructed by the computer program. It may have its own internal control sequence unit, which is not to be confused with the CPU's main control unit, some registers, and other internal units such as an arithmetic logic unit (ALU) or a floating-point unit (FPU), or some smaller and more specific components.[1]
