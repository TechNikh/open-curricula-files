---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Horrocks%E2%80%93Mumford_bundle'
offline_file: ""
offline_thumbnail: ""
uuid: 4ae67820-0d40-4238-9900-51cdd8cf3117
updated: 1484309363
title: Horrocks–Mumford bundle
categories:
    - Geometry stubs
---
In algebraic geometry, the Horrocks–Mumford bundle is an indecomposable rank 2 vector bundle on 4-dimensional projective space P4 introduced by Geoffrey Horrocks and David Mumford (1973). It is the only such bundle known, although a generalized construction involving Paley graphs produces other rank 2 sheaves (Sasukara et al. 1993). The zero sets of sections of the Horrocks–Mumford bundle are abelian surfaces of degree 10, called Horrocks–Mumford surfaces.
