---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spraying
offline_file: ""
offline_thumbnail: ""
uuid: 2b277ba5-fcd5-4aff-8978-977faeee2fc1
updated: 1484310539
title: spraying
categories:
    - Dictionary
---
spraying
     n 1: the dispersion of fungicides or insecticides or fertilizer
          on growing crops (often from a low-flying aircraft)
          [syn: {crop-dusting}]
     2: a quantity of small objects flying through the air; "a spray
        of bullets" [syn: {spray}]
     3: the application of a liquid in the form of small particles
        ejected from a sprayer
