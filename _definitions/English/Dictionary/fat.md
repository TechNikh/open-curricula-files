---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fat
offline_file: ""
offline_thumbnail: ""
uuid: d47bbdf5-94ec-44e0-9c7c-1ca42727cb36
updated: 1484310377
title: fat
categories:
    - Dictionary
---
fat
     adj 1: having much flesh (especially fat); "he hadn't remembered
            how fat she was" [ant: {thin}]
     2: having a relatively large diameter; "a fat rope"
     3: containing or composed of fat; "fatty food"; "fat tissue"
        [syn: {fatty}] [ant: {nonfat}]
     4: lucrative; "a juicy contract"; "a nice fat job" [syn: {juicy}]
     5: marked by great fruitfulness; "fertile farmland"; "a fat
        land"; "a productive vineyard"; "rich soil" [syn: {fertile},
         {productive}, {rich}]
     6: a chubby body; "the boy had a rounded face and fat cheeks"
        [syn: {rounded}]
     n 1: a soft greasy substance occurring in organic tissue and
          consisting of a ...
