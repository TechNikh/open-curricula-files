---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Covalency
offline_file: ""
offline_thumbnail: ""
uuid: 13e482b8-5bef-49e2-af18-9e359c9d1159
updated: 1484308386
title: Covalent bond
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Covalent_bond_hydrogen.svg.png
tags:
    - History
    - Types of covalent bonds
    - Covalent structures
    - 'One- and three-electron bonds'
    - Resonance
    - Aromaticity
    - Hypervalence
    - Electron-deficiency
    - Quantum mechanical description
    - >
        Covalency from atomic contribution to the electronic density
        of states
    - Sources
categories:
    - Uncategorized
---
A covalent bond, also called a molecular bond, is a chemical bond that involves the sharing of electron pairs between atoms. These electron pairs are known as shared pairs or bonding pairs, and the stable balance of attractive and repulsive forces between atoms, when they share electrons, is known as covalent bonding.[1][better source needed] For many molecules, the sharing of electrons allows each atom to attain the equivalent of a full outer shell, corresponding to a stable electronic configuration.
