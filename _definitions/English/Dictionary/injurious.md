---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/injurious
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484612881
title: injurious
categories:
    - Dictionary
---
injurious
     adj 1: harmful to living things; "deleterious chemical additives"
            [syn: {deleterious}, {hurtful}]
     2: tending to cause great harm [syn: {evil}, {harmful}]
