---
version: 1
type: article
id: https://en.wikipedia.org/wiki/DIMACS
offline_file: ""
offline_thumbnail: ""
uuid: b55ef3c1-c1db-47dc-a421-213c7937fe46
updated: 1484308567
title: DIMACS
categories:
    - Combinatorics
---
The Center for Discrete Mathematics and Theoretical Computer Science (DIMACS) is a collaboration between Rutgers University, Princeton University, and the research firms AT&T, Bell Labs, Applied Communication Sciences, and NEC. It was founded in 1989 with money from the National Science Foundation. Its offices are located on the Rutgers campus, and 250 members from the six institutions form its permanent members.
