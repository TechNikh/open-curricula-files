---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dose
offline_file: ""
offline_thumbnail: ""
uuid: a7c60f3d-172f-446d-b973-8b8905b97e56
updated: 1484310424
title: dose
categories:
    - Dictionary
---
dose
     n 1: a measured portion of medicine taken at any one time
     2: the quantity of an active agent (substance or radiation)
        taken in or absorbed at any one time [syn: {dosage}]
     3: street name for lysergic acid diethylamide [syn: {acid}, {back
        breaker}, {battery-acid}, {dot}, {Elvis}, {loony toons}, {Lucy
        in the sky with diamonds}, {pane}, {superman}, {window
        pane}, {Zen}]
     v 1: treat with an agent; add (an agent) to; "The ray dosed the
          paint"
     2: administer a drug to; "They drugged the kidnapped tourist"
        [syn: {drug}]
