---
version: 1
type: video
provider: YouTube
id: b3RaBB7IDZc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Intro%20to%20adverbs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5cfd8b82-dcda-4c06-aed0-d684dec42f6e
updated: 1486069601
title: Intro to adverbs
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/b3RaBB7IDZc/default.jpg
    - https://i3.ytimg.com/vi/b3RaBB7IDZc/1.jpg
    - https://i3.ytimg.com/vi/b3RaBB7IDZc/2.jpg
    - https://i3.ytimg.com/vi/b3RaBB7IDZc/3.jpg
---
Adverbs are a kind of modifier that you can use to change verbs or adjectives, like 'very' or 'carefully'. 

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-modifier/e/meet-the-adverb?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-modifier/v/adjective-order?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/the-modifier/v/definite-and-indefinite-articles?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
