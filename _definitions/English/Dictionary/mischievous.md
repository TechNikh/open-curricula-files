---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mischievous
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484535121
title: mischievous
categories:
    - Dictionary
---
mischievous
     adj 1: naughtily or annoyingly playful; "teasing and worrying with
            impish laughter" [syn: {impish}, {implike}, {pixilated},
             {prankish}, {puckish}]
     2: badly behaved; "he was saucy and mischievous in school"; "a
        naughty boy" [syn: {naughty}]
     3: deliberately causing harm or damage; "mischievous rumors and
        falsehoods"
