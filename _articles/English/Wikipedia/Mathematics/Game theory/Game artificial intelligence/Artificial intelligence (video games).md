---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Artificial_intelligence_(video_games)
offline_file: ""
offline_thumbnail: ""
uuid: 1fe2612c-5464-4f37-a5ca-eaec69e9d22c
updated: 1484309004
title: Artificial intelligence (video games)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_6.jpg
tags:
    - History
    - Views
    - Usage
    - In computer simulations of board games
    - In modern video games
    - Video game combat AI
    - Uses in games beyond NPCs
    - Cheating AI
    - Examples
    - Bibliography
categories:
    - Game artificial intelligence
---
In video games, artificial intelligence is used to generate intelligent behaviors primarily in non-player characters (NPCs), often simulating human-like intelligence. The techniques used typically draw upon existing methods from the field of artificial intelligence (AI). However, the term game AI is often used to refer to a broad set of algorithms that also include techniques from control theory, robotics, computer graphics and computer science in general.
