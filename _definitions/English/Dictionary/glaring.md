---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glaring
offline_file: ""
offline_thumbnail: ""
uuid: 2bb2b0d6-00a2-4486-a002-43fe6253dc88
updated: 1484310599
title: glaring
categories:
    - Dictionary
---
glaring
     adj 1: shining intensely; "the blazing sun"; "blinding headlights";
            "dazzling snow"; "fulgent patterns of sunlight"; "the
            glaring sun" [syn: {blazing}, {blinding}, {dazzling},
            {fulgent}, {glary}]
     2: conspicuously and outrageously bad or reprehensible; "a
        crying shame"; "an egregious lie"; "flagrant violation of
        human rights"; "a glaring error"; "gross ineptitude";
        "gross injustice"; "rank treachery" [syn: {crying(a)}, {egregious},
         {flagrant}, {gross}, {rank}]
