---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weekend
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484616301
title: weekend
categories:
    - Dictionary
---
weekend
     n : a time period usually extending from Friday night through
         Sunday; more loosely defined as any period of successive
         days including one and only one Sunday
     v : spend the weekend
