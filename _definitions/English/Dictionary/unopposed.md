---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unopposed
offline_file: ""
offline_thumbnail: ""
uuid: d4294b8d-2a08-4886-9eab-f83a1b3dc48b
updated: 1484310192
title: unopposed
categories:
    - Dictionary
---
unopposed
     adj : not having opposition or an opponent; "unopposed military
           forces"; "the candidate was unopposed" [ant: {opposed}]
