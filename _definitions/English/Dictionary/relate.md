---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relate
offline_file: ""
offline_thumbnail: ""
uuid: cf363a26-0a80-4959-9cb0-a63ee205b831
updated: 1484310591
title: relate
categories:
    - Dictionary
---
relate
     v 1: make a logical or causal connection; "I cannot connect these
          two pieces of evidence in my mind"; "colligate these
          facts"; "I cannot relate these events at all" [syn: {associate},
           {tie in}, {link}, {colligate}, {link up}, {connect}]
          [ant: {decouple}]
     2: have to do with or be relevant to; "There were lots of
        questions referring to her talk"; "My remark pertained to
        your earlier comments" [syn: {refer}, {pertain}, {concern},
         {come to}, {bear on}, {touch}, {touch on}]
     3: give an account of; "The witness related the events"
     4: be in a relationship with; "How are these two observations
        ...
