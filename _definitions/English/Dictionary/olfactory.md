---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/olfactory
offline_file: ""
offline_thumbnail: ""
uuid: 4248295a-7e53-4a50-bddb-40014513f327
updated: 1484310351
title: olfactory
categories:
    - Dictionary
---
olfactory
     adj : of or relating to olfaction [syn: {olfactive}]
