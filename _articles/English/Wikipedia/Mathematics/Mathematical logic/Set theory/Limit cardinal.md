---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Limit_cardinal
offline_file: ""
offline_thumbnail: ""
uuid: 37c7b089-fad0-410a-8b4a-0779436dd151
updated: 1484309506
title: Limit cardinal
tags:
    - Constructions
    - Relationship with ordinal subscripts
    - The notion of inaccessibility and large cardinals
categories:
    - Set theory
---
In mathematics, limit cardinals are certain cardinal numbers. A cardinal number λ is a weak limit cardinal if λ is neither a successor cardinal nor zero. This means that one cannot "reach" λ from another cardinal by repeated successor operations. These cardinals are sometimes called simply "limit cardinals" when the context is clear.
