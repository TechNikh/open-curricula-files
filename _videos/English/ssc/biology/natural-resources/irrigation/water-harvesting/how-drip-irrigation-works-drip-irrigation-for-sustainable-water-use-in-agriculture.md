---
title: How Drip irrigation works. (Drip irrigation for sustainable water use in Agriculture).
youtube_id: hM_HHwXEq_8
tags:
    - Drip irrigation
    - water cultivation
    - water land consumption
    - percentage of water consumption in drip irrigation
categories:
    - Water harvesting
---
