---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/correction
offline_file: ""
offline_thumbnail: ""
uuid: 5010c490-7b31-476b-b9a9-9f245649ee5b
updated: 1484310399
title: correction
categories:
    - Dictionary
---
correction
     n 1: the act of offering an improvement to replace a mistake;
          setting right [syn: {rectification}]
     2: a quantity that is added or subtracted in order to increase
        the accuracy of a scientific measure [syn: {fudge factor}]
     3: something substituted for an error
     4: a rebuke for making a mistake [syn: {chastening}, {chastisement}]
     5: a drop in stock market activity or stock prices following a
        period of increases; "market runups are invariably
        followed by a correction"
     6: the act of punishing; "the offenders deserved the harsh
        discipline they received" [syn: {discipline}]
     7: treatment of a specific defect; ...
