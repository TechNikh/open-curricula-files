---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/motion
offline_file: ""
offline_thumbnail: ""
uuid: cecb517c-a1e9-4e11-8386-155376a9614c
updated: 1484310328
title: motion
categories:
    - Dictionary
---
motion
     n 1: a natural event that involves a change in the position or
          location of something [syn: {movement}]
     2: the use of movements (especially of the hands) to
        communicate familiar or prearranged signals [syn: {gesture}]
     3: a change of position that does not entail a change of
        location; "the reflex motion of his eyebrows revealed his
        surprise"; "movement is a sign of life"; "an impatient
        move of his hand"; "gastrointestinal motility" [syn: {movement},
         {move}, {motility}]
     4: a state of change; "they were in a state of steady motion"
        [ant: {motionlessness}]
     5: a formal proposal for action made to a ...
