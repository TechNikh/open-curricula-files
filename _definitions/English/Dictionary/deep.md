---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deep
offline_file: ""
offline_thumbnail: ""
uuid: e003e971-6b4b-4a75-893d-e90bc806c28d
updated: 1484310315
title: deep
categories:
    - Dictionary
---
deep
     adj 1: relatively deep or strong; affecting one deeply; "a deep
            breath"; "a deep sigh"; "deep concentration"; "deep
            emotion"; "a deep trance"; "in a deep sleep" [ant: {shallow}]
     2: marked by depth of thinking; "deep thoughts"; "a deep
        allegory"
     3: having great spatial extension or penetration downward or
        inward from an outer surface or backward or laterally or
        outward from a center; sometimes used in combination; "a
        deep well"; "a deep dive"; "deep water"; "a deep
        casserole"; "a deep gash"; "deep massage"; "deep pressure
        receptors in muscles"; "deep shelves"; "a deep closet";
        "surrounded by a ...
