---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Newton%27s_theorem_about_ovals'
offline_file: ""
offline_thumbnail: ""
uuid: 404a4f03-d84c-426a-9288-18c6c6562364
updated: 1484309119
title: "Newton's theorem about ovals"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lemniscate-of-Gerono.svg.png
categories:
    - Theorems in geometry
---
In mathematics, Newton's theorem about ovals states that the area cut off by a secant of a smooth convex oval is not an algebraic function of the secant.
