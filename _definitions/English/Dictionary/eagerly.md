---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eagerly
offline_file: ""
offline_thumbnail: ""
uuid: 1650b126-7377-41a9-9d8d-fcdf40b671fd
updated: 1484310585
title: eagerly
categories:
    - Dictionary
---
eagerly
     adv : with eagerness; in an eager manner; "the news was eagerly
           awaited" [syn: {thirstily}]
