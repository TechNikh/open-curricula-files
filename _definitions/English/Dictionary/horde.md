---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/horde
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484578681
title: horde
categories:
    - Dictionary
---
horde
     n 1: a vast multitude [syn: {host}, {legion}]
     2: a nomadic community
     3: a moving crowd [syn: {drove}, {swarm}]
