---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rivalry
offline_file: ""
offline_thumbnail: ""
uuid: 9ae09ef1-86d7-47a1-bf0c-0cca7e1d7cc2
updated: 1484310178
title: rivalry
categories:
    - Dictionary
---
rivalry
     n : the act of competing as for profit or a prize; "the teams
         were in fierce contention for first place" [syn: {competition},
          {contention}] [ant: {cooperation}]
