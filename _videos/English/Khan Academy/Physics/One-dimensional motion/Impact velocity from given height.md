---
version: 1
type: video
provider: YouTube
id: 2ZgBJxT9pbU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/One-dimensional%20motion/Impact%20velocity%20from%20given%20height.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 54da4ac2-03fa-4797-bb4f-c0ce8825ccde
updated: 1486069597
title: Impact velocity from given height
categories:
    - One-dimensional motion
thumbnail_urls:
    - https://i3.ytimg.com/vi/2ZgBJxT9pbU/default.jpg
    - https://i3.ytimg.com/vi/2ZgBJxT9pbU/1.jpg
    - https://i3.ytimg.com/vi/2ZgBJxT9pbU/2.jpg
    - https://i3.ytimg.com/vi/2ZgBJxT9pbU/3.jpg
---
Determining how fast something will be traveling upon impact when it is released from a given height. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/one-dimensional-motion/kinematic-formulas/v/viewing-g-as-the-value-of-earth-s-gravitational-field-near-the-surface?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/one-dimensional-motion/kinematic-formulas/v/deriving-max-projectile-displacement-given-time?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
