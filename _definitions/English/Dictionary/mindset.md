---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mindset
offline_file: ""
offline_thumbnail: ""
uuid: cfa6d13c-a3b7-4e36-9b4c-cbe5cebaa949
updated: 1484310551
title: mindset
categories:
    - Dictionary
---
mindset
     n : a habitual or characteristic mental attitude that determines
         how you will interpret and respond to situations [syn: {mentality},
          {outlook}, {mind-set}]
