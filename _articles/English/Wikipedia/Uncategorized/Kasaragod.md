---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kasargod
offline_file: ""
offline_thumbnail: ""
uuid: 8028073f-6e55-47d0-9877-c363facde434
updated: 1484308466
title: Kasaragod
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Malkassignboard.JPG
tags:
    - language
    - Politics
    - Science and research
    - Health
    - Climate
    - Image gallery
    - Geographic Location
categories:
    - Uncategorized
---
Kasaragod (Malayalam: കാസർഗോഡ്‌) (Tulu : ಕಾಸರಗೋಡು) is a town and a municipality in the Kasaragod district, state of Kerala. Kasargod is located 585 km north of Thiruvananthapuram and 50 km south of Mangalore.
