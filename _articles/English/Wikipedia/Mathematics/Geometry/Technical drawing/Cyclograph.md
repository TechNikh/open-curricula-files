---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyclograph
offline_file: ""
offline_thumbnail: ""
uuid: a8d9e4e2-f9fe-4033-ad49-729f8403cbd4
updated: 1484309324
title: Cyclograph
categories:
    - Technical drawing
---
A cyclograph (also known as an arcograph) is an instrument for drawing arcs of large diameter circles whose centres are inconveniently or inaccessibly located, one version of which was invented by Scottish architect and mathematician Peter Nicholson.[1][2][3]
