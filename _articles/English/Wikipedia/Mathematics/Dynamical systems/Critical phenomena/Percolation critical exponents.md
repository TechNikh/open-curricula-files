---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Percolation_critical_exponents
offline_file: ""
offline_thumbnail: ""
uuid: 83c93607-94f0-4b26-8a22-625b1a0dfe54
updated: 1484308753
title: Percolation critical exponents
tags:
    - Description
    - Definitions of exponents
    - Self-similarity at the percolation threshold
    - Critical behavior close to the percolation threshold
    - Scaling relations
    - Hyperscaling relations
    - |
        Relations based on
        
        
        
        {
        σ
        ,
        τ
        }
        
        
        {\displaystyle \{\sigma ,\tau \}}
    - |
        Relations based on
        
        
        
        {
        
        d
        
        f
        
        
        ,
        ν
        }
        
        
        {\displaystyle \{d_{\text{f}},\nu \}}
    - Exponents for standard percolation
categories:
    - Critical phenomena
---
In the context of percolation theory, a percolation transition is characterized by a set of universal critical exponents, which describe the fractal properties of the percolating medium at large scales and sufficiently close to the transition. The exponents are universal in the sense that they only depend on the type of percolation model and on the space dimension. They are expected not to depend on microscopic details like the lattice structure or whether site or bond percolation is considered. This article deals with the critical exponents of random percolation.
