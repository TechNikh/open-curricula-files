---
version: 1
type: article
id: https://en.wikipedia.org/wiki/LLT_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: 6324d299-bd5b-4059-a5d3-04eae0546720
updated: 1484309511
title: LLT polynomial
categories:
    - Algebraic geometry
---
In mathematics, an LLT polynomial is one of a family of symmetric functions introduced by Alain Lascoux, Bernard Leclerc, and Jean-Yves Thibon (1997) as q-analogues of products of Schur functions.
