---
version: 1
type: video
provider: YouTube
id: JtsyP0tnVRY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Sequences%2C%20series%20and%20induction/Finding%20the%20100th%20term%20in%20a%20sequence.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ff9dbede-b2fe-464e-9f6a-e9482f1527b6
updated: 1486069559
title: Finding the 100th term in a sequence
categories:
    - Sequences, series and induction
thumbnail_urls:
    - https://i3.ytimg.com/vi/JtsyP0tnVRY/default.jpg
    - https://i3.ytimg.com/vi/JtsyP0tnVRY/1.jpg
    - https://i3.ytimg.com/vi/JtsyP0tnVRY/2.jpg
    - https://i3.ytimg.com/vi/JtsyP0tnVRY/3.jpg
---
