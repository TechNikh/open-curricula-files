---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imagine
offline_file: ""
offline_thumbnail: ""
uuid: 488a27d9-e304-4bfb-af02-1944d3529426
updated: 1484310359
title: imagine
categories:
    - Dictionary
---
imagine
     v 1: form a mental image of something that is not present or that
          is not the case; "Can you conceive of him as the
          president?" [syn: {conceive of}, {ideate}, {envisage}]
     2: expect, believe, or suppose; "I imagine she earned a lot of
        money with her new novel"; "I thought to find her in a bad
        state"; "he didn't think to find her in the kitchen"; "I
        guess she is angry at me for standing her up" [syn: {think},
         {opine}, {suppose}, {reckon}, {guess}]
