---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frenzied
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484608201
title: frenzied
categories:
    - Dictionary
---
frenzied
     adj 1: affected with or marked by frenzy or mania uncontrolled by
            reason; "a frenzied attack"; "a frenzied mob"; "the
            prosecutor's frenzied denunciation of the accused"-
            H.W.Carter; "outbursts of drunken violence and manic
            activity and creativity" [syn: {manic}]
     2: excessively agitated; transported with rage or other violent
        emotion; "frantic with anger and frustration"; "frenetic
        screams followed the accident"; "a frenzied look in his
        eye" [syn: {frantic}, {frenetic}, {phrenetic}]
