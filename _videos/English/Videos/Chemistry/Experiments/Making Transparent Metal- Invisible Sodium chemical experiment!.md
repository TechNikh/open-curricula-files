---
version: 1
type: video
provider: YouTube
id: DJGVZNth68k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c02d5708-a32b-4c52-a3f0-2d71f58b4545
updated: 1486069661
title: >
    Making Transparent Metal? Invisible Sodium chemical
    experiment!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/DJGVZNth68k/default.jpg
    - https://i3.ytimg.com/vi/DJGVZNth68k/1.jpg
    - https://i3.ytimg.com/vi/DJGVZNth68k/2.jpg
    - https://i3.ytimg.com/vi/DJGVZNth68k/3.jpg
---
