---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Opaque_forest_problem
offline_file: ""
offline_thumbnail: ""
uuid: efe4e93b-ac5d-4820-98b3-f2c0677e0ad7
updated: 1484309173
title: Opaque forest problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Unit_Square_Opaque_Forest_Solutions.svg.png
tags:
    - History and difficulty
    - Bounding the optimal solution
    - Upper bound
    - Lower bound
    - Special cases
    - Approximations
    - Barrier verification and computing the coverage of forests
categories:
    - Computational geometry
---
In computational geometry, The opaque forest problem can be stated as follows: "Given a convex polygon C in the plane, determine the minimal forest T of closed, bounded line segments such that every line through C also intersects T". T is said to be the opaque forest, or barrier of C. C is said to be the coverage of T. While any forest that covers C is a barrier of C, we wish to find the one with shortest length.
