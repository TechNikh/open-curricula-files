---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Barnebys
offline_file: ""
offline_thumbnail: ""
uuid: de91c99f-646e-408f-b96e-eaafc90fc3ce
updated: 1484309006
title: Barnebys
categories:
    - Auctions
---
Barnebys is the leading search engine for art, antiques and collectibles from more than 1,600 auction houses around the world. Barnebys users are – for the first time – able to search the whole of the auctions market in one user-friendly site. Barnebys offers a free-to-use database of realized prices, dating back to the beginning of the 1980s and providing over eighteen million sold lots. [1]
