---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/holding
offline_file: ""
offline_thumbnail: ""
uuid: 6d23fce6-fb7c-4dca-8e5e-bfe10f978c91
updated: 1484310196
title: holding
categories:
    - Dictionary
---
holding
     adj : designed for (usually temporary) retention; "a holding pen";
           "a retaining wall" [syn: {retaining}]
     n 1: the act of keeping in your possession [syn: {retention}, {keeping}]
     2: something owned; any tangible or intangible possession that
        is owned by someone; "that hat is my property"; "he is a
        man of property"; [syn: {property}, {belongings}, {material
        possession}]
