---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Roger_Schank
offline_file: ""
offline_thumbnail: ""
uuid: 88171511-6239-4a88-9633-80dac5a46211
updated: 1484308684
title: Roger Schank
tags:
    - Academic career
    - Entrepreneurship
    - Educational reform
    - Influence
    - Works
categories:
    - Theoretical computer science
---
Roger Carl Schank (born 1946) is an American artificial intelligence theorist, cognitive psychologist, learning scientist, educational reformer, and entrepreneur.
