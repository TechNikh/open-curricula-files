---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prefix
offline_file: ""
offline_thumbnail: ""
uuid: ee200ebe-da62-4a89-91d8-e96dcbafdc09
updated: 1484310399
title: prefix
categories:
    - Dictionary
---
prefix
     n : an affix that added in front of the word
     v : attach a prefix to; "prefixed words" [ant: {suffix}]
