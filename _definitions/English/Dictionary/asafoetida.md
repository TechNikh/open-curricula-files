---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/asafoetida
offline_file: ""
offline_thumbnail: ""
uuid: 73270db4-9f38-40ed-ac6f-4f91b4c1c5ea
updated: 1484310343
title: asafoetida
categories:
    - Dictionary
---
asafoetida
     n : the brownish gum resin of various plants; has strong taste
         and odor; formerly used as an antispasmodic [syn: {asafetida}]
