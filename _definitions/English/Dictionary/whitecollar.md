---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/whitecollar
offline_file: ""
offline_thumbnail: ""
uuid: 016d718a-d0ff-4765-9d75-0cce6e82a62f
updated: 1484310573
title: whitecollar
categories:
    - Dictionary
---
white-collar
     adj : of or designating salaried professional or clerical  work or
           workers; "the coal miner's son aspired to a
           white-collar occupation as a bookkeeper" [ant: {blue-collar}]
