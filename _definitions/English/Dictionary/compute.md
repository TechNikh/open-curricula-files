---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compute
offline_file: ""
offline_thumbnail: ""
uuid: 1ad107ae-4109-4b35-a295-bacd0c9c2515
updated: 1484310451
title: compute
categories:
    - Dictionary
---
compute
     v : make a mathematical calculation or computation [syn: {calculate},
          {cipher}, {cypher}, {work out}, {reckon}, {figure}]
