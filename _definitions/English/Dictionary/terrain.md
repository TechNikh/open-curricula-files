---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terrain
offline_file: ""
offline_thumbnail: ""
uuid: 98508fe8-2ee0-4471-8aed-8f6047824d87
updated: 1484310439
title: terrain
categories:
    - Dictionary
---
terrain
     n : a piece of ground having specific characteristics or
         military potential; "they decided to attack across the
         rocky terrain"
