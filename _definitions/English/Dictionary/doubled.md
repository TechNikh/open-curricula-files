---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/doubled
offline_file: ""
offline_thumbnail: ""
uuid: 65808e7c-4944-480f-959f-ffa2e8a55694
updated: 1484310154
title: doubled
categories:
    - Dictionary
---
doubled
     adj 1: twice as great or many; "ate a double portion"; "the dose is
            doubled"; "a twofold increase" [syn: {double}, {twofold}]
     2: folded in two; "doubled sheets of paper"
