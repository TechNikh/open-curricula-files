---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dot
offline_file: ""
offline_thumbnail: ""
uuid: d2af5b81-0aac-4394-9714-4086776c049e
updated: 1484310208
title: dot
categories:
    - Dictionary
---
dot
     n 1: a very small circular shape; "a row of points"; "draw lines
          between the dots" [syn: {point}]
     2: the United States federal department that institutes and
        coordinates national transportation programs; created in
        1966 [syn: {Department of Transportation}, {Transportation}]
     3: the shorter of the two telegraphic signals used in Morse
        code [syn: {dit}]
     4: street name for lysergic acid diethylamide [syn: {acid}, {back
        breaker}, {battery-acid}, {dose}, {Elvis}, {loony toons},
        {Lucy in the sky with diamonds}, {pane}, {superman}, {window
        pane}, {Zen}]
     v 1: scatter or intersperse like dots or studs; "Hills
     ...
