---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unmistakably
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484637841
title: unmistakably
categories:
    - Dictionary
---
unmistakably
     adv 1: without possibility of mistake; "this watercolor is
            unmistakably a synthesis of nature"
     2: in a signal manner; "signally inappropriate methods" [syn: {signally},
         {remarkably}]
