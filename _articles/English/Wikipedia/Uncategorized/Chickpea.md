---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bengal_gram
offline_file: ""
offline_thumbnail: ""
uuid: 07b07453-eabb-46e5-b5b7-4257230120bb
updated: 1484308365
title: Chickpea
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Cicer_arietinum_HabitusFruits_BotGardBln0906a.jpg
tags:
    - Etymology
    - History
    - Genome sequencing
    - Description
    - Types
    - Uses
    - Human consumption
    - Animal feed
    - Nutrition
    - Effects of cooking
    - Germination
    - Autoclaving, microwave cooking, boiling
    - Leaves
    - Production
    - Heat and micronutrient cultivation
    - Pathogens
categories:
    - Uncategorized
---
The chickpea or chick pea (Cicer arietinum) is a legume of the family Fabaceae, subfamily Faboideae. It is also known as gram,[2][3] or Bengal gram,[3] garbanzo[3] or garbanzo bean, and sometimes known as Egyptian pea,[2] ceci, cece or chana, or Kabuli chana (particularly in northern India). Its seeds are high in protein. It is one of the earliest cultivated legumes: 7,500-year-old remains have been found in the Middle East.[4]
