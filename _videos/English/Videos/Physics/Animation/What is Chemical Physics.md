---
version: 1
type: video
provider: YouTube
id: 3DUGnEBbPb0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bae779ca-43d2-4b10-9269-148670a91172
updated: 1486069654
title: What is Chemical Physics
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/3DUGnEBbPb0/default.jpg
    - https://i3.ytimg.com/vi/3DUGnEBbPb0/1.jpg
    - https://i3.ytimg.com/vi/3DUGnEBbPb0/2.jpg
    - https://i3.ytimg.com/vi/3DUGnEBbPb0/3.jpg
---
