---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/generate
offline_file: ""
offline_thumbnail: ""
uuid: 1ac2dfcf-edc0-4e01-b953-607c0b37e390
updated: 1484310293
title: generate
categories:
    - Dictionary
---
generate
     v 1: bring into existence; "The new manager generated a lot of
          problems"; "The computer bug generated chaos in the
          office" [syn: {bring forth}]
     2: give or supply; "The cow brings in 5 liters of milk"; "This
        year's crop yielded 1,000 bushels of corn"; "The estate
        renders some revenue for the family" [syn: {render}, {yield},
         {return}, {give}]
     3: produce (energy); "We can't generate enough power for the
        entire city"; "The hydroelectric plant needs to to
        generate more electricity"
     4: make children; "Abraham begot Isaac"; "Men often father
        children but don't recognize them" [syn: {beget}, {get}, ...
