---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fled
offline_file: ""
offline_thumbnail: ""
uuid: bb9a0a04-2e52-4050-b3b1-eec30ebc7c6d
updated: 1484310593
title: fled
categories:
    - Dictionary
---
flee
     v : run away quickly; "He threw down his gun and fled" [syn: {fly},
          {take flight}]
     [also: {fled}]
