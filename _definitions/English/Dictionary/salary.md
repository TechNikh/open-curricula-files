---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/salary
offline_file: ""
offline_thumbnail: ""
uuid: 9b595896-15d9-4c93-b991-55b7bdf1a215
updated: 1484310457
title: salary
categories:
    - Dictionary
---
salary
     n : something that remunerates; "wages were paid by check"; "he
         wasted his pay on drink"; "they saved a quarter of all
         their earnings" [syn: {wage}, {pay}, {earnings}, {remuneration}]
     [also: {salaried}]
