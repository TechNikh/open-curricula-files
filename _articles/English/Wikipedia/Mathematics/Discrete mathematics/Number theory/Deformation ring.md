---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deformation_ring
offline_file: ""
offline_thumbnail: ""
uuid: e4565e75-34b0-4a1f-a662-bd2477d0f35a
updated: 1484308639
title: Deformation ring
categories:
    - Number theory
---
In mathematics, a deformation ring is a ring that controls liftings of a representation of a Galois group from a finite field to a local field. In particular for any such lifting problem there is often a universal deformation ring that classifies all such liftings, and whose spectrum is the universal deformation space.
