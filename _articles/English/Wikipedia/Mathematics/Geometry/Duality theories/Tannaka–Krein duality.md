---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tannaka%E2%80%93Krein_duality'
offline_file: ""
offline_thumbnail: ""
uuid: d1f5adf0-8d30-4dd5-bf52-db0ddfc59f37
updated: 1484309237
title: Tannaka–Krein duality
tags:
    - 'The idea of Tannaka–Krein duality: category of representations of a group'
    - Theorems of Tannaka and Krein
    - Generalization
    - Doplicher–Roberts theorem
    - Notes
categories:
    - Duality theories
---
In mathematics, Tannaka–Krein duality theory concerns the interaction of a compact topological group and its category of linear representations. It is a natural extension of Pontryagin duality, between compact and discrete commutative topological groups, to groups that are compact but noncommutative. The theory is named for two men, the Soviet mathematician Mark Grigorievich Krein, and the Japanese Tadao Tannaka. In contrast to the case of commutative groups considered by Lev Pontryagin, the notion dual to a noncommutative compact group is not a group, but a category Π(G) with some additional structures, formed by the finite-dimensional representations of G.
