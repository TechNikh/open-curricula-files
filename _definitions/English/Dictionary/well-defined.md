---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/well-defined
offline_file: ""
offline_thumbnail: ""
uuid: 30905a1e-cf48-43e5-8a69-d17276ad903e
updated: 1484310140
title: well-defined
categories:
    - Dictionary
---
well-defined
     adj 1: having a clean and distinct outline as if precisely cut
            along the edges; "a finely chiseled nose";
            "well-defined features" [syn: {chiseled}]
     2: accurately stated or described; "a set of well-defined
        values" [syn: {clear}] [ant: {ill-defined}]
