---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/struggling
offline_file: ""
offline_thumbnail: ""
uuid: cc5ff7d4-a1e5-4af7-9933-bba3e489190c
updated: 1484310527
title: struggling
categories:
    - Dictionary
---
struggling
     adj : engaged in a struggle to overcome especially poverty or
           obscurity; "a financially struggling theater";
           "struggling artists"
