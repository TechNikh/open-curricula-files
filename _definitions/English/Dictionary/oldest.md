---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oldest
offline_file: ""
offline_thumbnail: ""
uuid: 2de6a390-929f-409a-bf5e-778959af5b21
updated: 1484310533
title: oldest
categories:
    - Dictionary
---
oldest
     adj : first in time; "the oldest rocks on the planet"
