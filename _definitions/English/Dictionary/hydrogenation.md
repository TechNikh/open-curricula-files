---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydrogenation
offline_file: ""
offline_thumbnail: ""
uuid: 101bf1f0-3786-43ab-b4b5-c5699dfae34b
updated: 1484310423
title: hydrogenation
categories:
    - Dictionary
---
hydrogenation
     n : a chemical process that adds hydrogen atoms to an
         unsaturated oil; "food producers use hydrogenation to
         keep fat from becoming rancid"
