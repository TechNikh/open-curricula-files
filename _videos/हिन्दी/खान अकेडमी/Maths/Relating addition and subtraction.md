---
version: 1
type: video
provider: YouTube
id: nuRbTPbP0K0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 812ebfdc-fe58-435d-9e03-48565ff5f3ef
updated: 1486069618
title: Relating addition and subtraction
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/nuRbTPbP0K0/default.jpg
    - https://i3.ytimg.com/vi/nuRbTPbP0K0/1.jpg
    - https://i3.ytimg.com/vi/nuRbTPbP0K0/2.jpg
    - https://i3.ytimg.com/vi/nuRbTPbP0K0/3.jpg
---
