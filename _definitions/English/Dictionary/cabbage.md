---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cabbage
offline_file: ""
offline_thumbnail: ""
uuid: 105d1965-21f0-4076-acdc-b8f58fa67670
updated: 1484310379
title: cabbage
categories:
    - Dictionary
---
cabbage
     n 1: any of various types of cabbage [syn: {chou}]
     2: informal terms for money [syn: {boodle}, {bread}, {clams}, {dinero},
         {dough}, {gelt}, {kale}, {lettuce}, {lolly}, {lucre}, {loot},
         {moolah}, {pelf}, {scratch}, {shekels}, {simoleons}, {sugar},
         {wampum}]
     3: any of various cultivars of the genus Brassica oleracea
        grown for their edible leaves or flowers [syn: {cultivated
        cabbage}, {Brassica oleracea}]
     v : make off with belongings of others [syn: {pilfer}, {purloin},
          {pinch}, {abstract}, {snarf}, {swipe}, {hook}, {sneak},
         {filch}, {nobble}, {lift}]
