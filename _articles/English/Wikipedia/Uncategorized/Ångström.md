---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Angstrom
offline_file: ""
offline_thumbnail: ""
uuid: 6cc82a40-dd75-4995-b3ad-ebcc61e37ff4
updated: 1484308386
title: Ångström
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Anders_%25C3%2585ngstr%25C3%25B6m_painting.jpg'
tags:
    - use
    - History
    - Symbol
categories:
    - Uncategorized
---
The ångström (Swedish: [ˈɔŋstrøm]) or angstrom is a unit of length equal to 6990100000000000000♠10−10 m (one ten-billionth of a metre) or 0.1 nanometre. Its symbol is Å, a letter in the Swedish alphabet.
