---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wheat
offline_file: ""
offline_thumbnail: ""
uuid: 2445b703-43db-40e9-8676-f342b117e2a8
updated: 1484310246
title: wheat
categories:
    - Dictionary
---
wheat
     n 1: annual or biennial grass having erect flower spikes and
          light brown grains [syn: {corn}]
     2: grains of common wheat; sometimes cooked whole or cracked as
        cereal; usually ground into flour [syn: {wheat berry}]
