---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ministry
offline_file: ""
offline_thumbnail: ""
uuid: 3b385354-902a-4d29-a7bb-7820f769f85d
updated: 1484310144
title: ministry
categories:
    - Dictionary
---
ministry
     n 1: religious ministers collectively (especially Presbyterian)
     2: building where the business of a government ministry is
        transacted
     3: a government department under the direction of a minister
