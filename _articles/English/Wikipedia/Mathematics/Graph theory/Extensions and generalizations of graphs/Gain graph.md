---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gain_graph
offline_file: ""
offline_thumbnail: ""
uuid: f868f41c-048f-4e28-baad-5b63d0dedc14
updated: 1484309408
title: Gain graph
categories:
    - Extensions and generalizations of graphs
---
A gain graph is a graph whose edges are labelled "invertibly", or "orientably", by elements of a group G. This means that, if an edge e in one direction has label g (a group element), then in the other direction it has label g −1. The label function φ therefore has the property that it is defined differently, but not independently, on the two different orientations, or directions, of an edge e. The group G is called the gain group, φ is the gain function, and the value φ(e) is the gain of e (in some indicated direction). A gain graph is a generalization of a signed graph, where the gain group G has only two elements. See Zaslavsky (1989, 1991).
