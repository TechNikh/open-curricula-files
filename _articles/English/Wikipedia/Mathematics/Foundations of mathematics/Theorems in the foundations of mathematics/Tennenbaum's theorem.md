---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tennenbaum%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 90197f7e-0fc0-4ee8-b980-660ea865d968
updated: 1484309001
title: "Tennenbaum's theorem"
tags:
    - Recursive structures for PA
    - Statement of the theorem
    - Proof Strategy
    - Proof Outline
categories:
    - Theorems in the foundations of mathematics
---
Tennenbaum's theorem, named for Stanley Tennenbaum who presented the theorem in 1959, is a result in mathematical logic that states that no countable nonstandard model of Peano arithmetic (PA)[clarification needed] can be recursive.
