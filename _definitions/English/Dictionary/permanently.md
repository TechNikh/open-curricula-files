---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/permanently
offline_file: ""
offline_thumbnail: ""
uuid: 7687daf7-ed73-4750-afc6-9cf3a9b76aad
updated: 1484310515
title: permanently
categories:
    - Dictionary
---
permanently
     adv : for a long time without essential change; "he is permanently
           disabled" [syn: {for good}] [ant: {temporarily}]
