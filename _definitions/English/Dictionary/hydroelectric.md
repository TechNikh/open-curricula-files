---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydroelectric
offline_file: ""
offline_thumbnail: ""
uuid: f0a4ec78-2539-4030-96a2-25649e005118
updated: 1484310539
title: hydroelectric
categories:
    - Dictionary
---
hydroelectric
     adj : of or relating to or used in the production of electricity
           by waterpower; "hydroelectric power"
