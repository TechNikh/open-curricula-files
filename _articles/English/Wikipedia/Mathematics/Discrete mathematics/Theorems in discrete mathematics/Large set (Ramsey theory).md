---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Large_set_(Ramsey_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 5a69fd8e-9140-4778-a861-b49b02f14cf4
updated: 1484308659
title: Large set (Ramsey theory)
tags:
    - Examples
    - Properties
    - 2-large and k-large sets
categories:
    - Theorems in discrete mathematics
---
In Ramsey theory, a set S of natural numbers is considered to be a large set if and only if Van der Waerden's theorem can be generalized to assert the existence of arithmetic progressions with common difference in S. That is, S is large if and only if every finite partition of the natural numbers has a cell containing arbitrarily long arithmetic progressions having common differences in S.
