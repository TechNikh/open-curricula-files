---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shkarofsky_function
offline_file: ""
offline_thumbnail: ""
uuid: f1f66ac9-55e4-4436-a471-f8aaf5e387f6
updated: 1484308896
title: Shkarofsky function
categories:
    - Waves
---
The Shkarofsky function is a physics formula which describes the behavior of microwaves. It is named after Canadian physicist Issie Shkarofsky, who first identified the function in 1966.[1]
