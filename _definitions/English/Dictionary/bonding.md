---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bonding
offline_file: ""
offline_thumbnail: ""
uuid: e8677bf4-f190-4e51-ac86-d49c36f99d42
updated: 1484310373
title: bonding
categories:
    - Dictionary
---
bonding
     n 1: fastening firmly together [syn: {soldering}]
     2: a close personal relationship that forms between people (as
        between husband and wife or parent and child)
     3: (dentistry) a technique for repairing a tooth; resinous
        material is applied to the surface of the tooth where it
        adheres to the tooth's enamel
