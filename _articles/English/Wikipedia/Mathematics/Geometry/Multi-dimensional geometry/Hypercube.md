---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hypercube
offline_file: ""
offline_thumbnail: ""
uuid: 1167a23a-a7bb-4322-8cd0-6ad2b5e3c40c
updated: 1484309285
title: Hypercube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Dimension_levels.svg.png
tags:
    - Construction
    - Coordinates
    - Elements
    - Graphs
    - Related families of polytopes
    - Relation to n-simplices
    - Generalized hypercubes
    - Notes
categories:
    - Multi-dimensional geometry
---
In geometry, a hypercube is an n-dimensional analogue of a square (n = 2) and a cube (n = 3). It is a closed, compact, convex figure whose 1-skeleton consists of groups of opposite parallel line segments aligned in each of the space's dimensions, perpendicular to each other and of the same length. A unit hypercube's longest diagonal in n-dimensions is equal to 
  
    
      
        
          
            n
          
        
      
    
    {\displaystyle {\sqrt {n}}}
  
.
