---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diameter
offline_file: ""
offline_thumbnail: ""
uuid: fbcc437e-e6c8-4c84-8375-98ac56806c53
updated: 1484308930
title: Diameter
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Circle-withsegments.svg_0.png
tags:
    - Generalizations
    - Diameter symbol
    - Notes
categories:
    - Elementary geometry
---
In geometry, a diameter of a circle is any straight line segment that passes through the center of the circle and whose endpoints lie on the circle. It can also be defined as the longest chord of the circle. Both definitions are also valid for the diameter of a sphere. The word "diameter" is derived from Greek διάμετρος (diametros), "diameter of a circle", from δια- (dia-), "across, through" + μέτρον (metron), "measure".[1] It is often abbreviated DIA, dia, d, or ⌀.
