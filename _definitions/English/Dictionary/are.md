---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/are
offline_file: ""
offline_thumbnail: ""
uuid: ae6bf34c-f308-49b4-9e80-77acd95cd809
updated: 1484310359
title: are
categories:
    - Dictionary
---
are
     n : a unit of surface area equal to 100 square meters [syn: {ar}]
