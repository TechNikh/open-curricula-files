---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seriously
offline_file: ""
offline_thumbnail: ""
uuid: f089fd87-48d0-43a8-913d-1df2b2f9c63d
updated: 1484310462
title: seriously
categories:
    - Dictionary
---
seriously
     adv 1: in a serious manner; "talking earnestly with his son"; "she
            started studying snakes in earnest"; "a play dealing
            seriously with the question of divorce" [syn: {earnestly},
             {in earnest}]
     2: to a severe or serious degree; "fingers so badly frozen they
        had to be amputated"; "badly injured"; "a severely
        impaired heart"; "is gravely ill"; "was seriously ill"
        [syn: {badly}, {severely}, {gravely}]
