---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/structural
offline_file: ""
offline_thumbnail: ""
uuid: 27d670ff-992f-4437-8dc5-2963065a3c2a
updated: 1484310284
title: structural
categories:
    - Dictionary
---
structural
     adj 1: relating to or caused by structure, especially political or
            economic structure; "structural unemployment in a
            technological society"
     2: relating to or having or characterized by structure;
        "structural engineer"; "structural errors"; "structural
        simplicity"
     3: affecting or involved in structure or construction; "the
        structural details of a house such as beams and joists and
        rafters; not ornamental elements"; "structural damage"
     4: concerned with systematic structure in a particular field of
        study
     5: pertaining to geological structure; "geomorphological
        features of the Black ...
