---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exhaustive
offline_file: ""
offline_thumbnail: ""
uuid: 38f71422-2c26-4ab2-96cf-fdb38ca959f2
updated: 1484310264
title: exhaustive
categories:
    - Dictionary
---
exhaustive
     adj : very thorough; exhaustively complete; "an exhaustive study";
           "made a thorough search"; "thoroughgoing research"
           [syn: {thorough}, {thoroughgoing}]
