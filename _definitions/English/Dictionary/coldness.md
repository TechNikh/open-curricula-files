---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coldness
offline_file: ""
offline_thumbnail: ""
uuid: 9cf61593-47fb-470d-bb81-6a9510a4fc2d
updated: 1484310232
title: coldness
categories:
    - Dictionary
---
coldness
     n 1: the sensation produced by low temperatures; "he shivered
          from the cold"; "the cold helped clear his head" [syn: {cold}]
     2: a lack of affection or enthusiasm [syn: {coolness}, {frigidity}]
     3: the absence of heat; "the coldness made our breath visible";
        "come in out of the cold"; "cold is a vasoconstrictor"
        [syn: {cold}, {low temperature}] [ant: {hotness}]
