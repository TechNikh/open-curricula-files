---
version: 1
type: video
provider: YouTube
id: 5jSuC-sxe74
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b88bd512-2026-4816-867a-238808f8adb7
updated: 1486069618
title: Graphing linear inequalities in two variables 3
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/5jSuC-sxe74/default.jpg
    - https://i3.ytimg.com/vi/5jSuC-sxe74/1.jpg
    - https://i3.ytimg.com/vi/5jSuC-sxe74/2.jpg
    - https://i3.ytimg.com/vi/5jSuC-sxe74/3.jpg
---
