---
version: 1
type: video
provider: YouTube
id: bxQdUugzEPk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e15b4989-61ca-4399-b093-9330bae100ae
updated: 1486069613
title: Example 1 Simplifying polynomials
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/bxQdUugzEPk/default.jpg
    - https://i3.ytimg.com/vi/bxQdUugzEPk/1.jpg
    - https://i3.ytimg.com/vi/bxQdUugzEPk/2.jpg
    - https://i3.ytimg.com/vi/bxQdUugzEPk/3.jpg
---
