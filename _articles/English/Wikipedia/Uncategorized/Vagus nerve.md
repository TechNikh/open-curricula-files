---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vagus
offline_file: ""
offline_thumbnail: ""
uuid: a854959a-70e4-4f80-8f70-029cec9fdb00
updated: 1484308380
title: Vagus nerve
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray791.png
tags:
    - Structure
    - Nuclei
    - Development
    - Function
    - The vagus nerve and the heart
    - Physical and emotional effects
    - Clinical significance
    - Vagus nerve stimulation
    - Vagotomy
    - Chagasic disease
    - History
    - Etymology
    - Additional images
categories:
    - Uncategorized
---
The vagus nerve (/ˈveɪɡəs/ VAY-gəs), historically cited as the pneumogastric nerve, is the tenth cranial nerve or CN X, and interfaces with parasympathetic control of the heart, lungs and digestive tract. The vagus nerves are paired; however, they are normally referred to in the singular. It is the longest nerve of the autonomic nervous system in the human body.
