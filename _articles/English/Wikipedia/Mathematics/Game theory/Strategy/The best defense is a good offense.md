---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/The_best_defense_is_a_good_offense
offline_file: ""
offline_thumbnail: ""
uuid: 0e4f70ef-dcf5-4bdb-b3b8-43e0a84c1c5a
updated: 1484309062
title: The best defense is a good offense
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-All_Harvester_Products_are_war_products..._either_for_offense_or_defense._-_NARA_-_534818.jpg
tags:
    - Military
    - Games
    - Business
categories:
    - Strategy
---
"The best defense is a good offense" is an adage that has been applied to many fields of endeavor, including games and military combat. Also known as the Strategic Offensive principle of war. Generally, the idea is that Proactivity (a strong offensive action) instead of a passive attitude, will preoccupy the opposition and ultimately hinder its ability to mount an opposing counterattack, leading to a strategic advantage.
