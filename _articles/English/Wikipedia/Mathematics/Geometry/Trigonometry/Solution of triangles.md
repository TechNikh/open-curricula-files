---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Solution_of_triangles
offline_file: ""
offline_thumbnail: ""
uuid: 5b058aff-abf2-4d43-9fd6-c70c4fa85fd9
updated: 1484309348
title: Solution of triangles
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Triangle_-_angles%252C_vertices%252C_sides.svg.png'
tags:
    - Solving plane triangles
    - Main theorems
    - Notes
    - Three sides given (SSS)
    - Two sides and the included angle given (SAS)
    - Two sides and non-included angle given (SSA)
    - A side and two adjacent angles given (ASA)
    - >
        A side, one adjacent angle and the opposite angle given
        (AAS)
    - Solving spherical triangles
    - Three sides given (spherical SSS)
    - Two sides and the included angle given (spherical SAS)
    - Two sides and non-included angle given (spherical SSA)
    - A side and two adjacent angles given (spherical ASA)
    - >
        A side, one adjacent angle and the opposite angle given
        (spherical AAS)
    - Three angles given (spherical AAA)
    - Solving right-angled spherical triangles
    - Some applications
    - Triangulation
    - The distance between two points on the globe
categories:
    - Trigonometry
---
Solution of triangles (Latin: solutio triangulorum) is the main trigonometric problem of finding the characteristics of a triangle (angles and lengths of sides), when some of these are known. The triangle can be located on a plane or on a sphere. Applications requiring triangle solutions include geodesy, astronomy, construction, and navigation.
