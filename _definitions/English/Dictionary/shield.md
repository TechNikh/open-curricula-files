---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shield
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484505841
title: shield
categories:
    - Dictionary
---
shield
     n 1: a protective covering or structure
     2: armor carried on the arm to intercept blows [syn: {buckler}]
     v 1: protect, hide, or conceal from danger or harm [syn: {screen}]
     2: hold back a thought or feeling about; "She is harboring a
        grudge against him" [syn: {harbor}, {harbour}]
