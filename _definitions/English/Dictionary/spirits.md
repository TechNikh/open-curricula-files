---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spirits
offline_file: ""
offline_thumbnail: ""
uuid: 49fadc8e-44ad-4fc5-88ff-bb0de9fe7352
updated: 1484310152
title: spirits
categories:
    - Dictionary
---
spirits
     n : distilled rather than fermented [syn: {liquor}, {booze}, {hard
         drink}, {hard liquor}, {John Barleycorn}, {strong drink}]
