---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/energy
offline_file: ""
offline_thumbnail: ""
uuid: 0273b4f0-7f24-4cc5-9e7f-81915b21e8d6
updated: 1484310319
title: energy
categories:
    - Dictionary
---
energy
     n 1: (physics) the capacity of a physical system to do work; the
          units of energy are joules or ergs; "energy can take a
          wide variety of forms"
     2: an exertion of force; "he plays tennis with great energy"
        [syn: {vigor}, {vigour}]
     3: enterprising or ambitious drive; "Europeans often laugh at
        American energy" [syn: {push}, {get-up-and-go}]
     4: an imaginative lively style (especially style of writing);
        "his writing conveys great energy" [syn: {vigor}, {vigour},
         {vim}]
     5: a healthy capacity for vigorous activity; "jogging works off
        my excess energy"; "he seemed full of vim and vigor" [syn:
         {vim}, ...
