---
version: 1
type: video
provider: YouTube
id: MnQ7Lizkpqk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Sequences%2C%20series%20and%20induction/Inductive%20reasoning%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d9ad16cb-affb-4b32-9783-cd00f7fff31f
updated: 1486069559
title: Inductive reasoning 1
categories:
    - Sequences, series and induction
thumbnail_urls:
    - https://i3.ytimg.com/vi/MnQ7Lizkpqk/default.jpg
    - https://i3.ytimg.com/vi/MnQ7Lizkpqk/1.jpg
    - https://i3.ytimg.com/vi/MnQ7Lizkpqk/2.jpg
    - https://i3.ytimg.com/vi/MnQ7Lizkpqk/3.jpg
---
