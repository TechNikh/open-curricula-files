---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Negentropy
offline_file: ""
offline_thumbnail: ""
uuid: facab001-d886-40d9-b110-8482d9ec7d1d
updated: 1484308773
title: Negentropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/275px-Wykres_Gibbsa.svg.png
tags:
    - Information theory
    - "Correlation between statistical negentropy and Gibbs' free energy"
    - Risk management
    - "Brillouin's negentropy principle of information"
    - Notes
categories:
    - Entropy and information
---
The negentropy has different meanings in information theory and theoretical biology. In a biological context, the negentropy (also negative entropy, syntropy, extropy, ectropy or entaxy[1]) of a living system is the entropy that it exports to keep its own entropy low; it lies at the intersection of entropy and life. The concept and phrase "negative entropy" was introduced by Erwin Schrödinger in his 1944 popular-science book What is Life?[2] Later, Léon Brillouin shortened the phrase to negentropy,[3][4] to express it in a more "positive" way: a living system imports negentropy and stores it.[5] In 1974, Albert Szent-Györgyi proposed replacing the term negentropy with syntropy. That term ...
