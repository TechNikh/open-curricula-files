---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Top_trading_cycle
offline_file: ""
offline_thumbnail: ""
uuid: cd8e8974-1100-4849-ac83-dd0e64867a60
updated: 1484308520
title: Top trading cycle
tags:
    - Housing market
    - Extensions
    - Implementation in software package
categories:
    - Fair division
---
Top trading cycle (TTC) is an algorithm for trading indivisible items without using money. It was developed by David Gale and published by Herbert Scarf and Lloyd Shapley.[1]:30–31
