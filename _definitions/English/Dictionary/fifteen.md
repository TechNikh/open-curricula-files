---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fifteen
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484316661
title: fifteen
categories:
    - Dictionary
---
fifteen
     adj : being one more than fourteen [syn: {15}, {xv}]
     n : the cardinal number that is the sum of fourteen and one
         [syn: {15}, {XV}]
