---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/text
offline_file: ""
offline_thumbnail: ""
uuid: 48126a4a-566c-4dc8-9dfc-342a260f2df9
updated: 1484310445
title: text
categories:
    - Dictionary
---
text
     n 1: the words of something written; "there were more than a
          thousand words of text"; "they handed out the printed
          text of the mayor's speech"; "he wants to reconstruct
          the original text" [syn: {textual matter}]
     2: a passage from the Bible that is used as the subject of a
        sermon; "the preacher chose a text from Psalms to
        introduce his sermon"
     3: a book prepared for use in schools or colleges; "his
        economics textbook is in its tenth edition"; "the
        professor wrote the text that he assigned students to buy"
        [syn: {textbook}, {text edition}, {schoolbook}, {school
        text}] [ant: {trade book}]
     4: ...
