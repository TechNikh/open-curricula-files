---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suspicious
offline_file: ""
offline_thumbnail: ""
uuid: 35d29c1b-8c43-4571-a24b-92f4f840ea91
updated: 1484310551
title: suspicious
categories:
    - Dictionary
---
suspicious
     adj 1: openly distrustful and unwilling to confide [syn: {leery}, {mistrustful},
             {untrusting}, {wary}]
     2: not as expected; "there was something fishy about the
        accident"; "up to some funny business"; "some definitely
        queer goings-on"; "a shady deal"; "her motives were
        suspect"; "suspicious behavior" [syn: {fishy}, {funny}, {queer},
         {shady}, {suspect}]
