---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Volkswagen
offline_file: ""
offline_thumbnail: ""
uuid: 8f4fe7d4-5d92-4304-8a52-9c1dcbbac55a
updated: 1484308482
title: Volkswagen
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Porsche_Typ12_Model_Nuremberg_crop.jpg
tags:
    - History
    - "1932–1938: People's Car project"
    - '1945–1948: British Army intervention, unclear future'
    - '1948–1961: Icon of post war West German regeneration'
    - '1961–1973: Beetle to Golf'
    - '1974–1990: Product line expansion'
    - 1991–1999
    - '2000–present: Further expansion'
    - Operations
    - Worldwide presence
categories:
    - Uncategorized
---
Volkswagen (German pronunciation: [ˈfɔlksˌvaːɡŋ̍] -  listen (help·info); shortened to VW) is a German automaker founded on 4 January 1937 by the German Labour Front, headquartered in Wolfsburg, Germany. It is the flagship marque of the Volkswagen Group and is the largest automaker worldwide currently.[1]
