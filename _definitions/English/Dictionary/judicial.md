---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/judicial
offline_file: ""
offline_thumbnail: ""
uuid: 517b8508-5ee9-4523-a9cf-071aaef4a45e
updated: 1484096619
title: judicial
categories:
    - Dictionary
---
judicial
     adj 1: decreed by or proceeding from a court of justice; "a
            judicial decision"
     2: belonging or appropriate to the office of a judge; "judicial
        robes"
     3: relating to the administration of justice or the function of
        a judge; "judicial system" [syn: {juridical}, {juridic}]
     4: expressing careful judgment; "discriminative censure"; "a
        biography ...appreciative and yet judicial in
        purpose"-Tyler Dennett [syn: {discriminative}]
