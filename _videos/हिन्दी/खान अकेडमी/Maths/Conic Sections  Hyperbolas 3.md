---
version: 1
type: video
provider: YouTube
id: XHPgDnJOkWM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e20f2002-0464-4d25-96c6-7bdd7fdc3ddf
updated: 1486069609
title: 'Conic Sections  Hyperbolas 3'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/XHPgDnJOkWM/default.jpg
    - https://i3.ytimg.com/vi/XHPgDnJOkWM/1.jpg
    - https://i3.ytimg.com/vi/XHPgDnJOkWM/2.jpg
    - https://i3.ytimg.com/vi/XHPgDnJOkWM/3.jpg
---
