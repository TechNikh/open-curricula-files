---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oscillon
offline_file: ""
offline_thumbnail: ""
uuid: 082f22db-0b6c-4470-a94e-65693f66f2ef
updated: 1484308707
title: Oscillon
categories:
    - Chaos theory
---
In physics, an oscillon is a soliton-like phenomenon that occurs in granular and other dissipative media. Oscillons in granular media result from vertically vibrating a plate with a layer of uniform particles placed freely on top. When the sinusoidal vibrations are of the correct amplitude and frequency and the layer of sufficient thickness, a localized wave, referred to as an oscillon, can be formed by locally disturbing the particles. This meta-stable state will remain for a long time (many hundreds of thousands of oscillations) in the absence of further perturbation. An oscillon changes form with each collision of the grain layer and the plate, switching between a peak that projects ...
