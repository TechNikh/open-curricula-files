---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Product_and_manufacturing_information
offline_file: ""
offline_thumbnail: ""
uuid: 338bd503-6c16-439d-a2c7-a4d8263ed5e9
updated: 1484309332
title: Product and manufacturing information
tags:
    - Uses and visualization
    - Communication deliverables
    - Standards
categories:
    - Technical drawing
---
Product and manufacturing information, also abbreviated PMI, conveys non-geometric attributes in 3D computer-aided design (CAD) and Collaborative Product Development systems necessary for manufacturing product components and assemblies. PMI may include geometric dimensions and tolerances, 3D annotation (text) and dimensions, surface finish, and material specifications. PMI is used in conjunction with the 3D model within model-based definition to allow for the elimination of 2D drawings for data set utilization.
