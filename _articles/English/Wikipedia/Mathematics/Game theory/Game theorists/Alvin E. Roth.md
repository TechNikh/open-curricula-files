---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alvin_E._Roth
offline_file: ""
offline_thumbnail: ""
uuid: 22b63542-8c0e-4ca5-93b5-aa851441e977
updated: 1484309025
title: Alvin E. Roth
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Al_Roth%252C_Sydney_Ideas_lecture_2012c.jpg'
tags:
    - Biography
    - Work
    - Case study in game theory
    - New York City public school system
    - "Boston's public school system"
    - New England Program for Kidney Exchange
    - Personal
    - Books
    - Journal articles
categories:
    - Game theorists
---
Alvin Elliot Roth (born December 18, 1951) is an American academician personality, he is the Craig and Susan McCaw professor of economics at Stanford University and the Gund professor of economics and business administration emeritus at Harvard University.[2]
