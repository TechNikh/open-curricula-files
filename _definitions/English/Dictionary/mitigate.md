---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mitigate
offline_file: ""
offline_thumbnail: ""
uuid: 210170f0-ea91-43c2-9464-ea8cedddac4c
updated: 1484310561
title: mitigate
categories:
    - Dictionary
---
mitigate
     v 1: lessen or to try to lessen the seriousness or extent of;
          "The circumstances extenuate the crime" [syn: {extenuate},
           {palliate}]
     2: make less severe or harsh; "mitigating circumstances"
