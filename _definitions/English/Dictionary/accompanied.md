---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accompanied
offline_file: ""
offline_thumbnail: ""
uuid: f3004c9c-23db-40e5-9628-80173da20013
updated: 1484310429
title: accompanied
categories:
    - Dictionary
---
accompanied
     adj : having accompaniment or companions or escort; "there were
           lone gentlemen and gentlemen accompanied by their
           wives" [syn: {attended}] [ant: {unaccompanied}]
