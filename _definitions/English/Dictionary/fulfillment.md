---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fulfillment
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484346721
title: fulfillment
categories:
    - Dictionary
---
fulfillment
     n 1: a feeling of satisfaction at having achieved your desires
          [syn: {fulfilment}]
     2: the act of consummating something (a desire or promise etc)
        [syn: {fulfilment}]
