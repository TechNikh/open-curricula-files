---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reconstruction
offline_file: ""
offline_thumbnail: ""
uuid: 7127e802-7c6c-49d1-8825-b3ebb9019e9f
updated: 1484310529
title: reconstruction
categories:
    - Dictionary
---
Reconstruction
     n 1: the period after the Civil War in the United States when the
          southern states were reorganized and reintegrated into
          the Union; 1865-1877 [syn: {Reconstruction Period}]
     2: the activity of constructing something again
     3: an interpretation formed by piecing together bits of
        evidence
     4: recall that is hypothesized to work by storing abstract
        features which are then used to construct the memory
        during recall [syn: {reconstructive memory}]
