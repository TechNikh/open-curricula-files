---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorial_optimization
offline_file: ""
offline_thumbnail: ""
uuid: 86279c6a-f285-416e-85ac-4e713b9a4045
updated: 1484308669
title: Combinatorial optimization
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Minimum_spanning_tree.svg.png
tags:
    - Applications
    - Methods
    - Specific problems
    - Notes
categories:
    - Theoretical computer science
---
In applied mathematics and theoretical computer science, combinatorial optimization is a topic that consists of finding an optimal object from a finite set of objects.[1] In many such problems, exhaustive search is not feasible. It operates on the domain of those optimization problems, in which the set of feasible solutions is discrete or can be reduced to discrete, and in which the goal is to find the best solution. Some common problems involving combinatorial optimization are the traveling salesman problem ("TSP") and the minimum spanning tree problem ("MST").
