---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Analytic_combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: bcf23912-7602-4279-8f1e-e5478ac08f79
updated: 1484308565
title: Analytic combinatorics
tags:
    - Classes of combinatorial structures
    - The Flajolet–Sedgewick fundamental theorem
    - |
        The sequence operator
        
        
        
        
        
        S
        
        
        
        
        {\displaystyle {\mathfrak {S}}}
    - |
        The cycle operator
        
        
        
        
        
        C
        
        
        
        
        {\displaystyle {\mathfrak {C}}}
    - |
        The multiset/set operator
        
        
        
        
        
        M
        
        
        
        /
        
        
        
        P
        
        
        
        
        {\displaystyle {\mathfrak {M}}/{\mathfrak {P}}}
    - Procedure
    - Combinatorial sum
    - Unlabelled structures
    - Product
    - Sequence
    - Set
    - Multiset
    - Other elementary constructions
    - Examples
    - Labelled structures
    - Product
    - Sequence
    - Set
    - cycle
    - Boxed product
    - Example
    - Other elementary constructions
    - Example
categories:
    - Combinatorics
---
In mathematics, analytic combinatorics is one of the many techniques of counting combinatorial objects. It uses the internal structure of the objects to derive formulas for their generating functions and then complex analysis techniques to get asymptotics. This particular theory was mostly developed by Philippe Flajolet[citation needed], and is detailed in his book with Robert Sedgewick, Analytic Combinatorics. Earlier contributors to the key ideas and techniques include Leonhard Euler, Arthur Cayley, Srinivasa Ramanujan, George Pólya, and Donald Knuth.
