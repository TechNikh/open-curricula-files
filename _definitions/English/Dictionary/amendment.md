---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amendment
offline_file: ""
offline_thumbnail: ""
uuid: 73abc75d-7c89-4f93-97ef-496aea5df158
updated: 1484310597
title: amendment
categories:
    - Dictionary
---
amendment
     n 1: the act of amending or correcting
     2: a statement that is added to or revises or improves a
        proposal or document (a bill or constitution etc.)
