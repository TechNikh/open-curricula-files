---
title: Diaphragmatic Breathing Technique
youtube_id: 0Ua9bOsZTYg
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - mechanism-of-respiration
---
