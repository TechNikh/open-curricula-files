---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scanty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484470621
title: scanty
categories:
    - Dictionary
---
scanty
     adj : lacking in amplitude or quantity; "a bare livelihood"; "a
           scanty harvest"; "a spare diet" [syn: {bare(a)}, {spare}]
     n : short underpants for women or children (usually used in the
         plural) [syn: {pantie}, {panty}, {step-in}]
     [also: {scantiest}, {scantier}]
