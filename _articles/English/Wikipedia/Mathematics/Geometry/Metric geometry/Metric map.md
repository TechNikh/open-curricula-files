---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metric_map
offline_file: ""
offline_thumbnail: ""
uuid: 5fac0e39-27e1-432a-82c7-b514af7cddb4
updated: 1484309269
title: Metric map
tags:
    - Category of metric maps
    - Strictly metric maps
    - Multivalued version
categories:
    - Metric geometry
---
In the mathematical theory of metric spaces, a metric map is a function between metric spaces that does not increase any distance (such functions are always continuous). These maps are the morphisms in the category of metric spaces, Met (Isbell 1964). They are also called Lipschitz functions with Lipschitz constant 1, nonexpansive maps, nonexpanding maps, weak contractions, or short maps.
