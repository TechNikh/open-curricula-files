---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ammeter
offline_file: ""
offline_thumbnail: ""
uuid: 7a83fadb-ea2c-4510-b674-41e751a960cf
updated: 1484310202
title: ammeter
categories:
    - Dictionary
---
ammeter
     n : a meter that measures the flow of electrical current in
         amperes
