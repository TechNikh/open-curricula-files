---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/browse
offline_file: ""
offline_thumbnail: ""
uuid: b9764d2e-b0e3-4e6e-b73d-5c3d16c0a5f0
updated: 1484310271
title: browse
categories:
    - Dictionary
---
browse
     n 1: reading superficially or at random [syn: {browsing}]
     2: the act of feeding by continual nibbling [syn: {browsing}]
     v 1: shop around; not necessarily buying; "I don't need help, I'm
          just browsing" [syn: {shop}]
     2: feed as in a meadow or pasture; "the herd was grazing" [syn:
         {crop}, {graze}, {range}, {pasture}]
     3: look around casually and randomly, without seeking anything
        in particular; "browse a computer directory"; "surf the
        internet or the world wide web" [syn: {surf}]
     4: eat lightly, try different dishes; "There was so much food
        at the party that we quickly got sated just by browsing"
        [syn: ...
