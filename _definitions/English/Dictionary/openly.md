---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/openly
offline_file: ""
offline_thumbnail: ""
uuid: 2f62c9e9-7418-4811-900d-980debd415fb
updated: 1484310575
title: openly
categories:
    - Dictionary
---
openly
     adv : in an open way; "he openly flaunted his affection for his
           sister"
