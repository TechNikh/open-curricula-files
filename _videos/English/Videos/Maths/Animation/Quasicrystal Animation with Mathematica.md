---
version: 1
type: video
provider: YouTube
id: W_ufqM-V84o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4a9fccc0-6c00-4052-aa94-4a89f09f42b5
updated: 1486069648
title: Quasicrystal Animation with Mathematica
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/W_ufqM-V84o/default.jpg
    - https://i3.ytimg.com/vi/W_ufqM-V84o/1.jpg
    - https://i3.ytimg.com/vi/W_ufqM-V84o/2.jpg
    - https://i3.ytimg.com/vi/W_ufqM-V84o/3.jpg
---
