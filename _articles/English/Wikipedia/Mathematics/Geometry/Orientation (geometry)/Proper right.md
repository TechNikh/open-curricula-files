---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proper_right
offline_file: ""
offline_thumbnail: ""
uuid: 081c73b3-a192-413a-85ca-3eb9cd7b79d4
updated: 1484309302
title: Proper right
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Charlemagne-by-Durer.jpg
categories:
    - Orientation (geometry)
---
Proper right and proper left are conceptual terms used to unambiguously convey relative direction when describing an image or other object. The "proper right" hand of a figure is the hand that would be regarded by that figure as its right hand.[1] In a frontal representation, that appears on the left as the viewer sees it, creating the potential for ambiguity if the hand is just described as the "right hand".
