---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reckless
offline_file: ""
offline_thumbnail: ""
uuid: 1d62c181-ae49-4eb6-b422-445c2fbefede
updated: 1484310581
title: reckless
categories:
    - Dictionary
---
reckless
     adj 1: marked by unthinking boldness; with defiant disregard for
            danger or consequences; "foolhardy enough to try to
            seize the gun from the hijacker"; "became the fiercest
            and most reckless of partisans"-Macaulay; "a reckless
            driver"; "a rash attempt to climb the World Trade
            Center" [syn: {foolhardy}, {rash}]
     2: characterized by careless unconcerned; "the heedless
        generosity and the spasmodic extravagance of persons used
        to large fortunes"- Edith Wharton; "reckless squandering
        of public funds" [syn: {heedless}]
