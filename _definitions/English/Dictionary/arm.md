---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arm
offline_file: ""
offline_thumbnail: ""
uuid: 7c82e18c-afed-4733-bbae-aa64048c5f8c
updated: 1484310286
title: arm
categories:
    - Dictionary
---
arm
     n 1: a human limb; technically the part of the superior limb
          between the shoulder and the elbow but commonly used to
          refer to the whole superior limb
     2: any instrument or instrumentality used in fighting or
        hunting; "he was licensed to carry a weapon" [syn: {weapon},
         {weapon system}]
     3: an administrative division of some larger or more complex
        organization; "a branch of Congress" [syn: {branch}, {subdivision}]
     4: any projection that is thought to resemble an arm; "the arm
        of the record player"; "an arm of the sea"; "a branch of
        the sewer" [syn: {branch}, {limb}]
     5: the part of an armchair or sofa that ...
