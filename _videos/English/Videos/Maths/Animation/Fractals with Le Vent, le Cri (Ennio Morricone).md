---
version: 1
type: video
provider: YouTube
id: 1SGKtunjQns
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b5caf045-f5e0-42ce-8a5b-b0f1dd03c122
updated: 1486069648
title: Fractals with Le Vent, le Cri (Ennio Morricone)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/1SGKtunjQns/default.jpg
    - https://i3.ytimg.com/vi/1SGKtunjQns/1.jpg
    - https://i3.ytimg.com/vi/1SGKtunjQns/2.jpg
    - https://i3.ytimg.com/vi/1SGKtunjQns/3.jpg
---
