---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Miguel_%C3%81ngel_Fern%C3%A1ndez_Sanju%C3%A1n'
offline_file: ""
offline_thumbnail: ""
uuid: 0a8e0382-7de9-434e-bf06-358e097b85b7
updated: 1484308719
title: Miguel Ángel Fernández Sanjuán
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Yorke_and_Miguel.jpg
tags:
    - Career
    - Awards
    - Writings
    - Research
categories:
    - Chaos theory
---
Miguel Angel Fernández Sanjuán (Miguel A. F. Sanjuán) is a Spanish Theoretical Physicist from Leon, Spain. He is known for his contributions in nonlinear dynamics, chaos theory, and control of chaos,[1] and has published several scientific papers and popular news articles. He has supervised around 20 PhD students in Nonlinear Dynamics, Chaos and Complex Systems.
