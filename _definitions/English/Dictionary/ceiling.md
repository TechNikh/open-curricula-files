---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ceiling
offline_file: ""
offline_thumbnail: ""
uuid: ad4ae940-6a3f-4de9-969a-33320578304c
updated: 1484310230
title: ceiling
categories:
    - Dictionary
---
ceiling
     n 1: the overhead upper surface of a room; "he hated painting the
          ceiling"
     2: (meteorology) altitude of the lowest layer of clouds
     3: an upper limit on what is allowed; "they established a cap
        for prices" [syn: {cap}]
     4: maximum altitude at which a plane can fly (under specified
        conditions)
