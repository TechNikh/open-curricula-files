---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/practice
offline_file: ""
offline_thumbnail: ""
uuid: efcfdb76-0e23-4f03-ac4b-600460ab67e0
updated: 1484310252
title: practice
categories:
    - Dictionary
---
practice
     n 1: a customary way of operation or behavior; "it is their
          practice to give annual raises"; "they changed their
          dietary pattern" [syn: {pattern}]
     2: systematic training by multiple repetitions; "practice makes
        perfect" [syn: {exercise}, {drill}, {practice session}, {recitation}]
     3: translating an idea into action; "a hard theory to put into
        practice"; "differences between theory and praxis of
        communism" [syn: {praxis}]
     4: the exercise of a profession; "the practice of the law"; "I
        took over his practice when he retired"
     5: knowledge of how something is usually done; "it is not the
        local practice ...
