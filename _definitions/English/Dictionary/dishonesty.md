---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dishonesty
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484627882
title: dishonesty
categories:
    - Dictionary
---
dishonesty
     n 1: the quality of being dishonest [ant: {honesty}]
     2: lack of honesty; acts of lying or cheating or stealing [syn:
         {knavery}]
