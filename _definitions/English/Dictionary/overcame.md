---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overcame
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484420641
title: overcame
categories:
    - Dictionary
---
overcome
     adj : rendered powerless especially by an excessive amount or
           profusion of something; "a desk flooded with
           applications"; "felt inundated with work"; "too much
           overcome to notice"; "a man engulfed by fear"; "swamped
           by work" [syn: {flooded}, {inundated}, {overpowered}, {overwhelmed},
            {swamped}, {engulfed}]
     v 1: win a victory over; "You must overcome all difficulties";
          "defeat your enemies"; "He overcame his shyness"; "She
          conquered here fear of mice"; "He overcame his
          infirmity"; "Her anger got the better of her and she
          blew up" [syn: {get the better of}, {defeat}]
     2: get ...
