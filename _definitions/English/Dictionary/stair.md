---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stair
offline_file: ""
offline_thumbnail: ""
uuid: 47a1c0a8-74c0-4edc-b077-1091188ca048
updated: 1484310297
title: stair
categories:
    - Dictionary
---
stair
     n : support consisting of a place to rest the foot while
         ascending or descending a stairway; "he paused on the
         bottom step" [syn: {step}]
