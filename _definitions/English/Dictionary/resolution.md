---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resolution
offline_file: ""
offline_thumbnail: ""
uuid: a831264b-8b5a-4554-a46a-5a2aa9389ad2
updated: 1484310389
title: resolution
categories:
    - Dictionary
---
resolution
     n 1: a formal expression by a meeting; agreed to by a vote [syn:
          {declaration}, {resolve}]
     2: the ability of a microscope or telescope to measure the
        angular separation of images that are close together [syn:
         {resolving power}]
     3: the trait of being resolute; firmness of purpose; "his
        resoluteness carried him through the battle"; "it was his
        unshakeable resolution to finish the work" [syn: {resoluteness},
         {firmness}, {resolve}] [ant: {irresoluteness}]
     4: finding a solution to a problem [syn: {solving}]
     5: something settled or resolved; the outcome of decision
        making; "the finally reached a ...
