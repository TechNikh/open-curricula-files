---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/executive
offline_file: ""
offline_thumbnail: ""
uuid: 975d721d-3e4d-4a73-94f0-5ff8bb0e489b
updated: 1484310585
title: executive
categories:
    - Dictionary
---
executive
     adj : having the function of carrying out plans or orders etc.;
           "the executive branch"
     n 1: a person responsible for the administration of a business
          [syn: {executive director}]
     2: persons who administer the law
     3: someone who manages a government agency or department [syn:
        {administrator}]
