---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shannon_switching_game
offline_file: ""
offline_thumbnail: ""
uuid: 84786845-a8cf-4511-85fe-729c01b197ba
updated: 1484308533
title: Shannon switching game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/175px-Shannon_game_graph.svg.png
tags:
    - Rules
    - Winning algorithms
categories:
    - Combinatorial game theory
---
The Shannon switching game is an abstract strategy game for two players, invented by Claude Shannon. It is commonly played on a rectangular grid; this special case of the game was independently invented by David Gale and is also known as Gale or Bridg-It.[1][2]
