---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reform
offline_file: ""
offline_thumbnail: ""
uuid: 59037757-7ae7-4063-a7c0-bb5720f299db
updated: 1484310561
title: reform
categories:
    - Dictionary
---
reform
     n 1: a change for the better as a result of correcting abuses;
          "justice was for sale before the reform of the law
          courts"
     2: a campaign aimed to correct abuses or malpractices; "the
        reforms he proposed were too radical for the politicians"
     3: self-improvement in behavior or morals by abandoning some
        vice; "the family rejoiced in the drunkard's reform"
     v 1: make changes for improvement in order to remove abuse and
          injustices; "reform a political system"
     2: bring, lead, or force to abandon a wrong or evil course of
        life, conduct, and adopt a right one; "The Church reformed
        me"; "reform your conduct" ...
