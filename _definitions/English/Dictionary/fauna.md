---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fauna
offline_file: ""
offline_thumbnail: ""
uuid: 29434137-be4e-47ef-8c9a-62209dc35d21
updated: 1484310289
title: fauna
categories:
    - Dictionary
---
fauna
     n 1: all the animal life in a particular region [ant: {vegetation}]
     2: a living organism characterized by voluntary movement [syn:
        {animal}, {animate being}, {beast}, {brute}, {creature}]
     [also: {faunae} (pl)]
