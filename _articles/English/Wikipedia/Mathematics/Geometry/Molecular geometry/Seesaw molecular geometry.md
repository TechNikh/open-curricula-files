---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Seesaw_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: ae0e781c-0c66-4d9c-b4df-d1ed6e045f80
updated: 1484309282
title: Seesaw molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Seesaw-3D-balls.png
tags:
    - Structure
    - Examples
categories:
    - Molecular geometry
---
Disphenoidal or Seesaw is a type of molecular geometry where there are four bonds to a central atom with overall C2v symmetry. The name "seesaw" comes from the observation that it looks like a playground seesaw. Most commonly, four bonds to a central atom result in tetrahedral or, less commonly, square planar geometry. The seesaw geometry, just like its name, is unusual.
