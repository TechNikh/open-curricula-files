---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/educate
offline_file: ""
offline_thumbnail: ""
uuid: f568fac8-e6e1-49e8-832c-33d31c0393a4
updated: 1484310158
title: educate
categories:
    - Dictionary
---
educate
     v 1: give an education to; "We must educate our youngsters
          better"
     2: create by training and teaching; "The old master is training
        world-class violinists"; "we develop the leaders for the
        future" [syn: {train}, {develop}, {prepare}]
     3: train to be discriminative in taste or judgment; "Cultivate
        your musical taste"; "Train your tastebuds"; "She is well
        schooled in poetry" [syn: {school}, {train}, {cultivate},
        {civilize}, {civilise}]
