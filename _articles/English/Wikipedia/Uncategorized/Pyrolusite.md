---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pyrolusite
offline_file: ""
offline_thumbnail: ""
uuid: fc29c065-03c3-4446-872b-c4680150d866
updated: 1484308389
title: Pyrolusite
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Pyrolusite_botryoidal.jpg
tags:
    - Occurrence
    - use
    - Dendritic manganese oxides
categories:
    - Uncategorized
---
Pyrolusite is a mineral consisting essentially of manganese dioxide (MnO2) and is important as an ore of manganese. It is a black, amorphous appearing mineral, often with a granular, fibrous or columnar structure, sometimes forming reniform crusts. It has a metallic luster, a black or bluish-black streak, and readily soils the fingers. The specific gravity is about 4.8. Its name is from the Greek for fire and to wash, in reference to its use as a way to remove tints from glass.[4]
