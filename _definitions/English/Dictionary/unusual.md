---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unusual
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484470861
title: unusual
categories:
    - Dictionary
---
unusual
     adj 1: not usual or common or ordinary; "a scene of unusual
            beauty"; "a man of unusual ability"; "cruel and
            unusual punishment"; "an unusual meteorite" [ant: {usual}]
     2: being definitely out of the ordinary and unexpected;
        slightly odd or even a bit weird; "a strange exaltation
        that was indefinable"; "a strange fantastical mind"; "what
        a strange sense of humor she has" [syn: {strange}] [ant: {familiar}]
     3: not commonly encountered; "two-career families are no longer
        unusual"
