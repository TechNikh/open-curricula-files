---
version: 1
type: video
provider: YouTube
id: bXS_gv6m00k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 659d7f1b-acbc-47f6-8afc-e8fa8771c733
updated: 1486069630
title: Use of Second Hindi Vowel Swar आ
categories:
    - Class 1
thumbnail_urls:
    - https://i3.ytimg.com/vi/bXS_gv6m00k/default.jpg
    - https://i3.ytimg.com/vi/bXS_gv6m00k/1.jpg
    - https://i3.ytimg.com/vi/bXS_gv6m00k/2.jpg
    - https://i3.ytimg.com/vi/bXS_gv6m00k/3.jpg
---
