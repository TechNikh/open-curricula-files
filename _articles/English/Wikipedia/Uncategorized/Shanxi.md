---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shanxi
offline_file: ""
offline_thumbnail: ""
uuid: 34d97249-bd1e-4923-a4b0-889281875277
updated: 1484308485
title: Shanxi
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/125px-Shanxi_%2528Chinese_characters%2529.svg.png'
tags:
    - History
    - Pre-Imperial China
    - Imperial China
    - Modern China
    - geography
    - Administrative divisions
    - Politics
    - Economy
    - Industrial zones
    - Taiyuan Economic and Technology Development Zone
categories:
    - Uncategorized
---
Shanxi (Chinese: 山西; pinyin:  Shānxī; postal: Shansi) is a province of China, located in the North China region. Its one-character abbreviation is "晋" (pinyin: Jìn), after the state of Jin that existed here during the Spring and Autumn period.
