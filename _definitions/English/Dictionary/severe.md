---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/severe
offline_file: ""
offline_thumbnail: ""
uuid: f0dbdbf5-a136-4131-892c-47af38fbf9f5
updated: 1484310353
title: severe
categories:
    - Dictionary
---
severe
     adj 1: intensely or extremely bad or unpleasant in degree or
            quality; "severe pain"; "a severe case of flu"; "a
            terrible cough"; "under wicked fire from the enemy's
            guns"; "a wicked cough" [syn: {terrible}, {wicked}]
     2: very strong or vigorous; "strong winds"; "a hard left to the
        chin"; "a knockout punch"; "a severe blow" [syn: {hard}, {knockout}]
     3: severely simple; "a stark interior" [syn: {austere}, {stark}]
     4: unsparing and uncompromising in discipline or judgment; "a
        parent severe to the pitch of hostility"- H.G.Wells; "a
        hefty six-footer with a rather severe mien"; "a strict
        disciplinarian"; ...
