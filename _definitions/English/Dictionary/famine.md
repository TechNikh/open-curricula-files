---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/famine
offline_file: ""
offline_thumbnail: ""
uuid: a3f8a29d-e6ea-4220-b3c6-bc9ab3ac8b69
updated: 1484310471
title: famine
categories:
    - Dictionary
---
famine
     n 1: an acute insufficiency [syn: {dearth}, {shortage}]
     2: a severe shortage of food (as through crop failure)
        resulting in violent hunger and starvation and death
