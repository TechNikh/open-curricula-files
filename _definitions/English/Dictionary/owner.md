---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/owner
offline_file: ""
offline_thumbnail: ""
uuid: 5e8b0340-5d8c-4bde-9e02-331b1fb75676
updated: 1484310451
title: owner
categories:
    - Dictionary
---
owner
     n 1: (law) someone who owns (is legal possessor of) a business;
          "he is the owner of a chain of restaurants" [syn: {proprietor}]
     2: a person who owns something; "they are searching for the
        owner of the car"; "who is the owner of that friendly
        smile?" [syn: {possessor}]
