---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adage
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484545021
title: adage
categories:
    - Dictionary
---
adage
     n : a condensed but memorable saying embodying some important
         fact of experience that is taken as true by many people
         [syn: {proverb}, {saw}, {byword}]
