---
title: Watershed (image processing)
article_ref: https://en.wikipedia.org/wiki/Watershed_(image_processing)
tags:
    - Definitions
    - Watershed by flooding
    - Watershed by topographic distance
    - Watershed by the drop of water principle
    - Inter-pixel watershed
    - Topological watershed
    - Algorithms
    - Meyer's flooding algorithm
    - Optimal spanning forest algorithms (watershed cuts)
    - Links with other algorithms in computer vision
    - Graph cuts
    - Shortest-path forests
    - Random walker
    - Hierarchies
categories:
    - Water Shed
---
