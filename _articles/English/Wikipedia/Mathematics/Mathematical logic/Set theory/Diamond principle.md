---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diamond_principle
offline_file: ""
offline_thumbnail: ""
uuid: 8a23dae8-cbc7-4066-b28a-0b14cf37e940
updated: 1484309504
title: Diamond principle
tags:
    - Definitions
    - Properties and use
categories:
    - Set theory
---
In mathematics, and particularly in axiomatic set theory, the diamond principle ◊ is a combinatorial principle introduced by Ronald Jensen in Jensen (1972) that holds in the constructible universe (L) and that implies the continuum hypothesis. Jensen extracted the diamond principle from his proof that the Axiom of constructibility (V=L) implies the existence of a Suslin tree.
