---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Infinitary_combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: 190bcab9-98bc-489e-b710-eca69d36832f
updated: 1484308580
title: Infinitary combinatorics
tags:
    - Ramsey theory for infinite sets
    - Large cardinals
    - Notes
categories:
    - Combinatorics
---
In mathematics, infinitary combinatorics, or combinatorial set theory, is an extension of ideas in combinatorics to infinite sets. Some of the things studied include continuous graphs and trees, extensions of Ramsey's theorem, and Martin's axiom. Recent developments concern combinatorics of the continuum[1] and combinatorics on successors of singular cardinals.[2]
