---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/content
offline_file: ""
offline_thumbnail: ""
uuid: 1c0f45c8-3c1f-49a1-9ef2-125dbb6a5f8f
updated: 1484310172
title: content
categories:
    - Dictionary
---
content
     adj : satisfied or showing satisfaction with things as they are;
           "a contented smile" [syn: {contented}] [ant: {discontented}]
     n 1: everything that is included in a collection; "he emptied the
          contents of his pockets"; "the two groups were similar
          in content"
     2: what a communication that is about something is about [syn:
        {message}, {subject matter}, {substance}]
     3: the proportion of a substance that is contained in a mixture
        or alloy etc.
     4: the amount that can be contained; "the gas tank has a
        capacity of 12 gallons" [syn: {capacity}]
     5: the sum or range of what has been perceived, discovered, or
   ...
