---
version: 1
type: video
provider: YouTube
id: USSy7cuZWmU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/6.5%20DNA%20Replication%20Fork.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f8f32108-4e01-469b-9e64-e84790a23d7b
updated: 1486069676
title: 6.5 DNA Replication Fork
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/USSy7cuZWmU/default.jpg
    - https://i3.ytimg.com/vi/USSy7cuZWmU/1.jpg
    - https://i3.ytimg.com/vi/USSy7cuZWmU/2.jpg
    - https://i3.ytimg.com/vi/USSy7cuZWmU/3.jpg
---
