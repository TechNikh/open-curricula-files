---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parliament
offline_file: ""
offline_thumbnail: ""
uuid: 290fc90e-f873-4d18-8595-1b105c401354
updated: 1484310475
title: parliament
categories:
    - Dictionary
---
parliament
     n 1: a legislative assembly in certain countries (e.g., Great
          Britain)
     2: a card game in which you play your sevens and other cards in
        sequence in the same suit as their sevens; you win if you
        are the first to use all your cards [syn: {fantan}, {sevens}]
