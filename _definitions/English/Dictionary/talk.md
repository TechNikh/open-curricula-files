---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/talk
offline_file: ""
offline_thumbnail: ""
uuid: dfb5afa8-2632-4869-97a2-a9cf03bd013d
updated: 1484310283
title: talk
categories:
    - Dictionary
---
talk
     n 1: an exchange of ideas via conversation; "let's have more work
          and less talk around here" [syn: {talking}]
     2: (`talk about' is a less formal alternative for `discussion
        of') discussion; "his poetry contains much talk about love
        and anger"
     3: the act of giving a talk to an audience; "I attended an
        interesting talk on local history"
     4: a speech that is open to the public; "he attended a lecture
        on telecommunications" [syn: {lecture}, {public lecture}]
     5: idle gossip or rumor; "there has been talk about you lately"
        [syn: {talk of the town}]
     v 1: exchange thoughts; talk with; "We often talk business";
       ...
