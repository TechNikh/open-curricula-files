---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Control_volume
offline_file: ""
offline_thumbnail: ""
uuid: f2525ccf-eff0-46c7-9909-6f42ac3a2b25
updated: 1484308834
title: Control volume
tags:
    - Overview
    - Substantive derivative
    - Notes
categories:
    - Thermodynamics
---
In continuum mechanics and thermodynamics, a control volume is a mathematical abstraction employed in the process of creating mathematical models of physical processes. In an inertial frame of reference, it is a volume fixed in space or moving with constant flow velocity through which the continuum (gas, liquid or solid) flows. The surface enclosing the control volume is referred to as the control surface.[1]
