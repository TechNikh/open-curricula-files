---
version: 1
type: video
provider: YouTube
id: bOLTCBTex-s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 83e8f2a4-965f-4bfa-ab39-cf62ceb81ef1
updated: 1486069647
title: Deeper Zoom Into Fractal Worlds
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/bOLTCBTex-s/default.jpg
    - https://i3.ytimg.com/vi/bOLTCBTex-s/1.jpg
    - https://i3.ytimg.com/vi/bOLTCBTex-s/2.jpg
    - https://i3.ytimg.com/vi/bOLTCBTex-s/3.jpg
---
