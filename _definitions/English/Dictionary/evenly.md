---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evenly
offline_file: ""
offline_thumbnail: ""
uuid: d8fdca8c-2320-4190-8e82-698758ca88e7
updated: 1484310339
title: evenly
categories:
    - Dictionary
---
evenly
     adv 1: in equal amounts or shares; in a balanced or impartial way;
            "a class evenly divided between girls and boys"; "they
            split their winnings equally"; "deal equally with rich
            and poor" [syn: {equally}] [ant: {unevenly}, {unevenly}]
     2: in a level and regular way [ant: {unevenly}]
