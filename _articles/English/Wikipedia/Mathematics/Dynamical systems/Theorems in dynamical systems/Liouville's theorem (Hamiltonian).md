---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Liouville%27s_theorem_(Hamiltonian)'
offline_file: ""
offline_thumbnail: ""
uuid: 17763ca5-c2ea-4892-ba79-7d77e90a24ab
updated: 1484308692
title: "Liouville's theorem (Hamiltonian)"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Hamiltonian_flow_classical.gif
tags:
    - Liouville equations
    - Other formulations
    - Poisson bracket
    - Ergodic theory
    - Symplectic geometry
    - Quantum Liouville equation
    - Remarks
categories:
    - Theorems in dynamical systems
---
In physics, Liouville's theorem, named after the French mathematician Joseph Liouville, is a key theorem in classical statistical and Hamiltonian mechanics. It asserts that the phase-space distribution function is constant along the trajectories of the system — that is that the density of system points in the vicinity of a given system point traveling through phase-space is constant with time. This time-independent density is in statistical mechanics known as the classical a priori probability.[1]
