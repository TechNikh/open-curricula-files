---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/right
offline_file: ""
offline_thumbnail: ""
uuid: 096f6a5b-73e4-4ac5-86b4-6d75da2ef8df
updated: 1484310319
title: right
categories:
    - Dictionary
---
right
     adj 1: free from error; especially conforming to fact or truth;
            "the correct answer"; "the correct version"; "the
            right answer"; "took the right road"; "the right
            decision" [syn: {correct}] [ant: {incorrect}, {incorrect}]
     2: being or located on or directed toward the side of the body
        to the east when facing north; "my right hand"; "right
        center field"; "a right-hand turn"; "the right bank of a
        river is the bank on your right side when you are facing
        downstream" [ant: {left}]
     3: socially right or correct; "it isn't right to leave the
        party without saying goodbye"; "correct behavior" [syn: ...
