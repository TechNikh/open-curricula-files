---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/traded
offline_file: ""
offline_thumbnail: ""
uuid: 35c3a184-1b84-41d1-8b88-129fc44d1045
updated: 1484310479
title: traded
categories:
    - Dictionary
---
traded
     adj : (of securities) bought and sold on a stock exchange
