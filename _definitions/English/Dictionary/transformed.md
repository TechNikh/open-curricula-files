---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transformed
offline_file: ""
offline_thumbnail: ""
uuid: 34d20e07-f719-42dc-aedd-680ee164201f
updated: 1484310477
title: transformed
categories:
    - Dictionary
---
transformed
     adj : given a completely different form or appearance; "shocked to
           see the transformed landscape"
