---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Link_analysis
offline_file: ""
offline_thumbnail: ""
uuid: 0585ae4a-5ac9-4179-9a23-947de711bda3
updated: 1484309435
title: Link analysis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Association_Matrix.png
tags:
    - Knowledge discovery
    - History
    - Applications
    - Issues with link analysis
    - Information overload
    - Prosecution vs. crime prevention
    - Proposed solutions
    - CrimeNet explorer
categories:
    - Network theory
---
In network theory, link analysis is a data-analysis technique used to evaluate relationships (connections) between nodes. Relationships may be identified among various types of nodes (objects), including organizations, people and transactions. Link analysis has been used for investigation of criminal activity (fraud detection, counterterrorism, and intelligence), computer security analysis, search engine optimization, market research, medical research, and art.
