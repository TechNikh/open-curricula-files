---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blend
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484638681
title: blend
categories:
    - Dictionary
---
blend
     n 1: an occurrence of thorough mixing
     2: a new word formed by joining two others and combining their
        meanings; "`smog' is a blend of `smoke' and `fog'";
        "`motel' is a portmanteau word made by combining `motor'
        and `hotel'"; "`brunch' is a well-known portmanteau" [syn:
         {portmanteau word}, {portmanteau}]
     3: the act of blending components together thoroughly [syn: {blending}]
     v 1: combine into one; "blend the nuts and raisins together"; "he
          blends in with the crowd"; "We don't intermingle much"
          [syn: {intermix}, {immingle}, {intermingle}]
     2: blend or harmonize; "This flavor will blend with those in
        your ...
