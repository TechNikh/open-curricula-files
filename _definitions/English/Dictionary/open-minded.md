---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/open-minded
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484584681
title: open-minded
categories:
    - Dictionary
---
open-minded
     adj : ready to entertain new ideas; "an open-minded curiosity";
           "open-minded impartiality"
