---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chenchu
offline_file: ""
offline_thumbnail: ""
uuid: 51acba67-6a70-44a9-83a9-9067eea70c6f
updated: 1484308333
title: Chenchu people
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Mandal_Parishad_Primary_School%252C_Chelama.jpg'
tags:
    - Footnotes
categories:
    - Uncategorized
---
The Chenchus are a designated Scheduled Tribe in the Indian states of Andhra Pradesh, Telangana, Karnataka and Odisha.[1] They are an aboriginal tribe whose traditional way of life has been based on hunting and gathering. The Chenchus speak the Chenchu language, a member of the Dravidian language family. In general, the Chenchu relationship to non-tribal people has been largely symbiotic. Some Chenchus have continued to specialize in collecting forest products for sale to non-tribal people. Many Chenchus live in the dense Nallamala forest of Andhra Pradesh.
