---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dosa
offline_file: ""
offline_thumbnail: ""
uuid: 9e5eb19a-242b-448c-988d-9ecabb6279d1
updated: 1484308428
title: Dosa
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Masala_Dosa_as_served_in_Tamil_Nadu%252CIndia.JPG'
tags:
    - History
    - Names
    - Nutrition
    - Preparation
    - Serving
    - Variations
    - Masala dosa
    - Related foods
categories:
    - Uncategorized
---
Dosa is a type of pancake made from a fermented batter. It is not a crepe. Its main ingredients are rice and black gram. Dosa is a typical part of the South Indian diet and popular all over the Indian subcontinent. Traditionally, Dosa is served hot along with sambar and chutney. It can be consumed with idli podi as well.
