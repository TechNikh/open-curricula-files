---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tube
offline_file: ""
offline_thumbnail: ""
uuid: dd0bff4f-0c33-4fc5-bd44-d25f1678672a
updated: 1484310335
title: tube
categories:
    - Dictionary
---
tube
     n 1: conduit consisting of a long hollow object (usually
          cylindrical) used to hold and conduct objects or liquids
          or gases [syn: {tubing}]
     2: electronic device consisting of a system of electrodes
        arranged in an evacuated glass or metal envelope [syn: {vacuum
        tube}, {thermionic vacuum tube}, {thermionic tube}, {electron
        tube}, {thermionic valve}]
     3: a hollow cylindrical shape [syn: {pipe}]
     4: (anatomy) any hollow cylindrical body structure [syn: {tube-shaped
        structure}]
     5: electric underground railway [syn: {metro}, {subway}, {underground}]
     v 1: provide with a tube or insert a tube into
     2: convey in ...
