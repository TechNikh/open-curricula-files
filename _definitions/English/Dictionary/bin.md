---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bin
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484595902
title: bin
categories:
    - Dictionary
---
bin
     n 1: a container; usually has a lid
     2: the quantity contained in a bin [syn: {binful}]
     3: an identification number consisting of a two-part code
        assigned to banks and savings associations; the first part
        shows the location and the second identifies the bank
        itself [syn: {bank identification number}, {ABA transit
        number}]
     v : store in bins
     [also: {binning}, {binned}]
