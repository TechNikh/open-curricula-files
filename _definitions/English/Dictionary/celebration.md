---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/celebration
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484633881
title: celebration
categories:
    - Dictionary
---
celebration
     n 1: a joyful occasion for special festivities to mark some happy
          event [syn: {jubilation}]
     2: any joyous diversion [syn: {festivity}]
     3: the public performance of a sacrament or solemn ceremony
        with all appropriate ritual; "the celebration of marriage"
        [syn: {solemnization}, {solemnisation}]
