---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crossing_number_inequality
offline_file: ""
offline_thumbnail: ""
uuid: 93c85fc4-28c6-4e70-8221-2f2fde78f505
updated: 1484309479
title: Crossing number inequality
tags:
    - Statement and history
    - Applications
    - Proof
    - Variations
categories:
    - Topological graph theory
---
In the mathematics of graph drawing, the crossing number inequality or crossing lemma gives a lower bound on the minimum number of crossings of a given graph, as a function of the number of edges and vertices of the graph. It states that, for graphs where the number e of edges is sufficiently larger than the number n of vertices, the crossing number is at least proportional to e3/n2.
