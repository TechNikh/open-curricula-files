---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leaf
offline_file: ""
offline_thumbnail: ""
uuid: e5c30955-d639-438b-9258-5aefff1372a4
updated: 1484310325
title: leaf
categories:
    - Dictionary
---
leaf
     n 1: the main organ of photosynthesis and transpiration in higher
          plants [syn: {leafage}, {foliage}]
     2: a sheet of any written or printed material (especially in a
        manuscript or book) [syn: {folio}]
     3: hinged or detachable flat section (as of a table or door)
     v 1: look through a book or other written material; "He thumbed
          through the report"; "She leafed through the volume"
          [syn: {flick}, {flip}, {thumb}, {riffle}, {riff}]
     2: turn over pages; "leaf through a book"; "leaf a manuscript"
     3: produce leaves, of plants
     [also: {leaves} (pl)]
