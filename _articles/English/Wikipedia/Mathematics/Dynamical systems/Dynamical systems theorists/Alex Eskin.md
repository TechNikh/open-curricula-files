---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alex_Eskin
offline_file: ""
offline_thumbnail: ""
uuid: f3184222-204e-4e47-822f-343f93aeff5b
updated: 1484308684
title: Alex Eskin
categories:
    - Dynamical systems theorists
---
Alex Eskin (born May 19, 1965) is an American mathematician, born in the former USSR. He works on rational billiards and geometric group theory. For his contribution to joint work with David Fisher and Kevin Whyte establishing the quasi-isometric rigidity of sol, he was awarded the 2007 Clay Research Award.[1]
