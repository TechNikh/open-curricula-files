---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trough
offline_file: ""
offline_thumbnail: ""
uuid: 84cda58b-8e16-4f25-9f34-42a351614d25
updated: 1484310379
title: trough
categories:
    - Dictionary
---
trough
     n 1: a narrow depression (as in the earth or between ocean waves
          or in the ocean bed)
     2: a channel along the eaves or on the roof; collects and
        carries away rainwater [syn: {gutter}]
     3: a concave shape with an open top [syn: {bowl}]
     4: a treasury for government funds [syn: {public treasury}, {till}]
     5: a long narrow shallow receptacle
     6: a container (usually in a barn or stable) from which cattle
        or horses feed [syn: {manger}]
