---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/generally
offline_file: ""
offline_thumbnail: ""
uuid: 1c0d3bf3-94c7-4392-9ed2-a470781ada23
updated: 1484310351
title: generally
categories:
    - Dictionary
---
generally
     adv 1: usually; as a rule; "by and large it doesn't rain much here"
            [syn: {by and large}, {more often than not}, {mostly}]
     2: without distinction of one from others; "he is interested in
        snakes in general" [syn: {in general}, {in the main}]
        [ant: {specifically}]
     3: without regard to specific details or exceptions; "he
        interprets the law broadly" [syn: {broadly}, {loosely}, {broadly
        speaking}] [ant: {narrowly}]
