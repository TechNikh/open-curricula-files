---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Great-circle_distance
offline_file: ""
offline_thumbnail: ""
uuid: 53f9dc3a-f3b7-406f-bde8-3ef5f3a07491
updated: 1484309268
title: Great-circle distance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Illustration_of_great-circle_distance.svg.png
tags:
    - Formulas
    - Computational formulas
    - Vector version
    - From chord length
    - Radius for spherical Earth
    - References and notes
categories:
    - Metric geometry
---
The great-circle distance or orthodromic distance is the shortest distance between two points on the surface of a sphere, measured along the surface of the sphere (as opposed to a straight line through the sphere's interior). The distance between two points in Euclidean space is the length of a straight line between them, but on the sphere there are no straight lines. In non-Euclidean geometry, straight lines are replaced by geodesics. Geodesics on the sphere are the great circles (circles on the sphere whose centers coincide with the center of the sphere).
