---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Monty_Hall_problem
offline_file: ""
offline_thumbnail: ""
uuid: 88a6ca1c-00af-42c1-963a-353e4a2b2ce3
updated: 1484309046
title: Monty Hall problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Monty_open_door.svg.png
tags:
    - The paradox
    - Standard assumptions
    - Simple solutions
    - Vos Savant and the media furor
    - Confusion and criticism
    - Sources of confusion
    - Solutions using conditional probability and other solutions
    - Refining the simple solution
    - Conditional probability by direct calculation
    - "Bayes' theorem"
    - Direct calculation
    - Strategic dominance solution
    - Solutions by simulation
    - Criticism of the simple solutions
    - Variants
    - Other host behaviors
    - N doors
    - Quantum version
    - History
categories:
    - Games (probability)
---
The Monty Hall problem is a brain teaser, in the form of a probability puzzle (Gruber, Krauss and others), loosely based on the American television game show Let's Make a Deal and named after its original host, Monty Hall. The problem was originally posed in a letter by Steve Selvin to the American Statistician in 1975 (Selvin 1975a), (Selvin 1975b). It became famous as a question from a reader's letter quoted in Marilyn vos Savant's "Ask Marilyn" column in Parade magazine in 1990 (vos Savant 1990a):
