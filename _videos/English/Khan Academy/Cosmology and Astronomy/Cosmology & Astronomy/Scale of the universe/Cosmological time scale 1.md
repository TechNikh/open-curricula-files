---
version: 1
type: video
provider: YouTube
id: DRtLXagrMHw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Cosmological%20time%20scale%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2846e721-4167-4b43-9616-961d1863f8dd
updated: 1486069685
title: Cosmological time scale 1
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/DRtLXagrMHw/default.jpg
    - https://i3.ytimg.com/vi/DRtLXagrMHw/1.jpg
    - https://i3.ytimg.com/vi/DRtLXagrMHw/2.jpg
    - https://i3.ytimg.com/vi/DRtLXagrMHw/3.jpg
---
