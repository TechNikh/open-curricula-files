---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tellurium
offline_file: ""
offline_thumbnail: ""
uuid: b7798b41-737b-49a1-aa20-818e6e7c2450
updated: 1484308380
title: Tellurium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Tellurium2.jpg
tags:
    - characteristics
    - Physical properties
    - Chemical properties
    - Isotopes
    - Occurrence
    - History
    - Production
    - Compounds
    - Applications
    - Metallurgy
    - Semiconductor and electronic industry uses
    - Other uses
    - Biological role
    - Precautions
categories:
    - Uncategorized
---
Tellurium is a chemical element with symbol Te and atomic number 52. It is a brittle, mildly toxic, rare, silver-white metalloid. Tellurium is chemically related to selenium and sulfur. It is occasionally found in native form as elemental crystals. Tellurium is far more common in the universe as a whole than on Earth. Its extreme rarity in the Earth's crust, comparable to that of platinum, is due partly to its high atomic number, but also to its formation of a volatile hydride which caused it to be lost to space as a gas during the hot nebular formation of the planet.
