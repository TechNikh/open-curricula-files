---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/describe
offline_file: ""
offline_thumbnail: ""
uuid: 2371f93b-b198-40cc-a021-000eee62a0a1
updated: 1484310316
title: describe
categories:
    - Dictionary
---
describe
     v 1: give a description of; "He drew an elaborate plan of attack"
          [syn: {depict}, {draw}]
     2: to give an account or representation of in words; "Discreet
        Italian police described it in a manner typically
        continental" [syn: {report}, {account}]
     3: identify as in botany or biology, for example [syn: {identify},
         {discover}, {key}, {key out}, {distinguish}, {name}]
     4: make a mark or lines on a surface; "draw a line"; "trace the
        outline of a figure in the sand" [syn: {trace}, {draw}, {line},
         {delineate}]
