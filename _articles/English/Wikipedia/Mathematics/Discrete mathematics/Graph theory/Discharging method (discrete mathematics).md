---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Discharging_method_(discrete_mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 5855ef63-2201-479b-b858-7217ffffa7ac
updated: 1484308611
title: Discharging method (discrete mathematics)
categories:
    - Graph theory
---
The discharging method is a technique used to prove lemmas in structural graph theory. Discharging is most well known for its central role in the proof of the Four Color Theorem. The discharging method is used to prove that every graph in a certain class contains some subgraph from a specified list. The presence of the desired subgraph is then often used to prove a coloring result.
