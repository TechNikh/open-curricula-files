---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Anning_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: d6bf08e6-2d6c-4c56-be30-0ea662495df5
updated: 1484308669
title: Erdős–Anning theorem
tags:
    - Rationality versus integrality
    - Proof
    - Maximal point sets with integral distances
categories:
    - Theorems in discrete mathematics
---
The Erdős–Anning theorem states that an infinite number of points in the plane can have mutual integer distances only if all the points lie on a straight line. It is named after Paul Erdős and Norman H. Anning, who published a proof of it in 1945.[1]
