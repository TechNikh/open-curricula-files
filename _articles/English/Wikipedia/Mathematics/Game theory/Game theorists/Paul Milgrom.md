---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paul_Milgrom
offline_file: ""
offline_thumbnail: ""
uuid: 20ae2389-2027-47a1-8ac0-27fe6a8e65af
updated: 1484308506
title: Paul Milgrom
tags:
    - Biography
    - Academic career
    - Personal life
    - Research
    - Context and Overview
    - Game theory
    - Reputation Formation
    - Distributional Strategies
    - Repeated Games
    - Supermodular Games
    - Learning in Games
    - Comparative Statics
    - Market Design
    - Auction Theory
    - Matching Theory
    - Simplifying Participants’ Messages
    - Organizational and Information Economics
    - Agency Theory
    - Information economics
    - Organizational Economics
    - Industrial Organization
    - Law, Institutions and Economic History
    - Finance and Macroeconomics
    - Securities Markets
    - Labor Markets
    - Policy
    - FCC Spectrum Auction 1993
    - FCC Incentive Auctions
    - Teaching
    - Business
    - Publications (Selected)
categories:
    - Game theorists
---
Paul Robert Milgrom (born April 20, 1948 in Detroit, Michigan) is an American economist. He is the Shirley and Leonard Ely Professor of Humanities and Sciences at Stanford University, a position he has held since 1987. Professor Milgrom is an expert in game theory, specifically auction theory and pricing strategies. He is the co-creator of the no-trade theorem with Nancy Stokey. He is the co-founder of several companies, the most recent of which, Auctionomics,[1] provides software and services that create efficient markets for complex commercial auctions and exchanges.
