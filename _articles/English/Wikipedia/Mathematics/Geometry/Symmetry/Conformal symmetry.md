---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conformal_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: a582cc4f-e00e-44bb-aa04-153076000ece
updated: 1484309305
title: Conformal symmetry
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/288px-Conformal_grid_before_M%25C3%25B6bius_transformation.svg.png'
tags:
    - Generators and commutation relations
    - Uses
categories:
    - Symmetry
---
In mathematical physics, the conformal symmetry of spacetime is expressed by an extension of the Poincaré group. The extension includes special conformal transformations and dilations. In three spatial plus one time dimensions, conformal symmetry has 15 degrees of freedom: ten for the Poincaré group, four for special conformal transformations, and one for a dilation.
