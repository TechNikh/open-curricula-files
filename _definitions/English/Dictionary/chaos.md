---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chaos
offline_file: ""
offline_thumbnail: ""
uuid: c813791f-73e9-4b6a-bec0-d88f9c575893
updated: 1484310597
title: chaos
categories:
    - Dictionary
---
chaos
     n 1: a state of extreme confusion and disorder [syn: {pandemonium},
           {bedlam}, {topsy-turvydom}, {topsy-turvyness}]
     2: the formless and disordered state of matter before the
        creation of the cosmos
     3: (Greek mythology) the most ancient of gods; the
        personification of the infinity of space preceding
        creation of the universe
     4: (physics) a dynamical system that is extremely sensitive to
        its initial conditions
