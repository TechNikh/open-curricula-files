---
version: 1
type: video
provider: YouTube
id: FR4JZBBaetA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0d82d6e1-1e52-4216-8c9f-a1194af490c0
updated: 1486069635
title: >
    Amazing lasers reflect off convex lens ray trace fog.
    (Science experiment) ASMR
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/FR4JZBBaetA/default.jpg
    - https://i3.ytimg.com/vi/FR4JZBBaetA/1.jpg
    - https://i3.ytimg.com/vi/FR4JZBBaetA/2.jpg
    - https://i3.ytimg.com/vi/FR4JZBBaetA/3.jpg
---
