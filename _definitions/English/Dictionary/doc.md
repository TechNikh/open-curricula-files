---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/doc
offline_file: ""
offline_thumbnail: ""
uuid: f30099bd-c093-4048-9158-2674a022102a
updated: 1484310577
title: doc
categories:
    - Dictionary
---
doc
     n 1: a licensed medical practitioner; "I felt so bad I went to
          see my doctor" [syn: {doctor}, {physician}, {MD}, {Dr.},
           {medico}]
     2: the United States federal department that promotes and
        administers domestic and foreign trade (including
        management of the census and the patent office); created
        in 1913 [syn: {Department of Commerce}, {Commerce
        Department}, {Commerce}]
