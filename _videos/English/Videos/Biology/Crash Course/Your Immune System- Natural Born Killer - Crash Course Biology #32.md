---
version: 1
type: video
provider: YouTube
id: CeVtPDjJBPU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a9daa9ad-79f3-4ad5-a2b5-0e5efef69116
updated: 1486069678
title: 'Your Immune System- Natural Born Killer - Crash Course Biology #32'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/CeVtPDjJBPU/default.jpg
    - https://i3.ytimg.com/vi/CeVtPDjJBPU/1.jpg
    - https://i3.ytimg.com/vi/CeVtPDjJBPU/2.jpg
    - https://i3.ytimg.com/vi/CeVtPDjJBPU/3.jpg
---
