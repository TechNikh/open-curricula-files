---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ruler
offline_file: ""
offline_thumbnail: ""
uuid: d7484a69-0cdd-4d10-bcc4-88a5970d7a6e
updated: 1484310607
title: ruler
categories:
    - Dictionary
---
ruler
     n 1: measuring stick consisting of a strip of wood or metal or
          plastic with a straight edge that is used for drawing
          straight lines and measuring lengths [syn: {rule}]
     2: a person who rules or commands; "swayer of the universe"
        [syn: {swayer}]
