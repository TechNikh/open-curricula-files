---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Multiplicity_function_for_N_noninteracting_spins
offline_file: ""
offline_thumbnail: ""
uuid: ac79222d-1228-4b09-9401-88f98b2d3f88
updated: 1484308857
title: Multiplicity function for N noninteracting spins
categories:
    - Thermodynamics
---
The multiplicity function for a two state paramagnet, W(n,N), is the number of spin states such that n of the N spins point in the z-direction. This function is given by the combinatoric function C(N,n). That is:
