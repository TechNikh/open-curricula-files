---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Three_hares
offline_file: ""
offline_thumbnail: ""
uuid: 29485864-60d8-4992-a3bd-cfcd8696fd8d
updated: 1484309322
title: Three hares
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Paderborner_Dom_Dreihasenfenster.jpg
tags:
    - Origins in Buddhism and diffusion on the Silk Road
    - In Christianity
    - In Judaism
    - As an optical illusion or puzzle
    - Other uses and related designs
    - Footnotes
    - Citations
categories:
    - Symmetry
---
The three hares is a circular motif appearing in sacred sites from the Middle and Far East to the churches of Devon, England (as the "Tinners' Rabbits"),[1] and historical synagogues in Europe.[2] It is used as an architectural ornament, a religious symbol, and in other modern works of art[3][4] or a logo for adornment (including tattoos),[5] jewelry and a coat of arms on an escutcheon.[6][7] It is viewed as a puzzle, a topology problem or a visual challenge, and has been rendered as sculpture, drawing, and painting.[citation needed]
