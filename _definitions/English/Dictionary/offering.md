---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/offering
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484568481
title: offering
categories:
    - Dictionary
---
offering
     n 1: something offered (as a proposal or bid); "noteworthy new
          offerings for investors included several index funds"
          [syn: {offer}]
     2: money contributed to a religious organization
     3: the verbal act of offering; "a generous offer of assistance"
        [syn: {offer}]
     4: the act of contributing to the funds of a church or charity;
        "oblations for aid to the poor" [syn: {oblation}]
