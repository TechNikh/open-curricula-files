---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surprise
offline_file: ""
offline_thumbnail: ""
uuid: 590b36b5-8baf-428b-bcc9-4912d9273d08
updated: 1484310309
title: surprise
categories:
    - Dictionary
---
surprise
     n 1: the astonishment you feel when something totally unexpected
          happens to you
     2: a sudden unexpected event
     3: the act of surprising someone [syn: {surprisal}]
     v 1: cause to be surprised; "The news really surprised me"
     2: come upon or take unawares; "She surprised the couple"; "He
        surprised an interesting scene"
     3: attack by storm; attack suddenly [syn: {storm}]
