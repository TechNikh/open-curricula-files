---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Joule_expansion
offline_file: ""
offline_thumbnail: ""
uuid: 02f62b7e-f6ce-4a93-b62b-64711ae8034c
updated: 1484308845
title: Joule expansion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-JouleExpansion.svg.png
tags:
    - Description
    - Ideal gases
    - Real gases
    - Entropy production
    - Real-gas effect
categories:
    - Thermodynamics
---
The Joule expansion is an irreversible process in thermodynamics in which a volume of gas is kept in one side of a thermally isolated container (via a small partition), with the other side of the container being evacuated. The partition between the two parts of the container is then opened, and the gas fills the whole container.
