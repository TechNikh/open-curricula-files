---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gangue
offline_file: ""
offline_thumbnail: ""
uuid: 91317ad4-de84-4895-8452-1579f765aac6
updated: 1484308389
title: Gangue
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cassiterite_-_Mt_Bischoff_mine%252C_Waratah%252C_Tasmania%252C_Australia.jpg'
categories:
    - Uncategorized
---
In mining, gangue (/ɡæŋ/)[1] is the commercially worthless material that surrounds, or is closely mixed with, a wanted mineral in an ore deposit. It is thus distinct from overburden, which is the waste rock or materials overlying an ore or mineral body that are displaced during mining without being processed.
