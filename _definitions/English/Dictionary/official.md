---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/official
offline_file: ""
offline_thumbnail: ""
uuid: 84023779-9f98-439f-b032-499902c30781
updated: 1484310462
title: official
categories:
    - Dictionary
---
official
     adj 1: having official authority or sanction; "official
            permission"; "an official representative" [ant: {unofficial}]
     2: of or relating to an office; "official privileges"
     3: verified officially; "the election returns are now official"
     4: conforming to set usage, procedure, or discipline; "in
        prescribed order" [syn: {prescribed}]
     5: (of a church) given official status as a national or state
        institution
     n 1: a worker who holds or is invested with an office [syn: {functionary}]
     2: someone who administers the rules of a game or sport; "the
        golfer asked for an official who could give him a ruling"
