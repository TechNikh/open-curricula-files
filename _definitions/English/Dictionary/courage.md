---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/courage
offline_file: ""
offline_thumbnail: ""
uuid: a02069eb-f26d-4f8e-afc4-4fea11f82cd6
updated: 1484310561
title: courage
categories:
    - Dictionary
---
courage
     n : a quality of spirit that enables you to face danger of pain
         without showing fear [syn: {courageousness}, {bravery}]
         [ant: {cowardice}]
