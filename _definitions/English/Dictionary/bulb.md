---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bulb
offline_file: ""
offline_thumbnail: ""
uuid: c6ae47a8-2c4f-4c39-ad7c-63c91c9ceb52
updated: 1484310230
title: bulb
categories:
    - Dictionary
---
bulb
     n 1: a modified bud consisting of a thickened globular
          underground stem serving as a reproductive structure
     2: electric lamp consisting of a glass bulb containing a wire
        filament (usually tungsten) that emits light when heated
        [syn: {light bulb}, {lightbulb}, {incandescent lamp}, {electric
        light}, {electric-light bulb}]
     3: a rounded part of a cylindrical instrument (usually at one
        end); "the bulb of a syringe"
     4: lower or hindmost part of the brain; continuous with spinal
        cord; (`bulb' is an old term for medulla oblongata); "the
        medulla oblongata is the most vital part of the brain
        because it contains ...
