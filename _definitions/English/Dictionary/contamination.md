---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contamination
offline_file: ""
offline_thumbnail: ""
uuid: b20b4898-9c02-42c8-a8ec-4798e1b4bce9
updated: 1484310539
title: contamination
categories:
    - Dictionary
---
contamination
     n 1: the state of being contaminated [syn: {taint}]
     2: a substance that contaminates [syn: {contaminant}]
     3: the act of contaminating or polluting; including (either
        intentionally or accidentally) unwanted substances or
        factors [syn: {pollution}] [ant: {decontamination}]
