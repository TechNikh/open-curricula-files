---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lean
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484410801
title: lean
categories:
    - Dictionary
---
lean
     adj 1: lacking excess flesh; "you can't be too rich or too thin";
            "Yon Cassius has a lean and hungry look"-Shakespeare
            [syn: {thin}] [ant: {fat}]
     2: lacking in mineral content or combustible material; "lean
        ore"; "lean fuel" [ant: {rich}]
     3: containing little excess; "a lean budget"; "a skimpy
        allowance" [syn: {skimpy}]
     4: low in mineral content; "a lean ore"
     5: not profitable or prosperous; "a lean year"
     n : the property possessed by a line or surface that departs
         from the vertical; "the tower had a pronounced tilt";
         "the ship developed a list to starboard"; "he walked with
         a heavy ...
