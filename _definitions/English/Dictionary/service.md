---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/service
offline_file: ""
offline_thumbnail: ""
uuid: 2a842244-f545-4f33-b888-91cf70b21263
updated: 1484310451
title: service
categories:
    - Dictionary
---
service
     n 1: work done by one person or group that benefits another;
          "budget separately for goods and services"
     2: a company or agency that performs a public service; subject
        to government regulation
     3: the act of public worship following prescribed rules; "the
        Sunday service" [syn: {religious service}, {divine service}]
     4: an act of help or assistance; "he did them a service" [ant:
        {disservice}]
     5: employment in or work for another; "he retired after 30
        years of service"
     6: a force that is a branch of the armed forces [syn: {military
        service}, {armed service}]
     7: the performance of duties by a waiter or ...
