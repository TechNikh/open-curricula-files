---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sunlight
offline_file: ""
offline_thumbnail: ""
uuid: 469dec51-558d-4246-8435-2f8f7f7d16a7
updated: 1484310275
title: sunlight
categories:
    - Dictionary
---
sunlight
     n : the rays of the sun; "the shingles were weathered by the sun
         and wind" [syn: {sunshine}, {sun}]
