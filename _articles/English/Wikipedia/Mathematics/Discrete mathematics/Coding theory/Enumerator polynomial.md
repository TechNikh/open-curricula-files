---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Enumerator_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: e09d6014-b134-4148-adb3-044af346287f
updated: 1484308551
title: Enumerator polynomial
tags:
    - Basic properties
    - MacWilliams identity
    - Distance enumerator
categories:
    - Coding theory
---
Let 
  
    
      
        C
        ⊂
        
          
            F
          
          
            2
          
          
            n
          
        
      
    
    {\displaystyle C\subset \mathbb {F} _{2}^{n}}
  
 be a binary linear code length 
  
    
      
        n
      
    
    {\displaystyle n}
  
. The weight distribution is the sequence of numbers
