---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cumin
offline_file: ""
offline_thumbnail: ""
uuid: 6531ed63-079f-48e6-9c53-5bc5bafb2de7
updated: 1484310346
title: cumin
categories:
    - Dictionary
---
cumin
     n 1: dwarf Mediterranean annual long cultivated for its aromatic
          seeds [syn: {Cuminum cyminum}]
     2: aromatic seeds of the cumin herb of the carrot family [syn:
        {cumin seed}]
