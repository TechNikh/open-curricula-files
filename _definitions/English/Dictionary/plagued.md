---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plagued
offline_file: ""
offline_thumbnail: ""
uuid: 0686a87c-233b-4c2b-b97e-94fe0abcc42b
updated: 1484310581
title: plagued
categories:
    - Dictionary
---
plagued
     adj : (often followed by `with' or used in combination) troubled
           by or encroached upon in large numbers; "waters
           infested with sharks"; "shark-infested waters"; "the
           locust-overrun countryside"; "drug-plagued streets"
           [syn: {infested}, {overrun}]
