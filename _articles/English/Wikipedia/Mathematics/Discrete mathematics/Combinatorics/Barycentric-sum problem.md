---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Barycentric-sum_problem
offline_file: ""
offline_thumbnail: ""
uuid: 5f47583b-9c33-4686-85cf-6dec009fb931
updated: 1484308565
title: Barycentric-sum problem
categories:
    - Combinatorics
---
Combinatorial number theory deals with number theoretic problems which involve combinatorial ideas in their formulations or solutions. Paul Erdős is the main founder of this branch of number theory. Typical topics include covering system, zero-sum problems, various restricted sumsets, and arithmetic progressions in a set of integers. Algebraic or analytic methods are powerful in this field.
