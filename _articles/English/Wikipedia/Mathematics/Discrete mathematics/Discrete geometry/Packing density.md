---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Packing_density
offline_file: ""
offline_thumbnail: ""
uuid: 530c0f17-2681-41ee-8d82-ac3aa7375781
updated: 1484308506
title: Packing density
tags:
    - In compact spaces
    - In Euclidean space
    - Optimal packing density
categories:
    - Discrete geometry
---
A packing density or packing fraction of a packing in some space is the fraction of the space filled by the figures making up the packing. In packing problems, the objective is usually to obtain a packing of the greatest possible density.
