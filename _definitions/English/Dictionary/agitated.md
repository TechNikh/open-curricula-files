---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agitated
offline_file: ""
offline_thumbnail: ""
uuid: 36b062b2-1caf-4762-8e8d-f9b9c550d5fd
updated: 1484310186
title: agitated
categories:
    - Dictionary
---
agitated
     adj 1: troubled emotionally and usually deeply; "agitated parents"
            [ant: {unagitated}]
     2: physically disturbed or set in motion; "the agitated mixture
        foamed and bubbled" [ant: {unagitated}]
     3: thrown from side to side; "a tossing ship" [syn: {tossing}]
