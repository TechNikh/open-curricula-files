---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hereby
offline_file: ""
offline_thumbnail: ""
uuid: 9a8a5187-98ad-404c-acb4-f26ba28bd8b1
updated: 1484310591
title: hereby
categories:
    - Dictionary
---
hereby
     adv : (formal) by means of this; "I hereby declare you man and
           wife" [syn: {herewith}]
