---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/india
offline_file: ""
offline_thumbnail: ""
uuid: c9b56230-f753-45fa-abd3-71481567c060
updated: 1484310303
title: india
categories:
    - Dictionary
---
India
     n : a republic in the Asian subcontinent in southern Asia;
         second most populous country in the world; achieved
         independence from the United Kingdom in 1947 [syn: {Republic
         of India}, {Bharat}]
