---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Radical_axis
offline_file: ""
offline_thumbnail: ""
uuid: 51f4b991-0c6b-443e-b72a-319aadde4ec7
updated: 1484308941
title: Radical axis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Radical_axis_orthogonal_circles.svg.png
tags:
    - Definition and general properties
    - Radical center of three circles
    - Geometric construction
    - Algebraic construction
    - Determinant calculation
    - Notes
categories:
    - Elementary geometry
---
The radical axis (or power line) of two circles is the locus of points at which tangents drawn to both circles have the same length. For any point P on the radical axis, there is a unique circle centered on P that intersects both circles at right angles (orthogonally); conversely, the center of any circle that cuts both circles orthogonally must lie on the radical axis. In technical language, each point P on the radical axis has the same power with respect to both circles[1]
