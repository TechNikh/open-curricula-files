---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_ergodicity
offline_file: ""
offline_thumbnail: ""
uuid: aa2e82e4-d64e-4f62-bfcf-c7d7f0f62394
updated: 1484308702
title: Quantum ergodicity
categories:
    - Chaos theory
---
In quantum chaos, a branch of mathematical physics, quantum ergodicity is a property of the quantization of classical mechanical systems that are chaotic in the sense of exponential sensitivity to initial conditions. Quantum ergodicity states, roughly, that in the high-energy limit, the probability distributions associated to energy eigenstates of a quantized ergodic Hamiltonian tend to a uniform distribution in the classical phase space. This is consistent with the intuition that the flows of ergodic systems are equidistributed in phase space. By contrast, classical completely integrable systems generally have periodic orbits in phase space, and this is exhibited in a variety of ways in ...
