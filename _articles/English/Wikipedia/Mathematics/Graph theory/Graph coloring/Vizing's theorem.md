---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Vizing%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 76d78eda-6136-4814-8608-1b4211c5e268
updated: 1484309389
title: "Vizing's theorem"
tags:
    - Examples
    - Proof
    - Classification of graphs
    - Planar graphs
    - Graphs on nonplanar surfaces
    - Algorithms
    - History
    - Notes
categories:
    - Graph coloring
---
In graph theory, Vizing's theorem (named for Vadim G. Vizing who published it in 1964) states that the edges of every simple undirected graph may be colored using a number of colors that is at most one larger than the maximum degree Δ of the graph.
