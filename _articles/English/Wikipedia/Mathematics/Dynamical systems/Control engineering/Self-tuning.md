---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-tuning
offline_file: ""
offline_thumbnail: ""
uuid: 04f0eedc-8f7a-44ee-907b-7d4a8b249490
updated: 1484308739
title: Self-tuning
tags:
    - Examples
    - Architecture
    - Literature
categories:
    - Control engineering
---
In control theory a self-tuning system is capable of optimizing its own internal running parameters in order to maximize or minimize the fulfilment of an objective function; typically the maximization of efficiency or error minimization.
