---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hackmud
offline_file: ""
offline_thumbnail: ""
uuid: bfae077b-0be3-47f0-8902-bab26c22da60
updated: 1484308528
title: Hackmud
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hackmud_headshot_crop.png
categories:
    - Cooperative games
---
Hackmud is a massively multiplayer online video game and/or MUD that simulates 1990s hacker subculture through text-based adventure. Players use social engineering, scripting, and cracks in a text-based terminal to influence and control other players in the simulation.[1] Reviewers wrote that the game's "campy hacking" mimics that of films like WarGames (1983) and Jurassic Park (1993).[2][3]
