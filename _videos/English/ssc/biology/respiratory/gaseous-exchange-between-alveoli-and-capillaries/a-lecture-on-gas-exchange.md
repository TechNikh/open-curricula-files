---
title: A lecture on gas exchange
youtube_id: GXhHgIZMRDg
tags:
    - SSC
    - Biology
    - Respiration
    - Lecture
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
