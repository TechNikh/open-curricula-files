---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Diaconescu%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 642e9991-b34f-4f9c-8cd4-4217a4d542c3
updated: 1484309504
title: "Diaconescu's theorem"
categories:
    - Set theory
---
In mathematical logic, Diaconescu's theorem, or the Goodman–Myhill theorem, states that the full axiom of choice is sufficient to derive the law of the excluded middle, or restricted forms of it, in constructive set theory. It was discovered in 1975 by Diaconescu[1] and later by Goodman and Myhill.[2] Already in 1967, Errett Bishop posed the Theorem as an exercise (Problem 2 on page 58 in Foundations of constructive analysis[3]).
