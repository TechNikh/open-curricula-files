---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pan
offline_file: ""
offline_thumbnail: ""
uuid: 83d199ed-4273-4f81-a685-10ba60bc8123
updated: 1484310315
title: pan
categories:
    - Dictionary
---
pan
     n 1: cooking utensil consisting of a wide metal vessel [syn: {cooking
          pan}]
     2: (Greek mythology) god of fields and woods and shepherds and
        flocks; represented as a man with goat's legs and horns
        and ears; identified with Roman Sylvanus or Faunus [syn: {the
        goat god}]
     3: shallow container made of metal
     4: chimpanzees; more closely related to Australopithecus than
        to other pongids [syn: {genus Pan}]
     v 1: make a sweeping movement; "The camera panned across the
          room"
     2: wash dirt in a pan to separate out the precious minerals
        [syn: {pan out}, {pan off}]
     3: express a totally negative opinion of; ...
