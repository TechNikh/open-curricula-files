---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_William_Theodore_Youngs
offline_file: ""
offline_thumbnail: ""
uuid: a39b3d8a-c8e4-462e-be5a-670e0dedd070
updated: 1484309476
title: John William Theodore Youngs
categories:
    - Graph theorists
---
John William Theodore Youngs (usually cited as J. W. T. Youngs, known as Ted Youngs; 21 August 1910 Bilaspur, Chhattisgarh, India – 20 July 1970 Santa Cruz, California) was an American mathematician.
