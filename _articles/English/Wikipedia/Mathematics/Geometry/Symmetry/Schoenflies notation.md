---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Schoenflies_notation
offline_file: ""
offline_thumbnail: ""
uuid: f4facc1b-b40d-4957-9076-7a64e8f5a9ff
updated: 1484309315
title: Schoenflies notation
tags:
    - Symmetry elements
    - Point groups
    - Space groups
categories:
    - Symmetry
---
The Schoenflies (or Schönflies) notation, named after the German mathematician Arthur Moritz Schoenflies, is one of two conventions commonly used to describe point groups. This notation is used in spectroscopy. The other convention is the Hermann–Mauguin notation, also known as the International notation. A point group in the Schoenflies convention is completely adequate to describe the symmetry of a molecule; this is sufficient for spectroscopy. The Hermann–Mauguin notation is able to describe the space group of a crystal lattice, while the Schoenflies notation isn't. Thus the Hermann–Mauguin notation is used in crystallography.
