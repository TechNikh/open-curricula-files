---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pericardium
offline_file: ""
offline_thumbnail: ""
uuid: 7f9b64ef-11d2-40a8-8ed4-93372732bfa6
updated: 1484310158
title: pericardium
categories:
    - Dictionary
---
pericardium
     n : a double-layered serous membrane that surrounds the heart
     [also: {pericardia} (pl)]
