---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/documentary
offline_file: ""
offline_thumbnail: ""
uuid: abd52b74-836e-48ac-af15-c8758d8208b9
updated: 1484310467
title: documentary
categories:
    - Dictionary
---
documentary
     adj : relating to or consisting of or derived from documents [syn:
            {documental}]
     n : a film or TV program presenting the facts about a person or
         event [syn: {docudrama}, {documentary film}, {infotainment}]
