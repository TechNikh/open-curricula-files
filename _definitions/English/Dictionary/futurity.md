---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/futurity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484548921
title: futurity
categories:
    - Dictionary
---
futurity
     n 1: the time yet to come [syn: {future}, {hereafter}, {time to
          come}] [ant: {past}]
     2: the quality of being in or of the future [ant: {pastness}, {presentness}]
