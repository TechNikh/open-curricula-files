---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cantor%27s_diagonal_argument'
offline_file: ""
offline_thumbnail: ""
uuid: 97652e3a-0ea1-438e-8418-bb6ad01341ea
updated: 1484308991
title: "Cantor's diagonal argument"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Diagonal_argument_01_svg.svg.png
tags:
    - Uncountable set
    - Interpretation
    - Real numbers
    - General sets
    - Consequences
    - "Version for Quine's New Foundations"
categories:
    - Theorems in the foundations of mathematics
---
In set theory, Cantor's diagonal argument, also called the diagonalisation argument, the diagonal slash argument or the diagonal method, was published in 1891 by Georg Cantor as a mathematical proof that there are infinite sets which cannot be put into one-to-one correspondence with the infinite set of natural numbers.[1][2][3] Such sets are now known as uncountable sets, and the size of infinite sets is now treated by the theory of cardinal numbers which Cantor began.
