---
version: 1
type: video
provider: YouTube
id: htXJ2MoczvE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f59ce1c7-611f-4812-a9d3-4b2acc13c550
updated: 1486069637
title: 'In This Experiment- Observe Refraction'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/htXJ2MoczvE/default.jpg
    - https://i3.ytimg.com/vi/htXJ2MoczvE/1.jpg
    - https://i3.ytimg.com/vi/htXJ2MoczvE/2.jpg
    - https://i3.ytimg.com/vi/htXJ2MoczvE/3.jpg
---
