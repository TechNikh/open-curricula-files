---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/powered
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484438101
title: powered
categories:
    - Dictionary
---
powered
     adj : (often used in combination) having or using or propelled by
           means of power or power of a specified kind; "powered
           flight"; "kerosine-powered jet engines" [ant: {unpowered}]
