---
version: 1
type: article
id: https://en.wikipedia.org/wiki/153_(number)
offline_file: ""
offline_thumbnail: ""
uuid: b055e98f-943f-4363-b7d8-9143db0644e8
updated: 1484308979
title: 153 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-153_Triangular.gif
tags:
    - Mathematical properties
    - In the Bible
    - In the military
    - In transportation
    - In sports
    - In radio and TV
    - In other fields
categories:
    - Integers
---
