---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Automatic_group
offline_file: ""
offline_thumbnail: ""
uuid: 5cceca7c-bff8-4656-aad4-7d3c1553502d
updated: 1484309481
title: Automatic group
tags:
    - Properties
    - Examples of automatic groups
    - Examples of non-automatic groups
    - Biautomatic groups
    - Automatic structures
    - Additional reading
categories:
    - Computability theory
---
In mathematics, an automatic group is a finitely generated group equipped with several finite-state automata. These automata represent the Cayley graph of the group, i. e. can tell if a given word representation of a group element is in a "canonical form" and can tell if two elements given in canonical words differ by a generator.[1]
