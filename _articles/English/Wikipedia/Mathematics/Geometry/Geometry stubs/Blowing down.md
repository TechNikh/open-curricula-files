---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Blowing_down
offline_file: ""
offline_thumbnail: ""
uuid: 0a97c169-7f38-4584-9253-25453c31b188
updated: 1484309352
title: Blowing down
categories:
    - Geometry stubs
---
On an algebraic surface, blowing down a curve lying on the surface is a typical effect of a birational transformation. The curves that blow down, to a non-singular point, are of a special kind: they are rational curves, with self-intersection number −1.
