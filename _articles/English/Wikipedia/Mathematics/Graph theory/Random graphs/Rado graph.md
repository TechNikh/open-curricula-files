---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rado_graph
offline_file: ""
offline_thumbnail: ""
uuid: 1f2c502a-af68-4fbc-8d9b-723dcc1ef28f
updated: 1484309452
title: Rado graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Rado_graph.svg.png
tags:
    - History
    - Constructions
    - Binary numbers
    - Random graph
    - Other constructions
    - Properties
    - Extension
    - Induced subgraphs
    - Uniqueness
    - Symmetry
    - Robustness against finite changes
    - Partition
    - Model theory and 0-1 laws
    - First order properties
    - Completeness
    - Finite graphs and computational complexity
    - Saturated model
    - Related concepts
    - Notes
categories:
    - Random graphs
---
In the mathematical field of graph theory, the Rado graph, Erdős–Rényi graph, or random graph is a countably infinite graph that can be constructed (with probability one) by choosing independently at random for each pair of its vertices whether to connect the vertices by an edge. The same graph can also be constructed non-randomly, by symmetrizing the membership relation of the hereditarily finite sets, by applying the BIT predicate to the binary representations of the natural numbers, or as an infinite Paley graph that has edges connecting pairs of prime numbers congruent to 1 mod 4 when one is a quadratic residue modulo the other.
