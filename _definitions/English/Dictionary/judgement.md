---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/judgement
offline_file: ""
offline_thumbnail: ""
uuid: 5a51facd-67c0-4846-af05-7258ea42df53
updated: 1484310599
title: judgement
categories:
    - Dictionary
---
judgement
     n 1: the legal document stating the reasons for a judicial
          decision; "opinions are usually written by a single
          judge" [syn: {opinion}, {legal opinion}, {judgment}]
     2: an opinion formed by judging something; "he was reluctant to
        make his judgment known"; "she changed her mind" [syn: {judgment},
         {mind}]
     3: the cognitive process of reaching a decision or drawing
        conclusions [syn: {judgment}, {judging}]
     4: ability to make good judgments [syn: {sagacity}, {sagaciousness},
         {judgment}, {discernment}]
     5: the capacity to assess situations or circumstances shrewdly
        and to draw sound conclusions [syn: ...
