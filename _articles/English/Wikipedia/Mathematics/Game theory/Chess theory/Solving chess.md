---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Solving_chess
offline_file: ""
offline_thumbnail: ""
uuid: 310d0683-a440-45fa-94dc-47efdf9a3241
updated: 1484309020
title: Solving chess
tags:
    - Partial results
    - Predictions on when/if chess will be solved
categories:
    - Chess theory
---
Solving chess means finding an optimal strategy for playing chess, i.e. one by which one of the players (White or Black) can always force a victory, or both can force a draw (see Solved game). According to Zermelo's theorem, a hypothetically determinable optimal strategy does exist for chess.
