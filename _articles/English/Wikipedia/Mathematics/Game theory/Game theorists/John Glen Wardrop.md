---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_Glen_Wardrop
offline_file: ""
offline_thumbnail: ""
uuid: c440799e-1aaa-4c38-9f42-ba2a6ecc3e34
updated: 1484309029
title: John Glen Wardrop
categories:
    - Game theorists
---
The concepts are related to the idea of Nash equilibrium in game theory developed separately. However, in transportation networks, there are many players, making the analysis complex.
