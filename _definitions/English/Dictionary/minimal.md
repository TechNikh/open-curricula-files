---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minimal
offline_file: ""
offline_thumbnail: ""
uuid: 94eb6a2a-c0fe-4f83-a2e3-108c55700390
updated: 1484310533
title: minimal
categories:
    - Dictionary
---
minimal
     adj : the least possible; "needed to enforce minimal standards";
           "her grades were minimal"; "minimum wage"; "a minimal
           charge for the service" [syn: {minimum}] [ant: {maximal},
            {maximal}]
