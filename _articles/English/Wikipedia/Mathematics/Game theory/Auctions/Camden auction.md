---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Camden_auction
offline_file: ""
offline_thumbnail: ""
uuid: 0130dd39-5212-4e28-8c4d-2c5f4078dbb2
updated: 1484309010
title: Camden auction
categories:
    - Auctions
---
A Camden Auction is an auction used in backgammon clubs whereby the price of a seat for participants in a tournament is auctioned up to the highest price that all are comfortable paying. The money paid for the seats by the players constitutes the prize money. The tournament itself is run as a single elimination (knockout tournament) with the prize money being awarded as a jackpot to the outright winner.
