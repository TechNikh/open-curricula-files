---
version: 1
type: article
id: https://en.wikipedia.org/wiki/North-west
offline_file: ""
offline_thumbnail: ""
uuid: 92378f79-2c72-4982-9896-78b8c739d51a
updated: 1484308435
title: Points of the compass
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Compass_Card_1.png
tags:
    - Compass point names
    - 16-wind compass rose
    - 32-wind compass points
    - Traditional names
    - 32 cardinal points
    - 'Half- and quarter-points'
categories:
    - Uncategorized
---
The points of the compass, specifically on the compass rose, mark divisions of a compass; such divisions may be referred to as "winds" or "directions". A compass point allows reference to a specific heading (or course or azimuth) in a general or colloquial fashion, without having to compute or remember degrees.
