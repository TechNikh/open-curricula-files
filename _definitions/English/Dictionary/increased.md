---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/increased
offline_file: ""
offline_thumbnail: ""
uuid: 8d73c5ab-e5cd-4986-8371-a3aed7cce906
updated: 1484310290
title: increased
categories:
    - Dictionary
---
increased
     adj : made greater in size or amount or degree [ant: {decreased}]
