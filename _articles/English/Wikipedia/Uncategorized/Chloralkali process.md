---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chlor-alkali
offline_file: ""
offline_thumbnail: ""
uuid: 66882308-906e-47cb-bfce-81cda92a0799
updated: 1484308309
title: Chloralkali process
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Old_drawing_of_a_Chlorine-Caustic_Soda_Plant_%2528Edgewood%252C_Maryland%2529.JPG'
tags:
    - Process systems
    - Membrane cell
    - Diaphragm cell
    - Mercury cell
    - Manufacturer Associations
    - Laboratory procedure
categories:
    - Uncategorized
---
The chloralkali process (also chlor-alkali and chlor alkali) is an industrial process for the electrolysis of NaCl. It is the technology used to produce chlorine and sodium hydroxide (caustic soda), which are commodity chemicals required by industry. 35 million tons of chlorine were prepared by this process in 1987.[1] Industrial scale production began in 1892.
