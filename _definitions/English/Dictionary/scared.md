---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scared
offline_file: ""
offline_thumbnail: ""
uuid: d3b49e57-cffd-4080-954f-f7532f13f884
updated: 1484310587
title: scared
categories:
    - Dictionary
---
scared
     adj : made afraid; "the frightened child cowered in the corner";
           "too shocked and scared to move" [syn: {frightened}]
