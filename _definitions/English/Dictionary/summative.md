---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/summative
offline_file: ""
offline_thumbnail: ""
uuid: 0edfc1c7-a7d1-40e9-b72a-c1562e6690c4
updated: 1484310154
title: summative
categories:
    - Dictionary
---
summative
     adj : of or relating to a summation or produced by summation [syn:
            {summational}]
