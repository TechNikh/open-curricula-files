---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/charles
offline_file: ""
offline_thumbnail: ""
uuid: 23b7c564-42ce-4e8e-9f7a-28b8726c5cf5
updated: 1484310293
title: charles
categories:
    - Dictionary
---
Charles
     n 1: the eldest son of Elizabeth II and heir to the English
          throne (born in 1948) [syn: {Prince Charles}]
     2: French physicist and uathor of Charles's law which
        anticipated Gay-Lussac's law (1746-1823) [syn: {Jacques
        Charles}, {Jacques Alexandre Cesar Charles}]
     3: a river in eastern Massachusetts that empties into Boston
        Harbor and that separates Cambridge from Boston [syn: {Charles
        River}]
