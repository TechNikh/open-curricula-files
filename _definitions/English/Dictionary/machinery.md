---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/machinery
offline_file: ""
offline_thumbnail: ""
uuid: 091127ed-6b10-4dcb-896c-4b82fe94be13
updated: 1484310242
title: machinery
categories:
    - Dictionary
---
machinery
     n 1: machines or machine systems collectively
     2: a system of means and activities whereby a social
        institution functions; "the complex machinery of
        negotiation"; "the machinery of command labored and
        brought forth an order"
