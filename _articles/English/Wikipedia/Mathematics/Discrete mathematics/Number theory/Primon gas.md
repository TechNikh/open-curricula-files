---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Primon_gas
offline_file: ""
offline_thumbnail: ""
uuid: 8eebf44d-6eab-411d-8579-1ba81a37dfd1
updated: 1484308654
title: Primon gas
tags:
    - The model
    - State space
    - Energies
    - Statistical mechanics
    - The supersymmetric model
    - More complex models
categories:
    - Number theory
---
In mathematical physics, the primon gas or free Riemann gas is a toy model illustrating in a simple way some correspondences between number theory and ideas in quantum field theory and dynamical systems. It is a quantum field theory of a set of non-interacting particles, the primons; it is called a gas or a free model because the particles are non-interacting. The idea of the primon gas was independently discovered by Donald Spector[1] and Bernard Julia.[2] Later works by Bakas and Bowick[3] and Spector [4] explored the connection of such systems to string theory.
