---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rani
offline_file: ""
offline_thumbnail: ""
uuid: 179fb94d-79cf-46a7-a071-612319a0ffdd
updated: 1484310545
title: rani
categories:
    - Dictionary
---
rani
     n : (the feminine of raja) a Hindu princess or the wife of a
         raja [syn: {ranee}]
