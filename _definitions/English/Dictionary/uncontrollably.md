---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uncontrollably
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484558521
title: uncontrollably
categories:
    - Dictionary
---
uncontrollably
     adv : in an uncontrolled manner; "she laughed uncontrollably"
