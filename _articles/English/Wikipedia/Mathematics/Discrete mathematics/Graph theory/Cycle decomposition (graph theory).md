---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Cycle_decomposition_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 94dc60b1-8cb2-4221-b44d-6de1e6d7fb2c
updated: 1484308620
title: Cycle decomposition (graph theory)
categories:
    - Graph theory
---
In graph theory, a cycle decomposition is a decomposition (a partitioning of a graph's edges) into cycles. Every vertex in a graph that has a cycle decomposition must have even degree.
