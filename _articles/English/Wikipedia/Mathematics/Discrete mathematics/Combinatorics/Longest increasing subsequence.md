---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Longest_increasing_subsequence
offline_file: ""
offline_thumbnail: ""
uuid: 7067776a-e464-4f33-9ff4-96d03bf487d6
updated: 1484308588
title: Longest increasing subsequence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-LISDemo.gif
tags:
    - Example
    - Relations to other algorithmic problems
    - Efficient algorithms
    - Length bounds
    - Online algorithms
categories:
    - Combinatorics
---
In computer science, the longest increasing subsequence problem is to find a subsequence of a given sequence in which the subsequence's elements are in sorted order, lowest to highest, and in which the subsequence is as long as possible. This subsequence is not necessarily contiguous, or unique. Longest increasing subsequences are studied in the context of various disciplines related to mathematics, including algorithmics, random matrix theory, representation theory, and physics.[1] The longest increasing subsequence problem is solvable in time O(n log n), where n denotes the length of the input sequence.[2]
