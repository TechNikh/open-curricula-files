---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chromatic_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: 1fcd9382-acf9-4bb8-aa56-838848d027bb
updated: 1484309379
title: Chromatic polynomial
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Chromatic_polynomial_of_all_3-vertex_graphs_BW.png
tags:
    - History
    - Definition
    - Examples
    - Properties
    - Chromatic equivalence
    - Chromatic uniqueness
    - Chromatic roots
    - Categorification
    - Algorithms
    - Efficient algorithms
    - Deletion–contraction
    - Cube Method
    - Computational complexity
    - Notes
categories:
    - Graph coloring
---
The chromatic polynomial is a polynomial studied in algebraic graph theory, a branch of mathematics. It counts the number of graph colorings as a function of the number of colors and was originally defined by George David Birkhoff to attack the four color problem. It was generalised to the Tutte polynomial by H. Whitney and W. T. Tutte, linking it to the Potts model of statistical physics.
