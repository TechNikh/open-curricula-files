---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imagines
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484376481
title: imagines
categories:
    - Dictionary
---
imago
     n 1: (psychoanalysis) an idealized image of someone (usually a
          parent) formed in childhood
     2: an adult insect produced after metamorphosis
     [also: {imagoes} (pl), {imagines} (pl)]
