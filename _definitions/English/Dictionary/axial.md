---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/axial
offline_file: ""
offline_thumbnail: ""
uuid: 526f12c0-bf25-4d8e-b41c-44ecd136f1d6
updated: 1484310303
title: axial
categories:
    - Dictionary
---
axial
     adj 1: of or relating to or resembling an axis of rotation
     2: relating to or attached to the axis; "axial angle" [syn: {axile}]
     3: situated on or along or in the direction of an axis
