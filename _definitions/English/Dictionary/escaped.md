---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/escaped
offline_file: ""
offline_thumbnail: ""
uuid: 24cf0622-e8e7-4d40-9e3d-ade56bf26511
updated: 1484310176
title: escaped
categories:
    - Dictionary
---
escaped
     adj : having escaped, especially from confinement; "a convict
           still at large"; "searching for two escaped prisoners";
           "dogs loose on the streets"; "criminals on the loose in
           the neighborhood" [syn: {at large(p)}, {at liberty(p)},
            {loose}, {on the loose(p)}]
