---
version: 1
type: video
provider: YouTube
id: ANi709MYnWg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Acid-Base%20Reactions%20in%20Solution-%20Crash%20Course%20Chemistry%20%238.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1bf0c4c0-4249-4620-ab8e-99db348cff10
updated: 1486069680
title: 'Acid-Base Reactions in Solution- Crash Course Chemistry #8'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/ANi709MYnWg/default.jpg
    - https://i3.ytimg.com/vi/ANi709MYnWg/1.jpg
    - https://i3.ytimg.com/vi/ANi709MYnWg/2.jpg
    - https://i3.ytimg.com/vi/ANi709MYnWg/3.jpg
---
