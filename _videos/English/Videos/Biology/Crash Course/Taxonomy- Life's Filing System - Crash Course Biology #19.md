---
version: 1
type: video
provider: YouTube
id: F38BmgPcZ_I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 562e03dd-3d1d-411a-b7b8-582aeaa4534f
updated: 1486069678
title: "Taxonomy- Life's Filing System - Crash Course Biology #19"
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/F38BmgPcZ_I/default.jpg
    - https://i3.ytimg.com/vi/F38BmgPcZ_I/1.jpg
    - https://i3.ytimg.com/vi/F38BmgPcZ_I/2.jpg
    - https://i3.ytimg.com/vi/F38BmgPcZ_I/3.jpg
---
