---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pulse_wave
offline_file: ""
offline_thumbnail: ""
uuid: 40eb6eb0-0b6c-4401-82b6-47bde7a93abd
updated: 1484308900
title: Pulse wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Dutycycle.svg.png
categories:
    - Waves
---
A pulse wave or pulse train is a kind of non-sinusoidal waveform that is similar to a square wave, but does not have the symmetrical shape associated with a perfect square wave. It is a term common to synthesizer programming, and is a typical waveform available on many synthesizers. The exact shape of the wave is determined by the duty cycle of the oscillator. In many synthesizers, the duty cycle can be modulated (sometimes called pulse-width modulation) for a more dynamic timbre.[1] The pulse wave is also known as the rectangular wave, the periodic version of the rectangular function.
