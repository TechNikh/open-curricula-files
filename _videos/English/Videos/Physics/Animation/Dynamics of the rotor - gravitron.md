---
version: 1
type: video
provider: YouTube
id: LdyJLLumCEc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Dynamics%20of%20the%20rotor%20-%20gravitron.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a50d3aa9-e2e9-46d8-985c-e687a5142901
updated: 1486069652
title: Dynamics of the rotor / gravitron
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/LdyJLLumCEc/default.jpg
    - https://i3.ytimg.com/vi/LdyJLLumCEc/1.jpg
    - https://i3.ytimg.com/vi/LdyJLLumCEc/2.jpg
    - https://i3.ytimg.com/vi/LdyJLLumCEc/3.jpg
---
