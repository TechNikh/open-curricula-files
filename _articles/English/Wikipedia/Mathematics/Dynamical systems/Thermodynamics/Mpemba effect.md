---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mpemba_effect
offline_file: ""
offline_thumbnail: ""
uuid: 383a5185-ad30-4a11-b2e9-c1a1aab98342
updated: 1484308855
title: Mpemba effect
tags:
    - Definition
    - Observations
    - Historical context
    - "Mpemba's observation"
    - Modern context
    - Suggested explanations
    - Recent views
    - Similar effects
    - Bibliography
categories:
    - Thermodynamics
---
The Mpemba effect, named after Erasto Batholomeo Mpemba (b.1950) in 1963, is the observation that, in some circumstances, warmer water can freeze faster than colder water. Although there is evidence of the effect, there is disagreement on exactly what the effect is and under what circumstances it occurs. There have been reports of similar phenomena since ancient times, although with insufficient detail for the claims to be replicated. A number of possible explanations for the effect have been proposed. Further investigations will need to decide on a precise definition of "freezing" and control a vast number of starting parameters in order to confirm or explain the effect.
