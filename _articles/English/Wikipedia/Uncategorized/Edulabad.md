---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edulabad
offline_file: ""
offline_thumbnail: ""
uuid: 1bb5b04f-ffa7-4ab7-b036-c9db08fce222
updated: 1484308340
title: Edulabad
categories:
    - Uncategorized
---
Edulabad is a village in Ranga Reddy district in Telangana, India. It falls under Ghatkesar mandal.[1] Edulabad has a big lake known as Edulabad Water Reservoir (EBWR). It is famous for Andal Ranganayaka swamy temple and Venu Gopala swamy temple.
