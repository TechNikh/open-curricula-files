---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neglect
offline_file: ""
offline_thumbnail: ""
uuid: 744446e0-b6ac-4024-9d6a-93ace2b1bde6
updated: 1484310471
title: neglect
categories:
    - Dictionary
---
neglect
     n 1: lack of attention and due care [syn: {disregard}]
     2: the state of something that has been unused and neglected;
        "the house was in a terrible state of neglect" [syn: {disuse}]
     3: willful lack of care and attention [syn: {disregard}]
     4: the trait of neglecting responsibilities and lacking concern
        [syn: {negligence}, {neglectfulness}]
     5: failure to act with the prudence that a reasonable person
        would exercise under the same circumstances [syn: {negligence},
         {carelessness}, {nonperformance}]
     v 1: leave undone or leave out; "How could I miss that typo?";
          "The workers on the conveyor belt miss one out of ten"
   ...
