---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terrorist
offline_file: ""
offline_thumbnail: ""
uuid: bd28d13f-ca3a-45be-97aa-8b5a1b68314b
updated: 1484310188
title: terrorist
categories:
    - Dictionary
---
terrorist
     adj : characteristic of someone who employs terrorism (especially
           as a political weapon); "terrorist activity";
           "terrorist state"
     n : a radical who employs terror as a political weapon; usually
         organizes with other terrorists in small cells; often
         uses religion as a cover for terrorist activities
