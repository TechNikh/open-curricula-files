---
title: Farm pond- Water harvesting structures in semi arid lands
youtube_id: PzH26oXIjbI
tags:
    - Rainwater Harvesting
    - farm ponds
    - semi arid area
    - fertigation
    - water harvesting
    - durface runoff
categories:
    - Water management
---
