---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/narrowly
offline_file: ""
offline_thumbnail: ""
uuid: 1beb164d-953e-4c00-b5a6-3d9589f3aecf
updated: 1484310539
title: narrowly
categories:
    - Dictionary
---
narrowly
     adv : in a narrow manner; not allowing for exceptions; "he
           interprets the law narrowly" [ant: {broadly}]
