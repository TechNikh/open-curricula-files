---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conserved
offline_file: ""
offline_thumbnail: ""
uuid: 67808e46-1e75-47cb-acd5-f3faacbd0ae5
updated: 1484310246
title: conserved
categories:
    - Dictionary
---
conserved
     adj : protected from harm or loss
