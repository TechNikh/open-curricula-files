---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydrophobic
offline_file: ""
offline_thumbnail: ""
uuid: 05cd1080-9e24-46f1-a2ab-ccb876d69a62
updated: 1484310426
title: hydrophobic
categories:
    - Dictionary
---
hydrophobic
     adj 1: lacking affinity for water; tending to repel and not absorb
            water; tending not to dissolve in or mix with or be
            wetted by water [ant: {hydrophilic}]
     2: abnormally afraid of water [syn: {aquaphobic}]
