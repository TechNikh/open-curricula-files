---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rayleigh_wave
offline_file: ""
offline_thumbnail: ""
uuid: ab3f3576-b4d1-4564-ba98-9bd478d438c7
updated: 1484308896
title: Rayleigh wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rayleigh_wave.jpg
tags:
    - characteristics
    - Rayleigh wave dispersion
    - Rayleigh waves in non-destructive testing
    - Rayleigh waves in electronic devices
    - Rayleigh waves in geophysics
    - Rayleigh waves from earthquakes
    - Rayleigh waves in seismology
    - Other manifestations
    - Animals
    - Notes
categories:
    - Waves
---
Rayleigh waves are a type of surface acoustic wave that travel on solids. They can be produced in materials in many ways, such as by a localized impact or by piezo-electric transduction, and are frequently used in non-destructive testing for detecting defects. They are part of the seismic waves that are produced on the Earth by earthquakes. When guided in layers they are referred to as Lamb waves, Rayleigh–Lamb waves, or generalized Rayleigh waves.
