---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Castelnuovo%E2%80%93Mumford_regularity'
offline_file: ""
offline_thumbnail: ""
uuid: e7760826-1ce4-4944-a470-b3c5fac078d2
updated: 1484309134
title: Castelnuovo–Mumford regularity
categories:
    - Algebraic geometry
---
In algebraic geometry, the Castelnuovo–Mumford regularity of a coherent sheaf F over projective space Pn is the smallest integer r such that it is r-regular, meaning that
