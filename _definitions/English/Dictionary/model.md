---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/model
offline_file: ""
offline_thumbnail: ""
uuid: 7fec7253-d5da-4560-98bb-eecb44d48727
updated: 1484310341
title: model
categories:
    - Dictionary
---
model
     adj : worthy of imitation; "exemplary behavior"; "model citizens"
           [syn: {exemplary}, {model(a)}]
     n 1: a simplified description of a complex entity or process;
          "the computer program was based on a model of the
          circulatory and respiratory systems" [syn: {theoretical
          account}, {framework}]
     2: a type of product; "his car was an old model"
     3: a person who poses for a photographer or painter or
        sculptor; "the president didn't have time to be a model so
        the artist worked from photos" [syn: {poser}]
     4: representation of something (sometimes on a smaller scale)
        [syn: {simulation}]
     5: something to be ...
