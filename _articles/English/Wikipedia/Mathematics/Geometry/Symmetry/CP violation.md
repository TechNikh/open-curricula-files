---
version: 1
type: article
id: https://en.wikipedia.org/wiki/CP_violation
offline_file: ""
offline_thumbnail: ""
uuid: 26b565a5-4ddb-4263-ae5e-a680406b34e6
updated: 1484309309
title: CP violation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kaon-box-diagram.svg.png
tags:
    - CP-symmetry
    - CP violation in the Standard Model
    - Experimental status
    - Indirect CP violation
    - Direct CP violation
    - Strong CP problem
    - Little CP problem
    - CP violation and the matter–antimatter imbalance
categories:
    - Symmetry
---
In particle physics, CP violation (CP standing for charge parity) is a violation of the postulated CP-symmetry (or charge conjugation parity symmetry): the combination of C-symmetry (charge conjugation symmetry) and P-symmetry (parity symmetry). CP-symmetry states that the laws of physics should be the same if a particle is interchanged with its antiparticle (C symmetry), and when its spatial coordinates are inverted ("mirror" or P symmetry). The discovery of CP violation in 1964 in the decays of neutral kaons resulted in the Nobel Prize in Physics in 1980 for its discoverers James Cronin and Val Fitch.
