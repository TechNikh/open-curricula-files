---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/census
offline_file: ""
offline_thumbnail: ""
uuid: bd5ab8c0-f2b3-4a98-8d96-7dcf4bce8aa7
updated: 1484310266
title: census
categories:
    - Dictionary
---
census
     n : a period count of the population [syn: {nose count}, {nosecount}]
     v : conduct a census; "They censused the deer in the forest"
