---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vindicated
offline_file: ""
offline_thumbnail: ""
uuid: abab3220-79cb-4216-ac08-8f044783770b
updated: 1484310192
title: vindicated
categories:
    - Dictionary
---
vindicated
     adj : freed from any question of guilt; "is absolved from all
           blame"; "was now clear of the charge of cowardice";
           "his official honor is vindicated" [syn: {absolved}, {clear},
            {cleared}, {exculpated}, {exonerated}]
