---
version: 1
type: video
provider: YouTube
id: NgD9yHSJ29I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/How%20atoms%20bond%20-%20George%20Zaidan%20and%20Charles%20Morton.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9b0e0392-b70e-4398-a231-f27be696600a
updated: 1486069664
title: 'How atoms bond - George Zaidan and Charles Morton'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/NgD9yHSJ29I/default.jpg
    - https://i3.ytimg.com/vi/NgD9yHSJ29I/1.jpg
    - https://i3.ytimg.com/vi/NgD9yHSJ29I/2.jpg
    - https://i3.ytimg.com/vi/NgD9yHSJ29I/3.jpg
---
