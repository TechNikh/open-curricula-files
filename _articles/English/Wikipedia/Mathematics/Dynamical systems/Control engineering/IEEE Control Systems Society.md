---
version: 1
type: article
id: https://en.wikipedia.org/wiki/IEEE_Control_Systems_Society
offline_file: ""
offline_thumbnail: ""
uuid: a97c39de-0c87-4890-9b20-4b8d52c99775
updated: 1484308732
title: IEEE Control Systems Society
tags:
    - History
    - Selected publications
    - CSS co-sponsored conferences
categories:
    - Control engineering
---
IEEE Control Systems Society (CSS) is an organizational unit of the IEEE. It was founded in 1954 and is dedicated to the advancement of the theory and practice of systems and control in engineering.
