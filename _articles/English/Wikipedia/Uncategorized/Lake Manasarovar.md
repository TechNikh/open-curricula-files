---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Manasarovar
offline_file: ""
offline_thumbnail: ""
uuid: 86e623d2-1d6e-425a-96ae-cd544254ce2f
updated: 1484308435
title: Lake Manasarovar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mansarovar_map.jpg
tags:
    - geography
    - Etymology
    - Religious significance
    - In Hinduism
    - In the Bon religion
    - In Buddhism
    - In Jainism
    - Regional terrain
categories:
    - Uncategorized
---
Lake Manasarovar (also Manas Sarovar, Mapam Yumtso; Chinese: 玛旁雍错, Tibetan: མ་ཕམ་གཡུ་མཚོ།, Wylie: ma pham g.yu mtsho; Sanskrit: मानसरोवर ) is a freshwater lake in the Tibet Autonomous Region, 940 kilometres (580 mi) from Lhasa. To the west of it is Lake Rakshastal; to the north is Mount Kailash.
