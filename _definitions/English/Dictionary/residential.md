---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/residential
offline_file: ""
offline_thumbnail: ""
uuid: f66cac39-b3d9-44d8-ad0b-7c4b440f23f3
updated: 1484310480
title: residential
categories:
    - Dictionary
---
residential
     adj 1: used or designed for residence or limited to residences; "a
            residential hotel"; "a residential quarter"; "a
            residential college"; "residential zoning" [ant: {nonresidential}]
     2: of or relating to or connected with residence; "a
        residential requirement for the doctorate"
