---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/briskly
offline_file: ""
offline_thumbnail: ""
uuid: c5fdec94-e855-4a30-8e6e-e814f9a3f43c
updated: 1484310150
title: briskly
categories:
    - Dictionary
---
briskly
     adv : in a brisk manner; "she walked briskly in the cold air";
           "`after lunch,' she said briskly"
