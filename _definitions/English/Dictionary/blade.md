---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blade
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484360041
title: blade
categories:
    - Dictionary
---
blade
     n 1: especially a leaf of grass or the broad portion of a leaf as
          distinct from the petiole [syn: {leaf blade}]
     2: a dashing young man; "gay young blades bragged of their
        amorous adventures"
     3: something long and thin resembling a blade of grass; "a
        blade of lint on his suit"
     4: a cutting or thrusting weapon with a long blade [syn: {sword},
         {brand}, {steel}]
     5: a cut of beef from the shoulder blade
     6: a broad flat body part (as of the shoulder or tongue)
     7: the part of the skate that slides on the ice
     8: flat surface that rotates and pushes against air or water
        [syn: {vane}]
     9: the flat part of a ...
