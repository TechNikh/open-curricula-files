---
version: 1
type: article
id: https://en.wikipedia.org/wiki/16_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 616e2155-d3c3-4187-b549-5fc4433fca45
updated: 1484308946
title: 16 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Muybridge_race_horse_animated.gif
tags:
    - In mathematics
    - In science
    - Age 16
    - In sports
    - American football
    - Association football
    - Auto racing
    - Baseball
    - Rugby union
    - Multiple sports
    - In other fields
categories:
    - Integers
---
16 (sixteen) is the natural number following 15 and preceding 17. 16 is a composite number, and a square number, being 42 = 4 × 4. It is the smallest number with exactly five divisors, its proper divisors being 1, 2, 4 and 8.
