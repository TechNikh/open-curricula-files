---
version: 1
type: video
provider: YouTube
id: I3GlErUi2q0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/eureka%205%20acceleration%20II.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fdcb613e-f430-49d4-9360-02cdc51ebe4b
updated: 1486069665
title: eureka 5 acceleration II
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/I3GlErUi2q0/default.jpg
    - https://i3.ytimg.com/vi/I3GlErUi2q0/1.jpg
    - https://i3.ytimg.com/vi/I3GlErUi2q0/2.jpg
    - https://i3.ytimg.com/vi/I3GlErUi2q0/3.jpg
---
