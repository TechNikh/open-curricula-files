---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Minakshisundaram%E2%80%93Pleijel_zeta_function'
offline_file: ""
offline_thumbnail: ""
uuid: a96c727f-2d69-496d-b212-6e48a5e6716f
updated: 1484309230
title: Minakshisundaram–Pleijel zeta function
tags:
    - Definition
    - Heat kernel
    - Example
    - Applications
categories:
    - Differential geometry
---
The Minakshisundaram–Pleijel zeta function is a zeta function encoding the eigenvalues of the Laplacian of a compact Riemannian manifold. It was introduced by Subbaramiah Minakshisundaram and Åke Pleijel (1949). The case of a compact region of the plane was treated earlier by Carleman (1935).
