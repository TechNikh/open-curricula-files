---
version: 1
type: video
provider: YouTube
id: ndNAp0c8UoE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Zero%20Conditional%20English%20Grammar.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a80a65cc-b710-40be-886b-50c1b624ef41
updated: 1486069631
title: Zero Conditional English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/ndNAp0c8UoE/default.jpg
    - https://i3.ytimg.com/vi/ndNAp0c8UoE/1.jpg
    - https://i3.ytimg.com/vi/ndNAp0c8UoE/2.jpg
    - https://i3.ytimg.com/vi/ndNAp0c8UoE/3.jpg
---
