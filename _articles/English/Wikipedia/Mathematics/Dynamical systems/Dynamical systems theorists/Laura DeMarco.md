---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laura_DeMarco
offline_file: ""
offline_thumbnail: ""
uuid: 0d4c9d01-e49f-4406-b65f-177fee123cb1
updated: 1484308684
title: Laura DeMarco
categories:
    - Dynamical systems theorists
---
She received her Ph.D. from Harvard University in 2002 under the supervision of Curtis T. McMullen.[2]
