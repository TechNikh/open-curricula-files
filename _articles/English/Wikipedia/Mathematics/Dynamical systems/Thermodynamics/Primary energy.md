---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Primary_energy
offline_file: ""
offline_thumbnail: ""
uuid: 4de8735b-4517-4c64-85b5-5ce5de799c27
updated: 1484308859
title: Primary energy
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Different_energy_forms_%2528PES%2529.png'
tags:
    - Examples of sources
    - Usable energy
    - Conversion to energy carriers (or secondary energy)
    - Outlook
    - Energy accidents and fatalities
    - Notes
categories:
    - Thermodynamics
---
Primary energy (PE) is an energy form found in nature that has not been subjected to any conversion or transformation process. It is energy contained in raw fuels, and other forms of energy received as input to a system. Primary energy can be non-renewable or renewable.
