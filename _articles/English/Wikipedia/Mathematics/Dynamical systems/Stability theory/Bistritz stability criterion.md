---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bistritz_stability_criterion
offline_file: ""
offline_thumbnail: ""
uuid: c0b8e867-04fc-4ffd-9dc6-066e2b62141a
updated: 1484308834
title: Bistritz stability criterion
tags:
    - Algorithm
    - Stability condition
    - Example
    - Comments
categories:
    - Stability theory
---
In signal processing and control theory, the Bistritz criterion is a simple method to determine whether a discrete linear time invariant (LTI) system is stable proposed by Yuval Bistritz.[1][2] Stability of a discrete LTI system requires that its characteristic polynomials
