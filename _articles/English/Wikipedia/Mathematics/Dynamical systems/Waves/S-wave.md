---
version: 1
type: article
id: https://en.wikipedia.org/wiki/S-wave
offline_file: ""
offline_thumbnail: ""
uuid: d62b1707-356e-435f-8973-ab5dd038acc3
updated: 1484308896
title: S-wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Onde_cisaillement_impulsion_1d_30_petit.gif
tags:
    - Theory
categories:
    - Waves
---
In seismology, S-waves, secondary waves, or shear waves (sometimes called an elastic S-wave) are a type of elastic wave, and are one of the two main types of elastic body waves, so named because they move through the body of an object, unlike surface waves.
