---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equ
offline_file: ""
offline_thumbnail: ""
uuid: ae87d08e-ce39-4721-9db4-bd3d85b1b182
updated: 1484308444
title: Equuleus
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sidney_Hall_-_Urania%2527s_Mirror_-_Pegasus_and_Equuleus_%2528best_currently_available_version_-_2014%2529.jpg'
tags:
    - Notable features
    - Stars
    - Deep-sky objects
    - Mythology
    - Equivalents
categories:
    - Uncategorized
---
Equuleus /ᵻˈkwuːliəs/ is a constellation. Its name is Latin for 'little horse', a foal. It was one of the 48 constellations listed by the 2nd century astronomer Ptolemy, and remains one of the 88 modern constellations. It is the second smallest of the modern constellations (after Crux), spanning only 72 square degrees. It is also very faint, having no stars brighter than the fourth magnitude.
