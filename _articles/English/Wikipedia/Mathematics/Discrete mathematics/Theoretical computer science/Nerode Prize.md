---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nerode_Prize
offline_file: ""
offline_thumbnail: ""
uuid: 83c3e0ca-e7bc-4d43-a2e9-ee9404b8b689
updated: 1484308678
title: Nerode Prize
categories:
    - Theoretical computer science
---
The EATCS--IPEC Nerode Prize is a theoretical computer science prize awarded for outstanding research in the area of multivariate algorithmics. It is awarded by the European Association for Theoretical Computer Science and the International Symposium on Parameterized and Exact Computation.[1] The prize was offered for the first time in 2013.[2]
