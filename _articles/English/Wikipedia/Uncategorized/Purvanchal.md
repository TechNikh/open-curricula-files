---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Purvanchal
offline_file: ""
offline_thumbnail: ""
uuid: b84887dc-d393-41fb-bdbc-4faed1d6a273
updated: 1484308419
title: Purvanchal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Benares_1890.jpg
tags:
    - Languages used in Purvanchal
    - Regions of Awadh in Purvanchal
    - History of Purvanchal
    - About Purvanchal
    - List of districts in Purvanchal
    - Education
    - Major universities
    - 'Major Engineering, Medical & Research Institutes in Purvanchal'
    - Economic Zone
    - Tourist attractions
    - Important Roads, Railways and Airports
    - Major Roads
    - Airports
    - Notable people from Purvanchal
categories:
    - Uncategorized
---
Purvanchal is a geographic region of northern India, which comprises the eastern end of Uttar Pradesh state where Hindi, Bhojpuri is the predominant language. It is bounded by Nepal to the north, Indian state Bihar to the east, Bagelkhand region of Madhya Pradesh state to the south, the Awadh region of Uttar Pradesh to the west and the end of Lower Doab[1] (Kanpur-Fatehpur-Allahabad region) in Uttar Pradesh to its southwest.
