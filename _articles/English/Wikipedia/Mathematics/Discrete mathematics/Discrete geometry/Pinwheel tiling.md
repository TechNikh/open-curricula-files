---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pinwheel_tiling
offline_file: ""
offline_thumbnail: ""
uuid: 60a2d8c9-772f-479f-9028-43c3be5dab12
updated: 1484308619
title: Pinwheel tiling
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Pinwheel_1.svg.png
tags:
    - The Conway tessellation
    - The pinwheel tilings
    - Generalizations
    - Use in architecture
categories:
    - Discrete geometry
---
Pinwheel tilings are non-periodic tilings defined by Charles Radin and based on a construction due to John Conway. They are the first known non-periodic tilings to each have the property that their tiles appear in infinitely many orientations.
