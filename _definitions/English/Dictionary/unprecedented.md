---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unprecedented
offline_file: ""
offline_thumbnail: ""
uuid: d39ee88f-64ca-4bb1-a8b3-2e30ec720e3c
updated: 1484310537
title: unprecedented
categories:
    - Dictionary
---
unprecedented
     adj : having no precedent; novel; "an unprecedented expansion in
           population and industry" [ant: {precedented}]
