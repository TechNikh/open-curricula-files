---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finance
offline_file: ""
offline_thumbnail: ""
uuid: ce78aa48-bcc5-41f9-bd49-e65cffcc52a6
updated: 1484310451
title: finance
categories:
    - Dictionary
---
finance
     n 1: the commercial activity of providing funds and capital
     2: the branch of economics that studies the management of money
        and other assets
     3: the management of money and credit and banking and
        investments
     v 1: obtain or provide money for; "Can we finance the addition to
          our home?"
     2: sell or provide on credit
