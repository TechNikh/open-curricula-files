---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chemical_clock
offline_file: ""
offline_thumbnail: ""
uuid: 56b6a21a-d833-456f-8fce-e93c71ddd6c2
updated: 1484308801
title: Chemical clock
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bzr_fotos.jpg
tags:
    - History
    - Theory
    - Types
    - BZ reaction
    - Briggs–Rauscher reaction
    - Bray–Liebhafsky reaction
categories:
    - Non-equilibrium thermodynamics
---
A chemical clock or oscillating reaction is a complex mixture of reacting chemical compounds in which the concentration of one or more components exhibits periodic changes, or where sudden property changes occur after a predictable induction time.[1] They are a class of reactions that serve as an example of non-equilibrium thermodynamics, resulting in the establishment of a nonlinear oscillator. The reactions are theoretically important in that they show that chemical reactions do not have to be dominated by equilibrium thermodynamic behavior.
