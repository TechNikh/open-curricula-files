---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Neural_accommodation
offline_file: ""
offline_thumbnail: ""
uuid: 4a96fc73-3b99-4eaf-8fda-e7abbe8fcf45
updated: 1484308808
title: Neural accommodation
categories:
    - Nonlinear systems
---
Neural accommodation or neuronal accommodation occurs when a neuron or muscle cell is depolarised by slowly rising current (ramp depolarisation) in vitro.[1][2] The Hodgkin–Huxley model also shows accommodation.[3] Sudden depolarisation of a nerve evokes propagated action potential by activating voltage-gated fast sodium channels incorporated in the cell membrane if the depolarisation is strong enough to reach threshold. The open sodium channels allow more sodium ions to flow into the cell and resulting in further depolarisation, which will subsequently open even more sodium channels. At a certain moment this process becomes regenerative (vicious cycle) and results in the rapid ascending ...
