---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fashion
offline_file: ""
offline_thumbnail: ""
uuid: 61d8034a-7a6f-4591-a13b-c6e322ca59d4
updated: 1484310601
title: fashion
categories:
    - Dictionary
---
fashion
     n 1: how something is done or how it happens; "her dignified
          manner"; "his rapid manner of talking"; "their nomadic
          mode of existence"; "in the characteristic New York
          style"; "a lonely way of life"; "in an abrasive fashion"
          [syn: {manner}, {mode}, {style}, {way}]
     2: characteristic or habitual practice
     3: the latest and most admired style in clothes and cosmetics
        and behavior
     v : make out of components (often in an improvising manner);
         "She fashioned a tent out of a sheet and a few sticks"
         [syn: {forge}]
