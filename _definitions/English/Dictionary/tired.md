---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tired
offline_file: ""
offline_thumbnail: ""
uuid: 0b94b139-fa3c-4cf7-bbc0-a41aa0387281
updated: 1484310357
title: tired
categories:
    - Dictionary
---
tired
     adj 1: depleted of strength or energy; "tired mothers with crying
            babies"; "too tired to eat" [ant: {rested}]
     2: repeated too often; overfamiliar through overuse; "bromidic
        sermons"; "his remarks were trite and commonplace";
        "hackneyed phrases"; "a stock answer"; "repeating
        threadbare jokes"; "parroting some timeworn axiom"; "the
        trite metaphor `hard as nails'" [syn: {banal}, {commonplace},
         {hackneyed}, {old-hat}, {shopworn}, {stock(a)}, {threadbare},
         {timeworn}, {trite}, {well-worn}]
