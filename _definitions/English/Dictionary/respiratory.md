---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/respiratory
offline_file: ""
offline_thumbnail: ""
uuid: 157dc515-0721-4626-b3c6-cf68e880cc6d
updated: 1484310158
title: respiratory
categories:
    - Dictionary
---
respiratory
     adj : pertaining to respiration; "respiratory assistance"
