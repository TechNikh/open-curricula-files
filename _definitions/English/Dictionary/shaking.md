---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shaking
offline_file: ""
offline_thumbnail: ""
uuid: 34c77fe9-8746-47cd-98a4-7fef5cecfc2d
updated: 1484310426
title: shaking
categories:
    - Dictionary
---
shaking
     adj : vibrating slightly and irregularly; as e.g. with fear or
           cold or like the leaves of an aspen in a breeze; "a
           quaking bog"; "the quaking child asked for more";
           "quivering leaves of a poplar tree"; "with shaking
           knees"; "seemed shaky on her feet"; "sparkling light
           from the shivering crystals of the chandelier";
           "trembling hands" [syn: {quaking}, {quivering}, {shaky},
            {shivering}, {trembling}]
     n 1: the act of causing something to move up and down (or back
          and forth) with quick movements
     2: a shaky motion; "the shaking of his fingers as he lit his
        pipe" [syn: {shakiness}, ...
