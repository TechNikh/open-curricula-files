---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/age-old
offline_file: ""
offline_thumbnail: ""
uuid: 957b0025-35ac-4c07-b74d-f668f36a5850
updated: 1484310599
title: age-old
categories:
    - Dictionary
---
age-old
     adj : belonging to or lasting from times long ago; "age-old
           customs"; "the antique fear that days would dwindle
           away to complete darkness" [syn: {antique}]
