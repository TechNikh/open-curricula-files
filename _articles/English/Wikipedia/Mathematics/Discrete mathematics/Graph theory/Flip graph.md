---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flip_graph
offline_file: ""
offline_thumbnail: ""
uuid: ccd33227-f790-4c12-8721-ae3c1ed8dca9
updated: 1484308629
title: Flip graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Flip_graphs.svg.png
tags:
    - Examples
    - Finite sets of points in Euclidean space
    - Topological surfaces
    - Other flip graphs
    - Properties
    - Polytopality
    - Connectedness
categories:
    - Graph theory
---
A flip graph is a graph whose vertices are combinatorial or geometric objects, and whose edges link two of these objects when they can be obtained from one another by an elementary operation called a flip. Flip graphs are special cases of geometric graphs.
