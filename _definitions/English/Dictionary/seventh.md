---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seventh
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484329621
title: seventh
categories:
    - Dictionary
---
seventh
     adj : coming next after the sixth and just before the eighth in
           position [syn: {7th}]
     n 1: position seven in a countable series of things
     2: a seventh part [syn: {one-seventh}]
     3: the musical interval between one note and another seven
        notes away from it
