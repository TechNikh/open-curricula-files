---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Milliken%27s_tree_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 2a0d1f29-eda7-4e5a-bbf0-8f99622afdb6
updated: 1484308664
title: "Milliken's tree theorem"
categories:
    - Theorems in discrete mathematics
---
In mathematics, Milliken's tree theorem in combinatorics is a partition theorem generalizing Ramsey's theorem to infinite trees, objects with more structure than sets.
