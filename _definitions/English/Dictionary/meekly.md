---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meekly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484651341
title: meekly
categories:
    - Dictionary
---
meekly
     adv 1: in a submissive or spiritless manner; "meekly bowed to his
            wishes"
     2: in a humble manner; "he humbly lowered his head" [syn: {humbly}]
