---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fee
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484472901
title: fee
categories:
    - Dictionary
---
fee
     n 1: a fixed charge for a privilege or for professional services
     2: an interest in land capable of being inherited
     v : give a tip or gratuity to in return for a service, beyond
         the agreed-on compensation; "Remember to tip the waiter";
         "fee the steward" [syn: {tip}, {bung}]
