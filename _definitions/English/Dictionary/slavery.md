---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slavery
offline_file: ""
offline_thumbnail: ""
uuid: 279f71eb-514f-4c3d-a961-079b4806f347
updated: 1484310567
title: slavery
categories:
    - Dictionary
---
slavery
     n 1: the state of being under the control of another person [syn:
           {bondage}, {thrall}, {thralldom}, {thraldom}]
     2: the practice of owning slaves [syn: {slaveholding}]
     3: work done under harsh conditions for little or no pay
