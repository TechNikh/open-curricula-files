---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 818a69e5-8582-449d-8dfb-bcd82dcfa231
updated: 1484308868
title: Thermal equilibrium
tags:
    - Two varieties of thermal equilibrium
    - >
        Relation of thermal equilibrium between two thermally
        connected bodies
    - Internal thermal equilibrium of an isolated body
    - Thermal contact
    - >
        Bodies prepared with separately uniform temperatures, then
        put into purely thermal communication with each other
    - Change of internal state of an isolated system
    - In a gravitational field
    - Distinctions between thermal and thermodynamic equilibria
    - Citations
    - Citation references
categories:
    - Thermodynamics
---
Two physical systems are in thermal equilibrium if no heat flows between them when they are connected by a path permeable to heat. Thermal equilibrium obeys the zeroth law of thermodynamics. A system is said to be in thermal equilibrium with itself if the temperature within the system is spatially and temporally uniform.
