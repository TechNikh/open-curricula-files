---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spy
offline_file: ""
offline_thumbnail: ""
uuid: be3eb46e-0052-4c3c-be32-733bfa7a6d01
updated: 1484310180
title: spy
categories:
    - Dictionary
---
spy
     n 1: (military) a secret agent hired by a state to obtain
          information about its enemies or by a business to obtain
          industrial secrets from competitors [syn: {undercover
          agent}]
     2: a secret watcher; someone who secretly watches other people;
        "my spies tell me that you had a good time last night"
     v 1: catch sight of [syn: {descry}, {spot}, {espy}]
     2: watch, observe, or inquire secretly [syn: {stag}, {snoop}, {sleuth}]
     3: secretly collect sensitive or classified information; engage
        in espionage; "spy for the Russians"
     [also: {spied}]
