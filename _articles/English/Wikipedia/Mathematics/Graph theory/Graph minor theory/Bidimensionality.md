---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bidimensionality
offline_file: ""
offline_thumbnail: ""
uuid: e0a77385-a36b-4ffb-9e9a-169cba79b317
updated: 1484309411
title: Bidimensionality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Gamma_graph.jpg
tags:
    - Definition
    - Excluded grid theorems
    - Subexponential parameterized algorithms
    - Polynomial time approximation schemes
    - Kernelization
    - Notes
categories:
    - Graph minor theory
---
Bidimensionality theory characterizes a broad range of graph problems (bidimensional) that admit efficient approximate, fixed-parameter or kernel solutions in a broad range of graphs. These graph classes include planar graphs, map graphs, bounded-genus graphs and graphs excluding any fixed minor. In particular, bidimensionality theory builds on the graph minor theory of Robertson and Seymour by extending the mathematical results and building new algorithmic tools. The theory was introduced in the work of Demaine, Fomin, Hajiaghayi, and Thilikos,[1] for which the authors received the Nerode Prize in 2015.
