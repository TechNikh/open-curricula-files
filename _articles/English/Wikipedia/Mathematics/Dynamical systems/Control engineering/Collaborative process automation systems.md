---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Collaborative_process_automation_systems
offline_file: ""
offline_thumbnail: ""
uuid: ef48a22a-159b-4e73-b84d-904f099afd64
updated: 1484308735
title: Collaborative process automation systems
categories:
    - Control engineering
---
Distributed control systems (DCSs) evolved into process automation systems (PAS) by the inclusion of additional functionality beyond basic control. The evolution of PAS into the collaborative process automation systems (CPAS) will add even more capability. Process automation systems (PAS) will be considered the sentinel of plant performance in the next phase of their evolution. They will continue to facilitate process control but will also become the primary source of manufacturing data and information for collaborative manufacturing management (CMM) applications all within a robust environment.
