---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Deligne%E2%80%93Mumford_stack'
offline_file: ""
offline_thumbnail: ""
uuid: c7db8b2c-cba9-4ee1-b2e7-b6abc12574ff
updated: 1484309142
title: Deligne–Mumford stack
categories:
    - Algebraic geometry
---
If the "étale" is weakened to "smooth", then such a stack is called an Artin stack. An algebraic space is Deligne–Mumford.
