---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Duality_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: a8386975-4a6e-48f1-aed4-29c8e9373cb1
updated: 1484309231
title: Duality (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dual_cone_illustration.svg_0.png
tags:
    - Introductory examples
    - Complement of a subset
    - Dual cone
    - Dual vector space
    - Galois theory
    - Order-reversing dualities
    - Dimension-reversing dualities
    - Duality in logic and set theory
    - Dual objects
    - Dual vector spaces revisited
    - 'Isomorphisms of V and V* and inner product spaces'
    - Duality in projective geometry
    - Topological vector spaces and Hilbert spaces
    - Further dual objects
    - Dual categories
    - Opposite category and adjoint functors
    - Spaces and functions
    - Galois connections
    - Pontryagin duality
    - Analytic dualities
    - Homology and cohomology
    - Poincaré duality
    - Duality in algebraic and arithmetic geometry
    - Notes
    - Duality in general
    - Duality in algebraic topology
    - Specific dualities
categories:
    - Duality theories
---
In mathematics, a duality, generally speaking, translates concepts, theorems or mathematical structures into other concepts, theorems or structures, in a one-to-one fashion, often (but not always) by means of an involution operation: if the dual of A is B, then the dual of B is A. Such involutions sometimes have fixed points, so that the dual of A is A itself. For example, Desargues' theorem is self-dual in this sense under the standard duality in projective geometry.
