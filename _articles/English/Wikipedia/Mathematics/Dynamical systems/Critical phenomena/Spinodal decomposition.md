---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spinodal_decomposition
offline_file: ""
offline_thumbnail: ""
uuid: aa539f88-3627-4a73-9efb-c35a530ee827
updated: 1484308749
title: Spinodal decomposition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/CahnHilliard_Animation.gif
tags:
    - Early evidence
    - Gibbs criteria
    - Gradient energy
    - Fourier components
    - Phase diagram
    - Diffusion equation
    - Coherency strains
    - Fourier transform
    - Dynamics in k-space
categories:
    - Critical phenomena
---
Spinodal decomposition is a mechanism for the rapid unmixing of a mixture of liquids or solids [1] from one thermodynamic phase, to form two coexisting phases. As an example, consider a hot mixture of water and an oil. At high temperatures the oil and the water may mix to form a single thermodynamic phase in which water molecules are surrounded by oil molecules and vice versa. The mixture is then suddenly cooled to a temperature at which thermodynamic equilibrium favours an oil-rich phase coexisting with a water-rich phase. Spinodal decomposition then occurs when the mixture is such that there is essentially no barrier to nucleation of the new oil-rich and water-rich phases. In other words, ...
