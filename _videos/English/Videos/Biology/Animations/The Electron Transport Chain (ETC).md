---
version: 1
type: video
provider: YouTube
id: RvqR4pExHX8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/The%20Electron%20Transport%20Chain%20%28ETC%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5e288cb2-f769-4a40-b8fa-f621176c0aab
updated: 1486069668
title: The Electron Transport Chain (ETC)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/RvqR4pExHX8/default.jpg
    - https://i3.ytimg.com/vi/RvqR4pExHX8/1.jpg
    - https://i3.ytimg.com/vi/RvqR4pExHX8/2.jpg
    - https://i3.ytimg.com/vi/RvqR4pExHX8/3.jpg
---
