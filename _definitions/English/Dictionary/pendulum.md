---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pendulum
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484331601
title: pendulum
categories:
    - Dictionary
---
pendulum
     n : an apparatus consisting of an object mounted so that it
         swings freely under the influence of gravity
