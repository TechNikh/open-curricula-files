---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abc
offline_file: ""
offline_thumbnail: ""
uuid: 2e81b4b9-0f68-4bf2-9083-31f3c645857d
updated: 1484310473
title: abc
categories:
    - Dictionary
---
ABC
     n : the elementary stages of any subject (usually plural); "he
         mastered only the rudiments of geometry" [syn: {rudiment},
          {first rudiment}, {first principle}, {alphabet}, {ABC's},
          {ABCs}]
