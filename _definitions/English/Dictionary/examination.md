---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/examination
offline_file: ""
offline_thumbnail: ""
uuid: c17594fd-640e-4b8e-b908-4ba18d989324
updated: 1484310311
title: examination
categories:
    - Dictionary
---
examination
     n 1: the act of examining something closely (as for mistakes)
          [syn: {scrutiny}]
     2: a set of questions or exercises evaluating skill or
        knowledge; "when the test was stolen the professor had to
        make a new set of questions" [syn: {exam}, {test}]
     3: formal systematic questioning [syn: {interrogation}, {interrogatory}]
     4: examination of conscience (as done daily by Jesuits) [syn: {examen}]
     5: the act of giving students or candidates a test (as by
        questions) to determine what they know or have learned
        [syn: {testing}]
