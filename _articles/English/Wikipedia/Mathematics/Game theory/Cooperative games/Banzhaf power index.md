---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Banzhaf_power_index
offline_file: ""
offline_thumbnail: ""
uuid: aa313a0a-1d60-4021-9e56-1fbfc421e892
updated: 1484308528
title: Banzhaf power index
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-BanzhafPowerIndex.jpg
tags:
    - Examples
    - Voting game
    - Simple voting game
    - U.S. Electoral College
    - Cartel game
    - History
    - Footnotes
categories:
    - Cooperative games
---
The Banzhaf power index, named after John F. Banzhaf III (originally invented by Lionel Penrose in 1946 and sometimes called Penrose–Banzhaf index; also known as the Banzhaf–Coleman index after James Samuel Coleman), is a power index defined by the probability of changing an outcome of a vote where voting rights are not necessarily equally divided among the voters or shareholders.
