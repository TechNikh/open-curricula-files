---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quadratic_equation
offline_file: ""
offline_thumbnail: ""
uuid: c8ddf0d9-804a-43d4-b215-f55779a5c170
updated: 1484308909
title: Quadratic equation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Quadratic_formula.svg.png
tags:
    - Solving the quadratic equation
    - Factoring by inspection
    - Completing the square
    - Quadratic formula and its derivation
    - Reduced quadratic equation
    - Discriminant
    - Geometric interpretation
    - Quadratic factorization
    - Graphing for real roots
    - Avoiding loss of significance
    - Examples and applications
    - History
    - Advanced topics
    - Alternative methods of root calculation
    - "Vieta's formulas"
    - Trigonometric solution
    - Solution for complex roots in polar coordinates
    - Geometric solution
    - Generalization of quadratic equation
    - Characteristic 2
categories:
    - Elementary algebra
---
where x represents an unknown, and a, b, and c represent known numbers such that a is not equal to 0. If a = 0, then the equation is linear, not quadratic. The numbers a, b, and c are the coefficients of the equation, and may be distinguished by calling them, respectively, the quadratic coefficient, the linear coefficient and the constant or free term.[1]
