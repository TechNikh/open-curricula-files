---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stretching
offline_file: ""
offline_thumbnail: ""
uuid: bf40f5c0-46e2-4545-9157-558b89d1effb
updated: 1484310290
title: stretching
categories:
    - Dictionary
---
stretching
     adj : extending far; "beyond the misty gray of the rain he saw the
           stretching hutment"; "wide-spreading plains" [syn: {stretching(a)},
            {wide-spreading}]
     n 1: act of expanding by lengthening or widening
     2: exercise designed to extend the limbs and muscles to their
        full extent [syn: {stretch}]
