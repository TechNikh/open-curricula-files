---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Particle_number
offline_file: ""
offline_thumbnail: ""
uuid: 6d80a69e-721d-4426-a422-5386abc016a4
updated: 1484308855
title: Particle number
tags:
    - Determining the particle number
    - Particle number density
    - In quantum mechanics
    - In air quality
categories:
    - Thermodynamics
---
The particle number (or number of particles) of a thermodynamic system, conventionally indicated with the letter N, is the number of constituent particles in that system.[1] The particle number is a fundamental parameter in thermodynamics which is conjugate to the chemical potential. Unlike most physical quantities, particle number is a dimensionless quantity. It is an extensive parameter, as it is directly proportional to the size of the system under consideration, and thus meaningful only for closed systems.
