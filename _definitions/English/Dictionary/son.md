---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/son
offline_file: ""
offline_thumbnail: ""
uuid: cefb46c8-ef3c-4da3-bc0b-3930b3f836ae
updated: 1484310459
title: son
categories:
    - Dictionary
---
son
     n 1: a male human offspring; "their son became a famous judge";
          "his boy is taller than he is" [syn: {boy}] [ant: {daughter},
           {daughter}]
     2: the divine word of God; the second person in the Trinity
        (incarnate in Jesus) [syn: {Word}, {Logos}]
