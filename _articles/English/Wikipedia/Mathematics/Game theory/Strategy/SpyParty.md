---
version: 1
type: article
id: https://en.wikipedia.org/wiki/SpyParty
offline_file: ""
offline_thumbnail: ""
uuid: 9da6987a-d687-41bc-b1d2-6b166f61fb58
updated: 1484309065
title: SpyParty
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-SpyParty_Sniper_Beta_Gameplay.png
tags:
    - Gameplay
    - Development
categories:
    - Strategy
---
SpyParty is an indie video game currently being developed by Chris Hecker. SpyParty was first shown at the Experimental Gameplay Workshop at the 2009 Game Developers Conference. Hecker describes his game as "an asymmetric multiplayer espionage game, dealing with the subtlety of human behavior, character, personality, and social mores, instead of the usual spy games explosions and car chases"[1]
