---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alakananda
offline_file: ""
offline_thumbnail: ""
uuid: 52e4d626-b651-4c8c-94d1-9b4adc9ba5f6
updated: 1484308432
title: Alaknanda River
tags:
    - Overview
    - Attractions
    - Recreation
    - Panch Prayag
    - Dams
    - Towns along Alaknanda River
    - Photo gallery
categories:
    - Uncategorized
---
The Alaknanda is a Himalayan river in the Indian state of Uttarakhand and one of the two headstreams of the Ganga, the major river of Northern India and the holy river of Hinduism. In hydrology, the Alaknanda is considered the source stream of the Ganges on account of its greater length and discharge;[1] however, in Hindu mythology and culture, the other headstream, the Bhagirathi, is considered the source stream.
