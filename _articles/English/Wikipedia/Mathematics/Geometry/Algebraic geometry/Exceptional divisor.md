---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exceptional_divisor
offline_file: ""
offline_thumbnail: ""
uuid: 03a94a81-c460-45b5-9e1b-34e5f9137b8c
updated: 1484309145
title: Exceptional divisor
categories:
    - Algebraic geometry
---
of varieties is a kind of 'large' subvariety of 
  
    
      
        X
      
    
    {\displaystyle X}
  
 which is 'crushed' by 
  
    
      
        f
      
    
    {\displaystyle f}
  
, in a certain definite sense. More strictly, f has an associated exceptional locus which describes how it identifies nearby points in codimension one, and the exceptional divisor is an appropriate algebraic construction whose support is the exceptional locus. The same ideas can be found in the theory of holomorphic mappings of complex manifolds.
