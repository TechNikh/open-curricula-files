---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/basis
offline_file: ""
offline_thumbnail: ""
uuid: 45dc5b3c-c874-4f20-b832-4e4e777efe15
updated: 1484310341
title: basis
categories:
    - Dictionary
---
basis
     n 1: a relation that provides the foundation for something; "they
          were on a friendly footing"; "he worked on an interim
          basis" [syn: {footing}, {ground}]
     2: the fundamental assumptions from which something is begun or
        developed or calculated or explained; "the whole argument
        rested on a basis of conjecture" [syn: {base}, {foundation},
         {fundament}, {groundwork}, {cornerstone}]
     3: the most important or necessary part of something; "the
        basis of this drink is orange juice" [syn: {base}]
