---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/continue
offline_file: ""
offline_thumbnail: ""
uuid: 3c32d636-6ee6-4c1a-953e-e24468647a0e
updated: 1484310357
title: continue
categories:
    - Dictionary
---
continue
     v 1: continue a certain state, condition, or activity; "Keep on
          working!"; "We continued to work into the night"; "Keep
          smiling"; "We went on working until well past midnight"
          [syn: {go on}, {proceed}, {go along}, {keep}] [ant: {discontinue}]
     2: continue with one's activities; "I know it's hard," he
        continued, "but there is no choice"; "carry on--pretend we
        are not in the room" [syn: {go on}, {carry on}, {proceed}]
     3: keep or maintain in unaltered condition; cause to remain or
        last; "preserve the peace in the family"; "continue the
        family tradition"; "Carry on the old traditions" [syn: {uphold},
         ...
