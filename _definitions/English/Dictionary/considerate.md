---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/considerate
offline_file: ""
offline_thumbnail: ""
uuid: 6f8b4478-93fa-4020-ad0a-410f5cdd0173
updated: 1484310555
title: considerate
categories:
    - Dictionary
---
considerate
     adj : showing concern for the rights and feelings of others;
           "friends considerate enough to leave us alone" [ant: {inconsiderate}]
