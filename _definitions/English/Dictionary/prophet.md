---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prophet
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484522461
title: prophet
categories:
    - Dictionary
---
prophet
     n 1: an authoritative person who divines the future [syn: {oracle},
           {seer}, {vaticinator}]
     2: someone who speaks by divine inspiration; someone who is an
        interpreter of the will of God
