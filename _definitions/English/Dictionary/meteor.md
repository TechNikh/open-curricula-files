---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meteor
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484559421
title: meteor
categories:
    - Dictionary
---
meteor
     n 1: a streak of light in the sky at night that results when a
          meteoroid hits the earth's atmosphere and air friction
          causes the meteoroid to melt or vaporize or explode
          [syn: {shooting star}]
     2: (astronomy) any of the small solid extraterrestrial bodies
        that hits the earth's atmosphere [syn: {meteoroid}]
