---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Embryologist
offline_file: ""
offline_thumbnail: ""
uuid: bc242878-7b66-4f00-b0f0-18e8c1ee7139
updated: 1484308344
title: Embryology
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Blastulation.png
tags:
    - Embryonic development of animals
    - Bilateria
    - Drosophila melanogaster (fruit fly)
    - Humans
    - History
    - After 1827
    - After 1950
    - Vertebrate and invertebrate embryology
    - Modern embryology research
    - Citations
    - Sources
categories:
    - Uncategorized
---
Embryology (from Greek ἔμβρυον, embryon, "the unborn, embryo"; and -λογία, -logia) is the branch of biology that studies the prenatal development of gametes (sex cells), fertilization, and development of embryos and fetuses. Additionally, embryology is the study of congenital disorders that occur before birth.[1]
