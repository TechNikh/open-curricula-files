---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/owing
offline_file: ""
offline_thumbnail: ""
uuid: 3f06eede-a8ed-4681-a042-4697332b06ca
updated: 1484310433
title: owing
categories:
    - Dictionary
---
owing
     adj 1: owed as a debt; "outstanding bills"; "the amount still
            owed"; "undischarged debts" [syn: {outstanding}, {owed},
             {owing(p)}, {undischarged}]
     2: owed as a debt; "must pay what is owing"; "sleep owing to
        you because of a long vigil" [syn: {owing(p)}]
