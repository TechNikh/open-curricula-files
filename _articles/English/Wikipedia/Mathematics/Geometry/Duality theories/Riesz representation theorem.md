---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Riesz_representation_theorem
offline_file: ""
offline_thumbnail: ""
uuid: fc19f906-d060-4c1c-af1d-6b4e96da4bc8
updated: 1484309237
title: Riesz representation theorem
categories:
    - Duality theories
---
This article will describe his theorem concerning the dual of a Hilbert space, which is sometimes called the Fréchet-Riesz theorem. For the theorems relating linear functionals to measures, see Riesz–Markov–Kakutani representation theorem.
