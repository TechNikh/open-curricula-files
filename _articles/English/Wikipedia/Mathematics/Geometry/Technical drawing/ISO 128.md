---
version: 1
type: article
id: https://en.wikipedia.org/wiki/ISO_128
offline_file: ""
offline_thumbnail: ""
uuid: acb9947a-9bc5-46dd-afe0-ad86fcc7e85d
updated: 1484309329
title: ISO 128
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-A_size_illustration.svg.png
tags:
    - Overview
    - Composition of the ISO 128
    - Other ISO standard related to technical drawing
categories:
    - Technical drawing
---
ISO 128 is an international standard (ISO), about the general principles of presentation in technical drawings, specifically the graphical representation of objects on technical drawings.[1]
