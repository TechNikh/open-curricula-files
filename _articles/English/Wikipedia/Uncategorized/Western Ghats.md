---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sahayadri
offline_file: ""
offline_thumbnail: ""
uuid: 3f5ea475-abdd-49f8-99fb-7e00456bb754
updated: 1484308440
title: Western Ghats
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tamil_Nadu_topo_deutsch_mit_Gebirgen.png
tags:
    - Geology
    - Topography
    - Peaks
    - Water bodies
    - Climate
    - Ecoregions
    - Biodiversity protection
    - Fauna
    - Flora
    - Notes
categories:
    - Uncategorized
---
Western Ghats (also known as Sahyadri meaning The Benevolent Mountains) is a mountain range that runs parallel to the western coast of the Indian peninsula, located entirely in India. It is a UNESCO World Heritage Site and is one of the eight "hottest hot-spots" of biological diversity in the world.[1][2] It is sometimes called the Great Escarpment of India.[3] The range runs north to south along the western edge of the Deccan Plateau, and separates the plateau from a narrow coastal plain, called Konkan, along the Arabian Sea. A total of thirty nine properties including national parks, wildlife sanctuaries and reserve forests were designated as world heritage sites - twenty in Kerala, ten ...
