---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isolation_(poker)
offline_file: ""
offline_thumbnail: ""
uuid: 6c4c90c6-f2c4-4b36-be1b-a970ada77131
updated: 1484309059
title: Isolation (poker)
categories:
    - Poker strategy
---
In poker, an isolation play is usually a raise designed to encourage one or more players to fold, specifically for the purpose of making the hand a one-on-one contest with a specific opponent. For example, if an opponent raises and a player suspects he is holding a weak, but playable hand, they may reraise to pressure other opponents to fold, with the aim of getting heads up with the opening raiser.
