---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tristearin
offline_file: ""
offline_thumbnail: ""
uuid: de936f0f-5d72-472e-be54-7fe12769665a
updated: 1484310426
title: tristearin
categories:
    - Dictionary
---
tristearin
     n : a triglyceride of stearic acid [syn: {glycerol tristearate}]
