---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/productive
offline_file: ""
offline_thumbnail: ""
uuid: 6e6f465d-394a-4fa5-80b5-9a8262a76d02
updated: 1484310455
title: productive
categories:
    - Dictionary
---
productive
     adj 1: producing or capable of producing (especially abundantly);
            "productive farmland"; "his productive years"; "a
            productive collaboration" [ant: {unproductive}]
     2: having the ability to produce or originate; "generative
        power"; "generative forces" [syn: {generative}] [ant: {consumptive}]
     3: yielding positive results
     4: marked by great fruitfulness; "fertile farmland"; "a fat
        land"; "a productive vineyard"; "rich soil" [syn: {fat}, {fertile},
         {rich}]
