---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swimmer
offline_file: ""
offline_thumbnail: ""
uuid: b79f192c-c6d8-4078-b5db-5a433bd75418
updated: 1484310286
title: swimmer
categories:
    - Dictionary
---
swimmer
     n 1: a trained athlete who participates in swimming meets; "he
          was an Olympic swimmer"
     2: a person who travels through the water by swimming; "he is
        not a good swimmer" [syn: {natator}, {bather}]
