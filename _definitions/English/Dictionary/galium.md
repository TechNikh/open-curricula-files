---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/galium
offline_file: ""
offline_thumbnail: ""
uuid: dabf73e4-d94c-4858-ad1c-96c42b081d0d
updated: 1484310156
title: galium
categories:
    - Dictionary
---
Galium
     n : annual or perennial herbs: bedstraw; cleavers [syn: {genus
         Galium}]
