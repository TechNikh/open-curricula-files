---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Impunity_game
offline_file: ""
offline_thumbnail: ""
uuid: 3bf522b7-3527-4b9b-a02f-ea793ed3de98
updated: 1484309052
title: Impunity game
categories:
    - Non-cooperative games
---
The first player "the proposer" chooses between two possible divisions of some endowment (such as a cash prize):
