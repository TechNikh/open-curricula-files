---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperbolic_geometric_graph
offline_file: ""
offline_thumbnail: ""
uuid: 09e689c8-1db3-43e2-b8c7-54302423cf4b
updated: 1484309435
title: Hyperbolic geometric graph
tags:
    - Mathematical formulation
    - Connectivity decay function
    - Findings
    - Applications
categories:
    - Network theory
---
A hyperbolic geometric graph (HGG) or hyperbolic geometric network (HGN) is special spatial network where nodes are (1) sprinkled according to a probability distribution onto a hyperbolic space of constant negative curvature and (2) an edge between two nodes is present if they are close according to a function of the metric,[1][2] a HGG generalizes a random geometric graph (RGG) whose embedding space is Euclidean.
