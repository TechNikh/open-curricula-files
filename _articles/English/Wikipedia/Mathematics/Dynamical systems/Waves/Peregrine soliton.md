---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Peregrine_soliton
offline_file: ""
offline_thumbnail: ""
uuid: 0b3ccd92-88ce-4aff-ad59-273ff884a648
updated: 1484308898
title: Peregrine soliton
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Soliton_de_Peregrine.png
tags:
    - Main properties
    - Mathematical expression
    - In the spatio-temporal domain
    - In the spectral domain
    - Different interpretations of the Peregrine soliton
    - As a rational soliton
    - As an Akhmediev breather
    - As a Kuznetsov-Ma soliton
    - Experimental demonstration
    - Generation in optics
    - Generation in hydrodynamics
    - Generation in other fields of physics
    - Notes and references
categories:
    - Waves
---
The Peregrine soliton (or Peregrine breather) is an analytic solution of the nonlinear Schrödinger equation.[1] This solution has been proposed in 1983 by Howell Peregrine, researcher at the mathematics department of the University of Bristol.
