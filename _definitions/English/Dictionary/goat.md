---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/goat
offline_file: ""
offline_thumbnail: ""
uuid: db0dd7b7-fa15-4100-98ae-75f76a414e3f
updated: 1484310279
title: goat
categories:
    - Dictionary
---
goat
     n 1: any of numerous agile ruminants related to sheep but having
          a beard and straight horns [syn: {caprine animal}]
     2: a victim of ridicule or pranks [syn: {butt}, {laughingstock},
         {stooge}]
     3: (astrology) a person who is born while the sun is in
        Capricorn [syn: {Capricorn}]
     4: the tenth sign of the zodiac; the sun is in this sign from
        about December 22 to January 19 [syn: {Capricorn}, {Capricorn
        the Goat}]
