---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bestow
offline_file: ""
offline_thumbnail: ""
uuid: 2c348184-882d-4ce4-8973-dd8246904d71
updated: 1484310426
title: bestow
categories:
    - Dictionary
---
bestow
     v 1: present; "The university conferred a degree on its most
          famous former student, who never graduated"; "bestow an
          honor on someone" [syn: {confer}]
     2: give as a gift
     3: bestow a quality on; "Her presence lends a certain cachet to
        the company"; "The music added a lot to the play"; "She
        brings a special atmosphere to our meetings"; "This adds a
        light note to the program" [syn: {lend}, {impart}, {contribute},
         {add}, {bring}]
