---
version: 1
type: video
provider: YouTube
id: thnDxFdkzZs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/The%20History%20of%20Atomic%20Chemistry-%20Crash%20Course%20Chemistry%20%2337.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 76e705a3-bed5-4d41-bda9-12e77ab7658b
updated: 1486069662
title: 'The History of Atomic Chemistry- Crash Course Chemistry #37'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/thnDxFdkzZs/default.jpg
    - https://i3.ytimg.com/vi/thnDxFdkzZs/1.jpg
    - https://i3.ytimg.com/vi/thnDxFdkzZs/2.jpg
    - https://i3.ytimg.com/vi/thnDxFdkzZs/3.jpg
---
