---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dipped
offline_file: ""
offline_thumbnail: ""
uuid: 6af4a986-be32-4e8d-aaaf-1df2615bec00
updated: 1484310206
title: dipped
categories:
    - Dictionary
---
dip
     n 1: a depression in an otherwise level surface; "there was a dip
          in the road"
     2: (physics) the angle that a magnetic needle makes with the
        plane of the horizon [syn: {angle of dip}, {magnetic dip},
         {magnetic inclination}, {inclination}]
     3: a thief who steals from the pockets or purses of others in
        public places [syn: {pickpocket}, {cutpurse}]
     4: tasty mixture or liquid into which bite-sized foods are
        dipped
     5: a brief immersion
     6: a sudden sharp decrease in some quantity; "a drop of 57
        points on the Dow Jones index"; "there was a drop in
        pressure in the pulmonary artery"; "a dip in prices";
        ...
