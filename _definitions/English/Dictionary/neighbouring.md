---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neighbouring
offline_file: ""
offline_thumbnail: ""
uuid: 33f19ab5-57a0-4871-90b3-dfd335634dbe
updated: 1484310238
title: neighbouring
categories:
    - Dictionary
---
neighbouring
     adj : situated near one another; "neighbor states" [syn: {neighbor},
            {neighbour}, {neighboring(a)}, {neighbouring(a)}]
