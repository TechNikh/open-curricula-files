---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Class_(set_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 977a0357-6963-409f-8a3d-cf8a9b36e1df
updated: 1484309501
title: Class (set theory)
tags:
    - Examples
    - Paradoxes
    - Classes in formal set theories
categories:
    - Set theory
---
In set theory and its applications throughout mathematics, a class is a collection of sets (or sometimes other mathematical objects) that can be unambiguously defined by a property that all its members share. The precise definition of "class" depends on foundational context. In work on Zermelo–Fraenkel set theory, the notion of class is informal, whereas other set theories, such as Von Neumann–Bernays–Gödel set theory, axiomatize the notion of "proper class", e.g., as entities that are not members of another entity.
