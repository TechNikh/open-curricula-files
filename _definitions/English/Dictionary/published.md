---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/published
offline_file: ""
offline_thumbnail: ""
uuid: 38bd181a-a535-41d6-b218-70986d031fa3
updated: 1484310286
title: published
categories:
    - Dictionary
---
published
     adj 1: prepared and printed for distribution and sale; "the
            complete published works Dickens" [ant: {unpublished}]
     2: formally made public; "published accounts" [syn: {promulgated}]
