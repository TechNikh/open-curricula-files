---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frobenioid
offline_file: ""
offline_thumbnail: ""
uuid: e04c89b4-77d5-4ef4-a2c4-3ea925cf7f41
updated: 1484308641
title: Frobenioid
tags:
    - The Frobenioid of a monoid
    - Elementary Frobenioids
    - Frobenioids
categories:
    - Number theory
---
In arithmetic geometry, a Frobenioid is a category with some extra structure that generalizes the theory of line bundles on models of finite extensions of global fields. Frobenioids were introduced by Shinichi Mochizuki (2008). The word "Frobenioid" is a portmanteau of Frobenius and monoid, as certain Frobenius morphisms between Frobenioids are analogues of the usual Frobenius morphism, and some of the simplest examples of Frobenioids are essentially monoids.
