---
version: 1
type: video
provider: YouTube
id: orI2m6IarJg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/MCAT/Biomolecules/Oxidation%20and%20reduction%20review%20from%20biological%20point-of-view.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4a3d9533-f9f9-46aa-93bb-20f5f7852fb5
updated: 1486069579
title: Oxidation and reduction review from biological point-of-view
tags:
    - dotsub
categories:
    - Biomolecules
thumbnail_urls:
    - https://i3.ytimg.com/vi/orI2m6IarJg/default.jpg
    - https://i3.ytimg.com/vi/orI2m6IarJg/1.jpg
    - https://i3.ytimg.com/vi/orI2m6IarJg/2.jpg
    - https://i3.ytimg.com/vi/orI2m6IarJg/3.jpg
---
Taking a look at oxidation and reduction in a biological context. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/test-prep/mcat/biomolecules/overview-metabolism/v/oxidation-and-reduction-in-metabolism?utm_source=YT&utm_medium=Desc&utm_campaign=mcat

Missed the previous lesson? https://www.khanacademy.org/test-prep/mcat/biomolecules/overview-metabolism/v/atp-hydrolysis-transfer-of-a-phosphate-group?utm_source=YT&utm_medium=Desc&utm_campaign=mcat

MCAT on Khan Academy: Go ahead and practice some passage-based questions!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s MCAT channel: https://www.youtube.com/channel/UCDkK5wqSuwDlJ3_nl3rgdiQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
