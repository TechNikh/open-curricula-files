---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decomposition
offline_file: ""
offline_thumbnail: ""
uuid: 03a2cd62-a626-4ac8-9c35-671a502f53aa
updated: 1484310266
title: decomposition
categories:
    - Dictionary
---
decomposition
     n 1: the analysis of a vector field [syn: {vector decomposition}]
     2: in a decomposed state [syn: {disintegration}]
     3: (chemistry) separation of a substance into two or more
        substances that may differ from each other and from the
        original substance [syn: {decomposition reaction}]
     4: the organic phenomenon of rotting [syn: {decay}]
     5: (biology) decaying caused by bacterial or fungal action
        [syn: {rot}, {rotting}, {putrefaction}]
