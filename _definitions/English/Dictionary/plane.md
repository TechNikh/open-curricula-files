---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plane
offline_file: ""
offline_thumbnail: ""
uuid: 4bf55e0b-95ec-46b1-99bb-3eb3da24a631
updated: 1484310224
title: plane
categories:
    - Dictionary
---
plane
     adj : having a horizontal surface in which no part is higher or
           lower than another; "a flat desk"; "acres of level
           farmland"; "a plane surface" [syn: {flat}, {level}]
     n 1: an aircraft that has a fixed wing and is powered by
          propellers or jets; "the flight was delayed due to
          trouble with the airplane" [syn: {airplane}, {aeroplane}]
     2: (mathematics) an unbounded two-dimensional shape; "we will
        refer to the plane of the graph as the X-Y plane"; "any
        line joining two points on a plane lies wholly on that
        plane" [syn: {sheet}]
     3: a level of existence or development; "he lived on a worldly
        plane"
  ...
