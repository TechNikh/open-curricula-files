---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assimilation
offline_file: ""
offline_thumbnail: ""
uuid: 51828dd2-b3c0-43a4-907a-e781dcc55984
updated: 1484310365
title: assimilation
categories:
    - Dictionary
---
assimilation
     n 1: the state of being assimilated; people of different
          backgrounds come to see themselves as part of a larger
          national family
     2: the social process of absorbing one cultural group into
        harmony with another [syn: {absorption}]
     3: the process of absorbing nutrients into the body after
        digestion [syn: {absorption}]
     4: a linguistic process by which a sound becomes similar to an
        adjacent sound
     5: the process of assimilating new ideas into an existing
        cognitive structure [syn: {acculturation}]
     6: in the theories of Jean Piaget: the application of a general
        schema to a particular instance
