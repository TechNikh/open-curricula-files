---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quarrel
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484442841
title: quarrel
categories:
    - Dictionary
---
quarrel
     n 1: an angry dispute; "they had a quarrel"; "they had words"
          [syn: {wrangle}, {row}, {words}, {run-in}, {dustup}]
     2: an arrow that is shot from a crossbow; has a head with four
        edges
     v : have a disagreement over something; "We quarreled over the
         question as to who discovered America"; "These tewo
         fellows are always scrapping over something" [syn: {dispute},
          {scrap}, {argufy}, {altercate}]
     [also: {quarrelling}, {quarrelled}]
