---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%CE%A9-logic'
offline_file: ""
offline_thumbnail: ""
uuid: 72daf2b0-3c07-44f3-949f-d3ad4056845f
updated: 1484309510
title: Ω-logic
categories:
    - Set theory
---
In set theory, Ω-logic is an infinitary logic and deductive system proposed by W. Hugh Woodin (1999) as part of an attempt to generalize the theory of determinacy of pointclasses to cover the structure 
  
    
      
        
          H
          
            
              ℵ
              
                2
              
            
          
        
      
    
    {\displaystyle H_{\aleph _{2}}}
  
. Just as the axiom of projective determinacy yields a canonical theory of 
  
    
      
        
          H
          
            
              ℵ
              
                1
              
            
          
        
      
    
    {\displaystyle H_{\aleph _{1}}}
  ...
