---
version: 1
type: video
provider: YouTube
id: yAQSucmHrAk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/The%20mohorovicic%20seismic%20discontinuity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7f8b35ce-c0a3-4961-b24e-305f706498af
updated: 1486069688
title: The mohorovicic seismic discontinuity
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/yAQSucmHrAk/default.jpg
    - https://i3.ytimg.com/vi/yAQSucmHrAk/1.jpg
    - https://i3.ytimg.com/vi/yAQSucmHrAk/2.jpg
    - https://i3.ytimg.com/vi/yAQSucmHrAk/3.jpg
---
