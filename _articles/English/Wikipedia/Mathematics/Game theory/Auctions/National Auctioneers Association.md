---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/National_Auctioneers_Association
offline_file: ""
offline_thumbnail: ""
uuid: d96b56f2-8a85-48c2-8061-f0f5bb3ac66c
updated: 1484309017
title: National Auctioneers Association
categories:
    - Auctions
---
The National Auctioneers Association (NAA), founded in 1949, is an advocacy group representing auctioneers, auction businesses and related companies that seeks to promote the auction method of marketing and the practice of auctioneering in the United States. Its headquarters is located at 8880 Ballentine, Overland Park, Kansas, 66214, USA.
