---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/admire
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484439121
title: admire
categories:
    - Dictionary
---
admire
     v 1: feel admiration for [syn: {look up to}]
     2: look at with admiration
