---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/club
offline_file: ""
offline_thumbnail: ""
uuid: 08b51ec8-47e3-42fb-a680-08ad7e1647e2
updated: 1484310156
title: club
categories:
    - Dictionary
---
club
     n 1: a team of professional baseball players who play and travel
          together; "each club played six home games with teams in
          its own division" [syn: {baseball club}, {ball club}, {nine}]
     2: a formal association of people with similar interests; "he
        joined a golf club"; "they formed a small lunch society";
        "men from the fraternal order will staff the soup kitchen
        today" [syn: {society}, {guild}, {gild}, {lodge}, {order}]
     3: stout stick that is larger at one end; "he carried a club in
        self defense"; "he felt as if he had been hit with a club"
     4: a building occupied by a club; "the clubhouse needed a new
        roof" ...
