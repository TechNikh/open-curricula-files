---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Canonical_ring
offline_file: ""
offline_thumbnail: ""
uuid: 8144804b-6d00-4561-8ea3-01a3ffda4b69
updated: 1484309138
title: Canonical ring
tags:
    - Properties
    - Birational invariance
    - Fundamental conjecture of birational geometry
    - The plurigenera
    - Notes
categories:
    - Algebraic geometry
---
of sections of powers of the canonical bundle K. Its nth graded component (for 
  
    
      
        n
        ≥
        0
      
    
    {\displaystyle n\geq 0}
  
) is:
