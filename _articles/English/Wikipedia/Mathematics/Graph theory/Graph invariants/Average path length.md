---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Average_path_length
offline_file: ""
offline_thumbnail: ""
uuid: fec7524b-223d-4e56-9b02-07a1e824b8fa
updated: 1484309417
title: Average path length
tags:
    - concept
    - Definition
    - Applications
categories:
    - Graph invariants
---
Average path length is a concept in network topology that is defined as the average number of steps along the shortest paths for all possible pairs of network nodes. It is a measure of the efficiency of information or mass transport on a network.
