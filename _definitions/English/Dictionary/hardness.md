---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hardness
offline_file: ""
offline_thumbnail: ""
uuid: a220f175-41ec-4af1-959c-70a99287b438
updated: 1484310379
title: hardness
categories:
    - Dictionary
---
hardness
     n 1: the property of being rigid and resistant to pressure; not
          easily scratched; measured on Mohs scale [ant: {softness}]
     2: devoid of passion or feeling [syn: {unfeelingness}, {callousness},
         {insensibility}]
     3: the quality of being difficult to do; "he assigned a series
        of problems of increasing hardness"
     4: excessive sternness; "severity of character"; "the harshness
        of his punishment was inhuman"; "the rigors of boot camp"
        [syn: {severity}, {harshness}, {rigor}, {rigour}, {inclemency},
         {stiffness}]
