---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Refrigeration
offline_file: ""
offline_thumbnail: ""
uuid: f5bee9ba-83c3-44b5-aea1-60cc63026a31
updated: 1484308860
title: Refrigeration
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kuehlregal_USA.jpg
tags:
    - History
    - Earliest forms of cooling
    - Ice harvesting
    - Refrigeration research
    - Commercial use
    - Home and consumer use
    - Impact on settlement patterns
    - Refrigerated rail cars
    - Expansion west and into rural areas
    - Rise of the galactic city
    - Impact on agriculture and food production
    - Demographics
    - Meat packing and trade
    - Electricity in rural areas
    - Farm use
    - Effects on lifestyle and diet
    - Impact on nutrition
    - Current applications of refrigeration
    - Methods of refrigeration
    - Non-cyclic refrigeration
    - Cyclic refrigeration
    - Vapor-compression cycle
    - Vapor absorption cycle
    - Gas cycle
    - Thermoelectric refrigeration
    - Magnetic refrigeration
    - Other methods
    - Fridge Gate
    - Capacity ratings
categories:
    - Thermodynamics
---
Refrigeration is a process of moving heat from one location to another in controlled conditions. The work of heat transport is traditionally driven by mechanical work, but can also be driven by heat, magnetism, electricity, laser, or other means. Refrigeration has many applications, including, but not limited to: household refrigerators, industrial freezers, cryogenics, and air conditioning. Heat pumps may use the heat output of the refrigeration process, and also may be designed to be reversible, but are otherwise similar to air conditioning units.
