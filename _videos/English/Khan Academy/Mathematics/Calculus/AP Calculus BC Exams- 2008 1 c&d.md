---
version: 1
type: video
provider: YouTube
id: _l0Mfsu__gU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/AP%20Calculus%20BC%20Exams-%202008%201%20c%26d.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d9ce3edf-d685-44b0-bf5d-a25ef9d0b0d1
updated: 1486069690
title: 'AP Calculus BC Exams- 2008 1 c&d'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/_l0Mfsu__gU/default.jpg
    - https://i3.ytimg.com/vi/_l0Mfsu__gU/1.jpg
    - https://i3.ytimg.com/vi/_l0Mfsu__gU/2.jpg
    - https://i3.ytimg.com/vi/_l0Mfsu__gU/3.jpg
---
