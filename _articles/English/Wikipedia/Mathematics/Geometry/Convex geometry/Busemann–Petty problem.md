---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Busemann%E2%80%93Petty_problem'
offline_file: ""
offline_thumbnail: ""
uuid: bfb5de7b-37c9-4eb9-8d5d-1a3166b9307b
updated: 1484309189
title: Busemann–Petty problem
categories:
    - Convex geometry
---
In the mathematical field of convex geometry, the Busemann–Petty problem, introduced by Herbert Busemann and Clinton Myers Petty (1956, problem 1), asks whether it is true that a symmetric convex body with larger central hyperplane sections has larger volume. More precisely, if K, T are symmetric convex bodies in Rn such that
