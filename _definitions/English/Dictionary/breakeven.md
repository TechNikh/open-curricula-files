---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breakeven
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484317681
title: breakeven
categories:
    - Dictionary
---
break even
     v 1: make neither profit nor loss [ant: {profit}, {lose}]
     2: attain a level at which there is neither gain nor loss, as
        in business, gambling, or a competitive sport
