---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Connected_dominating_set
offline_file: ""
offline_thumbnail: ""
uuid: 85895826-87be-4c28-8f34-b75f25f95c8f
updated: 1484309389
title: Connected dominating set
tags:
    - Definitions
    - Complementarity
    - Algorithms
    - Applications
categories:
    - Computational problems in graph theory
---
