---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arming
offline_file: ""
offline_thumbnail: ""
uuid: a41042d1-d8eb-4bf4-af58-09309ca33c9e
updated: 1484310174
title: arming
categories:
    - Dictionary
---
arming
     n : the act of equiping with weapons in preparation for war
         [syn: {armament}, {equipping}] [ant: {disarming}, {disarming}]
