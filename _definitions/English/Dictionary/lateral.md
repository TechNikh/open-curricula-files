---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lateral
offline_file: ""
offline_thumbnail: ""
uuid: 2eee98d2-9824-4b26-b213-bb6fab8caffd
updated: 1484310220
title: lateral
categories:
    - Dictionary
---
lateral
     adj 1: situated at or extending to the side; "the lateral branches
            of a tree"; "shot out sidelong boughs"- Tennyson [syn:
             {sidelong}]
     2: lying away from the median and sagittal plane of a body;
        "lateral lemniscus"
     n : a pass to a receiver upfield from the passer [syn: {lateral
         pass}]
