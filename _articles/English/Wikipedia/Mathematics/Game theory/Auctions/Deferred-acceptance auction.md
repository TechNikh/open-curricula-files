---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deferred-acceptance_auction
offline_file: ""
offline_thumbnail: ""
uuid: 78792672-968b-4cd0-8064-6ec5f6c2aa8f
updated: 1484309013
title: Deferred-acceptance auction
categories:
    - Auctions
---
A deferred-acceptance auction (DAA) is an auction in which the allocation is chosen by repeatedly rejecting the least attractive bids. It is a truthful mechanisms with strategic properties that make it particularly suitable to complex auctions such as the radio spectrum reallocation auction.[1]
