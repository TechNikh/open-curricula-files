---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sky
offline_file: ""
offline_thumbnail: ""
uuid: caff7ed6-4783-4ba7-944b-0376466226c8
updated: 1484310212
title: sky
categories:
    - Dictionary
---
sky
     n : the atmosphere and outer space as viewed from the earth
     v : throw or toss with a light motion; "flip me the beachball";
         "toss me newspaper" [syn: {flip}, {toss}, {pitch}]
