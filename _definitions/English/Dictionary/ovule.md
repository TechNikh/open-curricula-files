---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ovule
offline_file: ""
offline_thumbnail: ""
uuid: 722ce21a-7a45-4b84-ada5-7da58e696f81
updated: 1484310162
title: ovule
categories:
    - Dictionary
---
ovule
     n 1: a small body that contains the female germ cell of a plant;
          develops into a seed after fertilization
     2: a small or immature ovum
