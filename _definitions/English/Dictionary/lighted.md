---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lighted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484342701
title: lighted
categories:
    - Dictionary
---
lighted
     adj 1: set afire or burning; "the lighted candles"; "a lighted
            cigarette"; "a lit firecracker" [syn: {lit}] [ant: {unlighted}]
     2: provided with artificial light; "illuminated advertising";
        "looked up at the lighted windows"; "a brightly lit room";
        "a well-lighted stairwell" [syn: {illuminated}, {lit}, {well-lighted}]
