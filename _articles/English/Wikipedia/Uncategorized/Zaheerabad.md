---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zaheerabad
offline_file: ""
offline_thumbnail: ""
uuid: b5704050-7106-4a73-9df7-3220f0f3786a
updated: 1484308471
title: Zaheerabad
tags:
    - Etymology
    - History
    - Demographics
    - Government and politics
    - Economy
categories:
    - Uncategorized
---
Zahirabad is a town and Revenue division in Sangareddy district of the Indian state of Telangana.[4] It is located in Zahirabad mandal of Sangareddy district.[3]:12
