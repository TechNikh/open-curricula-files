---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/applicant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484605561
title: applicant
categories:
    - Dictionary
---
applicant
     n : a person who requests or seeks something such as assistance
         or employment or admission [syn: {applier}]
