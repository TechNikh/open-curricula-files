---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fractal_dimension
offline_file: ""
offline_thumbnail: ""
uuid: 7aa981a1-16e3-4eac-9ff6-035637fcdc22
updated: 1484308709
title: Fractal dimension
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-32_segment_fractal.jpg
tags:
    - Introduction
    - History
    - Role of scaling
    - D is not a unique descriptor
    - Examples
    - Estimating from real-world data
    - Notes
categories:
    - Chaos theory
---
In mathematics, more specifically in fractal geometry, a fractal dimension is a ratio providing a statistical index of complexity comparing how detail in a pattern (strictly speaking, a fractal pattern) changes with the scale at which it is measured. It has also been characterized as a measure of the space-filling capacity of a pattern that tells how a fractal scales differently from the space it is embedded in; a fractal dimension does not have to be an integer.[1][2][3]
