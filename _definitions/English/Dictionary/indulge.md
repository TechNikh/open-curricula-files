---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indulge
offline_file: ""
offline_thumbnail: ""
uuid: 80650164-398b-4cba-a627-d39e94866df3
updated: 1484310152
title: indulge
categories:
    - Dictionary
---
indulge
     v 1: give free rein to; "The writer indulged in metaphorical
          language"
     2: yield (to); give satisfaction to [syn: {gratify}, {pander}]
     3: enjoy to excess [syn: {luxuriate}]
     4: treat with excessive indulgence; "grandparents often pamper
        the children"; "Let's not mollycoddle our students!" [syn:
         {pamper}, {featherbed}, {cosset}, {cocker}, {baby}, {coddle},
         {mollycoddle}, {spoil}]
