---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dyadic_transformation
offline_file: ""
offline_thumbnail: ""
uuid: 2e5aff4a-e345-4acf-8770-5b375b9b8500
updated: 1484308715
title: Dyadic transformation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dyadic_trans.gif
tags:
    - Relation to tent map and logistic map
    - Periodicity and non-periodicity
    - Solvability
    - >
        Rate of information loss and sensitive dependence on initial
        conditions
    - Notes
categories:
    - Chaotic maps
---
The dyadic transformation (also known as the dyadic map, bit shift map, 2x mod 1 map, Bernoulli map, doubling map or sawtooth map[1][2]) is the mapping (i.e., recurrence relation)
