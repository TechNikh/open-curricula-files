---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/methylene
offline_file: ""
offline_thumbnail: ""
uuid: 4f93d380-22e5-4c63-99f7-349eff1efcf6
updated: 1484310424
title: methylene
categories:
    - Dictionary
---
methylene
     n : the bivalent radical CH2 derived from methane [syn: {methylene
         group}, {methylene radical}]
