---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conducting
offline_file: ""
offline_thumbnail: ""
uuid: 7d08133a-2174-451f-8564-3dd086c6b764
updated: 1484310204
title: conducting
categories:
    - Dictionary
---
conducting
     n 1: the way of administering a business
     2: the direction of an orchestra or choir; "he does not use a
        baton for conducting"
