---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tax
offline_file: ""
offline_thumbnail: ""
uuid: 06ddbb7d-942f-4714-ba52-20ece3d5c6d1
updated: 1484310384
title: tax
categories:
    - Dictionary
---
tax
     n : charge against a citizen's person or property or activity
         for the support of government [syn: {taxation}, {revenue
         enhancement}]
     v 1: levy a tax on; "The State taxes alcohol heavily"; "Clothing
          is not taxed in our state"
     2: set or determine the amount of (a payment such as a fine)
        [syn: {assess}]
     3: use to the limit; "you are taxing my patience" [syn: {task}]
     4: make a charge against or accuse; "They taxed him failure to
        appear in court"
     [also: {taxes} (pl)]
