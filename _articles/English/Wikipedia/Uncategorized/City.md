---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cities
offline_file: ""
offline_thumbnail: ""
uuid: 73a45c57-4fc3-4d5e-8996-8f5136eefe6f
updated: 1484308451
title: City
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Piraeus_map_1908.jpg
tags:
    - Origins
    - geography
    - History
    - Ancient times
    - Middle Ages
    - Early modern
    - Industrial age
    - External effects
    - Distinction between cities and towns
    - Global cities
    - Inner city
    - 21st century
    - Networks of cities
categories:
    - Uncategorized
---
A city is a large and permanent human settlement.[1][2] Although there is no agreement on how a city is distinguished from a town in general English language meanings, many cities have a particular administrative, legal, or historical status based on local law.
