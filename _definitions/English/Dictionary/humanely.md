---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humanely
offline_file: ""
offline_thumbnail: ""
uuid: c1591ea1-fc52-4dfb-bc86-cfb1a6d62ae0
updated: 1484310561
title: humanely
categories:
    - Dictionary
---
humanely
     adv : in a humane manner; "let's treat the prisoners of war
           humanely" [ant: {inhumanely}]
