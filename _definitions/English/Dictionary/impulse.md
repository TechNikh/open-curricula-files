---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impulse
offline_file: ""
offline_thumbnail: ""
uuid: 316fb06d-c515-4a81-988d-321fbd590c35
updated: 1484310553
title: impulse
categories:
    - Dictionary
---
impulse
     n 1: an instinctive motive; "profound religious impulses" [syn: {urge}]
     2: a sudden desire; "he bought it on an impulse" [syn: {caprice},
         {whim}]
     3: the electrical discharge that travels along a nerve fiber;
        "they demonstrated the transmission of impulses from the
        cortex to the hypothalamus" [syn: {nerve impulse}]
     4: (electronics) a sharp transient wave in the normal
        electrical state (or a series of such transients); "the
        pulsations seemed to be coming from a star" [syn: {pulsation},
         {pulsing}, {pulse}]
     5: the act of applying force suddenly; "the impulse knocked him
        over" [syn: {impulsion}, ...
