---
version: 1
type: video
provider: YouTube
id: FP2arCfAfBY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Logarithms/Proof-%20log%20a%20%2B%20log%20b%20%3D%20log%20ab.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1ae86997-8fe1-4e54-87b6-7401ab2f50ba
updated: 1486069573
title: 'Proof- log a + log b = log ab'
categories:
    - Logarithms
thumbnail_urls:
    - https://i3.ytimg.com/vi/FP2arCfAfBY/default.jpg
    - https://i3.ytimg.com/vi/FP2arCfAfBY/1.jpg
    - https://i3.ytimg.com/vi/FP2arCfAfBY/2.jpg
    - https://i3.ytimg.com/vi/FP2arCfAfBY/3.jpg
---
Proof of the logarithm property: log a + log b = log ab

Watch the next lesson: https://www.khanacademy.org/math/algebra2/logarithms-tutorial/logarithm_properties/v/proof-a-log-b-log-b-a-log-a-log-b-log-a-b?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Missed the previous lesson? 
https://www.khanacademy.org/math/algebra2/logarithms-tutorial/logarithm_properties/v/change-of-base-formula?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Algebra II on Khan Academy: Your studies in algebra 1 have built a solid foundation from which you can explore linear equations, inequalities, and functions. In algebra 2 we build upon that foundation and not only extend our knowledge of algebra 1, but slowly become capable of tackling the BIG questions of the universe. We'll again touch on systems of equations, inequalities, and functions...but we'll also address exponential and logarithmic functions, logarithms, imaginary and complex numbers, conic sections, and matrices. Don't let these big words intimidate you. We're on this journey with you!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Algebra II channel:
https://www.youtube.com/channel/UCsCA3_VozRtgUT7wWC1uZDg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
