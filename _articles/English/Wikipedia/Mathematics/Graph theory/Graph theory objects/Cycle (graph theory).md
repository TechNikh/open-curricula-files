---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cycle_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 42c4c49b-84e2-4f74-8005-6b3e0d1cb767
updated: 1484309442
title: Cycle (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Graph_cycle.gif
tags:
    - Definitions
    - Chordless cycles
    - Cycle space
    - Cycle detection
    - Covering graphs by cycles
    - Graph classes defined by cycles
categories:
    - Graph theory objects
---
In graph theory, there are several different types of objects called cycles, principally a closed walk and a simple cycle; also, e.g., an element of the cycle space of the graph.
