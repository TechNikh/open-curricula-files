---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/governmental
offline_file: ""
offline_thumbnail: ""
uuid: e40c4182-4976-466e-8416-a75660c15d5c
updated: 1484310607
title: governmental
categories:
    - Dictionary
---
governmental
     adj 1: of or relating to the governing authorities; "the core of a
            governmental system"; "public confidence and
            governmental morale"
     2: dealing with the affairs or structure of government or
        politics or the state; "governmental policy"
