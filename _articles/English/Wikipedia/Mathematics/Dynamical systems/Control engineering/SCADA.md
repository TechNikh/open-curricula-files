---
version: 1
type: article
id: https://en.wikipedia.org/wiki/SCADA
offline_file: ""
offline_thumbnail: ""
uuid: 286b44fa-879b-458c-9b91-1082f8096d7c
updated: 1484308743
title: SCADA
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/500px-Functional_levels_of_a_Distributed_Control_System.svg_1.png
tags:
    - Common system components
    - Systems concepts
    - Human–machine interface
    - Hardware
    - Supervisory station
    - Operational philosophy
    - Communication infrastructure and methods
    - SCADA architectures
    - 'First generation: "monolithic"'
    - 'Second generation: "distributed"'
    - 'Third generation: "networked"'
    - 'Fourth generation: "Internet of things"'
    - Security issues
    - SCADA In the workplace
categories:
    - Control engineering
---
Supervisory control and data acquisition (SCADA) is a system for remote monitoring and control that operates with coded signals over communication channels (using typically one communication channel per remote station).
