---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Love_wave
offline_file: ""
offline_thumbnail: ""
uuid: 16868043-d5da-4dce-8951-29e454dbb109
updated: 1484308889
title: Love wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Love_wave.svg.png
tags:
    - Description
    - Basic theory
categories:
    - Waves
---
In elastodynamics, Love waves, named after Augustus Edward Hough Love, are horizontally polarized surface waves. The Love wave is a result of the interference of many shear waves (S–waves) guided by an elastic layer, which is welded to an elastic half space on one side while bordering a vacuum on the other side. In seismology, Love waves (also known as Q waves (Quer: German for lateral)) are surface seismic waves that cause horizontal shifting of the Earth during an earthquake. Augustus Edward Hough Love predicted the existence of Love waves mathematically in 1911. They form a distinct class, different from other types of seismic waves, such as P-waves and S-waves (both body waves), or ...
