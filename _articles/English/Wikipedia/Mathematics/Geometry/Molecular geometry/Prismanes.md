---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Prismanes
offline_file: ""
offline_thumbnail: ""
uuid: d9233eab-8b55-4b65-a25f-991eb9a0ec16
updated: 1484309280
title: Prismanes
categories:
    - Molecular geometry
---
The prismanes are a class of hydrocarbon compounds consisting of prism-like polyhedra of various numbers of sides on the polygonal base. Chemically, it is a series of fused cyclobutane rings (a ladderane, with all-cis/all-syn geometry) that wraps around to join its ends and form a band, with cycloalkane edges. Their chemical formula is (C2H2)n, where n is the number of cyclobutane sides (the size of the cycloalkane base), and that number also forms the basis for a system of nomenclature within this class. The first few chemicals in this class are:
