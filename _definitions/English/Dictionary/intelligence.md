---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intelligence
offline_file: ""
offline_thumbnail: ""
uuid: 4117ed53-ef4c-439f-ad60-5547169378ae
updated: 1484310144
title: intelligence
categories:
    - Dictionary
---
intelligence
     n 1: the ability to comprehend; to understand and profit from
          experience [ant: {stupidity}]
     2: a unit responsible for gathering and interpreting
        information about an enemy [syn: {intelligence service}, {intelligence
        agency}]
     3: secret information about an enemy (or potential enemy); "we
        sent out planes to gather intelligence on their radar
        coverage" [syn: {intelligence information}]
     4: new information about specific and timely events; "they
        awaited news of the outcome" [syn: {news}, {tidings}, {word}]
     5: the operation of gathering information about an enemy [syn:
        {intelligence activity}, ...
