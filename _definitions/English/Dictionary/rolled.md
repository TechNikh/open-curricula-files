---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rolled
offline_file: ""
offline_thumbnail: ""
uuid: 7d79e6b3-a0ca-4c5f-9cf8-f9ab472d2630
updated: 1484310416
title: rolled
categories:
    - Dictionary
---
rolled
     adj 1: especially of petals or leaves in bud; having margins rolled
            inward [syn: {involute}]
     2: uttered with a trill; "she used rolling r's as in Spanish"
        [syn: {rolling}, {trilled}]
     3: folded in on itself to form a roll; "the edges of the
        handkerchief were rolled and whipped"; "jeans with
        rolled-up legs"; "swatted the fly with a rolled newspaper"
        [syn: {rolled-up(a)}]
     4: rolled up and secured; "furled sails bound securely to the
        spar"; "a furled flag"; "his rolled umbrella hanging on
        his arm" [syn: {furled}]
