---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/classical
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484383921
title: classical
categories:
    - Dictionary
---
classical
     adj 1: of or characteristic of a form or system felt to be of first
            significance before modern times [ant: {nonclassical}]
     2: of recognized authority or excellence; "the definitive work
        on Greece"; "classical methods of navigation" [syn: {authoritative},
         {definitive}]
