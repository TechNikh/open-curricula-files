---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instead
offline_file: ""
offline_thumbnail: ""
uuid: 4c7c259c-960a-4478-bcbf-69c7a47b1aa5
updated: 1484310297
title: instead
categories:
    - Dictionary
---
instead
     adv 1: in place of, or as an alternative to; "Felix became a
            herpetologist instead"; "alternatively we could buy a
            used car" [syn: {alternatively}, {as an alternative},
            {or else}]
     2: on the contrary; "rather than disappoint the children, he
        did two quick tricks before he left"; "he didn't call;
        rather (or instead), he wrote her a letter"; "used English
        terms instead of Latin ones" [syn: {rather}]
