---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oreste_Piro
offline_file: ""
offline_thumbnail: ""
uuid: d36fcb91-1aa5-4772-961f-a110bb039180
updated: 1484308713
title: Oreste Piro
categories:
    - Chaos theory
---
Piro is known for work including his studies of forced relaxation oscillators such as the exactly-solvable PiroGon oscillator,[1] the dynamics of passive scalars in fluid flows,[2][3][4] bailout embeddings,[5] the influence of fluid mechanics on the development of vertebrate left-right asymmetry,[6] and the self-tuned critical network model[7]
