---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pyrex
offline_file: ""
offline_thumbnail: ""
uuid: f2c2ed31-2b82-4209-8189-7cbacb1e6ff8
updated: 1484310226
title: pyrex
categories:
    - Dictionary
---
Pyrex
     n : a borosilicate glass with a low coefficient of expansion;
         used for heat-resistant glassware in cooking and
         chemistry
