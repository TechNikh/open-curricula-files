---
version: 1
type: article
id: https://en.wikipedia.org/wiki/El_Farol_Bar_problem
offline_file: ""
offline_thumbnail: ""
uuid: 6ab1f946-06cd-4e96-b1a4-22d2137f3e2c
updated: 1484309052
title: El Farol Bar problem
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-El_Farol_Restaurant_and_Cantina%252C_Santa_Fe_NM.jpg'
tags:
    - Minority game
    - Kolkata Paise Restaurant Problem
categories:
    - Non-cooperative games
---
The problem (without the name of El Farol Bar) was formulated and solved dynamically six years earlier by B. A. Huberman and T. Hogg in "The Ecology of Computation", Studies in Computer Science and Artificial Intelligence, North Holland publisher, page 99. 1988.
