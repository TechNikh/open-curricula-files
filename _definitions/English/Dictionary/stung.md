---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stung
offline_file: ""
offline_thumbnail: ""
uuid: 6fb98b38-908b-46b8-ae46-20f974cd1908
updated: 1484310384
title: stung
categories:
    - Dictionary
---
sting
     n 1: a kind of pain; something as sudden and painful as being
          stung; "the sting of death"; "he felt the stinging of
          nettles" [syn: {stinging}]
     2: a mental pain or distress; "a pang of conscience" [syn: {pang}]
     3: a painful wound caused by the thrust of an insect's stinger
        into skin [syn: {bite}, {insect bite}]
     4: a swindle in which you cheat at gambling or persuade a
        person to buy worthless property [syn: {bunco}, {bunco
        game}, {bunko}, {bunko game}, {con}, {confidence trick}, {confidence
        game}, {con game}, {gyp}, {hustle}, {flimflam}]
     v 1: cause a sharp or stinging pain or discomfort; "The sun
          ...
