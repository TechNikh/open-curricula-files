---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Horizontal_translation
offline_file: ""
offline_thumbnail: ""
uuid: f4c153c0-66d5-446c-b230-345c444165e0
updated: 1484309336
title: Horizontal translation
categories:
    - Transformation (function)
---
In function graphing, a horizontal translation is a transformation which results in a graph that is equivalent to shifting the base graph left or right in the direction of the x-axis. A graph is translated k units horizontally by moving each point on the graph k units horizontally.
