---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commutative
offline_file: ""
offline_thumbnail: ""
uuid: 2264b722-db3f-44e9-aea8-e038566e16a2
updated: 1484310142
title: commutative
categories:
    - Dictionary
---
commutative
     adj : of a binary operation; independent of order; as in e.g. "a x
           b = b x a"
