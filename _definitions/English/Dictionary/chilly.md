---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chilly
offline_file: ""
offline_thumbnail: ""
uuid: 1486fa46-9c1f-4549-b9f1-9af99be95e41
updated: 1484310221
title: chilly
categories:
    - Dictionary
---
chilly
     adj 1: uncomfortably cool; "a chill wind"; "chilly weather" [syn: {chill}]
     2: not characterized by emotion; "a female form in marble--a
        chilly but ideal medium for depicting abstract
        virtues"-C.W.Cunningham
     3: lacking warmth of feeling; "a chilly greeting"; "an
        unfriendly manner" [syn: {unfriendly}]
     n : very hot and finely tapering pepper of special pungency
         [syn: {chili}, {chili pepper}, {chilli}, {chile}]
     [also: {chilliest}, {chillier}]
