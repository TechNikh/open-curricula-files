---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Binary_Goppa_code
offline_file: ""
offline_thumbnail: ""
uuid: 67856f6f-ab43-4f3b-b22d-01661adf0146
updated: 1484308539
title: Binary Goppa code
tags:
    - Construction and properties
    - Decoding
    - Properties and usage
categories:
    - Coding theory
---
In mathematics and computer science, the binary Goppa code is an error-correcting code that belongs to the class of general Goppa codes originally described by Valerii Denisovich Goppa, but the binary structure gives it several mathematical advantages over non-binary variants, also providing a better fit for common usage in computers and telecommunication. Binary Goppa codes have interesting properties suitable for cryptography in McEliece-like cryptosystems and similar setups.
