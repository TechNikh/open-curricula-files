---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Stern%E2%80%93Brocot_tree'
offline_file: ""
offline_thumbnail: ""
uuid: a4409bac-c33b-4254-b97a-c54b4893a560
updated: 1484308661
title: Stern–Brocot tree
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-SternBrocotTree.svg.png
tags:
    - A tree of continued fractions
    - Mediants and binary search
    - Relation to Farey sequences
    - Additional properties
    - Notes
categories:
    - Number theory
---
In number theory, the Stern–Brocot tree is an infinite complete binary tree in which the vertices correspond one-for-one to the positive rational numbers, whose values are ordered from the left to the right as in a search tree.
