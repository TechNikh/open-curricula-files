---
version: 1
type: video
provider: YouTube
id: PNHSIEO-KOQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6bdc85f2-2ca6-48cb-9fea-37282b640340
updated: 1486069656
title: Momentum and Angular Momentum of the Universe
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/PNHSIEO-KOQ/default.jpg
    - https://i3.ytimg.com/vi/PNHSIEO-KOQ/1.jpg
    - https://i3.ytimg.com/vi/PNHSIEO-KOQ/2.jpg
    - https://i3.ytimg.com/vi/PNHSIEO-KOQ/3.jpg
---
