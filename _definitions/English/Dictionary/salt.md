---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/salt
offline_file: ""
offline_thumbnail: ""
uuid: 994fd91e-ff56-4bfc-8fce-0dafb4b213a4
updated: 1484310323
title: salt
categories:
    - Dictionary
---
salt
     adj 1: containing or filled with salt; "salt water" [ant: {fresh}]
     2: of speech that is painful or bitter; "salt scorn"-
        Shakespeare; "a salt apology"
     3: one of the four basic taste sensations; like the taste of
        sea water [syn: {salty}]
     n 1: a compound formed by replacing hydrogen in an acid by a
          metal (or a radical that acts like a metal)
     2: white crystalline form of especially sodium chloride used to
        season and preserve food [syn: {table salt}, {common salt}]
     3: negotiations between the United States and the Union of
        Soviet Socialist Republics opened in 1969 in Helsinki
        designed to limit both countries' ...
