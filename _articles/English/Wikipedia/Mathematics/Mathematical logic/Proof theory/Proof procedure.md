---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proof_procedure
offline_file: ""
offline_thumbnail: ""
uuid: 7023cb3f-2125-468b-a067-f8aa44107869
updated: 1484309495
title: Proof procedure
tags:
    - Types of proof calculi used
    - Completeness
categories:
    - Proof theory
---
In logic, and in particular proof theory, a proof procedure for a given logic is a systematic method for producing proofs in some proof calculus of (provable) statements.
