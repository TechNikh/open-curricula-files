---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Participation_criterion
offline_file: ""
offline_thumbnail: ""
uuid: 4c1c033f-0b30-4380-ae62-d9736080c7db
updated: 1484309050
title: Participation criterion
tags:
    - Quorum requirements
    - Incompatibility with the Condorcet criterion
    - Examples
    - Copeland
    - Voters not participating
    - Voters participating
    - Conclusion
    - Instant-runoff voting
    - Voters not participating
    - Voters participating
    - Conclusion
    - Kemeny–Young method
    - Voters not participating
    - Voters participating
    - Conclusion
    - Majority Judgment
    - Voters not participating
    - Voters participating
    - Conclusion
    - Minimax
    - Voters not participating
    - Voters participating
    - Conclusion
    - Ranked pairs
    - Voters not participating
    - Voters participating
    - Conclusion
    - Schulze method
    - Voters not participating
    - Voters participating
    - Conclusion
categories:
    - Mechanism design
---
The participation criterion is a voting system criterion. Voting systems that fail the participation criterion are said to exhibit the no show paradox[1] and allow a particularly unusual strategy of tactical voting: abstaining from an election can help a voter's preferred choice win. The criterion has been defined[2] as follows:
