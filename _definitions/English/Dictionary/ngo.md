---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ngo
offline_file: ""
offline_thumbnail: ""
uuid: b6b9a988-7d04-4c98-8168-74d258ccad3f
updated: 1484310577
title: ngo
categories:
    - Dictionary
---
NGO
     n : an organization that is not part of the local or state or
         federal government [syn: {nongovernmental organization}]
