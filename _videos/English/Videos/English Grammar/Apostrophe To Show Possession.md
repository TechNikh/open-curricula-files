---
version: 1
type: video
provider: YouTube
id: mQa0AEacIS8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Apostrophe%20To%20Show%20Possession.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c157fb22-0542-471d-9cff-87a093615bc2
updated: 1486069631
title: Apostrophe To Show Possession
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/mQa0AEacIS8/default.jpg
    - https://i3.ytimg.com/vi/mQa0AEacIS8/1.jpg
    - https://i3.ytimg.com/vi/mQa0AEacIS8/2.jpg
    - https://i3.ytimg.com/vi/mQa0AEacIS8/3.jpg
---
