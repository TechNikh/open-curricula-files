---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nourished
offline_file: ""
offline_thumbnail: ""
uuid: 30c63d83-c618-4e3b-9d89-a514b28cd0d2
updated: 1484310293
title: nourished
categories:
    - Dictionary
---
nourished
     adj 1: being provided with adequate nourishment [ant: {malnourished}]
     2: encouraged or promoted in growth or development; "dreams of
        liberty nourished by the blood of patriots cannot easily
        be given up" [syn: {fostered}]
