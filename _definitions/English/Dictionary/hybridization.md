---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hybridization
offline_file: ""
offline_thumbnail: ""
uuid: e1ea4a3e-edb4-4b20-ad1e-1da73d4b37e8
updated: 1484310303
title: hybridization
categories:
    - Dictionary
---
hybridization
     n : (genetics) the act of mixing different species or varieties
         of animals or plants and thus to produce hybrids [syn: {hybridisation},
          {crossbreeding}, {crossing}, {cross}, {interbreeding}, {hybridizing}]
