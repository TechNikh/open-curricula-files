---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/iii
offline_file: ""
offline_thumbnail: ""
uuid: 549a8164-2e16-4ce9-9692-1c89ca234cb5
updated: 1484310315
title: iii
categories:
    - Dictionary
---
iii
     adj : being one more than two [syn: {three}, {3}]
     n : the cardinal number that is the sum of one and one and one
         [syn: {three}, {3}, {trio}, {threesome}, {tierce}, {leash},
          {troika}, {triad}, {trine}, {trinity}, {ternary}, {ternion},
          {triplet}, {tercet}, {terzetto}, {trey}, {deuce-ace}]
