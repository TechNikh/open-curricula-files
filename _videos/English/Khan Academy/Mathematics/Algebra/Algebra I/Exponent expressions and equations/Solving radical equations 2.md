---
version: 1
type: video
provider: YouTube
id: b6WtwQddAcY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Exponent%20expressions%20and%20equations/Solving%20radical%20equations%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b006dcab-271f-4c22-bcea-f50bc7b65411
updated: 1486069559
title: Solving radical equations 2
categories:
    - Exponent expressions and equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/b6WtwQddAcY/default.jpg
    - https://i3.ytimg.com/vi/b6WtwQddAcY/1.jpg
    - https://i3.ytimg.com/vi/b6WtwQddAcY/2.jpg
    - https://i3.ytimg.com/vi/b6WtwQddAcY/3.jpg
---
