---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamiltonian_system
offline_file: ""
offline_thumbnail: ""
uuid: c47f3198-e474-49b3-a61a-a014c97377b4
updated: 1484308797
title: Hamiltonian system
tags:
    - Overview
    - Time independent Hamiltonian system
    - Example
    - Symplectic structure
    - Examples
categories:
    - Hamiltonian mechanics
---
A Hamiltonian system is a dynamical system governed by Hamilton's equations. In physics, this dynamical system describes the evolution of a physical system such as a planetary system or an electron in an electromagnetic field. These systems can be studied in both Hamiltonian mechanics and dynamical systems theory.
