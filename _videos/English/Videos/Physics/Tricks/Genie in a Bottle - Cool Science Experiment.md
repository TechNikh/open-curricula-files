---
version: 1
type: video
provider: YouTube
id: 5q5bzHckSIM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b021681e-2671-46ba-921c-63109dc33974
updated: 1486069658
title: 'Genie in a Bottle - Cool Science Experiment'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/5q5bzHckSIM/default.jpg
    - https://i3.ytimg.com/vi/5q5bzHckSIM/1.jpg
    - https://i3.ytimg.com/vi/5q5bzHckSIM/2.jpg
    - https://i3.ytimg.com/vi/5q5bzHckSIM/3.jpg
---
