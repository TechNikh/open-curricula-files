---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/populated
offline_file: ""
offline_thumbnail: ""
uuid: ac3f6bbd-cce3-429f-b08d-7f3261bb17bb
updated: 1484310473
title: populated
categories:
    - Dictionary
---
populated
     adj : furnished with inhabitants; "the area is well populated";
           "forests populated with all kinds of wild life"
