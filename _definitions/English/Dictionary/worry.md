---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484485981
title: worry
categories:
    - Dictionary
---
worry
     n 1: something or someone that causes anxiety; a source of
          unhappiness; "New York traffic is a constant concern";
          "it's a major worry" [syn: {concern}, {headache}, {vexation}]
     2: a strong feeling of anxiety; "his worry over the prospect of
        being fired"; "it is not work but worry that kills"; "he
        wanted to die and end his troubles" [syn: {trouble}]
     v 1: be worried, concerned, anxious, troubled, or uneasy; "I
          worry about my job"
     2: be concerned with; "I worry about my grades" [syn: {care}]
     3: disturb the peace of mind of; afflict with mental agitation
        or distress; "I cannot sleep--my daughter's health is
     ...
