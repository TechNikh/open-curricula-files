---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pit
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484476801
title: pit
categories:
    - Dictionary
---
pit
     n 1: a sizeable hole (usually in the ground); "they dug a pit to
          bury the body" [syn: {cavity}]
     2: a concavity in a surface (especially an anatomical
        depression) [syn: {fossa}]
     3: the hard inner (usually woody) layer of the pericarp of some
        fruits (as peaches or plums or cherries or olives) that
        contains the seed; "you should remove the stones from
        prunes before cooking" [syn: {stone}, {endocarp}]
     4: a trap in the form of a concealed hole [syn: {pitfall}]
     5: a surface excavation for extracting stone or slate; "a
        British term for `quarry' is `stone pit'" [syn: {quarry},
        {stone pit}]
     6: lowered area in ...
