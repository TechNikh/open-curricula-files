---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/whereby
offline_file: ""
offline_thumbnail: ""
uuid: 4ebdfa19-64ff-4e53-b5fd-27a5e5e68b87
updated: 1484310597
title: whereby
categories:
    - Dictionary
---
whereby
     adv 1: as a result of which
     2: by or through which; "the means whereby we achieved our
        goal"
