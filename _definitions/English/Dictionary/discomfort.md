---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discomfort
offline_file: ""
offline_thumbnail: ""
uuid: 03b59112-e2ee-4df0-89ca-205352da9f99
updated: 1484310189
title: discomfort
categories:
    - Dictionary
---
discomfort
     n 1: the state of being tense and feeling pain [syn: {uncomfortableness}]
          [ant: {comfort}]
     2: an uncomfortable feeling in some part of the body [syn: {soreness},
         {irritation}]
