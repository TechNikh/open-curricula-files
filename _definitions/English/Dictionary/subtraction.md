---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subtraction
offline_file: ""
offline_thumbnail: ""
uuid: 0bbc7533-2cef-462a-a110-964ec63ea110
updated: 1484310142
title: subtraction
categories:
    - Dictionary
---
subtraction
     n 1: an arithmetic operation in which the difference between two
          numbers is calculated; "the subtraction of three from
          four leaves one"; "four minus three equals one" [syn: {minus}]
     2: the act of subtracting (removing a part from the whole); "he
        complained about the subtraction of money from their
        paychecks" [syn: {deduction}] [ant: {addition}]
