---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Height_of_a_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: 5e9e2936-34f5-45b7-94f0-0dd35b32ae13
updated: 1484308649
title: Height of a polynomial
tags:
    - Definition
    - Relation to Mahler measure
categories:
    - Number theory
---
