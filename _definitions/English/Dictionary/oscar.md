---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oscar
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484537521
title: oscar
categories:
    - Dictionary
---
Oscar
     n : an annual award by the Academy of Motion Picture Arts and
         Sciences for achievements in motion picture production
         and performance [syn: {Academy Award}]
