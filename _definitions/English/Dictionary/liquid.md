---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liquid
offline_file: ""
offline_thumbnail: ""
uuid: 06dab36e-c0d7-423d-8ac2-8f56babe801a
updated: 1484310344
title: liquid
categories:
    - Dictionary
---
liquid
     adj 1: existing as or having characteristics of a liquid;
            especially tending to flow; "water and milk and blood
            are liquid substances" [ant: {gaseous}, {solid}]
     2: filled or brimming with tears; "swimming eyes"; "watery
        eyes"; "sorrow made the eyes of many grow liquid" [syn: {swimming},
         {watery}]
     3: clear and bright; "the liquid air of a spring morning";
        "eyes shining with a liquid luster"; "limpid blue eyes"
        [syn: {limpid}]
     4: changed from a solid to a liquid state; "rivers filled to
        overflowing by melted snow" [syn: {melted}, {liquified}]
        [ant: {unmelted}]
     5: smooth and flowing in ...
