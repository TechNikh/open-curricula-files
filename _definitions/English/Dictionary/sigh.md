---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sigh
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484477401
title: sigh
categories:
    - Dictionary
---
sigh
     n 1: an utterance made by exhaling audibly [syn: {suspiration}]
     2: a sound like a person sighing; "she heard the sigh of the
        wind in the trees"
     v 1: heave or utter a sigh; breathe deeply and heavily; "She
          sighed sadly" [syn: {suspire}]
     2: utter with a sigh
