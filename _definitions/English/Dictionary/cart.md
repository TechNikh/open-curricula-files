---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cart
offline_file: ""
offline_thumbnail: ""
uuid: 506dcf1e-d2a4-4ceb-9d1b-04b441fc06aa
updated: 1484310455
title: cart
categories:
    - Dictionary
---
cart
     n 1: a heavy open wagon usually having two wheels and drawn by an
          animal
     2: wheeled vehicle that can be pushed by a person; may have one
        or two or four wheels; "he used a handcart to carry the
        rocks away"; "their pushcart was piled high with
        groceries" [syn: {handcart}, {pushcart}, {go-cart}]
     v 1: draw slowly or heavily; "haul stones"; "haul nets" [syn: {haul},
           {hale}, {drag}]
     2: transport something in a cart
