---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Richard_M._Pollack
offline_file: ""
offline_thumbnail: ""
uuid: 26e72760-2939-4934-af35-3d35e025697e
updated: 1484309096
title: Richard M. Pollack
categories:
    - Geometers
---
Richard M. Pollack is a geometer who has spent most of his career at the Courant Institute of New York University, where he is now Professor Emeritus.[2] In 1986 he and Jacob E. Goodman were the founding co-editors-in-chief of the journal Discrete and Computational Geometry (Springer-Verlag).[3]
