---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regulate
offline_file: ""
offline_thumbnail: ""
uuid: cde6a88c-e4d0-444f-8926-05c00fbae6bb
updated: 1484310527
title: regulate
categories:
    - Dictionary
---
regulate
     v 1: fix or adjust the time, amount, degree, or rate of;
          "regulate the temperature"; "modulate the pitch" [syn: {modulate}]
     2: bring into conformity with rules or principles or usage;
        impose regulations; "We cannot regulate the way people
        dress"; "This town likes to regulate" [syn: {regularize},
        {regularise}, {order}, {govern}] [ant: {deregulate}]
     3: shape or influence; give direction to; "experience often
        determines ability"; "mold public opinion" [syn: {determine},
         {shape}, {mold}, {influence}]
     4: check the emission of (sound) [syn: {baffle}]
