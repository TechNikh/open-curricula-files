---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bernstein%E2%80%93Kushnirenko_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 3bd21395-3411-42a9-a4de-8ffc24293df2
updated: 1484309107
title: Bernstein–Kushnirenko theorem
categories:
    - Theorems in geometry
---
Bernstein–Kushnirenko theorem (also known as BKK theorem or Bernstein–Khovanskii–Kushnirenko theorem [1]), proven by David Bernstein [2] and Anatoli Kushnirenko (ru) [3] in 1975, is a theorem in algebra. It claims that the number of non-zero complex solutions of a system of Laurent polynomial equations f1 = 0, ..., fn = 0 is equal to the mixed volume of the Newton polytopes of f1, ..., fn, assuming that all non-zero coefficients of fn are generic. More precise statement is as follows:
