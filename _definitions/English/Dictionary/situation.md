---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/situation
offline_file: ""
offline_thumbnail: ""
uuid: e20d898f-755a-47a7-87f2-c5c02d9dd1f3
updated: 1484310299
title: situation
categories:
    - Dictionary
---
situation
     n 1: the general state of things; the combination of
          circumstances at a given time; "the present
          international situation is dangerous"; "wondered how
          such a state of affairs had come about"; "eternal truths
          will be neither true nor eternal unless they have fresh
          meaning for every new social situation"- Franklin
          D.Roosevelt [syn: {state of affairs}]
     2: a condition or position in which you find yourself; "the
        unpleasant situation (or position) of having to choose
        between two evils"; "found herself in a very fortunate
        situation" [syn: {position}]
     3: a complex or critical or unusual ...
