---
version: 1
type: video
provider: YouTube
id: N0bqgkwxAMM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ca4149e9-4b71-4f4d-b1d9-936ca59bbac7
updated: 1486069616
title: Slice a rectangular pyramid
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/N0bqgkwxAMM/default.jpg
    - https://i3.ytimg.com/vi/N0bqgkwxAMM/1.jpg
    - https://i3.ytimg.com/vi/N0bqgkwxAMM/2.jpg
    - https://i3.ytimg.com/vi/N0bqgkwxAMM/3.jpg
---
