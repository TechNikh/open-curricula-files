---
version: 1
type: article
id: https://en.wikipedia.org/wiki/General_frame
offline_file: ""
offline_thumbnail: ""
uuid: 3d129096-440f-41f1-a901-66bfc01e99eb
updated: 1484309236
title: General frame
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Rieger-Nishimura_ladder.svg.png
tags:
    - Definition
    - Types of frames
    - Operations and morphisms on frames
    - Completeness
    - Jónsson–Tarski duality
    - Intuitionistic frames
categories:
    - Duality theories
---
In logic, general frames (or simply frames) are Kripke frames with an additional structure, which are used to model modal and intermediate logics. The general frame semantics combines the main virtues of Kripke semantics and algebraic semantics: it shares the transparent geometrical insight of the former, and robust completeness of the latter.
