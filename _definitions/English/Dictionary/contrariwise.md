---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contrariwise
offline_file: ""
offline_thumbnail: ""
uuid: 9c951929-c3eb-47a6-a14d-408172e8f6d4
updated: 1484310597
title: contrariwise
categories:
    - Dictionary
---
contrariwise
     adv 1: in a contrary disobedient manner [syn: {perversely}, {contrarily}]
     2: with the order reversed; "she hates him and vice versa"
        [syn: {vice versa}, {the other way around}]
     3: contrary to expectations; "he didn't stay home; on the
        contrary, he went out with his friends" [syn: {contrarily},
         {to the contrary}, {on the contrary}]
