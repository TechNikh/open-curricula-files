---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vegetative
offline_file: ""
offline_thumbnail: ""
uuid: 4da4730f-a0f1-4ee1-aaa5-ced5820e50fd
updated: 1484310160
title: vegetative
categories:
    - Dictionary
---
vegetative
     adj 1: of or relating to an activity that is passive and
            monotonous; "a dull vegetative lifestyle" [syn: {vegetive}]
     2: used of involuntary bodily functions; "vegetative functions
        such as digestion or growth or circulation"
     3: (of reproduction) characterized by asexual processes [syn: {vegetal}]
     4: composed of vegetation or plants; "regions rich in vegetal
        products"; "vegetational cover"; "the decaying vegetative
        layer covering a forest floor" [syn: {vegetal}, {vegetational}]
