---
version: 1
type: video
provider: YouTube
id: fXBJJjOL4hM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dceccaad-ce95-4264-818c-aa3cfc502bb0
updated: 1486069616
title: More Simplifying Radical Expressions
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/fXBJJjOL4hM/default.jpg
    - https://i3.ytimg.com/vi/fXBJJjOL4hM/1.jpg
    - https://i3.ytimg.com/vi/fXBJJjOL4hM/2.jpg
    - https://i3.ytimg.com/vi/fXBJJjOL4hM/3.jpg
---
