---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imagination
offline_file: ""
offline_thumbnail: ""
uuid: 64dea060-c79f-4d92-b67a-614aaf27fd89
updated: 1484310521
title: imagination
categories:
    - Dictionary
---
imagination
     n 1: the formation of a mental image of something that is not
          perceived as real and is not present to the senses;
          "popular imagination created a world of demons";
          "imagination reveals what the world could be" [syn: {imaginativeness},
           {vision}]
     2: the ability to form mental images of things or events; "he
        could still hear her in his imagination" [syn: {imaging},
        {imagery}, {mental imagery}]
     3: the ability to deal resourcefully with unusual problems; "a
        man of resource" [syn: {resource}, {resourcefulness}]
