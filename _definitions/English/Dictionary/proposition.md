---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proposition
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484514901
title: proposition
categories:
    - Dictionary
---
proposition
     n 1: (logic) a statement that affirms or denies something and is
          either true or false
     2: a proposal offered for acceptance or rejection; "it was a
        suggestion we couldn't refuse" [syn: {suggestion}, {proffer}]
     3: an offer for a private bargain (especially a request for
        sexual favors)
     4: the act of making a proposal; "they listened to her
        proposal" [syn: {proposal}]
     5: a task to be dealt with; "securing adequate funding is a
        time-consuming proposition"
     v : suggest sex to; "She was propositioned by a stranger at the
         party"
