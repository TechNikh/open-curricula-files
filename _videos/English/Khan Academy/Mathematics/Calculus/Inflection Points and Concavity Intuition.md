---
version: 1
type: video
provider: YouTube
id: dIE22eL6q90
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Inflection%20Points%20and%20Concavity%20Intuition.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6634d94d-e6aa-4307-b4fa-3728a168acdb
updated: 1486069693
title: Inflection Points and Concavity Intuition
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/dIE22eL6q90/default.jpg
    - https://i3.ytimg.com/vi/dIE22eL6q90/1.jpg
    - https://i3.ytimg.com/vi/dIE22eL6q90/2.jpg
    - https://i3.ytimg.com/vi/dIE22eL6q90/3.jpg
---
