---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Owens
offline_file: ""
offline_thumbnail: ""
uuid: fdb08efa-99f6-4f99-a1e9-e15d02851209
updated: 1484308473
title: Owens
tags:
    - people
    - Places
    - schools
    - Companies
categories:
    - Uncategorized
---
Owens is a traditional Welsh surname found in the UK. The name's original roots come from Wales. Owens, son of Owen, is an old personal name from Latin Eugenius or Audoenus. Historically in Welsh Owen is spelt Owain. It can also be found in Ireland (especially in Ulster) as an anglicisation of the surname McKeown.
