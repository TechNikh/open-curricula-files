---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Orientation_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: e09e8ae6-f5c0-44af-ac80-0ea7fbcf5ceb
updated: 1484309299
title: Orientation (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Change_of_axes.svg.png
tags:
    - Mathematical representations
    - Three dimensions
    - Two dimensions
    - Rigid body in three dimensions
    - Euler angles
    - Tait–Bryan angles
    - Orientation vector
    - Orientation matrix
    - Orientation quaternion
    - Plane in three dimensions
    - Miller indices
    - Strike and dip
    - Usage examples
    - Rigid body
categories:
    - Orientation (geometry)
---
In geometry the orientation, angular position, or attitude of an object such as a line, plane or rigid body is part of the description of how it is placed in the space it is in.[1] Namely, it is the imaginary rotation that is needed to move the object from a reference placement to its current placement. A rotation may not be enough to reach the current placement. It may be necessary to add an imaginary translation, called the object's location (or position, or linear position). The location and orientation together fully describe how the object is placed in space. The above-mentioned imaginary rotation and translation may be thought to occur in any order, as the orientation of an object ...
