---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/closely
offline_file: ""
offline_thumbnail: ""
uuid: 63bb11ef-a370-4d27-88ba-37944cfafcaf
updated: 1484310346
title: closely
categories:
    - Dictionary
---
closely
     adv 1: in a close relation or position in time or space; "the
            onsets were closely timed"; "houses set closely
            together"; "was closely involved in monitoring daily
            progress"
     2: in a close manner; "the two phenomena are intimately
        connected"; "the person most nearly concerned" [syn: {intimately},
         {nearly}]
     3: in an attentive manner; "he remained close on his guard"
        [syn: {close}, {tight}]
