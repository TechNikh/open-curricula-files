---
version: 1
type: article
id: https://en.wikipedia.org/wiki/LP-type_problem
offline_file: ""
offline_thumbnail: ""
uuid: 09ae4356-b732-4517-9eaa-730f03cdc53e
updated: 1484309175
title: LP-type problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Smallest_circle_problem.svg.png
tags:
    - Definition
    - Examples and applications
    - Algorithms
    - Seidel
    - Clarkson
    - Matoušek, Sharir, and Welzl
    - Variations
    - Optimization with outliers
    - Implicit problems
    - History and related problems
    - Notes
categories:
    - Computational geometry
---
In the study of algorithms, an LP-type problem (also called a generalized linear program) is an optimization problem that shares certain properties with low-dimensional linear programs and that may be solved by similar algorithms. LP-type problems include many important optimization problems that are not themselves linear programs, such as the problem of finding the smallest circle containing a given set of planar points. They may be solved by a combination of randomized algorithms in an amount of time that is linear in the number of elements defining the problem, and subexponential in the dimension of the problem.
