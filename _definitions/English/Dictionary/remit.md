---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remit
offline_file: ""
offline_thumbnail: ""
uuid: e1c95171-3b05-4272-9627-750ac817ee89
updated: 1484310515
title: remit
categories:
    - Dictionary
---
remit
     n : (law) the act of remitting (especially the referral of a law
         case to another court) [syn: {remission}, {remitment}]
     v 1: send (money) in payment; "remit $25"
     2: hold back to a later time; "let's postpone the exam" [syn: {postpone},
         {prorogue}, {hold over}, {put over}, {table}, {shelve}, {set
        back}, {defer}, {put off}]
     3: release from (claims, debts, or taxes); "The texes were
        remitted"
     4: refer (a matter or legal case) to another committe or
        authority or court for decision [syn: {remand}, {send back}]
     5: forgive; "God will remit their sins"
     6: make slack as by lessening tension or firmness [syn: ...
