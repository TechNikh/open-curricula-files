---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/established
offline_file: ""
offline_thumbnail: ""
uuid: 9ee452e5-51fd-4775-ad7a-79b55b623517
updated: 1484310244
title: established
categories:
    - Dictionary
---
established
     adj 1: brought about or set up or accepted; especially long
            established; "the established social order"; "distrust
            the constituted authority"; "a team established as a
            member of a major league"; "enjoyed his prestige as an
            established writer"; "an established precedent"; "the
            established Church" [syn: {constituted}] [ant: {unestablished}]
     2: securely established; "an established reputation"; "holds a
        firm position as the country's leading poet" [syn: {firm}]
     3: settled securely and unconditionally; "that smoking causes
        health problems is an accomplished fact" [syn: {accomplished},
         ...
