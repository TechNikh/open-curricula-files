---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Turing_degree
offline_file: ""
offline_thumbnail: ""
uuid: 5174ff87-fc67-4405-84f7-0975d8d74c44
updated: 1484309490
title: Turing degree
tags:
    - Overview
    - Turing equivalence
    - Basic properties of the Turing degrees
    - Structure of the Turing degrees
    - Order properties
    - Properties involving the jump
    - Logical properties
    - Structure of the r.e. Turing degrees
    - "Post's problem and the priority method"
categories:
    - Computability theory
---
In computer science and mathematical logic the Turing degree (named after Alan Turing) or degree of unsolvability of a set of natural numbers measures the level of algorithmic unsolvability of the set.
