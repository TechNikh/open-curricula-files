---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Peano%E2%80%93Russell_notation'
offline_file: ""
offline_thumbnail: ""
uuid: d77ba496-3739-4124-988b-91b52e3e6d06
updated: 1484309497
title: Peano–Russell notation
tags:
    - Variables
    - Fundamental functions of propositions
    - Contradictory function
    - Logical sum
    - Logical product
    - Implicative function
    - More complex functions of propositions
    - Notes
categories:
    - Proof theory
---
In mathematical logic, Peano–Russell notation was Bertrand Russell's application of Giuseppe Peano's logical notation to the logical notions of Frege and was used in the writing of Principia Mathematica in collaboration with Alfred North Whitehead:[1]
