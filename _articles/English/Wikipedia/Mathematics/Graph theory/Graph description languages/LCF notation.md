---
version: 1
type: article
id: https://en.wikipedia.org/wiki/LCF_notation
offline_file: ""
offline_thumbnail: ""
uuid: 22dd511c-9ef5-4297-9d3f-fff4883ada13
updated: 1484309409
title: LCF notation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nauru_graph_LCF.svg.png
tags:
    - Description
    - Applications
    - Examples
    - Extended LCF notation
categories:
    - Graph description languages
---
In combinatorial mathematics, LCF notation or LCF code is a notation devised by Joshua Lederberg, and extended by Coxeter and Frucht, for the representation of cubic graphs that contain a Hamiltonian cycle.[2][3] The cycle itself includes two out of the three adjacencies for each vertex, and the LCF notation specifies how far along the cycle each vertex's third neighbor is. A single graph may have multiple different representations in LCF notation.
