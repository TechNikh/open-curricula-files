---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/healthy
offline_file: ""
offline_thumbnail: ""
uuid: 4ba82ddc-495f-4e94-9df9-33c2dc795f31
updated: 1484310293
title: healthy
categories:
    - Dictionary
---
healthy
     adj 1: having or indicating good health in body or mind; free from
            infirmity or disease; "a rosy healthy baby"; "staying
            fit and healthy" [ant: {unhealthy}]
     2: financially secure and functioning well; "a healthy economy"
     3: promoting health; healthful; "a healthy diet"; "clean
        healthy air"; "plenty of healthy sleep"; "healthy and
        normal outlets for youthful energy"; "the salubrious
        mountain air and water"- C.B.Davis; "carrots are good for
        you" [syn: {salubrious}, {good for you(p)}]
     4: physically and mentally sound or healthy; "felt relaxed and
        fit after their holiday"; "keeps fit with diet and
       ...
