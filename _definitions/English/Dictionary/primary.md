---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/primary
offline_file: ""
offline_thumbnail: ""
uuid: 02a906d7-db7a-40d1-9890-e50ee246798b
updated: 1484310273
title: primary
categories:
    - Dictionary
---
primary
     adj 1: of first rank or importance or value; direct and immediate
            rather than secondhand; "primary goals"; "a primary
            effect"; "primary sources"; "a primary interest" [ant:
             {secondary}]
     2: not derived from or reducible to something else; basic; "a
        primary instinct"
     3: most important element; "the chief aim of living"; "the main
        doors were of solid glass"; "the principal rivers of
        America"; "the principal example"; "policemen were primary
        targets" [syn: {chief(a)}, {main(a)}, {primary(a)}, {principal(a)}]
     4: of or being the essential or basic part; "an elementary need
        for love and ...
