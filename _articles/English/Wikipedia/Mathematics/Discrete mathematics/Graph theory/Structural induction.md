---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Structural_induction
offline_file: ""
offline_thumbnail: ""
uuid: 45e87dec-48bd-4237-965e-b7fbc604b3fc
updated: 1484308629
title: Structural induction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Waldburg_Ahnentafel.jpg
tags:
    - Examples
    - Well-ordering
categories:
    - Graph theory
---
Structural induction is a proof method that is used in mathematical logic (e.g., in the proof of Łoś' theorem), computer science, graph theory, and some other mathematical fields. It is a generalization of mathematical induction over natural numbers, and can be further generalized to arbitrary Noetherian induction. Structural recursion is a recursion method bearing the same relationship to structural induction as ordinary recursion bears to ordinary mathematical induction.
