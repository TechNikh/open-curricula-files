---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seized
offline_file: ""
offline_thumbnail: ""
uuid: 8aff4674-8465-4e8a-8f1d-fedd5195eed8
updated: 1484310561
title: seized
categories:
    - Dictionary
---
seized
     adj : taken without permission or consent especially by public
           authority; "the condemned land was used for a highway
           cloverleaf"; "the confiscated liquor was poured down
           the drain" [syn: {appropriated}, {condemned}, {confiscate},
            {confiscated}, {taken over}]
