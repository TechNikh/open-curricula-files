---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Change_of_variables
offline_file: ""
offline_thumbnail: ""
uuid: 7d5f9380-91f3-4494-b941-424afb910631
updated: 1484308903
title: Change of variables
tags:
    - Simple example
    - Formal introduction
    - Other examples
    - Coordinate transformation
    - Differentiation
    - Integration
    - Differential equations
    - Scaling and shifting
    - Momentum vs. velocity
    - Lagrangian mechanics
categories:
    - Elementary algebra
---
In mathematics, a change of variables is a basic technique used to simplify problems in which the original variables are replaced with functions of other variables. The intent is that when expressed in new variables, the problem may become simpler, or equivalent to a better understood problem.
