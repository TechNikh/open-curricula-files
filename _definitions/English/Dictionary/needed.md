---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/needed
offline_file: ""
offline_thumbnail: ""
uuid: 0444df59-5904-433d-ac2b-b6410f1311fc
updated: 1484310264
title: needed
categories:
    - Dictionary
---
needed
     adj : necessary for relief or supply; "provided them with all
           things needful" [syn: {needful}, {required}, {requisite}]
