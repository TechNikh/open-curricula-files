---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boon
offline_file: ""
offline_thumbnail: ""
uuid: 18d8b967-8d66-4754-9e80-e596d0ac6837
updated: 1484310479
title: boon
categories:
    - Dictionary
---
boon
     adj : very close and convivial; "boon companions"
     n : a desirable state; "enjoy the blessings of peace"; "a
         spanking breeze is a boon to sailors" [syn: {blessing}]
