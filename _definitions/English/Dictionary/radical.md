---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radical
offline_file: ""
offline_thumbnail: ""
uuid: 300404be-7fbc-4031-8ae1-862059c93efe
updated: 1484310571
title: radical
categories:
    - Dictionary
---
radical
     adj 1: (used of opinions and actions) far beyond the norm;
            "extremist political views"; "radical opinions on
            education"; "an ultra conservative" [syn: {extremist},
             {ultra}]
     2: markedly new or introducing radical change; "a revolutionary
        discovery"; "radical political views" [syn: {revolutionary}]
     3: arising from or going to the root; "a radical flaw in the
        plan"
     4: of or relating to or constituting a linguistic root; "a
        radical verb form"
     5: especially of leaves; located at the base of a plant or
        stem; especially arising directly from the root or
        rootstock or a root-like stem; ...
