---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sweet
offline_file: ""
offline_thumbnail: ""
uuid: 1b378799-3b55-4891-a2eb-6118e4381f16
updated: 1484310349
title: sweet
categories:
    - Dictionary
---
sweet
     adj 1: having a pleasant taste (as of sugar) [ant: {sour}]
     2: having a sweet nature befitting an angel or cherub; "an
        angelic smile"; "a cherubic face"; "looking so seraphic
        when he slept"; "a sweet disposition" [syn: {angelic}, {angelical},
         {cherubic}, {seraphic}]
     3: pleasing to the ear; "the dulcet tones of the cello" [syn: {dulcet},
         {honeyed}, {mellifluous}, {mellisonant}]
     4: one of the four basic taste sensations; very pleasant; like
        the taste of sugar or honey
     5: pleasing to the senses; "the sweet song of the lark"; "the
        sweet face of a child"
     6: pleasing to the mind or feeling; "sweet revenge" [syn: ...
