---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aluminium
offline_file: ""
offline_thumbnail: ""
uuid: 9ca11da7-62e9-4245-b9a5-385382744eb8
updated: 1484310198
title: aluminium
categories:
    - Dictionary
---
aluminium
     n : a silvery ductile metallic element found primarily in
         bauxite [syn: {aluminum}, {Al}, {atomic number 13}]
