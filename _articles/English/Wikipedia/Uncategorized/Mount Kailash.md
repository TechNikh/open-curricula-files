---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kailas
offline_file: ""
offline_thumbnail: ""
uuid: 09cd5947-f6fa-41e4-9097-b71bd86c3dce
updated: 1484308435
title: Mount Kailash
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hindukailash_0.JPG
tags:
    - Name
    - Religious significance
    - In Hinduism
    - In Jainism
    - In Buddhism
    - In Bön
    - Pilgrimage
    - Geology
    - Mountaineering
    - Notes
categories:
    - Uncategorized
---
Mount Kailash (also Mount Kailas; Kangrinboqê or Gang Rinpoche (Tibetan: གངས་རིན་པོ་ཆེ) (Sanskrit: कैलाश) (Kailāśa) is a peak in the Kailash Range (Gangdisê Mountains), which forms part of the Transhimalaya in Tibet. It lies near the source of some of the longest rivers in Asia: the Indus River, the Sutlej River (a major tributary of the Indus River), the Brahmaputra River, and the Karnali River (a tributary of the River Ganga). It is considered a sacred place in four religions: Bön, Buddhism, Hinduism and Jainism. The mountain lies near Lake Manasarovar and Lake Rakshastal in Tibet Autonomous Region, China.
