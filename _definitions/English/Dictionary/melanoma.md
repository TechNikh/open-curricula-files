---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/melanoma
offline_file: ""
offline_thumbnail: ""
uuid: a09ad4cb-e919-402c-bf4e-dfedd7c5db3a
updated: 1484310416
title: melanoma
categories:
    - Dictionary
---
melanoma
     n : any of several malignant neoplasms (usually of the skin)
         consisting of melanocytes [syn: {malignant melanoma}]
     [also: {melanomata} (pl)]
