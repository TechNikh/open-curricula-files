---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/version
offline_file: ""
offline_thumbnail: ""
uuid: 4771508b-481d-4643-a064-fe7ad6ce91b4
updated: 1484310268
title: version
categories:
    - Dictionary
---
version
     n 1: an interpretation of a matter from a particular viewpoint;
          "his version of the fight was different from mine"
     2: something a little different from others of the same type;
        "an experimental version of the night fighter"; "an emery
        wheel is a modern variant of the grindstone"; "the boy is
        a younger edition of his father" [syn: {variant}, {variation},
         {edition}]
     3: a written work (as a novel) that has been recast in a new
        form; "the play is an adaptation of a short novel" [syn: {adaptation}]
     4: a written communication in a second language having the same
        meaning as the written communication in a first ...
