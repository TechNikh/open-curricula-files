---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/popularity
offline_file: ""
offline_thumbnail: ""
uuid: 9f3df9fc-783a-4d3d-abc4-4585eeaf5629
updated: 1484310555
title: popularity
categories:
    - Dictionary
---
popularity
     n : the quality of being widely admired or accepted or sought
         after; "his charm soon won him affection and popularity";
         "the universal popularity of American movies" [ant: {unpopularity}]
