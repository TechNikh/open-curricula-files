---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defy
offline_file: ""
offline_thumbnail: ""
uuid: a0ac6fac-8365-4f0c-95e1-bd0b3970aa2c
updated: 1484310275
title: defy
categories:
    - Dictionary
---
defy
     v 1: resist or confront with resistance; "The politician defied
          public opinion"; "The new material withstands even the
          greatest wear and tear"; "The bridge held" [syn: {withstand},
           {hold}, {hold up}]
     2: elude, especially in a baffling way; "This behavior defies
        explanation" [syn: {resist}, {refuse}] [ant: {lend oneself}]
     3: challenge; "I dare you!" [syn: {dare}]
     [also: {defied}]
