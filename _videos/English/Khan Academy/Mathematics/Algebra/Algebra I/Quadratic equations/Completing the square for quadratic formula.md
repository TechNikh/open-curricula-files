---
version: 1
type: video
provider: YouTube
id: 95tChNVzodY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Quadratic%20equations/Completing%20the%20square%20for%20quadratic%20formula.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b7c04a78-b525-4875-aff2-3e68f250af0d
updated: 1486069559
title: Completing the square for quadratic formula
categories:
    - Quadratic equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/95tChNVzodY/default.jpg
    - https://i3.ytimg.com/vi/95tChNVzodY/1.jpg
    - https://i3.ytimg.com/vi/95tChNVzodY/2.jpg
    - https://i3.ytimg.com/vi/95tChNVzodY/3.jpg
---
