---
version: 1
type: video
provider: YouTube
id: M9071V5gye4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c739a532-44ac-4c46-939c-1833ac7de04d
updated: 1486069611
title: Dividing Complex Numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/M9071V5gye4/default.jpg
    - https://i3.ytimg.com/vi/M9071V5gye4/1.jpg
    - https://i3.ytimg.com/vi/M9071V5gye4/2.jpg
    - https://i3.ytimg.com/vi/M9071V5gye4/3.jpg
---
