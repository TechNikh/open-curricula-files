---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Concurrent_lines
offline_file: ""
offline_thumbnail: ""
uuid: 0d253eb7-ea94-48a2-b587-d7e794d7cda8
updated: 1484308930
title: Concurrent lines
tags:
    - Examples
    - Quadrilaterals
    - Hexagons
    - Regular polygons
    - Circles
    - Ellipses
    - Hyperbolas
    - Tetrahedrons
    - Algebra
    - Projective geometry
categories:
    - Elementary geometry
---
