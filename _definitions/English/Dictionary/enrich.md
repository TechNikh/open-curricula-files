---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enrich
offline_file: ""
offline_thumbnail: ""
uuid: 5bc42085-fad7-4111-a615-019e289f59ba
updated: 1484310409
title: enrich
categories:
    - Dictionary
---
enrich
     v 1: make better or improve in quality; "The experience enriched
          her understanding"; "enriched foods" [ant: {deprive}]
     2: make wealthy or richer; "the oil boom enriched a lot of
        local people" [ant: {impoverish}]
