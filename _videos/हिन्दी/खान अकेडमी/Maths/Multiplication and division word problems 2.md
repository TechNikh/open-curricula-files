---
version: 1
type: video
provider: YouTube
id: IYgRRKXlRMI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4e450529-e6ad-4643-99c5-c5e70470df54
updated: 1486069617
title: Multiplication and division word problems 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/IYgRRKXlRMI/default.jpg
    - https://i3.ytimg.com/vi/IYgRRKXlRMI/1.jpg
    - https://i3.ytimg.com/vi/IYgRRKXlRMI/2.jpg
    - https://i3.ytimg.com/vi/IYgRRKXlRMI/3.jpg
---
