---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/linguist
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441041
title: linguist
categories:
    - Dictionary
---
linguist
     n 1: a specialist in linguistics [syn: {linguistic scientist}]
     2: a person who speaks more than one language [syn: {polyglot}]
