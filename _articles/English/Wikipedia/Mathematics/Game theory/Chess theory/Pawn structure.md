---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pawn_structure
offline_file: ""
offline_thumbnail: ""
uuid: 4eaaa534-5e79-426b-a4ac-2dee5ae06545
updated: 1484309020
title: Pawn structure
tags:
    - General observations
    - The major pawn formations
    - Caro formation
    - Slav formation
    - Sicilian – Scheveningen
    - Sicilian – Dragon
    - Sicilian – Maróczy bind
    - Hedgehog
    - Sicilian – Boleslavsky hole
    - d5 chain
    - e5 chain
    - "King's Indian – Rauzer formation"
    - "King's Indian – Boleslavsky Wall"
    - "Queen's Gambit – Isolani"
    - "Queen's Gambit – hanging pawns"
    - "Queen's Gambit – Orthodox Exchange"
    - Panov formation
    - Stonewall formation
    - Closed Sicilian formation
categories:
    - Chess theory
---
In chess, the pawn structure (sometimes known as the pawn skeleton) is the configuration of pawns on the chessboard. Since pawns are the least mobile of the chess pieces, the pawn structure is relatively static and thus largely determines the strategic nature of the position.
