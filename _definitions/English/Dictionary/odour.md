---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/odour
offline_file: ""
offline_thumbnail: ""
uuid: 8ce4f939-7a53-4828-a247-9fd5e13d3237
updated: 1484310379
title: odour
categories:
    - Dictionary
---
odour
     n 1: the sensation that results when olfactory receptors in the
          nose are stimulated by particular chemicals in gaseous
          form; "she loved the smell of roses" [syn: {smell}, {odor},
           {olfactory sensation}, {olfactory perception}]
     2: any property detected by the olfactory system [syn: {olfactory
        property}, {smell}, {aroma}, {odor}, {scent}]
