---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Additive_state_decomposition
offline_file: ""
offline_thumbnail: ""
uuid: 580cf335-3ac1-4f97-8091-d7d2704414a5
updated: 1484308814
title: Additive state decomposition
tags:
    - Additive state decomposition on a dynamical control system
    - Examples
    - Example 1
    - Example 2
    - Example 3
    - Comparison with superposition principle
    - Applications
categories:
    - Nonlinear systems
---
Additive state decomposition occurs when a system is decomposed into two or more subsystems with the same dimension as that of the original system.[1][2] A commonly used decomposition in the control field is to decompose a system into two or more lower-order subsystems, called lower-order subsystem decomposition here. In contrast, additive state decomposition is to decompose a system into two or more subsystems with the same dimension as that of the original system.[3]
