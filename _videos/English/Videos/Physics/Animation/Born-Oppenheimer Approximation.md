---
version: 1
type: video
provider: YouTube
id: bsufMOAvs4w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2c9846e6-19ed-4d2d-ae53-d67a4072284e
updated: 1486069653
title: Born--Oppenheimer Approximation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/bsufMOAvs4w/default.jpg
    - https://i3.ytimg.com/vi/bsufMOAvs4w/1.jpg
    - https://i3.ytimg.com/vi/bsufMOAvs4w/2.jpg
    - https://i3.ytimg.com/vi/bsufMOAvs4w/3.jpg
---
