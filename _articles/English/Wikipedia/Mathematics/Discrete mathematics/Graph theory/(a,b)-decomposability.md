---
version: 1
type: article
id: https://en.wikipedia.org/wiki/(a,b)-decomposability
offline_file: ""
offline_thumbnail: ""
uuid: d01f23c3-bebf-49fd-a007-d3a485f0314b
updated: 1484308612
title: (a,b)-decomposability
categories:
    - Graph theory
---
In graph theory, the (a, b)-decomposability of an undirected graph is the existence of a partition of its edges into a + 1 sets, each one of them inducing a forest, except one who induces a graph with maximum degree b. If this graph is also a forest, then we call this a F(a, b)-decomposition.
