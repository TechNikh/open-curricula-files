---
version: 1
type: video
provider: YouTube
id: eJ9e6P_xb9Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 59d800de-041b-4747-8073-68a7fe6b5f1f
updated: 1486069613
title: >
    Example using algebra to find measure of supplementary
    angles
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/eJ9e6P_xb9Y/default.jpg
    - https://i3.ytimg.com/vi/eJ9e6P_xb9Y/1.jpg
    - https://i3.ytimg.com/vi/eJ9e6P_xb9Y/2.jpg
    - https://i3.ytimg.com/vi/eJ9e6P_xb9Y/3.jpg
---
