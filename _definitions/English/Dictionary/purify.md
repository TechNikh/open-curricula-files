---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purify
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484473621
title: purify
categories:
    - Dictionary
---
purify
     v 1: remove impurities from, increase the concentration of, and
          separate through the process of distillation; "purify
          the water" [syn: {sublimate}, {make pure}, {distill}]
     2: make pure or free from sin or guilt; "he left the monastery
        purified" [syn: {purge}, {sanctify}]
     3: become clean or pure or free of guilt and sin; "The hippies
        came to the ashram in order to purify"
     [also: {purified}]
