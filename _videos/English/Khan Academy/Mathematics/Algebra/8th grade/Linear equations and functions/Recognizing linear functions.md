---
version: 1
type: video
provider: YouTube
id: AZroE4fJqtQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/8th%20grade/Linear%20equations%20and%20functions/Recognizing%20linear%20functions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b5587b23-bd9e-4ac0-be9c-eca8799f6e20
updated: 1486069567
title: Recognizing linear functions
categories:
    - Linear equations and functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/AZroE4fJqtQ/default.jpg
    - https://i3.ytimg.com/vi/AZroE4fJqtQ/1.jpg
    - https://i3.ytimg.com/vi/AZroE4fJqtQ/2.jpg
    - https://i3.ytimg.com/vi/AZroE4fJqtQ/3.jpg
---
