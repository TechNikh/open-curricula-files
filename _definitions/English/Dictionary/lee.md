---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lee
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484421181
title: lee
categories:
    - Dictionary
---
lee
     adj : towards the side away from the wind [syn: {downwind}, {lee(a)}]
     n 1: United States filmmaker whose works explore the richness of
          Black culture in America (born in 1957) [syn: {Spike Lee},
           {Shelton Jackson Lee}]
     2: United States striptease artist who became famous on
        Broadway in the 1930s (1914-1970) [syn: {Gypsy Rose Lee},
        {Rose Louise Hovick}]
     3: United States actor who was an expert in kung fu and starred
        in martial arts films (1941-1973) [syn: {Bruce Lee}, {Lee
        Yuen Kam}]
     4: United States physicist (born in China) who collaborated
        with Yang Chen Ning in disproving the principle of
        ...
