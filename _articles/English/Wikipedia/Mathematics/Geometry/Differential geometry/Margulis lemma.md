---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Margulis_lemma
offline_file: ""
offline_thumbnail: ""
uuid: a8f5508e-e1df-4a23-b45f-a1b67bbcc48e
updated: 1484309228
title: Margulis lemma
tags:
    - The Margulis lemma for manifolds of non-positive curvature
    - Formal statement
    - Margulis constants
    - Zassenhaus neighbourhoods
    - Thick-thin decomposition
    - Other applications
    - Notes
categories:
    - Differential geometry
---
In differential geometry, a subfield of mathematics, the Margulis lemma (named after Grigory Margulis) is a result about discrete subgroups of isometries of a non-positively curved Riemannian manifolds (e.g. the hyperbolic n-space). Roughly, it states that within a fixed radius, usually called the Margulis constant, the structure of the orbits of such a group cannot be too complicated. More precisely, within this radius around a point all points in its orbit are in fact in the orbit of a nilpotent subgroup (in fact a bounded finite number of such).
