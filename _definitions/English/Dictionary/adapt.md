---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adapt
offline_file: ""
offline_thumbnail: ""
uuid: eb290062-bb28-49c4-aeb1-dc8aa9f038f9
updated: 1484310289
title: adapt
categories:
    - Dictionary
---
adapt
     v 1: make fit for, or change to suit a new purpose; "Adapt our
          native cuisine to the available food resources of the
          new country" [syn: {accommodate}]
     2: adapt or conform oneself to new or different conditions; "We
        must adjust to the bad economic situation" [syn: {adjust},
         {conform}]
