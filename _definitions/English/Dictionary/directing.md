---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/directing
offline_file: ""
offline_thumbnail: ""
uuid: fbc19551-e20f-491a-ab3b-9e2a7d2f057b
updated: 1484310293
title: directing
categories:
    - Dictionary
---
directing
     adj : showing the way by conducting or leading; imposing direction
           on; "felt his mother's directing arm around him"; "the
           directional role of science on industrial progress"
           [syn: {directional}, {directive}, {guiding}]
