---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pl%C3%BCcker%27s_conoid'
offline_file: ""
offline_thumbnail: ""
uuid: e294398c-025e-4d6d-ba7c-b5227c04e1c4
updated: 1484309371
title: "Plücker's conoid"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Plucker%2527s_conoid_%2528n%253D2%2529.jpg'
categories:
    - Geometry stubs
---
In geometry, Plücker’s conoid is a ruled surface named after the German mathematician Julius Plücker. It is also called a conical wedge or cylindroid; however, the latter name is ambiguous, as "cylindroid" may also refer to an elliptic cylinder.
