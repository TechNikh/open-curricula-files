---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bureau
offline_file: ""
offline_thumbnail: ""
uuid: 15bfbb60-276f-4559-b332-fffac2324d50
updated: 1484310150
title: bureau
categories:
    - Dictionary
---
bureau
     n 1: an administrative unit of government; "the Central
          Intelligence Agency"; "the Census Bureau"; "Office of
          Management and Budget"; "Tennessee Valley Authority"
          [syn: {agency}, {federal agency}, {government agency}, {office},
           {authority}]
     2: furniture with drawers for keeping clothes [syn: {chest of
        drawers}, {chest}, {dresser}]
     [also: {bureaux} (pl)]
