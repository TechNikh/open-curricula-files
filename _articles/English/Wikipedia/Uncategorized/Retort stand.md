---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Retort_stand
offline_file: ""
offline_thumbnail: ""
uuid: f9e66ffc-65b5-417b-ba81-cc47f1f0eb26
updated: 1484308363
title: Retort stand
categories:
    - Uncategorized
---
A retort stand, sometimes called a ring stand,[1] is a piece of scientific equipment, to which clamps can be attached to hold test tubes and other equipment such as burettes which are most often used in titration experiments.
