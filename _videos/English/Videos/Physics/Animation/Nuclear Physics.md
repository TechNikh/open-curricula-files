---
version: 1
type: video
provider: YouTube
id: r40h66qiF5I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Nuclear%20Physics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c2562d85-c77f-4fb2-9d3f-65520e450ae5
updated: 1486069654
title: Nuclear Physics
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/r40h66qiF5I/default.jpg
    - https://i3.ytimg.com/vi/r40h66qiF5I/1.jpg
    - https://i3.ytimg.com/vi/r40h66qiF5I/2.jpg
    - https://i3.ytimg.com/vi/r40h66qiF5I/3.jpg
---
