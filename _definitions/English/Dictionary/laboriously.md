---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laboriously
offline_file: ""
offline_thumbnail: ""
uuid: e93911dc-9659-4621-bb21-eb8319493b3d
updated: 1484310601
title: laboriously
categories:
    - Dictionary
---
laboriously
     adv : in a laborious manner; "their lives were spent in committee
           making decisions for others to execute on the basis of
           data laboriously gathered for them"
