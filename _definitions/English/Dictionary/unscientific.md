---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unscientific
offline_file: ""
offline_thumbnail: ""
uuid: 39a1da5f-46e3-4171-8e10-89d6951d08dd
updated: 1484310533
title: unscientific
categories:
    - Dictionary
---
unscientific
     adj : not consistent with the methods or principles of science;
           "an unscientific lack of objectivity" [ant: {scientific}]
