---
version: 1
type: video
provider: YouTube
id: Z36dUduOk1Y
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/B%20Lymphocytes%20%28B%20cells%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: acad1b61-61bb-4400-91d5-7ff8cf5848f0
updated: 1486069583
title: B Lymphocytes (B cells)
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/Z36dUduOk1Y/default.jpg
    - https://i3.ytimg.com/vi/Z36dUduOk1Y/1.jpg
    - https://i3.ytimg.com/vi/Z36dUduOk1Y/2.jpg
    - https://i3.ytimg.com/vi/Z36dUduOk1Y/3.jpg
---
Overview of B cells (B lymphocytes) and how they are activated and produce antibodies
More free lessons at: http://www.khanacademy.org/video?v=Z36dUduOk1Y
