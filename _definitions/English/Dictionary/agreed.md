---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agreed
offline_file: ""
offline_thumbnail: ""
uuid: 47ddc80b-4443-439d-b30e-645d7fde8c29
updated: 1484310517
title: agreed
categories:
    - Dictionary
---
agreed
     adj : united by being of the same opinion; "agreed in their
           distrust of authority" [syn: {in agreement(p)}]
