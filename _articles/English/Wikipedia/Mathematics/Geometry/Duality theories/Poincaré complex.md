---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poincar%C3%A9_complex'
offline_file: ""
offline_thumbnail: ""
uuid: b5c6c502-d8d5-4599-877a-a13966255260
updated: 1484309237
title: Poincaré complex
tags:
    - Definition
    - Example
categories:
    - Duality theories
---
In mathematics, and especially topology, a Poincaré complex (named after the mathematician Henri Poincaré) is an abstraction of the singular chain complex of a closed, orientable manifold.
