---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/description
offline_file: ""
offline_thumbnail: ""
uuid: 9281f815-47be-414d-a187-cf2d88a8d43c
updated: 1484310319
title: description
categories:
    - Dictionary
---
description
     n 1: a statement that represents something in words [syn: {verbal
          description}]
     2: the act of describing something
     3: sort or variety; "every description of book was there"
