---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prosper
offline_file: ""
offline_thumbnail: ""
uuid: 02ff90cf-31ef-46fc-9c60-9ea7efaec727
updated: 1484310249
title: prosper
categories:
    - Dictionary
---
prosper
     v 1: grow stronger; "The economy was booming" [syn: {boom}, {thrive},
           {get ahead}, {flourish}, {expand}]
     2: gain in wealth [syn: {thrive}, {fly high}, {flourish}]
