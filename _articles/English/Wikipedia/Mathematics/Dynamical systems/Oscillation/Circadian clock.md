---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circadian_clock
offline_file: ""
offline_thumbnail: ""
uuid: d88e3dc1-59b8-4307-ae9c-0622333642e5
updated: 1484308822
title: Circadian clock
tags:
    - Transcriptional and non-transcriptional control
    - Mammalian clocks
    - Other organisms
    - Post-transcriptional modification
    - Post-translational modification
    - Regulation of circadian oscillators
    - >
        Systems biology approaches to elucidate oscillating
        mechanisms
categories:
    - Oscillation
---
The circadian clock, or circadian oscillator, in most living things makes it possible for organisms to coordinate their biology and behavior with daily environmental changes in the day-night cycle. The term circadian derives from the Latin circa (about) diem (a day), since when taken away from external cues (such as the day-night cycle), they do not run to exactly 24 hours. Clocks in humans in a lab in constant low light, for example, will average about 24.2 hours per day, rather than 24 hours exactly.[1]
