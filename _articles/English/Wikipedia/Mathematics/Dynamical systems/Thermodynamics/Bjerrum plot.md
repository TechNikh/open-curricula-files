---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bjerrum_plot
offline_file: ""
offline_thumbnail: ""
uuid: 663b56c4-98df-4bd8-9653-417722a14c7b
updated: 1484308838
title: Bjerrum plot
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Carbonate_system_of_seawater.svg.png
tags:
    - Bjerrum plot equations for carbonate system
    - >
        Chemical and mathematical derivation of Bjerrum plot
        equations for carbonate system
categories:
    - Thermodynamics
---
A Bjerrum plot is a graph of the concentrations of the different species of a polyprotic acid in a solution, as functions of the solution's pH,[1] when the solution is at equilibrium. Due to the many orders of magnitude spanned by the concentrations, they are commonly plotted on a logarithmic scale. Sometimes the ratios of the concentrations are plotted rather than the actual concentrations. Occasionally H+ and OH− are also plotted.
