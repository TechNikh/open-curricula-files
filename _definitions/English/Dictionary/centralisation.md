---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/centralisation
offline_file: ""
offline_thumbnail: ""
uuid: f43db7b7-ba29-44b1-8e33-e2e3fcf366b5
updated: 1484310567
title: centralisation
categories:
    - Dictionary
---
centralisation
     n 1: the act of consolidating power under a central control [syn:
           {centralization}] [ant: {decentralization}]
     2: gathering to a center [syn: {centralization}]
