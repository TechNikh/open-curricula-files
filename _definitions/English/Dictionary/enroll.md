---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enroll
offline_file: ""
offline_thumbnail: ""
uuid: ab9a236c-9a24-44e2-887f-b5f610dbe013
updated: 1484310575
title: enroll
categories:
    - Dictionary
---
enroll
     v : register formally as a participant or member; "The party
         recruited many new members" [syn: {inscribe}, {enter}, {enrol},
          {recruit}]
