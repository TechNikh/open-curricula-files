---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vigorously
offline_file: ""
offline_thumbnail: ""
uuid: ee23f7f6-e4f6-4c49-8a0e-120356a2cc23
updated: 1484310387
title: vigorously
categories:
    - Dictionary
---
vigorously
     adv : with vigor; in a vigorous manner; "he defended his ideas
           vigorously" [syn: {smartly}]
