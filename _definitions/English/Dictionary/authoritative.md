---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/authoritative
offline_file: ""
offline_thumbnail: ""
uuid: 63e8f9e0-17da-4329-b525-6d051b6a475c
updated: 1484310168
title: authoritative
categories:
    - Dictionary
---
authoritative
     adj 1: having authority or ascendancy or influence; "an important
            official"; "the captain's authoritative manner" [syn:
            {important}]
     2: of recognized authority or excellence; "the definitive work
        on Greece"; "classical methods of navigation" [syn: {classical},
         {definitive}]
     3: sanctioned by established authority; "an authoritative
        communique"; "the authorized biography" [syn: {authorized},
         {authorised}]
