---
version: 1
type: article
id: https://en.wikipedia.org/wiki/STAR_model
offline_file: ""
offline_thumbnail: ""
uuid: f6fba972-e7ed-4c15-bf6d-622013f71982
updated: 1484308816
title: STAR model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/He1523a_3.jpg
tags:
    - Definition
    - Autoregressive Models
    - STAR as an Extension of the AutoRegressive Model
    - Basic Structure
    - Transition Function
categories:
    - Nonlinear systems
---
In statistics, Smooth Transition Autoregressive (STAR) models are typically applied to time series data as an extension of autoregressive models, in order to allow for higher degree of flexibility in model parameters through a smooth transition.
