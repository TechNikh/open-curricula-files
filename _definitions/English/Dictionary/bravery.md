---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bravery
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484410681
title: bravery
categories:
    - Dictionary
---
bravery
     n 1: a quality of spirit that enables you to face danger of pain
          without showing fear [syn: {courage}, {courageousness}]
          [ant: {cowardice}]
     2: feeling no fear [syn: {fearlessness}] [ant: {fear}]
