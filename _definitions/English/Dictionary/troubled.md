---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/troubled
offline_file: ""
offline_thumbnail: ""
uuid: 56e1d420-c8e4-43b6-b799-eac47cba9f90
updated: 1484310152
title: troubled
categories:
    - Dictionary
---
troubled
     adj 1: characterized by or indicative of distress or affliction or
            danger or need; "troubled areas"; "fell into a
            troubled sleep"; "a troubled expression"; "troubled
            teenagers" [ant: {untroubled}]
     2: characterized by unrest or disorder or insubordination;
        "effects of the struggle will be violent and disruptive";
        "riotous times"; "these troubled areas"; "the tumultuous
        years of his administration"; "a turbulent and unruly
        childhood" [syn: {disruptive}, {riotous}, {tumultuous}, {turbulent}]
