---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phenyl
offline_file: ""
offline_thumbnail: ""
uuid: fee7d37d-7d93-4f70-be84-a314e6bcb9e9
updated: 1484308405
title: Phenyl group
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Phenyl-group.png
tags:
    - Nomenclature
    - Etymology
    - Structure, bonding, characterization
    - Preparation, occurrence, and applications
categories:
    - Uncategorized
---
In organic chemistry, the phenyl group or phenyl ring is a cyclic group of atoms with the formula C6H5. Phenyl groups are closely related to benzene and can be viewed as a benzene ring, minus a hydrogen, serving as a functional group. Phenyl groups have six carbon atoms bonded together in a hexagonal planar ring, five of which are bonded to individual hydrogen atoms, with the remaining carbon bonded to a substituent. Phenyl groups are commonplace in organic chemistry.[1] Although often depicted with alternating double and single bonds, phenyl groups are chemically aromatic and show nearly equal bond lengths between carbon atoms in the ring.[1][2]
