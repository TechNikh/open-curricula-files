---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/southeast
offline_file: ""
offline_thumbnail: ""
uuid: 91c323b2-6817-4ade-b1b1-2e538a6f71ad
updated: 1484310575
title: southeast
categories:
    - Dictionary
---
south-east
     adv : to, toward, or in the southeast [syn: {southeast}, {sou'-east}]
