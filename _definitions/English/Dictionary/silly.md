---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silly
offline_file: ""
offline_thumbnail: ""
uuid: b6c5a666-803b-4188-a5d7-eada11aee222
updated: 1484310154
title: silly
categories:
    - Dictionary
---
silly
     adj 1: pungent adjectives of disesteem; "gave me a cockamamie
            reason for not going"; "wore a goofy hat"; "a silly
            idea"; "some wacky plan for selling more books" [syn:
            {cockamamie}, {cockamamy}, {goofy}, {sappy}, {wacky},
            {whacky}, {zany}, {unreasonable}]
     2: lacking seriousness; given to frivolity; "a dizzy blonde";
        "light-headed teenagers"; "silly giggles" [syn: {airheaded},
         {dizzy}, {empty-headed}, {featherbrained}, {giddy}, {light-headed},
         {lightheaded}]
     3: inspiring scornful pity; "how silly an ardent and
        unsuccessful wooer can be especially if he is getting on
        in years"- ...
