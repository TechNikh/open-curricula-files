---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dishonest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484461681
title: dishonest
categories:
    - Dictionary
---
dishonest
     adj 1: deceptive or fraudulent; disposed to cheat or defraud or
            deceive [syn: {dishonorable}] [ant: {honest}]
     2: lacking honesty and oblivious to what is honorable [syn: {unscrupulous}]
     3: lacking truthfulness; "a dishonest answer"
     4: capable of being corrupted; "corruptible judges"; "dishonest
        politicians"; "a purchasable senator"; "a venal police
        officer" [syn: {corruptible}, {bribable}, {purchasable}, {venal}]
