---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impartial
offline_file: ""
offline_thumbnail: ""
uuid: 014f82cb-402b-4c0d-acbc-c3aae364adc8
updated: 1484310170
title: impartial
categories:
    - Dictionary
---
impartial
     adj 1: showing lack of favoritism; "the cold neutrality of an
            impartial judge" [syn: {fair}] [ant: {partial}]
     2: free from undue bias or preconceived opinions; "an
        unprejudiced appraisal of the pros and cons"; "the
        impartial eye of a scientist" [syn: {unprejudiced}] [ant:
        {prejudiced}]
