---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nonextensive_entropy
offline_file: ""
offline_thumbnail: ""
uuid: 20a1eee4-54ae-4754-b33b-11b5d7f33589
updated: 1484308773
title: Nonextensive entropy
categories:
    - Entropy and information
---
Entropy is considered to be an extensive property, i.e., that its value depends on the amount of material present. Constantino Tsallis has proposed a nonextensive entropy (Tsallis entropy), which is a generalization of the traditional Boltzmann–Gibbs entropy.
