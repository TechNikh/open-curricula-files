---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bojan_Mohar
offline_file: ""
offline_thumbnail: ""
uuid: b4f6433e-df4f-4a29-b2d5-a5a2bf7dca56
updated: 1484309472
title: Bojan Mohar
tags:
    - Education
    - Research
    - Awards and honors
categories:
    - Graph theorists
---
Bojan Mohar is a Slovenian and Canadian mathematician, specializing in graph theory. He is a professor of mathematics at the University of Ljubljana[1] and the holder of a Canada Research Chair in graph theory at Simon Fraser University in Vancouver, Canada.[2]
