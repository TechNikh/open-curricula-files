---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scorched
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484579941
title: scorched
categories:
    - Dictionary
---
scorched
     adj 1: dried out by heat or excessive exposure to sunlight; "a vast
            desert all adust"; "land lying baked in the heat";
            "parched soil"; "the earth was scorched and bare";
            "sunbaked salt flats" [syn: {adust}, {baked}, {parched},
             {sunbaked}]
     2: having everything destroyed so nothing is left salvageable
        by an enemy; "Sherman's scorched earth policy"
     3: damaged or discolored by superficial burning:"the scorched
        blouse tore easily"
