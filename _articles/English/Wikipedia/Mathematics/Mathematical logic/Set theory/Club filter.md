---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Club_filter
offline_file: ""
offline_thumbnail: ""
uuid: 7f770ca3-b4c9-4da7-8e71-c6f8e1c5b94e
updated: 1484309501
title: Club filter
categories:
    - Set theory
---
In mathematics, particularly in set theory, if 
  
    
      
        κ
      
    
    {\displaystyle \kappa }
  
 is a regular uncountable cardinal then 
  
    
      
        club
        ⁡
        (
        κ
        )
      
    
    {\displaystyle \operatorname {club} (\kappa )}
  
, the filter of all sets containing a club subset of 
  
    
      
        κ
      
    
    {\displaystyle \kappa }
  
, is a 
  
    
      
        κ
      
    
    {\displaystyle \kappa }
  
-complete filter closed under diagonal intersection called the club filter.
