---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_of_automation_protocols
offline_file: ""
offline_thumbnail: ""
uuid: 385ef61e-81a6-4de8-9e80-31eb016a5397
updated: 1484308743
title: List of automation protocols
tags:
    - Process automation protocols
    - Industrial control system protocols
    - Building automation protocols
    - Power system automation protocols
    - Automatic meter reading protocols
    - Automobile / Vehicle protocol buses
categories:
    - Control engineering
---
This includes list of communication network protocols used for process or industrial automation, building automation, substation automation, automatic meter reading and vehicle automation applications
