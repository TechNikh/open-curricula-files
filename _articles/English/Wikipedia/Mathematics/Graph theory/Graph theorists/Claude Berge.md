---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Claude_Berge
offline_file: ""
offline_thumbnail: ""
uuid: 08e97565-fa77-4551-a0fb-72daddde7b0f
updated: 1484309461
title: Claude Berge
tags:
    - Biography and professional history
    - Mathematical contributions
    - Wordplay
    - Awards and honors
    - Selected publications
    - Major Mathematical Works
    - Literary Work
categories:
    - Graph theorists
---
Claude Jacques Berge (5 June 1926 – 30 June 2002) was a French mathematician, recognized as one of the modern founders of combinatorics and graph theory.
