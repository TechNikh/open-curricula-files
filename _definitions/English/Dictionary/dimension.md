---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dimension
offline_file: ""
offline_thumbnail: ""
uuid: 7341ebd3-cf65-4c54-a48f-8f1db4babcee
updated: 1484310533
title: dimension
categories:
    - Dictionary
---
dimension
     n 1: the magnitude of something in a particular direction
          (especially length or width or height)
     2: a construct whereby objects or individuals can be
        distinguished; "self-confidence is not an endearing
        property" [syn: {property}, {attribute}]
     3: one of three cartesian coordinates that determine a position
        in space
     4: magnitude or extent; "a building of vast proportions" [syn:
        {proportion}]
     v 1: indicate the dimensions on; "These techniques permit us to
          dimension the human heart"
     2: shape or form to required dimensions
