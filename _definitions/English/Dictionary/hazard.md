---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hazard
offline_file: ""
offline_thumbnail: ""
uuid: 74a9045f-296d-47f1-9b06-de2a33e98acf
updated: 1484310163
title: hazard
categories:
    - Dictionary
---
hazard
     n 1: a source of danger; a possibility of incurring loss or
          misfortune; "drinking alcohol is a health hazard" [syn:
          {jeopardy}, {peril}, {risk}]
     2: an unknown and unpredictable phenomenon that causes an event
        to result one way rather than another; "bad luck caused
        his downfall"; "we ran into each other by pure chance"
        [syn: {luck}, {fortune}, {chance}]
     3: an obstacle on a golf course
     v 1: put forward, of a guess, in spite of possible refutation; "I
          am guessing that the price of real estate will rise
          again"; "I cannot pretend to say that you are wrong"
          [syn: {guess}, {venture}, {pretend}]
    ...
