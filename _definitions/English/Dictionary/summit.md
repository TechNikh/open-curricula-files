---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/summit
offline_file: ""
offline_thumbnail: ""
uuid: 12bca12c-089c-4730-a4f5-fc6b3468e8f2
updated: 1484310273
title: summit
categories:
    - Dictionary
---
summit
     n 1: the highest level or degree attainable; "his landscapes were
          deemed the acme of beauty"; "the artist's gifts are at
          their acme"; "at the height of her career"; "the peak of
          perfection"; "summer was at its peak"; "...catapulted
          Einstein to the pinnacle of fame"; "the summit of his
          ambition"; "so many highest superlatives achieved by
          man"; "at the top of his profession" [syn: {acme}, {height},
           {elevation}, {peak}, {pinnacle}, {superlative}, {top}]
     2: the top point of a mountain or hill; "the view from the peak
        was magnificent"; "they clambered to the summit of
        Monadnock" [syn: {peak}, ...
