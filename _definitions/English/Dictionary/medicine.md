---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/medicine
offline_file: ""
offline_thumbnail: ""
uuid: 5870d1c8-98e8-4f87-956a-46c0ff62e751
updated: 1484310315
title: medicine
categories:
    - Dictionary
---
medicine
     n 1: the branches of medical science that deal with nonsurgical
          techniques [syn: {medical specialty}]
     2: (medicine) something that treats or prevents or alleviates
        the symptoms of disease [syn: {medication}, {medicament},
        {medicinal drug}]
     3: the learned profession that is mastered by graduate training
        in a medical school and that is devoted to preventing or
        alleviating or curing diseases and injuries; "he studied
        medicine at Harvard" [syn: {practice of medicine}]
     4: punishment for one's actions; "you have to face the music";
        "take your medicine" [syn: {music}]
     v : treat medicinally, treat with ...
