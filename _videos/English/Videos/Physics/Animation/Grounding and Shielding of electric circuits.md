---
version: 1
type: video
provider: YouTube
id: rIhF410L2SU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3e2741a4-7e9e-48ec-a0b9-b2f1995ecf10
updated: 1486069656
title: Grounding and Shielding of electric circuits
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/rIhF410L2SU/default.jpg
    - https://i3.ytimg.com/vi/rIhF410L2SU/1.jpg
    - https://i3.ytimg.com/vi/rIhF410L2SU/2.jpg
    - https://i3.ytimg.com/vi/rIhF410L2SU/3.jpg
---
