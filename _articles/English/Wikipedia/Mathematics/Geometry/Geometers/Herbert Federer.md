---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Herbert_Federer
offline_file: ""
offline_thumbnail: ""
uuid: f030753e-a079-483a-8bde-6fc1a55ea495
updated: 1484309078
title: Herbert Federer
tags:
    - Career
    - Normal and integral currents
    - Earlier work
    - Geometric measure theory
categories:
    - Geometers
---
Herbert Federer (July 23, 1920 – April 21, 2010)[1] was an American mathematician. He is one of the creators of geometric measure theory, at the meeting point of differential geometry and mathematical analysis.[2]
