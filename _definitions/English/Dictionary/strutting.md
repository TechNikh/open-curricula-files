---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strutting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484578022
title: strutting
categories:
    - Dictionary
---
strut
     n 1: a proud stiff pompous gait [syn: {prance}, {swagger}]
     2: brace consisting of a bar or rod used to resist longitudinal
        compression
     v : to walk with a lofty proud gait, often in an attempt to
         impress others; "He struts around like a rooster in a hen
         house" [syn: {swagger}, {ruffle}, {prance}, {sashay}, {cock}]
     [also: {strutting}, {strutted}]
