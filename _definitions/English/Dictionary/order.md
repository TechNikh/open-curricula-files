---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/order
offline_file: ""
offline_thumbnail: ""
uuid: 5c29b41a-352b-4524-923e-0e570151dc7e
updated: 1484310309
title: order
categories:
    - Dictionary
---
order
     n 1: (often plural) a command given by a superior (e.g., a
          military or law enforcement officer) that must be
          obeyed; "the British ships dropped anchor and waited for
          orders from London"
     2: a degree in a continuum of size or quantity; "it was on the
        order of a mile"; "an explosion of a low order of
        magnitude" [syn: {order of magnitude}]
     3: established customary state (especially of society); "order
        ruled in the streets"; "law and order" [ant: {disorder}]
     4: logical or comprehensible arrangement of separate elements;
        "we shall consider these questions in the inverse order of
        their presentation" ...
