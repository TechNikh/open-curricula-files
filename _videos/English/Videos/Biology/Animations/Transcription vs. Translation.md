---
version: 1
type: video
provider: YouTube
id: WvZ1PK8Uxao
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Transcription%20vs.%20Translation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1ebf5449-751b-4dc8-ae02-a498ba50f602
updated: 1486069672
title: Transcription vs. Translation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/WvZ1PK8Uxao/default.jpg
    - https://i3.ytimg.com/vi/WvZ1PK8Uxao/1.jpg
    - https://i3.ytimg.com/vi/WvZ1PK8Uxao/2.jpg
    - https://i3.ytimg.com/vi/WvZ1PK8Uxao/3.jpg
---
