---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unfamiliar
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484654221
title: unfamiliar
categories:
    - Dictionary
---
unfamiliar
     adj : not known or well known; "a name unfamiliar to most"; "be
           alert at night especially in unfamiliar surroundings"
           [ant: {familiar}]
