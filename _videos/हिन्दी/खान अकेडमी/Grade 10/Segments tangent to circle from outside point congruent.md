---
version: 1
type: video
provider: YouTube
id: XTKKZIDoJz4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a84e2947-b355-46b7-bc19-58f724a2dc13
updated: 1486069619
title: Segments tangent to circle from outside point congruent
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/XTKKZIDoJz4/default.jpg
    - https://i3.ytimg.com/vi/XTKKZIDoJz4/1.jpg
    - https://i3.ytimg.com/vi/XTKKZIDoJz4/2.jpg
    - https://i3.ytimg.com/vi/XTKKZIDoJz4/3.jpg
---
