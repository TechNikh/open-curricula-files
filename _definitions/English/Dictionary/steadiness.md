---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/steadiness
offline_file: ""
offline_thumbnail: ""
uuid: 8650b593-914f-4ea6-943d-88027c125543
updated: 1484310232
title: steadiness
categories:
    - Dictionary
---
steadiness
     n 1: the quality of being steady or securely and immovably fixed
          in place [syn: {firmness}] [ant: {unsteadiness}]
     2: the quality of being steady--regular and unvarying [ant: {unsteadiness}]
