---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Chladni%27s_law'
offline_file: ""
offline_thumbnail: ""
uuid: 1e92e427-0256-4b2a-a4cc-883010eddbfa
updated: 1484308885
title: "Chladni's law"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Chladni_plate_10.jpg
categories:
    - Waves
---
Chladni's law, named after Ernst Chladni, relates the frequency of modes of vibration for flat circular surfaces with fixed center as a function of the numbers m of diametric (linear) nodes and n of radial (circular) nodes. It is stated as the equation
