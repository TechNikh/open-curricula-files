---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/majority
offline_file: ""
offline_thumbnail: ""
uuid: 4b20205d-fbbf-4d74-973d-13ced4b57c4b
updated: 1484310260
title: majority
categories:
    - Dictionary
---
majority
     n 1: the property resulting from being or relating to the greater
          in number of two parts; the main part; "the majority of
          his customers prefer it"; "the bulk of the work is
          finished" [syn: {bulk}] [ant: {minority}]
     2: (elections) more than half of the votes [syn: {absolute
        majority}]
     3: the age at which a person is considered competent to manage
        their own affairs [syn: {legal age}] [ant: {minority}]
