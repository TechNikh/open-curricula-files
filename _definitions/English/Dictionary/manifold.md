---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manifold
offline_file: ""
offline_thumbnail: ""
uuid: 4254dfc6-9002-4a4a-a96e-13195ba52356
updated: 1484310553
title: manifold
categories:
    - Dictionary
---
manifold
     adj : many and varied; having many features or forms; "manifold
           reasons"; "our manifold failings"; "manifold
           intelligence"; "the multiplex opportunities in high
           technology" [syn: {multiplex}]
     n 1: a pipe that has several lateral outlets to or from other
          pipes
     2: a lightweight paper used with carbon paper to make multiple
        copies; "an original and two manifolds" [syn: {manifold
        paper}]
     3: a set of points such as those of a closed surface or and
        analogue in three or more dimensions
     v 1: make multiple copies of; "multiply a letter"
     2: combine or increase by multiplication; "He managed to
   ...
