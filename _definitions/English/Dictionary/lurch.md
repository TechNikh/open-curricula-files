---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lurch
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484563141
title: lurch
categories:
    - Dictionary
---
lurch
     n 1: an unsteady uneven gait [syn: {stumble}, {stagger}]
     2: a decisive defeat in a game (especially in cribbage)
     3: abrupt up-and-down motion (as caused by a ship or other
        conveyance); "the pitching and tossing was quite exciting"
        [syn: {pitch}, {pitching}]
     4: the act of moving forward suddenly [syn: {lunge}]
     v 1: walk as if unable to control one's movements; "The drunken
          man staggered into the room" [syn: {stagger}, {reel}, {keel},
           {swag}, {careen}]
     2: move abruptly; "The ship suddenly lurched to the left" [syn:
         {pitch}, {shift}]
     3: move slowly and unsteadily; "The truck lurched down the
        road"
   ...
