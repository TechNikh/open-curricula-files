---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/analyzed
offline_file: ""
offline_thumbnail: ""
uuid: 91c81294-131e-4422-bbf0-90682655be87
updated: 1484310393
title: analyzed
categories:
    - Dictionary
---
analyzed
     adj : examined carefully and methodically; broken down for
           consideration of constituent parts; "the analyzed data
           indicated surprising trends"; "a carefully analyzed
           poem can be like a dead butterfly pinned to a board"
           [ant: {unanalyzed}]
