---
version: 1
type: video
provider: YouTube
id: ay8838UZ4nM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Calculus-%20Derivatives%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0859a42c-4537-4645-86db-57315a55c126
updated: 1486069693
title: 'Calculus- Derivatives 2'
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/ay8838UZ4nM/default.jpg
    - https://i3.ytimg.com/vi/ay8838UZ4nM/1.jpg
    - https://i3.ytimg.com/vi/ay8838UZ4nM/2.jpg
    - https://i3.ytimg.com/vi/ay8838UZ4nM/3.jpg
---
