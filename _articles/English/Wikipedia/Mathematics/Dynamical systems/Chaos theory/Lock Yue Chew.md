---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lock_Yue_Chew
offline_file: ""
offline_thumbnail: ""
uuid: 7fd1906a-c872-481e-85c7-99c928b444eb
updated: 1484308709
title: Lock Yue Chew
tags:
    - Public appearance
    - Awards
categories:
    - Chaos theory
---
Lock Yue Chew is an associate professor in the School of Physical & Mathematical Sciences, Nanyang Technological University .[1] He works in Complex Systems, General Relativity, Quantum Chaos, Statistical and Nonlinear Physics, and has published several scientific papers. In 2004 he got his PhD in Physics from the National University of Singapore (N.U.S). He also hold a Master of Science Degree from the University of Southern California. During 1991–1992 period, he had worked as a communications engineer in Singapore Airlines Limited. After that, he became the senior member of the technical staff at DSO National Laboratories and later he became and adjunct assistant professor there. Then ...
