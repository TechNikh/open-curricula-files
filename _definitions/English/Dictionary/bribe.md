---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bribe
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484587801
title: bribe
categories:
    - Dictionary
---
bribe
     n : payment made to a person in a position of trust to corrupt
         his judgment [syn: {payoff}]
     v : make illegal payments to in exchange for favors or
         influence; "This judge can be bought" [syn: {corrupt}, {buy},
          {grease one's palms}]
