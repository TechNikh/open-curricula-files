---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Indal
offline_file: ""
offline_thumbnail: ""
uuid: 47aebcb9-b111-4168-8100-13353d6adc70
updated: 1484308470
title: Indal
categories:
    - Uncategorized
---
Indal is a locality situated in Sundsvall Municipality, Västernorrland County, Sweden with 661 inhabitants in 2010.[1] It is located close to the river Indalsälven.
