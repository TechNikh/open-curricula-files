---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disastrous
offline_file: ""
offline_thumbnail: ""
uuid: b62b1486-649e-4e2e-b6fd-3f2abce7e964
updated: 1484310545
title: disastrous
categories:
    - Dictionary
---
disastrous
     adj : (of events) having extremely unfortunate or dire
           consequences; bringing ruin; "the stock market crashed
           on Black Friday"; "a calamitous defeat"; "the battle
           was a disastrous end to a disastrous campaign"; "such
           doctrines, if true, would be absolutely fatal to my
           theory"- Charles Darwin; "it is fatal to enter any war
           without the will to win it"- Douglas MacArthur; "a
           fateful error" [syn: {black}, {calamitous}, {fatal}, {fateful}]
