---
version: 1
type: video
provider: YouTube
id: ZqzAOZ9pP9Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Find%20the%20volume%20of%20a%20triangular%20prism%20and%20cube.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 306ccbfb-639e-4aca-ae90-5099b595ce62
updated: 1486069579
title: Find the volume of a triangular prism and cube
categories:
    - Geometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZqzAOZ9pP9Q/default.jpg
    - https://i3.ytimg.com/vi/ZqzAOZ9pP9Q/1.jpg
    - https://i3.ytimg.com/vi/ZqzAOZ9pP9Q/2.jpg
    - https://i3.ytimg.com/vi/ZqzAOZ9pP9Q/3.jpg
---
In this tutorial we'll practice finding solid geometry volume. Keep paper and pencil nearby in case you want to remember the formulas.

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/geometry/basic-geometry/volume_tutorial/e/solid_geometry?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Watch the next lesson: https://www.khanacademy.org/math/geometry/basic-geometry/volume_tutorial/v/cylinder-volume-and-surface-area?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Missed the previous lesson? 
https://www.khanacademy.org/math/geometry/basic-geometry/volume_tutorial/v/surface-area-from-net?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Geometry on Khan Academy: We are surrounded by space. And that space contains lots of things. And these things have shapes. In geometry we are concerned with the nature of these shapes, how we define them, and what they teach us about the world at large--from math to architecture to biology to astronomy (and everything in between). Learning geometry is about more than just taking your medicine ("It's good for you!"), it's at the core of everything that exists--including you. Having said all that, some of the specific topics we'll cover include angles, intersecting lines, right triangles, perimeter, area, volume, circles, triangles, quadrilaterals, analytic geometry, and geometric constructions. Wow. That's a lot. To summarize: it's difficult to imagine any area of math that is more widely used than geometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Geometry channel:
https://www.youtube.com/channel/UCD3OtKxPRUFw8kzYlhJXa1Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
