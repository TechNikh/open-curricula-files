---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dissatisfaction
offline_file: ""
offline_thumbnail: ""
uuid: 57e3aed0-296b-46a7-9b2b-fbd20a3c8915
updated: 1484310484
title: dissatisfaction
categories:
    - Dictionary
---
dissatisfaction
     n : the feeling of being displeased and discontent; "he was
         never slow to express his dissatisfaction with the
         service he received" [ant: {satisfaction}]
