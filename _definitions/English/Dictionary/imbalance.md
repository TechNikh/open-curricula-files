---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imbalance
offline_file: ""
offline_thumbnail: ""
uuid: 030a5119-283e-4411-852a-67d33680be2a
updated: 1484310240
title: imbalance
categories:
    - Dictionary
---
imbalance
     n 1: a state of disequilibrium (as may occur in cases of inner
          ear disease) [syn: {instability}, {unbalance}] [ant: {balance}]
     2: (mathematics) a lack of symmetry [syn: {asymmetry}] [ant: {symmetry}]
