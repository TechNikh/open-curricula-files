---
version: 1
type: video
provider: YouTube
id: qOwCpnQsDLM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Stars%2C%20black%20holes%20and%20galaxies/Supernova%20%28supernovae%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 89edc334-0761-4aec-8312-1d3e5e645534
updated: 1486069688
title: Supernova (supernovae)
categories:
    - Stars, black holes and galaxies
thumbnail_urls:
    - https://i3.ytimg.com/vi/qOwCpnQsDLM/default.jpg
    - https://i3.ytimg.com/vi/qOwCpnQsDLM/1.jpg
    - https://i3.ytimg.com/vi/qOwCpnQsDLM/2.jpg
    - https://i3.ytimg.com/vi/qOwCpnQsDLM/3.jpg
---
