---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jaw
offline_file: ""
offline_thumbnail: ""
uuid: 5a759d25-c508-413b-9377-3b80b8f8cce4
updated: 1484310344
title: jaw
categories:
    - Dictionary
---
jaw
     n 1: the part of the skull of a vertebrate that frames the mouth
          and holds the teeth
     2: the bones of the skull that frame the mouth and serve to
        open it; the bones that hold the teeth
     3: holding device consisting of one or both of the opposing
        parts of a tool that close to hold an object [syn: {jaws}]
     v 1: talk socially without exchanging too much information; "the
          men were sitting in the cafe and shooting the breeze"
          [syn: {chew the fat}, {shoot the breeze}, {chat}, {confabulate},
           {confab}, {chitchat}, {chatter}, {chaffer}, {natter}, {gossip},
           {claver}, {visit}]
     2: talk incessantly and ...
