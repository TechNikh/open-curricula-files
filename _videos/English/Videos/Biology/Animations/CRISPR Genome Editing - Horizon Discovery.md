---
version: 1
type: video
provider: YouTube
id: 2pm9u1E8k3s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/CRISPR%20Genome%20Editing%20-%20Horizon%20Discovery.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 954d416c-0bc4-4d9f-9c8c-088da21e7a22
updated: 1486069673
title: 'CRISPR Genome Editing - Horizon Discovery'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/2pm9u1E8k3s/default.jpg
    - https://i3.ytimg.com/vi/2pm9u1E8k3s/1.jpg
    - https://i3.ytimg.com/vi/2pm9u1E8k3s/2.jpg
    - https://i3.ytimg.com/vi/2pm9u1E8k3s/3.jpg
---
