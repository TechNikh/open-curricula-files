---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accord
offline_file: ""
offline_thumbnail: ""
uuid: 3bbe1ad9-3c05-4bf6-ba3b-d56219108051
updated: 1484310448
title: accord
categories:
    - Dictionary
---
accord
     n 1: harmony of people's opinions or actions or characters; "the
          two parties were in agreement" [syn: {agreement}] [ant:
          {disagreement}]
     2: concurrence of opinion; "we are in accord with your
        proposal" [syn: {conformity}, {accordance}]
     3: a written agreement between two states or sovereigns [syn: {treaty},
         {pact}]
     4: sympathetic compatibility [syn: {rapport}]
     v 1: go together; "The colors don't harmonize"; "Their ideas
          concorded" [syn: {harmonize}, {harmonise}, {consort}, {concord},
           {fit in}, {agree}]
     2: allow to have; "grant a privilege" [syn: {allot}, {grant}]
