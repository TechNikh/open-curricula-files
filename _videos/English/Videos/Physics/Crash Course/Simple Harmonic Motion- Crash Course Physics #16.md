---
version: 1
type: video
provider: YouTube
id: jxstE6A_CYQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e7eb02d9-fd99-482f-b11c-c7902f540965
updated: 1486069685
title: 'Simple Harmonic Motion- Crash Course Physics #16'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/jxstE6A_CYQ/default.jpg
    - https://i3.ytimg.com/vi/jxstE6A_CYQ/1.jpg
    - https://i3.ytimg.com/vi/jxstE6A_CYQ/2.jpg
    - https://i3.ytimg.com/vi/jxstE6A_CYQ/3.jpg
---
