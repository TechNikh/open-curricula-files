---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incorporate
offline_file: ""
offline_thumbnail: ""
uuid: 8fa683cb-178f-40b6-87e0-9a75c53813da
updated: 1484310595
title: incorporate
categories:
    - Dictionary
---
incorporate
     adj : formed or united into a whole [syn: {incorporated}, {integrated},
            {merged}, {unified}]
     v 1: make into a whole or make part of a whole; "She incorporated
          his suggestions into her proposal" [syn: {integrate}]
          [ant: {disintegrate}]
     2: include or contain; have as a component; "A totally new idea
        is comprised in this paper"; "The record contains many old
        songs from the 1930's" [syn: {contain}, {comprise}]
     3: form a corporation
     4: unite or merge with something already in existence;
        "incorporate this document with those pertaining to the
        same case"
