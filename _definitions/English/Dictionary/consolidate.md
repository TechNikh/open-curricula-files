---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consolidate
offline_file: ""
offline_thumbnail: ""
uuid: 44b8e296-55b0-4ce3-b133-9c7dc25ace0a
updated: 1484310555
title: consolidate
categories:
    - Dictionary
---
consolidate
     v 1: unite into one; "The companies consolidated"
     2: make firm or secure; strengthen; "consolidate one's gains";
        "consolidate one's hold on first place"
     3: bring together into a single whole or system; "The town and
        county schools are being consolidated"
     4: form into a solid mass or whole; "The mud had consolidated
        overnight"
     5: make or form into a solid or hardened mass; "consolidate
        fibers into boards"
