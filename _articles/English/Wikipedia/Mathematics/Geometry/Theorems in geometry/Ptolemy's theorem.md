---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ptolemy%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: bcd45a57-434a-4d23-99a0-7eff23b93e65
updated: 1484309123
title: "Ptolemy's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ptolemy_Theorem.svg.png
tags:
    - Examples
    - Equilateral triangle
    - Square
    - Rectangle
    - Pentagon
    - Side of decagon
    - Proofs
    - Proof by similarity of triangles
    - Proof by trigonometric identities
    - Corollaries
    - "Corollary 1. Pythagoras' theorem"
    - Corollary 2. The law of cosines
    - 'Corollary 3: Compound angle sine (+)'
    - 'Corollary 4: Compound angle sine (−)'
    - 'Corollary 5: Compound angle cosine (+)'
    - "Ptolemy's inequality"
    - Notes
categories:
    - Theorems in geometry
---
In Euclidean geometry, Ptolemy's theorem is a relation between the four sides and two diagonals of a cyclic quadrilateral (a quadrilateral whose vertices lie on a common circle). The theorem is named after the Greek astronomer and mathematician Ptolemy (Claudius Ptolemaeus).[1] Ptolemy used the theorem as an aid to creating his table of chords, a trigonometric table that he applied to astronomy.
