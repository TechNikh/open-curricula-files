---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intrepid
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414941
title: intrepid
categories:
    - Dictionary
---
intrepid
     adj : invulnerable to fear or intimidation; "audacious explorers";
           "fearless reporters and photographers"; "intrepid
           pioneers" [syn: {audacious}, {brave}, {dauntless}, {fearless},
            {unfearing}]
