---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Corpus_luteum
offline_file: ""
offline_thumbnail: ""
uuid: ef480ddc-bd07-49e2-b196-40dfa75a0315
updated: 1484308368
title: Corpus luteum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Gray1163.png
tags:
    - Development and structure
    - Function
    - When egg is not fertilized
    - When egg is fertilized
    - Content of carotenoids
    - Additional images
    - Pathology
categories:
    - Uncategorized
---
The corpus luteum (Latin for "yellow body"; plural corpora lutea) is a temporary endocrine structure in female ovaries that is involved in the production of relatively high levels of progesterone and moderate levels of estradiol and inhibin A. It is colored as a result of concentrating carotenoids (including lutein) from the diet and secretes a moderate amount of estrogen to inhibit further release of gonadotropin-releasing hormone (GnRH) and thus secretion of luteinizing hormone (LH) and follicle-stimulating hormone (FSH). A new corpus luteum develops with each menstrual cycle.
