---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vlasov_equation
offline_file: ""
offline_thumbnail: ""
uuid: f6666a44-7219-4e22-a178-c24f6923a3cb
updated: 1484308802
title: Vlasov equation
tags:
    - Difficulties of the standard kinetic approach
    - The Vlasov–Maxwell system of equations (gaussian units)
    - The Vlasov–Poisson equation
    - Moment equations
    - Continuity equation
    - Momentum equation
    - The frozen-in approximation
categories:
    - Non-equilibrium thermodynamics
---
The Vlasov equation is a differential equation describing time evolution of the distribution function of plasma consisting of charged particles with long-range (for example, Coulomb) interaction. The equation was first suggested for description of plasma by Anatoly Vlasov in 1938[1] (see also [2]) and later discussed by him in detail in a monograph.[3]
