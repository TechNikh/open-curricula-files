---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Trembling_hand_perfect_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: bc766cb3-0f73-423d-98d5-980374042805
updated: 1484309038
title: Trembling hand perfect equilibrium
tags:
    - Definition
    - Example
    - Trembling hand perfect equilibria of two-player games
    - Trembling hand perfect equilibria of extensive form games
categories:
    - Game theory equilibrium concepts
---
In game theory, trembling hand perfect equilibrium is a refinement of Nash equilibrium due to Reinhard Selten.[1] A trembling hand perfect equilibrium is an equilibrium that takes the possibility of off-the-equilibrium play into account by assuming that the players, through a "slip of the hand" or tremble, may choose unintended strategies, albeit with negligible probability.
