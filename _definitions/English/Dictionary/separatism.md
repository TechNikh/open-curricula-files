---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/separatism
offline_file: ""
offline_thumbnail: ""
uuid: 70114509-ade9-44d6-8164-aba3d54996d2
updated: 1484310188
title: separatism
categories:
    - Dictionary
---
separatism
     n 1: a social system that provides separate facilities for
          minority groups [syn: {segregation}]
     2: a disposition toward schism and secession from a larger
        group; the principles and practices of separatists;
        "separatism is a serious problem in Quebec"; "demands for
        some form of separatism on grounds of religion have been
        perceived as a threat to mainstream education"
