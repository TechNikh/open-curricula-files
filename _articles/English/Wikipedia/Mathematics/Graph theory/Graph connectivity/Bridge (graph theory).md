---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bridge_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: ac58d015-5219-4cb2-b221-6c5bf3f7710d
updated: 1484309398
title: Bridge (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Graph_cut_edges.svg.png
tags:
    - Trees and forests
    - Relation to vertex connectivity
    - Bridgeless graphs
    - "Tarjan's Bridge-finding algorithm"
    - Bridge-Finding with Chain Decompositions
    - Notes
categories:
    - Graph connectivity
---
In graph theory, a bridge, isthmus, cut-edge, or cut arc is an edge of a graph whose deletion increases its number of connected components.[1] Equivalently, an edge is a bridge if and only if it is not contained in any cycle. A graph is said to be bridgeless or isthmus-free if it contains no bridges.
