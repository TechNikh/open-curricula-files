---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glasses
offline_file: ""
offline_thumbnail: ""
uuid: 267786cf-97df-40f7-97b3-c1bce178e689
updated: 1484310216
title: glasses
categories:
    - Dictionary
---
glasses
     n : optical instrument consisting of a pair of lenses for
         correcting defective vision [syn: {spectacles}, {specs},
         {eyeglasses}]
