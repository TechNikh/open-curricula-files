---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minkowski_content
offline_file: ""
offline_thumbnail: ""
uuid: 2e89cc20-9b24-4a43-a1c5-424fb7959351
updated: 1484309162
title: Minkowski content
tags:
    - Definition
    - Properties
    - Footnotes
categories:
    - Analytic geometry
---
The Minkowski content (named after Hermann Minkowski), or the boundary measure, of a set is a basic concept that uses concepts from geometry and measure theory to generalize the notions of length of a smooth curve in the plane, and area of a smooth surface in the space, to arbitrary measurable sets.
