---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invasion
offline_file: ""
offline_thumbnail: ""
uuid: e0614128-ae29-4f21-b6f3-b598e8905e73
updated: 1484310607
title: invasion
categories:
    - Dictionary
---
invasion
     n 1: the act of invading; the act of an army that invades for
          conquest or plunder
     2: any entry into an area not previously occupied; "an invasion
        of tourists"; "an invasion of locusts" [syn: {encroachment},
         {intrusion}]
     3: (pathology) the spread of pathogenic microorganisms or
        malignant cells to new sites in the body; "the tumor's
        invasion of surrounding structures"
