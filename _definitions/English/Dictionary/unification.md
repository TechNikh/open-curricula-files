---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unification
offline_file: ""
offline_thumbnail: ""
uuid: 19d95a8e-00ea-4e06-ae1f-543bb59bca12
updated: 1484310551
title: unification
categories:
    - Dictionary
---
unification
     n 1: an occurrence that involves the production of a union [syn:
          {fusion}, {merger}]
     2: the state of being joined or united or linked; "there is
        strength in union" [syn: {union}] [ant: {separation}]
     3: the act of making or becoming a single unit; "the union of
        opposing factions"; "he looked forward to the unification
        of his family for the holidays" [syn: {union}, {uniting},
        {conjugation}, {jointure}] [ant: {disunion}]
