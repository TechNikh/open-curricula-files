---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Faber%E2%80%93Lov%C3%A1sz_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 93ef11b2-ab21-4def-8a0a-3a88d021361b
updated: 1484309379
title: Erdős–Faber–Lovász conjecture
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Erd%25C5%2591s%25E2%2580%2593Faber%25E2%2580%2593Lov%25C3%25A1sz_conjecture.svg.png'
tags:
    - Equivalent formulations
    - History and partial results
    - Related problems
    - Notes
categories:
    - Graph coloring
---
In graph theory, the Erdős–Faber–Lovász conjecture is an unsolved problem about graph coloring, named after Paul Erdős, Vance Faber, and László Lovász, who formulated it in 1972.[1] It says:
