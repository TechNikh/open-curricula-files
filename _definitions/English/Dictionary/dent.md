---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dent
offline_file: ""
offline_thumbnail: ""
uuid: a8460668-07c5-41b6-9ef8-246f467ca764
updated: 1484310181
title: dent
categories:
    - Dictionary
---
dent
     n 1: an appreciable consequence (especially a lessening); "it
          made a dent in my bank account"
     2: a depression scratched or carved into a surface [syn: {incision},
         {scratch}, {prick}, {slit}]
     3: an impression in a surface (as made by a blow) [syn: {gouge},
         {nick}]
     v : make a depression into; "The bicycle dented my car" [syn: {indent}]
