---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solemnly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484449501
title: solemnly
categories:
    - Dictionary
---
solemnly
     adv : in a grave and sedate manner; "the judge sat there solemnly"
