---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometric_quotient
offline_file: ""
offline_thumbnail: ""
uuid: faada8c7-1a3f-4dc5-983d-7659a66b8568
updated: 1484309149
title: Geometric quotient
categories:
    - Algebraic geometry
---
In algebraic geometry, a geometric quotient of an algebraic variety X with the action of an algebraic group G is a morphism of varieties 
  
    
      
        π
        :
        X
        →
        Y
      
    
    {\displaystyle \pi :X\to Y}
  
 such that[1]
