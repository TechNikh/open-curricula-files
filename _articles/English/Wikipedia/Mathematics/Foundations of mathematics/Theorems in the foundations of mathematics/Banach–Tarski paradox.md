---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Banach%E2%80%93Tarski_paradox'
offline_file: ""
offline_thumbnail: ""
uuid: b36ef153-bf99-47c7-a4b3-f9ba0562cef2
updated: 1484308991
title: Banach–Tarski paradox
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Banach-Tarski_Paradox.svg.png
tags:
    - Banach and Tarski publication
    - Formal treatment
    - >
        Connection with earlier work and the role of the axiom of
        choice
    - A sketch of the proof
    - Step 1
    - Step 2
    - Step 3
    - Step 4
    - Some details, fleshed out
    - Obtaining infinitely many balls from one
    - Von Neumann paradox in the Euclidean plane
    - Recent progress
    - Notes
categories:
    - Theorems in the foundations of mathematics
---
The Banach–Tarski paradox is a theorem in set-theoretic geometry, which states the following: Given a solid ball in 3‑dimensional space, there exists a decomposition of the ball into a finite number of disjoint subsets, which can then be put back together in a different way to yield two identical copies of the original ball. Indeed, the reassembly process involves only moving the pieces around and rotating them, without changing their shape. However, the pieces themselves are not "solids" in the usual sense, but infinite scatterings of points. The reconstruction can work with as few as five pieces.[1]
