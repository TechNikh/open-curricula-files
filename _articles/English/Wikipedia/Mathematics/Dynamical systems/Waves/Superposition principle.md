---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Superposition_principle
offline_file: ""
offline_thumbnail: ""
uuid: 7f8945d5-40fe-4fb4-b345-51d008b4a71b
updated: 1484308900
title: Superposition principle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Anas_platyrhynchos_with_ducklings_reflecting_water.jpg
tags:
    - Relation to Fourier analysis and similar methods
    - Wave superposition
    - Wave diffraction vs. wave interference
    - Wave interference
    - Departures from linearity
    - Quantum superposition
    - Boundary value problems
    - Additive state decomposition
    - Other example applications
    - History
categories:
    - Waves
---
In physics and systems theory, the superposition principle,[1] also known as superposition property, states that, for all linear systems, the net response at a given place and time caused by two or more stimuli is the sum of the responses which would have been caused by each stimulus individually. So that if input A produces response X and input B produces response Y then input (A + B) produces response (X + Y).
