---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bluish
offline_file: ""
offline_thumbnail: ""
uuid: dae35b85-48db-4cca-83b1-fe90c943a789
updated: 1484310424
title: bluish
categories:
    - Dictionary
---
bluish
     adj : having a color similar to that of a clear unclouded sky;
           "October's bright blue weather"- Helen Hunt Jackson; "a
           blue flame"; "blue haze of tobacco smoke" [syn: {blue},
            {blueish}, {light-blue}, {dark-blue}, {blue-black}]
