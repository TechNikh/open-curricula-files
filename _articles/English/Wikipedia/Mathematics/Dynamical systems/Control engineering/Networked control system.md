---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Networked_control_system
offline_file: ""
offline_thumbnail: ""
uuid: 230d068f-0cf1-4fbe-a01f-528b1077bc2a
updated: 1484308732
title: Networked control system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-ISpace.jpg
tags:
    - Overview
    - Types of communication networks
    - Problems and solutions
categories:
    - Control engineering
---
A Networked Control System (NCS) is a control system wherein the control loops are closed through a communication network. The defining feature of an NCS is that control and feedback signals are exchanged among the system's components in the form of information packages through a network.
