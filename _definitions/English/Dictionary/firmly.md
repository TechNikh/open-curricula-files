---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/firmly
offline_file: ""
offline_thumbnail: ""
uuid: 7f9a51e8-be4b-4124-8a5a-6ef972740ffb
updated: 1484310593
title: firmly
categories:
    - Dictionary
---
firmly
     adv 1: with resolute determination; "we firmly believed it"; "you
            must stand firm" [syn: {firm}, {steadfastly}, {unwaveringly}]
     2: in a secure manner; in a manner free from danger; "she held
        the child securely" [syn: {securely}]
     3: with firmness; "held hard to the railing" [syn: {hard}]
