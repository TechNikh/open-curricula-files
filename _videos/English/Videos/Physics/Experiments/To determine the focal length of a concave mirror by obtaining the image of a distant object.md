---
version: 1
type: video
provider: YouTube
id: E-zQ51o90tI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7a33c205-0b36-4f51-afab-c6a9ceecd93e
updated: 1486069637
title: >
    To determine the focal length of a concave mirror by
    obtaining the image of a distant object
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/E-zQ51o90tI/default.jpg
    - https://i3.ytimg.com/vi/E-zQ51o90tI/1.jpg
    - https://i3.ytimg.com/vi/E-zQ51o90tI/2.jpg
    - https://i3.ytimg.com/vi/E-zQ51o90tI/3.jpg
---
