---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/closest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484608561
title: closest
categories:
    - Dictionary
---
closest
     adv : (superlative of `near' or `close') within the shortest
           distance; "that was the time he came nearest to death"
           [syn: {nearest}, {nighest}]
