---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kuratowski%27s_free_set_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 1d2cf247-3a94-4650-b2c7-78ac70643eb5
updated: 1484309507
title: "Kuratowski's free set theorem"
categories:
    - Set theory
---
Kuratowski's free set theorem, named after Kazimierz Kuratowski, is a result of set theory, an area of mathematics. It is a result which has been largely forgotten for almost 50 years, but has been applied recently in solving several lattice theory problems, such as the congruence lattice problem.
