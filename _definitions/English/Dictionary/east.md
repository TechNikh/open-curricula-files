---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/east
offline_file: ""
offline_thumbnail: ""
uuid: 7a181b77-5351-4156-8642-648d9548419a
updated: 1484310279
title: east
categories:
    - Dictionary
---
east
     adj : situated in or facing or moving toward the east [ant: {west}]
     n 1: the cardinal compass point that is at 90 degrees [syn: {due
          east}, {E}]
     2: the countries of Asia [syn: {Orient}]
     3: the region of the United States lying north of the Ohio
        River and east of the Mississippi River [syn: {eastern
        United States}]
     adv : to, toward, or in the east; "we travelled east for several
           miles"
