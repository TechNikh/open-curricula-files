---
version: 1
type: video
provider: YouTube
id: RuEJzPQ_N10
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%236b.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f3a244ec-575c-4b42-bb43-0be745f2a3e9
updated: 1486069692
title: '2011 Calculus AB Free Response #6b'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/RuEJzPQ_N10/default.jpg
    - https://i3.ytimg.com/vi/RuEJzPQ_N10/1.jpg
    - https://i3.ytimg.com/vi/RuEJzPQ_N10/2.jpg
    - https://i3.ytimg.com/vi/RuEJzPQ_N10/3.jpg
---
