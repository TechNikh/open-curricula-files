---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Discrete_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 07f36ef4-6704-4cbb-97e4-243d31043f9a
updated: 1484308588
title: Discrete geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Unit_disk_graph.svg.png
tags:
    - History
    - Topics in discrete geometry
    - Polyhedra and polytopes
    - Packings, coverings and tilings
    - Structural rigidity and flexibility
    - Incidence structures
    - Oriented matroids
    - Geometric graph theory
    - Simplicial complexes
    - Topological combinatorics
    - Lattices and discrete groups
    - Digital geometry
    - Discrete differential geometry
    - Notes
categories:
    - Discrete geometry
---
Discrete geometry and combinatorial geometry are branches of geometry that study combinatorial properties and constructive methods of discrete geometric objects. Most questions in discrete geometry involve finite or discrete sets of basic geometric objects, such as points, lines, planes, circles, spheres, polygons, and so forth. The subject focuses on the combinatorial properties of these objects, such as how they intersect one another, or how they may be arranged to cover a larger object.
