---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/afford
offline_file: ""
offline_thumbnail: ""
uuid: cfadc82b-0627-43df-9e28-da3d9c6893d9
updated: 1484310240
title: afford
categories:
    - Dictionary
---
afford
     v 1: be able to spare or give up; "I can't afford to spend two
          hours with this person"
     2: be the cause or source of; "He gave me a lot of trouble";
        "Our meeting afforded much interesting information" [syn:
        {yield}, {give}]
     3: have the financial means to do something or buy something;
        "We can't afford to send our children to college"; "Can
        you afford this car?"
     4: afford access to; "the door opens to the patio"; "The French
        doors give onto a terrace" [syn: {open}, {give}]
