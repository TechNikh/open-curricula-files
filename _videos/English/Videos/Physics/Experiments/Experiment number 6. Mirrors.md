---
version: 1
type: video
provider: YouTube
id: H0ZoBmXdi_0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e3985cea-f3f9-4806-b082-741575f9a07c
updated: 1486069637
title: Experiment number 6. Mirrors
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/H0ZoBmXdi_0/default.jpg
    - https://i3.ytimg.com/vi/H0ZoBmXdi_0/1.jpg
    - https://i3.ytimg.com/vi/H0ZoBmXdi_0/2.jpg
    - https://i3.ytimg.com/vi/H0ZoBmXdi_0/3.jpg
---
