---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tragedy
offline_file: ""
offline_thumbnail: ""
uuid: bb59d031-c026-450f-946c-b6186ed7ef75
updated: 1484310186
title: tragedy
categories:
    - Dictionary
---
tragedy
     n 1: an event resulting in great loss and misfortune; "the whole
          city was affected by the irremediable calamity"; "the
          earthquake was a disaster" [syn: {calamity}, {catastrophe},
           {disaster}, {cataclysm}]
     2: drama in which the protagonist is overcome by some superior
        force or circumstance; excites terror or pity [ant: {comedy}]
