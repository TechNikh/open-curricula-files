---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/isolated
offline_file: ""
offline_thumbnail: ""
uuid: a7f5dcd8-838b-47c5-9449-7926bd2d4928
updated: 1484310210
title: isolated
categories:
    - Dictionary
---
isolated
     adj 1: not close together in time; "isolated instances of
            rebellion"; "scattered fire"; "a stray bullet grazed
            his thigh" [syn: {scattered}, {stray}]
     2: being or feeling set or kept apart from others; "she felt
        detached from the group"; "could not remain the isolated
        figure he had been"- Sherwood Anderson; "thought of
        herself as alone and separated from the others"; "had a
        set-apart feeling" [syn: {detached}, {separated}, {set-apart}]
     3: marked by separation of or from usually contiguous elements;
        "little isolated worlds, as abruptly disjunct and
        unexpected as a palm-shaded well in the Sahara"-
  ...
