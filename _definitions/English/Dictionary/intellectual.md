---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intellectual
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484433901
title: intellectual
categories:
    - Dictionary
---
intellectual
     adj 1: of or relating to the intellect; "his intellectual career"
     2: of or associated with or requiring the use of the mind;
        "intellectual problems"; "the triumph of the rational over
        the animal side of man" [syn: {rational}, {noetic}]
     3: appealing to or using the intellect; "satire is an
        intellectual weapon"; "intellectual workers engaged in
        creative literary or artistic or scientific labor"; "has
        tremendous intellectual sympathy for oppressed people";
        "coldly intellectual"; "sort of the intellectual type";
        "intellectual literature" [ant: {nonintellectual}]
     4: involving intelligence rather than ...
