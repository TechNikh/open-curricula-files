---
version: 1
type: video
provider: YouTube
id: GcjgWov7mTM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Introduction%20to%20Evolution%20and%20Natural%20Selection.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e5cf9d4e-1da9-4186-8883-a5cbfa7dce66
updated: 1486069581
title: Introduction to Evolution and Natural Selection
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/GcjgWov7mTM/default.jpg
    - https://i3.ytimg.com/vi/GcjgWov7mTM/1.jpg
    - https://i3.ytimg.com/vi/GcjgWov7mTM/2.jpg
    - https://i3.ytimg.com/vi/GcjgWov7mTM/3.jpg
---
Introduction to evolution, variation in a population and natural selection
More free lessons at: http://www.khanacademy.org/video?v=GcjgWov7mTM
