---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cripple
offline_file: ""
offline_thumbnail: ""
uuid: 46d802ea-d43e-4466-ac10-6e5402211a7c
updated: 1484310553
title: cripple
categories:
    - Dictionary
---
cripple
     n : someone whose legs are disabled
     v 1: deprive of strength or efficiency; make useless or
          worthless; "This measure crippled our efforts"; "Their
          behavior stultified the boss's hard work" [syn: {stultify}]
     2: deprive of the use of a limb, especially a leg; "The
        accident has crippled her for life" [syn: {lame}]
