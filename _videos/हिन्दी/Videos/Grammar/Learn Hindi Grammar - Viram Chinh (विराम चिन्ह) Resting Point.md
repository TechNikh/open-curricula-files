---
version: 1
type: video
provider: YouTube
id: K_Dx_p_CEQc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3bb34818-9a98-4460-ad0a-c0e45ff0e362
updated: 1486069629
title: 'Learn Hindi Grammar - Viram Chinh (विराम चिन्ह) Resting Point'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/K_Dx_p_CEQc/default.jpg
    - https://i3.ytimg.com/vi/K_Dx_p_CEQc/1.jpg
    - https://i3.ytimg.com/vi/K_Dx_p_CEQc/2.jpg
    - https://i3.ytimg.com/vi/K_Dx_p_CEQc/3.jpg
---
