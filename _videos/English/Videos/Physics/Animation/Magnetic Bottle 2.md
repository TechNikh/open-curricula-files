---
version: 1
type: video
provider: YouTube
id: Tvy7tHrbNpQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Magnetic%20Bottle%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6bb6f847-bd38-45be-b77a-81e84daa9c98
updated: 1486069651
title: Magnetic Bottle 2
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Tvy7tHrbNpQ/default.jpg
    - https://i3.ytimg.com/vi/Tvy7tHrbNpQ/1.jpg
    - https://i3.ytimg.com/vi/Tvy7tHrbNpQ/2.jpg
    - https://i3.ytimg.com/vi/Tvy7tHrbNpQ/3.jpg
---
