---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sphincter
offline_file: ""
offline_thumbnail: ""
uuid: cc880545-ebfa-4615-ad03-054171559ae1
updated: 1484310333
title: sphincter
categories:
    - Dictionary
---
sphincter
     n : a ring of muscle that contracts to close an opening [syn: {anatomical
         sphincter}, {sphincter muscle}]
