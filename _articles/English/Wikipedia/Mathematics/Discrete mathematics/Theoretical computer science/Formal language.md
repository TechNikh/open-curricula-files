---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Formal_language
offline_file: ""
offline_thumbnail: ""
uuid: afdb38d8-1f2f-4ea2-a657-aa3bb9d5625e
updated: 1484308675
title: Formal language
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Syntax_tree.svg.png
tags:
    - History
    - Words over an alphabet
    - Definition
    - Examples
    - Constructions
    - Language-specification formalisms
    - Operations on languages
    - Applications
    - Programming languages
    - Formal theories, systems and proofs
    - Interpretations and models
    - Citation footnotes
    - General references
categories:
    - Theoretical computer science
---
In mathematics, computer science, and linguistics, a formal language is a set of strings of symbols that may be constrained by rules that are specific to it.
