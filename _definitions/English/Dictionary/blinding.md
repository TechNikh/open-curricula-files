---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blinding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484476981
title: blinding
categories:
    - Dictionary
---
blinding
     adj : shining intensely; "the blazing sun"; "blinding headlights";
           "dazzling snow"; "fulgent patterns of sunlight"; "the
           glaring sun" [syn: {blazing}, {dazzling}, {fulgent}, {glaring},
            {glary}]
