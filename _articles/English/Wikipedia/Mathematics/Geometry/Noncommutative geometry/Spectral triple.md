---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spectral_triple
offline_file: ""
offline_thumbnail: ""
uuid: 5da8d2fc-575f-4079-a0e8-0a1e212576d8
updated: 1484309290
title: Spectral triple
tags:
    - Motivation
    - Definition
    - Important concepts
    - Metric on the pure state space
    - Pairing with K-theory
    - Notes
categories:
    - Noncommutative geometry
---
In noncommutative geometry and related branches of mathematics and mathematical physics, a spectral triple is a set of data which encodes a geometric phenomenon in an analytic way. The definition typically involves a Hilbert space, an algebra of operators on it and an unbounded self-adjoint operator, endowed with supplemental structures. It was conceived by Alain Connes who was motivated by the Atiyah-Singer index theorem and sought its extension to 'noncommutative' spaces. Some authors refer to this notion as unbounded K-cycles or as unbounded Fredholm modules.
