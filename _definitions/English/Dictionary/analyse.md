---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/analyse
offline_file: ""
offline_thumbnail: ""
uuid: fcd058c2-3632-4835-b1b0-90000820e707
updated: 1484310455
title: analyse
categories:
    - Dictionary
---
analyse
     v 1: consider in detail and subject to an analysis in order to
          discover essential features or meaning; "analyze a
          sonnet by Shakespeare"; "analyze the evidence in a
          criminal trial"; "analyze your real motives" [syn: {analyze},
           {study}, {examine}, {canvass}, {canvas}]
     2: break down into components or essential features; "analyze
        today's financial market" [syn: {analyze}]
     3: make a mathematical, chemical, or grammatical analysis of;
        break down into components or essential features; "analyze
        a specimen"; "analyze a sentence"; "analyze a chemical
        compound" [syn: {analyze}, {break down}, {dissect}, ...
