---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/generalized
offline_file: ""
offline_thumbnail: ""
uuid: 09f4fc36-a15a-4b6a-bfe5-87f2a23e22db
updated: 1484310381
title: generalized
categories:
    - Dictionary
---
generalized
     adj 1: not biologically differentiated or adapted to a specific
            function or environment; "the hedgehog is a primitive
            and generalized mammal" [syn: {generalised}]
     2: made general; widely prevalent; "a problem of generalized
        human needs"; "a state of generalized discontent" [syn: {generalised}]
     3: spread throughout a body or system; "generalized edema"
        [syn: {generalised}]
