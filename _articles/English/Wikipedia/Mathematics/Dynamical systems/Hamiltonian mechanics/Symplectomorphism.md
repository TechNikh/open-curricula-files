---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symplectomorphism
offline_file: ""
offline_thumbnail: ""
uuid: 7f6d27be-2477-4737-98c2-0d8617d3122c
updated: 1484308797
title: Symplectomorphism
tags:
    - Formal definition
    - Flows
    - The group of (Hamiltonian) symplectomorphisms
    - Comparison with Riemannian geometry
    - Quantizations
    - Arnold conjecture
categories:
    - Hamiltonian mechanics
---
In mathematics, a symplectomorphism or symplectic map is an isomorphism in the category of symplectic manifolds. In classical mechanics, a symplectomorphism represents a transformation of phase space that is volume-preserving and preserves the symplectic structure of phase space, and is called a canonical transformation.
