---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prehistory
offline_file: ""
offline_thumbnail: ""
uuid: 305cd0b7-d087-4e0b-ab36-8ac9eb0de4e7
updated: 1484310413
title: prehistory
categories:
    - Dictionary
---
prehistory
     n : the time during the development of human culture before the
         appearance of the written word [syn: {prehistoric culture}]
