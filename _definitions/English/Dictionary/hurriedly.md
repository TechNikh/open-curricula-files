---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hurriedly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484480342
title: hurriedly
categories:
    - Dictionary
---
hurriedly
     adv : in a hurried or hasty manner; "the way they buried him so
           hurriedly was disgraceful"; "hastily, he scanned the
           headlines"; "sold in haste and at a sacrifice" [syn: {hastily},
            {in haste}] [ant: {unhurriedly}]
