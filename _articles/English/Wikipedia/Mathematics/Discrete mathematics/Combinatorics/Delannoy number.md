---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Delannoy_number
offline_file: ""
offline_thumbnail: ""
uuid: b8920f5a-55d8-4b18-8e07-b328f105cdca
updated: 1484308577
title: Delannoy number
tags:
    - Example
    - Delannoy array
    - Central Delannoy numbers
    - Computation
    - Delannoy numbers
    - Central Delannoy numbers
categories:
    - Combinatorics
---
In mathematics, a Delannoy number 
  
    
      
        D
      
    
    {\displaystyle D}
  
 describes the number of paths from the southwest corner (0, 0) of a rectangular grid to the northeast corner (m, n), using only single steps north, northeast, or east. The Delannoy numbers are named after French army officer and amateur mathematician Henri Delannoy.[1]
