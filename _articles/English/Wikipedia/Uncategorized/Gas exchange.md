---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gaseous_exchange
offline_file: ""
offline_thumbnail: ""
uuid: ca5366a4-1e7a-42ad-a03d-fd29fc3a31b5
updated: 1484308372
title: Gas exchange
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Gas_exchange_in_the_aveolus_simple_%2528en%2529.svg.png'
tags:
    - Diffusion
    - In humans
    - Varying response
    - In plants
    - In fish
    - Summary of main systems
    - Other examples
categories:
    - Uncategorized
---
Gas exchange is a biological process through which different gases are transferred in opposite directions across a specialized respiratory surface. Gases are constantly required by, and produced as a by-product of, cellular and metabolic reactions, so an efficient system for their exchange is extremely important. It is linked with respiration in animals, and both respiration and photosynthesis in plants.[1]
