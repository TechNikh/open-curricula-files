---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tribute
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484540461
title: tribute
categories:
    - Dictionary
---
tribute
     n 1: something given or done as an expression of esteem [syn: {testimonial}]
     2: payment by one nation for protection by another
     3: payment extorted by gangsters on threat of violence; "every
        store in the neighborhood had to pay him protection" [syn:
         {protection}]
