---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/register
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484396701
title: register
categories:
    - Dictionary
---
register
     n 1: an official written record of names or events or
          transactions [syn: {registry}]
     2: (music) the timbre that is characteristic of a certain range
        and manner of production of the human voice or of
        different pipe organ stops or of different musical
        instruments
     3: a book in which names and transactions are listed
     4: (computer science) memory device that is the part of
        computer memory that has a specific address and that is
        used to hold information of a specific kind
     5: an air passage (usually in the floor or a wall of a room)
        for admitting or excluding heated air from the room
     6: a regulator (as ...
