---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plasma_stability
offline_file: ""
offline_thumbnail: ""
uuid: ef89bacd-a01e-4bc2-a881-c6f0dd3f6c1b
updated: 1484308840
title: Plasma stability
tags:
    - Plasma instabilities
    - List of plasma instabilities
    - MHD Instabilities
    - Ideal Instabilities
    - Resistive Wall Modes
    - Resistive instabilities
    - Opportunities for Improving MHD Stability
    - Configuration
    - Internal Structure
    - Feedback Control
    - Disruption Mitigation
categories:
    - Stability theory
---
An important field of plasma physics is the stability of the plasma. It usually only makes sense to analyze the stability of a plasma once it has been established that the plasma is in equilibrium. "Equilibrium" asks whether there are not forces that will accelerate any part of the plasma. If there are not, then "stability" asks whether a small perturbation will grow, oscillate, or be damped out.
