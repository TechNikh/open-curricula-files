---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/idiomatic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484467921
title: idiomatic
categories:
    - Dictionary
---
idiomatic
     adj : of or relating to or conforming to idiom; "idiomatic
           English" [syn: {idiomatical}]
