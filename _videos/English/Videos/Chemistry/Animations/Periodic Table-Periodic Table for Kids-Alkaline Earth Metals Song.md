---
version: 1
type: video
provider: YouTube
id: k8g2hN55gVo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cef1076c-9ffc-481c-89b5-5a935b10335a
updated: 1486069662
title: >
    Periodic Table/Periodic Table for Kids/Alkaline Earth Metals
    Song
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/k8g2hN55gVo/default.jpg
    - https://i3.ytimg.com/vi/k8g2hN55gVo/1.jpg
    - https://i3.ytimg.com/vi/k8g2hN55gVo/2.jpg
    - https://i3.ytimg.com/vi/k8g2hN55gVo/3.jpg
---
