---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flow_graph_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 9763b708-344b-41d8-9a1e-b008b40129ef
updated: 1484308735
title: Flow graph (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Signal_flow_graph_example.png
tags:
    - Deriving a flow graph from equations
categories:
    - Control engineering
---
Although this definition uses the terms "signal flow graph" and "flow graph" interchangeably, the term "signal flow graph" is most often used to designate the Mason signal-flow graph, Mason being the originator of this terminology in his work on electrical networks.[3][4] Likewise, some authors use the term "flow graph" to refer strictly to the Coates flow graph.[5][6] According to Henley & Williams:[7]
