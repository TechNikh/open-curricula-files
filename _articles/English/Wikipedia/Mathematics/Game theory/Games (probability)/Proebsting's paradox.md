---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Proebsting%27s_paradox'
offline_file: ""
offline_thumbnail: ""
uuid: 9cb8413a-40b1-436b-b86b-402591ea7aa1
updated: 1484309045
title: "Proebsting's paradox"
tags:
    - Statement of the paradox
    - Practical application
    - Resolution
categories:
    - Games (probability)
---
In probability theory, Proebsting's paradox is an argument that appears to show that the Kelly criterion can lead to ruin. Although it can be resolved mathematically, it raises some interesting issues about the practical application of Kelly, especially in investing. It was named and first discussed by Edward O. Thorp in 2008.[1] The paradox was named for Todd Proebsting, its creator.
