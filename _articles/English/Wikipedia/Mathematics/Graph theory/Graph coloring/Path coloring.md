---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Path_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 39de1810-a2ae-4020-91e1-a2f6270393bb
updated: 1484309386
title: Path coloring
categories:
    - Graph coloring
---
In both the above problems, the goal is usually to minimise the number of colors used in the coloring. In different variants of path coloring, 
  
    
      
        G
      
    
    {\displaystyle G}
  
 may be a simple graph, digraph or multigraph.
