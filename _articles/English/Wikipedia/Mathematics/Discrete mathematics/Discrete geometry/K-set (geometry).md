---
version: 1
type: article
id: https://en.wikipedia.org/wiki/K-set_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 8d1bb91d-2bae-41ce-b0da-cbe057543f79
updated: 1484308608
title: K-set (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-K-sets.svg.png
tags:
    - Combinatorial bounds
    - Construction algorithms
    - Matroid generalizations
    - Notes
categories:
    - Discrete geometry
---
In discrete geometry, a k-set of a finite point set S in the Euclidean plane is a subset of k elements of S that can be strictly separated from the remaining points by a line. More generally, in Euclidean space of higher dimensions, a k-set of a finite point set is a subset of k elements that can be separated from the remaining points by a hyperplane. In particular, when k = n/2 (where n is the size of S), the line or hyperplane that separates a k-set from the rest of S is a halving line or halving plane.
