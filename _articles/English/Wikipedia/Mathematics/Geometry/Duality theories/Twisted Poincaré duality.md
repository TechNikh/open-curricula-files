---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Twisted_Poincar%C3%A9_duality'
offline_file: ""
offline_thumbnail: ""
uuid: fa3606f4-5afa-4e25-a148-07fb47189d98
updated: 1484309242
title: Twisted Poincaré duality
categories:
    - Duality theories
---
In mathematics, the twisted Poincaré duality is a theorem removing the restriction on Poincaré duality to oriented manifolds. The existence of a global orientation is replaced by carrying along local information, by means of a local coefficient system.
