---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plump
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484445061
title: plump
categories:
    - Dictionary
---
plump
     adj : euphemisms for slightly fat; "a generation ago...buxom
           actresses were popular"- Robt.A.Hamilton; "chubby
           babies"; "pleasingly plump" [syn: {buxom}, {chubby}, {embonpoint},
            {zaftig}, {zoftig}]
     n : the sound of a sudden heavy fall
     adv : straight down especially heavily or abruptly; "the anchor
           fell plump into the sea"; "we dropped the rock plump
           into the water"
     v 1: drop sharply; "The stock market plummeted" [syn: {plummet}]
     2: set (something or oneself) down with or as if with a noise;
        "He planked the money on the table"; "He planked himself
        into the sofa" [syn: {plank}, {flump}, ...
