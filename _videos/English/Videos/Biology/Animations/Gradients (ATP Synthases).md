---
version: 1
type: video
provider: YouTube
id: 3y1dO4nNaKY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Gradients%20%28ATP%20Synthases%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 133ba9fa-bfb2-4635-9178-6214ffbcaa02
updated: 1486069669
title: Gradients (ATP Synthases)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/3y1dO4nNaKY/default.jpg
    - https://i3.ytimg.com/vi/3y1dO4nNaKY/1.jpg
    - https://i3.ytimg.com/vi/3y1dO4nNaKY/2.jpg
    - https://i3.ytimg.com/vi/3y1dO4nNaKY/3.jpg
---
