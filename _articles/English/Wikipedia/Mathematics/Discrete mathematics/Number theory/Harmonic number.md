---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Harmonic_number
offline_file: ""
offline_thumbnail: ""
uuid: 21edb0b3-4c55-4dd8-94fa-b15cd947dd73
updated: 1484308645
title: Harmonic number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-HarmonicNumbers.svg.png
tags:
    - Identities involving harmonic numbers
    - Identities involving π
    - Calculation
    - Special values for fractional arguments
    - Asymptotic formulation
    - Generating functions
    - Applications
    - Generalization
    - Generalized harmonic numbers
    - Multiplication formulas
    - Generalization to the complex plane
    - Relation to the Riemann zeta function
    - Hyperharmonic numbers
    - Notes
categories:
    - Number theory
---
Harmonic numbers are related to the harmonic mean in that the n-th harmonic number is also n times the reciprocal of the harmonic mean of the first n positive integers.
