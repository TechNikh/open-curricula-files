---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/particular
offline_file: ""
offline_thumbnail: ""
uuid: 4f818225-cd60-455c-98ed-eb4637a5818f
updated: 1484310355
title: particular
categories:
    - Dictionary
---
particular
     adj 1: unique or specific to a person or thing or category; "the
            particular demands of the job"; "has a paraticular
            preference for Chinese art"; "a peculiar bond of
            sympathy between them"; "an expression peculiar to
            Canadians"; "rights peculiar to the rich"; "the
            special features of a computer"; "my own special
            chair" [syn: {particular(a)}, {peculiar(a)}, {special(a)}]
     2: separate and distinct from others; "an exception in this
        particular case" [syn: {particular(a)}]
     3: separate and distinct from others of the same group or
        category; "interested in one particular artist"; "a ...
