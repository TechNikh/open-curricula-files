---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disintegrative
offline_file: ""
offline_thumbnail: ""
uuid: ffb6d31d-a2bb-4e80-af55-dd03096b3d7d
updated: 1484310603
title: disintegrative
categories:
    - Dictionary
---
disintegrative
     adj : tending to cause breakup into constituent elements or parts
           [ant: {integrative}]
