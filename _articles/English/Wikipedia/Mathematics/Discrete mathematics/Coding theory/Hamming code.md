---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamming_code
offline_file: ""
offline_thumbnail: ""
uuid: 8f82dfb6-efd4-4d86-9cbb-bec46adf6436
updated: 1484308565
title: Hamming code
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/160px-Hamming%25287%252C4%2529.svg_0.png'
tags:
    - History
    - Codes predating Hamming
    - Parity
    - Two-out-of-five code
    - Repetition
    - Hamming codes
    - General algorithm
    - Hamming codes with additional parity (SECDED)
    - '[7,4] Hamming code'
    - Construction of G and H
    - Encoding
    - '[7,4] Hamming code with an additional parity bit'
    - Notes
categories:
    - Coding theory
---
In telecommunication, Hamming codes are a family of linear error-correcting codes that generalize the Hamming(7,4)-code, and were invented by Richard Hamming in 1950. Hamming codes can detect up to two-bit errors or correct one-bit errors without detection of uncorrected errors. By contrast, the simple parity code cannot correct errors, and can detect only an odd number of bits in error. Hamming codes are perfect codes, that is, they achieve the highest possible rate for codes with their block length and minimum distance of three.[1]
