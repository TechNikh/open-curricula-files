---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Information_geometry
offline_file: ""
offline_thumbnail: ""
uuid: f40a8377-1069-43ed-82ff-184fa8f93f09
updated: 1484309224
title: Information geometry
tags:
    - Introduction
    - Information and probability
    - Statistical model, Parameters
    - Differential geometry applied to probability
    - Tangent space
    - alpha representation
    - Inner product
    - Fisher metric as inner product
    - Affine connection
    - Alpha connection
    - Divergence
    - Canonical divergence
    - Properties of divergence
    - Canonical divergence for the exponential family
    - Canonical divergence for general alpha families
    - History
    - Applications
categories:
    - Differential geometry
---
Information geometry is a branch of mathematics that applies the techniques of differential geometry to the field of probability theory. This is done by taking probability distributions for a statistical model as the points of a Riemannian manifold, forming a statistical manifold. The Fisher information metric provides the Riemannian metric.
