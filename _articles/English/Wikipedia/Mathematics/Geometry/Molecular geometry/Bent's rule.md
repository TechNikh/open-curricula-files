---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bent%27s_rule'
offline_file: ""
offline_thumbnail: ""
uuid: 3a248869-d0df-4978-8bdd-85ff92412c14
updated: 1484309275
title: "Bent's rule"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Water_skeleton_with_bond_angle_included.png
tags:
    - History
    - Justification
    - Examples
    - Bond angles
    - Bond lengths
    - JCH Coupling constants
    - Inductive effect
    - Formal theory
categories:
    - Molecular geometry
---
In chemistry, Bent's rule describes and explains the relationship between the orbital hybridization of central atoms in molecules and the electronegativities of substituents.[1][2] The rule was stated by Henry Bent as follows: "Atomic s character concentrates in orbitals directed toward electropositive substituents".[2]
