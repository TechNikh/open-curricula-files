---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relevant
offline_file: ""
offline_thumbnail: ""
uuid: 6efe91c7-8139-4ce9-ba1c-2bb2dab63abd
updated: 1484310447
title: relevant
categories:
    - Dictionary
---
relevant
     adj 1: having a bearing on or connection with the subject at issue;
            "the scientist corresponds with colleagues in order to
            learn about matters relevant to her own research"
            [ant: {irrelevant}]
     2: having crucial relevance; "crucial to the case"; "relevant
        testimony" [syn: {crucial}]
