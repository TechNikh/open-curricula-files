---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/whopping
offline_file: ""
offline_thumbnail: ""
uuid: 18cdfc19-3b31-4f44-a9a8-092e5a7744f0
updated: 1484310517
title: whopping
categories:
    - Dictionary
---
whop
     v 1: hit hard; "The teacher whacked the boy" [syn: {whack}, {wham},
           {wallop}]
     2: hit hard [syn: {sock}, {bop}, {whap}, {bonk}, {bash}]
     [also: {whopping}, {whopped}]
