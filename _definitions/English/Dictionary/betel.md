---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/betel
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484576581
title: betel
categories:
    - Dictionary
---
betel
     n : Asian pepper plant whose dried leaves are chewed with betel
         nut (seed of the betel palm) by southeast Asians [syn: {betel
         pepper}, {Piper betel}]
