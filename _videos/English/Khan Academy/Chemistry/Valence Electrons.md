---
version: 1
type: video
provider: YouTube
id: 1TZA171yxY4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Valence%20Electrons.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 47445917-6b56-41f2-931b-126c46725120
updated: 1486069589
title: Valence Electrons
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/1TZA171yxY4/default.jpg
    - https://i3.ytimg.com/vi/1TZA171yxY4/1.jpg
    - https://i3.ytimg.com/vi/1TZA171yxY4/2.jpg
    - https://i3.ytimg.com/vi/1TZA171yxY4/3.jpg
---
Looking at valence electrons to figure out reactivity
More free lessons at: http://www.khanacademy.org/video?v=1TZA171yxY4
