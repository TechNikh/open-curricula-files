---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_squaring_map
offline_file: ""
offline_thumbnail: ""
uuid: 7b1f4afe-428d-4910-bc65-c2fc6869ca6a
updated: 1484308719
title: Complex squaring map
tags:
    - Chaos and the complex squaring map
    - Generalisations
categories:
    - Chaotic maps
---
In mathematics, the complex squaring map, a polynomial mapping of degree two, is a simple and accessible demonstration of chaos in dynamical systems. It can be constructed by performing the following steps:
