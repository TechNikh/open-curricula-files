---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/completed
offline_file: ""
offline_thumbnail: ""
uuid: 76aefbd9-4ccf-4e3a-8816-dd317d2d0e8b
updated: 1484310254
title: completed
categories:
    - Dictionary
---
completed
     adj 1: successfully completed or brought to an end; "his mission
            accomplished he took a vacation"; "the completed
            project"; "the joy of a realized ambition overcame
            him" [syn: {accomplished}, {realized}, {realised}]
     2: (of a marriage) completed by the first act of sexual
        intercourse after the ceremony
     3: caught; "a completed forward pass"
