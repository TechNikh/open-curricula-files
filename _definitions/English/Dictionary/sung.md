---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sung
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484577721
title: sung
categories:
    - Dictionary
---
sung
     adj : using the voice in song; "vocal music" [syn: {vocal}]
     n : the imperial dynasty of China from 960 to 1279; noted for
         art and literature and philosophy [syn: {Sung dynasty}, {Song},
          {Song dynasty}]
