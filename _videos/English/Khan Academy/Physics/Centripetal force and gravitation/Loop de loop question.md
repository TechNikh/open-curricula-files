---
version: 1
type: video
provider: YouTube
id: wjZb3zPuoeA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Centripetal%20force%20and%20gravitation/Loop%20de%20loop%20question.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bbca4083-d7fc-4a02-a636-0628dac965a0
updated: 1486069597
title: Loop de loop question
categories:
    - Centripetal force and gravitation
thumbnail_urls:
    - https://i3.ytimg.com/vi/wjZb3zPuoeA/default.jpg
    - https://i3.ytimg.com/vi/wjZb3zPuoeA/1.jpg
    - https://i3.ytimg.com/vi/wjZb3zPuoeA/2.jpg
    - https://i3.ytimg.com/vi/wjZb3zPuoeA/3.jpg
---
Asks students to find the minimum speed necessary to complete the loop de loop. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/centripetal-force-and-gravitation/centripetal-acceleration-tutoria/v/loop-de-loop-answer-part-1?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/centripetal-force-and-gravitation/centripetal-acceleration-tutoria/v/calculus-proof-of-centripetal-acceleration-formula?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
