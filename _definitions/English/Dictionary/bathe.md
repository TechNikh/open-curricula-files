---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bathe
offline_file: ""
offline_thumbnail: ""
uuid: cbd5fdc5-9e3e-4fac-b6cb-11f2fe5e6b0a
updated: 1484310545
title: bathe
categories:
    - Dictionary
---
bathe
     n : the act of swimming; "the Englishman said he had a good
         bathe"
     v 1: cleanse the entire body; "bathe daily"
     2: suffuse with or as if with light; "The room was bathed in
        sunlight"
     3: clean one's body by immersion into water; "The child should
        bathe every day" [syn: {bath}]
