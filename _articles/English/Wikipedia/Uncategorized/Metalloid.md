---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metalloids
offline_file: ""
offline_thumbnail: ""
uuid: 5ba2bb4f-f9fd-4bae-a5bf-e5316a6bb4c9
updated: 1484308384
title: Metalloid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Copper_germanium.jpg
tags:
    - Definitions
    - Judgement-based
    - Criteria-based
    - Periodic table territory
    - Location
    - Alternative treatments
    - Properties
    - Compared to metals and nonmetals
    - Common applications
    - Alloys
    - Biological agents
    - Catalysts
    - Flame retardants
    - Glass formation
    - Optical storage and optoelectronics
    - Pyrotechnics
    - Semiconductors and electronics
    - Nomenclature and history
    - Derivation and other names
    - Origin and usage
    - Elements commonly recognised as metalloids
    - Boron
    - Silicon
    - Germanium
    - Arsenic
    - Antimony
    - Tellurium
    - Elements less commonly recognised as metalloids
    - carbon
    - Aluminium
    - Selenium
    - Polonium
    - Astatine
    - Related concepts
    - Near metalloids
    - Allotropes
    - Abundance, extraction and cost
    - Abundance
    - Extraction
    - Cost
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
A metalloid is any chemical element which has properties in between those of metals and nonmetals, or that has a mixture of them. There is neither a standard definition of a metalloid nor complete agreement on the elements appropriately classified as such. Despite the lack of specificity, the term remains in use in the literature of chemistry.
