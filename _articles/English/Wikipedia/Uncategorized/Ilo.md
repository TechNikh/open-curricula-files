---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ilo
offline_file: ""
offline_thumbnail: ""
uuid: 9325b0ac-a477-400e-b580-4e2939ca7791
updated: 1484094290
title: Ilo
tags:
    - History
    - Transport
    - Climate
    - Pacific Ocean terminal of the Interoceanic Highway
    - Main sights
categories:
    - Uncategorized
---
Coordinates: 17°38′45.09″S 71°20′43.13″W﻿ / ﻿17.6458583°S 71.3453139°W﻿ / -17.6458583; -71.3453139 Ilo is a port city in southern Peru, with some 67,000 inhabitants. It is the second largest city in the Moquegua Region and capital of the Ilo Province.
