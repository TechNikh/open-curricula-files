---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contract
offline_file: ""
offline_thumbnail: ""
uuid: 2ba658b2-8729-4906-92a2-cbf8fcd0e764
updated: 1484310517
title: contract
categories:
    - Dictionary
---
contract
     n 1: a binding agreement between two or more persons that is
          enforceable by law
     2: (contract bridge) the highest bid becomes the contract
        setting the number of tricks that the bidder must make
        [syn: {declaration}]
     3: a variety of bridge in which the bidder receives points
        toward game only for the number of tricks he bid [syn: {contract
        bridge}]
     v 1: enter into a contractual arrangement [syn: {undertake}]
     2: engage by written agreement; "They signed two new pitchers
        for the next season" [syn: {sign}, {sign on}, {sign up}]
     3: squeeze or press together; "she compressed her lips"; "the
        spasm ...
