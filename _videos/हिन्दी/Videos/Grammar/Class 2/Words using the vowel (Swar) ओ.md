---
version: 1
type: video
provider: YouTube
id: bZs_l-MeMDc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 689ae7c3-290d-4682-b453-9760d8b5bb65
updated: 1486069630
title: Words using the vowel (Swar) ओ
categories:
    - Class 2
thumbnail_urls:
    - https://i3.ytimg.com/vi/bZs_l-MeMDc/default.jpg
    - https://i3.ytimg.com/vi/bZs_l-MeMDc/1.jpg
    - https://i3.ytimg.com/vi/bZs_l-MeMDc/2.jpg
    - https://i3.ytimg.com/vi/bZs_l-MeMDc/3.jpg
---
