---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conchospiral
offline_file: ""
offline_thumbnail: ""
uuid: 8b1d19aa-03ac-48d4-86ea-29c3763034f8
updated: 1484309360
title: Conchospiral
categories:
    - Geometry stubs
---
The projection of a conchospiral on the (r, θ)-plane is a logarithmic spiral.
