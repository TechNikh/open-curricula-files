---
version: 1
type: article
id: https://en.wikipedia.org/wiki/G-structure_on_a_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 085c10f0-9186-45fb-b000-77061b38f58c
updated: 1484309221
title: G-structure on a manifold
tags:
    - Principal bundles and G-structures
    - Integrability conditions
    - Isomorphism of G-structures
    - Connections on G-structures
    - Torsion of a G-structure
    - 'Example: Torsion for almost complex structures'
    - Higher order G-structures
    - Reduction of the structure group
    - Examples
    - Integrability
    - Notes
categories:
    - Differential geometry
---
In differential geometry, a G-structure on an n-manifold M, for a given structure group[1] G, is a G-subbundle of the tangent frame bundle FM (or GL(M)) of M.
