---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multidimensional_network
offline_file: ""
offline_thumbnail: ""
uuid: 97c4a9f6-49ba-46af-81b9-e9406896ba12
updated: 1484309440
title: Multidimensional network
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-MuxViz_EU_Airports.png
tags:
    - Terminology
    - Definition
    - Unweighted multilayer networks
    - Weighted multilayer networks
    - General formulation in terms of tensors
    - Multidimensional network-specific definitions
    - Multi-layer neighbors
    - Multi-layer path length
    - Network of layers
    - Centrality measures
    - Degree
    - Versatility as multilayer centrality
    - Eigenvector versatility
    - Katz versatility
    - HITS versatility
    - PageRank versatility
    - Triadic closure and clustering coefficients
    - Community discovery
    - Modularity maximization
    - Tensor decomposition
    - Statistical inference
    - Structural reducibility
    - Other multilayer network descriptors
    - Degree correlations
    - Path dominance
    - Shortest path discovery
    - Multidimensional distance
    - Dimension relevance
    - Dimension connectivity
    - Burst detection
    - Diffusion processes on multilayer networks
    - Random walks
    - Classical diffusion
    - Information and epidemics spreading
    - Software
categories:
    - Network theory
---
Multidimensional networks, a special type of multilayer network, are networks with multiple kinds of relations.[1][2][3][4] Increasingly sophisticated attempts to model real-world systems as multidimensional networks have yielded valuable insight in the fields of social network analysis,[2][5][6][7][8] economics, urban and international transport,[9][10][11] ecology,[12] psychology,[13] medicine, biology,[14] commerce, climatology, physics,[15][16] computational neuroscience,[17][18][19] operations management, and finance.
