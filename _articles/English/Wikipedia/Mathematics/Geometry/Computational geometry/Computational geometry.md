---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computational_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 2f60f8f0-971e-4a00-8092-6e752e7759be
updated: 1484309173
title: Computational geometry
tags:
    - Combinatorial computational geometry
    - Problem classes
    - Static problems
    - Geometric query problems
    - Dynamic problems
    - Variations
    - Numerical computational geometry
    - Journals
    - Combinatorial/algorithmic computational geometry
categories:
    - Computational geometry
---
Computational geometry is a branch of computer science devoted to the study of algorithms which can be stated in terms of geometry. Some purely geometrical problems arise out of the study of computational geometric algorithms, and such problems are also considered to be part of computational geometry. While modern computational geometry is a recent development, it is one of the oldest fields of computing with history stretching back to antiquity.
