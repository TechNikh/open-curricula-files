---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Numbering_(computability_theory)
offline_file: ""
offline_thumbnail: ""
uuid: e626b878-3ef1-4520-99e8-c12cf0f3e6a9
updated: 1484309487
title: Numbering (computability theory)
tags:
    - Definition and examples
    - Types of numberings
    - Comparison of numberings
    - Computable numberings
categories:
    - Computability theory
---
In computability theory a numbering is the assignment of natural numbers to a set of objects such as functions, rational numbers, graphs, or words in some language. A numbering can be used to transfer the idea of computability and related concepts, which are originally defined on the natural numbers using computable functions, to these different types of objects.
