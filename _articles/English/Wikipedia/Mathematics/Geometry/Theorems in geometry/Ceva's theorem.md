---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ceva%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 241c7b04-4b41-4512-801a-dae7a89d530c
updated: 1484309114
title: "Ceva's theorem"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Ceva%2527s_theorem_1.svg.png'
tags:
    - Proof of the theorem
    - Generalizations
categories:
    - Theorems in geometry
---
Ceva's theorem is a theorem about triangles in Euclidean plane geometry. Given a triangle ABC, let the lines AO, BO and CO be drawn from the vertices to a common point O (not on one of the sides of ABC), to meet opposite sides at D, E and F respectively. (The segments AD, BE, and CF are known as cevians.) Then, using signed lengths of segments,
