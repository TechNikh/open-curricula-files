---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Groupoid_scheme
offline_file: ""
offline_thumbnail: ""
uuid: c9a19304-f748-4905-a136-b51daf78e163
updated: 1484309153
title: Groupoid scheme
categories:
    - Algebraic geometry
---
In algebraic geometry, a groupoid scheme is a pair of schemes 
  
    
      
        R
        ,
        U
      
    
    {\displaystyle R,U}
  
 together with five morphisms 
  
    
      
        s
        ,
        t
        :
        R
        →
        U
        ,
        e
        :
        U
        →
        R
        ,
        m
        :
        R
        
          ×
          
            U
            ,
            s
            ,
            t
          
        
        R
        →
        R
        ,
        i
        :
        R
        →
        R
      
    
    {\displaystyle s,t:R\to U,e:U\to R,m:R\times _{U,s,t}R\to R,i:R\to R}
  
 satisfying 
  
    
      ...
