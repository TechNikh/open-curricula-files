---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quasi-perfect_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 50046340-b2d6-4612-8658-89f8aa7b8adc
updated: 1484309032
title: Quasi-perfect equilibrium
categories:
    - Game theory equilibrium concepts
---
Informally, a player playing by a strategy from a quasi-perfect equilibrium takes observed as well as potential future mistakes of his opponents into account but assumes that he himself will not make a mistake in the future, even if he observes that he has done so in the past.
