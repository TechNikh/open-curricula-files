---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seated
offline_file: ""
offline_thumbnail: ""
uuid: 78a6aa0c-092b-4970-a87b-67435c90e553
updated: 1484310148
title: seated
categories:
    - Dictionary
---
seated
     adj : (of persons) having the torso erect and legs bent with the
           body supported on the buttocks; "the seated Madonna";
           "the audience remained seated" [syn: {sitting}] [ant: {standing}]
