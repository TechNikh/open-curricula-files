---
version: 1
type: video
provider: YouTube
id: s_R8d3isJDA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Conservation%20of%20angular%20momemtum.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 43de3818-e0cd-465e-bc20-f7faa4b62052
updated: 1486069595
title: Conservation of angular momemtum
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/s_R8d3isJDA/default.jpg
    - https://i3.ytimg.com/vi/s_R8d3isJDA/1.jpg
    - https://i3.ytimg.com/vi/s_R8d3isJDA/2.jpg
    - https://i3.ytimg.com/vi/s_R8d3isJDA/3.jpg
---
Angular momentum is constant when there is no net torque.
More free lessons at: http://www.khanacademy.org/video?v=s_R8d3isJDA
