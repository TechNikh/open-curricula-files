---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/save
offline_file: ""
offline_thumbnail: ""
uuid: 19115931-aa5e-4fc8-8bc1-c5739d64ee98
updated: 1484310266
title: save
categories:
    - Dictionary
---
save
     n : (sports) the act of preventing the opposition from scoring;
         "the goalie made a brilliant save"; "the relief pitcher
         got credit for a save"
     v 1: save from ruin, destruction, or harm [syn: {salvage}, {salve},
           {relieve}]
     2: to keep up and reserve for personal or special use; "She
        saved the old family photographs in a drawer" [syn: {preserve}]
     3: bring into safety; "We pulled through most of the victims of
        the bomb attack" [syn: {carry through}, {pull through}, {bring
        through}]
     4: spend less; buy at a reduced price
     5: feather one's nest; have a nest egg; "He saves half his
        salary" [syn: {lay ...
