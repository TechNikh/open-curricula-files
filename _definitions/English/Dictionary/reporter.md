---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reporter
offline_file: ""
offline_thumbnail: ""
uuid: 2bbf4243-eacd-4709-9163-29bc29c87ab5
updated: 1484310480
title: reporter
categories:
    - Dictionary
---
reporter
     n : a person who investigates and reports or edits news stories
         [syn: {newsman}, {newsperson}]
