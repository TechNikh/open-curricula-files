---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corrupt
offline_file: ""
offline_thumbnail: ""
uuid: c0362370-b3f7-49c6-b592-09d5100b2bbd
updated: 1484310581
title: corrupt
categories:
    - Dictionary
---
corrupt
     adj 1: lacking in integrity; "humanity they knew to be
            corrupt...from the day of Adam's creation"; "a corrupt
            and incompetent city government" [ant: {incorrupt}]
     2: not straight; dishonest or immoral or evasive [syn: {crooked}]
        [ant: {straight}]
     3: containing errors or alterations; "a corrupt text"; "spoke a
        corrupted version of the language" [syn: {corrupted}]
     4: touched by rot or decay; "tainted bacon"; "`corrupt' is
        archaic" [syn: {tainted}]
     v 1: corrupt morally or by intemperance or sensuality; "debauch
          the young people with wine and women"; "Socrates was
          accused of corrupting young ...
