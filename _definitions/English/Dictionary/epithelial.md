---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/epithelial
offline_file: ""
offline_thumbnail: ""
uuid: ba39cd20-8a80-4be4-9620-acc2c95df284
updated: 1484310313
title: epithelial
categories:
    - Dictionary
---
epithelial
     adj : of or belonging to the epithelium; "epithelial layer"
