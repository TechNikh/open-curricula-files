---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/release
offline_file: ""
offline_thumbnail: ""
uuid: be3d14c2-f753-4d9e-9c15-e65f00035535
updated: 1484310268
title: release
categories:
    - Dictionary
---
release
     n 1: merchandise issued for sale or public showing (especially a
          record or film); "a new release from the London Symphony
          Orchestra"
     2: the act of liberating someone or something [syn: {liberation},
         {freeing}]
     3: a process that liberates or discharges something; "there was
        a sudden release of oxygen"; "the release of iodine from
        the thyroid gland"
     4: an announcement distributed to members of the press in order
        to supplement or replace an oral presentation [syn: {handout},
         {press release}]
     5: the termination of someone's employment (leaving them free
        to depart) [syn: {dismissal}, ...
