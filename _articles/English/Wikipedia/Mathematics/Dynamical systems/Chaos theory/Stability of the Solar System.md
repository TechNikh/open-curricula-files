---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stability_of_the_Solar_System
offline_file: ""
offline_thumbnail: ""
uuid: 099ebc0b-a00f-4ad8-832d-c06895479c24
updated: 1484308727
title: Stability of the Solar System
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/401px-Semimajorhistogramofkbos.svg.png
tags:
    - Overview and challenges
    - Resonance
    - Predictability
    - Scenarios
    - Neptune–Pluto resonance
    - Jovian moon resonance
    - Mercury–Jupiter 1:1 perihelion-precession resonance
    - Asteroid influence
    - Chaos from geological processes
    - Studies
    - LONGSTOP
    - Digital Orrery
    - 'Laskar #1'
    - 'Laskar & Gastineau'
categories:
    - Chaos theory
---
The stability of the Solar System is a subject of much inquiry in astronomy. Though the planets have been stable when historically observed, and will be in the short term, their weak gravitational effects on one another can add up in unpredictable ways. For this reason (among others) the Solar System is stated to be chaotic,[1] and even the most precise long-term models for the orbital motion of the Solar System are not valid over more than a few tens of millions of years.[2]
