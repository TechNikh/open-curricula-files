---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Art_auction
offline_file: ""
offline_thumbnail: ""
uuid: 0c0905e8-7f13-45f9-a1e7-2a46b2aceeb7
updated: 1484309013
title: Art auction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_8.jpg
tags:
    - History
    - Early days
    - Mid-19th century
    - Late 19th to early 20th centuries
    - Late 20th century
    - 21st century
    - Contemporary market structure
    - Competitors
    - Segments
    - Estimates
    - "Commissions and Buyer's Premium"
    - Performance-based fees
    - Guarantees
    - Online sales
    - Controversy
    - Historical bibliography
    - Notes
categories:
    - Auctions
---
In England this dates from the latter part of the 17th century, when in most cases the names of the auctioneers were suppressed. In June 1693, John Evelyn mentions a "great auction of pictures (Lord Melfort's) in the Banqueting House, Whitehall",[1][2] and the practice is frequently referred to by other contemporary and later writers.[1]
