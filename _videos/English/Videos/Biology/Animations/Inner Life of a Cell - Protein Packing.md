---
version: 1
type: video
provider: YouTube
id: uHeTQLNFTgU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Inner%20Life%20of%20a%20Cell%20-%20Protein%20Packing.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 70cc028a-7534-4fdc-9234-3e8b6864b21f
updated: 1486069666
title: Inner Life of a Cell | Protein Packing
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/uHeTQLNFTgU/default.jpg
    - https://i3.ytimg.com/vi/uHeTQLNFTgU/1.jpg
    - https://i3.ytimg.com/vi/uHeTQLNFTgU/2.jpg
    - https://i3.ytimg.com/vi/uHeTQLNFTgU/3.jpg
---
