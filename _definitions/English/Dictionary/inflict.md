---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inflict
offline_file: ""
offline_thumbnail: ""
uuid: a30bacbb-be0a-4aa7-8905-8af032d85685
updated: 1484310325
title: inflict
categories:
    - Dictionary
---
inflict
     v : impose something unpleasant; "The principal visited his rage
         on the students" [syn: {bring down}, {visit}, {impose}]
