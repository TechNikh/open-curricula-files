---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trigonometric_number
offline_file: ""
offline_thumbnail: ""
uuid: 7bbdb7fb-e7bd-46d6-ab5d-293f053e26d6
updated: 1484309350
title: Trigonometric number
categories:
    - Trigonometry
---
In mathematics, a trigonometric number[1]:ch. 5 is an irrational number produced by taking the sine or cosine of a rational multiple of a circle, or equivalently, the sine or cosine of an angle which in radians is a rational multiple of π, or the sine or cosine of a rational number of degrees.
