---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bogomolov_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 9af2fe5f-9eb2-4a87-93ea-021770cb07ca
updated: 1484309356
title: Bogomolov conjecture
categories:
    - Geometry stubs
---
Let C be an algebraic curve of genus g at least two defined over a number field K, let 
  
    
      
        
          
            K
            ¯
          
        
      
    
    {\displaystyle {\overline {K}}}
  
 denote the algebraic closure of K, fix an embedding of C into its Jacobian variety J, and let 
  
    
      
        
          
            
              h
              ^
            
          
        
      
    
    {\displaystyle {\hat {h}}}
  
 denote the Néron-Tate height on J associated to an ample symmetric divisor. Then there exists an 
  
    
      
        ϵ
        >
        0
      
    
    {\displaystyle \epsilon >0}
  
 such that the set
