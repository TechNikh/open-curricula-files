---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/creeps
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484485261
title: creeps
categories:
    - Dictionary
---
creeps
     n 1: a disease of cattle and sheep attributed to a dietary
          deficiency; characterized by anemia and softening of the
          bones and a slow stiff gait
     2: a feeling of fear and revulsion; "he gives me the creeps"
