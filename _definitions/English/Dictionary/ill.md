---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ill
offline_file: ""
offline_thumbnail: ""
uuid: 94b2d59f-c760-46a5-a8f5-71d72c1dca6e
updated: 1484310170
title: ill
categories:
    - Dictionary
---
ill
     adj 1: not in good physical or mental health; "ill from the
            monotony of his suffering" [syn: {sick}] [ant: {well}]
     2: resulting in suffering or adversity; "ill effects"; "it's an
        ill wind that blows no good"
     3: distressing; "ill manners"; "of ill repute"
     4: indicating hostility or enmity; "you certainly did me an ill
        turn"; "ill feelings"; "ill will"
     5: presaging ill-fortune; "ill omens"; "ill predictions"; "my
        words with inauspicious thunderings shook heaven"-
        P.B.Shelley; "a dead and ominous silence prevailed"; "a
        by-election at a time highly unpropitious for the
        Government" [syn: {inauspicious}, ...
