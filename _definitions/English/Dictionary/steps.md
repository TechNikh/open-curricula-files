---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/steps
offline_file: ""
offline_thumbnail: ""
uuid: 0f04c0a8-821b-4c36-80e5-f984e92e2e98
updated: 1484310295
title: steps
categories:
    - Dictionary
---
steps
     n 1: a way of access consisting of a set of steps [syn: {stairway},
           {staircase}, {stairs}]
     2: the course along which a person has walked or is walking in;
        "I followed in his steps"; "he retraced his steps"
