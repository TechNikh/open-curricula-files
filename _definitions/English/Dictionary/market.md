---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/market
offline_file: ""
offline_thumbnail: ""
uuid: f8b06249-a0ea-4eb2-924b-6ef03549d4b8
updated: 1484310453
title: market
categories:
    - Dictionary
---
market
     n 1: the world of commercial activity where goods and services
          are bought and sold; "without competition there would be
          no market"; "they were driven from the marketplace"
          [syn: {marketplace}]
     2: the securities markets in the aggregate; "the market always
        frustrates the small investor" [syn: {securities industry}]
     3: the customers for a particular product or service; "before
        they publish any book they try to determine the size of
        the market for it"
     4: a marketplace where groceries are sold; "the grocery store
        included a meat market" [syn: {grocery store}, {grocery},
        {food market}]
     v 1: ...
