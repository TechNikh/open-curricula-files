---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breathed
offline_file: ""
offline_thumbnail: ""
uuid: 8da8d0a2-c0a4-4955-a9a0-a58b92db5d65
updated: 1484310321
title: breathed
categories:
    - Dictionary
---
breathed
     adj 1: having breath or breath as specified; usually used in
            combination; "sweet-breathed"
     2: uttered without voice; "could hardly hear her breathed plea,
        `Help me'"; "voiceless whispers" [syn: {voiceless}]
