---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reference
offline_file: ""
offline_thumbnail: ""
uuid: 28b4a2e3-2eef-4c00-a51f-f624e8f0393b
updated: 1484310429
title: reference
categories:
    - Dictionary
---
reference
     n 1: a remark that calls attention to something or someone; "she
          made frequent mention of her promotion"; "there was no
          mention of it"; "the speaker made several references to
          his wife" [syn: {mention}]
     2: a short note recognizing a source of information or of a
        quoted passage; "the student's essay failed to list
        several important citations"; "the acknowledgments are
        usually printed at the front of a book"; "the article
        includes mention of similar clinical cases" [syn: {citation},
         {acknowledgment}, {credit}, {mention}, {quotation}]
     3: an indicator that orients you generally; "it is used as a
     ...
