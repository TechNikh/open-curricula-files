---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transfer_entropy
offline_file: ""
offline_thumbnail: ""
uuid: cd19376b-72b9-4e0a-94ac-761af93cba77
updated: 1484308773
title: Transfer entropy
categories:
    - Entropy and information
---
Transfer entropy is a non-parametric statistic measuring the amount of directed (time-asymmetric) transfer of information between two random processes.[1][2][3] Transfer entropy from a process X to another process Y is the amount of uncertainty reduced in future values of Y by knowing the past values of X given past values of Y. More specifically, if 
  
    
      
        
          X
          
            t
          
        
      
    
    {\displaystyle X_{t}}
  
 and 
  
    
      
        
          Y
          
            t
          
        
      
    
    {\displaystyle Y_{t}}
  
 for 
  
    
      
        t
        ∈
        
          N
        
      
    
    ...
