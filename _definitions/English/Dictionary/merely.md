---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/merely
offline_file: ""
offline_thumbnail: ""
uuid: 306dc005-ab0c-4fad-aeee-77335fc43aaf
updated: 1484310321
title: merely
categories:
    - Dictionary
---
merely
     adv : and nothing more; "I was merely asking"; "it is simply a
           matter of time"; "just a scratch"; "he was only a
           child"; "hopes that last but a moment" [syn: {simply},
           {just}, {only}, {but}]
