---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/passive
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484631481
title: passive
categories:
    - Dictionary
---
passive
     adj 1: lacking in energy or will; "Much benevolence of the passive
            order may be traced to a disinclination to inflict
            pain upon oneself"- George Meredith [syn: {inactive}]
            [ant: {active}]
     2: peacefully resistant in response to injustice; "passive
        resistance" [syn: {peaceful}]
     3: expressing thatthe subject of the sentence is the patient of
        the action denoted by the verb; "academics seem to favor
        passive sentences" [ant: {active}]
     n : the voice used to indicate that the grammatical subject of
         the verb is the recipient (not the source) of the action
         denoted by the verb; "`The ball was ...
