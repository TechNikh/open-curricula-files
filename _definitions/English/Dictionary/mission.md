---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mission
offline_file: ""
offline_thumbnail: ""
uuid: c7368dc9-9fa6-4a63-9856-0a506913e432
updated: 1484310573
title: mission
categories:
    - Dictionary
---
mission
     n 1: an organization of missionaries in a foreign land sent to
          carry on religious work [syn: {missionary post}, {missionary
          station}, {foreign mission}]
     2: an operation that is assigned by a higher headquarters; "the
        planes were on a bombing mission" [syn: {military mission}]
     3: a special assignment that is given to a person or group; "a
        confidential mission to London"; "his charge was deliver a
        message" [syn: {charge}, {commission}]
     4: the organized work of a religious missionary [syn: {missionary
        work}]
     5: a group of representatives or delegates [syn: {deputation},
        {commission}, {delegation}, ...
