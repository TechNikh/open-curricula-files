---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adjunct
offline_file: ""
offline_thumbnail: ""
uuid: de1a7d3b-8296-4aff-b7a3-95ab69d895e5
updated: 1484310437
title: adjunct
categories:
    - Dictionary
---
adjunct
     adj 1: relating to something that is added but is not essential;
            "an ancillary pump"; "an adjuvant discipline to forms
            of mysticism"; "The mind and emotions are auxilliary
            to each other" [syn: {accessory}, {ancillary}, {adjuvant},
             {appurtenant}, {auxiliary}, {subsidiary}]
     2: of or relating to a person who is subordinate to another
        [syn: {assistant}]
     n 1: something added to another thing but not an essential part
          of it
     2: a person who is an assistant or subordinate to another
     3: a construction that is part of a sentence but not essential
        to its meaning and can be omitted without making ...
