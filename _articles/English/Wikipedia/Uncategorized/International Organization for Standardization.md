---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Iso
offline_file: ""
offline_thumbnail: ""
uuid: be21d117-9f9d-4717-9c3f-251d0f575ef0
updated: 1484308394
title: International Organization for Standardization
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Memory_plaque_of_founding_ISA_in_Prague_cropped.jpg
tags:
    - Overview
    - Name and abbreviations
    - History
    - Structure
    - IEC joint committees
    - ISO/IEC JTC 1
    - ISO/IEC JTC 2
    - Membership
    - Financing
    - International Standards and other publications
    - Document copyright
    - Standardization process
    - Products named after ISO
    - Criticism
    - Notes and references
categories:
    - Uncategorized
---
The International Organization for Standardization (ISO) is an international standard-setting body composed of representatives from various national standards organizations.
