---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/legal
offline_file: ""
offline_thumbnail: ""
uuid: dd3f43e2-df9b-464e-a507-6b1075396d9b
updated: 1484310475
title: legal
categories:
    - Dictionary
---
legal
     adj 1: established by or founded upon law or official or accepted
            rules [ant: {illegal}]
     2: of or relating to jurisprudence; "legal loophole"
     3: having legal efficacy or force; "a sound title to the
        property" [syn: {sound}]
     4: relating to or characteristic of the profession of law; "the
        legal profession"
     5: allowed by official rules; "a legal pass receiver"
