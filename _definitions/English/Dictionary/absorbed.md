---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/absorbed
offline_file: ""
offline_thumbnail: ""
uuid: 9e195ce6-530c-4bfe-9238-7fe1b827b271
updated: 1484310321
title: absorbed
categories:
    - Dictionary
---
absorbed
     adj 1: wholly absorbed as in thought; "deep in thought"; "that
            engrossed look or rapt delight"; "the book had her
            totally engrossed"; "enwrapped in dreams"; "so intent
            on this fantastic...narrative that she hardly
            stirred"- Walter de la Mare; "rapt with wonder";
            "wrapped in thought" [syn: {engrossed}, {enwrapped}, {intent},
             {rapt}, {wrapped}]
     2: retained without reflection; "the absorbed light intensity"
     3: taken in through the pores of a surface; "the absorbed water
        expanded the sponge"
