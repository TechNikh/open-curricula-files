---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pause
offline_file: ""
offline_thumbnail: ""
uuid: 739e8465-d92c-4c67-b253-9d9e59f7293e
updated: 1484310150
title: pause
categories:
    - Dictionary
---
pause
     n 1: a time interval during which there is a temporary cessation
          of something [syn: {intermission}, {break}, {interruption},
           {suspension}]
     2: temporary inactivity
     v 1: interrupt temporarily an activity before continuing; "The
          speaker paused" [syn: {hesitate}]
     2: cease an action temporarily; "We pause for station
        identification"; "let's break for lunch" [syn: {intermit},
         {break}]
