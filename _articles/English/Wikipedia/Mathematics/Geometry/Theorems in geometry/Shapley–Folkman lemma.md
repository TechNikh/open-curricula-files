---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Shapley%E2%80%93Folkman_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: fc611349-162d-4244-aa98-b82cf8bc1792
updated: 1484309126
title: Shapley–Folkman lemma
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Shapley%25E2%2580%2593Folkman_lemma.svg.png'
tags:
    - Introductory example
    - Preliminaries
    - Real vector spaces
    - Convex sets
    - Convex hull
    - Minkowski addition
    - Convex hulls of Minkowski sums
    - Statements
    - Lemma of Shapley and Folkman
    - Dimension of a real vector space
    - "Shapley–Folkman theorem and Starr's corollary"
    - Proofs and computations
    - Applications
    - Economics
    - Non-convex preferences
    - "Starr's 1969 paper and contemporary economics"
    - Mathematical optimization
    - Preliminaries of optimization theory
    - Additive optimization problems
    - Probability and measure theory
    - Notes
categories:
    - Theorems in geometry
---
The Shapley–Folkman lemma is a result in convex geometry with applications in mathematical economics that describes the Minkowski addition of sets in a vector space. Minkowski addition is defined as the addition of the sets' members: for example, adding the set consisting of the integers zero and one to itself yields the set consisting of zero, one, and two:
