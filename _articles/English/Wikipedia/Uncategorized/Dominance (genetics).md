---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dominant_trait
offline_file: ""
offline_thumbnail: ""
uuid: f681e850-b0a3-4810-a7ee-cc8660e8a1d9
updated: 1484308377
title: Dominance (genetics)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Journal_of_Agricultural_Research_%25281917%2529_%252814582377398%2529.jpg'
tags:
    - Background
    - Chromosomes, genes, and alleles
    - Dominance
    - Complete dominance
    - Incomplete dominance
    - Co-dominance
    - Which trait is dominant?
    - Addressing common misconceptions
    - Nomenclature
    - Relationship to other genetic concepts
    - Multiple alleles
    - Autosomal versus sex-linked dominance
    - Epistasis
    - Hardy-Weinberg principle (Estimation of carrier frequency)
    - Molecular mechanisms
    - Loss of function and haplosufficiency
    - Dominant-negative mutations
    - Dominant and recessive genetic diseases in humans
categories:
    - Uncategorized
---
Dominance in genetics is a relationship between alleles of one gene, in which the effect on phenotype of one allele masks the contribution of a second allele at the same locus.[1][2] The first allele is dominant and the second allele is recessive. For genes on an autosome (any chromosome other than a sex chromosome), the alleles and their associated traits are autosomal dominant or autosomal recessive. Dominance is a key concept in Mendelian inheritance and classical genetics. Often the dominant allele codes for a functional protein whereas the recessive allele does not.
