---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/normalcy
offline_file: ""
offline_thumbnail: ""
uuid: 47d22113-eefd-4457-876b-3a3fbeadd79b
updated: 1484310189
title: normalcy
categories:
    - Dictionary
---
normalcy
     n 1: being within certain limits that define the range of normal
          functioning [syn: {normality}] [ant: {abnormality}]
     2: expectedness as a consequence of being usual or regular or
        common [syn: {normality}]
