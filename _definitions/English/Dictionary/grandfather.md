---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grandfather
offline_file: ""
offline_thumbnail: ""
uuid: 39bf2b57-f57e-4a14-a079-18228a5a0ffd
updated: 1484310301
title: grandfather
categories:
    - Dictionary
---
grandfather
     n : the father of your father or mother [syn: {gramps}, {granddad},
          {grandad}, {granddaddy}, {grandpa}]
