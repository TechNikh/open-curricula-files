---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/underneath
offline_file: ""
offline_thumbnail: ""
uuid: bc2122d1-08c3-4b77-893f-e729d19515e5
updated: 1484310194
title: underneath
categories:
    - Dictionary
---
underneath
     adv 1: on the lower or downward side; "a chest of drawers all
            scratched underneath"
     2: lower down on the page; "diagrams with figures underneath"
     3: beneath by way of support; "a house with a good foundation
        underneath"
     4: under or below an object or a surface; "we could see the
        original painting underneath"
