---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bi-isotropic_material
offline_file: ""
offline_thumbnail: ""
uuid: ed2a0018-fb97-4e02-9cf0-1ce5115e21e8
updated: 1484309292
title: Bi-isotropic material
tags:
    - Definition
    - Coupling constant
    - Classification
    - Examples
categories:
    - Orientation (geometry)
---
In physics, engineering and materials science, bi-isotropic materials have the special optical property that they can rotate the polarization of light in either refraction or transmission. This does not mean all materials with twist effect fall in the bi-isotropic class. The twist effect of the class of bi-isotropic materials is caused by the chirality and non-reciprocity of the structure of the media, in which the electric and magnetic field of an electromagnetic wave (or simply, light) interact in an unusual way.
