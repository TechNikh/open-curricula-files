---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wine
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484479321
title: wine
categories:
    - Dictionary
---
wine
     n 1: fermented juice (of grapes especially) [syn: {vino}]
     2: a red as dark as red wine [syn: {wine-colored}]
     v 1: drink wine
     2: treat to wine; "Our relatives in Italy wined and dined us
        for a week"
