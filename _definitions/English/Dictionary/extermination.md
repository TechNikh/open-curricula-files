---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extermination
offline_file: ""
offline_thumbnail: ""
uuid: 75741dfc-98d8-48f1-806d-e8b57c0c23cd
updated: 1484310565
title: extermination
categories:
    - Dictionary
---
extermination
     n 1: complete annihilation; "they think a meteor cause the
          extinction of the dinosaurs" [syn: {extinction}]
     2: the act of exterminating [syn: {liquidation}]
