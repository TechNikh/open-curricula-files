---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rod
offline_file: ""
offline_thumbnail: ""
uuid: dcf7cc86-934b-4df0-91b8-8824fcda4001
updated: 1484310224
title: rod
categories:
    - Dictionary
---
rod
     n 1: a linear measure of 16.5 feet [syn: {perch}, {pole}]
     2: a long thin implement made of metal or wood
     3: any rod-shaped bacterium
     4: a square rod of land [syn: {perch}, {pole}]
     5: visual receptor cell sensitive to dim light [syn: {rod cell},
         {retinal rod}]
     6: a gangster's pistol [syn: {gat}]
