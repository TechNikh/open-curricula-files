---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/A*_search_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 0eacbfb4-258f-45b4-8a2d-10293f9de954
updated: 1484309001
title: 'A* search algorithm'
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-SRI_Shakey_with_callouts.jpg
tags:
    - History
    - Description
    - Pseudocode
    - Example
    - Properties
    - Special cases
    - Implementation details
    - Admissibility and optimality
    - Bounded relaxation
    - Complexity
    - Applications
    - Relations to other algorithms
    - 'Variants of A*'
    - Notes
categories:
    - Game artificial intelligence
---
In computer science, A* (pronounced as "A star" ( listen)) is a computer algorithm that is widely used in pathfinding and graph traversal, the process of plotting an efficiently traversable path between multiple points, called nodes. It enjoys widespread use due to its performance and accuracy. However, in practical travel-routing systems, it is generally outperformed by algorithms which can pre-process the graph to attain better performance,[1] although other work has found A* to be superior to other approaches.[2]
