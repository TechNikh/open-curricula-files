---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/project
offline_file: ""
offline_thumbnail: ""
uuid: 5ccd1eb9-fcdf-4b14-bdc3-81b0bc1f96af
updated: 1484310258
title: project
categories:
    - Dictionary
---
project
     n 1: any piece of work that is undertaken or attempted; "he
          prepared for great undertakings" [syn: {undertaking}, {task},
           {labor}]
     2: a planned undertaking [syn: {projection}]
     v 1: communicate vividly; "He projected his feelings"
     2: extend out or project in space; "His sharp nose jutted out";
        "A single rock sticks out from the cliff" [syn: {stick out},
         {protrude}, {jut out}, {jut}]
     3: transfer (ideas or principles) from one domain into another
     4: project on a screen; "The images are projected onto the
        screen"
     5: cause to be heard; "His voice projects well"
     6: draw a projection of
     7: make or ...
