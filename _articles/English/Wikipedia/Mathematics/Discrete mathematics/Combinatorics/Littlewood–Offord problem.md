---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Littlewood%E2%80%93Offord_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 6d7cc9a5-76e9-4133-8ed8-037a7d4cd660
updated: 1484308577
title: Littlewood–Offord problem
categories:
    - Combinatorics
---
In mathematical field of combinatorial geometry, the Littlewood–Offord problem is the problem of determining the number of subsums of a set of vectors that fall in a given convex set. More formally, if V is a vector space of dimension d, the problem is to determine, given a finite subset of vectors S and a convex subset A, the number of subsets of S whose summation is in A.
