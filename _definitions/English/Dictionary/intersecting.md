---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intersecting
offline_file: ""
offline_thumbnail: ""
uuid: 60d6f282-e3e1-4c89-afa5-fb45de75025e
updated: 1484310138
title: intersecting
categories:
    - Dictionary
---
intersecting
     adj : crossed or intersected in the form of an X [syn: {decussate},
            {intersectant}]
