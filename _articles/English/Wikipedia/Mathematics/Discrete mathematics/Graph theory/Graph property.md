---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_property
offline_file: ""
offline_thumbnail: ""
uuid: 4f862fff-1dc9-46d8-886f-ac4099762cd7
updated: 1484308632
title: Graph property
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-6n-graf.svg_2.png
tags:
    - Definitions
    - Properties of properties
    - Values of invariants
    - Graph invariants and graph isomorphism
    - Examples
    - Properties
    - Integer invariants
    - Real number invariants
    - Sequences and polynomials
categories:
    - Graph theory
---
In graph theory, a graph property or graph invariant is a property of graphs that depends only on the abstract structure, not on graph representations such as particular labellings or drawings of the graph.[1]
