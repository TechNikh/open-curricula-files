---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/open
offline_file: ""
offline_thumbnail: ""
uuid: 93e9181d-9c44-4411-886d-4916bcee1fbf
updated: 1484310319
title: open
categories:
    - Dictionary
---
open
     adj 1: affording unobstructed entrance and exit; not shut or
            closed; "an open door"; "they left the door open"
            [syn: {unfastened}] [ant: {shut}]
     2: affording free passage or access; "open drains"; "the road
        is open to traffic"; "open ranks" [ant: {closed}]
     3: with no protection or shield; "the exposed northeast
        frontier"; "open to the weather"; "an open wound" [syn: {exposed}]
     4: open to or in view of all; "an open protest"; "an open
        letter to the editor"
     5: used of mouth or eyes; "keep your eyes open"; "his mouth
        slightly opened" [syn: {opened}] [ant: {closed}]
     6: not having been filled; "the job is ...
