---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/provincial
offline_file: ""
offline_thumbnail: ""
uuid: dbf958ab-7cd1-4b68-aff0-589394197a1c
updated: 1484310583
title: provincial
categories:
    - Dictionary
---
provincial
     adj 1: of or associated with a province; "provincial government"
     2: characteristic of the provinces or their people; "deeply
        provincial and conformist"; "in that well-educated company
        I felt uncomfortably provincial"; "narrow provincial
        attitudes" [ant: {cosmopolitan}]
     n 1: (Roman Catholic Church) an official in charge of an
          ecclesiastical province acting under the superior
          general of a religious order; "the general of the
          Jesuits receives monthly reports from the provincials"
     2: a country person [syn: {peasant}, {bucolic}]
