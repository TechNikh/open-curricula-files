---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/B%C3%A4cklund_transform'
offline_file: ""
offline_thumbnail: ""
uuid: 7eed4765-f136-4212-b958-b71441c0593c
updated: 1484309204
title: Bäcklund transform
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pseudosphere.png
tags:
    - History
    - The Cauchy–Riemann equations
    - The sine-Gordon equation
    - The Liouville equation
categories:
    - Differential geometry
---
In mathematics, Bäcklund transforms or Bäcklund transformations (named after the Swedish mathematician Albert Victor Bäcklund) relate partial differential equations and their solutions. They are an important tool in soliton theory and integrable systems. A Bäcklund transform is typically a system of first order partial differential equations relating two functions, and often depending on an additional parameter. It implies that the two functions separately satisfy partial differential equations, and each of the two functions is then said to be a Bäcklund transformation of the other.
