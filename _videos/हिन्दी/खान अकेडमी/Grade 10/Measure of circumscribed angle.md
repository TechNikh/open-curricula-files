---
version: 1
type: video
provider: YouTube
id: 8SCGrEHl0RI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 909f0ee5-77e7-49ef-bb30-4d79676a3b26
updated: 1486069619
title: Measure of circumscribed angle
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/8SCGrEHl0RI/default.jpg
    - https://i3.ytimg.com/vi/8SCGrEHl0RI/1.jpg
    - https://i3.ytimg.com/vi/8SCGrEHl0RI/2.jpg
    - https://i3.ytimg.com/vi/8SCGrEHl0RI/3.jpg
---
