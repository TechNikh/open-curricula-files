---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Simplex
offline_file: ""
offline_thumbnail: ""
uuid: b34db11a-aeb6-4278-9ee4-eb2e0794ae4d
updated: 1484309289
title: Simplex
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tetrahedron.png
tags:
    - Examples
    - Elements
    - Symmetric graphs of regular simplices
    - The standard simplex
    - Examples
    - Increasing coordinates
    - Projection onto the standard simplex
    - Corner of cube
    - >
        Cartesian coordinates for regular n-dimensional simplex in
        Rn
    - Geometric properties
    - Volume
    - Simplexes with an "orthogonal corner"
    - Relation to the (n+1)-hypercube
    - Topology
    - Probability
    - Algebraic topology
    - Algebraic geometry
    - Applications
    - Notes
categories:
    - Multi-dimensional geometry
---
In geometry, a simplex (plural: simplexes or simplices) is a generalization of the notion of a triangle or tetrahedron to arbitrary dimensions. Specifically, a k-simplex is a k-dimensional polytope which is the convex hull of its k + 1 vertices. More formally, suppose the k + 1 points 
  
    
      
        
          u
          
            0
          
        
        ,
        …
        ,
        
          u
          
            k
          
        
        ∈
        
          
            R
          
          
            k
          
        
      
    
    {\displaystyle u_{0},\dots ,u_{k}\in \mathbb {R} ^{k}}
  
 are affinely independent, which means 
  
    
      ...
