---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_scale
offline_file: ""
offline_thumbnail: ""
uuid: ccb189a8-a4e4-4845-82e7-d23d976bdf10
updated: 1484309327
title: Linear scale
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ma%25C3%259Fstabsleiste.png'
categories:
    - Technical drawing
---
A linear scale, also called a bar scale, scale bar, graphic scale, or graphical scale, is a means of visually showing the scale of a map, nautical chart, engineering drawing, or architectural drawing.
