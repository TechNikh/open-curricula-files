---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stochastic_resonance
offline_file: ""
offline_thumbnail: ""
uuid: ec2c048f-1cb6-4320-8d78-5c887f2cd900
updated: 1484308814
title: Stochastic resonance
tags:
    - Technical Description
    - Suprathreshold stochastic resonance
    - Neuroscience/psychology and biology
    - Medicine
    - Signal analysis
    - Bibliography
    - Bibliography for suprathreshold stochastic resonance
categories:
    - Oscillation
---
Stochastic resonance (SR) is a phenomenon where a signal that is normally too weak to be detected by a sensor, can be boosted by adding white noise to the signal, which contains a wide spectrum of frequencies. The frequencies in the white noise corresponding to the original signal's frequencies will resonate with each other, amplifying the original signal while not amplifying the rest of the white noise (thereby increasing the signal-to-noise ratio which makes the original signal more prominent). Further, the added white noise can be enough to be detectable by the sensor, which can then filter it out to effectively detect the original, previously undetectable signal.
