---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gillespie
offline_file: ""
offline_thumbnail: ""
uuid: d84d5e4f-9c73-4443-9ea5-57b6ea22e5cc
updated: 1484308386
title: Gillespie
tags:
    - Surname
    - Places
    - Other
categories:
    - Uncategorized
---
Gillespie (/ɡᵻˈlɛspi/) is both a masculine given name, and a surname in the English language. The given name is an Anglicised form of the Gaelic Gille Easbaig (also rendered Gilleasbaig), meaning "bishop's servant".[1] The surname Gillespie is an Anglicised form of the Scottish Gaelic Mac Gille Easbuig, and the Irish Mac Giolla Easpaig, both of which mean "servant of the bishop".[2] The given name itself is derived from a word of Latin origin.[3] Specifically, the Old Irish epscop being derived from the Latin episcopus.[4] An early example of the name in Scotland occurs in a charter dated 1175–1199, recording a certain "Ewano filio Gillaspeck".[3][5] In Ireland, a family bearing the ...
