---
version: 1
type: video
provider: YouTube
id: 0Dd-y_apbRw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Scientific%20notation%20examples.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 31f63d1a-b6ea-4efd-94c7-7582f9dd4749
updated: 1486069567
title: Scientific notation examples
categories:
    - Pre-Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/0Dd-y_apbRw/default.jpg
    - https://i3.ytimg.com/vi/0Dd-y_apbRw/1.jpg
    - https://i3.ytimg.com/vi/0Dd-y_apbRw/2.jpg
    - https://i3.ytimg.com/vi/0Dd-y_apbRw/3.jpg
---
More scientific notation examples

Practice this lesson yourself on KhanAcademy.org right now: https://www.khanacademy.org/math/pre-algebra/exponents-radicals/scientific-notation/e/scientific_notation?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Watch the next lesson: https://www.khanacademy.org/math/pre-algebra/exponents-radicals/scientific-notation/v/scientific-notation-i?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Missed the previous lesson? 
https://www.khanacademy.org/math/pre-algebra/exponents-radicals/scientific-notation/v/scientific-notation?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Pre-Algebra on Khan Academy: No way, this isn't your run of the mill arithmetic. This is Pre-algebra. You're about to play with the professionals. Think of pre-algebra as a runway. You're the airplane and algebra is your sunny vacation destination. Without the runway you're not going anywhere. Seriously, the foundation for all higher mathematics is laid with many of the concepts that we will introduce to you here: negative numbers, absolute value, factors, multiples, decimals, and fractions to name a few. So buckle up and move your seat into the upright position. We're about to take off!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy’s Pre-Algebra channel:: https://www.youtube.com/channel/UCIMlYkATtXOFswVoCZN7nAA?sub_confirmation=1
Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
