---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uncombined
offline_file: ""
offline_thumbnail: ""
uuid: d112d495-8f4b-4205-97f3-e13e1cf06f40
updated: 1484310413
title: uncombined
categories:
    - Dictionary
---
uncombined
     adj : not joined or united into one [ant: {combined}]
