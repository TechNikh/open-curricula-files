---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carbamoyl
offline_file: ""
offline_thumbnail: ""
uuid: 5f42bc2c-1b7b-4ca4-b4db-536673471048
updated: 1484308403
title: Carbamic acid
tags:
    - Occurrence
    - Derivatives of carbamic acid
categories:
    - Uncategorized
---
Carbamic acid is the compound with the formula NH2COOH. The attachment of the acid group to a nitrogen or amine (instead of carbon) distinguishes it from carboxylic acid and an amide. Many derivatives and analogues of carbamic acid are known. They are generally unstable, reverting to the parent amine and carbon dioxide.[1] The deprotonated anion (or conjugate base) of this functional group is a carbamate. Carbamic acid is a planar molecule.[2]
