---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/venn
offline_file: ""
offline_thumbnail: ""
uuid: 7304b58d-9667-408f-9d24-0707f8c30267
updated: 1484310138
title: venn
categories:
    - Dictionary
---
Venn
     n : English logician who introduced Venn diagrams (1834-1923)
         [syn: {John Venn}]
