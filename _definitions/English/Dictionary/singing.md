---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/singing
offline_file: ""
offline_thumbnail: ""
uuid: 918dde3b-26fc-4c1d-83f4-bdb0e5b78583
updated: 1484310545
title: singing
categories:
    - Dictionary
---
sing
     v 1: deliver by singing; "Sing Christmas carols"
     2: produce tones with the voice; "She was singing while she was
        cooking"; "My brother sings very well"
     3: to make melodious sounds; "The nightingale was singing"
     4: make a whining, ringing, or whistling sound; "the kettle was
        singing"; "the bullet sang past his ear" [syn: {whistle}]
     5: divulge confidential information or secrets;  "Be
        careful--his secretary talks" [syn: {spill the beans}, {let
        the cat out of the bag}, {talk}, {tattle}, {blab}, {peach},
         {babble}, {babble out}, {blab out}] [ant: {keep quiet}]
     [also: {sung}, {singing}, {sang}]
