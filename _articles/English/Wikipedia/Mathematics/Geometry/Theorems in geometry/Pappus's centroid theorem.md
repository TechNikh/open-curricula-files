---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pappus%27s_centroid_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: d5b2821e-9aa0-4576-ac9e-5da6fa5f7d8a
updated: 1484309119
title: "Pappus's centroid theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Pappus_centroid_theorem_areas.gif
tags:
    - The first theorem
    - The second theorem
    - Generalizations
    - Footnotes
categories:
    - Theorems in geometry
---
In mathematics, Pappus's centroid theorem (also known as the Guldinus theorem, Pappus–Guldinus theorem or Pappus's theorem) is either of two related theorems dealing with the surface areas and volumes of surfaces and solids of revolution.
