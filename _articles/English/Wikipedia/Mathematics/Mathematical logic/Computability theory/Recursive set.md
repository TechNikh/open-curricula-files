---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Recursive_set
offline_file: ""
offline_thumbnail: ""
uuid: 30f032ba-43c4-459a-aad9-b3d0b4739ff2
updated: 1484309490
title: Recursive set
tags:
    - Formal definition
    - Examples
    - Properties
categories:
    - Computability theory
---
In computability theory, a set of natural numbers is called recursive, computable or decidable if there is an algorithm which terminates after a finite amount of time and correctly decides whether a given number belongs to the set.
