---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anhydride
offline_file: ""
offline_thumbnail: ""
uuid: 4a4b964b-432a-4fa4-9043-5c2ef2ae99d3
updated: 1484308401
title: Organic acid anhydride
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-FunktionelleGruppen_Carbons%25C3%25A4ureanhydrid.svg.png'
tags:
    - Preparation
    - Reactions
    - Applications and occurrence of acid anhydrides
    - Sulfur analogues
categories:
    - Uncategorized
---
An organic acid anhydride is an acid anhydride that is an organic compound. An acid anhydride is a compound that has two acyl groups bonded to the same oxygen atom.[1] A common type of organic acid anhydride is a carboxylic anhydride, where the parent acid is a carboxylic acid, the formula of the anhydride being (RC(O))2O. Symmetrical acid anhydrides of this type are named by replacing the word acid in the name of the parent carboxylic acid by the word anhydride.[2] Thus, (CH3CO)2O is called acetic anhydride. Mixed (or unsymmetrical) acid anhydrides, such as acetic formic anhydride (see below), are known.
