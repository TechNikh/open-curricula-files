---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/missing
offline_file: ""
offline_thumbnail: ""
uuid: 140b5153-ad10-4d26-88dc-87861a2eb7b0
updated: 1484310397
title: missing
categories:
    - Dictionary
---
missing
     adj 1: not existing; "innovation has been sadly lacking";
            "character development is missing from the book" [syn:
             {lacking(p)}, {nonexistent}, {wanting(a)}]
     2: not able to be found; "missing in action"; "a missing
        person"
