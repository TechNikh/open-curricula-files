---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/call
offline_file: ""
offline_thumbnail: ""
uuid: e317b7e9-3c86-40c6-ba02-f16fd49b46d2
updated: 1484310226
title: call
categories:
    - Dictionary
---
call
     n 1: a telephone connection; "she reported several anonymous
          calls"; "he placed a phone call to London"; "he heard
          the phone ringing but didn't want to take the call"
          [syn: {phone call}, {telephone call}]
     2: a special disposition (as if from a divine source) to pursue
        a particular course; "he was disappointed that he had not
        heard the Call"
     3: a loud utterance; often in protest or opposition; "the
        speaker was interrupted by loud cries from the rear of the
        audience" [syn: {cry}, {outcry}, {yell}, {shout}, {vociferation}]
     4: a demand especially in the phrase "the call of duty" [syn: {claim}]
     5: the ...
