---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hair
offline_file: ""
offline_thumbnail: ""
uuid: 2dc7a8a6-dec3-4c87-bd97-7820a9c9764c
updated: 1484310311
title: hair
categories:
    - Dictionary
---
hair
     n 1: dense growth of hairs covering the body or parts of it (as
          on the human head); helps prevent heat loss; "he combed
          his hair"
     2: a very small distance or space; "they escaped by a
        hair's-breadth"; "they lost the election by a whisker"
        [syn: {hair's-breadth}, {hairsbreadth}, {whisker}]
     3: filamentous hairlike growth on a plant; "peach fuzz" [syn: {fuzz},
         {tomentum}]
     4: any of the cylindrical filaments characteristically growing
        from the epidermis of a mammal; "there is a hair in my
        soup" [syn: {pilus}]
     5: cloth woven from horsehair or camelhair; used for upholstery
        or stiffening in garments ...
