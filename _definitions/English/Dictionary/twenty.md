---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/twenty
offline_file: ""
offline_thumbnail: ""
uuid: 8a50a5cd-c5c6-4c54-83a8-85c6716cf8b3
updated: 1484310293
title: twenty
categories:
    - Dictionary
---
twenty
     adj : denoting a quantity consisting of 20 items or units [syn: {20},
            {xx}]
     n 1: the cardinal number that is the sum of nineteen and one
          [syn: {20}, {XX}]
     2: a United States bill worth 20 dollars [syn: {twenty dollar
        bill}]
