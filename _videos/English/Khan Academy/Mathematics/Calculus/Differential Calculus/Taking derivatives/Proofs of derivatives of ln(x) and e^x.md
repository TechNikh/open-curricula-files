---
version: 1
type: video
provider: YouTube
id: 3nQejB-XPoY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Proofs%20of%20derivatives%20of%20ln%28x%29%20and%20e%5Ex.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1b35c287-62b3-45f4-8f4c-72092f115f35
updated: 1486069693
title: Proofs of derivatives of ln(x) and e^x
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/3nQejB-XPoY/default.jpg
    - https://i3.ytimg.com/vi/3nQejB-XPoY/1.jpg
    - https://i3.ytimg.com/vi/3nQejB-XPoY/2.jpg
    - https://i3.ytimg.com/vi/3nQejB-XPoY/3.jpg
---
