---
version: 1
type: video
provider: YouTube
id: UOeCoMGeEI8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 82ef05da-374e-415c-9642-8b99d1932a3c
updated: 1486069616
title: Foci of an Ellipse
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/UOeCoMGeEI8/default.jpg
    - https://i3.ytimg.com/vi/UOeCoMGeEI8/1.jpg
    - https://i3.ytimg.com/vi/UOeCoMGeEI8/2.jpg
    - https://i3.ytimg.com/vi/UOeCoMGeEI8/3.jpg
---
