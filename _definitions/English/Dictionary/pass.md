---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pass
offline_file: ""
offline_thumbnail: ""
uuid: 9906cc68-379a-4dfb-aaa0-5bf2d9f18780
updated: 1484310328
title: pass
categories:
    - Dictionary
---
pass
     adj : of advancing the ball by throwing it; "a team with a good
           passing attack"; "a pass play" [syn: {passing(a)}, {pass(a)}]
           [ant: {running(a)}]
     n 1: (baseball) an advance to first base by a batter who receives
          four balls; "he worked the pitcher for a base on balls"
          [syn: {base on balls}, {walk}]
     2: (military) a written leave of absence; "he had a pass for
        three days"
     3: (American football) a play that involves one player throwing
        the ball to a teammate; "the coach sent in a passing play
        on third and long" [syn: {passing play}, {passing game}, {passing}]
     4: the location in a range of mountains ...
