---
version: 1
type: video
provider: YouTube
id: hBBVXcB7v4I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a3ba4985-119a-4adf-9371-004fc63779af
updated: 1486069609
title: Translations of polygons
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/hBBVXcB7v4I/default.jpg
    - https://i3.ytimg.com/vi/hBBVXcB7v4I/1.jpg
    - https://i3.ytimg.com/vi/hBBVXcB7v4I/2.jpg
    - https://i3.ytimg.com/vi/hBBVXcB7v4I/3.jpg
---
