---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Space-based_solar_power
offline_file: ""
offline_thumbnail: ""
uuid: 17643c9f-9205-40b0-aad6-fc5139230cde
updated: 1484308864
title: Space-based solar power
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/750px-Earth-moon_0.jpg
tags:
    - History
    - Discontinuation
    - >
        Space Solar Power Exploratory Research and Technology
        program
    - Japan Aerospace Exploration Agency
    - Challenges
    - Potential
    - Drawbacks
    - Design
    - Microwave power transmission
    - Laser power beaming
    - Orbital location
    - Earth-based receiver
    - In space applications
    - Launch costs
    - Building from space
    - From lunar materials launched in orbit
    - On the Moon
    - From an asteroid
    - Gallery
    - Counter arguments
    - Safety
    - Timeline
    - In the 20th century
    - In the 21st century
    - Non-typical configurations and architectural considerations
    - In fiction
    - videos
categories:
    - Thermodynamics
---
Space-based solar power (SBSP) is the concept of collecting solar power in space (using an "SPS", that is, a "solar-power satellite" or a "satellite power system") for use on Earth. It has been in research since the early 1970s.[1][2]
