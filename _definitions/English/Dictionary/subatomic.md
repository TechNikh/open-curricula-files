---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subatomic
offline_file: ""
offline_thumbnail: ""
uuid: 1028a208-3475-4a06-b9ab-f87541004dee
updated: 1484310393
title: subatomic
categories:
    - Dictionary
---
subatomic
     adj 1: of or relating to constituents of the atom or forces within
            the atom; "subatomic particles"; "harnessing subatomic
            energy"
     2: of dimensions smaller than atomic dimensions
