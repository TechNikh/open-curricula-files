---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/categorise
offline_file: ""
offline_thumbnail: ""
uuid: e2c8eec9-c719-4646-9426-69c1bc9a5ba9
updated: 1484310484
title: categorise
categories:
    - Dictionary
---
categorise
     v : place into or assign to a category; "Children learn early on
         to categorize" [syn: {categorize}]
