---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/negligible
offline_file: ""
offline_thumbnail: ""
uuid: 7ad362ed-0f3c-41ab-86e0-a674457cde43
updated: 1484310268
title: negligible
categories:
    - Dictionary
---
negligible
     adj 1: so small as to be meaningless; insignificant; "the effect
            was negligible"
     2: not worth considering; "he considered the prize too paltry
        for the lives it must cost"; "piffling efforts"; "a
        trifling matter" [syn: {paltry}, {trifling}]
