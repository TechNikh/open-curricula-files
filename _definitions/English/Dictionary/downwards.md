---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/downwards
offline_file: ""
offline_thumbnail: ""
uuid: ea655ce2-7988-4394-a1ec-b6d6d89afcfd
updated: 1484310328
title: downwards
categories:
    - Dictionary
---
downwards
     adv : spatially or metaphorically from a higher to a lower level
           or position; "don't fall down"; "rode the lift up and
           skied down"; "prices plunged downward" [syn: {down}, {downward},
            {downwardly}] [ant: {up}, {up}, {up}, {up}]
