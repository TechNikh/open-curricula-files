---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oxidise
offline_file: ""
offline_thumbnail: ""
uuid: 07d5401e-69b7-4a6f-8d21-7296de5e3f60
updated: 1484310198
title: oxidise
categories:
    - Dictionary
---
oxidise
     v 1: add oxygen to or combine with oxygen [syn: {oxidize}, {oxidate}]
          [ant: {deoxidize}, {deoxidize}]
     2: enter into a combination with oxygen or become converted
        into an oxide; "This metal oxidizes easily" [syn: {oxidize},
         {oxidate}]
