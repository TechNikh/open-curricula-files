---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bicircular_matroid
offline_file: ""
offline_thumbnail: ""
uuid: 32e6bf67-95f5-49fd-9af0-d57b88272e0b
updated: 1484308608
title: Bicircular matroid
tags:
    - Circuits
    - Flats
    - As transversal matroids
    - Minors
    - Characteristic polynomial
    - Vector representation
categories:
    - Graph theory
---
In the mathematical subject of matroid theory, the bicircular matroid of a graph G is the matroid B(G) whose points are the edges of G and whose independent sets are the edge sets of pseudoforests of G, that is, the edge sets in which each connected component contains at most one cycle.
