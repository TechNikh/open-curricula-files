---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lemke%E2%80%93Howson_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 2c442dd4-18d7-4ac6-bd11-df2b9c04cce5
updated: 1484309055
title: Lemke–Howson algorithm
categories:
    - Non-cooperative games
---
The Lemke–Howson algorithm [1] is an algorithm that computes a Nash equilibrium of a bimatrix game, named after its inventors, Carlton E. Lemke and J. T. Howson. It is said to be “the best known among the combinatorial algorithms for finding a Nash equilibrium”.[2]
