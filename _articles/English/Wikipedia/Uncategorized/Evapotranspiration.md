---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Evapotranspiration
offline_file: ""
offline_thumbnail: ""
uuid: 17c67134-3481-4498-b4b3-d9722e1fabc0
updated: 1484308438
title: Evapotranspiration
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Surface_water_cycle.svg.png
tags:
    - Water cycle
    - Estimating evapotranspiration
    - Indirect methods
    - Catchment water balance
    - Evapotranspiration of Urban Vegetation
    - Hydrometeorological equations
    - Energy balance
    - Experimental methods for measuring evapotranspiration
    - Remote sensing
    - Eddy covariance
    - Urban landscape plants
    - Potential evapotranspiration
categories:
    - Uncategorized
---
Evapotranspiration (ET) is the sum of evaporation and plant transpiration from the Earth's land and ocean surface to the atmosphere. Evaporation accounts for the movement of water to the air from sources such as the soil, canopy interception, and waterbodies. Transpiration accounts for the movement of water within a plant and the subsequent loss of water as vapor through stomata in its leaves. Evapotranspiration is an important part of the water cycle. An element (such as a tree) that contributes to evapotranspiration can be called an evapotranspirator.[1]
