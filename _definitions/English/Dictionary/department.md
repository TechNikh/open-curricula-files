---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/department
offline_file: ""
offline_thumbnail: ""
uuid: 53fe38c4-8eb5-401b-9440-c0571edd97fb
updated: 1484310479
title: department
categories:
    - Dictionary
---
department
     n 1: a specialized division of a large organization; "you'll find
          it in the hardware department"; "she got a job in the
          historical section of the Treasury" [syn: {section}]
     2: the territorial and administrative division of some
        countries (such as France)
     3: a specialized sphere of knowledge; "baking is not my
        department"; "his work established a new department of
        literature"
