---
version: 1
type: video
provider: YouTube
id: Yr8aheN5OrU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20AstroPhysics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c07e48f6-0db6-4a6b-8017-cb4756196ac0
updated: 1486069654
title: What is AstroPhysics
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Yr8aheN5OrU/default.jpg
    - https://i3.ytimg.com/vi/Yr8aheN5OrU/1.jpg
    - https://i3.ytimg.com/vi/Yr8aheN5OrU/2.jpg
    - https://i3.ytimg.com/vi/Yr8aheN5OrU/3.jpg
---
