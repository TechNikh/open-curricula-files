---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cpi
offline_file: ""
offline_thumbnail: ""
uuid: 73ce09fe-0cbe-4cfe-958f-4f290b6a820a
updated: 1484310589
title: cpi
categories:
    - Dictionary
---
CPI
     n 1: an index of the cost of all goods and services to a typical
          consumer [syn: {consumer price index}, {cost-of-living
          index}]
     2: a self-report personality inventory originally derived from
        the MMPI; consists of several hundred yes-no questions and
        yields scores on a number of scales including dominance
        and self acceptance and self control and socialization and
        achievement etc. [syn: {California Personality Inventory}]
