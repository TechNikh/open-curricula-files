---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/can
offline_file: ""
offline_thumbnail: ""
uuid: 0ba49a5b-d522-4de6-8085-218c69cff053
updated: 1484310357
title: can
categories:
    - Dictionary
---
can
     n 1: airtight sealed metal container for food or drink or paint
          etc. [syn: {tin}, {tin can}]
     2: the quantity contained in a can [syn: {canful}]
     3: a buoy with a round bottom and conical top [syn: {can buoy}]
     4: the fleshy part of the human body that you sit on; "he
        deserves a good kick in the butt"; "are you going to sit
        on your fanny and do nothing?" [syn: {buttocks}, {nates},
        {arse}, {butt}, {backside}, {bum}, {buns}, {fundament}, {hindquarters},
         {hind end}, {keister}, {posterior}, {prat}, {rear}, {rear
        end}, {rump}, {stern}, {seat}, {tail}, {tail end}, {tooshie},
         {tush}, {bottom}, {behind}, {derriere}, ...
