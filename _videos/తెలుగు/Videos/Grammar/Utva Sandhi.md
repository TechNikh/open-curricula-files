---
version: 1
type: video
provider: YouTube
id: Rlg0OMvsIyU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c426b103-77ea-40e5-9c70-e135b0e3dea6
updated: 1486069626
title: Utva Sandhi
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/Rlg0OMvsIyU/default.jpg
    - https://i3.ytimg.com/vi/Rlg0OMvsIyU/1.jpg
    - https://i3.ytimg.com/vi/Rlg0OMvsIyU/2.jpg
    - https://i3.ytimg.com/vi/Rlg0OMvsIyU/3.jpg
---
