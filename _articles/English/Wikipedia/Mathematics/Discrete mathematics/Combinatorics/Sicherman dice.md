---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sicherman_dice
offline_file: ""
offline_thumbnail: ""
uuid: 16c04f44-e6c3-47c4-a8c2-3aaf630134ce
updated: 1484308577
title: Sicherman dice
tags:
    - Mathematics
    - History
    - Mathematical justification
categories:
    - Combinatorics
---
Sicherman dice /ˈsɪkərmən/ are the only pair of 6-sided dice that are not normal dice, bear only positive integers, and have the same probability distribution for the sum as normal dice.
