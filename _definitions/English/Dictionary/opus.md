---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/opus
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484528041
title: opus
categories:
    - Dictionary
---
opus
     n : a musical work that has been created; "the composition is
         written in four movements" [syn: {musical composition}, {composition},
          {piece}, {piece of music}]
