---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorial_data_analysis
offline_file: ""
offline_thumbnail: ""
uuid: 9a710032-e164-4d99-8630-54a8b0f99b65
updated: 1484308577
title: Combinatorial data analysis
categories:
    - Combinatorics
---
Combinatorial data analysis (CDA) is the study of data sets where the arrangement of objects is important. CDA can be used either to determine how well a given combinatorial construct reflects the observed data, or to search for a suitable combinatorial construct that does fit the data.[1][2][3]
