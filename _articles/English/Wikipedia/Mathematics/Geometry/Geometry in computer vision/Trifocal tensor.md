---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trifocal_tensor
offline_file: ""
offline_thumbnail: ""
uuid: 2c713a5b-90a7-46f9-8616-8353449163e0
updated: 1484309181
title: Trifocal tensor
tags:
    - Correlation slices
    - Trilinear constraints
    - Transfer
categories:
    - Geometry in computer vision
---
In computer vision, the trifocal tensor (also tritensor) is a 3×3×3 array of numbers (i.e., a tensor) that incorporates all projective geometric relationships among three views. It relates the coordinates of corresponding points or lines in three views, being independent of the scene structure and depending only on the relative motion (i.e., pose) among the three views and their intrinsic calibration parameters. Hence, the trifocal tensor can be considered as the generalization of the fundamental matrix in three views. It is noted that despite that the tensor is made up of 27 elements, only 18 of them are actually independent.
