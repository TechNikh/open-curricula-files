---
version: 1
type: video
provider: YouTube
id: ND-Bbp_q46s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Functions%20and%20their%20graphs/New%20operator%20definitions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2f16c02a-13d9-47ec-b4d8-7f01b6376e37
updated: 1486069571
title: New operator definitions
categories:
    - Functions and their graphs
thumbnail_urls:
    - https://i3.ytimg.com/vi/ND-Bbp_q46s/default.jpg
    - https://i3.ytimg.com/vi/ND-Bbp_q46s/1.jpg
    - https://i3.ytimg.com/vi/ND-Bbp_q46s/2.jpg
    - https://i3.ytimg.com/vi/ND-Bbp_q46s/3.jpg
---
Getting comfortable with evaluating newly defined function operators

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/algebra2/functions_and_graphs/new_operators/e/new_definitions_1?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Watch the next lesson: https://www.khanacademy.org/math/algebra2/functions_and_graphs/new_operators/v/new-definitions-2?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Missed the previous lesson? 
https://www.khanacademy.org/math/algebra2/functions_and_graphs/undefined_indeterminate/v/undefined-and-indeterminate?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Algebra II on Khan Academy: Your studies in algebra 1 have built a solid foundation from which you can explore linear equations, inequalities, and functions. In algebra 2 we build upon that foundation and not only extend our knowledge of algebra 1, but slowly become capable of tackling the BIG questions of the universe. We'll again touch on systems of equations, inequalities, and functions...but we'll also address exponential and logarithmic functions, logarithms, imaginary and complex numbers, conic sections, and matrices. Don't let these big words intimidate you. We're on this journey with you!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Algebra II channel:
https://www.youtube.com/channel/UCsCA3_VozRtgUT7wWC1uZDg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
