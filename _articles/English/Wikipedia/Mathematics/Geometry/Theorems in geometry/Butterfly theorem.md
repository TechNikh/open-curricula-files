---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Butterfly_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 2a0e4759-98c1-4d5f-a026-7d94b6a0d3b8
updated: 1484309110
title: Butterfly theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/245px-Butterfly_theorem.svg.png
tags:
    - Proof
    - History
categories:
    - Theorems in geometry
---
Let M be the midpoint of a chord PQ of a circle, through which two other chords AB and CD are drawn; AD and BC intersect chord PQ at X and Y correspondingly. Then M is the midpoint of XY.
