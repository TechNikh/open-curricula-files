---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drained
offline_file: ""
offline_thumbnail: ""
uuid: 8c6617b0-28f6-4fca-b1d5-08c49abbf471
updated: 1484310459
title: drained
categories:
    - Dictionary
---
drained
     adj 1: emptied or exhausted of (as by drawing off e.g. water or
            other liquid); "a drained marsh"; "a drained tank"; "a
            drained and apathetic old man...not caring any longer
            about anything" [ant: {undrained}]
     2: very tired [syn: {knackered}]
     3: drained of electric charge; discharged; "a dead battery";
        "left the lights on and came back to find the battery
        drained" [syn: {dead}]
