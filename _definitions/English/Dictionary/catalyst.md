---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/catalyst
offline_file: ""
offline_thumbnail: ""
uuid: 436d9f1e-c81a-4896-9201-8c6ff2e2ca4a
updated: 1484310373
title: catalyst
categories:
    - Dictionary
---
catalyst
     n 1: (chemistry) a substance that initiates or accelerates a
          chemical reaction without itself being affected [syn: {accelerator}]
          [ant: {anticatalyst}]
     2: something that causes an important event to happen; "the
        invasion acted as a catalyst to unite the country"
