---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rafael_Robb
offline_file: ""
offline_thumbnail: ""
uuid: bda334e2-0b3f-42c5-9697-052a2c190ba5
updated: 1484309026
title: Rafael Robb
tags:
    - Career
    - Killing of wife
    - Notable Publications
categories:
    - Game theorists
---
Rafael Robb (born October 31, 1950)[1] is an economist and former professor at the University of Pennsylvania who confessed to killing his wife in 2006.
