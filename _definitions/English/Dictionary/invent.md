---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484422021
title: invent
categories:
    - Dictionary
---
invent
     v 1: come up with (an idea, plan, explanation, theory, or
          priciple) after a mental effort; "excogitate a way to
          measure the speed of light" [syn: {contrive}, {devise},
          {excogitate}, {formulate}, {forge}]
     2: make up something artificial or untrue [syn: {fabricate}, {manufacture},
         {cook up}, {make up}]
