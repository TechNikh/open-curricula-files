---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recognise
offline_file: ""
offline_thumbnail: ""
uuid: 0ddc112b-fe64-4f91-a261-610384629aa8
updated: 1484310361
title: recognise
categories:
    - Dictionary
---
recognise
     v 1: show approval or appreciation of; "My work is not recognized
          by anybody!"; "The best student was recognized by the
          Dean" [syn: {recognize}]
     2: grant credentials to; "The Regents officially recognized the
        new educational institution"; "recognize an academic
        degree" [syn: {accredit}, {recognize}]
     3: detect with the senses; "The fleeing convicts were picked
        out of the darkness by the watchful prison guards"; "I
        can't make out the faces in this photograph" [syn: {recognize},
         {distinguish}, {discern}, {pick out}, {make out}, {tell
        apart}]
     4: express greetings upon meeting someone [syn: ...
