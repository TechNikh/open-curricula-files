---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nikolay_Nekhoroshev
offline_file: ""
offline_thumbnail: ""
uuid: b7320ddd-31ed-4548-a40b-208a8df3b6f0
updated: 1484308692
title: Nikolay Nekhoroshev
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kolya-08-12-09_1350bwe.jpg
categories:
    - Dynamical systems theorists
---
Nikolai Nikolaevich Nekhoroshev (Russian: Николай Николаевич Нехорошев; 2 October 1946 – 18 October 2008) was a prominent Soviet Russian mathematician, specializing in classical mechanics and dynamical systems. His research concerned Hamiltonian mechanics, perturbation theory, celestial mechanics, integrable systems, dynamical systems, the quasiclassical approximation, and singularity theory. He proved, in particular, a stability result in KAM-theory stating that, under certain conditions, solutions of nearly integrable systems stay close to invariant tori for exponentially long times (Giorgilli 1989).
