---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/start
offline_file: ""
offline_thumbnail: ""
uuid: b632d100-c9e0-49b7-9e0d-c324fbe9e4f5
updated: 1484310357
title: start
categories:
    - Dictionary
---
start
     n 1: the beginning of anything; "it was off to a good start"
     2: the time at which something is supposed to begin; "they got
        an early start"; "she knew from the get-go that he was the
        man for her" [syn: {beginning}, {commencement}, {first}, {outset},
         {get-go}, {kickoff}, {starting time}, {showtime}, {offset}]
        [ant: {middle}, {end}]
     3: a turn to be a starter (in a game at the beginning); "he got
        his start because one of the regular pitchers was in the
        hospital"; "his starting meant that the coach thought he
        was one of their best linemen" [syn: {starting}]
     4: a sudden involuntary movement; "he awoke with a ...
