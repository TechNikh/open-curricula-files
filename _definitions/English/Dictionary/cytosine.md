---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cytosine
offline_file: ""
offline_thumbnail: ""
uuid: 52181a10-5221-46d4-8267-3f2f41f7ef85
updated: 1484310295
title: cytosine
categories:
    - Dictionary
---
cytosine
     n : a base found in DNA and RNA and derived from pyrimidine;
         pairs with guanine [syn: {C}]
