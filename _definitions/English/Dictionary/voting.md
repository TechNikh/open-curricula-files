---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/voting
offline_file: ""
offline_thumbnail: ""
uuid: f5ad2316-9b2f-4b5a-8055-0f71ea363e0a
updated: 1484310529
title: voting
categories:
    - Dictionary
---
voting
     n : a choice that is made by voting; "there were only 17 votes
         in favor of the motion" [syn: {vote}, {ballot}, {balloting}]
