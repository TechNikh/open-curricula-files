---
version: 1
type: video
provider: YouTube
id: DBMQOK64VQY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/Punctuating%20a%20list.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 173dfe4b-7934-4807-9f8a-594684f0b98f
updated: 1486069595
title: Punctuating a list
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Punctuation
thumbnail_urls:
    - https://i3.ytimg.com/vi/DBMQOK64VQY/default.jpg
    - https://i3.ytimg.com/vi/DBMQOK64VQY/1.jpg
    - https://i3.ytimg.com/vi/DBMQOK64VQY/2.jpg
    - https://i3.ytimg.com/vi/DBMQOK64VQY/3.jpg
---
Learn how to use commas to punctuate a written list of people, things, actions, or events. 

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/e/punctuating-lists?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/v/more-uses-for-commas-the-comma-punctuation-khan-academy?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/v/commas-and-introductory-elements-the-comma-punctuation-khan-academy?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
