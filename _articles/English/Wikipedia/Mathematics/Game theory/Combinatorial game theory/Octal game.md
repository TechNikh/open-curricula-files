---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Octal_game
offline_file: ""
offline_thumbnail: ""
uuid: 64637d2d-333e-4f67-bbfa-0610f2467a4d
updated: 1484308538
title: Octal game
tags:
    - Game specification
    - Particular octal games
    - Nim
    - Kayles
    - "Dawson's Chess"
    - "Dawson's Kayles"
    - Generalization to other bases
    - Nim-sequence
    - Computation records
categories:
    - Combinatorial game theory
---
The octal games are a class of two-player games that involve removing tokens (game pieces or stones) from heaps of tokens. They have been studied in combinatorial game theory as a generalization of Nim, Kayles, and similar games.[1][2]
