---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/henry
offline_file: ""
offline_thumbnail: ""
uuid: b23641e0-a267-43c0-8ac5-b383b1433d4d
updated: 1484310150
title: henry
categories:
    - Dictionary
---
henry
     n 1: a unit of inductance in which an induced electromotive force
          of one volt is produced when the current is varied at
          the rate of one ampere per second [syn: {H}]
     2: English chemist who studied the quantities of gas absorbed
        by water at different temperatures and under different
        pressures (1775-1836) [syn: {William Henry}]
     3: a leader of the American Revolution and a famous orator who
        spoke out against British rule of the American colonies
        (1736-1799) [syn: {Patrick Henry}]
     4: United States physicist who studied electromagnetic
        phenomena (1791-1878) [syn: {Joseph Henry}]
