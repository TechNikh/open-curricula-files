---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lake
offline_file: ""
offline_thumbnail: ""
uuid: 05b159d1-1794-4844-b5a7-a5496ee29965
updated: 1484310461
title: lake
categories:
    - Dictionary
---
lake
     n 1: a body of (usually fresh) water surrounded by land
     2: a purplish red pigment prepared from lac or cochineal
     3: any of numerous bright translucent organic pigments
