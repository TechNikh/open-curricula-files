---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Road_coloring_theorem
offline_file: ""
offline_thumbnail: ""
uuid: c555c689-ce96-4af0-971d-156d2db776d2
updated: 1484308590
title: Road coloring theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Road_coloring_conjecture.svg.png
tags:
    - Example and intuition
    - Mathematical description
    - Previous partial results
    - Notes
categories:
    - Combinatorics
---
In graph theory the road coloring theorem, known until recently as the road coloring conjecture, deals with synchronized instructions. The issue involves whether by using such instructions, one can reach or locate an object or destination from any other point within a network (which might be a representation of city streets or a maze).[1] In the real world, this phenomenon would be as if you called a friend to ask for directions to his house, and he gave you a set of directions that worked no matter where you started from. This theorem also has implications in symbolic dynamics.
