---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Route_inspection_problem
offline_file: ""
offline_thumbnail: ""
uuid: 0e09bbd3-0254-4f5d-904b-687b7dde9e37
updated: 1484309396
title: Route inspection problem
tags:
    - Undirected solution
    - Directed solution
    - Windy postman problem
    - Applications
    - Variants
categories:
    - Computational problems in graph theory
---
In graph theory, a branch of mathematics and computer science, the Chinese postman problem (CPP), postman tour or route inspection problem is to find a shortest closed path or circuit that visits every edge of a (connected) undirected graph. When the graph has an Eulerian circuit (a closed walk that covers every edge once), that circuit is an optimal solution. Otherwise, the optimization problem is to find the smallest number of graph edges to duplicate (or the subset of edges with the minimum possible total weight) so that the resulting multigraph does have an Eulerian circuit.[1] It may be solved in polynomial time.[2]
