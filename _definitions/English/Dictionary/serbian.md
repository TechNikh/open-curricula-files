---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/serbian
offline_file: ""
offline_thumbnail: ""
uuid: f6b86b5a-ea6c-4daf-9f6e-7320fe51cbd1
updated: 1484310549
title: serbian
categories:
    - Dictionary
---
Serbian
     adj : of or relating to the people or language or culture of the
           region of Serbia
     n : a member of a Slavic people who settled in Serbia and
         neighboring areas in the 6th and 7th centuries [syn: {Serb}]
