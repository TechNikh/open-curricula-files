---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relax
offline_file: ""
offline_thumbnail: ""
uuid: 5ed845fd-26e6-4f11-bbe8-afc6840fa441
updated: 1484310330
title: relax
categories:
    - Dictionary
---
relax
     v 1: become less tense, rest, or take one's ease; "He relaxed in
          the hot tub"; "Let's all relax after a hard day's work"
          [syn: {loosen up}, {unbend}, {unwind}, {decompress}, {slow
          down}] [ant: {tense}]
     2: make less tight; "relax the tension on the rope" [syn: {unbend}]
     3: become loose or looser or less tight; "The noose loosened";
        "the rope relaxed" [syn: {loosen}, {loose}] [ant: {stiffen}]
     4: cause to feel relaxed; "A hot bath always relaxes me" [syn:
        {unstrain}, {unlax}, {loosen up}, {unwind}, {make relaxed}]
        [ant: {tense}, {tense}]
     5: become less tense, less formal, or less restrained, and
        assume ...
