---
version: 1
type: video
provider: YouTube
id: S2zLnGPwYoY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Eureka%2020%20-%20Measuring%20Temperature.mov.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a42c8a4c-0879-4c0c-a6aa-a1b82cb4e8f4
updated: 1486069665
title: 'Eureka 20 - Measuring Temperature.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/S2zLnGPwYoY/default.jpg
    - https://i3.ytimg.com/vi/S2zLnGPwYoY/1.jpg
    - https://i3.ytimg.com/vi/S2zLnGPwYoY/2.jpg
    - https://i3.ytimg.com/vi/S2zLnGPwYoY/3.jpg
---
