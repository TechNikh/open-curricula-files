---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elevation
offline_file: ""
offline_thumbnail: ""
uuid: 681c8c47-1088-4be0-8c91-5e9e9bb53802
updated: 1484310214
title: elevation
categories:
    - Dictionary
---
elevation
     n 1: the event of something being raised upward; "an elevation of
          the temperature in the afternoon"; "a raising of the
          land resulting from volcanic activity" [syn: {lift}, {raising}]
     2: the highest level or degree attainable; "his landscapes were
        deemed the acme of beauty"; "the artist's gifts are at
        their acme"; "at the height of her career"; "the peak of
        perfection"; "summer was at its peak"; "...catapulted
        Einstein to the pinnacle of fame"; "the summit of his
        ambition"; "so many highest superlatives achieved by man";
        "at the top of his profession" [syn: {acme}, {height}, {peak},
         {pinnacle}, ...
