---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neutralization
offline_file: ""
offline_thumbnail: ""
uuid: 9054d177-2efa-4d45-8695-0bd0a152dc5a
updated: 1484310381
title: neutralization
categories:
    - Dictionary
---
neutralization
     n 1: a chemical reaction in which an acid and a base interact
          with the formation of a salt; with strong acids and
          bases the essential reaction is the combination of
          hydrogen ions with hydroxyl ions to form water [syn: {neutralisation},
           {neutralization reaction}, {neutralisation reaction}]
     2: action intended to keep a country politically neutral or
        exclude it from a possible war; "the neutralization of
        Belgium" [syn: {neutralisation}]
     3: (euphemism) the removal of a threat by killing or destroying
        it (especially in a covert operation or military
        operation) [syn: {neutralisation}]
     4: ...
