---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/engrossed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484574721
title: engrossed
categories:
    - Dictionary
---
engrossed
     adj 1: wholly absorbed as in thought; "deep in thought"; "that
            engrossed look or rapt delight"; "the book had her
            totally engrossed"; "enwrapped in dreams"; "so intent
            on this fantastic...narrative that she hardly
            stirred"- Walter de la Mare; "rapt with wonder";
            "wrapped in thought" [syn: {absorbed}, {enwrapped}, {intent},
             {rapt}, {wrapped}]
     2: written formally in a large clear script, as a deed or other
        legal document
