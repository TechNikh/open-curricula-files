---
version: 1
type: video
provider: YouTube
id: Ux2pmF2JK2Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2351f4d4-8bd5-4e8a-b6c2-d2f107305232
updated: 1486069648
title: A Fractal Symphonic Odyssey
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ux2pmF2JK2Y/default.jpg
    - https://i3.ytimg.com/vi/Ux2pmF2JK2Y/1.jpg
    - https://i3.ytimg.com/vi/Ux2pmF2JK2Y/2.jpg
    - https://i3.ytimg.com/vi/Ux2pmF2JK2Y/3.jpg
---
