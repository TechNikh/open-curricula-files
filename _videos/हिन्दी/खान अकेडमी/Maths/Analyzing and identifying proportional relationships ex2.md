---
version: 1
type: video
provider: YouTube
id: DgZzrFXeWY0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: de88027d-2005-482f-871e-9987613917c6
updated: 1486069611
title: Analyzing and identifying proportional relationships ex2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/DgZzrFXeWY0/default.jpg
    - https://i3.ytimg.com/vi/DgZzrFXeWY0/1.jpg
    - https://i3.ytimg.com/vi/DgZzrFXeWY0/2.jpg
    - https://i3.ytimg.com/vi/DgZzrFXeWY0/3.jpg
---
