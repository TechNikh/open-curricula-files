---
version: 1
type: video
provider: YouTube
id: s8fqGQXhrrU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Active%20and%20Passive%20English%20Grammar.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b4034061-1d85-41b1-a4aa-c493912a467d
updated: 1486069631
title: Active and Passive English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/s8fqGQXhrrU/default.jpg
    - https://i3.ytimg.com/vi/s8fqGQXhrrU/1.jpg
    - https://i3.ytimg.com/vi/s8fqGQXhrrU/2.jpg
    - https://i3.ytimg.com/vi/s8fqGQXhrrU/3.jpg
---
