---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/european
offline_file: ""
offline_thumbnail: ""
uuid: a1ca7972-abd2-437f-9edf-84ace073f79f
updated: 1484310525
title: european
categories:
    - Dictionary
---
European
     adj : of or relating to or characteristic of Europe or the people
           of Europe; "European Community"
     n : a native or inhabitant of Europe
