---
version: 1
type: video
provider: YouTube
id: qKbwpqSB0B0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5e10fabe-edc7-486d-8a41-521ad515ac72
updated: 1486069629
title: >
    Types of Compound in Hindi / समास के
    प्रकार एवम् भेद | Hindi Grammar
    Class 7
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/qKbwpqSB0B0/default.jpg
    - https://i3.ytimg.com/vi/qKbwpqSB0B0/1.jpg
    - https://i3.ytimg.com/vi/qKbwpqSB0B0/2.jpg
    - https://i3.ytimg.com/vi/qKbwpqSB0B0/3.jpg
---
