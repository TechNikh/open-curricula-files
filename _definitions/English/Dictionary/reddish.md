---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reddish
offline_file: ""
offline_thumbnail: ""
uuid: e502f323-b833-4f79-9e0e-648618b7b365
updated: 1484310375
title: reddish
categories:
    - Dictionary
---
reddish
     adj : having any of numerous bright or strong colors reminiscent
           of the color of blood or cherries or tomatoes or rubies
           [syn: {red}, {ruddy}, {blood-red}, {carmine}, {cerise},
            {cherry}, {cherry-red}, {crimson}, {ruby}, {ruby-red},
            {scarlet}]
