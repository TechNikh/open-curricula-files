---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Principle_of_minimum_energy
offline_file: ""
offline_thumbnail: ""
uuid: cad4f407-9155-4d58-86ce-0e4c0b83c5e6
updated: 1484308857
title: Principle of minimum energy
tags:
    - Mathematical explanation
    - Examples
    - Thermodynamic potentials
categories:
    - Thermodynamics
---
The principle of minimum energy is essentially a restatement of the second law of thermodynamics. It states that for a closed system, with constant external parameters and entropy, the internal energy will decrease and approach a minimum value at equilibrium. External parameters generally means the volume, but may include other parameters which are specified externally, such as a constant magnetic field.
