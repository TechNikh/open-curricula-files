---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calorie
offline_file: ""
offline_thumbnail: ""
uuid: 018c9219-0390-4191-8d8c-38703f179a23
updated: 1484310232
title: calorie
categories:
    - Dictionary
---
calorie
     n 1: unit of heat defined as the quantity of heat required to
          raise the temperature of 1 gram of water by 1 degree
          centigrade at atmospheric pressure [syn: {gram calorie},
           {small calorie}]
     2: a unit of heat equal to the amount of heat required to raise
        the temperature of one kilogram of water by one degree at
        one atmosphere pressure; used by nutritionists to
        characterize the energy-producing potential in food [syn:
        {kilogram calorie}, {kilocalorie}, {large calorie}, {nutritionist's
        calorie}]
