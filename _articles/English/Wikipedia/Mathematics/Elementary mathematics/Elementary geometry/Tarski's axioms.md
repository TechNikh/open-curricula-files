---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tarski%27s_axioms'
offline_file: ""
offline_thumbnail: ""
uuid: ac79f757-2943-48f6-9864-2cfe0ed1bc96
updated: 1484308946
title: "Tarski's axioms"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tarski%2527s_formulation_of_Pasch%2527s_axiom.svg.png'
tags:
    - Overview
    - The axioms
    - Fundamental relations
    - Congruence axioms
    - Commentary
    - Betweenness axioms
    - Congruence and betweenness
    - Discussion
    - Comparison with Hilbert
    - Notes
categories:
    - Elementary geometry
---
Tarski's axioms, due to Alfred Tarski, are an axiom set for the substantial fragment of Euclidean geometry, called "elementary," that is formulable in first-order logic with identity, and requiring no set theory (Tarski 1959). Other modern axiomizations of Euclidean geometry are those by Hilbert and George Birkhoff.
