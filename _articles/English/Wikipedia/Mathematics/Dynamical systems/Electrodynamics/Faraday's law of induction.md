---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Faraday%27s_law_of_induction'
offline_file: ""
offline_thumbnail: ""
uuid: ddfa2a2a-9e6b-42ca-bfb2-6e3ad24951e7
updated: 1484308754
title: "Faraday's law of induction"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Faraday_emf_experiment.svg.png
tags:
    - History
    - "Faraday's law"
    - Qualitative statement
    - Quantitative
    - Maxwell–Faraday equation
    - "Proof of Faraday's law"
    - |
        "Counterexamples" to Faraday's law
    - "Faraday's law and relativity"
    - Two phenomena
    - "Einstein's view"
categories:
    - Electrodynamics
---
Faraday's law of induction is a basic law of electromagnetism predicting how a magnetic field will interact with an electric circuit to produce an electromotive force (EMF)—a phenomenon called electromagnetic induction. It is the fundamental operating principle of transformers, inductors, and many types of electrical motors, generators and solenoids.[1][2]
