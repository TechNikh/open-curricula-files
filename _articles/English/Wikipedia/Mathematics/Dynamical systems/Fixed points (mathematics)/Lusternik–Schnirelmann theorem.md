---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lusternik%E2%80%93Schnirelmann_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: d5e2430c-2d98-4ca1-962d-161faea78846
updated: 1484308782
title: Lusternik–Schnirelmann theorem
categories:
    - Fixed points (mathematics)
---
If the sphere Sn is covered by n + 1 open sets, then one of these sets contains a pair (x, −x) of antipodal points.
