---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-information
offline_file: ""
offline_thumbnail: ""
uuid: 490d1c8a-b2ac-4874-ba25-df7086631fdb
updated: 1484308770
title: Self-information
tags:
    - Definition
    - Examples
    - Self-information of a partitioning
    - Relationship to entropy
categories:
    - Entropy and information
---
In information theory, self-information or surprisal is a measure of the information content[clarification needed] associated with an event in a probability space or with the value of a discrete random variable. It is expressed in a unit of information, for example bits, nats, or hartleys, depending on the base of the logarithm used in its calculation.
