---
version: 1
type: video
provider: YouTube
id: 1mPFTYrG4qA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 241da6d0-b3e6-408b-96f3-59970d6250a1
updated: 1486069657
title: 'Electric charge - physics experiment'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/1mPFTYrG4qA/default.jpg
    - https://i3.ytimg.com/vi/1mPFTYrG4qA/1.jpg
    - https://i3.ytimg.com/vi/1mPFTYrG4qA/2.jpg
    - https://i3.ytimg.com/vi/1mPFTYrG4qA/3.jpg
---
