---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leafy
offline_file: ""
offline_thumbnail: ""
uuid: 679c6093-3097-4412-a108-2e8fda86ae3c
updated: 1484310535
title: leafy
categories:
    - Dictionary
---
leafy
     adj : having or covered with leaves; "leafy trees"; "leafy
           vegetables" [ant: {leafless}]
     [also: {leafiest}, {leafier}]
