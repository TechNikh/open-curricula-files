---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Piecewise_syndetic_set
offline_file: ""
offline_thumbnail: ""
uuid: 3429f4a2-c8a3-4bdd-ac88-327e5917cad7
updated: 1484308577
title: Piecewise syndetic set
tags:
    - Properties
    - Other Notions of Largeness
    - Notes
categories:
    - Combinatorics
---
A set 
  
    
      
        S
        ⊂
        
          N
        
      
    
    {\displaystyle S\subset \mathbb {N} }
  
 is called piecewise syndetic if there exists a finite subset G of 
  
    
      
        
          N
        
      
    
    {\displaystyle \mathbb {N} }
  
 such that for every finite subset F of 
  
    
      
        
          N
        
      
    
    {\displaystyle \mathbb {N} }
  
 there exists an 
  
    
      
        x
        ∈
        
          N
        
      
    
    {\displaystyle x\in \mathbb {N} }
  
 such that
