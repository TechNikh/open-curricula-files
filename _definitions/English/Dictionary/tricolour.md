---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tricolour
offline_file: ""
offline_thumbnail: ""
uuid: 0cf8d4cb-77c5-474d-897d-7b60fef8ff68
updated: 1484310587
title: tricolour
categories:
    - Dictionary
---
tricolour
     n : a flag having three colored stripes (especially the French
         flag) [syn: {tricolor}]
