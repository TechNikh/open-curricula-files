---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/named
offline_file: ""
offline_thumbnail: ""
uuid: ee874394-8b7d-4656-939f-602d5e5376f1
updated: 1484310297
title: named
categories:
    - Dictionary
---
named
     adj 1: given or having a specified name; "they called his name
            Jesus"; "forces...which Empedocles called `love' and
            `hate'"; "an actor named Harold Lloyd"; "a building in
            Cardiff named the Temple of Peace" [syn: {called}]
     2: bearing the author's name; "a named source"
