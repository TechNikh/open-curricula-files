---
version: 1
type: video
provider: YouTube
id: 10EnRI80zvk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 76db7260-0985-488d-ac63-1b73e5f73f97
updated: 1486069660
title: 'Magic Sand - Sand that is Always Dry!'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/10EnRI80zvk/default.jpg
    - https://i3.ytimg.com/vi/10EnRI80zvk/1.jpg
    - https://i3.ytimg.com/vi/10EnRI80zvk/2.jpg
    - https://i3.ytimg.com/vi/10EnRI80zvk/3.jpg
---
