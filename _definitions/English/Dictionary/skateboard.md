---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/skateboard
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484406301
title: skateboard
categories:
    - Dictionary
---
skateboard
     n : a board with wheels that is ridden in a standing or
         crouching position
     v : ride on a flat board with rollers attached to the bottom
