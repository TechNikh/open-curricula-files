---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/successful
offline_file: ""
offline_thumbnail: ""
uuid: 00fc5de4-3979-45ef-8c2f-73fcb7d64d06
updated: 1484310389
title: successful
categories:
    - Dictionary
---
successful
     adj : having succeeded or being marked by a favorable outcome; "a
           successful architect"; "a successful business venture"
           [ant: {unsuccessful}]
