---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sight
offline_file: ""
offline_thumbnail: ""
uuid: 7c8abd24-2cbc-4923-bd8f-24144c345c41
updated: 1484310353
title: sight
categories:
    - Dictionary
---
sight
     n 1: an instance of visual perception; "the sight of his wife
          brought him back to reality"; "the train was an
          unexpected sight"
     2: anything that is seen; "he was a familiar sight on the
        television"; "they went to Paris to see the sights"
     3: the ability to see; the faculty of vision [syn: {vision}, {visual
        sense}, {visual modality}]
     4: a optical instrument for aiding the eye in aiming, as on a
        firearm or surveying instrument
     5: a range of mental vision; "in his sight she could do no
        wrong"
     6: the range of vision; "out of sight of land" [syn: {ken}]
     7: the act of looking or seeing or observing; "he ...
