---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Instanton
offline_file: ""
offline_thumbnail: ""
uuid: 669c1fed-f031-405d-b0df-203170448904
updated: 1484309224
title: Instanton
tags:
    - Mathematics
    - Quantum mechanics
    - Example
    - Alternative
    - Explicit formula for double-well potential
    - Results
    - Periodic Instantons
    - Inverted double-well formula
    - Quantum field theory
    - Yang–Mills theory
    - Various numbers of dimensions
    - 4d supersymmetric gauge theories
    - References and notes
categories:
    - Differential geometry
---
An instanton[1] (or pseudoparticle[2][3]) is a notion appearing in theoretical and mathematical physics. An instanton is a classical solution to equations of motion[note 1] with a finite, non-zero action, either in quantum mechanics or in quantum field theory. More precisely, it is a solution to the equations of motion of the classical field theory on a Euclidean spacetime.
