---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dressed
offline_file: ""
offline_thumbnail: ""
uuid: 53f41e9b-6839-4ed3-aad6-b5223a99e367
updated: 1484310315
title: dressed
categories:
    - Dictionary
---
dressed
     adj 1: dressed or clothed especially in fine attire; often used in
            combination; "the elegantly attired gentleman";
            "neatly dressed workers"; "monks garbed in hooded
            robes"; "went about oddly garmented"; "professors
            robed in crimson"; "tuxedo-attired gentlemen";
            "crimson-robed Harvard professors" [syn: {appareled},
            {attired}, {garbed}, {garmented}, {habilimented}, {robed}]
     2: treated with medications and protective covering
     3: (of lumber or stone) to trim and smooth [syn: {polished}]
     4: dressed in fancy or formal clothing [syn: {dressed(p)}, {dressed-up},
         {dressed to the nines(p)}, ...
