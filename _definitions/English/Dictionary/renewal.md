---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/renewal
offline_file: ""
offline_thumbnail: ""
uuid: 4624a7bb-da22-4318-8fe0-83fa5d08d914
updated: 1484310607
title: renewal
categories:
    - Dictionary
---
renewal
     n 1: the conversion of wasteland into land suitable for use of
          habitation or cultivation [syn: {reclamation}, {rehabilitation}]
     2: the act of renewing
     3: filling again by supplying what has been used up [syn: {refilling},
         {replenishment}, {replacement}]
