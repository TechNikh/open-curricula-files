---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obediently
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484651401
title: obediently
categories:
    - Dictionary
---
obediently
     adv : in an obedient manner; "obediently she slipped off her right
           shoe and stocking" [syn: {yieldingly}] [ant: {disobediently}]
