---
version: 1
type: article
id: https://en.wikipedia.org/wiki/System_U
offline_file: ""
offline_thumbnail: ""
uuid: 61cb1eb4-c9cb-4769-bfc6-9d52e6b83b73
updated: 1484309499
title: System U
tags:
    - Formal definition
    - "Girard's paradox"
categories:
    - Proof theory
---
In mathematical logic, System U and System U− are pure type systems, i.e. special forms of a typed lambda calculus with an arbitrary number of sorts, axioms and rules (or dependencies between the sorts). They were both proved inconsistent by Jean-Yves Girard in 1972.[1] This result led to the realization that Martin-Löf's original 1971 type theory was inconsistent as it allowed the same "Type in Type" behaviour that Girard's paradox exploits.
