---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Knuth_Prize
offline_file: ""
offline_thumbnail: ""
uuid: d9179900-054a-4cec-9458-4a4b86f5383d
updated: 1484308684
title: Knuth Prize
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Strassen_Knuth_Prize_presentation.jpg
tags:
    - History
    - Winners
categories:
    - Theoretical computer science
---
