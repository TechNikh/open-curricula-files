---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Foundations_of_Algebraic_Geometry
offline_file: ""
offline_thumbnail: ""
uuid: dedfdc68-48c7-43e4-9cef-64b1d1168f1c
updated: 1484309149
title: Foundations of Algebraic Geometry
categories:
    - Algebraic geometry
---
Foundations of Algebraic Geometry is a book by André Weil (1946, 1962) that develops algebraic geometry over fields of any characteristic. In particular it gives a careful treatment of intersection theory by defining the local intersection multiplicity of two subvarieties.
