---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/durables
offline_file: ""
offline_thumbnail: ""
uuid: ac559215-0cd1-4f64-8c68-af7dbad62ea8
updated: 1484310486
title: durables
categories:
    - Dictionary
---
durables
     n : consumer goods that are not destroyed by use [syn: {durable
         goods}, {consumer durables}]
