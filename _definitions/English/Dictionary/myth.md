---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/myth
offline_file: ""
offline_thumbnail: ""
uuid: a63481b6-8d90-4db6-854d-98d6d9033ce4
updated: 1484310587
title: myth
categories:
    - Dictionary
---
myth
     n : a traditional story accepted as history; serves to explain
         the world view of a people
