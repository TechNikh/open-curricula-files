---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Public_auction
offline_file: ""
offline_thumbnail: ""
uuid: 527b9313-b02d-42c5-9a27-3a989c959fb2
updated: 1484309013
title: Public auction
tags:
    - Sale of property owned by the government
    - Sale of private property in a public auction
categories:
    - Auctions
---
A public auction is an auction held on behalf of a government in which the property to be auctioned is either property owned by the government, or property which is sold under the authority of a court of law or a government agency with similar authority.
