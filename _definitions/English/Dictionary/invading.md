---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invading
offline_file: ""
offline_thumbnail: ""
uuid: b3eb9e43-3bc9-42d3-8dc3-f82b3f106218
updated: 1484310174
title: invading
categories:
    - Dictionary
---
invading
     adj : involving invasion or aggressive attack; "invasive war"
           [syn: {incursive}, {invasive}]
