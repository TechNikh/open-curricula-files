---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kharagpur
offline_file: ""
offline_thumbnail: ""
uuid: 9b3a4097-eb1c-4f94-84d9-3082efd572f5
updated: 1484308412
title: Kharagpur
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-IIT-KGP.jpg
tags:
    - History
    - Railways in Kharagpur
    - geography
    - Urban structure
    - Climate
    - Demographics
    - Civic administration and utility services
    - Transport
    - Railways
    - Roadways
    - Education
    - school
    - College
    - IIT Kharagpur
    - Healthcare
    - culture
    - Tourism
    - Industrial setup
categories:
    - Uncategorized
---
Kharagpur  pronunciation (help·info) (Bengali: খড়্গপুর) is an important industrial city in Paschim Medinipur district of West Bengal, India. It is multi-cultural and cosmopolitan city. It is the most populated city of Paschim Medinipur district.[3] The first Indian Institutes of Technology (IIT), a group of Institutes of National Importance, was founded in Kharagpur as early as in May 1950.[4] It is 116 km from Kolkata and forms an important station on Howrah-Mumbai, Howrah-Chennai road and railway route. This city also has one of the largest railway workshops in India, and the third longest railway platform in the world (1072.5 m).[5] Kharagpur has an Air Force base in ...
