---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shut
offline_file: ""
offline_thumbnail: ""
uuid: 8e29f783-baf1-46a4-aef0-dde989be027d
updated: 1484310365
title: shut
categories:
    - Dictionary
---
shut
     adj 1: not open; "the door slammed shut" [syn: {unopen}, {closed}]
            [ant: {open}]
     2: used especially of mouth or eyes; "he sat quietly with
        closed eyes"; "his eyes were shut against the sunlight"
        [syn: {closed}] [ant: {open}]
     v 1: move so that an opening or passage is obstructed; make shut;
          "Close the door"; "shut the window" [syn: {close}] [ant:
           {open}]
     2: become closed; "The windows closed with a loud bang" [syn: {close}]
        [ant: {open}]
     3: prevent from entering; shut out; "The trees were shutting
        out all sunlight"; "This policy excludes people who have a
        criminal record from entering the ...
