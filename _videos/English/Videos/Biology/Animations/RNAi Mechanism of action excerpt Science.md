---
version: 1
type: video
provider: YouTube
id: oiRXMoyMREs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/RNAi%20Mechanism%20of%20action%20excerpt%20Science.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bebd336f-4e7c-40a2-b328-bc6ee82dce10
updated: 1486069673
title: RNAi Mechanism of action excerpt Science
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/oiRXMoyMREs/default.jpg
    - https://i3.ytimg.com/vi/oiRXMoyMREs/1.jpg
    - https://i3.ytimg.com/vi/oiRXMoyMREs/2.jpg
    - https://i3.ytimg.com/vi/oiRXMoyMREs/3.jpg
---
