---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perpetual
offline_file: ""
offline_thumbnail: ""
uuid: f730a15e-6aee-4d48-b189-a8339b81d0f5
updated: 1484310275
title: perpetual
categories:
    - Dictionary
---
perpetual
     adj 1: continuing forever or indefinitely; "the ageless themes of
            love and revenge"; "eternal truths"; "life
            everlasting"; "hell's perpetual fires"; "the unending
            bliss of heaven" [syn: {ageless}, {eternal}, {everlasting},
             {unending}, {unceasing}]
     2: uninterrupted in time and indefinitely long continuing; "the
        ceaseless thunder of surf"; "in constant pain"; "night and
        day we live with the incessant noise of the city"; "the
        never-ending search for happiness"; "the perpetual
        struggle to maintain standards in a democracy"; "man's
        unceasing warfare with drought and isolation";
        ...
