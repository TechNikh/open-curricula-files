---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/G%C3%B6del_Prize'
offline_file: ""
offline_thumbnail: ""
uuid: c402abdc-243b-438b-b056-da23eb0499df
updated: 1484308671
title: Gödel Prize
categories:
    - Theoretical computer science
---
The Gödel Prize is an annual prize for outstanding papers in the area of theoretical computer science, given jointly by European Association for Theoretical Computer Science (EATCS) and the Association for Computing Machinery Special Interest Group on Algorithms and Computational Theory (ACM SIGACT). The award is named in honor of Kurt Gödel. Gödel's connection to theoretical computer science is that he was the first to mention the "P versus NP" question, in a 1956 letter to John von Neumann in which Gödel asked whether a certain NP-complete problem could be solved in quadratic or linear time.
