---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/joining
offline_file: ""
offline_thumbnail: ""
uuid: 12341488-9b8b-40b8-9f98-ec8441bd3fa1
updated: 1484310210
title: joining
categories:
    - Dictionary
---
joining
     n : the act of bringing two things into contact (especially for
         communication); "the joining of hands around the table";
         "there was a connection via the internet" [syn: {connection},
          {connexion}]
