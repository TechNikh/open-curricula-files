---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jehangir
offline_file: ""
offline_thumbnail: ""
uuid: 619396f8-7d7c-496c-87a7-8382b9d52928
updated: 1484308409
title: Jahangir
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Prince_Salim%252C_the_future_Jahangir.jpg'
tags:
    - Early life
    - Reign
    - Foreign relations
    - Marriage
    - Nur Jahan
    - Conquests
    - Death
    - religion
    - art
    - Science
    - In media
    - Works online
categories:
    - Uncategorized
---
Mirza Nur-ud-din Beig Mohammad Khan Salim, known by his imperial name Jahangir (Persian for "conqueror of the world" (31 August 1569 – 28 October 1627),[1] was the fourth Mughal Emperor who ruled from 1605 until his death in 1627. Much romance has gathered around his name (Jahangir means 'conqueror of the world', 'world-conqueror' or 'world-seizer' (Jahan = world, gir the root of the Persian verb gereftan, gireftan = to seize, to grab)), and the tale of his illicit relationship with the Mughal courtesan, Anarkali, has been widely adapted into the literature, art and cinema of India.
