---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rim
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484368621
title: rim
categories:
    - Dictionary
---
rim
     n 1: the shape of a raised edge of a more or less circular object
     2: (basketball) the hoop from which the net is suspended; "the
        ball hit the rim and bounced off"
     3: the outer part of a wheel to which the tire is attached
     4: a projection used for strength or for attaching to another
        object [syn: {flange}]
     5: the top edge of a vessel [syn: {brim}, {lip}]
     v 1: run around the rim of; "Sugar rimmed the dessert plate"
     2: furnish with a rim; "rim a hat"
     3: roll around the rim of; "the ball rimmed the basket"
     [also: {rimming}, {rimmed}]
