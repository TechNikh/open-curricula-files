---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jamaica
offline_file: ""
offline_thumbnail: ""
uuid: 0e5bc3f7-63cb-43e0-ad66-ea93229a4b67
updated: 1484310152
title: jamaica
categories:
    - Dictionary
---
Jamaica
     n 1: a country on the island of Jamaica; became independent of
          England in 1962; much poverty; the major industry is
          tourism
     2: an island in the West Indies south of Cuba and west of Haiti
