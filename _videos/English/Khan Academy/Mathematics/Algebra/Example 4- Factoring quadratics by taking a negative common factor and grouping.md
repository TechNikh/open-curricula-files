---
version: 1
type: video
provider: YouTube
id: d-2Lcp0QKfI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Example%204-%20Factoring%20quadratics%20by%20taking%20a%20negative%20common%20factor%20and%20grouping.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1e50fa8e-2675-4b41-8de0-e716744b339b
updated: 1486069561
title: 'Example 4- Factoring quadratics by taking a negative common factor and grouping'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/d-2Lcp0QKfI/default.jpg
    - https://i3.ytimg.com/vi/d-2Lcp0QKfI/1.jpg
    - https://i3.ytimg.com/vi/d-2Lcp0QKfI/2.jpg
    - https://i3.ytimg.com/vi/d-2Lcp0QKfI/3.jpg
---
