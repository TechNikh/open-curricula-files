---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magnetocrystalline_anisotropy
offline_file: ""
offline_thumbnail: ""
uuid: 636dd6ea-ed2c-40f2-a0cf-e8a71a8fd7c8
updated: 1484309299
title: Magnetocrystalline anisotropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Uniaxial_Anisotropy.jpg
tags:
    - Causes
    - Practical relevance
    - Microscopic origin
    - Thermodynamic theory
    - Uniaxial anisotropy
    - Hexagonal system
    - Tetragonal and Rhombohedral systems
    - Cubic anisotropy
    - Temperature dependence of anisotropy
    - Magnetostriction
    - Notes and references
categories:
    - Orientation (geometry)
---
In physics, a ferromagnetic material is said to have magnetocrystalline anisotropy if it takes more energy to magnetize it in certain directions than in others. These directions are usually related to the principal axes of its crystal lattice. It is a special case of magnetic anisotropy.
