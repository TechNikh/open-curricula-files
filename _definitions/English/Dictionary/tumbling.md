---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tumbling
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484577001
title: tumbling
categories:
    - Dictionary
---
tumbling
     adj 1: moving in surges and billows and rolls; "billowing smoke
            from burning houses"; "the rolling fog"; "the rolling
            sea"; "the tumbling water of the rapids" [syn: {billowing},
             {rolling}]
     2: pitching headlong with a rolling or twisting movement; "a
        violent tumbling fall"
     n : the gymnastic moves of an acrobat [syn: {acrobatics}]
