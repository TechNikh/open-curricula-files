---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dividing_a_circle_into_areas
offline_file: ""
offline_thumbnail: ""
uuid: fe740681-4056-4d6d-97d1-4a67c8962fc2
updated: 1484308573
title: Dividing a circle into areas
tags:
    - Lemma
    - Solution
    - Inductive method
    - Combinatorics and topology method
categories:
    - Combinatorics
---
In geometry, the problem of dividing a circle into areas by means of an inscribed polygon with n sides, in such a way as to maximise the number of areas created by the edges and diagonals, has a solution by an inductive method.
