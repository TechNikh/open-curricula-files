---
version: 1
type: video
provider: YouTube
id: bOho0VHPV4s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: be533af8-9490-48ca-8e76-3c61f7bb05eb
updated: 1486069647
title: 'Li- The Patterns of Nature'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/bOho0VHPV4s/default.jpg
    - https://i3.ytimg.com/vi/bOho0VHPV4s/1.jpg
    - https://i3.ytimg.com/vi/bOho0VHPV4s/2.jpg
    - https://i3.ytimg.com/vi/bOho0VHPV4s/3.jpg
---
