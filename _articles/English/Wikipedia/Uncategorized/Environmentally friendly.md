---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ecofriendly
offline_file: ""
offline_thumbnail: ""
uuid: 55c3d525-f2d9-42ee-85fb-b4b91b822be0
updated: 1484308335
title: Environmentally friendly
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Depuradora_de_Lluc.JPG
tags:
    - Regional variants
    - Europe
    - North America
    - Oceania
    - Asia
    - International
categories:
    - Uncategorized
---
Environmentally friendly or environment-friendly, (also referred to as eco-friendly, nature-friendly, and green) are sustainability and marketing terms referring to goods and services, laws, guidelines and policies that inflict reduced, minimal, or no harm upon ecosystems or the environment.[1] Companies use these ambiguous terms to promote goods and services, sometimes with additional, more specific certifications, such as ecolabels. Their overuse can be referred to as greenwashing.[2][3][4]
