---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/filial
offline_file: ""
offline_thumbnail: ""
uuid: 1c7fb63e-32e4-402f-ba77-f26abd172fe8
updated: 1484310305
title: filial
categories:
    - Dictionary
---
filial
     adj 1: designating the generation or the sequence of generations
            following the parental generation [ant: {parental}]
     2: relating to or characteristic of or befitting an offspring;
        "filial respect" [ant: {parental}]
