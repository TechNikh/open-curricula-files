---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/excessive
offline_file: ""
offline_thumbnail: ""
uuid: e7038a30-9c56-4dae-bf0b-7afdbf7b1910
updated: 1484310381
title: excessive
categories:
    - Dictionary
---
excessive
     adj 1: beyond normal limits; "excessive charges"; "a book of
            inordinate length"; "his dress stops just short of
            undue elegance"; "unreasonable demands" [syn: {inordinate},
             {undue}, {unreasonable}]
     2: unrestrained in especially feelings; "extravagant praise";
        "exuberant compliments"; "overweening ambition";
        "overweening greed" [syn: {extravagant}, {exuberant}, {overweening}]
