---
version: 1
type: video
provider: YouTube
id: KRjyWAtIcWQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ae1d374e-a12d-4ec5-8129-3eb970c0ccda
updated: 1486069609
title: How to simplify a variable cube root expression example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/KRjyWAtIcWQ/default.jpg
    - https://i3.ytimg.com/vi/KRjyWAtIcWQ/1.jpg
    - https://i3.ytimg.com/vi/KRjyWAtIcWQ/2.jpg
    - https://i3.ytimg.com/vi/KRjyWAtIcWQ/3.jpg
---
