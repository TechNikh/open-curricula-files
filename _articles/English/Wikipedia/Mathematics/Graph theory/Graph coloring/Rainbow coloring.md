---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rainbow_coloring
offline_file: ""
offline_thumbnail: ""
uuid: abbc2762-f731-4c1e-816c-4c3264dfb720
updated: 1484309387
title: Rainbow coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Wheel_rainbow_coloring.svg.png
tags:
    - Definitions and bounds
    - Exact rainbow or strong rainbow connection numbers
    - Complexity
    - Variants and generalizations
    - Notes
categories:
    - Graph coloring
---
In graph theory, a path in an edge-colored graph is said to be rainbow if no color repeats on it. A graph is said to be rainbow colored if there is a rainbow path between each pair of its vertices. If there is a rainbow shortest path between each pair of vertices, the graph is said to be strong rainbow colored.[1]
