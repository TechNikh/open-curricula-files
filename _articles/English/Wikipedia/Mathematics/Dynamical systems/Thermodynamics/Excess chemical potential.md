---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Excess_chemical_potential
offline_file: ""
offline_thumbnail: ""
uuid: be872d76-81eb-447d-9c90-84d3e5344378
updated: 1484308846
title: Excess chemical potential
categories:
    - Thermodynamics
---
The excess chemical potential is defined as the difference between the chemical potential of a given species and that of an ideal gas under the same conditions (in particular, at the same pressure, temperature, and composition).[1]
