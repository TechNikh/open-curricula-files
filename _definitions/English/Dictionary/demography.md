---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demography
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484385421
title: demography
categories:
    - Dictionary
---
demography
     n : the branch of sociology that studies the characteristics of
         human populations [syn: {human ecology}]
