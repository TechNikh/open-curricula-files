---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/profitably
offline_file: ""
offline_thumbnail: ""
uuid: 3854a9d6-415f-410b-b7e7-78ae64959f38
updated: 1484310407
title: profitably
categories:
    - Dictionary
---
profitably
     adv : in a productive way; "they worked together productively for
           two years" [syn: {productively}, {fruitfully}] [ant: {unproductively},
            {unproductively}, {unproductively}]
