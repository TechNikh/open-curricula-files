---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cubane-type_cluster
offline_file: ""
offline_thumbnail: ""
uuid: 20744a02-b9b9-434a-a06d-be02093989d5
updated: 1484309277
title: Cubane-type cluster
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Cuba.svg_0.png
categories:
    - Molecular geometry
---
A cubane-type cluster is an arrangement of atoms in a molecular structure that forms a cube. Cubane itself is a hydrocarbon with chemical formula C8H8, having the carbon atoms at the corners of a cube and covalent bonds forming the edges. Other compounds having different elements in the corners, various atoms or groups bonded off the corners beyond the cube, or additional bonding among the atoms of the cube itself are all part of this class of structures. The structure may be a simple covalent compound or be a more macromolecular or supramolecular cluster compound. Many of these compounds have a high strain due to the 90° angles between the cube-edge bonds at each corner, though others are ...
