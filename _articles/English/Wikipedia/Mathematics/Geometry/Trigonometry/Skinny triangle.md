---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Skinny_triangle
offline_file: ""
offline_thumbnail: ""
uuid: 2ae8ab69-e371-4054-8032-c040e0e03856
updated: 1484309350
title: Skinny triangle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Isosceles_skinny_triangle.svg.png
tags:
    - Isosceles triangle
    - Right triangle
    - Applications
    - Astronomy
    - Gunnery
    - Aviation
    - Bibliography
categories:
    - Trigonometry
---
A skinny triangle in trigonometry is a triangle whose height is much greater than its base. The solution of such triangles can be greatly simplified by using the approximation that the sine of a small angle is equal to the angle in radians. The solution is particularly simple for skinny triangles that are also isosceles or right triangles: in these cases the need for trigonometric functions or tables can be entirely dispensed with.
