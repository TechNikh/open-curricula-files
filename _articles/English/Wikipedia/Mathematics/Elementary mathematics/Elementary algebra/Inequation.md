---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inequation
offline_file: ""
offline_thumbnail: ""
uuid: 6ec6305d-14ca-4613-b841-6ea0486f34e2
updated: 1484308909
title: Inequation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Linear_Programming_Feasible_Region.svg.png
tags:
    - Chains of inequations
    - Solving inequations
    - Special
categories:
    - Elementary algebra
---
In mathematics, an inequation is a statement that an inequality holds between two values.[1] It is usually written in the form of a pair of expressions denoting the values in question, with a relational sign between them indicating the specific inequality relation. Some examples of inequations are:
