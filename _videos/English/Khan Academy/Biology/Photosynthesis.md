---
version: 1
type: video
provider: YouTube
id: -rsYk4eCKnA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Photosynthesis.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 433ce0cb-9179-4ed4-a41d-deb3c2c8dcab
updated: 1486069579
title: Photosynthesis
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/-rsYk4eCKnA/default.jpg
    - https://i3.ytimg.com/vi/-rsYk4eCKnA/1.jpg
    - https://i3.ytimg.com/vi/-rsYk4eCKnA/2.jpg
    - https://i3.ytimg.com/vi/-rsYk4eCKnA/3.jpg
---
Overview of photosythesis
More free lessons at: http://www.khanacademy.org/video?v=-rsYk4eCKnA
