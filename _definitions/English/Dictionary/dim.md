---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dim
offline_file: ""
offline_thumbnail: ""
uuid: 3a12ec8f-d35b-4982-9be8-90f3320b7e8c
updated: 1484310200
title: dim
categories:
    - Dictionary
---
dim
     adj 1: lacking in light; not bright or harsh; "a dim light beside
            the bed"; "subdued lights and soft music" [syn: {subdued}]
     2: lacking clarity or distinctness; "a dim figure in the
        distance"; "only a faint recollection"; "shadowy figures
        in the gloom"; "saw a vague outline of a building through
        the fog"; "a few wispy memories of childhood" [syn: {faint},
         {shadowy}, {vague}, {wispy}]
     3: made dim or less bright; "the dimmed houselights brought a
        hush of anticipation"; "dimmed headlights"; "we like
        dimmed lights when we have dinner" [syn: {dimmed}] [ant: {undimmed}]
     4: offering little or no hope; "the future ...
