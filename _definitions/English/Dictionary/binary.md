---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/binary
offline_file: ""
offline_thumbnail: ""
uuid: 5233a596-1f11-45f7-b801-7d868be8dc73
updated: 1484310160
title: binary
categories:
    - Dictionary
---
binary
     adj 1: of or pertaining to a number system have 2 as its base; "a
            binary digit"
     2: consisting of two (units or components or elements or terms)
        or based on two; "a binary star is a system in which two
        stars revolve around each other"; "a binary compound";
        "the binary number system has two as its base"
     n : a system of two stars that revolve around each other under
         their mutual gravitation [syn: {binary star}, {double
         star}]
