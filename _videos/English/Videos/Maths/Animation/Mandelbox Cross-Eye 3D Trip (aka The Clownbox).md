---
version: 1
type: video
provider: YouTube
id: Co4Sql0aVXM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 669e57ab-d334-4ce6-8f9e-e134d67e4bbc
updated: 1486069647
title: Mandelbox Cross-Eye 3D Trip (aka The Clownbox)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Co4Sql0aVXM/default.jpg
    - https://i3.ytimg.com/vi/Co4Sql0aVXM/1.jpg
    - https://i3.ytimg.com/vi/Co4Sql0aVXM/2.jpg
    - https://i3.ytimg.com/vi/Co4Sql0aVXM/3.jpg
---
