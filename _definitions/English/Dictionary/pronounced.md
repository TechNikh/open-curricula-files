---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pronounced
offline_file: ""
offline_thumbnail: ""
uuid: 69731847-fba7-40cc-bced-5fcd954a6808
updated: 1484310577
title: pronounced
categories:
    - Dictionary
---
pronounced
     adj 1: strongly marked; easily noticeable; "walked with a marked
            limp"; "a pronounced flavor of cinnamon" [syn: {marked}]
     2: produced by the organs of speech
