---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/napalm
offline_file: ""
offline_thumbnail: ""
uuid: b2afc449-d46c-4700-bb66-44625d846fba
updated: 1484310579
title: napalm
categories:
    - Dictionary
---
napalm
     n : gasoline jelled with aluminum soaps; highly incendiary
         liquid used in fire bombs and flame throwers
