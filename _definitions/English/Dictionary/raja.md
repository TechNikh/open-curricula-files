---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/raja
offline_file: ""
offline_thumbnail: ""
uuid: 163a7617-c0fb-472c-947f-4bbb2c1d6e7e
updated: 1484310545
title: raja
categories:
    - Dictionary
---
raja
     n 1: a Hindu prince or king in India [syn: {rajah}]
     2: type genus of the family Rajidae [syn: {genus Raja}]
