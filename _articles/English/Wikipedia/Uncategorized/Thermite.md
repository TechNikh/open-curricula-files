---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermite
offline_file: ""
offline_thumbnail: ""
uuid: 9eee49b9-e23c-4080-9958-6123ae25fa13
updated: 1484308391
title: Thermite
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Thermite_mix.jpg
tags:
    - Chemical reactions
    - History
    - Types
    - Iron thermite
    - Copper thermite
    - Thermates
    - Ignition
    - Civilian uses
    - Military uses
    - Hazards
categories:
    - Uncategorized
---
Thermite (/ˈθɜː(r)maɪt/)[1] is a pyrotechnic composition of metal powder, fuel and metal oxide. When ignited by heat, thermite undergoes an exothermic reduction-oxidation (redox) reaction. Most varieties are not explosive but can create brief bursts of high temperature in a small area. Its form of action is similar to that of other fuel-oxidizer mixtures, such as black powder.
