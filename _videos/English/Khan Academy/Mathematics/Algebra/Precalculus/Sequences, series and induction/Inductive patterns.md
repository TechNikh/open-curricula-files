---
version: 1
type: video
provider: YouTube
id: GvbrtnEYRpY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Sequences%2C%20series%20and%20induction/Inductive%20patterns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ad2567a4-b31a-4438-92d7-7eb6c822af99
updated: 1486069563
title: Inductive patterns
categories:
    - Sequences, series and induction
thumbnail_urls:
    - https://i3.ytimg.com/vi/GvbrtnEYRpY/default.jpg
    - https://i3.ytimg.com/vi/GvbrtnEYRpY/1.jpg
    - https://i3.ytimg.com/vi/GvbrtnEYRpY/2.jpg
    - https://i3.ytimg.com/vi/GvbrtnEYRpY/3.jpg
---
