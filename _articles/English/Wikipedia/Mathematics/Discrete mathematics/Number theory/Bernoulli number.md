---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bernoulli_number
offline_file: ""
offline_thumbnail: ""
uuid: 292f3efb-7abd-48b2-9c61-16b962762cc6
updated: 1484308636
title: Bernoulli number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Bernoulli_numbers_graphs.svg.png
tags:
    - Sum of powers
    - Definitions
    - Recursive definition
    - Explicit definition
    - Generating function
    - Algorithmic description
    - Efficient computation of Bernoulli numbers
    - Different viewpoints and conventions
    - Applications of the Bernoulli numbers
    - Asymptotic analysis
    - Taylor series of tan and tanh
    - Use in topology
    - Combinatorial definitions
    - Connection with Worpitzky numbers
    - Connection with Stirling numbers of the second kind
    - Connection with Stirling numbers of the first kind
    - Connection with Eulerian numbers
    - Connection with Balmer series
    - Representation of the second Bernoulli numbers
    - A binary tree representation
    - Asymptotic approximation
    - Integral representation and continuation
    - The relation to the Euler numbers and π
    - 'An algorithmic view: the Seidel triangle'
    - 'A combinatorial view: alternating permutations'
    - Related sequences
    - Generalization to the odd-index Bernoulli numbers
    - A companion to the second Bernoulli numbers
    - Arithmetical properties of the Bernoulli numbers
    - The Kummer theorems
    - p-adic continuity
    - "Ramanujan's congruences"
    - Von Staudt–Clausen theorem
    - Why do the odd Bernoulli numbers vanish?
    - A restatement of the Riemann hypothesis
    - History
    - Early history
    - Reconstruction of "Summae Potestatum"
    - Generalized Bernoulli numbers
    - Appendix
    - Assorted identities
    - Values of the first Bernoulli numbers
    - A subsequence of the Bernoulli number denominators
    - Notes
categories:
    - Number theory
---
In mathematics, the Bernoulli numbers Bn are a sequence of rational numbers with deep connections to number theory. The values of the first few Bernoulli numbers are
