---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mussolini
offline_file: ""
offline_thumbnail: ""
uuid: d99dc92e-3783-4b33-95bf-6ea0d23f47a5
updated: 1484308482
title: Benito Mussolini
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Benito_Mussolini_mugshot_1903.jpg
tags:
    - Early life
    - Emigration to Switzerland and military service
    - Political journalist, intellectual and socialist
    - Expulsion from the Italian Socialist Party
    - Beginning of Fascism and service in World War I
    - Rise to power
    - Formation of the National Fascist Party
    - March on Rome
    - Appointment as Prime Minister
    - Acerbo Law
categories:
    - Uncategorized
---
Benito Amilcare Andrea Mussolini (Italian pronunciation: [beˈniːto mussoˈliːni];[1] 29 July 1883 – 28 April 1945) was an Italian politician, journalist, and leader of the National Fascist Party (Partito Nazionale Fascista; PNF), ruling the country as Prime Minister from 1922 to 1943. He ruled constitutionally until 1925, when he dropped all pretense of democracy and set up a legal dictatorship. Known as Il Duce (The Leader), Mussolini was the founder of Italian Fascism.[2][3][4]
