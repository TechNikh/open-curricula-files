---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strategic_management
offline_file: ""
offline_thumbnail: ""
uuid: e61caddb-985c-4464-9674-d798ce1d71e7
updated: 1484309065
title: Strategic management
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Strategic_Management_Framework.png
tags:
    - Definition
    - Formulation
    - Implementation
    - Many definitions of strategy
    - Historical development
    - Origins
    - Change in focus from production to marketing
    - Nature of strategy
    - Concepts and frameworks
    - SWOT Analysis
    - Experience curve
    - Corporate strategy and portfolio theory
    - Competitive advantage
    - Industry structure and profitability
    - Generic competitive strategies
    - Value chain
    - Core competence
    - Theory of the business
    - Strategic thinking
    - Strategic planning
    - Environmental analysis
    - Scenario planning
    - Measuring and controlling implementation
    - Evaluation
    - Limitations
    - Strategic themes
    - Self-service
    - Globalization and the virtual firm
    - Internet and information availability
    - sustainability
    - Strategy as learning
    - Strategy as adapting to change
    - Strategy as operational excellence
    - Quality
    - Reengineering
    - Other perspectives on strategy
    - Strategy as problem solving
    - Creative vs analytic approaches
    - Non-strategic management
    - Strategy as marketing
    - 'Information- and technology-driven strategy'
    - Maturity of planning process
    - PIMS study
    - Other influences on business strategy
    - Military strategy
    - Traits of successful companies
categories:
    - Strategy
---
Strategic management involves the formulation and implementation of the major goals and initiatives taken by a company's top management on behalf of owners, based on consideration of resources and an assessment of the internal and external environments in which the organization competes.[1]
