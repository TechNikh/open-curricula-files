---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inequality_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 6fcb4559-5e90-48b0-a074-17a059c7da36
updated: 1484308912
title: Inequality (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Linear_Programming_Feasible_Region.svg_0.png
tags:
    - Properties
    - Transitivity
    - Converse
    - Addition and subtraction
    - Multiplication and division
    - Additive inverse
    - Multiplicative inverse
    - Applying a function to both sides
    - Ordered fields
    - Chained notation
    - Sharp inequalities
    - Inequalities between means
    - Power inequalities
    - Examples
    - Well-known inequalities
    - Complex numbers and inequalities
    - Vector inequalities
    - General existence theorems
    - Notes
categories:
    - Elementary algebra
---
If the values in question are elements of an ordered set, such as the integers or the real numbers, they can be compared in size.
