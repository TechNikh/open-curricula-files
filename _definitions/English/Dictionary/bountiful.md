---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bountiful
offline_file: ""
offline_thumbnail: ""
uuid: 987c12e9-91bd-4669-ae61-37f1fc39d156
updated: 1484310521
title: bountiful
categories:
    - Dictionary
---
bountiful
     adj 1: given or giving freely; "was a big tipper"; "the bounteous
            goodness of God"; "bountiful compliments"; "a
            freehanded host"; "a handsome allowance"; "Saturday's
            child is loving and giving"; "a liberal backer of the
            arts"; "a munificent gift"; "her fond and openhanded
            grandfather" [syn: {big}, {bighearted}, {bounteous}, {freehanded},
             {handsome}, {giving}, {liberal}, {openhanded}]
     2: producing in abundance; "the bountiful earth"; "a plentiful
        year"; "fruitful soil" [syn: {plentiful}]
