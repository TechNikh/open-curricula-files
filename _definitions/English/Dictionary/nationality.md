---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nationality
offline_file: ""
offline_thumbnail: ""
uuid: 5dc78fb4-1c69-4145-9fb9-b2bf57e3d5cc
updated: 1484310557
title: nationality
categories:
    - Dictionary
---
nationality
     n : the status of belonging to a particular nation by birth or
         naturalization
