---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grief
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484424781
title: grief
categories:
    - Dictionary
---
grief
     n 1: intense sorrow caused by loss of a loved one (especially by
          death) [syn: {heartache}, {heartbreak}, {brokenheartedness}]
     2: something that causes great unhappiness; "her death was a
        great grief to John" [syn: {sorrow}]
