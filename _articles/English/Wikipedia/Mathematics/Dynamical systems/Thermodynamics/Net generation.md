---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Net_generation
offline_file: ""
offline_thumbnail: ""
uuid: 5c50983e-f539-4637-8097-b7c07e918393
updated: 1484308857
title: Net generation
categories:
    - Thermodynamics
---
Net generation is the amount of electricity generated by a power plant that is transmitted and distributed for consumer use. Net generation is less than the total gross power generation as some power produced is consumed within the plant itself to power auxiliary equipment such as pumps, motors and pollution control devices. Thus
