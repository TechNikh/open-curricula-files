---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Chasles%27_theorem_(geometry)'
offline_file: ""
offline_thumbnail: ""
uuid: 2b42b679-9483-460c-ae6f-cf0a3c45cda4
updated: 1484309357
title: "Chasles' theorem (geometry)"
categories:
    - Geometry stubs
---
In algebraic geometry, Chasles' theorem says that if two pencils of curves have no curves in common, then the intersections of those curves form another pencil of curves the degree of which can be calculated from the degrees of the initial two pencils.[1]
