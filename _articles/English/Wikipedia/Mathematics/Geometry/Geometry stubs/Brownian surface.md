---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Brownian_surface
offline_file: ""
offline_thumbnail: ""
uuid: 91839767-f7ea-48dd-918f-58c4d2c09165
updated: 1484309354
title: Brownian surface
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Brownian_surface.png
categories:
    - Geometry stubs
---
For instance, in the three-dimensional case, where two variables X and Y are given as coordinates, the elevation function between any two points (x1, y1) and (x2, y2) can be set to have a mean or expected value that increases as the vector distance between (x1, y1) and (x2, y2).[1] There are, however, many ways of defining the elevation function. For instance, the fractional Brownian motion variable may be used, or various rotation functions may be used to achieve more natural looking surfaces.[2]
