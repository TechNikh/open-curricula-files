---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aesthetic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484642041
title: aesthetic
categories:
    - Dictionary
---
aesthetic
     adj 1: relating to or dealing with the subject of aesthetics;
            "aesthetic values" [syn: {esthetic}]
     2: concerning or characterized by an appreciation of beauty or
        good taste; "the aesthetic faculties"; "an aesthetic
        person"; "aesthetic feeling"; "the illustrations made the
        book an aesthetic success" [syn: {esthetic}, {aesthetical},
         {esthetical}] [ant: {inaesthetic}]
     3: aesthetically pleasing; "an artistic flower arrangement"
        [syn: {esthetic}, {artistic}, {pleasing}]
     n : (philosophy) a philosophical theory as to what is beautiful;
         "he despised the esthetic of minimalism" [syn: {esthetic}]
