---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mobile
offline_file: ""
offline_thumbnail: ""
uuid: d71e2688-aa18-4c3d-a032-360c939e5055
updated: 1484310198
title: mobile
categories:
    - Dictionary
---
mobile
     adj 1: moving or capable of moving readily (especially from place
            to place); "a mobile missile system"; "the tongue
            is...the most mobile articulator" [ant: {immobile}]
     2: (of groups of people) tending to travel and change
        settlements frequently; "a restless mobile society"; "the
        nomadic habits of the Bedouins"; "believed the profession
        of a peregrine typist would have a happy future";
        "wandering tribes" [syn: {nomadic}, {peregrine}, {roving},
         {wandering}]
     3: having transportation available
     4: capable of changing quickly from one state or condition to
        another; "a highly mobile face"
     5: ...
