---
version: 1
type: video
provider: YouTube
id: OYSu1FYB95k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9b954ab4-78cc-4b1f-90b3-17c5be6934a2
updated: 1486069614
title: Solving Linear Systems By Graphing
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/OYSu1FYB95k/default.jpg
    - https://i3.ytimg.com/vi/OYSu1FYB95k/1.jpg
    - https://i3.ytimg.com/vi/OYSu1FYB95k/2.jpg
    - https://i3.ytimg.com/vi/OYSu1FYB95k/3.jpg
---
