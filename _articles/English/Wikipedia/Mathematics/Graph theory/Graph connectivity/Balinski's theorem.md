---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Balinski%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 19802118-d6d8-46a4-955d-cc0577e47b7f
updated: 1484309400
title: "Balinski's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Balinski.svg.png
categories:
    - Graph connectivity
---
In polyhedral combinatorics, a branch of mathematics, Balinski's theorem is a statement about the graph-theoretic structure of three-dimensional polyhedra and higher-dimensional polytopes. It states that, if one forms an undirected graph from the vertices and edges of a convex d-dimensional polyhedron or polytope (its skeleton), then the resulting graph is at least d-vertex-connected: the removal of any d − 1 vertices leaves a connected subgraph. For instance, for a three-dimensional polyhedron, even if two of its vertices (together with their incident edges) are removed, for any pair of vertices there will still exist a path of vertices and edges connecting the pair.[1]
