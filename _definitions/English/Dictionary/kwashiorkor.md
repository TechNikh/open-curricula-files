---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kwashiorkor
offline_file: ""
offline_thumbnail: ""
uuid: 94802c2a-ac3b-49be-ac4f-6e47e2023bb6
updated: 1484310163
title: kwashiorkor
categories:
    - Dictionary
---
kwashiorkor
     n : severe malnutrition in children resulting from a diet
         excessively high in carbohydrates and low in protein
