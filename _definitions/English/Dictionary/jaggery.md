---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jaggery
offline_file: ""
offline_thumbnail: ""
uuid: c6a76d5d-90df-4d75-94f4-4285d898a70c
updated: 1484310518
title: jaggery
categories:
    - Dictionary
---
jaggery
     n : unrefined brown sugar made from palm sap [syn: {jagghery}, {jaggary}]
