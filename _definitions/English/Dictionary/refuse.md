---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refuse
offline_file: ""
offline_thumbnail: ""
uuid: 5124ad2f-d1c7-43d0-bda6-d1dca07612a6
updated: 1484310515
title: refuse
categories:
    - Dictionary
---
refuse
     n : food that is discarded (as from a kitchen) [syn: {garbage},
         {food waste}, {scraps}]
     v 1: show unwillingness towards; "he declined to join the group
          on a hike" [syn: {decline}] [ant: {accept}]
     2: refuse to accept; "He refused my offer of hospitality" [syn:
         {reject}, {pass up}, {turn down}, {decline}] [ant: {accept}]
     3: elude, especially in a baffling way; "This behavior defies
        explanation" [syn: {defy}, {resist}] [ant: {lend oneself}]
     4: refuse to let have; "She denies me every pleasure"; "he
        denies her her weekly allowance" [syn: {deny}] [ant: {allow}]
     5: resist immunologically the introduction of some ...
