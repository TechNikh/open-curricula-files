---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entente
offline_file: ""
offline_thumbnail: ""
uuid: bdb683c0-8e8e-4831-ab93-2cdea76af42f
updated: 1484310551
title: entente
categories:
    - Dictionary
---
entente
     n 1: an informal alliance between countries [syn: {entente
          cordiale}]
     2: a friendly understanding between political powers [syn: {entente
        cordiale}]
