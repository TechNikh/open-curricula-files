---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bundle_metric
offline_file: ""
offline_thumbnail: ""
uuid: 3858f4d7-f77d-4c5c-80b4-ae4a3a21c599
updated: 1484309206
title: Bundle metric
tags:
    - Definition
    - Properties
    - 'Example: Riemann metric'
    - 'Example: on vertical bundles'
    - In relation to Kaluza–Klein theory
categories:
    - Differential geometry
---
In differential geometry, the notion of a metric tensor can be extended to an arbitrary vector bundle, and to some principle fiber bundles. This metric is often called a bundle metric, or fibre metric.
