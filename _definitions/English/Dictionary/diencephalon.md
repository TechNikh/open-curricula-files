---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diencephalon
offline_file: ""
offline_thumbnail: ""
uuid: 12bbbe76-dc8f-4479-bef4-73b9b599487c
updated: 1484310357
title: diencephalon
categories:
    - Dictionary
---
diencephalon
     n : the posterior division of the forebrain; connects the
         cerebral hemispheres with the mesencephalon [syn: {interbrain},
          {betweenbrain}, {thalmencephalon}]
