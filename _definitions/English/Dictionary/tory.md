---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tory
offline_file: ""
offline_thumbnail: ""
uuid: 61d1337f-2652-417d-8e6f-5e4a8960f0aa
updated: 1484310486
title: tory
categories:
    - Dictionary
---
Tory
     n 1: an American who favored the British side during the American
          Revolution
     2: a supporter of traditional political and social institutions
        against the forces of reform; a political conservative
