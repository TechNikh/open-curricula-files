---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Group_delay_and_phase_delay
offline_file: ""
offline_thumbnail: ""
uuid: 2a33d54c-dc43-4b59-8871-3d6d6970d773
updated: 1484308893
title: Group delay and phase delay
tags:
    - Introduction
    - Group delay in optics
    - Group delay in audio
categories:
    - Waves
---
In signal processing, group delay is the time delay of the amplitude envelopes of the various sinusoidal components of a signal through a device under test, and is a function of frequency for each component. Phase delay, in contrast, is the time delay of the phase as opposed to the time delay of the amplitude envelope.
