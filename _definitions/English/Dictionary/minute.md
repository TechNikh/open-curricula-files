---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minute
offline_file: ""
offline_thumbnail: ""
uuid: 54a0ea31-f564-4b41-a35f-3b9ba99bbb68
updated: 1484310224
title: minute
categories:
    - Dictionary
---
minute
     adj 1: infinitely or immeasurably small; "two minute whiplike
            threads of protoplasm"; "reduced to a microscopic
            scale" [syn: {infinitesimal}, {microscopic}]
     2: immeasurably small [syn: {atomic}, {atomlike}]
     3: characterized by painstaking care and detailed examination;
        "a minute inspection of the grounds"; "a narrow scrutiny";
        "an exact and minute report" [syn: {narrow}]
     n 1: a unit of time equal to 60 seconds or 1/60th of an hour; "he
          ran a 4 minute mile" [syn: {min}]
     2: an indefinitely short time; "wait just a moment"; "it only
        takes a minute"; "in just a bit" [syn: {moment}, {second},
         ...
