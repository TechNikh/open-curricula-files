---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Canberra_distance
offline_file: ""
offline_thumbnail: ""
uuid: a95e1e6d-60d9-4ee1-bcb1-9c6d4c6d5047
updated: 1484309262
title: Canberra distance
tags:
    - Definition
    - Notes
categories:
    - Metric geometry
---
The Canberra distance is a numerical measure of the distance between pairs of points in a vector space, introduced in 1966[1] and refined in 1967[2] by G. N. Lance and W. T. Williams. It is a weighted version of L₁ (Manhattan) distance.[3] The Canberra distance has been used as a metric for comparing ranked lists[3] and for intrusion detection in computer security.[4]
