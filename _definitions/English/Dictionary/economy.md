---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/economy
offline_file: ""
offline_thumbnail: ""
uuid: 7def833b-57d3-4e3d-87b2-175e21809c54
updated: 1484310445
title: economy
categories:
    - Dictionary
---
economy
     n 1: the system of production and distribution and consumption
          [syn: {economic system}]
     2: the efficient use of resources; "economy of effort"
     3: frugality in the expenditure of money or resources; "the
        Scots are famous for their economy" [syn: {thriftiness}]
     4: an act of economizing; reduction in cost; "it was a small
        economy to walk to work every day"; "there was a saving of
        50 cents" [syn: {saving}]
