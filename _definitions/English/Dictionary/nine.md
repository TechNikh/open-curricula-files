---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nine
offline_file: ""
offline_thumbnail: ""
uuid: 5b1d08e3-f7c5-47bf-8529-560802476795
updated: 1484310321
title: nine
categories:
    - Dictionary
---
nine
     adj : denoting a quantity consisting of one more than eight and
           one less than ten [syn: {9}, {ix}]
     n 1: the cardinal number that is the sum of eight and one [syn: {9},
           {IX}, {niner}, {Nina from Carolina}, {ennead}]
     2: a team of professional baseball players who play and travel
        together; "each club played six home games with teams in
        its own division" [syn: {baseball club}, {ball club}, {club}]
