---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Monge%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 85f4c64d-6490-4b3f-ae7f-864dd4f1444d
updated: 1484309119
title: "Monge's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Monge_theorem.svg.png
tags:
    - Proofs
    - Bibliography
categories:
    - Theorems in geometry
---
In geometry, Monge's theorem, named after Gaspard Monge, states that for any three circles in a plane, none of which is inside one of the others, the intersection points of each of the three pairs of external tangent lines are collinear.
