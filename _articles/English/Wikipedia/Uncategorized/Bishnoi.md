---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bishnoi
offline_file: ""
offline_thumbnail: ""
uuid: 0a351e87-2184-423a-862d-bde73e794886
updated: 1484308331
title: Bishnoi
tags:
    - History
    - Demographics
    - Places of pilgrimage
    - Khejarli Massacre
    - 29 tenets
categories:
    - Uncategorized
---
Bishnoi (also known as Vishnoi) is a religious group found in the Western Thar Desert and northern states of India. They follow a set of 29 principles given by Guru Jambheshwar.[1] Jambheshwar, who lived in the 15th century, said that trees and wildlife should be protected, prophesying that harming the environment means harming oneself....
