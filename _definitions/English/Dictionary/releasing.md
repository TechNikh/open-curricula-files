---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/releasing
offline_file: ""
offline_thumbnail: ""
uuid: d7cbcaad-74d1-4198-b686-1b5a76630433
updated: 1484310328
title: releasing
categories:
    - Dictionary
---
releasing
     adj : emotionally purging (of e.g. art) [syn: {cathartic}]
