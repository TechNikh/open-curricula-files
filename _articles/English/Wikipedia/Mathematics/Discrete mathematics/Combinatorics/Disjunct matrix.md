---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Disjunct_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 393b15ed-4cd8-4cee-821f-bf8cde6cae63
updated: 1484308577
title: Disjunct matrix
tags:
    - d-separable
    - Decoding algorithm
    - d-disjunct
    - Decoding algorithm
    - d^e-disjunct
    - Upper bounds for non-adaptive group testing
    - Randomized construction
    - Strongly explicit construction
    - Examples
    - Notes
categories:
    - Combinatorics
---
Disjunct and separable matrices play a pivotal role in the mathematical area of non-adaptive group testing. This area investigates efficient designs and procedures to identify 'needles in haystacks' by conducting the tests on groups of items instead of each item alone. The main concept is that if there are very few special items (needles) and the groups are constructed according to certain combinatorial guidelines, then one can test the groups and find all the needles. This can reduce the cost and the labor associated with of large scale experiments.
