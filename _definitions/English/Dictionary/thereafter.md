---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thereafter
offline_file: ""
offline_thumbnail: ""
uuid: 2e08e679-5e2f-4cf6-9154-b826bce8df6b
updated: 1484310461
title: thereafter
categories:
    - Dictionary
---
thereafter
     adv : from that time on; "thereafter he never called again" [syn:
           {thenceforth}]
