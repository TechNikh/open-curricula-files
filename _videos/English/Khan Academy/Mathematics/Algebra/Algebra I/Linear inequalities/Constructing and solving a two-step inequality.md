---
version: 1
type: video
provider: YouTube
id: wo7DSaPP8hQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20inequalities/Constructing%20and%20solving%20a%20two-step%20inequality.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 34156bb8-c9c5-45bd-9d64-c8dc3f61359c
updated: 1486069563
title: Constructing and solving a two-step inequality
categories:
    - Linear inequalities
thumbnail_urls:
    - https://i3.ytimg.com/vi/wo7DSaPP8hQ/default.jpg
    - https://i3.ytimg.com/vi/wo7DSaPP8hQ/1.jpg
    - https://i3.ytimg.com/vi/wo7DSaPP8hQ/2.jpg
    - https://i3.ytimg.com/vi/wo7DSaPP8hQ/3.jpg
---
