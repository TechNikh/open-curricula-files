---
version: 1
type: video
provider: YouTube
id: Q9t1LghwdGc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Example%20of%20closed%20line%20integral%20of%20conservative%20field.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2312e164-e307-454b-a68f-33bb98414d27
updated: 1486069696
title: Example of closed line integral of conservative field
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/Q9t1LghwdGc/default.jpg
    - https://i3.ytimg.com/vi/Q9t1LghwdGc/1.jpg
    - https://i3.ytimg.com/vi/Q9t1LghwdGc/2.jpg
    - https://i3.ytimg.com/vi/Q9t1LghwdGc/3.jpg
---
