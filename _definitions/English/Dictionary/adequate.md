---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adequate
offline_file: ""
offline_thumbnail: ""
uuid: 49f6a540-34da-4356-8eda-cdc542188927
updated: 1484310445
title: adequate
categories:
    - Dictionary
---
adequate
     adj 1: (sometimes followed by `to') meeting the requirements
            especially of a task; "she had adequate training";
            "her training was adequate"; "she was adequate to the
            job" [ant: {inadequate}]
     2: enough to meet a purpose; "an adequate income"; "the food
        was adequate"; "a decent wage"; "enough food"; "food
        enough" [syn: {decent}, {enough}]
     3: about average; acceptable; "more than adequate as a
        secretary" [syn: {passable}, {fair to middling}]
