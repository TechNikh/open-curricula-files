---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pig
offline_file: ""
offline_thumbnail: ""
uuid: d8e2f88b-e32c-4701-9246-915fde5d11d8
updated: 1484310283
title: pig
categories:
    - Dictionary
---
pig
     n 1: domestic swine [syn: {hog}, {grunter}, {squealer}, {Sus
          scrofa}]
     2: a coarse obnoxious person [syn: {slob}, {sloven}, {slovenly
        person}]
     3: a person regarded as greedy and pig-like [syn: {hog}]
     4: uncomplimentary terms for a policeman [syn: {bull}, {cop}, {copper},
         {fuzz}]
     5: mold consisting of a bed of sand in which pig iron is cast
        [syn: {pig bed}]
     6: a crude block of metal (lead or iron) poured from a smelting
        furnace
     v 1: live like a pig, in squalor [syn: {pig it}]
     2: eat greedily; "he devoured three sandwiches" [syn: {devour},
         {guttle}, {raven}]
     3: give birth; "sows farrow" [syn: ...
