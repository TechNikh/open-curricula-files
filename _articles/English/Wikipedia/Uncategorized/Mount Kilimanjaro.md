---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kilimanjaro
offline_file: ""
offline_thumbnail: ""
uuid: 2b3d1dcb-5ab0-4fa3-8220-095c30cf9dc4
updated: 1484308342
title: Mount Kilimanjaro
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Mount_Kilimanjaro_Dec_2009_edit1.jpg
tags:
    - Geology and physical features
    - Geology
    - Drainage
    - Name
    - History
    - First sightings by non-indigenous explorers
    - Climbing history
    - Nineteenth-century explorers
    - Fastest male ascent and descent
    - Fastest female ascent and descent
    - Youngest and oldest people to summit
    - Ascents by people with disabilities
    - First Descent By Snowboard
    - Trekking Kilimanjaro
    - Overview
    - Dangers
    - Deaths
    - Mapping
    - Vegetation
    - Animal life
    - Climate
    - Glaciers
    - Mythology
    - In popular culture
    - Miscellany
categories:
    - Uncategorized
---
Mount Kilimanjaro (pronunciation: /ˌkɪlɪmənˈdʒɑːroʊ/),[6] with its three volcanic cones, "Kibo", "Mawenzi", and "Shira", is a dormant volcano in Tanzania. It is the highest mountain in Africa, and rises approximately 4,900 m (16,000 ft) from its base to 5,895 metres (19,341 ft) above sea level. The first recorded ascent to the summit of the mountain was by Hans Meyer and Ludwig Purtscheller in 1889. The mountain is part of the Kilimanjaro National Park and is a major climbing destination. The mountain has been the subject of many scientific studies because of its shrinking glaciers.
