---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Uncorrelated_asymmetry
offline_file: ""
offline_thumbnail: ""
uuid: 2e393872-4bef-4788-9ea5-f1c8d2a3d334
updated: 1484309321
title: Uncorrelated asymmetry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_19.png
categories:
    - Symmetry
---
In game theory an uncorrelated asymmetry is an arbitrary asymmetry in a game which is otherwise symmetrical. The name 'uncorrelated asymmetry' is due to John Maynard Smith who called payoff relevant asymmetries in games with similar roles for each player 'correlated asymmetries' (note that any game with correlated asymmetries must also have uncorrelated asymmetries).
