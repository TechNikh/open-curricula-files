---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Quality_control_system_for_paper,_board_and_tissue_machines
offline_file: ""
offline_thumbnail: ""
uuid: 9f80ea60-d2a9-430b-8cc3-a0bc5fdd6b48
updated: 1484308727
title: Quality control system for paper, board and tissue machines
tags:
    - Sensor platform
    - Scanner beam
    - Variables to be measured
    - Measurement process
    - Sensor requirements
categories:
    - Control engineering
---
A quality control system (QCS) refers to a system used to measure and control the quality of moving sheet processes on-line as in the paper produced by a paper machine. Generally, a control system is concerned with measurement and control of one or multiple properties in time in a single dimension. A QCS is designed to continuously measure and control the material properties of the moving sheet in two dimensions: in the machine direction (MD) and the cross-machine direction (CD). The ultimate goal is maintaining a good and homogenous quality and meeting users' economic goals.A basic quality measurement system generally includes basis weight and moisture profile measurements and in addition ...
