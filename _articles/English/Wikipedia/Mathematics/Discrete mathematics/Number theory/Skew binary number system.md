---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Skew_binary_number_system
offline_file: ""
offline_thumbnail: ""
uuid: 9f3b5756-b860-4826-ae72-ca3774029feb
updated: 1484308659
title: Skew binary number system
tags:
    - Examples
    - Arithmetical operations
    - From skew binary representation to binary representation
    - From binary representation to skew binary representation
    - Applications
    - Notes
categories:
    - Number theory
---
The skew binary number system is a non-standard positional numeral system in which the nth digit has a value of 
  
    
      
        
          2
          
            n
            +
            1
          
        
        −
        1
      
    
    {\displaystyle 2^{n+1}-1}
  
 (digits are indexed from 0) instead of 
  
    
      
        
          2
          
            n
          
        
      
    
    {\displaystyle 2^{n}}
  
 as they have in binary. Each digit has a value of 0, 1, or 2. Notice that a number in decimal can have many skew binary representations. For example a decimal number 15 can be written as 1000, 201 and 122. Each number can be written uniquely in ...
