---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illustrative
offline_file: ""
offline_thumbnail: ""
uuid: ef83c787-7a7d-4092-94f1-dc8670e454ef
updated: 1484310457
title: illustrative
categories:
    - Dictionary
---
illustrative
     adj 1: clarifying by use of examples [syn: {exemplifying}]
     2: serving to demonstrate [syn: {demonstrative}]
