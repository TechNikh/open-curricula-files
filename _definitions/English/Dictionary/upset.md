---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upset
offline_file: ""
offline_thumbnail: ""
uuid: 6484acb8-ae7e-4bde-81cd-56db2f4b6b06
updated: 1484310479
title: upset
categories:
    - Dictionary
---
upset
     adj 1: afflicted with or marked by anxious uneasiness or trouble or
            grief; "too upset to say anything"; "spent many
            disquieted moments"; "distressed about her son's
            leaving home"; "lapsed into disturbed sleep"; "worried
            parents"; "a worried frown"; "one last worried check
            of the sleeping children" [syn: {disquieted}, {distressed},
             {disturbed}, {worried}]
     2: thrown into a state of disarray or confusion; "troops
        fleeing in broken ranks"; "a confused mass of papers on
        the desk"; "the small disordered room"; "with everything
        so upset" [syn: {broken}, {confused}, {disordered}]
     3: ...
