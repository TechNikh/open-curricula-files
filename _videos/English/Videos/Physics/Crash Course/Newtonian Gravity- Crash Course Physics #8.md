---
version: 1
type: video
provider: YouTube
id: 7gf6YpdvtE0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 83ba0177-c9ed-433a-ba1f-7cfcd3c41d85
updated: 1486069682
title: 'Newtonian Gravity- Crash Course Physics #8'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/7gf6YpdvtE0/default.jpg
    - https://i3.ytimg.com/vi/7gf6YpdvtE0/1.jpg
    - https://i3.ytimg.com/vi/7gf6YpdvtE0/2.jpg
    - https://i3.ytimg.com/vi/7gf6YpdvtE0/3.jpg
---
