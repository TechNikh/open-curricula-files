---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impressive
offline_file: ""
offline_thumbnail: ""
uuid: e34675b4-5de8-43bb-ab8e-de144c13fda7
updated: 1484310609
title: impressive
categories:
    - Dictionary
---
impressive
     adj 1: making a strong or vivid impression; "an impressive
            ceremony" [ant: {unimpressive}]
     2: producing a strong effect; "gave an impressive performance
        as Othello"; "a telling gesture" [syn: {telling}]
