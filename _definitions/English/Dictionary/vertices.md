---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vertices
offline_file: ""
offline_thumbnail: ""
uuid: 4d5a8121-a29e-4d1f-a13a-66ed4498c82d
updated: 1484310216
title: vertices
categories:
    - Dictionary
---
vertex
     n 1: the point of intersection of lines or the point opposite the
          base of a figure
     2: the highest point (of something); "at the peak of the
        pyramid" [syn: {peak}, {apex}, {acme}]
     [also: {vertices} (pl)]
