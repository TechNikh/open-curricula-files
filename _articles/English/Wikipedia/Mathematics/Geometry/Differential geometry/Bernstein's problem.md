---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bernstein%27s_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 0c294225-2856-46bc-9e16-ad12a6241544
updated: 1484309204
title: "Bernstein's problem"
tags:
    - Statement
    - History
categories:
    - Differential geometry
---
In differential geometry, Bernstein's problem is as follows: if the graph of a function on Rn−1 is a minimal surface in Rn, does this imply that the function is linear? This is true in dimensions n at most 8, but false in dimensions n at least 9. The problem is named for Sergei Natanovich Bernstein who solved the case n = 3 in 1914.
