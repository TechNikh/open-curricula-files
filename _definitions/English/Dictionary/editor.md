---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/editor
offline_file: ""
offline_thumbnail: ""
uuid: 7180ebea-18a5-42d3-b28c-2271f28050d1
updated: 1484310144
title: editor
categories:
    - Dictionary
---
editor
     n 1: a person responsible for the editorial aspects of
          publication; the person who determines the final content
          of a text (especially of a newspaper or magazine) [syn:
          {editor in chief}]
     2: (computer science) a program designed to perform such
        editorial functions as rearrangement or modification or
        deletion of data [syn: {editor program}]
