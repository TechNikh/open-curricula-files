---
version: 1
type: video
provider: YouTube
id: u0weRjutkfw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 27d8c300-681a-41b1-8b7b-6c0802443eee
updated: 1486069605
title: Properties and patterns for multiplication
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/u0weRjutkfw/default.jpg
    - https://i3.ytimg.com/vi/u0weRjutkfw/1.jpg
    - https://i3.ytimg.com/vi/u0weRjutkfw/2.jpg
    - https://i3.ytimg.com/vi/u0weRjutkfw/3.jpg
---
