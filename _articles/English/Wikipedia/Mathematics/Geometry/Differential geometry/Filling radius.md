---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Filling_radius
offline_file: ""
offline_thumbnail: ""
uuid: e33b24d8-db16-45d3-8629-915f41b9f289
updated: 1484309219
title: Filling radius
tags:
    - Dual definition via neighborhoods
    - Homological definition
    - Properties
categories:
    - Differential geometry
---
In Riemannian geometry, the filling radius of a Riemannian manifold X is a metric invariant of X. It was originally introduced in 1983 by Mikhail Gromov, who used it to prove his systolic inequality for essential manifolds, vastly generalizing Loewner's torus inequality and Pu's inequality for the real projective plane, and creating systolic geometry in its modern form.
