---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/antacid
offline_file: ""
offline_thumbnail: ""
uuid: b28d0b6c-5f9e-4178-85c3-da82fc6d66cd
updated: 1484310381
title: antacid
categories:
    - Dictionary
---
antacid
     adj : acting to neutralize acid (especially in the stomach)
     n : an agent that counteracts or neutralizes acidity (especially
         in the stomach) [syn: {gastric antacid}, {alkalizer}, {alkaliser},
          {antiacid}]
