---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Divisor
offline_file: ""
offline_thumbnail: ""
uuid: e77b6247-6bd2-44dd-87c6-6ecd5ead1f48
updated: 1484308941
title: Divisor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cuisenaire_ten.JPG
tags:
    - Definition
    - General
    - Examples
    - Further notions and facts
    - In abstract algebra
    - Notes
categories:
    - Elementary number theory
---
In mathematics a divisor of an integer 
  
    
      
        n
      
    
    {\displaystyle n}
  
, also called a factor of 
  
    
      
        n
      
    
    {\displaystyle n}
  
, is an integer that can be multiplied by some other integer to produce 
  
    
      
        n
      
    
    {\displaystyle n}
  
. An integer 
  
    
      
        n
      
    
    {\displaystyle n}
  
 is divisible by another integer 
  
    
      
        m
      
    
    {\displaystyle m}
  
 if 
  
    
      
        m
      
    
    {\displaystyle m}
  
 is a factor of 
  
    
      
        n
      
    
    {\displaystyle n}
  
, so that dividing 
  
    
      
        n
      
    ...
