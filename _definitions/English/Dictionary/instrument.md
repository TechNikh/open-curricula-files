---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instrument
offline_file: ""
offline_thumbnail: ""
uuid: 99aebf30-213b-489d-b64e-0fff114a08c2
updated: 1484310200
title: instrument
categories:
    - Dictionary
---
instrument
     n 1: a device that requires skill for proper use
     2: the means whereby some act is accomplished; "my greed was
        the instrument of my destruction"; "science has given us
        new tools to fight disease" [syn: {tool}]
     3: a person used by another to gain an end [syn: {pawn}, {cat's-paw}]
     4: (law) a document that states some contractual relationship
        or grants some right [syn: {legal document}, {legal
        instrument}, {official document}]
     5: the semantic role of the entity (usually inanimate) that the
        agent uses to perform an action or start a process [syn: {instrumental
        role}]
     6: any of various devices or contrivances ...
