---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/International_Conference_on_Mechanical_Industrial_%26_Energy_Engineering'
offline_file: ""
offline_thumbnail: ""
uuid: 4cc594ad-dd49-45a6-9ede-fb35758c4346
updated: 1484308735
title: 'International Conference on Mechanical Industrial & Energy Engineering'
categories:
    - Control engineering
---
International Conference on Mechanical Industrial & Energy Engineering (ICMIEE) is held in Bangladesh every alternate 2 years starting from 2010.[1] The objective of ICMIEE is to present the latest research and results of scientists and Researchers. The conference provides opportunities for different areas' delegates to exchange new ideas and applications experiences face to face to establish research relationships.
