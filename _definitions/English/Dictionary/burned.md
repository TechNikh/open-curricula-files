---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/burned
offline_file: ""
offline_thumbnail: ""
uuid: 71b6cb4c-d8db-4e9e-8536-2d889266187c
updated: 1484310216
title: burned
categories:
    - Dictionary
---
burned
     adj 1: having undergone oxidation; "burned powder" [syn: {burnt}]
            [ant: {unburned}]
     2: injured by intense heat (as of fire or the sun); "his
        cracked, black burned lips"
     3: treated by heating to a high temperature but below the
        melting or fusing point; "burnt sienna" [syn: {burnt}]
     4: hardened by subjecting to intense heat; "baked bricks";
        "burned bricks" [syn: {baked}, {burnt}]
     5: destroyed or badly damaged by fire; "a row of burned
        houses"; "a charred bit of burnt wood"; "barricaded the
        street with burnt-out cars" [syn: {burnt}, {burned-out}, {burnt-out}]
     6: ruined by overcooking; "she served us ...
