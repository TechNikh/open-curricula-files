---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nurture
offline_file: ""
offline_thumbnail: ""
uuid: 2a8a4707-e11f-4120-b7cf-6a65d79f5169
updated: 1484310603
title: nurture
categories:
    - Dictionary
---
nurture
     n 1: the properties acquired as a consequence of the way you were
          treated as a child [syn: {raising}, {rearing}]
     2: raising someone to be an accepted member of the community;
        "they debated whether nature or nurture was more
        important" [syn: {breeding}, {bringing up}, {fostering}, {fosterage},
         {raising}, {rearing}, {upbringing}]
     v 1: help develop, help grow; "nurture his talents" [syn: {foster}]
     2: bring up; "raise a family"; "bring up children" [syn: {rear},
         {raise}, {bring up}, {parent}]
     3: provide with nourishment; "We sustained ourselves on bread
        and water"; "This kind of food is not nourishing for ...
