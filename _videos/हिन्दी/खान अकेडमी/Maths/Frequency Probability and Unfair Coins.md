---
version: 1
type: video
provider: YouTube
id: BsBXVpOBNQ8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 524a0c4d-4492-4cd7-94d9-58474814ece8
updated: 1486069609
title: Frequency Probability and Unfair Coins
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/BsBXVpOBNQ8/default.jpg
    - https://i3.ytimg.com/vi/BsBXVpOBNQ8/1.jpg
    - https://i3.ytimg.com/vi/BsBXVpOBNQ8/2.jpg
    - https://i3.ytimg.com/vi/BsBXVpOBNQ8/3.jpg
---
