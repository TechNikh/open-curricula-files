---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lakh
offline_file: ""
offline_thumbnail: ""
uuid: d430270b-138b-4811-b47c-b747d10b8a61
updated: 1484310475
title: lakh
categories:
    - Dictionary
---
lakh
     n : the cardinal number that is the fifth power of ten [syn: {hundred
         thousand}, {100000}]
