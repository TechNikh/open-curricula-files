---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dna
offline_file: ""
offline_thumbnail: ""
uuid: c4debc80-6d5f-41be-926e-b39c18565439
updated: 1484308347
title: DNA
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/340px-DNA_Structure%252BKey%252BLabelled.pn_NoBB.png'
tags:
    - Properties
    - Nucleobase classification
    - Grooves
    - Base pairing
    - Sense and antisense
    - Supercoiling
    - Alternative DNA structures
    - Alternative DNA chemistry
    - Quadruplex structures
    - Branched DNA
    - Chemical modifications and altered DNA packaging
    - Base modifications and DNA packaging
    - Damage
    - Biological functions
    - Genes and genomes
    - Transcription and translation
    - Replication
    - Extracellular nucleic acids
    - Interactions with proteins
    - DNA-binding proteins
    - DNA-modifying enzymes
    - Nucleases and ligases
    - Topoisomerases and helicases
    - Polymerases
    - Genetic recombination
    - Evolution
    - Uses in technology
    - Genetic engineering
    - DNA profiling
    - DNA enzymes or catalytic DNA
    - Bioinformatics
    - DNA nanotechnology
    - History and anthropology
    - Information storage
    - History of DNA research
categories:
    - Uncategorized
---
Deoxyribonucleic acid (i/diˈɒksiˌraɪboʊnjʊˌkliːɪk, -ˌkleɪɪk/;[1] DNA) is a molecule that carries the genetic instructions used in the growth, development, functioning and reproduction of all known living organisms and many viruses. DNA and RNA are nucleic acids; alongside proteins, lipids and complex carbohydrates (polysaccharides), they are one of the four major types of macromolecules that are essential for all known forms of life. Most DNA molecules consist of two biopolymer strands coiled around each other to form a double helix.
