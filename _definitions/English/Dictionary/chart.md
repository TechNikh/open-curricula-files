---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chart
offline_file: ""
offline_thumbnail: ""
uuid: 48bb297a-978a-476c-8936-6ca487bfd6ef
updated: 1484310346
title: chart
categories:
    - Dictionary
---
chart
     n 1: a visual display of information
     2: a map designed to assist navigation by air or sea
     v 1: make a chart of; "chart the territory"
     2: plan in detail; "Bush is charting a course to destroy Saddam
        Hussein"
     3: represent by means of a graph; "chart the data" [syn: {graph}]
