---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/authorised
offline_file: ""
offline_thumbnail: ""
uuid: a5b11e41-bdfb-4456-89b4-0577ee07f5a8
updated: 1484310180
title: authorised
categories:
    - Dictionary
---
authorised
     adj 1: endowed with authority [syn: {authorized}] [ant: {unauthorized}]
     2: sanctioned by established authority; "an authoritative
        communique"; "the authorized biography" [syn: {authoritative},
         {authorized}]
