---
version: 1
type: video
provider: YouTube
id: JL1ZO4lM-uw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 630f1585-f8b3-439a-b5d0-1463c1cac1ab
updated: 1486069617
title: >
    Example using algebra to find measure of complementary
    angles
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/JL1ZO4lM-uw/default.jpg
    - https://i3.ytimg.com/vi/JL1ZO4lM-uw/1.jpg
    - https://i3.ytimg.com/vi/JL1ZO4lM-uw/2.jpg
    - https://i3.ytimg.com/vi/JL1ZO4lM-uw/3.jpg
---
