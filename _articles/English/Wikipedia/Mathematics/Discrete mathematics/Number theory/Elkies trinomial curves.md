---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Elkies_trinomial_curves
offline_file: ""
offline_thumbnail: ""
uuid: fd208b00-745f-42f8-86a4-b8a84485ba45
updated: 1484308645
title: Elkies trinomial curves
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-C168.svg.png
categories:
    - Number theory
---
In number theory, the Elkies trinomial curves are certain hyperelliptic curves constructed by Noam Elkies which have the property that rational points on them correspond to trinomial polynomials giving an extension of Q with particular Galois groups.
