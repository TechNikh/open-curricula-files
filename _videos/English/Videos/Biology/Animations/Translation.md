---
version: 1
type: video
provider: YouTube
id: 5bLEDd-PSTQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Translation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 55b53c1c-b555-4831-8e2b-973c0c4e185d
updated: 1486069669
title: Translation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/5bLEDd-PSTQ/default.jpg
    - https://i3.ytimg.com/vi/5bLEDd-PSTQ/1.jpg
    - https://i3.ytimg.com/vi/5bLEDd-PSTQ/2.jpg
    - https://i3.ytimg.com/vi/5bLEDd-PSTQ/3.jpg
---
