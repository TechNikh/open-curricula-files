---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electromagnetic
offline_file: ""
offline_thumbnail: ""
uuid: 0442f2c1-7c0d-4654-a5a2-6a8ca259db21
updated: 1484310194
title: electromagnetic
categories:
    - Dictionary
---
electromagnetic
     adj : pertaining to or exhibiting magnetism produced by electric
           charge in motion; "electromagnetic energy"
