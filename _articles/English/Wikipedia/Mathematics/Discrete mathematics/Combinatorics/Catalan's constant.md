---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Catalan%27s_constant'
offline_file: ""
offline_thumbnail: ""
uuid: 6b6db0ce-b4c5-40ac-bec5-65d2555cb2a5
updated: 1484308563
title: "Catalan's constant"
tags:
    - Integral identities
    - Uses
    - Relation to other special functions
    - Quickly converging series
    - Known digits
    - Notes
categories:
    - Combinatorics
---
where β is the Dirichlet beta function. Its numerical value [1] is approximately (sequence A006752 in the OEIS)
