---
version: 1
type: video
provider: YouTube
id: lYEdR8-u9qo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d1ccf0ad-6668-44fa-9b69-7d06d03ac991
updated: 1486069605
title: 'Inverting 3x3 part 1  Calculating Matrix of Minors and Cofactor Matrix'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/lYEdR8-u9qo/default.jpg
    - https://i3.ytimg.com/vi/lYEdR8-u9qo/1.jpg
    - https://i3.ytimg.com/vi/lYEdR8-u9qo/2.jpg
    - https://i3.ytimg.com/vi/lYEdR8-u9qo/3.jpg
---
