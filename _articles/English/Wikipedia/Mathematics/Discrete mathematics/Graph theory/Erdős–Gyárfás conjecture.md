---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Gy%C3%A1rf%C3%A1s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: ccaec922-5c38-4d40-af13-f9719e31c7ab
updated: 1484308622
title: Erdős–Gyárfás conjecture
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Markstr%25C3%25B6m-Graph.svg.png'
categories:
    - Graph theory
---
In graph theory, the unproven Erdős–Gyárfás conjecture, made in 1995 by the prolific mathematician Paul Erdős and his collaborator András Gyárfás, states that every graph with minimum degree 3 contains a simple cycle whose length is a power of two. Erdős offered a prize of $100 for proving the conjecture, or $50 for a counterexample; it is one of many conjectures of Erdős.
