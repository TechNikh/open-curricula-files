---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Sumner%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: d419fd0a-f2ac-4c46-8d78-d82ee6850921
updated: 1484308620
title: "Sumner's conjecture"
tags:
    - Examples
    - Partial results
    - Related conjectures
    - Notes
categories:
    - Graph theory
---
David Sumner (a graph theorist at the University of South Carolina) conjectured in 1971 that tournaments are universal graphs for polytrees. More precisely, Sumner's conjecture (also called Sumner's universal tournament conjecture) states that every orientation of every 
  
    
      
        n
      
    
    {\displaystyle n}
  
-vertex tree is a subgraph of every 
  
    
      
        (
        2
        n
        −
        2
        )
      
    
    {\displaystyle (2n-2)}
  
-vertex tournament.[1] The conjecture remains unproven; Kühn, Mycroft & Osthus (2011a) call it "one of the most well-known problems on tournaments."
