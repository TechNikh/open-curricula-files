---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/modulation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484595421
title: modulation
categories:
    - Dictionary
---
modulation
     n 1: a musical passage moving from one key to another [syn: {transition}]
     2: (electronics) the transmission of a signal by using it to
        vary a carrier wave; changing the carrier's amplitude or
        frequency or phase
     3: rise and fall of the voice pitch [syn: {intonation}, {pitch
        contour}]
     4: a manner of speaking in which the loudness or pitch or tone
        of the voice is modified [syn: {inflection}]
     5: the act of modifying or adjusting according to due measure
        and proportion (as with regard to artistic effect)
