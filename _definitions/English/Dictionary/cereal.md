---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cereal
offline_file: ""
offline_thumbnail: ""
uuid: 8a928241-abe9-48bb-bf4a-b959837f7a51
updated: 1484310549
title: cereal
categories:
    - Dictionary
---
cereal
     adj : made of grain or relating to grain or the plants that
           produce it; "a cereal beverage"; "cereal grasses"
     n 1: grass whose starchy grains are used as food: wheat; rice;
          rye; oats; maize; buckwheat; millet [syn: {cereal grass}]
     2: foodstuff prepared from the starchy grains of cereal grasses
        [syn: {grain}, {food grain}]
     3: a breakfast food prepared from grain
