---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subsistence
offline_file: ""
offline_thumbnail: ""
uuid: c36b1c19-fe2d-406d-b866-862d97586df6
updated: 1484310545
title: subsistence
categories:
    - Dictionary
---
subsistence
     n 1: minimal (or marginal) resources for subsisting; "social
          security provided only a bare subsistence"
     2: a means of surviving; "farming is a hard means of
        subsistence"
     3: the state of existing in reality; having substance
