---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trap
offline_file: ""
offline_thumbnail: ""
uuid: f97a1d2b-5527-46f7-8203-f3717f1fd3fa
updated: 1484310264
title: trap
categories:
    - Dictionary
---
trap
     n 1: a device in which something (usually an animal) can be
          caught and penned
     2: drain consisting of a U-shaped section of drainpipe that
        holds liquid and so prevents a return flow of sewer gas
     3: something (often something deceptively attractive) that
        catches you unawares; "the exam was full of trap
        questions"; "it was all a snare and delusion" [syn: {snare}]
     4: a device to hurl clay pigeons into the air for trapshooters
     5: the act of concealing yourself and lying in wait to attack
        by surprise [syn: {ambush}, {ambuscade}, {lying in wait}]
     6: informal terms for the mouth [syn: {cakehole}, {hole}, {maw},
         ...
