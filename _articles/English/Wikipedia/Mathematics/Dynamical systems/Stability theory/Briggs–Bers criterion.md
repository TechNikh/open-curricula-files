---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Briggs%E2%80%93Bers_criterion'
offline_file: ""
offline_thumbnail: ""
uuid: 81de84d5-2bd3-44e7-afb2-1394b9f896f5
updated: 1484308828
title: Briggs–Bers criterion
categories:
    - Stability theory
---
In stability theory, the Briggs–Bers criterion is a criterion for determining whether the trivial solution to a linear partial differential equation with constant coefficients is stable, convectively unstable or absolutely unstable. This is often useful in applied mathematics, especially in fluid dynamics, because linear PDEs often govern small perturbations to a system, and we are interested in whether such perturbations grow or decay. The Briggs–Bers criterion is named after R. J. Briggs and A. Bers.[1]
