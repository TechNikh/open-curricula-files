---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Fredkin_finite_nature_hypothesis
offline_file: ""
offline_thumbnail: ""
uuid: 8425ddeb-a54c-4f8c-b9dd-a1864000513e
updated: 1484308671
title: Fredkin finite nature hypothesis
tags:
    - Conceptions
    - "Fredkin's ideas on inertia"
categories:
    - Theoretical computer science
---
In digital physics, the Fredkin finite nature hypothesis states that ultimately all quantities of physics, including space and time, are discrete and finite. All measurable physical quantities arise from some Planck scale substrate for multiverse information processing. Also, the amount of information in any small volume of spacetime will be finite and equal to a small number of possibilities.[1]
