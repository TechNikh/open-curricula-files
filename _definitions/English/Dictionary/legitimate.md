---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/legitimate
offline_file: ""
offline_thumbnail: ""
uuid: 556e1777-14fb-49f5-86d9-7bd299ae164d
updated: 1484310178
title: legitimate
categories:
    - Dictionary
---
legitimate
     adj 1: of marriages and offspring; recognized as lawful [ant: {illegitimate}]
     2: in accordance with reason or logic; "a logical conclusion"
        [syn: {logical}]
     3: in accordance with recognized or accepted standards or
        principles; "legitimate advertising practices"
     4: authorized, sanctioned by, or in accordance with law; "a
        legitimate government" [syn: {lawful}, {licit}]
     v 1: make legal; "Marijuana should be legalized" [syn: {legalize},
           {legalise}, {decriminalize}, {decriminalise}, {legitimize},
           {legitimise}, {legitimatize}, {legitimatise}] [ant: {outlaw},
           {outlaw}, {outlaw}, {outlaw}]
     2: show or ...
