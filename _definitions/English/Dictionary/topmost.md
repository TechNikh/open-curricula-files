---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/topmost
offline_file: ""
offline_thumbnail: ""
uuid: d0b5545b-797f-45c8-aa24-901d90f62201
updated: 1484310477
title: topmost
categories:
    - Dictionary
---
topmost
     adj : at or nearest to the top; "the uppermost book in the pile";
           "on the topmost step" [syn: {uppermost}, {upmost}]
