---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fpqc_morphism
offline_file: ""
offline_thumbnail: ""
uuid: cef9d3b8-1a65-42fe-9af9-003699be5b18
updated: 1484309362
title: Fpqc morphism
categories:
    - Geometry stubs
---
Sometimes a fpqc morphism means one that is faithfully flat and quasicompact. This is where the abbrviation fpqc comes from: fpqc stands for the French phrase "fidèlement plat et quasi-compact", meaning "faithfully flat and quasi-compact".
