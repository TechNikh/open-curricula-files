---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rna
offline_file: ""
offline_thumbnail: ""
uuid: cae6b2d9-e6b8-44b4-9206-dc22b3340776
updated: 1484094390
title: RNA
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pre-mRNA-1ysv-tubes.png
tags:
    - Comparison with DNA
    - Structure
    - Synthesis
    - Types of RNA
    - Overview
    - In length
    - In translation
    - Regulatory RNAs
    - In RNA processing
    - RNA genomes
    - In reverse transcription
    - Double-stranded RNA
    - Circular RNA
    - Key discoveries in RNA biology
    - Evolution
categories:
    - Uncategorized
---
Ribonucleic acid (RNA) is a polymeric molecule essential in various biological roles in coding, decoding, regulation, and expression of genes. RNA and DNA are nucleic acids, and, along with proteins and carbohydrates, constitute the four major macromolecules essential for all known forms of life. Like DNA, RNA is assembled as a chain of nucleotides, but unlike DNA it is more often found in nature as a single-strand folded onto itself, rather than a paired double-strand. Cellular organisms use messenger RNA (mRNA) to convey genetic information (using the letters G, U, A, and C to denote the nitrogenous bases guanine, uracil, adenine, and cytosine) that directs synthesis of specific proteins. ...
