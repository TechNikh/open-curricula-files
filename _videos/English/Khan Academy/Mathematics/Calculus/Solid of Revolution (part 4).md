---
version: 1
type: video
provider: YouTube
id: OtmjNuiTHp0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Solid%20of%20Revolution%20%28part%204%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 699aba1f-d5bc-416f-8aa4-7655df9c2db9
updated: 1486069694
title: Solid of Revolution (part 4)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/OtmjNuiTHp0/default.jpg
    - https://i3.ytimg.com/vi/OtmjNuiTHp0/1.jpg
    - https://i3.ytimg.com/vi/OtmjNuiTHp0/2.jpg
    - https://i3.ytimg.com/vi/OtmjNuiTHp0/3.jpg
---
