---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/planar
offline_file: ""
offline_thumbnail: ""
uuid: b39cd062-6bf7-43c0-b0e6-49b94116dc1a
updated: 1484310156
title: planar
categories:
    - Dictionary
---
planar
     adj : involving two dimensions [syn: {two-dimensional}] [ant: {cubic},
            {linear}]
