---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/evolution
offline_file: ""
offline_thumbnail: ""
uuid: 4f2b8ba0-1ece-4a15-a7fb-7e98d9f85646
updated: 1484310309
title: evolution
categories:
    - Dictionary
---
evolution
     n 1: a process in which something passes by degrees to a
          different stage (especially a more advanced or mature
          stage); "the development of his ideas took many years";
          "the evolution of Greek civilization"; "the slow
          development of her skill as a writer" [syn: {development}]
          [ant: {degeneration}]
     2: (biology) the sequence of events involved in the
        evolutionary development of a species or taxonomic group
        of organisms [syn: {phylogeny}, {phylogenesis}]
