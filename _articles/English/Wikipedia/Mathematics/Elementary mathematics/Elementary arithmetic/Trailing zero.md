---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trailing_zero
offline_file: ""
offline_thumbnail: ""
uuid: 5d42b97f-3201-4b8f-9802-ee82452d046a
updated: 1484308924
title: Trailing zero
tags:
    - Factorial
categories:
    - Elementary arithmetic
---
In mathematics, trailing zeros are a sequence of 0s in the decimal representation (or more generally, in any positional representation) of a number, after which no other digits follow.
