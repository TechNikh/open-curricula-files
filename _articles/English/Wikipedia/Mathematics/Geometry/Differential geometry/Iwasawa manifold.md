---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Iwasawa_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 61d76cc6-6f81-4088-8dd3-a56bb8a418b7
updated: 1484309226
title: Iwasawa manifold
categories:
    - Differential geometry
---
In mathematics, in the field of differential geometry, an Iwasawa manifold is a compact quotient of a 3-dimensional complex Heisenberg group by a cocompact, discrete subgroup. An Iwasawa manifold is a nilmanifold, of real dimension 6.
