---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commentary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484462341
title: commentary
categories:
    - Dictionary
---
commentary
     n : a written explanation or criticism or illustration that is
         added to a book or other textual material; "he wrote an
         extended comment on the proposal" [syn: {comment}]
