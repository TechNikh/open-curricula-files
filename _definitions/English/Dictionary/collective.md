---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collective
offline_file: ""
offline_thumbnail: ""
uuid: 8470b557-5eb4-4c00-8f20-0ff827d9254b
updated: 1484310448
title: collective
categories:
    - Dictionary
---
collective
     adj 1: done by or characteristic of individuals acting together; "a
            joint identity"; "the collective mind"; "the corporate
            good" [syn: {corporate}]
     2: forming a whole or aggregate [ant: {distributive}]
     3: set up on the principle of collectivism or ownership and
        production by the workers involved usually under the
        supervision of a government; "collective farms"
     n : members of a cooperative enterprise
