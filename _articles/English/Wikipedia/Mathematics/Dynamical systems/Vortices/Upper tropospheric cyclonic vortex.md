---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Upper_tropospheric_cyclonic_vortex
offline_file: ""
offline_thumbnail: ""
uuid: a1960691-ba36-4e37-9c2b-5ea0bccc5b18
updated: 1484308879
title: Upper tropospheric cyclonic vortex
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-TUTTcellWestPac2007071012WV_0.jpg
tags:
    - History of research
    - characteristics
    - Climatology
    - Interaction with tropical cyclones
    - Interaction with monsoon regimes
categories:
    - Vortices
---
An upper tropospheric cyclonic vortex is a vortex, or a circulation with a definable center, that usually moves slowly from east-northeast to west-southwest and is prevalent across Northern Hemisphere's warm season. Its circulations generally do not extend below 6,080 metres (19,950 ft) in altitude, as it is an example of a cold-core low. A weak inverted wave in the easterlies is generally found beneath it, and it may also be associated with broad areas of high-level clouds. Downward development results in an increase of cumulus clouds and the appearance of circulation at ground level. In rare cases, a warm-core cyclone can develop in its associated convective activity, resulting in a ...
