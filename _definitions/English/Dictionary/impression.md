---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impression
offline_file: ""
offline_thumbnail: ""
uuid: e67fde6c-92cb-427c-8204-c786dda82e41
updated: 1484310283
title: impression
categories:
    - Dictionary
---
impression
     n 1: a vague idea in which some confidence is placed; "his
          impression of her was favorable"; "what are your
          feelings about the crisis?"; "it strengthened my belief
          in his sincerity"; "I had a feeling that she was lying"
          [syn: {feeling}, {belief}, {notion}, {opinion}]
     2: an outward appearance; "he made a good impression"; "I
        wanted to create an impression of success"; "she retained
        that bold effect in her reproductions of the original
        painting" [syn: {effect}]
     3: a clear and telling mental image; "he described his mental
        picture of his assailant"; "he had no clear picture of
        himself or ...
