---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sand
offline_file: ""
offline_thumbnail: ""
uuid: e4a30c23-e58f-4cd5-9835-eb5c78080966
updated: 1484310266
title: sand
categories:
    - Dictionary
---
sand
     n 1: a loose material consisting of grains of rock or coral
     2: French writer known for works concerning women's rights and
        independence (1804-1876) [syn: {George Sand}, {Amandine
        Aurore Lucie Dupin}, {Baroness Dudevant}]
     3: fortitude and determination; "he didn't have the guts to try
        it" [syn: {backbone}, {grit}, {guts}, {moxie}, {gumption}]
     v : rub with sandpaper; "sandpaper the wooden surface" [syn: {sandpaper}]
