---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/theme
offline_file: ""
offline_thumbnail: ""
uuid: fe25f3fd-70b9-4b04-a1bd-fea33a086955
updated: 1484310148
title: theme
categories:
    - Dictionary
---
theme
     n 1: the subject matter of a conversation or discussion; "he
          didn't want to discuss that subject"; "it was a very
          sensitive topic"; "his letters were always on the theme
          of love" [syn: {subject}, {topic}]
     2: a unifying idea that is a recurrent element in a literary or
        artistic work; "it was the usual `boy gets girl' theme"
        [syn: {motif}]
     3: (music) melodic subject of a musical composition; "the theme
        is announced in the first measures"; "the accompanist
        picked up the idea and elaborated it" [syn: {melodic theme},
         {musical theme}, {idea}]
     4: an essay (especially one written as an assignment); "he ...
