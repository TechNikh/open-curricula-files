---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/encompass
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484560081
title: encompass
categories:
    - Dictionary
---
encompass
     v : include in scope; include as part of something broader; have
         as one's sphere or territory; "This group encompasses a
         wide range of people from different backgrounds"; "this
         should cover everyone in the group" [syn: {embrace}, {comprehend},
          {cover}]
