---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/story
offline_file: ""
offline_thumbnail: ""
uuid: 6cd6e7ee-fbec-47a8-935c-2115a401ba25
updated: 1484310216
title: story
categories:
    - Dictionary
---
story
     n 1: a message that tells the particulars of an act or occurrence
          or course of events; presented in writing or drama or
          cinema or as a radio or television program; "his
          narrative was interesting"; "Disney's stories entertain
          adults as well as children" [syn: {narrative}, {narration},
           {tale}]
     2: a piece of fiction that narrates a chain of related events;
        "he writes stories for the magazines"
     3: structure consisting of a room or set of rooms comprising a
        single level of a multilevel building; "what level is the
        office on?" [syn: {floor}, {level}, {storey}]
     4: a record or narrative description ...
