---
version: 1
type: video
provider: YouTube
id: c3A8ZbNUVRY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20Cosmology.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 93341167-75c3-4cc4-bb13-c6daa149ac6d
updated: 1486069653
title: What is Cosmology
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/c3A8ZbNUVRY/default.jpg
    - https://i3.ytimg.com/vi/c3A8ZbNUVRY/1.jpg
    - https://i3.ytimg.com/vi/c3A8ZbNUVRY/2.jpg
    - https://i3.ytimg.com/vi/c3A8ZbNUVRY/3.jpg
---
