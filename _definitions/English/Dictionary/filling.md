---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/filling
offline_file: ""
offline_thumbnail: ""
uuid: 98e17474-38ed-4062-96d8-79731382455b
updated: 1484310234
title: filling
categories:
    - Dictionary
---
filling
     n 1: any material that fills a space or container; "there was not
          enough fill for the trench" [syn: {fill}]
     2: flow into something (as a container)
     3: (dentistry) a dental appliance consisting of any of various
        substances (as metal or plastic) inserted into a prepared
        cavity in a tooth; "when he yawned I could see the gold
        fillings in his teeth"; "an informal British term for
        `filling' is `stopping'"
     4: a food mixture used to fill pastry or sandwiches etc.
     5: the yarn woven across the warp yarn in weaving [syn: {woof},
         {weft}, {pick}]
     6: the act of filling something
