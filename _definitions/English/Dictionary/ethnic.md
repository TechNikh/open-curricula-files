---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethnic
offline_file: ""
offline_thumbnail: ""
uuid: ad13f605-75ac-4f55-a1b2-89493cbea65f
updated: 1484310581
title: ethnic
categories:
    - Dictionary
---
ethnic
     adj 1: denoting or deriving from or distinctive of the ways of
            living built up by a group of people; "influenced by
            ethnic and cultural ties"- J.F.Kennedy; "ethnic food"
            [syn: {cultural}, {ethnical}]
     2: not acknowledging the God of Christianity and Judaism and
        Islam [syn: {heathen}, {heathenish}, {pagan}]
