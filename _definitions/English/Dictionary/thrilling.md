---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thrilling
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484484301
title: thrilling
categories:
    - Dictionary
---
thrilling
     adj 1: causing a surge of emotion or excitement; "she gave a
            electrifying performance"; "a thrilling performer to
            watch" [syn: {electrifying}]
     2: causing quivering or shivering as by cold or fear or
        electric shock; "a thrilling wind blew off the frozen
        lake"
