---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Cantic_order-4_hexagonal_tiling
offline_file: ""
offline_thumbnail: ""
uuid: ac9f175e-cfb1-4a6b-a4db-f29276fc2dc6
updated: 1484309357
title: Cantic order-4 hexagonal tiling
tags:
    - Related polyhedra and tiling
categories:
    - Geometry stubs
---
In geometry, the tritetratrigonal tiling or cantic order-4 hexagonal tiling is a uniform tiling of the hyperbolic plane. It has Schläfli symbol of t0,1{(4,4,3)} or h2{6,4}.
