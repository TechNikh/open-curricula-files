---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Courant_bracket
offline_file: ""
offline_thumbnail: ""
uuid: 3d10adb7-d1d7-4217-88f7-90f64c5bbe61
updated: 1484309212
title: Courant bracket
tags:
    - Definition
    - Properties
    - The Jacobi identity
    - Symmetries
    - Dirac and generalized complex structures
    - Dorfman bracket
    - Courant algebroid
    - Twisted Courant bracket
    - Definition and properties
    - 'p=0: Circle-invariant vector fields'
    - Integral twists and gerbes
categories:
    - Differential geometry
---
In a field of mathematics known as differential geometry, the Courant bracket is a generalization of the Lie bracket from an operation on the tangent bundle to an operation on the direct sum of the tangent bundle and the vector bundle of p-forms.
