---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/factory
offline_file: ""
offline_thumbnail: ""
uuid: b6a1770b-22b2-4314-9985-716f84c5c73c
updated: 1484310249
title: factory
categories:
    - Dictionary
---
factory
     n : a plant consisting of buildings with facilities for
         manufacturing [syn: {mill}, {manufacturing plant}, {manufactory}]
