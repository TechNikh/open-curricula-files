---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/critic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484501221
title: critic
categories:
    - Dictionary
---
critic
     n 1: a person who is professionally engaged in the analysis and
          interpretation of works of art
     2: anyone who expresses a reasoned judgment of something
     3: someone who frequently finds fault or makes harsh and unfair
        judgments
