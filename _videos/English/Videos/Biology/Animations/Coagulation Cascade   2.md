---
version: 1
type: video
provider: YouTube
id: YSRga6buuR8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Coagulation%20Cascade%20%20%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b38973fc-5059-4ad1-8542-dc51ff008453
updated: 1486069666
title: 'Coagulation Cascade   2'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/YSRga6buuR8/default.jpg
    - https://i3.ytimg.com/vi/YSRga6buuR8/1.jpg
    - https://i3.ytimg.com/vi/YSRga6buuR8/2.jpg
    - https://i3.ytimg.com/vi/YSRga6buuR8/3.jpg
---
