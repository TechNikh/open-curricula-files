---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/news
offline_file: ""
offline_thumbnail: ""
uuid: 03e8e754-39bc-4407-8707-b595e43c9185
updated: 1484310238
title: news
categories:
    - Dictionary
---
news
     n 1: new information about specific and timely events; "they
          awaited news of the outcome" [syn: {intelligence}, {tidings},
           {word}]
     2: new information of any kind; "it was news to me"
     3: a program devoted to news; "we watch the 7 o'clock news
        every night" [syn: {news program}, {news show}]
     4: information reported in a newspaper or news magazine; "the
        news of my death was greatly exaggerated"
     5: the quality of being sufficiently interesting to be reported
        in news bulletins; "the judge conceded the newsworthiness
        of the trial"; "he is no longer news in the fashion world"
        [syn: {newsworthiness}]
