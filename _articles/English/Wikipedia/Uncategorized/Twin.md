---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Identical_twins
offline_file: ""
offline_thumbnail: ""
uuid: 9e532cfa-f782-4e6c-a07f-8923d64c9382
updated: 1484308371
title: Twin
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Marian_and_Vivian_Brown.jpg
tags:
    - Statistics
    - 'Types of Twins & Zygosity'
    - Dizygotic (fraternal) twins
    - Monozygotic (identical) twins
    - Mechanism
    - Incidence
    - Genetic and epigenetic similarity
    - 'Polar Body & Semi-Identical Twin'
    - Unknown Twin Type
    - Degree of separation
    - Demographics
    - Ethnicity
    - Predisposing factors
    - Delivery interval
    - Complications during pregnancy
    - Vanishing twins
    - Conjoined twins
    - Chimerism
    - Parasitic twins
    - Partial molar twins
    - Miscarried twin
    - Low birth weight
    - Twin-to-twin transfusion syndrome
    - Stillbirths
    - Management of birth
    - Human twin studies
    - Unusual twinnings
    - Semi-identical twins
    - Mirror image twins
    - Language development
    - Other animals
    - Foundations
categories:
    - Uncategorized
---
Twins are two offspring produced by the same pregnancy.[1] Twins can be either monozygotic ("identical"), meaning that they develop from one zygote, which splits and forms two embryos, or dizygotic ("fraternal"), meaning that they develop from two different eggs. In fraternal twins, each twin is fertilized by its own sperm cell.[2]
