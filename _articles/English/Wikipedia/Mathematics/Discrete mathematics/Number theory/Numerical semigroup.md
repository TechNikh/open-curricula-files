---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Numerical_semigroup
offline_file: ""
offline_thumbnail: ""
uuid: 67e72193-25ae-48bf-9c1a-1cd0af3e696d
updated: 1484308649
title: Numerical semigroup
tags:
    - Definition and examples
    - Definition
    - Theorem
    - Examples
    - Embedding dimension, multiplicity
    - Frobenius number and genus
    - Examples
    - Computation of Frobenius number
    - Numerical semigroups with embedding dimension two
    - Numerical semigroups with embedding dimension three
    - "Rödseth's algorithm"
    - Special classes of numerical semigroups
categories:
    - Number theory
---
In mathematics, a numerical semigroup is a special kind of a semigroup. Its underlying set is the set of all nonnegative integers except a finite number and the binary operation is the operation of addition of integers. Also, the integer 0 must be an element of the semigroup. For example, while the set {0, 2, 3, 4, 5, 6, ...} is a numerical semigroup, the set {0, 1, 3, 5, 6, ...} is not because 1 is in the set and 1 + 1 = 2 is not in the set. Numerical semigroups are commutative monoids and are also known as numerical monoids. [1][2]
