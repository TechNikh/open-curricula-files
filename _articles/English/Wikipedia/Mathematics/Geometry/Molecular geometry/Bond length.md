---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bond_length
offline_file: ""
offline_thumbnail: ""
uuid: 8e310484-c71f-4e41-b236-c6d795a5e895
updated: 1484309277
title: Bond length
tags:
    - Explanation
    - Bond lengths of carbon with other elements
    - Bond lengths in organic compounds
categories:
    - Molecular geometry
---
In molecular geometry, bond length or bond distance is the average distance between nuclei of two bonded atoms in a molecule. It is a transferable property of a bond between atoms of fixed types, relatively independent of the rest of the molecule.
