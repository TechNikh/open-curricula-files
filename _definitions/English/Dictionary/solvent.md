---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solvent
offline_file: ""
offline_thumbnail: ""
uuid: b145b267-8138-48dc-a3f8-981a46f9053a
updated: 1484310311
title: solvent
categories:
    - Dictionary
---
solvent
     adj : capable of meeting financial obligations [ant: {insolvent}]
     n 1: a liquid substance capable of dissolving other substances;
          "the solvent does not change its state in forming a
          solution" [syn: {dissolvent}, {dissolver}, {dissolving
          agent}, {resolvent}]
     2: a statement that solves a problem or explains how to solve
        the problem; "they were trying to find a peaceful
        solution"; "the answers were in the back of the book"; "he
        computed the result to four decimal places" [syn: {solution},
         {answer}, {result}, {resolution}]
