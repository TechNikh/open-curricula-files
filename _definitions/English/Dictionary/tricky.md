---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tricky
offline_file: ""
offline_thumbnail: ""
uuid: 27a1f0ca-9ed3-4ee5-941b-b745782e537a
updated: 1484310238
title: tricky
categories:
    - Dictionary
---
tricky
     adj 1: not to be trusted; "how extraordinarily slippery a liar the
            camera is"- James Agee; "they called Reagan the teflon
            president because mud never stuck to him" [syn: {slippery},
             {teflon}]
     2: having concealed difficulty; "a catchy question"; "a tricky
        recipe to follow" [syn: {catchy}]
     3: marked by skill in deception; "cunning men often pass for
        wise"; "deep political machinations"; "a foxy scheme"; "a
        slick evasive answer"; "sly as a fox"; "tricky Dik"; "a
        wily old attorney" [syn: {crafty}, {cunning}, {dodgy}, {foxy},
         {guileful}, {knavish}, {slick}, {sly}, {tricksy}, {wily}]
     [also: ...
