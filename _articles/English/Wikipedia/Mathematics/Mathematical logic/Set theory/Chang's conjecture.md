---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Chang%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: ec08138d-4691-4334-9e3e-7da68b9bb21b
updated: 1484309501
title: "Chang's conjecture"
categories:
    - Set theory
---
In model theory, a branch of mathematical logic, Chang's conjecture, attributed to Chen Chung Chang by Vaught (1963, p. 309), states that every model of type (ω2,ω1) for a countable language has an elementary submodel of type (ω1, ω). A model is of type (α,β) if it is of cardinality α and a unary relation is represented by a subset of cardinality β. The usual notation is 
  
    
      
        (
        
          ω
          
            2
          
        
        ,
        
          ω
          
            1
          
        
        )
        ↠
        (
        
          ω
          
            1
          
        
        ,
        ω
        )
      
    
    ...
