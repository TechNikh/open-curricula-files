---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Irrationality_sequence
offline_file: ""
offline_thumbnail: ""
uuid: 89bc405f-c6b0-4e5d-80cd-121198057adf
updated: 1484308645
title: Irrationality sequence
tags:
    - Examples
    - Growth rate
    - Related properties
categories:
    - Number theory
---
In mathematics, a sequence of positive integers an is called an irrationality sequence if it has the property that for every sequence xn of positive integers, the sum of the series
