---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gross
offline_file: ""
offline_thumbnail: ""
uuid: ffa0da39-7a3c-4ae9-8181-6753863862ce
updated: 1484310451
title: gross
categories:
    - Dictionary
---
gross
     adj 1: before any deductions; "gross income" [ant: {net}]
     2: visible to the naked eye (especially of rocks and anatomical
        features) [syn: {megascopic}]
     3: of general aspects or broad distinctions; "the gross details
        of the structure appear reasonable"
     4: repellently fat; "a bald porcine old man" [syn: {porcine}]
     5: conspicuously and outrageously bad or reprehensible; "a
        crying shame"; "an egregious lie"; "flagrant violation of
        human rights"; "a glaring error"; "gross ineptitude";
        "gross injustice"; "rank treachery" [syn: {crying(a)}, {egregious},
         {flagrant}, {glaring}, {rank}]
     6: without qualification; used ...
