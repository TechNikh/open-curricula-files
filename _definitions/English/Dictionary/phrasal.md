---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/phrasal
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484584981
title: phrasal
categories:
    - Dictionary
---
phrasal
     adj : of or relating to or functioning as a phrase; "phrasal verb"
