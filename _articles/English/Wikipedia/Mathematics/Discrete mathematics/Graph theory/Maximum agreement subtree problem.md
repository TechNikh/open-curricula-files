---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Maximum_agreement_subtree_problem
offline_file: ""
offline_thumbnail: ""
uuid: ba8f43a1-1daa-49a3-b7ca-461859052a76
updated: 1484308625
title: Maximum agreement subtree problem
tags:
    - Formulations
    - 'Maximum homeomorphic agreement subtree[1]'
    - Rooted maximum homeomorphic agreement subtree
    - Other variants
categories:
    - Graph theory
---
The maximum agreement subtree problem is any of several closely related problems problems in graph theory and computer science. In all of these problems one is given a collection of trees 
  
    
      
        
          T
          
            1
          
        
        ,
        …
        ,
        
          T
          
            m
          
        
      
    
    {\displaystyle T_{1},\ldots ,T_{m}}
  
 each containing 
  
    
      
        n
      
    
    {\displaystyle n}
  
 leaves. The leaves of these trees are given labels from some set 
  
    
      
        L
      
    
    {\displaystyle L}
  
 with 
  
    
      
        
          |
        
        L
      ...
