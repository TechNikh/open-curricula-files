---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paw
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484484241
title: paw
categories:
    - Dictionary
---
paw
     n 1: a clawed foot of an animal especially a quadruped
     2: the (prehensile) extremity of the superior limb; "he had the
        hands of a surgeon"; "he extended his mitt" [syn: {hand},
        {manus}, {mitt}]
     v 1: scrape with the paws; "The bear pawed the door"
     2: touch clumsily; "The man tried to paw her"
