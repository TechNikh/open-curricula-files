---
version: 1
type: video
provider: YouTube
id: vl9o9XEfXtw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Quadratic%20equations/Dimensions%20from%20volume%20of%20box.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0ab70097-4e1b-4d63-8fe2-e7d0a05ccf8b
updated: 1486069557
title: Dimensions from volume of box
categories:
    - Quadratic equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/vl9o9XEfXtw/default.jpg
    - https://i3.ytimg.com/vi/vl9o9XEfXtw/1.jpg
    - https://i3.ytimg.com/vi/vl9o9XEfXtw/2.jpg
    - https://i3.ytimg.com/vi/vl9o9XEfXtw/3.jpg
---
