---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lie_algebroid
offline_file: ""
offline_thumbnail: ""
uuid: 5ba4be09-d955-402d-adb5-5daa07ba0535
updated: 1484309226
title: Lie algebroid
tags:
    - Examples
    - Lie algebroid associated to a Lie groupoid
categories:
    - Differential geometry
---
In mathematics, Lie algebroids serve the same role in the theory of Lie groupoids that Lie algebras serve in the theory of Lie groups: reducing global problems to infinitesimal ones. Just as a Lie groupoid can be thought of as a "Lie group with many objects", a Lie algebroid is like a "Lie algebra with many objects".
