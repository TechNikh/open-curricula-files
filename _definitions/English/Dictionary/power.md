---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/power
offline_file: ""
offline_thumbnail: ""
uuid: 1056e262-eaa9-40d9-aef8-7f619523c073
updated: 1484310252
title: power
categories:
    - Dictionary
---
power
     n 1: possession of controlling influence; "the deterrent power of
          nuclear weapons"; "the power of his love saved her";
          "his powerfulness was concealed by a gentle facade"
          [syn: {powerfulness}] [ant: {powerlessness}, {powerlessness}]
     2: (physics) the rate of doing work; measured in watts (=
        joules/second)
     3: possession of the qualities (especially mental qualities)
        required to do something or get something done; "danger
        heightened his powers of discrimination" [syn: {ability}]
        [ant: {inability}]
     4: a state powerful enough to influence events throughout the
        world [syn: {world power}, {major power}, ...
