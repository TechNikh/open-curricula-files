---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proust
offline_file: ""
offline_thumbnail: ""
uuid: 1374f241-d47f-4309-81e8-afbe161a23c1
updated: 1484308380
title: Marcel Proust
tags:
    - Biography
    - Early writing
    - In Search of Lost Time
    - Personal life
    - Gallery
    - Bibliography
    - Translations
categories:
    - Uncategorized
---
Valentin Louis Georges Eugène Marcel Proust (/pruːst/;[1] French: [maʁsɛl pʁust]; 10 July 1871 – 18 November 1922) was a French novelist, critic, and essayist best known for his monumental novel À la recherche du temps perdu (In Search of Lost Time; earlier rendered as Remembrance of Things Past), published in seven parts between 1913 and 1927: The Walk by Swann's Place, In the Shade of Blooming Young Girls, The Guermantes Walk, Sodom and Gomorrah, The Captive Girl, Vanished Albertine, Time Found Again.[2] He is considered by English critics and writers to be one of the most influential authors of the 20th century.[3][4]
