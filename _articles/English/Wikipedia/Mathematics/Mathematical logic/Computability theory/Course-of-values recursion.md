---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Course-of-values_recursion
offline_file: ""
offline_thumbnail: ""
uuid: 02e4b4b0-3dbd-48b5-bb37-a44cea8f5776
updated: 1484309485
title: Course-of-values recursion
tags:
    - Definition and examples
    - Equivalence to primitive recursion
    - Application to primitive recursive functions
    - Limitations
categories:
    - Computability theory
---
In computability theory, course-of-values recursion is a technique for defining number-theoretic functions by recursion. In a definition of a function f by course-of-values recursion, the value of f(n+1) is computed from the sequence 
  
    
      
        ⟨
        f
        (
        1
        )
        ,
        f
        (
        2
        )
        ,
        …
        ,
        f
        (
        n
        )
        ⟩
      
    
    {\displaystyle \langle f(1),f(2),\ldots ,f(n)\rangle }
  
.
