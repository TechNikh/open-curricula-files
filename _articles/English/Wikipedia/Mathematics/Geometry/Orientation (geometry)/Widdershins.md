---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Widdershins
offline_file: ""
offline_thumbnail: ""
uuid: c2c72eae-068c-41c0-b343-2632f43108bc
updated: 1484309300
title: Widdershins
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Counterclockwise_arrow.svg.png
tags:
    - Etymology
    - Superstition and religion
categories:
    - Orientation (geometry)
---
Widdershins (sometimes withershins, widershins or widderschynnes) is a term meaning to go counter-clockwise, to go anti-clockwise, or to go lefthandwise, or to walk around an object by always keeping it on the left. i.e. literally, it means to take a course opposite the apparent motion of the sun viewed from the Northern Hemisphere, (the centre of this imaginary clock is the ground the viewer stands upon).[1] The Oxford English Dictionary's entry cites the earliest uses of the word from 1513, where it was found in the phrase widdersyns start my hair, i.e. my hair stood on end.[citation needed]
