---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dense_subgraph
offline_file: ""
offline_thumbnail: ""
uuid: e763a4a4-6ce5-4dbf-aff7-61ba11889a4a
updated: 1484308632
title: Dense subgraph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dense_subgraph.png
tags:
    - Densest k subgraph
    - Densest at most k subgraph
    - Densest at least k subgraph
    - K-clique densest subgraph
    - Additional reading
categories:
    - Graph theory
---
In computer science the notion of highly connected subgraphs appears frequently. This notion can be formalized as follows. Let 
  
    
      
        G
        =
        (
        E
        ,
        V
        )
      
    
    {\displaystyle G=(E,V)}
  
 be an undirected graph and let 
  
    
      
        S
        =
        (
        
          E
          
            S
          
        
        ,
        
          V
          
            S
          
        
        )
      
    
    {\displaystyle S=(E_{S},V_{S})}
  
 be a subgraph of 
  
    
      
        G
      
    
    {\displaystyle G}
  
. Then the density of 
  
    
      
        S
      
    
    {\displaystyle ...
