---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/experimental
offline_file: ""
offline_thumbnail: ""
uuid: 6121ed64-34b0-48bb-926f-a9d8c108b3dd
updated: 1484310307
title: experimental
categories:
    - Dictionary
---
experimental
     adj 1: relating to or based on experiment; "experimental physics"
     2: relying on observation or experiment; "experimental results
        that supported the hypothesis" [syn: {data-based}, {observational}]
     3: of the nature of or undergoing an experiment; "an
        experimental drug"; "a pilot project"; "a test run"; "a
        trial separation"
