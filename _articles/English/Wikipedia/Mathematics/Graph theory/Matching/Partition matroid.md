---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partition_matroid
offline_file: ""
offline_thumbnail: ""
uuid: 5302394b-096b-420e-ae67-be3dd849bf04
updated: 1484309426
title: Partition matroid
tags:
    - Definition
    - Properties
    - Matching
    - Clique complexes
    - Enumeration
categories:
    - Matching
---
