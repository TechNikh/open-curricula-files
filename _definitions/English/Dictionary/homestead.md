---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/homestead
offline_file: ""
offline_thumbnail: ""
uuid: d3aea972-895c-4572-a884-114ad9809421
updated: 1484310486
title: homestead
categories:
    - Dictionary
---
homestead
     n 1: the home and adjacent grounds occupied by a family
     2: land acquired from the United States public lands by filing
        a record and living on and cultivating it under the
        homestead law
     3: dwelling that is usually a farmhouse and adjoining land
     v : settle land given by the government and occupy it as a
         homestead
