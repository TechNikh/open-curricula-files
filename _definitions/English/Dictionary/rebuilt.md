---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rebuilt
offline_file: ""
offline_thumbnail: ""
uuid: f1647f58-316a-48ba-801f-c3c7c827f001
updated: 1484310567
title: rebuilt
categories:
    - Dictionary
---
rebuild
     v : build again; "The house was rebuild after it was hit by a
         bomb" [syn: {reconstruct}]
     [also: {rebuilt}]
