---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vested
offline_file: ""
offline_thumbnail: ""
uuid: 2f5d0eb0-b9e7-4819-bf32-c51d39d811f2
updated: 1484310597
title: vested
categories:
    - Dictionary
---
vested
     adj : fixed and absolute and without contingency; "a vested right"
