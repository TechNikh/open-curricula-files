---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sympathetic_cooling
offline_file: ""
offline_thumbnail: ""
uuid: 3f282234-9cc8-459b-bc29-48c208368d50
updated: 1484308860
title: Sympathetic cooling
categories:
    - Thermodynamics
---
Typically, atomic ions that can be directly laser cooled are used to cool nearby ions or atoms, by way of their mutual Coulomb interaction. This technique allows cooling of ions and atoms that can't be cooled directly by laser cooling. This includes most molecular ion species, especially large organic molecules.[1] However, sympathetic cooling is most efficient when the mass/charge ratios of the sympathetic- and laser-cooled ions are similar.[2]
