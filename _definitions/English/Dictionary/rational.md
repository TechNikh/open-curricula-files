---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rational
offline_file: ""
offline_thumbnail: ""
uuid: 555b1e35-e8d9-49cf-a29d-6a4b4d49be64
updated: 1484310462
title: rational
categories:
    - Dictionary
---
rational
     adj 1: consistent with or based on or using reason; "rational
            behavior"; "a process of rational inference";
            "rational thought" [ant: {irrational}]
     2: of or associated with or requiring the use of the mind;
        "intellectual problems"; "the triumph of the rational over
        the animal side of man" [syn: {intellectual}, {noetic}]
     3: capable of being expressed as a quotient of integers;
        "rational numbers" [ant: {irrational}]
     4: having its source in or being guided by the intellect
        (distinguished from experience or emotion); "a rational
        analysis"
