---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frantically
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484608141
title: frantically
categories:
    - Dictionary
---
frantically
     adv : in an uncontrolled manner; "they searched frantically for
           their child"
