---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ergodic_sequence
offline_file: ""
offline_thumbnail: ""
uuid: 7a560163-5607-42ca-84c6-b3e83ebbfd30
updated: 1484308773
title: Ergodic sequence
categories:
    - Ergodic theory
---
Let 
  
    
      
        A
        =
        {
        
          a
          
            j
          
        
        }
      
    
    {\displaystyle A=\{a_{j}\}}
  
 be an infinite, strictly increasing sequence of positive integers. Then, given an integer q, this sequence is said to be ergodic mod q if, for all integers 
  
    
      
        1
        ≤
        k
        ≤
        q
      
    
    {\displaystyle 1\leq k\leq q}
  
, one has
