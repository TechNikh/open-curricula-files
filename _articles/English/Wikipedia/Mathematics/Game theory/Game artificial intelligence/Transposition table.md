---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transposition_table
offline_file: ""
offline_thumbnail: ""
uuid: bf228816-6556-4511-a090-5a007d3b1a77
updated: 1484309006
title: Transposition table
tags:
    - Functionality
    - Related techniques
    - Notes and references
categories:
    - Game artificial intelligence
---
In computer chess and other computer games, transposition tables are used to speed up the search of the game tree. Transposition tables are primarily useful in perfect information games, meaning the entire state of the game is known to all players at all times. The usage of transposition tables is essentially memoization applied to the tree search and is a form of dynamic programming.
