---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unsuccessful
offline_file: ""
offline_thumbnail: ""
uuid: d00c9d86-d82c-48f6-95d5-96ea1f311bf3
updated: 1484310589
title: unsuccessful
categories:
    - Dictionary
---
unsuccessful
     adj 1: not successful; having failed or having an unfavorable
            outcome [ant: {successful}]
     2: failing to accomplish an intended result; "an abortive
        revolt"; "a stillborn plot to assassinate the President"
        [syn: {abortive}, {stillborn}]
