---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deceitful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484461621
title: deceitful
categories:
    - Dictionary
---
deceitful
     adj 1: intended to deceive; "deceitful advertising"; "fallacious
            testimony"; "smooth, shining, and deceitful as thin
            ice" - S.T.Coleridge; "a fraudulent scheme to escape
            paying taxes" [syn: {fallacious}, {fraudulent}]
     2: marked by deliberate deceptiveness especially by pretending
        one set of feelings and acting under the influence of
        another; "she was a deceitful scheming little thing"-
        Israel Zangwill; "a double-dealing double agent"; "a
        double-faced infernal traitor and schemer"- W.M.Thackeray
        [syn: {ambidextrous}, {double-dealing}, {duplicitous}, {Janus-faced},
         {two-faced}, ...
