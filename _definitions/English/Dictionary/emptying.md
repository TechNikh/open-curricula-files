---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emptying
offline_file: ""
offline_thumbnail: ""
uuid: dc27b40d-cfab-453e-a31e-6b846bb830b8
updated: 1484310330
title: emptying
categories:
    - Dictionary
---
emptying
     n : the act of removing the contents of something [syn: {voidance},
          {evacuation}]
