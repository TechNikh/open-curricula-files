---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monsoon
offline_file: ""
offline_thumbnail: ""
uuid: f4e8f479-ab7c-459f-bb15-b5f4a0fea338
updated: 1484310258
title: monsoon
categories:
    - Dictionary
---
monsoon
     n 1: a seasonal wind in southern Asia; blows from the southwest
          (bringing rain) in summer and from the northeast in
          winter
     2: rainy season in southern Asia when the southwestern monsoon
        blows, bringing heavy rains
     3: any wind that changes direction with the seasons
