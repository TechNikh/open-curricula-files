---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/novel
offline_file: ""
offline_thumbnail: ""
uuid: 307f8e99-1b9f-476b-9bc5-5a333aa19b70
updated: 1484310431
title: novel
categories:
    - Dictionary
---
novel
     adj 1: of a kind not seen before; "the computer produced a
            completely novel proof of a well-known theorem" [syn:
            {fresh}, {new}]
     2: pleasantly novel or different; "common sense of a most
        refreshing sort" [syn: {refreshing}]
     n 1: a extended fictional work in prose; usually in the form of a
          story
     2: a printed and bound book that is an extended work of
        fiction; "his bookcases were filled with nothing but
        novels"; "he burned all the novels"
