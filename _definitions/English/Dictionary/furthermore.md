---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/furthermore
offline_file: ""
offline_thumbnail: ""
uuid: e5a15223-71d4-4f05-821b-79856d3a02e9
updated: 1484310545
title: furthermore
categories:
    - Dictionary
---
furthermore
     adv : in addition; "computer chess games are getting cheaper all
           the time; furthermore, their quality is improving";
           "the cellar was dark; moreover, mice nested there";
           "what is more, there's no sign of a change" [syn: {moreover},
            {what is more}]
