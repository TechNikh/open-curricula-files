---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loose
offline_file: ""
offline_thumbnail: ""
uuid: b2206f5c-76fb-49fc-98ec-88c3f5a9d202
updated: 1484310323
title: loose
categories:
    - Dictionary
---
loose
     adj 1: not restrained or confined or attached; "a pocket full of
            loose bills"; "knocked the ball loose"; "got loose
            from his attacker"
     2: not compact or dense in structure or arrangement; "loose
        gravel" [ant: {compact}]
     3: (of a ball in sport) not in the possession or control of any
        player; "a loose ball"
     4: not tight; not closely constrained or constricted or
        constricting; "loose clothing"; "the large shoes were very
        loose" [ant: {tight}]
     5: not officially recognized or controlled; "an informal
        agreement"; "a loose organization of the local farmers"
        [syn: {informal}]
     6: not literal; ...
