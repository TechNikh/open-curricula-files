---
version: 1
type: video
provider: YouTube
id: GmHjckFMq9Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a5232f22-1dd7-4d7d-a134-00163574c646
updated: 1486069635
title: 'High-pressure mineralogy- theory and experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/GmHjckFMq9Y/default.jpg
    - https://i3.ytimg.com/vi/GmHjckFMq9Y/1.jpg
    - https://i3.ytimg.com/vi/GmHjckFMq9Y/2.jpg
    - https://i3.ytimg.com/vi/GmHjckFMq9Y/3.jpg
---
