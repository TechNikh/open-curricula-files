---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rounded
offline_file: ""
offline_thumbnail: ""
uuid: 756c99b6-3baa-47ae-bc1b-12943972614a
updated: 1484310437
title: rounded
categories:
    - Dictionary
---
rounded
     adj 1: curving and somewhat round in shape rather than jagged; "low
            rounded hills"; "rounded shoulders" [ant: {angular}]
     2: a chubby body; "the boy had a rounded face and fat cheeks"
        [syn: {fat}]
