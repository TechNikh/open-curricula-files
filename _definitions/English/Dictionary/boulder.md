---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boulder
offline_file: ""
offline_thumbnail: ""
uuid: 3355f13e-addb-4f86-b4be-d50478f4c4f9
updated: 1484310462
title: boulder
categories:
    - Dictionary
---
boulder
     n 1: a large smooth mass of rock detached from its place of
          origin [syn: {bowlder}]
     2: a town in north central Colorado; Rocky Mountains resort
        center and university town
