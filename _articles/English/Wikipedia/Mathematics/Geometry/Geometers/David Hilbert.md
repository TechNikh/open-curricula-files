---
version: 1
type: article
id: https://en.wikipedia.org/wiki/David_Hilbert
offline_file: ""
offline_thumbnail: ""
uuid: 64144f6c-4e42-4ace-b013-3182b67120c7
updated: 1484309088
title: David Hilbert
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mathematik_G%25C3%25B6ttingen.jpg'
tags:
    - life
    - Early life and education
    - Career
    - Göttingen school
    - Later years
    - Personal life
    - "Hilbert solves Gordan's Problem"
    - Axiomatization of geometry
    - The 23 problems
    - Formalism
    - "Hilbert's program"
    - "Gödel's work"
    - Functional analysis
    - Physics
    - Number theory
    - Works
    - Notes
    - Primary literature in English translation
    - Secondary literature
categories:
    - Geometers
---
David Hilbert (German: [ˈdaːvɪt ˈhɪlbɐt]; 23 January 1862 – 14 February 1943) was a German mathematician. He is recognized as one of the most influential and universal mathematicians of the 19th and early 20th centuries. Hilbert discovered and developed a broad range of fundamental ideas in many areas, including invariant theory and the axiomatization of geometry. He also formulated the theory of Hilbert spaces,[3] one of the foundations of functional analysis.
