---
version: 1
type: video
provider: YouTube
id: hl58vTCqVIY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Conic%20sections/Conic%20sections-%20Hyperbolas%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: edee26ef-5917-4b25-af79-c592ea6f862c
updated: 1486069571
title: 'Conic sections- Hyperbolas 2'
categories:
    - Conic sections
thumbnail_urls:
    - https://i3.ytimg.com/vi/hl58vTCqVIY/default.jpg
    - https://i3.ytimg.com/vi/hl58vTCqVIY/1.jpg
    - https://i3.ytimg.com/vi/hl58vTCqVIY/2.jpg
    - https://i3.ytimg.com/vi/hl58vTCqVIY/3.jpg
---
Continuation of the intro to hyperbolas

Watch the next lesson: https://www.khanacademy.org/math/algebra2/conics_precalc/hyperbolas-precalc/v/conic-sections-hyperbolas-3?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Missed the previous lesson? 
https://www.khanacademy.org/math/algebra2/conics_precalc/hyperbolas-precalc/v/conic-sections-intro-to-hyperbolas?utm_source=YT&utm_medium=Desc&utm_campaign=AlgebraII

Algebra II on Khan Academy: Your studies in algebra 1 have built a solid foundation from which you can explore linear equations, inequalities, and functions. In algebra 2 we build upon that foundation and not only extend our knowledge of algebra 1, but slowly become capable of tackling the BIG questions of the universe. We'll again touch on systems of equations, inequalities, and functions...but we'll also address exponential and logarithmic functions, logarithms, imaginary and complex numbers, conic sections, and matrices. Don't let these big words intimidate you. We're on this journey with you!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Algebra II channel:
https://www.youtube.com/channel/UCsCA3_VozRtgUT7wWC1uZDg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
