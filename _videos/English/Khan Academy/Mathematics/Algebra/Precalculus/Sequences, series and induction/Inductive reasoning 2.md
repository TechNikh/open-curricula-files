---
version: 1
type: video
provider: YouTube
id: ZSsBh4Ybbj8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Sequences%2C%20series%20and%20induction/Inductive%20reasoning%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e6d44508-3b5c-4cba-a3dd-b0af3b212dc2
updated: 1486069559
title: Inductive reasoning 2
categories:
    - Sequences, series and induction
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZSsBh4Ybbj8/default.jpg
    - https://i3.ytimg.com/vi/ZSsBh4Ybbj8/1.jpg
    - https://i3.ytimg.com/vi/ZSsBh4Ybbj8/2.jpg
    - https://i3.ytimg.com/vi/ZSsBh4Ybbj8/3.jpg
---
