---
version: 1
type: video
provider: YouTube
id: zfleOQocdPc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/7.6%20tRNA.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6d699f1e-8a90-4569-930b-1506cb3f33b9
updated: 1486069677
title: 7.6 tRNA
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/zfleOQocdPc/default.jpg
    - https://i3.ytimg.com/vi/zfleOQocdPc/1.jpg
    - https://i3.ytimg.com/vi/zfleOQocdPc/2.jpg
    - https://i3.ytimg.com/vi/zfleOQocdPc/3.jpg
---
