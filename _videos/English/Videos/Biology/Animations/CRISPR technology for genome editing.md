---
version: 1
type: video
provider: YouTube
id: LrtrM_CPtQQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/CRISPR%20technology%20for%20genome%20editing.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2c8558ce-761d-40c0-94fd-9bc76b1abd70
updated: 1486069670
title: CRISPR technology for genome editing
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/LrtrM_CPtQQ/default.jpg
    - https://i3.ytimg.com/vi/LrtrM_CPtQQ/1.jpg
    - https://i3.ytimg.com/vi/LrtrM_CPtQQ/2.jpg
    - https://i3.ytimg.com/vi/LrtrM_CPtQQ/3.jpg
---
