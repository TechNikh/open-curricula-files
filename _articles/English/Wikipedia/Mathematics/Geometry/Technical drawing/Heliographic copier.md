---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heliographic_copier
offline_file: ""
offline_thumbnail: ""
uuid: e24fd025-683e-41a3-9d13-a34fdea332e3
updated: 1484309327
title: Heliographic copier
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Joy_Oil_gas_station_blueprints.jpg
tags:
    - History
    - Cyano copier
    - features
    - Diazo copier
    - features
    - Operation
    - Bibliography
categories:
    - Technical drawing
---
A heliographic copier or heliographic duplicator[1] is an apparatus used in the world of reprography for making contact prints on paper from original drawings made with that purpose on tracing paper, parchment paper or any other transparent or translucent material using different procedures. In general terms some type of heliographic copier is used for making: Hectographic prints, Ferrogallic prints, Gel-lithographs or Silver halide prints. All of them, until a certain size, can be achieved using a contact printer with an appropriate lamp (ultraviolet, etc..) but for big engineering and architectural plans, the heliographic copiers used with the cyanotype and the diazotype technologies, are ...
