---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wheelchair
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484409602
title: wheelchair
categories:
    - Dictionary
---
wheelchair
     n : a movable chair mounted on large wheels; for invalids or
         those who cannot walk; frequently propelled by the
         occupant
