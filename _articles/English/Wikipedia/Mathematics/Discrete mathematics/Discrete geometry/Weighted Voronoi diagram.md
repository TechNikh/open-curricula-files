---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weighted_Voronoi_diagram
offline_file: ""
offline_thumbnail: ""
uuid: 400e7072-fc10-4698-8e4f-2c16a3c8e5f7
updated: 1484308502
title: Weighted Voronoi diagram
categories:
    - Discrete geometry
---
In mathematics, a weighted Voronoi diagram in n dimensions is a special case of a Voronoi diagram. The Voronoi cells in a weighted Voronoi diagram are defined in terms of a distance function. The distance function may specify the usual Euclidean distance, or may be some other, special distance function. Usually, the distance function is a function of the generator points' weights.
