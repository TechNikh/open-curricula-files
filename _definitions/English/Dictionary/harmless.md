---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harmless
offline_file: ""
offline_thumbnail: ""
uuid: 4ce57303-5d0b-432e-8c10-504cfb0dabb1
updated: 1484310537
title: harmless
categories:
    - Dictionary
---
harmless
     adj 1: not causing or capable of causing harm; "harmless bacteria";
            "rendered the bomb harmless" [ant: {harmful}]
     2: not injurious to physical or mental health [syn: {innocuous}]
        [ant: {noxious}]
     3: not producing any toxic effects
     4: unlikely to harm or disturb anyone; "harmless old man" [syn:
         {innocuous}]
     5: not threatening to life or health; not malignant; "a benign
        tumor is usually harmless" [syn: {nonmalignant}]
