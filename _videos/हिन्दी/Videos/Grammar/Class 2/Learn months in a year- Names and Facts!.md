---
version: 1
type: video
provider: YouTube
id: oJwGY7Gevjo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 31f26410-f9fd-4157-ab8d-15e1b9ccf061
updated: 1486069630
title: 'Learn months in a year- Names and Facts!'
categories:
    - Class 2
thumbnail_urls:
    - https://i3.ytimg.com/vi/oJwGY7Gevjo/default.jpg
    - https://i3.ytimg.com/vi/oJwGY7Gevjo/1.jpg
    - https://i3.ytimg.com/vi/oJwGY7Gevjo/2.jpg
    - https://i3.ytimg.com/vi/oJwGY7Gevjo/3.jpg
---
