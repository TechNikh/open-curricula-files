---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Patrice_Ossona_de_Mendez
offline_file: ""
offline_thumbnail: ""
uuid: cd180f1c-c9c9-4eeb-b86c-acf8b68b6d3f
updated: 1484309472
title: Patrice Ossona de Mendez
categories:
    - Graph theorists
---
Patrice Ossona de Mendez is a French mathematician specializing in topological graph theory who works as a researcher at the Centre national de la recherche scientifique in Paris.[1] With Pierre Rosenstiehl, he is editor-in-chief of the European Journal of Combinatorics, a position he has held since 2009.[1][2]
