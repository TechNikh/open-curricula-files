---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/congo
offline_file: ""
offline_thumbnail: ""
uuid: 151e4c6b-2541-4228-b7a4-0b9e53a6c811
updated: 1484310180
title: congo
categories:
    - Dictionary
---
Congo
     n 1: a republic in central Africa; achieved independence from
          Belgium in 1960 [syn: {Democratic Republic of the Congo},
           {Zaire}, {Belgian Congo}]
     2: a major African river (one of the world's longest); flows
        through Congo into the South Atlantic [syn: {Congo River}]
     3: a republic in west-central Africa; achieved independence
        from France in 1960 [syn: {Republic of the Congo}, {French
        Congo}]
     4: black tea grown in China [syn: {congou}, {congou tea}, {English
        breakfast tea}]
