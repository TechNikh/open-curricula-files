---
version: 1
type: video
provider: YouTube
id: VUk8HNut0WA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/2nd%20conditional%20English%20Grammar.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6fc74189-1808-48cf-b7fb-3ac4491aba6c
updated: 1486069632
title: 2nd conditional English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/VUk8HNut0WA/default.jpg
    - https://i3.ytimg.com/vi/VUk8HNut0WA/1.jpg
    - https://i3.ytimg.com/vi/VUk8HNut0WA/2.jpg
    - https://i3.ytimg.com/vi/VUk8HNut0WA/3.jpg
---
