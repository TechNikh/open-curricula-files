---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Slimmy
offline_file: ""
offline_thumbnail: ""
uuid: 771a4878-2b3a-449c-ae03-7c05d3942e95
updated: 1484308356
title: Slimmy
tags:
    - Life and career
    - '1999–2005: early life and career beginnings'
    - '2006–present: career breakthrough, Beatsound Loverboy, Be Someone Else and Freestyle Heart'
    - Image
    - Awards
    - MTV Europe Music Awards
    - Golden Globes
    - Discography
    - Videoclips
    - Singles
categories:
    - Uncategorized
---
Slimmy, born Paulo Fernandes, is a Portuguese singer/songwriter who combines various genres of rock, especially electro rock. He has an irreverent visual look and an addictive musical style and has achieved mainstream success after an appearance on the Portuguese soap opera Morangos com Açúcar. His music has also appeared in one episode of CSI. His song, Self Control, has been featured on Sky Sports.
