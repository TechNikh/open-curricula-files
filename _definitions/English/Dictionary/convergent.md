---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convergent
offline_file: ""
offline_thumbnail: ""
uuid: 284c7c8e-d769-4229-9972-77b98144f26f
updated: 1484310286
title: convergent
categories:
    - Dictionary
---
convergent
     adj : tending to come together from different directions [ant: {divergent}]
