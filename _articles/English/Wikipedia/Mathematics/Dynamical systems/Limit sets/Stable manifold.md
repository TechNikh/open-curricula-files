---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stable_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 2805344b-195e-4999-9543-d5411ab17046
updated: 1484308795
title: Stable manifold
tags:
    - Definition
    - Remark
categories:
    - Limit sets
---
In mathematics, and in particular the study of dynamical systems, the idea of stable and unstable sets or stable and unstable manifolds give a formal mathematical definition to the general notions embodied in the idea of an attractor or repellor. In the case of hyperbolic dynamics, the corresponding notion is that of the hyperbolic set.
