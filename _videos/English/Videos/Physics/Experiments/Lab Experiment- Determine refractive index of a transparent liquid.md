---
version: 1
type: video
provider: YouTube
id: 4i9_91704yc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 18eab452-922c-4e33-b632-50beb20fdf6b
updated: 1486069637
title: 'Lab Experiment- Determine refractive index of a transparent liquid'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/4i9_91704yc/default.jpg
    - https://i3.ytimg.com/vi/4i9_91704yc/1.jpg
    - https://i3.ytimg.com/vi/4i9_91704yc/2.jpg
    - https://i3.ytimg.com/vi/4i9_91704yc/3.jpg
---
