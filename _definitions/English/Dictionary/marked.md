---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marked
offline_file: ""
offline_thumbnail: ""
uuid: ed5697b1-0366-4b9f-b206-5da6190b63d5
updated: 1484310275
title: marked
categories:
    - Dictionary
---
marked
     adj 1: strongly marked; easily noticeable; "walked with a marked
            limp"; "a pronounced flavor of cinnamon" [syn: {pronounced}]
     2: singled out for notice or especially for a dire fate; "a
        marked man"
     3: having or as if having an identifying mark or a mark as
        specified; often used in combination; "played with marked
        cards"; "a scar-marked face"; "well-marked roads" [ant: {unmarked}]
