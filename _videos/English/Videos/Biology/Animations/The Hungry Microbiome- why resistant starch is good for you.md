---
version: 1
type: video
provider: YouTube
id: NI3KtR3LoqM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/The%20Hungry%20Microbiome-%20why%20resistant%20starch%20is%20good%20for%20you.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 396bac82-083c-4866-b098-ad66d7a088f8
updated: 1486069669
title: 'The Hungry Microbiome- why resistant starch is good for you'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/NI3KtR3LoqM/default.jpg
    - https://i3.ytimg.com/vi/NI3KtR3LoqM/1.jpg
    - https://i3.ytimg.com/vi/NI3KtR3LoqM/2.jpg
    - https://i3.ytimg.com/vi/NI3KtR3LoqM/3.jpg
---
