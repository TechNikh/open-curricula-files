---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/working
offline_file: ""
offline_thumbnail: ""
uuid: b3e89526-4714-4582-8e58-a5b4a1e50f02
updated: 1484310327
title: working
categories:
    - Dictionary
---
working
     adj 1: actively engaged in paid work; "the working population";
            "the ratio of working men to unemployed"; "a working
            mother"; "robots can be on the job day and night"
            [syn: {working(a)}, {on the job(p)}]
     2: adequate for practical use; especially sufficient in
        strength or numbers to accomplish something; "the party
        has a working majority in the House"; "a working knowledge
        of Spanish"
     3: adopted as a temporary basis for further work; "a working
        draft"; "a working hypothesis" [syn: {working(a)}]
     4: (of e.g. a machine) performing or capable of performing; "in
        running (or working) order"; "a ...
