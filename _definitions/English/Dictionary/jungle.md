---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jungle
offline_file: ""
offline_thumbnail: ""
uuid: 754b5122-be1e-4348-be8d-1890fc79c99f
updated: 1484310579
title: jungle
categories:
    - Dictionary
---
jungle
     n 1: an impenetrable equatorial forest
     2: a location marked by an intense competition and struggle for
        survival
     3: a place where hoboes camp [syn: {hobo camp}]
