---
version: 1
type: video
provider: YouTube
id: P56TrUDdUr4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f42c82a6-4757-4710-952a-cc9601519ef4
updated: 1486069647
title: 'The Infinite Rainbow - Mandelbrot Zoom 2.463x10^114'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/P56TrUDdUr4/default.jpg
    - https://i3.ytimg.com/vi/P56TrUDdUr4/1.jpg
    - https://i3.ytimg.com/vi/P56TrUDdUr4/2.jpg
    - https://i3.ytimg.com/vi/P56TrUDdUr4/3.jpg
---
