---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ranking
offline_file: ""
offline_thumbnail: ""
uuid: c94b6ceb-ddf8-4a62-9508-c807e81f5a83
updated: 1484310448
title: ranking
categories:
    - Dictionary
---
ranking
     adj 1: of the highest rank; used of persons; "the commanding
            officer" [syn: {commanding}, {top-level}, {top-ranking}]
     2: having a higher rank; "superior officer" [syn: {ranking(a)},
         {superior}, {higher-ranking}]
     n : position on a scale in relation to others in a sport
