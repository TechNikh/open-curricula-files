---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worker
offline_file: ""
offline_thumbnail: ""
uuid: 670a6f7d-68d6-4a6e-8eb7-e5b787adcbbf
updated: 1484310457
title: worker
categories:
    - Dictionary
---
worker
     n 1: a person who works at a specific occupation; "he is a good
          worker" [ant: {nonworker}]
     2: a member of the working class (not necessarily employed);
        "workers of the world--unite!" [syn: {proletarian}, {prole}]
     3: sterile member of a colony of social insects that forages
        for food and cares for the larvae
     4: a person who acts and gets things done; "he's a principal
        actor in this affair"; "when you want something done get a
        doer"; "he's a miracle worker" [syn: {actor}, {doer}]
