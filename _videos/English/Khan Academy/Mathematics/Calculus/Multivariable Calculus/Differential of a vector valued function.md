---
version: 1
type: video
provider: YouTube
id: FYMn61HLw1k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Differential%20of%20a%20vector%20valued%20function.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 00aa02af-a567-4b1b-aa18-e02f462f0089
updated: 1486069696
title: Differential of a vector valued function
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/FYMn61HLw1k/default.jpg
    - https://i3.ytimg.com/vi/FYMn61HLw1k/1.jpg
    - https://i3.ytimg.com/vi/FYMn61HLw1k/2.jpg
    - https://i3.ytimg.com/vi/FYMn61HLw1k/3.jpg
---
