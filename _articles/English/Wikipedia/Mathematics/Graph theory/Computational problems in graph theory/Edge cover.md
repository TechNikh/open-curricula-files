---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edge_cover
offline_file: ""
offline_thumbnail: ""
uuid: 3ebc9635-e424-4a0b-a1cf-7c3f7d9ec064
updated: 1484309389
title: Edge cover
tags:
    - Definition
    - Examples
    - Algorithms
    - Notes
categories:
    - Computational problems in graph theory
---
In graph theory, an edge cover of a graph is a set of edges such that every vertex of the graph is incident to at least one edge of the set. In computer science, the minimum edge cover problem is the problem of finding an edge cover of minimum size. It is an optimization problem that belongs to the class of covering problems and can be solved in polynomial time.
