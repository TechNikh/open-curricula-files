---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Extended_order
offline_file: ""
offline_thumbnail: ""
uuid: 5cb84503-b2de-4d5a-a289-c0fd82474f46
updated: 1484308830
title: Extended order
tags:
    - Development of the extended order in society
categories:
    - Self-organization
---
Extended order is an economics and sociology concept introduced by Friedrich Hayek in his book The Fatal Conceit. It is a description of what happens when a system embraces specialization and trade and "constitutes an information gathering process, able to call up, and put to use, widely dispersed information that no central planning agency, let alone any individual, could know as a whole, possess or control.”[1]:14 The result is an interconnected web where people can benefit from the actions and knowledge of those they don't know. This is possible and efficient because a proper legal framework replaces trust, which is only practical in small circles of people who know each other ...
