---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Synchronization_of_chaos
offline_file: ""
offline_thumbnail: ""
uuid: 6d1b929f-d4de-438c-8b76-54547e0aa861
updated: 1484308712
title: Synchronization of chaos
tags:
    - Identical synchronization
    - Generalized synchronization
    - Phase synchronization
    - Anticipated and lag synchronization
    - Amplitude envelope synchronization
    - Notes
categories:
    - Chaos theory
---
Synchronization of chaos is a phenomenon that may occur when two, or more, dissipative chaotic systems are coupled. Because of the exponential divergence of the nearby trajectories of chaotic system, having two chaotic systems evolving in synchrony might appear surprising. However, synchronization of coupled or driven chaotic oscillators is a phenomenon well established experimentally and reasonably well understood theoretically. The stability of synchronization for coupled systems can be analyzed using Master Stability. Synchronization of chaos is a rich phenomenon and a multi-disciplinary subject with a broad range of applications.[1][2]
