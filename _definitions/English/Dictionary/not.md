---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/not
offline_file: ""
offline_thumbnail: ""
uuid: 46ca1285-d740-4220-8eb7-b4275b38a542
updated: 1484310353
title: not
categories:
    - Dictionary
---
not
     adv : negation of a word or group of words; "he does not speak
           French"; "she is not going"; "they are not friends";
           "not many"; "not much"; "not at all"
