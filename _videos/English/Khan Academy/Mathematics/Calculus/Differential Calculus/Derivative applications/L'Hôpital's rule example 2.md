---
version: 1
type: video
provider: YouTube
id: FJo18AwLfuI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Derivative%20applications/L%27Ho%CC%82pital%27s%20rule%20example%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2bb80641-fb5d-4255-a5dc-efa12d234d6e
updated: 1486069693
title: "L'Hôpital's rule example 2"
categories:
    - Derivative applications
thumbnail_urls:
    - https://i3.ytimg.com/vi/FJo18AwLfuI/default.jpg
    - https://i3.ytimg.com/vi/FJo18AwLfuI/1.jpg
    - https://i3.ytimg.com/vi/FJo18AwLfuI/2.jpg
    - https://i3.ytimg.com/vi/FJo18AwLfuI/3.jpg
---
