---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electromagnets
offline_file: ""
offline_thumbnail: ""
uuid: 0abd76c6-4959-4407-80ca-f28e1c46e884
updated: 1484308319
title: Electromagnet
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Simple_electromagnet2.gif
tags:
    - History
    - Uses of electromagnets
    - Simple solenoid
    - Physics
    - "Ampere's law"
    - Magnetic core
    - Magnetic circuit – the constant B field approximation
    - Magnetic field created by a current
    - Force exerted by magnetic field
    - Closed magnetic circuit
    - Force between electromagnets
    - Side effects
    - Ohmic heating
    - Inductive voltage spikes
    - Lorentz forces
    - Core losses
    - High field electromagnets
    - Superconducting electromagnets
    - Bitter electromagnets
    - Exploding electromagnets
    - Definition of terms
categories:
    - Uncategorized
---
An electromagnet is a type of magnet in which the magnetic field is produced by an electric current. The magnetic field disappears when the current is turned off. Electromagnets usually consist of a large number of closely spaced turns of wire that create the magnetic field. The wire turns are often wound around a magnetic core made from a ferromagnetic or ferrimagnetic material such as iron; the magnetic core concentrates the magnetic flux and makes a more powerful magnet.
