---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Imre_B%C3%A1r%C3%A1ny'
offline_file: ""
offline_thumbnail: ""
uuid: 0c297fd6-1033-47f8-9487-ce9438783682
updated: 1484309069
title: Imre Bárány
tags:
    - Notable results
    - Career
categories:
    - Geometers
---
Imre Bárány (Mátyásföld, 7 December 1947) is a Hungarian mathematician, working in combinatorics and discrete geometry. He works at the Rényi Mathematical Institute of the Hungarian Academy of Sciences, and has a part-time job at University College London.
