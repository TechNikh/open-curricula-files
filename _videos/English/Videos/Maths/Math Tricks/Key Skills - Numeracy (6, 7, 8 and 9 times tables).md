---
version: 1
type: video
provider: YouTube
id: YL3PtlxYkZ4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 79eceb39-a07d-4781-83b2-d668eed9406e
updated: 1486069625
title: 'Key Skills - Numeracy (6, 7, 8 and 9 times tables)'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/YL3PtlxYkZ4/default.jpg
    - https://i3.ytimg.com/vi/YL3PtlxYkZ4/1.jpg
    - https://i3.ytimg.com/vi/YL3PtlxYkZ4/2.jpg
    - https://i3.ytimg.com/vi/YL3PtlxYkZ4/3.jpg
---
