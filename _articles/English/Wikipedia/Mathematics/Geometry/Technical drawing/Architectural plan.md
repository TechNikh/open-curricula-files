---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Architectural_plan
offline_file: ""
offline_thumbnail: ""
uuid: 457c6be9-fd24-4364-9cbd-587e9148f7bc
updated: 1484309321
title: Architectural plan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Abbot_Academy_proposed_sketch_1829_buildings_and_grounds_Andover_Massachusetts.jpg
tags:
    - The term "Architectural plan"
    - Architectural plan aspects
    - Buildings
    - Design process
    - Architectural drawing
    - Architectural design values
    - Floor plan
    - Planning
    - Building construction
    - Related types of design
    - Garden design
    - Landscape design
    - Site planning
    - Urban planning
categories:
    - Technical drawing
---
In the field of architecture an architectural plan is a design and planning for a building, and can contain architectural drawings, specifications of the design, calculations, time planning of the building process, and other documentation.
