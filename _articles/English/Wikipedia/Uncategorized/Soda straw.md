---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Soda_straws
offline_file: ""
offline_thumbnail: ""
uuid: 93959b4d-b88c-4add-aa29-76ffd42e03f7
updated: 1484308372
title: Soda straw
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Rats-Nest-straw.jpg
categories:
    - Uncategorized
---
A soda straw (or simply straw) is a speleothem in the form of a hollow mineral cylindrical tube. They are also known as tubular stalactites. Soda straws grow in places where water leaches slowly through cracks in rock, such as on the roofs of caves. A soda straw can turn into a stalactite if the hole at the bottom is blocked, or if the water begins flowing on the outside surface of the hollow tube.
