---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Most_expensive_cars_sold_at_auction
offline_file: ""
offline_thumbnail: ""
uuid: ced0aa09-7bd9-41e0-a6d1-c157df72a168
updated: 1484309017
title: Most expensive cars sold at auction
tags:
    - Common contributing factors
    - World economy affecting car values
    - Interactive graph
    - Absolute record
    - Notes
categories:
    - Auctions
---
This is a list of the most expensive cars sold in auto auctions' through the traditional bidding process, consisting of those that attracted headline grabbing publicity, mainly for the high price their new owners have paid. A 1962 Ferrari 250 GTO serial number 3851GT sold at Bonham's Quail Auction on August 14, 2014 for US $34,650,000.00 ($38,115,000.00 including buyers premium), breaking the record previously held by a 1954 Mercedes-Benz W196R race car, sold for a record $31 million at an auction in England on July 12, 2013. While collectible cars have been sold privately for more, this is the highest price ever paid for a car at a public auction.
