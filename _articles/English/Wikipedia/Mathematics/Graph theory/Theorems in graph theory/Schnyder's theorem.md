---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Schnyder%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: f1ad2e8c-fa44-4551-94fa-9d9fecc562e1
updated: 1484309456
title: "Schnyder's theorem"
categories:
    - Theorems in graph theory
---
In graph theory, Schnyder's theorem is a characterization of planar graphs in terms of the order dimension of their incidence posets. It is named after Walter Schnyder, who published its proof in 1989.
