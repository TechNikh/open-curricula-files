---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rainfall
offline_file: ""
offline_thumbnail: ""
uuid: 67b3946f-af76-4097-855d-c1d23d8e9b24
updated: 1484310271
title: rainfall
categories:
    - Dictionary
---
rainfall
     n : water falling in drops from vapor condensed in the
         atmosphere [syn: {rain}]
