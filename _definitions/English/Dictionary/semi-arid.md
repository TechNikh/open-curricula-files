---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/semi-arid
offline_file: ""
offline_thumbnail: ""
uuid: 607e1e4f-517e-41f5-9dbe-b1b5a6b6290a
updated: 1484310256
title: semi-arid
categories:
    - Dictionary
---
semiarid
     adj : somewhat arid; "a semiarid region with with little annual
           rainfall"
