---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shunned
offline_file: ""
offline_thumbnail: ""
uuid: 991f4f51-7f7a-4f5c-b015-a2f8262b71ec
updated: 1484310583
title: shunned
categories:
    - Dictionary
---
shun
     v 1: avoid and stay away from deliberately; stay clear of [syn: {eschew}]
     2: expel from a community or group [syn: {banish}, {ban}, {ostracize},
         {ostracise}, {cast out}, {blackball}]
     [also: {shunning}, {shunned}]
