---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cream
offline_file: ""
offline_thumbnail: ""
uuid: 93025d37-497d-4e30-b3e6-6b13759e6a43
updated: 1484310156
title: cream
categories:
    - Dictionary
---
cream
     n 1: the best people or things in a group; "the cream of
          England's young men were killed in the Great War" [syn:
          {pick}]
     2: the part of milk containing the butterfat
     3: toiletry consisting of any of various substances resembling
        cream that have a soothing and moisturizing effect when
        applied to the skin [syn: {ointment}, {emollient}]
     v 1: make creamy by beating; "Cream the butter"
     2: put on cream, as on one's face or body; "She creams her face
        every night"
     3: remove from the surface; "skim cream from the surface of
        milk" [syn: {skim}, {skim off}, {cream off}]
     4: add cream to one's coffee, for ...
