---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weakness
offline_file: ""
offline_thumbnail: ""
uuid: 3a501b28-576e-48d0-92ba-f6f6b2815972
updated: 1484310178
title: weakness
categories:
    - Dictionary
---
weakness
     n 1: a flaw or weak point; "he was quick to point out his wife's
          failings" [syn: {failing}]
     2: powerlessness revealed by an inability to act; "in spite of
        their weakness the group remains highly active" [syn: {helplessness},
         {impuissance}]
     3: the property of lacking physical or mental strength;
        liability to failure under pressure or stress or strain;
        "his weakness increased as he became older"; "the weakness
        of the span was overlooked until it collapsed" [ant: {strength}]
     4: the condition of being financially weak; "the weakness of
        the dollar against the yen" [ant: {strength}]
     5: a penchant for ...
