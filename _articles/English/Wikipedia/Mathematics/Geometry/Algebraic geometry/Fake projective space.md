---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fake_projective_space
offline_file: ""
offline_thumbnail: ""
uuid: 7277b772-b51b-401e-93b7-df70676f5156
updated: 1484309145
title: Fake projective space
categories:
    - Algebraic geometry
---
In mathematics, a fake projective space is a complex algebraic variety that has the same Betti numbers as some projective space, but is not isomorphic to it.
