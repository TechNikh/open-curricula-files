---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mole
offline_file: ""
offline_thumbnail: ""
uuid: c8992c32-4cb1-41f9-aca9-c5ae814bc08f
updated: 1484310286
title: mole
categories:
    - Dictionary
---
mole
     n 1: the molecular weight of a substance expressed in grams; the
          basic unit of amount of substance adopted under the
          Systeme International d'Unites [syn: {gram molecule}, {mol}]
     2: a spy who works against enemy espionage [syn: {counterspy}]
     3: spicy sauce often containing chocolate
     4: a small congenital pigmented spot on the skin
     5: a protective structure of stone or concrete; extends from
        shore into the water to prevent a beach from washing away
        [syn: {breakwater}, {groin}, {groyne}, {bulwark}, {seawall},
         {jetty}]
     6: small velvety-furred burrowing mammal having small eyes and
        fossorial forefeet
