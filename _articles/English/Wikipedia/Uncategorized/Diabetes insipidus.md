---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diabetes_insipidus
offline_file: ""
offline_thumbnail: ""
uuid: bf449f76-82a0-4293-9b25-1362f38f967f
updated: 1484308371
title: Diabetes insipidus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Arginine_vasopressin3d.png
tags:
    - Signs and symptoms
    - Diagnosis
    - Pathophysiology
    - Classification
    - Neurogenic
    - Nephrogenic
    - Dipsogenic
    - Gestational
    - Treatment
    - Central DI
    - Nephrogenic DI
    - Etymology
categories:
    - Uncategorized
---
Diabetes insipidus (DI) is a condition characterized by excessive thirst and excretion of large amounts of severely dilute urine, with reduction of fluid intake having no effect on the concentration of the urine.[1]
