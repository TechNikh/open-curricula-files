---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ur
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1483929361
title: Ur
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Map_of_Ur_III.svg_752.png
tags:
    - History
    - Prehistory
    - Third millennium BC (Early Bronze Age)
    - Later Bronze Age
    - Iron Age
    - Identification with Biblical Ur
    - Archaeology
    - Archaeological remains
    - Preservation
    - Data
    - Notes
categories:
    - Uncategorized
---
Ur (Sumerian: Urim;[1] Sumerian Cuneiform: 𒋀𒀕𒆠 URIM2KI or 𒋀𒀊𒆠 URIM5KI;[2] Akkadian: Uru;[3] Arabic: أور‎‎) was an important Sumerian city-state in ancient Mesopotamia, located at the site of modern Tell el-Muqayyar (Arabic: تل المقير‎‎) in south Iraq's Dhi Qar Governorate.[4] Although Ur was once a coastal city near the mouth of the Euphrates on the Persian Gulf, the coastline has shifted and the city is now well inland, south of the Euphrates on its right bank, 16 kilometres (9.9 mi) from Nasiriyah.[5]
