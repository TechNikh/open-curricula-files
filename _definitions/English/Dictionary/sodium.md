---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sodium
offline_file: ""
offline_thumbnail: ""
uuid: bc0207fc-a9c2-4967-9680-7943531cfff6
updated: 1484310371
title: sodium
categories:
    - Dictionary
---
sodium
     n : a silvery soft waxy metallic element of the alkali metal
         group; occurs abundantly in natural compounds (especially
         in salt water); burns with a yellow flame and reacts
         violently in water; occurs in sea water and in the
         mineral halite (rock salt) [syn: {Na}, {atomic number 11}]
