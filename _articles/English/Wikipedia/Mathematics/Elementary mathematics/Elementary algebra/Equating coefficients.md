---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equating_coefficients
offline_file: ""
offline_thumbnail: ""
uuid: 059dcac7-acad-4c7b-9a8d-b9f04069b20e
updated: 1484308907
title: Equating coefficients
tags:
    - Example in real fractions
    - Example in nested radicals
    - Example of testing for linear dependence of equations
    - Example in complex numbers
categories:
    - Elementary algebra
---
In mathematics, the method of equating the coefficients is a way of solving a functional equation of two expressions such as polynomials for a number of unknown parameters. It relies on the fact that two expressions are identical precisely when corresponding coefficients are equal for each different type of term. The method is used to bring formulas into a desired form.
