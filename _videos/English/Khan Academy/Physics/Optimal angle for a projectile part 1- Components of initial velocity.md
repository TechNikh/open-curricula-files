---
version: 1
type: video
provider: YouTube
id: RhUdv0jjfcE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Optimal%20angle%20for%20a%20projectile%20part%201-%20Components%20of%20initial%20velocity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 33b7f6f7-3865-4125-9304-b3c90ce22a55
updated: 1486069591
title: 'Optimal angle for a projectile part 1- Components of initial velocity'
tags:
    - Optimal
    - angle
    - for
    - projectile
    - part
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/RhUdv0jjfcE/default.jpg
    - https://i3.ytimg.com/vi/RhUdv0jjfcE/1.jpg
    - https://i3.ytimg.com/vi/RhUdv0jjfcE/2.jpg
    - https://i3.ytimg.com/vi/RhUdv0jjfcE/3.jpg
---
You want a projectile to fly as far as possible, at which angle should you launch it? We'll start with formulas for the initial velocity. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/two-dimensional-motion/optimal-projectile-angle/v/optimal-angle-for-a-projectile-part-2-hangtime?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/two-dimensional-motion/two-dimensional-projectile-mot/v/projectile-motion-with-ordered-set-notation?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
