---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aunt
offline_file: ""
offline_thumbnail: ""
uuid: a510e466-a4a8-4f2d-8ef4-a074166bfab0
updated: 1484310301
title: aunt
categories:
    - Dictionary
---
aunt
     n : the sister of your father or mother; the wife of your uncle
         [syn: {auntie}, {aunty}] [ant: {uncle}]
