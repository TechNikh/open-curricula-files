---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ph
offline_file: ""
offline_thumbnail: ""
uuid: 6c69227b-dd40-4b4f-af64-0f812a8f659b
updated: 1484310337
title: ph
categories:
    - Dictionary
---
pH
     n : (chemistry) p(otential of) H(ydrogen); the logarithm of the
         reciprocal of hydrogen-ion concentration in gram atoms
         per liter; provides a measure on a scale from 0 to 14 of
         the acidity or alkalinity of a solution (where 7 is
         neutral and greater than 7 is acidic and less than 7 is
         basic) [syn: {pH scale}]
