---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defective
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484394961
title: defective
categories:
    - Dictionary
---
defective
     adj 1: having a defect; "I returned the appliance because it was
            defective" [syn: {faulty}]
     2: markedly subnormal in structure or function or intelligence
        or behavior; "defective speech"
     3: not working properly; "a bad telephone connection"; "a
        defective appliance" [syn: {bad}]
