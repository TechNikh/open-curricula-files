---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tapping
offline_file: ""
offline_thumbnail: ""
uuid: 3e38811d-072c-40dd-9550-0b8ef7665314
updated: 1484310258
title: tapping
categories:
    - Dictionary
---
tap
     n 1: the sound made by a gentle blow [syn: {pat}, {rap}]
     2: a faucet for drawing water from a pipe or cask [syn: {water
        faucet}, {water tap}, {spigot}, {hydrant}]
     3: a gentle blow [syn: {rap}, {strike}]
     4: a small metal plate that attaches to the toe or heel of a
        shoe (as in tap dancing)
     5: a tool for cutting female (internal) screw threads
     6: a plug for a bunghole in a cask [syn: {spigot}]
     7: the act of tapping a telephone or telegraph line to get
        information [syn: {wiretap}]
     8: a light touch or stroke [syn: {pat}, {dab}]
     v 1: cut a female screw thread with a tap
     2: draw from or dip into to get something; "tap ...
