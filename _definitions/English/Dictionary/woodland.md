---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/woodland
offline_file: ""
offline_thumbnail: ""
uuid: 8ca1b522-9590-4ac6-999f-83724e6316bc
updated: 1484310268
title: woodland
categories:
    - Dictionary
---
woodland
     n : land that is covered with trees and shrubs [syn: {forest}, {timberland},
          {timber}]
