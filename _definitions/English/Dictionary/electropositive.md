---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electropositive
offline_file: ""
offline_thumbnail: ""
uuid: 8342fe3c-90e4-4911-9f64-99a67ce99e18
updated: 1484310403
title: electropositive
categories:
    - Dictionary
---
electropositive
     adj : having a positive electric charge; "protons are positive"
           [syn: {positive}] [ant: {negative}, {neutral}]
