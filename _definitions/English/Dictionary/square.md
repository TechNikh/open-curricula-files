---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/square
offline_file: ""
offline_thumbnail: ""
uuid: b05d5806-dcd4-49b7-83d0-f025e9b3c54d
updated: 1484310268
title: square
categories:
    - Dictionary
---
square
     adj 1: having four equal sides and four right angles or forming a
            right angle; "a square peg in a round hole"; "a square
            corner" [ant: {round}]
     2: leaving no balance; "my account with you is now all square"
        [syn: {square(p)}]
     3: characterized by honesty and fairness; "a square deal";
        "wanted to do the square thing" [syn: {straight}]
     4: without evasion or compromise; "a square contradiction"; "he
        is not being as straightforward as it appears" [syn: {square(a)},
         {straightforward}]
     5: rigidly conventional or old-fashioned [syn: {straight}]
     n 1: (geometry) a plane rectangle with four equal sides and ...
