---
version: 1
type: video
provider: YouTube
id: ZVwoYVkg-m4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: af55e6f5-d309-4679-8d6e-d90f71d7c2f3
updated: 1486069643
title: 'Fractal Geometry - Frax HD and Mandelbulb 3D Animation'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZVwoYVkg-m4/default.jpg
    - https://i3.ytimg.com/vi/ZVwoYVkg-m4/1.jpg
    - https://i3.ytimg.com/vi/ZVwoYVkg-m4/2.jpg
    - https://i3.ytimg.com/vi/ZVwoYVkg-m4/3.jpg
---
