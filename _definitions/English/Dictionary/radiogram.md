---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radiogram
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484508601
title: radiogram
categories:
    - Dictionary
---
radiogram
     n 1: a message transmitted by wireless telegraphy
     2: a photographic image produced on a radiosensitive surface by
        radiation other than visible light (especially by X-rays
        or gamma rays) [syn: {radiograph}, {shadowgraph}, {skiagraph},
         {skiagram}]
