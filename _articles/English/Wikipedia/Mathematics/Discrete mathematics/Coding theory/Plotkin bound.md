---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plotkin_bound
offline_file: ""
offline_thumbnail: ""
uuid: 36404705-c68b-4a38-82fd-9dad96626fe9
updated: 1484308549
title: Plotkin bound
tags:
    - Statement of the bound
    - Proof of case i)
categories:
    - Coding theory
---
In the mathematics of coding theory, the Plotkin bound, named after Morris Plotkin, is a limit (or bound) on the maximum possible number of codewords in binary codes of given length n and given minimum distance d.
