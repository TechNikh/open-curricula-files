---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaotic_scattering
offline_file: ""
offline_thumbnail: ""
uuid: 47347133-df7b-4322-aa5a-434968308639
updated: 1484308702
title: Chaotic scattering
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Gaspard-Rice_scattering_system.png
tags:
    - Gaspard–Rice system
    - Decay rate
    - An experimental system and the stable manifold
    - The invariant set and the symbolic dynamics
    - >
        Relationship between the fractal dimension, decay rate and
        Lyapunov exponents
categories:
    - Chaos theory
---
Chaotic scattering is a branch of chaos theory dealing with scattering systems displaying a strong sensitivity to initial conditions. In a classical scattering system there will be one or more impact parameters, b, in which a particle is sent into the scatterer. This gives rise to one or more exit parameters, y, as the particle exits towards infinity. While the particle is traversing the system, there may also be a delay time, T—the time it takes for the particle to exit the system—in addition to the distance travelled, s, which in certain systems, i.e., "billiard-like" systems in which the particle undergoes lossless collisions with hard, fixed objects, the two will be equivalent—see ...
