---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Scientific_method
offline_file: ""
offline_thumbnail: ""
uuid: d9ca73c5-3759-4d5c-acec-7f359b56d9c6
updated: 1484308377
title: Scientific method
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/450px-The_Scientific_Method_as_an_Ongoing_Process.svg.png
tags:
    - Overview
    - Process
    - Formulation of a question
    - Hypothesis
    - Prediction
    - Testing
    - Analysis
    - DNA example
    - Other components
    - Replication
    - External review
    - Data recording and sharing
    - Scientific inquiry
    - Properties of scientific inquiry
    - Beliefs and biases
    - Elements of the scientific method
    - Characterizations
    - Uncertainty
    - Definition
    - DNA-characterizations
    - 'Another example: precession of Mercury'
    - Hypothesis development
    - DNA-hypotheses
    - Predictions from the hypothesis
    - DNA-predictions
    - 'Another example: general relativity'
    - Experiments
    - DNA-experiments
    - Evaluation and improvement
    - DNA-iterations
    - Confirmation
    - Models of scientific inquiry
    - Classical model
    - Pragmatic model
    - Communication and community
    - Peer review evaluation
    - Documentation and replication
    - Archiving
    - Data sharing
    - Limitations
    - Dimensions of practice
    - Philosophy and sociology of science
    - Role of chance in discovery
    - History
    - Relationship with mathematics
    - Relationship with statistics
    - Problems and issues
    - History, philosophy, sociology
    - Notes
categories:
    - Uncategorized
---
The scientific method is a body of techniques for investigating phenomena, acquiring new knowledge, or correcting and integrating previous knowledge.[2] To be termed scientific, a method of inquiry is commonly based on empirical or measurable evidence subject to specific principles of reasoning.[3] The Oxford Dictionaries Online define the scientific method as "a method or procedure that has characterized natural science since the 17th century, consisting in systematic observation, measurement, and experiment, and the formulation, testing, and modification of hypotheses".[4] Experiments need to be designed to test hypotheses. The most important part of the scientific method is the ...
