---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/charcoal
offline_file: ""
offline_thumbnail: ""
uuid: b39ed5ae-c24e-43a2-8aed-316dc21b326b
updated: 1484310413
title: charcoal
categories:
    - Dictionary
---
charcoal
     adj : very dark gray [syn: {charcoal-gray}, {charcoal-grey}]
     n 1: a carbonaceous material obtained by heating wood or other
          organic matter in the absence of air [syn: {wood coal}]
     2: a stick of black carbon material used for drawing [syn: {fusain}]
     3: a very dark gray color [syn: {charcoal gray}, {charcoal grey},
         {oxford gray}, {oxford grey}]
     4: a drawing made with charcoal
     v : draw, trace, or represent with charcoal
