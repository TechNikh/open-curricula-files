---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/industrialist
offline_file: ""
offline_thumbnail: ""
uuid: b45839e2-ddd9-4023-b109-b953b3071160
updated: 1484310529
title: industrialist
categories:
    - Dictionary
---
industrialist
     n : someone who manages or has significant financial interest in
         an industrial enterprise
