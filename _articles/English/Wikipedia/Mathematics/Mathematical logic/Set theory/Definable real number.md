---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Definable_real_number
offline_file: ""
offline_thumbnail: ""
uuid: 060d2146-502d-471e-ac46-22cb454d46b4
updated: 1484309501
title: Definable real number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Square_root_of_2_triangle.svg.png
tags:
    - Constructible numbers
    - Algebraic numbers
    - Computable real numbers
    - Definability in arithmetic
    - Definability in models of ZFC
categories:
    - Set theory
---
Definable real numbers are those that can be uniquely specified by a description. The description may be expressed as a construction or as a formula of a formal language. For example, the positive square root of 2, 
  
    
      
        
          
            2
          
        
      
    
    {\displaystyle {\sqrt {2}}}
  
, can be defined as the unique positive solution to the equation 
  
    
      
        
          x
          
            2
          
        
        =
        2
      
    
    {\displaystyle x^{2}=2}
  
, and it can be constructed with a compass and straightedge.
