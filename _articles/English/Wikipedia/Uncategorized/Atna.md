---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Atna
offline_file: ""
offline_thumbnail: ""
uuid: 1e592ff5-3d1d-48a3-99d0-c4fec318be34
updated: 1484308413
title: Atna
categories:
    - Uncategorized
---
Atna is a village in the Stor-Elvdal municipality, in Hedmark county, Norway. It lies in the Østerdalen valley between Koppang and Alvdal. It has a train halt on the Rørosbanen where the train stops by request. The Rondane mountains, Gudbrandsdal valley, and Ringebu are nearby. There is a brewery founded by Morten M (of Verdens Gang) and a group of local investors including Sverre Oskar Øverby. The former has since left the brewery, which continues to brew a natural beer with a rarely seen post-fermenting process in the bottle. Strolling past the brewery for a minute you will even find an airstrip. Although regular, commercial flights are unlikely the 500 meter gravel strip is kept well ...
