---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robin_Wilson_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 9f5c90b7-6a23-474d-bbd7-b1ce58b69231
updated: 1484309479
title: Robin Wilson (mathematician)
tags:
    - Education
    - Mathematics
    - Other interests
    - Other publications
categories:
    - Graph theorists
---
Robin James Wilson (born 5 December 1943) is an emeritus professor in the Department of Mathematics at the Open University, having previously been Head of the Pure Mathematics Department and Dean of the Faculty.[1] He was a Stipendiary Lecturer at Pembroke College, Oxford[2] and, as of 2006[update], Professor of Geometry at Gresham College, London, where he has also been a visiting professor.[3] On occasion, he guest teaches at Colorado College.
