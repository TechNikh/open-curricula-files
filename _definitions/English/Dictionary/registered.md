---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/registered
offline_file: ""
offline_thumbnail: ""
uuid: c41668d2-2e3d-4f90-b038-43b0fd8931b4
updated: 1484310455
title: registered
categories:
    - Dictionary
---
registered
     adj 1: (of animals) officially recorded with or certified by a
            recognized breed association; especially in a stud
            book; "a registered Percheron" [ant: {unregistered}]
     2: listed or recorded officially; "record is made of
        `registered mail' at each point on its route to assure
        safe delivery"; "registered bonds" [ant: {unregistered}]
     3: (of a boat or vessel) furnished with necessary official
        documents specifying ownership etc
