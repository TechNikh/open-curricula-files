---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fresh
offline_file: ""
offline_thumbnail: ""
uuid: c226e220-20f7-4868-828e-e680d58357d5
updated: 1484310260
title: fresh
categories:
    - Dictionary
---
fresh
     adj 1: not stale or old; "fresh bread"; "a fresh scent" [ant: {stale}]
     2: (of a cycle) beginning or occurring again; "a fresh start";
        "fresh ideas"
     3: imparting vitality and energy; "the bracing mountain air"
        [syn: {bracing}, {brisk}, {energizing}, {energising}, {refreshing},
         {refreshful}, {tonic}]
     4: of a kind not seen before; "the computer produced a
        completely novel proof of a well-known theorem" [syn: {new},
         {novel}]
     5: not canned or otherwise preserved; "fresh vegetables" [ant:
        {preserved}]
     6: not containing or composed of salt water; "fresh water"
        [ant: {salt}]
     7: having recently calved ...
