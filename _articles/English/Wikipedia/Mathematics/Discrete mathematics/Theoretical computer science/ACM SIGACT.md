---
version: 1
type: article
id: https://en.wikipedia.org/wiki/ACM_SIGACT
offline_file: ""
offline_thumbnail: ""
uuid: 84362452-69f2-47a9-bdeb-2117c3d70312
updated: 1484308662
title: ACM SIGACT
tags:
    - Publications
    - Conferences
    - Awards and prizes
categories:
    - Theoretical computer science
---
ACM SIGACT or SIGACT is the Association for Computing Machinery Special Interest Group on Algorithms and Computation Theory, whose purpose is support of research in theoretical computer science. It was founded in 1968 by Patrick C. Fischer.[1]
