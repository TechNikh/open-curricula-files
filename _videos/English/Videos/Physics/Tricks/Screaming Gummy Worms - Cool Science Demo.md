---
version: 1
type: video
provider: YouTube
id: nSYDd3j1Luk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2aec6a18-40d4-49ac-8c00-4f2ddf051fd0
updated: 1486069660
title: 'Screaming Gummy Worms - Cool Science Demo'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/nSYDd3j1Luk/default.jpg
    - https://i3.ytimg.com/vi/nSYDd3j1Luk/1.jpg
    - https://i3.ytimg.com/vi/nSYDd3j1Luk/2.jpg
    - https://i3.ytimg.com/vi/nSYDd3j1Luk/3.jpg
---
