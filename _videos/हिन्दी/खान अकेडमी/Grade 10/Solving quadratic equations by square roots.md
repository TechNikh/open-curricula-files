---
version: 1
type: video
provider: YouTube
id: 8MuJEfE308U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 910ff1d3-78f5-459f-aab0-1f49d68fe24e
updated: 1486069619
title: Solving quadratic equations by square roots
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/8MuJEfE308U/default.jpg
    - https://i3.ytimg.com/vi/8MuJEfE308U/1.jpg
    - https://i3.ytimg.com/vi/8MuJEfE308U/2.jpg
    - https://i3.ytimg.com/vi/8MuJEfE308U/3.jpg
---
