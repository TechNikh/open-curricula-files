---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minkowski_addition
offline_file: ""
offline_thumbnail: ""
uuid: d0bcd5b2-8d1d-460e-82bf-547351e605b4
updated: 1484309193
title: Minkowski addition
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-%25D0%25A1%25D1%2583%25D0%25BC%25D0%25BC%25D0%25B0_%25D0%259C%25D0%25B8%25D0%25BD%25D0%25BA%25D0%25BE%25D0%25B2%25D1%2581%25D0%25BA%25D0%25BE%25D0%25B3%25D0%25BE.svg.png'
tags:
    - Example
    - Convex hulls of Minkowski sums
    - Applications
    - Motion planning
    - Numerical control (NC) machining
    - 3d Solid Modeling
    - Aggregation Theory
    - Algorithms for computing Minkowski sums
    - Planar case
    - Two convex polygons in the plane
    - Other
    - Essential Minkowski sum
    - Notes
categories:
    - Convex geometry
---
In geometry, the Minkowski sum (also known as dilation) of two sets of position vectors A and B in Euclidean space is formed by adding each vector in A to each vector in B, i.e., the set
