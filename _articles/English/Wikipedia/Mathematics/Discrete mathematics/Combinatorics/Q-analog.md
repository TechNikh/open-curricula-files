---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Q-analog
offline_file: ""
offline_thumbnail: ""
uuid: c57c813f-2540-48bb-9f3b-46e0b007a176
updated: 1484308583
title: Q-analog
tags:
    - "Classical" q-theory
    - Combinatorial q-analogs
    - q → 1
categories:
    - Combinatorics
---
Roughly speaking, in mathematics, specifically in the areas of combinatorics and special functions, a q-analog of a theorem, identity or expression is a generalization involving a new parameter q that returns the original theorem, identity or expression in the limit as q → 1 (this limit is often formal, as q is often discrete-valued). Typically, mathematicians are interested in q-analogs that arise naturally, rather than in arbitrarily contriving q-analogs of known results. The earliest q-analog studied in detail is the basic hypergeometric series, which was introduced in the 19th century.[1]
