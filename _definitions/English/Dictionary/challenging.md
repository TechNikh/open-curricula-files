---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/challenging
offline_file: ""
offline_thumbnail: ""
uuid: 059d1283-dd65-4867-998a-d24c2d5c787b
updated: 1484310591
title: challenging
categories:
    - Dictionary
---
challenging
     adj 1: requiring full use of your abilities or resources;
            "ambitious schedule"; "performed the most challenging
            task without a mistake" [syn: {ambitious}]
     2: stimulating interest or thought; "a challenging hypothesis";
        "a thought-provoking book" [syn: {thought-provoking}]
     3: disturbingly provocative; "an intriguing smile" [syn: {intriguing}]
