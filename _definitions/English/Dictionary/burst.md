---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/burst
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484615641
title: burst
categories:
    - Dictionary
---
burst
     adj : suddenly and violently broken open especially from internal
           pressure (`busted' is an informal term for `burst'); "a
           burst balloon"; "burst pipes"; "burst seams"; "a
           ruptured appendix"; "a busted balloon" [syn: {ruptured},
            {busted}]
     n 1: the act of exploding or bursting something; "the explosion
          of the firecrackers awoke the children"; "the burst of
          an atom bomb creates enormous radiation aloft" [syn: {explosion}]
     2: rapid simultaneous discharge of firearms; "our fusillade
        from the left flank caught them by surprise" [syn: {fusillade},
         {salvo}, {volley}]
     3: a sudden flurry of ...
