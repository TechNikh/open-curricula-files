---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cost
offline_file: ""
offline_thumbnail: ""
uuid: b23a2cdd-2966-4f11-be10-1fadd793e618
updated: 1484310260
title: cost
categories:
    - Dictionary
---
cost
     n 1: the total spent for goods or services including money and
          time and labor
     2: the property of having material worth (often indicated by
        the amount of money something would bring if sold); "the
        fluctuating monetary value of gold and silver"; "he puts a
        high price on his services"; "he couldn't calculate the
        cost of the collection" [syn: {monetary value}, {price}]
     3: value measured by what must be given or done or undergone to
        obtain something; "the cost in human life was enormous";
        "the price of success is hard work"; "what price glory?"
        [syn: {price}, {toll}]
     v 1: be priced at; "These shoes cost ...
