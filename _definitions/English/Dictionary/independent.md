---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/independent
offline_file: ""
offline_thumbnail: ""
uuid: 6c337ed1-431f-44c0-87dd-fe6f2411ca8c
updated: 1484310299
title: independent
categories:
    - Dictionary
---
independent
     adj 1: free from external control and constraint; "an independent
            mind"; "a series of independent judgments"; "fiercely
            independent individualism"; "an independent republic"
            [ant: {dependent}]
     2: not dependent on or conditioned by or relative to anything
        else
     3: of political bodies; "an autonomous judiciary"; "a sovereign
        state" [syn: {autonomous}, {self-governing}, {sovereign}]
     4: not contingent
     5: of a clause; able to stand alone syntactically as a complete
        sentence; "the main (or independent) clause in a complex
        sentence has at least a subject and a verb" [syn: {main(a)}]
        ...
