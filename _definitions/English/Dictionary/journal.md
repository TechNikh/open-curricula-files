---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/journal
offline_file: ""
offline_thumbnail: ""
uuid: 3ee67ad8-6e95-4958-bd09-ba4e155dfeb2
updated: 1484310311
title: journal
categories:
    - Dictionary
---
journal
     n 1: a daily written record of (usually personal) experiences and
          observations [syn: {diary}]
     2: a periodical dedicated to a particular subject; "he reads
        the medical journals"
     3: a ledger in which transactions have been recorded as they
        occurred [syn: {daybook}]
     4: a record book as a physical object
     5: the part of the axle contained by a bearing
