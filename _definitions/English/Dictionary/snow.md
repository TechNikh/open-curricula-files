---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/snow
offline_file: ""
offline_thumbnail: ""
uuid: c4fd4d25-e0db-482f-9add-173ef54d8114
updated: 1484310275
title: snow
categories:
    - Dictionary
---
snow
     n 1: precipitation falling from clouds in the form of ice
          crystals [syn: {snowfall}]
     2: a layer of snowflakes (white crystals of frozen water)
        covering the ground
     3: English writer of novels about moral dilemmas in academe
        (1905-1980) [syn: {C. P. Snow}, {Charles Percy Snow}, {Baron
        Snow of Leicester}]
     4: street names for cocaine [syn: {coke}, {blow}, {nose candy},
         {C}]
     v 1: fall as snow; "It was snowing all night"
     2: conceal one's true motives from especially by elaborately
        feigning good intentions so as to gain an end; "He
        bamboozled his professors into thinking that he knew the
        subject ...
