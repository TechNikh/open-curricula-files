---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vulnerable
offline_file: ""
offline_thumbnail: ""
uuid: afe4be37-5f9e-4b7e-8d7b-2ebb84f939ad
updated: 1484310457
title: vulnerable
categories:
    - Dictionary
---
vulnerable
     adj 1: susceptible to attack; "a vulnerable bridge" [ant: {invulnerable}]
     2: susceptible to criticism or persuasion or temptation;
        "vulnerable to bribery"; "an argument vulnerable to
        refutation"
     3: capable of being wounded or hurt; "vulnerable parts of the
        body"
     4: susceptible to physical or emotional injury; "at a tender
        age" [syn: {tender}]
