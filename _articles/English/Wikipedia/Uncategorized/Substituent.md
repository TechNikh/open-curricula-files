---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Substituent
offline_file: ""
offline_thumbnail: ""
uuid: e9f88882-81e9-4db2-8657-9d4620f9510f
updated: 1484308401
title: Substituent
tags:
    - Nomenclature
    - Methane substituents
    - Structures
    - Statistical distribution
categories:
    - Uncategorized
---
In organic chemistry and biochemistry, a substituent is an atom or group of atoms which replaces hydrogen atom on the parent chain of a hydrocarbon, becoming a moiety of the resultant new molecule. The terms substituent, side chain, group, branch, or pendant group are used almost interchangeably to describe branches from a parent structure,[1] though certain distinctions are made in the context of polymer chemistry.[2] In polymers, side chains extend from a backbone structure. In proteins, side chains are attached to the alpha carbon atoms of the amino acid backbone.
