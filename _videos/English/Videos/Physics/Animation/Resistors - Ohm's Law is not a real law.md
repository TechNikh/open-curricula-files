---
version: 1
type: video
provider: YouTube
id: G3H5lKoWPpY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fafdf9e1-8bfd-4d8a-b9f5-1c072e244ca3
updated: 1486069657
title: "Resistors - Ohm's Law is not a real law"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/G3H5lKoWPpY/default.jpg
    - https://i3.ytimg.com/vi/G3H5lKoWPpY/1.jpg
    - https://i3.ytimg.com/vi/G3H5lKoWPpY/2.jpg
    - https://i3.ytimg.com/vi/G3H5lKoWPpY/3.jpg
---
