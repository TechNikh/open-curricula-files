---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/visual
offline_file: ""
offline_thumbnail: ""
uuid: c9c3c50f-a4d6-4e04-b539-6d9b50333bb3
updated: 1484310601
title: visual
categories:
    - Dictionary
---
visual
     adj 1: relating to or using sight; "ocular inspection"; "an optical
            illusion"; "visual powers"; "visual navigation" [syn:
            {ocular}, {optic}, {optical}]
     2: able to be seen; "be sure of it; give me the ocular proof"-
        Shakespeare; "a visual presentation"; "a visual image"
        [syn: {ocular}]
