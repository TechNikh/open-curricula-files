---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/admitted
offline_file: ""
offline_thumbnail: ""
uuid: 956a730f-bdd4-4533-b060-5b6e6170a45d
updated: 1484310575
title: admitted
categories:
    - Dictionary
---
admit
     v 1: declare to be true or admit the existence or reality or
          truth of; "He admitted his errors"; "She acknowledged
          that she might have forgotten" [syn: {acknowledge}]
          [ant: {deny}]
     2: allow to enter; grant entry to; "We cannot admit non-members
        into our club" [syn: {allow in}, {let in}, {intromit}]
        [ant: {reject}]
     3: allow participation in or the right to be part of; permit to
        exercise the rights, functions, and responsibilities of;
        "admit someone to the profession"; "She was admitted to
        the New Jersey Bar" [syn: {let in}, {include}] [ant: {exclude}]
     4: admit into a group or community; "accept ...
