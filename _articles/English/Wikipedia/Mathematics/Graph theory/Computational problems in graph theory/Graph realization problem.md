---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_realization_problem
offline_file: ""
offline_thumbnail: ""
uuid: 45ce749c-0441-4671-adc1-4a41e10c29ac
updated: 1484309394
title: Graph realization problem
tags:
    - Solutions
    - Other notations
    - Related problems
categories:
    - Computational problems in graph theory
---
The graph realization problem is a decision problem in graph theory. Given a finite sequence 
  
    
      
        (
        
          d
          
            1
          
        
        ,
        …
        ,
        
          d
          
            n
          
        
        )
      
    
    {\displaystyle (d_{1},\dots ,d_{n})}
  
 of natural numbers, the problem asks whether there is labeled simple graph such that 
  
    
      
        (
        
          d
          
            1
          
        
        ,
        …
        ,
        
          d
          
            n
          
        
        )
      
    
    {\displaystyle (d_{1},\dots ,d_{n})}
  
 is the ...
