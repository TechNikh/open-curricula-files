---
version: 1
type: video
provider: YouTube
id: bJ_09eoCmag
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Determining%20a%20position%20vector-valued%20function%20for%20a%20parametrization%20of%20two%20parameters.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8ce2cb17-d71d-4879-906d-56a5a4780b24
updated: 1486069692
title: >
    Determining a position vector-valued function for a
    parametrization of two parameters
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/bJ_09eoCmag/default.jpg
    - https://i3.ytimg.com/vi/bJ_09eoCmag/1.jpg
    - https://i3.ytimg.com/vi/bJ_09eoCmag/2.jpg
    - https://i3.ytimg.com/vi/bJ_09eoCmag/3.jpg
---
