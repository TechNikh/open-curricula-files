---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hurly-burly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484496121
title: hurly-burly
categories:
    - Dictionary
---
hurly burly
     n : a disorderly outburst or tumult; "they were amazed by the
         furious disturbance they had caused" [syn: {disturbance},
          {disruption}, {commotion}, {stir}, {flutter}, {to-do}, {hoo-ha},
          {hoo-hah}, {kerfuffle}]
