---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alignments_of_random_points
offline_file: ""
offline_thumbnail: ""
uuid: 8ec6eee2-d53b-4191-bc69-d5f05e06e1cf
updated: 1484308567
title: Alignments of random points
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Ley_lines.svg.png
tags:
    - An estimate of the probability of chance alignments
    - A more precise estimate of the expected number of alignments
    - Computer simulation of alignments
categories:
    - Combinatorics
---
Alignments of random points in the plane can be demonstrated by statistics to be remarkably and counter-intuitively easy to find when a large number of random points are marked on a bounded flat surface. This has been put forward as a demonstration that ley lines and other similar mysterious alignments believed by some to be phenomena of deep significance might exist solely due to chance alone, as opposed to the supernatural or anthropological explanations put forward by their proponents.
