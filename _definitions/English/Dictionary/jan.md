---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jan
offline_file: ""
offline_thumbnail: ""
uuid: 126e812f-4134-45f9-92ed-c6f86b8dcbc4
updated: 1484310433
title: jan
categories:
    - Dictionary
---
Jan
     n : the first month of the year; begins 10 days after the winter
         solstice [syn: {January}]
