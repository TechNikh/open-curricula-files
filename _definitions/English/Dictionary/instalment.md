---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instalment
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484513941
title: instalment
categories:
    - Dictionary
---
instalment
     n 1: a part of a broadcast serial [syn: {episode}, {installment}]
     2: a part of a published serial [syn: {installment}]
     3: the act of installing something (as equipment); "the
        telephone installation took only a few minutes" [syn: {installation},
         {installing}, {installment}]
