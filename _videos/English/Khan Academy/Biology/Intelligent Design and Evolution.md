---
version: 1
type: video
provider: YouTube
id: qxOEz9aPZNY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Intelligent%20Design%20and%20Evolution.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8322c679-e7f8-4957-a778-f611d7bef580
updated: 1486069579
title: Intelligent Design and Evolution
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/qxOEz9aPZNY/default.jpg
    - https://i3.ytimg.com/vi/qxOEz9aPZNY/1.jpg
    - https://i3.ytimg.com/vi/qxOEz9aPZNY/2.jpg
    - https://i3.ytimg.com/vi/qxOEz9aPZNY/3.jpg
---
The argument that evolution speaks to being the most "intelligent design"
More free lessons at: http://www.khanacademy.org/video?v=qxOEz9aPZNY
