---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/torch
offline_file: ""
offline_thumbnail: ""
uuid: b9ce828d-3cbe-41cf-9c0b-e7180050336d
updated: 1484310200
title: torch
categories:
    - Dictionary
---
torch
     n 1: a light carried in the hand; consists of some flammable
          substance
     2: tall-stalked very woolly mullein with densely packed yellow
        flowers; ancient Greeks and Romans dipped the stalks in
        tallow for funeral torches [syn: {common mullein}, {great
        mullein}, {Aaron's rod}, {flannel mullein}, {woolly
        mullein}, {Verbascum thapsus}]
     3: a small portable battery-powered electric lamp [syn: {flashlight}]
     4: a burner that mixes air and gas to produce a very hot flame
        [syn: {blowtorch}, {blowlamp}]
     v : burn maliciously, as by arson; "The madman torched the
         barns"
