---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dil
offline_file: ""
offline_thumbnail: ""
uuid: c621e82d-36af-43b9-ba9a-ad3033e3e7e0
updated: 1484308317
title: Dil
tags:
    - Plot
    - Cast
    - Soundtrack
    - Awards and nominations
    - Won
    - Nominations
    - Controversies
categories:
    - Uncategorized
---
Dil (translation: Heart) is a 1990 Indian Hindi romantic drama film starring Madhuri Dixit, Aamir Khan, Anupam Kher and Saeed Jaffrey. It was directed by Indra Kumar with music composed by Anand-Milind. Dixit received the Filmfare Award for Best Actress for her performance. The film was remade in Telugu in 1993 under the title Tholi Muddhu, starring Divya Bharti and Prashanth; it was also remade in Bengali (Bangladesh) in 1997 under the title Amar Ghor Amar Beheshto (my home my heaven). The film was also remade in Kannada as Shivaranjini.
