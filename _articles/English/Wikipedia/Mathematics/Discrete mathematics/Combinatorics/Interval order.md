---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Interval_order
offline_file: ""
offline_thumbnail: ""
uuid: 260c3f46-3c8e-4daa-8a33-f860d2d4aaa4
updated: 1484308585
title: Interval order
tags:
    - Interval dimension
    - Combinatorics
    - Notes
categories:
    - Combinatorics
---
In mathematics, especially order theory, the interval order for a collection of intervals on the real line is the partial order corresponding to their left-to-right precedence relation—one interval, I1, being considered less than another, I2, if I1 is completely to the left of I2. More formally, a poset 
  
    
      
        P
        =
        (
        X
        ,
        ≤
        )
      
    
    {\displaystyle P=(X,\leq )}
  
 is an interval order if and only if there exists a bijection from 
  
    
      
        X
      
    
    {\displaystyle X}
  
 to a set of real intervals, so 
  
    
      
        
          x
          
            i
          
        
        ↦
  ...
