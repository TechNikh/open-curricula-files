---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mantelpiece
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448121
title: mantelpiece
categories:
    - Dictionary
---
mantelpiece
     n : shelf that projects from wall above fireplace; "in England
         they call a mantel a chimneypiece" [syn: {mantel}, {mantle},
          {mantlepiece}, {chimneypiece}]
