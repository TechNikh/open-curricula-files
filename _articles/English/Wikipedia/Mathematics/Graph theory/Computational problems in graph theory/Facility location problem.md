---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Facility_location_problem
offline_file: ""
offline_thumbnail: ""
uuid: 034e5f34-5220-46dc-b967-6448197e2138
updated: 1484309391
title: Facility location problem
tags:
    - Minimum facility location
    - Minimax facility location
    - NP hardness
    - Algorithms
    - Maxmin facility location
    - Free software for solving facility location problems
categories:
    - Computational problems in graph theory
---
The facility location problem, also known as location analysis or k-center problem, is a branch of operations research and computational geometry concerned with the optimal placement of facilities to minimize transportation costs while considering factors like avoiding placing hazardous materials near housing, and competitors' facilities. The techniques also apply to cluster analysis.
