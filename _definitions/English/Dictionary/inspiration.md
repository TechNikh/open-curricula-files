---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inspiration
offline_file: ""
offline_thumbnail: ""
uuid: b8c0d511-2050-4921-8395-ef38eba978f1
updated: 1484310172
title: inspiration
categories:
    - Dictionary
---
inspiration
     n 1: arousal of the mind to special unusual activity or
          creativity
     2: a product of your creative thinking and work; "he had little
        respect for the inspirations of other artists"; "after
        years of work his brainchild was a tangible reality" [syn:
         {brainchild}]
     3: a sudden intuition as part of solving a problem
     4: (theology) a special influence of a divinity on the minds of
        human beings; "they believe that the books of Scripture
        were written under divine guidance" [syn: {divine guidance}]
     5: arousing to a particular emotion or action [syn: {stirring}]
     6: the act of inhaling; the drawing in of air (or ...
