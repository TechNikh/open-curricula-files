---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comte
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484383441
title: comte
categories:
    - Dictionary
---
Comte
     n : French philosopher remembered as the founder of positivism;
         he also established sociology as a systematic field of
         study [syn: {Auguste Comte}, {Isidore Auguste Marie
         Francois Comte}]
