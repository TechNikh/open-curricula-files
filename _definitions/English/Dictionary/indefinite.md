---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indefinite
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484594161
title: indefinite
categories:
    - Dictionary
---
indefinite
     adj 1: vague or not clearly defined or stated; "must you be so
            indefinite?"; "amorphous blots of color having vague
            and indefinite edges"; "he would not answer so
            indefinite a proposal" [ant: {definite}]
     2: not decided or not known; "were indefinite about their
        plans"; "plans are indefinite"
