---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poem
offline_file: ""
offline_thumbnail: ""
uuid: 50718c60-4c0b-469c-b1ef-ba7927853b28
updated: 1484310316
title: poem
categories:
    - Dictionary
---
poem
     n : a composition written in metrical feet forming rhythmical
         lines [syn: {verse form}]
