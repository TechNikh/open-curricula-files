---
version: 1
type: video
provider: YouTube
id: s3EK1lGkf2s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dd7006fd-f37d-4d88-ad50-5e85838ea938
updated: 1486069637
title: 'Refraction of light explained through cup water and coin - Science experiments for kids'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/s3EK1lGkf2s/default.jpg
    - https://i3.ytimg.com/vi/s3EK1lGkf2s/1.jpg
    - https://i3.ytimg.com/vi/s3EK1lGkf2s/2.jpg
    - https://i3.ytimg.com/vi/s3EK1lGkf2s/3.jpg
---
