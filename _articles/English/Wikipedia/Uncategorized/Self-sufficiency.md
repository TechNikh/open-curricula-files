---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-sufficiency
offline_file: ""
offline_thumbnail: ""
uuid: 28b0e7e6-7cb5-43c9-864c-2f8a34feeff9
updated: 1484308470
title: Self-sufficiency
tags:
    - Influential people
categories:
    - Uncategorized
---
Self-sufficiency (also called self-containment) is the state of not requiring any aid, support, or interaction for survival; it is a type of personal or collective autonomy.[1] On a national scale, a totally self-sufficient economy that does not trade with the outside world is called an autarky.
