---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equation
offline_file: ""
offline_thumbnail: ""
uuid: b7e5ad66-1730-4c95-ab36-63ac69bc75be
updated: 1484310232
title: equation
categories:
    - Dictionary
---
equation
     n 1: a mathematical statement that two expressions are equal
     2: a state of being essentially equal or equivalent; equally
        balanced; "on a par with the best" [syn: {equality}, {equivalence},
         {par}]
     3: the act of regarding as equal [syn: {equating}]
