---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/backward
offline_file: ""
offline_thumbnail: ""
uuid: 94431a59-5d01-4c07-a4d2-5882b470f270
updated: 1484310341
title: backward
categories:
    - Dictionary
---
backward
     adj 1: directed or facing toward the back or rear; "a backward
            view" [ant: {forward}]
     2: (used of temperament or behavior) marked by a retiring
        nature; "a backward lover" [ant: {forward}]
     3: retarded in intellectual development [syn: {feebleminded}]
     adv 1: at or to or toward the back or rear; "he moved back";
            "tripped when he stepped backward"; "she looked
            rearward out the window of the car" [syn: {back}, {backwards},
             {rearward}, {rearwards}] [ant: {forward}]
     2: in a manner or order or direction the reverse of normal;
        "it's easy to get the `i' and the `e' backward in words
        like `seize' ...
