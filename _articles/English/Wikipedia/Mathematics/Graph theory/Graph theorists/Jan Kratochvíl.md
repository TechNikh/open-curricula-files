---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Jan_Kratochv%C3%ADl'
offline_file: ""
offline_thumbnail: ""
uuid: e4ac717b-13a0-483f-83d0-816830c3afa7
updated: 1484309470
title: Jan Kratochvíl
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Prof._Kratochv%25C3%25ADl.jpg'
categories:
    - Graph theorists
---
Kratochvíl was born on 10 February 1959 in Prague. He studied at Charles University in Prague, earning a master's degree in 1983 and a Ph.D. in 1987;[1] his dissertation, supervised by Jaroslav Nešetřil, combined graph theory with coding theory.[2] He remained at Charles University as a faculty member, earned his habilitation in 1995, and was promoted to full professor in 2003. From 2003 to 2011 he chaired the department of applied mathematics at Charles University, and since 2012 he has been dean of the Faculty of Mathematics and Physics there.[1]
