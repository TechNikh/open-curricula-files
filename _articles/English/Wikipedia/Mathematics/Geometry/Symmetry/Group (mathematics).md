---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Group_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 9c79f0de-6341-4fc7-b8e2-bcb764a4d10e
updated: 1484309309
title: Group (mathematics)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rubik%2527s_cube.svg.png'
tags:
    - Definition and illustration
    - 'First example: the integers'
    - Definition
    - 'Second example: a symmetry group'
    - History
    - Elementary consequences of the group axioms
    - Uniqueness of identity element and inverses
    - Division
    - Basic concepts
    - Group homomorphisms
    - Subgroups
    - Cosets
    - Quotient groups
    - Examples and applications
    - Numbers
    - Integers
    - Rationals
    - Modular arithmetic
    - Cyclic groups
    - Symmetry groups
    - General linear group and representation theory
    - Galois groups
    - Finite groups
    - Classification of finite simple groups
    - Groups with additional structure
    - Topological groups
    - Lie groups
    - Generalizations
    - Notes
    - Citations
    - General references
    - Special references
    - Historical references
categories:
    - Symmetry
---
In mathematics, a group is an algebraic structure consisting of a set of elements equipped with an operation that combines any two elements to form a third element. The operation satisfies four conditions called the group axioms, namely closure, associativity, identity and invertibility. One of the most familiar examples of a group is the set of integers together with the addition operation, but the abstract formalization of the group axioms, detached as it is from the concrete nature of any particular group and its operation, applies much more widely. It allows entities with highly diverse mathematical origins in abstract algebra and beyond to be handled in a flexible way while retaining ...
