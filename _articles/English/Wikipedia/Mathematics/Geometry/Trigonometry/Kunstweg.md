---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kunstweg
offline_file: ""
offline_thumbnail: ""
uuid: 3c3c8ada-dfa5-4073-b42a-feb3e489b28b
updated: 1484309341
title: Kunstweg
categories:
    - Trigonometry
---
Bürgi's Kunstweg is a set of algorithms invented by Jost Bürgi at the end of the 16th century.[1] They can be used for the calculation of sines to an arbitrary precision. Bürgi used these algorithms to calculate a Canon Sinuum, a table of sines in steps of 2 arc seconds. It is thought that this table had 8 sexagesimal places. Some authors have speculated that this table only covered the range from 0 to 45 degrees, but nothing seems to support this claim. Such tables were extremely important for navigation at sea. Johannes Kepler called the Canon Sinuum the most precise known table of sines (reference?). Bürgi explained his algorithms in his work Fundamentum Astronomiae which he ...
