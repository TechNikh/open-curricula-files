---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alliance
offline_file: ""
offline_thumbnail: ""
uuid: b7b23f5c-cc0f-44c7-8b30-67f2de5ab8dc
updated: 1484310236
title: alliance
categories:
    - Dictionary
---
alliance
     n 1: the state of being allied or confederated [syn: {confederation}]
     2: a connection based on kinship or marriage or common
        interest; "the shifting alliances within a large family";
        "their friendship constitutes a powerful bond between
        them" [syn: {bond}]
     3: an organization of people (or countries) involved in a pact
        or treaty [syn: {coalition}, {alignment}, {alinement}]
        [ant: {nonalignment}]
     4: a formal agreement establishing an association or alliance
        between nations or other groups to achieve a particular
        aim
     5: the act of forming an alliance or confederation [syn: {confederation}]
