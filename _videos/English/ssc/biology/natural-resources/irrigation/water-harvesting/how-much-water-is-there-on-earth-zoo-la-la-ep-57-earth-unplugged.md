---
title: How much water is there on Earth? - Zoo La La (Ep 57) - Earth Unplugged
youtube_id: jKOmNGEi0DY
tags:
    - Earth
    - Unplugged
    - sahara aquifer
    - Sam Hume
    - Zoo La La
    - Lake Michigan
    - Planet Earth
    - 70% water
    - ground water
    - sea water
    - Ice caps
    - Aquafirm
    - virtual water
    - processed water
    - how much water do we need
    - what if the water ran out
    - water scarity. How much water do we use.
categories:
    - Water harvesting
---
