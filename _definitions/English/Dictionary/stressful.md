---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stressful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484630521
title: stressful
categories:
    - Dictionary
---
stressful
     adj : extremely irritating to the nerves; "nerve-racking noise";
           "the stressful days before a war"; "a trying day at the
           office" [syn: {nerve-racking}, {nerve-wracking}, {trying}]
