---
version: 1
type: article
id: https://en.wikipedia.org/wiki/BCH_code
offline_file: ""
offline_thumbnail: ""
uuid: e63e2973-46f7-46d0-a09f-26d50e219b39
updated: 1484308547
title: BCH code
tags:
    - Definition and illustration
    - Primitive narrow-sense BCH codes
    - Example
    - General BCH codes
    - Special cases
    - Properties
    - Encoding
    - Decoding
    - Calculate the syndromes
    - Calculate the error location polynomial
    - Peterson–Gorenstein–Zierler algorithm
    - Factor error locator polynomial
    - Calculate error values
    - Forney algorithm
    - Explanation of Forney algorithm computation
    - Decoding based on extended Euclidean algorithm
    - Explanation of the decoding process
    - Correct the errors
    - Decoding examples
    - Decoding of binary code without unreadable characters
    - Decoding with unreadable characters
    - >
        Decoding with unreadable characters with a small number of
        errors
    - Citations
    - Primary sources
    - Secondary sources
categories:
    - Coding theory
---
In coding theory, the BCH codes form a class of cyclic error-correcting codes that are constructed using finite fields. BCH codes were invented in 1959 by French mathematician Alexis Hocquenghem, and independently in 1960 by Raj Bose and D. K. Ray-Chaudhuri.[1][2][3] The acronym BCH comprises the initials of these inventors' surnames (mistakingly, in the case of Ray-Chaudhuri).
