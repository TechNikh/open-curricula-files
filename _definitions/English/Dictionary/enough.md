---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enough
offline_file: ""
offline_thumbnail: ""
uuid: 738c96f4-4988-4e41-9ca8-8222e65b9fa4
updated: 1484310325
title: enough
categories:
    - Dictionary
---
enough
     adj : enough to meet a purpose; "an adequate income"; "the food
           was adequate"; "a decent wage"; "enough food"; "food
           enough" [syn: {adequate}, {decent}]
     n : an adequate quantity; a quantity that is large enough to
         achieve a purpose; "enough is as good as a feast"; "there
         is more than a sufficiency of lawyers in this country"
         [syn: {sufficiency}]
     adv : as much as necessary; "Have I eaten enough?"; (`plenty' is
           nonstandard) "I've had plenty, thanks" [syn: {plenty}]
