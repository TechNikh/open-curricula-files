---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/despite
offline_file: ""
offline_thumbnail: ""
uuid: ce64746c-efaf-4a2a-b2df-744050d94963
updated: 1484310443
title: despite
categories:
    - Dictionary
---
despite
     n 1: lack of respect accompanied by a feeling of intense dislike;
          "he was held in contempt"; "the despite in which
          outsiders were held is legendary" [syn: {contempt}, {disdain},
           {scorn}]
     2: contemptuous disregard; "she wanted neither favor nor
        despite"
