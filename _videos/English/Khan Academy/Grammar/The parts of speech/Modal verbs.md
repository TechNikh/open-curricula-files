---
version: 1
type: video
provider: YouTube
id: hp9T-7on2Ow
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Modal%20verbs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a6a37636-f2da-4dd6-b00c-6b40a45cbf5d
updated: 1486069601
title: Modal verbs
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/hp9T-7on2Ow/default.jpg
    - https://i3.ytimg.com/vi/hp9T-7on2Ow/1.jpg
    - https://i3.ytimg.com/vi/hp9T-7on2Ow/2.jpg
    - https://i3.ytimg.com/vi/hp9T-7on2Ow/3.jpg
---
There's a class of helper verbs known as modals that we use to express a bunch of conditions: we can use them to give advice, make guesses at how necessary or likely something is, make requests of people, and so on. They're super useful.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/e/modal-verbs?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/what-is-a-pronoun-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-verbs/e/modal-verbs?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
