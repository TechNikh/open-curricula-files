---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/select
offline_file: ""
offline_thumbnail: ""
uuid: 3b9b013e-790f-4282-a107-3d2c7f66a57f
updated: 1484310236
title: select
categories:
    - Dictionary
---
select
     adj 1: of superior grade; "choice wines"; "prime beef"; "prize
            carnations"; "quality paper"; "select peaches" [syn: {choice},
             {prime(a)}, {prize}, {quality}]
     2: selected or chosen for special qualifications; "the
        blue-ribbon event of the season" [syn: {blue-ribbon(a)}]
     v : pick out, select, or choose from a number of alternatives;
         "Take any one of these cards"; "Choose a good husband for
         your daughter"; "She selected a pair of shoes from among
         the dozen the salesgirl had shown her" [syn: {choose}, {take},
          {pick out}]
