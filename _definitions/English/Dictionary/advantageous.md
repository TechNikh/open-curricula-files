---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advantageous
offline_file: ""
offline_thumbnail: ""
uuid: 090cc4e5-d9a1-437a-9c4f-a6caec123dcf
updated: 1484310210
title: advantageous
categories:
    - Dictionary
---
advantageous
     adj 1: giving an advantage; "a contract advantageous to our
            country"; "socially advantageous to entertain often"
            [ant: {disadvantageous}]
     2: appropriate for achieving a particular end; implies a lack
        of concern for fairness [syn: {appropriate}]
