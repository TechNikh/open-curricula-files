---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parametric_array
offline_file: ""
offline_thumbnail: ""
uuid: 59b8b166-9d49-4833-b881-07e38f49e6b8
updated: 1484308811
title: Parametric array
tags:
    - History
    - Foundations
    - Applications
categories:
    - Nonlinear systems
---
In the field of acoustics, a parametric array is a nonlinear transduction mechanism that generates narrow, nearly side lobe-free beams of low frequency sound, through the mixing and interaction of high frequency sound waves, effectively overcoming the diffraction limit (a kind of spatial 'uncertainty principle') associated with linear acoustics.[1] The main side lobe-free beam of low frequency sound is created as a result of nonlinear mixing of two high frequency sound beams at their difference frequency. Parametric arrays can be formed in water,[2] air,[3] and earth materials/rock.[4][5]
