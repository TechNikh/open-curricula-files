---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fifth_power_(algebra)
offline_file: ""
offline_thumbnail: ""
uuid: d2f25fd3-a01e-4681-9ca2-a62ff97c82d8
updated: 1484308639
title: Fifth power (algebra)
categories:
    - Number theory
---
Fifth powers are also formed by multiplying a number by its fourth power, or the square of a number by its cube.
