---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Camera_resectioning
offline_file: ""
offline_thumbnail: ""
uuid: 98d5a6c6-5aa4-45dd-a6ac-c1bc65d79aac
updated: 1484309179
title: Camera resectioning
tags:
    - Parameters of camera model
    - Intrinsic parameters
    - Extrinsic parameters
    - Algorithms
    - "Zhang's method"
    - Derivation
    - "Tsai's Algorithm"
    - "Selby's method (for X-ray cameras)"
categories:
    - Geometry in computer vision
---
Camera resectioning is the process of estimating the parameters of a pinhole camera model approximating the camera that produced a given photograph or video. Usually, the pinhole camera parameters are represented in a 3 × 4 matrix called the camera matrix.
