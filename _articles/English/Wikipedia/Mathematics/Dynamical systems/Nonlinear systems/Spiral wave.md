---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spiral_wave
offline_file: ""
offline_thumbnail: ""
uuid: 1200ee1c-1882-4cad-86d4-60f9df11d47f
updated: 1484308814
title: Spiral wave
categories:
    - Nonlinear systems
---
Spiral waves are travelling waves that rotate outward from a center in a spiral. They are a feature of many excitable media. Spiral waves have been observed in various biological systems[1][2] including systems such as heart ventricular fibrillation,[3] retinal spreading depression,[4] Xenopus oocyte calcium waves,[5] and glial calcium waves in cortical tissue culture.[6]
