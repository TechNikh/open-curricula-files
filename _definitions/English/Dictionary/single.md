---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/single
offline_file: ""
offline_thumbnail: ""
uuid: f6406bc4-0264-45de-abe4-7bc78ea208d3
updated: 1484310279
title: single
categories:
    - Dictionary
---
single
     adj 1: existing alone or consisting of one entity or part or aspect
            or individual; "upon the hill stood a single tower";
            "had but a single thought which was to escape"; "a
            single survivor"; "a single serving"; "a single lens";
            "a single thickness" [syn: {single(a)}] [ant: {multiple}]
     2: used of flowers having usually only one row or whorl of
        petals; "single chrysanthemums resemble daisies and may
        have more than one row of petals" [ant: {double}]
     3: not married or related to the unmarried state; "unmarried
        men and women"; "unmarried life"; "sex and the single
        girl"; "single parenthood"; "are ...
