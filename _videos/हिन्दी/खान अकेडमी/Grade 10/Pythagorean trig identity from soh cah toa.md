---
version: 1
type: video
provider: YouTube
id: tmddDrPuGAc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 26f80b44-aab8-4c8b-96f3-8176c8501dfa
updated: 1486069619
title: Pythagorean trig identity from soh cah toa
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/tmddDrPuGAc/default.jpg
    - https://i3.ytimg.com/vi/tmddDrPuGAc/1.jpg
    - https://i3.ytimg.com/vi/tmddDrPuGAc/2.jpg
    - https://i3.ytimg.com/vi/tmddDrPuGAc/3.jpg
---
