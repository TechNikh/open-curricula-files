---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/encircled
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484580241
title: encircled
categories:
    - Dictionary
---
encircled
     adj 1: confined on all sides; "a camp surrounded by enemies"; "the
            encircled pioneers" [syn: {surrounded}]
     2: enclosed within a circle; "the encircled camp was complete
        cut off"
     3: adorned or crowned with a circlet; sometimes used as
        combining forms; "a brow encircled with laurel"; "wreathed
        in an extraordinary luminescence"; "ringed round with
        daisies"; "smoke-wreathed" [syn: {ringed}, {wreathed}]
