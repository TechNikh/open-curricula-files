---
version: 1
type: video
provider: YouTube
id: arYcc6IQ-WU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 832e17df-ee77-4980-86da-1a7e93c4a752
updated: 1486069607
title: First order homegenous equations
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/arYcc6IQ-WU/default.jpg
    - https://i3.ytimg.com/vi/arYcc6IQ-WU/1.jpg
    - https://i3.ytimg.com/vi/arYcc6IQ-WU/2.jpg
    - https://i3.ytimg.com/vi/arYcc6IQ-WU/3.jpg
---
