---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/handle
offline_file: ""
offline_thumbnail: ""
uuid: 1f4c5fb6-8f09-413d-9dd2-4cd10c068d36
updated: 1484310323
title: handle
categories:
    - Dictionary
---
handle
     n : the appendage to an object that is designed to be held in
         order to use or move it; "he grabbed the hammer by the
         handle"; "it was an old briefcase but it still had a good
         grip" [syn: {grip}, {handgrip}, {hold}]
     v 1: be in charge of, act on, or dispose of; "I can deal with
          this crew of workers"; "This blender can't handle nuts";
          "She managed her parents' affairs after they got too
          old" [syn: {manage}, {deal}, {care}]
     2: interact in a certain way; "Do right by her"; "Treat him
        with caution, please"; "Handle the press reporters gently"
        [syn: {treat}, {do by}]
     3: deal with verbally or in some ...
