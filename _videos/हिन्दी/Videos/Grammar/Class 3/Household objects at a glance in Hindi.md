---
version: 1
type: video
provider: YouTube
id: qrcMeLpKe74
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 86925f2b-6cbc-48a9-97fb-d2d4bac27521
updated: 1486069631
title: Household objects at a glance in Hindi
categories:
    - Class 3
thumbnail_urls:
    - https://i3.ytimg.com/vi/qrcMeLpKe74/default.jpg
    - https://i3.ytimg.com/vi/qrcMeLpKe74/1.jpg
    - https://i3.ytimg.com/vi/qrcMeLpKe74/2.jpg
    - https://i3.ytimg.com/vi/qrcMeLpKe74/3.jpg
---
