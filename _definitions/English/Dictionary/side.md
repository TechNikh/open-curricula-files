---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/side
offline_file: ""
offline_thumbnail: ""
uuid: 6bce296e-7fdf-4477-9f70-5bf3a62dfb00
updated: 1484310337
title: side
categories:
    - Dictionary
---
side
     adj 1: located on a side; "side fences"; "the side porch" [syn: {side(a)}]
            [ant: {top(a)}, {bottom(a)}]
     2: added as a consequence or supplement; "a side benefit" [syn:
         {side(a)}]
     n 1: a place within a region identified relative to a center or
          reference location; "they always sat on the right side
          of the church"; "he never left my side"
     2: one of two or more contesting groups; "the Confederate side
        was prepared to attack"
     3: either the left or right half of a body; "he had a pain in
        his side"
     4: an extended outer surface of an object; "he turned the box
        over to examine the bottom side"; "they ...
