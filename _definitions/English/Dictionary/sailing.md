---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sailing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484466241
title: sailing
categories:
    - Dictionary
---
sailing
     adj : traveling by boat or ship [syn: {at sea(p)}]
     n 1: the work of a sailor [syn: {seafaring}, {navigation}]
     2: riding in a sailboat
     3: the departure of a vessel from a port
     4: the activity of flying a glider [syn: {glide}, {gliding}, {sailplaning},
         {soaring}]
