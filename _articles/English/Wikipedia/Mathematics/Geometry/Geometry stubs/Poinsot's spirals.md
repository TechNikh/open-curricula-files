---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poinsot%27s_spirals'
offline_file: ""
offline_thumbnail: ""
uuid: aafbd9ea-564c-49ad-a219-956e5c861ed8
updated: 1484309369
title: "Poinsot's spirals"
categories:
    - Geometry stubs
---
where csch is the hyperbolic cosecant, and sech is the hyperbolic secant.[1] They are named after the French mathematician Louis Poinsot.
