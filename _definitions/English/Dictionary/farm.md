---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/farm
offline_file: ""
offline_thumbnail: ""
uuid: 68a559ca-3ce4-4100-b429-76a3f0ce5384
updated: 1484310246
title: farm
categories:
    - Dictionary
---
farm
     n : workplace consisting of farm buildings and cultivated land
         as a unit; "it takes several people to work the farm"
     v 1: be a farmer; work as a farmer; "My son is farming in
          California"
     2: collect fees or profits
     3: cultivate by growing, often involving improvements by means
        of agricultural techniques; "The Bordeaux region produces
        great red wines"; "They produce good ham in Parma"; "We
        grow wheat here"; "We raise hogs here" [syn: {grow}, {raise},
         {produce}]
