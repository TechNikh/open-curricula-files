---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tradition
offline_file: ""
offline_thumbnail: ""
uuid: 250f42de-c440-41d0-b2ba-3404f8581011
updated: 1484310240
title: tradition
categories:
    - Dictionary
---
tradition
     n 1: an inherited pattern of thought or action
     2: a specific practice of long standing [syn: {custom}]
