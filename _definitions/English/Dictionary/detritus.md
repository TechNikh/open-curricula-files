---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/detritus
offline_file: ""
offline_thumbnail: ""
uuid: 9187dc5d-cfb8-4177-bca8-c30878c9c3c8
updated: 1484310268
title: detritus
categories:
    - Dictionary
---
detritus
     n 1: the remains of something that has been destroyed or broken
          up [syn: {debris}, {dust}, {junk}, {rubble}]
     2: loose material (stone fragments and silt etc) that is worn
        away from rocks
