---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Connection_(affine_bundle)
offline_file: ""
offline_thumbnail: ""
uuid: 22fe3780-9fa9-4298-8170-3c401ac8187d
updated: 1484309207
title: Connection (affine bundle)
categories:
    - Differential geometry
---
Let 
  
    
      
        Y
        →
        X
        ,
      
    
    {\displaystyle Y\to X,}
  
 be an affine bundle modelled over a vector bundle 
  
    
      
        
          
            Y
            ¯
          
        
        →
        X
      
    
    {\displaystyle {\overline {Y}}\to X}
  
. A connection 
  
    
      
        Γ
      
    
    {\displaystyle \Gamma }
  
 on 
  
    
      
        Y
        →
        X
      
    
    {\displaystyle Y\to X}
  
 is called the affine connection if it as a section 
  
    
      
        Γ
        :
        Y
        →
        
          J
          
            1
          
        
        Y
      
    
   ...
