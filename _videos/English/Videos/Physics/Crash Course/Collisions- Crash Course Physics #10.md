---
version: 1
type: video
provider: YouTube
id: Y-QOfc2XqOk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 03b324e8-ff78-4e72-9ffd-ee92a0ee8274
updated: 1486069682
title: 'Collisions- Crash Course Physics #10'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/Y-QOfc2XqOk/default.jpg
    - https://i3.ytimg.com/vi/Y-QOfc2XqOk/1.jpg
    - https://i3.ytimg.com/vi/Y-QOfc2XqOk/2.jpg
    - https://i3.ytimg.com/vi/Y-QOfc2XqOk/3.jpg
---
