---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appropriate
offline_file: ""
offline_thumbnail: ""
uuid: a341e62c-5e4b-4b40-875a-76eab63cee16
updated: 1484310327
title: appropriate
categories:
    - Dictionary
---
appropriate
     adj 1: suitable for a particular person or place or condition etc;
            "a book not appropriate for children"; "a funeral
            conducted the appropriate solemnity"; "it seems that
            an apology is appropriate" [ant: {inappropriate}]
     2: appropriate for achieving a particular end; implies a lack
        of concern for fairness [syn: {advantageous}]
     3: meant or adapted for an occasion or use; "a tractor suitable
        (or fit) for heavy duty"; "not an appropriate (or fit)
        time for flippancy" [syn: {suitable}, {suited}]
     4: suitable and fitting; "the tailored clothes were harmonious
        with her military bearing" [syn: ...
