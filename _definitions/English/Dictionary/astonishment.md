---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/astonishment
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484647201
title: astonishment
categories:
    - Dictionary
---
astonishment
     n : the feeling that accompanies something extremely surprising;
         "he looked at me in astonishment" [syn: {amazement}]
