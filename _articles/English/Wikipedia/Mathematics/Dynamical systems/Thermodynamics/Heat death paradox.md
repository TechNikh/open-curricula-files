---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heat_death_paradox
offline_file: ""
offline_thumbnail: ""
uuid: 4fe888b0-2a01-479e-84c8-1aa453b8eb80
updated: 1484308843
title: Heat death paradox
categories:
    - Thermodynamics
---
Formulated in 1862 by Lord Kelvin, Hermann von Helmholtz and William John Macquorn Rankine,[1] the heat death paradox, also known as Clausius' paradox and thermodynamic paradox,[2] is a reductio ad absurdum argument that uses thermodynamics to show the impossibility of an infinitely old universe.
