---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Estimation_of_signal_parameters_via_rotational_invariance_techniques
offline_file: ""
offline_thumbnail: ""
uuid: 639e7ec6-6256-4704-895b-91a572249c6d
updated: 1484309343
title: >
    Estimation of signal parameters via rotational invariance
    techniques
categories:
    - Trigonometry
---
In estimation theory, estimation of signal parameters via rotational invariant techniques (ESPRIT) is a technique to determine parameters of a mixture of sinusoids in a background noise.
