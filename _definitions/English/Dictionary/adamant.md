---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adamant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484588461
title: adamant
categories:
    - Dictionary
---
adamant
     adj : not capable of being swayed or diverted from a course;
           unsusceptible to persuasion; "he is adamant in his
           refusal to change his mind"; "Cynthia was inexorable;
           she would have none of him"- W.Churchill; "an
           intransigent conservative opposed to every liberal
           tendancy" [syn: {adamantine}, {inexorable}, {intransigent}]
     n : very hard native crystalline carbon valued as a gem [syn: {diamond}]
