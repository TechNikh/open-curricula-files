---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Straightedge
offline_file: ""
offline_thumbnail: ""
uuid: 22247afc-18e3-4b96-b151-6e6f83727047
updated: 1484309332
title: Straightedge
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Architectural_scale.jpg
tags:
    - Compass-and-straightedge construction
categories:
    - Technical drawing
---
A straightedge is a tool with an edge free from curves, or straight, used for transcribing straight lines, or checking the straightness of lines. If it has equally spaced markings along its length, it is usually called a ruler.
