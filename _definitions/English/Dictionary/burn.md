---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/burn
offline_file: ""
offline_thumbnail: ""
uuid: f7591409-57bd-4ed6-b7f1-8998c873462b
updated: 1484310246
title: burn
categories:
    - Dictionary
---
burn
     n 1: pain that feels hot as if it were on fire [syn: {burning}]
     2: a browning of the skin resulting from exposure to the rays
        of the sun [syn: {tan}, {suntan}, {sunburn}]
     3: an injury cause by exposure to heat or chemicals or
        radiation
     4: a burned place or area [syn: {burn mark}]
     5: damage inflicted by burning
     v 1: destroy by fire; "They burned the house and his diaries"
          [syn: {fire}, {burn down}]
     2: shine intensely, as if with heat; "The coals were glowing in
        the dark"; "The candles were burning" [syn: {glow}]
     3: undergo combustion; "Maple wood burns well" [syn: {combust}]
     4: cause a sharp or stinging pain ...
