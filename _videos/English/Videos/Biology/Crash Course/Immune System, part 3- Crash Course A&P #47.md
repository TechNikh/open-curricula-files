---
version: 1
type: video
provider: YouTube
id: rd2cf5hValM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Immune%20System%2C%20part%203-%20Crash%20Course%20A%26P%20%2347.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 56087d05-89fa-430a-8d09-8d08fe0b8251
updated: 1486069681
title: 'Immune System, part 3- Crash Course A&P #47'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/rd2cf5hValM/default.jpg
    - https://i3.ytimg.com/vi/rd2cf5hValM/1.jpg
    - https://i3.ytimg.com/vi/rd2cf5hValM/2.jpg
    - https://i3.ytimg.com/vi/rd2cf5hValM/3.jpg
---
