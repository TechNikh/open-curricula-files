---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/globe
offline_file: ""
offline_thumbnail: ""
uuid: 88e05494-161f-434e-b91b-38454d78cb62
updated: 1484310433
title: globe
categories:
    - Dictionary
---
globe
     n 1: the 3rd planet from the sun; the planet on which we live;
          "the Earth moves around the sun"; "he sailed around the
          world" [syn: {Earth}, {world}]
     2: an object with a spherical shape; "a ball of fire" [syn: {ball},
         {orb}]
     3: a sphere on which a map (especially of the earth) is
        represented
