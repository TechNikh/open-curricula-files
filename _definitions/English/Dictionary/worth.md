---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worth
offline_file: ""
offline_thumbnail: ""
uuid: 6601c292-483b-47cc-aee3-6205cd854d18
updated: 1484310453
title: worth
categories:
    - Dictionary
---
worth
     adj 1: having sufficient worth; "an idea worth considering"; "a
            cause deserving or meriting support"; "the deserving
            poor" (often used ironically) [syn: {deserving(p)}, {meriting(p)},
             {worth(p)}]
     2: having a specified value;  "not worth his salt"; "worth her
        weight in gold" [syn: {worth(p)}]
     n 1: an indefinite quantity of something having a specified
          value; "10 dollars worth of gasoline"
     2: the quality that renders something desirable or valuable or
        useful [ant: {worthlessness}]
     3: French couturier (born in England) regarded as the founder
        of Parisian haute couture; noted for introducing ...
