---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/received
offline_file: ""
offline_thumbnail: ""
uuid: 3bf6bf53-1dfc-45c3-9212-bbc2714fc1d8
updated: 1484310286
title: received
categories:
    - Dictionary
---
received
     adj 1: conforming to the established language usage of educated
            native speakers; "standard English" (American);
            "received standard English is sometimes called the
            King's English" (British) [syn: {standard}] [ant: {nonstandard}]
     2: widely accepted as true or worthy; "the accepted wisdom
        about old age"; "a received moral idea"; "Received
        political wisdom says not; surveys show otherwise"-
        Economist [syn: {accepted}]
