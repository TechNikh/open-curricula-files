---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/linen
offline_file: ""
offline_thumbnail: ""
uuid: eb1c66c0-bfc3-46e0-b128-32ccaadbad76
updated: 1484310384
title: linen
categories:
    - Dictionary
---
linen
     n 1: a fabric woven with fibers from the flax plant
     2: a high-quality paper made of linen fibers or with a linen
        finish [syn: {linen paper}]
     3: white goods or clothing made with linen cloth
