---
version: 1
type: video
provider: YouTube
id: SjunMcrXgE0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Dangling%20modifiers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2313862d-f0e8-4f31-9d43-3135a63752d6
updated: 1486069601
title: Dangling modifiers
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/SjunMcrXgE0/default.jpg
    - https://i3.ytimg.com/vi/SjunMcrXgE0/1.jpg
    - https://i3.ytimg.com/vi/SjunMcrXgE0/2.jpg
    - https://i3.ytimg.com/vi/SjunMcrXgE0/3.jpg
---
A modifying word or phrase “dangles” when it doesn’t apply to the word it’s supposed to modify. Learn how to spot and fix this problem!

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/syntax/e/dangling-modifiers/ 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/phrases-and-clauses-khan-academy 

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/syntax/v/identifying-subject-direct-object-and-indirect-object-syntax-khan-academy 

Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
