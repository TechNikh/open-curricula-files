---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Travelling_salesman_problem
offline_file: ""
offline_thumbnail: ""
uuid: c26f7cff-c6d8-4dfb-ad42-e94c04e714f9
updated: 1484309398
title: Travelling salesman problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-GLPK_solution_of_a_travelling_salesman_problem.svg.png
tags:
    - History
    - Description
    - As a graph problem
    - Asymmetric and symmetric
    - Related problems
    - Integer linear programming formulation
    - Computing a solution
    - Exact algorithms
    - Heuristic and approximation algorithms
    - Constructive heuristics
    - "Christofides' algorithm for the TSP"
    - Iterative improvement
    - Randomized improvement
    - Ant colony optimization
    - Special cases of the TSP
    - Metric TSP
    - Euclidean TSP
    - Asymmetric TSP
    - Solving by conversion to symmetric TSP
    - "Analyst's travelling salesman problem"
    - TSP path length for random sets of points in a square
    - Upper bound
    - Lower bound
    - Computational complexity
    - Complexity of approximation
    - Human performance on TSP
    - Natural computation
    - Benchmarks
    - Popular culture
    - Notes
categories:
    - Computational problems in graph theory
---
The travelling salesman problem (TSP) asks the following question: "Given a list of cities and the distances between each pair of cities, what is the shortest possible route that visits each city exactly once and returns to the origin city?" It is an NP-hard problem in combinatorial optimization, important in operations research and theoretical computer science.
