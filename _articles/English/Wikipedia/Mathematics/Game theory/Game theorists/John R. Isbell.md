---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_R._Isbell
offline_file: ""
offline_thumbnail: ""
uuid: a233152b-69ef-47fc-9e05-e2121623d969
updated: 1484308506
title: John R. Isbell
tags:
    - Biography
    - Research
    - External resources
    - Mathematical Reviews
categories:
    - Game theorists
---
John Rolfe Isbell (October 27, 1930 – August 6, 2005)[1] was an American mathematician, for many years a professor of mathematics at the University at Buffalo (SUNY).
