---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bass_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: c41482ff-0da6-47d5-93d2-0f867ef424ec
updated: 1484309134
title: Bass conjecture
tags:
    - Statement of the conjecture
    - Known cases
    - Implications
categories:
    - Algebraic geometry
---
In mathematics, especially algebraic geometry, the Bass conjecture says that certain algebraic K-groups are supposed to be finitely generated. The conjecture was proposed by Hyman Bass.
