---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magnetic_field
offline_file: ""
offline_thumbnail: ""
uuid: 5c3bf8a9-26f9-499a-99d3-f1024e3bbf20
updated: 1484308317
title: Magnetic field
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-VFPt_cylindrical_magnet_thumb.svg.png
tags:
    - History
    - Definitions, units, and measurement
    - The B-field
    - The H-field
    - Units
    - Measurement
    - Magnetic field lines
    - Magnetic field and permanent magnets
    - Magnetic field of permanent magnets
    - Magnetic pole model and the H-field
    - Amperian loop model and the B-field
    - Force between magnets
    - Magnetic torque on permanent magnets
    - Magnetic field and electric currents
    - Magnetic field due to moving charges and electric currents
    - Force on moving charges and current
    - Force on a charged particle
    - Force on current-carrying wire
    - Direction of force
    - Relation between H and B
    - Magnetization
    - H-field and magnetic materials
    - magnetism
    - Energy stored in magnetic fields
    - 'Electromagnetism: the relationship between magnetic and electric fields'
    - "Faraday's Law: Electric force due to a changing B-field"
    - "Maxwell's correction to Ampère's Law: The magnetic field due to a changing electric field"
    - "Maxwell's equations"
    - 'Electric and magnetic fields: different aspects of the same phenomenon'
    - Magnetic vector potential
    - Quantum electrodynamics
    - Important uses and examples of magnetic field
    - "Earth's magnetic field"
    - Rotating magnetic fields
    - Hall effect
    - Magnetic circuits
    - Magnetic field shape descriptions
    - Magnetic dipoles
    - Magnetic monopole (hypothetical)
    - General
    - Mathematics
    - Applications
    - Notes
    - Information
    - Field density
    - Rotating magnetic fields
    - Diagrams
categories:
    - Uncategorized
---
A magnetic field is the magnetic effect of electric currents and magnetic materials. The magnetic field at any given point is specified by both a direction and a magnitude (or strength); as such it is a vector field.[nb 1] The term is used for two distinct but closely related fields denoted by the symbols B and H, where H is measured in units of amperes per meter (symbol: A⋅m−1 or A/m) in the SI. B is measured in teslas (symbol: T) and newtons per meter per ampere (symbol: N⋅m−1⋅A−1 or N/(m⋅A)) in the SI. B is most commonly defined in terms of the Lorentz force it exerts on moving electric charges.
