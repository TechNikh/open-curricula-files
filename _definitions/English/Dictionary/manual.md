---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manual
offline_file: ""
offline_thumbnail: ""
uuid: 8ae737a1-3ab8-42e0-bd00-050d3fd7c7ac
updated: 1484310518
title: manual
categories:
    - Dictionary
---
manual
     adj 1: of or relating to the hands; "manual dexterity"
     2: requiring human effort; "a manual transmission" [ant: {automatic}]
     3: doing or requiring physical work; "manual labor"; "manual
        laborer" [syn: {manual(a)}]
     n 1: a small handbook
     2: (military) a prescribed drill in handling a rifle [syn: {manual
        of arms}]
