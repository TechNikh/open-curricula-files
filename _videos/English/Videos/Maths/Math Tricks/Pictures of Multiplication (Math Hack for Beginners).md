---
version: 1
type: video
provider: YouTube
id: gaZgMPjxTiU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 21ad1dd0-3ec6-49f0-8a1f-59d78ed85b40
updated: 1486069625
title: Pictures of Multiplication (Math Hack for Beginners)
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/gaZgMPjxTiU/default.jpg
    - https://i3.ytimg.com/vi/gaZgMPjxTiU/1.jpg
    - https://i3.ytimg.com/vi/gaZgMPjxTiU/2.jpg
    - https://i3.ytimg.com/vi/gaZgMPjxTiU/3.jpg
---
