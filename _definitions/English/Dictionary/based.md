---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/based
offline_file: ""
offline_thumbnail: ""
uuid: c7bd4c81-dbda-4efd-9a6c-21fb1810b914
updated: 1484310344
title: based
categories:
    - Dictionary
---
based
     adj 1: being derived from (usually followed by `on' or `upon'); "a
            film based on a best-selling novel"
     2: having a base; "firmly based ice"
     3: having a basis; often used as combining terms; "a soundly
        based argument"; "well-founded suspicions" [syn: {founded}]
     4: having a base of operations; "a company based in Atlanta"
        [syn: {based(p)}]
