---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vortex_core_line
offline_file: ""
offline_thumbnail: ""
uuid: 8309c05c-1696-4b8d-b764-bd2c7bf38933
updated: 1484308878
title: Vortex core line
categories:
    - Vortices
---
Several methods exist to detect vortex core lines in a flow field. Jiang, Machiraju & Thompson (2004) studied and compared nine methods for vortex detection, including five methods for the identification of vortex core lines. Although this list is incomplete, they considered it representative for the state of the art (as of 2004).
