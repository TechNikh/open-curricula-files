---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proth_number
offline_file: ""
offline_thumbnail: ""
uuid: f32939ad-c9a6-4d60-94dc-0ae25a3b494a
updated: 1484308654
title: Proth number
categories:
    - Number theory
---
where 
  
    
      
        k
      
    
    {\displaystyle k}
  
 is an odd positive integer and 
  
    
      
        n
      
    
    {\displaystyle n}
  
 is a positive integer such that 
  
    
      
        
          2
          
            n
          
        
        >
        k
      
    
    {\displaystyle 2^{n}>k}
  
. Without the latter condition, all odd integers greater than 1 would be Proth numbers.[1]
