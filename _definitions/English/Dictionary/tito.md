---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tito
offline_file: ""
offline_thumbnail: ""
uuid: d111f286-1930-4e5c-be00-03a73bc4cc56
updated: 1484310178
title: tito
categories:
    - Dictionary
---
Tito
     n : Yugoslav statesman who led the resistance to German
         occupation during World War II and established a
         communist state after the war (1892-1980) [syn: {Marshal
         Tito}, {Josip Broz}]
