---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chyme
offline_file: ""
offline_thumbnail: ""
uuid: e7fc4eef-037b-43de-bfa3-56baf0c4bbb5
updated: 1484308358
title: Chyme
tags:
    - Properties
    - Path of chyme
    - Uses
categories:
    - Uncategorized
---
Chyme or chymus (/kaɪm/; from Greek χυμός khymos, "juice"[1][2]) is the semi-fluid mass of partly digested food that is expelled by the stomach, through the pyloric valve, into the duodenum[3] (the beginning of the small intestine).
