---
version: 1
type: video
provider: YouTube
id: 6CPVU6LmW7A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 92b08fb9-04bf-4733-8b1e-eb9cb2f6c73f
updated: 1486069619
title: Similar triangle example problems
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/6CPVU6LmW7A/default.jpg
    - https://i3.ytimg.com/vi/6CPVU6LmW7A/1.jpg
    - https://i3.ytimg.com/vi/6CPVU6LmW7A/2.jpg
    - https://i3.ytimg.com/vi/6CPVU6LmW7A/3.jpg
---
