---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Parity_of_zero
offline_file: ""
offline_thumbnail: ""
uuid: 4b30f901-b28c-4e99-adc2-ab0577279571
updated: 1484308921
title: Parity of zero
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Scale_of_justice_2.svg.png
tags:
    - Why zero is even
    - Basic explanations
    - Defining parity
    - Mathematical contexts
    - Not being odd
    - Even-odd alternation
    - Algebraic patterns
    - 2-adic order
    - Education
    - "Students' knowledge"
    - "Teachers' knowledge"
    - Implications for instruction
    - Numerical cognition
    - Everyday contexts
    - Notes
    - Bibliography
categories:
    - Elementary arithmetic
---
Zero is an even number. In other words, its parity—the quality of an integer being even or odd—is even. The simplest way to prove that zero is even is to check that it fits the definition of "even": it is an integer multiple of 2, specifically 0 × 2. As a result, zero shares all the properties that characterize even numbers: 0 is divisible by 2, 0 is neighbored on both sides by odd numbers, 0 is the sum of an integer (0) with itself, and a set of 0 objects can be split into two equal sets.
