---
version: 1
type: video
provider: YouTube
id: SCIBqg64PMQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/11.1%20Membrane%20Fluidity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 20e4edcf-c0b1-41be-99bb-0ec577af9c50
updated: 1486069674
title: 11.1 Membrane Fluidity
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/SCIBqg64PMQ/default.jpg
    - https://i3.ytimg.com/vi/SCIBqg64PMQ/1.jpg
    - https://i3.ytimg.com/vi/SCIBqg64PMQ/2.jpg
    - https://i3.ytimg.com/vi/SCIBqg64PMQ/3.jpg
---
