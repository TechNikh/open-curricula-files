---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ostrogradsky_instability
offline_file: ""
offline_thumbnail: ""
uuid: 81e4a3b4-2a0c-4efd-a011-3e02c20385bb
updated: 1484308795
title: Ostrogradsky instability
categories:
    - Hamiltonian mechanics
---
In applied mathematics, the Ostrogradsky instability is a consequence of a theorem of Mikhail Ostrogradsky in classical mechanics according to which a non-degenerate Lagrangian dependent on time derivatives of higher than the first corresponds to a linearly unstable Hamiltonian associated with the Lagrangian via a Legendre transform. The Ostrogradsky instability has been proposed as an explanation as to why no differential equations of higher order than two appear to describe physical phenomena.[1]
