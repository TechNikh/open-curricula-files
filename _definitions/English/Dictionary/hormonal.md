---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hormonal
offline_file: ""
offline_thumbnail: ""
uuid: 613049b8-cbb3-4d8f-a883-db1657e75b2d
updated: 1484310349
title: hormonal
categories:
    - Dictionary
---
hormonal
     adj : of or relating to or caused by hormones; "hormonal changes"
