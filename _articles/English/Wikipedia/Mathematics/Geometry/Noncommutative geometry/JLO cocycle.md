---
version: 1
type: article
id: https://en.wikipedia.org/wiki/JLO_cocycle
offline_file: ""
offline_thumbnail: ""
uuid: 495e9e24-424d-4829-ab7b-ec1b2ba8b7a8
updated: 1484309292
title: JLO cocycle
categories:
    - Noncommutative geometry
---
In noncommutative geometry, the JLO cocycle is a cocycle (and thus defines a cohomology class) in entire cyclic cohomology. It is a non-commutative version of the classic Chern character of the conventional differential geometry. In noncommutative geometry, the concept of a manifold is replaced by a noncommutative algebra 
  
    
      
        
          
            A
          
        
      
    
    {\displaystyle {\mathcal {A}}}
  
 of "functions" on the putative noncommutative space. The cyclic cohomology of the algebra 
  
    
      
        
          
            A
          
        
      
    
    {\displaystyle {\mathcal {A}}}
  
 contains the information about the topology ...
