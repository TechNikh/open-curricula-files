---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/house
offline_file: ""
offline_thumbnail: ""
uuid: 3509d61e-b1a8-4994-8850-2a64a32d29fc
updated: 1484310246
title: house
categories:
    - Dictionary
---
house
     n 1: a dwelling that serves as living quarters for one or more
          families; "he has a house on Cape Cod"; "she felt she
          had to get out of the house"
     2: an official assembly having legislative powers; "the
        legislature has two houses"
     3: a building in which something is sheltered or located; "they
        had a large carriage house"
     4: a social unit living together; "he moved his family to
        Virginia"; "It was a good Christian household"; "I waited
        until the whole house was asleep"; "the teacher asked how
        many people made up his home" [syn: {family}, {household},
         {home}, {menage}]
     5: a building where ...
