---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/family
offline_file: ""
offline_thumbnail: ""
uuid: 1c25835c-4c5e-4cdc-b70c-f65af3d5d7af
updated: 1484310309
title: family
categories:
    - Dictionary
---
family
     n 1: a social unit living together; "he moved his family to
          Virginia"; "It was a good Christian household"; "I
          waited until the whole house was asleep"; "the teacher
          asked how many people made up his home" [syn: {household},
           {house}, {home}, {menage}]
     2: primary social group; parents and children; "he wanted to
        have a good job before starting a family" [syn: {family
        unit}]
     3: people descended from a common ancestor; "his family has
        lived in Massachusetts since the Mayflower" [syn: {family
        line}, {folk}, {kinfolk}, {kinsfolk}, {sept}, {phratry}]
     4: a collection of things sharing a common ...
