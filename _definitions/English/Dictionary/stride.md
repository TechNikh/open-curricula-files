---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stride
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484650741
title: stride
categories:
    - Dictionary
---
stride
     n 1: a step in walking or running [syn: {pace}, {tread}]
     2: the distance covered by a step; "he stepped off ten paces
        from the old tree and began to dig" [syn: {footstep}, {pace},
         {step}]
     3: significant progress (especially in the phrase "make
        strides"); "they made big strides in productivity"
     v 1: walk with long steps; "He strode confidently across the
          hall"
     2: cover or traverse by taking long steps; "She strode several
        miles towards the woods"
     [also: {strode}, {stridden}]
