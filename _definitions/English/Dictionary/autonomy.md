---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/autonomy
offline_file: ""
offline_thumbnail: ""
uuid: 42cbdd1b-33fe-476c-8be6-d3a1b9f57c0b
updated: 1484310581
title: autonomy
categories:
    - Dictionary
---
autonomy
     n 1: immunity from arbitrary exercise of authority: political
          independence [syn: {liberty}]
     2: personal independence [syn: {self-direction}, {self-reliance},
         {self-sufficiency}]
