---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pocket
offline_file: ""
offline_thumbnail: ""
uuid: 6ca6f0d3-3548-4164-a22e-20bf2931e481
updated: 1484310445
title: pocket
categories:
    - Dictionary
---
pocket
     n 1: a small pouch inside a garment for carrying small articles
     2: an enclosed space; "the trapped miners found a pocket of
        air" [syn: {pouch}, {sac}, {sack}]
     3: a supply of money; "they dipped into the taxpayers' pockets"
     4: (bowling) the space between the headpin and the pins next
        bnehind it on the right or left; "the ball hit the pocket
        and gave him a perfect strike"
     5: a hollow concave shape made by removing something [syn: {scoop}]
     6: a local region of low pressure or descending air that causes
        a plane to lose height suddenly [syn: {air pocket}, {air
        hole}]
     7: a small isolated group of people; "they were ...
