---
version: 1
type: video
provider: YouTube
id: F5FvHFN8SE8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Compound-Complex%20Sentences%20in%20English.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a791ea52-26df-4661-8174-154ae875b264
updated: 1486069632
title: Compound-Complex Sentences in English
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/F5FvHFN8SE8/default.jpg
    - https://i3.ytimg.com/vi/F5FvHFN8SE8/1.jpg
    - https://i3.ytimg.com/vi/F5FvHFN8SE8/2.jpg
    - https://i3.ytimg.com/vi/F5FvHFN8SE8/3.jpg
---
