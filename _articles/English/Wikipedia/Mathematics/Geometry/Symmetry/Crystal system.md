---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crystal_system
offline_file: ""
offline_thumbnail: ""
uuid: f6b663f1-4ae7-45b7-a969-b438e37bd13c
updated: 1484309307
title: Crystal system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Carbon_lattice_diamond.png
tags:
    - Overview
    - Crystal classes
    - Bravais lattices
    - Crystal systems in four-dimensional space
categories:
    - Symmetry
---
In crystallography, the terms crystal system, crystal family, and lattice system each refer to one of several classes of space groups, lattices, point groups, or crystals. Informally, two crystals are in the same crystal system if they have similar symmetries, though there are many exceptions to this.
