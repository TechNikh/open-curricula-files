---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saved
offline_file: ""
offline_thumbnail: ""
uuid: 4f06ac6c-8f17-40d8-93d6-16c378dc3e2c
updated: 1484310330
title: saved
categories:
    - Dictionary
---
saved
     adj 1: rescued; especially from the power and consequences of sin;
            "a saved soul" [ant: {lost}]
     2: guarded from injury or destruction [syn: {protected}]
