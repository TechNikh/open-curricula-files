---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/whale
offline_file: ""
offline_thumbnail: ""
uuid: 41d72cf6-682a-40e7-bd71-5d2a22755b54
updated: 1484310286
title: whale
categories:
    - Dictionary
---
whale
     n 1: a very large person; impressive in size or qualities [syn: {giant},
           {hulk}, {heavyweight}]
     2: any of the larger cetacean mammals having a streamlined body
        and breathing through a blowhole on the head
     v : hunt for whales
