---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appointment
offline_file: ""
offline_thumbnail: ""
uuid: dcccd2c6-9e94-49e3-b528-c75dcf73529b
updated: 1484310457
title: appointment
categories:
    - Dictionary
---
appointment
     n 1: the act of putting a person into a non-elective position;
          "the appointment had to be approved by the whole
          committee" [syn: {assignment}, {designation}, {naming}]
     2: a meeting arranged in advance; "she asked how to avoid
        kissing at the end of a date" [syn: {date}, {engagement}]
     3: (usually plural) furnishings and equipment (especially for a
        ship or hotel) [syn: {fitting}]
     4: a person who is appointed to a job or position [syn: {appointee}]
     5: the job to which you are (or hope to be) appointed; "he
        applied for an appointment in the treasury"
     6: (law) the act of disposing of property by virtue of the
   ...
