---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bimatrix_game
offline_file: ""
offline_thumbnail: ""
uuid: 2eef309d-c406-4517-8e76-c1b6e2cf8b4d
updated: 1484309035
title: Bimatrix game
categories:
    - Game theory game classes
---
In game theory, a bimatrix game is a simultaneous game for two players in which each player has a finite number of possible actions. The name comes from the fact that the normal form of such a game can be described by two matrixes - matrix A describing the payoffs of player 1 and matrix B describing the payoffs of player 2.[1]
