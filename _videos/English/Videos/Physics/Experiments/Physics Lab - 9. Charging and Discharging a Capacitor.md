---
version: 1
type: video
provider: YouTube
id: c5sHcmUUZlA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c040191e-6baa-4f11-b721-267f50660e15
updated: 1486069640
title: 'Physics Lab - 9. Charging and Discharging a Capacitor'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/c5sHcmUUZlA/default.jpg
    - https://i3.ytimg.com/vi/c5sHcmUUZlA/1.jpg
    - https://i3.ytimg.com/vi/c5sHcmUUZlA/2.jpg
    - https://i3.ytimg.com/vi/c5sHcmUUZlA/3.jpg
---
