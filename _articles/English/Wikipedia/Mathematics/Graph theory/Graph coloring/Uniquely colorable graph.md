---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Uniquely_colorable_graph
offline_file: ""
offline_thumbnail: ""
uuid: f199a6a4-8c32-40a0-89b1-49b31208ed53
updated: 1484309386
title: Uniquely colorable graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_20.png
tags:
    - Examples
    - Properties
    - Related concepts
    - Minimal imperfection
    - Unique edge colorability
    - Unique total colorability
categories:
    - Graph coloring
---
