---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Apex_graph
offline_file: ""
offline_thumbnail: ""
uuid: 724300fb-9d2e-4feb-8819-c2bb98785c8a
updated: 1484309411
title: Apex graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Apex_graph.svg.png
tags:
    - Characterization and recognition
    - Chromatic number
    - Local treewidth
    - Embeddings
    - YΔY-reducibility
    - Nearly planar graphs
    - Notes
categories:
    - Graph minor theory
---
In graph theory, a branch of mathematics, an apex graph is a graph that can be made planar by the removal of a single vertex. The deleted vertex is called an apex of the graph. It is an apex, not the apex because an apex graph may have more than one apex; for example, in the minimal nonplanar graphs K5 or K3,3, every vertex is an apex. The apex graphs include graphs that are themselves planar, in which case again every vertex is an apex. The null graph is also counted as an apex graph even though it has no vertex to remove.
