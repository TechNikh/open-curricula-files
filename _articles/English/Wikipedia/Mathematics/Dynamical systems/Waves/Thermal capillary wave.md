---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_capillary_wave
offline_file: ""
offline_thumbnail: ""
uuid: 1545fe91-30c8-4343-9f57-1c8604b16f8c
updated: 1484308900
title: Thermal capillary wave
categories:
    - Waves
---
Thermal motion is able to produce capillary waves at the molecular scale. At this scale, gravity and hydrodynamics can be neglected, and only the surface tension contribution is relevant.
