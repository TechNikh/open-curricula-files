---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inhalation
offline_file: ""
offline_thumbnail: ""
uuid: c8563b1b-68d1-4b59-824b-05efd2879d5c
updated: 1484310323
title: inhalation
categories:
    - Dictionary
---
inhalation
     n 1: the act of inhaling; the drawing in of air (or other gases)
          as in breathing [syn: {inspiration}, {aspiration}, {breathing
          in}]
     2: a medication to be taken by inhaling it [syn: {inhalant}]
