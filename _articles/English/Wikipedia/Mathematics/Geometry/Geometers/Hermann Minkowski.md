---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hermann_Minkowski
offline_file: ""
offline_thumbnail: ""
uuid: 8c3a972a-26d3-4ec5-9b81-2e8fdbe6da8f
updated: 1484309093
title: Hermann Minkowski
tags:
    - Personal life and family
    - Education and career
    - Work on relativity
    - Publications
    - Notes
categories:
    - Geometers
---
Hermann Minkowski (22 June 1864 – 12 January 1909) was a Jewish German mathematician, professor at Königsberg, Zürich and Göttingen. He created and developed the geometry of numbers and used geometrical methods to solve problems in number theory, mathematical physics, and the theory of relativity.
