---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bled
offline_file: ""
offline_thumbnail: ""
uuid: d37b2c7a-f18f-4031-a9af-d6ffddc24ee7
updated: 1484310559
title: bled
categories:
    - Dictionary
---
bleed
     v 1: lose blood from one's body [syn: {shed blood}, {hemorrhage}]
     2: draw blood; "In the old days, doctors routinely bled
        patients as part of the treatment" [syn: {leech}, {phlebotomize},
         {phlebotomise}]
     3: get or extort (money or other possessions) from someone;
        "They bled me dry--I have nothing left!"
     4: be diffused; "These dyes and colors are guaranteed not to
        run" [syn: {run}]
     5: drain of liquid or steam; "bleed the radiators"; "the
        mechanic bled the engine"
     [also: {bled}]
