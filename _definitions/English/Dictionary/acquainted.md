---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acquainted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484515861
title: acquainted
categories:
    - Dictionary
---
acquainted
     adj : having fair knowledge of; "they were acquainted"; "fully
           acquainted with the facts" [syn: {acquainted(p)}, {acquainted
           with(p)}, {familiar with(p)}]
