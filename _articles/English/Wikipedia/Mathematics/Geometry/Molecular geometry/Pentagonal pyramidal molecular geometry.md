---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Pentagonal_pyramidal_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 82d37609-5f81-4470-b1ec-08ae95feece7
updated: 1484309279
title: Pentagonal pyramidal molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pentagonal-pyramidal-3D-balls.png
categories:
    - Molecular geometry
---
In chemistry, pentagonal pyramidal molecular geometry describes the shape of compounds where in six atoms or groups of atoms or ligands are arranged around a central atom, defining the vertices of a pentagonal pyramid. It is one of the few molecular geometries with uneven bond angles.
