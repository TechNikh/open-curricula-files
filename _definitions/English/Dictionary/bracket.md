---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bracket
offline_file: ""
offline_thumbnail: ""
uuid: 0171c5d1-b1ae-4863-afc7-7cae151d4033
updated: 1484310459
title: bracket
categories:
    - Dictionary
---
bracket
     n 1: a category falling within certain defined limits
     2: either of two punctuation marks ([ or ]) used to enclose
        textual material [syn: {square bracket}]
     3: either of two punctuation marks (`<' or `>') sometimes used
        to enclose textual material [syn: {angle bracket}]
     4: an L-shaped support projecting from a wall (as to hold a
        shelf)
     v 1: support with brackets; "bracket bookshelves"
     2: place into brackets; "Please bracket this remark" [syn: {bracket
        out}]
     3: classify or group
