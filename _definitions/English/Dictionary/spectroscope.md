---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spectroscope
offline_file: ""
offline_thumbnail: ""
uuid: c564ac24-ff8e-4d63-b681-34ca4f22b3b0
updated: 1484310391
title: spectroscope
categories:
    - Dictionary
---
spectroscope
     n : an optical instrument for spectrographic analysis [syn: {prism
         spectroscope}]
