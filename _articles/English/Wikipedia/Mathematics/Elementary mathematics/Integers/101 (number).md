---
version: 1
type: article
id: https://en.wikipedia.org/wiki/101_(number)
offline_file: ""
offline_thumbnail: ""
uuid: b53f7029-5f31-48d5-9ec7-209dfc469cf5
updated: 1484308970
title: 101 (number)
tags:
    - In mathematics
    - In science
    - In books
    - In education
    - In other fields
categories:
    - Integers
---
101 (one hundred [and] one) is the natural number following 100 and preceding 102.
It is variously pronounced "one hundred and one" / "a hundred and one", "one hundred one" / "a hundred one", and "one oh one". As an ordinal number, 101st (one hundred [and] first), rather than 101th, is the correct form.
