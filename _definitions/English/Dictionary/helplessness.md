---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/helplessness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484436241
title: helplessness
categories:
    - Dictionary
---
helplessness
     n 1: powerlessness revealed by an inability to act; "in spite of
          their weakness the group remains highly active" [syn: {weakness},
           {impuissance}]
     2: the state of needing help from something
     3: a feeling of being unable to manage
