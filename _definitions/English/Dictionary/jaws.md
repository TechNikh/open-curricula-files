---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jaws
offline_file: ""
offline_thumbnail: ""
uuid: 6b65e304-0a5c-41d5-addd-bd7c976bad9f
updated: 1484310273
title: jaws
categories:
    - Dictionary
---
jaws
     n : holding device consisting of one or both of the opposing
         parts of a tool that close to hold an object [syn: {jaw}]
