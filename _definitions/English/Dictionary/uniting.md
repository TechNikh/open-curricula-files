---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uniting
offline_file: ""
offline_thumbnail: ""
uuid: 24d504d1-2f49-4c6d-997f-a6131e27b51e
updated: 1484310603
title: uniting
categories:
    - Dictionary
---
uniting
     n 1: the combination of two or more commercial companies [syn: {amalgamation},
           {merger}]
     2: the act of making or becoming a single unit; "the union of
        opposing factions"; "he looked forward to the unification
        of his family for the holidays" [syn: {union}, {unification},
         {conjugation}, {jointure}] [ant: {disunion}]
