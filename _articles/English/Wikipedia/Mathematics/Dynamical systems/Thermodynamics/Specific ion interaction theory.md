---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Specific_ion_interaction_theory
offline_file: ""
offline_thumbnail: ""
uuid: 309897ba-0efe-48e9-b0b4-eb7d1b83f967
updated: 1484308864
title: Specific ion interaction theory
tags:
    - Background
    - Development
    - Determination and application
categories:
    - Thermodynamics
---
Specific ion Interaction Theory (SIT theory) is a theory used to estimate single-ion activity coefficients in electrolyte solutions at relatively high concentrations.[1][2] It does so by taking into consideration interaction coefficients between the various ions present in solution. Interaction coefficients are determined from equilibrium constant values obtained with solutions at various ionic strengths. The determination of SIT interaction coefficients also yields the value of the equilibrium constant at infinite dilution.
