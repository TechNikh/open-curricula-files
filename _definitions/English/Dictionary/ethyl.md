---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethyl
offline_file: ""
offline_thumbnail: ""
uuid: 5c9d35a8-dca6-4244-a782-78e6be6d20e4
updated: 1484310421
title: ethyl
categories:
    - Dictionary
---
ethyl
     n : the univalent hydrocarbon radical C2H5 derived from ethane
         by the removal of one hydrogen atom [syn: {ethyl group},
         {ethyl radical}]
