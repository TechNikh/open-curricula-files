---
version: 1
type: video
provider: YouTube
id: Rm-xMzXMiPU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c2953ea6-68c5-47be-9052-cce82aff751e
updated: 1486069623
title: 'Multiply Numbers In 90s Or 100s In Your Head - Math Trick'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Rm-xMzXMiPU/default.jpg
    - https://i3.ytimg.com/vi/Rm-xMzXMiPU/1.jpg
    - https://i3.ytimg.com/vi/Rm-xMzXMiPU/2.jpg
    - https://i3.ytimg.com/vi/Rm-xMzXMiPU/3.jpg
---
