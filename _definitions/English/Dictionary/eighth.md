---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eighth
offline_file: ""
offline_thumbnail: ""
uuid: d441d679-d3fb-4a0f-a7d3-ac40ca525e6a
updated: 1484310397
title: eighth
categories:
    - Dictionary
---
eighth
     adj : coming next after the seventh and just before the ninth in
           position [syn: {8th}]
     n 1: position eight in a countable series of things
     2: an eighth part [syn: {one-eighth}]
