---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Centipede_game
offline_file: ""
offline_thumbnail: ""
uuid: 0151fa19-2c6a-4ec4-a4c4-67933d1738bc
updated: 1484309050
title: Centipede game
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/370px-Centipede_game.png
tags:
    - Play
    - Equilibrium analysis and backward induction
    - Empirical results
    - Explanations
    - Significance
categories:
    - Non-cooperative games
---
In game theory, the centipede game, first introduced by Robert Rosenthal in 1981, is an extensive form game in which two players take turns choosing either to take a slightly larger share of an increasing pot, or to pass the pot to the other player. The payoffs are arranged so that if one passes the pot to one's opponent and the opponent takes the pot on the next round, one receives slightly less than if one had taken the pot on this round. Although the traditional centipede game had a limit of 100 rounds (hence the name), any game with this structure but a different number of rounds is called a centipede game.
