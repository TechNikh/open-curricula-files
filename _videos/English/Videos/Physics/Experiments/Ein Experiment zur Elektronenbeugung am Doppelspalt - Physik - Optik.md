---
version: 1
type: video
provider: YouTube
id: Otr6wp9v6J0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9631e5ec-b150-48ca-83f8-b632b2f4380a
updated: 1486069639
title: >
    Ein Experiment zur Elektronenbeugung am Doppelspalt | Physik
    | Optik
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/Otr6wp9v6J0/default.jpg
    - https://i3.ytimg.com/vi/Otr6wp9v6J0/1.jpg
    - https://i3.ytimg.com/vi/Otr6wp9v6J0/2.jpg
    - https://i3.ytimg.com/vi/Otr6wp9v6J0/3.jpg
---
