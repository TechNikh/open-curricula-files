---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spectacles
offline_file: ""
offline_thumbnail: ""
uuid: 97e69ff5-27b2-4731-b104-a4b72a3d26cf
updated: 1484310208
title: spectacles
categories:
    - Dictionary
---
spectacles
     n : optical instrument consisting of a pair of lenses for
         correcting defective vision [syn: {specs}, {eyeglasses},
         {glasses}]
