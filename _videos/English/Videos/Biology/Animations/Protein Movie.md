---
version: 1
type: video
provider: YouTube
id: iaHHgEoa2c8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Protein%20Movie.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 53aea0b8-ef23-4964-a1f8-2cd762ceb073
updated: 1486069673
title: Protein Movie
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/iaHHgEoa2c8/default.jpg
    - https://i3.ytimg.com/vi/iaHHgEoa2c8/1.jpg
    - https://i3.ytimg.com/vi/iaHHgEoa2c8/2.jpg
    - https://i3.ytimg.com/vi/iaHHgEoa2c8/3.jpg
---
