---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Patent_drawing
offline_file: ""
offline_thumbnail: ""
uuid: 1abe1f3f-0953-4fa5-8333-a9e1206e7da0
updated: 1484309329
title: Patent drawing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Patent_model_for_loom.jpg
tags:
    - Jurisdictions
    - Europe
    - Patent Cooperation Treaty
    - United States
    - History
    - Drawings and photographs
    - features
    - Views
    - References and notes
categories:
    - Technical drawing
---
A patent application or patent may contain drawings, also called patent drawings, illustrating the invention, some of its embodiments (which are particular implementations or methods of carrying out the invention), or the prior art. The drawings may be required by the law to be in a particular form, and the requirements may vary depending on the jurisdiction.
