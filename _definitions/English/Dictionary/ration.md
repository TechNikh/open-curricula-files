---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ration
offline_file: ""
offline_thumbnail: ""
uuid: 823960df-95ba-4e33-b999-0858068e1bfe
updated: 1484310447
title: ration
categories:
    - Dictionary
---
ration
     n 1: the food allowance for one day (especially for service
          personnel); "the rations should be nutritionally
          balanced"
     2: a fixed portion that is allotted (especially in times of
        scarcity)
     v 1: restrict the consumption of a relatively scarce commodity,
          as during war; "Bread was rationed during the siege of
          the city"
     2: distribute in rations, as in the army; "Cigarettes are
        rationed" [syn: {ration out}]
