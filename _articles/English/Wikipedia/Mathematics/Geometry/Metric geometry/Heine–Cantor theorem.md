---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Heine%E2%80%93Cantor_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: f1f8815c-a630-444b-967e-837450c0c665
updated: 1484309266
title: Heine–Cantor theorem
categories:
    - Metric geometry
---
In mathematics, the Heine–Cantor theorem, named after Eduard Heine and Georg Cantor, states that if f : M → N is a continuous function between two metric spaces, and M is compact, then f is uniformly continuous. An important special case is that every continuous function from a bounded closed interval to the real numbers is uniformly continuous.
