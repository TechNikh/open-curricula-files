---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/passenger
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484323561
title: passenger
categories:
    - Dictionary
---
passenger
     n : a traveler riding in a vehicle (a boat or bus or car or
         plane or train etc) who is not operating it [syn: {rider}]
