---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Knaster%E2%80%93Tarski_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: e52a491d-af3a-40a6-91f5-6a720e403e23
updated: 1484308791
title: Knaster–Tarski theorem
tags:
    - 'Consequences: least and greatest fixed points'
    - Weaker versions of the theorem
    - Proof
    - Notes
categories:
    - Fixed points (mathematics)
---
In the mathematical areas of order and lattice theory, the Knaster–Tarski theorem, named after Bronisław Knaster and Alfred Tarski, states the following:
