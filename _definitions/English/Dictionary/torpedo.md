---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/torpedo
offline_file: ""
offline_thumbnail: ""
uuid: 099bbb88-ef0d-4d19-9449-1bf962c1e3f3
updated: 1484310553
title: torpedo
categories:
    - Dictionary
---
torpedo
     n 1: a professional killer who uses a gun [syn: {gunman}, {gunslinger},
           {hired gun}, {gun}, {gun for hire}, {triggerman}, {hit
          man}, {hitman}, {shooter}]
     2: a large sandwich made of a long crusty roll split lengthwise
        and filled with meats and cheese (and tomato and onion and
        lettuce and condiments); different names are used in
        different sections of the United States [syn: {bomber}, {grinder},
         {hero}, {hero sandwich}, {hoagie}, {hoagy}, {Cuban
        sandwich}, {Italian sandwich}, {poor boy}, {sub}, {submarine},
         {submarine sandwich}, {wedge}, {zep}]
     3: an explosive device that is set off in an oil well ...
