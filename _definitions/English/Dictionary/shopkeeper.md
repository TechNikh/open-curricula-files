---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shopkeeper
offline_file: ""
offline_thumbnail: ""
uuid: 62e30ef4-4b03-435c-842b-1ee2ead14ca6
updated: 1484310395
title: shopkeeper
categories:
    - Dictionary
---
shopkeeper
     n : a merchant who owns or manages a shop [syn: {tradesman}, {storekeeper},
          {market keeper}]
