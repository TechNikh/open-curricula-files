---
version: 1
type: video
provider: YouTube
id: aTftyFboC_M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Natural%20Selection%20-%20Crash%20Course%20Biology%20%2314.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fdcee34e-57f0-4965-a009-0ba17599a889
updated: 1486069678
title: 'Natural Selection - Crash Course Biology #14'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/aTftyFboC_M/default.jpg
    - https://i3.ytimg.com/vi/aTftyFboC_M/1.jpg
    - https://i3.ytimg.com/vi/aTftyFboC_M/2.jpg
    - https://i3.ytimg.com/vi/aTftyFboC_M/3.jpg
---
