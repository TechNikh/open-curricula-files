---
version: 1
type: video
provider: YouTube
id: vB4FuGmfEGY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Events%20and%20Outcomes%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 75b15b41-1987-4e89-82a0-b3976a7265d2
updated: 1486069565
title: Events and Outcomes 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/vB4FuGmfEGY/default.jpg
    - https://i3.ytimg.com/vi/vB4FuGmfEGY/1.jpg
    - https://i3.ytimg.com/vi/vB4FuGmfEGY/2.jpg
    - https://i3.ytimg.com/vi/vB4FuGmfEGY/3.jpg
---
