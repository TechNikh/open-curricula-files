---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/socio-economic
offline_file: ""
offline_thumbnail: ""
uuid: efd0a3ff-67ab-4a3c-b4b9-77c8be9bb026
updated: 1484310262
title: socio-economic
categories:
    - Dictionary
---
socioeconomic
     adj : involving social as well as economic factors; "socioeconomic
           status"
