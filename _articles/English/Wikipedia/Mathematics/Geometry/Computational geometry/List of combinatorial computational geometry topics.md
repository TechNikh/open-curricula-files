---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_combinatorial_computational_geometry_topics
offline_file: ""
offline_thumbnail: ""
uuid: 9941b072-9c2b-41ac-87c5-06df1b2bc1b7
updated: 1484309175
title: List of combinatorial computational geometry topics
tags:
    - Construction/representation
    - Extremal shapes
    - Interaction/search
    - Proximity problems
    - Visibility
    - Other
categories:
    - Computational geometry
---
List of combinatorial computational geometry topics enumerates the topics of computational geometry that states problems in terms of geometric objects as discrete entities and hence the methods of their solution are mostly theories and algorithms of combinatorial character.
