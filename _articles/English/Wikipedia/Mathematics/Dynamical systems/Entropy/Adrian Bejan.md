---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adrian_Bejan
offline_file: ""
offline_thumbnail: ""
uuid: 044a13a3-21cd-4785-b0ae-819104d3b71c
updated: 1484308764
title: Adrian Bejan
categories:
    - Entropy
---
Adrian Bejan is an American professor who developed modern thermodynamics and the constructal law of design and evolution in nature. He is J. A. Jones Distinguished Professor of Mechanical Engineering at Duke University[1] and author of the 2016 book The Physics of Life.[2]
