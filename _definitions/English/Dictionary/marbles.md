---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marbles
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484320801
title: marbles
categories:
    - Dictionary
---
marbles
     n 1: a children's game played with little balls made of a hard
          substance (as glass)
     2: the basic human power of intelligent thought and perception;
        "he used his wits to get ahead"; "I was scared out of my
        wits"; "he still had all his marbles and was in full
        possession of a lively mind" [syn: {wits}]
