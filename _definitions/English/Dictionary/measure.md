---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/measure
offline_file: ""
offline_thumbnail: ""
uuid: 61302363-713f-4d6d-9c63-776a51f76903
updated: 1484310268
title: measure
categories:
    - Dictionary
---
measure
     n 1: the act or process of measuring; "the measurements were
          carefully done"; "his mental measurings proved
          remarkably accurate" [syn: {measurement}, {measuring}, {mensuration}]
     2: a basis for comparison; a reference point against which
        other things can be evaluated; "they set the measure for
        all subsequent work" [syn: {standard}, {criterion}, {touchstone}]
     3: how much there is of something that you can quantify [syn: {quantity},
         {amount}]
     4: any maneuver made as part of progress toward a goal; "the
        situation called for strong measures"; "the police took
        steps to reduce crime" [syn: {step}]
     5: a ...
