---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bisimulation
offline_file: ""
offline_thumbnail: ""
uuid: 6e647552-f3ba-42cb-ab64-8ac6dd11f2d2
updated: 1484308674
title: Bisimulation
tags:
    - Formal definition
    - Alternative definitions
    - Relational definition
    - Fixpoint definition
    - Game theoretical definition
    - Coalgebraic definition
    - Variants of bisimulation
    - Bisimulation and modal logic
    - Software tools
categories:
    - Theoretical computer science
---
In theoretical computer science a bisimulation is a binary relation between state transition systems, associating systems that behave in the same way in the sense that one system simulates the other and vice versa.
