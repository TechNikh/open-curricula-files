---
version: 1
type: video
provider: YouTube
id: YdXuSftZELQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 13c13a57-bc3d-49f5-95ad-dff840e82df0
updated: 1486069662
title: 'How To Memorize The Periodic Table - Easiest Way Possible (Video 1)'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/YdXuSftZELQ/default.jpg
    - https://i3.ytimg.com/vi/YdXuSftZELQ/1.jpg
    - https://i3.ytimg.com/vi/YdXuSftZELQ/2.jpg
    - https://i3.ytimg.com/vi/YdXuSftZELQ/3.jpg
---
