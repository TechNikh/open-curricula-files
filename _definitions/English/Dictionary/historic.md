---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/historic
offline_file: ""
offline_thumbnail: ""
uuid: 33707322-f09f-4014-add5-090a568a2874
updated: 1484310429
title: historic
categories:
    - Dictionary
---
historic
     adj 1: belonging to the past; of what is important or famous in the
            past; "historic victories"; "historical (or historic)
            times"; "a historical character" [syn: {historical}]
     2: important in history; "the historic first voyage to outer
        space"
