---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/workable
offline_file: ""
offline_thumbnail: ""
uuid: 817e0a8b-4dbc-4777-b4dd-ea97d18a2e4e
updated: 1484310589
title: workable
categories:
    - Dictionary
---
workable
     adj : capable of being done with means at hand and circumstances
           as they are [syn: {feasible}, {executable}, {practicable},
            {viable}]
