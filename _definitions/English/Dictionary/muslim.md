---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/muslim
offline_file: ""
offline_thumbnail: ""
uuid: ad209b7e-2ff9-486e-90ae-11fb0fe7e9df
updated: 1484310583
title: muslim
categories:
    - Dictionary
---
Muslim
     adj : of or relating to or supporting Islamism; "Islamic art"
           [syn: {Moslem}, {Islamic}]
     n : a believer or follower of Islam [syn: {Moslem}, {Mohammedan},
          {Muhammedan}, {Muhammadan}, {Islamist}]
