---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nikolai_V._Ivanov
offline_file: ""
offline_thumbnail: ""
uuid: 89507d49-9c1f-45bb-9ac7-d3eeca80843f
updated: 1484309088
title: Nikolai V. Ivanov
categories:
    - Geometers
---
Nikolai V. Ivanov (in Russian: Николай В. Иванов, born in 1954) is a Russian mathematician who works on topology, geometry and group theory (particularly, modular Teichmüller groups).[1] He is a professor at Michigan State University.[2]
