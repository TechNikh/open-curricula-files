---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/3x_%2B_1_semigroup'
offline_file: ""
offline_thumbnail: ""
uuid: 5972edc3-9ab7-4679-9613-99d27c21109b
updated: 1484308544
title: 3x + 1 semigroup
tags:
    - Definition
    - The weak Collatz conjecture
    - The wild semigroup
categories:
    - Number theory
---
In algebra, the 3x + 1 semigroup is a special subsemigroup of the multiplicative semigroup of all positive rational numbers.[1] The elements of a generating set of this semigroup are related to the sequence of numbers involved in the yet to be proved conjecture known as the Collatz conjecture or the "3x + 1 problem". The 3x + 1 semigroup has been used to prove a weaker form of the Collatz conjecture. In fact, it was in such context the concept of the 3x + 1 semigroup was introduced by H. Farkas in 2005.[2] Various generalizations of the 3x + 1 semigroup have been constructed and their properties have been investigated.[3]
