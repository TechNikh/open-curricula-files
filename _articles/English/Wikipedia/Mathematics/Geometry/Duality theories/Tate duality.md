---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tate_duality
offline_file: ""
offline_thumbnail: ""
uuid: 60c6b037-f02a-43d7-a3e3-037fc17023bb
updated: 1484309239
title: Tate duality
categories:
    - Duality theories
---
In mathematics, Tate duality or Poitou–Tate duality is a duality theorem for Galois cohomology groups of modules over the Galois group of an algebraic number field or local field, introduced by Tate (1962) and Poitou (1967).
