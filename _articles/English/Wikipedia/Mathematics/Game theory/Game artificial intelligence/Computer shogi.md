---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computer_shogi
offline_file: ""
offline_thumbnail: ""
uuid: 341dbf2f-9f5d-4e79-bd6f-e172cf68ea70
updated: 1484309006
title: Computer shogi
tags:
    - Game complexity
    - Components
    - Computers versus humans
    - Bonanza versus Watanabe (2007)
    - Annual CSA tournament exhibition games (2003–2009)
    - Akara versus Shimizu (2010)
    - >
        Computers Bonanza and Akara beat amateurs Kosaku and Shinoda
        (2011)
    - Bonkras versus Yonenaga (2011–2012)
    - Denou-sen (2013)
    - Miura versus GPS Shogi
    - Funae versus Tsutsukana (revenge match)
    - Denou-sen 3 (2014)
    - Computer restrictions
    - Yashiki versus Ponanza
    - 'Denou-sen 3.1: Sugai versus Shueso (revenge match)'
    - Programmer tools
    - Shogidokoro
    - WinBoard/XBoard and BCMShogi
    - Floodgate
    - World Computer Shogi Championship
    - Video game systems
    - Restrictions
    - Milestones
categories:
    - Game artificial intelligence
---
Computer shogi is a field of artificial intelligence concerned with the creation of computer programs which can play shogi. The research and development of shogi software has been carried out mainly by freelance programmers, university research groups and private companies. The strongest programs can outperform most human players and play at a level comparable to professional players.
