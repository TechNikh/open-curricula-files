---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Uncertainty_exponent
offline_file: ""
offline_thumbnail: ""
uuid: 14b73948-aa78-4dff-ac19-05d4a4377d8a
updated: 1484308713
title: Uncertainty exponent
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_3.png
categories:
    - Chaos theory
---
In mathematics, the uncertainty exponent is a method of measuring the fractal dimension of a basin boundary. In a chaotic scattering system, the invariant set of the system is usually not directly accessible because it is non-attracting and typically of measure zero. Therefore, the only way to infer the presence of members and to measure the properties of the invariant set is through the basins of attraction. Note that in a scattering system, basins of attraction are not limit cycles therefore do not constitute members of the invariant set.
