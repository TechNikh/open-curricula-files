---
version: 1
type: video
provider: YouTube
id: jEY9Bie92aM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/The%20Fluid%20Mosaic%20Model%20of%20the%20Cell%20Membrane.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bd2c462f-d5b0-45b3-a7b2-4134aa40b445
updated: 1486069665
title: The Fluid Mosaic Model of the Cell Membrane
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/jEY9Bie92aM/default.jpg
    - https://i3.ytimg.com/vi/jEY9Bie92aM/1.jpg
    - https://i3.ytimg.com/vi/jEY9Bie92aM/2.jpg
    - https://i3.ytimg.com/vi/jEY9Bie92aM/3.jpg
---
