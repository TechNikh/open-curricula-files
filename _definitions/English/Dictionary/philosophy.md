---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/philosophy
offline_file: ""
offline_thumbnail: ""
uuid: 1c71be8b-87ae-4385-a5fc-565cce035afd
updated: 1484310569
title: philosophy
categories:
    - Dictionary
---
philosophy
     n 1: a belief (or system of beliefs) accepted as authoritative by
          some group or school [syn: {doctrine}, {philosophical
          system}, {school of thought}, {ism}]
     2: the rational investigation of questions about existence and
        knowledge and ethics
     3: any personal belief about how to live or how to deal with a
        situation; "self-indulgence was his only philosophy"; "my
        father's philosophy of child-rearing was to let mother do
        it"
