---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/triple
offline_file: ""
offline_thumbnail: ""
uuid: c3c1ab53-9a0b-45d2-bbe9-0cd4b72a87f9
updated: 1484310405
title: triple
categories:
    - Dictionary
---
triple
     adj 1: having three units or components or elements; "a ternary
            operation"; "a treble row of red beads"; "overcrowding
            made triple sessions necessary"; "triple time has
            three beats per measure"; "triplex windows" [syn: {ternary},
             {treble}, {triplex}]
     2: three times as great or many; "a claim for treble (or
        triple) damages"; "a threefold increase" [syn: {treble}, {threefold}]
     n 1: a base hit at which the batter stops safely at third base
          [syn: {three-base hit}, {three-bagger}]
     2: a quantity that is three times as great as another
     v 1: increase threefold; "Triple your income!" [syn: {treble}]
   ...
