---
version: 1
type: video
provider: YouTube
id: qIpuq8Y0Jvw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Ballistic%20pendulum%20HD.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 492fc552-291a-4381-867e-8225207a7bab
updated: 1486069653
title: Ballistic pendulum HD
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/qIpuq8Y0Jvw/default.jpg
    - https://i3.ytimg.com/vi/qIpuq8Y0Jvw/1.jpg
    - https://i3.ytimg.com/vi/qIpuq8Y0Jvw/2.jpg
    - https://i3.ytimg.com/vi/qIpuq8Y0Jvw/3.jpg
---
