---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Jensen%27s_covering_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 888cf6b0-0fd7-405e-9e68-c75215c3da1c
updated: 1484309506
title: "Jensen's covering theorem"
categories:
    - Set theory
---
In set theory, Jensen's covering theorem states that if 0# does not exist then every uncountable set of ordinals is contained in a constructible set of the same cardinality. Informally this conclusion says that the constructible universe is close to the universe of all sets. The first proof appeared in (Devlin & Jensen 1975). Silver later gave a fine structure free proof using his machines and finally Magidor (1990) gave an even simpler proof.
