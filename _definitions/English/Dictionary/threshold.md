---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/threshold
offline_file: ""
offline_thumbnail: ""
uuid: 5dc4c6f8-77b2-4fa3-9ffd-c64c2994ac3f
updated: 1484310140
title: threshold
categories:
    - Dictionary
---
threshold
     n 1: the starting point for a new state or experience; "on the
          threshold of manhood"
     2: the smallest detectable sensation [syn: {limen}]
     3: the entrance (the space in a wall) through which you enter
        or leave a room or building; the space that a door can
        close; "he stuck his head in the doorway" [syn: {doorway},
         {door}, {room access}]
     4: the sill of a door; a horizontal piece of wood or stone that
        forms the bottom of a doorway and offer support when
        passing through a doorway [syn: {doorsill}, {doorstep}]
     5: a region marking a boundary [syn: {brink}, {verge}]
