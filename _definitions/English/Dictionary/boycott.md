---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boycott
offline_file: ""
offline_thumbnail: ""
uuid: e3af11cd-625c-4028-abb0-609f19663416
updated: 1484310172
title: boycott
categories:
    - Dictionary
---
boycott
     n : a group's refusal to have commercial dealings with some
         organization in protest against its policies
     v : refuse to sponsor; refuse to do business with [ant: {patronize},
          {patronize}]
