---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drinking
offline_file: ""
offline_thumbnail: ""
uuid: 096609e8-6e9c-41a9-8a0d-2ff44b75eb71
updated: 1484310198
title: drinking
categories:
    - Dictionary
---
drinking
     n 1: the act of consuming liquids [syn: {imbibing}, {imbibition}]
     2: the act of drinking alcoholic beverages to excess; "drink
        was his downfall" [syn: {drink}, {boozing}, {drunkenness},
         {crapulence}]
