---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Boxicity
offline_file: ""
offline_thumbnail: ""
uuid: 7b2927f0-9056-4784-bad6-97a736406b4d
updated: 1484309242
title: Boxicity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Boxicity.svg.png
tags:
    - Examples
    - Relation to other graph classes
    - Algorithmic results
    - Notes
categories:
    - Geometric graph theory
---
The boxicity of a graph is the minimum dimension in which a given graph can be represented as an intersection graph of axis-parallel boxes. That is, there must exist a one-to-one correspondence between the vertices of the graph and a set of boxes, such that two boxes intersect if and only if there is an edge connecting the corresponding vertices.
