---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_embedding
offline_file: ""
offline_thumbnail: ""
uuid: cef95716-ec96-46f7-b7b8-6dabb464c574
updated: 1484309481
title: Graph embedding
tags:
    - Terminology
    - Combinatorial embedding
    - Computational complexity
    - Embeddings of graphs into higher-dimensional spaces
categories:
    - Topological graph theory
---
In topological graph theory, an embedding (also spelled imbedding) of a graph 
  
    
      
        G
      
    
    {\displaystyle G}
  
 on a surface Σ is a representation of 
  
    
      
        G
      
    
    {\displaystyle G}
  
 on Σ in which points of Σ are associated to vertices and simple arcs (homeomorphic images of [0,1]) are associated to edges in such a way that:
