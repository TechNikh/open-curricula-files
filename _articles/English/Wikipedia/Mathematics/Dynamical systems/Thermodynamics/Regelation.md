---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Regelation
offline_file: ""
offline_thumbnail: ""
uuid: d499ee3c-f494-430e-9d69-47248d1f4ea1
updated: 1484308853
title: Regelation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Melting_curve_of_water.jpg
tags:
    - Surface Melting
    - Examples of Regelation
    - Misconceptions
    - Latest Progress
categories:
    - Thermodynamics
---
Regelation is the phenomenon of melting under pressure and freezing again when the pressure is reduced. Many sources state that regelation can be demonstrated by looping a fine wire around a block of ice, with a heavy weight attached to it. The pressure exerted on the ice slowly melts it locally, permitting the wire to pass through the entire block. The wire's track will refill as soon as pressure is relieved, so the ice block will remain solid even after wire passes completely through. This experiment is possible for ice at −10 °C or cooler, and while essentially valid, the details of the process by which the wire passes through the ice are complex.[1] The phenomenon works best with ...
