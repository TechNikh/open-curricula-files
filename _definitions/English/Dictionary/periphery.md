---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/periphery
offline_file: ""
offline_thumbnail: ""
uuid: b94b960b-a5da-4a8f-afa4-cfc88a4d5cc4
updated: 1484310597
title: periphery
categories:
    - Dictionary
---
periphery
     n : the outside boundary or surface of something [syn: {fringe},
          {outer boundary}]
