---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hypertension
offline_file: ""
offline_thumbnail: ""
uuid: d8b3397f-bffc-4d00-9509-e94fe1628736
updated: 1484310163
title: hypertension
categories:
    - Dictionary
---
hypertension
     n : a common disorder in which blood pressure remains abnormally
         high (a reading of 140/90 mm Hg or greater) [syn: {high
         blood pressure}] [ant: {hypotension}]
