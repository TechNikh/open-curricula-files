---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/king
offline_file: ""
offline_thumbnail: ""
uuid: 9c3d2a44-3db5-4ccd-aefb-74f2c9510659
updated: 1484310593
title: king
categories:
    - Dictionary
---
king
     n 1: a male sovereign; ruler of a kingdom [syn: {male monarch}]
          [ant: {queen}, {queen}]
     2: a competitor who holds a preeminent position [syn: {queen},
        {world-beater}]
     3: a very wealthy or powerful businessman; "an oil baron" [syn:
         {baron}, {big businessman}, {business leader}, {magnate},
         {mogul}, {power}, {top executive}, {tycoon}]
     4: preeminence in a particular category or group or field; "the
        lion is the king of beasts"
     5: United States woman tennis player (born in 1943) [syn: {Billie
        Jean King}, {Billie Jean Moffitt King}]
     6: United States guitar player and singer of the blues (born in
        1925) ...
