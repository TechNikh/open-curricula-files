---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/so
offline_file: ""
offline_thumbnail: ""
uuid: 753344b1-a48b-4a11-bd6c-aea02862cae5
updated: 1484310344
title: so
categories:
    - Dictionary
---
so
     adj 1: conforming to truth; "I wouldn't have told you this if it
            weren't so"; "a truthful statement" [syn: {so(p)}, {truthful}]
     2: marked by system; in good order; "everything is in order";
        "his books are always just so"; "things must be exactly
        so" [syn: {in order(p)}, {so(p)}]
     n : the syllable naming the fifth (dominant) note of any musical
         scale in solmization [syn: {sol}, {soh}]
     adv 1: to a very great extent or degree; "the idea is so obvious";
            "never been so happy"; "I love you so"; "my head aches
            so!"
     2: in order that; "he stooped down so he could pick up his hat"
     3: in such a condition or ...
