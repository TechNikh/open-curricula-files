---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Born_reciprocity
offline_file: ""
offline_thumbnail: ""
uuid: 6c571c7a-22cd-444b-9434-c46d37985e26
updated: 1484309232
title: Born reciprocity
categories:
    - Duality theories
---
In physics, Born reciprocity, also called reciprocal relativity or Born–Green reciprocity, is a principle set up by theoretical physicist Max Born that calls for a duality-symmetry among space and momentum. Born and his co-workers expanded his principle to a framework that is also known as reciprocity theory.[1][2]
