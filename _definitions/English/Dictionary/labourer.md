---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/labourer
offline_file: ""
offline_thumbnail: ""
uuid: 94b2cc71-3358-44d1-8f29-cacd3e910265
updated: 1484310471
title: labourer
categories:
    - Dictionary
---
labourer
     n : someone who works with their hands; someone engaged in
         manual labor [syn: {laborer}, {manual laborer}, {jack}]
