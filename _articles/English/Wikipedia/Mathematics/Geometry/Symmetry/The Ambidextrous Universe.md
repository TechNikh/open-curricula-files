---
version: 1
type: article
id: https://en.wikipedia.org/wiki/The_Ambidextrous_Universe
offline_file: ""
offline_thumbnail: ""
uuid: 3deac7c2-6317-4c71-9806-15a722b163f1
updated: 1484309305
title: The Ambidextrous Universe
tags:
    - Content
    - The Ozma Problem
    - Literary references
    - W.H. Auden
    - Vladimir Nabokov
categories:
    - Symmetry
---
The Ambidextrous Universe is a popular science book by Martin Gardner covering aspects of symmetry and asymmetry in human culture, science and the wider universe.
