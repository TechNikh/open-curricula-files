---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/womanhood
offline_file: ""
offline_thumbnail: ""
uuid: f12fa566-6b0d-4ca8-a043-eab1c77ebe10
updated: 1484310575
title: womanhood
categories:
    - Dictionary
---
womanhood
     n 1: the state of being an adult woman [syn: {muliebrity}]
     2: women as a class; "it's an insult to American womanhood";
        "woman is the glory of creation" [syn: {woman}]
     3: the status of a woman
