---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aromatic
offline_file: ""
offline_thumbnail: ""
uuid: 4ce0299d-ecc1-4e06-af28-2b9361178443
updated: 1484310423
title: aromatic
categories:
    - Dictionary
---
aromatic
     adj 1: (chemistry) of or relating to or containing one or more
            benzene rings; "an aromatic organic compound"
     2: having a strong distinctive fragrance; "the pine woods were
        more redolent"- Jean Stafford [syn: {redolent}]
