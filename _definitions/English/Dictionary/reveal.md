---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reveal
offline_file: ""
offline_thumbnail: ""
uuid: c3e851a3-3bed-42ab-9799-b4312b1a82e5
updated: 1484310419
title: reveal
categories:
    - Dictionary
---
reveal
     v 1: make visible; "Summer brings out bright clothes"; "He brings
          out the best in her"; "The newspaper uncovered the
          President's illegal dealings" [syn: {uncover}, {bring
          out}, {unveil}]
     2: make known to the public information that was previously
        known only to a few people or that was meant to be kept a
        secret; "The auction house would not disclose the price at
        which the van Gogh had sold"; "The actress won't reveal
        how old she is"; "bring out the truth"; "he broke the news
        to her" [syn: {disclose}, {let on}, {bring out}, {discover},
         {expose}, {divulge}, {impart}, {break}, {give away}, {let
      ...
