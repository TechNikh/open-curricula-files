---
version: 1
type: video
provider: YouTube
id: fr-8tjLoeDw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/2011%20Calculus%20AB%20Free%20Response%20%236a.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d61af29c-2995-42c4-9d43-2399276fe97b
updated: 1486069692
title: '2011 Calculus AB Free Response #6a'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/fr-8tjLoeDw/default.jpg
    - https://i3.ytimg.com/vi/fr-8tjLoeDw/1.jpg
    - https://i3.ytimg.com/vi/fr-8tjLoeDw/2.jpg
    - https://i3.ytimg.com/vi/fr-8tjLoeDw/3.jpg
---
