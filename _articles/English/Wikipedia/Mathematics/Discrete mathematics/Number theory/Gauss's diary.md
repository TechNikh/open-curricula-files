---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gauss%27s_diary'
offline_file: ""
offline_thumbnail: ""
uuid: 88f55443-81cf-426c-9762-0c35e0ebe3f5
updated: 1484308645
title: "Gauss's diary"
categories:
    - Number theory
---
Gauss's diary was a record of the mathematical discoveries of C. F. Gauss from 1796 to 1814. It was rediscovered in 1897 and published by Klein (1903), and reprinted in volume X1 of his collected works and in (Gauss 2005). There is an English translation with commentary given by Gray (1984), reprinted in the second edition of (Dunnington 2004).
