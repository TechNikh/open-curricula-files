---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/external
offline_file: ""
offline_thumbnail: ""
uuid: 589c93c2-34ba-4f23-b4c9-e1a8a82b2f4d
updated: 1484310328
title: external
categories:
    - Dictionary
---
external
     adj 1: happening or arising or located outside or beyond some
            limits or especially surface; "the external auditory
            canal"; "external pressures" [ant: {internal}]
     2: coming from the outside; "extraneous light in the camera
        spoiled the photograph"; "relying upon an extraneous
        income"; "disdaining outside pressure groups" [syn: {extraneous},
         {outside}]
     3: from or between other countries; "external commerce";
        "international trade"; "developing nations need outside
        help" [syn: {international}, {outside(a)}]
     4: purely outward or superficial; "external composure"; "an
        external concern for ...
