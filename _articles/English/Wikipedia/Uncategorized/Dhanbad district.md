---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mohuda
offline_file: ""
offline_thumbnail: ""
uuid: d183fbf0-fef2-4477-b578-7d8de99fe06b
updated: 1484308451
title: Dhanbad district
tags:
    - Economy
    - Demographics
categories:
    - Uncategorized
---
Dhanbad district is one of the twenty-four districts of Jharkhand state, India, and Dhanbad is the administrative headquarters of this district. As of 2011 it is the second most populous district of Jharkhand (out of 24).[1]
