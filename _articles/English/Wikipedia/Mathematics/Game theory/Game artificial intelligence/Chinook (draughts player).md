---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chinook_(draughts_player)
offline_file: ""
offline_thumbnail: ""
uuid: 6488ca26-e0d2-4399-9dfd-6e99589bfc08
updated: 1484309002
title: Chinook (draughts player)
tags:
    - Man vs. Machine World Champion
    - Algorithm
    - Timeline
categories:
    - Game artificial intelligence
---
Chinook is a computer program that plays checkers (also known as draughts). It was developed 1989–2007 at the University of Alberta, by a team led by Jonathan Schaeffer and consisting of Rob Lake, Paul Lu, Martin Bryant, and Norman Treloar.
