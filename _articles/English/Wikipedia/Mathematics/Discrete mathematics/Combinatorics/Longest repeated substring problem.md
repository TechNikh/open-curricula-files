---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Longest_repeated_substring_problem
offline_file: ""
offline_thumbnail: ""
uuid: 09715c1c-26e8-4030-b0a1-0b3304490e10
updated: 1484308573
title: Longest repeated substring problem
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-ATCGATCGA%2524_Suffix_Tree.png'
categories:
    - Combinatorics
---
This problem can be solved in linear time and space [ Θ(n) ] by building a suffix tree for the string, and finding the deepest internal node in the tree. Depth is measured by the number of characters traversed from the root. The string spelled by the edges from the root to such a node is a longest repeated substring. The problem of finding the longest substring with at least 
  
    
      
        k
      
    
    {\displaystyle k}
  
 occurrences can be solved by first preprocessing the tree to count the number of leaf descendants for each internal node, and then finding the deepest node with at least 
  
    
      
        k
      
    
    {\displaystyle k}
  
 leaf descendants that ...
