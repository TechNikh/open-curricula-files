---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Blossom_tree_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: e2fc4486-b6d8-484f-962c-38381d7aebfd
updated: 1484309440
title: Blossom tree (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Blossom_tree.png
tags:
    - Description
    - Relationship with planar graphs
    - Use in knot theory
categories:
    - Graph theory objects
---
In the study of planar graphs, blossom trees are trees with additional directed half edges. Each blossom tree is associated with an embedding of a planar graph. Blossom trees can be used to sample random planar graphs.[1]
