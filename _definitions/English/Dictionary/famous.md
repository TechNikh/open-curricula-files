---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/famous
offline_file: ""
offline_thumbnail: ""
uuid: 75bce5a9-3b4a-4734-9e98-add48df17b61
updated: 1484310290
title: famous
categories:
    - Dictionary
---
famous
     adj : widely known and esteemed; "a famous actor"; "a celebrated
           musician"; "a famed scientist"; "an illustrious judge";
           "a notable historian"; "a renowned painter" [syn: {celebrated},
            {famed}, {far-famed}, {illustrious}, {notable}, {noted},
            {renowned}]
