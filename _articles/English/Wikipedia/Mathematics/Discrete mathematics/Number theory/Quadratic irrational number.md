---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quadratic_irrational_number
offline_file: ""
offline_thumbnail: ""
uuid: 055528f4-750e-428d-bb08-3df74205518e
updated: 1484308664
title: Quadratic irrational number
tags:
    - Square root of non-square is irrational
categories:
    - Number theory
---
In mathematics, a quadratic irrational number (also known as a quadratic irrational, a quadratic irrationality or quadratic surd) is an irrational number that is the solution to some quadratic equation with rational coefficients which is irreducible over the set of rational numbers.[1] Since fractions in the coefficients of a quadratic equation can be cleared by multiplying both sides by their common denominator, a quadratic irrational is an irrational root of some quadratic equation whose coefficients are integers. The quadratic irrational numbers, a subset of the complex numbers, are algebraic numbers of degree 2, and can therefore be expressed as
