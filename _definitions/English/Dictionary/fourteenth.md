---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fourteenth
offline_file: ""
offline_thumbnail: ""
uuid: e465c395-0a4e-4d30-80ce-663e11c5e1a1
updated: 1484310413
title: fourteenth
categories:
    - Dictionary
---
fourteenth
     adj : coming next after the thirteenth in position [syn: {14th}]
     n : position 14 in a countable series of things
