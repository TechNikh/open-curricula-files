---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exclusive
offline_file: ""
offline_thumbnail: ""
uuid: 8b1d915a-9dc3-40ba-a208-c43b2e9dd72e
updated: 1484310181
title: exclusive
categories:
    - Dictionary
---
exclusive
     adj 1: not divided or shared with others; "they have exclusive use
            of the machine"; "sole rights of publication" [syn: {sole(a)}]
     2: excluding much or all; especially all but a particular group
        or minority; "exclusive clubs"; "an exclusive restaurants
        and shops" [ant: {inclusive}]
     3: not divided among or brought to bear on more than one object
        or objective; "judging a contest with a single eye"; "a
        single devotion to duty"; "undivided affection"; "gained
        their exclusive attention" [syn: {single(a)}, {undivided}]
     n : a news report that is reported first by one news
         organization; "he got a scoop on the ...
