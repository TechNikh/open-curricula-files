---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thickness
offline_file: ""
offline_thumbnail: ""
uuid: 06c7e531-6630-4cb2-aa99-fbda990eb154
updated: 1484310212
title: thickness
categories:
    - Dictionary
---
thickness
     n 1: the dimension through an object as opposed to its length or
          width [ant: {thinness}]
     2: used of a line or mark [syn: {heaviness}]
     3: resistance to flow [ant: {thinness}]
