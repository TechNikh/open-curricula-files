---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_algebra
offline_file: ""
offline_thumbnail: ""
uuid: 7fcb7a36-8531-48b4-921f-a1a1a2b210d9
updated: 1484308614
title: Graph algebra
tags:
    - Definition
    - Applications
categories:
    - Graph theory
---
In mathematics, especially in the fields of universal algebra and graph theory, a graph algebra is a way of giving a directed graph an algebraic structure. It was introduced in (McNulty & Shallon 1983), and has seen many uses in the field of universal algebra since then.
