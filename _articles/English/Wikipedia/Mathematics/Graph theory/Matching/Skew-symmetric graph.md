---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Skew-symmetric_graph
offline_file: ""
offline_thumbnail: ""
uuid: 9340e0fa-22d3-4b15-bebe-a118138f29eb
updated: 1484309429
title: Skew-symmetric graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Implication_graph.svg.png
tags:
    - Definition
    - Examples
    - >
        Polar/switch graphs, double covering graphs, and bidirected
        graphs
    - Matching
    - Still life theory
    - Satisfiability
    - Recognition
categories:
    - Matching
---
In graph theory, a branch of mathematics, a skew-symmetric graph is a directed graph that is isomorphic to its own transpose graph, the graph formed by reversing all of its edges, under an isomorphism that is an involution without any fixed points. Skew-symmetric graphs are identical to the double covering graphs of bidirected graphs.
