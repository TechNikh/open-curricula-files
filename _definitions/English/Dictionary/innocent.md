---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/innocent
offline_file: ""
offline_thumbnail: ""
uuid: 3c0352b6-4cfa-45f4-8849-b6858cca1468
updated: 1484310565
title: innocent
categories:
    - Dictionary
---
innocent
     adj 1: free from evil or guilt; "an innocent child"; "the principle
            that one is innocent until proved guilty" [syn: {guiltless},
             {clean-handed}] [ant: {guilty}]
     2: lacking intent or capacity to injure; "an innocent prank"
        [syn: {innocuous}]
     3: free from sin [syn: {impeccant}, {sinless}]
     4: lacking in sophistication or worldliness; "a child's
        innocent stare"; "his ingenuous explanation that he would
        not have burned the church if he had not thought the
        bishop was in it" [syn: {ingenuous}]
     5: used of things; lacking sense or awareness; "ignorant hope";
        "fine innocent weather" [syn: {ignorant}]
   ...
