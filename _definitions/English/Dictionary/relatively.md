---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relatively
offline_file: ""
offline_thumbnail: ""
uuid: d4e9bc44-2567-4ef1-bd4e-2b447c0ac7e0
updated: 1484310273
title: relatively
categories:
    - Dictionary
---
relatively
     adv : in a relative manner; by comparison to something else; "the
           situation is relatively calm now" [syn: {comparatively}]
