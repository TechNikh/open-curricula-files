---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enjoy
offline_file: ""
offline_thumbnail: ""
uuid: abe09af0-39c4-4680-9843-8684370a99eb
updated: 1484310391
title: enjoy
categories:
    - Dictionary
---
enjoy
     v 1: derive or receive pleasure from; get enjoyment from; take
          pleasure in; "She relished her fame and basked in her
          glory" [syn: {bask}, {relish}, {savor}, {savour}]
     2: have benefit from; "enjoy privileges"
     3: get pleasure from; "I love cooking" [syn: {love}]
     4: have for one's benefit; "The industry enjoyed a boom" [ant:
        {suffer}]
     5: take delight in; "he delights in his granddaughter" [syn: {delight},
         {revel}]
