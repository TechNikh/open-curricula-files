---
version: 1
type: video
provider: YouTube
id: jN2YUT_PnD0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 997e0c07-0982-4adc-aab3-8d29ef559be0
updated: 1486069616
title: Explicit and recursive definitions of sequences
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/jN2YUT_PnD0/default.jpg
    - https://i3.ytimg.com/vi/jN2YUT_PnD0/1.jpg
    - https://i3.ytimg.com/vi/jN2YUT_PnD0/2.jpg
    - https://i3.ytimg.com/vi/jN2YUT_PnD0/3.jpg
---
