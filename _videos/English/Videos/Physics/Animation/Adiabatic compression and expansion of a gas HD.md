---
version: 1
type: video
provider: YouTube
id: dQeCEqkE9eE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Adiabatic%20compression%20and%20expansion%20of%20a%20gas%20HD.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0d83cec0-0077-4eb4-ba21-f257c84463b5
updated: 1486069652
title: Adiabatic compression and expansion of a gas HD
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/dQeCEqkE9eE/default.jpg
    - https://i3.ytimg.com/vi/dQeCEqkE9eE/1.jpg
    - https://i3.ytimg.com/vi/dQeCEqkE9eE/2.jpg
    - https://i3.ytimg.com/vi/dQeCEqkE9eE/3.jpg
---
