---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/speaking
offline_file: ""
offline_thumbnail: ""
uuid: c806ffdc-3abb-4059-847e-4ebfaf73868b
updated: 1484310525
title: speaking
categories:
    - Dictionary
---
speaking
     adj 1: capable of or involving speech or speaking; "human
            beings--the speaking animals"; "a speaking part in the
            play" [syn: {speaking(a)}] [ant: {nonspeaking}]
     2: capable of speech; "the speaking animal" [syn: {speaking(a)},
         {speech-endowed}]
     n 1: the utterance of intelligible speech [syn: {speech
          production}]
     2: delivering an address to a public audience; "people came to
        see the candidates and hear the speechmaking" [syn: {public
        speaking}, {speechmaking}, {oral presentation}]
