---
version: 1
type: video
provider: YouTube
id: CQYELiTtUs8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/More%20on%20Newton%27s%20first%20law%20of%20motion.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 65a6ace3-7109-47c7-84c9-f534e23f87e5
updated: 1486069597
title: "More on Newton's first law of motion"
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/CQYELiTtUs8/default.jpg
    - https://i3.ytimg.com/vi/CQYELiTtUs8/1.jpg
    - https://i3.ytimg.com/vi/CQYELiTtUs8/2.jpg
    - https://i3.ytimg.com/vi/CQYELiTtUs8/3.jpg
---
Newton's First Law (Galileo's Law of Inertia). Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/forces-newtons-laws/newtons-laws-of-motion/v/newton-s-second-law-of-motion?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/forces-newtons-laws/newtons-laws-of-motion/v/newton-s-first-law-of-motion-concepts?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
