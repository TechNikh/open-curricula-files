---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faded
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484452861
title: faded
categories:
    - Dictionary
---
faded
     adj 1: having lost freshness or brilliance of color; "sun-bleached
            deck chairs"; "faded jeans"; "a very pale washed-out
            blue"; "washy colors" [syn: {bleached}, {washed-out},
            {washy}]
     2: reduced in strength; "the faded tones of an old recording"
        [syn: {attenuate}, {attenuated}, {weakened}]
