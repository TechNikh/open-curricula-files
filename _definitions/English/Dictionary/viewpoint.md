---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/viewpoint
offline_file: ""
offline_thumbnail: ""
uuid: 81f75584-208a-4f40-be5c-2c420dbcbe1b
updated: 1484310172
title: viewpoint
categories:
    - Dictionary
---
viewpoint
     n 1: a mental position from which things are viewed; "we should
          consider this problem from the viewpoint of the
          Russians"; "teaching history gave him a special point of
          view toward current events" [syn: {point of view}, {stand},
           {standpoint}]
     2: a place from which something can be viewed; "from that
        vantage point he could survey the whole valley" [syn: {vantage
        point}]
