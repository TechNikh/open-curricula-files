---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/product
offline_file: ""
offline_thumbnail: ""
uuid: 8a1da96d-6b6e-49fc-be86-bace08bc8b34
updated: 1484310202
title: product
categories:
    - Dictionary
---
product
     n 1: commodities offered for sale; "good business depends on
          having good merchandise"; "that store offers a variety
          of products" [syn: {merchandise}, {wares}]
     2: an artifact that has been created by someone or some
        process; "they improve their product every year"; "they
        export most of their agricultural production" [syn: {production}]
     3: a consequence of someone's efforts or of a particular set of
        circumstances; "skill is the product of hours of
        practice"; "his reaction was the product of hunger and
        fatigue"
     4: a chemical substance formed as a result of a chemical
        reaction; "a product of lime and ...
