---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/N-dimensional_sequential_move_puzzle
offline_file: ""
offline_thumbnail: ""
uuid: 4afe625d-3d54-433b-a045-e282d879311f
updated: 1484309289
title: N-dimensional sequential move puzzle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-5-cube_2x2x2x2x2.png
tags:
    - Glossary
    - Magic 4D Cube
    - 34 4-cube
    - 24 4-cube
    - 44 4-cube
    - 54 4-cube
    - Magic 5D Cube
    - 35 5-cube
    - 25 5-cube
    - 45 5-cube
    - 55 5-cube
    - 65 5-cube
    - 75 5-cube
    - Magic 7D Cube
    - Magic 120-cell
    - 3x3 2D square
    - 1D projection
categories:
    - Multi-dimensional geometry
---
The Rubik's Cube is the original and best known of the three-dimensional sequential move puzzles. There have been many virtual implementations of this puzzle in software. It is a natural extension to create sequential move puzzles in more than three dimensions. Although no such puzzle could ever be physically constructed, the rules of how they operate are quite rigorously defined mathematically and are analogous to the rules found in three-dimensional geometry. Hence, they can be simulated by software. As with the mechanical sequential move puzzles, there are records for solvers, although not yet the same degree of competitive organisation.
