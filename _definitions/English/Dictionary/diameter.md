---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diameter
offline_file: ""
offline_thumbnail: ""
uuid: 4fff0c3a-e5b9-495c-83e8-27303bac22d4
updated: 1484310210
title: diameter
categories:
    - Dictionary
---
diameter
     n 1: the length of a straight line passing through the center of
          a circle and connecting two points on the circumference
          [syn: {diam}]
     2: a straight line connecting the center of a circle with two
        points on its perimeter (or the center of a sphere with
        two points on its surface)
