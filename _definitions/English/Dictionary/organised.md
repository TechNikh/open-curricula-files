---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/organised
offline_file: ""
offline_thumbnail: ""
uuid: 58ec664f-8eb4-4cf3-91dc-990b0608dd7f
updated: 1484310395
title: organised
categories:
    - Dictionary
---
organised
     adj : being a member of or formed into a labor union; "organized
           labor"; "unionized workers"; "a unionized shop" [syn: {organized},
            {unionized}, {unionised}]
