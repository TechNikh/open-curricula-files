---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/buddhism
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484567161
title: buddhism
categories:
    - Dictionary
---
Buddhism
     n 1: a religion represented by the many groups (especially in
          Asia) that profess various forms of the Buddhist
          doctrine and that venerate Buddha
     2: the teaching of Buddha that life is permeated with suffering
        caused by desire, that suffering ceases when desire
        ceases, and that enlightenment obtained through right
        conduct and wisdom and meditation releases one from desire
        and suffering and rebirth
