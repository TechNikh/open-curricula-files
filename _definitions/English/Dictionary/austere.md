---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/austere
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484617981
title: austere
categories:
    - Dictionary
---
austere
     adj 1: severely simple; "a stark interior" [syn: {severe}, {stark}]
     2: of a stern or strict bearing or demeanor; forbidding in
        aspect; "an austere expression"; "a stern face" [syn: {stern}]
     3: practicing great self-denial; "Be systematically
        ascetic...do...something for no other reason than that you
        would rather not do it"- William James; "a desert nomad's
        austere life"; "a spartan diet"; "a spartan existence"
        [syn: {ascetic}, {ascetical}, {spartan}]
