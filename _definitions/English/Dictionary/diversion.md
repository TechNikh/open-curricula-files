---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diversion
offline_file: ""
offline_thumbnail: ""
uuid: 754f28f0-a614-490d-b784-a629fd3e3da0
updated: 1484310557
title: diversion
categories:
    - Dictionary
---
diversion
     n 1: an activity that diverts or amuses or stimulates; "scuba
          diving is provided as a diversion for tourists"; "for
          recreation he wrote poetry and solved crossword
          puzzles"; "drug abuse is often regarded as a form of
          recreation" [syn: {recreation}]
     2: a turning aside (of your course or attention or concern); "a
        diversion from the main highway"; "a digression into
        irrelevant details"; "a deflection from his goal" [syn: {deviation},
         {digression}, {deflection}, {deflexion}, {divagation}]
     3: an attack calculated to draw enemy defense away from the
        point of the principal attack [syn: {diversionary ...
