---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/courageous
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484412961
title: courageous
categories:
    - Dictionary
---
courageous
     adj : possessing or displaying courage; able to face and deal with
           danger or fear without flinching; "Familiarity with
           danger makes a brave man braver but less daring"-
           Herman Melville; "a frank courageous heart...triumphed
           over pain"- William Wordsworth; "set a courageous
           example by leading them safely into and out of
           enemy-held territory" [syn: {brave}, {fearless}] [ant:
           {cowardly}]
