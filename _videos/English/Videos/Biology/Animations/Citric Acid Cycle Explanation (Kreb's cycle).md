---
version: 1
type: video
provider: YouTube
id: jsMNyGbKxqk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Citric%20Acid%20Cycle%20Explanation%20%28Kreb%27s%20cycle%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b68bcd47-a92a-4bcd-b46b-415ddcc7db4c
updated: 1486069666
title: "Citric Acid Cycle Explanation (Kreb's cycle)"
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/jsMNyGbKxqk/default.jpg
    - https://i3.ytimg.com/vi/jsMNyGbKxqk/1.jpg
    - https://i3.ytimg.com/vi/jsMNyGbKxqk/2.jpg
    - https://i3.ytimg.com/vi/jsMNyGbKxqk/3.jpg
---
