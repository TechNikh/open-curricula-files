---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equivalent
offline_file: ""
offline_thumbnail: ""
uuid: c66fa0bb-8e2c-4846-aaa9-32e15bfaac8d
updated: 1484310200
title: equivalent
categories:
    - Dictionary
---
equivalent
     adj 1: equal in amount or value; "like amounts"; "equivalent
            amounts"; "the same amount"; "gave one six blows and
            the other a like number"; "an equal number"; "the same
            number" [syn: {like}, {equal}, {same}] [ant: {unlike}]
     2: being essentially equal to something; "it was as good as
        gold"; "a wish that was equivalent to a command"; "his
        statement was tantamount to an admission of guilt" [syn: {tantamount(p)}]
     n 1: a person or thing equal to another in value or measure or
          force or effect or significance etc; "send two dollars
          or the equivalent in stamps"
     2: the atomic weight of an element ...
