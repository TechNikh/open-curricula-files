---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cardinal_direction
offline_file: ""
offline_thumbnail: ""
uuid: d3c19b82-5b81-4f16-8bf9-d9ab85020e05
updated: 1484309295
title: Cardinal direction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Brosen_windrose.svg.png
tags:
    - Locating the directions
    - Direction versus bearing
    - Magnetic compass
    - The Sun
    - Watch face
    - Sundial
    - Astronomy
    - Gyrocompass
    - Satellite navigation
    - Additional points
    - Usefulness of cardinal points
    - Beyond geography
    - Germanic origin of names
    - Cardinal directions in world cultures
    - Arabic language
    - Far East
    - Americas
    - Australia
    - Unique (non-compound) names of intercardinal directions
    - Non-compass directional systems
    - Notes
categories:
    - Orientation (geometry)
---
The four cardinal directions or cardinal points are the directions of north, east, south, and west, commonly denoted by their initials: N, E, S, W. East and west are at right angles to north and south, with east being in the clockwise direction of rotation from north and west being directly opposite east. Intermediate points between the four cardinal directions form the points of the compass. The intermediate (intercardinal, or ordinal) directions are northeast (NE), southeast (SE), southwest (SW), and northwest (NW). Further, the intermediate direction of every set of intercardinal and cardinal direction is called a secondary-intercardinal direction, the eight shortest points in the ...
