---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dimensional_operator
offline_file: ""
offline_thumbnail: ""
uuid: 284c4f05-aeb0-4dfd-935b-cc5c1c394a3e
updated: 1484309504
title: Dimensional operator
categories:
    - Set theory
---
If the power set of E is denoted P(E) then a dimensional operator on E is a map
