---
version: 1
type: article
id: https://en.wikipedia.org/wiki/3D_mirror_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 4989fb55-9184-4a79-8f78-f0639acfd818
updated: 1484309304
title: 3D mirror symmetry
categories:
    - Symmetry
---
In theoretical physics, 3D mirror symmetry is a version of mirror symmetry in 3-dimensional gauge theories with N=4 supersymmetry, or 8 supercharges. It was first proposed by Kenneth Intriligator and Nathan Seiberg in their 1996 paper, Mirror symmetry in three-dimensional gauge theories[1], as a relation between pairs of 3-dimensional gauge theories, such that the Coulomb branch of the moduli space of one is the Higgs branch of the moduli space of the other. It was demonstrated using D-brane cartoons by Amihay Hanany and Edward Witten 4 months later,[2] where they found that it is a consequence of S-duality in type IIB string theory.
