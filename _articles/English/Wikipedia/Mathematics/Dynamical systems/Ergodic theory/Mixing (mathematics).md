---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mixing_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: ca91251f-4649-479c-9ec7-d618adeaa57e
updated: 1484308780
title: Mixing (mathematics)
tags:
    - Mixing in stochastic processes
    - Types of mixing
    - Mixing in dynamical systems
    - |
        L
        
        2
        
        
        
        
        {\displaystyle L^{2}}
        
        formulation
    - Products of dynamical systems
    - Generalizations
    - Examples
    - Topological mixing
categories:
    - Ergodic theory
---
In mathematics, mixing is an abstract concept originating from physics: the attempt to describe the irreversible thermodynamic process of mixing in the everyday world: mixing paint, mixing drinks, etc.
