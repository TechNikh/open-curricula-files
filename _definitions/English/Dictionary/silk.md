---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silk
offline_file: ""
offline_thumbnail: ""
uuid: 0555d834-6490-493c-bebd-824c71486dba
updated: 1484310416
title: silk
categories:
    - Dictionary
---
silk
     n 1: a fabric made from the fine threads produced by certain
          insect larvae
     2: fibers from silkworm cocoons provide threads for knitting
