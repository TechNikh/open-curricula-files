---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Paris%E2%80%93Harrington_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 73489bfe-263b-403f-b03f-f345e4de0c92
updated: 1484308999
title: Paris–Harrington theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/357px-Eiffel_tower.svg.png
tags:
    - The strengthened finite Ramsey theorem
    - The Paris–Harrington theorem
categories:
    - Theorems in the foundations of mathematics
---
In mathematical logic, the Paris–Harrington theorem states that a certain combinatorial principle in Ramsey theory, namely the strengthened finite Ramsey theorem, is true, but not provable in Peano arithmetic. This was the first "natural" example of a true statement about the integers that could be stated in the language of arithmetic, but not proved in Peano arithmetic; it was already known that such statements existed by Gödel's first incompleteness theorem.
