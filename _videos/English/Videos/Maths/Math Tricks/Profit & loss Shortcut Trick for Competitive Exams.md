---
version: 1
type: video
provider: YouTube
id: xJ_-tgrqDjk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 91a2912e-16ea-4a02-9fff-7aa6cd82d7ed
updated: 1486069622
title: 'Profit & loss Shortcut Trick for Competitive Exams'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/xJ_-tgrqDjk/default.jpg
    - https://i3.ytimg.com/vi/xJ_-tgrqDjk/1.jpg
    - https://i3.ytimg.com/vi/xJ_-tgrqDjk/2.jpg
    - https://i3.ytimg.com/vi/xJ_-tgrqDjk/3.jpg
---
