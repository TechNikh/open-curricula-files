---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mine
offline_file: ""
offline_thumbnail: ""
uuid: 10f14bdf-f885-4387-8b70-6bb91e51401f
updated: 1484310146
title: mine
categories:
    - Dictionary
---
mine
     n 1: excavation in the earth from which ores and minerals are
          extracted
     2: explosive device that explodes on contact; designed to
        destroy vehicles or ships or to kill or maim personnel
     v 1: get from the earth by excavation; "mine ores and metals"
     2: lay mines; "The Vietnamese mined Cambodia"
