---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Courcelle%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: fe758509-a887-40af-9bd7-52093eb8e3f6
updated: 1484309411
title: "Courcelle's theorem"
tags:
    - Formulations
    - Vertex sets
    - Edge sets
    - Modular congruences
    - Decision versus optimization
    - Space complexity
    - Proof strategy and complexity
    - "Courcelle's conjecture"
    - "Satisfiability and Seese's theorem"
    - Applications
categories:
    - Graph minor theory
---
In the study of graph algorithms, Courcelle's theorem is the statement that every graph property definable in the monadic second-order logic of graphs can be decided in linear time on graphs of bounded treewidth.[1][2][3] The result was first proved by Bruno Courcelle in 1990[4] and independently rediscovered by Borie, Parker & Tovey (1992).[5] It is considered the archetype of algorithmic meta-theorems.[6][7]
