---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/latitude
offline_file: ""
offline_thumbnail: ""
uuid: 1c4f3d51-7d8a-42d6-9e11-b2ea6842412a
updated: 1484310431
title: latitude
categories:
    - Dictionary
---
latitude
     n 1: the angular distance between an imaginary line around a
          heavenly body parallel to its equator and the equator
          itself
     2: freedom from normal restraints in conduct; "the new freedom
        in movies and novels"; "allowed his children considerable
        latitude in how they spent their money"
     3: an imaginary line around the Earth parallel to the equator
        [syn: {line of latitude}, {parallel of latitude}, {parallel}]
     4: scope for freedom of e.g. action or thought; freedom from
        restriction
