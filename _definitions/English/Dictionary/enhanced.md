---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enhanced
offline_file: ""
offline_thumbnail: ""
uuid: 816940d4-12a6-4555-948f-e6fb5f99b943
updated: 1484310567
title: enhanced
categories:
    - Dictionary
---
enhanced
     adj : increased or intensified in value or beauty or quality; "her
           enhanced beauty was the result of a good night's sleep
           rather than makeup"; "careful cleaning was responsible
           for the enhanced value of the painting"
