---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concluded
offline_file: ""
offline_thumbnail: ""
uuid: dfff9ae8-1e55-4b4f-8101-d111eb0e34fa
updated: 1484310311
title: concluded
categories:
    - Dictionary
---
concluded
     adj : having come or been brought to a conclusion; "the harvesting
           was complete"; "the affair is over, ended, finished";
           "the abruptly terminated interview" [syn: {complete}, {ended},
            {over(p)}, {all over}, {terminated}]
