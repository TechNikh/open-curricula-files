---
version: 1
type: video
provider: YouTube
id: TfYf_rPWUdY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ddcd537c-1112-41b3-9787-9c20d8d21db9
updated: 1486069670
title: mRNA Translation (Advanced)
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/TfYf_rPWUdY/default.jpg
    - https://i3.ytimg.com/vi/TfYf_rPWUdY/1.jpg
    - https://i3.ytimg.com/vi/TfYf_rPWUdY/2.jpg
    - https://i3.ytimg.com/vi/TfYf_rPWUdY/3.jpg
---
