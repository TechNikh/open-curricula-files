---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exterior_covariant_derivative
offline_file: ""
offline_thumbnail: ""
uuid: 2a999a4f-75c0-4f9b-89be-fafaf3fe2dba
updated: 1484309218
title: Exterior covariant derivative
tags:
    - Definition
    - Exterior covariant derivative for vector bundles
    - Examples
    - Notes
categories:
    - Differential geometry
---
