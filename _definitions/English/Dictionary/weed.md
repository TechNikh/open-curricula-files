---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weed
offline_file: ""
offline_thumbnail: ""
uuid: c2f84cc8-4834-45aa-a7e1-a3b203948cd2
updated: 1484310545
title: weed
categories:
    - Dictionary
---
weed
     n 1: any plant that crowds out cultivated plants [ant: {cultivated
          plant}]
     2: street names for marijuana [syn: {pot}, {grass}, {green
        goddess}, {dope}, {gage}, {sess}, {sens}, {smoke}, {skunk},
         {locoweed}, {Mary Jane}]
     v : clear of weeds; "weed the garden"
