---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Interval_edge_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 40f37638-31d0-41a4-9188-72464994e99e
updated: 1484309384
title: Interval edge coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Interval_5-coloring_of_complete_graph_on_6_verices.jpg
tags:
    - History
    - Definition
    - A few results
    - 'Interval edge coloring of complete graph [7]'
    - Interval edge coloring of bipatite graphs
    - 'Interval edge coloring of Planar graphs[9]'
    - >
        Interval edge coloring of biregular bipartite graphs with
        small vertex degrees
    - 'Interval edge coloring of Grid graphs [11]'
    - Example
    - Equitable K-interval edge coloring
    - Applications
    - Conjectures
categories:
    - Graph coloring
---
Interval edge coloring is a type of edge coloring in which edges are colored such that they form a set of intervals and each color in the interval set is at least used once to color the edges of the graph.It is to be noted that at each vertex in the graph the colors used form a consecutive set of natural numbers.
