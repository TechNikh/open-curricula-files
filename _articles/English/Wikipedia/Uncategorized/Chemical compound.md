---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chemical_compounds
offline_file: ""
offline_thumbnail: ""
uuid: 1bfd4054-a687-48dd-b1ff-f3da7fddb93a
updated: 1484308375
title: Chemical compound
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-2006-02-13_Drop-impact.jpg
tags:
    - Definitions
    - Elementary concepts
    - Comparison to mixtures
    - Formula
    - Phases and thermal properties
categories:
    - Uncategorized
---
A chemical compound (or just compound if used in the context of chemistry) is an entity consisting of two or more atoms, at least two from different elements, which associate via chemical bonds. There are four types of compounds, depending on how the constituent atoms are held together: molecules held together by covalent bonds, salts held together by ionic bonds, intermetallic compounds held together by metallic bonds, and certain complexes held together by coordinate covalent bonds. Many chemical compounds have a unique numerical identifier assigned by the Chemical Abstracts Service (CAS): its CAS number.
