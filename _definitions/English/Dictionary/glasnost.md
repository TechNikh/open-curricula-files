---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glasnost
offline_file: ""
offline_thumbnail: ""
uuid: 10a25e81-af72-4cc6-a038-b740c35d5ece
updated: 1484310176
title: glasnost
categories:
    - Dictionary
---
glasnost
     n : a policy of the Soviet government allowing freer discussion
         of social problems
