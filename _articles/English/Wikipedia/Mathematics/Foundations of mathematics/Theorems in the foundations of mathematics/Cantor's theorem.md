---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cantor%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 90ac03ab-6729-405b-ba1a-fe6f3a5d5f3f
updated: 1484308987
title: "Cantor's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hasse_diagram_of_powerset_of_3.svg.png
tags:
    - Proof
    - >
        A detailed explanation of the proof when X is countably
        infinite
    - Related paradoxes
    - History
    - Generalizations
    - Other applications
categories:
    - Theorems in the foundations of mathematics
---
In elementary set theory, Cantor's theorem states that, for any set A, the set of all subsets of A (the power set of A) has a strictly greater cardinality than A itself. For finite sets, Cantor's theorem can be seen to be true by a much simpler proof than that given below. Counting the empty subset, subsets of A with just one member, etc. for a set with n members there are 2n subsets and the cardinality of the set of subsets n < 2n is clearly larger. But the theorem is true of infinite sets as well. In particular, the power set of a countably infinite set is uncountably infinite. The theorem is named for German mathematician Georg Cantor, who first stated and proved it.
