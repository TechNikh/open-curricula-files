---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Contractile_vacuole
offline_file: ""
offline_thumbnail: ""
uuid: de2e34e9-4089-44eb-9ff8-a50d93b730ca
updated: 1484308372
title: Contractile vacuole
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Paramecium_contractile_vacuoles.jpg
tags:
    - Overview
    - Water flow into the CV
    - Unresolved issues
categories:
    - Uncategorized
---
A contractile vacuole (CV) is a sub-cellular structure (organelle) involved in osmoregulation. It is found predominantly in protists and in unicellular algae. It was previously known as pulsatile or pulsating vacuole.
