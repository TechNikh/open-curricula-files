---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bowen_ratio
offline_file: ""
offline_thumbnail: ""
uuid: 2f26cb58-1f11-477c-8d31-16ce5d7036e1
updated: 1484308840
title: Bowen ratio
categories:
    - Thermodynamics
---
In meteorology and hydrology, the Bowen ratio is used to describe the type of heat transfer in a water body. Heat transfer can either occur as sensible heat (differences in temperature without evapotranspiration) or latent heat (the energy required during a change of state, without a change in temperature). The Bowen ratio is the mathematical method generally used to calculate heat lost (or gained) in a substance; it is the ratio of energy fluxes from one state to another by sensible heat and latent heating respectively. It is calculated by the equation:
