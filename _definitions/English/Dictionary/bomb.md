---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bomb
offline_file: ""
offline_thumbnail: ""
uuid: 6b73c7dc-c7f5-45ef-beb4-fef15cf740da
updated: 1484310549
title: bomb
categories:
    - Dictionary
---
bomb
     n 1: an explosive device fused to denote under specific
          conditions
     2: strong sealed vessel for measuring heat of combustion [syn:
        {bomb calorimeter}]
     3: an event that fails badly or is totally ineffectual; "the
        first experiment was a real turkey"; "the meeting was a
        dud as far as new business was concerned" [syn: {turkey},
        {dud}]
     v 1: throw bombs at or attack with bombs; "The Americans bombed
          Dresden" [syn: {bombard}]
     2: fail to get a passing grade; "She studied hard but failed
        nevertheless"; "Did I fail the test?" [syn: {fail}, {flunk},
         {flush it}] [ant: {pass}]
