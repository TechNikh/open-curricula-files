---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/second-class
offline_file: ""
offline_thumbnail: ""
uuid: 7fcf87c4-26e5-4327-88aa-834b34946ab2
updated: 1484310189
title: second-class
categories:
    - Dictionary
---
second class
     n 1: not the highest rank in a classification
     2: not the highest rank in a classification
     3: accommodations on a ship or train or plane that are less
        expensive than first class accommodations [syn: {Cabin
        class}]
     adv : by second class conveyance; "we traveled second class"
