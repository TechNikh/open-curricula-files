---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Game-theoretic_rough_sets
offline_file: ""
offline_thumbnail: ""
uuid: f3358792-6041-4b8a-9f74-8d27050f3c8f
updated: 1484309504
title: Game-theoretic rough sets
categories:
    - Set theory
---
The rough sets can be used to induce three-way classification decisions. The positive negative and boundary regions can be interpreted as regions of acceptance, rejection and deferment decisions, respectively. The probabilistic rough set model extends the conventional rough sets by providing more effective way for classifying objects. A main result of probabilistic rough sets is the interpretation of three-way decisions using a pair of probabilistic thresholds. The game-theoretic rough set model determine and interprets the required thresholds by utilizing a game-theoretic environment for analyzing strategic situations between cooperative or conflicting decision making criteria. The ...
