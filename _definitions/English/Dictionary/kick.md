---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kick
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484406241
title: kick
categories:
    - Dictionary
---
kick
     n 1: the act of delivering a blow with the foot; "he gave the
          ball a powerful kick"; "the team's kicking was
          excellent" [syn: {boot}, {kicking}]
     2: the swift release of a store of affective force; "they got a
        great bang out of it"; "what a boot!"; "he got a quick
        rush from injecting heroin"; "he does it for kicks" [syn:
        {bang}, {boot}, {charge}, {rush}, {flush}, {thrill}]
     3: the backward jerk of a gun when it is fired [syn: {recoil}]
     4: informal terms for objecting; "I have a gripe about the
        service here" [syn: {gripe}, {beef}, {bitch}, {squawk}]
     5: the sudden stimulation provided by strong drink (or certain
  ...
