---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stir
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484635801
title: stir
categories:
    - Dictionary
---
stir
     n 1: a disorderly outburst or tumult; "they were amazed by the
          furious disturbance they had caused" [syn: {disturbance},
           {disruption}, {commotion}, {flutter}, {hurly burly}, {to-do},
           {hoo-ha}, {hoo-hah}, {kerfuffle}]
     2: emotional agitation and excitement
     3: a rapid bustling commotion [syn: {bustle}, {hustle}, {flurry},
         {ado}, {fuss}]
     v 1: move an implement through with a circular motion; "stir the
          soup"; "stir my drink"
     2: move very slightly; "He shifted in his seat" [syn: {shift},
        {budge}, {agitate}]
     3: stir feelings in; "stimulate my appetite"; "excite the
        audience"; "stir emotions" [syn: ...
