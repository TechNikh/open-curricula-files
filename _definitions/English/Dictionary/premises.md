---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/premises
offline_file: ""
offline_thumbnail: ""
uuid: 05b29a72-d0d6-4eec-a823-b31237933c3f
updated: 1484310188
title: premises
categories:
    - Dictionary
---
premises
     n : land and buildings together considered as a place of
         business; "bread is baked on the premises"
