---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sterling
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484538781
title: sterling
categories:
    - Dictionary
---
sterling
     adj : highest in quality [syn: {greatest}, {sterling(a)}, {superlative}]
     n : British money; especially the pound sterling as the basic
         monetary unit of the UK
