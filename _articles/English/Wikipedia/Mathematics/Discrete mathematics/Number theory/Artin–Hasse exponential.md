---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Artin%E2%80%93Hasse_exponential'
offline_file: ""
offline_thumbnail: ""
uuid: ae2c340c-63af-4ed4-a23e-b5c36b828bee
updated: 1484308632
title: Artin–Hasse exponential
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_0.jpg
tags:
    - Motivation
    - Combinatorial interpretation
    - Conjectures
categories:
    - Number theory
---
