---
version: 1
type: video
provider: YouTube
id: 8Fdt5WnYn1k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Buffers%2C%20the%20Acid%20Rain%20Slayer-%20Crash%20Course%20Chemistry%20%2331.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5ad1abc4-9675-448b-b9eb-20000aa422f9
updated: 1486069680
title: 'Buffers, the Acid Rain Slayer- Crash Course Chemistry #31'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/8Fdt5WnYn1k/default.jpg
    - https://i3.ytimg.com/vi/8Fdt5WnYn1k/1.jpg
    - https://i3.ytimg.com/vi/8Fdt5WnYn1k/2.jpg
    - https://i3.ytimg.com/vi/8Fdt5WnYn1k/3.jpg
---
