---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/atlas
offline_file: ""
offline_thumbnail: ""
uuid: 2777980e-b95e-4f21-b5c1-3c7a6195ff2b
updated: 1484310431
title: atlas
categories:
    - Dictionary
---
Atlas
     n 1: (Greek mythology) a Titan who was forced by Zeus to bear the
          sky on his shoulders
     2: a collection of maps in book form [syn: {book of maps}, {map
        collection}]
     3: the 1st cervical vertebra [syn: {atlas vertebra}]
     4: a figure of a man used as a supporting column [syn: {telamon}]
