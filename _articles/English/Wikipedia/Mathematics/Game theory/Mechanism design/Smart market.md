---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Smart_market
offline_file: ""
offline_thumbnail: ""
uuid: c28fabb3-386e-4126-884b-72aeabcca495
updated: 1484309040
title: Smart market
categories:
    - Mechanism design
---
A "smart market" is a periodic auction which is cleared by the operations research technique of mathematical optimization, such as linear programming. The smart market is operated by a market manager. Trades are not bilateral, between pairs of people, but rather to or from a pool. A smart market can assist market operation when trades would otherwise have significant transaction costs or externalities.
