---
version: 1
type: video
provider: YouTube
id: leZX0GpV5W0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Torque%2C%20Levers%2C%20and%20the%20Universal%20Law%20of%20Rotation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 580479be-307f-40d9-a1c7-176098448f54
updated: 1486069654
title: Torque, Levers, and the Universal Law of Rotation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/leZX0GpV5W0/default.jpg
    - https://i3.ytimg.com/vi/leZX0GpV5W0/1.jpg
    - https://i3.ytimg.com/vi/leZX0GpV5W0/2.jpg
    - https://i3.ytimg.com/vi/leZX0GpV5W0/3.jpg
---
