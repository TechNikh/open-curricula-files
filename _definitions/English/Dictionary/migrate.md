---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/migrate
offline_file: ""
offline_thumbnail: ""
uuid: b96f66b6-a70c-4cab-8f91-8a65ba181c49
updated: 1484310477
title: migrate
categories:
    - Dictionary
---
migrate
     v 1: move from one country or region to another and settle there;
          "Many Germans migrated to South America in the mid-19th
          century"; "This tribe transmigrated many times over the
          centuries" [syn: {transmigrate}]
     2: move periodically or seasonally; "birds migrate in the
        Winter"; "The worker migrate to where the crops need
        harvesting"
