---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermocole
offline_file: ""
offline_thumbnail: ""
uuid: a71f5a3b-2308-4748-898d-b64eeca88b39
updated: 1484308327
title: Polystyrene
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Polystyrene_stick_trp.png
tags:
    - History
    - Structure
    - Polymerization
    - Atactic polystyrene
    - Syndiotactic polystyrene
    - Degradation
    - Biodegradation
    - Forms produced
    - Sheet or molded polystyrene
    - Foams
    - Expanded polystyrene (EPS)
    - Extruded polystyrene foam
    - Water absorption of polystyrene foams
    - Copolymers
    - Oriented polystyrene
    - Environmental issues
    - Production
    - Non-biodegradable
    - Litter
    - Reducing
    - United States
    - Outside the United States
    - Recycling
    - Incineration
    - Safety
    - Health
    - Fire hazards
    - Bibliography
categories:
    - Uncategorized
---
Polystyrene (PS) /ˌpɒliˈstaɪriːn/ is a synthetic aromatic polymer made from the monomer styrene. Polystyrene can be solid or foamed. General-purpose polystyrene is clear, hard, and rather brittle. It is an inexpensive resin per unit weight. It is a rather poor barrier to oxygen and water vapor and has a relatively low melting point.[4] Polystyrene is one of the most widely used plastics, the scale of its production being several million tonnes per year.[5] Polystyrene can be naturally transparent, but can be colored with colorants. Uses include protective packaging (such as packing peanuts and CD and DVD cases), containers (such as "clamshells"), lids, bottles, trays, tumblers, and ...
