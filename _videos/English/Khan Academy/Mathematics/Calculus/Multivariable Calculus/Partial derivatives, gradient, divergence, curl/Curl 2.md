---
version: 1
type: video
provider: YouTube
id: hTSyVgBa1T0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Partial%20derivatives%2C%20gradient%2C%20divergence%2C%20curl/Curl%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8cf95048-0826-4c92-abae-e3a84a5d866b
updated: 1486069690
title: Curl 2
categories:
    - Partial derivatives, gradient, divergence, curl
thumbnail_urls:
    - https://i3.ytimg.com/vi/hTSyVgBa1T0/default.jpg
    - https://i3.ytimg.com/vi/hTSyVgBa1T0/1.jpg
    - https://i3.ytimg.com/vi/hTSyVgBa1T0/2.jpg
    - https://i3.ytimg.com/vi/hTSyVgBa1T0/3.jpg
---
