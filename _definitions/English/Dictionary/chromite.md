---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chromite
offline_file: ""
offline_thumbnail: ""
uuid: 4c56fc74-a5af-4a1f-9ca5-a652009e4bbe
updated: 1484310547
title: chromite
categories:
    - Dictionary
---
chromite
     n : a brownish-black mineral; the major source of chromium
