---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clay
offline_file: ""
offline_thumbnail: ""
uuid: f714998c-51f8-46ee-b130-4bd9f47cd809
updated: 1484310266
title: clay
categories:
    - Dictionary
---
clay
     n 1: a very fine-grained soil that is plastic when moist but hard
          when fired
     2: water soaked soil; soft wet earth [syn: {mud}]
     3: United States general who commanded United States forces in
        Europe from 1945 to 1949 and who oversaw the Berlin
        airlift (1897-1978) [syn: {Lucius Clay}, {Lucius DuBignon
        Clay}]
     4: United States politician responsible for the Missouri
        Compromise between free and slave states (1777-1852) [syn:
         {Henry Clay}, {the Great Compromiser}]
     5: the dead body of a human being [syn: {cadaver}, {corpse}, {stiff},
         {remains}]
