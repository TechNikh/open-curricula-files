---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/addressed
offline_file: ""
offline_thumbnail: ""
uuid: 43b3e1f8-d981-4c9c-bb5e-d93a172de208
updated: 1484310183
title: addressed
categories:
    - Dictionary
---
addressed
     adj : of mail; marked with a destination; "I throw away all mail
           addressed to `resident'" [ant: {unaddressed}]
