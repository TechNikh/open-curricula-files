---
version: 1
type: video
provider: YouTube
id: sXOIIEZh6qg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Another%20mass%20composition%20problem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 57b8eced-52bd-4756-a627-e7749149d5ae
updated: 1486069587
title: Another mass composition problem
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/sXOIIEZh6qg/default.jpg
    - https://i3.ytimg.com/vi/sXOIIEZh6qg/1.jpg
    - https://i3.ytimg.com/vi/sXOIIEZh6qg/2.jpg
    - https://i3.ytimg.com/vi/sXOIIEZh6qg/3.jpg
---
Another exercise converting a mass composition to an empirical formula. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/types-of-chemical-reactions/v/oxidation-reduction-or-redox-reactions?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/empirical-molecular-formula/v/formula-mass-composition?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
