---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Blue_sky_catastrophe
offline_file: ""
offline_thumbnail: ""
uuid: 214eff61-d27c-41e5-9948-319422c42007
updated: 1484308696
title: Blue sky catastrophe
categories:
    - Bifurcation theory
---
The blue sky catastrophe is a type of bifurcation of a periodic orbit. In other words, it describes a sort of behaviour stable solutions of a set of differential equations can undergo as the equations are gradually changed. This type of bifurcation is characterised by both the period and length of the orbit approaching infinity as the control parameter approaches a finite bifurcation value, but with the orbit still remaining within a bounded part of the phase space, and without loss of stability before the bifurcation point. In other words, the orbit vanishes into the blue sky.
