---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Sprague%E2%80%93Grundy_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: e5224da4-7f2d-4540-ad51-fbd2fb6fb58a
updated: 1484308669
title: Sprague–Grundy theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nim_qc_0.jpg
tags:
    - Definitions
    - First Lemma
    - Second Lemma
    - Proof
    - Development
categories:
    - Theorems in discrete mathematics
---
In combinatorial game theory, the Sprague–Grundy theorem states that every impartial game under the normal play convention is equivalent to a nimber. The Grundy value or nim-value of an impartial game is then defined as the unique nimber that the game is equivalent to. In the case of a game whose positions (or summands of positions) are indexed by the natural numbers (for example the possible heap sizes in nim-like games), the sequence of nimbers for successive heap sizes is called the nim-sequence of the game.
