---
version: 1
type: video
provider: YouTube
id: tTNdR0U6R94
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a4c7dffc-e18e-44da-bac4-a6029e5206c0
updated: 1486069622
title: >
    Square In Your Head The Numbers 34, 334, 3334, etc. and 67,
    667, 6667, etc
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/tTNdR0U6R94/default.jpg
    - https://i3.ytimg.com/vi/tTNdR0U6R94/1.jpg
    - https://i3.ytimg.com/vi/tTNdR0U6R94/2.jpg
    - https://i3.ytimg.com/vi/tTNdR0U6R94/3.jpg
---
