---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/logical
offline_file: ""
offline_thumbnail: ""
uuid: 38390b9b-1ec3-4398-ba27-3982177d91d0
updated: 1484310403
title: logical
categories:
    - Dictionary
---
logical
     adj 1: capable of or reflecting the capability for correct and
            valid reasoning; "a logical mind" [ant: {illogical}]
     2: in accordance with reason or logic; "a logical conclusion"
        [syn: {legitimate}]
     3: marked by an orderly, logical, and aesthetically consistent
        relation of parts; "a logical argument"; "the orderly
        presentation" [syn: {consistent}, {ordered}, {orderly}]
     4: based on known statements or events or conditions; "rain was
        a logical expectation, given the time of year"
     5: capable of thinking and expressing yourself in a clear and
        consistent manner; "a lucid thinker"; "she was more
        coherent ...
