---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preliminary
offline_file: ""
offline_thumbnail: ""
uuid: 9fca74be-6ecb-4beb-96aa-c67c2763773f
updated: 1484310563
title: preliminary
categories:
    - Dictionary
---
preliminary
     adj : designed to orient or acquaint with a situation before
           proceeding; "a preliminary investigation"
     n 1: a minor match preceding the main event [syn: {prelim}]
     2: something that serves as a preceding event or introduces
        what follows; "training is a necessary preliminary to
        employment"; "drinks were the overture to dinner" [syn: {overture},
         {prelude}]
