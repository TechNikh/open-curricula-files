---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accurately
offline_file: ""
offline_thumbnail: ""
uuid: c936ae8e-8579-489b-90ca-a2950721364d
updated: 1484310391
title: accurately
categories:
    - Dictionary
---
accurately
     adv 1: with few mistakes; "he works very accurately" [ant: {inaccurately}]
     2: strictly correctly; "repeated the order accurately"
