---
title: Gas exchange by the Lungs-lecture
youtube_id: aqdsjm4csqU
tags:
    - SSC
    - Biology
    - Respiration
    - Animation
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
