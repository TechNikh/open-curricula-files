---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Saddle_point
offline_file: ""
offline_thumbnail: ""
uuid: 65f3d240-5b37-4aab-8620-2da0e80f3b50
updated: 1484308830
title: Saddle point
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Saddle_point.svg.png
tags:
    - Mathematical discussion
    - Saddle surface
    - Other uses
    - Notes
categories:
    - Stability theory
---
In mathematics, a saddle point is a point in the domain of a function where the slopes (derivatives) of orthogonal function components defining the surface become zero (a stationary point) but are not a local extremum on both axes. The saddle point will always occur at a relative minimum along one axial direction (between peaks) and where the crossing axis is a relative maximum.
