---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fiction
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484438881
title: fiction
categories:
    - Dictionary
---
fiction
     n 1: a literary work based on the imagination and not necessarily
          on fact
     2: a deliberately false or improbable account [syn: {fabrication},
         {fable}]
