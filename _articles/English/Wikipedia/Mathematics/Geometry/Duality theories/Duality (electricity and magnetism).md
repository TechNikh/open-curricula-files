---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Duality_(electricity_and_magnetism)
offline_file: ""
offline_thumbnail: ""
uuid: 5b49df58-bd88-4b17-87d0-68d0a6621f0c
updated: 1484309232
title: Duality (electricity and magnetism)
categories:
    - Duality theories
---
In physics, the electromagnetic dual concept is based on the idea that, in the static case, electromagnetism has two separate facets: electric fields and magnetic fields. Expressions in one of these will have a directly analogous, or dual, expression in the other. The reason for this can ultimately be traced to special relativity where applying the Lorentz transformation to the electric field will transform it into a magnetic field.
