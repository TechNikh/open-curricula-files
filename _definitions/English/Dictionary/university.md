---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/university
offline_file: ""
offline_thumbnail: ""
uuid: eabd84e2-58c8-42f8-bc7e-e8dedf40c817
updated: 1484310307
title: university
categories:
    - Dictionary
---
university
     n 1: the body of faculty and students at a university
     2: establishment where a seat of higher learning is housed,
        including administrative and living quarters as well as
        facilities for research and teaching
     3: a large and diverse institution of higher learning created
        to educate for life and for a profession and to grant
        degrees
