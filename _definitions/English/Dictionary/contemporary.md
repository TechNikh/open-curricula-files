---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contemporary
offline_file: ""
offline_thumbnail: ""
uuid: 47634ea9-f1f0-4cc7-84bf-8d1e8d9baaa5
updated: 1484310467
title: contemporary
categories:
    - Dictionary
---
contemporary
     adj 1: characteristic of the present; "contemporary trends in
            design"; "the role of computers in modern-day
            medicine" [syn: {modern-day}]
     2: belonging to the present time; "contemporary leaders" [syn:
        {present-day(a)}]
     3: occurring in the same period of time; "a rise in interest
        rates is often contemporaneous with an increase in
        inflation"; "the composer Salieri was contemporary with
        Mozart" [syn: {contemporaneous}]
     n : a person of nearly the same age as another [syn: {coeval}]
