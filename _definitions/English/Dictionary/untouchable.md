---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/untouchable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484566561
title: untouchable
categories:
    - Dictionary
---
untouchable
     adj 1: beyond the reach of criticism or attack or impeachment; "for
            the first time criticism was directed at a hitherto
            untouchable target"- Newsweek
     2: impossible to assail [syn: {unassailable}]
     3: forbidden to the touch; "in most museums such articles are
        untouchable"
     4: defiling to the touch; especially used in traditional Hindu
        belief of the lowest caste or castes
     5: not capable of being obtained; "a rare work, today almost
        inaccessible"; "timber is virtually unobtainable in the
        islands"; "untouchable resources buried deep within the
        earth" [syn: {inaccessible}, {unobtainable}, ...
