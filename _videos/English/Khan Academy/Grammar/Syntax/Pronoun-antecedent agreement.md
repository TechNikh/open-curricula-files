---
version: 1
type: video
provider: YouTube
id: j9kIACViG60
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Pronoun-antecedent%20agreement.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 43993947-9ffa-410c-90af-97f6d24cd8fd
updated: 1486069601
title: Pronoun-antecedent agreement
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/j9kIACViG60/default.jpg
    - https://i3.ytimg.com/vi/j9kIACViG60/1.jpg
    - https://i3.ytimg.com/vi/j9kIACViG60/2.jpg
    - https://i3.ytimg.com/vi/j9kIACViG60/3.jpg
---
An antecedent is “the thing that came before”.  When you use a pronoun, it’s standing in for a word you used previously—that’s the antecedent.  Join us as we demonstrate how to make sure that your pronouns and antecedents match up with one another: that’s called agreement!

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/syntax/e/pronoun-antecedent-agreement 


Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/recognizing-fragments-syntax-khan-academy 

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/syntax/v/subject-verb-agreement-syntax-khan-academy 

Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
