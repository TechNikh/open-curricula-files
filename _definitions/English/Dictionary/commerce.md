---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commerce
offline_file: ""
offline_thumbnail: ""
uuid: b3b9c1c9-5e83-4171-a1c0-29fdaba48793
updated: 1484310479
title: commerce
categories:
    - Dictionary
---
commerce
     n 1: transactions (sales and purchases) having the objective of
          supplying commodities (goods and services) [syn: {commercialism},
           {mercantilism}]
     2: the United States federal department that promotes and
        administers domestic and foreign trade (including
        management of the census and the patent office); created
        in 1913 [syn: {Department of Commerce}, {Commerce
        Department}, {DoC}]
     3: social exchange, especially of opinions, attitudes, etc.
