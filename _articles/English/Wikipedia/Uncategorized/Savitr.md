---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Savita
offline_file: ""
offline_thumbnail: ""
uuid: b3fc7627-f6b3-419d-992a-e59492f0eff3
updated: 1484308458
title: Savitr
tags:
    - Rigvedic deity
    - Epithets
    - Savitr in the Brahmanas
    - Hindu revivalism
    - Notes
categories:
    - Uncategorized
---
Savitaṛ (Sanskrit: stem savitṛ-, nominative singular savitā) is a solar deity in the Rigveda, and possibly one of the Adityas i.e. off-spring of the female Vedic deity Aditi. His name in Vedic Sanskrit connotes "impeller, rouser, vivifier."
