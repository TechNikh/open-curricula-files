---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cournot_competition
offline_file: ""
offline_thumbnail: ""
uuid: 5bdba23c-e26d-4432-866a-6233a5f65bd0
updated: 1484309052
title: Cournot competition
tags:
    - History
    - Graphically finding the Cournot duopoly equilibrium
    - Calculating the equilibrium
    - An example
    - Cournot competition with many firms and the Cournot theorem
    - Implications
    - Bertrand versus Cournot
categories:
    - Non-cooperative games
---
Cournot competition is an economic model used to describe an industry structure in which companies compete on the amount of output they will produce, which they decide on independently of each other and at the same time. It is named after Antoine Augustin Cournot (1801–1877) who was inspired by observing competition in a spring water duopoly.[1] It has the following features:
