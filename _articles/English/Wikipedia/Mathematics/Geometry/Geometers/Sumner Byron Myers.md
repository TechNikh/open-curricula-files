---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sumner_Byron_Myers
offline_file: ""
offline_thumbnail: ""
uuid: c2a3dde1-72fe-4996-ae43-6f10d2fea376
updated: 1484309091
title: Sumner Byron Myers
categories:
    - Geometers
---
Sumner Byron Myers (February 19, 1910 – October 8, 1955) was an American mathematician specialized in topology. He studied at Harvard University under H. C. Marston Morse,[1] where he was graduated with a Ph.D. in 1932.[2] Myers then pursued postdoctoral studies at Princeton University (1934–1936)[3] before becoming a professor for mathematics at the University of Michigan, where an award for outstanding students of mathematics has been named in his honor.[4] He died unexpectedly from a heart attack during the 1955 Michigan–Army football game at Michigan Stadium.[5]
