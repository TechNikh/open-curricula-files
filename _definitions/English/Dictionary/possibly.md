---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/possibly
offline_file: ""
offline_thumbnail: ""
uuid: bfe51baa-fcc9-4191-ba85-9b7bc7e16379
updated: 1484310599
title: possibly
categories:
    - Dictionary
---
possibly
     adv 1: by chance; "perhaps she will call tomorrow"; "we may
            possibly run into them at the concert"; "it may
            peradventure be thought that there never was such a
            time" [syn: {perchance}, {perhaps}, {maybe}, {mayhap},
             {peradventure}]
     2: with a possibility of becoming actual; "introducing possibly
        dangerous innovations"; "he is potentially dangerous";
        "potentially useful" [syn: {potentially}]
     3: in a manner or to a degree possible of conceiving; "is it
        possibly true?" [ant: {impossibly}]
     4: to a degree possible of achievement or by possible means;
        "they can't possibly get here in time ...
