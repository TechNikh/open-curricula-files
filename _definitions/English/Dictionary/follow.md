---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/follow
offline_file: ""
offline_thumbnail: ""
uuid: dd447ffd-dfff-4332-a7d4-f98f51aae7ed
updated: 1484310297
title: follow
categories:
    - Dictionary
---
follow
     v 1: to travel behind, go after, come after; "The ducklings
          followed their mother around the pond"; "Please follow
          the guide through the museum" [ant: {precede}]
     2: be later in time; "Tuesday always follows Monday" [syn: {postdate}]
        [ant: {predate}]
     3: come as a logical consequence; follow logically; "It follows
        that your assertion is false"; "the theorem falls out
        nicely" [syn: {fall out}]
     4: travel along a certain course; "follow the road"; "follow
        the trail" [syn: {travel along}]
     5: act in accordance with someone's rules, commands, or wishes;
        "He complied with my instructions"; "You must comply ...
