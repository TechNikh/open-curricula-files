---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/introspection
offline_file: ""
offline_thumbnail: ""
uuid: 5fde8022-43ed-4dc7-8d82-e20e752d9e19
updated: 1484310186
title: introspection
categories:
    - Dictionary
---
introspection
     n : the contemplation of your own thoughts and desires and
         conduct [syn: {self-contemplation}, {self-examination}]
