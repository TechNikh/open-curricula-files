---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conformal_field_theory
offline_file: ""
offline_thumbnail: ""
uuid: 6f1c9779-2dd0-45d7-bf57-c60ff56a488b
updated: 1484309305
title: Conformal field theory
tags:
    - Scale invariance vs. conformal invariance
    - Dimensional considerations
    - Two dimensions
    - More than two dimensions
    - Conformal symmetry
categories:
    - Symmetry
---
A conformal field theory (CFT) is a quantum field theory that is invariant under conformal transformations. In two dimensions, there is an infinite-dimensional algebra of local conformal transformations, and conformal field theories can sometimes be exactly solved or classified.
