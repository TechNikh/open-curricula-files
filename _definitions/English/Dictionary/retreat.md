---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retreat
offline_file: ""
offline_thumbnail: ""
uuid: 5323f880-fd5f-474b-80d2-53fa6ec3f069
updated: 1484310575
title: retreat
categories:
    - Dictionary
---
retreat
     n 1: (military) withdrawal of troops to a more favorable position
          to escape the enemy's superior forces or after a defeat;
          "the disorderly retreat of French troops"
     2: a place of privacy; a place affording peace and quiet
     3: (military) a signal to begin a withdrawal from a dangerous
        position
     4: (military) a bugle call signaling the lowering of the flag
        at sunset
     5: an area where you can be alone [syn: {hideaway}]
     6: withdrawal for prayer and study and meditation; "a religious
        retreat" [syn: {retirement}]
     v 1: pull back or move away or backward; "The enemy withdrew";
          "The limo pulled away from ...
