---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Devi
offline_file: ""
offline_thumbnail: ""
uuid: 846d927d-6551-4491-b2d1-5bb966b5096b
updated: 1484308333
title: Devi
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-WLA_lacma_Hindu_Goddess_Parvati_Orissa.jpg
tags:
    - Etymology
    - History
    - Examples
    - Parvati
    - Lakshmi
    - Saraswati
    - Durga and Kali
    - Tridevi
    - Sita
    - Radha
    - Mahadevi
    - Tantra and Devis
    - Matrikas
    - Bibliography
categories:
    - Uncategorized
---
Devī is the Sanskrit word for "goddess"; the masculine form is Deva. Devi – the feminine form, and Deva – the masculine form, mean "heavenly, divine, anything of excellence", and are also gender specific terms for a deity in Hinduism.
