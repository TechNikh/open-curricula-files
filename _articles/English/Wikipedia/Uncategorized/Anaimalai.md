---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anaimalai
offline_file: ""
offline_thumbnail: ""
uuid: cf45562b-97db-4ffa-ae5d-da4684797757
updated: 1484308422
title: Anaimalai
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Amaravathi_Reservoir_%2526_Dam.jpg'
tags:
    - geography
    - Demographics
    - Places of interest
categories:
    - Uncategorized
---
Anaimalai is a panchayat town in Coimbatore district in the state of Tamil Nadu, India. Anaimalai is named after the Anaimudi hills around it. Place is famous pilgrimage spot for people going to Sabarimala, Kerala due to the presence of powerful woman goddess MASANI AMMAN.
