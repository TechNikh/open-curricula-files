---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/likely
offline_file: ""
offline_thumbnail: ""
uuid: 1002adf5-605f-4996-a6f7-bd015091e287
updated: 1484310301
title: likely
categories:
    - Dictionary
---
likely
     adj 1: has a good chance of being the case or of coming about;
            "these services are likely to be available to us all
            before long"; "she is likely to forget"; "a likely
            place for a restaurant"; "the broken limb is likely to
            fall"; "rain is likely"; "a likely topic for
            investigation"; "likely candidates for the job" [ant:
            {unlikely}]
     2: likely but not certain to be or become true or real; "a
        likely result"; "he foresaw a probable loss" [syn: {probable},
         {plausible}] [ant: {improbable}]
     3: expected to become or be; in prospect; "potential clients";
        "expected income" [syn: ...
