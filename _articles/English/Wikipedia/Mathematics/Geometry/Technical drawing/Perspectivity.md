---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perspectivity
offline_file: ""
offline_thumbnail: ""
uuid: 0b7e167c-5905-4fe2-b3ec-a5c66e8fe938
updated: 1484309327
title: Perspectivity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Projection_geometry.svg.png
tags:
    - Graphics
    - Projective geometry
    - Projectivity
    - Higher-dimensional perspectivities
    - Perspective collineations
    - Notes
categories:
    - Technical drawing
---
