---
title: Ocean
article_ref: https://en.wikipedia.org/wiki/Ocean
tags:
    - Etymology
    - Earth's global ocean
    - Oceanic divisions
    - Global system
    - Physical properties
    - Oceanic zones
    - Exploration
    - Oceanic maritime currents
    - Climate
    - Biology
    - Gases
    - Ocean surface
    - Mixing time
    - Salinity
    - Absorption of light
    - Economic value
    - Waves
    - Extraterrestrial oceans
    - Planets
    - Natural satellites
    - Dwarf planets and trans-Neptunian objects
    - Extrasolar
    - Non-water surface liquids
categories:
    - Water harvesting
---
