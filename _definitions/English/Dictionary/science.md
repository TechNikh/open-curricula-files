---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/science
offline_file: ""
offline_thumbnail: ""
uuid: 05853051-4474-404a-81a5-0872ef5724d5
updated: 1484310315
title: science
categories:
    - Dictionary
---
science
     n 1: a particular branch of scientific knowledge; "the science of
          genetics" [syn: {scientific discipline}]
     2: ability to produce solutions in some problem domain; "the
        skill of a well-trained boxer"; "the sweet science of
        pugilism" [syn: {skill}]
