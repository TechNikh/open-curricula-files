---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amine
offline_file: ""
offline_thumbnail: ""
uuid: 18a04e9e-68ec-4356-b508-e1991ed221c9
updated: 1484310419
title: amine
categories:
    - Dictionary
---
amine
     n : a compound derived from ammonia by replacing hydrogen atoms
         by univalent hydrocarbon radicals [syn: {aminoalkane}]
