---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Chern%E2%80%93Weil_homomorphism'
offline_file: ""
offline_thumbnail: ""
uuid: 0508f71e-4368-4b8b-9a67-a4bb2c9d59a0
updated: 1484309209
title: Chern–Weil homomorphism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Cher_Assinatura_0.png
tags:
    - Definition of the homomorphism
    - 'Example: Chern classes and Chern character'
    - 'Example: Pontrjagin classes'
    - The homomorphism for holomorphic vector bundles
    - Notes
categories:
    - Differential geometry
---
In mathematics, the Chern–Weil homomorphism is a basic construction in the Chern–Weil theory that computes topological invariants of vector bundles and principal bundles on a smooth manifold M in terms of connections and curvature representing classes in the de Rham cohomology rings of M. That is, the theory forms a bridge between the areas of algebraic topology and differential geometry. It was developed in the late 1940s by Shiing-Shen Chern and André Weil, in the wake of proofs of the generalized Gauss–Bonnet theorem. This theory was an important step in the theory of characteristic classes.
