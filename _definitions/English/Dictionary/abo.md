---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abo
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484364601
title: abo
categories:
    - Dictionary
---
Abo
     n : a dark-skinned member of a race of people living in
         Australia when Europeans arrived [syn: {Aborigine}, {Aboriginal},
          {native Australian}, {Australian Aborigine}]
