---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rotation_map
offline_file: ""
offline_thumbnail: ""
uuid: 873854a5-78bd-4782-b321-ed5ab9a11780
updated: 1484309406
title: Rotation map
tags:
    - Definition
    - Basic properties
    - Special cases and properties
categories:
    - Extensions and generalizations of graphs
---
In mathematics, a rotation map is a function that represents an undirected edge-labeled graph, where each vertex enumerates its outgoing neighbors. Rotation maps were first introduced by Reingold, Vadhan and Wigderson (“Entropy waves, the zig-zag graph product, and new constant-degree expanders”, 2002) in order to conveniently define the zig-zag product and prove its properties. Given a vertex 
  
    
      
        v
      
    
    {\displaystyle v}
  
 and an edge label 
  
    
      
        i
      
    
    {\displaystyle i}
  
, the rotation map returns the 
  
    
      
        i
      
    
    {\displaystyle i}
  
'th neighbor of 
  
    
      
        v
      
    
    ...
