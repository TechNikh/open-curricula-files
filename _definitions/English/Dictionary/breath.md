---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breath
offline_file: ""
offline_thumbnail: ""
uuid: 7ddc6d98-e6b0-4feb-adf7-56574cfb3c9a
updated: 1484310424
title: breath
categories:
    - Dictionary
---
breath
     n 1: the process of taking in and expelling air during breathing;
          "he took a deep breath and dived into the pool"; "he was
          fighting to his last breath"
     2: the air that is inhaled and exhaled in respiration; "his
        sour breath offended her"
     3: a short respite [syn: {breather}, {breathing place}, {breathing
        space}, {breathing spell}, {breathing time}]
     4: an indirect suggestion; "not a breath of scandal ever
        touched her" [syn: {hint}, {intimation}]
     5: a slight movement of the air; "there wasn't a breath of air
        in the room"
