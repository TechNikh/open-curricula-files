---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electrolyte
offline_file: ""
offline_thumbnail: ""
uuid: 9268c79b-1798-4fa1-ba17-66a4ba4a7926
updated: 1484310204
title: electrolyte
categories:
    - Dictionary
---
electrolyte
     n : a solution that conducts electricity; "the proper amount and
         distribution of electrolytes in the body is essential for
         health"
