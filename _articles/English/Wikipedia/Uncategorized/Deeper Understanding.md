---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deeper_understanding
offline_file: ""
offline_thumbnail: ""
uuid: 3eead76c-e41a-4dbf-a1ee-8938adde3340
updated: 1484308356
title: Deeper Understanding
tags:
    - Background
    - Song information
    - Music video
    - Release
    - Charts
categories:
    - Uncategorized
---
"Deeper Understanding" is a song by Kate Bush released as the lead single from the 2011 album Director's Cut.[1] The single was a re-working of the song originally released on her 1989 album, The Sensual World. The song's lyrics describe an increasingly intense relationship between a lonely person and a computer.[2] It is her first single release since 2005.
