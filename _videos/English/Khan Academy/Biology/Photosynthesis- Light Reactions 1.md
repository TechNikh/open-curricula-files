---
version: 1
type: video
provider: YouTube
id: GR2GA7chA_c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Photosynthesis-%20Light%20Reactions%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 200c1af6-8fa3-44ca-832f-a425bf2e061a
updated: 1486069583
title: 'Photosynthesis- Light Reactions 1'
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/GR2GA7chA_c/default.jpg
    - https://i3.ytimg.com/vi/GR2GA7chA_c/1.jpg
    - https://i3.ytimg.com/vi/GR2GA7chA_c/2.jpg
    - https://i3.ytimg.com/vi/GR2GA7chA_c/3.jpg
---
Details on the light-dependent reactions of photosynthesis
More free lessons at: http://www.khanacademy.org/video?v=GR2GA7chA_c
