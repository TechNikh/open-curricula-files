---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Metric_derivative
offline_file: ""
offline_thumbnail: ""
uuid: 3a754eb1-e3f5-43e7-a2f4-c746acc16559
updated: 1484309269
title: Metric derivative
categories:
    - Metric geometry
---
In mathematics, the metric derivative is a notion of derivative appropriate to parametrized paths in metric spaces. It generalizes the notion of "speed" or "absolute velocity" to spaces which have a notion of distance (i.e. metric spaces) but not direction (such as vector spaces).
