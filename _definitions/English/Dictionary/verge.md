---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/verge
offline_file: ""
offline_thumbnail: ""
uuid: 0c635d25-81ad-478e-992b-b169e4e3a5b2
updated: 1484310529
title: verge
categories:
    - Dictionary
---
verge
     n 1: a region marking a boundary [syn: {brink}, {threshold}]
     2: the limit beyond which something happens or changes; "on the
        verge of tears"; "on the brink of bankruptcy" [syn: {brink}]
     3: a ceremonial or emblematic staff [syn: {scepter}, {sceptre},
         {wand}]
     4: a grass border along a road
     v : border on; come close to; "His behavior verges on the
         criminal"
