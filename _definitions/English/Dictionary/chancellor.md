---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chancellor
offline_file: ""
offline_thumbnail: ""
uuid: aeb5d79c-6b81-4241-a8fb-23a94abaab27
updated: 1484310551
title: chancellor
categories:
    - Dictionary
---
chancellor
     n 1: the person who is head of state (in several countries) [syn:
           {premier}, {prime minister}]
     2: the honorary or titular head of a university
