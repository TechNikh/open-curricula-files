---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lifted
offline_file: ""
offline_thumbnail: ""
uuid: 4872bd28-0001-4e21-bffc-b3a82fe784c3
updated: 1484310541
title: lifted
categories:
    - Dictionary
---
lifted
     adj : held up in the air; "stood with arms upraised"; "her
           upraised flag" [syn: {upraised}]
