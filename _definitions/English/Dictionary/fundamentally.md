---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fundamentally
offline_file: ""
offline_thumbnail: ""
uuid: 0cfe38e5-61f1-47ab-bb0d-0f146509a049
updated: 1484310595
title: fundamentally
categories:
    - Dictionary
---
fundamentally
     adv : at bottom or by one's (or its) very nature; "He is basically
           dishonest"; "the argument was essentially a technical
           one"; "for all his bluster he is in essence a shy
           person" [syn: {basically}, {essentially}, {in essence},
            {au fond}]
