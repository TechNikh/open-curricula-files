---
version: 1
type: video
provider: YouTube
id: 3FRmP2K4NLo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4c7d8ee8-3c8f-4260-9332-d91492f61186
updated: 1486069647
title: Household Goods
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/3FRmP2K4NLo/default.jpg
    - https://i3.ytimg.com/vi/3FRmP2K4NLo/1.jpg
    - https://i3.ytimg.com/vi/3FRmP2K4NLo/2.jpg
    - https://i3.ytimg.com/vi/3FRmP2K4NLo/3.jpg
---
