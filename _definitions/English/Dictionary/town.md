---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/town
offline_file: ""
offline_thumbnail: ""
uuid: 30b5e8dd-7c9e-4c4d-88f6-bb03c0bde6e4
updated: 1484310441
title: town
categories:
    - Dictionary
---
town
     n 1: an urban area with a fixed boundary that is smaller than a
          city; "they drive through town on their way to work"
     2: an administrative division of a county; "the town is
        responsible for snow removal" [syn: {township}]
     3: the people living in a municipality smaller than a city;
        "the whole town cheered the team" [syn: {townspeople}, {townsfolk}]
