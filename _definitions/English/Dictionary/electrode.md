---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electrode
offline_file: ""
offline_thumbnail: ""
uuid: 5fab79dc-89e6-4653-8a04-ee9fdfce8f81
updated: 1484310403
title: electrode
categories:
    - Dictionary
---
electrode
     n : a conductor used to make electrical contact with some part
         of a circuit
