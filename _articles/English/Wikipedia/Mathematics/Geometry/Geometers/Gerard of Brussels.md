---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gerard_of_Brussels
offline_file: ""
offline_thumbnail: ""
uuid: 482ea8b2-46f1-4aa1-aaa0-5cfe5db9a636
updated: 1484309083
title: Gerard of Brussels
categories:
    - Geometers
---
Gerard of Brussels (French: Gérard de Bruxelles, Latin: Gerardus Bruxellensis) was an early thirteenth-century geometer and philosopher known primarily for his Latin book Liber de motu (or On Motion), which was a pioneering study in kinematics, probably written between 1187 and 1260. It has been described as "the first Latin treatise that was to take the fundamental approach to kinematics that was to characterize modern kinematics."[1] He brought the works of Euclid and Archimedes back into popularity and was a direct influence on the Oxford Calculators (four kinematicists of Merton College) in the next century. Gerard is cited by Thomas Bradwardine in his Tractatus de proportionibus ...
