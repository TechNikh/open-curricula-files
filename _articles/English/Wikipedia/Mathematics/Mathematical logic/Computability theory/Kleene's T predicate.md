---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kleene%27s_T_predicate'
offline_file: ""
offline_thumbnail: ""
uuid: bc4c9080-7b90-48e2-a019-7b586949d10b
updated: 1484309487
title: "Kleene's T predicate"
tags:
    - Definition
    - Normal form theorem
    - Formalization
    - Arithmetical hierarchy
    - Notes
categories:
    - Computability theory
---
In computability theory, the T predicate, first studied by mathematician Stephen Cole Kleene, is a particular set of triples of natural numbers that is used to represent computable functions within formal theories of arithmetic. Informally, the T predicate tells whether a particular computer program will halt when run with a particular input, and the corresponding U function is used to obtain the results of the computation if the program does halt. As with the smn theorem, the original notation used by Kleene has become standard terminology for the concept.[1]
