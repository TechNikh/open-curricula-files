---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carbonyl
offline_file: ""
offline_thumbnail: ""
uuid: 89145eda-04fa-44b6-90ee-3f891851f0a6
updated: 1484310423
title: carbonyl
categories:
    - Dictionary
---
carbonyl
     adj : relating to or containing the carbonyl group [syn: {carbonylic}]
     n : a compound containing metal combined with carbon monoxide
