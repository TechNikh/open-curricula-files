---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rethink
offline_file: ""
offline_thumbnail: ""
uuid: 31ba4eb6-c197-427f-8761-4fef0ed83dc6
updated: 1484310170
title: rethink
categories:
    - Dictionary
---
rethink
     n : thinking again about a choice previously made; "he had
         second thoughts about his purchase" [syn: {reconsideration},
          {second thought}, {afterthought}]
     v : change one's mind; "He rethought his decision to take a
         vacation"
     [also: {rethought}]
