---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Herbert_Gintis
offline_file: ""
offline_thumbnail: ""
uuid: d6c3c6f6-f52c-4846-a710-b0add9c3f270
updated: 1484308517
title: Herbert Gintis
tags:
    - Life and career
    - Books
categories:
    - Game theorists
---
Herbert Gintis (born February 11, 1940) is an American economist, behavioral scientist, and educator known for his theoretical contributions to sociobiology, especially altruism, cooperation, epistemic game theory, gene-culture coevolution, efficiency wages, strong reciprocity, and human capital theory. Throughout his career, he has worked extensively with economist Samuel Bowles. Their landmark book, Schooling in Capitalist America, has had multiple editions in five languages since it was first published in 1976. Their most recent book, A Cooperative Species: Human Reciprocity and its Evolution was published by Princeton University Press in 2011.
