---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Twenty-four
offline_file: ""
offline_thumbnail: ""
uuid: 860906b8-887c-4103-aa4b-6b5a8e1b8e5b
updated: 1484308412
title: 24 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Prague_Apr04_015.jpg
tags:
    - In mathematics
    - In science
    - In religion
    - In music
    - In sports
    - In other fields
categories:
    - Uncategorized
---
The SI prefix for 1024 is yotta (Y), and for 10−24 (i.e., the reciprocal of 1024) yocto (y). These numbers are the largest and smallest number to receive an SI prefix to date. In a 24-hour clock, the twentyfourth hour is in conventional language called twelve or twelve o'clock.
