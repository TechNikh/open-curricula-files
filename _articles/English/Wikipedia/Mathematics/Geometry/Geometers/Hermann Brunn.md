---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hermann_Brunn
offline_file: ""
offline_thumbnail: ""
uuid: 2e8d4d26-d515-4af4-91ec-48a8fdeae152
updated: 1484309075
title: Hermann Brunn
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-6Loops-Brunnian-link.svg.png
categories:
    - Geometers
---
Karl Hermann Brunn (August 1, 1862 – September 20, 1939) was a German mathematician, known for his work in convex geometry (see Brunn–Minkowski inequality) and in knot theory. Brunnian links are named after him, as his 1892 article "Über Verkettung" included examples of such links.
