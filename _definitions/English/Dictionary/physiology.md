---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/physiology
offline_file: ""
offline_thumbnail: ""
uuid: 87d50e6c-659f-4e37-8ce4-aa6cd29eb62a
updated: 1484310357
title: physiology
categories:
    - Dictionary
---
physiology
     n 1: the branch of the biological sciences dealing with the
          functioning of organisms
     2: processes and functions of an organism
