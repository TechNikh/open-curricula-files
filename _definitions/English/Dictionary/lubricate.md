---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lubricate
offline_file: ""
offline_thumbnail: ""
uuid: c070e97c-e5e5-4e9f-b5c2-ad31479dffb6
updated: 1484310335
title: lubricate
categories:
    - Dictionary
---
lubricate
     v 1: have lubricating properties; "the liquid in this can
          lubricates well"
     2: apply a lubricant to; "lubricate my car" [syn: {lube}]
     3: make slippery or smooth through the application of a
        lubricant; "lubricate the key"
