---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Whole_number
offline_file: ""
offline_thumbnail: ""
uuid: 96120299-56f7-4edb-b8a5-c79c768d5fdc
updated: 1484308313
title: Natural number
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Three_apples%25281%2529.svg.png'
tags:
    - History
    - Ancient roots
    - Modern definitions
    - Notation
    - Properties
    - Addition
    - Multiplication
    - Relationship between addition and multiplication
    - Order
    - Division
    - Algebraic properties satisfied by the natural numbers
    - Generalizations
    - Formal definitions
    - Peano axioms
    - Constructions based on set theory
    - von Neumann construction
    - Other constructions
    - Notes
categories:
    - Uncategorized
---
In mathematics, the natural numbers are those used for counting (as in "there are six coins on the table") and ordering (as in "this is the third largest city in the country"). In common language, words used for counting are "cardinal numbers" and words used for ordering are "ordinal numbers".
