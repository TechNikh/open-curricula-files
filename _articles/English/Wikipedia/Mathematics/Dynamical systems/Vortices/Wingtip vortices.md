---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wingtip_vortices
offline_file: ""
offline_thumbnail: ""
uuid: e36d3b79-ce7d-4d71-aa2e-d2dd1caf9a6a
updated: 1484308887
title: Wingtip vortices
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tip_vortex_rollup.png
tags:
    - Generation of trailing vortices
    - Effects and mitigation
    - Visibility of vortices
    - Aerodynamic condensation and freezing
    - Formation flight
    - Hazards
    - Gallery
    - Notes
categories:
    - Vortices
---
Wingtip vortices are circular patterns of rotating air left behind a wing as it generates lift.[1] One wingtip vortex trails from the tip of each wing. Wingtip vortices are sometimes named trailing or lift-induced vortices because they also occur at points other than at the wing tips.[1] Indeed, vorticity is trailed at any point on the wing where the lift varies span-wise (a fact described and quantified by the lifting-line theory); it eventually rolls up into large vortices near the wingtip, at the edge of flap devices, or at other abrupt changes in wing planform.
