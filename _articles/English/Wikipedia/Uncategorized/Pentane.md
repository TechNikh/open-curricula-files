---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pentane
offline_file: ""
offline_thumbnail: ""
uuid: 43ac6f69-12ef-4fe7-b573-710c6164252b
updated: 1484308394
title: Pentane
tags:
    - Industrial uses
    - Laboratory use
    - Physical properties
    - Reactions
categories:
    - Uncategorized
---
Pentane is an organic compound with the formula C5H12 — that is, an alkane with five carbon atoms. The term may refer to any of three structural isomers, or to a mixture of them: in the IUPAC nomenclature, however, pentane means exclusively the n-pentane isomer; the other two are called isopentane (methylbutane) and neopentane (dimethylpropane). Cyclopentane is not an isomer of pentane because it has only 10 hydrogen atoms where pentane has 12.
