---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/watering
offline_file: ""
offline_thumbnail: ""
uuid: f342861e-4f93-43b0-9327-55177fe7f69a
updated: 1484310346
title: watering
categories:
    - Dictionary
---
watering
     n 1: shedding tears [syn: {lacrimation}, {lachrymation}, {tearing}]
     2: wetting with water; "the lawn needs a great deal of
        watering"
