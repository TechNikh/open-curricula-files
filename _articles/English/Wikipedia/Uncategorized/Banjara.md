---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Banjara
offline_file: ""
offline_thumbnail: ""
uuid: 88288c52-2ec3-47a4-a2db-64efca54498e
updated: 1484094302
title: Banjara
tags:
    - Origin
    - culture
    - language
    - art
    - Festivals
    - religion
    - Society
    - Distribution
    - Classification
    - Bibliography
categories:
    - Uncategorized
---
The Banjara (also called Gor, Lambani, Vanjara and Gormati) are a community usually described as nomadic people from the northwestern belt of the Indian subcontinent (from Afghanistan to the state of Rajasthan) now spread out all over India.
