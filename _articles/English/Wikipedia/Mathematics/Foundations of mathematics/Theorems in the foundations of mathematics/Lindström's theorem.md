---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lindstr%C3%B6m%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: b544285a-ba17-48b4-a73f-1f7f434ea368
updated: 1484308992
title: "Lindström's theorem"
categories:
    - Theorems in the foundations of mathematics
---
In mathematical logic, Lindström's theorem (named after Swedish logician Per Lindström, who published it in 1969) states that first-order logic is the strongest logic [1] (satisfying certain conditions, e.g. closure under classical negation) having both the (countable) compactness property and the (downward) Löwenheim–Skolem property.[2]
