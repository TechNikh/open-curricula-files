---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Node_(physics)
offline_file: ""
offline_thumbnail: ""
uuid: 3bb32669-c090-4843-9747-984000f60aeb
updated: 1484308894
title: Node (physics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Standing_wave.gif
tags:
    - Explanation
    - Boundary conditions
    - Examples
    - Sound
    - chemistry
categories:
    - Waves
---
A node is a point along a standing wave where the wave has minimum amplitude. For instance, in a vibrating guitar string, the ends of the string are nodes. By changing the position of the end node through frets, the guitarist changes the effective length of the vibrating string and thereby the note played. The opposite of a node is an anti-node, a point where the amplitude of the standing wave is a maximum. These occur midway between the nodes.[1]
