---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Period-doubling_bifurcation
offline_file: ""
offline_thumbnail: ""
uuid: a7ab9cb9-9dab-4743-b79a-e90f7aef7bc2
updated: 1484308699
title: Period-doubling bifurcation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Logistic_Bifurcation_map_High_Resolution.png
tags:
    - Examples
    - Logistic map
    - Logistical map for a modified Phillips curve
    - Complex quadratic map
    - Period-halving bifurcation
categories:
    - Bifurcation theory
---
In mathematics, a period doubling bifurcation in a discrete dynamical system is a bifurcation in which a slight change in a parameter value in the system's equations leads to the system switching to a new behavior with twice the period of the original system. With the doubled period, it takes twice as many iterations as before for the numerical values visited by the system to repeat themselves.
