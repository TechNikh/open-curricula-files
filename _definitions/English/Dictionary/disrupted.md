---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disrupted
offline_file: ""
offline_thumbnail: ""
uuid: 7d6fd8ed-e34f-4764-bf24-a409a61a0667
updated: 1484310537
title: disrupted
categories:
    - Dictionary
---
disrupted
     adj : maked by breaks or gaps; "many routes are unsafe or
           disrupted"
