---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/usa
offline_file: ""
offline_thumbnail: ""
uuid: 8fa3f702-c796-4559-af00-d96bbe2f35cd
updated: 1484310471
title: usa
categories:
    - Dictionary
---
U.S.A.
     n : North American republic containing 50 states - 48
         conterminous states in North America plus Alaska in
         northwest North America and the Hawaiian Islands in the
         Pacific Ocean; achieved independence in 1776 [syn: {United
         States}, {United States of America}, {America}, {US}, {U.S.},
          {USA}]
