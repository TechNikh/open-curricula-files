---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peacock
offline_file: ""
offline_thumbnail: ""
uuid: 6696ce38-2459-458c-b9f0-f8fb1822d26f
updated: 1484310162
title: peacock
categories:
    - Dictionary
---
peacock
     n 1: European butterfly having reddish-brown wings each marked
          with a purple eyespot [syn: {peacock butterfly}, {Inachis
          io}]
     2: male peafowl; having a crested head and very large fanlike
        tail marked with iridescent eyes or spots
