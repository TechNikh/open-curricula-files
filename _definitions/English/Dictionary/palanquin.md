---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/palanquin
offline_file: ""
offline_thumbnail: ""
uuid: ca7e89e9-0209-47fb-b10f-e13269cfd296
updated: 1484310146
title: palanquin
categories:
    - Dictionary
---
palanquin
     n : a closed litter carried on the shoulders of four bearers
         [syn: {palankeen}]
