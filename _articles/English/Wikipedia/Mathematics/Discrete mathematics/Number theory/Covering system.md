---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Covering_system
offline_file: ""
offline_thumbnail: ""
uuid: e224ad77-cdb8-433c-9a5d-7358af8aea21
updated: 1484308636
title: Covering system
tags:
    - Examples and definitions
    - Mirsky–Newman theorem
    - Primefree sequences
    - Boundedness of the smallest modulus
    - Systems of odd moduli
categories:
    - Number theory
---
of finitely many residue classes 
  
    
      
        
          a
          
            i
          
        
        (
        
          m
          o
          d
        
         
        
          
            n
            
              i
            
          
        
        )
        =
        {
        
          a
          
            i
          
        
        +
        
          n
          
            i
          
        
        x
        :
         
        x
        ∈
        
          Z
        
        }
      
    
    {\displaystyle a_{i}(\mathrm {mod} \ {n_{i}})=\{a_{i}+n_{i}x:\ x\in \mathbb {Z} \}}
  
 whose union contains every integer.
