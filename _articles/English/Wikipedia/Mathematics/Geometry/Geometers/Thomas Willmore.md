---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thomas_Willmore
offline_file: ""
offline_thumbnail: ""
uuid: cd0716fd-3675-46b8-8b6d-0130d9bab2e6
updated: 1484309105
title: Thomas Willmore
categories:
    - Geometers
---
Thomas James Willmore (16 April 1919 – 20 February 2005) was an English geometer. He is best known for his work on Riemannian 3-space and harmonic spaces.
