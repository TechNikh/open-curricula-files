---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gorenstein_scheme
offline_file: ""
offline_thumbnail: ""
uuid: 710167ed-1700-4c7d-8133-2d0a64be90f7
updated: 1484309149
title: Gorenstein scheme
tags:
    - Related properties
    - Examples
    - Notes
categories:
    - Algebraic geometry
---
In algebraic geometry, a Gorenstein scheme is a locally Noetherian scheme whose local rings are all Gorenstein.[1] The canonical line bundle is defined for any Gorenstein scheme over a field, and its properties are much the same as in the special case of smooth schemes.
