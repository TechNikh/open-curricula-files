---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Correspondence_problem
offline_file: ""
offline_thumbnail: ""
uuid: 64050985-33c1-483e-86af-dcfa28b02ff2
updated: 1484309179
title: Correspondence problem
tags:
    - Overview
    - Basic methods
    - use
    - Simple example
    - Simple correlation-based example
categories:
    - Geometry in computer vision
---
The correspondence problem refers to the problem of ascertaining which parts of one image correspond to which parts of another image, where differences are due to movement of the camera, the elapse of time, and/or movement of objects in the photos.
