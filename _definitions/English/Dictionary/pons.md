---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pons
offline_file: ""
offline_thumbnail: ""
uuid: f72803ce-3318-45a1-bc38-bb9762884e83
updated: 1484310316
title: pons
categories:
    - Dictionary
---
pons
     n : a band of nerve fibers linking the medulla oblongata and the
         cerebellum with the midbrain [syn: {pons Varolii}]
     [also: {pontes} (pl)]
