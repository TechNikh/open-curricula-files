---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/itch
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484406061
title: itch
categories:
    - Dictionary
---
itch
     n 1: a contagious skin infection caused by the itch mite;
          characterized by persistent itching and skin irritation;
          "he has a bad case of the itch" [syn: {scabies}]
     2: a strong restless desire; "why this urge to travel?" [syn: {urge}]
     3: an irritating cutaneous sensation that produces a desire to
        scratch [syn: {itchiness}, {itching}]
     v 1: scrape or rub as if to relieve itching; "Don't scratch your
          insect bites!" [syn: {rub}, {scratch}]
     2: have or perceive an itch; "I'm itching--the air is so dry!"
     3: have a strong desire or urge to do something; "She is
        itching to start the project"; "He is spoiling for a
       ...
