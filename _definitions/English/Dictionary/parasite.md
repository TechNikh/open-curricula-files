---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parasite
offline_file: ""
offline_thumbnail: ""
uuid: cd9336de-d5e8-4284-a4b4-b107d08ba9a7
updated: 1484310160
title: parasite
categories:
    - Dictionary
---
parasite
     n 1: an animal or plant that lives in or on a host (another
          animal or plant); the parasite obtains nourishment from
          the host without benefiting or killing the host [ant: {host}]
     2: a follower who hangs around a host (without benefit to the
        host) in hope of gain or advantage [syn: {leech}, {sponge},
         {sponger}]
