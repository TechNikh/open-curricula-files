---
version: 1
type: video
provider: YouTube
id: hvNJ3yWZQbE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 15b5e179-4415-4792-a5d2-47d4ef1d4bd7
updated: 1486069670
title: Ubiquitin Proteasome System programme
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/hvNJ3yWZQbE/default.jpg
    - https://i3.ytimg.com/vi/hvNJ3yWZQbE/1.jpg
    - https://i3.ytimg.com/vi/hvNJ3yWZQbE/2.jpg
    - https://i3.ytimg.com/vi/hvNJ3yWZQbE/3.jpg
---
