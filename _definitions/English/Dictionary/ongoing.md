---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ongoing
offline_file: ""
offline_thumbnail: ""
uuid: df48b4f8-95f6-4bb5-915e-5eef092a12d6
updated: 1484310441
title: ongoing
categories:
    - Dictionary
---
ongoing
     adj : currently happening; "an ongoing economic crisis";
           "negotiations are in progress" [syn: {in progress(p)}]
