---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spherical_shell
offline_file: ""
offline_thumbnail: ""
uuid: 630a2aa1-1974-47a1-a2ef-a9498fca6905
updated: 1484308944
title: Spherical shell
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Kugelschale.svg.png
categories:
    - Elementary geometry
---
In geometry, a spherical shell is a generalization of an annulus to three dimensions. A spherical shell is the region between two concentric spheres of differing radii.
