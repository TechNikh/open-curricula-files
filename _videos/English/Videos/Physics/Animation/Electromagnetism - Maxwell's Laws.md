---
version: 1
type: video
provider: YouTube
id: 9Tm2c6NJH4Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 275d2745-ac4b-4490-a2fa-f7fc30e02d84
updated: 1486069656
title: "Electromagnetism - Maxwell's Laws"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/9Tm2c6NJH4Y/default.jpg
    - https://i3.ytimg.com/vi/9Tm2c6NJH4Y/1.jpg
    - https://i3.ytimg.com/vi/9Tm2c6NJH4Y/2.jpg
    - https://i3.ytimg.com/vi/9Tm2c6NJH4Y/3.jpg
---
