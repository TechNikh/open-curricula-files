---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Varignon%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 60f9c2ce-1865-45fb-9f95-cda4f7448e03
updated: 1484309126
title: "Varignon's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Varignon_parallelogram_convex.svg.png
tags:
    - Theorem
    - Proof
    - The Varignon parallelogram
    - Properties
    - Special cases
    - Notes
    - References and further reading
categories:
    - Theorems in geometry
---
Varignon's theorem is a statement in Euclidean geometry, that deals with the construction of a particular parallelogram, the Varignon parallelogram, from an arbitrary quadrilateral (quadrangle). It is named after Pierre Varignon, who published it in 1731.
