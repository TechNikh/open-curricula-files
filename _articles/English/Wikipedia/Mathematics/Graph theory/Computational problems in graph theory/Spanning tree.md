---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spanning_tree
offline_file: ""
offline_thumbnail: ""
uuid: e7acdfd8-8e2f-4923-8b86-9dec7a37a0d7
updated: 1484309396
title: Spanning tree
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-4x4_grid_spanning_tree.svg.png
tags:
    - Applications
    - Definitions
    - Fundamental cycles
    - Fundamental cutsets
    - Spanning forests
    - Counting spanning trees
    - In specific graphs
    - In arbitrary graphs
    - Deletion-contraction
    - Tutte polynomial
    - Algorithms
    - Construction
    - Optimization
    - Randomization
    - Enumeration
    - In infinite graphs
    - In directed multigraphs
    - Notes
categories:
    - Computational problems in graph theory
---
In the mathematical field of graph theory, a spanning tree T of an undirected graph G is a subgraph that is a tree which includes all of the vertices of G. In general, a graph may have several spanning trees, but a graph that is not connected will not contain a spanning tree (but see Spanning forests below). If all of the edges of G are also edges of a spanning tree T of G, then G is a tree and is identical to T (that is, a tree has a unique spanning tree and it is itself).
