---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distributed
offline_file: ""
offline_thumbnail: ""
uuid: 8cfaa6e9-068b-4597-a334-5631243419ee
updated: 1484310323
title: distributed
categories:
    - Dictionary
---
distributed
     adj 1: spread out or scattered about or divided up [ant: {concentrated}]
     2: (of investments) distributed among a variety of securities
