---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Camera_auto-calibration
offline_file: ""
offline_thumbnail: ""
uuid: 139ddfa5-2ccb-4bae-a412-591e86656335
updated: 1484309176
title: Camera auto-calibration
tags:
    - Problem statement
    - Solution Domains
    - Algorithms of camera auto-calibration
categories:
    - Geometry in computer vision
---
Camera auto-calibration is the process of determining internal camera parameters directly from multiple uncalibrated images of unstructured scenes. In contrast to classic camera calibration, auto-calibration does not require any special calibration objects in the scene. In the special effects industry, camera auto-calibration is often part of the "Match Moving" process where a synthetic camera trajectory and intrinsic projection model are solved to reproject synthetic content into video.
