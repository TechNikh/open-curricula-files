---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/breathe
offline_file: ""
offline_thumbnail: ""
uuid: c7a46c14-3e92-42d7-88df-c0fef8b20b40
updated: 1484310319
title: breathe
categories:
    - Dictionary
---
breathe
     v 1: draw air into, and expel out of, the lungs; "I can breathe
          better when the air is clean"; "The patient is
          respiring" [syn: {take a breath}, {respire}, {suspire}]
     2: be alive; "Every creature that breathes"
     3: impart as if by breathing; "He breathed new life into the
        old house"
     4: allow the passage of air through; "Our new synthetic fabric
        breathes and is perfect for summer wear"
     5: utter or tell; "not breathe a word"
     6: manifest or evince; "She breathes the Christian spirit"
     7: take a short break from one's activities in order to relax
        [syn: {rest}, {catch one's breath}, {take a breather}]
     8: ...
