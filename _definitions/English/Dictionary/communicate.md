---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/communicate
offline_file: ""
offline_thumbnail: ""
uuid: 908a7f56-d1e7-4bf1-b217-65800b0f7a32
updated: 1484310321
title: communicate
categories:
    - Dictionary
---
communicate
     v 1: transmit information ; "Please communicate this message to
          all employees" [syn: {pass on}, {pass}, {put across}]
     2: transmit thoughts or feelings; "He communicated his
        anxieties to the psychiatrist" [syn: {intercommunicate}]
     3: transfer to another; "communicate a disease" [syn: {convey},
         {transmit}]
     4: join or connect; "The rooms communicated"
     5: be in verbal contact; interchange information or ideas; "He
        and his sons haven't communicated for years"; "Do you
        communicate well with your advisor?"
     6: administer communion; in church [ant: {excommunicate}]
     7: receive Communion, in the Catholic church ...
