---
version: 1
type: video
provider: YouTube
id: rse0I7rZ8jM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2ab0a59c-956e-4e86-a516-d871defdb981
updated: 1486069591
title: >
    Object image and focal distance relationship (proof of
    formula)
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/rse0I7rZ8jM/default.jpg
    - https://i3.ytimg.com/vi/rse0I7rZ8jM/1.jpg
    - https://i3.ytimg.com/vi/rse0I7rZ8jM/2.jpg
    - https://i3.ytimg.com/vi/rse0I7rZ8jM/3.jpg
---
Object Image and Focal Distance Relationship (Proof of Formula). Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/geometric-optics/mirrors-and-lenses/v/object-image-height-and-distance-relationship?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/geometric-optics/mirrors-and-lenses/v/concave-lenses?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
