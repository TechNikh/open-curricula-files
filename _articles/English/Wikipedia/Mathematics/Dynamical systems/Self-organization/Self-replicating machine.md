---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-replicating_machine
offline_file: ""
offline_thumbnail: ""
uuid: 9d08de1f-d2ef-49d1-b576-bd540b3074a8
updated: 1484308823
title: Self-replicating machine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Advanced_Automation_for_Space_Missions_figure_5-29.gif
tags:
    - History
    - "von Neumann's kinematic model"
    - "Moore's artificial living plants"
    - "Dyson's replicating systems"
    - Advanced Automation for Space Missions
    - Lackner-Wendt Auxon replicators
    - Recent work
    - Self-replicating rapid prototypers
    - NIAC studies on self-replicating systems
    - "Cornell University's self-assembler"
    - New York University artificial DNA tile motifs
    - Self-replication of magnetic polymers
    - Self-replication of neural circuits
    - Partial construction
    - Self-replicating spacecraft
    - Other references
    - In fiction
    - In literature
    - In film
    - On television
    - In video games
    - Other
    - Prospects for implementation
    - Bibliography
categories:
    - Self-organization
---
A self-replicating machine is a type of autonomous robot that is capable of reproducing itself autonomously using raw materials found in the environment, thus exhibiting self-replication in a way analogous to that found in nature. The concept of self-replicating machines has been advanced and examined by Homer Jacobsen, Edward F. Moore, Freeman Dyson, John von Neumann and in more recent times by K. Eric Drexler in his book on nanotechnology, Engines of Creation and by Robert Freitas and Ralph Merkle in their review Kinematic Self-Replicating Machines[1] which provided the first comprehensive analysis of the entire replicator design space. The future development of such technology is an ...
