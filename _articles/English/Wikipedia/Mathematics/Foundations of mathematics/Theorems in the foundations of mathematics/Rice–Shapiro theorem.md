---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Rice%E2%80%93Shapiro_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 31336b35-20b9-41f0-8f25-05bb4bc49003
updated: 1484308999
title: Rice–Shapiro theorem
categories:
    - Theorems in the foundations of mathematics
---
Let A be a set of partial-recursive unary functions on the domain of natural numbers such that the set 
  
    
      
        {
        n
        ∣
        
          φ
          
            n
          
        
        ∈
        A
        }
      
    
    {\displaystyle \{n\mid \varphi _{n}\in A\}}
  
 is recursively enumerable, where 
  
    
      
        
          φ
          
            n
          
        
      
    
    {\displaystyle \varphi _{n}}
  
 denotes the 
  
    
      
        n
      
    
    {\displaystyle n}
  
-th partial-recursive function in a Gödel numbering.
