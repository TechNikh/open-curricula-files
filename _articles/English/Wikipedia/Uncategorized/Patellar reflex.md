---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Knee_jerk
offline_file: ""
offline_thumbnail: ""
uuid: 7bf0eef3-6c9d-417c-8fe6-272a3263d923
updated: 1484308366
title: Patellar reflex
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Patellar_tendon_reflex_arc.png
tags:
    - Mechanism
    - Purpose of testing
    - History
    - Popular culture
    - Notes
categories:
    - Uncategorized
---
