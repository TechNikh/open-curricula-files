---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swami
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484369881
title: swami
categories:
    - Dictionary
---
swami
     n : a Hindu religious teacher; used as a title of respect
     [also: {swamies} (pl)]
