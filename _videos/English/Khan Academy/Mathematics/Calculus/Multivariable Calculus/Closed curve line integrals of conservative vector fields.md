---
version: 1
type: video
provider: YouTube
id: I2dbzp0zHuw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Closed%20curve%20line%20integrals%20of%20conservative%20vector%20fields.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 014ff08a-6f13-418b-ad71-d15d0230e2e8
updated: 1486069689
title: Closed curve line integrals of conservative vector fields
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/I2dbzp0zHuw/default.jpg
    - https://i3.ytimg.com/vi/I2dbzp0zHuw/1.jpg
    - https://i3.ytimg.com/vi/I2dbzp0zHuw/2.jpg
    - https://i3.ytimg.com/vi/I2dbzp0zHuw/3.jpg
---
