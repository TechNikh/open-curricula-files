---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Independence_system
offline_file: ""
offline_thumbnail: ""
uuid: b234771b-d71e-42be-b411-f8a3de04870d
updated: 1484308573
title: Independence system
categories:
    - Combinatorics
---
In combinatorial mathematics, an independence system S is a pair (E, I), where E is a finite set and I is a collection of subsets of E (called the independent sets) with the following properties:
