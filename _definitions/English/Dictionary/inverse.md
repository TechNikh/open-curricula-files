---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inverse
offline_file: ""
offline_thumbnail: ""
uuid: 44d4fbab-7dab-4b69-b8d4-ffaa6831a806
updated: 1484310142
title: inverse
categories:
    - Dictionary
---
inverse
     adj 1: reversed (turned backward) in order or nature or effect
            [syn: {reverse}]
     2: opposite in nature or effect or relation to another quantity
        ; "a term is in inverse proportion to another term if it
        increases (or decreases) as the other decreases (or
        increases)" [ant: {direct}]
     n : something inverted in sequence or character or effect; "when
         the direct approach failed he tried the inverse" [syn: {opposite}]
