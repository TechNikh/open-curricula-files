---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recover
offline_file: ""
offline_thumbnail: ""
uuid: a89d5f9d-70e2-4229-b3a2-9b1e45ed4a1d
updated: 1484310522
title: recover
categories:
    - Dictionary
---
recover
     v 1: get or find back; recover the use of; "She regained control
          of herself"; "She found her voice and replied quickly"
          [syn: {retrieve}, {find}, {regain}]
     2: get over an illness or shock; "The patient is recuperating"
        [syn: {recuperate}, {convalesce}] [ant: {devolve}]
     3: regain a former condition after a financial loss; "We expect
        the stocks to recover to $2.90"; "The company managed to
        recuperate" [syn: {go back}, {recuperate}]
     4: regain or make up for; "recuperate one's losses" [syn: {recoup},
         {recuperate}]
     5: of materials from waste products [syn: {reclaim}]
     6: cover anew; "recover a chair"
