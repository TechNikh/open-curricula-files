---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bankoff_circle
offline_file: ""
offline_thumbnail: ""
uuid: 6b22740b-1af1-4aff-8cbc-d5404a085d8e
updated: 1484308927
title: Bankoff circle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Bankoff_Circle.svg.png
tags:
    - Construction
    - Radius of the circle
categories:
    - Elementary geometry
---
In geometry, the Bankoff circle or Bankoff triplet circle is a certain Archimedean circle that can be constructed from an arbelos; an Archimedean circle is any circle with area equal to each of Archimedes' twin circles. The Bankoff circle was first constructed by Leon Bankoff.[1][2][3]
