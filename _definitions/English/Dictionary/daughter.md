---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/daughter
offline_file: ""
offline_thumbnail: ""
uuid: 4ea2c606-f252-4272-a799-7d92e1f349ef
updated: 1484310279
title: daughter
categories:
    - Dictionary
---
daughter
     n : a female human offspring; "her daughter cared for her in her
         old age" [syn: {girl}] [ant: {son}, {son}]
