---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flown
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484590261
title: flown
categories:
    - Dictionary
---
fly
     adj : (British informal) not to be deceived or hoodwinked
     n 1: two-winged insects characterized by active flight
     2: flap consisting of a piece of canvas that can be drawn back
        to provide entrance to a tent [syn: {tent-fly}, {rainfly},
         {fly sheet}, {tent flap}]
     3: an opening in a garment that is closed by a zipper or
        buttons concealed by a fold of cloth [syn: {fly front}]
     4: (baseball) a hit that flies up in the air [syn: {fly ball}]
     5: fisherman's lure consisting of a fishhook decorated to look
        like an insect
     v 1: travel through the air; be airborne; "Man cannot fly" [syn:
          {wing}]
     2: move quickly or ...
