---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484507761
title: nest
categories:
    - Dictionary
---
nest
     n 1: a structure in which animals lay eggs or give birth to their
          young
     2: a kind of gun emplacement; "a machine-gun nest"; "a nest of
        snipers"
     3: a cosy or secluded retreat
     4: a gang of people (criminals or spies or terrorists)
        assembled in one locality; "a nest of thieves"
     5: furniture pieces made to fit close together
     v 1: inhabit a nest, usually after building; "birds are nesting
          outside my window every Spring"
     2: fit together or fit inside; "nested bowls"
     3: move or arrange oneself in a comfortable and cozy position;
        "We cuddled against each other to keep warm"; "The
        children snuggled into ...
