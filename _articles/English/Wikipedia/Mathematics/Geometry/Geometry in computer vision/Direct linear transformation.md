---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Direct_linear_transformation
offline_file: ""
offline_thumbnail: ""
uuid: 003b994c-ff04-4de3-a4db-48017b0a6d6d
updated: 1484309184
title: Direct linear transformation
tags:
    - Introduction
    - Example
    - More general cases
    - Example p = 3
categories:
    - Geometry in computer vision
---
where 
  
    
      
        
          
            x
          
          
            k
          
        
      
    
    {\displaystyle \mathbf {x} _{k}}
  
 and 
  
    
      
        
          
            y
          
          
            k
          
        
      
    
    {\displaystyle \mathbf {y} _{k}}
  
 are known vectors, 
  
    
      
        
        ∝
      
    
    {\displaystyle \,\propto }
  
 denotes equality up to an unknown scalar multiplication, and 
  
    
      
        
          A
        
      
    
    {\displaystyle \mathbf {A} }
  
 is a matrix (or linear transformation) which contains the unknowns to be solved.
