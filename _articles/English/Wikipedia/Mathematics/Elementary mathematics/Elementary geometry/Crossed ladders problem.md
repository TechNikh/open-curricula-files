---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crossed_ladders_problem
offline_file: ""
offline_thumbnail: ""
uuid: 3f6d3489-2a15-4586-b46e-b724bf9fb8c9
updated: 1484308935
title: Crossed ladders problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-CrossedLadders.svg.png
tags:
    - The problem
    - Solution
    - First method
    - Second method
    - Integer solutions
categories:
    - Elementary geometry
---
The crossed ladders problem is a puzzle of unknown origin that has appeared in various publications and regularly reappears in Web pages and Usenet discussions.
