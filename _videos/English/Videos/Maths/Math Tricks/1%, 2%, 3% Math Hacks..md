---
version: 1
type: video
provider: YouTube
id: 58ml5oMTXLc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b78260a0-e79e-42dc-a3fc-d1915f1ef809
updated: 1486069625
title: '1%, 2%, 3% Math Hacks.'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/58ml5oMTXLc/default.jpg
    - https://i3.ytimg.com/vi/58ml5oMTXLc/1.jpg
    - https://i3.ytimg.com/vi/58ml5oMTXLc/2.jpg
    - https://i3.ytimg.com/vi/58ml5oMTXLc/3.jpg
---
