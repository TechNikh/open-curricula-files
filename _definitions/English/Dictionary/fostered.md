---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fostered
offline_file: ""
offline_thumbnail: ""
uuid: 76edfbd9-c221-4bdb-be81-8867a4973702
updated: 1484310549
title: fostered
categories:
    - Dictionary
---
fostered
     adj 1: encouraged or promoted in growth or development; "dreams of
            liberty nourished by the blood of patriots cannot
            easily be given up" [syn: {nourished}]
     2: provided with parental care and nurture especially by a
        surrogate or surrogates
