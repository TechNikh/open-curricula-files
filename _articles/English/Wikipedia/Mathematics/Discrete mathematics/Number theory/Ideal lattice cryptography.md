---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ideal_lattice_cryptography
offline_file: ""
offline_thumbnail: ""
uuid: de945796-65f1-42dd-a8c1-1dac339dc1e0
updated: 1484308645
title: Ideal lattice cryptography
tags:
    - Introduction
    - Definition
    - Notation
    - Related properties
    - Use in cryptography
    - Efficient collision resistant hash functions
    - Digital signatures
    - The SWIFFT hash function
    - Learning with errors (LWE)
    - Ring-LWE
    - Ideal-LWE
    - Fully homomorphic encryption
categories:
    - Number theory
---
Ideal lattices are a special class of lattices and a generalization of cyclic lattices.[1] Ideal lattices naturally occur in many parts of number theory, but also in other areas. In particular, they have a significant place in cryptography. Micciancio defined a generalization of cyclic lattices as ideal lattices. They can be used in cryptosystems to decrease by a square root the number of parameters necessary to describe a lattice, making them more efficient. Ideal lattices are a new concept, but similar lattice classes have been used for a long time. For example cyclic lattices, a special case of ideal lattices, are used in NTRUEncrypt and NTRUSign.
