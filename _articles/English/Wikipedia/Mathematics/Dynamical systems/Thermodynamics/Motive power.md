---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Motive_power
offline_file: ""
offline_thumbnail: ""
uuid: df1f3217-682e-4c9f-a91b-04188ee57656
updated: 1484308855
title: Motive power
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Coaltub.gif
tags:
    - History
    - 1824 definition
    - 1834 definition
categories:
    - Thermodynamics
---
In thermodynamics, motive power is a natural agent, such as water or steam, wind or electricity, used to impart motion to machinery such as an engine. Motive power may also be a locomotive or a motor, which provides motive power to a system. Motive power may be thought of as a synonym for either "work", i.e. force times distance, or "power".
