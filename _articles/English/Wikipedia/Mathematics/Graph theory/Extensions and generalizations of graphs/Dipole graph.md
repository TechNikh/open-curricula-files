---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dipole_graph
offline_file: ""
offline_thumbnail: ""
uuid: 511845eb-06ec-4a85-b0f0-327bbd573473
updated: 1484309408
title: Dipole graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/140px-Dipole_graph.svg.png
categories:
    - Extensions and generalizations of graphs
---
In graph theory, a dipole graph (also called a dipole or bond graph) is a multigraph consisting of two vertices connected with a number of parallel edges. A dipole graph containing n edges is called the order-n dipole graph, and is denoted by Dn. The order-n dipole graph is dual to the cycle graph Cn.
