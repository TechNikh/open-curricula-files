---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Grundy%27s_game'
offline_file: ""
offline_thumbnail: ""
uuid: 77fed88e-fb74-4503-b2ff-88a4c41b5a6b
updated: 1484308535
title: "Grundy's game"
tags:
    - Illustration
    - Mathematical theory
categories:
    - Combinatorial game theory
---
Grundy's game is a two-player mathematical game of strategy. The starting configuration is a single heap of objects, and the two players take turn splitting a single heap into two heaps of different sizes. The game ends when only heaps of size two and smaller remain, none of which can be split unequally. The game is usually played as a normal play game, which means that the last person who can make an allowed move wins.
