---
version: 1
type: article
id: https://en.wikipedia.org/wiki/N-player_game
offline_file: ""
offline_thumbnail: ""
uuid: 28d16653-527b-4dee-ab09-3cb48a2ae65e
updated: 1484309032
title: N-player game
categories:
    - Game theory game classes
---
In game theory, an n-player game is a game which is well defined for any number of players. This is usually used in contrast to standard 2-player games that are only specified for two players. In defining n-player games, game theorists usually provide a definition that allow for any (finite) number of players. [1]
