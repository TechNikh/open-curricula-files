---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Simplex_graph
offline_file: ""
offline_thumbnail: ""
uuid: b95cbd8c-7ebd-4b00-8ec7-7fad9f0c0f61
updated: 1484309450
title: Simplex graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Simplex_graph.svg.png
categories:
    - Graph operations
---
In graph theory, a branch of mathematics, the simplex graph κ(G) of an undirected graph G is itself a graph, with one node for each clique (a set of mutually adjacent vertices) in G. Two nodes of κ(G) are linked by an edge whenever the corresponding two cliques differ in the presence or absence of a single vertex.
