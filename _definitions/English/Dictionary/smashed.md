---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smashed
offline_file: ""
offline_thumbnail: ""
uuid: 8367e1f3-332c-4f5d-a7cb-fea29d2304cc
updated: 1484310587
title: smashed
categories:
    - Dictionary
---
smashed
     adj 1: broken into sharp pieces; "shattered glass"; "your
            eyeglasses are smashed"; "the police came in through
            the splintered door" [syn: {shattered}, {splintered}]
     2: very drunk [syn: {besotted}, {blind drunk}, {blotto}, {crocked},
         {cockeyed}, {fuddled}, {loaded}, {pie-eyed}, {pissed}, {pixilated},
         {plastered}, {potty}, {slopped}, {sloshed}, {soaked}, {soused},
         {sozzled}, {squiffy}, {stiff}, {tiddly}, {tiddley}, {tight},
         {tipsy}, {wet}]
