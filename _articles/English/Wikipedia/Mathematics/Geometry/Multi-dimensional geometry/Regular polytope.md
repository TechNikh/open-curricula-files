---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Regular_polytope
offline_file: ""
offline_thumbnail: ""
uuid: 4687cf9f-afac-4321-a7aa-c001d24b61a6
updated: 1484309287
title: Regular polytope
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-8-cell-simple.gif
tags:
    - Classification and description
    - Schläfli symbols
    - Duality of the regular polytopes
    - Regular simplices
    - Measure polytopes (hypercubes)
    - Cross polytopes (orthoplexes)
    - History of discovery
    - Convex polygons and polyhedra
    - Star polygons and polyhedra
    - Higher-dimensional polytopes
    - Apeirotopes — infinite polytopes
    - Regular complex polytopes
    - Abstract polytopes
    - Regularity of abstract polytopes
    - Vertex figure of abstract polytopes
    - Constructions
    - Polygons
    - Polyhedra
    - Higher dimensions
    - Regular polytopes in nature
    - Notes
    - Bibliography
categories:
    - Multi-dimensional geometry
---
In mathematics, a regular polytope is a polytope whose symmetry group acts transitively on its flags, thus giving it the highest degree of symmetry. All its elements or j-faces (for all 0 ≤ j ≤ n, where n is the dimension of the polytope) — cells, faces and so on — are also transitive on the symmetries of the polytope, and are regular polytopes of dimension ≤ n.
