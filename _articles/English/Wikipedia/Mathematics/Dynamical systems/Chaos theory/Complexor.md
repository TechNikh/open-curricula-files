---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complexor
offline_file: ""
offline_thumbnail: ""
uuid: 0ba027df-aefd-453f-b7fb-6b1eae6196f8
updated: 1484308707
title: Complexor
categories:
    - Chaos theory
---
The word complexor was coined by Marcial Losada (Losada & Heaphy, 2004), derived from the words "complex order," to refer to chaotic attractors that are strange and thus have fractal structure (in contrast to fixed point or limit cycle attractors).
