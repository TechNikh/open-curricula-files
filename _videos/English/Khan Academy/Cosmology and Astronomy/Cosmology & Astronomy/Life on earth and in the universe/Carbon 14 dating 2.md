---
version: 1
type: video
provider: YouTube
id: 4YUtnod-YuE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Life%20on%20earth%20and%20in%20the%20universe/Carbon%2014%20dating%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3eebe180-05c9-4fe6-b94a-8321d2b9988d
updated: 1486069685
title: Carbon 14 dating 2
categories:
    - Life on earth and in the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/4YUtnod-YuE/default.jpg
    - https://i3.ytimg.com/vi/4YUtnod-YuE/1.jpg
    - https://i3.ytimg.com/vi/4YUtnod-YuE/2.jpg
    - https://i3.ytimg.com/vi/4YUtnod-YuE/3.jpg
---
