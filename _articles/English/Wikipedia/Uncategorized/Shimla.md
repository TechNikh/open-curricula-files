---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Simla
offline_file: ""
offline_thumbnail: ""
uuid: 5ce30143-2cc8-4d50-a7e8-9ef5b1caf46f
updated: 1484308415
title: Shimla
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Rashtrapati_Niwas%252C_Shimla.jpg'
tags:
    - History
    - geography
    - Climate
    - Economy
    - Civic administration
    - Demographics
    - culture
    - Education
    - Places of interest
    - Transport
    - Road
    - air
    - Rail
    - Media and communications
    - Notable people born in Shimla
    - Notable people educated in Shimla
    - Gallery
    - Sister cities
categories:
    - Uncategorized
---
Shimla (Hindi: शिमला; Punjabi: ਸ਼ਿਮਲਾ; English pronunciation: /ˈʃɪmlə/; Hindi: [ˈʃɪmlaː] ( listen)), also known as Simla, is the capital and largest city of the northern Indian state of Himachal Pradesh. Shimla is also a district which is bounded by Mandi and Kullu in the north, Kinnaur in the east, the state of Uttarakhand in the south-east, and Solan and Sirmaur. In 1864, Shimla was declared as the summer capital of British India, succeeding Murree, northeast of Rawalpindi. After independence, the city became the capital of Punjab and was later named the capital of Himachal Pradesh. It is the principal commercial, cultural and educational centre of the ...
