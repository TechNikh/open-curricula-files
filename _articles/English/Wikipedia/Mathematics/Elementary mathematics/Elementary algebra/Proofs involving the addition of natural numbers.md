---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Proofs_involving_the_addition_of_natural_numbers
offline_file: ""
offline_thumbnail: ""
uuid: ba95e1c7-8b6f-4627-8413-ce716f3183c4
updated: 1484308905
title: Proofs involving the addition of natural numbers
tags:
    - Definitions
    - Proof of associativity
    - Proof of identity element
    - Proof of commutativity
categories:
    - Elementary algebra
---
This article contains mathematical proofs for some properties of addition of the natural numbers: the additive identity, commutativity, and associativity. These proofs are used in the article Addition of natural numbers.
