---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prompting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484457601
title: prompting
categories:
    - Dictionary
---
prompting
     n 1: persuasion formulated as a suggestion [syn: {suggestion}]
     2: a cue given to a performer (usually the beginning of the
        next line to be spoken); "the audience could hear his
        prompting" [syn: {prompt}]
