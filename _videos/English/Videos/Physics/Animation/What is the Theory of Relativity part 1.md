---
version: 1
type: video
provider: YouTube
id: yqlGRF_a6qM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20the%20Theory%20of%20Relativity%20part%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a1e96245-4e0e-480e-9929-769dd85c610c
updated: 1486069653
title: What is the Theory of Relativity part 1
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/yqlGRF_a6qM/default.jpg
    - https://i3.ytimg.com/vi/yqlGRF_a6qM/1.jpg
    - https://i3.ytimg.com/vi/yqlGRF_a6qM/2.jpg
    - https://i3.ytimg.com/vi/yqlGRF_a6qM/3.jpg
---
