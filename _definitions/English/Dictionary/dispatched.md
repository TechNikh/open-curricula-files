---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dispatched
offline_file: ""
offline_thumbnail: ""
uuid: c77a1b5b-f031-441c-aaa0-62411f727050
updated: 1484310573
title: dispatched
categories:
    - Dictionary
---
dispatched
     adj : sent off or away; "the dispatched messenger was has hardly
           out of sight before she wished call him back"
