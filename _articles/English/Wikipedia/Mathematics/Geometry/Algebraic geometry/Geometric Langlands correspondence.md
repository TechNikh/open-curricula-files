---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Geometric_Langlands_correspondence
offline_file: ""
offline_thumbnail: ""
uuid: 6da2ef1d-ad51-4fca-a1f5-f9f857ad82e1
updated: 1484309153
title: Geometric Langlands correspondence
categories:
    - Algebraic geometry
---
In mathematics, the classical Langlands correspondence is a collection of results and conjectures relating number theory to the branch of mathematics known as representation theory. Formulated by Robert Langlands in the late 1960s, the Langlands correspondence is related to important conjectures in number theory such as the Taniyama-Shimura conjecture, which includes Fermat's last theorem as a special case.[1]
