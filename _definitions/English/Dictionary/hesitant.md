---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hesitant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484624521
title: hesitant
categories:
    - Dictionary
---
hesitant
     adj 1: lacking decisiveness of character; unable to act or decide
            quickly or firmly; "stood irresolute waiting for some
            inspiration" [syn: {hesitating}, {irresolute}]
     2: acting with uncertainty or hesitance or lack of confidence;
        "a groping effort to understand" [syn: {groping}]
