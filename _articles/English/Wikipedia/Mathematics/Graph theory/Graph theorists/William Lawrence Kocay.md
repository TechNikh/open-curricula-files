---
version: 1
type: article
id: https://en.wikipedia.org/wiki/William_Lawrence_Kocay
offline_file: ""
offline_thumbnail: ""
uuid: 418d1720-9ee3-4ca9-bce7-8c907adc3e49
updated: 1484309470
title: William Lawrence Kocay
categories:
    - Graph theorists
---
William Lawrence Kocay is a Canadian professor at the department of computer science at St. Paul's College of the University of Manitoba and a graph theorist. He is known for his work in graph algorithms and the reconstruction conjecture and is affectionately referred to as "Wild Bill" by his students. Bill Kocay is a former managing editor (from Jan 1988 to May 1997) of Ars Combinatoria, a Canadian journal of combinatorial mathematics, is a founding fellow of the Institute of Combinatorics and its Applications.
