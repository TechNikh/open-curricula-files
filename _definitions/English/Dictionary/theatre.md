---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/theatre
offline_file: ""
offline_thumbnail: ""
uuid: 465ad545-b45e-4837-91ca-fc6cc6adf1af
updated: 1484310277
title: theatre
categories:
    - Dictionary
---
theatre
     n 1: a building where theatrical performances or motion-picture
          shows can be presented; "the house was full" [syn: {theater},
           {house}]
     2: the art of writing and producing plays [syn: {dramaturgy}, {dramatic
        art}, {dramatics}, {theater}]
     3: a region in which active military operations are in
        progress; "the army was in the field awaiting action"; "he
        served in the Vietnam theater for three years" [syn: {field},
         {field of operations}, {theater}, {theater of operations},
         {theatre of operations}]
