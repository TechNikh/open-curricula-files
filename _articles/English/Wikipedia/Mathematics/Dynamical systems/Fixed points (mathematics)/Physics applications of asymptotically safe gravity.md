---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Physics_applications_of_asymptotically_safe_gravity
offline_file: ""
offline_thumbnail: ""
uuid: 81f9a995-0738-439c-9faf-f33759e8bc9b
updated: 1484308786
title: Physics applications of asymptotically safe gravity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/266px-Stylised_atom_with_three_Bohr_model_orbits_and_stylised_nucleus.svg.png
tags:
    - Asymptotic safety and the parameters of the Standard Model
    - The mass of the Higgs boson
    - The fine structure constant
    - Asymptotic safety in astrophysics and cosmology
categories:
    - Fixed points (mathematics)
---
The asymptotic safety approach to quantum gravity provides a nonperturbative notion of renormalization in order to find a consistent and predictive quantum field theory of the gravitational interaction and spacetime geometry. It is based upon a nontrivial fixed point of the corresponding renormalization group (RG) flow such that the running coupling constants approach this fixed point in the ultraviolet (UV) limit. This suffices to avoid divergences in physical observables. Moreover, it has predictive power: Generically an arbitrary starting configuration of coupling constants given at some RG scale does not run into the fixed point for increasing scale, but a subset of configurations might ...
