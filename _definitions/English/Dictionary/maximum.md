---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maximum
offline_file: ""
offline_thumbnail: ""
uuid: 57476c6b-31b5-40cf-876a-2277b39a5022
updated: 1484310262
title: maximum
categories:
    - Dictionary
---
maximum
     adj : the greatest or most complete or best possible; "maximal
           expansion"; "maximum pressure" [syn: {maximal}] [ant: {minimal},
            {minimal}]
     n 1: the largest possible quantity [syn: {upper limit}] [ant: {minimum}]
     2: the greatest possible degree; "he tried his utmost" [syn: {utmost},
         {uttermost}, {level best}]
     3: the point on a curve where the tangent changes from positive
        on the left to negative on the right [ant: {minimum}]
     [also: {maxima} (pl)]
