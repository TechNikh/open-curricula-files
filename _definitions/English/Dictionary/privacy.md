---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/privacy
offline_file: ""
offline_thumbnail: ""
uuid: 7aae10a3-6357-4557-8c68-f3f7d223b4da
updated: 1484310166
title: privacy
categories:
    - Dictionary
---
privacy
     n 1: the quality of being secluded from the presence or view of
          others [syn: {privateness}, {seclusion}]
     2: the condition of being concealed or hidden [syn: {privateness},
         {secrecy}, {concealment}]
