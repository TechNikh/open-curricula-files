---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biodiversity
offline_file: ""
offline_thumbnail: ""
uuid: 097ca8d2-22d4-4ee1-90a7-063219f651e9
updated: 1484310244
title: biodiversity
categories:
    - Dictionary
---
biodiversity
     n : the diversity of plant and animal life in a particular
         habitat (or in the world as a whole); "a high level of
         biodiversity is desirable"
