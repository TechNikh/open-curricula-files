---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Himachal
offline_file: ""
offline_thumbnail: ""
uuid: 3b720d1b-ee2e-4bc5-bc61-638bcf1ec62b
updated: 1484308415
title: Himachal Pradesh
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/218px-ROCK_CUT_TEMPLES%252C_MASROOR.JPG'
tags:
    - History
    - Geography and Climate
    - Flora and Fauna
    - government
    - Administrative divisions
    - Economy
    - Agriculture
    - Heritage
    - Tourism
    - Transportation
    - Demographics
    - population
    - Languages
    - religion
    - culture
    - Notable people
    - Education
    - State profile
    - Notes
categories:
    - Uncategorized
---
Himachal Pradesh ([ɦɪmaːtʃəl prəd̪eːʃ] ( listen); literally "Snow-laden Province") is a state of India located in Northern India. It is bordered by Jammu and Kashmir on the north, Punjab and Chandigarh on the west, Haryana on the south-west, Uttarakhand on the south-east and by the Tibet Autonomous Region on the east. The name was coined from Sanskrit him 'snow' and anchal 'lap', by Acharya Diwakar Datt Sharma, one of the state's most eminent Sanskrit scholars.[7]
