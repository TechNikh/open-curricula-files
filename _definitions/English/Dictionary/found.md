---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/found
offline_file: ""
offline_thumbnail: ""
uuid: 243102df-19b6-4b36-ae76-7290783ad53c
updated: 1484310344
title: found
categories:
    - Dictionary
---
find
     n 1: a productive insight [syn: {discovery}, {breakthrough}]
     2: the act of discovering something [syn: {discovery}, {uncovering}]
     v 1: come upon, as if by accident; meet with; "We find this idea
          in Plato"; "I happened upon the most wonderful bakery
          not very far from here"; "She chanced upon an
          interesting book in the bookstore the other day" [syn: {happen},
           {chance}, {bump}, {encounter}]
     2: discover or determine the existence, presence, or fact of;
        "She detected high levels of lead in her drinking water";
        "We found traces of lead in the paint" [syn: {detect}, {observe},
         {discover}, {notice}]
     3: ...
