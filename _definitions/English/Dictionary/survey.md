---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/survey
offline_file: ""
offline_thumbnail: ""
uuid: e6972d9b-d952-4ed5-877f-75913e4b93f7
updated: 1484310290
title: survey
categories:
    - Dictionary
---
survey
     n 1: a detailed critical inspection [syn: {study}]
     2: short descriptive summary (of events) [syn: {sketch}, {resume}]
     3: the act of looking or seeing or observing; "he tried to get
        a better view of it"; "his survey of the battlefield was
        limited" [syn: {view}, {sight}]
     v 1: consider in a comprehensive way; "He appraised the situation
          carefully before acting" [syn: {appraise}]
     2: look over in a comprehensively, inspect; "He surveyed his
        new classmates"
     3: keep under surveillance; "The police had been following him
        for weeks but they could not prove his involvement in the
        bombing" [syn: {surveil}, ...
