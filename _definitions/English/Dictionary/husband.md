---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/husband
offline_file: ""
offline_thumbnail: ""
uuid: 7114df0e-3749-4f39-980c-16500a58dd67
updated: 1484310518
title: husband
categories:
    - Dictionary
---
husband
     n : a married man; a woman's partner in marriage [syn: {hubby},
         {married man}] [ant: {wife}]
     v : use cautiously and frugally; "I try to economize my spare
         time"; "conserve your energy for the ascent to the
         summit" [syn: {conserve}, {economize}, {economise}] [ant:
          {waste}]
