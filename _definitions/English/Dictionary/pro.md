---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pro
offline_file: ""
offline_thumbnail: ""
uuid: ba3ce17d-5756-4348-b364-6bb377f1f701
updated: 1484310593
title: pro
categories:
    - Dictionary
---
pro
     adj : in favor of (an action or proposal etc.); "a pro vote" [ant:
            {anti}]
     n 1: an athlete who plays for pay [syn: {professional}] [ant: {amateur}]
     2: an argument in favor of a proposal [ant: {con}]
     adv : on the affirmative side [ant: {con}]
