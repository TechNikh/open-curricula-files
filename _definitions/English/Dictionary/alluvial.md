---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alluvial
offline_file: ""
offline_thumbnail: ""
uuid: a3a29df9-1d40-41bd-b795-b019c5d0adf0
updated: 1484310435
title: alluvial
categories:
    - Dictionary
---
alluvial
     adj : of or relating to alluvium
