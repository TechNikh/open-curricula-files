---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Solid_partition
offline_file: ""
offline_thumbnail: ""
uuid: cc75966d-4482-4328-a51e-255bca14d6d4
updated: 1484308599
title: Solid partition
tags:
    - Ferrers diagrams for solid partitions
    - Equivalence of the two representations
    - Generating function
    - Exact enumeration using computers
    - Asymptotic behavior
categories:
    - Combinatorics
---
In mathematics, solid partitions are natural generalizations of partitions and plane partitions defined by Percy Alexander MacMahon.[1] A solid partition of 
  
    
      
        n
      
    
    {\displaystyle n}
  
 is a three-dimensional array, 
  
    
      
        
          n
          
            i
            ,
            j
            ,
            k
          
        
      
    
    {\displaystyle n_{i,j,k}}
  
, of non-negative integers (the indices 
  
    
      
        i
        ,
         
        j
        ,
         
        k
        ≥
        1
      
    
    {\displaystyle i,\ j,\ k\geq 1}
  
) such that
