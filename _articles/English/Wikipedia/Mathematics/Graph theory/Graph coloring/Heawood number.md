---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heawood_number
offline_file: ""
offline_thumbnail: ""
uuid: f5492ede-16b7-4fe0-9e0e-4c4e6afeaae2
updated: 1484309382
title: Heawood number
categories:
    - Graph coloring
---
In mathematics, the Heawood number of a surface is a certain upper bound for the maximal number of colors needed to color any graph embedded in the surface.
