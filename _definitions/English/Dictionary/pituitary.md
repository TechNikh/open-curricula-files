---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pituitary
offline_file: ""
offline_thumbnail: ""
uuid: 64eac186-f56c-4e68-8b7b-a94e44b57428
updated: 1484310162
title: pituitary
categories:
    - Dictionary
---
pituitary
     adj 1: of or relating to the pituitary gland; "pituitary hormone"
     2: having abnormal size with overgrown extremities resulting
        from abnormal pituitary secretion; "a protruding
        acromegalic jaw"; "a pituitary dwarf" [syn: {acromegalic}]
     n : the master gland of the endocrine system; located at the
         base of the brain [syn: {pituitary gland}, {pituitary
         body}, {hypophysis}]
