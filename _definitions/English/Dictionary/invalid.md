---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invalid
offline_file: ""
offline_thumbnail: ""
uuid: 50eb55aa-bb91-4b8e-a754-992f4f3154e0
updated: 1484310181
title: invalid
categories:
    - Dictionary
---
invalid
     adj 1: having no cogency or legal force; "invalid reasoning"; "an
            invalid driver's license" [ant: {valid}]
     2: no longer valid; "the license is invalid"
     n : someone who is incapacitated by a chronic illness or injury
         [syn: {shut-in}]
     v 1: force to retire, remove from active duty, as of firemen
     2: injure permanently; "He was disabled in a car accident"
        [syn: {disable}, {incapacitate}, {handicap}]
