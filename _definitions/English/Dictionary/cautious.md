---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cautious
offline_file: ""
offline_thumbnail: ""
uuid: 9e775456-1344-46f1-a78b-0b12fcc58c16
updated: 1484310451
title: cautious
categories:
    - Dictionary
---
cautious
     adj 1: showing careful forethought; "reserved and cautious; never
            making swift decisions"; "a cautious driver" [ant: {incautious}]
     2: avoiding excess; "a conservative estimate" [syn: {conservative}]
     3: cautious in attitude and careful in actions; prudent; "a
        cautious answer"; "very cautious about believing
        everything she was told"
     4: unwilling to take risks
     n : people who are fearful and cautious; "whitewater rafting is
         not for the timid" [syn: {timid}] [ant: {brave}]
