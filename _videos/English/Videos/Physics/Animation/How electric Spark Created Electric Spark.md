---
version: 1
type: video
provider: YouTube
id: Wjj2vuX8uzE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/How%20electric%20Spark%20Created%20Electric%20Spark.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1c3c776d-47a9-4695-b768-1ef6dddf557e
updated: 1486069653
title: How electric Spark Created Electric Spark
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Wjj2vuX8uzE/default.jpg
    - https://i3.ytimg.com/vi/Wjj2vuX8uzE/1.jpg
    - https://i3.ytimg.com/vi/Wjj2vuX8uzE/2.jpg
    - https://i3.ytimg.com/vi/Wjj2vuX8uzE/3.jpg
---
