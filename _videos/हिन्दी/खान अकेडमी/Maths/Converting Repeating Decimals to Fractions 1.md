---
version: 1
type: video
provider: YouTube
id: mpXx_W5wTwA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4a29332b-1afe-4738-b124-985898f9456d
updated: 1486069616
title: Converting Repeating Decimals to Fractions 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/mpXx_W5wTwA/default.jpg
    - https://i3.ytimg.com/vi/mpXx_W5wTwA/1.jpg
    - https://i3.ytimg.com/vi/mpXx_W5wTwA/2.jpg
    - https://i3.ytimg.com/vi/mpXx_W5wTwA/3.jpg
---
