---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hermitian_symmetric_space
offline_file: ""
offline_thumbnail: ""
uuid: 987691c4-0381-4ae2-90ad-723060a5ab6f
updated: 1484309224
title: Hermitian symmetric space
tags:
    - Hermitian symmetric spaces of compact type
    - Definition
    - Symmetry and center of isotropy subgroup
    - Irreducible decomposition
    - Complex structure
    - Classification
    - Classical examples
    - Hermitian symmetric spaces of noncompact type
    - Definition
    - Borel embedding
    - Cartan decomposition
    - Strongly orthogonal roots
    - Polysphere and polydisk theorem
    - Harish-Chandra embedding
    - Bounded symmetric domains
    - Classification
    - Classical domains
    - Boundary components
    - Geometric properties
    - Jordan algebras
    - Notes
categories:
    - Differential geometry
---
In mathematics, a Hermitian symmetric space is a Hermitian manifold which at every point has as an inversion symmetry preserving the Hermitian structure. First studied by Élie Cartan, they form a natural generalization of the notion of Riemannian symmetric space from real manifolds to complex manifolds.
