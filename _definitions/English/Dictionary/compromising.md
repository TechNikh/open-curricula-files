---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compromising
offline_file: ""
offline_thumbnail: ""
uuid: a119dc59-9b35-4338-9693-adebaace78d6
updated: 1484310541
title: compromising
categories:
    - Dictionary
---
compromising
     adj 1: making or willing to make concessions; "loneliness tore
            through him...whenever he thought of...even the
            compromising Louis du Tillet" [syn: {conciliatory}, {flexible}]
            [ant: {uncompromising}]
     2: vulnerable to danger especially of discredit or suspicion;
        "she found herself in a compromising situation"
