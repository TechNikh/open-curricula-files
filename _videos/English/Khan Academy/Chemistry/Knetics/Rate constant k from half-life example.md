---
version: 1
type: video
provider: YouTube
id: dqvoAwA9nUQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Knetics/Rate%20constant%20k%20from%20half-life%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b59bb769-ec91-4715-a4f5-e99835fb06b9
updated: 1486069589
title: Rate constant k from half-life example
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Knetics
thumbnail_urls:
    - https://i3.ytimg.com/vi/dqvoAwA9nUQ/default.jpg
    - https://i3.ytimg.com/vi/dqvoAwA9nUQ/1.jpg
    - https://i3.ytimg.com/vi/dqvoAwA9nUQ/2.jpg
    - https://i3.ytimg.com/vi/dqvoAwA9nUQ/3.jpg
---
Example problem showing how to find the rate constant k from the half-life of a first order reaction.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/chem-kinetics/copy-of-kinetics/v/second-order-reaction-with-calculus?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/chem-kinetics/copy-of-kinetics/v/first-order-reaction-example?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
