---
version: 1
type: video
provider: YouTube
id: IYdiKeQ9xEI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Introduction%20to%20Random%20Variables.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 04c840d3-44d3-43f2-a7c4-96271c43f86f
updated: 1486069688
title: Introduction to Random Variables
categories:
    - Probability
thumbnail_urls:
    - https://i3.ytimg.com/vi/IYdiKeQ9xEI/default.jpg
    - https://i3.ytimg.com/vi/IYdiKeQ9xEI/1.jpg
    - https://i3.ytimg.com/vi/IYdiKeQ9xEI/2.jpg
    - https://i3.ytimg.com/vi/IYdiKeQ9xEI/3.jpg
---
