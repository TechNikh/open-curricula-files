---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deforest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484600101
title: deforest
categories:
    - Dictionary
---
De Forest
     n : United States electrical engineer who in 1907 patented the
         first triode vacuum tube, which made it possible to
         detect and amplify radio waves (1873-1961) [syn: {Lee De
         Forest}, {The Father of Radio}]
