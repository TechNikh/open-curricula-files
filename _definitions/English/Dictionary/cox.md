---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cox
offline_file: ""
offline_thumbnail: ""
uuid: babb3894-70fc-48b7-9bbe-6701ebe36f53
updated: 1484310421
title: cox
categories:
    - Dictionary
---
Cox
     n 1: either of two related enzymes that control the production of
          prostaglandins and are blocked by aspirin [syn: {cyclooxygenase}]
     2: the helmsman of a ship's boat or a racing crew [syn: {coxswain}]
     v : act as the coxswain, in a boat race
