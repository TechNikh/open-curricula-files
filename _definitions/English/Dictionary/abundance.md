---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abundance
offline_file: ""
offline_thumbnail: ""
uuid: 7c8b6105-4a0a-432d-8140-825273d2753d
updated: 1484310290
title: abundance
categories:
    - Dictionary
---
abundance
     n 1: the property of a more than adequate quantity or supply; "an
          age of abundance" [syn: {copiousness}, {teemingness}]
          [ant: {scarcity}]
     2: (physics) the ratio of the number of atoms of a specific
        isotope of an element to the total number of isotopes
        present
     3: (chemistry) the ratio of the total mass of an element in the
        earth's crust to the total mass of the earth's crust;
        expressed as a percentage or in parts per million
