---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/internal
offline_file: ""
offline_thumbnail: ""
uuid: 1f9f7a63-2f89-4d22-8f1b-5e8eee782687
updated: 1484310327
title: internal
categories:
    - Dictionary
---
internal
     adj 1: happening or arising or located within some limits or
            especially surface; "internal organs"; "internal
            mechanism of a toy"; "internal party maneuvering"
            [ant: {external}]
     2: occurring within an institution or community; "intragroup
        squabbling within the corporation" [syn: {intragroup}]
     3: inside the country; "the British Home Office has broader
        responsibilities than the United States Department of the
        Interior"; "the nation's internal politics" [syn: {home(a)},
         {interior(a)}, {national}]
     4: located inward; "Beethoven's manuscript looks like a bloody
        record of a tremendous inner ...
