---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aqua
offline_file: ""
offline_thumbnail: ""
uuid: e4cb350d-34e6-4da6-b291-b08f090b9fac
updated: 1484310581
title: aqua
categories:
    - Dictionary
---
aqua
     n : a shade of blue tinged with green [syn: {greenish blue}, {aquamarine},
          {turquoise}, {cobalt blue}, {peacock blue}]
     [also: {aquae} (pl)]
