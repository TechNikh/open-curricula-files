---
version: 1
type: video
provider: YouTube
id: DFVQdPVMrIc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Three-layer%20colored%20liquid%20-%20Beautiful%20chemical%20experiment%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bf2f7bb3-2808-4aba-bbdf-11555105336e
updated: 1486069661
title: 'Three-layer colored liquid - Beautiful chemical experiment!'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/DFVQdPVMrIc/default.jpg
    - https://i3.ytimg.com/vi/DFVQdPVMrIc/1.jpg
    - https://i3.ytimg.com/vi/DFVQdPVMrIc/2.jpg
    - https://i3.ytimg.com/vi/DFVQdPVMrIc/3.jpg
---
