---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tenant
offline_file: ""
offline_thumbnail: ""
uuid: ce56e61c-faf8-4f6a-885d-d0d98a9d667c
updated: 1484310571
title: tenant
categories:
    - Dictionary
---
tenant
     n 1: someone who pays rent to use land or a building or a car
          that is owned by someone else; "the landlord can evict a
          tenant who doesn't pay the rent" [syn: {renter}]
     2: a holder of buildings or lands by any kind of title (as
        ownership or lease)
     3: any occupant who dwells in a place
     v : occupy as a tenant
