---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/board
offline_file: ""
offline_thumbnail: ""
uuid: f62ce8d4-e31d-4366-98bb-f782d700eb41
updated: 1484310275
title: board
categories:
    - Dictionary
---
board
     n 1: a committee having supervisory powers; "the board has seven
          members"
     2: a flat piece of material designed for a special purpose; "he
        nailed boards across the windows"
     3: a stout length of sawn timber; made in a wide variety of
        sizes and used for many purposes [syn: {plank}]
     4: a board on which information can be displayed to public view
        [syn: {display panel}, {display board}]
     5: a flat portable surface (usually rectangular) designed for
        board games; "he got out the board and set up the pieces"
        [syn: {gameboard}]
     6: food or meals in general; "she sets a fine table"; "room and
        board" [syn: ...
