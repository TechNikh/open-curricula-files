---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lot
offline_file: ""
offline_thumbnail: ""
uuid: 51b93460-3176-4527-be33-72675997e607
updated: 1484310365
title: lot
categories:
    - Dictionary
---
lot
     n 1: (often followed by `of') a large number or amount or extent;
          "a batch of letters"; "a deal of trouble"; "a lot of
          money"; "he made a mint on the stock market"; "it must
          have cost plenty" [syn: {batch}, {deal}, {flock}, {good
          deal}, {great deal}, {hatful}, {heap}, {mass}, {mess}, {mickle},
           {mint}, {muckle}, {peck}, {pile}, {plenty}, {pot}, {quite
          a little}, {raft}, {sight}, {slew}, {spate}, {stack}, {tidy
          sum}, {wad}, {whole lot}, {whole slew}]
     2: a parcel of land having fixed boundaries; "he bought a lot
        on the lake"
     3: your overall circumstances or condition in life (including
        ...
