---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regionally
offline_file: ""
offline_thumbnail: ""
uuid: 0bdc1b57-f968-4dba-8b6e-dd23bfc215e9
updated: 1484310435
title: regionally
categories:
    - Dictionary
---
regionally
     adv : in a regional manner; "regionally governed"
