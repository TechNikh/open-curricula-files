---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inspect
offline_file: ""
offline_thumbnail: ""
uuid: 797efe23-6840-4a25-9ed4-73258994bc64
updated: 1484310150
title: inspect
categories:
    - Dictionary
---
inspect
     v 1: look over carefully; "Please inspect your father's will
          carefully"
     2: come to see in an official or professional capacity; "The
        governor visited the prison"; "The grant administrator
        visited the laboratory" [syn: {visit}]
     3: of accounts and tax returns; with the intent to verify [syn:
         {audit}, {scrutinize}, {scrutinise}]
