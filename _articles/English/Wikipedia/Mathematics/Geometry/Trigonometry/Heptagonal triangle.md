---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heptagonal_triangle
offline_file: ""
offline_thumbnail: ""
uuid: 3d4a0668-0b6d-44c1-98d3-4d36cb19dde7
updated: 1484309341
title: Heptagonal triangle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Heptagrams.svg.png
tags:
    - Key points
    - Relations of distances
    - Sides
    - Altitudes
    - Internal angle bisectors
    - Circumradius, inradius, and exradius
    - Orthic triangle
    - Trigonometric properties
categories:
    - Trigonometry
---
A heptagonal triangle is an obtuse scalene triangle whose vertices coincide with the first, second, and fourth vertices of a regular heptagon (from an arbtrary starting vertex). Thus its sides coincide with one side and the shorter and longer diagonals of the regular heptagon. All heptagonal triangles are similar (have the same shape), and so they are collectively known as the heptagonal triangle. Its angles have measures 
  
    
      
        π
        
          /
        
        7
        ,
        2
        π
        
          /
        
        7
        ,
      
    
    {\displaystyle \pi /7,2\pi /7,}
  
 and 
  
    
      
        4
        π
        
          /
        
  ...
