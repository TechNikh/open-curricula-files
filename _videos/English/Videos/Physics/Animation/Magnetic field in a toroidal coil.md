---
version: 1
type: video
provider: YouTube
id: edqGNOrW1GM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3b6ea66f-474d-440c-83a8-4b2b66f70beb
updated: 1486069652
title: Magnetic field in a toroidal coil
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/edqGNOrW1GM/default.jpg
    - https://i3.ytimg.com/vi/edqGNOrW1GM/1.jpg
    - https://i3.ytimg.com/vi/edqGNOrW1GM/2.jpg
    - https://i3.ytimg.com/vi/edqGNOrW1GM/3.jpg
---
