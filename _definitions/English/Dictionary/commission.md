---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commission
offline_file: ""
offline_thumbnail: ""
uuid: f7c954bc-7d8c-49b0-80a2-cb70eccb586f
updated: 1484310429
title: commission
categories:
    - Dictionary
---
commission
     n 1: a special group delegated to consider some matter; "a
          committee is a group that keeps minutes and loses hours"
          - Milton Berle [syn: {committee}]
     2: a fee for services rendered based on a percentage of an
        amount received or collected or agreed to be paid (as
        distinguished from a salary); "he works on commission"
     3: the act of granting authority to undertake certain functions
        [syn: {commissioning}]
     4: the state of being in good working order and ready for
        operation; "put the ships into commission"; "the motor was
        out of commission"
     5: a group of representatives or delegates [syn: ...
