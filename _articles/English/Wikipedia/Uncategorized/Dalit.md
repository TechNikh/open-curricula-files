---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dalit
offline_file: ""
offline_thumbnail: ""
uuid: 3cd7573a-3644-4bf1-8d2e-804f3db49775
updated: 1484308464
title: Dalit
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-A_school_of_untouchables_near_Bangalore_by_Lady_Ottoline_Morrell_2_0.jpg
tags:
    - Etymology
    - Other terms
    - Official Term (Scheduled castes)
    - Harijan
    - Other terms
    - Social status
    - History
    - Reform
    - Economic status
    - Discrimination
categories:
    - Uncategorized
---
Dalit, meaning "oppressed" in Sanskrit is the self-chosen political name of castes in India which are "untouchable".[1][2] Though the name Dalit has been in existence since the nineteenth century, the economist and reformer B. R. Ambedkar (1891–1956) popularised the term.[3] Dalits were excluded from the four-fold Varna system and formed the unmentioned fifth varna; they were also called Panchama.[4]
