---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Algorithmic_Lov%C3%A1sz_local_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: e41e6741-9253-47bb-9c84-17998a7c2ff4
updated: 1484308572
title: Algorithmic Lovász local lemma
tags:
    - Review of Lovász local lemma
    - Algorithmic version of the Lovász local lemma
    - History
    - Algorithm
    - Main theorem
    - Symmetric version
    - Example
    - Applications
    - Parallel version
categories:
    - Combinatorics
---
In theoretical computer science, the algorithmic Lovász local lemma gives an algorithmic way of constructing objects that obey a system of constraints with limited dependence.
