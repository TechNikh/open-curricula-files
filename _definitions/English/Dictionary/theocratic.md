---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/theocratic
offline_file: ""
offline_thumbnail: ""
uuid: b92e9179-7764-4c85-a9b1-2ab22aca6c8f
updated: 1484310183
title: theocratic
categories:
    - Dictionary
---
theocratic
     adj : of or relating to or being a theocracy; "a theocratic state"
