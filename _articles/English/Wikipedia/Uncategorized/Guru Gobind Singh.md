---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gobind
offline_file: ""
offline_thumbnail: ""
uuid: efd5e9ac-73dc-4a58-beab-62472e74124a
updated: 1484308458
title: Guru Gobind Singh
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/265px-GuruGobindBirthPlace.jpg
tags:
    - Family and early life
    - Founding the Khalsa
    - Sikh scriptures
    - Wars
    - Significant battles
    - Death of family members
    - Mughal accounts
    - Post-war years
    - Zafarnama
    - Final days
categories:
    - Uncategorized
---
Guru Gobind Singh, born Gobind Rai (22 December 1666 – 7 October 1708),[4][5] was the 10th Sikh Guru, a spiritual master, warrior, poet and philosopher. When his father, Guru Tegh Bahadur, was beheaded for refusing to convert to Islam,[6][7] Guru Gobind Singh was formally installed as the leader of the Sikhs at age nine, becoming the last of the living Sikh Gurus.[8] His four sons died during his lifetime in Mughal-Sikh wars – two in battle, two executed by the Mughal army.[9][10][11]
