---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pompous
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413741
title: pompous
categories:
    - Dictionary
---
pompous
     adj : puffed up with vanity; "a grandiloquent and boastful
           manner"; "overblown oratory"; "a pompous speech";
           "pseudo-scientific gobbledygook and pontifical hooey"-
           Newsweek [syn: {grandiloquent}, {overblown}, {pontifical},
            {portentous}]
