---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nine-point_circle
offline_file: ""
offline_thumbnail: ""
uuid: 417a6cbb-93fa-4246-af8e-2f1d75411bdf
updated: 1484309119
title: Nine-point circle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Triangle.NinePointCircle.svg.png
tags:
    - Significant nine points
    - discovery
    - Tangent circles
    - Other properties of the nine-point circle
    - Generalization
    - Notes
categories:
    - Theorems in geometry
---
In geometry, the nine-point circle is a circle that can be constructed for any given triangle. It is so named because it passes through nine significant concyclic points defined from the triangle. These nine points are:
