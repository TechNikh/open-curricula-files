---
version: 1
type: video
provider: YouTube
id: R-rhSQzFJL0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Example%203-%20Factoring%20quadratics%20by%20taking%20a%20common%20factor%20and%20grouping.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5ab32db2-98a9-4a7c-a5b4-0f9ffd27e039
updated: 1486069561
title: 'Example 3- Factoring quadratics by taking a common factor and grouping'
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/R-rhSQzFJL0/default.jpg
    - https://i3.ytimg.com/vi/R-rhSQzFJL0/1.jpg
    - https://i3.ytimg.com/vi/R-rhSQzFJL0/2.jpg
    - https://i3.ytimg.com/vi/R-rhSQzFJL0/3.jpg
---
