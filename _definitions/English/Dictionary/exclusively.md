---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exclusively
offline_file: ""
offline_thumbnail: ""
uuid: a1ce28f5-2bfa-434b-8219-e01bcb3f5327
updated: 1484310321
title: exclusively
categories:
    - Dictionary
---
exclusively
     adv : without any others being included or involved; "was entirely
           to blame"; "a school devoted entirely to the needs of
           problem children"; "he works for Mr. Smith
           exclusively"; "did it solely for money"; "the burden of
           proof rests on the prosecution alone"; "a privilege
           granted only to him" [syn: {entirely}, {solely}, {alone},
            {only}]
