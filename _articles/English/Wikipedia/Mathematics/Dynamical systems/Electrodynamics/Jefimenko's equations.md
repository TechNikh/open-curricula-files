---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Jefimenko%27s_equations'
offline_file: ""
offline_thumbnail: ""
uuid: 6866cfcd-cc11-4715-9a62-8ccd56103ab4
updated: 1484308759
title: "Jefimenko's equations"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Universal_charge_distribution.svg.png
tags:
    - Equations
    - Electric and magnetic fields
    - Origin from retarded potentials
    - Discussion
    - Notes
categories:
    - Electrodynamics
---
In electromagnetism, Jefimenko's equations (named after Oleg D. Jefimenko) describe the behavior of the electric and magnetic fields in terms of the charge and current distributions at retarded times.
