---
version: 1
type: video
provider: YouTube
id: sBldw95xMD4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Position%20vector%20valued%20functions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8b134c50-3b9a-4d84-8191-aedc2c9f71a8
updated: 1486069696
title: Position vector valued functions
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/sBldw95xMD4/default.jpg
    - https://i3.ytimg.com/vi/sBldw95xMD4/1.jpg
    - https://i3.ytimg.com/vi/sBldw95xMD4/2.jpg
    - https://i3.ytimg.com/vi/sBldw95xMD4/3.jpg
---
