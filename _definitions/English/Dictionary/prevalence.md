---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prevalence
offline_file: ""
offline_thumbnail: ""
uuid: 1ca94a5d-87d8-4e37-b480-4192ebacfa1d
updated: 1484096619
title: prevalence
categories:
    - Dictionary
---
prevalence
     n 1: the quality of prevailing generally; being widespread; "he
          was surprised by the prevalence of optimism about the
          future"
     2: (epidemiology) the ratio (for a given time period) of the
        number of occurrences of a disease or event to the number
        of units at risk in the population
     3: a superiority in numbers or amount; "there is a
        preponderance of Blacks in our prisons" [syn: {preponderance}]
