---
version: 1
type: video
provider: YouTube
id: rpucjakcwmw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a7344a8c-3549-4652-a106-f53bd1032e8a
updated: 1486069617
title: Segment Addition
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/rpucjakcwmw/default.jpg
    - https://i3.ytimg.com/vi/rpucjakcwmw/1.jpg
    - https://i3.ytimg.com/vi/rpucjakcwmw/2.jpg
    - https://i3.ytimg.com/vi/rpucjakcwmw/3.jpg
---
