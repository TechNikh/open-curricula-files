---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contradictory
offline_file: ""
offline_thumbnail: ""
uuid: 6f992f7b-0ee6-4aa7-b5eb-505c6847e93f
updated: 1484310369
title: contradictory
categories:
    - Dictionary
---
contradictory
     adj 1: of words or propositions so related that both cannot be true
            and both cannot be false; "`perfect' and `imperfect'
            are contradictory terms"
     2: that confounds or contradicts or confuses [syn: {confounding}]
     3: in disagreement; "the figures are at odds with our
        findings"; "contradictory attributes of unjust justice and
        loving vindictiveness"- John Morley [syn: {at odds(p)}, {conflicting},
         {self-contradictory}]
     4: unable to be both true at the same time [syn: {mutually
        exclusive}]
     n : two propositions are contradictories if both cannot be true
         (or both cannot be false) at the same ...
