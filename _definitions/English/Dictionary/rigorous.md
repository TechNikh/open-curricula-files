---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rigorous
offline_file: ""
offline_thumbnail: ""
uuid: 6cb3d8db-aa01-416b-9696-901815f83821
updated: 1484310397
title: rigorous
categories:
    - Dictionary
---
rigorous
     adj 1: rigidly accurate; allowing no deviation from a standard;
            "rigorous application of the law"; "a strict
            vegetarian" [syn: {strict}]
     2: demanding strict attention to rules and procedures;
        "rigorous discipline"; "tight security"; "stringent safety
        measures" [syn: {stringent}, {tight}]
     3: used of circumstances (especially weather) that cause
        suffering; "brutal weather"; "northern winters can be
        cruel"; "a cruel world"; "a harsh climate"; "a rigorous
        climate"; "unkind winters" [syn: {brutal}, {cruel}, {harsh},
         {unkind}]
