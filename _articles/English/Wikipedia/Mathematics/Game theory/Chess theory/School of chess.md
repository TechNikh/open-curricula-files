---
version: 1
type: article
id: https://en.wikipedia.org/wiki/School_of_chess
offline_file: ""
offline_thumbnail: ""
uuid: e7e0b7d8-8b9f-4d41-8dde-11ef9506ac31
updated: 1484309020
title: School of chess
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Andr%25C3%25A9_Philidor_0.jpg'
tags:
    - Philidor
    - Modenese school
    - English school
    - Romantic chess
    - Classical school
    - Hypermodern school
    - Soviet hegemony
categories:
    - Chess theory
---
A school of chess means a chess player or group of players that share common ideas about the strategy of the game. There have been several schools in the history of modern chess. Today there is less dependence on schools – players draw on many sources and play according to their personal style.
