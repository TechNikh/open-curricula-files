---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accuse
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484524381
title: accuse
categories:
    - Dictionary
---
accuse
     v 1: bring an accusation against; level a charge against; "He
          charged the man with spousal abuse" [syn: {impeach}, {incriminate},
           {criminate}]
     2: blame for, make a claim of wrongdoing or misbehavior
        against; "he charged me director with indifference" [syn:
        {charge}]
