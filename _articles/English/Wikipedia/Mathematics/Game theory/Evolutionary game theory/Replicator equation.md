---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Replicator_equation
offline_file: ""
offline_thumbnail: ""
uuid: 25cf1503-37f4-48b3-bbb6-2eb9e5a6cba1
updated: 1484308524
title: Replicator equation
tags:
    - Equational forms
    - Analysis
    - Relationships to other equations
    - Generalizations
categories:
    - Evolutionary game theory
---
In mathematics, the replicator equation is a deterministic monotone non-linear and non-innovative game dynamic used in evolutionary game theory. The replicator equation differs from other equations used to model replication, such as the quasispecies equation, in that it allows the fitness function to incorporate the distribution of the population types rather than setting the fitness of a particular type constant. This important property allows the replicator equation to capture the essence of selection. Unlike the quasispecies equation, the replicator equation does not incorporate mutation and so is not able to innovate new types or pure strategies.
