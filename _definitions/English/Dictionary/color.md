---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/color
offline_file: ""
offline_thumbnail: ""
uuid: cd1a0a2e-b167-41ef-a9a6-140b1642dbf3
updated: 1484310339
title: color
categories:
    - Dictionary
---
color
     adj : having or capable of producing colors; "color film"; "he
           rented a color television"; "marvelous color
           illustrations" [syn: {colour}] [ant: {black-and-white}]
     n 1: a visual attribute of things that results from the light
          they emit or transmit or reflect; "a white color is made
          up of many different wavelengths of light" [syn: {colour},
           {coloring}, {colouring}] [ant: {colorlessness}]
     2: interest and variety and intensity; "the Puritan Period was
        lacking in color" [syn: {colour}, {vividness}]
     3: the timbre of a musical sound; "the recording fails to
        capture the true color of the original music" ...
