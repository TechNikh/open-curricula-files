---
version: 1
type: video
provider: YouTube
id: _-vZ_g7K6P0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/DNA.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cef40223-74e8-4694-b560-217b245386de
updated: 1486069579
title: DNA
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/_-vZ_g7K6P0/default.jpg
    - https://i3.ytimg.com/vi/_-vZ_g7K6P0/1.jpg
    - https://i3.ytimg.com/vi/_-vZ_g7K6P0/2.jpg
    - https://i3.ytimg.com/vi/_-vZ_g7K6P0/3.jpg
---
An introduction to DNA
More free lessons at: http://www.khanacademy.org/video?v=_-vZ_g7K6P0
