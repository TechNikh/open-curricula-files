---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quasinormal_mode
offline_file: ""
offline_thumbnail: ""
uuid: f88402a5-2e85-4d48-a66e-857438c4d807
updated: 1484308896
title: Quasinormal mode
tags:
    - Example
    - Mathematical Physics
    - Biophysics
categories:
    - Waves
---
Quasinormal modes (QNM) are the modes of energy dissipation of a perturbed object or field, i.e. they describe perturbations of a field that decay in time.
