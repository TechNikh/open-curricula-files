---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moth
offline_file: ""
offline_thumbnail: ""
uuid: 7872d4a3-636e-4c9a-b7a3-edcc5d77c8e6
updated: 1484310543
title: moth
categories:
    - Dictionary
---
moth
     n : typically crepuscular or nocturnal insect having a stout
         body and feathery or hairlike antennae
