---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carsten_Thomassen
offline_file: ""
offline_thumbnail: ""
uuid: ba4a4876-fb9d-4221-aa75-34500fc6031d
updated: 1484309476
title: Carsten Thomassen
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/563px-Sportcar_sergio_luiz_ara_01.svg.png
categories:
    - Graph theorists
---
Carsten Thomassen (born August 22, 1948 in Grindsted) is a Danish mathematician. He has been a Professor of Mathematics at the Technical University of Denmark since 1981, and since 1990 a member of the Royal Danish Academy of Sciences and Letters. His research concerns discrete mathematics and more specifically graph theory.[1]
