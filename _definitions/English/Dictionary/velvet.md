---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/velvet
offline_file: ""
offline_thumbnail: ""
uuid: 4552c9fb-cb9e-4ef0-8613-16d8b2ec0e28
updated: 1484310601
title: velvet
categories:
    - Dictionary
---
velvet
     adj 1: smooth and soft to sight or hearing or touch or taste [syn:
            {velvety}]
     2: resembling velvet in having a smooth soft surface [syn: {velvety}]
     n : a silky densely piled fabric with a plain back
