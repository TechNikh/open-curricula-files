---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stably
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484593981
title: stably
categories:
    - Dictionary
---
stably
     adv 1: in a stable solid fixed manner; "the boulder was balanced
            stably at the edge of the canyon"
     2: in a stable unchanging manner; "the death rate in Russia has
        been stably high"
