---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/International_Journal_of_Computational_Geometry_and_Applications
offline_file: ""
offline_thumbnail: ""
uuid: 3789b56e-6be1-4a92-bd3f-ca471d9b5408
updated: 1484309175
title: >
    International Journal of Computational Geometry and
    Applications
categories:
    - Computational geometry
---
The International Journal of Computational Geometry and Applications (IJCGA) is a bimonthly journal published since 1991, by World Scientific. It covers the application of computational geometry in design and analysis of algorithms, focusing on problems arising in various fields of science and engineering such as computer-aided geometry design (CAGD), operations research, and others.
