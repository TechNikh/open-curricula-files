---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Raoul_Bott
offline_file: ""
offline_thumbnail: ""
uuid: 7c6e57d1-8249-404d-b4e4-b538ef07d735
updated: 1484309073
title: Raoul Bott
tags:
    - Early life
    - Career
    - Awards
    - Students
    - Publications
categories:
    - Geometers
---
Raoul Bott, ForMemRS (September 24, 1923 – December 20, 2005)[1] was a Hungarian-American mathematician known for numerous basic contributions to geometry in its broad sense. He is best known for his Bott periodicity theorem, the Morse–Bott functions which he used in this context, and the Borel–Bott–Weil theorem.
