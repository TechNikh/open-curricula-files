---
version: 1
type: video
provider: YouTube
id: VdLOP9teko4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/the%20MUTANT%20PLURALS/Irregular%20plural%20nouns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 337b21a8-2bbe-4b76-8940-1f44cff6c555
updated: 1486069597
title: Irregular plural nouns
tags:
    - Education
    - online learning
    - learning
    - lessons
    - foot
    - feet
    - goose
    - geese
categories:
    - the MUTANT PLURALS
thumbnail_urls:
    - https://i3.ytimg.com/vi/VdLOP9teko4/default.jpg
    - https://i3.ytimg.com/vi/VdLOP9teko4/1.jpg
    - https://i3.ytimg.com/vi/VdLOP9teko4/2.jpg
    - https://i3.ytimg.com/vi/VdLOP9teko4/3.jpg
---
English has seven words that behave very strangely when you change them from the singular to the plural. These are called THE MUTANT PLURALS of English. They're super cool.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/e/irregular-plural-nouns--mutant-plurals?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/v/foreign-plurals-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/grammar-nouns/v/irregular-plurals-the-base-plural?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
