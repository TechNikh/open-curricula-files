---
version: 1
type: video
provider: YouTube
id: 52wxpYnS64U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cd44ee17-0720-47c3-9da9-d85545a82b18
updated: 1486069595
title: Tension in an accelerating system and pie in the face
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/52wxpYnS64U/default.jpg
    - https://i3.ytimg.com/vi/52wxpYnS64U/1.jpg
    - https://i3.ytimg.com/vi/52wxpYnS64U/2.jpg
    - https://i3.ytimg.com/vi/52wxpYnS64U/3.jpg
---
The second part to the complicated problem. We figure out the tension in the wire connecting the two masses. Then we figure our how much we need to accelerate a pie for it to safely reach a man's face. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/forces-newtons-laws/treating-systems/v/treating-systems-the-hard-way?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/forces-newtons-laws/tension-tutorial/v/tension-part-2?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
