---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arrow_of_time
offline_file: ""
offline_thumbnail: ""
uuid: 5d14af42-3f19-4013-85fa-4e69820915cf
updated: 1484308797
title: Arrow of time
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Arthur_Stanley_Eddington.jpg
tags:
    - Eddington
    - Overview
    - Arrows
    - The thermodynamic arrow of time
    - The cosmological arrow of time
    - The radiative arrow of time
    - The causal arrow of time
    - The particle physics (weak) arrow of time
    - The quantum arrow of time
    - The quantum source of time
    - The psychological/perceptual arrow of time
categories:
    - Non-equilibrium thermodynamics
---
The Arrow of Time, or Time's Arrow, is a concept developed in 1927 by the British astronomer Arthur Eddington involving the "one-way direction" or "asymmetry" of time. This direction, according to Eddington, can be determined by studying the organization of atoms, molecules, and bodies, might be drawn upon a four-dimensional relativistic map of the world ("a solid block of paper").[1]
