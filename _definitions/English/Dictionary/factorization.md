---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/factorization
offline_file: ""
offline_thumbnail: ""
uuid: 0073dd83-dfd0-499c-afdc-7843f1700a9e
updated: 1484310142
title: factorization
categories:
    - Dictionary
---
factorization
     n : (mathematics) the resolution of an integer or polynomial
         into factors such that when multiplied together they give
         the integer or polynomial [syn: {factorisation}, {factoring}]
