---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coframe
offline_file: ""
offline_thumbnail: ""
uuid: 133465b9-c20e-46f2-a403-ae98828dd454
updated: 1484309209
title: Coframe
categories:
    - Differential geometry
---
In mathematics, a coframe or coframe field on a smooth manifold 
  
    
      
        M
      
    
    {\displaystyle M}
  
 is a system of one-forms or covectors which form a basis of the cotangent bundle at every point. In the exterior algebra of 
  
    
      
        M
      
    
    {\displaystyle M}
  
, one has a natural map from 
  
    
      
        
          v
          
            k
          
        
        :
        
          ⨁
          
            k
          
        
        
          T
          
            ∗
          
        
        M
        →
        
          ⋀
          
            k
          
        
        
          T
          
      ...
