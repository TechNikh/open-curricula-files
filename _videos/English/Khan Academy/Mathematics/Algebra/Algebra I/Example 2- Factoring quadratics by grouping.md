---
version: 1
type: video
provider: YouTube
id: u1SAo2GiX8A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Example%202-%20Factoring%20quadratics%20by%20grouping.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 55863968-5a6f-48b6-ba58-a33b188c5d3c
updated: 1486069559
title: 'Example 2- Factoring quadratics by grouping'
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/u1SAo2GiX8A/default.jpg
    - https://i3.ytimg.com/vi/u1SAo2GiX8A/1.jpg
    - https://i3.ytimg.com/vi/u1SAo2GiX8A/2.jpg
    - https://i3.ytimg.com/vi/u1SAo2GiX8A/3.jpg
---
