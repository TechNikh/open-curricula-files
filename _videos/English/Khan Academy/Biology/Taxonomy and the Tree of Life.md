---
version: 1
type: video
provider: YouTube
id: oHvLlS_Sc54
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Taxonomy%20and%20the%20Tree%20of%20Life.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 66ba0e42-c839-4ac8-9564-197390f6c109
updated: 1486069585
title: Taxonomy and the Tree of Life
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/oHvLlS_Sc54/default.jpg
    - https://i3.ytimg.com/vi/oHvLlS_Sc54/1.jpg
    - https://i3.ytimg.com/vi/oHvLlS_Sc54/2.jpg
    - https://i3.ytimg.com/vi/oHvLlS_Sc54/3.jpg
---
The science of taxonomy and where humans fit into the tree of life
More free lessons at: http://www.khanacademy.org/video?v=oHvLlS_Sc54
