---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wonderfully
offline_file: ""
offline_thumbnail: ""
uuid: 5a5b5a08-ceff-4b46-9be0-d76c4a807566
updated: 1484310152
title: wonderfully
categories:
    - Dictionary
---
wonderfully
     adv : (used as an intensifier) extremely well; "her voice is
           superbly disciplined"; "the colors changed wondrously
           slowly" [syn: {wondrous}, {wondrously}, {superbly}, {toppingly},
            {marvellously}, {terrifically}, {marvelously}]
