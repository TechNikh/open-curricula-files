---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Liesegang_rings
offline_file: ""
offline_thumbnail: ""
uuid: e9431047-119b-48ce-9a69-cc142c124886
updated: 1484308849
title: Liesegang rings
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Liesegang_rings.jpg
tags:
    - History
    - Silver nitrate potassium dichromate reaction
    - Some general observations
    - Theories
categories:
    - Thermodynamics
---
Liesegang rings (/ˈliːzəɡɑːŋ/) are a phenomenon seen in many, if not most, chemical systems undergoing a precipitation reaction, under certain conditions of concentration and in the absence of convection.
