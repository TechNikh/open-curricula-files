---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/double-barrelled
offline_file: ""
offline_thumbnail: ""
uuid: f514f3f4-6783-40e6-a53d-4793862bddc4
updated: 1484310146
title: double-barrelled
categories:
    - Dictionary
---
double-barrelled
     adj 1: having two barrels mounted side by side; "a double-barreled
            shotgun" [syn: {double-barreled}] [ant: {single-barreled}]
     2: having two purposes; twofold; "our double-barreled desire to
        make things profitable as well as attractive"- Louis
        Kronenbergers [syn: {double-barreled}]
