---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Longest_alternating_subsequence
offline_file: ""
offline_thumbnail: ""
uuid: cc5cd254-7fb4-4f88-a0c2-b30ccc407449
updated: 1484308577
title: Longest alternating subsequence
tags:
    - Efficient algorithms
    - Distributional results
    - Online algorithms
categories:
    - Combinatorics
---
In combinatorial mathematics, probability, and computer science, in the longest alternating subsequence problem, one wants to find a subsequence of a given sequence in which the elements are in alternating order, and in which the sequence is as long as possible.
