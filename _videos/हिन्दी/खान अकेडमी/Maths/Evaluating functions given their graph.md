---
version: 1
type: video
provider: YouTube
id: 1UTudeInh0M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3cf393f6-d37c-436a-a930-81616f5ca358
updated: 1486069611
title: Evaluating functions given their graph
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/1UTudeInh0M/default.jpg
    - https://i3.ytimg.com/vi/1UTudeInh0M/1.jpg
    - https://i3.ytimg.com/vi/1UTudeInh0M/2.jpg
    - https://i3.ytimg.com/vi/1UTudeInh0M/3.jpg
---
