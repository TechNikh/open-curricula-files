---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bounded_expansion
offline_file: ""
offline_thumbnail: ""
uuid: 1e7b7483-85af-40e1-a629-04145cc7f860
updated: 1484308611
title: Bounded expansion
tags:
    - Definition and equivalent characterizations
    - Polynomial expansion and separator theorems
    - Classes of graphs with bounded expansion
    - Algorithmic applications
categories:
    - Graph theory
---
In graph theory, a family of graphs is said to have bounded expansion if all of its shallow minors are sparse graphs. Many natural families of sparse graphs have bounded expansion. A closely related but stronger property, polynomial expansion, is equivalent to the existence of separator theorems for these families. Families with these properties have efficient algorithms for problems including the subgraph isomorphism problem and model checking for the first order theory of graphs.
