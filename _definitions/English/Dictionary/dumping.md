---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dumping
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484613901
title: dumping
categories:
    - Dictionary
---
dumping
     n : selling goods abroad at a price below that charged in the
         domestic market
