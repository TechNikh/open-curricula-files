---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aga
offline_file: ""
offline_thumbnail: ""
uuid: 70fafdb6-14b7-4880-a66e-fa9845ccf20e
updated: 1484310163
title: aga
categories:
    - Dictionary
---
Aga
     n : title for a civil or military leader (especially in Turkey)
         [syn: {Agha}]
