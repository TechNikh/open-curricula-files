---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/toe
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484409121
title: toe
categories:
    - Dictionary
---
toe
     adj : having a toe or toes of a specified kind; often used in
           combination; "long-toed"; "five-toed" [syn: {toed}]
           [ant: {toeless}]
     n 1: one of the digits of the foot
     2: the part of footwear that provides a covering for the toes
     3: forepart of a hoof
     4: (golf) the part of a clubhead farthest from the shaft
     v 1: walk so that the toes assume an indicated position or
          direction; "She toes inwards"
     2: drive obliquely; "toe a nail" [syn: {toenail}]
     3: hit (a golf ball) with the toe of the club
     4: drive (a golf ball) with the toe of the club
     5: touch with the toe
