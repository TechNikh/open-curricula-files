---
version: 1
type: video
provider: YouTube
id: jqg4YPaRRjU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 55598e99-29d6-4498-be2b-ca1947b6f9d6
updated: 1486069617
title: Fewer word problems
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/jqg4YPaRRjU/default.jpg
    - https://i3.ytimg.com/vi/jqg4YPaRRjU/1.jpg
    - https://i3.ytimg.com/vi/jqg4YPaRRjU/2.jpg
    - https://i3.ytimg.com/vi/jqg4YPaRRjU/3.jpg
---
