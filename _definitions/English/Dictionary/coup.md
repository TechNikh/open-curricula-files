---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coup
offline_file: ""
offline_thumbnail: ""
uuid: 83f29a98-c262-43f0-b8d0-708ac98b7294
updated: 1484310559
title: coup
categories:
    - Dictionary
---
coup
     n 1: a sudden and decisive change of government illegally or by
          force [syn: {coup d'etat}, {putsch}, {takeover}]
     2: a brilliant and notable success
