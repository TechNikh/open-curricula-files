---
version: 1
type: article
id: https://en.wikipedia.org/wiki/James_Hoffman
offline_file: ""
offline_thumbnail: ""
uuid: b62f82ee-1e0f-49ea-837e-979ba7fe7175
updated: 1484309085
title: James Hoffman
tags:
    - Scientific visualizations
    - Inventions
    - Publications
    - Websites designed by Hoffman
categories:
    - Geometers
---
James Hoffman is a software engineer and inventor located in Alameda, California, who has worked in scientific visualization and was instrumental in producing the first visualization of Costa's minimal surface. His scientific visualizations have been published in Scientific American and Nature, among other journals. Most recently, Hoffman has been involved in the solar start-up company Sun Synchrony, which is developing his solar inventions.[1]
