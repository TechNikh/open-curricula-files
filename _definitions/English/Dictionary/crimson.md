---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crimson
offline_file: ""
offline_thumbnail: ""
uuid: faccbbf7-1670-4f6b-8b3e-caafc2564d91
updated: 1484310389
title: crimson
categories:
    - Dictionary
---
crimson
     adj 1: having any of numerous bright or strong colors reminiscent
            of the color of blood or cherries or tomatoes or
            rubies [syn: {red}, {reddish}, {ruddy}, {blood-red}, {carmine},
             {cerise}, {cherry}, {cherry-red}, {ruby}, {ruby-red},
             {scarlet}]
     2: characterized by violence or bloodshed; "writes of crimson
        deeds and barbaric days"- Andrea Parke; "fann'd by
        Conquest's crimson wing"- Thomas Gray; "convulsed with red
        rage"- Hudson Strode [syn: {red}, {violent}]
     3: (especially of the face) reddened or suffused with or as if
        with blood from emotion or exertion; "crimson with fury";
        ...
