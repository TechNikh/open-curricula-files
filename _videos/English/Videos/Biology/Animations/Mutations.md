---
version: 1
type: video
provider: YouTube
id: eDbK0cxKKsk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Mutations.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 604ff881-d7e1-43d8-a73d-9cca2aab591c
updated: 1486069672
title: Mutations
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/eDbK0cxKKsk/default.jpg
    - https://i3.ytimg.com/vi/eDbK0cxKKsk/1.jpg
    - https://i3.ytimg.com/vi/eDbK0cxKKsk/2.jpg
    - https://i3.ytimg.com/vi/eDbK0cxKKsk/3.jpg
---
