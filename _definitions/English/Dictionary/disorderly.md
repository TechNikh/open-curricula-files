---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disorderly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484570161
title: disorderly
categories:
    - Dictionary
---
disorderly
     adj 1: undisciplined and unruly; "disorderly youths"; "disorderly
            conduct" [ant: {orderly}]
     2: in utter disorder; "a disorderly pile of clothes" [syn: {higgledy-piggledy},
         {hugger-mugger}, {jumbled}, {topsy-turvy}]
     3: completely unordered and unpredictable and confusing [syn: {chaotic}]
