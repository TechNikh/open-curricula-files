---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purple
offline_file: ""
offline_thumbnail: ""
uuid: 8cd4eeb3-bcbf-43a2-908f-a4fe788199cc
updated: 1484310303
title: purple
categories:
    - Dictionary
---
purple
     adj 1: of a color midway between red and blue [syn: {violet}, {purplish}]
     2: excessively elaborate or showily expressed; "a writer of
        empurpled literature"; "many purple passages"; "speech
        embellished with classical quotations"; "an
        over-embellished story of the fish that got away" [syn: {embellished},
         {empurpled}, {over-embellished}]
     3: belonging to or befitting a supreme ruler; "golden age of
        imperial splendor"; "purple tyrant"; "regal attire";
        "treated with royal acclaim"; "the royal carriage of a
        stag's head" [syn: {imperial}, {majestic}, {regal}, {royal}]
     n 1: a chromatic color between red and blue ...
