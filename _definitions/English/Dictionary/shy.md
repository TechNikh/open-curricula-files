---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415361
title: shy
categories:
    - Dictionary
---
shy
     adj 1: lacking self-confidence; "stood in the doorway diffident and
            abashed"; "problems that call for bold not timid
            responses"; "a very unsure young man" [syn: {diffident},
             {timid}, {unsure}]
     2: easily startled or frightened
     3: short; "eleven is one shy of a dozen" [syn: {shy(p)}]
     4: wary and distrustful; disposed to avoid persons or things;
        "shy of strangers"
     n : a quick throw; "he gave the ball a shy to the first baseman"
     v 1: start suddenly, as from fight
     2: throw quickly
     [also: {shied}, {shyest}, {shyer}, {shiest}, {shier}]
