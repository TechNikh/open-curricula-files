---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poincar%C3%A9_recurrence_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 4073cf89-b63d-445a-a198-81b1f4a5c220
updated: 1484308696
title: Poincaré recurrence theorem
tags:
    - Precise formulation
    - Discussion of proof
    - Formal statement of the theorem
    - Theorem 1
    - Theorem 2
    - Quantum mechanical version
categories:
    - Theorems in dynamical systems
---
In mathematics, the Poincaré recurrence theorem states that certain systems will, after a sufficiently long but finite time, return to a state very close to the initial state. The Poincaré recurrence time is the length of time elapsed until the recurrence (this time may vary greatly depending on the exact initial state and required degree of closeness). The result applies to isolated mechanical systems subject to some constraints, e.g., all particles must be bound to a finite volume. The theorem is commonly discussed in the context of ergodic theory, dynamical systems and statistical mechanics.
