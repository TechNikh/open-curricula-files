---
version: 1
type: article
id: https://en.wikipedia.org/wiki/First-move_advantage_in_chess
offline_file: ""
offline_thumbnail: ""
uuid: 28c95917-0cca-410f-a3df-204dcaf8a623
updated: 1484309022
title: First-move advantage in chess
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Wilhelm_Steinitz2.jpg
tags:
    - Winning percentages
    - Drawn with best play
    - White wins
    - White wins with 1.e4
    - White wins with 1.d4
    - Modern perspectives
    - White has an enduring advantage
    - Black is OK!
    - Dynamism
    - Countervailing advantages
    - "White's advantages"
    - "Black's advantages"
    - Reversed openings
    - Symmetrical openings
    - Tournament and match play
    - Solving chess
    - Quotation
    - Notes
categories:
    - Chess theory
---
The first-move advantage in chess is the inherent advantage of the player (White) who makes the first move in chess. Chess players and theorists generally agree that White begins the game with some advantage. Since 1851, compiled statistics support this view; White consistently wins slightly more often than Black, usually scoring between 52 and 56 percent. White's winning percentage[1] is about the same for tournament games between humans and games between computers. However, White's advantage is less significant in blitz games and games between novices.
