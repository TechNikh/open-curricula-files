---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interestingly
offline_file: ""
offline_thumbnail: ""
uuid: 25b905d7-c427-492e-9aae-efa0dac49ca2
updated: 1484310518
title: interestingly
categories:
    - Dictionary
---
interestingly
     adv : in an interesting manner; "when he ceases to be just
           interestingly neurotic and...gets locked up"- Time
           [ant: {uninterestingly}]
