---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stuck
offline_file: ""
offline_thumbnail: ""
uuid: 869a58e5-8716-4028-9dd4-d975ee8be3f3
updated: 1484310218
title: stuck
categories:
    - Dictionary
---
stick
     n 1: implement consisting of a length of wood; "he collected dry
          sticks for a campfire"; "the kid had a candied apple on
          a stick"
     2: a small thin branch of a tree
     3: a lever used by a pilot to control the ailerons and
        elevators of an airplane [syn: {control stick}, {joystick}]
     4: informal terms of the leg; "fever left him weak on his
        sticks" [syn: {pin}, {peg}]
     5: marijuana leaves rolled into a cigarette for smoking [syn: {joint},
         {marijuana cigarette}, {reefer}, {spliff}]
     6: threat of a penalty; "the policy so far is all stick and no
        carrot"
     v 1: fix, force, or implant; "lodge a bullet in the ...
