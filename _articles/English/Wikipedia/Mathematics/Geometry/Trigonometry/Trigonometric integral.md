---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trigonometric_integral
offline_file: ""
offline_thumbnail: ""
uuid: ed389244-1812-462f-9889-2c5f7fa74258
updated: 1484309350
title: Trigonometric integral
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sine_cosine_integral.svg.png
tags:
    - Sine integral
    - Cosine integral
    - Hyperbolic sine integral
    - Hyperbolic cosine integral
    - Auxiliary functions
    - "Nielsen's spiral"
    - Expansion
    - Asymptotic series (for large argument)
    - Convergent series
    - Relation with the exponential integral of imaginary argument
    - Efficient evaluation
    - Signal processing
categories:
    - Trigonometry
---
In mathematics, the trigonometric integrals are a family of integrals involving trigonometric functions. A number of the basic trigonometric integrals are discussed at the list of integrals of trigonometric functions.
