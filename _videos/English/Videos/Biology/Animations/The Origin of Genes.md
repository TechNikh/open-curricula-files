---
version: 1
type: video
provider: YouTube
id: qLnr_3J1IT8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/The%20Origin%20of%20Genes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4ce9123b-7631-480e-9774-c0c7f3daca4c
updated: 1486069672
title: The Origin of Genes
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/qLnr_3J1IT8/default.jpg
    - https://i3.ytimg.com/vi/qLnr_3J1IT8/1.jpg
    - https://i3.ytimg.com/vi/qLnr_3J1IT8/2.jpg
    - https://i3.ytimg.com/vi/qLnr_3J1IT8/3.jpg
---
