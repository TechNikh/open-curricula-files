---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/afraid
offline_file: ""
offline_thumbnail: ""
uuid: 51a04cbc-6e09-4c28-a40d-3a59c90a777f
updated: 1484310543
title: afraid
categories:
    - Dictionary
---
afraid
     adj 1: filled with fear or apprehension; "afraid even to turn his
            head"; "suddenly looked afraid"; "afraid for his
            life"; "afraid of snakes"; "afraid to ask questions"
            [syn: {afraid(p)}] [ant: {unafraid(p)}]
     2: filled with regret or concern; used often to soften an
        unpleasant statement; "I'm afraid I won't be able to
        come"; "he was afraid he would have to let her go"; "I'm
        afraid you're wrong"
     3: feeling worry or concern or insecurity; "She was afraid that
        I might be embarrassed"; "terribly afraid of offending
        someone"; "I am afraid we have witnessed only the first
        phase of the ...
