---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slip
offline_file: ""
offline_thumbnail: ""
uuid: c1efc8d2-6829-4c8e-b722-b3c4e59a99a3
updated: 1484310366
title: slip
categories:
    - Dictionary
---
slip
     n 1: a socially awkward or tactless act [syn: {faux pas}, {gaffe},
           {solecism}, {gaucherie}]
     2: a minor inadvertent mistake usually observed in speech or
        writing or in small accidents or memory lapses etc. [syn:
        {slip-up}, {miscue}, {parapraxis}]
     3: potter's clay that is thinned and used for coating or
        decorating ceramics
     4: a part (sometimes a root or leaf or bud) removed from a
        plant to propagate a new plant through rooting or grafting
        [syn: {cutting}]
     5: a young and slender person; "he's a mere slip of a lad"
     6: a place where a craft can be made fast [syn: {mooring}, {moorage},
         {berth}]
     7: ...
