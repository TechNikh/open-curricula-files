---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Group_scheme
offline_file: ""
offline_thumbnail: ""
uuid: ec1dc033-20f5-4149-a889-65272a0c51ba
updated: 1484309234
title: Group scheme
tags:
    - Definition
    - Constructions
    - Examples
    - Basic properties
    - Finite flat group schemes
    - Cartier duality
    - Dieudonné modules
categories:
    - Duality theories
---
In mathematics, a group scheme is a type of algebro-geometric object equipped with a composition law. Group schemes arise naturally as symmetries of schemes, and they generalize algebraic groups, in the sense that all algebraic groups have group scheme structure, but group schemes are not necessarily connected, smooth, or defined over a field. This extra generality allows one to study richer infinitesimal structures, and this can help one to understand and answer questions of arithmetic significance. The category of group schemes is somewhat better behaved than that of group varieties, since all homomorphisms have kernels, and there is a well-behaved deformation theory. Group schemes that ...
