---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acre
offline_file: ""
offline_thumbnail: ""
uuid: 29b8eac2-ab5a-42f7-b8f0-b1eac514c411
updated: 1484310258
title: acre
categories:
    - Dictionary
---
acre
     n 1: a unit of area (4840 square yards) used in English-speaking
          countries
     2: a territory of western Brazil bordering on Bolivia and Peru
     3: a town and port in northwestern Israel in the eastern
        Mediterranean [syn: {Akko}, {Akka}, {Accho}]
