---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/periodical
offline_file: ""
offline_thumbnail: ""
uuid: 2daef137-abfe-4c54-b9e8-9fc451fc4fbb
updated: 1484310413
title: periodical
categories:
    - Dictionary
---
periodical
     adj : happening or recurring at regular intervals [syn: {periodic}]
           [ant: {aperiodic}]
     n : a publication that appears at fixed intervals
