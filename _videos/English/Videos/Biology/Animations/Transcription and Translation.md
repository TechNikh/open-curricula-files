---
version: 1
type: video
provider: YouTube
id: 41_Ne5mS2ls
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Transcription%20and%20Translation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 92412147-4018-45bb-bb61-291735bae8c2
updated: 1486069669
title: Transcription and Translation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/41_Ne5mS2ls/default.jpg
    - https://i3.ytimg.com/vi/41_Ne5mS2ls/1.jpg
    - https://i3.ytimg.com/vi/41_Ne5mS2ls/2.jpg
    - https://i3.ytimg.com/vi/41_Ne5mS2ls/3.jpg
---
