---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/taliban
offline_file: ""
offline_thumbnail: ""
uuid: 7a1c9ea5-6bb4-4023-9593-094630b2ea7a
updated: 1484310180
title: taliban
categories:
    - Dictionary
---
Taliban
     n : a fundamentalist Islamic militia; in 1995 the Taliban
         militia took over Afghanistan and in 1996 took Kabul and
         set up an Islamic government; "the Taliban enforced a
         strict Muslim code of behavior" [syn: {Taleban}]
