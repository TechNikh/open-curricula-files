---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/entry
offline_file: ""
offline_thumbnail: ""
uuid: 920f3f6b-70e7-4437-a8df-5bef9a98053b
updated: 1484310365
title: entry
categories:
    - Dictionary
---
entry
     n 1: an item inserted in a written record
     2: the act of beginning something new; "they looked forward to
        the debut of their new product line" [syn: {introduction},
         {debut}, {first appearance}, {launching}, {unveiling}]
     3: a written record of a commercial transaction [syn: {accounting
        entry}, {ledger entry}]
     4: something (manuscripts or architectural plans and models or
        estimates or works of art of all genres etc.) submitted
        for the judgment of others (as in a competition); "several
        of his submissions were rejected by publishers"; "what was
        the date of submission of your proposal?" [syn: {submission}]
     5: ...
