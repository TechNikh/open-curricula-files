---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/issue
offline_file: ""
offline_thumbnail: ""
uuid: c74278dc-cbd0-42c7-8fc6-059209e6b2ec
updated: 1484310238
title: issue
categories:
    - Dictionary
---
issue
     n 1: an important question that is in dispute and must be
          settled; "the issue could be settled by requiring public
          education for everyone"; "politicians never discuss the
          real issues"
     2: one of a series published periodically; "she found an old
        issue of the magazine in her dentist's waitingroom" [syn:
        {number}]
     3: the provision of something by issuing it (usually in
        quantity); "a new issue of stamps"; "the last issue of
        penicillin was over a month ago" [syn: {issuing}, {issuance}]
     4: some situation or event that is thought about; "he kept
        drifting off the topic"; "he had been thinking about the
  ...
