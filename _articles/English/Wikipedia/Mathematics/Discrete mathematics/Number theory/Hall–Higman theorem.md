---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hall%E2%80%93Higman_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: c0b81cdd-d04e-43a6-b6b9-229a0214a8ef
updated: 1484308645
title: Hall–Higman theorem
categories:
    - Number theory
---
In mathematical group theory, the Hall–Higman theorem, due to Philip Hall and Graham Higman (1956, Theorem B), describes the possibilities for the minimal polynomial of an element of prime power order for a representation of a p-solvable group.
