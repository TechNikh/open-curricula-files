---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yoshiharu_Kohayakawa
offline_file: ""
offline_thumbnail: ""
uuid: 39651131-9106-4ddd-859f-832ff6eca7fa
updated: 1484309466
title: Yoshiharu Kohayakawa
categories:
    - Graph theorists
---
Yoshiharu Kohayakawa (Japanese name: コハヤカワヨシハル, born 1963) is a Japanese-Brazilian mathematician working on discrete mathematics and probability theory.[1] He is known for his work on Szemerédi's regularity lemma, which he extended to sparser graphs.[2][3]
