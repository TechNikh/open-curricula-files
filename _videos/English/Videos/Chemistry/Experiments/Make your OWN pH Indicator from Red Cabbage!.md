---
version: 1
type: video
provider: YouTube
id: oG-pNRVHsc4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Make%20your%20OWN%20pH%20Indicator%20from%20Red%20Cabbage%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: db4f1d21-7c62-42e8-bc49-0ed13cb5c59a
updated: 1486069661
title: Make your OWN pH Indicator from Red Cabbage!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/oG-pNRVHsc4/default.jpg
    - https://i3.ytimg.com/vi/oG-pNRVHsc4/1.jpg
    - https://i3.ytimg.com/vi/oG-pNRVHsc4/2.jpg
    - https://i3.ytimg.com/vi/oG-pNRVHsc4/3.jpg
---
