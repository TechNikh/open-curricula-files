---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slowdown
offline_file: ""
offline_thumbnail: ""
uuid: 08b88ea9-c101-4bb6-84e7-f8d8f144dc20
updated: 1484310451
title: slowdown
categories:
    - Dictionary
---
slow down
     v 1: lose velocity; move more slowly; "The car decelerated" [syn:
           {decelerate}, {slow}, {slow up}, {retard}] [ant: {accelerate}]
     2: become slow or slower; "Production slowed" [syn: {slow}, {slow
        up}, {slack}, {slacken}]
     3: cause to proceed more slowly; "The illness slowed him down"
        [syn: {slow}, {slow up}]
     4: reduce the speed of; "He slowed down the car" [syn: {decelerate}]
        [ant: {accelerate}]
     5: become less tense, rest, or take one's ease; "He relaxed in
        the hot tub"; "Let's all relax after a hard day's work"
        [syn: {relax}, {loosen up}, {unbend}, {unwind}, {decompress}]
        [ant: {tense}]
