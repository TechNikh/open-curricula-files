---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cargo
offline_file: ""
offline_thumbnail: ""
uuid: e8abe963-1998-4a11-af8e-a78b9c41be90
updated: 1484310579
title: cargo
categories:
    - Dictionary
---
cargo
     n : goods carried by a large vehicle [syn: {lading}, {freight},
         {load}, {loading}, {payload}, {shipment}, {consignment}]
     [also: {cargoes} (pl)]
