---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Land_navigation_(military)
offline_file: ""
offline_thumbnail: ""
uuid: b13572d0-ccdd-4b28-9dbd-addef2ad8999
updated: 1484309299
title: Land navigation (military)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-US_Army_52149_Sgt._Joines_on_the_night_course.jpg
categories:
    - Orientation (geometry)
---
Land navigation, or orienteering, is the military term for the study of traversing through unfamiliar terrain by foot or in a land vehicle. Land navigation includes the ability to read maps, use a compass, and other navigational skills.
