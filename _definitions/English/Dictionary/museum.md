---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/museum
offline_file: ""
offline_thumbnail: ""
uuid: f9e1013f-1e4c-4d2f-bbb4-25462bdcb994
updated: 1484310277
title: museum
categories:
    - Dictionary
---
museum
     n : a depository for collecting and displaying objects having
         scientific or historical or artistic value
