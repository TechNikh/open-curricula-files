---
version: 1
type: video
provider: YouTube
id: 1xSQlwWGT8M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Biology/Chemistry%20of%20life/Introduction%20to%20the%20atom.webm"
offline_file: ""
offline_thumbnail: ""
uuid: db317548-8c88-41b0-8a46-703b3a10683a
updated: 1486069585
title: Introduction to the atom
tags:
    - dotsub
categories:
    - Chemistry of life
thumbnail_urls:
    - https://i3.ytimg.com/vi/1xSQlwWGT8M/default.jpg
    - https://i3.ytimg.com/vi/1xSQlwWGT8M/1.jpg
    - https://i3.ytimg.com/vi/1xSQlwWGT8M/2.jpg
    - https://i3.ytimg.com/vi/1xSQlwWGT8M/3.jpg
---
The atom, proton, neutron and electron

Watch the next lesson: https://www.khanacademy.org/science/biology/chemistry--of-life/electron-shells-and-orbitals/v/orbitals?utm_source=YT&utm_medium=Desc&utm_campaign=biology

Missed the previous lesson? https://www.khanacademy.org/science/biology/chemistry--of-life/elements-and-atoms/v/elements-and-atoms?utm_source=YT&utm_medium=Desc&utm_campaign=biology

Biology on Khan Academy: Life is beautiful! From atoms to cells, from genes to proteins, from populations to ecosystems, biology is the study of the fascinating and intricate systems that make life possible. Dive in to learn more about the many branches of biology and why they are exciting and important. Covers topics seen in a high school or first-year college biology course.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy's Biology channel: https://www.youtube.com/channel/UC82qE46vcTn7lP4tK_RHhdg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
