---
version: 1
type: video
provider: YouTube
id: XY9F__XpcXM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7aa582c5-eed2-48fc-848e-d442b7d865f2
updated: 1486069605
title: 'Adding 1 vs  adding 10'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/XY9F__XpcXM/default.jpg
    - https://i3.ytimg.com/vi/XY9F__XpcXM/1.jpg
    - https://i3.ytimg.com/vi/XY9F__XpcXM/2.jpg
    - https://i3.ytimg.com/vi/XY9F__XpcXM/3.jpg
---
