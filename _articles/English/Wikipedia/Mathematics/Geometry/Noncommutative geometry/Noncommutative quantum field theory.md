---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Noncommutative_quantum_field_theory
offline_file: ""
offline_thumbnail: ""
uuid: f6c9282a-e97d-4fbf-b3b0-bd8f65110ce2
updated: 1484309290
title: Noncommutative quantum field theory
tags:
    - History and motivation
    - Footnotes
categories:
    - Noncommutative geometry
---
In mathematical physics, noncommutative quantum field theory (or quantum field theory on noncommutative spacetime) is an application of noncommutative mathematics to the spacetime of quantum field theory that is an outgrowth of noncommutative geometry and index theory in which the coordinate functions [1] are noncommutative. One commonly studied version of such theories has the "canonical" commutation relation:
