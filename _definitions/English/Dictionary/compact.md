---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compact
offline_file: ""
offline_thumbnail: ""
uuid: 65929074-90fd-4b2f-a8d9-02bbf3879646
updated: 1484310258
title: compact
categories:
    - Dictionary
---
compact
     adj 1: closely and firmly united or packed together; "compact
            soil"; "compact clusters of flowers" [ant: {loose}]
     2: closely crowded together; "a compact shopping center"; "a
        dense population"; "thick crowds" [syn: {dense}, {thick}]
     3: heavy and compact in form or stature; "a wrestler of compact
        build"; "he was tall and heavyset"; "stocky legs"; "a
        thick middle-aged man"; "a thickset young man" [syn: {heavyset},
         {stocky}, {thick}, {thickset}]
     4: briefly giving the gist of something; "a short and
        compendious book"; "a compact style is brief and pithy";
        "succinct comparisons"; "a summary formulation of a
 ...
