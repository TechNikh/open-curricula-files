---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weyl_transformation
offline_file: ""
offline_thumbnail: ""
uuid: 908716be-3619-4b3a-89ed-29dd7f11bef4
updated: 1484309321
title: Weyl transformation
categories:
    - Symmetry
---
which produces another metric in the same conformal class. A theory or an expression invariant under this transformation is called conformally invariant, or is said to possess Weyl symmetry. The Weyl symmetry is an important symmetry in conformal field theory. It is, for example, a symmetry of the Polyakov action.
