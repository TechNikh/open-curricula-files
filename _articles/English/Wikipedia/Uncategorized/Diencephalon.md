---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diencephalon
offline_file: ""
offline_thumbnail: ""
uuid: a808cbfd-7cf0-40ca-85d5-ffc1fba5d612
updated: 1484308378
title: Diencephalon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray715.png
tags:
    - Structure
    - Attachments
    - Function
    - Additional images
categories:
    - Uncategorized
---
The diencephalon is part of the prosencephalon (forebrain), which develops from the foremost primary cerebral vesicle. The prosencephalon differentiates into a caudal diencephalon and rostral telencephalon. The cerebral hemispheres develop from the sides of the telencephalon, each containing a lateral ventricle. The diencephalon consists of structures that are lateral to the third ventricle, and includes the thalamus, the hypothalamus, the epithalamus and the subthalamus.
