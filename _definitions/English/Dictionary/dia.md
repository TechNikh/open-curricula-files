---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dia
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484528941
title: dia
categories:
    - Dictionary
---
DIA
     n : an intelligence agency of the United States in the
         Department of Defense; is responsible for providing
         intelligence in support of military planning and
         operations and weappons acquisition [syn: {Defense
         Intelligence Agency}]
