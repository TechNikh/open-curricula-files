---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/easily
offline_file: ""
offline_thumbnail: ""
uuid: b5b0b51e-1ec1-4a5f-9933-e0988521a779
updated: 1484310343
title: easily
categories:
    - Dictionary
---
easily
     adv 1: with ease (`easy' is sometimes used informally for
            `easily'); "she was easily excited"; "was easily
            confused"; "he won easily"; "this china breaks very
            easily"; "success came too easy" [syn: {easy}]
     2: without question; "easily the best book she's written"
     3: indicating high probability; in all likelihood; "I might
        well do it"; "a mistake that could easily have ended in
        disaster"; "you may well need your umbrella"; "he could
        equally well be trying to deceive us" [syn: {well}]
