---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deficiency
offline_file: ""
offline_thumbnail: ""
uuid: 6fdcd3c2-5c47-4ab7-bbe7-4a80ae8073ce
updated: 1484310535
title: deficiency
categories:
    - Dictionary
---
deficiency
     n 1: the state of needing something that is absent or
          unavailable; "there is a serious lack of insight into
          the problem"; "water is the critical deficiency in
          desert regions"; "for want of a nail the shoe was lost"
          [syn: {lack}, {want}]
     2: lack of an adequate quantity or number; "the inadequacy of
        unemployment benefits" [syn: {insufficiency}, {inadequacy}]
        [ant: {sufficiency}, {sufficiency}]
