---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/raised
offline_file: ""
offline_thumbnail: ""
uuid: 9f7af354-db3f-4444-841d-71d7bf7d0dda
updated: 1484310311
title: raised
categories:
    - Dictionary
---
raised
     adj 1: above the surround or above the normal position; "a raised
            design"; "raised eyebrows" [ant: {lowered}]
     2: embellished with a raised pattern created by pressure or
        embroidery; "brocaded silk"; "an embossed satin";
        "embossed leather"; "raised needlework"; "raised
        metalwork" [syn: {brocaded}, {embossed}]
     3: leavened usually with yeast; "raised bread"
     4: increased especially to abnormal levels; "the raised prices
        frightened away customers"; "inflated wages"; "an inflated
        economy" [syn: {raised(a)}, {inflated}]
