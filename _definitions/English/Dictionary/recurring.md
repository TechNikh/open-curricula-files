---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recurring
offline_file: ""
offline_thumbnail: ""
uuid: 33783dc9-ac7f-4cb1-8784-ed148d6b78f2
updated: 1484310142
title: recurring
categories:
    - Dictionary
---
recurring
     adj : coming back; "a revenant ghost" [syn: {revenant}]
