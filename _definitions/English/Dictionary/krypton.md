---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/krypton
offline_file: ""
offline_thumbnail: ""
uuid: eb76babc-aa4c-4956-ac39-9177d785e14a
updated: 1484310401
title: krypton
categories:
    - Dictionary
---
krypton
     n : a colorless element that is one of the six inert gasses;
         occurs in trace amounts in air [syn: {Kr}, {atomic number
         36}]
