---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hales%E2%80%93Jewett_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 1d252824-d69a-4d6c-b15e-4ff5e92e45d9
updated: 1484308669
title: Hales–Jewett theorem
tags:
    - Proof of Hales–Jewett theorem (in a special case)
    - Connections with other theorems
categories:
    - Theorems in discrete mathematics
---
In mathematics, the Hales–Jewett theorem is a fundamental combinatorial result of Ramsey theory named after Alfred W. Hales and Robert I. Jewett, concerning the degree to which high-dimensional objects must necessarily exhibit some combinatorial structure; it is impossible for such objects to be "completely random".[1]
