---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%C4%80ryabha%E1%B9%ADa%27s_sine_table'
offline_file: ""
offline_thumbnail: ""
uuid: 31d4fe0e-50a7-4295-bbfa-aeb811e1bb07
updated: 1484309338
title: "Āryabhaṭa's sine table"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Arc_and_chord.svg.png
tags:
    - The table
    - The original table
    - In modern notations
    - "Āryabhaṭa's computational method"
categories:
    - Trigonometry
---
Āryabhaṭa's sine table is a set of twenty-four numbers given in the astronomical treatise Āryabhaṭīya composed by the fifth century Indian mathematician and astronomer Āryabhaṭa (476–550 CE), for the computation of the half-chords of certain set of arcs of a circle. It is not a table in the modern sense of a mathematical table; that is, it is not a set of numbers arranged into rows and columns.[1][2]
