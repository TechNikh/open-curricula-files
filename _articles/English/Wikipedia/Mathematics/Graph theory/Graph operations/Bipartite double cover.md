---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bipartite_double_cover
offline_file: ""
offline_thumbnail: ""
uuid: 5dde3858-2af9-4749-adc6-8bea345a1912
updated: 1484309447
title: Bipartite double cover
tags:
    - Construction
    - Examples
    - Matrix interpretation
    - Properties
    - Other double covers
    - Notes
categories:
    - Graph operations
---
In graph theory, the bipartite double cover of an undirected graph G is a bipartite covering graph of G, with twice as many vertices as G. It can be constructed as the tensor product of graphs, G × K2. It is also called the Kronecker double cover, canonical double cover or simply the bipartite double of G.
