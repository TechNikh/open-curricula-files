---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leather
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484484121
title: leather
categories:
    - Dictionary
---
leather
     n : an animal skin made smooth and flexible by removing the hair
         and then tanning
