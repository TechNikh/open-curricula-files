---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/residence
offline_file: ""
offline_thumbnail: ""
uuid: 06c7837e-7354-42e0-8855-33b7f5d323d3
updated: 1484310455
title: residence
categories:
    - Dictionary
---
residence
     n 1: any address at which you dwell more than temporarily; "a
          person can have several residences" [syn: {abode}]
     2: the official house or establishment of an important person
        (as a sovereign or president); "he refused to live in the
        governor's residence"
     3: the act of dwelling in a place [syn: {residency}, {abidance}]
     4: a large and imposing house [syn: {mansion}, {mansion house},
         {manse}, {hall}]
