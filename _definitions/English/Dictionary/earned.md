---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/earned
offline_file: ""
offline_thumbnail: ""
uuid: f036e7c8-b2e4-409c-8180-414596dc9fe9
updated: 1484310445
title: earned
categories:
    - Dictionary
---
earned
     adj : gained or acquired; especially through merit or as a result
           of effort or action; "a well-earned reputation for
           honesty"; "earned income"; "an earned run in baseball"
           [ant: {unearned}]
