---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alimentary
offline_file: ""
offline_thumbnail: ""
uuid: 1bf9e492-5904-4ef1-9d36-42cc46aefafd
updated: 1484310321
title: alimentary
categories:
    - Dictionary
---
alimentary
     adj : of or providing nourishment; "good nourishing stew" [syn: {alimental},
            {nourishing}, {nutrient}, {nutritious}, {nutritive}]
