---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blackened
offline_file: ""
offline_thumbnail: ""
uuid: d5196fc8-9a4d-42f7-ba64-6147848e279e
updated: 1484310423
title: blackened
categories:
    - Dictionary
---
blackened
     adj 1: darkened by smoke; "blackened rafters"
     2: (of the face) made black especially as with suffused blood;
        "a face black with fury" [syn: {black}]
