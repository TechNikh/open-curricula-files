---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Davenport%E2%80%93Schinzel_sequence'
offline_file: ""
offline_thumbnail: ""
uuid: 69d51112-f2e2-44d2-bcde-9005bf96af23
updated: 1484308612
title: Davenport–Schinzel sequence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Line_segment_lower_envelope.svg.png
tags:
    - Definition
    - Length bounds
    - Application to lower envelopes
    - Notes
categories:
    - Discrete geometry
---
In combinatorics, a Davenport–Schinzel sequence is a sequence of symbols in which the number of times any two symbols may appear in alternation is limited. The maximum possible length of a Davenport–Schinzel sequence is bounded by the number of its distinct symbols multiplied by a small but nonconstant factor that depends on the number of alternations that are allowed. Davenport–Schinzel sequences were first defined in 1965 by Harold Davenport and Andrzej Schinzel to analyze linear differential equations. Following Atallah (1985) these sequences and their length bounds have also become a standard tool in discrete geometry and in the analysis of geometric algorithms.[1]
