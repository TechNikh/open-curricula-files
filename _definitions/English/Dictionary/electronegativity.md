---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electronegativity
offline_file: ""
offline_thumbnail: ""
uuid: 2b7eb0ea-f86d-4e43-8952-54375c4b2805
updated: 1484310403
title: electronegativity
categories:
    - Dictionary
---
electronegativity
     n : (chemistry) the tendency of an atom or radical to attract
         electrons in the formation of an ionic bond [syn: {negativity}]
