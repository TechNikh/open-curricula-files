---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neon
offline_file: ""
offline_thumbnail: ""
uuid: 6826dfdd-788e-40c6-9027-330e9ca1134b
updated: 1484310403
title: neon
categories:
    - Dictionary
---
neon
     n : a colorless odorless gaseous element that give a red glow in
         a vacuum tube; one of the six inert gasses; occurs in the
         air in small amounts [syn: {Ne}, {atomic number 10}]
