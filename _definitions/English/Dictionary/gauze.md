---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gauze
offline_file: ""
offline_thumbnail: ""
uuid: 90e0339d-3bab-49bc-bd67-e8b532634f8e
updated: 1484310375
title: gauze
categories:
    - Dictionary
---
gauze
     n 1: (medicine) bleached cotton cloth of plain weave used for
          bandages and dressings [syn: {gauze bandage}]
     2: a net of transparent fabric with a loose open weave [syn: {netting},
         {veiling}]
