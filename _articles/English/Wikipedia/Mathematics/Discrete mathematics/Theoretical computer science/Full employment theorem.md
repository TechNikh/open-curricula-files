---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Full_employment_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 3f318d94-204f-4a84-b204-4f4533156e28
updated: 1484308671
title: Full employment theorem
categories:
    - Theoretical computer science
---
In computer science and mathematics, a full employment theorem is a theorem which states that no algorithm can optimally perform a particular task done by some class of professionals. The name arises because such a theorem ensures that there is endless scope to keep discovering new techniques to improve the way at least some specific task is done.
