---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/utterly
offline_file: ""
offline_thumbnail: ""
uuid: c46b3256-dd5d-42f3-82eb-e57e0e6dc920
updated: 1484310569
title: utterly
categories:
    - Dictionary
---
utterly
     adv 1: completely and without qualification; used informally as
            intensifiers; "an absolutely magnificent painting"; "a
            perfectly idiotic idea"; "you're perfectly right";
            "utterly miserable"; "you can be dead sure of my
            innocence"; "was dead tired"; "dead right" [syn: {absolutely},
             {perfectly}, {dead}]
     2: with sublimity; in a sublime manner; "awaking in me,
        sublimely unconscious, interest and energy for tackling
        these tasks" [syn: {sublimely}]
