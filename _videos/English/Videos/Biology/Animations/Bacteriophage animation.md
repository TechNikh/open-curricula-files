---
version: 1
type: video
provider: YouTube
id: rzdwfwuVWUU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 07b28cbd-bcad-426f-80e6-a3fe3df9a853
updated: 1486069668
title: Bacteriophage animation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/rzdwfwuVWUU/default.jpg
    - https://i3.ytimg.com/vi/rzdwfwuVWUU/1.jpg
    - https://i3.ytimg.com/vi/rzdwfwuVWUU/2.jpg
    - https://i3.ytimg.com/vi/rzdwfwuVWUU/3.jpg
---
