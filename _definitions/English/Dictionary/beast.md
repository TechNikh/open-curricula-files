---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beast
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484478122
title: beast
categories:
    - Dictionary
---
beast
     n 1: a living organism characterized by voluntary movement [syn:
          {animal}, {animate being}, {brute}, {creature}, {fauna}]
     2: a cruelly rapacious person [syn: {wolf}, {savage}, {brute},
        {wildcat}]
