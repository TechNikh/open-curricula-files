---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%C3%89mile_Lemoine'
offline_file: ""
offline_thumbnail: ""
uuid: 6c2cb0e6-a7f3-41fa-856f-21611e3d0a09
updated: 1484309088
title: Émile Lemoine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ecole_Polytechnique_France_seen_from_lake_DSC03389_0.JPG
tags:
    - Biography
    - Early years (1840–1869)
    - Middle years (1870–1887)
    - Later years (1888–1912)
    - Contributions
    - Lemoine point and circle
    - Construction system
    - "Lemoine's conjecture and extensions"
    - Role in modern triangle geometry
    - List of selected works
    - Notes
categories:
    - Geometers
---
Émile Michel Hyacinthe Lemoine (French: [emil ləmwan]; 1840–1912) was a French civil engineer and a mathematician, a geometer in particular. He was educated at a variety of institutions, including the Prytanée National Militaire and, most notably, the École Polytechnique. Lemoine taught as a private tutor for a short period after his graduation from the latter school.
