---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strengthen
offline_file: ""
offline_thumbnail: ""
uuid: 23b852b6-bcfd-4ccb-be73-3ab904f5462e
updated: 1484310256
title: strengthen
categories:
    - Dictionary
---
strengthen
     v 1: make strong or stronger; "This exercise will strengthen your
          upper body"; "strenghten the relations between the two
          countries" [syn: {beef up}, {fortify}] [ant: {weaken}]
     2: gain strength; "His body strengthened" [ant: {weaken}]
     3: give a healthy elasticity to; "Let's tone our muscles" [syn:
         {tone}, {tone up}]
