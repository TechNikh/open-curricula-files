---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drilling
offline_file: ""
offline_thumbnail: ""
uuid: fc30d213-4cc5-476f-80a3-9bb69e9ebcb7
updated: 1484310258
title: drilling
categories:
    - Dictionary
---
drilling
     n 1: the act of drilling [syn: {boring}]
     2: the act of drilling a hole in the earth in the hope of
        producing petroleum [syn: {boring}, {oil production}]
