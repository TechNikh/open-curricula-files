---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/certificate
offline_file: ""
offline_thumbnail: ""
uuid: d72d1c29-cf09-45e2-a788-77c965a6b0e2
updated: 1484310429
title: certificate
categories:
    - Dictionary
---
certificate
     n 1: a document attesting to the truth of certain stated facts
          [syn: {certification}, {credential}, {credentials}]
     2: a formal declaration that documents a fact of relevance to
        finance and investment; the holder has a right to receive
        interest or dividends; "he held several valuable
        securities" [syn: {security}]
     v 1: present someone with a certificate
     2: authorize by certificate
