---
version: 1
type: video
provider: YouTube
id: dsVD1pRGyJc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7114a35b-4e85-4846-aa3a-ef0fd999544a
updated: 1486069601
title: >
    యాంత్రిక బలానికి
    పరిచయం
tags:
    - Physics
    - telugu
    - Mechanics
    - mechanical advantage
categories:
    - భౌతిక శాస్త్రం
thumbnail_urls:
    - https://i3.ytimg.com/vi/dsVD1pRGyJc/default.jpg
    - https://i3.ytimg.com/vi/dsVD1pRGyJc/1.jpg
    - https://i3.ytimg.com/vi/dsVD1pRGyJc/2.jpg
    - https://i3.ytimg.com/vi/dsVD1pRGyJc/3.jpg
---
Translated from original video
Title - Introduction to mechanical advantage
URL -http://www.khanacademy.org/science/physics/mechanics/v/introduction-to-mechanical-advantage
