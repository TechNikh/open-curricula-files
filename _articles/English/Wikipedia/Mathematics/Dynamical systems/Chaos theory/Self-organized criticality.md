---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-organized_criticality
offline_file: ""
offline_thumbnail: ""
uuid: a69c4536-b781-4cad-b381-eea8a56e1994
updated: 1484308713
title: Self-organized criticality
tags:
    - Overview
    - Examples of self-organized critical dynamics
categories:
    - Chaos theory
---
In physics, self-organized criticality (SOC) is a property of dynamical systems that have a critical point as an attractor. Their macroscopic behaviour thus displays the spatial and/or temporal scale-invariance characteristic of the critical point of a phase transition, but without the need to tune control parameters to a precise value, because the system, effectively, tunes itself as it evolves towards criticality.
