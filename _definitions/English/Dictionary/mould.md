---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mould
offline_file: ""
offline_thumbnail: ""
uuid: 15d1fee3-d367-45ac-909c-844cdc71b086
updated: 1484310144
title: mould
categories:
    - Dictionary
---
mould
     n 1: loose soil rich in organic matter [syn: {mold}]
     2: a fungus that produces a superficial growth on various kinds
        of damp or decaying organic matter [syn: {mold}]
     3: sculpture produced by molding [syn: {mold}, {molding}, {moulding},
         {modeling}, {clay sculpture}]
     4: container into which liquid is poured to create a given
        shape when it hardens [syn: {mold}, {cast}]
     v 1: form in clay, wax, etc; "model a head with clay" [syn: {model},
           {mold}]
     2: form by pouring (e.g., wax or hot metal) into a cast or
        mold; "cast a bronze sculpture" [syn: {cast}, {mold}]
     3: make something, usually for a specific function; ...
