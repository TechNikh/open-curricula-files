---
version: 1
type: video
provider: YouTube
id: R948Tsyq4vA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Finding%20the%20slope%20of%20a%20line%20from%20its%20graph.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e2b8260c-3036-4993-afb1-084980998fda
updated: 1486069559
title: Finding the slope of a line from its graph
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/R948Tsyq4vA/default.jpg
    - https://i3.ytimg.com/vi/R948Tsyq4vA/1.jpg
    - https://i3.ytimg.com/vi/R948Tsyq4vA/2.jpg
    - https://i3.ytimg.com/vi/R948Tsyq4vA/3.jpg
---
