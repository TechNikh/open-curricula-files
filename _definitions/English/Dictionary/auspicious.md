---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/auspicious
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484569681
title: auspicious
categories:
    - Dictionary
---
auspicious
     adj 1: attended by favorable circumstances; "an auspicious
            beginning for the campaign" [ant: {inauspicious}]
     2: tending to favor or bring good luck; "miracles are
        auspicious accidents"; "encouraging omens"; "a favorable
        time to ask for a raise"; "lucky stars"; "a prosperous
        moment to make a decision" [syn: {encouraging}, {favorable},
         {favourable}, {lucky}, {prosperous}]
