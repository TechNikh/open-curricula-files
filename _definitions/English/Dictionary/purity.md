---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purity
offline_file: ""
offline_thumbnail: ""
uuid: 85987391-d6a1-46a6-b5da-901c0b41805e
updated: 1484310144
title: purity
categories:
    - Dictionary
---
purity
     n 1: being undiluted or unmixed with extraneous material [syn: {pureness}]
          [ant: {impurity}]
     2: the state of being free from sin or moral wrong; lacking a
        knowledge of evil [syn: {sinlessness}, {innocence}]
     3: a woman's virtue or chastity [syn: {honor}, {honour}]
