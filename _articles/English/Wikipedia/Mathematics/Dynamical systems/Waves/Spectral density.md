---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spectral_density
offline_file: ""
offline_thumbnail: ""
uuid: b6b188a5-52f3-4e32-ac1f-e4569b1e46a3
updated: 1484308898
title: Spectral density
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fluorescent_lighting_spectrum_peaks_labelled.svg.png
tags:
    - Explanation
    - Definition
    - Energy spectral density
    - Power spectral density
    - Properties of the power spectral density
    - Cross-spectral density
    - Estimation
    - Properties
    - Related concepts
    - Applications
    - Electrical engineering
    - Cosmology
    - Notes
categories:
    - Waves
---
The power spectrum 
  
    
      
        
          S
          
            x
            x
          
        
        (
        f
        )
      
    
    {\displaystyle S_{xx}(f)}
  
 of a time series 
  
    
      
        x
        (
        t
        )
      
    
    {\displaystyle x(t)}
  
 describes the distribution of power into frequency components composing that signal. According to Fourier analysis any physical signal can be decomposed into a number of discrete frequencies, or a spectrum of frequencies over a continuous range. The statistical average of a certain signal or sort of signal (including noise) as analyzed in terms of its frequency content, is called its ...
