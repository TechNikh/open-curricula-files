---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-surveyable_proof
offline_file: ""
offline_thumbnail: ""
uuid: 861c5eb2-533a-4b2e-91f7-81858f796500
updated: 1484309497
title: Non-surveyable proof
tags:
    - "Tymoczko's argument"
    - "Counterarguments to Tymoczko's claims of non-surveyability"
    - Countermeasures against non-surveyability
categories:
    - Proof theory
---
In the philosophy of mathematics, a non-surveyable proof is a mathematical proof that is considered infeasible for a human mathematician to verify and so of controversial validity. The term was coined by Thomas Tymoczko in 1980 in criticism of Kenneth Appel and Wolfgang Haken's computer-assisted proof of the Four-Color Theorem, and has since been applied to other arguments, mainly those with excessive case splitting and/or with portions dispatched by a difficult-to-verify computer program. Surveyability remains an important consideration in computational mathematics.
