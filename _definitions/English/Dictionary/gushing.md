---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gushing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484560921
title: gushing
categories:
    - Dictionary
---
gushing
     adj 1: flowing profusely; "a gushing hydrant"; "pouring flood
            waters" [syn: {pouring}]
     2: uttered with unrestrained enthusiasm; "a novel told in
        burbly panting tones" [syn: {burbling}, {burbly}, {effusive}]
     3: extravagantly demonstrative; "insincere and effusive
        demonstrations of sentimental friendship"; "a large
        gushing female"; "write unrestrained and gushy poetry"
        [syn: {effusive}, {emotional}, {gushing(a)}, {gushy}]
