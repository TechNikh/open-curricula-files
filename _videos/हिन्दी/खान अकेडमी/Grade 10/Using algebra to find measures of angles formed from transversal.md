---
version: 1
type: video
provider: YouTube
id: 3XJBoS91BgY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 397bcf0a-f0fe-482e-80d7-eaa0a45aafba
updated: 1486069621
title: >
    Using algebra to find measures of angles formed from
    transversal
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/3XJBoS91BgY/default.jpg
    - https://i3.ytimg.com/vi/3XJBoS91BgY/1.jpg
    - https://i3.ytimg.com/vi/3XJBoS91BgY/2.jpg
    - https://i3.ytimg.com/vi/3XJBoS91BgY/3.jpg
---
