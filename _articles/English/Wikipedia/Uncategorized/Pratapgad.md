---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pratapgad
offline_file: ""
offline_thumbnail: ""
uuid: cf08e44a-911e-4aa8-b6cd-15ad0329019b
updated: 1484308450
title: Pratapgad
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/196px-Statue_of_Chhatrapati_Shivaji_Maharaj.jpg
tags:
    - geography
    - History
    - Structure
    - Tourism
categories:
    - Uncategorized
---
Pratapgad (also transcribed Pratapgarh or Pratapgadh) literally 'Valour Fort' is a large fort located in Satara district, in the Western Indian state of Maharashtra. Significant as the site of the Battle of Pratapgad, the fort is now a popular tourist destination.
