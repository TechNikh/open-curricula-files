---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kaishek
offline_file: ""
offline_thumbnail: ""
uuid: 8a1c7e70-9f69-4c25-b182-40272d28265d
updated: 1484308485
title: Chiang Kai-shek
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Wang_Jingwei_and_Chiang_Kai-shek.jpg
tags:
    - Names
    - life
    - Childhood
    - Education in Japan
    - Return to China
    - Establishment of the Kuomintang
    - Competition with Wang Jingwei
    - Rising power
    - Rule
    - First phase of the Chinese Civil War
categories:
    - Uncategorized
---
Chiang Kai-shek (October 31, 1887 – April 5, 1975), also romanized as Jiang Jieshi and known as Jiang Zhongzheng, was a Chinese political and military leader who served as the leader of the Republic of China between 1928 and 1975. Chiang was an influential member of the Kuomintang (KMT), the Chinese Nationalist Party, and was a close ally of Sun Yat-sen's. He became the Commandant of the Kuomintang's Whampoa Military Academy and took Sun's place as leader of the KMT, following the Canton Coup in early 1926. Having neutralized the party's left wing, Chiang then led Sun's long-postponed Northern Expedition, conquering or reaching accommodations with China's many warlords.[3]
