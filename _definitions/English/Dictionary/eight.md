---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eight
offline_file: ""
offline_thumbnail: ""
uuid: 1c8aff2a-decc-4614-a968-23669c849750
updated: 1484310397
title: eight
categories:
    - Dictionary
---
eight
     adj : being one more than seven [syn: {8}, {viii}]
     n 1: the cardinal number that is the sum of seven and one [syn: {8},
           {VIII}, {eighter}, {eighter from Decatur}, {octad}, {ogdoad},
           {octonary}, {octet}]
     2: a group of United States painters founded in 1907 and noted
        for their realistic depictions of sordid aspects of city
        life [syn: {Ashcan School}]
