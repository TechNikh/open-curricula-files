---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generatrix
offline_file: ""
offline_thumbnail: ""
uuid: 828d2864-ec00-4e4c-8509-d7e8e71b9904
updated: 1484308927
title: Generatrix
categories:
    - Elementary geometry
---
In geometry, a generatrix or generator is a point, curve or surface that, when moved along a given path, generates a new shape.[1] The path directing the motion of the generatrix motion is called a directrix.[2]
