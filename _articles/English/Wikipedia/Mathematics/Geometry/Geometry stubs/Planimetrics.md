---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Planimetrics
offline_file: ""
offline_thumbnail: ""
uuid: 06757cec-480c-4980-84a5-e83e7f9f8640
updated: 1484309373
title: Planimetrics
categories:
    - Geometry stubs
---
To measure planimetrics a planimeter is used. This rather advanced analog technology is being taken over by simple Image Measurement software tools like, ImageJ, Adobe Acrobat, Google Pro Earth, Gimp, Photoshop and KLONK Image Measurement which can help do this kind of work from digitalized images.
