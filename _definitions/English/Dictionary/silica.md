---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/silica
offline_file: ""
offline_thumbnail: ""
uuid: 4db3d37d-35bb-44cd-bdc1-599d56c85bcd
updated: 1484310413
title: silica
categories:
    - Dictionary
---
silica
     n : a white or colorless vitreous insoluble solid (SiO2);
         various forms occur widely in the earth's crust as quartz
         or cristobalite or tridymite or lechartelierite [syn: {silicon
         oxide}, {silicon dioxide}]
