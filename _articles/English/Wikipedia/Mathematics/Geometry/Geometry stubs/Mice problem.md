---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mice_problem
offline_file: ""
offline_thumbnail: ""
uuid: 1e18cbab-42c3-4f26-a8c3-5a7c35d94484
updated: 1484309367
title: Mice problem
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/N%253D2.jpg'
tags:
    - Path of the mice
categories:
    - Geometry stubs
---
In mathematics, the mice problem is a problem in which a number of mice (or insects, dogs, missiles, etc.) are placed at the corners of a regular polygon. Each mouse begins to move towards its immediate neighbour (clockwise or anticlockwise). It must be determined when the mice meet.
