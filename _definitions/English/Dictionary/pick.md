---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pick
offline_file: ""
offline_thumbnail: ""
uuid: 8a53655d-90ea-4210-a24c-e1bc7543675d
updated: 1484310299
title: pick
categories:
    - Dictionary
---
pick
     n 1: the person or thing chosen or selected; "he was my pick for
          mayor" [syn: {choice}, {selection}]
     2: the quantity of a crop that is harvested; "he sent the first
        picking of berries to the market"; "it was the biggest
        peach pick in years" [syn: {picking}]
     3: the best people or things in a group; "the cream of
        England's young men were killed in the Great War" [syn: {cream}]
     4: the yarn woven across the warp yarn in weaving [syn: {woof},
         {weft}, {filling}]
     5: a small thin device (of metal or plastic or ivory) used to
        pluck a stringed instrument [syn: {plectrum}, {plectron}]
     6: a thin sharp implement used ...
