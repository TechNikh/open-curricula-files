---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Elementary_algebra
offline_file: ""
offline_thumbnail: ""
uuid: 918da245-f012-407e-9581-7015d930420e
updated: 1484308898
title: Elementary algebra
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Quadratic_root.svg.png
tags:
    - Algebraic notation
    - Alternative notation
    - concepts
    - Variables
    - Evaluating expressions
    - Equations
    - Properties of equality
    - Properties of inequality
    - Substitution
    - Solving algebraic equations
    - Linear equations with one variable
    - Linear equations with two variables
    - Quadratic equations
    - Complex numbers
    - Exponential and logarithmic equations
    - Radical equations
    - System of linear equations
    - Elimination method
    - Substitution method
    - Other types of systems of linear equations
    - Inconsistent systems
    - Undetermined systems
    - 'Over- and underdetermined systems'
categories:
    - Elementary algebra
---
Elementary algebra encompasses some of the basic concepts of algebra, one of the main branches of mathematics. It is typically taught to secondary school students and builds on their understanding of arithmetic. Whereas arithmetic deals with specified numbers,[1] algebra introduces quantities without fixed values, known as variables.[2] This use of variables entails a use of algebraic notation and an understanding of the general rules of the operators introduced in arithmetic. Unlike abstract algebra, elementary algebra is not concerned with algebraic structures outside the realm of real and complex numbers.
