---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sarpanch
offline_file: ""
offline_thumbnail: ""
uuid: c467873a-bfb0-47e5-a66e-28efddf2ee42
updated: 1484308451
title: Sarpanch
tags:
    - Meaning of sarpanch
    - Panchayati raj (governance by sarpanch)
    - Panchayat elections
    - Reservation for females
categories:
    - Uncategorized
---
A sarpanch is an elected head of a village-level statutory institution of local self-government called the panchayat (village government) in India (gram panchayat),[1] Pakistan and Bangladesh. The sarpanch, together with other elected panchas (members), constitute the gram panchayat. The sarpanch is the focal point of contact between government officers and the village community. Recently, there have been proposals to give sarpanches small judicial powers under panchayati raj. In some states of India like Bihar, Sarpanch has been empowered to look into various civil and criminal cases, and given judicial power to punish and impose fine on those violating rules.[2]
