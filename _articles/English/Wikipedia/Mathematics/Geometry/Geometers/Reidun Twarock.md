---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Reidun_Twarock
offline_file: ""
offline_thumbnail: ""
uuid: 65e534f9-8cff-48df-989f-ff23d27f7c86
updated: 1484309103
title: Reidun Twarock
tags:
    - Education
    - Research
categories:
    - Geometers
---
Reidun Twarock is a German-born mathematical biologist at the University of York. She is known for developing mathematical models of viruses based on higher-dimensional lattices.[1]
