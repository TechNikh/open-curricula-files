---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Normal_form_(abstract_rewriting)
offline_file: ""
offline_thumbnail: ""
uuid: 244497eb-520c-49ea-9d06-e5181cfcf032
updated: 1484309489
title: Normal form (abstract rewriting)
tags:
    - Definition
    - Normalization properties
    - Normalization and Confluency
    - Notes
categories:
    - Computability theory
---
In abstract rewriting, an object is in normal form if it cannot be rewritten any further. Depending on the rewriting system and the object, several normal forms may exist, or none at all.
