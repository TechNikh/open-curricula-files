---
version: 1
type: video
provider: YouTube
id: rK2DIF_tgCg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Transport%20of%20Water%20and%20Salts%20in%20Plants%20-%20Science.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8941d6d5-7baa-4806-bf54-dfc9c7c3ff0a
updated: 1486069666
title: 'Transport of Water and Salts in Plants - Science'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/rK2DIF_tgCg/default.jpg
    - https://i3.ytimg.com/vi/rK2DIF_tgCg/1.jpg
    - https://i3.ytimg.com/vi/rK2DIF_tgCg/2.jpg
    - https://i3.ytimg.com/vi/rK2DIF_tgCg/3.jpg
---
