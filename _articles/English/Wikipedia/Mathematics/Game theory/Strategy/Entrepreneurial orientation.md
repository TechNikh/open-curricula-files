---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entrepreneurial_orientation
offline_file: ""
offline_thumbnail: ""
uuid: 983461de-9c4e-46a9-abb0-dc17a1285a9b
updated: 1484309069
title: Entrepreneurial orientation
categories:
    - Strategy
---
Entrepreneurial orientation (EO) is a firm-level strategic orientation which captures an organization's strategy-making practices, managerial philosophies, and firm behaviors that are entrepreneurial in nature.[1] Entrepreneurial orientation has become one of the most established and researched constructs in the entrepreneurship literature.[2][3][4] A general commonality among past conceptualizations of EO is the inclusion of innovativeness, proactiveness, and risk-taking as core defining aspects or dimensions of the orientation.[5][6] EO has been shown to be a strong predictor of firm performance with a meta-analysis of past research indicating a correlation in magnitude roughly equivalent ...
