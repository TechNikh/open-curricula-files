---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lewis
offline_file: ""
offline_thumbnail: ""
uuid: 62ba0668-9ebd-4558-83be-faf02526ef29
updated: 1484308384
title: Lewis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Calanais_Standing_Stones_20090610_01.jpg
tags:
    - Name
    - History
    - Historical sites
    - Geography and geology
    - Geology
    - Climate
    - Nature
    - birds
    - Marine life
    - Land mammals
    - Reptiles and amphibians
    - Insects
    - Plant life
    - Politics and government
    - Current representatives
    - Demographics
    - Parishes and districts of Lewis
    - Settlements
    - Economy
    - Industry
    - Commerce
    - Transport
    - Peats
    - religion
    - Education
    - Culture and sport
    - language
    - Media and the arts
    - Sport
    - Myths and legends
    - Gastronomy
    - People with Lewis connections
    - Notes
categories:
    - Uncategorized
---
Lewis (Scottish Gaelic: Leòdhas, pronounced [ʎɔː.əs̪], also Isle of Lewis) is the northern part of Lewis and Harris, the largest island of the Western Isles or Outer Hebrides (an archipelago) of Scotland. The total area of Lewis is 683 square miles (1,770 km2).[1]
