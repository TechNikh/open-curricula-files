---
version: 1
type: article
id: https://en.wikipedia.org/wiki/CPT_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 3adcd9e2-44b3-438b-88ee-d79ae548ef57
updated: 1484309307
title: CPT symmetry
tags:
    - History
    - Derivation of the CPT theorem
    - Consequences and implications
    - Sources
categories:
    - Symmetry
---
Charge, Parity, and Time Reversal Symmetry is a fundamental symmetry of physical laws under the simultaneous transformations of charge conjugation (C), parity transformation (P), and time reversal (T). CPT is the only combination of C, P and T that is observed to be an exact symmetry of nature at the fundamental level.[1] The CPT theorem says that CPT symmetry holds for all physical phenomena, or more precisely, that any Lorentz invariant local quantum field theory with a Hermitian Hamiltonian must have CPT symmetry.
