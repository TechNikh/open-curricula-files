---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transitive_relation
offline_file: ""
offline_thumbnail: ""
uuid: bb783269-0af8-4423-aeea-d7fa612f1286
updated: 1484308912
title: Transitive relation
tags:
    - Formal definition
    - Examples
    - Properties
    - Closure properties
    - Other properties
    - Properties that require transitivity
    - Counting transitive relations
    - Sources
    - Bibliography
categories:
    - Elementary algebra
---
In mathematics, a binary relation R over a set X is transitive if whenever an element a is related to an element b, and b is in turn related to an element c, then a is also related to c. Transitivity is a key property of both partial order relations and equivalence relations.
