---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vertex
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484343061
title: vertex
categories:
    - Dictionary
---
vertex
     n 1: the point of intersection of lines or the point opposite the
          base of a figure
     2: the highest point (of something); "at the peak of the
        pyramid" [syn: {peak}, {apex}, {acme}]
     [also: {vertices} (pl)]
