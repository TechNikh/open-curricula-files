---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/centimeter
offline_file: ""
offline_thumbnail: ""
uuid: 5a7c4f6e-5ffe-4ba9-8edf-2232ed8c607f
updated: 1484310416
title: centimeter
categories:
    - Dictionary
---
centimeter
     n : a metric unit of length equal to one hundredth of a meter
         [syn: {centimetre}, {cm}]
