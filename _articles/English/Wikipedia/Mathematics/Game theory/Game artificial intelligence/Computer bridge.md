---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computer_bridge
offline_file: ""
offline_thumbnail: ""
uuid: b943794e-988c-420a-a4ed-897b091c114c
updated: 1484309004
title: Computer bridge
tags:
    - World Computer-Bridge Championship
    - Computers versus humans
    - Cardplay algorithms
    - Comparison to other strategy games
    - The future
categories:
    - Game artificial intelligence
---
Computer bridge is the playing of the game contract bridge using computer software. After years of limited progress, since around the end of the 20th century the field of computer bridge has made major advances. In 1996 the American Contract Bridge League (ACBL) established an official World Computer-Bridge Championship, to be held annually along with a major bridge event. The first championship took place in 1997 at the North American Bridge Championships in Albuquerque. Since 1999 the event has been conducted as a joint activity of the American Contract Bridge League and the World Bridge Federation. Alvin Levy, ACBL Board member, initiated this championship and has coordinated the event ...
