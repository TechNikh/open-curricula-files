---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wheels
offline_file: ""
offline_thumbnail: ""
uuid: 01842601-24c6-4026-9dd3-0d1f50fbb249
updated: 1484310518
title: wheels
categories:
    - Dictionary
---
wheels
     n : forces that provide energy and direction; "the wheels of
         government began to turn"
