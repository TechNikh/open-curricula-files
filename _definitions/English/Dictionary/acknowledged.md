---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acknowledged
offline_file: ""
offline_thumbnail: ""
uuid: 9381af13-0916-4a4f-a188-719fdfa33373
updated: 1484310413
title: acknowledged
categories:
    - Dictionary
---
acknowledged
     adj 1: recognized or made known or admitted; "the acknowledged
            leader of the community"; "a woman of acknowledged
            accomplishments"; "his acknowledged error" [ant: {unacknowledged}]
     2: generally accepted
