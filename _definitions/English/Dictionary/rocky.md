---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rocky
offline_file: ""
offline_thumbnail: ""
uuid: 5459e703-2413-4ff8-8be9-4f325ef3d184
updated: 1484310409
title: rocky
categories:
    - Dictionary
---
rocky
     adj 1: abounding in rocks or stones; "rocky fields"; "stony
            ground"; "bouldery beaches" [syn: {bouldery}, {bouldered},
             {stony}]
     2: liable to rock; "on high rocky heels"
     3: full of hardship or trials; "the rocky road to success";
        "they were having a rough time" [syn: {rough}]
     [also: {rockiest}, {rockier}]
