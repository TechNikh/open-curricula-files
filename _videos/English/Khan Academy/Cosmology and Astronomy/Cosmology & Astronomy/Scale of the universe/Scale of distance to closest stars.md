---
version: 1
type: video
provider: YouTube
id: jEeJkkMXt6c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Scale%20of%20distance%20to%20closest%20stars.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 530e6ac4-399f-4f8d-b1f4-3eb9d411f310
updated: 1486069688
title: Scale of distance to closest stars
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/jEeJkkMXt6c/default.jpg
    - https://i3.ytimg.com/vi/jEeJkkMXt6c/1.jpg
    - https://i3.ytimg.com/vi/jEeJkkMXt6c/2.jpg
    - https://i3.ytimg.com/vi/jEeJkkMXt6c/3.jpg
---
