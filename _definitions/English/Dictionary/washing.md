---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/washing
offline_file: ""
offline_thumbnail: ""
uuid: 80897b16-003a-4d53-8867-6cd11582a4fb
updated: 1484310252
title: washing
categories:
    - Dictionary
---
washing
     n 1: the work of cleansing (usually with soap and water) [syn: {wash},
           {lavation}]
     2: garments or white goods that can be cleaned by laundering
        [syn: {laundry}, {wash}, {washables}]
