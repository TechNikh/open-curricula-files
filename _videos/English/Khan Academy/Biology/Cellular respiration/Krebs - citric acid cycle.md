---
version: 1
type: video
provider: YouTube
id: juM2ROSLWfw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Cellular%20respiration/Krebs%20-%20citric%20acid%20cycle.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 429a04ca-4ac1-4908-ba9e-3c898e89df27
updated: 1486069581
title: Krebs / citric acid cycle
tags:
    - dotsub
categories:
    - Cellular respiration
thumbnail_urls:
    - https://i3.ytimg.com/vi/juM2ROSLWfw/default.jpg
    - https://i3.ytimg.com/vi/juM2ROSLWfw/1.jpg
    - https://i3.ytimg.com/vi/juM2ROSLWfw/2.jpg
    - https://i3.ytimg.com/vi/juM2ROSLWfw/3.jpg
---
Overview of the Krebs or Citric Acid Cycle

Watch the next lesson: https://www.khanacademy.org/science/biology/cellular-respiration-and-fermentation/oxidative-phosphorylation/v/oxidative-phosphorylation-and-the-electon-transport-chain?utm_source=YT&utm_medium=Desc&utm_campaign=biology

Missed the previous lesson? https://www.khanacademy.org/science/biology/cellular-respiration-and-fermentation/glycolysis/v/glycolysis-overview?utm_source=YT&utm_medium=Desc&utm_campaign=biology

Biology on Khan Academy: Life is beautiful! From atoms to cells, from genes to proteins, from populations to ecosystems, biology is the study of the fascinating and intricate systems that make life possible. Dive in to learn more about the many branches of biology and why they are exciting and important. Covers topics seen in a high school or first-year college biology course.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy's Biology channel: https://www.youtube.com/channel/UC82qE46vcTn7lP4tK_RHhdg?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
