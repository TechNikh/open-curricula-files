---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prison
offline_file: ""
offline_thumbnail: ""
uuid: 4b3e06cb-5287-4ce0-9246-ff8a08ea673e
updated: 1484310585
title: prison
categories:
    - Dictionary
---
prison
     n 1: a correctional institution where persons are confined while
          on trial or for punishment [syn: {prison house}]
     2: a prisonlike situation; a place of seeming confinement [syn:
         {prison house}]
