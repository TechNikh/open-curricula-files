---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/halt
offline_file: ""
offline_thumbnail: ""
uuid: edc0402a-606f-4fb0-8489-32e2382f2d8a
updated: 1484310204
title: halt
categories:
    - Dictionary
---
halt
     adj : disabled in the feet or legs; "a crippled soldier"; "a game
           leg" [syn: {crippled}, {halting}, {lame}, {game}]
     n 1: the state of inactivity following an interruption; "the
          negotiations were in arrest"; "held them in check";
          "during the halt he got some lunch"; "the momentary stay
          enabled him to escape the blow"; "he spent the entire
          stop in his seat" [syn: {arrest}, {check}, {hitch}, {stay},
           {stop}, {stoppage}]
     2: the event of something ending; "it came to a stop at the
        bottom of the hill" [syn: {stop}]
     3: an interruption or temporary suspension of progress or
        movement; "a halt in the ...
