---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/permanent
offline_file: ""
offline_thumbnail: ""
uuid: b22d8e01-37af-4a6e-971f-f342cbd2d998
updated: 1484310371
title: permanent
categories:
    - Dictionary
---
permanent
     adj 1: continuing or enduring without marked change in status or
            condition or place; "permanent secretary to the
            president"; "permanent address"; "literature of
            permanent value" [syn: {lasting}] [ant: {impermanent}]
     2: not capable of being reversed or returned to the original
        condition; "permanent brain damage"
     n : a series of waves in the hair made by applying heat and
         chemicals [syn: {permanent wave}, {perm}]
