---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eastwards
offline_file: ""
offline_thumbnail: ""
uuid: ab17c5dd-62b7-4121-adb5-7f219d347b5d
updated: 1484310461
title: eastwards
categories:
    - Dictionary
---
eastwards
     adv : toward the east; "they migrated eastward to Sweden" [syn: {eastward}]
