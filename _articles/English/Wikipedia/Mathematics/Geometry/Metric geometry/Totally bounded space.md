---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Totally_bounded_space
offline_file: ""
offline_thumbnail: ""
uuid: dcd98760-4a4e-40e8-b289-ee19f91d69a3
updated: 1484309273
title: Totally bounded space
tags:
    - Definition for a metric space
    - Definitions in other contexts
    - Examples and nonexamples
    - Relationships with compactness and completeness
    - Use of the axiom of choice
    - Notes
categories:
    - Metric geometry
---
In topology and related branches of mathematics, a totally bounded space is a space that can be covered by finitely many subsets of any fixed "size" (where the meaning of "size" depends on the given context). The smaller the size fixed, the more subsets may be needed, but any specific size should require only finitely many subsets. A related notion is a totally bounded set, in which only a subset of the space needs to be covered. Every subset of a totally bounded space is a totally bounded set; but even if a space is not totally bounded, some of its subsets still will be.
