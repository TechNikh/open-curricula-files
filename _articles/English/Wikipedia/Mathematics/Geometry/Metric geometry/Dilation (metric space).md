---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dilation_(metric_space)
offline_file: ""
offline_thumbnail: ""
uuid: 43f88f74-7b6c-4fd7-8f1a-2b3bfe91ae91
updated: 1484309265
title: Dilation (metric space)
categories:
    - Metric geometry
---
In mathematics, a dilation is a function 
  
    
      
        f
      
    
    {\displaystyle f}
  
 from a metric space into itself that satisfies the identity
