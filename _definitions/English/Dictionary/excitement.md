---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/excitement
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484598721
title: excitement
categories:
    - Dictionary
---
excitement
     n 1: the feeling of lively and cheerful joy; "he could hardly
          conceal his excitement when she agreed" [syn: {exhilaration}]
     2: the state of being emotionally aroused and worked up; "his
        face was flushed with excitement and his hands trembled";
        "he tried to calm those who were in a state of extreme
        inflammation" [syn: {excitation}, {inflammation}, {fervor},
         {fervour}]
     3: something that agitates and arouses; "he looked forward to
        the excitements of the day" [syn: {excitation}]
     4: disturbance usually in protest [syn: {agitation}, {turmoil},
         {upheaval}, {hullabaloo}]
