---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pair
offline_file: ""
offline_thumbnail: ""
uuid: a55fd023-970d-470e-807f-3cc0e9159d6b
updated: 1484310303
title: pair
categories:
    - Dictionary
---
pair
     n 1: a set of two similar things considered as a unit [syn: {brace}]
     2: two items of the same kind [syn: {couple}, {twosome}, {twain},
         {brace}, {span}, {yoke}, {couplet}, {distich}, {duo}, {duet},
         {dyad}, {duad}]
     3: two people considered as a unit
     4: a poker hand with 2 cards of the same value
     v 1: form a pair or pairs; "The two old friends paired off" [syn:
           {pair off}, {partner off}, {couple}]
     2: bring two objects, ideas, or people together; "This fact is
        coupled to the other one"; "Matchmaker, can you match my
        daughter with a nice young man?"; "The student was paired
        with a partner for collaboration on ...
