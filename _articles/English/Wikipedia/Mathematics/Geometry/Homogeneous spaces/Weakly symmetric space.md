---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weakly_symmetric_space
offline_file: ""
offline_thumbnail: ""
uuid: 38919511-753e-4407-81cf-493efb0a50ec
updated: 1484309249
title: Weakly symmetric space
categories:
    - Homogeneous spaces
---
In mathematics, a weakly symmetric space is a notion introduced by the Norwegian mathematician Atle Selberg in the 1950s as a generalisation of symmetric space, due to Élie Cartan. Geometrically the spaces are defined as complete Riemannian manifolds such that any two points can be exchanged by an isometry, the symmetric case being when the isometry is required to have period two. The classification of weakly symmetric spaces relies on that of periodic automorphisms of complex semisimple Lie algebras. They provide examples of Gelfand pairs, although the corresponding theory of spherical functions in harmonic analysis, known for symmetric spaces, has not yet been developed.
