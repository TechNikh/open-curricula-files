---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cycle_space
offline_file: ""
offline_thumbnail: ""
uuid: 6f60956f-1b01-40b3-804b-f34f82c2a081
updated: 1484309373
title: Cycle space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Cycle_space_addition.svg_0.png
tags:
    - Definitions
    - Graph theory
    - Algebra
    - Topology
    - Circuit rank
    - Cycle bases
    - Existence
    - Fundamental and weakly fundamental bases
    - Minimum weight bases
    - Planar graphs
    - Homology
    - "Mac Lane's planarity criterion"
    - Duality
    - Nowhere zero flows
categories:
    - Algebraic graph theory
---
This set of subgraphs can be described algebraically as a vector space over the two-element finite field. The dimension of this space is the circuit rank of the graph. The same space can also be described in terms from algebraic topology as the first homology group of the graph. Using homology theory, the binary cycle space may be generalized to cycle spaces over arbitrary rings.
