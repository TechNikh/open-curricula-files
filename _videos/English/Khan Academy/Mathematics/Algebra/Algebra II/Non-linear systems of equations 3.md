---
version: 1
type: video
provider: YouTube
id: FksgVpM_iXs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Non-linear%20systems%20of%20equations%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 87674270-c537-4e15-be6a-fac37e0abbeb
updated: 1486069565
title: Non-linear systems of equations 3
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/FksgVpM_iXs/default.jpg
    - https://i3.ytimg.com/vi/FksgVpM_iXs/1.jpg
    - https://i3.ytimg.com/vi/FksgVpM_iXs/2.jpg
    - https://i3.ytimg.com/vi/FksgVpM_iXs/3.jpg
---
