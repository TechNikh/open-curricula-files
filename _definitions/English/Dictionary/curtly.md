---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curtly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484543881
title: curtly
categories:
    - Dictionary
---
curtly
     adv : in a curt, abrupt and discourteous manner; "he told me
           curtly to get on with it"; "he talked short with
           everyone"; "he said shortly that he didn't like it"
           [syn: {short}, {shortly}]
