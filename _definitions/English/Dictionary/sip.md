---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sip
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484488201
title: sip
categories:
    - Dictionary
---
sip
     n : a small drink [syn: {nip}]
     v : drink in sips; "She was sipping her tea"
     [also: {sipping}, {sipped}]
