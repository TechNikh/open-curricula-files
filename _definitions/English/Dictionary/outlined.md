---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outlined
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484582221
title: outlined
categories:
    - Dictionary
---
outlined
     adj : showing clearly the outline or profile or boundary; "hills
           defined against the evening sky"; "the setting sun
           showed the outlined figure of a man standing on the
           hill" [syn: {defined}]
