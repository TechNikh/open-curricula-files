---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chile
offline_file: ""
offline_thumbnail: ""
uuid: 5e25e0a7-a304-4fa7-afb7-bfeb62b31a5d
updated: 1484308444
title: Chile
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-The_three_areas_of_Chile.png
tags:
    - Etymology
    - History
    - Early history
    - Spanish colonization
    - Independence and nation building
    - 20th century
    - Pinochet era (1973–1990)
    - 21st century
    - Geography, climate, and environment
    - Climate
    - biodiversity
    - Flora
    - Fauna
    - Topography
    - Hydrography
    - Demographics
    - Ancestry and ethnicity
    - religion
    - Languages
    - Identity and traditions
    - Government and politics
    - Foreign relations
    - Administrative divisions
    - National symbols
    - Military
    - Economy
    - Infrastructure
    - Transport
    - Telecommunications
    - Water supply and sanitation
    - Agriculture
    - Tourism
    - Education
    - Higher education
    - Health
    - culture
    - Music and dance
    - Literature
    - Cuisine
    - Folklore
    - Mythology
    - Cinema
    - Sports
    - Cultural heritage
categories:
    - Uncategorized
---
Chile (/ˈtʃɪli/;[7] Spanish: [ˈtʃile]), officially the Republic of Chile (Spanish:  República de Chile (help·info)), is a South American country occupying a long, narrow strip of land between the Andes to the east and the Pacific Ocean to the west. It borders Peru to the north, Bolivia to the northeast, Argentina to the east, and the Drake Passage in the far south. Chilean territory includes the Pacific islands of Juan Fernández, Salas y Gómez, Desventuradas, and Easter Island in Oceania. Chile also claims about 1,250,000 square kilometres (480,000 sq mi) of Antarctica, although all claims are suspended under the Antarctic Treaty.
