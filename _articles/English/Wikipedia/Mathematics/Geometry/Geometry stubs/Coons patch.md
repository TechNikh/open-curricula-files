---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coons_patch
offline_file: ""
offline_thumbnail: ""
uuid: e081f0e8-a517-40af-9449-a03763f40b69
updated: 1484309360
title: Coons patch
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Example_of_coons_surface.svg.png
tags:
    - Bilinear blending
    - Bicubic blending
categories:
    - Geometry stubs
---
In mathematics, a Coons patch, is a type of manifold parametrization used in computer graphics to smoothly join other surfaces together, and in computational mechanics applications, particularly in finite element method and boundary element method, to mesh problem domains into elements.
