---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/profit
offline_file: ""
offline_thumbnail: ""
uuid: 12e8eea8-3e04-4fdf-811d-c3dcdec8a1c9
updated: 1484310521
title: profit
categories:
    - Dictionary
---
profit
     n 1: the excess of revenues over outlays in a given period of
          time (including depreciation and other non-cash
          expenses) [syn: {net income}, {net}, {net profit}, {lucre},
           {profits}, {earnings}]
     2: the advantageous quality of being beneficial [syn: {gain}]
     v 1: derive a benefit from; "She profited from his vast
          experience" [syn: {gain}, {benefit}]
     2: make a profit; gain money or materially; "The company has
        not profited from the merger" [syn: {turn a profit}] [ant:
         {lose}, {break even}]
