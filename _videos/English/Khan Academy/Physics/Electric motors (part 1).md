---
version: 1
type: video
provider: YouTube
id: pAgPfr7MkkU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Electric%20motors%20%28part%201%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c0e8902d-8704-4c47-8ff5-571a1ff39a83
updated: 1486069595
title: Electric motors (part 1)
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/pAgPfr7MkkU/default.jpg
    - https://i3.ytimg.com/vi/pAgPfr7MkkU/1.jpg
    - https://i3.ytimg.com/vi/pAgPfr7MkkU/2.jpg
    - https://i3.ytimg.com/vi/pAgPfr7MkkU/3.jpg
---
Sal shows that there will be a net torque on a loop of current in a wire. Sal shows that this net torque will cause the loop to rotate. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/magnetic-forces-and-magnetic-fields/electric-motors/v/magnetism-10-electric-motors?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/magnetic-forces-and-magnetic-fields/magnetic-field-current-carrying-wire/v/magnetism-12-induced-current-in-a-wire?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
