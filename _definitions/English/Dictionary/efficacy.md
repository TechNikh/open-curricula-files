---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/efficacy
offline_file: ""
offline_thumbnail: ""
uuid: a4c4de9a-6681-4025-beba-a1034fcadc84
updated: 1484310537
title: efficacy
categories:
    - Dictionary
---
efficacy
     n : capacity or power to produce a desired effect; "concern
         about the safety and efficacy of the vaccine" [syn: {efficaciousness}]
         [ant: {inefficacy}]
