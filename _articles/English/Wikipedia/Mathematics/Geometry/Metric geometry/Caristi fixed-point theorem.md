---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Caristi_fixed-point_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 122de625-9282-43c0-9359-c08a1e021b6c
updated: 1484309262
title: Caristi fixed-point theorem
categories:
    - Metric geometry
---
In mathematics, the Caristi fixed-point theorem (also known as the Caristi–Kirk fixed-point theorem) generalizes the Banach fixed point theorem for maps of a complete metric space into itself. Caristi's fixed-point theorem is a variation of the ε-variational principle of Ekeland (1974, 1979). Moreover, the conclusion of Caristi's theorem is equivalent to metric completeness, as proved by Weston (1977). The original result is due to the mathematicians James Caristi and William Arthur Kirk.
