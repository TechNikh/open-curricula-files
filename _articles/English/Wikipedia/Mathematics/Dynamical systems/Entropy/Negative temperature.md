---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Negative_temperature
offline_file: ""
offline_thumbnail: ""
uuid: 364ed3ac-da27-4a18-b3dd-ac8a321f81a2
updated: 1484308770
title: Negative temperature
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-ColdnessScale.png
tags:
    - >
        Clarification on different definitions of temperature and
        entropy
    - Heat and molecular energy distribution
    - Temperature and disorder
    - Examples
    - Noninteracting two–level particles
    - Nuclear spins
    - Lasers
    - Two-dimensional vortex motion
    - Motional degrees of freedom
categories:
    - Entropy
---
In physics, certain systems can achieve negative temperature; that is, their thermodynamic temperature can be expressed as a negative quantity on the Kelvin or Rankine scales.
