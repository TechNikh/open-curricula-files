---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/restrict
offline_file: ""
offline_thumbnail: ""
uuid: 3dadf5ac-882d-4aa7-b614-17d74d72b642
updated: 1484310555
title: restrict
categories:
    - Dictionary
---
restrict
     v 1: place restrictions on; "curtail drinking in school" [syn: {curtail},
           {curb}, {cut back}]
     2: place under restrictions; limit access to; "This substance
        is controlled" [ant: {derestrict}]
     3: place limits on (extent or access); "restrict the use of
        this parking lot"; "limit the time you can spend with your
        friends" [syn: {restrain}, {trammel}, {limit}, {bound}, {confine},
         {throttle}]
     4: make more specific; "qualify these remarks" [syn: {qualify}]
