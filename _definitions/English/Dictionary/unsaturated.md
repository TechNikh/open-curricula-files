---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unsaturated
offline_file: ""
offline_thumbnail: ""
uuid: f7ff5788-bf22-447a-a178-c57965980f1f
updated: 1484310419
title: unsaturated
categories:
    - Dictionary
---
unsaturated
     adj 1: not saturated; capable of dissolving more of a substance at
            a given temperature; "an unsaturated salt solution"
            [ant: {saturated}]
     2: used of a compound (especially of carbon) containing atoms
        sharing more than one valence bond; "unsaturated fats"
        [ant: {saturated}]
     3: (of color) not chromatically pure; diluted; "an unsaturated
        red" [ant: {saturated}]
