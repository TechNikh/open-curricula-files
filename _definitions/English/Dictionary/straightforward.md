---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/straightforward
offline_file: ""
offline_thumbnail: ""
uuid: 0457e72a-b3a7-441f-902d-0ad742c674d3
updated: 1484310220
title: straightforward
categories:
    - Dictionary
---
straightforward
     adj 1: free from ambiguity; "a straightforward set of instructions"
     2: without evasion or compromise; "a square contradiction"; "he
        is not being as straightforward as it appears" [syn: {square(a)}]
     3: without concealment or deception; honest; "their business
        was open and aboveboard"; "straightforward in all his
        business affairs" [syn: {aboveboard}]
     4: pointed directly ahead; "a straightforward gaze"
