---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adjunction_formula
offline_file: ""
offline_thumbnail: ""
uuid: 614a5fc0-44e7-4669-a96d-61d60b91e84d
updated: 1484309127
title: Adjunction formula
tags:
    - Adjunction for smooth varieties
    - Formula for a smooth subvariety
    - The particular case of a smooth divisor
    - Poincaré residue
    - Inversion of adjunction
    - Applications to curves
categories:
    - Algebraic geometry
---
In mathematics, especially in algebraic geometry and the theory of complex manifolds, the adjunction formula relates the canonical bundle of a variety and a hypersurface inside that variety. It is often used to deduce facts about varieties embedded in well-behaved spaces such as projective space or to prove theorems by induction.
