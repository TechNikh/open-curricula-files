---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_polygon
offline_file: ""
offline_thumbnail: ""
uuid: 21f1f699-538a-4c1a-b8ab-d72a75875209
updated: 1484309192
title: Convex polygon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Pentagon.svg.png
tags:
    - Properties
    - Strict convexity
categories:
    - Convex geometry
---
A convex polygon is a simple polygon (not self-intersecting) in which no line segment between two points on the boundary ever goes outside the polygon. Equivalently, it is a simple polygon whose interior is a convex set.[1] In a convex polygon, all interior angles are less than or equal to 180 degrees, while in a strictly convex polygon all interior angles are strictly less than 180 degrees.
