---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ship
offline_file: ""
offline_thumbnail: ""
uuid: e82c6525-7913-4ede-aa99-3c022ceda3a5
updated: 1484310290
title: ship
categories:
    - Dictionary
---
ship
     n : a vessel that carries passengers or freight
     v 1: transport commercially [syn: {transport}, {send}]
     2: hire for work on a ship
     3: go on board [syn: {embark}] [ant: {disembark}]
     4: travel by ship
     5: place on board a ship; "ship the cargo in the hold of the
        vessel"
     [also: {shipping}, {shipped}]
