---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Organ_donation
offline_file: ""
offline_thumbnail: ""
uuid: 7cebfd86-408d-457e-bef9-dc9b29c2393d
updated: 1484308368
title: Organ donation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-2016-06_Naarden_donor_02.jpg
tags:
    - Donation process
    - History and records
    - Legislation and global perspectives
    - Consent process
    - Opt-in versus opt-out
    - United States
    - Donor Leave Laws
    - Tax Incentives
    - Other Financial Incentives
    - Europe
    - Japan
    - India
    - Sri Lanka
    - Israel
    - Brazil
    - New Zealand
    - Iran
    - Bioethical issues
    - Deontological issues
    - Teleological issues
    - Brain death versus cardiac death
    - Political issues
    - Prison inmates
    - Religious viewpoints
    - Organ shortfall
    - Distribution
    - Suicide
    - Controversies
    - Public service announcements
categories:
    - Uncategorized
---
Organ donation is when a person allows healthy transplantable organs and tissues to be removed, either after death or while the donor is alive, and transplanted into another person.[1][2] Common transplantations include: kidneys, heart, liver, pancreas, intestines, lungs, bones, bone marrow, skin, and corneas.[1] Some organs and tissues can be donated by living donors, such as a kidney or part of the liver,[2] but most donations occur after the donor has died.[1]
