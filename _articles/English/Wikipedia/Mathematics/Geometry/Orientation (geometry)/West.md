---
version: 1
type: article
id: https://en.wikipedia.org/wiki/West
offline_file: ""
offline_thumbnail: ""
uuid: 4d951a97-abc2-49bf-acd6-3943c23ee6ac
updated: 1484309300
title: West
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Compass_Rose_English_West.svg.png
tags:
    - Etymology
    - Navigation
    - Cultural
    - Symbolic meanings
categories:
    - Orientation (geometry)
---
