---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Upper_critical_solution_temperature
offline_file: ""
offline_thumbnail: ""
uuid: 7409660a-c436-4895-a10f-8185052c66f6
updated: 1484308746
title: Upper critical solution temperature
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-LCST-UCST_plot.svg.png
categories:
    - Critical phenomena
---
The upper critical solution temperature (UCST) or upper consolute temperature is the critical temperature above which the components of a mixture are miscible in all proportions.[1] The word upper indicates that the UCST is an upper bound to a temperature range of partial miscibility, or miscibility for certain compositions only. For example, hexane-nitrobenzene mixtures have a UCST of 19 °C, so that these two substances are miscible in all proportions above 19 °C but not at lower temperatures.[2] Examples at higher temperatures are the aniline-water system at 168 °C (at pressures high enough for liquid water to exist at that temperature),[3] and the lead-zinc system at 798 °C (a ...
