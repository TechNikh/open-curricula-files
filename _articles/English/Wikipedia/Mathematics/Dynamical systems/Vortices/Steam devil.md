---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Steam_devil
offline_file: ""
offline_thumbnail: ""
uuid: 171bad9c-9bb2-4800-b309-4f90915c28b4
updated: 1484308879
title: Steam devil
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Lyons_and_Pease_steam_devils_and_steam_fog.jpg
tags:
    - Naming
    - Appearance
    - Formation
    - Occurrences
    - Notes
    - Bibliography
categories:
    - Vortices
---
A steam devil is a small, weak whirlwind over water (or sometimes wet land) that has drawn fog into the vortex, thus rendering it visible. They form over large lakes and oceans during cold air outbreaks while the water is still relatively warm, and can be an important mechanism in vertically transporting moisture.
