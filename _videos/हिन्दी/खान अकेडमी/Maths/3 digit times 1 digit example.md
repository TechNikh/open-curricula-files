---
version: 1
type: video
provider: YouTube
id: KiiURZ-0CtU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3f019af1-05ea-4f8b-a18b-249e892e7e8b
updated: 1486069609
title: 3 digit times 1 digit example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/KiiURZ-0CtU/default.jpg
    - https://i3.ytimg.com/vi/KiiURZ-0CtU/1.jpg
    - https://i3.ytimg.com/vi/KiiURZ-0CtU/2.jpg
    - https://i3.ytimg.com/vi/KiiURZ-0CtU/3.jpg
---
