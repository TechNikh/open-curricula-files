---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vigil
offline_file: ""
offline_thumbnail: ""
uuid: 48428391-eb43-4144-be51-b3203e5f82db
updated: 1484310166
title: vigil
categories:
    - Dictionary
---
vigil
     n 1: a period of sleeplessness
     2: a devotional watch (especially on the eve of a religious
        festival) [syn: {watch}]
     3: a purposeful surveillance to guard or observe [syn: {watch}]
