---
version: 1
type: article
id: https://en.wikipedia.org/wiki/T-square
offline_file: ""
offline_thumbnail: ""
uuid: fa5abc2f-b362-4310-80bb-ec1f1e63724f
updated: 1484309331
title: T-square
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Drafting_board_with_T_Square.jpg
categories:
    - Technical drawing
---
A T-square is a technical drawing instrument used by draftsmen primarily as a guide for drawing horizontal lines on a drafting table. It may also guide a set square to draw vertical or diagonal lines.[1] Its name comes from its resemblance to the letter T. T-squares come in varying sizes, common lengths being 18 inches (460 mm), 24 inches (610 mm), 30 inches (760 mm), 36 inches (910 mm) and 42 inches (1,100 mm).[citation needed]
