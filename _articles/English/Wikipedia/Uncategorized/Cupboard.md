---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cup_board
offline_file: ""
offline_thumbnail: ""
uuid: 0b38cd0e-52f4-434f-8466-29977b5881fa
updated: 1484308365
title: Cupboard
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-LiveryCupboard.JPG
tags:
    - Types of cupboards
    - Airing cupboard
    - Built-in cupboard
    - Linen cupboard
categories:
    - Uncategorized
---
A cupboard is a type of storage cabinet, often made of wood, used indoors to store household objects such as food, tableware, textiles and liquor, and protect them from dust and dirt.[1]
