---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finished
offline_file: ""
offline_thumbnail: ""
uuid: 2ece3a05-f3d3-4752-8d8e-eaff19304f4a
updated: 1484310479
title: finished
categories:
    - Dictionary
---
finished
     adj 1: (of materials or goods) brought to the desired final state;
            "a finished product" [ant: {unfinished}]
     2: ended or brought to an end; "are you finished?"; "gave me
        the finished manuscript"; "the manuscript is finished";
        "almost finished with his studies" [ant: {unfinished}]
     3: (of skills or the products of skills) brought to or having
        the greatest excellence; perfected; "a dazzling and
        finished piece of writing"; "a finished violinist"
     4: having a surface coating or finish applied; "the finished
        bookcase costs much more than the unfinished ones"
     5: brought to ruin; "after the revolution the ...
