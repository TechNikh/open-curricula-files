---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reactivity
offline_file: ""
offline_thumbnail: ""
uuid: d0c8e941-b297-4cf8-b736-7be05da222f1
updated: 1484310391
title: reactivity
categories:
    - Dictionary
---
reactivity
     n 1: responsive to stimulation [syn: {responsiveness}]
     2: ready susceptibility to chemical change
