---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chloro
offline_file: ""
offline_thumbnail: ""
uuid: 0ea88628-383b-45ba-bd10-cefb69f6eb4b
updated: 1484308401
title: Chlorine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Chlorine_ampoule_1.jpg
tags:
    - History
    - Properties
    - Isotopes
    - Chemistry and compounds
    - Hydrogen chloride
    - Other binary chlorides
    - Polychlorine compounds
    - Chlorine fluorides
    - Chlorine oxides
    - Chlorine oxoacids and oxyanions
    - Organochlorine compounds
    - Occurrence and production
    - Applications
    - Sanitation, disinfection, and antisepsis
    - Combating putrefaction
    - Against infection and contagion
    - Semmelweis and experiments with antisepsis
    - Public sanitation
    - Use as a weapon
    - World War I
    - Iraq
    - Biological role
    - Hazards
    - Chlorine-induced cracking in structural materials
    - Chlorine-iron fire
    - Bibliography
categories:
    - Uncategorized
---
Chlorine is a chemical element with symbol Cl and atomic number 17. The second-lightest of the halogens, it appears between fluorine and bromine in the periodic table and its properties are mostly intermediate between them. Chlorine is a yellow-green gas at room temperature. It is an extremely reactive element and a strong oxidising agent: among the elements, it has the highest electron affinity and the third-highest electronegativity, behind only oxygen and fluorine.
