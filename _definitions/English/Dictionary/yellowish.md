---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yellowish
offline_file: ""
offline_thumbnail: ""
uuid: 7f21eb9d-993a-489d-81ed-927dbfb4d84e
updated: 1484310323
title: yellowish
categories:
    - Dictionary
---
yellowish
     adj : similar to the color of an egg yolk [syn: {yellow}, {xanthous}]
