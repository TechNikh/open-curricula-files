---
version: 1
type: video
provider: YouTube
id: X7j_6gJQPzM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 51a71d3d-0bbe-4eac-b29f-ef1344e25dd5
updated: 1486069626
title: Itva Sandhi
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/X7j_6gJQPzM/default.jpg
    - https://i3.ytimg.com/vi/X7j_6gJQPzM/1.jpg
    - https://i3.ytimg.com/vi/X7j_6gJQPzM/2.jpg
    - https://i3.ytimg.com/vi/X7j_6gJQPzM/3.jpg
---
