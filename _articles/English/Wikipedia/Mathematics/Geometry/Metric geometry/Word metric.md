---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Word_metric
offline_file: ""
offline_thumbnail: ""
uuid: c559240e-c1e8-4f27-bb52-3668bfe8cc36
updated: 1484309272
title: Word metric
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cayley_graph_of_F2.svg.png
tags:
    - Examples
    - The group of integers Z
    - |
        The group
        
        
        
        
        Z
        
        ⊕
        
        Z
        
        
        
        {\displaystyle \mathbb {Z} \oplus \mathbb {Z} }
    - Definition
    - Variations
    - Example in a free group
    - Theorems
    - Isometry of the left action
    - Bilipschitz invariants of a group
    - Quasi-isometry invariants of a group
categories:
    - Metric geometry
---
In group theory, a branch of mathematics, a word metric on a group 
  
    
      
        G
      
    
    {\displaystyle G}
  
 is a way to measure distance between any two elements of 
  
    
      
        G
      
    
    {\displaystyle G}
  
. As the name suggests, the word metric is a metric on 
  
    
      
        G
      
    
    {\displaystyle G}
  
, assigning to any two elements 
  
    
      
        g
      
    
    {\displaystyle g}
  
, 
  
    
      
        h
      
    
    {\displaystyle h}
  
 of 
  
    
      
        G
      
    
    {\displaystyle G}
  
 a distance 
  
    
      
        d
        (
        g
        ,
        h
        )
      
    
    ...
