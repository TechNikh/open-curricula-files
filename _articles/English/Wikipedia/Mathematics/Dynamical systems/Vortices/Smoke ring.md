---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Smoke_ring
offline_file: ""
offline_thumbnail: ""
uuid: 81a8845c-4b13-475d-8561-329a73daa06a
updated: 1484308878
title: Smoke ring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Smoke_ring.jpg
tags:
    - Smoking and breathing
    - Volcanoes
categories:
    - Vortices
---
Smokers may blow smoke rings from the mouth, intentionally or accidentally. Smoke rings may also be formed by sudden bursts of fire (such as lighting and immediately putting out a cigarette lighter), by shaking a smoke source (such as an incense stick) up and down, by firing certain types of artillery, or by the use of special devices, such as vortex ring toys. The head of a mushroom cloud is a large smoke ring.
