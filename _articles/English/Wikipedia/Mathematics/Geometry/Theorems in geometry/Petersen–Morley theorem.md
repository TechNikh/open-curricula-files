---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Petersen%E2%80%93Morley_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 1d29de44-0083-419f-aba8-e7635e0ea340
updated: 1484309117
title: Petersen–Morley theorem
categories:
    - Theorems in geometry
---
In geometry, the Petersen–Morley theorem states that, if a, b, c are three general skew lines in space, if a′, b′, c′ are the lines of shortest distance respectively for the pairs (b,c), (c,a) and (a,b), and if p, q and r are the lines of shortest distance respectively for the pairs (a,a′), (b,b′) and (c,c′), then there is a single line meeting at right angles all of p, q, and r.
