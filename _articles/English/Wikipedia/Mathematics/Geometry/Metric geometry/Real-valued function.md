---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Real-valued_function
offline_file: ""
offline_thumbnail: ""
uuid: c7294477-6afe-4a0b-8157-0a3ea6369cbc
updated: 1484309272
title: Real-valued function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Weights_20mg~500g.jpg
tags:
    - In general
    - Measurable
    - Continuous
    - Smooth
    - Appearances in measure theory
    - Other appearances
    - Footnotes
categories:
    - Metric geometry
---
In mathematics, a real-valued function or real function is a function whose values are real numbers. In other words, it is a function that assigns a real number to each member of its domain.
