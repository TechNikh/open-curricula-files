---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vinyl
offline_file: ""
offline_thumbnail: ""
uuid: 8aca0c9f-ddf2-47c2-bff4-400587ca13d4
updated: 1484310419
title: vinyl
categories:
    - Dictionary
---
vinyl
     n 1: a univalent chemical radical derived from ethylene [syn: {vinyl
          group}, {vinyl radical}]
     2: shiny and tough and flexible plastic; used especially for
        floor coverings
