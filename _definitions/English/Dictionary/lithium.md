---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lithium
offline_file: ""
offline_thumbnail: ""
uuid: 9c563d3e-b81f-43b0-b405-38cdc04c6a69
updated: 1484310397
title: lithium
categories:
    - Dictionary
---
lithium
     n : a soft silver-white univalent element of the alkali metal
         group; the lightest metal known; occurs in several
         minerals [syn: {Li}, {atomic number 3}]
