---
version: 1
type: video
provider: YouTube
id: CLrImGKeuEI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Quadratic%20equations/Example%202-%20Using%20the%20quadratic%20formula.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ededdcc5-9e89-45dc-a5ca-57a275f2003e
updated: 1486069563
title: 'Example 2- Using the quadratic formula'
categories:
    - Quadratic equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/CLrImGKeuEI/default.jpg
    - https://i3.ytimg.com/vi/CLrImGKeuEI/1.jpg
    - https://i3.ytimg.com/vi/CLrImGKeuEI/2.jpg
    - https://i3.ytimg.com/vi/CLrImGKeuEI/3.jpg
---
