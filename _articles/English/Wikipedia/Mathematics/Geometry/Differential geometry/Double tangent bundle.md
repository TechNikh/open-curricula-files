---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Double_tangent_bundle
offline_file: ""
offline_thumbnail: ""
uuid: ed8f819f-2d69-4859-94b0-dcb82b2c567d
updated: 1484309218
title: Double tangent bundle
tags:
    - Secondary vector bundle structure and canonical flip
    - Canonical tensor fields on the tangent bundle
    - (Semi)spray structures
    - Nonlinear covariant derivatives on smooth manifolds
categories:
    - Differential geometry
---
In mathematics, particularly differential topology, the double tangent bundle or the second tangent bundle refers to the tangent bundle (TTM,πTTM,TM) of the total space TM of the tangent bundle (TM,πTM,M) of a smooth manifold M .[1] A note on notation: in this article, we denote projection maps by their domains, e.g., πTTM : TTM → TM. Some authors index these maps by their ranges instead, so for them, that map would be written πTM.
