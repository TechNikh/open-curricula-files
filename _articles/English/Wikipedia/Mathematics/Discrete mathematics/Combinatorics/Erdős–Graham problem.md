---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Graham_problem'
offline_file: ""
offline_thumbnail: ""
uuid: f8f3699c-b33e-4446-a724-cd3e9d757953
updated: 1484308590
title: Erdős–Graham problem
categories:
    - Combinatorics
---
In combinatorial number theory, the Erdős–Graham problem is the problem of proving that, if the set {2, 3, 4, ...} of integers greater than one is partitioned into finitely many subsets, then one of the subsets can be used to form an Egyptian fraction representation of unity. That is, for every r > 0, and every r-coloring of the integers greater than one, there is a finite monochromatic subset S of these integers such that
