---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pump
offline_file: ""
offline_thumbnail: ""
uuid: 5295ff76-3108-498e-9e00-191544710bd6
updated: 1484310258
title: pump
categories:
    - Dictionary
---
pump
     n 1: a mechanical device that moves fluid or gas by pressure or
          suction
     2: the hollow muscular organ located behind the sternum and
        between the lungs; its rhythmic contractions pump blood
        through the body; "he stood still, his heart thumping
        wildly" [syn: {heart}, {ticker}]
     3: a low-cut shoe without fastenings [syn: {pumps}]
     v 1: operate like a pump; move up and down, like a handle or a
          pedal
     2: deliver forth; "pump bullets into the dummy"
     3: draw or pour with a pump
     4: supply in great quantities; "Pump money into a project"
     5: flow intermittently
     6: move up and down; "The athlete pumps weights in ...
