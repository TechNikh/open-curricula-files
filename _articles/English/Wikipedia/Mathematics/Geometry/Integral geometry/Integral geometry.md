---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Integral_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 8af4e72b-8a7b-4cff-a47b-7a5284e7ddd2
updated: 1484309257
title: Integral geometry
categories:
    - Integral geometry
---
In mathematics, integral geometry is the theory of measures on a geometrical space invariant under the symmetry group of that space. In more recent times, the meaning has been broadened to include a view of invariant (or equivariant) transformations from the space of functions on one geometrical space to the space of functions on another geometrical space. Such transformations often take the form of integral transforms such as the Radon transform and its generalizations.
