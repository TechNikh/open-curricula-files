---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bost%E2%80%93Connes_system'
offline_file: ""
offline_thumbnail: ""
uuid: c644e0b9-ba69-473d-9f63-1bf82174017a
updated: 1484308632
title: Bost–Connes system
categories:
    - Number theory
---
In mathematics, a Bost–Connes system is a quantum statistical dynamical system related to an algebraic number field, whose partition function is related to the Dedekind zeta function of the number field. Bost & Connes (1995) introduced Bost–Connes systems by constructing one for the rational numbers. Connes, Marcolli & Ramachandran (2005) extended the construction to imaginary quadratic fields.
