---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grind
offline_file: ""
offline_thumbnail: ""
uuid: 451d19a5-00c4-4878-9f31-7d5ac8caefc6
updated: 1484310337
title: grind
categories:
    - Dictionary
---
grind
     n 1: an insignificant student who is ridiculed as being affected
          or studying excessively [syn: {swot}, {nerd}, {wonk}, {dweeb}]
     2: hard monotonous routine work [syn: {drudgery}, {plodding}, {donkeywork}]
     3: the act of grinding to a powder or dust [syn: {mill}, {pulverization},
         {pulverisation}]
     v 1: press or grind with a crunching noise [syn: {crunch}, {cranch},
           {craunch}]
     2: make a grating or grinding sound by rubbing together; "grate
        one's teeth in anger" [syn: {grate}]
     3: reduce to small pieces or particles by pounding or abrading;
        "grind the spices in a mortar"; "mash the garlic" [syn: {mash},
         ...
