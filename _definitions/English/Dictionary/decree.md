---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decree
offline_file: ""
offline_thumbnail: ""
uuid: 0e356f0b-d6d0-46aa-8561-887079b0d2d8
updated: 1484310172
title: decree
categories:
    - Dictionary
---
decree
     n : a legally binding command or decision entered on the court
         record (as if issued by a court or judge); "a friend in
         New Mexico said that the order caused no trouble out
         there" [syn: {edict}, {fiat}, {order}, {rescript}]
     v 1: issue a decree; "The King only can decree"
     2: decide with authority; "The King decreed that all first-born
        males should be killed" [syn: {rule}]
