---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_trigonometric_identities
offline_file: ""
offline_thumbnail: ""
uuid: 7c2a5436-6f82-47a5-98a7-2d0b29c5223e
updated: 1484309347
title: List of trigonometric identities
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Unit_circle_angles_color.svg.png
tags:
    - Notation
    - Angles
    - Trigonometric functions
    - Inverse functions
    - Pythagorean identity
    - Related identities
    - Historical shorthands
    - Symmetry, shifts, and periodicity
    - Symmetry
    - Shifts and periodicity
    - Angle sum and difference identities
    - Matrix form
    - Sines and cosines of sums of infinitely many terms
    - Tangents of sums
    - Secants and cosecants of sums
    - Multiple-angle formulae
    - Double-angle, triple-angle, and half-angle formulae
    - Double-angle formulae
    - Triple-angle formulae
    - Half-angle formulae
    - Table
    - Sine, cosine, and tangent of multiple angles
    - Chebyshev method
    - Tangent of an average
    - "Viète's infinite product"
    - Power-reduction formula
    - Product-to-sum and sum-to-product identities
    - Other related identities
    - "Hermite's cotangent identity"
    - "Ptolemy's theorem"
    - Linear combinations
    - Sine and cosine
    - Arbitrary phase shift
    - More than two sinusoids
    - "Lagrange's trigonometric identities"
    - Other sums of trigonometric functions
    - Certain linear fractional transformations
    - Inverse trigonometric functions
    - Compositions of trig and inverse trig functions
    - Relation to the complex exponential function
    - Infinite product formulae
    - Identities without variables
    - Computing π
    - A useful mnemonic for certain values of sines and cosines
    - Miscellany
    - An identity of Euclid
    - Composition of trigonometric functions
    - calculus
    - Implications
    - Some differential equations satisfied by the sine function
    - Exponential definitions
    - Miscellaneous
    - Dirichlet kernel
    - Tangent half-angle substitution
    - Notes
categories:
    - Trigonometry
---
In mathematics, trigonometric identities are equalities that involve trigonometric functions and are true for every single value of the occurring variables where both sides of the equality are defined. Geometrically, these are identities involving certain functions of one or more angles. They are distinct from triangle identities, which are identities potentially involving angles but also involving side lengths or other lengths of a triangle.
