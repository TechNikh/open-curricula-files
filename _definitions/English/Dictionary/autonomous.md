---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/autonomous
offline_file: ""
offline_thumbnail: ""
uuid: 19a8f669-9616-4a92-ae36-ff94e2949fd5
updated: 1484310337
title: autonomous
categories:
    - Dictionary
---
autonomous
     adj 1: of political bodies; "an autonomous judiciary"; "a sovereign
            state" [syn: {independent}, {self-governing}, {sovereign}]
     2: existing as an independent entity; "the partitioning of
        India created two separate and autonomous jute economies"
     3: of persons; free from external control and constraint in
        e.g. action and judgment [syn: {self-directed}, {self-reliant}]
