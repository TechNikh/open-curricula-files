---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_connectivity
offline_file: ""
offline_thumbnail: ""
uuid: 15458be6-1d9c-4865-bec8-d70e2b44dc68
updated: 1484309371
title: Algebraic connectivity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_13.png
tags:
    - Properties
    - Fiedler vector
categories:
    - Algebraic graph theory
---
The algebraic connectivity of a graph G is the second-smallest eigenvalue of the Laplacian matrix of G.[1] This eigenvalue is greater than 0 if and only if G is a connected graph. This is a corollary to the fact that the number of times 0 appears as an eigenvalue in the Laplacian is the number of connected components in the graph. The magnitude of this value reflects how well connected the overall graph is, and has been used in analysing the robustness and synchronizability of networks.
