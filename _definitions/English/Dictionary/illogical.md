---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illogical
offline_file: ""
offline_thumbnail: ""
uuid: da4e4bee-f288-4ee4-a9ba-40fb810a2e3e
updated: 1484310146
title: illogical
categories:
    - Dictionary
---
illogical
     adj 1: lacking in correct logical relation [syn: {unlogical}] [ant:
             {logical}]
     2: lacking orderly continuity; "a confused set of
        instructions"; "a confused dream about the end of the
        world"; "disconnected fragments of a story"; "scattered
        thoughts" [syn: {confused}, {disconnected}, {disjointed},
        {disordered}, {garbled}, {scattered}, {unconnected}]
