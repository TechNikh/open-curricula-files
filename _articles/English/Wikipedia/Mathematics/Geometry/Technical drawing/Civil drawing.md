---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Civil_drawing
offline_file: ""
offline_thumbnail: ""
uuid: ca7d2cea-4e7c-48a8-909a-be461bd6c73c
updated: 1484309325
title: Civil drawing
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Site_Drawing_for_Gov._Inst._for_Research_in_Physical_Education%252C_Japan.jpg'
categories:
    - Technical drawing
---
A civil drawing, or site drawing, is a type of technical drawing that shows information about grading, landscaping, or other site details. These drawings are intended to give a clear picture of all things in a construction site to a civil engineer.
