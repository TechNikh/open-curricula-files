---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/urinary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484475481
title: urinary
categories:
    - Dictionary
---
urinary
     adj 1: of or relating to the function or production or secretion of
            urine
     2: of or relating to the urinary system of the body
