---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/politeness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484549161
title: politeness
categories:
    - Dictionary
---
politeness
     n 1: a courteous manner that respects accepted social usage [syn:
           {niceness}] [ant: {impoliteness}]
     2: the act of showing regard for others [syn: {civility}]
