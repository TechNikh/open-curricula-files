---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/civilise
offline_file: ""
offline_thumbnail: ""
uuid: abd3eaed-08d5-4888-a5c3-a4ce18e0127c
updated: 1484310573
title: civilise
categories:
    - Dictionary
---
civilise
     v 1: train to be discriminative in taste or judgment; "Cultivate
          your musical taste"; "Train your tastebuds"; "She is
          well schooled in poetry" [syn: {educate}, {school}, {train},
           {cultivate}, {civilize}]
     2: raise from a barbaric to a civilized state; "The wild child
        found wandering in the forest was gradually civilized"
        [syn: {civilize}]
