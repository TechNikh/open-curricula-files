---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inlet
offline_file: ""
offline_thumbnail: ""
uuid: 3c062c5e-5c39-47be-92dd-f96b7621e637
updated: 1484310462
title: inlet
categories:
    - Dictionary
---
inlet
     n : an arm off of a larger body of water (often between rocky
         headlands) [syn: {recess}]
