---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yves_Benoist
offline_file: ""
offline_thumbnail: ""
uuid: a3442fd9-54bf-4026-aeff-7cbf548d898d
updated: 1484308681
title: Yves Benoist
categories:
    - Dynamical systems theorists
---
Yves Benoist is a French mathematician, known for his work on group dynamics on homogeneous spaces. He is currently a Director of Research (Directeur de Recherche) of CNRS at the University of Paris-Sud.
