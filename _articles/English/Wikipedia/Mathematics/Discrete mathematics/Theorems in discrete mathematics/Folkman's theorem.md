---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Folkman%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: b0080c71-9d5a-40c6-b385-a54ba91e724a
updated: 1484308662
title: "Folkman's theorem"
tags:
    - Statement of the theorem
    - "Relation to Rado's theorem and Schur's theorem"
    - Multiplication versus addition
    - Canonical Folkman Theorem
    - Previous results
categories:
    - Theorems in discrete mathematics
---
Folkman's theorem is a theorem in mathematics, and more particularly in arithmetic combinatorics and Ramsey theory. According to this theorem, whenever the natural numbers are partitioned into finitely many subsets, there exist arbitrarily large sets of numbers all of whose sums belong to the same subset of the partition.[1] The theorem had been discovered and proved independently by several mathematicians,[2][3] before it was named "Folkman's theorem", as a memorial to Jon Folkman, by Graham, Rothschild, and Spencer.[1]
