---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Friendship_paradox
offline_file: ""
offline_thumbnail: ""
uuid: b13ff6a9-a92b-4ebd-b3f0-bd8ac3d8d32f
updated: 1484308629
title: Friendship paradox
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/799px-Friends_logo.svg.png
tags:
    - Mathematical explanation
    - Applications
categories:
    - Graph theory
---
The friendship paradox is the phenomenon first observed by the sociologist Scott L. Feld in 1991 that most people have fewer friends than their friends have, on average.[1] It can be explained as a form of sampling bias in which people with greater numbers of friends have an increased likelihood of being observed among one's own friends. In contradiction to this, most people believe that they have more friends than their friends have.[2]
