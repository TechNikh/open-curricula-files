---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reasoning
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484313121
title: reasoning
categories:
    - Dictionary
---
reasoning
     adj : endowed with the capacity to reason [syn: {intelligent}, {reasoning(a)},
            {thinking(a)}]
     n : thinking that is coherent and logical [syn: {logical
         thinking}, {abstract thought}]
