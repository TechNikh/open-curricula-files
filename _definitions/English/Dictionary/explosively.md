---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/explosively
offline_file: ""
offline_thumbnail: ""
uuid: 823998ff-0d75-4adb-8e4b-e6e802c5b6c2
updated: 1484310411
title: explosively
categories:
    - Dictionary
---
explosively
     adv 1: suddenly and rapidly; "the population in Central America is
            growing explosively"
     2: in an explosive manner; "the political situation in Kashmir
        and Jammu is explosively unstable"
