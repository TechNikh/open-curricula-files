---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exhaled
offline_file: ""
offline_thumbnail: ""
uuid: fb1a9174-5b06-48e9-8167-421aaef1368a
updated: 1484310158
title: exhaled
categories:
    - Dictionary
---
exhaled
     adj : let or forced out of the lungs; breathed out; "an exhaled
           sigh" [ant: {inhaled}]
