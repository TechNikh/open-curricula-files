---
version: 1
type: video
provider: YouTube
id: gm2LCsM_S5o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7cfd8e60-c2ab-4f93-b55c-220a516d8cec
updated: 1486069636
title: Classic three polarizers experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/gm2LCsM_S5o/default.jpg
    - https://i3.ytimg.com/vi/gm2LCsM_S5o/1.jpg
    - https://i3.ytimg.com/vi/gm2LCsM_S5o/2.jpg
    - https://i3.ytimg.com/vi/gm2LCsM_S5o/3.jpg
---
