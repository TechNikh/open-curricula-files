---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chess_middlegame
offline_file: ""
offline_thumbnail: ""
uuid: 96b640a5-3fc7-4a8d-9558-32daf317ccdf
updated: 1484309020
title: Chess middlegame
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Nuvola_apps_package_games_strategy_3.png
tags:
    - Aims of the middlegame
    - Transition to the endgame
    - Notes
    - Reference works
    - Classical middlegame textbooks
    - Modern texts
categories:
    - Chess theory
---
The middlegame in chess refers to the portion of the game in between the opening and the endgame. There is no clear line between the opening and middlegame, and between the middlegame and endgame. In modern chess, the moves that make up an opening blend into the middlegame, so there is no sharp divide. At elementary level, both players will usually have completed the development of all or most pieces and the king will usually have been brought to relative safety. However, at master level, the opening analysis may go well into the middlegame.
