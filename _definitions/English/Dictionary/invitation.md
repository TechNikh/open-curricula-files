---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invitation
offline_file: ""
offline_thumbnail: ""
uuid: e5539c78-6cb2-4229-a9a4-19b00219db5e
updated: 1484310146
title: invitation
categories:
    - Dictionary
---
invitation
     n 1: a request (spoken or written) to participate or be present
          or take part in something; "an invitation to lunch";
          "she threw the invitation away"
     2: a tempting allurement; "she was an invitation to trouble"
