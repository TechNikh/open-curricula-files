---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/creative
offline_file: ""
offline_thumbnail: ""
uuid: d80c75a7-3ac7-4de3-9d00-6f215d180fd2
updated: 1484310469
title: creative
categories:
    - Dictionary
---
creative
     adj 1: having the ability or power to create; "a creative
            imagination" [syn: {originative}] [ant: {uncreative}]
     2: promoting construction or creation; "creative work"
     3: having the power to bring into being [syn: {originative}]
