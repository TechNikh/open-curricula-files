---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flux
offline_file: ""
offline_thumbnail: ""
uuid: ae9bab42-ef93-42a9-a3cb-556c366b5e3d
updated: 1484310196
title: flux
categories:
    - Dictionary
---
flux
     n 1: the rate of flow of energy or particles across a given
          surface
     2: a flow or discharge [syn: {fluxion}]
     3: a substance added to molten metals to bond with impurities
        that can then be readily removed
     4: excessive discharge of liquid from a cavity or organ (as in
        watery diarrhea)
     5: a state of uncertainty about what should be done (usually
        following some important event) preceding the
        establishment of a new direction of action; "the flux
        following the death of the emperor" [syn: {state of flux}]
     6: the lines of force surrounding a permanent magnet or a
        moving charged particle [syn: {magnetic ...
