---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prepositional
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484471281
title: prepositional
categories:
    - Dictionary
---
prepositional
     adj : of or relating to or formed with a preposition;
           "prepositional phrase"
