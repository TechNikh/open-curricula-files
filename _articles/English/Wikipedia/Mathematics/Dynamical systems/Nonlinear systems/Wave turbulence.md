---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wave_turbulence
offline_file: ""
offline_thumbnail: ""
uuid: 596518e2-901f-4dff-ad6a-49846d8b9083
updated: 1484308814
title: Wave turbulence
tags:
    - Appearance
    - Statistical wave turbulence and discrete wave turbulence
    - Notes
categories:
    - Nonlinear systems
---
In continuum mechanics, wave turbulence is a set of nonlinear waves deviated far from thermal equilibrium. Such a state is usually accompanied by dissipation. It is either decaying turbulence or requires an external source of energy to sustain it. Examples are waves on a fluid surface excited by winds or ships, and waves in plasma excited by electromagnetic waves etc.
