---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/absorb
offline_file: ""
offline_thumbnail: ""
uuid: 7e4f0cc6-b035-419a-8140-17eea4a6b94b
updated: 1484310264
title: absorb
categories:
    - Dictionary
---
absorb
     v 1: become imbued; "The liquids, light, and gases absorb"
     2: take up mentally; "he absorbed the knowledge or beliefs of
        his tribe" [syn: {assimilate}, {ingest}, {take in}]
     3: take up, as of debts or payments; "absorb the costs for
        something" [syn: {take over}]
     4: take in, also metaphorically; "The sponge absorbs water
        well"; "She drew strength from the minister's words" [syn:
         {suck}, {imbibe}, {soak up}, {sop up}, {suck up}, {draw},
         {take in}, {take up}]
     5: cause to become one with; "The sales tax is absorbed into
        the state income tax"
     6: suck or take up or in; "A black star absorbs all matter"
        ...
