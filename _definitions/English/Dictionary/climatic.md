---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/climatic
offline_file: ""
offline_thumbnail: ""
uuid: 13390c74-7e39-4d1d-af82-343504f3b363
updated: 1484310273
title: climatic
categories:
    - Dictionary
---
climatic
     adj : of or relating to a climate; "climatic changes" [syn: {climatical}]
