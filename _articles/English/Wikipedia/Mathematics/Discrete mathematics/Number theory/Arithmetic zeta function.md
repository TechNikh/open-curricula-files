---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arithmetic_zeta_function
offline_file: ""
offline_thumbnail: ""
uuid: fa65e65a-57b5-467f-aa99-c34834583464
updated: 1484308547
title: Arithmetic zeta function
tags:
    - Definition
    - Examples and properties
    - Varieties over a finite field
    - Ring of integers
    - Zeta functions of disjoint unions
    - Main conjectures
    - Meromorphic continuation and functional equation
    - The generalized Riemann hypothesis
    - Pole orders
    - Methods and theories
categories:
    - Number theory
---
In mathematics, the arithmetic zeta function is a zeta function associated with a scheme of finite type over integers. The arithmetic zeta function generalizes the Riemann zeta function and Dedekind zeta function to higher dimensions. The arithmetic zeta function is one of the most-fundamental objects of number theory.
