---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roof
offline_file: ""
offline_thumbnail: ""
uuid: b6d19bbd-560e-4c6a-9865-3b0c6e3c965a
updated: 1484310154
title: roof
categories:
    - Dictionary
---
roof
     n 1: a protective covering that covers or forms the top of a
          building
     2: protective covering on top of a motor vehicle
     v : provide a building with a roof; cover a building with a roof
