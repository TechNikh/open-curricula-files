---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Judgment_(mathematical_logic)
offline_file: ""
offline_thumbnail: ""
uuid: cc1d85a1-f805-464b-938e-86a52fa266b0
updated: 1484309495
title: Judgment (mathematical logic)
categories:
    - Proof theory
---
In mathematical logic, a judgment or assertion may be thought of as a statement or enunciation in the meta-language. For example, typical judgments in first-order logic would be that a string is a well-formed formula, or that a proposition is true. Similarly, a judgment may assert the occurrence of a free variable in an expression of the object language, or the provability of a proposition. In general, a judgment may be any inductively definable assertion in the metatheory.
