---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cope
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484514541
title: cope
categories:
    - Dictionary
---
cope
     n : brick that is laid sideways at the top of a wall [syn: {header},
          {coping}]
     v : come to terms or deal successfully with; "We got by on just
         a gallon of gas"; "They made do on half a loaf of bread
         every day" [syn: {get by}, {make out}, {make do}, {contend},
          {grapple}, {deal}, {manage}]
