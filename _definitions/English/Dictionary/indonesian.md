---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indonesian
offline_file: ""
offline_thumbnail: ""
uuid: 793f6f91-def8-4041-989a-e1e935644b40
updated: 1484310289
title: indonesian
categories:
    - Dictionary
---
Indonesian
     adj : of or relating to or characteristic of Indonesia or its
           people or languages
     n 1: a native or inhabitant of Indonesia
     2: the dialect of Malay used as the national language of the
        Republic of Indonesia or of Malaysia [syn: {Bahasa
        Indonesia}, {Bahasa}]
