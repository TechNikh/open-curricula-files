---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Slope_number
offline_file: ""
offline_thumbnail: ""
uuid: f33735ba-463b-45a0-a7e8-c57b1d5aeb68
updated: 1484309242
title: Slope number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Petersen_graph_with_slope_number_3.svg.png
tags:
    - Complete graphs
    - Relation to degree
    - Planar graphs
    - Complexity
    - Notes
categories:
    - Geometric graph theory
---
In graph drawing and geometric graph theory, the slope number of a graph is the minimum possible number of distinct slopes of edges in a drawing of the graph in which vertices are represented as points in the Euclidean plane and edges are represented as line segments that do not pass through any non-incident vertex.
