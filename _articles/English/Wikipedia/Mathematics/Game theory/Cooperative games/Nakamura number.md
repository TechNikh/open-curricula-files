---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nakamura_number
offline_file: ""
offline_thumbnail: ""
uuid: 209cb1a2-f2a0-4190-a9c6-e85a4fad56db
updated: 1484308528
title: Nakamura number
tags:
    - Overview
    - Framework
    - 'The Nakamura number: the definition and examples'
    - "Nakamura's theorem for acyclic preferences"
    - "A variant of Nakamura's theorem for preferences that may contain cycles"
    - Notes
categories:
    - Cooperative games
---
In cooperative game theory and social choice theory, the Nakamura number measures the degree of rationality of preference aggregation rules (collective decision rules), such as voting rules. It is an indicator of the extent to which an aggregation rule can yield well-defined choices.
