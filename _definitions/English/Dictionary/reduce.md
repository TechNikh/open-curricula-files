---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reduce
offline_file: ""
offline_thumbnail: ""
uuid: 30f93edd-bc2b-4630-bfef-1d5073161b35
updated: 1484310271
title: reduce
categories:
    - Dictionary
---
reduce
     v 1: cut down on; make a reduction in; "reduce your daily fat
          intake"; "The employer wants to cut back health
          benefits" [syn: {cut down}, {cut back}, {trim}, {trim
          down}, {trim back}, {cut}, {bring down}]
     2: make less complex; "reduce a problem to a single question"
     3: bring to humbler or weaker state or condition; "He reduced
        the population to slavery"
     4: simplify the form of a mathematical equation of expression
        by substituting one term for another
     5: lower in grade or rank or force somebody into an undignified
        situation; "She reduced her niece to a servant"
     6: be the essential element; "The ...
