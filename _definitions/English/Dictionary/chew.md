---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chew
offline_file: ""
offline_thumbnail: ""
uuid: 75fa6891-e7fa-49a5-9d65-6d40457e452e
updated: 1484310346
title: chew
categories:
    - Dictionary
---
chew
     n 1: a wad of something chewable as tobacco [syn: {chaw}, {cud},
          {quid}, {plug}, {wad}]
     2: biting and grinding food in your mouth so it becomes soft
        enough to swallow [syn: {chewing}, {mastication}, {manduction}]
     v : chew (food); "He jawed his bubble gum"; "Chew your food and
         don't swallow it!"; "The cows were masticating the grass"
         [syn: {masticate}, {manducate}, {jaw}]
