---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equiareal_map
offline_file: ""
offline_thumbnail: ""
uuid: 9112e8f2-1889-450a-9673-5cf1c5b219de
updated: 1484309216
title: Equiareal map
categories:
    - Differential geometry
---
In differential geometry, an equiareal map is a smooth map from one surface to another that preserves the area of figures. If M and N are two surfaces in the Euclidean space R3, then an equi-areal map ƒ can be characterized by any of the following equivalent conditions:
