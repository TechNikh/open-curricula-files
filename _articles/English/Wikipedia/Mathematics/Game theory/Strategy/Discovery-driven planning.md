---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Discovery-driven_planning
offline_file: ""
offline_thumbnail: ""
uuid: d971a46b-4e90-490c-bb0f-38274a1ab330
updated: 1484309062
title: Discovery-driven planning
tags:
    - Five disciplines
categories:
    - Strategy
---
Discovery-driven planning is a planning technique first introduced in a Harvard Business Review article by Rita Gunther McGrath and Ian C. MacMillan in 1995[1] and subsequently referenced in a number of books and articles.[2][3] Its main thesis is that when one is operating in arenas with significant amounts of uncertainty, that a different approach applies than is normally used in conventional planning. In conventional planning, the correctness of a plan is generally judged by how close outcomes come to projections. In discovery-driven planning, it is assumed that plan parameters may change as new information is revealed. With conventional planning, it is considered appropriate to fund the ...
