---
version: 1
type: video
provider: YouTube
id: EKvHQc3QEow
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Newton%20Leibniz%20and%20Usain%20Bolt.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3e4e167d-9b94-4457-bd87-10e1f06b759c
updated: 1486069689
title: Newton Leibniz and Usain Bolt
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/EKvHQc3QEow/default.jpg
    - https://i3.ytimg.com/vi/EKvHQc3QEow/1.jpg
    - https://i3.ytimg.com/vi/EKvHQc3QEow/2.jpg
    - https://i3.ytimg.com/vi/EKvHQc3QEow/3.jpg
---
