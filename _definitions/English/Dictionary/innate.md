---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/innate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484617321
title: innate
categories:
    - Dictionary
---
innate
     adj 1: not established by conditioning or learning; "an
            unconditioned reflex" [syn: {unconditioned}, {unlearned}]
            [ant: {conditioned}]
     2: being talented through inherited qualities; "a natural
        leader"; "a born musician"; "an innate talent" [syn: {natural},
         {born(p)}, {innate(p)}]
     3: present at birth but not necessarily hereditary; acquired
        during fetal development [syn: {congenital}, {inborn}, {inherent}]
