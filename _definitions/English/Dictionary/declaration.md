---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/declaration
offline_file: ""
offline_thumbnail: ""
uuid: 10286e5b-2269-4f3b-b1a1-90df1a0f045c
updated: 1484310484
title: declaration
categories:
    - Dictionary
---
declaration
     n 1: a statement that is emphatic and explicit (spoken or
          written)
     2: (law) unsworn statement that can be admitted in evidence in
        a legal transaction; "his declaration of innocence"
     3: a statement of taxable goods or of dutiable properties
     4: (contract bridge) the highest bid becomes the contract
        setting the number of tricks that the bidder must make
        [syn: {contract}]
     5: a formal public statement; "the government made an
        announcement about changes in the drug war"; "a
        declaration of independence" [syn: {announcement}, {proclamation},
         {annunciation}]
     6: a formal expression by a meeting; ...
