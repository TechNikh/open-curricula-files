---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_quadratic_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: 777665c7-b117-4111-baad-35ffa3895db9
updated: 1484308735
title: Complex quadratic polynomial
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cr_orbit_3.png
tags:
    - Forms
    - Conjugation
    - Between forms
    - With doubling map
    - Map
    - Notation
    - Critical items
    - Critical point
    - Critical value
    - Critical orbit
    - Critical sector
    - Critical polynomial
    - Critical curves
    - Planes
    - Parameter plane
    - Dynamical plane
    - Derivatives
    - Derivative with respect to c
    - Derivative with respect to z
    - Schwarzian derivative
categories:
    - Complex dynamics
---
