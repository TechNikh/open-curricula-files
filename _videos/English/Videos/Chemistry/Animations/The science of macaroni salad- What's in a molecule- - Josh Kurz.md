---
version: 1
type: video
provider: YouTube
id: 4WR0_gEEZ9I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/The%20science%20of%20macaroni%20salad-%20What%27s%20in%20a%20molecule-%20-%20Josh%20Kurz.webm"
offline_file: ""
offline_thumbnail: ""
uuid: dc98d28e-f68f-4a58-8f15-c3add7f19364
updated: 1486069665
title: "The science of macaroni salad- What's in a molecule? - Josh Kurz"
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/4WR0_gEEZ9I/default.jpg
    - https://i3.ytimg.com/vi/4WR0_gEEZ9I/1.jpg
    - https://i3.ytimg.com/vi/4WR0_gEEZ9I/2.jpg
    - https://i3.ytimg.com/vi/4WR0_gEEZ9I/3.jpg
---
