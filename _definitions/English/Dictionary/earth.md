---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/earth
offline_file: ""
offline_thumbnail: ""
uuid: 887a89e8-0b76-4605-9495-84057c24c325
updated: 1484310290
title: earth
categories:
    - Dictionary
---
Earth
     n 1: the 3rd planet from the sun; the planet on which we live;
          "the Earth moves around the sun"; "he sailed around the
          world" [syn: {world}, {globe}]
     2: the loose soft material that makes up a large part of the
        land surface; "they dug into the earth outside the church"
        [syn: {ground}]
     3: the solid part of the earth's surface; "the plane turned
        away from the sea and moved back over land"; "the earth
        shook for several minutes"; "he dropped the logs on the
        ground" [syn: {land}, {dry land}, {ground}, {solid ground},
         {terra firma}]
     4: the abode of mortals (as contrasted with heaven or hell);
        ...
