---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compounded
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484331481
title: compounded
categories:
    - Dictionary
---
compounded
     adj : combined into or constituting a chemical compound
