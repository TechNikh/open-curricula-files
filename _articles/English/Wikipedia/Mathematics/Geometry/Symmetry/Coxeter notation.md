---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coxeter_notation
offline_file: ""
offline_thumbnail: ""
uuid: 8b4ee257-018a-4ed4-9bcb-57226afb43f7
updated: 1484309309
title: Coxeter notation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-43-radial_subgroups.png
tags:
    - Reflectional groups
    - Subgroups
    - Halving subgroups
    - Radical subgroups
    - Trionic subgroups
    - Central inversion
    - Rotations and rotary reflections
    - Commutator subgroups
    - Example subgroups
    - Rank 2 example subgroups
    - Rank 3 Euclidean example subgroups
    - Hyperbolic example subgroups
    - Extended symmetry
    - Computation with reflection matrices as symmetry generators
    - Rank one groups
    - Rank two groups
    - Rank three groups
    - Subgroups
    - Rank four groups
    - Point groups
    - Subgroups
    - Space groups
    - Line groups
    - Duoprismatic group
    - Wallpaper groups
    - Complex reflections
    - Notes
categories:
    - Symmetry
---
In geometry, Coxeter notation (also Coxeter symbol) is a system of classifying symmetry groups, describing the angles between with fundamental reflections of a Coxeter group in a bracketed notation, with modifiers to indicate certain subgroups. The notation is named after H. S. M. Coxeter, and has been more comprehensively defined by Norman Johnson.
