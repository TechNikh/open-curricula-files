---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nasik
offline_file: ""
offline_thumbnail: ""
uuid: 0a2f738d-8da3-4754-86c2-9ce56289a2cd
updated: 1484308438
title: Nashik
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rama%252C_Lakshman_and_Sita_at_the_Kalaram_Temple%252C_Nashik..jpg'
tags:
    - geography
    - Climate
    - Civic administration
    - Court
    - Solid waste management
    - Demographics
    - Art and Culture
    - Tourism
    - Dams
    - culture
    - Kumbh Mela
    - Economy
    - Agriculture
    - Rain water harvesting
    - Industry
    - Wine industry
    - Education
    - People from Nashik
categories:
    - Uncategorized
---
Nashik (pron:ˈnʌʃɪk) ( pronunciation (help·info))[3] is a city in the northwest region of Maharashtra in India, and is the administrative headquarter of the Nashik District and Nashik Division.
