---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shine
offline_file: ""
offline_thumbnail: ""
uuid: b6f18c24-fedb-452b-8ca5-2319eb80ad44
updated: 1484310210
title: shine
categories:
    - Dictionary
---
shine
     n : the quality of being bright and sending out rays of light
         [syn: {radiance}, {radiancy}, {effulgence}, {refulgence},
          {refulgency}]
     v 1: be bright by reflecting or casting light; "Drive
          carefully--the wet road reflects" [syn: {reflect}]
     2: emit light; be bright, as of the sun or a light; "The sun
        shone bright that day"; "The fire beamed on their faces"
        [syn: {beam}]
     3: be shiny, as if wet; "His eyes were glistening" [syn: {glitter},
         {glisten}, {glint}, {gleam}]
     4: be distinguished or eminent; "His talent shines"
     5: be clear and obvious; "A shining example"
     6: especially of the complexion: show a ...
