---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dual_space
offline_file: ""
offline_thumbnail: ""
uuid: 683e81a1-98cf-41aa-a6eb-dc6827850211
updated: 1484309237
title: Dual space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Double_dual_nature.svg.png
tags:
    - Algebraic dual space
    - Finite-dimensional case
    - Infinite-dimensional case
    - Bilinear products and dual spaces
    - Injection into the double-dual
    - Transpose of a linear map
    - Quotient spaces and annihilators
    - Continuous dual space
    - Examples
    - Transpose of a continuous linear map
    - Annihilators
    - Further properties
    - Topologies on the dual
    - Double dual
    - Notes
categories:
    - Duality theories
---
In mathematics, any vector space V has a corresponding dual vector space (or just dual space for short) consisting of all linear functionals on V together with a naturally induced linear structure.
