---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Power_bandwidth
offline_file: ""
offline_thumbnail: ""
uuid: 4a1309b9-8183-41db-ad3d-5e1d8a431cba
updated: 1484308894
title: Power bandwidth
categories:
    - Waves
---
The power bandwidth of an amplifier is sometimes taken as the frequency range (or, rarely, the upper frequency limit) for which the rated power output[1] of an amplifier can be maintained (without excessive distortion) to at least half of the full rated power.[2][3] (Some specifications may mandate 100% of the rated power; sometimes referring to as the full-power bandwidth.)
