---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/physics
offline_file: ""
offline_thumbnail: ""
uuid: 97df0a18-306a-40a2-93e6-e5c66ecd3532
updated: 1484310391
title: physics
categories:
    - Dictionary
---
physics
     n : the science of matter and energy and their interactions
         [syn: {physical science}, {natural philosophy}]
