---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cantor%27s_paradise'
offline_file: ""
offline_thumbnail: ""
uuid: 52881985-a0de-4252-9430-fbf63ac37eda
updated: 1484309501
title: "Cantor's paradise"
categories:
    - Set theory
---
Cantor's paradise is an expression used by David Hilbert (1926, page 170) in describing set theory and infinite cardinal numbers developed by Georg Cantor. The context of Hilbert's comment was his opposition to what he saw as L. E. J. Brouwer's reductive attempts to circumscribe what kind of mathematics is acceptable; see Brouwer–Hilbert controversy.
