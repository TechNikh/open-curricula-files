---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mushroom_cloud
offline_file: ""
offline_thumbnail: ""
uuid: cb7d4736-c5a4-4ff4-a43c-6998895d158a
updated: 1484308875
title: Mushroom cloud
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-MtRedoubtedit1.jpg
tags:
    - Origin of the term
    - Physics
    - Nuclear mushroom clouds
    - Cloud composition
    - Radioisotopes
    - Fluorescent glow
    - Condensation effects
    - Bibliography
categories:
    - Vortices
---
A mushroom cloud is a distinctive pyrocumulus mushroom-shaped cloud of debris/smoke and usually condensed water vapor resulting from a large explosion. The effect is most commonly associated with a nuclear explosion, but any sufficiently energetic detonation or deflagration will produce the same sort of effect. They can be caused by powerful conventional weapons, like vacuum bombs, including the ATBIP and GBU-43/B Massive Ordnance Air Blast. Some volcanic eruptions and impact events can produce natural mushroom clouds.
