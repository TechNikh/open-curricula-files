---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multicritical_point
offline_file: ""
offline_thumbnail: ""
uuid: 63b12b6d-3612-49ff-abcb-3df2b99e8533
updated: 1484308746
title: Multicritical point
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Multicritical_end.png
tags:
    - Definition
    - Examples
    - Tricritical Point and Multicritical Points of Higher Order
    - Lifshitz Point
    - Lifshitz Tricritical Point
    - Renormalization Group
categories:
    - Critical phenomena
---
Multicritical points are special points in the parameter space of thermodynamic or other systems with a continuous phase transition. At least two thermodynamic or other parameters must be adjusted to reach a multicritical point. At a multicritical point the system belongs to a universality class different from the "normal" universality class.
