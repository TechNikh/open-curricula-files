---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/luxurious
offline_file: ""
offline_thumbnail: ""
uuid: be9c5b4a-589e-4986-b0ea-137d02fa2f53
updated: 1484310148
title: luxurious
categories:
    - Dictionary
---
luxurious
     adj 1: furnishing gratification of the senses; "an epicurean
            banquet"; "enjoyed a luxurious suite with a crystal
            chandelier and thick oriental rugs"; "Lucullus spent
            the remainder of his days in voluptuous magnificence";
            "a chinchilla robe of sybaritic lavishness" [syn: {epicurean},
             {sybaritic}, {voluptuary}, {voluptuous}]
     2: rich and superior in quality; "a princely sum"; "gilded
        dining rooms" [syn: {deluxe}, {gilded}, {opulent}, {princely},
         {sumptuous}]
