---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fermat_cubic
offline_file: ""
offline_thumbnail: ""
uuid: b6aeb917-8a27-4aed-b6d8-545151a2c5cf
updated: 1484309363
title: Fermat cubic
categories:
    - Geometry stubs
---
Methods of algebraic geometry provide the following parameterization of Fermat's cubic:
