---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impute
offline_file: ""
offline_thumbnail: ""
uuid: 00391fb9-c168-4ab0-a07e-180b07075020
updated: 1484310152
title: impute
categories:
    - Dictionary
---
impute
     v 1: attribute or credit to; "We attributed this quotation to
          Shakespeare"; "People impute great cleverness to cats"
          [syn: {ascribe}, {assign}, {attribute}]
     2: attribute (responsibility or fault) to a cause or source;
        "The teacher imputed the student's failure to his
        nervousness"
