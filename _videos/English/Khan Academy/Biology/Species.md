---
version: 1
type: video
provider: YouTube
id: Tmt4zrDK3dA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Species.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8d423e95-4a67-4d6b-8efd-5a8fd645a1a9
updated: 1486069581
title: Species
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/Tmt4zrDK3dA/default.jpg
    - https://i3.ytimg.com/vi/Tmt4zrDK3dA/1.jpg
    - https://i3.ytimg.com/vi/Tmt4zrDK3dA/2.jpg
    - https://i3.ytimg.com/vi/Tmt4zrDK3dA/3.jpg
---
What a species is and isn't.  Ligers, tiglons, mule, hinnies, and dogs
More free lessons at: http://www.khanacademy.org/video?v=Tmt4zrDK3dA
