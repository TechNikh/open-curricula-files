---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/R%C3%A9nyi_entropy'
offline_file: ""
offline_thumbnail: ""
uuid: 9f726d40-2586-4542-9abe-33758f6a7e8d
updated: 1484308775
title: Rényi entropy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Mplwp_reny_entropy012inf.svg.png
tags:
    - Definition
    - Special cases of the Rényi entropy
    - Hartley or max-entropy
    - Shannon entropy
    - Collision entropy
    - Min-entropy
    - Inequalities between different values of α
    - Rényi divergence
    - Why α=1 is special
    - Exponential families
    - Physical meaning
    - Notes
categories:
    - Entropy and information
---
In information theory, the Rényi entropy generalizes the Hartley entropy, the Shannon entropy, the collision entropy and the min entropy. Entropies quantify the diversity, uncertainty, or randomness of a system. The Rényi entropy is named after Alfréd Rényi.[1]
