---
version: 1
type: video
provider: YouTube
id: D5PnVCRC1cE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ea93639c-d5bf-4d06-9f7f-f6ebcebb35dd
updated: 1486069677
title: 11.3 Membrane Disruption
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/D5PnVCRC1cE/default.jpg
    - https://i3.ytimg.com/vi/D5PnVCRC1cE/1.jpg
    - https://i3.ytimg.com/vi/D5PnVCRC1cE/2.jpg
    - https://i3.ytimg.com/vi/D5PnVCRC1cE/3.jpg
---
