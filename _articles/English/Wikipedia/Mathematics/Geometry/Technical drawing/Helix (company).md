---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Helix_(company)
offline_file: ""
offline_thumbnail: ""
uuid: 02bb0dec-4664-42c9-ab19-07695c5a6c67
updated: 1484309327
title: Helix (company)
tags:
    - History
    - Brands
    - Administration
categories:
    - Technical drawing
---
Helix is a family-run United Kingdom-based manufacturer of scholastic stationery. It currently[when?] employs 75 people, and exports to over 65 countries, with offices in Chicago and Hong Kong and with UK headquarters based in Lye, West Midlands.
