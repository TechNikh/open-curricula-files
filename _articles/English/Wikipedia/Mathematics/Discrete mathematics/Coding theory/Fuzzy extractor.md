---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fuzzy_extractor
offline_file: ""
offline_thumbnail: ""
uuid: 9ebc0ea2-3927-4007-8d8f-592039500eac
updated: 1484308545
title: Fuzzy extractor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Secure_Sketch_Constructions.PNG
tags:
    - Motivation
    - Basic definitions
    - Predictability
    - Min-entropy
    - Statistical distance
    - Definition 1 (strong extractor)
    - Secure sketch
    - Definition 2 (secure sketch)
    - Fuzzy extractor
    - Definition 3 (fuzzy extractor)
    - Secure sketches and fuzzy extractors
    - Lemma 1 (fuzzy extractors from sketches)
    - Corollary 1
    - Basic constructions
    - Hamming distance constructions
    - Code-offset construction
    - Syndrome construction
    - Set difference constructions
    - Pin sketch construction
    - Edit distance constructions
    - Other distance measure constructions
    - Improving error-tolerance via relaxed notions of correctness
    - Random errors
    - Input-dependent errors
    - Computationally bounded errors
    - Privacy guarantees
    - Correlation between helper string and biometric input
    - Gen(W) as a probabilistic map
    - Uniform fuzzy extractors
    - Uniform secure sketches
    - Applications
    - Protection against active attacks
    - Robust fuzzy extractors
categories:
    - Coding theory
---
Fuzzy extractors convert biometric data into random strings, which makes it possible to apply cryptographic techniques for biometric security. They are used to encrypt and authenticate users records, with biometric inputs as a key. Historically, the first biometric system of this kind was designed by Juels and Wattenberg and was called "Fuzzy commitment", where the cryptographic key is decommitted using biometric data. "Fuzzy", in that context, implies that the value close to the original one can extract the committed value. Later, Juels and Sudan came up with Fuzzy vault schemes which are order invariant for the fuzzy commitment scheme but uses a Reed–Solomon code. Codeword is evaluated ...
