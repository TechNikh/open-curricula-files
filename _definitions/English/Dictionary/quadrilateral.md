---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quadrilateral
offline_file: ""
offline_thumbnail: ""
uuid: 1d72b726-f3e2-4d56-a59d-cf6f034bc519
updated: 1484310273
title: quadrilateral
categories:
    - Dictionary
---
quadrilateral
     adj : having four sides [syn: {four-sided}]
     n : a four-sided polygon [syn: {quadrangle}, {tetragon}]
