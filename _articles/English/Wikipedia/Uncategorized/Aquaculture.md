---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aquaculture
offline_file: ""
offline_thumbnail: ""
uuid: 0213d205-c1fa-4fd1-9b7b-865eec6c5a92
updated: 1484308340
title: Aquaculture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Yangzhuanghe_-_CIMG3404.JPG
tags:
    - History
    - 21st-century practice
    - Species groups
    - Aquatic plants
    - Fish
    - Crustaceans
    - Molluscs
    - Other groups
    - Around the world
    - National laws, regulations, and management
    - Over-reporting
    - Aquacultural methods
    - Mariculture
    - Integrated
    - Netting materials
    - Issues
    - Fish oils
    - Impacts on wild fish
    - Coastal ecosystems
    - Pollution from sea cage aquaculture
    - Genetic modification
    - Animal welfare
    - Common welfare concerns
    - Improving welfare
    - Prospects
    - Notes
categories:
    - Uncategorized
---
Aquaculture, also known as aquafarming, is the farming of fish, crustaceans, molluscs, aquatic plants, algae, and other aquatic organisms. Aquaculture involves cultivating freshwater and saltwater populations under controlled conditions, and can be contrasted with commercial fishing, which is the harvesting of wild fish.[2] Mariculture refers to aquaculture practiced in marine environments and in underwater habitats.
