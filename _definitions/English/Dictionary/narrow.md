---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/narrow
offline_file: ""
offline_thumbnail: ""
uuid: 30c8ea71-ba27-4f5e-9870-d5d5700208aa
updated: 1484310383
title: narrow
categories:
    - Dictionary
---
narrow
     adj 1: not wide; "a narrow bridge"; "a narrow line across the page"
            [ant: {wide}]
     2: limited in size or scope; "the narrow sense of a word"
     3: lacking tolerance or flexibility or breadth of view; "a
        brilliant but narrow-minded judge"; "narrow opinions"
        [syn: {narrow-minded}] [ant: {broad-minded}]
     4: very limited in degree; "won by a narrow margin"; "a narrow
        escape" [ant: {wide}]
     5: characterized by painstaking care and detailed examination;
        "a minute inspection of the grounds"; "a narrow scrutiny";
        "an exact and minute report" [syn: {minute}]
     n : a narrow strait connecting two bodies of water
     v 1: ...
