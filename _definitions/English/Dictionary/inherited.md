---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inherited
offline_file: ""
offline_thumbnail: ""
uuid: 5291864c-bd09-4ace-aebd-aa37885babce
updated: 1484310307
title: inherited
categories:
    - Dictionary
---
inherited
     adj : tending to occur among members of a family usually by
           heredity; "an inherited disease"; "familial traits";
           "genetically transmitted features" [syn: {familial}, {genetic},
            {hereditary}, {transmitted}, {transmissible}]
