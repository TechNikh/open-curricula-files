---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-employed
offline_file: ""
offline_thumbnail: ""
uuid: dfc7cf6e-27aa-4535-9c2e-2d277b3ea057
updated: 1484310469
title: self-employed
categories:
    - Dictionary
---
self-employed
     adj : working for yourself [syn: {freelance}] [ant: {salaried}]
