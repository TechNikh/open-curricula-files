---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Colin_de_Verdi%C3%A8re_graph_invariant'
offline_file: ""
offline_thumbnail: ""
uuid: 17df39e4-60e0-422f-b766-17247eab191b
updated: 1484309411
title: Colin de Verdière graph invariant
tags:
    - Definition
    - Characterization of known graph families
    - Graph minors
    - Chromatic number
    - Other properties
    - Influence
    - Notes
categories:
    - Graph minor theory
---
Colin de Verdière's invariant is a graph parameter 
  
    
      
        μ
        (
        G
        )
      
    
    {\displaystyle \mu (G)}
  
 for any graph G, introduced by Yves Colin de Verdière in 1990. It was motivated by the study of the maximum multiplicity of the second eigenvalue of certain Schrödinger operators.[1]
