---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cinch
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484425861
title: cinch
categories:
    - Dictionary
---
cinch
     n 1: any undertaking that is easy to do; "marketing this product
          will be no picnic" [syn: {breeze}, {picnic}, {snap}, {duck
          soup}, {child's play}, {pushover}, {walkover}, {piece of
          cake}]
     2: stable gear consisting of a band around a horse's belly that
        holds the saddle in place [syn: {girth}]
     3: a form of all fours in which the players bid for the
        privilege of naming trumps
     v 1: tie a cinch around; "cinch horses" [syn: {girth}]
     2: make sure of
     3: get a grip on; get mastery of
