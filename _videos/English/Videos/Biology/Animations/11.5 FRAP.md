---
version: 1
type: video
provider: YouTube
id: oWg0blrV994
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/11.5%20FRAP.webm"
offline_file: ""
offline_thumbnail: ""
uuid: be62bd1f-eb80-46d9-aa1f-d27f23ab1e68
updated: 1486069677
title: 11.5 FRAP
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/oWg0blrV994/default.jpg
    - https://i3.ytimg.com/vi/oWg0blrV994/1.jpg
    - https://i3.ytimg.com/vi/oWg0blrV994/2.jpg
    - https://i3.ytimg.com/vi/oWg0blrV994/3.jpg
---
