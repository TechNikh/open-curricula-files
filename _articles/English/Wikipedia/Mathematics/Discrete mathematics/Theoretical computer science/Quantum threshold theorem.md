---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_threshold_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 73cb5a45-0c60-42cd-9dc9-adec3189531e
updated: 1484308681
title: Quantum threshold theorem
categories:
    - Theoretical computer science
---
In quantum computing, the (quantum) threshold theorem (or quantum fault-tolerance theorem), proved by Michael Ben-Or and Dorit Aharonov (along with other groups),[who?] states that a quantum computer with noise can quickly and accurately simulate an ideal quantum computer, provided the level of noise is below a certain threshold. Practically, the Threshold Theorem implies that the error in quantum computers can be controlled as the number of qubits scales up.
