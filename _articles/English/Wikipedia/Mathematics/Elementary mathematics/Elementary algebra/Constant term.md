---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Constant_term
offline_file: ""
offline_thumbnail: ""
uuid: 972f6bf7-b7cf-416e-82a7-5fba5b016fa8
updated: 1484308913
title: Constant term
categories:
    - Elementary algebra
---
In mathematics, a constant term is a term in an algebraic expression that has a value that is constant or cannot change, because it does not contain any modifiable variables. For example, in the quadratic polynomial
