---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/young
offline_file: ""
offline_thumbnail: ""
uuid: 8f633c54-706c-4933-aa47-861b18bbec15
updated: 1484310473
title: young
categories:
    - Dictionary
---
young
     adj 1: (used of living things especially persons) in an early
            period of life or development or growth; "young
            people" [syn: {immature}] [ant: {old}]
     2: (of crops) harvested at an early stage of development;
        before complete maturity; "new potatoes"; "young corn"
        [syn: {new}]
     n 1: any immature animal [syn: {offspring}]
     2: United States film and television actress (1913-2000) [syn:
        {Loretta Young}]
     3: United States civil rights leader (1921-1971) [syn: {Whitney
        Young}, {Whitney Moore Young Jr.}]
     4: British physicist and Egyptologist; he revived the wave
        theory of light and proposed a ...
