---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irrigate
offline_file: ""
offline_thumbnail: ""
uuid: f09cc857-2c4b-4c7e-a166-7149d4d43868
updated: 1484310260
title: irrigate
categories:
    - Dictionary
---
irrigate
     v 1: supply with water, as with channels or ditches or streams;
          "Water the fields" [syn: {water}]
     2: supply with a constant flow or sprinkling of some liquid,
        for the purpose of cooling, cleansing, or disinfecting;
        "irrigate the wound"
