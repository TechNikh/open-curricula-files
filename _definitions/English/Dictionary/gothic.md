---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gothic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484404381
title: gothic
categories:
    - Dictionary
---
Gothic
     adj 1: characteristic of the style of type commonly used for
            printing German
     2: of or relating to the language of the ancient Goths; "the
        Gothic Bible translation"
     3: of or relating to the Goths; "Gothic migrations"
     4: as if belonging to the Middle Ages; old-fashioned and
        unenlightened; "a medieval attitude toward dating" [syn: {medieval},
         {mediaeval}]
     5: characterized by gloom and mystery and the grotesque;
        "gothic novels like `Frankenstein'"
     n 1: extinct East Germanic language of the ancient Goths; the
          only surviving record being fragments of a 4th-century
          translation of the Bible by ...
