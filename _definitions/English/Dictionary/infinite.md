---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infinite
offline_file: ""
offline_thumbnail: ""
uuid: 0e2c4708-6ac2-4240-921f-24e144d3d25b
updated: 1484310206
title: infinite
categories:
    - Dictionary
---
infinite
     adj 1: having no limits or boundaries in time or space or extent or
            magnitude; "the infinite ingenuity of man"; "infinite
            wealth" [ant: {finite}]
     2: of verbs; having neither person nor number nor mood (as a
        participle or gerund or infinitive); "infinite verb form"
        [syn: {non-finite}] [ant: {finite}]
     3: too numerous to be counted; "incalculable riches";
        "countless hours"; "an infinite number of reasons";
        "innumerable difficulties"; "the multitudinous seas";
        "myriad stars"; "untold thousands" [syn: {countless}, {innumerable},
         {innumerous}, {myriad(a)}, {multitudinous}, {numberless},
         ...
