---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Semilinear_response
offline_file: ""
offline_thumbnail: ""
uuid: 2516b99e-ece0-444d-9b88-0288ca122d75
updated: 1484308811
title: Semilinear response
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-ResistorsNetwork.jpg
tags:
    - Applications
    - Definition of semilinear response
    - Resistor network modeling
    - Fermi golden rule picture
categories:
    - Non-equilibrium thermodynamics
---
