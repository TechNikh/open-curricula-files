---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roughly
offline_file: ""
offline_thumbnail: ""
uuid: 0456fe70-982a-481b-bf94-5212d2b7bf2e
updated: 1484310273
title: roughly
categories:
    - Dictionary
---
roughly
     adv 1: (of quantities) imprecise but fairly close to correct;
            "lasted approximately an hour"; "in just about a
            minute"; "he's about 30 years old"; "I've had about
            all I can stand"; "we meet about once a month"; "some
            forty people came"; "weighs around a hundred pounds";
            "roughly $3,000"; "holds 3 gallons, more or less"; "20
            or so people were at the party" [syn: {approximately},
             {about}, {close to}, {just about}, {some}, {more or
            less}, {around}, {or so}]
     2: with roughness or violence (`rough' is an informal variant
        for `roughly'); "he was pushed roughly aside"; "they
   ...
