---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perestroika
offline_file: ""
offline_thumbnail: ""
uuid: 1c4fa19f-9350-40fa-9ccb-124feb08dcb5
updated: 1484310176
title: perestroika
categories:
    - Dictionary
---
perestroika
     n : an economic policy adopted in the former Soviet Union;
         intended to increase automation and labor efficiency but
         it led eventually to the end of central planning in the
         Russian economy
