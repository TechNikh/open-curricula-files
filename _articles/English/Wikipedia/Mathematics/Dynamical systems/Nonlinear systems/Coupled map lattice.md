---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coupled_map_lattice
offline_file: ""
offline_thumbnail: ""
uuid: 61b9f081-7a2a-41a5-8cb2-15b5fa4d1565
updated: 1484308811
title: Coupled map lattice
tags:
    - Introduction
    - History
    - Classification
    - Unique CML qualitative classes
    - Visual phenomena
    - Quantitative analysis quantifiers
categories:
    - Nonlinear systems
---
A coupled map lattice (CML) is a dynamical system that models the behavior of non-linear systems (especially partial differential equations). They are predominantly used to qualitatively study the chaotic dynamics of spatially extended systems. This includes the dynamics of spatiotemporal chaos where the number of effective degrees of freedom diverges as the size of the system increases.[1]
