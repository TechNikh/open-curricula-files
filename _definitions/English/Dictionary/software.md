---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/software
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484437861
title: software
categories:
    - Dictionary
---
software
     n : (computer science) written programs or procedures or rules
         and associated documentation pertaining to the operation
         of a computer system and that are stored in read/write
         memory; "the market for software is expected to expand"
         [syn: {software system}, {software package}, {package}]
         [ant: {hardware}]
