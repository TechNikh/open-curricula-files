---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Girth_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 6607aa10-7ae3-4d25-965b-85c88a9032b8
updated: 1484309420
title: Girth (graph theory)
tags:
    - Cages
    - Girth and graph coloring
    - Related concepts
categories:
    - Graph invariants
---
In graph theory, the girth of a graph is the length of a shortest cycle contained in the graph.[1] If the graph does not contain any cycles (i.e. it's an acyclic graph), its girth is defined to be infinity.[2] For example, a 4-cycle (square) has girth 4. A grid has girth 4 as well, and a triangular mesh has girth 3. A graph with girth four or more is triangle-free.
