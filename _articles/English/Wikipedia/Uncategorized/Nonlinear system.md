---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-linear
offline_file: ""
offline_thumbnail: ""
uuid: fc0853b2-abd2-46ed-98c5-e1ef0b49bb6a
updated: 1484308319
title: Nonlinear system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-PendulumLayout.svg.png
tags:
    - Definition
    - Nonlinear algebraic equations
    - Nonlinear recurrence relations
    - Nonlinear differential equations
    - Ordinary differential equations
    - Partial differential equations
    - Pendula
    - Types of nonlinear behaviors
    - Examples of nonlinear equations
categories:
    - Uncategorized
---
In physical sciences, a nonlinear system is a system in which the output is not directly proportional to the input.[1] Nonlinear problems are of interest to engineers, physicists and mathematicians and many other scientists because most systems are inherently nonlinear in nature. Nonlinear systems may appear chaotic, unpredictable or counterintuitive, contrasting with the much simpler linear systems.
