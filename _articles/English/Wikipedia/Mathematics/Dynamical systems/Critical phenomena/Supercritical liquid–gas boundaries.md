---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Supercritical_liquid%E2%80%93gas_boundaries'
offline_file: ""
offline_thumbnail: ""
uuid: 04292e7c-a958-4560-a6bb-eedb0f053bf3
updated: 1484308746
title: Supercritical liquid–gas boundaries
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-PT-FrenkelFisherWidomSchematic.png
categories:
    - Critical phenomena
---
Supercritical liquid–gas boundaries are lines in the p–T diagram that delimit more liquid-like and more gas-like states of a supercritical fluid. They comprise the Fisher–Widom line, the Widom line, and the Frenkel line.
