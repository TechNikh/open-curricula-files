---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Glossary_of_Riemannian_and_metric_geometry
offline_file: ""
offline_thumbnail: ""
uuid: dfca8aeb-c748-4ebb-90f8-26f093ba717a
updated: 1484309261
title: Glossary of Riemannian and metric geometry
categories:
    - Metric geometry
---
The following articles may also be useful; they either contain specialised vocabulary or provide more detailed expositions of the definitions given below.
