---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Parabolic_geometry_(differential_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 7ab4e47a-7e96-437a-8289-1b9b36f2eb0d
updated: 1484309249
title: Parabolic geometry (differential geometry)
categories:
    - Homogeneous spaces
---
In differential geometry and the study of Lie groups, a parabolic geometry is a homogeneous space G/P which is the quotient of a semisimple Lie group G by a parabolic subgroup P. More generally, the curved analogs of a parabolic geometry in this sense is also called a parabolic geometry: any geometry that is modeled on such a space by means of a Cartan connection.
