---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infected
offline_file: ""
offline_thumbnail: ""
uuid: 88f294e3-5cf4-4f3c-bc91-cd8744965bdc
updated: 1484310163
title: infected
categories:
    - Dictionary
---
infected
     adj 1: contaminated with infecting organisms; "dirty wounds";
            "obliged to go into infected rooms"- Jane Austen [syn:
             {contaminated}, {dirty}, {pestiferous}]
     2: having undergone infection; "a purulent  wound" [syn: {purulent},
         {pussy}, {putrid}]
