---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lazy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413621
title: lazy
categories:
    - Dictionary
---
lazy
     adj 1: moving slowly and gently; "up a lazy river"; "lazy white
            clouds"; "at a lazy pace"
     2: disinclined to work or exertion; "faineant kings under whose
        rule the country languished"; "an indolent hanger-on";
        "too lazy to wash the dishes"; "shiftless idle youth";
        "slothful employees"; "the unemployed are not necessarily
        work-shy" [syn: {faineant}, {indolent}, {otiose}, {slothful},
         {work-shy}]
     [also: {laziest}, {lazier}]
