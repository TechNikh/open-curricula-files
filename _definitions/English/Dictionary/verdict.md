---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/verdict
offline_file: ""
offline_thumbnail: ""
uuid: cf824d7a-17f9-4348-a2c6-b6a7dcacda6d
updated: 1484310535
title: verdict
categories:
    - Dictionary
---
verdict
     n : (law) the findings of a jury on issues of fact submitted to
         it for decision; can be used in formulating a judgment
         [syn: {finding of fact}]
