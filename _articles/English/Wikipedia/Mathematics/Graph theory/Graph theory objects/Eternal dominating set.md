---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eternal_dominating_set
offline_file: ""
offline_thumbnail: ""
uuid: f98c0675-850d-41c5-a2f7-79486804f2d6
updated: 1484309442
title: Eternal dominating set
tags:
    - History
    - Bounds
    - Bounds on the m-eternal domination number
    - Open Questions
categories:
    - Graph theory objects
---
In graph theory, an eternal dominating set for a graph G = (V, E) is a subset D of V such that D is a dominating set on which mobile guards are initially located (at most one guard may be located on any vertex). The set D must be such that for any infinite sequence of attacks occurring sequentially at vertices, the set D can be modified by moving a guard from an adjacent vertex to the attacked vertex, provided the attacked vertex has no guard on it at the time it is attacked. The configuration of guards after each attack must induce a dominating set. The eternal domination number, γ∞(G), is the minimum number of vertices possible in the initial set D. For example, the eternal ...
