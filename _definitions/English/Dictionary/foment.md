---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foment
offline_file: ""
offline_thumbnail: ""
uuid: f8dce4a4-3228-40b1-829d-432d85aae306
updated: 1484310174
title: foment
categories:
    - Dictionary
---
foment
     v 1: try to stir up public opinion [syn: {agitate}, {stir up}]
     2: bathe with warm water or medicated lotions; "His legs should
        be fomented"
