---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geometry
offline_file: ""
offline_thumbnail: ""
uuid: bc7664b0-b8f8-421f-8ec9-b593f1cd56e0
updated: 1484310218
title: geometry
categories:
    - Dictionary
---
geometry
     n : the pure mathematics of points and lines and curves and
         surfaces
