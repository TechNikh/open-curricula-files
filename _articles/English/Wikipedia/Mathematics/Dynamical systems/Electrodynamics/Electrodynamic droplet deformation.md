---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Electrodynamic_droplet_deformation
offline_file: ""
offline_thumbnail: ""
uuid: b780d0f3-62cb-4d08-99ab-90e6e152f024
updated: 1484308754
title: Electrodynamic droplet deformation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-UPDATED_ELECTRODYNAMIC_DROPLET_DEFORMATION.png
tags:
    - Motivation
    - Taylor’s 1966 Solution
    - "Torza's Solution"
    - Safety and practical considerations
categories:
    - Electrodynamics
---
Electrohydrodynamic droplet deformation is a phenomenon that occurs when liquid droplets suspended in a second immiscible liquid are exposed to an oscillating electric field. Under these conditions, the droplet will periodically deform between prolate and oblate ellipsoidal shapes. The characteristic frequency and magnitude of the deformation is determined by a balance of electrodynamic, hydrodynamic, and capillary stresses acting on the droplet interface. This phenomenon has been studied extensively both mathematically and experimentally because of the complex fluid dynamics that occur. Characterization and modulation of electrodynamic droplet deformation is of particular interest for ...
