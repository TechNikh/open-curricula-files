---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Interval_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: f8362cd6-93ee-412a-b11d-d9d8d17fc846
updated: 1484309447
title: Interval (graph theory)
categories:
    - Graph theory objects
---
In graph theory, an interval I(h) in a directed graph is a maximal, single entry subgraph in which h is the only entry to I(h) and all closed paths in I(h) contain h. Intervals were described in 1976[dubious – discuss] by F. E. Allen and J. Cooke.[1] Interval graphs are integral to some algorithms used in compilers, specifically data flow analyses.
