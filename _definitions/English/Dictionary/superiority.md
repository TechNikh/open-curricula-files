---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/superiority
offline_file: ""
offline_thumbnail: ""
uuid: 21f15024-933a-492b-93c8-2d0ff630b03c
updated: 1484310563
title: superiority
categories:
    - Dictionary
---
superiority
     n 1: the quality of being superior [syn: {high quality}] [ant: {inferiority},
           {inferiority}]
     2: the quality of being a competitive advantage [syn: {favorable
        position}, {favourable position}]
     3: displaying a sense of being better than others; "he hated
        the white man's superiority and condescension"
     4: the state of excelling or surpassing or going beyond usual
        limits [syn: {transcendence}, {transcendency}]
