---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kelvin%E2%80%93Planck_statement'
offline_file: ""
offline_thumbnail: ""
uuid: 8b1673da-8fae-438f-8545-6c99d185d58e
updated: 1484308801
title: Kelvin–Planck statement
categories:
    - Non-equilibrium thermodynamics
---
The Kelvin–Planck statement (or the heat engine statement) of the second law of thermodynamics states that it is impossible to devise a cyclically operating device, the sole effect of which is to absorb energy in the form of heat from a single thermal reservoir and to deliver an equivalent amount of work.[1] This implies that it is impossible to build a heat engine that has 100% thermal efficiency.[2]
