---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/important
offline_file: ""
offline_thumbnail: ""
uuid: 01620550-2ed9-4bfd-9afc-210930b5c27d
updated: 1484310355
title: important
categories:
    - Dictionary
---
important
     adj 1: of great significance or value; "important people"; "the
            important questions of the day" [syn: {of import}]
            [ant: {unimportant}]
     2: important in effect or meaning; "a significant change in tax
        laws"; "a significant change in the Constitution"; "a
        significant contribution"; "significant details";
        "statistically significant" [syn: {significant}] [ant: {insignificant}]
     3: of extreme importance; vital to the resolution of a crisis;
        "a crucial moment in his career"; "a crucial election"; "a
        crucial issue for women" [syn: {crucial}] [ant: {noncrucial}]
     4: having authority or ascendancy or ...
