---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cull
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413921
title: cull
categories:
    - Dictionary
---
cull
     n : the person or thing rejected or set aside as inferior in
         quality [syn: {reject}]
     v 1: remove something that has been rejected; "cull the sick
          members of the herd"
     2: look for and gather; "pick mushrooms"; "pick flowers" [syn:
        {pick}, {pluck}]
