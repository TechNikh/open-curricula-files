---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Loop_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 3f752861-2bfe-44c4-88a1-8766434b2586
updated: 1484308629
title: Loop (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-6n-graph2.svg.png
tags:
    - Degree
categories:
    - Graph theory
---
Depending on the context, a graph or a multigraph may be defined so as to either allow or disallow the presence of loops (often in concert with allowing or disallowing multiple edges between the same vertices):
