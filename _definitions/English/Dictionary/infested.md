---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infested
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484423281
title: infested
categories:
    - Dictionary
---
infested
     adj : (often followed by `with' or used in combination) troubled
           by or encroached upon in large numbers; "waters
           infested with sharks"; "shark-infested waters"; "the
           locust-overrun countryside"; "drug-plagued streets"
           [syn: {overrun}, {plagued}]
