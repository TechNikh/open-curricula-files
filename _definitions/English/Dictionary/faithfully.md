---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faithfully
offline_file: ""
offline_thumbnail: ""
uuid: 0d1d510d-7cd1-4e19-9346-ec412fd68b28
updated: 1484310595
title: faithfully
categories:
    - Dictionary
---
faithfully
     adv : in a faithful manner; "it always came on, faithfully, like
           the radio" [syn: {dependably}, {reliably}] [ant: {unfaithfully},
            {unfaithfully}, {unfaithfully}]
