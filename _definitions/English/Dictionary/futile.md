---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/futile
offline_file: ""
offline_thumbnail: ""
uuid: 362e16f7-3c7c-4ad1-9675-54f0ce8228b5
updated: 1484310172
title: futile
categories:
    - Dictionary
---
futile
     adj 1: producing no result or effect; "a futile effort"; "the
            therapy was ineffectual"; "an otiose undertaking"; "an
            unavailing attempt" [syn: {ineffectual}, {otiose}, {unavailing}]
     2: unproductive of success; "a fruitless search"; "futile years
        after her artistic peak"; "a sleeveless errand"; "a vain
        attempt" [syn: {bootless}, {fruitless}, {sleeveless}, {vain}]
