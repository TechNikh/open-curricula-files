---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Liouville%27s_equation'
offline_file: ""
offline_thumbnail: ""
uuid: 45070820-ad0d-44a7-9547-2b3240f34ff3
updated: 1484309226
title: "Liouville's equation"
tags:
    - "Other common forms of Liouville's equation"
    - A formulation using the Laplace-Beltrami operator
    - Properties
    - Relation to Gauss–Codazzi equations
    - General solution of the equation
    - Application
    - Notes
categories:
    - Differential geometry
---
In differential geometry, Liouville's equation, named after Joseph Liouville, is the nonlinear partial differential equation satisfied by the conformal factor f of a metric f2(dx2 + dy2) on a surface of constant Gaussian curvature K:
