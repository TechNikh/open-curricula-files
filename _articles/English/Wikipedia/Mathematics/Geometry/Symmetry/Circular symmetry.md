---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circular_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 37ca26cf-8e63-442b-a8b5-0859e968bb44
updated: 1484309304
title: Circular symmetry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-DoubleCone.png
tags:
    - Two dimensions
    - Three dimensions
    - Four dimensions
    - Spherical symmetry
categories:
    - Symmetry
---
Rotational circular symmetry is isomorphic with the circle group in the complex plane, or the special orthogonal group SO(2), and unitary group U(1). Reflective circular symmetry is isomorphic with the orthogonal group O(2).
