---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stint
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484531881
title: stint
categories:
    - Dictionary
---
stint
     n 1: an unbroken period of time during which you do something;
          "there were stretches of boredom"; "he did a stretch in
          the federal penitentiary" [syn: {stretch}]
     2: smallest American sandpiper [syn: {least sandpiper}, {Erolia
        minutilla}]
     3: an individuals prescribed share of work; "her stint as a
        lifeguard exhausted her"
     v 1: subsist on a meager allowance; "scratch and scrimp" [syn: {scrimp},
           {skimp}]
     2: supply sparingly and with restricted quantities; "sting with
        the allowance" [syn: {skimp}, {scant}]
