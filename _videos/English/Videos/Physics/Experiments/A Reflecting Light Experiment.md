---
version: 1
type: video
provider: YouTube
id: OoMZoLnuhqw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 55bf56f3-58b2-4656-a8b2-b3f844880b92
updated: 1486069639
title: A Reflecting Light Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/OoMZoLnuhqw/default.jpg
    - https://i3.ytimg.com/vi/OoMZoLnuhqw/1.jpg
    - https://i3.ytimg.com/vi/OoMZoLnuhqw/2.jpg
    - https://i3.ytimg.com/vi/OoMZoLnuhqw/3.jpg
---
