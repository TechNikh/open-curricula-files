---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tarski%27s_circle-squaring_problem'
offline_file: ""
offline_thumbnail: ""
uuid: ab80ea9a-1aab-4b20-8ca7-592c1f06c59b
updated: 1484308608
title: "Tarski's circle-squaring problem"
categories:
    - Discrete geometry
---
Tarski's circle-squaring problem is the challenge, posed by Alfred Tarski in 1925, to take a disc in the plane, cut it into finitely many pieces, and reassemble the pieces so as to get a square of equal area. This was proven to be possible by Miklós Laczkovich in 1990; the decomposition makes heavy use of the axiom of choice and is therefore non-constructive. Laczkovich's decomposition uses about 1050 different pieces.
