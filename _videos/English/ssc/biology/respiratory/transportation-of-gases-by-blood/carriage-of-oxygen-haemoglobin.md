---
title: Carriage of oxygen (Haemoglobin)
youtube_id: bcpX20xXDEc
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - transportation-of-gases-by-blood
---
