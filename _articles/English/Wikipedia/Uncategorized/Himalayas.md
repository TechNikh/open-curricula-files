---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Trans-himalaya
offline_file: ""
offline_thumbnail: ""
uuid: 12cd1894-7109-4028-b453-8317ddce1870
updated: 1484308450
title: Himalayas
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/287px-Himalayas_landsat_7_2.png
tags:
    - Name
    - Ecology
    - Geology
    - hydrology
    - Lakes
    - Impact on climate
    - Religions of the region
    - RESOURCES
    - culture
categories:
    - Uncategorized
---
The Himalayas, or Himalaya, (/ˌhɪməˈleɪ.ə/ or /hɪˈmɑːləjə/) are a mountain range in Asia separating the plains of the Indian subcontinent from the Tibetan Plateau.
