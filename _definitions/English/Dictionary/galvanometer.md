---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/galvanometer
offline_file: ""
offline_thumbnail: ""
uuid: a3f2aa67-b3d6-4bfb-b2a3-ac7b58b755bc
updated: 1484310366
title: galvanometer
categories:
    - Dictionary
---
galvanometer
     n : meter for detecting or comparing or measuring small electric
         currents
