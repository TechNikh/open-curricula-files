---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/steam
offline_file: ""
offline_thumbnail: ""
uuid: 8b36aad1-fc97-4bb1-b5b7-9a4e6534e7a6
updated: 1484310226
title: steam
categories:
    - Dictionary
---
steam
     n : water at boiling temperature diffused in the atmosphere
     v 1: travel by means of steam power; "The ship steamed off into
          the Pacific"
     2: emit steam; "The rain forest was literally steaming"
     3: rise as vapor
     4: get very angry; "her indifference to his amorous advances
        really steamed the young man"
     5: clean by means of steaming; "steam-clean the upholstered
        sofa" [syn: {steam clean}]
     6: cook something by letting steam pass over it; "just steam
        the vegetables"
