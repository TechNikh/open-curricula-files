---
version: 1
type: video
provider: YouTube
id: CgpExkmY6Y0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/Hyphens%20vs.%20dashes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 53696c6f-f83e-4ba4-9371-554365ac832e
updated: 1486069599
title: Hyphens vs. dashes
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Punctuation
thumbnail_urls:
    - https://i3.ytimg.com/vi/CgpExkmY6Y0/default.jpg
    - https://i3.ytimg.com/vi/CgpExkmY6Y0/1.jpg
    - https://i3.ytimg.com/vi/CgpExkmY6Y0/2.jpg
    - https://i3.ytimg.com/vi/CgpExkmY6Y0/3.jpg
---
Hyphens and dashes look pretty similar, but their uses are very different! Learn when to use each one in this video.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/miscellaneous-punctuation/e/dashes-and-hyphens

Watch the next lesson: https://www.khanacademy.org/miscellaneous-punctuation/v/ellipses

Missed the previous lesson? Watch here: https://www.khanacademy.org/miscellaneous-punctuation/v/dashes

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
