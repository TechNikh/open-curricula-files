---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/melia
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484456041
title: melia
categories:
    - Dictionary
---
Melia
     n : type genus of the Meliaceae: East Indian and Australian
         deciduous trees with leaves resembling those of the ash
         [syn: {genus Melia}]
