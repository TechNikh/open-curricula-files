---
version: 1
type: video
provider: YouTube
id: q5HmV3Czl6g
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Subject%20and%20object%20pronouns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ac4a9f38-e625-48cc-b649-8c71453c7b24
updated: 1486069601
title: Subject and object pronouns
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/q5HmV3Czl6g/default.jpg
    - https://i3.ytimg.com/vi/q5HmV3Czl6g/1.jpg
    - https://i3.ytimg.com/vi/q5HmV3Czl6g/2.jpg
    - https://i3.ytimg.com/vi/q5HmV3Czl6g/3.jpg
---
Before we get any further with pronouns, let's cover what the difference between a subject and an object pronoun is, because the distinction between those two concepts will start coming up a good deal.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/e/pronoun-case?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/grammatical-person-and-pronouns-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/the-pronoun/v/who-versus-whom-the-parts-of-speech-grammar?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
