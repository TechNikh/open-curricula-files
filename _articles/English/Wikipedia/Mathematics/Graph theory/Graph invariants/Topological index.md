---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Topological_index
offline_file: ""
offline_thumbnail: ""
uuid: 5ccd164d-2039-4262-a90d-3b60ef12b1db
updated: 1484309423
title: Topological index
tags:
    - Calculation
    - Types
    - Global and local indices
    - Discrimination capability and superindices
    - Computational complexity
    - List of topological indices
    - Application
    - QSAR
categories:
    - Graph invariants
---
In the fields of chemical graph theory, molecular topology, and mathematical chemistry, a topological index also known as a connectivity index is a type of a molecular descriptor that is calculated based on the molecular graph of a chemical compound.[1] Topological indices are numerical parameters of a graph which characterize its topology and are usually graph invariant. Topological indices are used for example in the development of quantitative structure-activity relationships (QSARs) in which the biological activity or other properties of molecules are correlated with their chemical structure.[2]
