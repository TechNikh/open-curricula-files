---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tiredness
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484514661
title: tiredness
categories:
    - Dictionary
---
tiredness
     n : temporary loss of strength and energy resulting from hard
         physical or mental work; "he was hospitalized for extreme
         fatigue"; "growing fatigue was apparent from the decline
         in the execution of their athletic skills"; "weariness
         overcame her after twelve hours and she fell asleep"
         [syn: {fatigue}, {weariness}]
