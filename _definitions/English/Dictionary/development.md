---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/development
offline_file: ""
offline_thumbnail: ""
uuid: 060bb959-3fbb-4ff7-95c5-f174df649e1f
updated: 1484310283
title: development
categories:
    - Dictionary
---
development
     n 1: act of improving by expanding or enlarging or refining; "he
          congratulated them on their development of a plan to
          meet the emergency"; "they funded research and
          development"
     2: a process in which something passes by degrees to a
        different stage (especially a more advanced or mature
        stage); "the development of his ideas took many years";
        "the evolution of Greek civilization"; "the slow
        development of her skill as a writer" [syn: {evolution}]
        [ant: {degeneration}]
     3: a recent event that has some relevance for the present
        situation; "recent developments in Iraq"; "what a
        ...
