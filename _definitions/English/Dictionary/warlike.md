---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/warlike
offline_file: ""
offline_thumbnail: ""
uuid: 9603c6a6-57b0-403c-ab18-2530681e91ad
updated: 1484310170
title: warlike
categories:
    - Dictionary
---
warlike
     adj 1: inclined to make war [syn: {unpacific}]
     2: suggesting war or military life [syn: {martial}]
