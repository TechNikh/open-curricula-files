---
version: 1
type: video
provider: YouTube
id: YVy_YaETV_Y
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b238dcda-168e-4924-bf92-645d975f559e
updated: 1486069617
title: Deducing with logarithmic and exponential form
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/YVy_YaETV_Y/default.jpg
    - https://i3.ytimg.com/vi/YVy_YaETV_Y/1.jpg
    - https://i3.ytimg.com/vi/YVy_YaETV_Y/2.jpg
    - https://i3.ytimg.com/vi/YVy_YaETV_Y/3.jpg
---
