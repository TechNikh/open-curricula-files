---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/full
offline_file: ""
offline_thumbnail: ""
uuid: 6790eadd-6f52-4e0c-aa28-a3687c9a0818
updated: 1484310349
title: full
categories:
    - Dictionary
---
full
     adj 1: containing as much or as many as is possible or normal; "a
            full glass"; "a sky full of stars"; "a full life";
            "the auditorium was full to overflowing" [ant: {empty}]
     2: constituting the full quantity or extent; complete; "an
        entire town devastated by an earthquake"; "gave full
        attention"; "a total failure" [syn: {entire}, {total}]
     3: complete in extent or degree and in every particular; "a
        full game"; "a total eclipse"; "a total disaster" [syn: {total}]
     4: filled to satisfaction with food or drink; "a full stomach"
        [syn: {replete(p)}]
     5: (of sound) having marked depth and body; "full tones"; "a
     ...
