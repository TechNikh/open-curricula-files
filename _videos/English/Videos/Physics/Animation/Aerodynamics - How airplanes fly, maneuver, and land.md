---
version: 1
type: video
provider: YouTube
id: JSIPr9kcxdA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Aerodynamics%20-%20How%20airplanes%20fly%2C%20maneuver%2C%20and%20land.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b9dc3f12-b7ff-4f4e-844f-40b3b2aa5e02
updated: 1486069656
title: 'Aerodynamics - How airplanes fly, maneuver, and land'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/JSIPr9kcxdA/default.jpg
    - https://i3.ytimg.com/vi/JSIPr9kcxdA/1.jpg
    - https://i3.ytimg.com/vi/JSIPr9kcxdA/2.jpg
    - https://i3.ytimg.com/vi/JSIPr9kcxdA/3.jpg
---
