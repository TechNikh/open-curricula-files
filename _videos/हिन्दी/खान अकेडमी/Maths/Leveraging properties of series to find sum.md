---
version: 1
type: video
provider: YouTube
id: -TOKLOGtrpg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1bbb2a0e-72df-4b13-846f-8d51267ad21a
updated: 1486069605
title: Leveraging properties of series to find sum
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/-TOKLOGtrpg/default.jpg
    - https://i3.ytimg.com/vi/-TOKLOGtrpg/1.jpg
    - https://i3.ytimg.com/vi/-TOKLOGtrpg/2.jpg
    - https://i3.ytimg.com/vi/-TOKLOGtrpg/3.jpg
---
