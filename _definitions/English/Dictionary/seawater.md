---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seawater
offline_file: ""
offline_thumbnail: ""
uuid: a8e90cb0-d5fd-4148-a176-6533bb140493
updated: 1484310383
title: seawater
categories:
    - Dictionary
---
seawater
     n : water containing salts; "the water in the ocean is all
         saltwater" [syn: {saltwater}, {brine}] [ant: {fresh water}]
