---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heritable
offline_file: ""
offline_thumbnail: ""
uuid: 8f1518d6-c5c5-46bc-a9ea-0e910b5c5b23
updated: 1484310297
title: heritable
categories:
    - Dictionary
---
heritable
     adj : that can be inherited; "inheritable traits such as eye
           color"; "an inheritable title" [syn: {inheritable}]
           [ant: {noninheritable}]
