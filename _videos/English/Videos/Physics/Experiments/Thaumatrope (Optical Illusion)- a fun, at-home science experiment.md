---
version: 1
type: video
provider: YouTube
id: BzQv7hozgHA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d12927a6-21b1-4015-810b-fce5be7463bf
updated: 1486069636
title: 'Thaumatrope (Optical Illusion)- a fun, at-home science experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/BzQv7hozgHA/default.jpg
    - https://i3.ytimg.com/vi/BzQv7hozgHA/1.jpg
    - https://i3.ytimg.com/vi/BzQv7hozgHA/2.jpg
    - https://i3.ytimg.com/vi/BzQv7hozgHA/3.jpg
---
