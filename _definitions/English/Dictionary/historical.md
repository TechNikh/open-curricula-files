---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/historical
offline_file: ""
offline_thumbnail: ""
uuid: 26933b77-4dd1-40f3-9b13-f0cda881394a
updated: 1484310313
title: historical
categories:
    - Dictionary
---
historical
     adj 1: of or relating to the study of history; "historical
            scholars"; "a historical perspective" [ant: {ahistorical}]
     2: having once lived or existed or taken place in the real
        world as distinct from being legendary; "the historical
        Jesus"; "doubt that a historical Camelot every existed";
        "actual historical events"
     3: belonging to the past; of what is important or famous in the
        past; "historic victories"; "historical (or historic)
        times"; "a historical character" [syn: {historic}]
     4: used of the study of a phenomenon (especially language) as
        it changes through time; "diachronic linguistics" [syn: ...
