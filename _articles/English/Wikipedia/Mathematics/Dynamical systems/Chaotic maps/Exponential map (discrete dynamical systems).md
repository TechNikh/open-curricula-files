---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Exponential_map_(discrete_dynamical_systems)
offline_file: ""
offline_thumbnail: ""
uuid: e2756783-0124-4e01-899b-615ffdf17ce4
updated: 1484308715
title: Exponential map (discrete dynamical systems)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Exponential_Parameter_Space_Detail_PSP_Rays.png
categories:
    - Chaotic maps
---
The family of exponential functions is called the exponential family.
