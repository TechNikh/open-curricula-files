---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expectation
offline_file: ""
offline_thumbnail: ""
uuid: a05f0c95-4582-4ab4-b285-33e2732a7e81
updated: 1484310484
title: expectation
categories:
    - Dictionary
---
expectation
     n 1: belief about (or mental picture of) the future [syn: {outlook},
           {prospect}]
     2: wishing with confidence of fulfillment [syn: {anticipation}]
     3: the feeling that something is about to happen
     4: the sum of the values of a random variable divided by the
        number of values [syn: {arithmetic mean}, {first moment},
        {expected value}]
