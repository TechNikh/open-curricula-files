---
version: 1
type: video
provider: YouTube
id: LiOfeSsjrB8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Tropomyosin%20and%20troponin%20and%20their%20role%20in%20regulating%20muscle%20contraction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8b388f60-6b10-4a6a-9fd5-63a74e1abfbf
updated: 1486069585
title: >
    Tropomyosin and troponin and their role in regulating muscle
    contraction
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/LiOfeSsjrB8/default.jpg
    - https://i3.ytimg.com/vi/LiOfeSsjrB8/1.jpg
    - https://i3.ytimg.com/vi/LiOfeSsjrB8/2.jpg
    - https://i3.ytimg.com/vi/LiOfeSsjrB8/3.jpg
---
Tropomyosin and troponin and their role in regulating muscle contraction.  How calcium ion concentration dictates whether a muscle is contracting or not.
More free lessons at: http://www.khanacademy.org/video?v=LiOfeSsjrB8
