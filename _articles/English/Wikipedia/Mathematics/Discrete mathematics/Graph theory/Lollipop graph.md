---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lollipop_graph
offline_file: ""
offline_thumbnail: ""
uuid: 4f1e08df-2d59-4814-848a-3a0c672c3dd7
updated: 1484308629
title: Lollipop graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/160px-Lollipop_graph.PNG
categories:
    - Graph theory
---
In the mathematical discipline of graph theory, the (m,n)-lollipop graph is a special type of graph consisting of a complete graph (clique) on m vertices and a path graph on n vertices, connected with a bridge. [1]
