---
version: 1
type: video
provider: YouTube
id: RWXXGHsb4Mk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e5321d32-54f6-40cc-a7d8-78793ef12e76
updated: 1486069637
title: Law of Refraction Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/RWXXGHsb4Mk/default.jpg
    - https://i3.ytimg.com/vi/RWXXGHsb4Mk/1.jpg
    - https://i3.ytimg.com/vi/RWXXGHsb4Mk/2.jpg
    - https://i3.ytimg.com/vi/RWXXGHsb4Mk/3.jpg
---
