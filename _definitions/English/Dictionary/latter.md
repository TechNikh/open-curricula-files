---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/latter
offline_file: ""
offline_thumbnail: ""
uuid: d575b4c2-8fa6-43f3-8235-1c9ddb9d8d05
updated: 1484310180
title: latter
categories:
    - Dictionary
---
latter
     adj : referring to the second of two things or persons mentioned
           (or the last one or ones of several); "in the latter
           case" [syn: {latter(a)}] [ant: {former(a)}]
     n : the second of two or the second mentioned of two; "Tom and
         Dick were both heroes but only the latter is remembered
         today" [ant: {former}]
