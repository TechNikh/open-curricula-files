---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Notation_for_theoretic_scheduling_problems
offline_file: ""
offline_thumbnail: ""
uuid: aade130e-14f0-41d9-8ac4-c1de25e5762f
updated: 1484308675
title: Notation for theoretic scheduling problems
tags:
    - Machine environment
    - Single stage problems
    - Multi-stage problem
    - Job characteristics
    - Objective functions
    - Examples
categories:
    - Theoretical computer science
---
A convenient notation for theoretic scheduling problems was introduced by Ronald Graham, Eugene Lawler, Jan Karel Lenstra and Alexander Rinnooy Kan in.[1] It consists of three fields: α, β and γ.
