---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mealtime
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484569381
title: mealtime
categories:
    - Dictionary
---
mealtime
     n : the hour at which a meal is habitually or customarily eaten
