---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worthwhile
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484518561
title: worthwhile
categories:
    - Dictionary
---
worthwhile
     adj : sufficiently valuable to justify the investment of time or
           interest; "a worthwhile book"
