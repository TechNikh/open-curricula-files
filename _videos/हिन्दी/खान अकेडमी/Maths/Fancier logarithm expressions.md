---
version: 1
type: video
provider: YouTube
id: FcJS9oUtXhs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d9a6174c-1fa8-4120-861e-f80302202dbe
updated: 1486069616
title: Fancier logarithm expressions
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/FcJS9oUtXhs/default.jpg
    - https://i3.ytimg.com/vi/FcJS9oUtXhs/1.jpg
    - https://i3.ytimg.com/vi/FcJS9oUtXhs/2.jpg
    - https://i3.ytimg.com/vi/FcJS9oUtXhs/3.jpg
---
