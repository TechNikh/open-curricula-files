---
version: 1
type: video
provider: YouTube
id: SlibbtBMqTw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 25d4b096-d7c1-4616-bceb-30a3c70e8f7a
updated: 1486069607
title: 'Area of Koch snowflake part 2  advanced'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/SlibbtBMqTw/default.jpg
    - https://i3.ytimg.com/vi/SlibbtBMqTw/1.jpg
    - https://i3.ytimg.com/vi/SlibbtBMqTw/2.jpg
    - https://i3.ytimg.com/vi/SlibbtBMqTw/3.jpg
---
