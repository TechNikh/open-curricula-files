---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/old
offline_file: ""
offline_thumbnail: ""
uuid: 977a9fac-12ab-43da-be5d-e27f15e65034
updated: 1484310293
title: old
categories:
    - Dictionary
---
old
     adj 1: (used especially of persons) having lived for a relatively
            long time or attained a specific age; especially not
            young; often used as a combining form to indicate an
            age as specified as in `a week-old baby'; "an old
            man's eagle mind"--William Butler Yeats; "his mother
            is very old"; "a ripe old age"; "how old are you?"
            [ant: {young}]
     2: of long duration; not new; "old tradition"; "old house";
        "old wine"; "old country"; "old friendships"; "old money"
        [ant: {new}]
     3: of an earlier time; "his old classmates"
     4: (used for emphasis) very familiar; "good old boy"; "same old
        ...
