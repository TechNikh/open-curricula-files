---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/detail
offline_file: ""
offline_thumbnail: ""
uuid: edc04b59-bbc4-4521-aa81-d481aba77f50
updated: 1484310361
title: detail
categories:
    - Dictionary
---
detail
     n 1: an isolated fact that is considered separately from the
          whole; "several of the details are similar"; "a point of
          information" [syn: {item}, {point}]
     2: a small part that can be considered separately from the
        whole; "it was perfect in all details" [syn: {particular},
         {item}]
     3: extended treatment of particulars; "the essay contained too
        much detail"
     4: a crew of workers selected for a particular task; "a detail
        was sent to remove the fallen trees"
     5: a temporary military unit; "the peace-keeping force includes
        one British contingent" [syn: {contingent}]
     v 1: provide details for
     2: ...
