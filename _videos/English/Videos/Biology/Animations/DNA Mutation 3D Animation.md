---
version: 1
type: video
provider: YouTube
id: 9bWjuwTiYXI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/DNA%20Mutation%203D%20Animation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5c6fa13e-594f-4e32-adad-4f1b9157cfeb
updated: 1486069670
title: DNA Mutation 3D Animation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/9bWjuwTiYXI/default.jpg
    - https://i3.ytimg.com/vi/9bWjuwTiYXI/1.jpg
    - https://i3.ytimg.com/vi/9bWjuwTiYXI/2.jpg
    - https://i3.ytimg.com/vi/9bWjuwTiYXI/3.jpg
---
