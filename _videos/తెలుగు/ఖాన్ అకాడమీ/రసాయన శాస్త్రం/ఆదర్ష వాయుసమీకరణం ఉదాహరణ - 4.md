---
version: 1
type: video
provider: YouTube
id: 1HVOzkJcHv0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4ac3ffac-7f86-4c28-b016-04743f523a61
updated: 1486069603
title: 'ఆదర్ష వాయుసమీకరణం ఉదాహరణ - 4'
tags:
    - telugu
    - chemistry
    - Ideal gas
    - Equation
categories:
    - రసాయన శాస్త్రం
thumbnail_urls:
    - https://i3.ytimg.com/vi/1HVOzkJcHv0/default.jpg
    - https://i3.ytimg.com/vi/1HVOzkJcHv0/1.jpg
    - https://i3.ytimg.com/vi/1HVOzkJcHv0/2.jpg
    - https://i3.ytimg.com/vi/1HVOzkJcHv0/3.jpg
---
Translated from original video
Title - Ideal Gas Example 4
URL -http://www.khanacademy.org/science/chemistry/ideal-gas-laws/v/ideal-gas-example-4
