---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fodor%27s_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 1ee47606-4c26-4c97-a087-e0ae928a0b0e
updated: 1484309504
title: "Fodor's lemma"
categories:
    - Set theory
---
If 
  
    
      
        κ
      
    
    {\displaystyle \kappa }
  
 is a regular, uncountable cardinal, 
  
    
      
        S
      
    
    {\displaystyle S}
  
 is a stationary subset of 
  
    
      
        κ
      
    
    {\displaystyle \kappa }
  
, and 
  
    
      
        f
        :
        S
        →
        κ
      
    
    {\displaystyle f:S\rightarrow \kappa }
  
 is regressive (that is, 
  
    
      
        f
        (
        α
        )
        <
        α
      
    
    {\displaystyle f(\alpha )<\alpha }
  
 for any 
  
    
      
        α
        ∈
        S
      
    
    {\displaystyle \alpha \in S}
  
, 
  
    
      
        α
     ...
