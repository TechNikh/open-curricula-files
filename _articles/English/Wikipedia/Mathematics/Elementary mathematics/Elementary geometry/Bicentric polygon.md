---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bicentric_polygon
offline_file: ""
offline_thumbnail: ""
uuid: 1e3525e3-8b40-46e2-8910-760620b70370
updated: 1484308930
title: Bicentric polygon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Equilateral_triangle_bicentric_001.svg.png
tags:
    - Triangles
    - Bicentric quadrilaterals
    - 'Polygons with n > 4'
    - Regular polygons
    - "Poncelet's porism"
categories:
    - Elementary geometry
---
In geometry, a bicentric polygon is a tangential polygon (a polygon all of whose sides are tangent to an inner incircle) which is also cyclic — that is, inscribed in an outer circle that passes through each vertex of the polygon. All triangles and all regular polygons are bicentric. On the other hand, a rectangle with unequal sides is not bicentric, because no circle can be tangent to all four sides.
