---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Dykstra%27s_projection_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 717d9f31-0faa-4015-bc40-862c89a7483d
updated: 1484309192
title: "Dykstra's projection algorithm"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Dykstra_algorithm.svg.png
categories:
    - Convex geometry
---
Dykstra's algorithm is a method that computes a point in the intersection of convex sets, and is a variant of the alternating projection method (also called the projections onto convex sets method). In its simplest form, the method finds a point in the intersection of two convex sets by iteratively projecting onto each of the convex set; it differs from the alternating projection method in that there are intermediate steps. A parallel version of the algorithm was developed by Gaffke and Mathar.
