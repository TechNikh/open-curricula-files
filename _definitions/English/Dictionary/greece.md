---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greece
offline_file: ""
offline_thumbnail: ""
uuid: 5a0e6988-92a1-48e2-9b8b-84217b83bfd1
updated: 1484310555
title: greece
categories:
    - Dictionary
---
Greece
     n : a republic in southeastern Europe on the southern part of
         the Balkan peninsula; known for grapes and olives and
         olive oil [syn: {Hellenic Republic}, {Ellas}]
