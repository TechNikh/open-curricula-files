---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prose
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484521441
title: prose
categories:
    - Dictionary
---
prose
     n 1: ordinary writing as distinguished from verse
     2: matter of fact, commonplace, or dull expression
