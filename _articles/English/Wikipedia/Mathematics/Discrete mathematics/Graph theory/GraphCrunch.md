---
version: 1
type: article
id: https://en.wikipedia.org/wiki/GraphCrunch
offline_file: ""
offline_thumbnail: ""
uuid: 6fdf04e9-7743-495c-8354-7be4b5b507f3
updated: 1484308625
title: GraphCrunch
tags:
    - Motivation
    - features
    - Network models supported by GraphCrunch
    - Network properties supported by GraphCrunch
    - Installation and usage
    - Applications
categories:
    - Graph theory
---
GraphCrunch is a comprehensive, parallelizable, and easily extendible open source software tool for analyzing and modeling large biological networks (or graphs); it compares real-world networks against a series of random graph models with respect to a multitude of local and global network properties.[1] It is available at http://bio-nets.doc.ic.ac.uk/graphcrunch2/.
