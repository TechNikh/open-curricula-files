---
version: 1
type: video
provider: YouTube
id: MjWbO4pOWpU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4567e37c-b3a9-45ee-b64d-5a28c64e5ef4
updated: 1486069621
title: Percentage Shortcut Tricks Part -1
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/MjWbO4pOWpU/default.jpg
    - https://i3.ytimg.com/vi/MjWbO4pOWpU/1.jpg
    - https://i3.ytimg.com/vi/MjWbO4pOWpU/2.jpg
    - https://i3.ytimg.com/vi/MjWbO4pOWpU/3.jpg
---
