---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forcible
offline_file: ""
offline_thumbnail: ""
uuid: 060f4e6f-83f1-475c-b310-cabf47ddf004
updated: 1484310189
title: forcible
categories:
    - Dictionary
---
forcible
     adj : impelled by physical force especially against resistance;
           "forcible entry"; "a real cop would get physical";
           "strong-arm tactics" [syn: {physical}, {strong-arm}]
