---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/User:Varkora/sandbox/Recursive_best-first_search
offline_file: ""
offline_thumbnail: ""
uuid: c2981c0f-231c-48e2-a238-18130b7317e2
updated: 1484309006
title: User:Varkora/sandbox/Recursive best-first search
categories:
    - Game artificial intelligence
---
Recursive best-first search is a graph traversal and path search algorithm that can find the shortest path between a designated start node and any member of a set of goal nodes in a weighted graph.[1]
