---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conley_index_theory
offline_file: ""
offline_thumbnail: ""
uuid: b0c08f8b-4e78-4f16-a9c8-1fafb2eaa663
updated: 1484308786
title: Conley index theory
categories:
    - Fixed points (mathematics)
---
In dynamical systems theory, Conley index theory, named after Charles Conley, analyzes topological structure of invariant sets of diffeomorphisms and of smooth flows. It is a far-reaching generalization of the Hopf index theorem that predicts existence of fixed points of a flow inside a planar region in terms of information about its behavior on the boundary. Conley's theory is related to Morse theory, which describes the topological structure of a closed manifold by means of a nondegenerate gradient vector field. It has an enormous range of applications to the study of dynamics, including existence of periodic orbits in Hamiltonian systems and travelling wave solutions for partial ...
