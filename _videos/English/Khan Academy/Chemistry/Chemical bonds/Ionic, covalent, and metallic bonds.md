---
version: 1
type: video
provider: YouTube
id: CGA8sRwqIFg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Chemical%20bonds/Ionic%2C%20covalent%2C%20and%20metallic%20bonds.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f46b38e9-d273-4947-a289-271c770a673a
updated: 1486069581
title: Ionic, covalent, and metallic bonds
tags:
    - dotsub
categories:
    - Chemical bonds
thumbnail_urls:
    - https://i3.ytimg.com/vi/CGA8sRwqIFg/default.jpg
    - https://i3.ytimg.com/vi/CGA8sRwqIFg/1.jpg
    - https://i3.ytimg.com/vi/CGA8sRwqIFg/2.jpg
    - https://i3.ytimg.com/vi/CGA8sRwqIFg/3.jpg
---
Introduction to ionic, covalent, polar covalent and metallic bonds.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/chemical-bonds/types-chemical-bonds/v/electronegativity-trends?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/periodic-table/periodic-table-trends-bonding/v/metallic-nature-trends?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
