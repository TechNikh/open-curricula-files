---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Point_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 7ece68b7-84c0-4552-bd5f-3ba3718d9b7c
updated: 1484308941
title: Point (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-ACP_3.svg.png
tags:
    - Points in Euclidean geometry
    - Dimension of a point
    - Vector space dimension
    - Topological dimension
    - Hausdorff dimension
    - Geometry without points
    - Point masses and the Dirac delta function
categories:
    - Elementary geometry
---
More specifically, in Euclidean geometry, a point is a primitive notion upon which the geometry is built. Being a primitive notion means that a point cannot be defined in terms of previously defined objects. That is, a point is defined only by some properties, called axioms, that it must satisfy. In particular, the geometric points do not have any length, area, volume, or any other dimensional attribute. A common interpretation is that the concept of a point is meant to capture the notion of a unique location in Euclidean space.
