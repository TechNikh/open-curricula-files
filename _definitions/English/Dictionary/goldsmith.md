---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/goldsmith
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484367361
title: goldsmith
categories:
    - Dictionary
---
goldsmith
     n 1: an artisan who makes jewelry and other objects out of gold
          [syn: {goldworker}, {gold-worker}]
     2: Irish writer of novels and poetry and plays and essays
        (1728-1774) [syn: {Oliver Goldsmith}]
