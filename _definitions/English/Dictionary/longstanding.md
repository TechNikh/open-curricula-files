---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/longstanding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484516161
title: longstanding
categories:
    - Dictionary
---
longstanding
     adj : having existed for a long time; "a longstanding friendship";
           "the longstanding conflict"
