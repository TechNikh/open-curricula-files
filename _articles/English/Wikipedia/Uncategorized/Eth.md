---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eth
offline_file: ""
offline_thumbnail: ""
uuid: 26d06f33-f1ec-4f95-a44a-80ee7320eac3
updated: 1484308398
title: Eth
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Latin_letter_%25C3%2590.svg.png'
tags:
    - Computer input
    - Miscellaneous
    - Notes and references
    - Bibliography
categories:
    - Uncategorized
---
Eth (/ɛð/, uppercase: Ð, lowercase: ð; also spelled edh or eð) is a letter used in Old English, Middle English, Icelandic, Faroese (in which it is called edd), and Elfdalian. It was also used in Scandinavia during the Middle Ages but was subsequently replaced with dh and later d. It is often transliterated as d (and d- is rarely used as a mnemonic).[1] The lowercase version has been adopted to represent a voiced dental fricative in the International Phonetic Alphabet.
