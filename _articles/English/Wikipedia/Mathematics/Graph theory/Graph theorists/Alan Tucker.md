---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alan_Tucker
offline_file: ""
offline_thumbnail: ""
uuid: 527e77b9-7270-42f5-97c2-e8f99e58eeda
updated: 1484309476
title: Alan Tucker
tags:
    - Education and career
    - Awards and honors
    - Selected publications
categories:
    - Graph theorists
---
Alan Curtiss Tucker is an American mathematician. He is a professor of applied mathematics at Stony Brook University, and the author of a widely used textbook on combinatorics;[1][2] he has also made research contributions to graph theory and coding theory.
