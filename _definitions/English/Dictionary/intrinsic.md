---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intrinsic
offline_file: ""
offline_thumbnail: ""
uuid: dd0b27c5-6591-49c5-9dc3-e18d63bfd94c
updated: 1484310395
title: intrinsic
categories:
    - Dictionary
---
intrinsic
     adj 1: belonging to a thing by its very nature; "form was treated
            as something intrinsic, as the very essence of the
            thing"- John Dewey [syn: {intrinsical}] [ant: {extrinsic}]
     2: situated within or belonging solely to the organ or body
        part on which it acts; "intrinsic muscles"
