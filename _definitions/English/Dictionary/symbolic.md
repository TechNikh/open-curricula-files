---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/symbolic
offline_file: ""
offline_thumbnail: ""
uuid: 01b8a278-6a7d-46a3-ae1c-c8c5bd8d4e91
updated: 1484310371
title: symbolic
categories:
    - Dictionary
---
symbolic
     adj 1: relating to or using or proceeding by means of symbols;
            "symbolic logic"; "symbolic operations"; "symbolic
            thinking" [syn: {symbolical}]
     2: serving as a visible symbol for something abstract; "a crown
        is emblematic of royalty"; "the spinning wheel was as
        symbolic of colonical Massachusetts as the codfish" [syn:
        {emblematic}, {emblematical}, {symbolical}]
     3: using symbolism; "symbolic art"
