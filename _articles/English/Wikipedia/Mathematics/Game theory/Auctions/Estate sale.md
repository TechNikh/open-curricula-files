---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Estate_sale
offline_file: ""
offline_thumbnail: ""
uuid: 6f91ccd7-b320-4c03-b9b8-d2cc9983976e
updated: 1484309013
title: Estate sale
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/lossy-page1-220px-Estate-sale-newspaper-ad-USA-1918.tif.jpg
tags:
    - Reasons for an estate sale
    - Conduct
    - Admittance procedure
    - Street numbers
    - Sign-up Sheet
categories:
    - Auctions
---
An estate sale or estate liquidation is a sale or auction to dispose of a substantial portion of the materials owned by a person who is recently deceased or who must dispose of his or her personal property to facilitate a move.[1]
