---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flies
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484352301
title: flies
categories:
    - Dictionary
---
flies
     n : (theater) the space over the stage (out of view of the
         audience) used to store scenery (drop curtains)
