---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rag
offline_file: ""
offline_thumbnail: ""
uuid: 8a925e33-0c5c-4224-9e97-6d55072a4c6f
updated: 1484310459
title: rag
categories:
    - Dictionary
---
rag
     n 1: a small piece of cloth or paper [syn: {shred}, {tag}, {tag
          end}, {tatter}]
     2: a week at British universities during which side-shows and
        processions of floats are organized to raise money for
        charities [syn: {rag week}]
     3: music with a syncopated melody (usually for the piano) [syn:
         {ragtime}]
     4: newspaper with half-size pages [syn: {tabloid}, {sheet}]
     5: a boisterous practical joke (especially by college students)
     v 1: treat cruelly; "The children tormented the stuttering
          teacher" [syn: {torment}, {bedevil}, {crucify}, {dun}, {frustrate}]
     2: cause annoyance in; disturb, especially by minor
        ...
