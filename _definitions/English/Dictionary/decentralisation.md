---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decentralisation
offline_file: ""
offline_thumbnail: ""
uuid: 4dc934d3-f328-4cb9-a954-216147739786
updated: 1484310189
title: decentralisation
categories:
    - Dictionary
---
decentralisation
     n : the spread of power away from the center to local branches
         or governments [syn: {decentralization}] [ant: {centralization}]
