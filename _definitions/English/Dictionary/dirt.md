---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dirt
offline_file: ""
offline_thumbnail: ""
uuid: edb4a4ac-c1e9-411b-9f9f-b5eb897b49f2
updated: 1484310426
title: dirt
categories:
    - Dictionary
---
dirt
     adj : (of roads) not leveled or drained; unsuitable for all year
           travel [syn: {ungraded}]
     n 1: the part of the earth's surface consisting of humus and
          disintegrated rock [syn: {soil}]
     2: the state of being covered with unclean things [syn: {filth},
         {grime}, {soil}, {stain}, {grease}, {grunge}]
     3: obscene terms for feces [syn: {crap}, {shit}, {shite}, {poop},
         {turd}]
     4: disgraceful gossip about the private lives of other people
        [syn: {scandal}, {malicious gossip}]
