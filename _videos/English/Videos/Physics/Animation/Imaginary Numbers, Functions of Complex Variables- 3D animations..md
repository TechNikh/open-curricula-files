---
version: 1
type: video
provider: YouTube
id: bIY6ahHVgqA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3b0126ec-5827-4b7c-a981-a69b9bf02d0e
updated: 1486069654
title: 'Imaginary Numbers, Functions of Complex Variables- 3D animations.'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/bIY6ahHVgqA/default.jpg
    - https://i3.ytimg.com/vi/bIY6ahHVgqA/1.jpg
    - https://i3.ytimg.com/vi/bIY6ahHVgqA/2.jpg
    - https://i3.ytimg.com/vi/bIY6ahHVgqA/3.jpg
---
