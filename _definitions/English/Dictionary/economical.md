---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/economical
offline_file: ""
offline_thumbnail: ""
uuid: 571d21d5-cd81-460c-9cfa-48841c418f8f
updated: 1484310405
title: economical
categories:
    - Dictionary
---
economical
     adj 1: using the minimum of time or resources necessary for
            effectiveness; "an economic use of home heating oil";
            "a modern economical heating system"; "an economical
            use of her time" [syn: {economic}]
     2: of or relating to an economy, the system of production and
        management of material wealth; "economic growth"; "aspects
        of social, political, and economical life" [syn: {economic}]
     3: avoiding waste; "an economical meal"; "an economical
        shopper"; "a frugal farmer"; "a frugal lunch"; "a sparing
        father and a spending son"; "sparing in their use of heat
        and light"; "stinting in bestowing ...
