---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_spherical_symmetry_groups
offline_file: ""
offline_thumbnail: ""
uuid: e82d2628-641f-49e5-828d-d67b5cc38bb8
updated: 1484309313
title: List of spherical symmetry groups
tags:
    - Involutional symmetry
    - Cyclic symmetry
    - Dihedral symmetry
    - Polyhedral symmetry
    - Notes
categories:
    - Symmetry
---
Spherical symmetry groups are also called point groups in three dimensions; however, this article is limited to the finite symmetries. There are five fundamental symmetry classes which have triangular fundamental domains: dihedral, cyclic, tetrahedral, octahedral, and icosahedral symmetry.
