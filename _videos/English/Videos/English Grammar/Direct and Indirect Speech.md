---
version: 1
type: video
provider: YouTube
id: pGOYlbXbN0o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0b9bb191-153f-4bf6-aaaf-268201ab3f6a
updated: 1486069631
title: Direct and Indirect Speech
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/pGOYlbXbN0o/default.jpg
    - https://i3.ytimg.com/vi/pGOYlbXbN0o/1.jpg
    - https://i3.ytimg.com/vi/pGOYlbXbN0o/2.jpg
    - https://i3.ytimg.com/vi/pGOYlbXbN0o/3.jpg
---
