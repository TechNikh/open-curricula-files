---
version: 1
type: video
provider: YouTube
id: ZFQUxdR0iDg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a66d5de2-6b47-426c-937f-766fe53c83dc
updated: 1486069616
title: Skip counting by 100 example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZFQUxdR0iDg/default.jpg
    - https://i3.ytimg.com/vi/ZFQUxdR0iDg/1.jpg
    - https://i3.ytimg.com/vi/ZFQUxdR0iDg/2.jpg
    - https://i3.ytimg.com/vi/ZFQUxdR0iDg/3.jpg
---
