---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soon
offline_file: ""
offline_thumbnail: ""
uuid: a49dd4e7-1680-4183-abed-a7ad851a8754
updated: 1484310435
title: soon
categories:
    - Dictionary
---
soon
     adv : in the near future; "the doctor will soon be here"; "the
           book will appear shortly"; "she will arrive presently";
           "we should have news before long" [syn: {shortly}, {presently},
            {before long}]
