---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disturbed
offline_file: ""
offline_thumbnail: ""
uuid: 060cb32b-c623-43d7-bfe1-3cf1089317d2
updated: 1484310200
title: disturbed
categories:
    - Dictionary
---
disturbed
     adj 1: having the place or position changed; "the disturbed books
            and papers on her desk"; "disturbed grass showed where
            the horse had passed"
     2: afflicted with or marked by anxious uneasiness or trouble or
        grief; "too upset to say anything"; "spent many disquieted
        moments"; "distressed about her son's leaving home";
        "lapsed into disturbed sleep"; "worried parents"; "a
        worried frown"; "one last worried check of the sleeping
        children" [syn: {disquieted}, {distressed}, {upset}, {worried}]
     3: emotionally unstable and having difficulty coping with
        personal relationships [syn: {maladjusted}]
     4: ...
