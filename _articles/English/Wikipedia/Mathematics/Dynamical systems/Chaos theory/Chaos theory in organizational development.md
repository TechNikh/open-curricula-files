---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Chaos_theory_in_organizational_development
offline_file: ""
offline_thumbnail: ""
uuid: 758dd4a4-4663-4a26-9b11-94972ec0c77d
updated: 1484308696
title: Chaos theory in organizational development
categories:
    - Chaos theory
---
In organizational development, chaos theory is a subset of more general chaos theory that incorporates principles of quantum mechanics and presents them in a complex systems environment. To the observer the systems seem to be in chaos. Organizational Development of a business system is the management of that apparent chaos. The term “Managing Organized Chaos” is used in the book by the same name Managing Organized Chaos – Business Planning 1.0, to describe the daily management of an entity (business) engaged in continual activity.
