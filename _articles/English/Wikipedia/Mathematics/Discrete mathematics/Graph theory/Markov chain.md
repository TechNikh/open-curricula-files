---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Markov_chain
offline_file: ""
offline_thumbnail: ""
uuid: 6d82ada0-9ba3-41d8-8422-0e5b4fa1ba4d
updated: 1484308629
title: Markov chain
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Markovkate_01.svg.png
tags:
    - Introduction
    - Examples
    - Gambling
    - A birth-death process
    - A non-Markov example
    - Markov property
    - The general case
    - For discrete-time Markov chains
    - Formal definition
    - Discrete-time Markov chain
    - Variations
    - Example
    - Continuous-time Markov chain
    - Infinitesimal definition
    - Jump chain/holding time definition
    - Transition probability definition
    - Transient evolution
    - Properties
    - Reducibility
    - Periodicity
    - Transience and recurrence
    - Mean recurrence time
    - Expected number of visits
    - Absorbing states
    - Ergodicity
    - Steady-state analysis and limiting distributions
    - >
        Steady-state analysis and the time-inhomogeneous Markov
        chain
    - Finite state space
    - >
        Stationary distribution relation to eigenvectors and
        simplices
    - Time-homogeneous Markov chain with a finite state space
    - Convergence speed to the stationary distribution
    - Reversible Markov chain
    - Closest reversible Markov chain
    - Bernoulli scheme
    - General state space
    - Harris chains
    - Locally interacting Markov chains
    - Markovian representations
    - Transient behaviour
    - Stationary distribution
    - Example
    - Hitting times
    - Expected hitting times
    - Time reversal
    - Embedded Markov chain
    - Applications
    - Physics
    - chemistry
    - Testing
    - Speech recognition
    - Information sciences
    - Queueing theory
    - Internet applications
    - Statistics
    - Economics and finance
    - Social sciences
    - Mathematical biology
    - Genetics
    - Games
    - Music
    - Baseball
    - Markov text generators
    - Bioinformatics
    - Fitting
    - History
    - Notes
categories:
    - Graph theory
---
In probability theory and statistics, a Markov chain or Markoff chain, named after the Russian mathematician Andrey Markov, is a stochastic process that satisfies the Markov property (usually characterized as "memorylessness"). Loosely speaking, a process satisfies the Markov property if one can make predictions for the future of the process based solely on its present state just as well as one could knowing the process's full history. i.e., conditional on the present state of the system, its future and past are independent.[1]
