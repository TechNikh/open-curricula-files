---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fertilized
offline_file: ""
offline_thumbnail: ""
uuid: 21d8208b-cbd0-4c33-b571-51c78068dd72
updated: 1484310162
title: fertilized
categories:
    - Dictionary
---
fertilized
     adj : made pregnant [syn: {fertilised}, {impregnated}, {inseminated}]
