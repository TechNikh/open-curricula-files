---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ball
offline_file: ""
offline_thumbnail: ""
uuid: c6408f37-1704-4024-8f25-5badef0150a1
updated: 1484310210
title: ball
categories:
    - Dictionary
---
ball
     n 1: round object that is hit or thrown or kicked in games; "the
          ball travelled 90 mph on his serve"; "the mayor threw
          out the first ball"; "the ball rolled into the corner
          pocket"
     2: a solid ball shot by a musket; "they had to carry a ramrod
        as well as powder and ball" [syn: {musket ball}]
     3: an object with a spherical shape; "a ball of fire" [syn: {globe},
         {orb}]
     4: the people assembled at a lavish formal dance; "the ball was
        already emptying out before the fire alarm sounded"
     5: one of the two male reproductive glands that produce
        spermatozoa and secrete androgens; "she kicked him in the
        ...
