---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bargaining_power
offline_file: ""
offline_thumbnail: ""
uuid: 0894ab15-c04d-41db-a59f-9f99793d045a
updated: 1484308533
title: Bargaining power
tags:
    - Calculation
    - Example
    - Buying power
categories:
    - Cooperative games
---
Bargaining power is the relative ability of parties in a situation to exert influence over each other. If both parties are on an equal footing in a debate, then they will have equal bargaining power, such as in a perfectly competitive market, or between an evenly matched monopoly and monopsony.
