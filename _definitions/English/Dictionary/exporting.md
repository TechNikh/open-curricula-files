---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exporting
offline_file: ""
offline_thumbnail: ""
uuid: 4fe0e6d8-0f67-41d6-a3dd-84af8308988d
updated: 1484310515
title: exporting
categories:
    - Dictionary
---
exporting
     n : the commercial activity of selling and shipping goods to a
         foreign country [syn: {exportation}]
