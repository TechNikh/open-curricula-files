---
version: 1
type: video
provider: YouTube
id: yZungE0D9fA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e460c6ad-fb66-46dc-9b96-b1fb2767aaf2
updated: 1486069613
title: Total seats in a theater
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/yZungE0D9fA/default.jpg
    - https://i3.ytimg.com/vi/yZungE0D9fA/1.jpg
    - https://i3.ytimg.com/vi/yZungE0D9fA/2.jpg
    - https://i3.ytimg.com/vi/yZungE0D9fA/3.jpg
---
