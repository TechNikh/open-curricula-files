---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disruption
offline_file: ""
offline_thumbnail: ""
uuid: f414f260-ad3d-440c-bd30-65ed30163891
updated: 1484310202
title: disruption
categories:
    - Dictionary
---
disruption
     n 1: an act of delaying or interrupting the continuity; "it was
          presented without commercial breaks" [syn: {break}, {interruption},
           {gap}]
     2: a disorderly outburst or tumult; "they were amazed by the
        furious disturbance they had caused" [syn: {disturbance},
        {commotion}, {stir}, {flutter}, {hurly burly}, {to-do}, {hoo-ha},
         {hoo-hah}, {kerfuffle}]
     3: an event that results in a displacement or discontinuity
        [syn: {dislocation}]
     4: the act of causing disorder [syn: {perturbation}]
