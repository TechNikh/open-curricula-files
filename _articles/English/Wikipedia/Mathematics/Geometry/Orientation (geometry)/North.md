---
version: 1
type: article
id: https://en.wikipedia.org/wiki/North
offline_file: ""
offline_thumbnail: ""
uuid: 80641bfe-3592-493c-813a-c2dd5556fac0
updated: 1484309297
title: North
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Compass_Rose_English_North.svg.png
tags:
    - Etymology
    - Mapping
    - Magnetic north and declination
    - Roles of north as prime direction
    - Roles of east and west as inherently subsidiary directions
    - Cultural references
categories:
    - Orientation (geometry)
---
North is a noun, adjective, or adverb indicating direction or geography. North is one of the four cardinal directions or compass points. It is the opposite of south and is perpendicular to east and west.
