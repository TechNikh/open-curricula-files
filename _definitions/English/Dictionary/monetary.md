---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monetary
offline_file: ""
offline_thumbnail: ""
uuid: 15948cf2-3532-47de-837a-7910beae59c6
updated: 1484310453
title: monetary
categories:
    - Dictionary
---
monetary
     adj : relating to or involving money; "monetary rewards"; "he
           received thanks but no pecuniary compensation for his
           services" [syn: {pecuniary}]
