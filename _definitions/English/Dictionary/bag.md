---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bag
offline_file: ""
offline_thumbnail: ""
uuid: 6551771e-e4d7-4294-aa4c-d79e5c5de02d
updated: 1484310328
title: bag
categories:
    - Dictionary
---
bag
     n 1: a flexible container with a single opening; "he stuffed his
          laundry into a large bag"
     2: the quantity of game taken in a particular period (usually
        by one person); "his bag included two deer"
     3: place that runner must touch before scoring; "he scrambled
        to get back to the bag" [syn: {base}]
     4: a bag used for carrying money and small personal items or
        accessories (especially by women); "she reached into her
        bag and found a comb" [syn: {handbag}, {pocketbook}, {purse}]
     5: the quantity that a bag will hold; "he ate a large bag of
        popcorn" [syn: {bagful}]
     6: a portable rectangular traveling bag for carrying ...
