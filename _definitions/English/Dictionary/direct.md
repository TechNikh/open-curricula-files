---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/direct
offline_file: ""
offline_thumbnail: ""
uuid: c70b1c2c-2051-4387-9e78-d4d1078b6dfd
updated: 1484310311
title: direct
categories:
    - Dictionary
---
direct
     adj 1: direct in spatial dimensions; proceeding without deviation
            or interruption; straight and short; "a direct route";
            "a direct flight"; "a direct hit" [ant: {indirect}]
     2: immediate or direct in bearing or force; having nothing
        intervening; "in direct sunlight"; "in direct contact with
        the voters"; "direct exposure to the disease"; "a direct
        link"; "the direct cause of the accident"
     3: extended senses; direct in means or manner or behavior or
        language or action; "a direct question"; "a direct
        response"; "a direct approach" [ant: {indirect}]
     4: in a straight unbroken line of descent from parent to ...
