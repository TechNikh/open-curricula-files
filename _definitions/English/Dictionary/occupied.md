---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/occupied
offline_file: ""
offline_thumbnail: ""
uuid: 52d7ed27-7823-4af8-a90e-d258baaa8d90
updated: 1484310393
title: occupied
categories:
    - Dictionary
---
occupied
     adj 1: held or filled or in use; "she keeps her time well
            occupied"; "the wc is occupied" [ant: {unoccupied}]
     2: seized and controlled as by military invasion; "the occupied
        countries of Europe" [ant: {unoccupied}]
     3: resided in; having tenants; "not all the occupied (or
        tenanted) apartments were well kept up" [syn: {tenanted}]
     4: having ones attention or mind or energy engaged; "she keeps
        herself fully occupied with volunteer activities"; "deeply
        engaged in conversation" [syn: {engaged}]
