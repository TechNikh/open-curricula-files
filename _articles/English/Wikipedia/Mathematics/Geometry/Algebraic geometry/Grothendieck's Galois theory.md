---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Grothendieck%27s_Galois_theory'
offline_file: ""
offline_thumbnail: ""
uuid: 10f6c65f-9eb2-4d58-aa4a-a54e0e6bbbb9
updated: 1484309156
title: "Grothendieck's Galois theory"
categories:
    - Algebraic geometry
---
In mathematics, Grothendieck's Galois theory is a highly abstract approach to the Galois theory of fields, developed around 1960 to provide a way to study the fundamental group of algebraic topology in the setting of algebraic geometry. It provides, in the classical setting of field theory, an alternative perspective to that of Emil Artin based on linear algebra, which became standard from about the 1930s.
