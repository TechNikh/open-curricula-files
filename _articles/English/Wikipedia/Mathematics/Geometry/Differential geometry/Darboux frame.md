---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Darboux_frame
offline_file: ""
offline_thumbnail: ""
uuid: 64c2da6e-43b4-479e-bb45-27cc688f1961
updated: 1484309212
title: Darboux frame
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Darboux_trihedron.svg.png
tags:
    - Darboux frame of an embedded curve
    - Definition
    - Geodesic curvature, normal curvature, and relative torsion
    - Darboux frame on a surface
    - The trihedron
    - Change of frame
    - Structure equations
    - Principal curves
    - Moving frames
    - Frames on Euclidean space
    - Structure equations
    - Adapted frames and the Gauss–Codazzi equations
    - Notes
categories:
    - Differential geometry
---
In the differential geometry of surfaces, a Darboux frame is a natural moving frame constructed on a surface. It is the analog of the Frenet–Serret frame as applied to surface geometry. A Darboux frame exists at any non-umbilic point of a surface embedded in Euclidean space. It is named after French mathematician Jean Gaston Darboux.
