---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nitriles
offline_file: ""
offline_thumbnail: ""
uuid: 462ecbca-2553-4d5f-9c5c-e9eea8ee32a0
updated: 1484308403
title: Nitrile
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nitrile_Structural_Formulae_V.1_0.png
tags:
    - Structure and basic properties
    - History
    - Synthesis
    - Ammoxidation
    - Hydrocyanation
    - From organic halides and cyanide salts
    - Cyanohydrins
    - Dehydration of amides and oximes
    - Sandmeyer reaction
    - Other methods
    - Reactions
    - Hydrolysis
    - Reduction
    - Alkylation
    - Nucleophiles
    - Miscellaneous methods and compounds
    - Nitrile derivatives
    - Organic cyanamides
    - Nitrile oxides
    - Occurrence and applications
categories:
    - Uncategorized
---
A nitrile is any organic compound that has a −C≡N functional group.[1] The prefix cyano- is used interchangeably with the term nitrile in industrial literature. Nitriles are found in many useful compounds, including methyl cyanoacrylate, used in super glue, and nitrile rubber, a nitrile-containing polymer used in the latex-free laboratory and medical gloves. Nitrile rubber is also widely used as automotive and other seals since it is resistant to fuels and oils. Organic compounds containing multiple nitrile groups are known as cyanocarbons.
