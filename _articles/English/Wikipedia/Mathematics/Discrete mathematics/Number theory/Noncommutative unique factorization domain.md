---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Noncommutative_unique_factorization_domain
offline_file: ""
offline_thumbnail: ""
uuid: f89cc1fc-d40b-4c4f-8e0f-6827480b870a
updated: 1484308649
title: Noncommutative unique factorization domain
categories:
    - Number theory
---
In mathematics, the noncommutative unique factorization domain is the noncommutative counterpart of the commutative or classical unique factorization domain (UFD).
