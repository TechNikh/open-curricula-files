---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dwarf
offline_file: ""
offline_thumbnail: ""
uuid: 8292a7e8-54cc-410b-b665-e7e69ffae0d3
updated: 1484310303
title: dwarf
categories:
    - Dictionary
---
dwarf
     n 1: a person who is abnormally small [syn: {midget}, {nanus}]
     2: a legendary creature resembling a tiny old man; lives in the
        depths of the earth and guards buried treasure [syn: {gnome}]
     v 1: make appear small by comparison; "This year's debt dwarves
          that of last year" [syn: {shadow}, {overshadow}]
     2: check the growth of; "the lack of sunlight dwarfed these
        pines"
     [also: {dwarves} (pl)]
