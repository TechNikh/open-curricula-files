---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Incidence_poset
offline_file: ""
offline_thumbnail: ""
uuid: d9c80a70-9640-40d1-a41e-4a942b75605b
updated: 1484308622
title: Incidence poset
categories:
    - Graph theory
---
In mathematics, an incidence poset or incidence order is a type of partially ordered set that represents the incidence relation between vertices and edges of an undirected graph. The incidence poset of a graph G has an element for each vertex or edge in G; in this poset, there is an order relation x ≤ y if and only if either x = y or x is a vertex, y is an edge, and x is an endpoint of y.
