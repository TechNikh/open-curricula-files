---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethically
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484437741
title: ethically
categories:
    - Dictionary
---
ethically
     adv : in an ethical manner; from an ethical point of view;
           according to ethics; "he behaved ethically"; "this is
           ethically unacceptable" [ant: {unethically}]
