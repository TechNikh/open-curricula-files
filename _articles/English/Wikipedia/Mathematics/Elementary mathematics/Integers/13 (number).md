---
version: 1
type: article
id: https://en.wikipedia.org/wiki/13_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 77816ad6-d50c-4861-bf50-d3bda5b2962c
updated: 1484308948
title: 13 (number)
tags:
    - In mathematics
    - List of basic calculations
    - In languages
    - grammar
    - Spelling
    - In religion
    - Roman Catholicism
    - Sikhism
    - Judaism
    - Zoroastrianism
    - Shia
    - Wicca
    - Other
    - Lucky and unlucky
    - Unlucky 13
    - Lucky 13
    - Music
    - Other
    - Age 13
    - History
    - In sports
    - In TV and films
categories:
    - Integers
---
In spoken English, the numbers 13 and 30 are often confused. When carefully enunciated, they differ in which syllable is stressed: 13 i/θɜːrˈtiːn/ vs. 30 /ˈθɜːrti/. However, in dates such as 1300 ("thirteen hundred") or when contrasting numbers in the teens, such as 13, 14, 15, the stress shifts to the first syllable: 13 /ˈθɜːrtiːn/.
