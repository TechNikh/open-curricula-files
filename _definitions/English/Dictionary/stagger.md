---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stagger
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448301
title: stagger
categories:
    - Dictionary
---
stagger
     n : an unsteady uneven gait [syn: {lurch}, {stumble}]
     v 1: walk as if unable to control one's movements; "The drunken
          man staggered into the room" [syn: {reel}, {keel}, {lurch},
           {swag}, {careen}]
     2: walk with great difficulty; "He staggered along in the heavy
        snow" [syn: {flounder}]
     3: to arrange in a systematic order; "stagger the chairs in the
        lecture hall" [syn: {distribute}]
     4: astound or overwhelm, as with shock; "She was staggered with
        bills after she tried to rebuild her house following the
        earthquake"
