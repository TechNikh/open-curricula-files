---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/judiciary
offline_file: ""
offline_thumbnail: ""
uuid: 04efc6c9-479d-4483-a2f5-50a782f37c50
updated: 1484310535
title: judiciary
categories:
    - Dictionary
---
judiciary
     n 1: persons who administer justice [syn: {bench}]
     2: the system of law courts that administer justice and
        constitute the judicial branch of government [syn: {judicature},
         {judicatory}, {judicial system}]
