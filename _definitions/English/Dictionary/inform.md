---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inform
offline_file: ""
offline_thumbnail: ""
uuid: 383d00b0-3fa9-4a9d-a00f-099b9d16a522
updated: 1484310537
title: inform
categories:
    - Dictionary
---
inform
     v 1: impart knowledge of some fact, state or affairs, or event
          to; "I informed him of his rights"
     2: give character or essence to; "The principles that inform
        modern teaching"
     3: act as an informer; "She had informed on her own parents for
        years"
