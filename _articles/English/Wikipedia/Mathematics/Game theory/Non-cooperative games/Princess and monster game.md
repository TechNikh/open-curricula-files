---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Princess_and_monster_game
offline_file: ""
offline_thumbnail: ""
uuid: 13a0ef11-f6bb-4992-9852-fe5608013272
updated: 1484309057
title: Princess and monster game
categories:
    - Non-cooperative games
---
In game theory, a princess and monster game is a pursuit-evasion game played by two players in a region. The game was devised by Rufus Isaacs and published in his book Differential Games (1965) as follows:
