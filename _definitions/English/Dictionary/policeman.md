---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/policeman
offline_file: ""
offline_thumbnail: ""
uuid: d6907383-18b1-4531-90e4-0145f29e4e0c
updated: 1484310473
title: policeman
categories:
    - Dictionary
---
policeman
     n : a member of a police force; "it was an accident, officer"
         [syn: {police officer}, {officer}]
