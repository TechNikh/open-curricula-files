---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/name
offline_file: ""
offline_thumbnail: ""
uuid: ffb22c3c-ba6e-4249-95ed-2d622e156bea
updated: 1484310349
title: name
categories:
    - Dictionary
---
name
     n 1: a language unit by which a person or thing is known; "his
          name really is George Washington"; "those are two names
          for the same thing"
     2: by the sanction or authority of; "halt in the name of the
        law"
     3: a person's reputation; "he wanted to protect his good name"
     4: a well-known or notable person; "they studied all the great
        names in the history of France"; "she is an important
        figure in modern music" [syn: {figure}, {public figure}]
     5: family based on male descent; "he had no sons and there was
        no one to carry on his name" [syn: {gens}]
     6: a defamatory or abusive word or phrase; "sticks and stones
   ...
