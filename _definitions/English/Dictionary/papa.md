---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/papa
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495401
title: papa
categories:
    - Dictionary
---
papa
     n : an informal term for a father; probably derived from baby
         talk [syn: {dad}, {dada}, {daddy}, {pa}, {pappa}, {pater},
          {pop}]
