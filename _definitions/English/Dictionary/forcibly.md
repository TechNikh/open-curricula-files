---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forcibly
offline_file: ""
offline_thumbnail: ""
uuid: c43fdfe7-7b52-48e1-a30e-f83a8c6888e4
updated: 1484310183
title: forcibly
categories:
    - Dictionary
---
forcibly
     adv : in a forcible manner; "keep in mind the dangers of imposing
           our own values and prejudices too forcibly"
