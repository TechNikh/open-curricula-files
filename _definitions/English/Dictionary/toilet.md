---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/toilet
offline_file: ""
offline_thumbnail: ""
uuid: 9bfbbe2e-879c-4687-85eb-d7837e24fdf7
updated: 1484310559
title: toilet
categories:
    - Dictionary
---
toilet
     n 1: a room equipped with toilet facilities [syn: {lavatory}, {lav},
           {can}, {john}, {privy}, {bathroom}]
     2: a plumbing fixture for defecation and urination [syn: {can},
         {commode}, {crapper}, {pot}, {potty}, {stool}, {throne}]
     3: misfortune resulting in lost effort or money; "his career
        was in the gutter"; "all that work went down the sewer";
        "pensions are in the toilet" [syn: {gutter}, {sewer}]
     4: the act of dressing and preparing yourself; "he made his
        morning toilet and went to breakfast" [syn: {toilette}]
