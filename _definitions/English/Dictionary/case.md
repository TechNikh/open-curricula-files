---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/case
offline_file: ""
offline_thumbnail: ""
uuid: b98ab043-79bb-4d28-8ac2-4c29e8cdf3db
updated: 1484310297
title: case
categories:
    - Dictionary
---
case
     n 1: a comprehensive term for any proceeding in a court of law
          whereby an individual seeks a legal remedy; "the family
          brought suit against the landlord" [syn: {lawsuit}, {suit},
           {cause}, {causa}]
     2: an occurrence of something; "it was a case of bad judgment";
        "another instance occurred yesterday"; "but there is
        always the famous example of the Smiths" [syn: {instance},
         {example}]
     3: a special set of circumstances; "in that event, the first
        possibility is excluded"; "it may rain in which case the
        picnic will be canceled" [syn: {event}]
     4: a problem requiring investigation; "Perry Mason solved ...
