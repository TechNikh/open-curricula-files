---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantal_response_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 2ff49dc2-eede-4fa1-9e41-30da44078580
updated: 1484309029
title: Quantal response equilibrium
tags:
    - Application to data
    - Logit equilibrium
    - For dynamic games
    - Critiques
    - Free parameter
categories:
    - Game theory equilibrium concepts
---
Quantal response equilibrium (QRE) is a solution concept in game theory. First introduced by Richard McKelvey and Thomas Palfrey[1] [2] , it provides an equilibrium notion with bounded rationality. QRE is not an equilibrium refinement, and it can give significantly different results from Nash equilibrium. QRE is only defined for games with discrete strategies, although there are continuous-strategy analogues.
