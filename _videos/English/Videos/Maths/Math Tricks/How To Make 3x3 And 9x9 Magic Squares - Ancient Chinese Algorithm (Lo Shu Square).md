---
version: 1
type: video
provider: YouTube
id: QSmPrDSVLEU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cfe27875-3d6c-427f-b0a1-816e55d383eb
updated: 1486069622
title: 'How To Make 3x3 And 9x9 Magic Squares - Ancient Chinese Algorithm (Lo Shu Square)'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/QSmPrDSVLEU/default.jpg
    - https://i3.ytimg.com/vi/QSmPrDSVLEU/1.jpg
    - https://i3.ytimg.com/vi/QSmPrDSVLEU/2.jpg
    - https://i3.ytimg.com/vi/QSmPrDSVLEU/3.jpg
---
