---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nerve_endings
offline_file: ""
offline_thumbnail: ""
uuid: 18b8b658-927a-46c7-a975-1ee3f17780b8
updated: 1484308368
title: Nerve
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Nerves_of_the_left_upper_extremity.gif
tags:
    - Anatomy
    - Physiology
    - Clinical significance
    - Growth and stimulation
    - Other animals
categories:
    - Uncategorized
---
A nerve is an enclosed, cable-like bundle of axons (nerve fibers, the long and slender projections of neurons) in the peripheral nervous system. A nerve provides a common pathway for the electrochemical nerve impulses that are transmitted along each of the axons to peripheral organs.
