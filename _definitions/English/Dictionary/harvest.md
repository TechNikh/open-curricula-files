---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harvest
offline_file: ""
offline_thumbnail: ""
uuid: 57590c79-0c04-4098-bc72-3afcc67aad8c
updated: 1484310252
title: harvest
categories:
    - Dictionary
---
harvest
     n 1: the yield from plants in a single growing season [syn: {crop}]
     2: the consequence of an effort or activity; "they gathered a
        harvest of examples"; "a harvest of love"
     3: the gathering of a ripened crop [syn: {harvesting}, {harvest
        home}]
     4: the season for gathering crops [syn: {harvest time}]
     v 1: gather, as of natural products; "harvest the grapes" [syn: {reap},
           {glean}]
     2: remove from a culture or a living or dead body, as for the
        purposes of transplantation; "The Chinese are said to
        harvest organs from executed criminals"
