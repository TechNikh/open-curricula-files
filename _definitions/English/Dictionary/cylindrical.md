---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cylindrical
offline_file: ""
offline_thumbnail: ""
uuid: 00f1a023-a88d-4971-8c4f-7b677a9c017b
updated: 1484310232
title: cylindrical
categories:
    - Dictionary
---
cylindrical
     adj 1: having the form of a cylinder or tube [syn: {tube-shaped}, {tubelike},
             {vasiform}]
     2: related to or having the shape or properties of a cylinder
        [syn: {cylindric}]
