---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enlarge
offline_file: ""
offline_thumbnail: ""
uuid: a3b8ba30-243c-42a2-b02f-0dceaf448034
updated: 1484310216
title: enlarge
categories:
    - Dictionary
---
enlarge
     v 1: make larger; "She enlarged the flower beds"
     2: make large; "blow up an image" [syn: {blow up}, {magnify}]
        [ant: {reduce}]
     3: become larger or bigger
     4: add details, as to an account or idea; clarify the meaning
        of and discourse in a learned way, usually in writing;
        "She elaborated on the main ideas in her dissertation"
        [syn: {elaborate}, {lucubrate}, {expatiate}, {exposit}, {flesh
        out}, {expand}, {expound}, {dilate}] [ant: {abridge}]
