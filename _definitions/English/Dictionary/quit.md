---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quit
offline_file: ""
offline_thumbnail: ""
uuid: 19d8a243-254c-43c6-bf69-edd195e33aa2
updated: 1484310585
title: quit
categories:
    - Dictionary
---
quit
     v 1: put an end to a state or an activity; "Quit teasing your
          little brother" [syn: {discontinue}, {stop}, {cease}, {give
          up}, {lay off}] [ant: {continue}]
     2: give up or retire from a position; "The Secretary fo the
        Navy will leave office next month"; "The chairman resigned
        over the financial scandal" [syn: {leave office}, {step
        down}, {resign}] [ant: {take office}]
     3: go away or leave [syn: {depart}, {take leave}] [ant: {stay}]
     4: turn away from; give up; "I am foreswearing women forever"
        [syn: {foreswear}, {renounce}, {relinquish}]
     5: give up in the face of defeat of lacking hope; admit defeat;
        "In ...
