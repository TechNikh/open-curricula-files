---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/epithelium
offline_file: ""
offline_thumbnail: ""
uuid: 39cd34bb-069c-4a25-8834-c0846491af6d
updated: 1484310325
title: epithelium
categories:
    - Dictionary
---
epithelium
     n : membranous tissue covering internal organs and other
         internal surfaces of the body [syn: {epithelial tissue}]
     [also: {epithelia} (pl)]
