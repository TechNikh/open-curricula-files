---
version: 1
type: video
provider: YouTube
id: FyzTvl4IhAc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c5a43626-3674-4e1a-ace9-6df65813eed5
updated: 1486069640
title: 2 Tag Experiment (Optik vor dem Spiegel)
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/FyzTvl4IhAc/default.jpg
    - https://i3.ytimg.com/vi/FyzTvl4IhAc/1.jpg
    - https://i3.ytimg.com/vi/FyzTvl4IhAc/2.jpg
    - https://i3.ytimg.com/vi/FyzTvl4IhAc/3.jpg
---
