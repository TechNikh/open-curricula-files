---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insight
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484516701
title: insight
categories:
    - Dictionary
---
in sight
     adj : at or within a reasonable distance for seeing; "not a
           policeman in sight"; "kept the monkey in view" [syn: {in
           view}]
