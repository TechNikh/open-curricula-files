---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jewellary
offline_file: ""
offline_thumbnail: ""
uuid: d52c8a6d-41b0-4671-8e27-bd39c4e4e5ea
updated: 1484308389
title: Jewellery
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Amber.pendants.800pix.050203.jpg
tags:
    - Form and function
    - Materials and methods
    - Diamonds
    - Other gemstones
    - Metal finishes
    - Impact on society
    - History
    - Prehistory
    - Egypt
    - Europe and the Middle East
    - Mesopotamia
    - Greece
    - Rome
    - Middle Ages
    - Renaissance
    - Romanticism
    - 18th Century / Romanticism/ Renaissance
    - Art Nouveau
    - Art Deco
    - Asia
    - China
    - Indian subcontinent
    - North and South America
    - Native American
    - Pacific
    - Modern
    - Masonic
    - Body modification
    - Jewellery market
categories:
    - Uncategorized
---
Jewellery or jewelry[1] (/ˈdʒuːᵊlᵊri/) consists of small decorative items worn for personal adornment, such as brooches, rings, necklaces, earrings, and bracelets. Jewellery may be attached to the body or the clothes, and the term is restricted to durable ornaments, excluding flowers for example. For many centuries metal, often combined with gemstones, has been the normal material for jewellery, but other materials such as shells and other plant materials may be used. It is one of the oldest type of archaeological artefact – with 100,000-year-old beads made from Nassarius shells thought to be the oldest known jewellery.[2] The basic forms of jewellery vary between cultures but are ...
