---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weighted_network
offline_file: ""
offline_thumbnail: ""
uuid: 7f7f0cc5-5f47-45a2-a6e2-e229addfb500
updated: 1484309440
title: Weighted network
tags:
    - Measures for weighted networks
    - Software for analysing weighted networks
categories:
    - Network theory
---
A weighted network is a network where the ties among nodes have weights assigned to them. A network is a system whose elements are somehow connected (Wasserman and Faust, 1994).[1] The elements of a system are represented as nodes (also known as actors or vertices) and the connections among interacting elements are known as ties, edges, arcs, or links. The nodes might be neurons, individuals, groups, organisations, airports, or even countries, whereas ties can take the form of friendship, communication, collaboration, alliance, flow, or trade, to name a few.
