---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/graphic
offline_file: ""
offline_thumbnail: ""
uuid: 954c32a0-275e-4f96-adb0-ec010c8deaf2
updated: 1484310268
title: graphic
categories:
    - Dictionary
---
graphic
     adj 1: written or drawn or engraved; "graphic symbols" [syn: {graphical},
             {in writing(p)}]
     2: describing nudity or sexual activity in graphic detail;
        "graphic sexual scenes"
     3: of or relating to the graphic arts; "the etchings,
        drypoints, lithographis, and engravings which together
        form his graphic work"- Brit. Book News
     4: relating to or presented by a graph; "a graphic presentation
        of the data" [syn: {graphical}]
     5: evoking lifelike images within the mind; "pictorial poetry
        and prose"; "graphic accounts of battle"; "a lifelike
        portrait"; "a vivid description" [syn: {lifelike}, {pictorial},
       ...
