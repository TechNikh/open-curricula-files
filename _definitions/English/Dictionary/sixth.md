---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sixth
offline_file: ""
offline_thumbnail: ""
uuid: 5008f504-92cb-480e-b766-2d8a5cee2ca0
updated: 1484310399
title: sixth
categories:
    - Dictionary
---
sixth
     adj : coming next after the fifth and just before the seventh in
           position [syn: {6th}]
     n 1: position six in a countable series of things
     2: a sixth part [syn: {one-sixth}]
     3: the musical interval between one note and another six notes
        away from it
