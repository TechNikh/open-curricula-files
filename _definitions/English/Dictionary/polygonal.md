---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polygonal
offline_file: ""
offline_thumbnail: ""
uuid: 94f2b60c-21b0-4076-90ab-ec0a109c6c22
updated: 1484310271
title: polygonal
categories:
    - Dictionary
---
polygonal
     adj : having many sides or relating to a surface marked by
           polygons; "polygonal structure"
