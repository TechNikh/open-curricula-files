---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weakened
offline_file: ""
offline_thumbnail: ""
uuid: 16db5943-ad32-4e0a-9ea4-6f4e6b2d5fe3
updated: 1484310414
title: weakened
categories:
    - Dictionary
---
weakened
     adj 1: impaired by diminution [syn: {diminished}, {lessened}, {vitiated}]
     2: made weak or weaker
     3: reduced in strength; "the faded tones of an old recording"
        [syn: {attenuate}, {attenuated}, {faded}]
     4: mixed with water; "sold cut whiskey"; "a cup of thinned
        soup" [syn: {cut}, {thinned}]
     5: used of inanimate objects or their value [syn: {hurt}]
