---
version: 1
type: article
id: https://en.wikipedia.org/wiki/BIBO_stability
offline_file: ""
offline_thumbnail: ""
uuid: a095e4ac-1a0e-4ede-98d7-db8fcd2158bb
updated: 1484308825
title: BIBO stability
tags:
    - Time-domain condition for linear time-invariant systems
    - Continuous-time necessary and sufficient condition
    - Discrete-time sufficient condition
    - Proof of sufficiency
    - Frequency-domain condition for linear time-invariant systems
    - Continuous-time signals
    - Discrete-time signals
categories:
    - Stability theory
---
In signal processing, specifically control theory, bounded-input, bounded-output (BIBO) stability is a form of stability for linear signals and systems that take inputs. If a system is BIBO stable, then the output will be bounded for every input to the system that is bounded.
