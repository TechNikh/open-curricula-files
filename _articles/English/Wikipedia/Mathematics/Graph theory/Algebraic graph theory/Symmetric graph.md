---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_graph
offline_file: ""
offline_thumbnail: ""
uuid: bee2ee0c-b216-4119-9de1-5b41e6ea901a
updated: 1484309376
title: Symmetric graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Petersen1_tiny.svg.png
tags:
    - Examples
    - Properties
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory, a graph G is symmetric (or arc-transitive) if, given any two pairs of adjacent vertices u1—v1 and u2—v2 of G, there is an automorphism
