---
version: 1
type: video
provider: YouTube
id: 7V2_aMG3YsI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Tricks/Red%20Hot%20Cannonball%20in%20Water-Ice.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 00a3116b-1d94-4dd0-b3fd-b3bb6d0fb275
updated: 1486069661
title: Red Hot Cannonball in Water/Ice
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/7V2_aMG3YsI/default.jpg
    - https://i3.ytimg.com/vi/7V2_aMG3YsI/1.jpg
    - https://i3.ytimg.com/vi/7V2_aMG3YsI/2.jpg
    - https://i3.ytimg.com/vi/7V2_aMG3YsI/3.jpg
---
