---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electromagnetic_induction
offline_file: ""
offline_thumbnail: ""
uuid: bbe47dfc-13f3-4912-8ab7-ef82c801e55e
updated: 1484308754
title: Electromagnetic induction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Induction_experiment.png
tags:
    - History
    - Theory
    - "Faraday's law of induction and Lenz's law"
    - Maxwell–Faraday equation
    - "Faraday's law and relativity"
    - Applications
    - Electrical generator
    - Electrical transformer
    - Current clamp
    - Magnetic flow meter
    - Eddy currents
    - Electromagnet laminations
    - Parasitic induction within conductors
categories:
    - Electrodynamics
---
Electromagnetic or Magnetic induction is the production of an electromotive force (i.e., voltage) across an electrical conductor due to its dynamic interaction with a magnetic field.
