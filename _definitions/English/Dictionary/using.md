---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/using
offline_file: ""
offline_thumbnail: ""
uuid: 90a468a6-ebdb-49cb-80ac-03d732512363
updated: 1484310346
title: using
categories:
    - Dictionary
---
using
     n : an act that exploits or victimizes someone (treats them
         unfairly); "capitalistic exploitation of the working
         class"; "paying Blacks less and charging them more is a
         form of victimization" [syn: {exploitation}, {victimization},
          {victimisation}]
