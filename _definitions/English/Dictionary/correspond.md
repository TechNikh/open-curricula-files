---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/correspond
offline_file: ""
offline_thumbnail: ""
uuid: 89eb0415-3ca2-49ce-9aa7-0ff59cf98139
updated: 1484310391
title: correspond
categories:
    - Dictionary
---
correspond
     v 1: be compatible, similar or consistent; coincide in their
          characteristics; "The two stories don't agree in many
          details"; "The handwriting checks with the signature on
          the check"; "The suspect's fingerprints don't match
          those on the gun" [syn: {match}, {fit}, {check}, {jibe},
           {gibe}, {tally}, {agree}] [ant: {disagree}]
     2: be equivalent or parallel, in mathematics [syn: {equate}]
     3: exchange messages; "My Russian pen pal and I have been
        corresponding for several years"
     4: take the place of or be parallel or equivalent to; "Because
        of the sound changes in the course of history, an 'h' in
      ...
