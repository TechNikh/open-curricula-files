---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Neovius_surface
offline_file: ""
offline_thumbnail: ""
uuid: 27390abf-77c5-45e9-8cd2-9dbce2b19b77
updated: 1484309232
title: Neovius surface
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Neovius%2527_minimal_surface.png'
categories:
    - Differential geometry
---
In differential geometry, the Neovius surface is a triply periodic minimal surface originally discovered by Finnish mathematician Edvard Rudolf Neovius (the uncle of Rolf Nevanlinna).[1][2]
