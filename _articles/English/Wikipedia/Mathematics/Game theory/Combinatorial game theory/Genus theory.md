---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Genus_theory
offline_file: ""
offline_thumbnail: ""
uuid: e9b84244-667e-46d7-9669-6b244642bde6
updated: 1484308538
title: Genus theory
tags:
    - Genus of a game
    - Outcomes of sums of games
    - Reversible moves
    - Types of games
    - Nim
    - Tame
categories:
    - Combinatorial game theory
---
In the mathematical theory of games, genus theory in impartial games is a theory by which some games played under the misère play convention can be analysed, to predict the outcome class of games.
