---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interjection
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464561
title: interjection
categories:
    - Dictionary
---
interjection
     n 1: an abrupt emphatic exclamation expressing emotion [syn: {ejaculation}]
     2: the action of interjecting or interposing an action or
        remark that interrupts [syn: {interposition}, {interpolation},
         {interpellation}]
