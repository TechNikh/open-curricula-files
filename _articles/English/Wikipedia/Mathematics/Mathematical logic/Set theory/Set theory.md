---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Set_theory
offline_file: ""
offline_thumbnail: ""
uuid: def69782-8915-424c-bb95-18adc42c68e8
updated: 1484309499
title: Set theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/384px-Venn0001.svg.png
tags:
    - History
    - Basic concepts and notation
    - Some ontology
    - Axiomatic set theory
    - Applications
    - Areas of study
    - Combinatorial set theory
    - Descriptive set theory
    - Fuzzy set theory
    - Inner model theory
    - Large cardinals
    - Determinacy
    - Forcing
    - Cardinal invariants
    - Set-theoretic topology
    - Objections to set theory as a foundation for mathematics
    - Notes
categories:
    - Set theory
---
Set theory is a branch of mathematical logic that studies sets, which informally are collections of objects. Although any type of object can be collected into a set, set theory is applied most often to objects that are relevant to mathematics. The language of set theory can be used in the definitions of nearly all mathematical objects.
