---
version: 1
type: video
provider: YouTube
id: ye3rTjLCvAU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Correction%20to%20Sodium%20and%20Potassium%20Pump%20Video.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5519cc0b-0139-4706-aeb7-d13f7ec006e3
updated: 1486069579
title: Correction to Sodium and Potassium Pump Video
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/ye3rTjLCvAU/default.jpg
    - https://i3.ytimg.com/vi/ye3rTjLCvAU/1.jpg
    - https://i3.ytimg.com/vi/ye3rTjLCvAU/2.jpg
    - https://i3.ytimg.com/vi/ye3rTjLCvAU/3.jpg
---
Correction to Sodium and Potassium Pump Video
More free lessons at: http://www.khanacademy.org/video?v=ye3rTjLCvAU
