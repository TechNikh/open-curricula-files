---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obstruction
offline_file: ""
offline_thumbnail: ""
uuid: e057edf3-81a8-47af-b84f-5e5e9dad85ac
updated: 1484310200
title: obstruction
categories:
    - Dictionary
---
obstruction
     n 1: any structure that makes progress difficult [syn: {obstructor},
           {obstructer}, {impediment}, {impedimenta}]
     2: the state or condition of being obstructed [syn: {blockage}]
     3: something immaterial that stands in the way and must be
        circumvented or surmounted; "lack of immagination is an
        obstacle to one's advancement"; "the poverty of a district
        is an obstacle to good education"; "the filibuster was a
        major obstruction to the success of their plan" [syn: {obstacle}]
     4: the act of obstructing; "obstruction of justice"
     5: getting in someone's way
