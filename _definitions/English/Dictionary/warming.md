---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/warming
offline_file: ""
offline_thumbnail: ""
uuid: 952422dd-d756-4764-80d2-72a8d09bc87c
updated: 1484310246
title: warming
categories:
    - Dictionary
---
warming
     adj 1: imparting heat; "a warming fire"
     2: producing the sensation of heat when applied to the body; "a
        mustard plaster is calefacient" [syn: {calefacient}]
     n 1: the process of becoming warmer; a rising temperature [syn: {heating}]
     2: warm weather following a freeze; snow and ice melt; "they
        welcomed the spring thaw" [syn: {thaw}, {thawing}]
