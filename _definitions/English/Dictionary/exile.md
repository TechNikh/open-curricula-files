---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exile
offline_file: ""
offline_thumbnail: ""
uuid: f662ae1c-a4c9-4720-aa1a-216044da34ba
updated: 1484310176
title: exile
categories:
    - Dictionary
---
exile
     n 1: voluntarily absent from home or country [syn: {expatriate}]
     2: expelled from home or country by authority [syn: {deportee}]
     3: the act of expelling a person from their native land; "men
        in exile dream of hope"; "his deportation to a penal
        colony"; "the expatriation of wealthy farmers"; "the
        sentence was one of transportation for life" [syn: {deportation},
         {expatriation}, {transportation}]
     v : expel from a country; "The poet was exiled because he signed
         a letter protesting the government's actions" [syn: {expatriate},
          {deport}] [ant: {repatriate}]
