---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paying
offline_file: ""
offline_thumbnail: ""
uuid: 3ef24bd1-119b-4cfb-897c-2ac7cfa95381
updated: 1484310459
title: paying
categories:
    - Dictionary
---
paying
     adj 1: yielding a fair profit [syn: {gainful}, {paid}]
     2: for which money is paid; "a paying job"; "remunerative
        work"; "salaried employment"; "stipendiary services" [syn:
         {compensable}, {paying(a)}, {remunerative}, {salaried}, {stipendiary}]
