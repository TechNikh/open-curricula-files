---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eligible
offline_file: ""
offline_thumbnail: ""
uuid: 6671d960-3481-45f5-a3ec-b129a47b8d6a
updated: 1484310144
title: eligible
categories:
    - Dictionary
---
eligible
     adj 1: qualified for or allowed or worthy of being chosen;
            "eligible to run for office"; "eligible for retirement
            benefits"; "an eligible bachelor" [ant: {ineligible}]
     2: prohibited by official rules; "an eligible pass receiver"
