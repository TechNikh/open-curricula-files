---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suggest
offline_file: ""
offline_thumbnail: ""
uuid: 8c69411b-92fd-45fb-ad7b-1b3aacb9a44b
updated: 1484310357
title: suggest
categories:
    - Dictionary
---
suggest
     v 1: make a proposal, declare a plan for something [syn: {propose},
           {advise}]
     2: imply as a possibility; "The evidence suggests a need for
        more clarification" [syn: {intimate}]
     3: drop a hint; intimate by a hint [syn: {hint}]
     4: suggest the necessity of an intervention; in medicine;
        "Tetracycline is indicated in such cases" [syn: {indicate}]
        [ant: {contraindicate}]
     5: call to mind or evoke [syn: {evoke}, {paint a picture}]
