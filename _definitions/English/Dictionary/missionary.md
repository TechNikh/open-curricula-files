---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/missionary
offline_file: ""
offline_thumbnail: ""
uuid: 396c79a7-8cab-461f-b056-d7cdb0f82169
updated: 1484310569
title: missionary
categories:
    - Dictionary
---
missionary
     n 1: someone who attempts to convert others to a particular
          doctrine or program
     2: someone sent on a mission--especially a religious or
        charitable mission to a foreign country [syn: {missioner}]
