---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/usage
offline_file: ""
offline_thumbnail: ""
uuid: d93f147e-c632-45af-b02c-c62566f862e0
updated: 1484310254
title: usage
categories:
    - Dictionary
---
usage
     n 1: the act of using; "he warned against the use of narcotic
          drugs"; "skilled in the utilization of computers" [syn:
          {use}, {utilization}, {utilisation}, {employment}, {exercise}]
     2: accepted or habitual practice [syn: {custom}, {usance}]
