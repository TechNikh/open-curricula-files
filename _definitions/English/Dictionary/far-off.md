---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/far-off
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484485381
title: far-off
categories:
    - Dictionary
---
far-off
     adj : very far away in space or time; "faraway mountains"; "the
           faraway future"; "troops landing on far-off shores";
           "far-off happier times" [syn: {faraway}]
