---
version: 1
type: video
provider: YouTube
id: 0SZw8jpfAk0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 32dfbcc7-4777-4e2f-8b2b-3d0665c6b6f6
updated: 1486069622
title: Multiply Numbers And Algebra Equations By Drawing Lines
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/0SZw8jpfAk0/default.jpg
    - https://i3.ytimg.com/vi/0SZw8jpfAk0/1.jpg
    - https://i3.ytimg.com/vi/0SZw8jpfAk0/2.jpg
    - https://i3.ytimg.com/vi/0SZw8jpfAk0/3.jpg
---
