---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/respectively
offline_file: ""
offline_thumbnail: ""
uuid: 30f2e545-552b-480a-b81b-9a2f8e4e6381
updated: 1484310228
title: respectively
categories:
    - Dictionary
---
respectively
     adv : in the order given; "the brothers were called Felix and Max,
           respectively" [syn: {severally}]
