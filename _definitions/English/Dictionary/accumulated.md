---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accumulated
offline_file: ""
offline_thumbnail: ""
uuid: 2b713be6-5cb1-43ea-b5ad-1f5491124d9c
updated: 1484310202
title: accumulated
categories:
    - Dictionary
---
accumulated
     adj 1: brought together into a group or crowd; "the accumulated
            letters in my office" [syn: {amassed}, {assembled}, {collected},
             {congregate}, {massed}]
     2: periodically accumulated over time; "accrued interest";
        "accrued leave" [syn: {accrued}]
