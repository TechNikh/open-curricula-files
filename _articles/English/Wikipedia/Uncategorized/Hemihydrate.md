---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hemihydrates
offline_file: ""
offline_thumbnail: ""
uuid: ead3e59f-2409-4eda-b429-c56457f5d8ab
updated: 1484308312
title: Hemihydrate
categories:
    - Uncategorized
---
A hemihydrate, or semihydrate, is a hydrate whose solid contains one molecule of water of crystallization per two molecules, or per two unit cells.
