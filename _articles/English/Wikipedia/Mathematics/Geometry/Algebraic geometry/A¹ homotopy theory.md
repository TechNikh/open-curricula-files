---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/A%C2%B9_homotopy_theory'
offline_file: ""
offline_thumbnail: ""
uuid: 0f34ec05-2fcd-4b16-839b-c1f30d7e1ce6
updated: 1484309130
title: A¹ homotopy theory
tags:
    - Construction
    - Step 1
    - Step 2
    - Formal definition
    - Properties of the theory
categories:
    - Algebraic geometry
---
In algebraic geometry and algebraic topology, a branch of mathematics, A1 homotopy theory is a way to apply the techniques of algebraic topology, specifically homotopy, to algebraic varieties and, more generally, to schemes. The theory is due to Fabien Morel and Vladimir Voevodsky. The underlying idea is that it should be possible to develop a purely algebraic approach to homotopy theory by replacing the unit interval [0, 1], which is not an algebraic variety, with the affine line A1, which is. The theory requires a substantial amount of technique to set up, but has spectacular applications such as Voevodsky's construction of the derived category of mixed motives and the proof of the Milnor ...
