---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homologs
offline_file: ""
offline_thumbnail: ""
uuid: 060a7cad-728d-47ba-add4-be26814bde4a
updated: 1484308398
title: Homologous chromosome
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Karyotype.png
tags:
    - Overview
    - History
    - Structure
    - In humans
    - Functions
    - In meiosis
    - Prophase I
    - Metaphase I
    - Anaphase I
    - Meiosis II
    - In mitosis
    - In somatic cells
    - Problems
    - Nondisjunction
    - Embryonic death
    - Other uses
    - Relevant research
categories:
    - Uncategorized
---
A couple of homologous chromosomes, or homologs, are a set of one maternal and one paternal chromosomes that pair up with each other inside a cell during meiosis. These copies have the same genes in the same loci where they provide points along each chromosome which enable a pair of chromosomes to align correctly with each other before separating during meiosis.[1] This is the basis for Mendelian inheritance which characterizes inheritance patterns of genetic material from an organism to its offspring parent developmental cell at the given time and area.[2]
