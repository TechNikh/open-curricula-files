---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_Lie_group
offline_file: ""
offline_thumbnail: ""
uuid: 5a36050d-7573-4acd-b58e-c7a33f19928f
updated: 1484309357
title: Complex Lie group
categories:
    - Geometry stubs
---
In geometry, a complex Lie group is a complex-analytic manifold that is also a group in such a way 
  
    
      
        G
        ×
        G
        →
        G
        ,
        (
        x
        ,
        y
        )
        ↦
        x
        
          y
          
            −
            1
          
        
      
    
    {\displaystyle G\times G\to G,(x,y)\mapsto xy^{-1}}
  
 is holomorphic. Basic examples are 
  
    
      
        
          GL
          
            n
          
        
        ⁡
        (
        
          C
        
        )
      
    
    {\displaystyle \operatorname {GL} _{n}(\mathbb {C} )}
  
, the general linear groups over the ...
