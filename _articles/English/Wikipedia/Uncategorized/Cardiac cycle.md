---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cardiac_cycle
offline_file: ""
offline_thumbnail: ""
uuid: c42df978-6204-4f2f-9faf-e07cbe447f53
updated: 1484308368
title: Cardiac cycle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Wiggers_Diagram.png
tags:
    - Physiology
    - Rhythmical conduction
    - Heart Conducting System
    - Stages
    - Atrial systole
    - Detection
    - Ventricular systole
    - Detection
    - Heart sounds
    - Electrocardiogram
    - Diastole
categories:
    - Uncategorized
---
The cardiac cycle refers to a complete heartbeat from its generation to the beginning of the next beat, and so includes the diastole, the systole, and the intervening pause. The frequency of the cardiac cycle is described by the heart rate, which is typically expressed as beats per minute. Each beat of the heart involves five major stages. The first two stages, often considered together as the "ventricular filling" stage, involve the movement of blood from the atria into the ventricles. The next three stages involve the movement of blood from the ventricles to the pulmonary artery (in the case of the right ventricle) and the aorta (in the case of the left ventricle).[1]
