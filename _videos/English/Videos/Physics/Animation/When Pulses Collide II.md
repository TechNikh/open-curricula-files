---
version: 1
type: video
provider: YouTube
id: 8IRZYOC7DeU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bc8709e0-a261-47a7-8d69-4749bfb92456
updated: 1486069652
title: When Pulses Collide II
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/8IRZYOC7DeU/default.jpg
    - https://i3.ytimg.com/vi/8IRZYOC7DeU/1.jpg
    - https://i3.ytimg.com/vi/8IRZYOC7DeU/2.jpg
    - https://i3.ytimg.com/vi/8IRZYOC7DeU/3.jpg
---
