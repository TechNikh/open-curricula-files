---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bar
offline_file: ""
offline_thumbnail: ""
uuid: 7653b5bd-60c1-4073-8540-68b1d504f0f6
updated: 1484310271
title: bar
categories:
    - Dictionary
---
bar
     n 1: a room or establishment where alcoholic drinks are served
          over a counter; "he drowned his sorrows in whiskey at
          the bar" [syn: {barroom}, {saloon}, {ginmill}, {taproom}]
     2: a counter where you can obtain food or drink; "he bought a
        hot dog and a coke at the bar"
     3: a rigid piece of metal or wood; usually used as a fastening
        or obstruction or weapon; "there were bars in the windows
        to prevent escape"
     4: musical notation for a repeating pattern of musical beats;
        "the orchestra omitted the last twelve bars of the song"
        [syn: {measure}]
     5: an obstruction (usually metal) placed at the top of a goal;
    ...
