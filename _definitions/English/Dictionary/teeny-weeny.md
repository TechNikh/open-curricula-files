---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/teeny-weeny
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484496061
title: teeny-weeny
categories:
    - Dictionary
---
teeny-weeny
     adj : (used informally) very small; "a wee tot" [syn: {bitty}, {bittie},
            {teensy}, {teentsy}, {teeny}, {wee}, {weeny}, {weensy},
            {teensy-weensy}, {itty-bitty}, {itsy-bitsy}]
