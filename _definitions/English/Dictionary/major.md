---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/major
offline_file: ""
offline_thumbnail: ""
uuid: e995fc4d-cde2-4fd7-a62b-d62d5fb5f588
updated: 1484310359
title: major
categories:
    - Dictionary
---
major
     adj 1: of greater importance or stature or rank; "a major artist";
            "a major role"; "major highways" [ant: {minor}]
     2: greater in scope or effect; "a major contribution"; "a major
        improvement"; "a major break with tradition"; "a major
        misunderstanding" [ant: {minor}]
     3: greater in number or size or amount; "a major portion (a
        majority) of the population"; "Ursa Major"; "a major
        portion of the winnings" [ant: {minor}]
     4: of the field of academic study in which one concentrates or
        specializes; "his major field was mathematics" [ant: {minor}]
     5: of a scale or mode; "major scales"; "the key of D major"
        ...
