---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biopiracy
offline_file: ""
offline_thumbnail: ""
uuid: 8bdb44ab-e9c5-4b98-9928-1082c1ee1e23
updated: 1484310162
title: biopiracy
categories:
    - Dictionary
---
biopiracy
     n : biological theft; illegal collection of indigenous plants by
         corporations who patent them for their own use
