---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plo
offline_file: ""
offline_thumbnail: ""
uuid: 11c4b99b-567a-49c6-b07b-136351f0a558
updated: 1484310178
title: plo
categories:
    - Dictionary
---
PLO
     n : a political movement uniting Palestinian Arabs in an effort
         to create an independent state of Palestine; when formed
         in 1964 it was a terrorist organization dominated by
         Yasser Arafat's al-Fatah; in 1968 Arafat became chairman;
         received recognition by the United Nations and by Arab
         states in 1974 as a government in exile; has played a
         largely political role since the creation of the
         Palestine National Authority [syn: {Palestine Liberation
         Organization}]
