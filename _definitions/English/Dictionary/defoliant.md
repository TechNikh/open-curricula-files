---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defoliant
offline_file: ""
offline_thumbnail: ""
uuid: 73b6355f-032d-450c-a9f5-f6e43ce95ef2
updated: 1484310577
title: defoliant
categories:
    - Dictionary
---
defoliant
     n : a chemical that is sprayed on plants and causes their leaves
         to fall off
