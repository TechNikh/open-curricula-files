---
version: 1
type: video
provider: YouTube
id: DSBvhNna3UA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/What%20is%20Relativity%20of%20simultaneity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0bef5b57-f2f4-464b-b96a-4979e06cb278
updated: 1486069653
title: What is Relativity of simultaneity
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/DSBvhNna3UA/default.jpg
    - https://i3.ytimg.com/vi/DSBvhNna3UA/1.jpg
    - https://i3.ytimg.com/vi/DSBvhNna3UA/2.jpg
    - https://i3.ytimg.com/vi/DSBvhNna3UA/3.jpg
---
