---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rival
offline_file: ""
offline_thumbnail: ""
uuid: d17591fa-7fe8-4f78-b2fe-9c6df4763b28
updated: 1484310601
title: rival
categories:
    - Dictionary
---
rival
     n : the contestant you hope to defeat; "he had respect for his
         rivals"; "he wanted to know what the competition was
         doing" [syn: {challenger}, {competitor}, {competition}, {contender}]
     v 1: be equal to in quality or ability; "Nothing can rival cotton
          for durability"; "Your performance doesn't even touch
          that of your colleagues"; "Her persistence and ambition
          only matches that of her parents" [syn: {equal}, {touch},
           {match}]
     2: be the rival of, be in competition with; "we are rivaling
        for first place in the race"
     [also: {rivalling}, {rivalled}]
