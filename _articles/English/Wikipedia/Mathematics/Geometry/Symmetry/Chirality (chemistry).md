---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chirality_(chemistry)
offline_file: ""
offline_thumbnail: ""
uuid: 76513dd1-797f-407c-b2de-4d81db486a36
updated: 1484309304
title: Chirality (chemistry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Chirality_with_hands.svg.png
tags:
    - Definition
    - Stereogenic centers
    - In biochemistry
    - In inorganic chemistry
    - Methods and practices
    - Miscellaneous nomenclature
    - History
categories:
    - Symmetry
---
Chirality /kaɪˈrælɪti/ is a geometric property of some molecules and ions. A chiral molecule/ion is non-superposable on its mirror image. The presence of an asymmetric carbon center is one of several structural features that induce chirality in organic and inorganic molecules.[1][2][3][4] The term chirality is derived from the Greek word for hand, χειρ (kheir).
