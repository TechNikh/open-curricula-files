---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cultivatable
offline_file: ""
offline_thumbnail: ""
uuid: 740c1534-0513-410a-bbfb-24a00ce71b8b
updated: 1484310547
title: cultivatable
categories:
    - Dictionary
---
cultivatable
     adj : (of farmland) capable of being farmed productively [syn: {arable},
            {cultivable}, {tillable}]
