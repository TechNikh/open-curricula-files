---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Recursive_ordinal
offline_file: ""
offline_thumbnail: ""
uuid: 0fb2a7d4-e0db-4c48-a3ed-c2adf4d222c4
updated: 1484309490
title: Recursive ordinal
categories:
    - Computability theory
---
In mathematics, specifically set theory, an ordinal 
  
    
      
        α
      
    
    {\displaystyle \alpha }
  
 is said to be recursive if there is a recursive well-ordering of a subset of the natural numbers having the order type 
  
    
      
        α
      
    
    {\displaystyle \alpha }
  
.
