---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/missile
offline_file: ""
offline_thumbnail: ""
uuid: 2ca6e9f8-4498-4a7e-a657-e4ac741bda01
updated: 1484310178
title: missile
categories:
    - Dictionary
---
missile
     n 1: rocket carrying passengers or instruments or a warhead
     2: a weapon that is thrown or projected [syn: {projectile}]
