---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Asymmetry
offline_file: ""
offline_thumbnail: ""
uuid: b3639464-70e4-415b-9384-d80010826a73
updated: 1484309305
title: Asymmetry
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Asymmetric_%2528PSF%2529.svg.png'
tags:
    - In organisms
    - Evolution of asymmetry in organisms
    - As an indicator of unfitness
    - In structures
    - In mathematics
    - In chemistry
    - In physics
    - Thermodynamics
    - Particle physics
    - Parity violation
    - CP violation
    - Baryon asymmetry of the universe
    - Isospin violation
    - In collider experiments
    - Lexical
categories:
    - Symmetry
---
Asymmetry is the absence of, or a violation of, symmetry (the property of an object being invariant to a transformation, such as reflection). Symmetry is an important property of both physical and abstract systems and it may be displayed in precise terms or in more aesthetic terms. The absence of violation of symmetry that are either expected or desired can have important consequences for a system.
