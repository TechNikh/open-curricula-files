---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pungent
offline_file: ""
offline_thumbnail: ""
uuid: c2a58943-e750-4fd5-ba18-5d61b936ea4c
updated: 1484310429
title: pungent
categories:
    - Dictionary
---
pungent
     adj 1: sharp biting or acrid especially in taste or smell; "tasting
            the pungent wood sorrel"; "pungent curry"; "a pungent
            smell of burning sulfur" [ant: {bland}]
     2: capable of wounding; "a barbed compliment"; "a biting
        aphorism"; "pungent satire" [syn: {barbed}, {biting}, {nipping},
         {mordacious}]
