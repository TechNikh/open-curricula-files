---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stability_criterion
offline_file: ""
offline_thumbnail: ""
uuid: 460c321c-836b-47e1-93bd-876e707c1c3d
updated: 1484308830
title: Stability criterion
categories:
    - Stability theory
---
In control theory, and especially stability theory, a stability criterion establishes when a system is stable. A number of stability criteria are in common use:
