---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Single-entry_single-exit
offline_file: ""
offline_thumbnail: ""
uuid: a50b7d74-c1ea-4180-90de-086bce70ff7b
updated: 1484308622
title: Single-entry single-exit
categories:
    - Graph theory
---
In graph theory, a single-entry single-exit (SESE) region in a given graph is an ordered edge pair (a, b) of distinct control flow edges a and b where:
