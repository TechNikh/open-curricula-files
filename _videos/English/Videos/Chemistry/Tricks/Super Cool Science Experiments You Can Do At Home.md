---
version: 1
type: video
provider: YouTube
id: ia8CKDIur3s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3044cbe3-55d3-41e7-8345-1700efae6aee
updated: 1486069661
title: Super Cool Science Experiments You Can Do At Home
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/ia8CKDIur3s/default.jpg
    - https://i3.ytimg.com/vi/ia8CKDIur3s/1.jpg
    - https://i3.ytimg.com/vi/ia8CKDIur3s/2.jpg
    - https://i3.ytimg.com/vi/ia8CKDIur3s/3.jpg
---
