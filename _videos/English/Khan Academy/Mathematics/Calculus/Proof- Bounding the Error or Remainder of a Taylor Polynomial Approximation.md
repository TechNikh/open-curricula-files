---
version: 1
type: video
provider: YouTube
id: yUUPP70Fhpo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Proof-%20Bounding%20the%20Error%20or%20Remainder%20of%20a%20Taylor%20Polynomial%20Approximation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: aad8852c-5d7f-4e2d-ae88-e7db29e2420b
updated: 1486069690
title: 'Proof- Bounding the Error or Remainder of a Taylor Polynomial Approximation'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/yUUPP70Fhpo/default.jpg
    - https://i3.ytimg.com/vi/yUUPP70Fhpo/1.jpg
    - https://i3.ytimg.com/vi/yUUPP70Fhpo/2.jpg
    - https://i3.ytimg.com/vi/yUUPP70Fhpo/3.jpg
---
