---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/offshoot
offline_file: ""
offline_thumbnail: ""
uuid: 47ac91cd-3154-402a-b1ad-875529c6d701
updated: 1484310189
title: offshoot
categories:
    - Dictionary
---
offshoot
     n : a natural consequence of development [syn: {outgrowth}, {branch},
          {offset}]
