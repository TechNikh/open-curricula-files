---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Beta_skeleton
offline_file: ""
offline_thumbnail: ""
uuid: 08ab408f-3e0d-40ee-a90f-57a7be3048c9
updated: 1484309170
title: Beta skeleton
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Beta-skeleton.svg.png
tags:
    - Circle-based definition
    - Lune-based definition
    - History
    - Properties
    - Algorithms
    - Applications
    - Notes
categories:
    - Computational geometry
---
In computational geometry and geometric graph theory, a β-skeleton or beta skeleton is an undirected graph defined from a set of points in the Euclidean plane. Two points p and q are connected by an edge whenever all the angles prq are sharper than a threshold determined from the numerical parameter β.
