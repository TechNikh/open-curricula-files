---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Guess_2/3_of_the_average
offline_file: ""
offline_thumbnail: ""
uuid: fffa3e6f-6970-481c-a9a5-0ae76a44e4ab
updated: 1484309052
title: Guess 2/3 of the average
tags:
    - Equilibrium analysis
    - Experimental results
    - Rationality versus common knowledge of rationality
    - History
    - Notes
categories:
    - Non-cooperative games
---
In game theory, "guess 2/3 of the average" is a game where several people guess what 2/3 of the average of their guesses will be, and where the numbers are restricted to the real numbers between 0 and 100, inclusive. The winner is the one closest to the 2/3 average.
