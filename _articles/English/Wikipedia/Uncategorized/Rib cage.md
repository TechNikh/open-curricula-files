---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rib_cage
offline_file: ""
offline_thumbnail: ""
uuid: c98dac60-40c1-430e-b8da-4f8924378925
updated: 1484308372
title: Rib cage
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray112.png
tags:
    - Structure
    - Attachment
    - Parts
    - Bones
    - Development
    - Variation
    - Function
    - Clinical significance
    - Society and culture
    - History
    - Other animals
    - Additional images
    - Notes
categories:
    - Uncategorized
---
The rib cage is an arrangement of bones in the thorax of all vertebrates except the lamprey and the frog. It is formed by the vertebral column, ribs, and sternum and encloses the heart and lungs. In humans, the rib cage, also known as the thoracic cage, is a bony and cartilaginous structure which surrounds the thoracic cavity and supports the pectoral girdle (shoulder girdle), forming a core portion of the human skeleton. A typical human rib cage consists of 24 ribs, the sternum (with xiphoid process), costal cartilages, and the 12 thoracic vertebrae. Together with the skin and associated fascia and muscles, the rib cage makes up the thoracic wall and provides attachments for the muscles of ...
