---
version: 1
type: video
provider: YouTube
id: z5Fow4cTt9s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 08f03654-8613-40ee-ab93-b50cbce719df
updated: 1486069645
title: '3D fractal zoom - Inverted Temple'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/z5Fow4cTt9s/default.jpg
    - https://i3.ytimg.com/vi/z5Fow4cTt9s/1.jpg
    - https://i3.ytimg.com/vi/z5Fow4cTt9s/2.jpg
    - https://i3.ytimg.com/vi/z5Fow4cTt9s/3.jpg
---
