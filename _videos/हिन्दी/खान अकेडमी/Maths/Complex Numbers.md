---
version: 1
type: video
provider: YouTube
id: hGGHbSgXXO4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a9b6d3ba-95a9-44bf-8c84-d6fbff0e388b
updated: 1486069614
title: Complex Numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/hGGHbSgXXO4/default.jpg
    - https://i3.ytimg.com/vi/hGGHbSgXXO4/1.jpg
    - https://i3.ytimg.com/vi/hGGHbSgXXO4/2.jpg
    - https://i3.ytimg.com/vi/hGGHbSgXXO4/3.jpg
---
