---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/daring
offline_file: ""
offline_thumbnail: ""
uuid: 3df3ebbb-0ca9-4486-a363-d3ddef0e1274
updated: 1484310587
title: daring
categories:
    - Dictionary
---
daring
     adj 1: disposed to venture or take risks; "audacious visions of the
            total conquest of space"; "an audacious interpretation
            of two Jacobean dramas"; "the most daring of
            contemporary fiction writers"; "a venturesome
            investor"; "a venturous spirit" [syn: {audacious}, {venturesome},
             {venturous}]
     2: radically new or original; "an avant-garde theater piece"
        [syn: {avant-garde}]
     n 1: a challenge to do something dangerous or foolhardy; "he
          could never refuse a dare" [syn: {dare}]
     2: the trait of being willing to undertake things that involve
        risk or danger; "the proposal required great ...
