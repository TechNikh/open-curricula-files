---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mandelbrot_set
offline_file: ""
offline_thumbnail: ""
uuid: 96e2eb78-efde-41c9-875a-57343ce80f36
updated: 1484308724
title: Mandelbrot set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/322px-Mandel_zoom_00_mandelbrot_set.jpg
tags:
    - History
    - Formal definition
    - Basic properties
    - Other properties
    - Main cardioid and period bulbs
    - Hyperbolic components
    - Local connectivity
    - Self-similarity
    - Further results
    - Relationship with Julia sets
    - Geometry
    - Image gallery of a zoom sequence
    - Generalizations
    - Other, non-analytic, mappings
    - Computer drawings
    - Escape time algorithm
    - Histogram coloring
    - Continuous (smooth) coloring
    - Distance estimates
    - Exterior distance estimation
    - Interior distance estimation
    - Optimizations
    - Cardioid / bulb checking
    - Periodicity checking
    - Border tracing / edge checking
    - Perturbation theory and series approximation
    - References in popular culture
categories:
    - Complex dynamics
---
The Mandelbrot set is the set of complex numbers c for which the function 
  
    
      
        
          f
          
            c
          
        
        (
        z
        )
        =
        
          z
          
            2
          
        
        +
        c
      
    
    {\displaystyle f_{c}(z)=z^{2}+c}
  
 does not diverge when iterated from 
  
    
      
        z
        =
        0
      
    
    {\displaystyle z=0}
  
, i.e., for which the sequence 
  
    
      
        
          f
          
            c
          
        
        (
        0
        )
      
    
    {\displaystyle f_{c}(0)}
  
, 
  
    
      
        
          f
          
       ...
