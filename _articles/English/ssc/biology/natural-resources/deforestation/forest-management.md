---
title: Forest management
article_ref: https://en.wikipedia.org/wiki/Forest_management
tags:
    - Definition
    - Public input and awareness
    - Wildlife considerations
    - Management intensity
categories:
    - Deforestation
---
