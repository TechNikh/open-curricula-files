---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agrarian
offline_file: ""
offline_thumbnail: ""
uuid: 45422f4c-b673-4b96-bb27-725095a032d4
updated: 1484310183
title: agrarian
categories:
    - Dictionary
---
agrarian
     adj : relating to rural matters; "an agrarian (or agricultural)
           society"; "farming communities" [syn: {agricultural}, {farming(a)}]
