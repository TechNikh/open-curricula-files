---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wisely
offline_file: ""
offline_thumbnail: ""
uuid: bfe61b64-0805-453b-bbca-229970ab5b44
updated: 1484310249
title: wisely
categories:
    - Dictionary
---
wisely
     adv : in a wise manner; "she acted wisely when she invited her
           parents" [syn: {sagely}, {with wisdom}, {showing wisdom}]
           [ant: {foolishly}]
