---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saw
offline_file: ""
offline_thumbnail: ""
uuid: e2bb49f7-1ef4-4a73-8d63-337019e20783
updated: 1484310277
title: saw
categories:
    - Dictionary
---
saw
     n 1: a condensed but memorable saying embodying some important
          fact of experience that is taken as true by many people
          [syn: {proverb}, {adage}, {byword}]
     2: hand tool having a toothed blade for cutting
     3: a power tool for cutting wood [syn: {power saw}, {sawing
        machine}]
     v : cut with a saw; "saw wood for the fireplace"
     [also: {sawn}]
