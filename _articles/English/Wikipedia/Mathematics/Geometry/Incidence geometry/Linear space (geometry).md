---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_space_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: a0dab6bb-f2a5-4b5c-9db4-22725b3b9889
updated: 1484309251
title: Linear space (geometry)
tags:
    - Definition
    - Examples
categories:
    - Incidence geometry
---
A linear space is a basic structure in incidence geometry. A linear space consists of a set of elements called points, and a set of elements called lines. Each line is a distinct subset of the points. The points in a line are said to be incident with the line. Any two lines may have no more than one point in common. Intuitively, this rule can be visualized as two straight lines, which never intersect more than once.
