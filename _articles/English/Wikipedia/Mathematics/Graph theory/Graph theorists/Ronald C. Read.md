---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ronald_C._Read
offline_file: ""
offline_thumbnail: ""
uuid: 2717ed94-41ae-4309-b522-193306a7d14f
updated: 1484309475
title: Ronald C. Read
tags:
    - Life and career
    - Selected papers
    - Books
categories:
    - Graph theorists
---
Ronald C. Read (born December 19, 1924) is a professor emeritus of mathematics at the University of Waterloo, Canada. He has published many books [1] and papers, primarily on enumeration of graphs, graph isomorphism, chromatic polynomials, and particularly, the use of computers in graph-theoretical research. A majority of his later work was done in Waterloo. Read received his Ph.D. (1959) in graph theory from the University of London.[2]
