---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/narrative
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484462521
title: narrative
categories:
    - Dictionary
---
narrative
     adj : consisting of or characterized by the telling of a story;
           "narrative poetry"
     n : a message that tells the particulars of an act or occurrence
         or course of events; presented in writing or drama or
         cinema or as a radio or television program; "his
         narrative was interesting"; "Disney's stories entertain
         adults as well as children" [syn: {narration}, {story}, {tale}]
