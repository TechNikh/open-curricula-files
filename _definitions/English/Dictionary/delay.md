---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/delay
offline_file: ""
offline_thumbnail: ""
uuid: 81f4c717-4ba2-4442-976e-c98638ef3c73
updated: 1484310477
title: delay
categories:
    - Dictionary
---
delay
     n 1: time during which some action is awaited; "instant replay
          caused too long a delay"; "he ordered a hold in the
          action" [syn: {hold}, {time lag}, {postponement}, {wait}]
     2: the act of delaying; inactivity resulting in something being
        put off until a later time [syn: {holdup}]
     v 1: cause to be slowed down or delayed; "Traffic was delayed by
          the bad weather"; "she delayed the work that she didn't
          want to perform" [syn: {detain}, {hold up}] [ant: {rush}]
     2: act later than planned, scheduled, or required; "Don't delay
        your application to graduate school or else it won't be
        considered"
     3: stop or ...
