---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coincidence_point
offline_file: ""
offline_thumbnail: ""
uuid: be3da2cf-4f26-4c54-9a95-28ffd0c178f7
updated: 1484308786
title: Coincidence point
categories:
    - Fixed points (mathematics)
---
Formally, given two mappings
