---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Joule_heating
offline_file: ""
offline_thumbnail: ""
uuid: 42607a76-fec2-48d7-a30d-309c9304de59
updated: 1484308846
title: Joule heating
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/450px-Toaster-quartz_element.JPG
tags:
    - History
    - Microscopic description
    - Power loss and noise
    - Formulas
    - Direct current
    - Alternating current
    - Differential Form
    - Reason for high-voltage transmission of electricity
    - Applications
    - Heating efficiency
    - Hydraulic equivalent
categories:
    - Thermodynamics
---
Joule heating, also known as ohmic heating and resistive heating, is the process by which the passage of an electric current through a conductor releases heat.
