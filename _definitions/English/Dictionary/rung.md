---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rung
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484327041
title: rung
categories:
    - Dictionary
---
ring
     n 1: a characteristic sound; "it has the ring of sincerity"
     2: a toroidal shape; "a ring of ships in the harbor"; "a halo
        of smoke" [syn: {halo}, {annulus}, {anulus}, {doughnut}, {anchor
        ring}]
     3: a rigid circular band of metal or wood or other material
        used for holding or fastening or hanging or pulling;
        "there was still a rusty iron hoop for tying a horse"
        [syn: {hoop}]
     4: (chemistry) a chain of atoms in a molecule that forms a
        closed loop [syn: {closed chain}] [ant: {open chain}]
     5: an association of criminals; "police tried to break up the
        gang"; "a pack of thieves" [syn: {gang}, {pack}, {mob}]
     6: ...
