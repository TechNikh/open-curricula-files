---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Arnold%E2%80%93Moser_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 99162a99-d61b-407d-be6b-2ed224da89cb
updated: 1484308699
title: Kolmogorov–Arnold–Moser theorem
tags:
    - Statement
    - Integrable Hamiltonian systems
    - Perturbations
    - Consequences
    - KAM Theory
categories:
    - Theorems in dynamical systems
---
The Kolmogorov–Arnold–Moser theorem (KAM theorem) is a result in dynamical systems about the persistence of quasiperiodic motions under small perturbations. The theorem partly resolves the small-divisor problem that arises in the perturbation theory of classical mechanics.
