---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tow
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484347381
title: tow
categories:
    - Dictionary
---
tow
     n : the act of hauling something (as a vehicle) by means of a
         hitch or rope; "the truck gave him a tow to the garage"
         [syn: {towage}]
     v : drag behind; "Horses used to tow barges along the canal"
