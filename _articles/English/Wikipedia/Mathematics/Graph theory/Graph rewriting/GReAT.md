---
version: 1
type: article
id: https://en.wikipedia.org/wiki/GReAT
offline_file: ""
offline_thumbnail: ""
uuid: c593b798-50c5-452e-ae44-28fdb81e8b86
updated: 1484309450
title: GReAT
categories:
    - Graph rewriting
---
Graph Rewriting and Transformation (GReAT) is a Model Transformation Language (MTL) for Model Integrated Computing available in the GME environment. GReAT has a rich pattern specification sublanguage, a graph transformation sublanguage and a high level control-flow sublanguage. It has been designed to address the specific needs of the model transformation area. The GME environment is an example of a Model Driven Engineering (MDE) framework.
