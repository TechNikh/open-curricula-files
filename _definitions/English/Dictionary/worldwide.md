---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worldwide
offline_file: ""
offline_thumbnail: ""
uuid: f77d2237-fa42-4f3e-a05d-a7d4b0584fbc
updated: 1484310249
title: worldwide
categories:
    - Dictionary
---
worldwide
     adj 1: spanning or extending throughout the entire world;
            "worldwide distribution"; "a worldwide epidemic"
     2: involving the entire earth; not limited or provincial in
        scope; "global war"; "global monetary policy"; "neither
        national nor continental but planetary"; "a world crisis";
        "of worldwide significance" [syn: {global}, {planetary}, {world(a)}]
     3: of worldwide scope or applicability; "an issue of
        cosmopolitan import"; "the shrewdest political and
        ecumenical comment of our time"- Christopher Morley;
        "universal experience" [syn: {cosmopolitan}, {ecumenical},
         {oecumenical}, {general}, {universal}]
