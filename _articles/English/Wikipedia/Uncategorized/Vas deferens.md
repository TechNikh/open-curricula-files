---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vas_deferens
offline_file: ""
offline_thumbnail: ""
uuid: 4d5e8d97-5037-4237-b93f-12f267b11294
updated: 1484308366
title: Vas deferens
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Male_anatomy_en.svg.png
tags:
    - Structure
    - Blood supply
    - Function
    - Clinical significance
    - Contraception
    - Disease
    - Uses in pharmacology and physiology
    - Other animals
    - Additional images
categories:
    - Uncategorized
---
The vas deferens (Latin: "carrying-away vessel"; plural: vasa deferentia), also called ductus deferens (Latin: "carrying-away duct"; plural: ductus deferentes), is part of the male reproductive system of many vertebrates; these vasa transport sperm from the epididymis to the ejaculatory ducts in anticipation of ejaculation.
