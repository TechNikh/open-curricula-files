---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/invaluable
offline_file: ""
offline_thumbnail: ""
uuid: 18261d3e-9827-4c16-804a-59d8df0d8f55
updated: 1484310426
title: invaluable
categories:
    - Dictionary
---
invaluable
     adj : having incalculable monetary worth [syn: {priceless}]
