---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Random_Fibonacci_sequence
offline_file: ""
offline_thumbnail: ""
uuid: 2209a014-e202-457b-a06f-f890bf32f8c4
updated: 1484308659
title: Random Fibonacci sequence
tags:
    - Description
    - Growth rate
    - Related work
categories:
    - Number theory
---
In mathematics, the random Fibonacci sequence is a stochastic analogue of the Fibonacci sequence defined by the recurrence relation fn = fn−1 ± fn−2, where the signs + or − are chosen at random with equal probability 1/2, independently for different n. By a theorem of Harry Kesten and Hillel Furstenberg, random recurrent sequences of this kind grow at a certain exponential rate, but it is difficult to compute the rate explicitly. In 1999, Divakar Viswanath showed that the growth rate of the random Fibonacci sequence is equal to 1.1319882487943…, a mathematical constant that was later named Viswanath's constant.[1][2][3]
