---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Intuitive_criterion
offline_file: ""
offline_thumbnail: ""
uuid: 6f2b21ca-b4d8-4f1d-b2c6-1f78e3f256f9
updated: 1484309032
title: Intuitive criterion
tags:
    - Criticisms
    - Example
    - Notes
categories:
    - Game theory equilibrium concepts
---
The intuitive criterion is a technique for equilibrium refinement in signaling games. It aims to reduce possible outcome scenarios by first restricting the type group to types of agents who could obtain higher utility levels by deviating to off-the-equilibrium messages and second by considering in this sub-set of types the types for which the off-the-equilibrium message is not equilibrium dominated.[1]
