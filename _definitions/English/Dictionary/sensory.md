---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sensory
offline_file: ""
offline_thumbnail: ""
uuid: a0e58f8a-2cad-4361-8abb-2e6f44e19b1b
updated: 1484310344
title: sensory
categories:
    - Dictionary
---
sensory
     adj 1: of a nerve fiber or impulse originating outside and passing
            toward the central nervous system; "sensory neurons"
            [syn: {centripetal}, {receptive}, {sensory(a)}]
     2: involving or derived from the senses; "sensory experience";
        "sensory channels" [syn: {sensorial}] [ant: {extrasensory}]
     3: relating to or concerned in sensation; "the sensory cortex";
        "sensory organs" [syn: {sensational}]
