---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/promptly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484456521
title: promptly
categories:
    - Dictionary
---
promptly
     adv 1: with little or no delay; "the rescue squad arrived
            promptly"; "come here, quick!" [syn: {quickly}, {quick}]
     2: in a punctual manner; "he did his homework promptly" [syn: {readily},
         {pronto}, {without delay}]
     3: at once (usually modifies an undesirable occurrence); "he
        promptly forgot the address" [syn: {right away}]
