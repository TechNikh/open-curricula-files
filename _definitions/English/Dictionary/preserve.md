---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preserve
offline_file: ""
offline_thumbnail: ""
uuid: 316e7a92-93b1-4f69-a43d-273ae83b5c6d
updated: 1484310236
title: preserve
categories:
    - Dictionary
---
preserve
     n 1: a domain that seems to be specially reserved for someone;
          "medicine is no longer a male preserve"
     2: a reservation where animals are protected
     3: fruit preserved by cooking with sugar [syn: {conserve}, {conserves},
         {preserves}]
     v 1: keep or maintain in unaltered condition; cause to remain or
          last; "preserve the peace in the family"; "continue the
          family tradition"; "Carry on the old traditions" [syn: {continue},
           {uphold}, {carry on}, {bear on}] [ant: {discontinue}]
     2: keep in safety and protect from harm, decay, loss, or
        destruction; "We preserve these archeological findings";
        "The old ...
