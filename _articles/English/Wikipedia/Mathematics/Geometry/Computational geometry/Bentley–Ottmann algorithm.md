---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bentley%E2%80%93Ottmann_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 985b87ff-cabc-46b2-b6c8-31f73e52f398
updated: 1484309170
title: Bentley–Ottmann algorithm
tags:
    - Overall strategy
    - Data structures
    - Detailed algorithm
    - Analysis
    - Special position and numerical precision issues
    - Faster algorithms
    - Notes
categories:
    - Computational geometry
---
In computational geometry, the Bentley–Ottmann algorithm is a sweep line algorithm for listing all crossings in a set of line segments. It extends the Shamos–Hoey algorithm,[1] a similar previous algorithm for testing whether or not a set of line segments has any crossings. For an input consisting of n line segments with k crossings, the Bentley–Ottmann algorithm takes time O((n + k) log n). In cases where k = o(n2 / log n), this is an improvement on a naïve algorithm that tests every pair of segments, which takes O(n2).
