---
title: Deforestation in Brazil
article_ref: https://en.wikipedia.org/wiki/Deforestation_in_Brazil
tags:
    - History
    - Solutions
    - Cattle ranching and infrastructure
    - Hydroelectric
    - Mining activities
    - Soybean production
    - Logging
    - Effects
    - NASA survey
    - Comparing Brazil to Australia
    - Measured rates
    - Response
    - future
categories:
    - Deforestation
---
