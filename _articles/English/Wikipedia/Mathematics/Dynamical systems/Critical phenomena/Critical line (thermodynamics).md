---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Critical_line_(thermodynamics)
offline_file: ""
offline_thumbnail: ""
uuid: ea6133cf-7bd2-4bf6-a72a-a58b89321cd5
updated: 1484308746
title: Critical line (thermodynamics)
categories:
    - Critical phenomena
---
In thermodynamics, a critical line is the higher-dimensional equivalent of a critical point.[1] It is the locus of contiguous critical points in a phase diagram. These lines cannot occur for a single substance due to the phase rule, but they can be observed in systems with more variables, such as mixtures. Two critical lines may meet and terminate in a tricritical point.
