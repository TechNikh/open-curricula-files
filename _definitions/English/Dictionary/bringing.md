---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bringing
offline_file: ""
offline_thumbnail: ""
uuid: 4a44af90-3a71-463b-a37b-7eeb7d4cdf17
updated: 1484310328
title: bringing
categories:
    - Dictionary
---
bringing
     n : the act of delivering or distributing something (as goods or
         mail); "his reluctant delivery of bad news" [syn: {delivery}]
