---
version: 1
type: video
provider: YouTube
id: 1h_2X9eg3_A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fc161aca-92b4-44ae-be6b-ec8b774fa1ea
updated: 1486069641
title: 'Mandala Matrix Fractal - Mandelbulb 3D HD 720p'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/1h_2X9eg3_A/default.jpg
    - https://i3.ytimg.com/vi/1h_2X9eg3_A/1.jpg
    - https://i3.ytimg.com/vi/1h_2X9eg3_A/2.jpg
    - https://i3.ytimg.com/vi/1h_2X9eg3_A/3.jpg
---
