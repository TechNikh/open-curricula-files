---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lignin
offline_file: ""
offline_thumbnail: ""
uuid: 8c179c75-7e5f-4140-9073-53d943fa4c41
updated: 1484310264
title: lignin
categories:
    - Dictionary
---
lignin
     n : a complex polymer; the chief non-carbohydrate constituent of
         wood; binds to cellulose fibers to harden and strengthen
         cell walls of plants
