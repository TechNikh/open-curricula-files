---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Interval_exchange_transformation
offline_file: ""
offline_thumbnail: ""
uuid: 70183bc6-7c07-4eb1-ab04-3d1c37d2b3db
updated: 1484308732
title: Interval exchange transformation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Interval_exchange.svg.png
tags:
    - Formal definition
    - Properties
    - Generalizations
    - Notes
categories:
    - Chaotic maps
---
In mathematics, an interval exchange transformation[1] is a kind of dynamical system that generalises circle rotation. The phase space consists of the unit interval, and the transformation acts by cutting the interval into several subintervals, and then permuting these subintervals.
