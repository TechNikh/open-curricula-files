---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Noetherian_scheme
offline_file: ""
offline_thumbnail: ""
uuid: 885ed668-1bd9-4dbe-a175-c978768ae39c
updated: 1484309367
title: Noetherian scheme
categories:
    - Geometry stubs
---
In algebraic geometry, a noetherian scheme is a scheme that admits a finite covering by open affine subsets 
  
    
      
        Spec
        ⁡
        
          A
          
            i
          
        
      
    
    {\displaystyle \operatorname {Spec} A_{i}}
  
, 
  
    
      
        
          A
          
            i
          
        
      
    
    {\displaystyle A_{i}}
  
 noetherian rings. More generally, a scheme is locally noetherian if it is covered by spectra of noetherian rings. Thus, a scheme is noetherian if and only if it is locally noetherian and quasi-compact. As with noetherian rings, the concept is named after Emmy Noether.
