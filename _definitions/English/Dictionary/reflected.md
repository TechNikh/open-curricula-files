---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reflected
offline_file: ""
offline_thumbnail: ""
uuid: 2c010724-2d20-47ef-9ebb-77fb176b6df0
updated: 1484310224
title: reflected
categories:
    - Dictionary
---
reflected
     adj : (especially of incident sound or light) bent or sent back;
           "reflected light"; "reflected heat"; "reflected glory"
           [ant: {unreflected}]
