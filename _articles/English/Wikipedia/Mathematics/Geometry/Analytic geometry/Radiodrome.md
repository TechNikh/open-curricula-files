---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Radiodrome
offline_file: ""
offline_thumbnail: ""
uuid: 20ba85cb-b92e-4fdd-b8cd-78a0e38465c5
updated: 1484309162
title: Radiodrome
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Radio_icon_3.png
categories:
    - Analytic geometry
---
In geometry, a radiodrome is the pursuit curve followed by a point that is pursuing another linearly-moving point. The term is derived from the Greek words "ῥάδιος" (easier) and "δρόμος" (running). The classic (and best-known) form of a radiodrome is known as the "dog curve"; this is the path a dog follows when it swims across a stream with a current after food it has spotted on the other side. Because the dog drifts downwards with the current, it will have to change its heading; it will also have to swim further than if it had computed the optimal heading. This case was described by Pierre Bouguer in 1732.
