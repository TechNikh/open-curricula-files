---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parallelogram
offline_file: ""
offline_thumbnail: ""
uuid: 141e8594-5e2e-46a7-9afe-590311f550fd
updated: 1484310140
title: parallelogram
categories:
    - Dictionary
---
parallelogram
     n : a quadrilateral whose opposite sides are both parallel and
         equal in length [ant: {trapezium}]
