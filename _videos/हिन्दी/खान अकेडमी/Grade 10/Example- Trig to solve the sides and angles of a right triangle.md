---
version: 1
type: video
provider: YouTube
id: cDrlrVIaS68
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 02bb5010-15c1-4aa9-9bf4-3f63d90b3782
updated: 1486069619
title: 'Example- Trig to solve the sides and angles of a right triangle'
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/cDrlrVIaS68/default.jpg
    - https://i3.ytimg.com/vi/cDrlrVIaS68/1.jpg
    - https://i3.ytimg.com/vi/cDrlrVIaS68/2.jpg
    - https://i3.ytimg.com/vi/cDrlrVIaS68/3.jpg
---
