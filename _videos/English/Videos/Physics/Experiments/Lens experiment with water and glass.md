---
version: 1
type: video
provider: YouTube
id: HY-k6utbjf0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 48e07d36-0a3f-4fbc-84a3-16fc2b07ac84
updated: 1486069639
title: Lens experiment with water and glass
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/HY-k6utbjf0/default.jpg
    - https://i3.ytimg.com/vi/HY-k6utbjf0/1.jpg
    - https://i3.ytimg.com/vi/HY-k6utbjf0/2.jpg
    - https://i3.ytimg.com/vi/HY-k6utbjf0/3.jpg
---
