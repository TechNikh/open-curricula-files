---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reign
offline_file: ""
offline_thumbnail: ""
uuid: 1b833f36-2d9a-4dda-b650-2a1bf7a35962
updated: 1484310174
title: reign
categories:
    - Dictionary
---
reign
     n 1: a period during which something or somebody is dominant or
          powerful; "he was helpless under the reign of his
          egotism"
     2: the period during which a monarch is sovereign; "during the
        reign of Henry VIII"
     3: royal authority; the dominion of a monarch [syn: {sovereignty}]
     v 1: have sovereign power; "Henry VIII reigned for a long time"
     2: be larger in number, quantity, power, status or importance;
        "Money reigns supreme here"; "Hispanics predominate in
        this neighborhood" [syn: {predominate}, {dominate}, {rule},
         {prevail}]
