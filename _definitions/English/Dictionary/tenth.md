---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tenth
offline_file: ""
offline_thumbnail: ""
uuid: 384348da-5dfc-4644-953f-5c878f7c34b2
updated: 1484310469
title: tenth
categories:
    - Dictionary
---
tenth
     adj : coming next after the ninth and just before the eleventh in
           position [syn: {10th}]
     n 1: a tenth part; one part in ten [syn: {one-tenth}, {tenth part},
           {ten percent}]
     2: position ten in a countable series of things
