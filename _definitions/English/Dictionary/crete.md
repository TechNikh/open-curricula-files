---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crete
offline_file: ""
offline_thumbnail: ""
uuid: 5d8af7a5-425b-4ea0-b7c6-26b14b6609da
updated: 1484310555
title: crete
categories:
    - Dictionary
---
Crete
     n : the largest Greek island in the Mediterranean; site of the
         Minoan civilization that reached its peak in 1600 BC
         [syn: {Kriti}]
