---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/violated
offline_file: ""
offline_thumbnail: ""
uuid: 2214053f-fb6b-48a1-8742-4e49d855168b
updated: 1484310395
title: violated
categories:
    - Dictionary
---
violated
     adj : treated irreverently or sacrilegiously [syn: {profaned}]
