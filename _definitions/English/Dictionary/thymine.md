---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thymine
offline_file: ""
offline_thumbnail: ""
uuid: ad0c3814-67aa-4d19-bb32-3b30c65ad872
updated: 1484310295
title: thymine
categories:
    - Dictionary
---
thymine
     n : a base found in DNA (but not in RNA) and derived from
         pyrimidine; pairs with adenine [syn: {T}]
