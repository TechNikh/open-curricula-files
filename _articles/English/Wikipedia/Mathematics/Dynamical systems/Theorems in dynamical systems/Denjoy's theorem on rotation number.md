---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Denjoy%27s_theorem_on_rotation_number'
offline_file: ""
offline_thumbnail: ""
uuid: a399a103-21da-4309-9376-d8148efb9d95
updated: 1484308690
title: "Denjoy's theorem on rotation number"
tags:
    - Statement of the theorem
    - Complements
categories:
    - Theorems in dynamical systems
---
In mathematics, the Denjoy theorem gives a sufficient condition for a diffeomorphism of the circle to be topologically conjugate to a diffeomorphism of a special kind, namely an irrational rotation. Denjoy (1932) proved the theorem in the course of his topological classification of homeomorphisms of the circle. He also gave an example of a C1 diffeomorphism with an irrational rotation number that is not conjugate to a rotation.
