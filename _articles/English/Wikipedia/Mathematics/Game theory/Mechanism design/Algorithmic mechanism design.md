---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algorithmic_mechanism_design
offline_file: ""
offline_thumbnail: ""
uuid: eb8f31e7-1131-4a77-8212-48cff140c774
updated: 1484309045
title: Algorithmic mechanism design
categories:
    - Mechanism design
---
Noam Nisan and Amir Ronen, from the Hebrew University of Jerusalem, first coined "Algorithmic mechanism design" in a research paper published in 1999.[1][2]
