---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/social
offline_file: ""
offline_thumbnail: ""
uuid: 7b3af7ad-ee1b-4232-8fa1-a686962ae9a9
updated: 1484310264
title: social
categories:
    - Dictionary
---
social
     adj 1: relating to human society and its members; "social
            institutions"; "societal evolution"; "societal
            forces"; "social legislation" [syn: {societal}]
     2: living together or enjoying life in communities or organized
        groups; "human beings are social animals"; "spent a
        relaxed social evening"; "immature social behavior" [ant:
        {unsocial}]
     3: relating to or belonging to or characteristic of high
        society; "made fun of her being so social and high-toned";
        "a social gossip colum"; "the society page"
     4: composed of sociable people or formed for the purpose of
        sociability; "a purely social club"; "the ...
