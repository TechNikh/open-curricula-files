---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ache
offline_file: ""
offline_thumbnail: ""
uuid: 6a57f03b-b3e3-4b30-a5b5-3fb1bffca5fd
updated: 1484310515
title: ache
categories:
    - Dictionary
---
ache
     n : a dull persistent (usually moderately intense) pain [syn: {aching}]
     v 1: feel physical pain; "Were you hurting after the accident?"
          [syn: {hurt}, {suffer}]
     2: have a desire for something or someone who is not present;
        "She ached for a cigarette"; "I am pining for my lover"
        [syn: {yearn}, {yen}, {pine}, {languish}]
     3: be the source of pain [syn: {smart}, {hurt}]
