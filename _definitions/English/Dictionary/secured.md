---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secured
offline_file: ""
offline_thumbnail: ""
uuid: 621bbbfd-fc65-4795-a391-78dc1a8340fd
updated: 1484310163
title: secured
categories:
    - Dictionary
---
secured
     adj 1: firmly fastened or secured against opening; "windows and
            doors were all fast"; "a locked closet"; "left the
            house properly secured" [syn: {barred}, {bolted}, {fast},
             {latched}, {locked}]
     2: secured by written agreement [syn: {bonded}, {guaranteed}, {warranted}]
