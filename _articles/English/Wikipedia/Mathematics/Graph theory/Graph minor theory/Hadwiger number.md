---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hadwiger_number
offline_file: ""
offline_thumbnail: ""
uuid: 170cf925-b554-4dca-b646-b5bb0ca61d11
updated: 1484309413
title: Hadwiger number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Hadwiger_conjecture.svg_0.png
tags:
    - Graphs with small Hadwiger number
    - Sparsity
    - Coloring
    - Computational complexity
    - Related concepts
    - Notes
categories:
    - Graph minor theory
---
In graph theory, the Hadwiger number of an undirected graph G is the size of the largest complete graph that can be obtained by contracting edges of G. Equivalently, the Hadwiger number h(G) is the largest number k for which the complete graph Kk is a minor of G, a smaller graph obtained from G by edge contractions and vertex and edge deletions. The Hadwiger number is also known as the contraction clique number of G[1] or the homomorphism degree of G.[2] It is named after Hugo Hadwiger, who introduced it in 1943 in conjunction with the Hadwiger conjecture, which states that the Hadwiger number is always at least as large as the chromatic number of G.
