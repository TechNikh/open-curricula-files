---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Faraday_paradox
offline_file: ""
offline_thumbnail: ""
uuid: 811a6eb0-464a-405c-b1ab-802f6faf1cdb
updated: 1484308759
title: Faraday paradox
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-M_Faraday_Th_Phillips_oil_1842.jpg
tags:
    - "Faraday's law compared to the Maxwell–Faraday equation"
    - "Paradoxes in which Faraday's law of induction seems to predict zero EMF but actually predicts non-zero EMF"
    - The equipment
    - The procedure
    - Why is this paradoxical?
    - "Faraday's explanation"
    - Modern explanations
    - Taking the return path into account
    - Using the Lorentz force
    - >
        When the magnet is rotating, but flux lines are stationary,
        and the conductor is stationary
    - >
        When the magnet and the flux lines are stationary and the
        conductor is rotating
    - "Use of special techniques with Faraday's law"
    - Configuration with a return path
    - Configuration without a return path
    - "Paradoxes in which Faraday's law of induction seems to predict non-zero EMF but actually predicts zero EMF"
    - An additional rule
categories:
    - Electrodynamics
---
The Faraday paradox or Faraday's paradox is any experiment in which Michael Faraday's law of electromagnetic induction appears to predict an incorrect result. The paradoxes fall into two classes:
