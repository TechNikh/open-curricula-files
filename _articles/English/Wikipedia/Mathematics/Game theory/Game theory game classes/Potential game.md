---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Potential_game
offline_file: ""
offline_thumbnail: ""
uuid: 29400fb2-6d29-4cb0-83e7-0105d11b34e9
updated: 1484309040
title: Potential game
tags:
    - Definition
    - A simple example
    - Equilibrium selection
    - Bounded rational models
categories:
    - Game theory game classes
---
In game theory, a game is said to be a potential game if the incentive of all players to change their strategy can be expressed using a single global function called the potential function. Robert W. Rosenthal created the concept of a congestion game in 1973. Dov Monderer and Lloyd Shapley [1] created the concept of a potential game and proved that every congestion game is a potential game.
