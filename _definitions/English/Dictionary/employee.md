---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/employee
offline_file: ""
offline_thumbnail: ""
uuid: 7d96ee68-8962-4eb8-b3e4-1f57967c95b6
updated: 1484310451
title: employee
categories:
    - Dictionary
---
employee
     n : a worker who is hired to perform a job [ant: {employer}]
