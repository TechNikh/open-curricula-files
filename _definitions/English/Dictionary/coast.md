---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coast
offline_file: ""
offline_thumbnail: ""
uuid: 3907dd95-9853-4520-bc82-d04a33f5de5e
updated: 1484310433
title: coast
categories:
    - Dictionary
---
coast
     n 1: the shore of a sea or ocean [syn: {seashore}, {seacoast}, {sea-coast}]
     2: a slope down which sleds may coast; "when it snowed they
        made a coast on the golf course"
     3: the area within view; "the coast is clear"
     4: the act of moving smoothly along a surface while remaining
        in contact with it; "his slide didn't stop until the
        bottom of the hill"; "the children lined up for a coast
        down the snowy slope" [syn: {slide}, {glide}]
     v : move effortlessly; by force of gravity
