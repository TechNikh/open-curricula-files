---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pinhole_camera_model
offline_file: ""
offline_thumbnail: ""
uuid: 26671ac7-c774-4409-a5f7-12016601ff8c
updated: 1484309184
title: Pinhole camera model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pinhole-camera.svg.png
tags:
    - The geometry and mathematics of the pinhole camera
    - Rotated image and the virtual image plane
    - Homogeneous coordinates
    - Bibliography
categories:
    - Geometry in computer vision
---
The pinhole camera model describes the mathematical relationship between the coordinates of a 3D point and its projection onto the image plane of an ideal pinhole camera, where the camera aperture is described as a point and no lenses are used to focus light. The model does not include, for example, geometric distortions or blurring of unfocused objects caused by lenses and finite sized apertures. It also does not take into account that most practical cameras have only discrete image coordinates. This means that the pinhole camera model can only be used as a first order approximation of the mapping from a 3D scene to a 2D image. Its validity depends on the quality of the camera and, in ...
