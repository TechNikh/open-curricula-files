---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Klee%E2%80%93Minty_cube'
offline_file: ""
offline_thumbnail: ""
uuid: 5c8bb321-55c4-4b4b-aca5-0772f12b2b5f
updated: 1484309197
title: Klee–Minty cube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Unitcube.svg.png
tags:
    - Description of the cube
    - Computational complexity
    - Worst case
    - Path-following algorithms
    - Average case
    - Notes
    - Algebraic description with illustration
    - Pictures with no linear system
categories:
    - Convex geometry
---
The Klee–Minty cube or Klee–Minty polytope (named after Victor Klee and George J. Minty) is a unit hypercube of variable dimension whose corners have been perturbed. Klee and Minty demonstrated that George Dantzig's simplex algorithm has poor worst-case performance when initialized at one corner of their "squashed cube".
