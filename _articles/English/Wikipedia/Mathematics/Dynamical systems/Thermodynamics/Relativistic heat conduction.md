---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Relativistic_heat_conduction
offline_file: ""
offline_thumbnail: ""
uuid: b2f6aba4-cf49-44e8-a27b-a81cb92626a0
updated: 1484308859
title: Relativistic heat conduction
tags:
    - Background
    - Classical model
    - Hyperbolic model
    - Criticism to the HHC model
    - Derivation of the RHC equation
    - Transformations
    - Implications
    - Criticism to RHC
    - Applications
    - Notes
categories:
    - Thermodynamics
---
The theory of relativistic heat conduction (RHC)[1] claims to be the only model for heat conduction (and similar diffusion processes) that is compatible with the theory of special relativity, the second law of thermodynamics, electrodynamics, and quantum mechanics, simultaneously. The main features of RHC are:
