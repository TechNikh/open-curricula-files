---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/learning
offline_file: ""
offline_thumbnail: ""
uuid: 7a841161-836d-4481-bc30-9e8bdd3a1730
updated: 1484310363
title: learning
categories:
    - Dictionary
---
learning
     n 1: the cognitive process of acquiring skill or knowledge; "the
          child's acquisition of language" [syn: {acquisition}]
     2: profound scholarly knowledge [syn: {eruditeness}, {erudition},
         {learnedness}, {scholarship}, {encyclopedism}, {encyclopaedism}]
