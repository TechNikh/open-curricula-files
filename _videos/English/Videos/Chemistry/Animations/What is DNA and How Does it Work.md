---
version: 1
type: video
provider: YouTube
id: zwibgNGe4aY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/What%20is%20DNA%20and%20How%20Does%20it%20Work.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 14eac6a3-deec-4594-8aed-636e92f5fc5a
updated: 1486069662
title: What is DNA and How Does it Work?
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/zwibgNGe4aY/default.jpg
    - https://i3.ytimg.com/vi/zwibgNGe4aY/1.jpg
    - https://i3.ytimg.com/vi/zwibgNGe4aY/2.jpg
    - https://i3.ytimg.com/vi/zwibgNGe4aY/3.jpg
---
