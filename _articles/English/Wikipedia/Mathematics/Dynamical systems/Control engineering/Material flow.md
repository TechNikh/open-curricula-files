---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Material_flow
offline_file: ""
offline_thumbnail: ""
uuid: 55a2f62c-36fd-4924-9350-ecb3b9dabd85
updated: 1484308738
title: Material flow
categories:
    - Control engineering
---
Material flow (MF) is the description of the transportation of raw materials, pre-fabricates, parts, components, integrated objects and final products as a flow of entities. The term applies mainly to advanced modeling of Supply chain management. As industrial material flow can easily become very complex.
