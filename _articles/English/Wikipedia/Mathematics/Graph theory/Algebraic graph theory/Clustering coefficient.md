---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clustering_coefficient
offline_file: ""
offline_thumbnail: ""
uuid: 08ae391a-ee75-48c5-88b9-34199963ef0e
updated: 1484309373
title: Clustering coefficient
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Clustering_coefficient_example.svg.png
tags:
    - Global clustering coefficient
    - Local clustering coefficient
    - Network average clustering coefficient
categories:
    - Algebraic graph theory
---
In graph theory, a clustering coefficient is a measure of the degree to which nodes in a graph tend to cluster together. Evidence suggests that in most real-world networks, and in particular social networks, nodes tend to create tightly knit groups characterised by a relatively high density of ties; this likelihood tends to be greater than the average probability of a tie randomly established between two nodes (Holland and Leinhardt, 1971;[1] Watts and Strogatz, 1998[2]).
