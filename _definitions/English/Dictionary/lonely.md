---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lonely
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484467141
title: lonely
categories:
    - Dictionary
---
lonely
     adj 1: lacking companions or companionship; "he was alone when we
            met him"; "she is alone much of the time"; "the lone
            skier on the mountain"; "a lonely fisherman stood on a
            tuft of gravel"; "a lonely soul"; "a solitary
            traveler" [syn: {alone(p)}, {lone(a)}, {lonely(a)}, {solitary}]
     2: marked by dejection from being alone; "felt sad and lonely";
        "the loneliest night of the week"; "lonesome when her
        husband is away"; "spent a lonesome hour in the bar" [syn:
         {lonesome}]
     3: separated from or unfrequented by others; remote or
        secluded; "a lonely crossroads"; "a solitary retreat"; "a
        ...
