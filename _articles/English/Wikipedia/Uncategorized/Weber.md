---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weber
offline_file: ""
offline_thumbnail: ""
uuid: a95c3f2c-0144-4c83-ab88-5b88a5fd2615
updated: 1484308317
title: Weber
categories:
    - Uncategorized
---
Weber (/ˈwɛbər/, /ˈwiːbər/ or /ˈweɪbər/; German: [ˈveːbɐ]) is a surname of German origin, derived from the noun meaning "weaver". In some cases, following migration to English-speaking countries, it has been anglicised to the English surname 'Webber' or even 'Weaver'.
