---
version: 1
type: video
provider: YouTube
id: pbgnxzmIL5c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Wave3%20HD.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3ef45497-f74b-406d-9fb8-3c4e7065a24a
updated: 1486069652
title: Wave3 HD
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/pbgnxzmIL5c/default.jpg
    - https://i3.ytimg.com/vi/pbgnxzmIL5c/1.jpg
    - https://i3.ytimg.com/vi/pbgnxzmIL5c/2.jpg
    - https://i3.ytimg.com/vi/pbgnxzmIL5c/3.jpg
---
