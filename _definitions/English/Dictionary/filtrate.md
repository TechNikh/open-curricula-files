---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/filtrate
offline_file: ""
offline_thumbnail: ""
uuid: 6eb3da30-61b8-43f9-bd84-f28bb742e387
updated: 1484310384
title: filtrate
categories:
    - Dictionary
---
filtrate
     n : the product of filtration; a gas or liquid that has been
         passed through a filter
     v : remove by passing through a filter; "filter out the
         impurities" [syn: {filter}, {strain}, {separate out}, {filter
         out}]
