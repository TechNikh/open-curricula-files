---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amazed
offline_file: ""
offline_thumbnail: ""
uuid: 6675f3a2-4d75-4066-bc61-e34e75e48319
updated: 1484310150
title: amazed
categories:
    - Dictionary
---
amazed
     adj : filled with the emotional impact of overwhelming surprise or
           shock; "an amazed audience gave the magician a standing
           ovation"; "I stood enthralled, astonished by the
           vastness and majesty of the cathedral"; "astounded
           viewers wept at the pictures from the Oklahoma City
           bombing"; "stood in stunned silence"; "stunned
           scientists found not one but at least three viruses"
           [syn: {astonied}, {astonished}, {astounded}, {stunned}]
