---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guanine
offline_file: ""
offline_thumbnail: ""
uuid: 39ab2fe9-0121-4883-89cf-944fc9b03057
updated: 1484310297
title: guanine
categories:
    - Dictionary
---
guanine
     n : a purine base found in DNA and RNA; pairs with cytosine
         [syn: {G}]
