---
version: 1
type: video
provider: YouTube
id: cPDptc0wUYI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7ed96ebb-f95a-4ba6-9d0a-dac7904e46d5
updated: 1486069678
title: 'Orbitals- Crash Course Chemistry #25'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/cPDptc0wUYI/default.jpg
    - https://i3.ytimg.com/vi/cPDptc0wUYI/1.jpg
    - https://i3.ytimg.com/vi/cPDptc0wUYI/2.jpg
    - https://i3.ytimg.com/vi/cPDptc0wUYI/3.jpg
---
