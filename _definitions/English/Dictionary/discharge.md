---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discharge
offline_file: ""
offline_thumbnail: ""
uuid: 566dca22-3bc4-4dde-8a9a-aff7b803e46c
updated: 1484310311
title: discharge
categories:
    - Dictionary
---
discharge
     n 1: the sudden giving off of energy
     2: the act of venting [syn: {venting}]
     3: a substance that is emitted or released [syn: {emission}]
     4: any of several bodily processes by which substances go out
        of the body; "the discharge of pus" [syn: {emission}, {expelling}]
     5: electrical conduction through a gas in an applied electric
        field [syn: {spark}, {arc}, {electric arc}, {electric
        discharge}]
     6: the pouring forth of a fluid [syn: {outpouring}, {run}]
     7: the termination of someone's employment (leaving them free
        to depart) [syn: {dismissal}, {dismission}, {firing}, {liberation},
         {release}, {sack}, {sacking}]
 ...
