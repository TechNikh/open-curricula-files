---
version: 1
type: video
provider: YouTube
id: TcynKnEUMfA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1376887f-8303-4126-887c-7f4bc092e689
updated: 1486069607
title: Factoring quadratics example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/TcynKnEUMfA/default.jpg
    - https://i3.ytimg.com/vi/TcynKnEUMfA/1.jpg
    - https://i3.ytimg.com/vi/TcynKnEUMfA/2.jpg
    - https://i3.ytimg.com/vi/TcynKnEUMfA/3.jpg
---
