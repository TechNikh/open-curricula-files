---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Art_gallery_problem
offline_file: ""
offline_thumbnail: ""
uuid: b2c87bca-bba7-41c0-83b8-5d2ff6cb58e9
updated: 1484309168
title: Art gallery problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_10.jpg
tags:
    - Two dimensions
    - "Chvátal's art gallery theorem"
    - "Fisk's short proof"
    - Generalizations
    - Computational complexity
    - Three dimensions
    - Notes
categories:
    - Computational geometry
---
The art gallery problem or museum problem is a well-studied visibility problem in computational geometry. It originates from a real-world problem of guarding an art gallery with the minimum number of guards who together can observe the whole gallery. In the computational geometry version of the problem the layout of the art gallery is represented by a simple polygon and each guard is represented by a point in the polygon. A set 
  
    
      
        S
      
    
    {\displaystyle S}
  
 of points is said to guard a polygon if, for every point 
  
    
      
        p
      
    
    {\displaystyle p}
  
 in the polygon, there is some 
  
    
      
        q
        ∈
        S
     ...
