---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circumscribed_sphere
offline_file: ""
offline_thumbnail: ""
uuid: f83c14a3-dba0-4018-977b-a45c4dcf993b
updated: 1484308932
title: Circumscribed sphere
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-%25D0%2592%25D0%25BF%25D0%25B8%25D1%2581%25D0%25B0%25D0%25BD%25D0%25BD%25D1%258B%25D0%25B9_%25D0%25BA%25D1%2583%25D0%25B1.gif'
tags:
    - Existence and optimality
    - Related concepts
categories:
    - Elementary geometry
---
In geometry, a circumscribed sphere of a polyhedron is a sphere that contains the polyhedron and touches each of the polyhedron's vertices.[1] The word circumsphere is sometimes used to mean the same thing.[2] As in the case of two-dimensional circumscribed circles, the radius of a sphere circumscribed around a polyhedron P is called the circumradius of P,[3] and the center point of this sphere is called the circumcenter of P.[4]
