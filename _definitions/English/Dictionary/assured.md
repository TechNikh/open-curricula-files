---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assured
offline_file: ""
offline_thumbnail: ""
uuid: 0c935e52-e8f4-4b09-a1dc-5236d2b78cc0
updated: 1484310441
title: assured
categories:
    - Dictionary
---
assured
     adj 1: marked by assurance; exhibiting confidence; "she paints with
            an assured hand"
     2: characterized by certainty or security; "a tiny but assured
        income"; "we can never have completely assured lives"
