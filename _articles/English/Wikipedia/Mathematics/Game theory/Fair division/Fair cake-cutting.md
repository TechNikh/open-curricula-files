---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fair_cake-cutting
offline_file: ""
offline_thumbnail: ""
uuid: a955c737-113e-46df-ac3e-3e1d45c3214b
updated: 1484308524
title: Fair cake-cutting
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cut_the_Cake_%25283032677729%2529.jpg'
tags:
    - Assumptions
    - Example cake
    - Justice criteria
    - Additional criteria
    - Results
    - 2 people – proportional and envy-free division
    - Proportional division
    - Envy-free division
    - Efficient division
    - Efficient fair division
categories:
    - Fair division
---
Fair cake-cutting is a kind of fair division problem. The problem involves a heterogeneous resource, such as a cake with different toppings, that is assumed to be divisible – it is possible to cut arbitrarily small pieces of it without destroying their value. The resource has to be divided among several partners who have different preferences over different parts of the cake, i.e., some people prefer the chocolate toppings, some prefer the cherries, some just want as large a piece as possible. The division should be subjectively fair, in that each person should receive a piece that he or she believes to be a fair share.
