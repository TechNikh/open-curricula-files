---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sunwise
offline_file: ""
offline_thumbnail: ""
uuid: ebe40d64-fa9e-4b41-a4c2-0abc9ce183fe
updated: 1484309300
title: Sunwise
categories:
    - Orientation (geometry)
---
In Scottish folklore, Sunwise or Sunward (clockwise) was considered the “prosperous course”, turning from east to west in the direction of the sun. The opposite course was known in Scotland as widdershins (Lowland Scots), or tuathal (Scottish Gaelic),[1] and would have been counterclockwise. It is no coincidence that, in the Northern Hemisphere, "sunwise" and "clockwise" run in the same direction. This is because of the use of the sun as a timekeeper on sundials etc., whose features were in turn transferred to clock faces themselves. Another influence may also have been the right-handed bias in many human cultures.
