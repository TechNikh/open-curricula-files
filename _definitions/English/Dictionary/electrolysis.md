---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electrolysis
offline_file: ""
offline_thumbnail: ""
uuid: f009590a-8c94-4bf7-bc32-58fafc338097
updated: 1484310204
title: electrolysis
categories:
    - Dictionary
---
electrolysis
     n 1: lysis of a bond produced by the passage of an electric
          current
     2: removing superfluous or unwanted hair by passing an electric
        current through the hair root
