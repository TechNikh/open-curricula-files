---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decreasing
offline_file: ""
offline_thumbnail: ""
uuid: 8cdb3695-4408-4a33-a8f5-583b7d801fa4
updated: 1484310220
title: decreasing
categories:
    - Dictionary
---
decreasing
     adj 1: becoming less or smaller [ant: {increasing}]
     2: music [ant: {increasing}]
