---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compound
offline_file: ""
offline_thumbnail: ""
uuid: 0966a0b0-3129-4a4d-bb11-1e162466d117
updated: 1484310371
title: compound
categories:
    - Dictionary
---
compound
     adj 1: of leaf shapes; of leaves composed of several similar parts
            or lobes [ant: {simple}]
     2: consisting of two or more substances or ingredients or
        elements or parts; "soap is a compound substance";
        "housetop is a compound word"; "a blackberry is a compound
        fruit"
     3: composed of many distinct individuals united to form a whole
        or colony; "coral is a colonial organism" [syn: {colonial}]
     n 1: (chemistry) a substance formed by chemical union of two or
          more elements or ingredients in definite proportion by
          weight [syn: {chemical compound}]
     2: a whole formed by a union of two or more elements or ...
