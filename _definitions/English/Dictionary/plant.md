---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plant
offline_file: ""
offline_thumbnail: ""
uuid: de68113a-0ce6-4545-901c-a11ea1b6a070
updated: 1484310303
title: plant
categories:
    - Dictionary
---
plant
     n 1: buildings for carrying on industrial labor; "they built a
          large plant to manufacture automobiles" [syn: {works}, {industrial
          plant}]
     2: a living organism lacking the power of locomotion [syn: {flora},
         {plant life}]
     3: something planted secretly for discovery by another; "the
        police used a plant to trick the thieves"; "he claimed
        that the evidence against him was a plant"
     4: an actor situated in the audience whose acting is rehearsed
        but seems spontaneous to the audience
     v 1: put or set (seeds or seedlings) into the ground; "Let's
          plant flowers in the garden" [syn: {set}]
     2: fix or set ...
