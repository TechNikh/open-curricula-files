---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smooth
offline_file: ""
offline_thumbnail: ""
uuid: c4d91078-1d7a-46ce-ae7e-093d256e38e4
updated: 1484310333
title: smooth
categories:
    - Dictionary
---
smooth
     adj 1: having a surface free from roughness or bumps or ridges or
            irregularities; "smooth skin"; "a smooth tabletop";
            "smooth fabric"; "a smooth road"; "water as smooth as
            a mirror" [ant: {rough}]
     2: smoothly agreeable and courteous with a degree of
        sophistication; "he was too politic to quarrel with so
        important a personage"; "the hostess averted a
        confrontation between two guests with a diplomatic change
        of subject"; "the manager pacified the customer with a
        smooth apology for the error"; "affable, suave, moderate
        men...smugly convinced of their respectability" - Ezra
        Pound [syn: ...
