---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obstinacy
offline_file: ""
offline_thumbnail: ""
uuid: 769c1c04-3dc3-4914-a054-6bd89b8bb706
updated: 1484310583
title: obstinacy
categories:
    - Dictionary
---
obstinacy
     n 1: the trait of being difficult to handle or overcome [syn: {stubbornness},
           {obstinance}, {mulishness}]
     2: resolute adherence to your own ideas or desires [syn: {stubbornness},
         {bullheadedness}, {obstinance}, {pigheadedness}, {self-will}]
