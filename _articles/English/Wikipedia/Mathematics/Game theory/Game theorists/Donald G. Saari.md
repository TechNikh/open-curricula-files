---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Donald_G._Saari
offline_file: ""
offline_thumbnail: ""
uuid: a4599301-7fb2-4257-b0e9-e33098d2d352
updated: 1484309025
title: Donald G. Saari
tags:
    - Books written by Donald Saari
    - Additional reading
categories:
    - Game theorists
---
Donald Gene Saari (born March 1940 in Houghton, Michigan) is the Distinguished Professor of Mathematics and Economics and director of the Institute for Mathematical Behavioral Sciences at the University of California Irvine. He received his Bachelor of Science in Mathematics in 1962 from Michigan Technological University, his Master of Science and PhD in Mathematics from Purdue University in 1964 and 1967, respectively. From 1968 to 2000, he served as assistant, associate, and full professor of mathematics at Northwestern University. He holds the Pacific Institute for the Mathematical Sciences Distinguished Chair at the University of Victoria in British Columbia, Canada. In 1985, with John ...
