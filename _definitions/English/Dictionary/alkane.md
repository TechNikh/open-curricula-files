---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alkane
offline_file: ""
offline_thumbnail: ""
uuid: ec2a3c3f-a2f7-4e82-880e-7d52b0b6fea3
updated: 1484310419
title: alkane
categories:
    - Dictionary
---
alkane
     n : a non-aromatic saturated hydrocarbon with the general
         formula CnH(2n+2) [syn: {methane series}, {alkane series},
          {paraffin}]
