---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/K%C3%A4hler_manifold'
offline_file: ""
offline_thumbnail: ""
uuid: 5c7dc3b5-7dc9-4482-9fad-0be76cc53858
updated: 1484309159
title: Kähler manifold
tags:
    - Definitions
    - Symplectic viewpoint
    - Complex viewpoint
    - Equivalence of definitions
    - Connection between Hermitian and symplectic definitions
    - Kähler potentials
    - Ricci tensor and Kähler manifolds
    - The Laplacians on Kähler manifolds
    - Applications
    - Examples
categories:
    - Algebraic geometry
---
In mathematics and especially differential geometry, a Kähler manifold is a manifold with three mutually compatible structures; a complex structure, a Riemannian structure, and a symplectic structure. On a Kähler manifold X there exists Kähler potential and the Levi-Civita connection corresponding to the metric of X gives rise to a connection on the canonical line bundle.
