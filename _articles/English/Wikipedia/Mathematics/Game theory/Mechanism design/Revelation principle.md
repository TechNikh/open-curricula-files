---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Revelation_principle
offline_file: ""
offline_thumbnail: ""
uuid: 81f655e6-4462-44bd-a485-6bb0c40a0f1e
updated: 1484309050
title: Revelation principle
tags:
    - Example
    - Proof
    - In correlated equilibrium
categories:
    - Mechanism design
---
The revelation principle is a fundamental principle in mechanism design. It states that if a social choice function can be implemented by an arbitrary mechanism (i.e. if that mechanism has an equilibrium outcome that corresponds to the outcome of the social choice function), then the same function can be implemented by an incentive-compatible-direct-mechanism (i.e. in which players truthfully report type) with the same equilibrium outcome (payoffs).[1]:224–225
