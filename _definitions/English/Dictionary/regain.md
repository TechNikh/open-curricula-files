---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regain
offline_file: ""
offline_thumbnail: ""
uuid: 929d13e0-ff64-4aa8-a7c3-7ecc44ef2961
updated: 1484310202
title: regain
categories:
    - Dictionary
---
regain
     v 1: get or find back; recover the use of; "She regained control
          of herself"; "She found her voice and replied quickly"
          [syn: {recover}, {retrieve}, {find}]
     2: come upon after searching; find the location of something
        that was missed or lost; "Did you find your glasses?"; "I
        cannot find my gloves!" [syn: {find}] [ant: {lose}]
