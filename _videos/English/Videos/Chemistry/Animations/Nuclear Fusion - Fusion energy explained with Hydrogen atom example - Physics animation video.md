---
version: 1
type: video
provider: YouTube
id: Cb8NX3HiS4U
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Nuclear%20Fusion%20-%20Fusion%20energy%20explained%20with%20Hydrogen%20atom%20example%20-%20Physics%20animation%20video.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3b6bffd4-c43c-4fd5-ac01-65457605b389
updated: 1486069664
title: >
    Nuclear Fusion | Fusion energy explained with Hydrogen atom
    example | Physics animation video
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Cb8NX3HiS4U/default.jpg
    - https://i3.ytimg.com/vi/Cb8NX3HiS4U/1.jpg
    - https://i3.ytimg.com/vi/Cb8NX3HiS4U/2.jpg
    - https://i3.ytimg.com/vi/Cb8NX3HiS4U/3.jpg
---
