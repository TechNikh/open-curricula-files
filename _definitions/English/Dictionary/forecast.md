---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forecast
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484385301
title: forecast
categories:
    - Dictionary
---
forecast
     n : a prediction about how something (as the weather) will
         develop [syn: {prognosis}]
     v 1: predict in advance [syn: {calculate}]
     2: judge to be probable [syn: {calculate}, {estimate}, {reckon},
         {count on}, {figure}]
     3: indicate by signs; "These signs bode bad news" [syn: {bode},
         {portend}, {auspicate}, {prognosticate}, {omen}, {presage},
         {betoken}, {foreshadow}, {augur}, {foretell}, {prefigure},
         {predict}]
