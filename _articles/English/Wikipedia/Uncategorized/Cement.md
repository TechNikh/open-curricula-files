---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cement
offline_file: ""
offline_thumbnail: ""
uuid: 866a0e1f-0ca1-4b6a-be3f-90ac4d13f1c2
updated: 1484308312
title: Cement
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-USMC-110806-M-IX060-148.jpg
tags:
    - chemistry
    - History
    - Alternatives to cement used in antiquity
    - Macedonians and Romans
    - Middle Ages
    - Cements in the 18th century
    - Cements in the 19th century
    - Cements in the 20th century
    - Modern cements
    - Portland cement
    - Portland cement blends
    - Other cements
    - Setting and curing
    - Safety issues
    - Cement industry in the world
    - China
    - Environmental impacts
    - CO2 emissions
    - Heavy metal emissions in the air
    - Heavy metals present in the clinker
    - Use of alternative fuels and by-products materials
    - Green cement
categories:
    - Uncategorized
---
A cement is a binder, a substance used in construction that sets and hardens and can bind other materials together. The most important types of cement are used as a component in the production of mortar in masonry, and of concrete, which is a combination of cement and an aggregate to form a strong building material.
