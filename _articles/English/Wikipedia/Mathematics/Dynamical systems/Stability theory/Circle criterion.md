---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circle_criterion
offline_file: ""
offline_thumbnail: ""
uuid: b7b9a990-6bc6-4677-9717-585845971359
updated: 1484308837
title: Circle criterion
categories:
    - Stability theory
---
In nonlinear control and stability theory, the circle criterion is a stability criterion for nonlinear time-varying systems. It can be viewed as a generalization of the Nyquist stability criterion for linear time-invariant (LTI) systems.
