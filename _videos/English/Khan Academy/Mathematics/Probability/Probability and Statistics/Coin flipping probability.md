---
version: 1
type: video
provider: YouTube
id: mkyZ45KQYi4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Coin%20flipping%20probability.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 96e57050-f051-4444-ae95-2a2af8651261
updated: 1486069688
title: Coin flipping probability
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/mkyZ45KQYi4/default.jpg
    - https://i3.ytimg.com/vi/mkyZ45KQYi4/1.jpg
    - https://i3.ytimg.com/vi/mkyZ45KQYi4/2.jpg
    - https://i3.ytimg.com/vi/mkyZ45KQYi4/3.jpg
---
