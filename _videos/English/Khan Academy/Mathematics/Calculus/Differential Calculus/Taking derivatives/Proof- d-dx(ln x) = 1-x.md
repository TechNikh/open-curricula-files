---
version: 1
type: video
provider: YouTube
id: yUpDRpkUhf4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Proof-%20d-dx%28ln%20x%29%20%3D%201-x.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 68c72853-5faf-4ff8-a4ea-37c76b3cbffe
updated: 1486069693
title: 'Proof- d/dx(ln x) = 1/x'
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/yUpDRpkUhf4/default.jpg
    - https://i3.ytimg.com/vi/yUpDRpkUhf4/1.jpg
    - https://i3.ytimg.com/vi/yUpDRpkUhf4/2.jpg
    - https://i3.ytimg.com/vi/yUpDRpkUhf4/3.jpg
---
