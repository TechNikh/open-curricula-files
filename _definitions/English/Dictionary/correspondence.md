---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/correspondence
offline_file: ""
offline_thumbnail: ""
uuid: 05de858d-c6f6-47f6-a8e2-9021a2e28a14
updated: 1484310166
title: correspondence
categories:
    - Dictionary
---
correspondence
     n 1: communication by the exchange of letters
     2: compatibility of observations; "there was no agreement
        between theory and measurement"; "the results of two tests
        were in correspondence" [syn: {agreement}]
     3: the relation of correspondence in degree or size or amount
        [syn: {commensurateness}, {proportionateness}]
     4: a function such that for every element of one set there is a
        unique element of another set [syn: {mapping}, {map}]
     5: communication by exchange of letters
     6: (mathematics) an attribute of a shape or relation; exact
        correspondence of form on opposite sides of a dividing
        line or plane ...
