---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plainly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484629021
title: plainly
categories:
    - Dictionary
---
plainly
     adv 1: unmistakably (`plain' is often used informally for
            `plainly'); "the answer is obviously wrong"; "she was
            in bed and evidently in great pain"; "he was
            manifestly too important to leave off the guest list";
            "it is all patently nonsense"; "she has apparently
            been living here for some time"; "I thought he owned
            the property, but apparently not"; "You are plainly
            wrong"; "he is plain stubborn" [syn: {obviously}, {evidently},
             {manifestly}, {patently}, {apparently}, {plain}]
     2: in a simple manner; without extravagance or embellishment;
        "she was dressed plainly"; "they ...
