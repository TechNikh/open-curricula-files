---
version: 1
type: video
provider: YouTube
id: HaFaf7vbgpE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Jupiter%27s%20Moons-%20Crash%20Course%20Astronomy%20%2317.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c0bd899b-ff84-4374-96e7-16281bfda332
updated: 1486069684
title: "Jupiter's Moons- Crash Course Astronomy #17"
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/HaFaf7vbgpE/default.jpg
    - https://i3.ytimg.com/vi/HaFaf7vbgpE/1.jpg
    - https://i3.ytimg.com/vi/HaFaf7vbgpE/2.jpg
    - https://i3.ytimg.com/vi/HaFaf7vbgpE/3.jpg
---
