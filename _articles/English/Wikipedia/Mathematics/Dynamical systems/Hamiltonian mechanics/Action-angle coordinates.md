---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Action-angle_coordinates
offline_file: ""
offline_thumbnail: ""
uuid: 2cc21d06-41a7-40f6-b1d2-279374c984d6
updated: 1484308784
title: Action-angle coordinates
tags:
    - Derivation
    - Summary of basic protocol
    - Degeneracy
categories:
    - Hamiltonian mechanics
---
In classical mechanics, action-angle coordinates are a set of canonical coordinates useful in solving many integrable systems. The method of action-angles is useful for obtaining the frequencies of oscillatory or rotational motion without solving the equations of motion. Action-angle coordinates are chiefly used when the Hamilton–Jacobi equations are completely separable. (Hence, the Hamiltonian does not depend explicitly on time, i.e., the energy is conserved.) Action-angle variables define an invariant torus, so called because holding the action constant defines the surface of a torus, while the angle variables parameterize the coordinates on the torus.
