---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heater
offline_file: ""
offline_thumbnail: ""
uuid: caa3f4eb-fd86-419c-98ab-5cbbbdee1cf3
updated: 1484310228
title: heater
categories:
    - Dictionary
---
heater
     n 1: device that heats water or supplies warmth to a room [syn: {warmer}]
     2: (baseball) a pitch thrown with maximum velocity; "he swung
        late on the fastball"; "he showed batters nothing but
        smoke" [syn: {fastball}, {smoke}, {hummer}, {bullet}]
