---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/determined
offline_file: ""
offline_thumbnail: ""
uuid: 89bfcbdc-5ff2-4c92-a635-b32769cc5f39
updated: 1484310295
title: determined
categories:
    - Dictionary
---
determined
     adj 1: characterized by great determination; "a struggle against a
            determined enemy"
     2: having been learned or found or determined especially by
        investigation [ant: {undetermined}]
     3: devoting full strength and concentrated attention to; "made
        continued and determined efforts to find and destroy enemy
        headquarters"
     4: determined or decided upon as by an authority; "date and
        place are already determined"; "the dictated terms of
        surrender"; "the time set for the launching" [syn: {dictated},
         {set}]
     5: strongly motivated to succeed [syn: {compulsive}, {driven}]
