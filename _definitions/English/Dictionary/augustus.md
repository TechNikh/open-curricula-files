---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/augustus
offline_file: ""
offline_thumbnail: ""
uuid: bb76bbf3-455b-4e17-be42-db40016eef71
updated: 1484310290
title: augustus
categories:
    - Dictionary
---
Augustus
     n : Roman statesman who  established the Roman Empire and became
         emperor in 27 BC; defeated Mark Antony and Cleopatra in
         31 BC at Actium (63 BC - AD 14) [syn: {Gaius Octavianus},
          {Gaius Julius Caesar Octavianus}, {Octavian}]
