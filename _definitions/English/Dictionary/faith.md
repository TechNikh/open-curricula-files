---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faith
offline_file: ""
offline_thumbnail: ""
uuid: 70239e75-34ba-4b8c-bfdb-c2f07265e8be
updated: 1484310565
title: faith
categories:
    - Dictionary
---
faith
     n 1: a strong belief in a supernatural power or powers that
          control human destiny; "he lost his faith but not his
          morality" [syn: {religion}, {religious belief}]
     2: complete confidence in a person or plan etc; "he cherished
        the faith of a good woman"; "the doctor-patient
        relationship is based on trust" [syn: {trust}]
     3: institution to express belief in a divine power; "he was
        raised in the Baptist religion"; "a member of his own
        faith contradicted him" [syn: {religion}]
     4: loyalty or allegiance to a cause or a person; "keep the
        faith"; "they broke faith with their investors"
