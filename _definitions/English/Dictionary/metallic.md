---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/metallic
offline_file: ""
offline_thumbnail: ""
uuid: d71b1838-d62f-4317-9e67-50b4efb432fa
updated: 1484310377
title: metallic
categories:
    - Dictionary
---
metallic
     adj : containing or made of or resembling or characteristic of a
           metal; "a metallic compound"; "metallic luster"; "the
           strange metallic note of the meadow lark, suggesting
           the clash of vibrant blades"- Ambrose Bierce [syn: {metal(a)}]
           [ant: {nonmetallic}]
