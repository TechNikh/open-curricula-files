---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Envelope_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 228ebe95-476a-49da-ad9c-7052c9bc5912
updated: 1484309162
title: Envelope (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/EnvelopeAnim.gif
tags:
    - Envelope of a family of curves
    - Alternative definitions
    - Examples
    - Example 1
    - Example 2
    - Example 3
    - Example 4
    - Example 5
    - Envelope of a family of surfaces
    - Generalisations
    - Applications
    - Ordinary differential equations
    - Partial differential equations
    - Caustics
    - "Huygens's principle"
categories:
    - Analytic geometry
---
In geometry, an envelope of a family of curves in the plane is a curve that is tangent to each member of the family at some point. Classically, a point on the envelope can be thought of as the intersection of two "adjacent" curves, meaning the limit of intersections of nearby curves. This idea can be generalized to an envelope of surfaces in space, and so on to higher dimensions.
