---
version: 1
type: article
id: https://en.wikipedia.org/wiki/East
offline_file: ""
offline_thumbnail: ""
uuid: bcb1f1e7-3dff-4846-a448-1076711db937
updated: 1484309293
title: East
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Compass_Rose_English_East.svg.png
tags:
    - Etymology
    - Navigation
    - Cultural
categories:
    - Orientation (geometry)
---
