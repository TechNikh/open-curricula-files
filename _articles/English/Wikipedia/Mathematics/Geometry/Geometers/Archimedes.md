---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Archimedes
offline_file: ""
offline_thumbnail: ""
uuid: 182dc17b-69bb-45da-8665-92bf44687b0d
updated: 1484309069
title: Archimedes
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cicero_Discovering_the_Tomb_of_Archimedes_by_Benjamin_West.jpeg
tags:
    - Biography
    - Discoveries and inventions
    - "Archimedes' principle"
    - "Archimedes' screw"
    - Claw of Archimedes
    - Heat ray
    - Other discoveries and inventions
    - Mathematics
    - Writings
    - Surviving works
    - Apocryphal works
    - Archimedes Palimpsest
    - Legacy
    - Notes
    - The Works of Archimedes online
categories:
    - Geometers
---
Archimedes of Syracuse (/ˌɑːkɪˈmiːdiːz/;[2] Greek: Ἀρχιμήδης; c. 287 BC – c. 212 BC) was an Ancient Greek mathematician, physicist, engineer, inventor, and astronomer.[3] Although few details of his life are known, he is regarded as one of the leading scientists in classical antiquity. Generally considered the greatest mathematician of antiquity and one of the greatest of all time,[4][5] Archimedes anticipated modern calculus and analysis by applying concepts of infinitesimals and the method of exhaustion to derive and rigorously prove a range of geometrical theorems, including the area of a circle, the surface area and volume of a sphere, and the area under a ...
