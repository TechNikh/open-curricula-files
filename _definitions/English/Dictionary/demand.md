---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demand
offline_file: ""
offline_thumbnail: ""
uuid: 85d16f76-0d90-43bf-8a09-25986f114908
updated: 1484310244
title: demand
categories:
    - Dictionary
---
demand
     n 1: the ability and desire to purchase goods and services; "the
          automobile reduced the demand for buggywhips"; "the
          demand exceeded the supply" [ant: {supply}]
     2: an urgent or peremptory request; "his demands for attention
        were unceasing"
     3: a condition requiring relief; "she satisfied his need for
        affection"; "God has no need of men to accomplish His
        work"; "there is a demand for jobs" [syn: {need}]
     4: the act of demanding; "the kidnapper's exorbitant demands
        for money"
     5: required activity; "the requirements of his work affected
        his health"; "there were many demands on his time" [syn: ...
