---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nimrod_Megiddo
offline_file: ""
offline_thumbnail: ""
uuid: f3f20686-6090-4709-96fc-3e7ade00485f
updated: 1484308504
title: Nimrod Megiddo
categories:
    - Game theorists
---
Nimrod Megiddo (Hebrew: נמרוד מגידו‎‎) is a mathematician and computer scientist. He is a research scientist at the IBM Almaden Research Center.
