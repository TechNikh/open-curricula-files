---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dull
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484583781
title: dull
categories:
    - Dictionary
---
dull
     adj 1: lacking in liveliness or animation; "he was so dull at
            parties"; "a dull political campaign"; "a large dull
            impassive man"; "dull days with nothing to do"; "how
            dull and dreary the world is"; "fell back into one of
            her dull moods" [ant: {lively}]
     2: emitting or reflecting very little light; "a dull glow";
        "dull silver badly in need of a polish"; "a dull sky"
        [ant: {bright}]
     3: being or made softer or less loud or clear; "the dull boom
        of distant breaking waves"; "muffled drums"; "the muffled
        noises of the street"; "muted trumpets" [syn: {muffled}, {muted},
         {softened}]
     4: ...
