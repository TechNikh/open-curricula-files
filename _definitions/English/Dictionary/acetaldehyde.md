---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acetaldehyde
offline_file: ""
offline_thumbnail: ""
uuid: 15e3d131-0da2-4efa-aa7a-7c5f9915c7ae
updated: 1484310419
title: acetaldehyde
categories:
    - Dictionary
---
acetaldehyde
     n : a colorless volatile water-soluble liquid aldehyde used
         chiefly in manufacture of acetic acid and perfumes and
         drugs [syn: {ethanal}]
