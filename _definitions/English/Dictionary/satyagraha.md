---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/satyagraha
offline_file: ""
offline_thumbnail: ""
uuid: 79507182-92cc-4f0c-859a-af3dc30e8dc2
updated: 1484310384
title: satyagraha
categories:
    - Dictionary
---
Satyagraha
     n : the form of nonviolent resistance initiated in India by
         Mahatma Gandhi in order to oppose British rule and to
         hasten political reforms
