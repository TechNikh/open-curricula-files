---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/running
offline_file: ""
offline_thumbnail: ""
uuid: 337fe56b-aaa6-43e0-9d7e-dce8c85005ee
updated: 1484310258
title: running
categories:
    - Dictionary
---
running
     adj 1: moving quickly on foot; "heard running footsteps behind him"
            [syn: {running(a)}]
     2: (of fluids) moving or issuing in a stream; "a mountain
        stream with freely running water"; "hovels without running
        water" [syn: {running(a)}] [ant: {standing(a)}]
     3: continually repeated over a period of time; "a running joke
        among us" [syn: {running(a)}]
     4: of advancing the ball by running; "the team's running plays
        worked better than its pass plays" [syn: {running(a)}]
        [ant: {passing(a)}]
     5: executed or initiated by running; "running plays worked
        better than pass plays"; "took a running jump"; "a running
     ...
