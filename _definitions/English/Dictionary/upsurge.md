---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/upsurge
offline_file: ""
offline_thumbnail: ""
uuid: 76e02f31-9ede-4ea0-8187-c3aa55e325fa
updated: 1484310587
title: upsurge
categories:
    - Dictionary
---
upsurge
     n 1: a sudden forceful flow [syn: {rush}, {spate}, {surge}]
     2: a sudden or abrupt strong increase; "stimulated a surge of
        speculation"; "an upsurge of emotion"; "an upsurge in
        violent crime" [syn: {surge}]
