---
version: 1
type: video
provider: YouTube
id: GIJK3dwCWCw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9c019347-b71d-476e-812f-60fed4380670
updated: 1486069681
title: 'Immune System, part 1- Crash Course A&P #45'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/GIJK3dwCWCw/default.jpg
    - https://i3.ytimg.com/vi/GIJK3dwCWCw/1.jpg
    - https://i3.ytimg.com/vi/GIJK3dwCWCw/2.jpg
    - https://i3.ytimg.com/vi/GIJK3dwCWCw/3.jpg
---
