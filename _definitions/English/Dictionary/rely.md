---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rely
offline_file: ""
offline_thumbnail: ""
uuid: 99fa22ee-4954-4606-a469-34d56e9161bc
updated: 1484310246
title: rely
categories:
    - Dictionary
---
rely
     v : have confidence or faith in; "We can trust in God"; "Rely on
         your friends"; "bank on your good education"; "I swear by
         my grandmother's recipes" [syn: {trust}, {swear}, {bank}]
         [ant: {distrust}, {distrust}]
     [also: {relied}]
