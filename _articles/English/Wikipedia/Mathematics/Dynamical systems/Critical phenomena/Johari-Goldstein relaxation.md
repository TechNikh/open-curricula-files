---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Johari-Goldstein_relaxation
offline_file: ""
offline_thumbnail: ""
uuid: 6feaf386-fb55-4100-b3fe-e49f49360b97
updated: 1484308746
title: Johari-Goldstein relaxation
categories:
    - Critical phenomena
---
Johari-Goldstein relaxation, also known as the JG β-relaxation, is a universal property of glasses and certain other disordered materials. Initially posited in 1969 by Martin Goldstein, its existence was proved experimentally by Gyan P. Johari and Martin Goldstein in 1970 by experiments on glasses made from rigid molecules. The relaxation, a peak in mechanical or dielectric loss at a particular frequency, had previously been attributed to a type of molecular flexibility. The fact that such a loss peak shows up in glasses of rigid molecules lacking this flexibility demonstrated its universal character.
