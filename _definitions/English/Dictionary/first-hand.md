---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/first-hand
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484637601
title: first-hand
categories:
    - Dictionary
---
firsthand
     adj : received directly from a source; "firsthand information"
     adv : at first hand or directly; "I heard this story firsthand"
