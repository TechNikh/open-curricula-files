---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vortex_ring_gun
offline_file: ""
offline_thumbnail: ""
uuid: e916c70a-42b8-48ee-b3ad-2924eda746f4
updated: 1484308878
title: Vortex ring gun
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Vortex_Ring_Gun_Test.jpg
tags:
    - Operation
    - History
    - The 1998 US Army project
    - The Hail cannon
categories:
    - Vortices
---
The vortex ring gun is an experimental non-lethal weapon for crowd control that uses high-energy vortex rings of gas to knock down people or spray them with marking ink or other chemicals.
