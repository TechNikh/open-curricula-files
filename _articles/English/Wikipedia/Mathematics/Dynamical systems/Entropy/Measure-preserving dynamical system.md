---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Measure-preserving_dynamical_system
offline_file: ""
offline_thumbnail: ""
uuid: 4a9a4336-5e3d-4082-ad26-dcf1fd6ce253
updated: 1484308771
title: Measure-preserving dynamical system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Exampleergodicmap.svg.png
tags:
    - Definition
    - Examples
    - Homomorphisms
    - Generic points
    - Symbolic names and generators
    - Operations on partitions
    - Measure-theoretic entropy
    - Examples
categories:
    - Entropy
---
In mathematics, a measure-preserving dynamical system is an object of study in the abstract formulation of dynamical systems, and ergodic theory in particular.
