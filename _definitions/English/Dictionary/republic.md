---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/republic
offline_file: ""
offline_thumbnail: ""
uuid: ff127e45-e69e-4647-a5f7-d4782c9ae16f
updated: 1484310557
title: republic
categories:
    - Dictionary
---
republic
     n 1: a political system in which the supreme power lies in a body
          of citizens who can elect people to represent them [syn:
           {democracy}, {commonwealth}] [ant: {autocracy}]
     2: a form of government whose head of state is not a monarch;
        "the head of state in a republic is usually a president"
