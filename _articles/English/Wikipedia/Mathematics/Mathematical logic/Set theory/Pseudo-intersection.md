---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pseudo-intersection
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1482181201
title: Pseudo-intersection
categories:
    - Set theory
---
In mathematical set theory, a pseudo-intersection of a family of sets is an infinite set S such that each element of the family contains all but a finite number of elements of S. The pseudo-intersection number, sometimes denoted by the fraktur letter 𝔭, is the smallest size of a family of infinite subsets of the natural numbers that has the strong finite intersection property but has no pseudo-intersection.
