---
version: 1
type: video
provider: YouTube
id: 01QWC-rZcfE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Cycles%20in%20the%20Sky-%20Crash%20Course%20Astronomy%20%233.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1433e11a-a159-4f21-90b6-5149c9ee8be9
updated: 1486069685
title: 'Cycles in the Sky- Crash Course Astronomy #3'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/01QWC-rZcfE/default.jpg
    - https://i3.ytimg.com/vi/01QWC-rZcfE/1.jpg
    - https://i3.ytimg.com/vi/01QWC-rZcfE/2.jpg
    - https://i3.ytimg.com/vi/01QWC-rZcfE/3.jpg
---
