---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anal
offline_file: ""
offline_thumbnail: ""
uuid: c49ff9a2-cbc8-4b26-a9a0-bf15785278a8
updated: 1484310316
title: anal
categories:
    - Dictionary
---
anal
     adj 1: of or related to the anus; "anal thermometer"
     2: a stage in psychosexual development when the child's
        interest is concentrated on the anal region; fixation at
        this stage is said to result in orderliness, meanness,
        stubbornness, compulsiveness, etc. [ant: {oral}]
