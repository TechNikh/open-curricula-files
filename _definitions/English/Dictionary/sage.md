---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sage
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484510821
title: sage
categories:
    - Dictionary
---
sage
     adj 1: having wisdom that comes with age and experience
     2: of the gray-green color of sage leaves [syn: {sage-green}]
     n 1: a mentor in spiritual and philosophical topics who is
          renowned for profound wisdom
     2: aromatic fresh or dried gray-green leaves used widely as
        seasoning for meats and fowl and game etc
     3: any of various plants of the genus Salvia; a cosmopolitan
        herb [syn: {salvia}]
