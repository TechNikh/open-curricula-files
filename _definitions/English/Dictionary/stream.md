---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stream
offline_file: ""
offline_thumbnail: ""
uuid: 0fa5f487-7713-44cd-bd55-14d861a0290a
updated: 1484310319
title: stream
categories:
    - Dictionary
---
stream
     n 1: a natural body of running water flowing on or under the
          earth [syn: {watercourse}]
     2: dominant course (suggestive of running water) of successive
        events or ideas; "two streams of development run through
        American history"; "stream of consciousness"; "the flow of
        thought"; "the current of history" [syn: {flow}, {current}]
     3: a steady flow (usually from natural causes); "the raft
        floated downstream on the current"; "he felt a stream of
        air" [syn: {current}]
     4: the act of flowing or streaming; continuous progression
        [syn: {flow}]
     5: something that resembles a flowing stream in moving
        ...
