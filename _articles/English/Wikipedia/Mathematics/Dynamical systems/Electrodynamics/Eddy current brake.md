---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eddy_current_brake
offline_file: ""
offline_thumbnail: ""
uuid: 16b54ef0-e665-442c-a9cf-3e32f771846c
updated: 1484308751
title: Eddy current brake
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Wirbelstrombremse_aktiv.jpg
tags:
    - How it works
    - Disk eddy current brakes
    - Linear eddy current brakes
    - Lab experiment
    - Notes
categories:
    - Electrodynamics
---
An eddy current brake, like a conventional friction brake, is a device used to slow or stop a moving object by dissipating its kinetic energy as heat. However, unlike electro-mechanical brakes, in which the drag force used to stop the moving object is provided by friction between two surfaces pressed together, the drag force in an eddy current brake is an electromagnetic force between a magnet and a nearby conductive object in relative motion, due to eddy currents induced in the conductor through electromagnetic induction.
