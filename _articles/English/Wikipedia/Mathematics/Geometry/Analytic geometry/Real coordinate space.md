---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Real_coordinate_space
offline_file: ""
offline_thumbnail: ""
uuid: 7727d06a-6bef-458b-ae52-4afb3bdd754f
updated: 1484309165
title: Real coordinate space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Real_2-space_as_square_grid.png
tags:
    - Definition and uses
    - The domain of a function of several variables
    - Vector space
    - Matrix notation
    - Standard basis
    - Geometric properties and uses
    - Orientation
    - Affine space
    - Convexity
    - Euclidean space
    - In algebraic and differential geometry
    - Other appearances
    - Polytopes in Rn
    - Topological properties
    - Examples
    - n ≤ 1
    - n = 2
    - n = 3
    - n = 4
    - Generalizations
    - Footnotes
categories:
    - Analytic geometry
---
In mathematics, real coordinate space of n dimensions, written Rn (/ɑːrˈɛn/ ar-EN) (also written ℝn with blackboard bold) is a coordinate space that allows several (n) real variables to be treated as a single variable. With various numbers of dimensions (sometimes unspecified), Rn is used in many areas of pure and applied mathematics, as well as in physics. With component-wise addition and scalar multiplication, it is the prototypical real vector space and is a frequently used representation of Euclidean n-space. Due to the latter fact, geometric metaphors are widely used for Rn, namely a plane for R2 and three-dimensional space for R3.
