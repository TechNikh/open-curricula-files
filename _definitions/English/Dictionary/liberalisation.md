---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liberalisation
offline_file: ""
offline_thumbnail: ""
uuid: 2281d0a7-df10-4446-9558-37f5a14110ae
updated: 1484310527
title: liberalisation
categories:
    - Dictionary
---
liberalisation
     n : the act of making less strict [syn: {liberalization}, {relaxation}]
