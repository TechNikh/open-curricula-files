---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/west
offline_file: ""
offline_thumbnail: ""
uuid: 2b22cf84-34b3-4056-874f-f83dd9191aa7
updated: 1484310281
title: west
categories:
    - Dictionary
---
west
     adj : situated in or facing or moving toward the west [ant: {east}]
     n 1: the countries of (originally) Europe and (now including)
          North and South America [syn: {Occident}]
     2: the cardinal compass point that is a 270 degrees [syn: {due
        west}, {W}]
     3: the region of the United States lying to the west of the
        Mississippi River [syn: {western United States}]
     4: British writer (born in Ireland) (1892-1983) [syn: {Rebecca
        West}, {Dame Rebecca West}, {Cicily Isabel Fairfield}]
     5: United States film actress (1892-1980) [syn: {Mae West}]
     6: English painter (born in America) who became the second
        president of the Royal ...
