---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Apollonius_of_Perga
offline_file: ""
offline_thumbnail: ""
uuid: d82b4ff4-177f-4b30-817d-9975fe62eb31
updated: 1484309244
title: Apollonius of Perga
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Parabola_connection_with_areas_of_a_square_and_a_rectangle.gif
tags:
    - Conics
    - Other works
    - De Rationis Sectione
    - De Spatii Sectione
    - De Sectione Determinata
    - De Tactionibus
    - De Inclinationibus
    - De Locis Planis
    - Additional works
    - Published editions
    - Notes
categories:
    - History of geometry
---
Apollonius of Perga (Greek: Ἀπολλώνιος; Latin: Apollonius Pergaeus; c. 262 BC – c. 190 BC) was a Greek geometer and astronomer noted for his writings on conic sections. His innovative methodology and terminology, especially in the field of conics, influenced many later scholars including Ptolemy, Francesco Maurolico, Johannes Kepler, Isaac Newton, and René Descartes. Apollonius gave the ellipse, the parabola, and the hyperbola their modern names. The hypothesis of eccentric orbits, or equivalently, deferent and epicycles, to explain the apparent motion of the planets and the varying speed of the Moon, is also attributed to him. Ptolemy describes Apollonius' theorem in the ...
