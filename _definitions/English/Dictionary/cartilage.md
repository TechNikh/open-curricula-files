---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cartilage
offline_file: ""
offline_thumbnail: ""
uuid: eab362eb-a296-499e-9243-4bc905ea735f
updated: 1484310268
title: cartilage
categories:
    - Dictionary
---
cartilage
     n : tough elastic tissue; mostly converted to bone in adults
         [syn: {gristle}]
