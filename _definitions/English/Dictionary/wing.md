---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wing
offline_file: ""
offline_thumbnail: ""
uuid: 6e5c0d39-bfab-4c7a-ac50-d0ec68a09f01
updated: 1484310284
title: wing
categories:
    - Dictionary
---
wing
     n 1: a movable organ for flying (one of a pair)
     2: one of the horizontal airfoils on either side of the
        fuselage of an airplane
     3: a stage area out of sight of the audience [syn: {offstage},
        {backstage}]
     4: a unit of military aircraft
     5: the side of military or naval formation; "they attacked the
        enemy's right flank" [syn: {flank}]
     6: a hockey player stationed in a forward positin on either
        side
     7: the wing of a fowl; "he preferred the drumsticks to the
        wings"
     8: a barrier that surrounds the wheels of a vehicle to block
        splashing water or mud; "in England they call a fender a
        wing" [syn: ...
