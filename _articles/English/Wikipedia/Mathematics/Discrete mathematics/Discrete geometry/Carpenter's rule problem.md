---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Carpenter%27s_rule_problem'
offline_file: ""
offline_thumbnail: ""
uuid: f4fe880a-43dd-4f1b-8c39-c56cc26e3bf4
updated: 1484308614
title: "Carpenter's rule problem"
tags:
    - Combinatorial proof
    - Generalization
categories:
    - Discrete geometry
---
The carpenter's rule problem is a discrete geometry problem, which can be stated in the following manner: Can a simple planar polygon be moved continuously to a position where all its vertices are in convex position, so that the edge lengths and simplicity are preserved along the way? A closely related problem is to show that any polygon can be convexified, that is, continuously transformed, preserving edge distances and avoiding crossings, into a convex polygon.
