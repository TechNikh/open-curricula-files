---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spoiling
offline_file: ""
offline_thumbnail: ""
uuid: 61f53621-9686-45f0-9bcb-e0fcefc46cf4
updated: 1484310377
title: spoiling
categories:
    - Dictionary
---
spoiling
     n 1: the process of becoming spoiled [syn: {spoilage}]
     2: the act of spoiling something by causing damage to it; "her
        spoiling my dress was deliberate" [syn: {spoil}, {spoilage}]
