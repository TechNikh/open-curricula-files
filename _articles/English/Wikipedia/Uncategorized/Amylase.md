---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Amylase
offline_file: ""
offline_thumbnail: ""
uuid: 9cf1c831-3b13-401a-8715-32fb55137068
updated: 1484308378
title: Amylase
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Salivary_alpha-amylase_1SMD.png
tags:
    - Classification
    - α-Amylase
    - β-Amylase
    - γ-Amylase
    - Uses
    - Fermentation
    - Flour additive
    - Molecular biology
    - Medical uses
    - Other uses
    - Hyperamylasemia
    - History
    - Human evolution
categories:
    - Uncategorized
---
An amylase (/ˈæmᵻleɪs/) is an enzyme that catalyses the hydrolysis of starch into sugars. Amylase is present in the saliva of humans and some other mammals, where it begins the chemical process of digestion. Foods that contain large amounts of starch but little sugar, such as rice and potatoes, may acquire a slightly sweet taste as they are chewed because amylase degrades some of their starch into sugar. The pancreas and salivary gland make amylase (alpha amylase) to hydrolyse dietary starch into disaccharides and trisaccharides which are converted by other enzymes to glucose to supply the body with energy. Plants and some bacteria also produce amylase. As diastase, amylase was the ...
