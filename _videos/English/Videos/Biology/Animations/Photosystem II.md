---
version: 1
type: video
provider: YouTube
id: 3UfV060N27g
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Photosystem%20II.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bc0152d8-ba32-40b0-8dd3-69dd46d7a7eb
updated: 1486069669
title: Photosystem II
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/3UfV060N27g/default.jpg
    - https://i3.ytimg.com/vi/3UfV060N27g/1.jpg
    - https://i3.ytimg.com/vi/3UfV060N27g/2.jpg
    - https://i3.ytimg.com/vi/3UfV060N27g/3.jpg
---
