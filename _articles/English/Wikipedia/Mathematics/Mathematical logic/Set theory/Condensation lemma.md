---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Condensation_lemma
offline_file: ""
offline_thumbnail: ""
uuid: ad4e28ff-6726-475e-bb51-df0efaa5570e
updated: 1484309501
title: Condensation lemma
categories:
    - Set theory
---
It states that if X is a transitive set and is an elementary submodel of some level of the constructible hierarchy Lα, that is, 
  
    
      
        (
        X
        ,
        ∈
        )
        ≺
        (
        
          L
          
            α
          
        
        ,
        ∈
        )
      
    
    {\displaystyle (X,\in )\prec (L_{\alpha },\in )}
  
, then in fact there is some ordinal 
  
    
      
        β
        ≤
        α
      
    
    {\displaystyle \beta \leq \alpha }
  
 such that 
  
    
      
        X
        =
        
          L
          
            β
          
        
      
    
    {\displaystyle X=L_{\beta }}
  
.
