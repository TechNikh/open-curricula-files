---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Method_of_distinguished_element
offline_file: ""
offline_thumbnail: ""
uuid: 0ed7d304-9a96-4a6e-bb95-d377a3cd8462
updated: 1484308585
title: Method of distinguished element
tags:
    - Definition
    - Examples
categories:
    - Combinatorics
---
In the mathematical field of enumerative combinatorics, identities are sometimes established by arguments that rely on singling out one "distinguished element" of a set.
