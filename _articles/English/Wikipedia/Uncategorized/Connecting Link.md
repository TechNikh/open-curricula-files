---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Connecting_links
offline_file: ""
offline_thumbnail: ""
uuid: c2c040ba-f55d-49c8-b36e-8489605bee6a
updated: 1484308378
title: Connecting Link
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Connecting_Link_Sign_24_Cambridge.png
tags:
    - Current links
categories:
    - Uncategorized
---
The Connecting Link program is a provincial subsidy provided to municipalities to assist with road construction, maintenance and repairs in the Canadian province of Ontario. Roads which are designated as connecting links form the portions of provincial highways through built-up communities which are not owned by the Ministry of Transportation (MTO). Connecting links are governed by several regulations, including section 144, subsection 31.1 of the Highway Traffic Act and section 21 of the Public Transportation and Highway Improvement Act. While the road is under local control and can be modified to their needs, extensions and traffic signals require the approval of the MTO to be ...
