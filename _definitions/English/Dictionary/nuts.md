---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nuts
offline_file: ""
offline_thumbnail: ""
uuid: 4cbc20c1-2c9b-4ed3-99be-82ae6bb26c0a
updated: 1484310529
title: nuts
categories:
    - Dictionary
---
nuts
     adj : informal or slang terms for mentally irregular; "it used to
           drive my husband balmy" [syn: {balmy}, {barmy}, {bats},
            {batty}, {bonkers}, {buggy}, {cracked}, {crackers}, {daft},
            {dotty}, {fruity}, {haywire}, {kooky}, {kookie}, {loco},
            {loony}, {loopy}, {nutty}, {round the bend}, {around
           the bend}, {wacky}, {whacky}]
