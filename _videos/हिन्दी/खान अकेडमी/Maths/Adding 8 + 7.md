---
version: 1
type: video
provider: YouTube
id: oAv5sEwiKOk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 786031bd-91fa-461c-b5ba-6aade348b95e
updated: 1486069613
title: Adding 8 + 7
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/oAv5sEwiKOk/default.jpg
    - https://i3.ytimg.com/vi/oAv5sEwiKOk/1.jpg
    - https://i3.ytimg.com/vi/oAv5sEwiKOk/2.jpg
    - https://i3.ytimg.com/vi/oAv5sEwiKOk/3.jpg
---
