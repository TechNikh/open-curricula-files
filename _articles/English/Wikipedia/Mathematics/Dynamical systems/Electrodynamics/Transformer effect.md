---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transformer_effect
offline_file: ""
offline_thumbnail: ""
uuid: e376dd40-3bef-412f-8e44-49b24f575f30
updated: 1484308762
title: Transformer effect
categories:
    - Electrodynamics
---
The transformer effect, or mutual induction, is one of the processes by which an electromotive force (e.m.f.) is induced. In a transformer, a changing electric current in a primary coil creates a changing magnetic field that induces a current in a secondary coil.
