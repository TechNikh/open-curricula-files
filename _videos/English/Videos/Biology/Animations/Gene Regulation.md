---
version: 1
type: video
provider: YouTube
id: 3S3ZOmleAj0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Gene%20Regulation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ae81a754-9078-4de4-a276-4b24293bfb31
updated: 1486069672
title: Gene Regulation
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/3S3ZOmleAj0/default.jpg
    - https://i3.ytimg.com/vi/3S3ZOmleAj0/1.jpg
    - https://i3.ytimg.com/vi/3S3ZOmleAj0/2.jpg
    - https://i3.ytimg.com/vi/3S3ZOmleAj0/3.jpg
---
