---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oblique
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484547781
title: oblique
categories:
    - Dictionary
---
oblique
     adj 1: slanting or inclined in direction or course or
            position--neither parallel nor perpendicular nor
            right-angular; "the oblique rays of the winter sun";
            "acute and obtuse angles are oblique angles"; "the
            axis of an oblique cone is not perpendicular to its
            base" [ant: {parallel}, {perpendicular}]
     2: indirect in departing from the accepted or proper way;
        misleading; "used devious means to achieve success"; "gave
        oblique answers to direct questions"; "oblique political
        maneuvers" [syn: {devious}]
     n 1: any grammatical case other than the nominative [syn: {oblique
          case}] [ant: ...
