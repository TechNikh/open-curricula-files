---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nettle
offline_file: ""
offline_thumbnail: ""
uuid: 85bafc1b-ec02-427c-8db1-7e95ae8379dc
updated: 1484310384
title: nettle
categories:
    - Dictionary
---
nettle
     n : any of numerous plants having stinging hairs that cause skin
         irritation on contact (especially of the genus Urtica or
         family Urticaceae)
     v 1: sting with or as with nettles and cause a stinging pain or
          sensation [syn: {urticate}]
     2: cause annoyance in; disturb, especially by minor
        irritations; "Mosquitoes buzzing in my ear really bothers
        me"; "It irritates me that she never closes the door after
        she leaves" [syn: {annoy}, {rag}, {get to}, {bother}, {get
        at}, {irritate}, {rile}, {nark}, {gravel}, {vex}, {chafe},
         {devil}]
