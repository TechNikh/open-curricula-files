---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carboxy
offline_file: ""
offline_thumbnail: ""
uuid: 98374b00-62db-4421-9d85-c8c7b894bc9e
updated: 1484308403
title: Carboxylic acid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Carboxylic-acid.svg_1.png
tags:
    - Example carboxylic acids and nomenclature
    - Carboxyl radical
    - Physical properties
    - Solubility
    - Boiling points
    - Acidity
    - Odor
    - Characterization
    - Occurrence and applications
    - Synthesis
    - Industrial routes
    - Laboratory methods
    - Less-common reactions
    - Reactions
    - Specialized reactions
categories:
    - Uncategorized
---
A carboxylic acid /ˌkɑːrbɒkˈsɪlɪk/ is an organic compound that contains a carboxyl group (C(=O)OH).[1] The general formula of a carboxylic acid is R–COOH, with R referring to the rest of the (possibly quite large) molecule. Carboxylic acids occur widely and include the amino acids (which make up proteins) and acetic acid (which is part of vinegar and occurs in metabolism).
