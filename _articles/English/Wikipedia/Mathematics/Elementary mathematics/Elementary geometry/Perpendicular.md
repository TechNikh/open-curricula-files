---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perpendicular
offline_file: ""
offline_thumbnail: ""
uuid: b09fbf48-9c6e-41b1-b340-e7942899c894
updated: 1484308938
title: Perpendicular
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/236px-Perpendicular-coloured.svg.png
tags:
    - Foot of a perpendicular
    - Construction of the perpendicular
    - In relationship to parallel lines
    - In computing distances
    - Graph of functions
    - In circles and other conics
    - Circles
    - Ellipses
    - Parabolas
    - Hyperbolas
    - In polygons
    - Triangles
    - Quadrilaterals
    - Lines in three dimensions
    - Notes
categories:
    - Elementary geometry
---
In elementary geometry, the property of being perpendicular (perpendicularity) is the relationship between two lines which meet at a right angle (90 degrees). The property extends to other related geometric objects.
