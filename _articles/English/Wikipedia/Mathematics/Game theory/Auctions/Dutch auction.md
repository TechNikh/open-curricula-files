---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dutch_auction
offline_file: ""
offline_thumbnail: ""
uuid: 3c759b86-acf1-48b7-8144-5c3f6eecf441
updated: 1484309011
title: Dutch auction
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Bundesarchiv_B_145_Bild-F004491-0002%252C_Kirschenversteigerung_an_der_Mosel.jpg'
tags:
    - Auction process
    - A second price auction
    - Public offerings
    - Dutch auction share repurchases
    - Notes
categories:
    - Auctions
---
A Dutch auction is a type of auction in which the auctioneer begins with a high asking price which is lowered until some participant is willing to accept the auctioneer's price, or a predetermined reserve price (the seller's minimum acceptable price) is reached. The winning participant pays the last announced price. This is also known as a clock auction or an open-outcry descending-price auction.
