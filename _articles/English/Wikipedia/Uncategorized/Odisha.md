---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Odisha
offline_file: ""
offline_thumbnail: ""
uuid: e9938ba4-94bb-44e3-89f6-a8dcea23a90a
updated: 1484308425
title: Odisha
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Hathigumpha.JPG
tags:
    - Etymology
    - History
    - geography
    - Climate
    - biodiversity
    - Government and politics
    - Legislative assembly
    - Administrative units
    - Economy
    - Macro-economic trend
    - Industrial growth
    - Infrastructure development
    - Transportation
    - air
    - Seaports
    - Railways
    - Demographics
    - religion
    - Education
    - Kalinga Prize
    - culture
    - Cuisine
    - Literature
    - Dance
    - Cinema
    - Music
    - Structural art
    - Tourism
categories:
    - Uncategorized
---
Odisha (pronunciation: i/əˈdɪsə/;[4])(formerly "Orissa")[5][6] (/ɒˈrɪsə, ɔː-, oʊ-/;[7](Odia: ଓଡ଼ିଶା) is one of the 29 states of India, located in the eastern coast. It is surrounded by the states of West Bengal to the north-east, Jharkhand to the north, Chhattisgarh to the west and north-west, Andhra Pradesh and Telengana to the south and south-west. Odisha has 485 kilometres (301 mi) of coastline along the Bay of Bengal on its east, from Balasore to Malkangiri.[8] It is the 9th largest state by area, and the 11th largest by population. Odia (formerly known as Oriya)[9] is the official and most widely spoken language, spoken by 33.2 million according to the 2001 ...
