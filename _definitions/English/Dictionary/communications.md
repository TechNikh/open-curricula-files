---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/communications
offline_file: ""
offline_thumbnail: ""
uuid: bf888c82-cf5d-40d3-8308-aecfed71c50c
updated: 1484310569
title: communications
categories:
    - Dictionary
---
communications
     n : the discipline that studies the principles of transmiting
         information and the methods by which it is delivered (as
         print or radio or television etc.); "communications is
         his major field of study" [syn: {communication theory}]
