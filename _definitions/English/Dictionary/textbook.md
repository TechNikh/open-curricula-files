---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/textbook
offline_file: ""
offline_thumbnail: ""
uuid: 20bc0099-f31f-4c71-b26f-743bef3164b5
updated: 1484310473
title: textbook
categories:
    - Dictionary
---
textbook
     adj : according to or characteristic of a casebook or textbook;
           typical; "a casebook schizophrenic"; "a textbook
           example" [syn: {casebook}]
     n : a book prepared for use in schools or colleges; "his
         economics textbook is in its tenth edition"; "the
         professor wrote the text that he assigned students to
         buy" [syn: {text}, {text edition}, {schoolbook}, {school
         text}] [ant: {trade book}]
