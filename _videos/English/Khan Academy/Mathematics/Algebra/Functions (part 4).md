---
version: 1
type: video
provider: YouTube
id: rbt51hXmzig
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Functions%20%28part%204%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5aa96c54-8f5a-4744-a59e-06576ef8b2fb
updated: 1486069575
title: Functions (part 4)
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/rbt51hXmzig/default.jpg
    - https://i3.ytimg.com/vi/rbt51hXmzig/1.jpg
    - https://i3.ytimg.com/vi/rbt51hXmzig/2.jpg
    - https://i3.ytimg.com/vi/rbt51hXmzig/3.jpg
---
An example of a functions problem submitted by a youtube viewer
More free lessons at: http://www.khanacademy.org/video?v=rbt51hXmzig
