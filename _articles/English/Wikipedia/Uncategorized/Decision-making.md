---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Decision-making
offline_file: ""
offline_thumbnail: ""
uuid: c68426d1-4025-400a-a844-e92be4c6438f
updated: 1484308378
title: Decision-making
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Wikipedia_article-creation-2.svg.png
tags:
    - Overview
    - Problem analysis
    - Analysis paralysis
    - Information overload
    - Post-decision analysis
    - Everyday techniques
    - Group
    - Individual
    - Steps
    - GOFER
    - DECIDE
    - Other
    - Group stages
    - Rational and irrational
    - Cognitive and personal biases
    - Cognitive styles
    - Optimizing vs. satisficing
    - Intuitive vs. rational
    - Combinatorial vs. positional
    - Influence of Myers-Briggs type
    - Neuroscience
    - In adolescents vs. adults
categories:
    - Uncategorized
---
In psychology, decision-making is regarded as the cognitive process resulting in the selection of a belief or a course of action among several alternative possibilities. Every decision-making process produces a final choice; it may or may not prompt action. Decision-making is the process of identifying and choosing alternatives based on the values and preferences of the decision-Maker.
