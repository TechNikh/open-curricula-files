---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Facility_location_(cooperative_game)
offline_file: ""
offline_thumbnail: ""
uuid: 76fa3637-0233-45bd-8f39-181511a6cba9
updated: 1484308527
title: Facility location (cooperative game)
categories:
    - Cooperative games
---
The cooperative facility location game is a cooperative game of cost sharing. The goal is to share the cost of opening new facilities between the clients enjoying these facilities.[1]:386 The game has the following components:
