---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Local_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: c0cb4338-09cc-4eeb-9c9f-cc86b0873707
updated: 1484309313
title: Local symmetry
categories:
    - Symmetry
---
In physics, a local symmetry is symmetry of some physical quantity, which smoothly depends on the point of the base manifold. Such quantities can be for example an observable, a tensor or the Lagrangian of a theory. If a symmetry is local in this sense, then one can apply a local transformation (resp. local gauge transformation), which means that the representation of the symmetry group is a function of the manifold and can thus be taken to act differently on different points of spacetime.
