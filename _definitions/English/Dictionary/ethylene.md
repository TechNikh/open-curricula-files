---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethylene
offline_file: ""
offline_thumbnail: ""
uuid: 4311e201-3cb5-4e0f-a679-501d60912129
updated: 1484310414
title: ethylene
categories:
    - Dictionary
---
ethylene
     n : a flammable colorless gaseous alkene; obtained from
         petroleum and natural gas and used in manufacturing many
         other chemicals; sometimes used as an anesthetic [syn: {ethene}]
