---
version: 1
type: video
provider: YouTube
id: CBezq1fFUEA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Heredity-%20Crash%20Course%20Biology%20%239.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0316bd51-37ec-4885-a28b-489bfd48ec04
updated: 1486069678
title: 'Heredity- Crash Course Biology #9'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/CBezq1fFUEA/default.jpg
    - https://i3.ytimg.com/vi/CBezq1fFUEA/1.jpg
    - https://i3.ytimg.com/vi/CBezq1fFUEA/2.jpg
    - https://i3.ytimg.com/vi/CBezq1fFUEA/3.jpg
---
