---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hysteresis
offline_file: ""
offline_thumbnail: ""
uuid: 8fa30f8b-1c44-47ed-959d-0087fa08c76f
updated: 1484308814
title: Hysteresis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ehysteresis.PNG
tags:
    - Etymology and history
    - Types
    - Rate-dependent
    - Rate-independent
    - In engineering
    - Control systems
    - Electronic circuits
    - User interface design
    - Aerodynamics
    - In mechanics
    - Elastic hysteresis
    - Contact angle hysteresis
    - Adsorption hysteresis
    - Matric potential hysteresis
    - In materials
    - Magnetic hysteresis
    - Physical origin
    - Magnetic hysteresis models
    - Applications
    - Electrical hysteresis
    - Liquid–solid-phase transitions
    - In biology
    - Cell biology and genetics
    - Immunology
    - Neuroscience
    - Respiratory physiology
    - Voice and speech physiology
    - Ecology and Epidemiology
    - In economics
    - Permanently higher unemployment
    - Game theory
    - In law
    - Additional considerations
    - Models of hysteresis
    - Energy
categories:
    - Nonlinear systems
---
Hysteresis is the time-based dependence of a system's output on present and past inputs. The dependence arises because the history affects the value of an internal state. To predict its future outputs, either its internal state or its history must be known.[1] If a given input alternately increases and decreases, a typical mark of hysteresis is that the output forms a loop as in the figure.
