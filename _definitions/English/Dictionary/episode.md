---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/episode
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484540161
title: episode
categories:
    - Dictionary
---
episode
     n 1: a happening that is distinctive in a series of related
          events
     2: a brief section of a literary or dramatic work that forms
        part of a connected series
     3: a part of a broadcast serial [syn: {installment}, {instalment}]
     4: film consisting of a succession of related shots that
        develop a given subject in a movie [syn: {sequence}]
