---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nodoid
offline_file: ""
offline_thumbnail: ""
uuid: a75e88b3-e56c-4c9f-ad8d-5d4150baab3a
updated: 1484309367
title: Nodoid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nodoid.svg.png
categories:
    - Geometry stubs
---
In differential geometry, a nodoid is a surface of revolution with constant nonzero mean curvature obtained by rolling a hyperbola along a fixed line, tracing the focus, and revolving the resulting nodary curve around the line.[1]
