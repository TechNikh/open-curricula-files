---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/authority
offline_file: ""
offline_thumbnail: ""
uuid: 7f297e46-f227-4c05-9936-47850884d3f5
updated: 1484310429
title: authority
categories:
    - Dictionary
---
authority
     n 1: the power or right to give orders or make decisions; "he has
          the authority to issue warrants"; "deputies are given
          authorization to make arrests" [syn: {authorization}, {authorisation},
           {dominance}, {say-so}]
     2: (usually plural) persons who exercise (administrative)
        control over others; "the authorities have issued a
        curfew"
     3: an expert whose views are taken as definitive; "he is an
        authority on corporate law"
     4: freedom from doubt; belief in yourself and your abilities;
        "his assurance in his superiority did not make him
        popular"; "after that failure he lost his confidence";
        ...
