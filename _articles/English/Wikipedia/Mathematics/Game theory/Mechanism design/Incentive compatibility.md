---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Incentive_compatibility
offline_file: ""
offline_thumbnail: ""
uuid: be8332fd-bef8-4593-87e3-4bcb20074cc7
updated: 1484309050
title: Incentive compatibility
tags:
    - Incentive-compatibility in randomized mechanisms
    - Revelation principles
categories:
    - Mechanism design
---
A mechanism is called incentive-compatible (IC) if every participant can achieve the best outcome to him/herself just by acting according to his/her true preferences. [1]:225
