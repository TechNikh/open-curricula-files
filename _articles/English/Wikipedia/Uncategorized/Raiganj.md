---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Raiganj
offline_file: ""
offline_thumbnail: ""
uuid: 763d6f83-15bb-447a-97a3-fe07c655c474
updated: 1484308456
title: Raiganj
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/170px-NBSTC%252C_Raiganj.jpg'
tags:
    - Etymology
    - geography
    - Location
    - Demographics
    - Climate
    - Transport
    - Roadways
    - Railways
    - Airways
    - Tourism
    - Raiganj Bird Sanctuary
    - Raiganj Church
    - Other notable places
    - Educational institutions
    - schools
    - Raiganj Coronation High School
    - Sudarshanpur Dwarika Prasad Ucha Vidyachakra
    - "Raiganj Girl's High School"
    - Durgapur Public School
    - Colleges
    - University
    - Raiganj University
    - Healthcare
    - Raiganj District Hospital
    - Proposed Raiganj AIIMS
    - Economy
    - Civic administration
    - Raiganj Bhavan
    - culture
    - Media
categories:
    - Uncategorized
---
Raiganj (Pron:ˈraɪˌgʌnʤ) (Bengali: রায়গঞ্জ) is a city and a municipality in Uttar Dinajpur district in the Indian state of West Bengal. It is the headquarters of a district as well as a subdivision. The place oozes heritage. Tradition and a rich past haunt the residents of the 300-year-old town. Even the police station was set up 125 years ago and railway connectivity reached the place 115 years ago. The high school is 101 years old. Raiganj got a railway communication way back in 1896. The train used to go to Parbatipur junction in Bangladesh from Raiganj. The rail link helped the town become an important trading centre, aided by the additional benefit of waterways.[2]
