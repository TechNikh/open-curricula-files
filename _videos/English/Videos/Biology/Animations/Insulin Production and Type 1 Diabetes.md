---
version: 1
type: video
provider: YouTube
id: c-0wtFV-Pa0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Insulin%20Production%20and%20Type%201%20Diabetes.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 376596bf-edfa-470b-8225-e00fa51676b4
updated: 1486069666
title: Insulin Production and Type 1 Diabetes
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/c-0wtFV-Pa0/default.jpg
    - https://i3.ytimg.com/vi/c-0wtFV-Pa0/1.jpg
    - https://i3.ytimg.com/vi/c-0wtFV-Pa0/2.jpg
    - https://i3.ytimg.com/vi/c-0wtFV-Pa0/3.jpg
---
