---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nucleic_acid
offline_file: ""
offline_thumbnail: ""
uuid: 213b56be-8866-4f1d-898a-069a8db3125f
updated: 1484308377
title: Nucleic acid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Difference_DNA_RNA-EN.svg.png
tags:
    - History of nucleic acids
    - 'Occurrence and nomenclature[10]'
    - 'Molecular composition and size[18]'
    - Topology
    - Nucleic acid sequences
    - Types of nucleic acids
    - Deoxyribonucleic acid
    - Ribonucleic acid
    - Artificial nucleic acid
    - Notes and references
    - Bibliography
categories:
    - Uncategorized
---
Nucleic acids are biopolymers, or large biomolecules, essential for all known forms of life. Nucleic acids, which include DNA (deoxyribonucleic acid) and RNA (ribonucleic acid), are made from monomers known as nucleotides. Each nucleotide has three components: a 5-carbon sugar, a phosphate group, and a nitrogenous base. If the sugar is deoxyribose, the polymer is DNA. If the sugar is ribose, the polymer is RNA. When all three components are combined, they form a nucleotide. Nucleotides are also known as phosphate nucleotides.
