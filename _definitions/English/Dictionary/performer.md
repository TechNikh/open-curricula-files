---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/performer
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484545861
title: performer
categories:
    - Dictionary
---
performer
     n : an entertainer who performs a dramatic or musical work for
         an audience [syn: {performing artist}]
