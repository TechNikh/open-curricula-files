---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/centrality
offline_file: ""
offline_thumbnail: ""
uuid: a263d3ca-304d-4b90-a423-11a58b2746bb
updated: 1484310597
title: centrality
categories:
    - Dictionary
---
centrality
     n : the property of being central [ant: {marginality}]
