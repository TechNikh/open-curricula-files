---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ferrous
offline_file: ""
offline_thumbnail: ""
uuid: 425b7b8c-a297-47fa-af76-5f274b1b5fd7
updated: 1484310375
title: ferrous
categories:
    - Dictionary
---
ferrous
     adj : of or relating to or containing iron [syn: {ferric}]
