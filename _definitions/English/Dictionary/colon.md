---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/colon
offline_file: ""
offline_thumbnail: ""
uuid: 4b6b0520-26a3-4324-8df8-34fdef196bc9
updated: 1484310330
title: colon
categories:
    - Dictionary
---
colon
     n 1: the part of the large intestine between the cecum and the
          rectum; it extracts moisture from food residues before
          they are excreted
     2: the basic unit of money in El Salvador; equal to 100
        centavos [syn: {El Salvadoran colon}]
     3: the basic unit of money in Costa Rica; equal to 100 centimos
        [syn: {Costa Rican colon}]
     4: a port city at the Caribbean entrance to the Panama Canal
        [syn: {Aspinwall}]
     5: a punctuation mark (:) used after a word introducing a
        series or an example or an explanation (or after the
        salutation of a business letter)
     [also: {colones} (pl), {cola} (pl)]
