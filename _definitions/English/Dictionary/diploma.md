---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diploma
offline_file: ""
offline_thumbnail: ""
uuid: dd865625-3a2f-45ff-b66c-81b0d8bd226b
updated: 1484310479
title: diploma
categories:
    - Dictionary
---
diploma
     n : a document certifying the successful completion of a course
         of study [syn: {sheepskin}]
