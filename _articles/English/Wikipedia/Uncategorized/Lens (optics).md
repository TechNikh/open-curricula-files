---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biconcave
offline_file: ""
offline_thumbnail: ""
uuid: c75e9e5b-e775-413d-be03-071ef42b6f6d
updated: 1484308321
title: Lens (optics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/40px-Out-of-focus_image_of_a_spoke_target..svg.png
tags:
    - History
    - Construction of simple lenses
    - Types of simple lenses
    - "Lensmaker's equation"
    - Sign convention for radii of curvature R1 and R2
    - Thin lens approximation
    - Imaging properties
    - Magnification
    - Aberrations
    - Spherical aberration
    - Coma
    - Chromatic aberration
    - Other types of aberration
    - Aperture diffraction
    - Compound lenses
    - Other types
    - Uses
    - Bibliography
    - Simulations
categories:
    - Uncategorized
---
A lens is a transmissive optical device that focuses or disperses a light beam by means of refraction. A simple lens consists of a single piece of transparent material, while a compound lens consists of several simple lenses (elements), usually arranged along a common axis. Lenses are made from materials such as glass or plastic, and are ground and polished or moulded to a desired shape. A lens can focus light to form an image, unlike a prism, which refracts light without focusing. Devices that similarly focus or disperse radiation other than visible light are also called lenses, such as microwave lenses, electron lenses or acoustic lenses.
