---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/miller
offline_file: ""
offline_thumbnail: ""
uuid: d3f1ff4c-bf0f-425a-adde-35c7f854385e
updated: 1484310451
title: miller
categories:
    - Dictionary
---
Miller
     n 1: United States bandleader of a popular big band (1909-1944)
          [syn: {Glenn Miller}, {Alton Glenn Miller}]
     2: United States novelist whose novels were originally banned
        as pornographic (1891-1980) [syn: {Henry Miller}, {Henry
        Valentine Miller}]
     3: United States playwright (born 1915) [syn: {Arthur Miller}]
     4: someone who works in a mill (especially a grain mill)
     5: machine tool in which metal that is secured to a carriage is
        fed against rotating cutters that shape it [syn: {milling
        machine}]
     6: any of various moths that have powdery wings [syn: {moth
        miller}]
