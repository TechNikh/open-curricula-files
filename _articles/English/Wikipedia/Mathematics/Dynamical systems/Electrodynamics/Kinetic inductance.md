---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kinetic_inductance
offline_file: ""
offline_thumbnail: ""
uuid: e179bfd9-fb59-4073-8a33-3ec31e678012
updated: 1484308760
title: Kinetic inductance
tags:
    - Explanation
    - Applications
categories:
    - Electrodynamics
---
Kinetic inductance is the manifestation of the inertial mass of mobile charge carriers in alternating electric fields as an equivalent series inductance. Kinetic inductance is observed in high carrier mobility conductors (e.g. superconductors) and at very high frequencies.
