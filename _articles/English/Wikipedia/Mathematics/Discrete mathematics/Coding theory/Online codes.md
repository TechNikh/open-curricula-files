---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Online_codes
offline_file: ""
offline_thumbnail: ""
uuid: c5c14c29-99f3-4efd-888b-adbbef050e7a
updated: 1484308556
title: Online codes
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Online_codes_highlevel.png
tags:
    - Detailed discussion
    - Outer encoding
    - Inner encoding
    - Decoding
categories:
    - Coding theory
---
In computer science, online codes are an example of rateless erasure codes. These codes can encode a message into a number of symbols such that knowledge of any fraction of them allows one to recover the original message (with high probability). Rateless codes produce an arbitrarily large number of symbols which can be broadcast until the receivers have enough symbols.
