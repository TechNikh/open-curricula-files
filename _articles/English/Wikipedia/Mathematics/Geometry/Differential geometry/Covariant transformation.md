---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Covariant_transformation
offline_file: ""
offline_thumbnail: ""
uuid: 0555326d-bc3e-4291-bbda-ecf8e56fe6a0
updated: 1484309215
title: Covariant transformation
tags:
    - Examples of covariant transformation
    - The derivative of a function transforms covariantly
    - Basis vectors transform covariantly
    - Contravariant transformation
    - Differential forms transform contravariantly
    - Dual properties
    - 'Co- and contravariant tensor components'
    - Without coordinates
    - With coordinates
categories:
    - Differential geometry
---
In physics, a covariant transformation is a rule that specifies how certain entities, such as vectors or tensors, change under a change of basis. The transformation that describes the new basis vectors as a linear combination of the old basis vectors is defined as a covariant transformation. Conventionally, indices identifying the basis vectors are placed as lower indices and so are all entities that transform in the same way. The inverse of a covariant transformation is a contravariant transformation. Since a vector should be[clarification needed] invariant under a change of basis, its components must transform according to the contravariant rule. Conventionally, indices identifying the ...
