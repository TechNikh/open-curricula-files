---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bent_function
offline_file: ""
offline_thumbnail: ""
uuid: 7130e31e-b3ea-41fc-b405-cb5af4b23a48
updated: 1484308570
title: Bent function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Boolean_functions_like_1000_nonlinearity.svg.png
tags:
    - Walsh transform
    - Definition and properties
    - Applications
    - Generalizations
categories:
    - Combinatorics
---
In the mathematical field of combinatorics, a bent function is a special type of Boolean function. This means it takes several inputs and gives one output, each of which has two possible values (such as 0 and 1, or true and false). The name is figurative. Bent functions are so called because they are as different as possible from all linear and affine functions, the simplest or "straight" functions. This makes the bent functions naturally hard to approximate. Bent functions were defined and named in the 1960s by Oscar Rothaus in research not published until 1976.[1] They have been extensively studied for their applications in cryptography, but have also been applied to spread spectrum, ...
