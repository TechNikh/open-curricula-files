---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/far-reaching
offline_file: ""
offline_thumbnail: ""
uuid: 94adb51d-1f0d-43c4-8855-668ac3a8b844
updated: 1484310321
title: far-reaching
categories:
    - Dictionary
---
far-reaching
     adj : having broad range or effect; "had extensive press
           coverage"; "far-reaching changes in the social
           structure"; "sweeping reforms" [syn: {extensive}, {sweeping}]
