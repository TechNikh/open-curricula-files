---
version: 1
type: video
provider: YouTube
id: eoJGBr-tJXo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Line%20integrals%20and%20Green%27s%20theorem/2D%20divergence%20theorem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 274fd65e-e773-4dff-a5aa-e75ccb20c1d7
updated: 1486069690
title: 2D divergence theorem
categories:
    - "Line integrals and Green's theorem"
thumbnail_urls:
    - https://i3.ytimg.com/vi/eoJGBr-tJXo/default.jpg
    - https://i3.ytimg.com/vi/eoJGBr-tJXo/1.jpg
    - https://i3.ytimg.com/vi/eoJGBr-tJXo/2.jpg
    - https://i3.ytimg.com/vi/eoJGBr-tJXo/3.jpg
---
