---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/effective
offline_file: ""
offline_thumbnail: ""
uuid: 97dd21ca-089b-4dc6-b934-f1de4b212018
updated: 1484310365
title: effective
categories:
    - Dictionary
---
effective
     adj 1: producing or capable of producing an intended result or
            having a striking effect; "an air-cooled motor was
            more effective than a witch's broomstick for rapid
            long-distance transportation"-LewisMumford; "effective
            teaching methods"; "effective steps toward peace";
            "made an effective entrance"; "his complaint proved to
            be effectual in bringing action"; "an efficacious law"
            [syn: {effectual}, {efficacious}] [ant: {ineffective}]
     2: able to accomplish a purpose; functioning effectively;
        "people who will do nothing unless they get something out
        of it for themselves are ...
