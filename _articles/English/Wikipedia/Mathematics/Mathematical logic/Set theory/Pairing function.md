---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pairing_function
offline_file: ""
offline_thumbnail: ""
uuid: 0a69e551-df11-4b73-85cc-3dcb95eb681e
updated: 1484309510
title: Pairing function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pairing_natural.svg.png
tags:
    - Definition
    - Cantor pairing function
    - Inverting the Cantor pairing function
    - Examples
    - Derivation
categories:
    - Set theory
---
Any pairing function can be used in set theory to prove that integers and rational numbers have the same cardinality as natural numbers. In theoretical computer science they are used to encode a function defined on a vector of natural numbers f : Nk → N into a new function g : N → N.
