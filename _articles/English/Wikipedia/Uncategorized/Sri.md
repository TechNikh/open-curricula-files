---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sri
offline_file: ""
offline_thumbnail: ""
uuid: 2d51bafa-021c-4ee7-96c6-30e180f614a7
updated: 1484308333
title: Sri
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Shri-symbol.svg.png
tags:
    - Etymology
    - Usage
    - Repetition
    - Other current usage
    - Indian music
    - Other languages
    - South and Southeast Asia
    - Place names
categories:
    - Uncategorized
---
Sri (Devanagari: श्री, IAST; Śrī), also transliterated as Sree, Shri, Shree, Si or Seri is a word of Sanskrit origin, used in the Indian subcontinent as a polite form of address equivalent to the English "Mr." or "Ms." in written and spoken language, but also as a title of veneration for deities. It is also widely used in other South and Southeast Asian languages.
