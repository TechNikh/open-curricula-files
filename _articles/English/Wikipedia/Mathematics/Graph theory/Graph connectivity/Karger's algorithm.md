---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Karger%27s_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: bf965c81-992d-4187-95b7-c4c47c18183f
updated: 1484309402
title: "Karger's algorithm"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Min_cut_example.svg.png
tags:
    - The global minimum cut problem
    - Contraction algorithm
    - Success probability of the contraction algorithm
    - Repeating the contraction algorithm
    - Karger–Stein algorithm
    - Analysis
    - Finding all min-cuts
    - Improvement bound
categories:
    - Graph connectivity
---
In computer science and graph theory, Karger's algorithm is a randomized algorithm to compute a minimum cut of a connected graph. It was invented by David Karger and first published in 1993.[1]
