---
version: 1
type: video
provider: YouTube
id: 3tmFTHOP6Pc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Adding%20and%20subtracting%20rational%20expressions%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4a14d4b3-7284-4e56-834d-95ab0d2d34ec
updated: 1486069561
title: Adding and subtracting rational expressions 2
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/3tmFTHOP6Pc/default.jpg
    - https://i3.ytimg.com/vi/3tmFTHOP6Pc/1.jpg
    - https://i3.ytimg.com/vi/3tmFTHOP6Pc/2.jpg
    - https://i3.ytimg.com/vi/3tmFTHOP6Pc/3.jpg
---
