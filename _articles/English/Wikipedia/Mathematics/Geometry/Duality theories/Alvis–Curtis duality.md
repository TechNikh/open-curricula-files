---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Alvis%E2%80%93Curtis_duality'
offline_file: ""
offline_thumbnail: ""
uuid: cf12db38-619a-413f-a155-6469520a83a7
updated: 1484309231
title: Alvis–Curtis duality
categories:
    - Duality theories
---
In mathematics, Alvis–Curtis duality is a duality operation on the characters of a reductive group over a finite field, introduced by Charles W. Curtis (1980) and studied by his student Dean Alvis (1979). Kawanaka (1981, 1982) introduced a similar duality operation for Lie algebras.
