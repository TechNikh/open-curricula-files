---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pleasant
offline_file: ""
offline_thumbnail: ""
uuid: 6bb3f008-763c-4230-9909-f37a8fa3ac6f
updated: 1484310242
title: pleasant
categories:
    - Dictionary
---
pleasant
     adj 1: affording pleasure; being in harmony with your taste or
            likings; "a pleasant person to be around"; "we had a
            pleasant evening together"; "a pleasant scene";
            "pleasant sensations" [ant: {unpleasant}]
     2: pleasant in manner or behavior; "I didn`t enjoy it and
        probably wasn't a pleasant person to be around"
