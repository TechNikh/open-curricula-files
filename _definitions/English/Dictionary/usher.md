---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/usher
offline_file: ""
offline_thumbnail: ""
uuid: 8ffc2931-6b74-477a-ac0f-921a3fbf5c3b
updated: 1484310183
title: usher
categories:
    - Dictionary
---
Usher
     n 1: Irish prelate who deduced from the Bible that Creation
          occurred in the year 4004 BC (1581-1656) [syn: {Ussher},
           {James Ussher}, {James Usher}]
     2: an official doorkeeper as in a courtroom or legislative
        chamber [syn: {doorkeeper}]
     3: someone employed to conduct others [syn: {guide}]
     v : show (someone) to their seats, as in theaters or
         auditoriums; "The usher showed us to our seats" [syn: {show}]
