---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/belching
offline_file: ""
offline_thumbnail: ""
uuid: f3489a7f-506a-4028-b4a6-0725b9c2b10a
updated: 1484310335
title: belching
categories:
    - Dictionary
---
belching
     n 1: the forceful expulsion of something from inside; "the
          belching of smoke from factory chimneys"
     2: a reflex that expels wind noisily from the stomach through
        the mouth [syn: {belch}, {burp}, {burping}, {eructation}]
