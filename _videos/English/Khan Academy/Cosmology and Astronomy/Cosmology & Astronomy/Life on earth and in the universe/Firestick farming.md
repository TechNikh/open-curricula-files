---
version: 1
type: video
provider: YouTube
id: mwUyaeWxJhA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Life%20on%20earth%20and%20in%20the%20universe/Firestick%20farming.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4d6f3d6c-906c-4b95-9ca5-5a5605315d20
updated: 1486069685
title: Firestick farming
categories:
    - Life on earth and in the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/mwUyaeWxJhA/default.jpg
    - https://i3.ytimg.com/vi/mwUyaeWxJhA/1.jpg
    - https://i3.ytimg.com/vi/mwUyaeWxJhA/2.jpg
    - https://i3.ytimg.com/vi/mwUyaeWxJhA/3.jpg
---
