---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unequal
offline_file: ""
offline_thumbnail: ""
uuid: 04b37688-3a4d-461d-b7cf-2e52fbb51e74
updated: 1484310411
title: unequal
categories:
    - Dictionary
---
unequal
     adj 1: not equal in amount; "they distributed unlike (or unequal)
            sums to the various charities" [syn: {unlike}] [ant: {like}]
     2: poorly balanced or matched in quantity or value or measure
        [ant: {equal}]
