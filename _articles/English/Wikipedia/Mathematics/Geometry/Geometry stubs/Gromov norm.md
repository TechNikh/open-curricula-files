---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gromov_norm
offline_file: ""
offline_thumbnail: ""
uuid: 28130057-61bf-49f4-b632-865300df1d8b
updated: 1484309362
title: Gromov norm
categories:
    - Geometry stubs
---
In mathematics, the Gromov norm (or simplicial volume) of a compact oriented n-manifold is a norm on the homology (with real coefficients) given by minimizing the sum of the absolute values of the coefficients over all singular chains representing a cycle. The Gromov norm of the manifold is the Gromov norm of the fundamental class.[1][2]
