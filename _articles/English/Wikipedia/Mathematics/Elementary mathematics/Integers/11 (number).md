---
version: 1
type: article
id: https://en.wikipedia.org/wiki/11_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 4161a9cb-7e12-4357-8b9e-7600d84480ff
updated: 1484308946
title: 11 (number)
tags:
    - Name
    - In mathematics
    - List of basic calculations
    - In numeral systems
    - In science
    - Astronomy
    - In religion
    - Christianity
    - Thelema
    - Babylonian
    - In music
    - In sports
    - In the military
    - In computing
    - In Canada
    - In other fields
categories:
    - Integers
---
11 (eleven i/ᵻˈlɛvᵻn/ or /iˈlɛvɛn/) is the natural number following 10 and preceding 12. In English, it is the smallest positive integer requiring three syllables and the largest prime number with a single-morpheme name.
