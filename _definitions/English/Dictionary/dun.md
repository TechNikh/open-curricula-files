---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dun
offline_file: ""
offline_thumbnail: ""
uuid: b094b31d-e521-4254-8b7d-bf7dee1022ec
updated: 1484310435
title: dun
categories:
    - Dictionary
---
dun
     adj : of a dull grayish brown to brownish gray color; "the dun and
           dreary prairie"
     n 1: horse of a dull brownish gray color
     2: a color varying around light grayish brown; "she wore a dun
        raincoat" [syn: {grayish brown}, {greyish brown}, {fawn}]
     v 1: treat cruelly; "The children tormented the stuttering
          teacher" [syn: {torment}, {rag}, {bedevil}, {crucify}, {frustrate}]
     2: persistently ask for overdue payment; "The grocer dunned his
        customers every day by telephone"
     3: cure by salting; "dun codfish"
     4: make a dun color
     [also: {dunning}, {dunned}, {dunnest}, {dunner}]
