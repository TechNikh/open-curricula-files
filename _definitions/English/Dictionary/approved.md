---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/approved
offline_file: ""
offline_thumbnail: ""
uuid: 663ed637-f9b1-4c53-a6ba-2afb1d89e8a7
updated: 1484310477
title: approved
categories:
    - Dictionary
---
approved
     adj 1: established by authority; given authoritative approval; "a
            list of approved candidates" [syn: {sanctioned}]
     2: generally or especially officially judged acceptable or
        satisfactory; "an approved method"; "work on the approved
        project went ahead on schedule" [ant: {disapproved}]
