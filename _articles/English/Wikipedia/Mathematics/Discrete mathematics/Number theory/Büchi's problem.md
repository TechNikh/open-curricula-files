---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/B%C3%BCchi%27s_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 0378048a-5d51-4704-ad3c-219b86c6b9f1
updated: 1484308639
title: "Büchi's problem"
tags:
    - "Statement of Büchi's problem"
    - Examples
    - Original motivation
    - Some results
categories:
    - Number theory
---
Büchi's problem, also known as the n squares' problem, is an open problem from number theory named after the Swiss mathematician Julius Richard Büchi. It asks whether there is a positive integer M such that every sequence of M or more integer squares, whose second difference is constant and equal to 2, is necessarily a sequence of squares of the form (x + i)2, i = 1, 2, ..., M,... for some integer x. In 1983, Douglas Hensley observed that Büchi's problem is equivalent to the following: Does there exist a positive integer M such that, for all integers x and a, the quantity (x + n)2 + a cannot be a square for more than M consecutive values of n, unless a = 0?
