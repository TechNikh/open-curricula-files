---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermodynamic_operation
offline_file: ""
offline_thumbnail: ""
uuid: a870051e-b12f-4c4b-b15e-aa748d56eaff
updated: 1484308868
title: Thermodynamic operation
tags:
    - >
        Distinction between thermodynamic operation and
        thermodynamic process
    - |
        Planck's "natural processes" contrasted with actions of Maxwell's demon
    - Examples of thermodynamic operations
    - Thermodynamic cycle
    - Virtual thermodynamic operations
    - Composition of systems
    - Additivity of extensive variables
    - Scaling of a system
    - Splitting and recomposition of systems
    - Statements of laws
    - Bibliography for citations
categories:
    - Thermodynamics
---
A thermodynamic operation is an externally imposed manipulation or change of connection or wall between a thermodynamic system and its surroundings.[1][2][3][4] An externally imposed change in the state of the surroundings of a system can usually be regarded as a virtual thermodynamic operation. It may usually be analyzed as due to changes in the walls that separate the systems in the surroundings. It is assumed in thermodynamics that the operation is conducted in ignorance of any pertinent microscopic information.
