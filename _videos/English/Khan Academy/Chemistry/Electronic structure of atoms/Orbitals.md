---
version: 1
type: video
provider: YouTube
id: yBrp8uvNAhI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Electronic%20structure%20of%20atoms/Orbitals.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f3aee92c-6109-45b8-885a-82eb0fd88377
updated: 1486069587
title: Orbitals
tags:
    - dotsub
categories:
    - Electronic structure of atoms
thumbnail_urls:
    - https://i3.ytimg.com/vi/yBrp8uvNAhI/default.jpg
    - https://i3.ytimg.com/vi/yBrp8uvNAhI/1.jpg
    - https://i3.ytimg.com/vi/yBrp8uvNAhI/2.jpg
    - https://i3.ytimg.com/vi/yBrp8uvNAhI/3.jpg
---
Introduction to orbitals. Relates energy shell to rows and periods in the periodic table. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/electronic-structure-of-atoms/electron-configurations-jay-sal/v/more-on-orbitals-and-electron-configuration?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/electronic-structure-of-atoms/orbitals-and-electrons/v/quantum-numbers-for-the-first-four-shells?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
