---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sequential
offline_file: ""
offline_thumbnail: ""
uuid: e3b02df2-b5ee-4bce-9f86-104def667fd7
updated: 1484310158
title: sequential
categories:
    - Dictionary
---
sequential
     adj : in regular succession without gaps; "serial concerts" [syn:
           {consecutive}, {sequent}, {serial}, {successive}]
