---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mendelevium
offline_file: ""
offline_thumbnail: ""
uuid: 69007096-6b49-4e00-9f7c-790233571c82
updated: 1484310397
title: mendelevium
categories:
    - Dictionary
---
mendelevium
     n : a radioactive transuranic element synthesized by bombarding
         einsteinium with alpha particles (Md is the current
         symbol for mendelevium but Mv was formerly the symbol)
         [syn: {Md}, {Mv}, {atomic number 101}]
