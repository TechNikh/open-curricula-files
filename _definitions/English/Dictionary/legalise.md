---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/legalise
offline_file: ""
offline_thumbnail: ""
uuid: 57db850d-d3c3-43f1-a7b9-4fe65b538f81
updated: 1484310533
title: legalise
categories:
    - Dictionary
---
legalise
     v : make legal; "Marijuana should be legalized" [syn: {legalize},
          {decriminalize}, {decriminalise}, {legitimize}, {legitimise},
          {legitimate}, {legitimatize}, {legitimatise}] [ant: {outlaw},
          {outlaw}, {outlaw}, {outlaw}]
