---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/much
offline_file: ""
offline_thumbnail: ""
uuid: 9c68d75f-cc45-4f88-8548-a24dedc2b124
updated: 1484310325
title: much
categories:
    - Dictionary
---
much
     adj : (quantifier used with mass nouns) great in quantity or
           degree or extent; "not much rain"; "much affection";
           "much grain is in storage" [syn: {much(a)}] [ant: {little(a)}]
     n : a great amount or extent; "they did much for humanity"
     adv 1: to a great degree or extent; "she's much better now"
     2: very; "he was much annoyed"
     3: to a very great degree or extent; "we enjoyed ourselves very
        much"; "she was very much interested"; "this would help a
        great deal" [syn: {a lot}, {a good deal}, {a great deal},
        {very much}]
     4: (degree adverb used before a noun phrase) for all practical
        purposes but not ...
