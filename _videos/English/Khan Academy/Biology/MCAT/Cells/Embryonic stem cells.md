---
version: 1
type: video
provider: YouTube
id: -yCIMk1x0Pk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/MCAT/Cells/Embryonic%20stem%20cells.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b308d7a5-86cb-4d46-b107-a30abc2a144e
updated: 1486069579
title: Embryonic stem cells
categories:
    - Cells
thumbnail_urls:
    - https://i3.ytimg.com/vi/-yCIMk1x0Pk/default.jpg
    - https://i3.ytimg.com/vi/-yCIMk1x0Pk/1.jpg
    - https://i3.ytimg.com/vi/-yCIMk1x0Pk/2.jpg
    - https://i3.ytimg.com/vi/-yCIMk1x0Pk/3.jpg
---
An overview of early development of a zygote to an embryo. Embryonic and somatic stem cells. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/test-prep/mcat/cells/cellular-division/v/cancer?utm_source=YT&utm_medium=Desc&utm_campaign=mcat

Missed the previous lesson? https://www.khanacademy.org/test-prep/mcat/cells/cellular-division/v/phases-of-meiosis-ii?utm_source=YT&utm_medium=Desc&utm_campaign=mcat

MCAT on Khan Academy: Go ahead and practice some passage-based questions!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s MCAT channel: https://www.youtube.com/channel/UCDkK5wqSuwDlJ3_nl3rgdiQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
