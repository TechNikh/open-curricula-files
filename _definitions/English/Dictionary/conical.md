---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conical
offline_file: ""
offline_thumbnail: ""
uuid: 2ee5cc4b-a039-4e7d-beac-21ebc1581045
updated: 1484310371
title: conical
categories:
    - Dictionary
---
conical
     adj : relating to or resembling a cone; "conical mountains";
           "conelike fruit" [syn: {conic}, {conelike}, {cone-shaped}]
