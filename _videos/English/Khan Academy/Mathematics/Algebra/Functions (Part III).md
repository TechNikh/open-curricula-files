---
version: 1
type: video
provider: YouTube
id: 5fcRSie63Hs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Functions%20%28Part%20III%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: dc8356c4-0271-4d6c-9c31-a8a1bce60bdb
updated: 1486069573
title: Functions (Part III)
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/5fcRSie63Hs/default.jpg
    - https://i3.ytimg.com/vi/5fcRSie63Hs/1.jpg
    - https://i3.ytimg.com/vi/5fcRSie63Hs/2.jpg
    - https://i3.ytimg.com/vi/5fcRSie63Hs/3.jpg
---
Even more examples of function exercises.  Introduction of a graph as definition of a function.
More free lessons at: http://www.khanacademy.org/video?v=5fcRSie63Hs
