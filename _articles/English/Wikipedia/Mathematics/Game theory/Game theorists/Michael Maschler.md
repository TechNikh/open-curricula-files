---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Michael_Maschler
offline_file: ""
offline_thumbnail: ""
uuid: 1fefca1c-61aa-4c46-adf0-7ca78b06a051
updated: 1484308506
title: Michael Maschler
categories:
    - Game theorists
---
Michael Bahir Maschler (Hebrew: מיכאל בהיר משלר) (July 22, 1927 – July 20, 2008) was an Israeli mathematician well known for his contributions to the field of game theory. He was a professor in the Einstein Institute of Mathematics and the Center for the Study of Rationality at the Hebrew University of Jerusalem in Israel.
