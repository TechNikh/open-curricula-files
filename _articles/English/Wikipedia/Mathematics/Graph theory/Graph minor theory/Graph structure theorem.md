---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_structure_theorem
offline_file: ""
offline_thumbnail: ""
uuid: a9e100a3-ffde-45b4-a2b2-e6bbbdc5368a
updated: 1484309413
title: Graph structure theorem
tags:
    - Setup and motivation for the theorem
    - Tree width
    - Surface embeddings
    - Clique-sums
    - Vortices (rough description)
    - Vortices (precise definition)
    - Statement of the graph structure theorem
    - Refinements
    - Notes
categories:
    - Graph minor theory
---
In mathematics, the graph structure theorem is a major result in the area of graph theory. The result establishes a deep and fundamental connection between the theory of graph minors and topological embeddings. The theorem is stated in the seventeenth of a series of 23 papers by Neil Robertson and Paul Seymour. Its proof is very long and involved. Kawarabayashi & Mohar (2007) and Lovász (2006) are surveys accessible to nonspecialists, describing the theorem and its consequences.
