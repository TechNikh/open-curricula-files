---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Markus%E2%80%93Yamabe_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 98354252-d755-491f-8348-b50b9f0bedbf
updated: 1484308692
title: Markus–Yamabe conjecture
categories:
    - Theorems in dynamical systems
---
In mathematics, the Markus–Yamabe conjecture is a conjecture on global asymptotic stability. The conjecture states that if a continuously differentiable map on an 
  
    
      
        n
      
    
    {\displaystyle n}
  
-dimensional real vector space has a single fixed point, and its Jacobian matrix is everywhere Hurwitz, then the fixed point is globally stable.
