---
version: 1
type: video
provider: YouTube
id: agujzHdvtjc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Transformers%20-%20Electric%20Power%20transmission.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bd02a486-ffe2-4c87-9ef4-3f15a4793732
updated: 1486069657
title: 'Transformers - Electric Power transmission'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/agujzHdvtjc/default.jpg
    - https://i3.ytimg.com/vi/agujzHdvtjc/1.jpg
    - https://i3.ytimg.com/vi/agujzHdvtjc/2.jpg
    - https://i3.ytimg.com/vi/agujzHdvtjc/3.jpg
---
