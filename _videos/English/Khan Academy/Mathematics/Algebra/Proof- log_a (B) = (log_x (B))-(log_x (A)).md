---
version: 1
type: video
provider: YouTube
id: cKOtT4WnZb4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Proof-%20log_a%20%28B%29%20%3D%20%28log_x%20%28B%29%29-%28log_x%20%28A%29%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 71f774cf-59fa-42f5-be73-e8e5271fbfc7
updated: 1486069569
title: 'Proof- log_a (B) = (log_x (B))/(log_x (A))'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/cKOtT4WnZb4/default.jpg
    - https://i3.ytimg.com/vi/cKOtT4WnZb4/1.jpg
    - https://i3.ytimg.com/vi/cKOtT4WnZb4/2.jpg
    - https://i3.ytimg.com/vi/cKOtT4WnZb4/3.jpg
---
Proof of the logarithm property 
log_a (B) = (log_x (B))/(log_x (A))
