---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vector_area
offline_file: ""
offline_thumbnail: ""
uuid: 2cac7e46-1632-4f7f-a607-701181aea52a
updated: 1484309168
title: Vector area
categories:
    - Analytic geometry
---
In 3-dimensional geometry, for a finite planar surface of scalar area 
  
    
      
        S
      
    
    {\displaystyle S}
  
 and unit normal 
  
    
      
        
          
            
              n
              ^
            
          
        
      
    
    {\displaystyle {\hat {n}}}
  
, the vector area 
  
    
      
        
          S
        
      
    
    {\displaystyle \mathbf {S} }
  
 is defined as the unit normal scaled by the area:
