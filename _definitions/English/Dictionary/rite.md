---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rite
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484342101
title: rite
categories:
    - Dictionary
---
rite
     n 1: an established ceremony prescribed by a religion; "the rite
          of baptism" [syn: {religious rite}]
     2: any customary observance or practice [syn: {ritual}]
