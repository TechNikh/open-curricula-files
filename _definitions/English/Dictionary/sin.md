---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sin
offline_file: ""
offline_thumbnail: ""
uuid: affb9800-527e-43c2-8929-d48ff673793f
updated: 1484310214
title: sin
categories:
    - Dictionary
---
sin
     n 1: estrangement from god [syn: {sinfulness}, {wickedness}]
     2: an act that is regarded by theologians as a transgression of
        God's will [syn: {sinning}]
     3: ratio of the opposite side to the hypotenuse of a
        right-angled triangle [syn: {sine}]
     4: (Akkadian) god of the moon; counterpart of Sumerian Nanna
     5: the 21st letter of the Hebrew alphabet
     6: violent and excited activity; "they began to fight like sin"
        [syn: {hell}]
     v 1: commit a sin; violate a law of God or a moral law [syn: {transgress},
           {trespass}]
     2: commit a faux pas or a fault or make a serious mistake; "I
        blundered during the job interview" ...
