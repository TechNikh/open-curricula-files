---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Soft_drink
offline_file: ""
offline_thumbnail: ""
uuid: 68432e76-9900-472a-bb67-b75137c68b44
updated: 1484308375
title: Soft drink
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tumbler_of_cola_with_ice.jpg
tags:
    - Terminology
    - History
    - Carbonated drinks
    - Mass market and industrialization
    - Production
    - Producers
    - Health concerns
    - Obesity and weight-related diseases
    - Dental decay
    - Hypokalemia
    - Soft drinks related to bone density and bone loss
    - Sugar content
    - Benzene
    - Pesticides in India
    - Kidney stones
    - Government regulation
    - schools
    - Taxation
    - Bans
categories:
    - Uncategorized
---
A soft drink (see terminology for other names) is a drink that typically contains carbonated water, a sweetener, and a natural or artificial flavoring. The sweetener may be sugar, high-fructose corn syrup, fruit juice, sugar substitutes (in the case of diet drinks), or some combination of these. Soft drinks may also contain caffeine, colorings, preservatives, and other ingredients.
