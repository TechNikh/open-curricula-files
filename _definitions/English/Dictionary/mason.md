---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mason
offline_file: ""
offline_thumbnail: ""
uuid: c2269fd5-c9be-4a0e-bc68-4af4a031e52f
updated: 1484310455
title: mason
categories:
    - Dictionary
---
Mason
     n 1: American Revolutionary leader from Virginia whose objections
          led to the drafting of the Bill of Rights (1725-1792)
          [syn: {George Mason}]
     2: English film actor (1909-1984) [syn: {James Mason}, {James
        Neville Mason}]
     3: English writer (1865-1948) [syn: {A. E. W. Mason}, {Alfred
        Edward Woodley Mason}]
     4: a craftsman who works with stone or brick [syn: {stonemason}]
     5: a member of a widespread secret fraternal order pledged to
        mutual assistance and brotherly love [syn: {Freemason}]
