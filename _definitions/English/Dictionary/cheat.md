---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cheat
offline_file: ""
offline_thumbnail: ""
uuid: f1d92611-67ae-4ee5-81f9-caad0a106fbd
updated: 1484310517
title: cheat
categories:
    - Dictionary
---
cheat
     n 1: weedy annual grass often occurs in grainfields and other
          cultivated land; seeds sometimes considered poisonous
          [syn: {darnel}, {tare}, {bearded darnel}, {Lolium
          temulentum}]
     2: weedy annual native to Europe but widely distributed as a
        weed especially in wheat [syn: {chess}, {Bromus secalinus}]
     3: someone who leads you to believe something that is not true
        [syn: {deceiver}, {cheater}, {trickster}, {beguiler}, {slicker}]
     4: the act of swindling by some fraudulent scheme; "that book
        is a fraud" [syn: {swindle}, {rig}]
     5: a deception for profit to yourself [syn: {cheating}]
     v 1: deprive somebody of ...
