---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/algae
offline_file: ""
offline_thumbnail: ""
uuid: dd95779f-440b-4751-be4f-755427e5ef94
updated: 1484310266
title: algae
categories:
    - Dictionary
---
algae
     n : primitive chlorophyll-containing mainly aquatic eukaryotic
         organisms lacking true stems and roots and leaves [syn: {alga}]
