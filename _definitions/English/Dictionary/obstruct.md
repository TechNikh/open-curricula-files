---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/obstruct
offline_file: ""
offline_thumbnail: ""
uuid: 37d74436-b8d2-40cc-91dc-2927dfee6f79
updated: 1484310218
title: obstruct
categories:
    - Dictionary
---
obstruct
     v 1: hinder or prevent the progress or accomplishment of; "His
          brother blocked him at every turn" [syn: {blockade}, {block},
           {hinder}, {stymie}, {stymy}, {embarrass}]
     2: block passage through; "obstruct the path" [syn: {obturate},
         {impede}, {occlude}, {jam}, {block}, {close up}] [ant: {free}]
     3: shut out from view or get in the way so as to hide from
        sight; "The thick curtain blocked the action on the
        stage"; "The trees obstruct my view of the mountains"
        [syn: {block}]
