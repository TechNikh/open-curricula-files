---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meaningful
offline_file: ""
offline_thumbnail: ""
uuid: 5c67e61f-76a3-4e26-a7d1-bb18f0238bdd
updated: 1484310447
title: meaningful
categories:
    - Dictionary
---
meaningful
     adj : having a meaning or purpose; "a meaningful explanation"; "a
           meaningful discussion"; "a meaningful pause" [ant: {meaningless}]
