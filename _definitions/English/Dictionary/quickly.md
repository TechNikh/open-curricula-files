---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quickly
offline_file: ""
offline_thumbnail: ""
uuid: 5ef9b451-3f30-4545-9506-f7a13ed5fcba
updated: 1484310325
title: quickly
categories:
    - Dictionary
---
quickly
     adv 1: with rapid movements; "he works quickly" [syn: {rapidly}, {speedily},
             {chop-chop}, {apace}] [ant: {slowly}]
     2: with little or no delay; "the rescue squad arrived
        promptly"; "come here, quick!" [syn: {promptly}, {quick}]
     3: without taking pains; "he looked cursorily through the
        magazine" [syn: {cursorily}]
