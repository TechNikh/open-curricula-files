---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Johnson%E2%80%93Lindenstrauss_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 7a827f59-26a9-4021-8551-69b21075f192
updated: 1484309268
title: Johnson–Lindenstrauss lemma
tags:
    - Lemma
    - Alternate Statement
    - Speeding up the JL Transform
    - Notes
categories:
    - Metric geometry
---
In mathematics, the Johnson–Lindenstrauss lemma is a result named after William B. Johnson and Joram Lindenstrauss concerning low-distortion embeddings of points from high-dimensional into low-dimensional Euclidean space. The lemma states that a small set of points in a high-dimensional space can be embedded into a space of much lower dimension in such a way that distances between the points are nearly preserved. The map used for the embedding is at least Lipschitz, and can even be taken to be an orthogonal projection.
