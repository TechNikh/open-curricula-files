---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dancing
offline_file: ""
offline_thumbnail: ""
uuid: 670f31e0-3a5d-41b2-9e50-c0c040675768
updated: 1484310603
title: dancing
categories:
    - Dictionary
---
dancing
     n : taking a series of rhythmical steps (and movements) in time
         to music [syn: {dance}, {terpsichore}, {saltation}]
