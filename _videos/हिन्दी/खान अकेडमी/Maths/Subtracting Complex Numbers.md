---
version: 1
type: video
provider: YouTube
id: ag-VRTxSRHw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 50b09e19-a1ed-43d0-929f-3db8454e0dde
updated: 1486069607
title: Subtracting Complex Numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ag-VRTxSRHw/default.jpg
    - https://i3.ytimg.com/vi/ag-VRTxSRHw/1.jpg
    - https://i3.ytimg.com/vi/ag-VRTxSRHw/2.jpg
    - https://i3.ytimg.com/vi/ag-VRTxSRHw/3.jpg
---
