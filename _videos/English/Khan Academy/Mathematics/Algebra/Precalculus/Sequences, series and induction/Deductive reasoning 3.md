---
version: 1
type: video
provider: YouTube
id: lYLaaMObgkw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Sequences%2C%20series%20and%20induction/Deductive%20reasoning%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e84a49c3-28ed-4d51-8a42-323a83875688
updated: 1486069561
title: Deductive reasoning 3
categories:
    - Sequences, series and induction
thumbnail_urls:
    - https://i3.ytimg.com/vi/lYLaaMObgkw/default.jpg
    - https://i3.ytimg.com/vi/lYLaaMObgkw/1.jpg
    - https://i3.ytimg.com/vi/lYLaaMObgkw/2.jpg
    - https://i3.ytimg.com/vi/lYLaaMObgkw/3.jpg
---
