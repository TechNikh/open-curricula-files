---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/psychological
offline_file: ""
offline_thumbnail: ""
uuid: 01163317-80c8-4e34-a3e9-d24c3aafd872
updated: 1484310529
title: psychological
categories:
    - Dictionary
---
psychological
     adj 1: mental or emotional as opposed to physical in nature; "give
            psychological support"; "psychological warfare"
     2: of or relating to or determined by psychology;
        "psychological theories"
