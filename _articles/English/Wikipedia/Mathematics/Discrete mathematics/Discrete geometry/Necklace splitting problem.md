---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Necklace_splitting_problem
offline_file: ""
offline_thumbnail: ""
uuid: 16a9869a-214a-4d49-a453-7b3976923ca5
updated: 1484308504
title: Necklace splitting problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Collier-de-perles-rouge-vert.svg.png
tags:
    - Variants
    - Proof
    - Further results
    - One cut fewer than needed
    - Negative result
    - Splitting multidimensional necklaces
    - Approximation algorithm
categories:
    - Discrete geometry
---
Necklace splitting is a picturesque name given to several related problems in combinatorics and measure theory. Its name and solutions are due to mathematicians Noga Alon [1] and Douglas B. West.[2]
