---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lambda-mu_calculus
offline_file: ""
offline_thumbnail: ""
uuid: 73d9c24d-0c0b-4135-bf79-8274db03278b
updated: 1484309495
title: Lambda-mu calculus
tags:
    - Formal definition
    - Reduction
categories:
    - Proof theory
---
In mathematical logic and computer science, the lambda-mu calculus is an extension of the lambda calculus introduced by M. Parigot.[1] It introduces two new operators: the μ operator (which is completely different both from the μ operator found in computability theory and from the μ operator of modal μ-calculus) and the bracket operator. Proof-theoretically, it provides a well-behaved formulation of classical natural deduction.
