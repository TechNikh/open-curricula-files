---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Manikonda
offline_file: ""
offline_thumbnail: ""
uuid: 373f200e-e414-439d-9f75-c132762ba13e
updated: 1484308335
title: Manikonda
tags:
    - Development
    - Commercial areas
    - Transport
    - schools
categories:
    - Uncategorized
---
Manikonda is a commercial hub and a residential suburb in Hyderabad, Telangana, India. This suburb has boomed in recent years due to the presence of leading software company headquarters in the area as well as Lanco hills, a luxury hi-rise residential project, and an upcoming Chitrapuri Colony residential layout for Telugu cinema workers.[1][2]
