---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/L%C3%A1szl%C3%B3_Lov%C3%A1sz'
offline_file: ""
offline_thumbnail: ""
uuid: d6a4c68b-7418-485d-8b59-3d2557c27242
updated: 1484309470
title: László Lovász
tags:
    - Biography
    - Awards
    - Books
    - Notes
categories:
    - Graph theorists
---
László Lovász (Hungarian pronunciation: [ˈlaːsloː ˈlovaːs]; born March 9, 1948) is a Hungarian mathematician, best known for his work in combinatorics, for which he was awarded the Wolf Prize and the Knuth Prize in 1999, and the Kyoto Prize in 2010. He is the current president of the Hungarian Academy of Sciences. He served as president of the International Mathematical Union between January 1, 2007 and December 31, 2010.[1]
