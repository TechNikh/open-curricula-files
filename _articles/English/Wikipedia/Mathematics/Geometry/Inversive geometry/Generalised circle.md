---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalised_circle
offline_file: ""
offline_thumbnail: ""
uuid: 4667caf6-7569-494a-ab65-dd531a61e70d
updated: 1484309258
title: Generalised circle
tags:
    - Equation in the extended complex plane
    - The transformation w = 1/z
    - Representation by Hermitian matrices
categories:
    - Inversive geometry
---
A generalized circle, also referred to as a "cline" or "circline", is a straight line or a circle. The concept is mainly used in inversive geometry, because straight lines and circles have very similar properties in that geometry and are best treated together.
