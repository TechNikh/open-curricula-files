---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_cone
offline_file: ""
offline_thumbnail: ""
uuid: 2b99fdeb-7e97-4097-a8ce-acf1c02eaa48
updated: 1484309193
title: Symmetric cone
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/120px-Jordan%252CPascual_1963_Kopenhagen.jpg'
tags:
    - Definitions
    - Group theoretic properties
    - Spectral decomposition in a Euclidean Jordan algebra
    - Definition
    - Power associativity
    - Idempotents and rank
    - Spectral decomposition
    - Spectral decomposition for an idempotent
    - Trace form
    - Simple Euclidean Jordan algebras
    - Central decomposition
    - List of all simple Euclidean Jordan algebras
    - Peirce decomposition
    - Reduction to Euclidean Hurwitz algebras
    - Exceptional and special Euclidean Jordan algebras
    - Positive cone in a Euclidean Jordan algebra
    - Definition
    - Quadratic representation
    - Homogeneity of positive cone
    - Euclidean Jordan algebra of a symmetric cone
    - Construction
    - Automorphism groups and trace form
    - Cartan decomposition
    - Iwasawa decomposition for cone
    - Complexification of a Euclidean Jordan algebra
    - Definition of complexification
    - Complexification of automorphism group
    - Structure groups
    - Spectral norm
    - Complex simple Jordan algebras
    - Symmetry groups of bounded domain and tube domain
    - Definitions
    - Möbius transformations
    - Cayley transform
    - Automorphism group of bounded domain
    - Automorphism group of tube domain
    - 3-graded Lie algebras
    - Iwasawa decomposition
    - Lie group structure
    - Generalizations
    - Notes
categories:
    - Convex geometry
---
In mathematics, symmetric cones, sometimes called domains of positivity, are open convex self-dual cones in Euclidean space which have a transitive group of symmetries, i.e. invertible operators that take the cone onto itself. By the Koecher–Vinberg theorem these correspond to the cone of squares in finite-dimensional real Euclidean Jordan algebras, originally studied and classified by Jordan, von Neumann & Wigner (1933). The tube domain associated with a symmetric cone is a noncompact Hermitian symmetric space of tube type. All the algebraic and geometric structures associated with the symmetric space can be expressed naturally in terms of the Jordan algebra. The other irreducible ...
