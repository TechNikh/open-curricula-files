---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/labour
offline_file: ""
offline_thumbnail: ""
uuid: a8a5796a-3b39-4f6d-ad38-800f86d81782
updated: 1484310258
title: labour
categories:
    - Dictionary
---
labour
     n 1: a social class comprising those who do manual labor or work
          for wages; "there is a shortage of skilled labor in this
          field" [syn: {labor}, {working class}, {proletariat}]
     2: concluding state of pregnancy; from the onset of labor to
        the birth of a child; "she was in labor for six hours"
        [syn: {parturiency}, {labor}, {confinement}, {lying-in}, {travail},
         {childbed}]
     3: a political party formed in Great Britain in 1900;
        characterized by the promotion of labor's interests and
        the socialization of key industries [syn: {Labour Party},
        {Labor Party}, {Labor}]
     4: productive work (especially physical ...
