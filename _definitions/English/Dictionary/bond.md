---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bond
offline_file: ""
offline_thumbnail: ""
uuid: b805682b-d87d-40c3-aa38-36258841fd5f
updated: 1484310399
title: bond
categories:
    - Dictionary
---
bond
     adj : held in slavery; "born of enslaved parents" [syn: {enslaved},
            {enthralled}, {in bondage}]
     n 1: an electrical force linking atoms [syn: {chemical bond}]
     2: a certificate of debt (usually interest-bearing or
        discounted) that is issued by a government or corporation
        in order to raise money; the issuer is required to pay a
        fixed sum annually until maturity and then a fixed sum to
        repay the principal [syn: {bond certificate}]
     3: a connection based on kinship or marriage or common
        interest; "the shifting alliances within a large family";
        "their friendship constitutes a powerful bond between
        them" ...
