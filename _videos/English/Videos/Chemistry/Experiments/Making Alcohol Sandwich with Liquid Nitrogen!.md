---
version: 1
type: video
provider: YouTube
id: g9lL6ZRpm3o
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Making%20Alcohol%20Sandwich%20with%20Liquid%20Nitrogen%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9989bb8c-4005-4a2e-bf18-0c9e26447c61
updated: 1486069661
title: Making Alcohol Sandwich with Liquid Nitrogen!
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/g9lL6ZRpm3o/default.jpg
    - https://i3.ytimg.com/vi/g9lL6ZRpm3o/1.jpg
    - https://i3.ytimg.com/vi/g9lL6ZRpm3o/2.jpg
    - https://i3.ytimg.com/vi/g9lL6ZRpm3o/3.jpg
---
