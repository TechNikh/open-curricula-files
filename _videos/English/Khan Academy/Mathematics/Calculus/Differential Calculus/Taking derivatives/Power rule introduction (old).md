---
version: 1
type: video
provider: YouTube
id: z1lwai-lIzY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Power%20rule%20introduction%20%28old%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 31262de9-bc5c-4e6a-9f54-3fdae403bdab
updated: 1486069693
title: Power rule introduction (old)
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/z1lwai-lIzY/default.jpg
    - https://i3.ytimg.com/vi/z1lwai-lIzY/1.jpg
    - https://i3.ytimg.com/vi/z1lwai-lIzY/2.jpg
    - https://i3.ytimg.com/vi/z1lwai-lIzY/3.jpg
---
