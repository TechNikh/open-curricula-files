---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/restoration
offline_file: ""
offline_thumbnail: ""
uuid: 19053d56-318e-460b-b0ad-b6d467151aa5
updated: 1484310252
title: restoration
categories:
    - Dictionary
---
Restoration
     n 1: the reign of Charles II in England; 1660-1685
     2: the act of restoring something or someone to a satisfactory
        state
     3: getting something back again; "upon the restitution of the
        book to its rightful owner the child was given a tongue
        lashing" [syn: {restitution}, {return}, {regaining}]
     4: the state of being restored to its former good condition;
        "the inn was a renovation of a Colonial house" [syn: {renovation},
         {refurbishment}]
     5: some artifact that has been restored or reconstructed; "the
        restoration looked exactly like the original"
     6: a model that represents the landscape of a former ...
