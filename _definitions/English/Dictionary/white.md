---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/white
offline_file: ""
offline_thumbnail: ""
uuid: 8266578b-5795-43fa-ba41-62d9898d98d7
updated: 1484310305
title: white
categories:
    - Dictionary
---
white
     adj 1: being of the achromatic color of maximum lightness; having
            little or no hue owing to reflection of almost all
            incident light; "as white as fresh snow"; "a bride's
            white dress" [syn: {achromatic}] [ant: {black}]
     2: of or belonging to a racial group having light skin
        coloration; "voting patterns within the white population"
        [syn: {caucasian}] [ant: {black}]
     3: free from moral blemish or impurity; unsullied; "in shining
        white armor"
     4: marked by the presence of snow; "a white Christmas"; "the
        white hills of a northern winter" [syn: {snowy}]
     5: restricted to whites only; "under segregation ...
