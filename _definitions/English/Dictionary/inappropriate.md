---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inappropriate
offline_file: ""
offline_thumbnail: ""
uuid: 867db1a0-2eb9-4dd6-a13a-4762a58ee1f2
updated: 1484310464
title: inappropriate
categories:
    - Dictionary
---
inappropriate
     adj 1: not suitable for a particular occasion etc; "noise seems
            inappropriate at a time of sadness"; "inappropriate
            shoes for a walk on the beach"; "put inappropriate
            pressure on them" [ant: {appropriate}]
     2: not conforming with accepted standards of propriety or
        taste; undesirable; "incorrect behavior"; "she was seen in
        all the wrong places"; "He thought it was wrong for her to
        go out to work" [syn: {incorrect}, {wrong}]
     3: not in keeping with what is correct or proper; "completely
        inappropriate behavior" [syn: {incompatible}, {out or
        keeping(p)}, {unfitting}]
