---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clarification
offline_file: ""
offline_thumbnail: ""
uuid: 6384384d-02ee-4b03-b0b5-cb28cf91db24
updated: 1484310599
title: clarification
categories:
    - Dictionary
---
clarification
     n 1: an interpretation that removes obstacles to understanding;
          "the professor's clarification helped her to understand
          the textbook" [syn: {elucidation}, {illumination}]
     2: the act of removing solid particles from a liquid [syn: {clearing}]
