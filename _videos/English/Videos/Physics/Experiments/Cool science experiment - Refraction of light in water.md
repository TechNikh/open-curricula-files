---
version: 1
type: video
provider: YouTube
id: ogwUh-NieXE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 92cc9ad7-c0ff-4e0f-b8a7-ba50fa38702f
updated: 1486069635
title: 'Cool science experiment - Refraction of light in water'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/ogwUh-NieXE/default.jpg
    - https://i3.ytimg.com/vi/ogwUh-NieXE/1.jpg
    - https://i3.ytimg.com/vi/ogwUh-NieXE/2.jpg
    - https://i3.ytimg.com/vi/ogwUh-NieXE/3.jpg
---
