---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/narrate
offline_file: ""
offline_thumbnail: ""
uuid: 98141fb4-be5e-41da-a60a-07bd34995d63
updated: 1484310262
title: narrate
categories:
    - Dictionary
---
narrate
     v 1: provide commentary for a film, for example
     2: narrate or give a detailed account of; "Tell what happened";
        "The father told a story to his child" [syn: {tell}, {recount},
         {recite}]
