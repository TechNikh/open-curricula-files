---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extortion
offline_file: ""
offline_thumbnail: ""
uuid: 13b4591d-5b94-43ea-9180-9ae4c0e899da
updated: 1484310188
title: extortion
categories:
    - Dictionary
---
extortion
     n 1: an exorbitant charge
     2: unjust exaction (as by the misuse of authority); "the
        extortion by dishonest officials of fees for performing
        their sworn duty"
     3: the felonious act of extorting money (as by threats of
        violence)
