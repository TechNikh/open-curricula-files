---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/attained
offline_file: ""
offline_thumbnail: ""
uuid: 64becd8b-1c71-482f-a9c1-85cccaaaa816
updated: 1484310232
title: attained
categories:
    - Dictionary
---
attained
     adj : achieved or reached; "the actual attained achievement test
           score"
