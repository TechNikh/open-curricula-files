---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maintain
offline_file: ""
offline_thumbnail: ""
uuid: a4429fee-3604-4c54-96e5-a40f3d70b92d
updated: 1484310273
title: maintain
categories:
    - Dictionary
---
maintain
     v 1: keep in a certain state, position, or activity; e.g., "keep
          clean"; "hold in place"; "She always held herself as a
          lady"; "The students keep me on my toes" [syn: {keep}, {hold}]
     2: keep in safety and protect from harm, decay, loss, or
        destruction; "We preserve these archeological findings";
        "The old lady could not keep up the building"; "children
        must be taught to conserve our national heritage"; "The
        museum curator conserved the ancient manuscripts" [syn: {conserve},
         {preserve}, {keep up}]
     3: supply with necessities and support; "She alone sustained
        her family"; "The money will sustain our ...
