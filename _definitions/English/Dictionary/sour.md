---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sour
offline_file: ""
offline_thumbnail: ""
uuid: 651232d6-a958-48e9-8d17-0b831a9b6dcd
updated: 1484310353
title: sour
categories:
    - Dictionary
---
sour
     adj 1: smelling of fermentation or staleness [syn: {rancid}]
     2: having a sharp biting taste [ant: {sweet}]
     3: one of the four basic taste sensations; like the taste of
        vinegar or lemons
     4: in an unpalatable state; "sour milk" [syn: {off}, {turned}]
     5: inaccurate in pitch; "a false (or sour) note"; "her singing
        was off key" [syn: {false}, {off-key}]
     6: showing a brooding ill humor; "a dark scowl"; "the
        proverbially dour New England Puritan"; "a glum, hopeless
        shrug"; "he sat in moody silence"; "a morose and
        unsociable manner"; "a saturnine, almost misanthropic
        young genius"- Bruce Bliven; "a sour temper"; "a ...
