---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nail
offline_file: ""
offline_thumbnail: ""
uuid: 7c17ecd3-8ec5-4ba8-a4bc-585869a89b72
updated: 1484310226
title: nail
categories:
    - Dictionary
---
nail
     n 1: horny plate covering and protecting part of the dorsal
          surface of the digits
     2: a thin pointed piece of metal that is hammered into
        materials as a fastener
     3: a former unit of length for cloth equal to 1/16 of a yard
     v 1: attach something somewhere by means of nails; "nail the
          board onto the wall"
     2: take into custody; "the police nabbed the suspected
        criminals" [syn: {collar}, {apprehend}, {arrest}, {pick up},
         {nab}, {cop}]
     3: hit hard; "He smashed a 3-run homer" [syn: {smash}, {boom},
        {blast}]
     4: succeed in obtaining a position; "He nailed down a spot at
        Harvard" [syn: {nail down}, ...
