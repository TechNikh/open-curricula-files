---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transmission
offline_file: ""
offline_thumbnail: ""
uuid: 6bd1ab2f-ef8d-495a-a1ac-f115e0a9b0d5
updated: 1484310297
title: transmission
categories:
    - Dictionary
---
transmission
     n 1: the act of sending a message; causing a message to be
          transmitted [syn: {transmittal}, {transmitting}]
     2: communication by means of transmitted signals
     3: the fraction of radiant energy that passes through a
        substance [syn: {transmittance}]
     4: an incident in which an infectious disease is transmitted
        [syn: {infection}, {contagion}]
     5: the gears that transmit power from an automobile engine via
        the driveshaft to the live axle [syn: {transmission system}]
