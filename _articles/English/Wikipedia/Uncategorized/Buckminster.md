---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Buckminster
offline_file: ""
offline_thumbnail: ""
uuid: dc1eb540-76e6-413b-93f6-d5e7f17e6701
updated: 1484308392
title: Buckminster
categories:
    - Uncategorized
---
Buckminster is a village and civil parish within the Melton district of Leicestershire, England. The population (including Sewstern) of the civil parish was 356 at the 2011 census. It is on the B676 road, between Melton Mowbray and the A1.
