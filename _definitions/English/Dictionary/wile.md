---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wile
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484467801
title: wile
categories:
    - Dictionary
---
wile
     n : the use of tricks to deceive someone (usually to extract
         money from them) [syn: {trickery}, {chicanery}, {chicane},
          {guile}, {shenanigan}]
