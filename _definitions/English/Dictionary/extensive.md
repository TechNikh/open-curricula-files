---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extensive
offline_file: ""
offline_thumbnail: ""
uuid: 7cdd9ba8-525c-4194-896d-77462b4c7960
updated: 1484310258
title: extensive
categories:
    - Dictionary
---
extensive
     adj 1: large in spatial extent or range; "an extensive Roman
            settlement in northwest England"; "extended farm
            lands" [syn: {extended}]
     2: having broad range or effect; "had extensive press
        coverage"; "far-reaching changes in the social structure";
        "sweeping reforms" [syn: {far-reaching}, {sweeping}]
     3: large in number or quantity (especially of discourse); "she
        took copious notes"; "extensive press coverage"; "a
        subject of voluminous legislation" [syn: {copious}, {voluminous}]
     4: great in range or scope; "an extended vocabulary"; "surgeons
        with extended experience"; "extensive examples of picture
  ...
