---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Control_engineering
offline_file: ""
offline_thumbnail: ""
uuid: 9c6bb2c4-7064-4808-86cc-a5630ec2f4d1
updated: 1484308728
title: Control engineering
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Space_Shuttle_Columbia_launching.jpg
tags:
    - Overview
    - History
    - Control theory
    - Control systems
    - Control engineering education
    - Recent advancement
categories:
    - Control engineering
---
Control engineering or control systems engineering is the engineering discipline that applies control theory to design systems with desired behaviors. The practice uses sensors to measure the output performance of the device being controlled and those measurements can be used to give feedback to the input actuators that can make corrections toward desired performance. When a device is designed to perform without the need of human inputs for correction it is called automatic control (such as cruise control for regulating the speed of a car). Multi-disciplinary in nature, control systems engineering activities focus on implementation of control systems mainly derived by mathematical modeling ...
