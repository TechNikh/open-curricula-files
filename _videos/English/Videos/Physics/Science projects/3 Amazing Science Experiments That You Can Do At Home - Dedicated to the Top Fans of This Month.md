---
version: 1
type: video
provider: YouTube
id: 0Ojq-0KKOL0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8f951032-a8bb-4736-9632-2f2cb407e94d
updated: 1486069634
title: >
    3 Amazing Science Experiments That You Can Do At Home |
    Dedicated to the Top Fans of This Month
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/0Ojq-0KKOL0/default.jpg
    - https://i3.ytimg.com/vi/0Ojq-0KKOL0/1.jpg
    - https://i3.ytimg.com/vi/0Ojq-0KKOL0/2.jpg
    - https://i3.ytimg.com/vi/0Ojq-0KKOL0/3.jpg
---
