---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rutherford
offline_file: ""
offline_thumbnail: ""
uuid: 20367ddf-c2f3-4100-a7e7-445d9be2e50e
updated: 1484310389
title: rutherford
categories:
    - Dictionary
---
rutherford
     n 1: a unit strength of a radioactive source equal to one million
          disintegrations per second
     2: British chemist who isolated nitrogen (1749-1819) [syn: {Daniel
        Rutherford}]
     3: British physicist (born in New Zealand) who discovered the
        atomic nucleus and proposed a nuclear model of the atom
        (1871-1937) [syn: {Ernest Rutherford}, {First Baron
        Rutherford}, {First Baron Rutherford of Nelson}]
