---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bent_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: f0190cfd-8756-4342-b337-ed98450094eb
updated: 1484309275
title: Bent molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Oxygen-difluoride-3D-vdW.png
categories:
    - Molecular geometry
---
In chemistry, the term "bent" can be applied to certain molecules to describe their molecular geometry. Certain atoms, such as oxygen, will almost always set their two (or more) covalent bonds in non-collinear directions due to their electron configuration. Water (H2O) is an example of a bent molecule, as well as its analogues. The bond angle between the two hydrogen atoms is approximately 104.45°.[1] Nonlinear geometry is commonly observed for other triatomic molecules and ions containing only main group elements, prominent examples being nitrogen dioxide (NO2), sulfur dichloride (SCl2), and methylene (CH2).
