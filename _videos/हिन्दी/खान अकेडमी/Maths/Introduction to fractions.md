---
version: 1
type: video
provider: YouTube
id: 72te1wvDN6M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bf04c375-6d3b-47d8-8de4-5005aa9a36ad
updated: 1486069614
title: Introduction to fractions
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/72te1wvDN6M/default.jpg
    - https://i3.ytimg.com/vi/72te1wvDN6M/1.jpg
    - https://i3.ytimg.com/vi/72te1wvDN6M/2.jpg
    - https://i3.ytimg.com/vi/72te1wvDN6M/3.jpg
---
