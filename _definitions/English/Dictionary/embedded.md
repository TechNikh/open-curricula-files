---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/embedded
offline_file: ""
offline_thumbnail: ""
uuid: ef6e3bfb-501f-4a19-b9e8-095f81598a2d
updated: 1484310325
title: embedded
categories:
    - Dictionary
---
embed
     v : fix or set securely or deeply; "He planted a knee in the
         back of his opponent"; "The dentist implanted a tooth in
         the gum" [syn: {implant}, {engraft}, {imbed}, {plant}]
     [also: {embedding}, {embedded}]
