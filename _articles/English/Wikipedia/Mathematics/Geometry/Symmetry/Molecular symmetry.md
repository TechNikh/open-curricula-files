---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Molecular_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 5c6dd3c1-8972-4722-9e0b-8c7c14066500
updated: 1484309315
title: Molecular symmetry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Formaldehyde_symmetry_elements.svg.png
tags:
    - Symmetry concepts
    - Elements
    - Operations
    - Molecular symmetry groups
    - Groups
    - Point group
    - Examples
    - Common point groups
    - Representations
    - Character tables
    - Historical background
    - Nonrigid molecules
categories:
    - Symmetry
---
Molecular symmetry in chemistry describes the symmetry present in molecules and the classification of molecules according to their symmetry. Molecular symmetry is a fundamental concept in chemistry, as it can predict or explain many of a molecule's chemical properties, such as its dipole moment and its allowed spectroscopic transitions (based on selection rules such as the Laporte rule). Many university level textbooks on physical chemistry, quantum chemistry, and inorganic chemistry devote a chapter to symmetry.[1][2][3][4][5]
