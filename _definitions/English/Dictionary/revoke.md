---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revoke
offline_file: ""
offline_thumbnail: ""
uuid: 52e3a63e-e324-4e1b-9916-7d12697b1145
updated: 1484310593
title: revoke
categories:
    - Dictionary
---
revoke
     n : the mistake of not following suit when able to do so [syn: {renege}]
     v 1: fail to follow suit when able and required to do so
     2: annul by recalling or rescinding; "He revoked the ban on
        smoking"; "lift an embargo"; "vacate a death sentence"
        [syn: {annul}, {lift}, {countermand}, {reverse}, {repeal},
         {overturn}, {rescind}, {vacate}]
