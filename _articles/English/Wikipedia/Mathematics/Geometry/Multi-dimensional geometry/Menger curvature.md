---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Menger_curvature
offline_file: ""
offline_thumbnail: ""
uuid: cc653d65-2c75-4150-ac7d-860a2266b880
updated: 1484309287
title: Menger curvature
tags:
    - Definition
    - Integral Curvature Rectifiability
categories:
    - Multi-dimensional geometry
---
In mathematics, the Menger curvature of a triple of points in n-dimensional Euclidean space Rn is the reciprocal of the radius of the circle that passes through the three points. It is named after the Austrian-American mathematician Karl Menger.
