---
version: 1
type: video
provider: YouTube
id: 1V9wVmO0Tfg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Hubble%27s%20law.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2dee56eb-2caf-430e-a172-cd960fea9096
updated: 1486069686
title: "Hubble's law"
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/1V9wVmO0Tfg/default.jpg
    - https://i3.ytimg.com/vi/1V9wVmO0Tfg/1.jpg
    - https://i3.ytimg.com/vi/1V9wVmO0Tfg/2.jpg
    - https://i3.ytimg.com/vi/1V9wVmO0Tfg/3.jpg
---
