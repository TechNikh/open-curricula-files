---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaotic_bubble
offline_file: ""
offline_thumbnail: ""
uuid: 7d43f684-e28b-46c8-ab38-809f663c7714
updated: 1484308713
title: Chaotic bubble
categories:
    - Chaos theory
---
Many dynamic processes that generate bubbles are nonlinear, many exhibiting mathematically chaotic patterns consistent with chaos theory. In such cases, chaotic bubbles can be said to occur. In most systems they arise out of a forcing pressure that encounters some kind of resistance or shear factor, but the details vary depending on the particular context.
