---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Integral_graph
offline_file: ""
offline_thumbnail: ""
uuid: d79a0077-b9a6-496c-8ce7-18fe4c98e41b
updated: 1484309374
title: Integral graph
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory, an integral graph is a graph whose spectrum consists entirely of integers. In other words, a graphs is an integral graph if all the eigenvalues of its characteristic polynomial are integers.[1]
