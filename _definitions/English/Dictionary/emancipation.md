---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emancipation
offline_file: ""
offline_thumbnail: ""
uuid: 1fdb21e2-06d5-4479-bd98-8c12dcc3d4ec
updated: 1484310172
title: emancipation
categories:
    - Dictionary
---
emancipation
     n : freeing someone from the control of another; especially a
         parent's relinquishing authority and control over a minor
         child
