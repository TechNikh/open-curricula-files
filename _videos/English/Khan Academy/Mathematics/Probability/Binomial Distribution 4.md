---
version: 1
type: video
provider: YouTube
id: H0ZgOGWUcJw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Binomial%20Distribution%204.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9b1f1918-5eb3-422e-a578-1256c76f9a1a
updated: 1486069688
title: Binomial Distribution 4
categories:
    - Probability
thumbnail_urls:
    - https://i3.ytimg.com/vi/H0ZgOGWUcJw/default.jpg
    - https://i3.ytimg.com/vi/H0ZgOGWUcJw/1.jpg
    - https://i3.ytimg.com/vi/H0ZgOGWUcJw/2.jpg
    - https://i3.ytimg.com/vi/H0ZgOGWUcJw/3.jpg
---
