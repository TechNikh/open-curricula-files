---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feelings
offline_file: ""
offline_thumbnail: ""
uuid: 366bb774-d8d3-4508-affa-b70bff846e4d
updated: 1484310315
title: feelings
categories:
    - Dictionary
---
feelings
     n : emotional or moral sensitivity (especially in relation to
         personal principles or dignity); "the remark hurt his
         feelings"
