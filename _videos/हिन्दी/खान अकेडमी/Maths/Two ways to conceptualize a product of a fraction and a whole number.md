---
version: 1
type: video
provider: YouTube
id: vIZas6JNmYM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 490f73e5-fe30-4cb5-84e7-4e18068a988a
updated: 1486069611
title: >
    Two ways to conceptualize a product of a fraction and a
    whole number
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/vIZas6JNmYM/default.jpg
    - https://i3.ytimg.com/vi/vIZas6JNmYM/1.jpg
    - https://i3.ytimg.com/vi/vIZas6JNmYM/2.jpg
    - https://i3.ytimg.com/vi/vIZas6JNmYM/3.jpg
---
