---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dawn
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484480941
title: dawn
categories:
    - Dictionary
---
dawn
     n 1: the first light of day; "we got up before dawn"; "they
          talked until morning" [syn: {dawning}, {morning}, {aurora},
           {first light}, {daybreak}, {break of day}, {break of
          the day}, {dayspring}, {sunrise}, {sunup}, {cockcrow}]
          [ant: {sunset}]
     2: the earliest period; "the dawn of civilization"; "the
        morning of the world" [syn: {morning}]
     3: an opening time period; "it was the dawn of the Roman
        Empire"
     v 1: become clear or enter one's consciousness or emotions; "It
          dawned on him that she had betrayed him"; "she was
          penetrated with sorrow" [syn: {click}, {get through}, {come
          home}, ...
