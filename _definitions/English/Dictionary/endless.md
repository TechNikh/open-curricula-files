---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/endless
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484555521
title: endless
categories:
    - Dictionary
---
endless
     adj 1: tiresomely long; seemingly without end; "endless debates";
            "an endless conversation"; "the wait seemed eternal";
            "eternal quarreling"; "an interminable sermon" [syn: {eternal},
             {interminable}]
     2: infinitely great in number; "endless waves"
     3: having no known beginning and presumably no end; "the
        dateless rise and fall of the tides"; "time is endless";
        "sempiternal truth" [syn: {dateless}, {sempiternal}]
     4: having the ends united so as to form a continuous whole; "an
        endless chain"
     5: occurring so frequently as to seem ceaseless or
        uninterrupted; "a child's incessant questions"; ...
