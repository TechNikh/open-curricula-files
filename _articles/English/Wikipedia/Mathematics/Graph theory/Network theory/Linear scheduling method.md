---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_scheduling_method
offline_file: ""
offline_thumbnail: ""
uuid: 47b0a3f9-f0a4-425e-8072-8eaf171c2aa0
updated: 1484309435
title: Linear scheduling method
tags:
    - Application
    - Alternative names
categories:
    - Network theory
---
Linear Scheduling Method (LSM) is a graphical scheduling method focusing on continuous resource utilization in repetitive activities. It is believed (see Harris and McCaffer 2006:79*) that it originally adopted the idea of Line-Of-Balance method.
