---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/communalism
offline_file: ""
offline_thumbnail: ""
uuid: 27750f85-b3fc-40f7-a6bb-ada53c6d2308
updated: 1484310186
title: communalism
categories:
    - Dictionary
---
communalism
     n 1: the practice of communal living and common ownership
     2: loyalty and commitment to the interests of your own minority
        or ethnic group rather than to society as a whole
