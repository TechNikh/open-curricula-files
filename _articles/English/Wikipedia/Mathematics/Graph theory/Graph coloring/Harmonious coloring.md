---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Harmonious_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 04ce0e3e-008f-426c-9fe0-8210ea4ed4aa
updated: 1484309384
title: Harmonious coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Harmonious_coloring_tree.svg.png
categories:
    - Graph coloring
---
In graph theory, a harmonious coloring is a (proper) vertex coloring in which every pair of colors appears on at most one pair of adjacent vertices. The harmonious chromatic number χH(G) of a graph G is the minimum number of colors needed for any harmonious coloring of G.
