---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/balsam
offline_file: ""
offline_thumbnail: ""
uuid: be3e097d-9cbd-4e4a-9216-5a1f2e52b9c8
updated: 1484310214
title: balsam
categories:
    - Dictionary
---
balsam
     n 1: any seed plant yielding balsam
     2: any of various fragrant oleoresins used in medicines and
        perfumes
     3: a fragrant ointment containing a balsam resin
