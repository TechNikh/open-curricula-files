---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/owe
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484509261
title: owe
categories:
    - Dictionary
---
owe
     v 1: be obliged to pay or repay
     2: be indebted to, in an abstract or intellectual sense; "This
        new theory owes much to Einstein's Relativity Theory"
     3: be in debt; "She owes me $200"; "The thesis owes much to his
        adviser"
