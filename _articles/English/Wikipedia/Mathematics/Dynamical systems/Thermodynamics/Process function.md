---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Process_function
offline_file: ""
offline_thumbnail: ""
uuid: 30c55c2a-0850-449d-8de7-bba4b82d71a2
updated: 1484308857
title: Process function
categories:
    - Thermodynamics
---
In thermodynamics, a quantity that is well defined so as to describe the path of a process through the equilibrium state space of a thermodynamic system is termed a process function,[1] or, alternatively, a process quantity, or a path function. As an example, mechanical work and heat are process functions because they describe quantitatively the transition between equilibrium states of a thermodynamic system.
