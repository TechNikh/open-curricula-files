---
version: 1
type: video
provider: YouTube
id: 5VuzaLxiUgE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a57edbf3-1059-4157-af89-e7c1b55ac1b4
updated: 1486069658
title: Fire Physics "Trick"
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/5VuzaLxiUgE/default.jpg
    - https://i3.ytimg.com/vi/5VuzaLxiUgE/1.jpg
    - https://i3.ytimg.com/vi/5VuzaLxiUgE/2.jpg
    - https://i3.ytimg.com/vi/5VuzaLxiUgE/3.jpg
---
