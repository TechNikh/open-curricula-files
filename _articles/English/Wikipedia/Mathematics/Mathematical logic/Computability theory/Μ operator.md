---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%CE%9C_operator'
offline_file: ""
offline_thumbnail: ""
uuid: 19888693-76ce-4f45-b629-b46f16155c5f
updated: 1484309489
title: Μ operator
tags:
    - Definition
    - Properties
    - Examples
    - 'Example #1: The bounded μ-operator is a primitive recursive function'
    - 'Example #2: The unbounded μ-operator is not primitive-recursive'
    - 'Example #3: Definition of the unbounded μ operator in terms of an abstract machine'
    - Footnotes
    - Total function demonstration
    - >
        Alternative abstract machine models of the unbounded μ
        operator from Minsky (1967) and Boolos-Burgess-Jeffrey
        (2002)
categories:
    - Computability theory
---
In computability theory, the μ operator, minimization operator, or unbounded search operator searches for the least natural number with a given property. Adding the μ-operator to the five primitive recursive operators makes it possible to define all computable functions (given that the Church-Turing thesis is true).
