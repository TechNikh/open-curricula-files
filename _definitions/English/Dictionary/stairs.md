---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stairs
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484573041
title: stairs
categories:
    - Dictionary
---
stairs
     n : a way of access consisting of a set of steps [syn: {stairway},
          {staircase}, {steps}]
