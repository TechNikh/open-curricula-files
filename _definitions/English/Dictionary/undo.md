---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undo
offline_file: ""
offline_thumbnail: ""
uuid: 4dff3fd1-e286-45fb-803a-6f12b4c90e63
updated: 1484310565
title: undo
categories:
    - Dictionary
---
undo
     v 1: cancel, annul, or reverse an action or its effect; "I wish I
          could undo my actions"
     2: deprive of certain characteristics [syn: {unmake}] [ant: {do}]
     3: cause the ruin or downfall of; "A single mistake undid the
        President and he had to resign"
     4: cause to become loose; "undo the shoelace"; "untie the
        knot"; "loosen the necktie" [syn: {untie}, {loosen}]
     5: remove the outer cover or wrapping of; "Let's unwrap the
        gifts!"; "undo the parcel" [syn: {unwrap}] [ant: {wrap}]
     [also: {undone}, {undid}]
