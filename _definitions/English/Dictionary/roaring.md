---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roaring
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484579641
title: roaring
categories:
    - Dictionary
---
roaring
     adj 1: very lively and profitable; "flourishing businesses"; "a
            palmy time for stockbrokers"; "a prosperous new
            business"; "doing a roaring trade"; "a thriving
            tourist center"; "did a thriving business in orchids"
            [syn: {booming}, {flourishing}, {palmy}, {prospering},
             {prosperous}, {thriving}]
     2: loud enough to cause (temporary) hearing loss [syn: {deafening},
         {earsplitting}, {thunderous}, {thundery}]
     n 1: a deep prolonged loud noise [syn: {boom}, {roar}, {thunder}]
     2: a very loud utterance (like the sound of an animal); "his
        bellow filled the hallway" [syn: {bellow}, {bellowing}, ...
