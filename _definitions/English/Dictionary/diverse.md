---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diverse
offline_file: ""
offline_thumbnail: ""
uuid: 207f18e8-7177-4f72-a9b7-04e942438d8e
updated: 1484310464
title: diverse
categories:
    - Dictionary
---
diverse
     adj 1: many and different; "tourist offices of divers
            nationalities"; "a person of diverse talents" [syn: {divers(a)}]
     2: distinctly dissimilar or unlike; "diverse parts of the
        country"; "celebrities as diverse as Bob Hope and Bob
        Dylan"; "animals as various as the jaguar and the cavy and
        the sloth" [syn: {various}]
