---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cancellation
offline_file: ""
offline_thumbnail: ""
uuid: 6c1a0175-d951-47ce-9df2-80efc922c6a2
updated: 1484310577
title: cancellation
categories:
    - Dictionary
---
cancellation
     n 1: the act of cancelling; calling off some arrangement
     2: the speech act of revoking or annulling or making void
