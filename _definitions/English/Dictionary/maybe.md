---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maybe
offline_file: ""
offline_thumbnail: ""
uuid: 368c5f81-5c84-453d-bb49-98884d8b36a9
updated: 1484310484
title: maybe
categories:
    - Dictionary
---
maybe
     adv : by chance; "perhaps she will call tomorrow"; "we may
           possibly run into them at the concert"; "it may
           peradventure be thought that there never was such a
           time" [syn: {possibly}, {perchance}, {perhaps}, {mayhap},
            {peradventure}]
