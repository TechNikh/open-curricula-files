---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/basket
offline_file: ""
offline_thumbnail: ""
uuid: c6567883-9e4f-4c9c-8d60-b30f7d59e8b6
updated: 1484310451
title: basket
categories:
    - Dictionary
---
basket
     n 1: a container that is usually woven and has handles [syn: {handbasket}]
     2: the quantity contained in a basket [syn: {basketful}]
     3: horizontal hoop with a net through which players try to
        throw the basketball [syn: {basketball hoop}, {hoop}]
     4: a score in basketball made by throwing the ball through the
        hoop [syn: {field goal}]
