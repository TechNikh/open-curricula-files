---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forth
offline_file: ""
offline_thumbnail: ""
uuid: 78366c72-b63d-4d35-b734-fcb6026136b8
updated: 1484310387
title: forth
categories:
    - Dictionary
---
forth
     adv 1: from a particular thing or place or position (`forth' is
            obsolete); "ran away from the lion"; "wanted to get
            away from there"; "sent the children away to boarding
            school"; "the teacher waved the children away from the
            dead animal"; "went off to school"; "they drove off";
            "go forth and preach" [syn: {away}, {off}]
     2: forward in time or order or degree; "from that time forth";
        "from the sixth century onward" [syn: {forward}, {onward}]
     3: out into view; "came forth from the crowd"; "put my ideas
        forth"
