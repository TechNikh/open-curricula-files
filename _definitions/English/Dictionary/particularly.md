---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/particularly
offline_file: ""
offline_thumbnail: ""
uuid: ac483ddc-7980-42b7-9ca4-385c177b8850
updated: 1484310232
title: particularly
categories:
    - Dictionary
---
particularly
     adv 1: to a distinctly greater extent or degree than is common; "he
            was particularly fussy about spelling"; "a
            particularly gruesome attack"; "under peculiarly
            tragic circumstances"; "an especially (or specially)
            cautious approach to the danger" [syn: {peculiarly}, {especially},
             {specially}]
     2: specifically or especially distinguished from others; "loves
        Bach, particularly his partitas"; "recommended one book in
        particular"; "trace major population movements for the
        Pueblo groups in particular" [syn: {in particular}]
     3: uniquely or characteristically; "these peculiarly cinematic
 ...
