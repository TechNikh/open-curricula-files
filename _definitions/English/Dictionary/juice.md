---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/juice
offline_file: ""
offline_thumbnail: ""
uuid: b6a7d112-f33a-4eb2-9337-bda8a1208878
updated: 1484310328
title: juice
categories:
    - Dictionary
---
juice
     n 1: the liquid part that can be extracted from plant or animal
          tissue
     2: energetic vitality; "her creative juices were flowing"
     3: electric current; "when the wiring was finished they turned
        on the juice"
     4: any of several liquids of the body; "digestive juices" [syn:
         {succus}]
