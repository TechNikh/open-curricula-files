---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repertoire
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484542021
title: repertoire
categories:
    - Dictionary
---
repertoire
     n 1: the entire range of skills or aptitudes or devices used in a
          particular field or occupation; "the repertory of the
          supposed feats of mesmerism"; "has a large repertory of
          dialects and characters" [syn: {repertory}]
     2: a collection of works that an artist or company can perform
