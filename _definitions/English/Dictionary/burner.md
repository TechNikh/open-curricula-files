---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/burner
offline_file: ""
offline_thumbnail: ""
uuid: 8cbe9053-0b93-4260-aa45-c90045171847
updated: 1484310228
title: burner
categories:
    - Dictionary
---
burner
     n 1: an apparatus for burning fuel (or refuse); "a diesel engine
          is an oil burner"
     2: the heating elements of a stove or range on which pots and
        pans are placed for cooking; "the electric range had one
        large burner and three smaller one"
