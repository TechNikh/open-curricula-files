---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/significant
offline_file: ""
offline_thumbnail: ""
uuid: d099cd29-ede1-47d4-94fd-3db998f97f4c
updated: 1484310366
title: significant
categories:
    - Dictionary
---
significant
     adj 1: important in effect or meaning; "a significant change in tax
            laws"; "a significant change in the Constitution"; "a
            significant contribution"; "significant details";
            "statistically significant" [syn: {important}] [ant: {insignificant}]
     2: fairly large; "won by a substantial margin" [syn: {substantial}]
     3: too closely correlated to be attributed to chance and
        therefore indicating a systematic relation; "the
        interaction effect is significant at the .01 level"; "no
        significant difference was found" [ant: {nonsignificant}]
     4: rich in significance or implication; "a meaning look";
        "pregnant ...
