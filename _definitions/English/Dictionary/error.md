---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/error
offline_file: ""
offline_thumbnail: ""
uuid: 603450ee-30cd-428c-82c1-b707567a66f2
updated: 1484310206
title: error
categories:
    - Dictionary
---
error
     n 1: a wrong action attributable to bad judgment or ignorance or
          inattention; "he made a bad mistake"; "she was quick to
          point out my errors"; "I could understand his English in
          spite of his grammatical faults" [syn: {mistake}, {fault}]
     2: inadvertent incorrectness [syn: {erroneousness}]
     3: a misconception resulting from incorrect information [syn: {erroneous
        belief}]
     4: (baseball) a failure of a defensive player to make an out
        when normal play would have sufficed [syn: {misplay}]
     5: departure from what is ethically acceptable [syn: {wrongdoing}]
     6: (computer science) the occurrence of an incorrect result
     ...
