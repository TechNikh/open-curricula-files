---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/made
offline_file: ""
offline_thumbnail: ""
uuid: f60f47f3-61a5-4d7a-a6b6-bcd3725aec09
updated: 1484310333
title: made
categories:
    - Dictionary
---
make
     n 1: a recognizable kind; "there's a new brand of hero in the
          movies now"; "what make of car is that?" [syn: {brand}]
     2: the act of mixing cards haphazardly [syn: {shuffle}, {shuffling}]
     v 1: engage in; "make love, not war"; "make an effort"; "do
          research"; "do nothing"; "make revolution" [syn: {do}]
     2: give certain properties to something; "get someone mad";
        "She made us look silly"; "He made a fool of himself at
        the meeting"; "Don't make this into a big deal"; "This
        invention will make you a millionaire"; "Make yourself
        clear" [syn: {get}]
     3: make or cause to be or to become; "make a mess in one's
        ...
