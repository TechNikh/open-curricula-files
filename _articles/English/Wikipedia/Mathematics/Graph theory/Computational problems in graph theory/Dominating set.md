---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dominating_set
offline_file: ""
offline_thumbnail: ""
uuid: ee81bf54-e8fc-42e1-b16e-bf8a536128c9
updated: 1484309389
title: Dominating set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Dominating-set.svg.png
tags:
    - History
    - Bounds
    - Independent domination
    - Examples
    - Algorithms and computational complexity
    - L-reductions
    - Special cases
    - Exact algorithms
    - Parameterized complexity
    - Variants
    - Software for searching minimum dominating set
categories:
    - Computational problems in graph theory
---
In graph theory, a dominating set for a graph G = (V, E) is a subset D of V such that every vertex not in D is adjacent to at least one member of D. The domination number γ(G) is the number of vertices in a smallest dominating set for G.
