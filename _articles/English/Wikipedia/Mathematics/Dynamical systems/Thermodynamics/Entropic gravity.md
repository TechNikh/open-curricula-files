---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entropic_gravity
offline_file: ""
offline_thumbnail: ""
uuid: ac6deec6-597d-4101-875f-8244365fb5c2
updated: 1484308843
title: Entropic gravity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-NewtonsLawOfUniversalGravitation.svg.png
tags:
    - Origin
    - "Erik Verlinde's theory"
    - Derivation of the law of gravitation
    - Criticism and experimental tests
    - Entropic gravity and quantum coherence
categories:
    - Thermodynamics
---
Entropic gravity is a theory in modern physics that describes gravity as an entropic force—not a fundamental interaction mediated by a quantum field theory and a gauge particle (like photons for the electromagnetic force, and gluons for the strong nuclear force), but a consequence of physical systems' tendency to increase their entropy. The proposal has been intensely contested in the physics community but it has also sparked a new line of research into thermodynamic properties of gravity.
