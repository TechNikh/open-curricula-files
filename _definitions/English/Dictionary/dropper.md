---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dropper
offline_file: ""
offline_thumbnail: ""
uuid: dc039e64-ac64-469b-a19e-9174b01f6f4a
updated: 1484310349
title: dropper
categories:
    - Dictionary
---
dropper
     n : pipet consisting of a small tube with a vacuum bulb at one
         end for drawing liquid in and releasing it a drop at a
         time [syn: {eye dropper}]
