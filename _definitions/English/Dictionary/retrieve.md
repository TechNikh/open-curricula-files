---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retrieve
offline_file: ""
offline_thumbnail: ""
uuid: e33b2a91-5a51-4ed7-8b12-0ab29ff59269
updated: 1484310315
title: retrieve
categories:
    - Dictionary
---
retrieve
     v 1: get or find back; recover the use of; "She regained control
          of herself"; "She found her voice and replied quickly"
          [syn: {recover}, {find}, {regain}]
     2: of trained dogs
     3: recall knowledge from memory; have a recollection; "I can't
        remember saying any such thing"; "I can't think what her
        last name was"; "can you remember her phone number?"; "Do
        you remember that he once loved you?"; "call up memories"
        [syn: {remember}, {recall}, {call back}, {call up}, {recollect},
         {think}] [ant: {forget}]
