---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Neanderthalensis
offline_file: ""
offline_thumbnail: ""
uuid: 6c07b137-c9a0-4aec-b6ec-6bbcb1433c61
updated: 1484308344
title: Neanderthal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Carte_Neandertaliens.jpg
tags:
    - Name
    - Classification
    - Contemporary views
    - Origin
    - discovery
    - Timeline of research
    - Habitat and range
    - Anatomy
    - Behavior
    - Genome
    - Background
    - Interbreeding with modern humans
    - Epigenetics
    - Extinction hypotheses
    - Climate change
    - Coexistence with Homo sapiens
    - Interbreeding hypotheses
    - Specimens
    - Notable specimens
    - Chronology
    - Mixed with H. heidelbergensis traits
    - Typical H. neanderthalensis traits
    - Homo sapiens with some neanderthal-like archaic traits
    - Popular culture
    - Notes
categories:
    - Uncategorized
---
Neanderthals or Neandertals (UK /niˈændərˌtɑːl/, us also /neɪ/-, -/ˈɑːndər/-, -/ˌtɔːl/, -/ˌθɔːl/;[3][4] named for the Neandertal in Germany) were a species or subspecies of archaic human, in the genus Homo, which became extinct around 40,000 years ago. They were closely related to modern humans,[5][6] sharing 99.7% of DNA.[7] Remains left by Neanderthals include bone and stone tools, which are found in Eurasia, from Western Europe to Central and Northern Asia. Neanderthals are generally classified by paleontologists as the species Homo neanderthalensis, having separated from the Homo sapiens lineage 600,000 years ago, but a minority consider them to be a subspecies of Homo ...
