---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polynomial_code
offline_file: ""
offline_thumbnail: ""
uuid: 705e5b2c-0a2d-45e8-b87c-67340c85fb58
updated: 1484308558
title: Polynomial code
tags:
    - Definition
    - Example
    - Encoding
    - Example
    - Decoding
    - Properties of polynomial codes
    - Specific families of polynomial codes
categories:
    - Coding theory
---
In coding theory, a polynomial code is a type of linear code whose set of valid code words consists of those polynomials (usually of some fixed length) that are divisible by a given fixed polynomial (of shorter length, called the generator polynomial).
