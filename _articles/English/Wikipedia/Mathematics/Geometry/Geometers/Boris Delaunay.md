---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Boris_Delaunay
offline_file: ""
offline_thumbnail: ""
uuid: 4cd85e9e-8491-4963-bf9b-36098f0d3b72
updated: 1484309077
title: Boris Delaunay
tags:
    - Biography
    - Books
categories:
    - Geometers
---
Boris Nikolaevich Delaunay or Delone (Russian: Бори́с Никола́евич Делоне́; March 15, 1890 – July 17, 1980) was one of the first Russian mountain climbers and a Soviet/Russian mathematician, and the father of physicist Nikolai Borisovich Delone.
