---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ring_(chemistry)
offline_file: ""
offline_thumbnail: ""
uuid: 9a3c03c9-f73e-4be2-b183-99deb9a1fa3d
updated: 1484309280
title: Ring (chemistry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-First_four_cycloalkanes.png
tags:
    - Homocyclic and heterocyclic rings
    - Rings and ring systems
categories:
    - Molecular geometry
---
In chemistry, a ring is an ambiguous term referring either to a simple cycle of atoms and bonds in a molecule or to a connected set of atoms and bonds in which every atom and bond is a member of a cycle (also called a ring system). A ring system that is a simple cycle is called a monocycle or simple ring, and one that is not a simple cycle is called a polycycle or polycyclic ring system. A simple ring contains the same number of sigma bonds as atoms, and a polycyclic ring system contains more sigma bonds than atoms.
