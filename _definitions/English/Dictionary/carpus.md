---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carpus
offline_file: ""
offline_thumbnail: ""
uuid: 90eb56d7-04e7-45ae-b67d-c558b626e6a5
updated: 1484310163
title: carpus
categories:
    - Dictionary
---
carpus
     n : a joint between the distal end of the radius and the
         proximal row of carpal bones [syn: {wrist}, {wrist joint},
          {radiocarpal joint}, {articulatio radiocarpea}]
     [also: {carpi} (pl)]
