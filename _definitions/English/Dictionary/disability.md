---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disability
offline_file: ""
offline_thumbnail: ""
uuid: 7b2e917a-65c5-43f0-9761-c7901816bdbb
updated: 1484310599
title: disability
categories:
    - Dictionary
---
disability
     n : the condition of being unable to perform as a consequence of
         physical or mental unfitness; "reading disability";
         "hearing impairment" [syn: {disablement}, {handicap}, {impairment}]
