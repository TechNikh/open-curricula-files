---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bengali
offline_file: ""
offline_thumbnail: ""
uuid: 5dc33e6b-91f8-4908-8e32-5fac48b6e393
updated: 1484310607
title: bengali
categories:
    - Dictionary
---
Bengali
     adj : of or relating to or characteristic of Bengal or its people;
           "Bengali hills"
     n 1: (Hinduism) a member of a people living in Bangladesh and
          West Bengal (mainly Hindus)
     2: an ethnic group speaking Bengali and living in Bangladesh
        and eastern India
     3: a Magadhan language spoken by the Bengali people; the
        official language of Bangladesh and Bengal
