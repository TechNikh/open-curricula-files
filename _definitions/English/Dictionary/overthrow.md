---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overthrow
offline_file: ""
offline_thumbnail: ""
uuid: 4306f9e2-a121-4e87-894b-296722c8b418
updated: 1484310573
title: overthrow
categories:
    - Dictionary
---
overthrow
     n 1: the termination of a ruler or institution (especially by
          force)
     2: the act of disturbing the mind or body; "his carelessness
        could have caused an ecological upset"; "she was
        unprepared for this sudden overthrow of their normal way
        of living" [syn: {upset}, {derangement}]
     v 1: cause the downfall of; of rulers; "The Czar was overthrown";
          "subvert the ruling class" [syn: {subvert}, {overturn},
          {bring down}]
     2: rule against; "The Republicans were overruled when the House
        voted on the bill" [syn: {overrule}, {overturn}, {override},
         {reverse}]
     [also: {overthrown}, {overthrew}]
