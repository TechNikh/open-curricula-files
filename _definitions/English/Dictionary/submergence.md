---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/submergence
offline_file: ""
offline_thumbnail: ""
uuid: 584a93dd-f886-4e3e-944d-cc9ccb3e6908
updated: 1484310541
title: submergence
categories:
    - Dictionary
---
submergence
     n : sinking until covered completely with water [syn: {submerging},
          {submersion}, {immersion}]
