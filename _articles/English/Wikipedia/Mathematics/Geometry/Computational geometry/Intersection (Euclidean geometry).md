---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Intersection_(Euclidean_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 6228eec7-67cb-48c5-be96-34662b9d2ed6
updated: 1484309175
title: Intersection (Euclidean geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Schnittpunkt-2g.svg.png
tags:
    - On a plane
    - Two lines
    - Two line segments
    - A line and a circle
    - Two circles
    - Two conic sections
    - Two curves
    - Two polygons
    - In space (three dimensions)
    - A line and a plane
    - Three planes
    - A curve and a surface
    - A line and a polyhedron
    - Two surfaces
categories:
    - Computational geometry
---
In geometry, an intersection is a point, line, or curve common in two or more objects (such as lines, curves, planes, and surfaces). The most simple case in Euclidean geometry is the intersection points of two distinct lines, that is either one point or does not exist if lines are parallel.
