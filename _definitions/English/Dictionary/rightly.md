---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rightly
offline_file: ""
offline_thumbnail: ""
uuid: 0cb86edc-a272-421c-b3b4-b7af79b5d739
updated: 1484310355
title: rightly
categories:
    - Dictionary
---
rightly
     adv : with honesty; "he was rightly considered the greatest singer
           of his time" [syn: {justly}, {justifiedly}] [ant: {unjustly}]
