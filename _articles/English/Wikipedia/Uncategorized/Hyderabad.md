---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyderabad
offline_file: ""
offline_thumbnail: ""
uuid: 79d46e26-dbe8-4567-91c1-78fecd7a6e01
updated: 1484308344
title: Hyderabad
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hyderabad_mills.jpg
tags:
    - History
    - Toponymy
    - Early and medieval history
    - Modern history
    - geography
    - Topography
    - Climate
    - Conservation
    - Administration
    - Common capital status
    - Local government
    - Utility services
    - Pollution control
    - Healthcare
    - Demographics
    - Ethnic groups, language and religion
    - Slums
    - Cityscape
    - Neighbourhoods
    - Landmarks
    - Economy
    - culture
    - Literature
    - Music and films
    - Art and handicrafts
    - Cuisine
    - Media
    - Education
    - Sports
    - Transport
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
Hyderabad (i/ˈhaɪdərəˌbæd/ HY-dər-ə-bad; often /ˈhaɪdrəˌbæd/) is the capital of the southern Indian state of Telangana and de jure capital of Andhra Pradesh.[A] Occupying 650 square kilometres (250 sq mi) along the banks of the Musi River, it has a population of about 6.7 million and a metropolitan population of about 7.75 million, making it the fourth most populous city and sixth most populous urban agglomeration in India. At an average altitude of 542 metres (1,778 ft), much of Hyderabad is situated on hilly terrain around artificial lakes, including Hussain Sagar—predating the city's founding—north of the city centre.
