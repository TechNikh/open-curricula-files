---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purified
offline_file: ""
offline_thumbnail: ""
uuid: 1c195187-73be-4c31-b779-206e3c56f845
updated: 1484310411
title: purified
categories:
    - Dictionary
---
purified
     adj : made pure [syn: {refined}, {sublimate}]
