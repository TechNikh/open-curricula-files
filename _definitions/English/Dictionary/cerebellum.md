---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cerebellum
offline_file: ""
offline_thumbnail: ""
uuid: 4db08a06-6531-46fa-b335-6d42ad85633a
updated: 1484310162
title: cerebellum
categories:
    - Dictionary
---
cerebellum
     n : a major division of the vertebrate brain; situated above the
         medulla oblongata and beneath the cerebrum in humans
     [also: {cerebella} (pl)]
