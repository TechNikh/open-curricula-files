---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Broken_Picture_Telephone
offline_file: ""
offline_thumbnail: ""
uuid: 7b250a3b-af03-4b27-ad56-afab598eb029
updated: 1484308524
title: Broken Picture Telephone
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/261px-Broken_Picture_Telephone_screenshot_-_drawing_interface.png
tags:
    - Gameplay
    - History
    - Reception
    - Similar games
    - Screenshots
categories:
    - Cooperative games
---
Broken Picture Telephone, sometimes abbreviated BPT, is a collaborative multiplayer online drawing and writing game, based on the game known as Chinese whispers or broken telephone.
