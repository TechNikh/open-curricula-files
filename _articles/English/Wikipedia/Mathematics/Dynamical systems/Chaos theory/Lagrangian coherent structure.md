---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lagrangian_coherent_structure
offline_file: ""
offline_thumbnail: ""
uuid: 9270b7b2-8fbb-491a-89e7-e3336ccc4334
updated: 1484308715
title: Lagrangian coherent structure
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/page1-400px-LCS_conceptual2.pdf.jpg
tags:
    - General definitions
    - Material surfaces
    - LCSs as exceptional material surfaces
    - LCSs vs. classical invariant manifolds
    - Objectivity (frame-indifference) of LCSs
    - Hyperbolic LCSs
    - 'Diagnostic approach: Finite-time Lyapunov exponent (FTLE) ridges'
    - Issues with inferring hyperbolic LCSs from FTLE ridges
    - 'Local variational approach: Shrink and stretch surfaces'
    - 'Global variational approach: Shrink- and stretchlines as null-geodesics'
    - Elliptic LCSs
    - Rotational coherence from the polar rotation angle (PRA)
    - >
        Rotational coherence from the Lagrangian-Averaged Vorticity
        Deviation (LAVD)
    - 'Stretching-based coherence from a local variational approach: Shear surfaces'
    - 'Stretching-based coherence from a global variational approach: lambda-lines'
    - Parabolic LCSs
    - 'Diagnostic approach: Finite-time Lyapunov Exponents (FTLE) trenches'
    - 'Global variational approach: Heteroclinic chains of null-geodesics'
    - Software packages for LCS computations
    - Further related papers
categories:
    - Chaos theory
---
Lagrangian coherent structures (LCSs) are distinguished surfaces of trajectories in a dynamical system that exert a major influence on nearby trajectories over a time interval of interest.[1][2][3] The type of this influence may vary, but it invariably creates a coherent trajectory pattern for which the underlying LCS serves as a theoretical centerpiece. In observations of tracer patterns in nature, one readily identifies coherent features, but it is often the underlying structure creating these features that is of interest.
