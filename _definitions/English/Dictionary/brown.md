---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brown
offline_file: ""
offline_thumbnail: ""
uuid: 18ebfd27-9eec-4925-96d9-66736dcbe2f2
updated: 1484310279
title: brown
categories:
    - Dictionary
---
brown
     adj : of a color similar to that of wood or earth [syn: {brownish},
            {dark-brown}]
     n 1: an orange of low brightness and saturation [syn: {brownness}]
     2: Scottish botanist who first observed the movement of small
        particles in fluids now known a Brownian motion
        (1773-1858) [syn: {Robert Brown}]
     3: abolitionist who was hanged after leading an unsuccessful
        raid at Harper's Ferry, Virginia (1800-1858) [syn: {John
        Brown}]
     4: a university in Rhode Island [syn: {Brown University}]
     v : fry in a pan until it changes color; "brown the meat in the
         pan"
