---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Catastrophe_theory
offline_file: ""
offline_thumbnail: ""
uuid: 7bd4867a-1884-4016-9b84-500cd43e193a
updated: 1484308696
title: Catastrophe theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Fold_bifurcation.svg.png
tags:
    - Elementary catastrophes
    - Potential functions of one active variable
    - Fold catastrophe
    - Cusp catastrophe
    - Swallowtail catastrophe
    - Butterfly catastrophe
    - Potential functions of two active variables
    - Hyperbolic umbilic catastrophe
    - Elliptic umbilic catastrophe
    - Parabolic umbilic catastrophe
    - "Arnold's notation"
    - Bibliography
categories:
    - Bifurcation theory
---
In mathematics, catastrophe theory is a branch of bifurcation theory in the study of dynamical systems; it is also a particular special case of more general singularity theory in geometry.
