---
version: 1
type: video
provider: YouTube
id: JOBwqwxgJqc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e57b97f5-b68d-4f3d-af6d-a0dc221e756a
updated: 1486069670
title: Eukaryotic transcription
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/JOBwqwxgJqc/default.jpg
    - https://i3.ytimg.com/vi/JOBwqwxgJqc/1.jpg
    - https://i3.ytimg.com/vi/JOBwqwxgJqc/2.jpg
    - https://i3.ytimg.com/vi/JOBwqwxgJqc/3.jpg
---
