---
version: 1
type: video
provider: YouTube
id: SV7U4yAXL5I
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Enthalpy-%20Crash%20Course%20Chemistry%20%2318.webm"
offline_file: ""
offline_thumbnail: ""
uuid: eb04025f-2f94-404a-9359-fa6609b504e3
updated: 1486069680
title: 'Enthalpy- Crash Course Chemistry #18'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/SV7U4yAXL5I/default.jpg
    - https://i3.ytimg.com/vi/SV7U4yAXL5I/1.jpg
    - https://i3.ytimg.com/vi/SV7U4yAXL5I/2.jpg
    - https://i3.ytimg.com/vi/SV7U4yAXL5I/3.jpg
---
