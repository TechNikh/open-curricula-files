---
title: Gliricidia Sepium - Quickstick Plant
youtube_id: xWrqVE8Hwmk
tags:
    - Garden
    - Gardening
    - Gardens
    - Plant
    - Plants
    - Nature
    - Natural
    - Eco
    - Environment
    - Gliricidia sepium
    - Gliricidia
    - Sepium
    - Quickstick Plant
    - Quickstick
    - Quickstick Plants
    - Green
categories:
    - soil
---
