---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/building
offline_file: ""
offline_thumbnail: ""
uuid: 6faa5f67-8b01-4765-b375-13f53bfae839
updated: 1484310256
title: building
categories:
    - Dictionary
---
building
     n 1: a structure that has a roof and walls and stands more or
          less permanently in one place; "there was a three-story
          building on the corner"; "it was an imposing edifice"
          [syn: {edifice}]
     2: the act of constructing or building something; "during the
        construction we had to take a detour"; "his hobby was the
        building of boats" [syn: {construction}]
     3: the commercial activity involved in constructing buildings;
        "their main business is home construction"; "workers in
        the building trades" [syn: {construction}]
     4: the occupants of a building; "the entire building complained
        about the noise"
