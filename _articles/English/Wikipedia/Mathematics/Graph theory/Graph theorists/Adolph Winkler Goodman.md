---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adolph_Winkler_Goodman
offline_file: ""
offline_thumbnail: ""
uuid: 6d2a071a-35fd-4231-9325-d29a0ed0a998
updated: 1484309463
title: Adolph Winkler Goodman
tags:
    - Life and work
    - Selected works
    - Notes
    - Biographical references
    - Additional sources
categories:
    - Graph theorists
---
Adolph Winkler Goodman (July 20, 1915 – July 30, 2004) was an American mathematician who contributed to number theory, graph theory and to the theory of univalent functions:[2] The conjecture on the coefficients of multivalent functions named after him is considered the most interesting challenge in the area after the Bieberbach conjecture, proved by Louis de Branges in 1985.[3]
