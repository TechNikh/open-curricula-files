---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/binding
offline_file: ""
offline_thumbnail: ""
uuid: c039a9c1-6969-4015-96d8-3448728dd106
updated: 1484310419
title: binding
categories:
    - Dictionary
---
binding
     adj 1: executed with proper legal authority; "a binding contract"
     2: hindering freedom of movement; "tight garments are
        uncomfortably binding" [syn: {constricting}]
     3: causing constipation [syn: {constipating}]
     n 1: the capacity to attract and hold something
     2: strip sewn over or along an edge for reinforcement or
        decoration
     3: the act of applying a bandage [syn: {dressing}, {bandaging}]
     4: the front and back covering of a book; "the book had a
        leather binding" [syn: {book binding}, {cover}, {back}]
