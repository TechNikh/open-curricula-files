---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seen
offline_file: ""
offline_thumbnail: ""
uuid: 023b4b05-7346-4503-ba1f-0a5c17264d11
updated: 1484310321
title: seen
categories:
    - Dictionary
---
see
     n : the seat within a bishop's diocese where his cathedral is
         located
     adv : compare (used in texts to point the reader to another
           location in the text) [syn: {cf.}, {cf}, {confer}, {see
           also}]
     v 1: perceive by sight or have the power to perceive by sight;
          "You have to be a good observer to see all the details";
          "Can you see the bird in that tree?"; "He is blind--he
          cannot see"
     2: perceive (an idea or situation) mentally; "Now I see!"; "I
        just can't see your point"; "Does she realize how
        important this decision is?"; "I don't understand the
        idea" [syn: {understand}, {realize}, ...
