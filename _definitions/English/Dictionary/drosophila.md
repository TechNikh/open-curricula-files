---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drosophila
offline_file: ""
offline_thumbnail: ""
uuid: 494b0a7e-eb5d-4b22-86bd-690c8b454f46
updated: 1484310295
title: drosophila
categories:
    - Dictionary
---
drosophila
     n : small fruit fly used by Thomas Hunt Morgan in studying basic
         mechanisms of inheritance [syn: {Drosophila melanogaster}]
     [also: {drosophilae} (pl)]
