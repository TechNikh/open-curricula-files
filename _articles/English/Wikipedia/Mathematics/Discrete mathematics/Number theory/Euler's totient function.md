---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Euler%27s_totient_function'
offline_file: ""
offline_thumbnail: ""
uuid: 5894a1de-0616-4004-89ce-2b98d5671600
updated: 1484308635
title: "Euler's totient function"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-EulerPhi.svg.png
tags:
    - History, terminology, and notation
    - "Computing Euler's totient function"
    - "Euler's product formula"
    - The function φ(n) is multiplicative
    - φ(pk) = pk − pk−1 = pk−1(p − 1)
    - "Proof of Euler's product formula"
    - Example
    - Fourier transform
    - Divisor sum
    - Riemann zeta function limit
    - Some values of the function
    - "Euler's theorem"
    - Other formulae
    - "Menon's identity"
    - Formulae involving the golden ratio
    - Generating functions
    - Growth rate
    - Ratio of consecutive values
    - Totient numbers
    - "Ford's theorem"
    - Applications
    - Cyclotomy
    - The RSA cryptosystem
    - Unsolved problems
    - "Lehmer's conjecture"
    - "Carmichael's conjecture"
    - Notes
categories:
    - Number theory
---
In number theory, Euler's totient function counts the positive integers up to a given integer n that are relatively prime to n. It is written using the Greek letter phi as φ(n) or ϕ(n), and may also be called Euler's phi function. It can be defined more formally as the number of integers k in the range 1 ≤ k ≤ n for which the greatest common divisor gcd(n, k) is equal to 1;[2][3] The integers k of this form are sometimes referred to as totatives of n.
