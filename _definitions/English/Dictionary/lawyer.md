---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lawyer
offline_file: ""
offline_thumbnail: ""
uuid: bde34a5a-7df5-4ebf-8725-08c1ee194a71
updated: 1484310221
title: lawyer
categories:
    - Dictionary
---
lawyer
     n : a professional person authorized to practice law; conducts
         lawsuits or gives legal advice [syn: {attorney}]
