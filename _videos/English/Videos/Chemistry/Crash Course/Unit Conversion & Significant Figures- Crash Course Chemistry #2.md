---
version: 1
type: video
provider: YouTube
id: hQpQ0hxVNTg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8ff084c9-146c-41c3-be06-690161ecc3ae
updated: 1486069678
title: 'Unit Conversion & Significant Figures- Crash Course Chemistry #2'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/hQpQ0hxVNTg/default.jpg
    - https://i3.ytimg.com/vi/hQpQ0hxVNTg/1.jpg
    - https://i3.ytimg.com/vi/hQpQ0hxVNTg/2.jpg
    - https://i3.ytimg.com/vi/hQpQ0hxVNTg/3.jpg
---
