---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Forcing_(recursion_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 0a951062-77b1-4339-a45c-db818041ec54
updated: 1484309487
title: Forcing (recursion theory)
categories:
    - Computability theory
---
Forcing in recursion theory is a modification of Paul Cohen's original set theoretic technique of forcing to deal with the effective concerns in recursion theory. Conceptually the two techniques are quite similar, in both one attempts to build generic objects (intuitively objects that are somehow 'typical') by meeting dense sets. Also both techniques are elegantly described as a relation (customarily denoted 
  
    
      
        ⊩
      
    
    {\displaystyle \Vdash }
  
) between 'conditions' and sentences. However, where set theoretic forcing is usually interested in creating objects that meet every dense set of conditions in the ground model, recursion theoretic forcing only aims ...
