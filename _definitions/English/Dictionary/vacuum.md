---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vacuum
offline_file: ""
offline_thumbnail: ""
uuid: dafc0dde-7dd4-4642-be51-132624432f28
updated: 1484310216
title: vacuum
categories:
    - Dictionary
---
vacuum
     n 1: the absence of matter [syn: {vacuity}]
     2: an empty area or space; "the huge desert voids"; "the
        emptiness of outer space"; "without their support he'll be
        ruling in a vacuum" [syn: {void}, {vacancy}, {emptiness}]
     3: a region empty of matter [syn: {vacuity}]
     4: an electrical home appliance that cleans by suction [syn: {vacuum
        cleaner}]
     v : clean with a vacuum cleaner; "vacuum the carpets" [syn: {vacuum-clean},
          {hoover}]
     [also: {vacua} (pl)]
