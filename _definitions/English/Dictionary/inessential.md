---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inessential
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484618041
title: inessential
categories:
    - Dictionary
---
inessential
     adj 1: not basic or fundamental [syn: {unessential}] [ant: {essential}]
     2: not absolutely necessary
     n : anything that is not essential; "they discarded all their
         inessentials" [syn: {nonessential}] [ant: {necessity}]
