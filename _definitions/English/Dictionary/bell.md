---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bell
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484471761
title: bell
categories:
    - Dictionary
---
bell
     n 1: a hollow device made of metal that makes a ringing sound
          when struck
     2: a push button at an outer door that gives a ringing or
        buzzing signal when pushed [syn: {doorbell}, {buzzer}]
     3: the sound of a bell being struck; "saved by the bell"; "she
        heard the distant toll of church bells" [syn: {toll}]
     4: (nautical) each of the eight half-hour units of nautical
        time signaled by strokes of a ship's bell; eight bells
        signals 4:00, 8:00, or 12:00 o'clock, either a.m. or p.m.
        [syn: {ship's bell}]
     5: the shape of a bell [syn: {bell shape}, {campana}]
     6: a phonetician and father of Alexander Graham Bell
        ...
