---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adjacency_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 5266d429-4741-4824-bfc8-9af1bafc6340
updated: 1484309371
title: Adjacency matrix
tags:
    - Definition
    - Adjacency matrix of a bipartite graph
    - Variations
    - Examples
    - Properties
    - Spectrum
    - Isomorphism and invariants
    - Matrix powers
    - Data structures
categories:
    - Algebraic graph theory
---
In graph theory, computer science, an adjacency matrix is a square matrix used to represent a finite graph. The elements of the matrix indicate whether pairs of vertices are adjacent or not in the graph.
