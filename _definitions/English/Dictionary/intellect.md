---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intellect
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484438161
title: intellect
categories:
    - Dictionary
---
intellect
     n 1: knowledge and intellectual ability; "he reads to improve his
          mind"; "he has a keen intellect" [syn: {mind}]
     2: the capacity for rational thought or inference or
        discrimination; "we are told that man is endowed with
        reason and capable of distinguishing good from evil" [syn:
         {reason}, {understanding}]
     3: a person who uses the mind creatively [syn: {intellectual}]
