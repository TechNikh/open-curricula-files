---
version: 1
type: video
provider: YouTube
id: 5Dzdrb8MKBg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Mixture%20problems%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2c3c4876-d924-475a-89c9-f5f406fa3699
updated: 1486069557
title: Mixture problems 1
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/5Dzdrb8MKBg/default.jpg
    - https://i3.ytimg.com/vi/5Dzdrb8MKBg/1.jpg
    - https://i3.ytimg.com/vi/5Dzdrb8MKBg/2.jpg
    - https://i3.ytimg.com/vi/5Dzdrb8MKBg/3.jpg
---
