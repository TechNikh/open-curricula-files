---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/progressively
offline_file: ""
offline_thumbnail: ""
uuid: cdcaf138-1719-414c-98b7-318cf4a767a6
updated: 1484310271
title: progressively
categories:
    - Dictionary
---
progressively
     adv : advancing in amount or intensity; "she became increasingly
           depressed" [syn: {increasingly}, {more and more}]
