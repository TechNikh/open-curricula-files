---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/listed
offline_file: ""
offline_thumbnail: ""
uuid: be100d9f-e67a-416f-ab2e-08f7d43ad8c7
updated: 1484310397
title: listed
categories:
    - Dictionary
---
listed
     adj 1: on a list [ant: {unlisted}]
     2: officially entered in a roll or list; "an enrolled student"
        [syn: {enrolled}]
