---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/occupational
offline_file: ""
offline_thumbnail: ""
uuid: 5c0daaad-d260-4273-8b2e-e66f91b79b01
updated: 1484310260
title: occupational
categories:
    - Dictionary
---
occupational
     adj : of or relating to the activity or business for which you are
           trained; "occupational hazard"
