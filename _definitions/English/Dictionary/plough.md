---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plough
offline_file: ""
offline_thumbnail: ""
uuid: f096f107-9944-4502-a8d5-6206c68ca0e4
updated: 1484310244
title: plough
categories:
    - Dictionary
---
Plough
     n 1: a group of seven bright stars in the constellation Ursa
          Major [syn: {Big Dipper}, {Dipper}, {Charles's Wain}, {Wain},
           {Wagon}]
     2: a farm tool having one or more heavy blades to break the
        soil and cut a furrow prior to sowing [syn: {plow}]
     v 1: move in a way resembling that of a plow cutting into or
          going through the soil; "The ship plowed through the
          water" [syn: {plow}]
     2: to break and turn over earth especially with a plow; "Farmer
        Jones plowed his east field last week"; "turn the earth in
        the Spring" [syn: {plow}, {turn}]
