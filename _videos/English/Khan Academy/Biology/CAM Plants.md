---
version: 1
type: video
provider: YouTube
id: xp6Zj24h8uA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/CAM%20Plants.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 64f8aee2-d8d0-4de6-b80b-73ea27e147ea
updated: 1486069583
title: CAM Plants
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/xp6Zj24h8uA/default.jpg
    - https://i3.ytimg.com/vi/xp6Zj24h8uA/1.jpg
    - https://i3.ytimg.com/vi/xp6Zj24h8uA/2.jpg
    - https://i3.ytimg.com/vi/xp6Zj24h8uA/3.jpg
---
How CAM Plants are able to fix carbon at night so they don't have to keep their stomata open during the day
More free lessons at: http://www.khanacademy.org/video?v=xp6Zj24h8uA
