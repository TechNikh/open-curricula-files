---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Folded_Reed%E2%80%93Solomon_code'
offline_file: ""
offline_thumbnail: ""
uuid: 5844e8b4-bdfe-42f6-a6bf-b4abc0a99853
updated: 1484308558
title: Folded Reed–Solomon code
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/450px-Folding_of_Reed-Solomon_Code_with_folding_parameter_m%253D3.png'
tags:
    - History
    - Definition
    - Graphic description
    - Folded Reed–Solomon codes and the singleton bound
    - Why folding might help?
    - >
        How folded Reed–Solomon (FRS) codes and Parvaresh Vardy
        (PV) codes are related
    - Brief overview of list-decoding folded Reed–Solomon codes
    - Linear-algebraic list decoding algorithm
    - 'Step 1: The interpolation step'
    - 'Step 2: The root-finding step'
    - 'Step 3: The prune step'
    - Summary
categories:
    - Coding theory
---
In coding theory, folded Reed–Solomon codes are like Reed–Solomon codes, which are obtained by mapping 
  
    
      
        m
      
    
    {\displaystyle m}
  
 Reed–Solomon codewords over a larger alphabet by careful bundling of codeword symbols.
