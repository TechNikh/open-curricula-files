---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/endosperm
offline_file: ""
offline_thumbnail: ""
uuid: 3473689b-315e-4bd8-b0a4-e57a4181a2a3
updated: 1484310307
title: endosperm
categories:
    - Dictionary
---
endosperm
     n : nutritive tissue surrounding the embryo within seeds of
         flowering plants
