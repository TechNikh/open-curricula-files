---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nasal_mucus
offline_file: ""
offline_thumbnail: ""
uuid: 76ee2215-c992-4d76-9e1d-d966e1f95c3b
updated: 1484308365
title: Mucus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mucus_cells.png
tags:
    - Respiratory system
    - Diseases involving mucus
    - Cystic fibrosis
    - Mucus as a medical symptom
    - Cold weather and nasal mucus
    - Digestive system
    - Reproductive system
    - Notes
categories:
    - Uncategorized
---
In vertebrates, mucus (/mjuːkəs/ MYOO-kəss; adjectival form: "mucous") is a slippery secretion produced by, and covering, mucous membranes. Mucous fluid is rich in glycoproteins and water and is typically produced from cells found in mucous glands. Mucous fluid may also originate from mixed glands, which contain both serous and mucous cells. It is a viscous colloid containing antiseptic enzymes (such as lysozymes), immunoglobulins, inorganic salts, proteins such as lactoferrin,[1] and glycoproteins known as mucins that are produced by goblet cells in the mucous membranes and submucosal glands. This mucus serves to protect epithelial cells (that line the tubes) in the respiratory, ...
