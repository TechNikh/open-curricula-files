---
version: 1
type: video
provider: YouTube
id: ob5U8zPbAX4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Anatomy%20of%20a%20Neuron.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1eca8f82-b8aa-4233-96c3-0f785f636578
updated: 1486069579
title: Anatomy of a Neuron
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/ob5U8zPbAX4/default.jpg
    - https://i3.ytimg.com/vi/ob5U8zPbAX4/1.jpg
    - https://i3.ytimg.com/vi/ob5U8zPbAX4/2.jpg
    - https://i3.ytimg.com/vi/ob5U8zPbAX4/3.jpg
---
Introduction to the neuron and its anatomy
More free lessons at: http://www.khanacademy.org/video?v=ob5U8zPbAX4
