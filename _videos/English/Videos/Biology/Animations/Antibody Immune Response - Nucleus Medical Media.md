---
version: 1
type: video
provider: YouTube
id: lrYlZJiuf18
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Antibody%20Immune%20Response%20-%20Nucleus%20Medical%20Media.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7dd4c1b3-a560-4945-b819-f02006e12a8d
updated: 1486069670
title: Antibody Immune Response | Nucleus Medical Media
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/lrYlZJiuf18/default.jpg
    - https://i3.ytimg.com/vi/lrYlZJiuf18/1.jpg
    - https://i3.ytimg.com/vi/lrYlZJiuf18/2.jpg
    - https://i3.ytimg.com/vi/lrYlZJiuf18/3.jpg
---
