---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hip-hop
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495161
title: hip-hop
categories:
    - Dictionary
---
hip-hop
     n 1: an urban youth culture associated with rap music and the
          fashions of African-American residents of the inner city
     2: genre of African-American music of the 1980s and 1990s in
        which rhyming lyrics are chanted to a musical
        accompaniment; several forms of rap have emerged [syn: {rap},
         {rap music}]
