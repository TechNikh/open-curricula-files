---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marguerite
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484476202
title: marguerite
categories:
    - Dictionary
---
marguerite
     n 1: tall leafy-stemmed Eurasian perennial with white flowers;
          widely naturalized; often placed in genus Chrysanthemum
          [syn: {oxeye daisy}, {ox-eyed daisy}, {moon daisy}, {white
          daisy}, {Leucanthemum vulgare}, {Chrysanthemum
          leucanthemum}]
     2: perennial subshrub of the Canary Islands having usually pale
        yellow daisylike flowers; often included in genus
        Chrysanthemum [syn: {marguerite daisy}, {Paris daisy}, {Chrysanthemum
        frutescens}, {Argyranthemum frutescens}]
