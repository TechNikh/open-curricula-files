---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fictional
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484428861
title: fictional
categories:
    - Dictionary
---
fictional
     adj 1: related to or involving literary fiction; "clever fictional
            devices"; "a fictional treatment of the train robbery"
            [ant: {nonfictional}]
     2: formed or conceived by the imagination; "a fabricated excuse
        for his absence"; "a fancied wrong"; "a fictional
        character"; "used fictitious names"; "a made-up story"
        [syn: {fabricated}, {fancied}, {fictitious}, {invented}, {made-up}]
