---
version: 1
type: video
provider: YouTube
id: 0RRVV4Diomg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/The%20Periodic%20Table-%20Crash%20Course%20Chemistry%20%234.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 005c8120-a4ed-489a-bab8-b6283b035704
updated: 1486069680
title: 'The Periodic Table- Crash Course Chemistry #4'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/0RRVV4Diomg/default.jpg
    - https://i3.ytimg.com/vi/0RRVV4Diomg/1.jpg
    - https://i3.ytimg.com/vi/0RRVV4Diomg/2.jpg
    - https://i3.ytimg.com/vi/0RRVV4Diomg/3.jpg
---
