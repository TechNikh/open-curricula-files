---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/horse
offline_file: ""
offline_thumbnail: ""
uuid: 28c943d4-7547-45fa-bdd1-94e8a69c6d30
updated: 1484310277
title: horse
categories:
    - Dictionary
---
horse
     n 1: solid-hoofed herbivorous quadruped domesticated since
          prehistoric times [syn: {Equus caballus}]
     2: a padded gymnastic apparatus on legs
     3: troops trained to fight on horseback; "500 horse led the
        attack" [syn: {cavalry}, {horse cavalry}]
     4: a framework for holding wood that is being sawed [syn: {sawhorse},
         {sawbuck}, {buck}]
     5: a chessman in the shape of a horse's head; can move two
        squares horizontally and one vertically (or vice versa)
        [syn: {knight}]
     v : provide with a horse or horses
