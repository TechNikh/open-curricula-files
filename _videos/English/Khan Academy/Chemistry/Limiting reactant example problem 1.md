---
version: 1
type: video
provider: YouTube
id: YNriRslOk9A
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Limiting%20reactant%20example%20problem%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 38e2d190-fe4f-4422-b3f6-ba7f5e422333
updated: 1486069585
title: Limiting reactant example problem 1
tags:
    - Limiting
    - Reactant
    - Example
    - Problem
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/YNriRslOk9A/default.jpg
    - https://i3.ytimg.com/vi/YNriRslOk9A/1.jpg
    - https://i3.ytimg.com/vi/YNriRslOk9A/2.jpg
    - https://i3.ytimg.com/vi/YNriRslOk9A/3.jpg
---
A limiting reagent problem to calculate mass of product and mass of excess reactant leftover after reaction. Created by Sal Khan.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/limiting-reagent-stoichiometry/e/limiting_reagent_stoichiometry?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Watch the next lesson: https://www.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/limiting-reagent-stoichiometry/v/2015-ap-chemistry-free-response-2-a?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/chemical-reactions-stoichiome/limiting-reagent-stoichiometry/v/stoichiometry-limiting-reagent?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
