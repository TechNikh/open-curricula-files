---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Otto_Hesse
offline_file: ""
offline_thumbnail: ""
uuid: 672624d8-4f74-47ce-bdea-cfe67d2b54c8
updated: 1484309083
title: Otto Hesse
tags:
    - life
    - Works
categories:
    - Geometers
---
Ludwig Otto Hesse (22 April 1811 – 4 August 1874) was a German mathematician. Hesse was born in Königsberg, Prussia, and died in Munich, Bavaria. He worked mainly on algebraic invariants, and geometry. The Hessian matrix, the Hesse normal form, the Hesse configuration, the Hessian group, Hessian pairs, Hesse's theorem, Hesse pencil, and the Hesse transfer principle[1] are named after him. Many of Hesse's research findings were presented for the first time in Crelle's Journal or Hesse's textbooks.[2]
