---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/study
offline_file: ""
offline_thumbnail: ""
uuid: b6885c30-4505-4f03-846d-0c929e5d5510
updated: 1484310359
title: study
categories:
    - Dictionary
---
study
     n 1: a detailed critical inspection [syn: {survey}]
     2: applying the mind to learning and understanding a subject
        (especially by reading); "mastering a second language
        requires a lot of work"; "no schools offer graduate study
        in interior design" [syn: {work}]
     3: a written document describing the findings of some
        individual or group; "this accords with the recent study
        by Hill and Dale" [syn: {report}, {written report}]
     4: a state of deep mental absorption; "she is in a deep study"
     5: a room used for reading and writing and studying; "he
        knocked lightly on the closed door of the study"
     6: a branch of ...
