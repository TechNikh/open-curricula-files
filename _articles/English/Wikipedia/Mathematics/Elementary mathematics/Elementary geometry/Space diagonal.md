---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Space_diagonal
offline_file: ""
offline_thumbnail: ""
uuid: cb786012-5b69-4642-a7c0-3d83845df558
updated: 1484308942
title: Space diagonal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/750px-Earth-moon_1.jpg
tags:
    - Axial diagonal
    - Space diagonals of magic cubes
    - r-agonals
categories:
    - Elementary geometry
---
In geometry a space diagonal (also interior diagonal, body diagonal or triagonal) of a polyhedron is a line connecting two vertices that are not on the same face. Space diagonals contrast with face diagonals, which connect vertices on the same face (but not on the same edge) as each other.[1]
