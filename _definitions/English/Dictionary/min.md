---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/min
offline_file: ""
offline_thumbnail: ""
uuid: f6cceaf4-3474-48aa-afbf-9481670848ed
updated: 1484310429
title: min
categories:
    - Dictionary
---
min
     n 1: a unit of time equal to 60 seconds or 1/60th of an hour; "he
          ran a 4 minute mile" [syn: {minute}]
     2: any of the forms of Chinese spoken in Fukien province [syn:
        {Min dialect}, {Fukien}, {Fukkianese}, {Hokkianese}, {Amoy},
         {Taiwanese}]
     3: an Egyptian  god of procreation
