---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hexacode
offline_file: ""
offline_thumbnail: ""
uuid: b881fded-88c9-4c21-9c0d-08693d13b410
updated: 1484308551
title: Hexacode
categories:
    - Coding theory
---
In coding theory, the hexacode is a length 6 linear code of dimension 3 over the Galois field 
  
    
      
        G
        F
        (
        4
        )
        =
        {
        0
        ,
        1
        ,
        ω
        ,
        
          ω
          
            2
          
        
        }
      
    
    {\displaystyle GF(4)=\{0,1,\omega ,\omega ^{2}\}}
  
 of 4 elements defined by
