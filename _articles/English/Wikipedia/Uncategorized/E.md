---
version: 1
type: article
id: https://en.wikipedia.org/wiki/E
offline_file: ""
offline_thumbnail: ""
uuid: 2ad3b302-6929-4f27-a48c-5c3172de1650
updated: 1484308309
title: E
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-E_cursiva.gif
tags:
    - History
    - Use in writing systems
    - english
    - Other languages
    - Other systems
    - Most common letter
    - Related characters
    - Descendants and related characters in the Latin alphabet
    - Ancestors and siblings in other alphabets
    - Derived signs, symbols and abbreviations
    - Computing codes
    - Other representations
categories:
    - Uncategorized
---
E (named e /ˈiː/, plural ees)[1] is the fifth letter and the second vowel in the modern English alphabet and the ISO basic Latin alphabet. It is the most commonly used letter in many languages, including: Czech,[2] Danish,[2] Dutch,[2] English, Latvian[3] French,[4] German,[5] Hungarian,[2] Latin,[2] Norwegian,[2] Spanish,[6] and Swedish.[2]
