---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/backing
offline_file: ""
offline_thumbnail: ""
uuid: fe8d4d2d-bf3b-41d4-8f5f-78a0229752de
updated: 1484310174
title: backing
categories:
    - Dictionary
---
backing
     n 1: the act of providing approval and support; "his vigorous
          backing of the conservatives got him in trouble with
          progressives" [syn: {backup}, {championship}, {patronage}]
     2: something forming a back that is added for strengthening
        [syn: {mount}]
     3: financial resources provided to make some project possible;
        "the foundation provided support for the experiment" [syn:
         {support}, {financial support}, {funding}, {financial
        backing}]
