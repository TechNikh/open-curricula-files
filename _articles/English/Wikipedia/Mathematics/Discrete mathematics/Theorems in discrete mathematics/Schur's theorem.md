---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Schur%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 00f0e005-b281-43aa-808f-abcc0730036f
updated: 1484308675
title: "Schur's theorem"
tags:
    - Ramsey theory
    - Combinatorics
    - Differential geometry
    - Linear algebra
    - Functional analysis
    - Number theory
categories:
    - Theorems in discrete mathematics
---
In discrete mathematics, Schur's theorem is any of several theorems of the mathematician Issai Schur. In differential geometry, Schur's theorem is a theorem of Axel Schur. In functional analysis, Schur's theorem is often called Schur's property, also due to Issai Schur.
