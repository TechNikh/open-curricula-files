---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/many
offline_file: ""
offline_thumbnail: ""
uuid: 29c7822c-76e6-4b0e-adb9-f5ccdd1d4adf
updated: 1484310349
title: many
categories:
    - Dictionary
---
many
     adj : a quantifier that can be used with count nouns and is often
           preceded by `as' or `too' or `so' or `that'; amounting
           to a large but indefinite number; "many temptations";
           "the temptations are many"; "a good many"; "a great
           many"; "many directions"; "take as many apples as you
           like"; "too many clouds to see"; "never saw so many
           people" [ant: {few}]
