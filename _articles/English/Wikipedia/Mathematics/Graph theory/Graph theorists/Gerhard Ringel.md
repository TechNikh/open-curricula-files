---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gerhard_Ringel
offline_file: ""
offline_thumbnail: ""
uuid: 4f4a7dba-4afd-4de1-bf51-22562b7fe3fe
updated: 1484309475
title: Gerhard Ringel
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gerhard_Ringel_surfing.jpg
categories:
    - Graph theorists
---
Gerhard Ringel (October 28, 1919, in Kollnbrunn, Austria – June 24, 2008, in Santa Cruz, California) was a German mathematician who earned his Ph.D. from the University of Bonn in 1951. He was one of the pioneers in graph theory and contributed significantly to the proof of the Heawood conjecture (now the Ringel-Youngs theorem), a mathematical problem closely linked with the Four Color Theorem.
