---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sponsor
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484428981
title: sponsor
categories:
    - Dictionary
---
sponsor
     n 1: someone who supports or champions something [syn: {patron},
          {supporter}]
     2: an advocate who presents a person (as for an award or a
        degree or an introduction etc.) [syn: {presenter}]
     v 1: assume sponsorship of [syn: {patronize}, {patronise}]
     2: assume responsibility for or leadership of; "The senator
        announced that he would sponsor the health care plan"
     3: do one's shopping at; do business with; be a customer or
        client of [syn: {patronize}, {patronise}, {shop}, {shop at},
         {buy at}, {frequent}] [ant: {boycott}, {boycott}]
