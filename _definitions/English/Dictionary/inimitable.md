---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inimitable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484440261
title: inimitable
categories:
    - Dictionary
---
inimitable
     adj : defying imitation; matchless; "an inimitable style"
