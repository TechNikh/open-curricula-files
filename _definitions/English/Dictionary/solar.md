---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solar
offline_file: ""
offline_thumbnail: ""
uuid: fb3a9a99-7070-4241-a357-3688109aacf9
updated: 1484310266
title: solar
categories:
    - Dictionary
---
solar
     adj : relating to or derived from the sun or utilizing the
           energies of the sun; "solar eclipse"; "solar energy"
