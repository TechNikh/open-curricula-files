---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vibrant
offline_file: ""
offline_thumbnail: ""
uuid: d7b0df93-fdb5-42ec-bc53-200d27a8b842
updated: 1484310477
title: vibrant
categories:
    - Dictionary
---
vibrant
     adj : vigorous and active; "a vibrant group that challenged the
           system"; "a charming and vivacious hostess"; "a
           vivacious folk dance" [syn: {vivacious}]
