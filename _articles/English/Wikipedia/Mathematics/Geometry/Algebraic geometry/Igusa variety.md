---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Igusa_variety
offline_file: ""
offline_thumbnail: ""
uuid: f1737ecf-70db-41e1-b344-7c576b8254f5
updated: 1484309153
title: Igusa variety
categories:
    - Algebraic geometry
---
In mathematics, an Igusa curve is (roughly) a coarse moduli space of elliptic curves in characteristic p with a level p Igusa structure, where an Igusa structure on an elliptic curve E is roughly a point of order p of E(p) generating the kernel of V:E(p) p→ E. An Igusa variety is a higher-dimensional analogue of an Igusa curve. Igusa curves were studied by Igusa (1968) and Igusa varieties were introduced by Harris & Taylor (2001).
