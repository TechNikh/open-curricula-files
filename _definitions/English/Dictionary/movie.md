---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/movie
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484538721
title: movie
categories:
    - Dictionary
---
movie
     n : a form of entertainment that enacts a story by a sequence of
         images giving the illusion of continuous movement; "they
         went to a movie every Saturday night"; "the film was shot
         on location" [syn: {film}, {picture}, {moving picture}, {moving-picture
         show}, {motion picture}, {motion-picture show}, {picture
         show}, {pic}, {flick}]
