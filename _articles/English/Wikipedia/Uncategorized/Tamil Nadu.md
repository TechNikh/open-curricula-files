---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tamilnadu
offline_file: ""
offline_thumbnail: ""
uuid: 1211d146-ca65-46a4-a59e-005aef16ccff
updated: 1484308422
title: Tamil Nadu
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-WLA_lacma_12th_century_Maharishi_Agastya.jpg
tags:
    - History
    - Prehistory
    - Indus valley script between 2000 and 1500 BC
    - Sangam period (300 BC – AD 300)
    - Bhakti Movement
    - Medieval period (600–1300)
    - Chola Empire
    - Vijayanagar and Nayak period (1336–1646)
    - Power struggles of the 18th century (1692–1801)
    - During British rule (1801–1947)
    - India (1947–present)
    - geography
    - Climate
    - Flora and Fauna
    - National and state parks
    - Governance and administration
    - Administrative subdivisions
    - Politics
    - Pre-Independence
    - Post-independence
    - Demographics
    - religion
    - language
    - Education
    - culture
    - Literature
    - Festivals and traditions
    - Music
    - Arts and dance
    - Film industry
    - Television industry
    - Cuisine
    - Economy
    - Agriculture
    - Textiles and leather
    - Automobiles
    - Heavy industries and engineering
    - Electronics and software
    - Others
    - Infrastructure
    - Transport
    - Road
    - Rail
    - Airports
    - Seaport
    - Energy
    - Sports
    - Tourism
    - Notes
categories:
    - Uncategorized
---
Tamil Nadu (/ˈtæmɪl ˈnɑːduː/ TAM-il-NAH-doo;   pronunciation (help·info); literally The Land of Tamils (Tamil) or Tamil Country) is one of the 29 states of India. Its capital and largest city is Chennai (formerly known as Madras). Tamil Nadu[8] lies in the southernmost part of the Indian Peninsula and is bordered by the union territory of Puducherry and the South Indian states of Kerala, Karnataka, and Andhra Pradesh. It is bounded by the Eastern Ghats on the north, by the Nilgiri, the Anamalai Hills, and Kerala on the west, by the Bay of Bengal in the east, by the Gulf of Mannar and the Palk Strait on the southeast, and by the Indian Ocean on the south. The state shares a ...
