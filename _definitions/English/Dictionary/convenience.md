---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convenience
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484377921
title: convenience
categories:
    - Dictionary
---
convenience
     n 1: the state of being suitable or opportune; "chairs arranged
          for his own convenience"
     2: the quality of being useful and convenient; "they offered
        the convenience of an installment plan" [ant: {inconvenience}]
     3: a toilet that is available to the public [syn: {public
        toilet}, {comfort station}, {public convenience}, {public
        lavatory}, {restroom}, {toilet facility}, {wash room}]
     4: a device that is very useful for a particular job [syn: {appliance},
         {contraption}, {contrivance}, {gadget}, {gizmo}, {gismo},
         {widget}]
