---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/animator
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484428441
title: animator
categories:
    - Dictionary
---
animator
     n 1: someone who imparts energy and vitality and spirit to other
          people [syn: {energizer}, {energiser}, {vitalizer}, {vitaliser}]
     2: the technician who produces animated cartoons
