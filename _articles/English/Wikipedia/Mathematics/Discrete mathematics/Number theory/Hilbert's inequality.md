---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hilbert%27s_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: 73739f4a-a6cf-415a-97b2-90e68b976119
updated: 1484308644
title: "Hilbert's inequality"
tags:
    - Formulation
    - Extensions
categories:
    - Number theory
---
for any sequence u1,u2,... of complex numbers. It was first demonstrated by David Hilbert with the constant 2π instead of π; the sharp constant was found by Issai Schur. It implies that the discrete Hilbert transform is a bounded operator in ℓ2.
