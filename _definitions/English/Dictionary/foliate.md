---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foliate
offline_file: ""
offline_thumbnail: ""
uuid: 4a6f59f5-82b4-4c71-86f6-8a023f3579ea
updated: 1484310351
title: foliate
categories:
    - Dictionary
---
foliate
     adj 1: ornamented with foliage or foils; "foliate tracery"; "a
            foliated capital" [syn: {foliated}]
     2: (often used as a combining form) having or resembling a leaf
        or having a specified kind or number of leaves; "`foliate'
        is combined with the prefix `tri' to form the word
        `trifoliate'"
     3: (especially of metamorphic rock) having thin leaflike layers
        or strata [syn: {foliated}, {foliaceous}]
     v 1: hammer into thin flat foils; "foliate metal"
     2: decorate with leaves
     3: coat or back with metal foil; "foliate glass"
     4: number the pages of a book or manuscript [syn: {paginate}, {page}]
     5: grow leaves; "the ...
