---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/have
offline_file: ""
offline_thumbnail: ""
uuid: d94e3033-25de-4013-a36b-433ee0c0d722
updated: 1484310363
title: have
categories:
    - Dictionary
---
have
     n : a person who possesses great material wealth [syn: {rich
         person}, {wealthy person}]
     v 1: have or possess, either in a concrete or an abstract sense;
          "She has $1,000 in the bank"; "He has got two beautiful
          daughters"; "She holds a Master's degree from Harvard"
          [syn: {have got}, {hold}]
     2: have as a feature; "This restaurant features the most famous
        chefs in France" [syn: {feature}] [ant: {miss}]
     3: of mental or physical states or experiences; "get an idea";
        "experience vertigo"; "get nauseous"; "undergo a strange
        sensation"; "The chemical undergoes a sudden change"; "The
        fluid undergoes ...
