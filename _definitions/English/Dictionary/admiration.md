---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/admiration
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484465041
title: admiration
categories:
    - Dictionary
---
admiration
     n 1: a feeling of delighted approval and liking [syn: {esteem}]
     2: the feeling aroused by something strange and surprising
        [syn: {wonder}, {wonderment}]
     3: a favorable judgment; "a small token in admiration of your
        works" [syn: {appreciation}]
