---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/copy
offline_file: ""
offline_thumbnail: ""
uuid: 55695290-ae0a-490d-b540-8418e0428cdd
updated: 1484310301
title: copy
categories:
    - Dictionary
---
copy
     n 1: a reproduction of a written record (e.g. of a legal or
          school record) [syn: {transcript}]
     2: a secondary representation of an original; "she made a copy
        of the designer dress"
     3: matter to be printed; exclusive of graphical materials [syn:
         {written matter}]
     4: material suitable for a journalistic account; "catastrophes
        make good copy"
     v 1: copy down as is; "The students were made to copy the
          alphabet over and over"
     2: reproduce someone's behavior or looks; "The mime imitated
        the passers-by"; "Children often copy their parents or
        older siblings" [syn: {imitate}, {simulate}]
     3: biology: ...
