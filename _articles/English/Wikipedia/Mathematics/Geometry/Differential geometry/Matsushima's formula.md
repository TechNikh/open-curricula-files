---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Matsushima%27s_formula'
offline_file: ""
offline_thumbnail: ""
uuid: 00562bac-6895-4e76-9f70-02cb9b47033f
updated: 1484309230
title: "Matsushima's formula"
categories:
    - Differential geometry
---
In mathematics, Matsushima’s formula, introduced by Matsushima (1967), is a formula for the Betti numbers of a quotient of a symmetric space G/H by a discrete group, in terms of unitary representations of the group G. [1] The Matsushima–Murakami formula is a generalization giving dimensions of spaces of automorphic forms, introduced by Matsushima & Murakami (1968).[2]
