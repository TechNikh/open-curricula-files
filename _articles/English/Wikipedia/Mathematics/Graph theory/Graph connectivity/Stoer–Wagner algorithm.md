---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Stoer%E2%80%93Wagner_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 8d899ce2-6047-40e5-9756-10aa09590dde
updated: 1484309402
title: Stoer–Wagner algorithm
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Stoer_wagner-example-min-cut.gif
tags:
    - Stoer–Wagner minimum cut algorithm
    - Example
    - Proof of correctness
    - Time complexity
    - 'Example code[5]'
categories:
    - Graph connectivity
---
In graph theory, the Stoer–Wagner algorithm is a recursive algorithm to solve the minimum cut problem in undirected weighted graphs. It was proposed by Mechthild Stoer and Frank Wagner in 1995. The essential idea of this algorithm is to shrink the graph by merging the most intensive vertices, until the graph only contains two combined vertex sets.[2] After each shrinking, the weight of the merged cut would be stored in a list. Finally, the minimum weight cut in the list will be the minimum of the graph.
