---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decompose
offline_file: ""
offline_thumbnail: ""
uuid: 8c062a2c-c8f1-48e1-a0d4-0396971b0047
updated: 1484310283
title: decompose
categories:
    - Dictionary
---
decompose
     v 1: separate (substances) into constituent elements or parts
          [syn: {break up}, {break down}]
     2: lose a stored charge, magnetic flux, or current; "the
        particles disintegrated during the nuclear fission
        process" [syn: {disintegrate}, {decay}]
     3: break down; "The bodies decomposed in the heat" [syn: {rot},
         {molder}, {moulder}]
