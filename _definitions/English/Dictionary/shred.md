---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shred
offline_file: ""
offline_thumbnail: ""
uuid: 6d38f6ce-578e-43ae-b4ff-527c30ee9f1b
updated: 1484310341
title: shred
categories:
    - Dictionary
---
shred
     n 1: a tiny or scarcely detectable amount [syn: {scintilla}, {whit},
           {iota}, {tittle}, {smidgen}, {smidgeon}, {smidgin}, {smidge}]
     2: a small piece of cloth or paper [syn: {rag}, {tag}, {tag end},
         {tatter}]
     v : tear into shreds [syn: {tear up}, {rip up}]
     [also: {shredding}, {shredded}]
