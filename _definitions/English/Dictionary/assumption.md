---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assumption
offline_file: ""
offline_thumbnail: ""
uuid: 58a34970-1c22-4dea-a82a-b538b2433ffe
updated: 1484310303
title: assumption
categories:
    - Dictionary
---
assumption
     n 1: a statement that is assumed to be true and from which a
          conclusion can be drawn; "on the assumption that he has
          been injured we can infer that he will not to play"
          [syn: {premise}, {premiss}]
     2: a hypothesis that is taken for granted; "any society is
        built upon certain assumptions" [syn: {supposition}, {supposal}]
     3: the act of taking possession of or power over something;
        "his assumption of office coincided with the trouble in
        Cuba"; "the Nazi assumption of power in 1934"; "he
        acquired all the company's assets for ten million dollars
        and the assumption of the company's debts" [syn: {laying
 ...
