---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flowing
offline_file: ""
offline_thumbnail: ""
uuid: 512271a9-d820-4f4d-b3a2-ba7aa445a414
updated: 1484310208
title: flowing
categories:
    - Dictionary
---
flowing
     adj 1: smooth and unconstrained in movement; "a long, smooth
            stride"; "the fluid motion of a cat"; "the liquid
            grace of a ballerina"; "liquid prose" [syn: {fluent},
            {fluid}, {liquid}, {smooth}]
     2: (of water) rising to the surface under internal hydrostatic
        pressure; "an artesian well"; "artesian pressure" [syn: {artesian}]
        [ant: {subartesian}]
     3: moving smoothly and continuously; "crowds flowing through
        the canyons of the streets"; "fan streaming into the
        concert hall" [syn: {streaming}]
     4: (of liquids) moving freely; "a flowing brook" [syn: {streaming}]
     n : the motion characteristic of ...
