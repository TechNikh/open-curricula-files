---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/details
offline_file: ""
offline_thumbnail: ""
uuid: e8b92ccf-33c8-44e1-b3d9-6dea2a66802f
updated: 1484310293
title: details
categories:
    - Dictionary
---
details
     n : true confidential information; "after the trial he gave us
         the real details" [syn: {inside information}]
