---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Antimatroid
offline_file: ""
offline_thumbnail: ""
uuid: 01f8fca1-f508-428b-82a7-af34282e9682
updated: 1484309192
title: Antimatroid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Antimatroid.svg.png
tags:
    - Definitions
    - Examples
    - Paths and basic words
    - Convex geometries
    - Join-distributive lattices
    - Supersolvable antimatroids
    - Join operation and convex dimension
    - Enumeration
    - Applications
    - Notes
categories:
    - Convex geometry
---
In mathematics, an antimatroid is a formal system that describes processes in which a set is built up by including elements one at a time, and in which an element, once available for inclusion, remains available until it is included. Antimatroids are commonly axiomatized in two equivalent ways, either as a set system modeling the possible states of such a process, or as a formal language modeling the different sequences in which elements may be included. Dilworth (1940) was the first to study antimatroids, using yet another axiomatization based on lattice theory, and they have been frequently rediscovered in other contexts;[1] see Korte et al. (1991) for a comprehensive survey of ...
