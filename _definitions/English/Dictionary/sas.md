---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sas
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484346781
title: sas
categories:
    - Dictionary
---
SAS
     n : a specialist regiment of the British army that is trained in
         commando techniques of warfare and used in clandestine
         operations (especially against terrorist groups) [syn: {Special
         Air Service}]
