---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circular_muscles
offline_file: ""
offline_thumbnail: ""
uuid: 3d07d70a-63b9-4854-8d1f-14f90d7e0d36
updated: 1484308375
title: Sphincter
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Ileocaecal_sphincter.png
tags:
    - Functions
    - Classifications
    - Examples
categories:
    - Uncategorized
---
A sphincter is a circular muscle that normally maintains constriction of a natural body passage or orifice and which relaxes as required by normal physiological functioning. Sphincters are found in many animals. There are over 60 types in the human body, some microscopically small, in particular the millions of precapillary sphincters.[1] Sphincters relax at death, often releasing fluids.[2]
