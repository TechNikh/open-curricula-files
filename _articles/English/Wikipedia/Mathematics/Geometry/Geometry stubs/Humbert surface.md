---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Humbert_surface
offline_file: ""
offline_thumbnail: ""
uuid: 4dd5fdab-f5bc-4e72-9b2b-7c31e3d6b369
updated: 1484309365
title: Humbert surface
categories:
    - Geometry stubs
---
In algebraic geometry, a Humbert surface, studied by Humbert (1899), is a surface in the moduli space of principally polarized abelian surfaces consisting of the surfaces with a symmetric endomorphism of some fixed discriminant.
