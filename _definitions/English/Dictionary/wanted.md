---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wanted
offline_file: ""
offline_thumbnail: ""
uuid: b728eb4f-5ff3-4158-96b1-723858a40117
updated: 1484310473
title: wanted
categories:
    - Dictionary
---
wanted
     adj 1: desired or wished for or sought; "couldn't keep her eyes off
            the wanted toy"; "a wanted criminal"; "a wanted
            poster" [ant: {unwanted}]
     2: freely permitted or invited; "invited guests" [syn: {invited}]
     3: characterized by feeling or showing fond affection for; "a
        cherished friend"; "children are precious"; "a treasured
        heirloom"; "so good to feel wanted" [syn: {cherished}, {precious},
         {treasured}]
