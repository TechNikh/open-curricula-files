---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamiltonian_vector_field
offline_file: ""
offline_thumbnail: ""
uuid: 7ef68fd2-6940-42c6-917d-91f5d375b55a
updated: 1484308787
title: Hamiltonian vector field
tags:
    - Definition
    - Examples
    - Properties
    - Poisson bracket
categories:
    - Hamiltonian mechanics
---
In mathematics and physics, a Hamiltonian vector field on a symplectic manifold is a vector field, defined for any energy function or Hamiltonian. Named after the physicist and mathematician Sir William Rowan Hamilton, a Hamiltonian vector field is a geometric manifestation of Hamilton's equations in classical mechanics. The integral curves of a Hamiltonian vector field represent solutions to the equations of motion in the Hamiltonian form. The diffeomorphisms of a symplectic manifold arising from the flow of a Hamiltonian vector field are known as canonical transformations in physics and (Hamiltonian) symplectomorphisms in mathematics.
