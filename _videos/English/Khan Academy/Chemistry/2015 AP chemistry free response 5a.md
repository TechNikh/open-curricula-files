---
version: 1
type: video
provider: YouTube
id: RoEZkrDWqmc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/2015%20AP%20chemistry%20free%20response%205a.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 76895aa9-cfec-4e36-bd92-4c68e8df4c15
updated: 1486069583
title: 2015 AP chemistry free response 5a
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/RoEZkrDWqmc/default.jpg
    - https://i3.ytimg.com/vi/RoEZkrDWqmc/1.jpg
    - https://i3.ytimg.com/vi/RoEZkrDWqmc/2.jpg
    - https://i3.ytimg.com/vi/RoEZkrDWqmc/3.jpg
---
Finding the order of reaction for kinetics of bleaching food coloring. An alternative method of solving 2015 AP Chemistry free response 5a.
