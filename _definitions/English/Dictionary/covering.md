---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/covering
offline_file: ""
offline_thumbnail: ""
uuid: 6aea73cc-78f3-46aa-ab3c-1f1243b159cc
updated: 1484310283
title: covering
categories:
    - Dictionary
---
covering
     n 1: a natural object that covers or envelops; "under a covering
          of dust"; "the fox was flushed from its cover" [syn: {natural
          covering}, {cover}]
     2: an artifact that covers something else (usually to protect
        or shelter or conceal it)
     3: the act of concealing the existence of something by
        obstructing the view of it; "the cover concealed their
        guns from enemy aircraft" [syn: {cover}, {screening}, {masking}]
     4: the act of protecting something by covering it
     5: the work of applying something; "the doctor prescribed a
        topical application of iodine"; "a complete bleach
        requires several applications"; ...
