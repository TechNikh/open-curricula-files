---
version: 1
type: article
id: https://en.wikipedia.org/wiki/0_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 4af46dbc-78eb-4758-b5fb-8fed1eff1355
updated: 1484308907
title: 0 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Estela_C_de_Tres_Zapotes_0.jpg
tags:
    - Etymology
    - History
    - Ancient Near East
    - Classical antiquity
    - India and Southeast Asia
    - Epigraphy
    - China
    - Middle Ages
    - Transmission to Islamic culture
    - Transmission to Europe
    - Pre-Columbian Americas
    - Mathematics
    - Elementary algebra
    - Other branches of mathematics
    - Related mathematical terms
    - Physics
    - chemistry
    - Astronomy
    - Computer science
    - Other fields
    - Symbols and representations
    - Year label
    - Bibliography
categories:
    - Elementary arithmetic
---
0 (zero; BrE: /ˈzɪərəʊ/ or AmE: /ˈziːroʊ/) is both a number[1] and the numerical digit used to represent that number in numerals. The number 0 fulfills a central role in mathematics as the additive identity of the integers, real numbers, and many other algebraic structures. As a digit, 0 is used as a placeholder in place value systems. Names for the number 0 in English include zero, nought or (US) naught (/ˈnɔːt/), nil, or—in contexts where at least one adjacent digit distinguishes it from the letter "O"—oh or o (/ˈoʊ/). Informal or slang terms for zero include zilch and zip.[2] Ought and aught (/ˈɔːt/),[3] as well as cipher,[4] have also been used historically.[5]
