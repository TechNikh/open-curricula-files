---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/printed
offline_file: ""
offline_thumbnail: ""
uuid: 2f995fe2-d7f1-4c00-ac5d-557c46990c66
updated: 1484310212
title: printed
categories:
    - Dictionary
---
printed
     adj : written in print characters or produced by means of e.g. a
           printing press
