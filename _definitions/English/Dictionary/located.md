---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/located
offline_file: ""
offline_thumbnail: ""
uuid: e236f2d1-f95f-498b-b295-d50d30b5d722
updated: 1484310240
title: located
categories:
    - Dictionary
---
located
     adj : situated in a particular spot or position; "valuable
           centrally located urban land"; "strategically placed
           artillery"; "a house set on a hilltop"; "nicely
           situated on a quiet riverbank" [syn: {placed}, {set}, {situated}]
