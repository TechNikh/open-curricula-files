---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emitting
offline_file: ""
offline_thumbnail: ""
uuid: bc211b86-cc25-49fe-a022-b4e7bdcce016
updated: 1484310395
title: emitting
categories:
    - Dictionary
---
emit
     v 1: expel (gases or odors) [syn: {breathe}, {pass off}]
     2: give off, send forth, or discharge; as of light, heat, or
        radiation, vapor, etc.; "The ozone layer blocks some
        harmful rays which the sun emits" [syn: {give out}, {give
        off}] [ant: {absorb}]
     3: express audibly; utter sounds (not necessarily words); "She
        let out a big heavy sigh"; "He uttered strange sounds that
        nobody could understand" [syn: {utter}, {let out}, {let
        loose}]
     [also: {emitting}, {emitted}]
