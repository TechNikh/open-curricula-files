---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/annoy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484451781
title: annoy
categories:
    - Dictionary
---
annoy
     v : cause annoyance in; disturb, especially by minor
         irritations; "Mosquitoes buzzing in my ear really bothers
         me"; "It irritates me that she never closes the door
         after she leaves" [syn: {rag}, {get to}, {bother}, {get
         at}, {irritate}, {rile}, {nark}, {nettle}, {gravel}, {vex},
          {chafe}, {devil}]
