---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collected
offline_file: ""
offline_thumbnail: ""
uuid: 6edd7be5-98e4-4141-80b9-29f56ede4dc7
updated: 1484310284
title: collected
categories:
    - Dictionary
---
collected
     adj 1: brought together in one place; "the collected works of
            Milton"; "the gathered folds of the skirt" [syn: {gathered}]
            [ant: {uncollected}, {uncollected}]
     2: brought together into a group or crowd; "the accumulated
        letters in my office" [syn: {accumulated}, {amassed}, {assembled},
         {congregate}, {massed}]
     3: in full control of your faculties; "the witness remained
        collected throughout the cross-examination"; "perfectly
        poised and sure of himself"; "more self-contained and more
        dependable than many of the early frontiersmen"; "strong
        and self-possessed in the face of trouble" [syn: ...
