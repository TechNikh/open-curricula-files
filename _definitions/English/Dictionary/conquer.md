---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conquer
offline_file: ""
offline_thumbnail: ""
uuid: 49f9a19c-a0d0-4431-8013-05bfba48c795
updated: 1484310561
title: conquer
categories:
    - Dictionary
---
conquer
     v 1: to put down by force or authority; "suppress a nascent
          uprising"; "stamp down on littering"; "conquer one's
          desires" [syn: {suppress}, {stamp down}, {inhibit}, {subdue},
           {curb}]
     2: take possession of by force, as after an invasion; "the
        invaders seized the land and property of the inhabitants";
        "The army seized the town"; "The militia captured the
        castle" [syn: {appropriate}, {capture}, {seize}]
     3: overcome by conquest; "conquer your fears"; "conquer a
        country"
