---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/babel
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484636221
title: babel
categories:
    - Dictionary
---
babel
     n 1: a confusion of voices and other sounds
     2: (Genesis 11:1-11) a tower built by Noah's descendants
        (probably in Babylon) who intended it to reach up to
        heaven; God foiled them by confusing their language so
        they could no longer understand one another [syn: {Tower
        of Babel}]
