---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intensive
offline_file: ""
offline_thumbnail: ""
uuid: 0f6f1e49-44d4-4628-ae6e-44a56a8e3d5f
updated: 1484310448
title: intensive
categories:
    - Dictionary
---
intensive
     adj 1: characterized by a high degree or intensity; often used as a
            combining form; "the questioning was intensive";
            "intensive care"; "research-intensive"; "a
            labor-intensive industry"
     2: tending to give force or emphasis; "an intensive adverb"
     3: of agriculture; intended to increase productivity of a fixed
        area by expending more capital and labor; "intensive
        agriculture"; "intensive conditions" [ant: {extensive}]
     n : a modifier that has little meaning except to intensify the
         meaning it modifies; "`up' in `finished up' is an
         intensifier"; "`honestly' in `I honestly don't know' is
         an ...
