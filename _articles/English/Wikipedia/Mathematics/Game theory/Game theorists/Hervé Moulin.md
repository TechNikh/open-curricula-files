---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Herv%C3%A9_Moulin'
offline_file: ""
offline_thumbnail: ""
uuid: 895b3f46-0c35-4ed8-abff-07aaa6ea8a88
updated: 1484308509
title: Hervé Moulin
tags:
    - Biography
    - Coauthors
categories:
    - Game theorists
---
Hervé Moulin (born in 1950 in Paris) is the Donald J. Robertson Chair of Economics at the Adam Smith Business School at the University of Glasgow.[1] He is known for his research contributions in mathematical economics, in particular in the fields of mechanism design, social choice, game theory and fair division.[2][3][4] He has written six books and over 100 peer-reviewed articles.[5][6]
