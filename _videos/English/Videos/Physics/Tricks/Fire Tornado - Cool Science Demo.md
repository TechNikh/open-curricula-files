---
version: 1
type: video
provider: YouTube
id: hVvW47qL8w8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4a3e9e1e-55d4-4b8b-8166-56ec2bd93d5d
updated: 1486069660
title: 'Fire Tornado - Cool Science Demo'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/hVvW47qL8w8/default.jpg
    - https://i3.ytimg.com/vi/hVvW47qL8w8/1.jpg
    - https://i3.ytimg.com/vi/hVvW47qL8w8/2.jpg
    - https://i3.ytimg.com/vi/hVvW47qL8w8/3.jpg
---
