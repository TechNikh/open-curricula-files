---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/met
offline_file: ""
offline_thumbnail: ""
uuid: d0c01509-8ebb-4108-b30f-29b5f450bdcb
updated: 1484310216
title: met
categories:
    - Dictionary
---
meet
     adj : being precisely fitting and right; "it is only meet that she
           should be seated first" [syn: {fitting}]
     n : a meeting at which a number of athletic contests are held
         [syn: {sports meeting}]
     v 1: come together; "I'll probably see you at the meeting"; "How
          nice to see you again!" [syn: {ran into}, {encounter}, {run
          across}, {come across}, {see}]
     2: get together socially or for a specific purpose [syn: {get
        together}]
     3: be adjacent or come together; "The lines converge at this
        point" [syn: {converge}] [ant: {diverge}, {diverge}]
     4: fill or meet a want or need [syn: {satisfy}, {fill}, {fulfill},
     ...
