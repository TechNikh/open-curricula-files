---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Utm_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 05711738-37b8-4b4f-ba3f-389c2886b1e6
updated: 1484309492
title: Utm theorem
categories:
    - Computability theory
---
In computability theory the utm theorem, or Universal Turing machine theorem, is a basic result about Gödel numberings of the set of computable functions. It affirms the existence of a computable universal function, which is capable of calculating any other computable function. The universal function is an abstract version of the universal turing machine, thus the name of the theorem.
