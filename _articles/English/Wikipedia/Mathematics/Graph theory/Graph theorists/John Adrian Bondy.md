---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_Adrian_Bondy
offline_file: ""
offline_thumbnail: ""
uuid: 90511a06-77df-463e-b9d4-3b2fc4e87888
updated: 1484309459
title: John Adrian Bondy
tags:
    - Select books and publications
    - Notes
categories:
    - Graph theorists
---
John Adrian Bondy, (Born 1944) a dual British and Canadian citizen, was a professor of graph theory at the University of Waterloo, in Canada. He is a faculty member of Université Lyon 1, France. Bondy is known for his work on Bondy–Chvátal theorem together with Václav Chvátal. His coauthors include Paul Erdős.
