---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quarter
offline_file: ""
offline_thumbnail: ""
uuid: 7c119f31-e32c-499b-9ed6-243382eab0a9
updated: 1484310369
title: quarter
categories:
    - Dictionary
---
quarter
     n 1: one of four equal parts; "a quarter of a pound" [syn: {one-fourth},
           {fourth}, {fourth part}, {twenty-five percent}, {quartern}]
     2: a district of a city having some distinguishing character;
        "the Latin Quarter"
     3: one of four periods of play into which some games are
        divided; "both teams scored in the first quarter"
     4: a unit of time equal to 15 minutes or a quarter of an hour;
        "it's a quarter til 4"; "a quarter after 4 o'clock"
     5: one of four periods into which the school year is divided;
        "the fall quarter ends at Christmas"
     6: a fourth part of a year; three months; "unemployment fell
        during the ...
