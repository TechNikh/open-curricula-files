---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Youtube
offline_file: ""
offline_thumbnail: ""
uuid: f4384981-42b4-4370-93e7-02a4ece52ece
updated: 1484308458
title: YouTube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Youtube_founders.jpg
tags:
    - Company history
    - features
    - Video technology
    - Playback
    - Uploading
    - Quality and formats
    - 3D videos
    - 360° videos
    - User features
    - Community
categories:
    - Uncategorized
---
YouTube is an American video-sharing website headquartered in San Bruno, California. The service was created by three former PayPal employees—Chad Hurley, Steve Chen, and Jawed Karim—in February 2005. In November 2006, it was bought by Google for US$1.65 billion.[4] YouTube now operates as one of Google's subsidiaries.[5] The site allows users to upload, view, rate, share, add to favorites, report and comment on videos, and it makes use of WebM, H.264/MPEG-4 AVC, and Adobe Flash Video technology to display a wide variety of user-generated and corporate media videos. Available content includes video clips, TV show clips, music videos, short and documentary films, audio recordings, movie ...
