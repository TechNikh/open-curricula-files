---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/limiting
offline_file: ""
offline_thumbnail: ""
uuid: 2fb89dbf-f952-4dbe-b401-e3d6af202129
updated: 1484310208
title: limiting
categories:
    - Dictionary
---
limiting
     adj 1: restricting the scope or freedom of action [syn: {confining},
             {constraining}, {constrictive}, {restricting}]
     2: strictly limiting the reference of a modified word or
        phrase; "the restrictive clause in `Each made a list of
        the books that had influenced him' limits the books on the
        list to only those particular ones defined by the clause"
     n : the grammatical relation that exists when a word qualifies
         the meaning of the phrase [syn: {modification}, {qualifying}]
