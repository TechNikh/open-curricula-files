---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yielding
offline_file: ""
offline_thumbnail: ""
uuid: cca845fe-3d4a-4b3e-973b-6045c2aaada3
updated: 1484310521
title: yielding
categories:
    - Dictionary
---
yielding
     adj 1: inclined to yield to argument or influence or control; "a
            timid yielding person"
     2: lacking stiffness and giving way to pressure; "a deep
        yielding layer of foam rubber"
     3: tending to give in or surrender or agree; "too yielding to
        make a stand against any encroachments"- V.I.Parrington
     4: happy to comply [syn: {complying}, {obliging}]
     n 1: a verbal act of admitting defeat [syn: {giving up}, {surrender}]
     2: the act of conceding or yielding [syn: {concession}, {conceding}]
