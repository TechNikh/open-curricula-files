---
version: 1
type: video
provider: YouTube
id: zaxYZxQS0yc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1d383b61-c0d7-4d78-ba9b-6f66d5c16eab
updated: 1486069639
title: "Experiment No. 2- Michelson's interferometer"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/zaxYZxQS0yc/default.jpg
    - https://i3.ytimg.com/vi/zaxYZxQS0yc/1.jpg
    - https://i3.ytimg.com/vi/zaxYZxQS0yc/2.jpg
    - https://i3.ytimg.com/vi/zaxYZxQS0yc/3.jpg
---
