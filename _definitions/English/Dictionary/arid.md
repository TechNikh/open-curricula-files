---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arid
offline_file: ""
offline_thumbnail: ""
uuid: 3275cb7d-ebb9-449b-b75d-0f98f448af4b
updated: 1484310437
title: arid
categories:
    - Dictionary
---
arid
     adj 1: lacking sufficient water or rainfall; "an arid climate"; "a
            waterless well"; "miles of waterless country to cross"
            [syn: {waterless}]
     2: lacking vitality or spirit; lifeless; "a technically perfect
        but arid performance of the sonata"; "a desiccate
        romance"; "a prissy and emotionless creature...settles
        into a mold of desiccated snobbery"-C.J.Rolo [syn: {desiccate},
         {desiccated}]
