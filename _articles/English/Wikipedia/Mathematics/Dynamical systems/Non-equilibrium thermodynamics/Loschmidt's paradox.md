---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Loschmidt%27s_paradox'
offline_file: ""
offline_thumbnail: ""
uuid: eec2c0b6-07af-4079-a2d1-efd7c0dd0792
updated: 1484308805
title: "Loschmidt's paradox"
tags:
    - Origin
    - Before Loschmidt
    - Arrow of time
    - Dynamical systems
    - Fluctuation theorem
    - The Big Bang
categories:
    - Non-equilibrium thermodynamics
---
Loschmidt's paradox, also known as the reversibility paradox, irreversibility paradox or Umkehreinwand, is the objection that it should not be possible to deduce an irreversible process from time-symmetric dynamics. This puts the time reversal symmetry of (almost) all known low-level fundamental physical processes at odds with any attempt to infer from them the second law of thermodynamics which describes the behaviour of macroscopic systems. Both of these are well-accepted principles in physics, with sound observational and theoretical support, yet they seem to be in conflict; hence the paradox.
