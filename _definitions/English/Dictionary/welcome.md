---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/welcome
offline_file: ""
offline_thumbnail: ""
uuid: a4f4731e-f119-4467-95bf-cdcbcbb17402
updated: 1484310448
title: welcome
categories:
    - Dictionary
---
welcome
     adj : giving pleasure or satisfaction or received with pleasure or
           freely granted; "a welcome relief"; "a welcome guest";
           "made the children feel welcome"; "you are welcome to
           join us" [ant: {unwelcome}]
     n 1: the state of being welcome; "don't outstay your welcome"
     2: a greeting or reception; "the proposal got a warm welcome"
     v 1: accept gladly; "I welcome your proposals"
     2: bid welcome to; greet upon arrival [syn: {receive}] [ant: {say
        farewell}]
     3: receive someone, as into one's house
