---
version: 1
type: video
provider: YouTube
id: kXFEex-dABU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Aromatics%20and%20Cyclic%20Compounds%20-%20Crash%20Course%20Chemistry%20%2342.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2b44648d-9689-4557-8781-8499f2c647c0
updated: 1486069680
title: 'Aromatics and Cyclic Compounds - Crash Course Chemistry #42'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/kXFEex-dABU/default.jpg
    - https://i3.ytimg.com/vi/kXFEex-dABU/1.jpg
    - https://i3.ytimg.com/vi/kXFEex-dABU/2.jpg
    - https://i3.ytimg.com/vi/kXFEex-dABU/3.jpg
---
