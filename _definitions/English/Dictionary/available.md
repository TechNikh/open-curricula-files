---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/available
offline_file: ""
offline_thumbnail: ""
uuid: 3d3b4a5e-5fbd-41bd-a3c4-cb787e0017c6
updated: 1484310273
title: available
categories:
    - Dictionary
---
available
     adj 1: obtainable or accessible and ready for use or service; "kept
            a fire extinguisher available"; "much information is
            available through computers"; "available in many
            colors"; "the list of available candidates is
            unusually long" [ant: {unavailable}]
     2: not busy; not otherwise committed; "he was not available for
        comment"; "he was available and willing to accompany her"
        [syn: {uncommitted}]
     3: convenient for use or disposal; "the house is available
        after July 1"; "2000 square feet of usable office space"
        [syn: {usable}, {useable}]
