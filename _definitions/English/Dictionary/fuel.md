---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fuel
offline_file: ""
offline_thumbnail: ""
uuid: 245dd666-38da-46c5-a428-e982e5d02e24
updated: 1484310266
title: fuel
categories:
    - Dictionary
---
fuel
     n : a substance that can be consumed to produce energy; "more
         fuel is needed during the winter months"; "they developed
         alternative fuels for aircraft"
     v 1: provide with a combustible substance that provides emergy;
          "fuel aircraft, ships, and cars"
     2: provide with fuel; "Oil fires the furnace" [syn: {fire}]
     3: take in fuel, as of a ship; "The tanker fueled in Bahrain"
     4: stimulate; "fuel the debate on creationism"
     [also: {fuelling}, {fuelled}]
