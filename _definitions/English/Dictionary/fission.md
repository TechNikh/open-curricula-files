---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fission
offline_file: ""
offline_thumbnail: ""
uuid: 943ca64c-d497-444d-824b-782ec0d6ea47
updated: 1484310160
title: fission
categories:
    - Dictionary
---
fission
     n 1: a nuclear reaction in which a massive nucleus splits into
          smaller nuclei with the simultaneous release of energy
          [syn: {nuclear fission}]
     2: reproduction of some unicellular organisms by division of
        the cell into two more or less equal parts
