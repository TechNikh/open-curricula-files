---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/briefly
offline_file: ""
offline_thumbnail: ""
uuid: 1546df22-dba3-4874-ac11-ded45862e8dd
updated: 1484310210
title: briefly
categories:
    - Dictionary
---
briefly
     adv 1: for a short time; "she visited him briefly"; "was briefly
            associated with IBM"
     2: in a concise manner; in a few words; "the history is summed
        up concisely in this book"; "she replied briefly";
        "briefly, we have a problem"; "to put it shortly" [syn: {concisely},
         {shortly}, {in brief}, {in short}]
