---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fore
offline_file: ""
offline_thumbnail: ""
uuid: 3ec45d19-91ff-4af5-a7fe-d66617c63063
updated: 1484310359
title: fore
categories:
    - Dictionary
---
fore
     adj 1: situated at or toward the bow of a vessel [syn: {fore(a)}]
            [ant: {aft(a)}]
     2: located anteriorly [syn: {fore(a)}, {front(a)}]
     n : front part of a vessel or aircraft; "he pointed the bow of
         the boat toward the finish line" [syn: {bow}, {prow}, {stem}]
     adv : near or toward the bow of a ship or cockpit of a plane; "the
           captain went fore (or forward) to check the
           instruments" [syn: {forward}] [ant: {aft}]
