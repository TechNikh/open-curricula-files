---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/other
offline_file: ""
offline_thumbnail: ""
uuid: 6af03116-988c-4767-9288-b3ff5807d253
updated: 1484310355
title: other
categories:
    - Dictionary
---
other
     adj 1: not the same one or ones already mentioned or implied;
            "today isn't any other day"- the White Queen; "the
            construction of highways and other public works"; "he
            asked for other employment"; "any other person would
            tell the truth"; "his other books are still in
            storage"; "then we looked at the other house";
            "hearing was good in his other ear"; "the other sex";
            "she lived on the other side of the street from me";
            "went in the other direction" [ant: {same}]
     2: further or added; "called for additional troops"; "need
        extra help"; "an extra pair of shoes"; "I have no ...
