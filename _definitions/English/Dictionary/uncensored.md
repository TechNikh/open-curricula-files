---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uncensored
offline_file: ""
offline_thumbnail: ""
uuid: beead19b-0326-48c6-99e0-7e294c206221
updated: 1484310172
title: uncensored
categories:
    - Dictionary
---
uncensored
     adj : not subject to censorship; "uncensored news reports" [ant: {censored}]
