---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Raw_materials
offline_file: ""
offline_thumbnail: ""
uuid: ff17bb26-ce1d-44c6-8108-2681e2fdd797
updated: 1484308372
title: Raw material
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/200px-FAB%2527s_BlaschakCoalCompany_IMG_4456_Cleaned_stove_coal_Heap_ready_for_front_end_loader_and_Dump_trucks.JPG'
categories:
    - Uncategorized
---
A raw material, also known as a feedstock or most correctly unprocessed material, is a basic material that is used to produce goods, finished products, energy, or intermediate materials which are feedstock for future finished products. As feedstock, the term connotes these materials are bottleneck assets and are highly important with regards to producing other products. An example of this is crude oil, which is a raw material and a feedstock used in the production of industrial chemicals, fuels, plastics, and pharmaceutical goods; lumber is a raw material used to produce a variety of products including furniture.[1]
