---
title: How is Plastic Made?
youtube_id: 6eCt0VDg-Kc
tags:
    - VEA
    - Plastic
    - Raw Materials
    - Product
    - Production Process
    - Manufacturing
    - what is plastic
    - how plastic is made
    - made out of
    - make
    - oil to plastic
    - oil
    - crude oil
    - for kids
    - kids
    - school
    - Students
    - learning
    - teach
    - explained
categories:
    - Minerals
---
