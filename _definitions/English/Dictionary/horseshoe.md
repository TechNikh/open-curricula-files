---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/horseshoe
offline_file: ""
offline_thumbnail: ""
uuid: 738b4a2a-08b8-4239-905d-b3e43a46dab3
updated: 1484310365
title: horseshoe
categories:
    - Dictionary
---
horseshoe
     n 1: game equipment consisting of an open ring of iron used in
          playing horseshoes
     2: U-shaped plate nailed to underside of horse's hoof [syn: {shoe}]
     v : equip (a horse) with a horseshoe or horseshoes
