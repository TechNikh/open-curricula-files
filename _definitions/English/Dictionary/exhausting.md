---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exhausting
offline_file: ""
offline_thumbnail: ""
uuid: 1c0ad011-d081-4dc1-ad73-4f90a8f5d1c2
updated: 1484310569
title: exhausting
categories:
    - Dictionary
---
exhausting
     adj 1: having a debilitating effect; "an exhausting job in the hot
            sun" [syn: {draining}]
     2: producing exhaustion; "an exhausting march"; "the visit was
        especially wearing" [syn: {tiring}, {wearing}, {wearying}]
