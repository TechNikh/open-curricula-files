---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/displacement
offline_file: ""
offline_thumbnail: ""
uuid: aaccf35a-472f-462e-8a85-7a728f79f397
updated: 1484310375
title: displacement
categories:
    - Dictionary
---
displacement
     n 1: an event in which something is displaced without rotation
          [syn: {shift}]
     2: act of taking the place of another especially using
        underhanded tactics [syn: {supplanting}]
     3: the act of uniform movement [syn: {translation}]
     4: (chemistry) a reaction in which an elementary substance
        displaces and sets free a constituent element from a
        compound [syn: {displacement reaction}]
     5: (psychiatry) a defense mechanism that transfers affect or
        reaction from the original object to some more acceptable
        one
     6: to move something from its natural environment [syn: {deracination}]
     7: act of removing from ...
