---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_ellipsoid
offline_file: ""
offline_thumbnail: ""
uuid: 5bbc1210-5d0b-4f00-a584-a02ffae62209
updated: 1484309197
title: John ellipsoid
categories:
    - Convex geometry
---
In mathematics, the John ellipsoid or Löwner-John ellipsoid E(K) associated to a convex body K in n-dimensional Euclidean space Rn is the ellipsoid of maximal n-dimensional volume contained within K. The John ellipsoid is named after the German mathematician Fritz John. The following refinement of John's original theorem, due to Ball (1992), gives necessary and sufficient conditions for the John ellipsoid of K to be a closed unit ball B in Rn:
