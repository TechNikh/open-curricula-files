---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/giant
offline_file: ""
offline_thumbnail: ""
uuid: 45fc003e-1a8d-4008-9473-892e8edb53c8
updated: 1484310433
title: giant
categories:
    - Dictionary
---
giant
     adj : of great mass; huge and bulky; "a jumbo jet"; "jumbo shrimp"
           [syn: {elephantine}, {gargantuan}, {jumbo}]
     n 1: any creature of exceptional size
     2: a person of exceptional importance and reputation [syn: {colossus},
         {behemoth}, {heavyweight}, {titan}]
     3: an unusually large enterprise; "Walton built a retail giant"
     4: a very large person; impressive in size or qualities [syn: {hulk},
         {heavyweight}, {whale}]
     5: someone or something that is abnormally large and powerful
        [syn: {goliath}, {behemoth}, {monster}, {colossus}]
     6: an imaginary figure of superhuman size and strength; appears
        in folklore and fair ...
