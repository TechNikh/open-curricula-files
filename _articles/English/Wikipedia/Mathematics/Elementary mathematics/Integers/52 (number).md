---
version: 1
type: article
id: https://en.wikipedia.org/wiki/52_(number)
offline_file: ""
offline_thumbnail: ""
uuid: b8f2ff04-68ce-434b-aedb-a95abdafa4d4
updated: 1484308958
title: 52 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/120px-Piano_keyboard.JPG
tags:
    - In mathematics
    - In science
    - Astronomy
    - In other fields
    - Historical years
categories:
    - Integers
---
