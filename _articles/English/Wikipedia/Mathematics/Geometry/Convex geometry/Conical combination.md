---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conical_combination
offline_file: ""
offline_thumbnail: ""
uuid: 2ca537a0-381d-4497-88fb-6b856c811cbb
updated: 1484309192
title: Conical combination
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Circle-conic-hull.svg.png
tags:
    - Conical hull
    - Related combinations
categories:
    - Convex geometry
---
Given a finite number of vectors 
  
    
      
        
          x
          
            1
          
        
        ,
        
          x
          
            2
          
        
        ,
        …
        ,
        
          x
          
            n
          
        
        
      
    
    {\displaystyle x_{1},x_{2},\dots ,x_{n}\,}
  
 in a real vector space, a conical combination, conical sum, or weighted sum[1][2] of these vectors is a vector of the form
