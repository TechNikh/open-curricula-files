---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/straighten
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448601
title: straighten
categories:
    - Dictionary
---
straighten
     v 1: straighten up or out; make straight [syn: {unbend}] [ant: {bend}]
     2: make straight [syn: {straighten out}]
     3: get up from a sitting or slouching position; "The students
        straightened when the teacher entered"
     4: put (things or places) in order; "Tidy up your room!" [syn:
        {tidy}, {tidy up}, {clean up}, {neaten}, {straighten out},
         {square away}]
     5: straighten by unrolling; "roll out the big map" [syn: {roll
        out}]
     6: make straight or straighter; "Straighten this post";
        "straighten hair"
