---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slater
offline_file: ""
offline_thumbnail: ""
uuid: 94da6971-a19a-499d-a75e-f5f8b36bcbf1
updated: 1484310150
title: slater
categories:
    - Dictionary
---
slater
     n : any of various small terrestrial isopods having a flat
         elliptical segmented body; found in damp habitats [syn: {woodlouse}]
