---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soccer
offline_file: ""
offline_thumbnail: ""
uuid: f5e51f71-b8d9-45b1-a486-98b4af7fe3ff
updated: 1484310414
title: soccer
categories:
    - Dictionary
---
soccer
     n : a football game in which two teams of 11 players try to kick
         or head a ball into the opponents' goal [syn: {association
         football}]
