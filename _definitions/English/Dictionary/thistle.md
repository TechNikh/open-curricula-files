---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thistle
offline_file: ""
offline_thumbnail: ""
uuid: 21bffea9-c23c-4889-854d-0489f67c20e9
updated: 1484310379
title: thistle
categories:
    - Dictionary
---
thistle
     n : any of numerous plants of the family Compositae and
         especially of the genera Carduus and Cirsium and
         Onopordum having prickly-edged leaves
