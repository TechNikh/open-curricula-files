---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reich
offline_file: ""
offline_thumbnail: ""
uuid: 53b3fbcb-04eb-4cad-8c96-21d261ba3e23
updated: 1484310563
title: reich
categories:
    - Dictionary
---
Reich
     n 1: the German state
     2: Austrian born psychoanalyst who lived in the United States;
        advocated sexual freedom and believed that cosmic energy
        could be concentrated in a human being (1897-1957) [syn: {Wilhelm
        Reich}]
     3: United States composer (born in 1936) [syn: {Steve Reich}, {Stephen
        Michael Reich}]
