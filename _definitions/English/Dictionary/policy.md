---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/policy
offline_file: ""
offline_thumbnail: ""
uuid: eb0f7985-918d-4991-9ced-74c664ecd01b
updated: 1484310527
title: policy
categories:
    - Dictionary
---
policy
     n 1: a line of argument rationalizing the course of action of a
          government; "they debated the policy or impolicy of the
          proposed legislation"
     2: a plan of action adopted by an individual or social group;
        "it was a policy of retribution"; "a politician keeps
        changing his policies"
     3: written contract or certificate of insurance; "you should
        have read the small print on your policy" [syn: {insurance
        policy}, {insurance}]
