---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_geodesic
offline_file: ""
offline_thumbnail: ""
uuid: 318ed0ec-4a8b-4737-bd6b-c1404a9235a9
updated: 1484309262
title: Complex geodesic
categories:
    - Metric geometry
---
Let (X, || ||) be a complex Banach space and let B be the open unit ball in X. Let Δ denote the open unit disc in the complex plane C, thought of as the Poincaré disc model for 2-dimensional real/1-dimensional complex hyperbolic geometry. Let the Poincaré metric ρ on Δ be given by
