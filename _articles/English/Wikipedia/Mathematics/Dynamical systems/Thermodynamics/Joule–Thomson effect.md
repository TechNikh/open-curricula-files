---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Joule%E2%80%93Thomson_effect'
offline_file: ""
offline_thumbnail: ""
uuid: 68f673b3-3cd4-4fe7-90b6-38cf490e9eed
updated: 1484308849
title: Joule–Thomson effect
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Joule-Thomson_sign.png
tags:
    - History
    - Description
    - Physical mechanism
    - The Joule–Thomson (Kelvin) coefficient
    - Applications
    - Proof that the specific enthalpy remains constant
    - Throttling in the T-s diagram
    - Derivation of the Joule–Thomson coefficient
    - "Joule's second law"
    - Bibliography
categories:
    - Thermodynamics
---
In thermodynamics, the Joule–Thomson effect (also known as the Joule–Kelvin effect, Kelvin–Joule effect, or Joule–Thomson expansion) describes the temperature change of a real gas or liquid (as differentiated from an ideal gas) when it is forced through a valve or porous plug while kept insulated so that no heat is exchanged with the environment.[1][2][3] This procedure is called a throttling process or Joule–Thomson process.[4] At room temperature, all gases except hydrogen, helium and neon cool upon expansion by the Joule–Thomson process; these three gases experience the same effect but only at lower temperatures.[5][6]
