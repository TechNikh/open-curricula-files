---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kakeya_set
offline_file: ""
offline_thumbnail: ""
uuid: a3b4fc46-d327-4e1a-ac05-c5227d46d8a3
updated: 1484308595
title: Kakeya set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Kakeya_needle.gif
tags:
    - Kakeya needle problem
    - Besicovitch sets
    - Kakeya needle sets
    - Kakeya conjecture
    - Statement
    - Kakeya maximal function
    - Results
    - Applications to analysis
    - Analogues and generalizations of the Kakeya problem
    - Sets containing circles and spheres
    - Sets containing k-dimensional disks
    - Kakeya sets in vector spaces over finite fields
    - Notes
categories:
    - Discrete geometry
---
In mathematics, a Kakeya set, or Besicovitch set, is a set of points in Euclidean space which contains a unit line segment in every direction. For instance, a disk of radius 1/2 in the Euclidean plane, or a ball of radius 1/2 in three-dimensional space, forms a Kakeya set. Much of the research in this area has studied the problem of how small such sets can be. Besicovitch showed that there are Besicovitch sets of measure zero.
