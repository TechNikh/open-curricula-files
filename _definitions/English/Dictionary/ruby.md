---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ruby
offline_file: ""
offline_thumbnail: ""
uuid: f0c8946d-a631-4826-91eb-ca68258b6df2
updated: 1484310212
title: ruby
categories:
    - Dictionary
---
ruby
     adj : having any of numerous bright or strong colors reminiscent
           of the color of blood or cherries or tomatoes or rubies
           [syn: {red}, {reddish}, {ruddy}, {blood-red}, {carmine},
            {cerise}, {cherry}, {cherry-red}, {crimson}, {ruby-red},
            {scarlet}]
     n 1: a transparent piece of ruby that has been cut and polished
          and is valued as a precious gem
     2: a transparent deep red variety of corundum; used as a
        gemstone and in lasers
     3: a deep and vivid red [syn: {crimson}, {deep red}]
