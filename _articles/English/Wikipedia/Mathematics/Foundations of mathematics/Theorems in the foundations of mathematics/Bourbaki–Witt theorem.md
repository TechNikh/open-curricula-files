---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bourbaki%E2%80%93Witt_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: ff262ccb-264c-4da4-8ff1-d46ccab1646d
updated: 1484308992
title: Bourbaki–Witt theorem
tags:
    - Special case of a finite poset
    - Proof of the theorem
    - Applications
categories:
    - Theorems in the foundations of mathematics
---
In mathematics, the Bourbaki–Witt theorem in order theory, named after Nicolas Bourbaki and Ernst Witt, is a basic fixed point theorem for partially ordered sets. It states that if X is a non-empty chain complete poset, and
