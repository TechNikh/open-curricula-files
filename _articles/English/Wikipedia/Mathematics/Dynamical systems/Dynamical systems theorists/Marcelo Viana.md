---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Marcelo_Viana
offline_file: ""
offline_thumbnail: ""
uuid: ed7e55f6-8366-460f-ba30-98a2b8c55268
updated: 1484308690
title: Marcelo Viana
tags:
    - Biography
    - Work
    - Selected publications
categories:
    - Dynamical systems theorists
---
He was a Guggenheim Fellow in 1993.[4] In 2005 he was awarded the inaugural ICTP Ramanujan Prize for his research achievements.[3]
