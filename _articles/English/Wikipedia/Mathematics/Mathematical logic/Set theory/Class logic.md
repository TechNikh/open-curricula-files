---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Class_logic
offline_file: ""
offline_thumbnail: ""
uuid: 361c77c7-221b-4289-8dea-2d5d156d9a32
updated: 1484309501
title: Class logic
categories:
    - Set theory
---
Class logic is a logic in its broad sense, whose objects are called classes. In a narrower sense, one speaks of a class of logic only if classes are described by a property of their elements. This class logic is thus a generalization of set theory, which allows only a limited consideration of classes.
