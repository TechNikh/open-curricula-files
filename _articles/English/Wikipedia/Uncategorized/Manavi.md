---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Manavi
offline_file: ""
offline_thumbnail: ""
uuid: 3cdb2422-9056-4f0a-aa44-12578fe2e2ae
updated: 1484308335
title: Manavi
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Manavi.jpg
categories:
    - Uncategorized
---
Manavi (Georgian: მანავი) is a village in the Georgian province of Kakheti. It is famous for its yellowish green wine, „Manavis Mtsvane“. In Georgia, wine drinking is central to the culture and is especially recommended for long celebrations.
