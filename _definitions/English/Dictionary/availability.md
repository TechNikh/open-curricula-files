---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/availability
offline_file: ""
offline_thumbnail: ""
uuid: 3e022aea-34f5-4efe-bfdc-71eb725b2229
updated: 1484310275
title: availability
categories:
    - Dictionary
---
availability
     n : the quality of being at hand when needed [syn: {handiness},
         {accessibility}, {availableness}] [ant: {inaccessibility},
          {inaccessibility}]
