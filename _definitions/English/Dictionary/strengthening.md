---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strengthening
offline_file: ""
offline_thumbnail: ""
uuid: 0aeb5d44-7161-4b66-bf14-60c8390e6f65
updated: 1484310609
title: strengthening
categories:
    - Dictionary
---
strengthening
     adj : invigorating physically or mentally [syn: {fortifying}]
     n 1: becoming stronger [ant: {weakening}]
     2: the act of increasing the strength of something [ant: {weakening}]
