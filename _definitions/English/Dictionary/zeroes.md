---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/zeroes
offline_file: ""
offline_thumbnail: ""
uuid: 6a65f145-fae3-4cd1-a17b-b522c833aa3d
updated: 1484310156
title: zeroes
categories:
    - Dictionary
---
zero
     adj 1: indicating the absence of any or all units under
            consideration; "a zero score" [syn: {0}]
     2: indicating an initial point or origin
     3: of or relating to the null set (a set with no members)
     4: having no measurable or otherwise determinable value; "the
        goal is zero population growth" [syn: {zero(a)}]
     n 1: a quantity of no importance; "it looked like nothing I had
          ever seen before"; "reduced to nil all the work we had
          done"; "we racked up a pathetic goose egg"; "it was all
          for naught"; "I didn't hear zilch about it" [syn: {nothing},
           {nil}, {nix}, {nada}, {null}, {aught}, {cipher}, {cypher},
       ...
