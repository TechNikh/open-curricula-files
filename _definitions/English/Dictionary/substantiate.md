---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/substantiate
offline_file: ""
offline_thumbnail: ""
uuid: 1b1f1dad-50b3-4533-b60d-45213d4b2698
updated: 1484310547
title: substantiate
categories:
    - Dictionary
---
substantiate
     v 1: establish or strengthen as with new evidence or facts; "his
          story confirmed my doubts"; "The evidence supports the
          defendant" [syn: {confirm}, {corroborate}, {sustain}, {support},
           {affirm}] [ant: {negate}]
     2: represent in bodily form; "He embodies all that is evil
        wrong with the system"; "The painting substantiates the
        feelings of the artist" [syn: {incarnate}, {body forth}, {embody}]
     3: make real or concrete; give reality or substance to; "our
        ideas must be substantiated into actions" [syn: {realize},
         {realise}, {actualize}, {actualise}]
     4: solidify, firm, or strengthen; "The president's ...
