---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/motorbike
offline_file: ""
offline_thumbnail: ""
uuid: 2782ace1-6e62-4268-8025-1de81bcf9e46
updated: 1484310453
title: motorbike
categories:
    - Dictionary
---
motorbike
     n : small motorcycle with a low frame and small wheels and
         elevated handlebars [syn: {minibike}]
