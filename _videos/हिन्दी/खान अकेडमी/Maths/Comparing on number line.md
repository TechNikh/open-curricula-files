---
version: 1
type: video
provider: YouTube
id: w7NxMmHJSp4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0978c884-36da-47ec-bc36-a736a0fe5f6e
updated: 1486069614
title: Comparing on number line
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/w7NxMmHJSp4/default.jpg
    - https://i3.ytimg.com/vi/w7NxMmHJSp4/1.jpg
    - https://i3.ytimg.com/vi/w7NxMmHJSp4/2.jpg
    - https://i3.ytimg.com/vi/w7NxMmHJSp4/3.jpg
---
