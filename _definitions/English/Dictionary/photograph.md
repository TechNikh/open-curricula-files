---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/photograph
offline_file: ""
offline_thumbnail: ""
uuid: b2075240-ba70-4083-98c4-9a22fa421108
updated: 1484310484
title: photograph
categories:
    - Dictionary
---
photograph
     n : a picture of a person or scene in the form of a print or
         transparent slide; recorded by a camera on
         light-sensitive material [syn: {photo}, {exposure}, {pic}]
     v 1: record on photographic film; "I photographed the scene of
          the accident"; "She snapped a picture of the President"
          [syn: {snap}, {shoot}]
     2: undergo being photographed in a certain way; "Children
        photograph well"
