---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bridegroom
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484474401
title: bridegroom
categories:
    - Dictionary
---
bridegroom
     n 1: a man who has recently been married [syn: {groom}]
     2: a man participant in his own marriage ceremony [syn: {groom}]
