---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ceiling_fan
offline_file: ""
offline_thumbnail: ""
uuid: 9d5c87f2-0d24-45bc-a17a-260437915c78
updated: 1484308377
title: Ceiling fan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-The_Black_Beauty.jpg
tags:
    - History
    - Uses
    - Parts of a ceiling fan
    - Configurations
    - Operating a ceiling fan
    - Types of ceiling fans
    - Safety concerns with installation
    - Low-hanging fans/danger to limbs
    - 'MythBusters: "Killer Ceiling Fan"'
    - Wobble
categories:
    - Uncategorized
---
A ceiling fan is a mechanical fan, usually electrically powered, suspended from the ceiling of a room, that uses hub-mounted rotating paddles to circulate air.
