---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shattered
offline_file: ""
offline_thumbnail: ""
uuid: c50701cb-6b9f-48c5-9504-9072108ae3e1
updated: 1484310416
title: shattered
categories:
    - Dictionary
---
shattered
     adj 1: broken into sharp pieces; "shattered glass"; "your
            eyeglasses are smashed"; "the police came in through
            the splintered door" [syn: {smashed}, {splintered}]
     2: ruined or disrupted; "our shattered dreams of peace and
        prosperity"; "a tattered remnant of its former strength";
        "my torn and tattered past" [syn: {tattered}]
