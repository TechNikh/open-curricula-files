---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shape
offline_file: ""
offline_thumbnail: ""
uuid: 4e5e2ca4-6b4c-4148-b519-0db7addb82bf
updated: 1484310343
title: shape
categories:
    - Dictionary
---
shape
     n 1: any spatial attributes (especially as defined by outline);
          "he could barely make out their shapes through the
          smoke" [syn: {form}, {configuration}, {contour}, {conformation}]
     2: the spatial arrangement of something as distinct from its
        substance; "geometry is the mathematical science of shape"
        [syn: {form}]
     3: alternative names for the body of a human being; "Leonardo
        studied the human body"; "he has a strong physique"; "the
        spirit is willing but the flesh is weak" [syn: {human body},
         {physical body}, {material body}, {soma}, {build}, {figure},
         {physique}, {anatomy}, {bod}, {chassis}, {frame}, ...
