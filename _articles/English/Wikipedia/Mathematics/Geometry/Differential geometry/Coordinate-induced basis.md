---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coordinate-induced_basis
offline_file: ""
offline_thumbnail: ""
uuid: cc78f1d9-5360-414a-aa24-6fe1dcd0f8bd
updated: 1484309212
title: Coordinate-induced basis
categories:
    - Differential geometry
---
In mathematics, a coordinate-induced basis is a basis for the tangent space or cotangent space of a manifold that is induced by a certain coordinate system. Given the coordinate system 
  
    
      
        
          x
          
            a
          
        
      
    
    {\displaystyle x^{a}}
  
, the coordinate-induced basis 
  
    
      
        
          e
          
            a
          
        
      
    
    {\displaystyle e_{a}}
  
 of the tangent space is given by
