---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arbitrarily
offline_file: ""
offline_thumbnail: ""
uuid: 01abfeee-3bb6-41c5-9dd3-446d3c926826
updated: 1484310579
title: arbitrarily
categories:
    - Dictionary
---
arbitrarily
     adv : in a random manner; "the houses were randomly scattered";
           "bullets were fired into the crowd at random" [syn: {randomly},
            {indiscriminately}, {haphazardly}, {willy-nilly}, {at
           random}, {every which way}]
