---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Slow-growing_hierarchy
offline_file: ""
offline_thumbnail: ""
uuid: e6e07a98-97b1-45ab-b38e-75e3f5dc8307
updated: 1484309489
title: Slow-growing hierarchy
tags:
    - Definition
    - Relation to fast-growing hierarchy
    - Relation to term rewriting
    - Notes
categories:
    - Computability theory
---
In computability theory, computational complexity theory and proof theory, the slow-growing hierarchy is an ordinal-indexed family of slowly increasing functions gα: N → N (where N is the set of natural numbers, {0, 1, ...}). It contrasts with the fast-growing hierarchy.
