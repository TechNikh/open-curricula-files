---
version: 1
type: video
provider: YouTube
id: V-SQAeHbdAE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e3969283-76b2-426e-9d38-8ad9948a7440
updated: 1486069619
title: How much of a pyramid is submerged
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/V-SQAeHbdAE/default.jpg
    - https://i3.ytimg.com/vi/V-SQAeHbdAE/1.jpg
    - https://i3.ytimg.com/vi/V-SQAeHbdAE/2.jpg
    - https://i3.ytimg.com/vi/V-SQAeHbdAE/3.jpg
---
