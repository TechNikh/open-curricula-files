---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pearl
offline_file: ""
offline_thumbnail: ""
uuid: 4b3c54fd-abbb-4cc3-be52-9a529a556bd8
updated: 1484310146
title: pearl
categories:
    - Dictionary
---
pearl
     n 1: a smooth lustrous round structure inside the shell of a clam
          or oyster; much valued as a jewel
     2: a shade of white the color of bleached bones [syn: {bone}, {ivory},
         {off-white}]
     3: a shape that is small and round; "he studied the shapes of
        low-viscosity drops"; "beads of sweat on his forehead"
        [syn: {drop}, {bead}]
     v : gather pearls, from oysters in the ocean
