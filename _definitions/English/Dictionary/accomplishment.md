---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accomplishment
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484425622
title: accomplishment
categories:
    - Dictionary
---
accomplishment
     n 1: the action of accomplishing something [syn: {achievement}]
     2: an ability that has been acquired by training [syn: {skill},
         {acquirement}, {acquisition}, {attainment}]
