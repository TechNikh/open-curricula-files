---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polygon_soup
offline_file: ""
offline_thumbnail: ""
uuid: 4f8d10d6-5ddb-487b-ad1b-bcbe5d2a9a93
updated: 1484309369
title: Polygon soup
categories:
    - Geometry stubs
---
A polygon soup is a group of unorganized triangles, with generally no relationship whatsoever.[1] Polygon soups are a geometry storage format in a 3D modeling package, such as Maya, Houdini, or Blender. Polygon soup can help save memory, load/write time, and disk space for large polygon meshes compared to the equivalent polygon mesh.[2] The larger the polygon soup, the larger the savings. For instance, fluid simulations, particle simulations, rigid-body simulations, environments, and character models can reach into the millions of polygon for feature films, causing large disk space and read/write overhead. As soon as any kind of hierarchical sorting or clustering scheme is applied, the ...
