---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Triagonal
offline_file: ""
offline_thumbnail: ""
uuid: 8ffd3ad4-9c99-4242-b2e4-08b4c67260e5
updated: 1484308386
title: Space diagonal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Cube_diagonals.svg.png
tags:
    - Axial diagonal
    - Space diagonals of magic cubes
    - r-agonals
categories:
    - Uncategorized
---
In geometry a space diagonal (also interior diagonal, body diagonal or triagonal) of a polyhedron is a line connecting two vertices that are not on the same face. Space diagonals contrast with face diagonals, which connect vertices on the same face (but not on the same edge) as each other.[1]
