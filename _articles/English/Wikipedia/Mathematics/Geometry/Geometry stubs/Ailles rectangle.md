---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ailles_rectangle
offline_file: ""
offline_thumbnail: ""
uuid: c6f21503-f6d7-431e-9e1b-5a93afea1e46
updated: 1484309352
title: Ailles rectangle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/ArtificialFictionBrain_2.png
categories:
    - Geometry stubs
---
The Ailles rectangle is a rectangle constructed from four right-angled triangles. It is named after high school teacher Doug Ailles,[1] who showed how to find the values of trigonometric function of 15° and 75° with the Ailles rectangle. [2]
