---
version: 1
type: video
provider: YouTube
id: HlEkPGuWQ2A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ddac97de-d22e-4a99-854e-28e7b2f75416
updated: 1486069616
title: Graphing a parabola in vertex form
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/HlEkPGuWQ2A/default.jpg
    - https://i3.ytimg.com/vi/HlEkPGuWQ2A/1.jpg
    - https://i3.ytimg.com/vi/HlEkPGuWQ2A/2.jpg
    - https://i3.ytimg.com/vi/HlEkPGuWQ2A/3.jpg
---
