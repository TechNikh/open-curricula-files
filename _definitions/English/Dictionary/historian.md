---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/historian
offline_file: ""
offline_thumbnail: ""
uuid: a364a47f-9bf0-4092-8ce6-a50bae1ea6c2
updated: 1484310549
title: historian
categories:
    - Dictionary
---
historian
     n : a person who is an authority on history and who studies it
         and writes about it [syn: {historiographer}]
