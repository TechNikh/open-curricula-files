---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Affine_transformation
offline_file: ""
offline_thumbnail: ""
uuid: e5229746-28d0-4d00-a846-931c71fc61e4
updated: 1484309334
title: Affine transformation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Fractal_fern_explained.png
tags:
    - Mathematical definition
    - Alternative definition
    - Representation
    - Augmented matrix
    - Example augmented matrix
    - Properties
    - Affine transformation of the plane
    - Examples of affine transformations
    - Affine transformations over the real numbers
    - Affine transformation over a finite field
    - Affine transformation in plane geometry
    - Notes
categories:
    - Transformation (function)
---
In geometry, an affine transformation, affine map[1] or an affinity (from the Latin, affinis, "connected with") is a function between affine spaces which preserves points, straight lines and planes. Also, sets of parallel lines remain parallel after an affine transformation. An affine transformation does not necessarily preserve angles between lines or distances between points, though it does preserve ratios of distances between points lying on a straight line.
