---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rancid
offline_file: ""
offline_thumbnail: ""
uuid: 4ace67f9-15db-4de5-8364-137ef6a1db2f
updated: 1484310379
title: rancid
categories:
    - Dictionary
---
rancid
     adj 1: used of decomposing oils or fats; "rancid butter"; "rancid
            bacon"
     2: smelling of fermentation or staleness [syn: {sour}]
