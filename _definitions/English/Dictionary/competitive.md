---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/competitive
offline_file: ""
offline_thumbnail: ""
uuid: 9bf53403-485d-4924-9f9b-cbaee72efae2
updated: 1484310591
title: competitive
categories:
    - Dictionary
---
competitive
     adj 1: involving competition or competitiveness; "competitive
            games"; "to improve one's competitive position" [syn:
            {competitory}] [ant: {noncompetitive}]
     2: subscribing to capitalistic competition [syn: {free-enterprise(a)},
         {private-enterprise(a)}]
     3: showing a fighting disposition without self-seeking; "highly
        competitive sales representative"; "militant in fighting
        for better wages for workers"; "his self-assertive and
        ubiquitous energy" [syn: {militant}]
