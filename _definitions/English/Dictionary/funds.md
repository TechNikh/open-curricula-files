---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/funds
offline_file: ""
offline_thumbnail: ""
uuid: dd21f5f3-0578-4312-a75c-844d74e3dc89
updated: 1484310561
title: funds
categories:
    - Dictionary
---
funds
     n : assets in the form of money [syn: {finances}, {monetary
         resource}, {cash in hand}, {pecuniary resource}]
