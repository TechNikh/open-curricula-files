---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eternal
offline_file: ""
offline_thumbnail: ""
uuid: f2214562-b2ea-4925-ac9a-f0d30095d29f
updated: 1484310563
title: eternal
categories:
    - Dictionary
---
eternal
     adj 1: continuing forever or indefinitely; "the ageless themes of
            love and revenge"; "eternal truths"; "life
            everlasting"; "hell's perpetual fires"; "the unending
            bliss of heaven" [syn: {ageless}, {everlasting}, {perpetual},
             {unending}, {unceasing}]
     2: lasting for an indefinitely long period of time [syn: {everlasting},
         {lasting}, {eonian}, {aeonian}]
     3: tiresomely long; seemingly without end; "endless debates";
        "an endless conversation"; "the wait seemed eternal";
        "eternal quarreling"; "an interminable sermon" [syn: {endless},
         {interminable}]
