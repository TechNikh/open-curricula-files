---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/longtime
offline_file: ""
offline_thumbnail: ""
uuid: 5cd516c0-8193-4db7-a3fe-d026490780d6
updated: 1484310369
title: longtime
categories:
    - Dictionary
---
long time
     n : a prolonged period of time; "we've known each other for
         ages"; "I haven't been there for years and years" [syn: {age},
          {years}]
