---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mortal
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484591461
title: mortal
categories:
    - Dictionary
---
mortal
     adj 1: subject to death; "mortal beings" [ant: {immortal}]
     2: involving loss of divine grace or spiritual death; "the
        seven deadly sins" [syn: {deadly}, {mortal(a)}]
     3: unrelenting and deadly; "mortal enemy" [syn: {mortal(a)}]
     4: causing or capable of causing death; "a fatal accident"; "a
        deadly enemy"; "mortal combat"; "a mortal illness" [syn: {deadly},
         {deathly}]
     n : a human being; "there was too much for one person to do"
         [syn: {person}, {individual}, {someone}, {somebody}, {human},
          {soul}]
