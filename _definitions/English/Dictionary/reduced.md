---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reduced
offline_file: ""
offline_thumbnail: ""
uuid: 6c6be7fd-c6f0-496f-b352-f9527c7c01b1
updated: 1484310290
title: reduced
categories:
    - Dictionary
---
reduced
     adj 1: made less in size or amount or degree [syn: {decreased}]
            [ant: {increased}]
     2: well below normal (especially in price) [syn: {rock-bottom}]
