---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Relative_direction
offline_file: ""
offline_thumbnail: ""
uuid: 6a836401-1ab9-427b-8e77-24c39f370198
updated: 1484309300
title: Relative direction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-XYZ_model.jpg
tags:
    - Traditions and conventions
    - Geometry of the natural environment
    - Nautical terminology
    - Cultures without relative directions
    - Left-right confusion
categories:
    - Orientation (geometry)
---
The most common relative directions are left,[1] right,[2] forward(s),[3] backward(s),[4] up,[5] and down.[6] No absolute direction corresponds to any of the relative directions. This is a consequence of the translational invariance of the laws of physics: nature, loosely speaking, behaves the same no matter what direction one moves. As demonstrated by the Michelson-Morley null result, there is no absolute inertial frame of reference. There are definite relationships between the relative directions, however. Left and right, forward and backward, and up and down are three pairs of complementary directions, each pair orthogonal to both of the others. Relative directions are also known as ...
