---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/officialdom
offline_file: ""
offline_thumbnail: ""
uuid: e07dd418-eef0-4931-9cbb-0edc325f705d
updated: 1484310189
title: officialdom
categories:
    - Dictionary
---
officialdom
     n : people elected or appointed to administer a government [syn:
          {government officials}]
