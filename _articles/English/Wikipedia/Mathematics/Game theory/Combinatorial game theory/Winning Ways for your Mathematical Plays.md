---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Winning_Ways_for_your_Mathematical_Plays
offline_file: ""
offline_thumbnail: ""
uuid: b45989c5-7479-4a6c-b83b-4b1f377416a9
updated: 1484308533
title: Winning Ways for your Mathematical Plays
categories:
    - Combinatorial game theory
---
Winning Ways for your Mathematical Plays (Academic Press, 1982) by Elwyn R. Berlekamp, John H. Conway, and Richard K. Guy is a compendium of information on mathematical games. It was first published in 1982 in two volumes.
