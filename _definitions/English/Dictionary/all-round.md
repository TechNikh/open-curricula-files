---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/all-round
offline_file: ""
offline_thumbnail: ""
uuid: 9dba783a-7b24-485b-84ae-d551fa9859ca
updated: 1484310445
title: all-round
categories:
    - Dictionary
---
all-round
     adj : many-sided; "an all-around athlete"; "a well-rounded
           curriculum" [syn: {all-around(a)}, {all-round(a)}, {well-rounded}]
