---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Calibrated_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 7f6f2026-efa4-4adc-95e9-92f00ec40bb2
updated: 1484309207
title: Calibrated geometry
categories:
    - Differential geometry
---
In the mathematical field of differential geometry, a calibrated manifold is a Riemannian manifold (M,g) of dimension n equipped with a differential p-form φ (for some 0 ≤ p ≤ n) which is a calibration in the sense that
