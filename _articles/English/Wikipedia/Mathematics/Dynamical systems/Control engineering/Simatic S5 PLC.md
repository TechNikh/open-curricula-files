---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Simatic_S5_PLC
offline_file: ""
offline_thumbnail: ""
uuid: d439d0b1-6be3-4154-8ce1-c04ffad26d74
updated: 1484308738
title: Simatic S5 PLC
tags:
    - Hardware
    - Software
    - Structured programming
    - Methods of representation
    - Blocks
    - Operations
    - Additional functions
categories:
    - Control engineering
---
The Simatic S5 PLC is an automation system based on Programmable Logic Controllers. It was manufactured and sold by Siemens AG. Such automation systems control process equipment and machinery used in manufacturing. This product line is considered obsolete, as the manufacturer has since replaced it with their newer Simatic S7 PLC. However, the S5 PLC still has a huge installation base in factories around the world. Most automation systems integrators still have the ability to provide support for the platform.
