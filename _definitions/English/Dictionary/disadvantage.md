---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disadvantage
offline_file: ""
offline_thumbnail: ""
uuid: 95951532-d9a9-4049-a648-934d822d51b5
updated: 1484310515
title: disadvantage
categories:
    - Dictionary
---
disadvantage
     n : the quality of having an inferior or less favorable position
         [ant: {advantage}]
     v : put at a disadvantage; hinder, harm; "This rule clearly
         disadvantages me" [syn: {disfavor}, {disfavour}] [ant: {advantage}]
