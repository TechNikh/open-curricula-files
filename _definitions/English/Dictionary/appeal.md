---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appeal
offline_file: ""
offline_thumbnail: ""
uuid: b76341fe-1da1-4207-909e-8e98ba581188
updated: 1484310583
title: appeal
categories:
    - Dictionary
---
appeal
     n 1: earnest or urgent request; "an entreaty to stop the
          fighting"; "an appeal for help"; "an appeal to the
          public to keep calm" [syn: {entreaty}, {prayer}]
     2: attractiveness that interests or pleases or stimulates; "his
        smile was part of his appeal to her" [syn: {appealingness},
         {charm}]
     3: (law) a legal proceeding in which the appellant resorts to a
        higher court for the purpose of obtaining a review of a
        lower court decision and a reversal of the lower court's
        judgment or the granting of a new trial; "their appeal was
        denied in the superior court"
     4: request for a sum of money; "an appeal to ...
