---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Impedance_control
offline_file: ""
offline_thumbnail: ""
uuid: 58794860-2b2a-460c-869d-a1be9db63c41
updated: 1484308738
title: Impedance control
categories:
    - Control engineering
---
Impedance control is an approach to the control of dynamic interaction between a manipulator and its environment. This type of control is suitable for environment interaction and object manipulation.
