---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/empire
offline_file: ""
offline_thumbnail: ""
uuid: 4a84208d-2efe-4760-a29f-ecd9c2a83867
updated: 1484310555
title: empire
categories:
    - Dictionary
---
empire
     n 1: the domain ruled by an emperor or empress
     2: a group of countries under a single authority; "the British
        empire"
     3: a monarchy with an emperor as head of state
     4: a group of diverse companies under common ownership and run
        as a single organization [syn: {conglomerate}]
     5: an eating apple that somewhat resembles a McIntosh; used as
        both an eating and a cooking apple
