---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collaboration
offline_file: ""
offline_thumbnail: ""
uuid: f953aedc-5144-4fd0-94c4-b39294f4926f
updated: 1484310525
title: collaboration
categories:
    - Dictionary
---
collaboration
     n 1: act of working jointly; "they worked either in collaboration
          or independently" [syn: {coaction}]
     2: act of cooperating traitorously with an enemy that is
        occupying your country [syn: {collaborationism}, {quislingism}]
