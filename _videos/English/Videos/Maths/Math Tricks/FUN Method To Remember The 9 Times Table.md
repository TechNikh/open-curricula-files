---
version: 1
type: video
provider: YouTube
id: vab1EbN5eTc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6510a705-f578-4401-b9d5-3c16f44a4879
updated: 1486069623
title: FUN Method To Remember The 9 Times Table
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/vab1EbN5eTc/default.jpg
    - https://i3.ytimg.com/vi/vab1EbN5eTc/1.jpg
    - https://i3.ytimg.com/vi/vab1EbN5eTc/2.jpg
    - https://i3.ytimg.com/vi/vab1EbN5eTc/3.jpg
---
