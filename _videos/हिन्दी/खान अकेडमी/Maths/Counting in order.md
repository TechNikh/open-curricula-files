---
version: 1
type: video
provider: YouTube
id: G3sy9leGKSY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7c476dd2-129f-4bc7-a501-47e723ececd1
updated: 1486069613
title: Counting in order
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/G3sy9leGKSY/default.jpg
    - https://i3.ytimg.com/vi/G3sy9leGKSY/1.jpg
    - https://i3.ytimg.com/vi/G3sy9leGKSY/2.jpg
    - https://i3.ytimg.com/vi/G3sy9leGKSY/3.jpg
---
