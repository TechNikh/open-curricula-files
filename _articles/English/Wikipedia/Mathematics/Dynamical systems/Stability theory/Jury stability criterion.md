---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jury_stability_criterion
offline_file: ""
offline_thumbnail: ""
uuid: 1fbfd5a9-ea54-41a3-b491-d85ca32afa66
updated: 1484308830
title: Jury stability criterion
tags:
    - Method
    - Stability Test
    - Sample Implementation
categories:
    - Stability theory
---
In signal processing and control theory, the Jury stability criterion is a method of determining the stability of a linear discrete time system by analysis of the coefficients of its characteristic polynomial. It is the discrete time analogue of the Routh–Hurwitz stability criterion. The Jury stability criterion requires that the system poles are located inside the unit circle centered at the origin, while the Routh-Hurwitz stability criterion requires that the poles are in the left half of the complex plane. The Jury criterion is named after Eliahu Ibraham Jury.
