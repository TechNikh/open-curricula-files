---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stopped
offline_file: ""
offline_thumbnail: ""
uuid: b11ecdb5-1eed-4b3f-9466-90e31810c73b
updated: 1484310230
title: stopped
categories:
    - Dictionary
---
stop
     n 1: the event of something ending; "it came to a stop at the
          bottom of the hill" [syn: {halt}]
     2: the act of stopping something; "the third baseman made some
        remarkable stops"; "his stoppage of the flow resulted in a
        flood" [syn: {stoppage}]
     3: a brief stay in the course of a journey; "they made a
        stopover to visit their friends" [syn: {stopover}, {layover}]
     4: the state of inactivity following an interruption; "the
        negotiations were in arrest"; "held them in check";
        "during the halt he got some lunch"; "the momentary stay
        enabled him to escape the blow"; "he spent the entire stop
        in his seat" [syn: ...
