---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poetic
offline_file: ""
offline_thumbnail: ""
uuid: b7b0d40b-158d-4413-80a5-3c66be56e6a7
updated: 1484310146
title: poetic
categories:
    - Dictionary
---
poetic
     adj 1: of or relating to poetry; "poetic works"; "a poetic romance"
            [syn: {poetical}]
     2: characterized by romantic imagery; "Turner's vision of the
        rainbow...was poetic"
     3: of or relating to poets; "poetic insight"
     4: characteristic of or befitting poetry; "poetic diction"
        [syn: {poetical}]
