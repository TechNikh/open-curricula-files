---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crackers
offline_file: ""
offline_thumbnail: ""
uuid: a863d23a-69f6-4b4b-9204-15533b9a87ae
updated: 1484310371
title: crackers
categories:
    - Dictionary
---
crackers
     adj : informal or slang terms for mentally irregular; "it used to
           drive my husband balmy" [syn: {balmy}, {barmy}, {bats},
            {batty}, {bonkers}, {buggy}, {cracked}, {daft}, {dotty},
            {fruity}, {haywire}, {kooky}, {kookie}, {loco}, {loony},
            {loopy}, {nuts}, {nutty}, {round the bend}, {around
           the bend}, {wacky}, {whacky}]
