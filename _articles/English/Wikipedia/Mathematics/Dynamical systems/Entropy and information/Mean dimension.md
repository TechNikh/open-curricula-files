---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mean_dimension
offline_file: ""
offline_thumbnail: ""
uuid: b2452f07-af44-49d0-8192-e15896145b99
updated: 1484308770
title: Mean dimension
tags:
    - General definition
    - Definition in the metric case
    - Properties
    - Example
categories:
    - Entropy and information
---
In mathematics, the mean (topological) dimension of a topological dynamical system is a non-negative extended real number that is a measure of the complexity of the system. Mean dimension was first introduced in 1999 by Gromov. Shortly after it was developed and studied systematically by Lindenstrauss and Weiss. In particular they proved the following key fact: a system with finite topological entropy has zero mean dimension. For various topological dynamical systems with infinite topological entropy, the mean dimension can be calculated or at least bounded from below and above. This allows mean dimension to be used to distinguish between systems with infinite topological entropy.
