---
version: 1
type: video
provider: YouTube
id: wL0Pgh61t6s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a08688e3-38f9-4668-bca6-6f005f950dec
updated: 1486069622
title: Calculate Percentages In Your Head
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/wL0Pgh61t6s/default.jpg
    - https://i3.ytimg.com/vi/wL0Pgh61t6s/1.jpg
    - https://i3.ytimg.com/vi/wL0Pgh61t6s/2.jpg
    - https://i3.ytimg.com/vi/wL0Pgh61t6s/3.jpg
---
