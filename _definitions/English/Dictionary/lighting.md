---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lighting
offline_file: ""
offline_thumbnail: ""
uuid: d4bb7d71-0b8c-4583-a16e-8fa7e70dfefb
updated: 1484310238
title: lighting
categories:
    - Dictionary
---
lighting
     n 1: having abundant light or illumination; "they played as long
          as it was light"; "as long as the lighting was good"
          [syn: {light}] [ant: {dark}]
     2: apparatus for supplying artificial light effects for the
        stage or a film
     3: the craft of providing artificial light; "an interior
        decorator must understand lighting"
     4: the act of setting on fire or catching fire [syn: {ignition},
         {firing}, {kindling}, {inflammation}]
