---
version: 1
type: article
id: https://en.wikipedia.org/wiki/E._M._V._Krishnamurthy
offline_file: ""
offline_thumbnail: ""
uuid: b1398007-42fc-46fd-91a5-4254c9cbf973
updated: 1484309470
title: E. M. V. Krishnamurthy
categories:
    - Graph theorists
---
Edayyathu Mangalam Venkatarama Krishnamurthy (18 June 1934 - 26 October 2012) was an Indian-born computer scientist. He was a professor at the Department of Computer science, Indian Institute of Science, Bangalore. He was an Emeritus Fellow, Computer Sciences Laboratory, Research School of Information Sciences and Engineering, Australian National University, Canberra.
