---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deprived
offline_file: ""
offline_thumbnail: ""
uuid: 4d28f73c-216c-48a8-99cc-faf80eb052f8
updated: 1484310539
title: deprived
categories:
    - Dictionary
---
deprived
     adj : marked by deprivation especially of the necessities of life
           or healthful environmental influences; "a childhood
           that was unhappy and deprived, the family living off
           charity"; "boys from a deprived environment, wherein
           the family life revealed a pattern of neglect, moral
           degradation, and disregard for law" [syn: {disadvantaged}]
