---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/verbatim
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484527501
title: verbatim
categories:
    - Dictionary
---
verbatim
     adj : in precisely the same words used by a writer or speaker; "a
           direct quotation"; "repeated their dialog verbatim"
           [syn: {direct}]
     adv : using exactly the same words; "he repeated her remarks
           verbatim" [syn: {word for word}]
