---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Angles_between_flats
offline_file: ""
offline_thumbnail: ""
uuid: 3f8be057-a22f-496f-9708-62fdbe505885
updated: 1484309160
title: Angles between flats
tags:
    - "Jordan's definition[1]"
    - Angles between subspaces
    - 'Variational characterisation[3]'
    - Definition
    - Examples
    - Geometric example
    - Algebraic example
    - Basic properties
categories:
    - Analytic geometry
---
The concept of angles between lines in the plane and between pairs of two lines, two planes or a line and a plane in space can be generalised to arbitrary dimension. This generalisation was first discussed by Jordan.[1] For any pair of flats in a Euclidean space of arbitrary dimension one can define a set of mutual angles which are invariant under isometric transformation of the Euclidean space. If the flats do not intersect, their shortest distance is one more invariant.[1] These angles are called canonical[2] or principal.[3] The concept of angles can be generalised to pairs of flats in a finite-dimensional inner product space over the complex numbers.
