---
version: 1
type: article
id: 'https://te.wikipedia.org/wiki/%E0%B0%85%E0%B0%82%E0%B0%A1%E0%B1%8B%E0%B0%A4%E0%B1%8D%E0%B0%AA%E0%B0%BE%E0%B0%A6%E0%B0%95%E0%B0%BE%E0%B0%B2%E0%B1%81'
offline_file: ""
offline_thumbnail: ""
uuid: 435f0454-b649-485d-9e4f-3c79afdfbe3e
updated: 1484308506
title: అండోత్పాదకాలు
categories:
    - జంతు శాస్త్రము
---
అండోత్పాదకాలు (ఆంగ్లం Oviparous animals) సంతానోత్పత్తి కోసం గుడ్లను పెట్టే జీవులు. ఆ గుడ్లలో పిండం అభివృద్ధి ఆరంభ దశలలో కాని, పూర్తి కాకుండా కాని ఉంటుంది. ఉదాహరణ: పక్షులు, కొన్ని పాములు.
