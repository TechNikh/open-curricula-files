---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pesticide
offline_file: ""
offline_thumbnail: ""
uuid: c965d2f0-031e-4487-8216-b600d472f5f9
updated: 1484310522
title: pesticide
categories:
    - Dictionary
---
pesticide
     n : a chemical used to kill pests (as rodents or insects)
