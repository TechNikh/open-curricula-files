---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/calm
offline_file: ""
offline_thumbnail: ""
uuid: e036eed7-c669-4ddb-9fda-70e505e8cfcc
updated: 1484310561
title: calm
categories:
    - Dictionary
---
calm
     adj 1: not agitated; without losing self-possession; "spoke in a
            calm voice"; "remained calm throughout the uproar"
            [syn: {unagitated}]
     2: characterized by absence of emotional agitation; "calm
        acceptance of the inevitable"; "remained serene in the
        midst of turbulence"; "a serene expression on her face";
        "she became more tranquil"; "tranquil life in the country"
        [syn: {serene}, {tranquil}]
     3: (of weather) free from storm or wind; "calm seas" [ant: {stormy}]
     4: marked by freedom from agitation or excitement; "the rioters
        gradually became calm and slowly dispersed"
     n : steadiness of mind under ...
