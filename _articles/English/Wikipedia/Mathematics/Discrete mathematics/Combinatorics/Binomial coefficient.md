---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Binomial_coefficient
offline_file: ""
offline_thumbnail: ""
uuid: 13ab8b95-7a28-4d5e-8591-2a55f2c658f0
updated: 1484308572
title: Binomial coefficient
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Pascal%2527s_triangle_5.svg.png'
tags:
    - History and notation
    - Definition and interpretations
    - Computing the value of binomial coefficients
    - Recursive formula
    - Multiplicative formula
    - Factorial formula
    - Generalization and connection to the binomial series
    - "Pascal's triangle"
    - Combinatorics and statistics
    - Binomial coefficients as polynomials
    - >
        Binomial coefficients as a basis for the space of
        polynomials
    - Integer-valued polynomials
    - Example
    - Identities involving binomial coefficients
    - Series involving binomial coefficients
    - Partial sums
    - Identities with combinatorial proofs
    - Sum of coefficients row
    - "Dixon's identity"
    - Continuous identities
    - Generating functions
    - Ordinary generating functions
    - Exponential generating function
    - Divisibility properties
    - Bounds and asymptotic formulas
    - Generalizations
    - Generalization to multinomials
    - Taylor series
    - Binomial coefficient with n = ½
    - Identity for the product of binomial coefficients
    - Partial fraction decomposition
    - "Newton's binomial series"
    - Multiset (rising) binomial coefficient
    - Generalization to negative integers
    - Two real or complex valued arguments
    - Generalization to q-series
    - Generalization to infinite cardinals
    - Binomial coefficient in programming languages
    - Notes
categories:
    - Combinatorics
---
In mathematics, any of the positive integers that occurs as a coefficient in the binomial theorem is a binomial coefficient. Commonly, a binomial coefficient is indexed by a pair of integers n ≥ k ≥ 0 and is written 
  
    
      
        
          
            
              
                (
              
              
                n
                k
              
              
                )
              
            
          
        
      
    
    {\displaystyle {\tbinom {n}{k}}}
  
. It is the coefficient of the xk term in the polynomial expansion of the binomial power (1 + x)n. The value of the coefficient is given by the expression 
  
    
      
        
    ...
