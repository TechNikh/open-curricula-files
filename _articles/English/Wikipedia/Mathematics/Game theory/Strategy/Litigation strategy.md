---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Litigation_strategy
offline_file: ""
offline_thumbnail: ""
uuid: dd4f5f2f-6052-4e1a-86c9-b5653e9e925f
updated: 1484309065
title: Litigation strategy
tags:
    - Description
    - Tools of litigation strategy
    - Case diagrams
    - Theme and theory
    - Maneuver strategy
    - Lines of effort
    - Bibliography
categories:
    - Strategy
---
Litigation strategy is the process by which counsel for one party to a lawsuit intends to integrate their actions with anticipated events and reactions to achieve the overarching goal of the litigation. The strategic goal may be the verdict, or the damages or sentence awarded in the case, or it may be more far-reaching, such as setting legal precedent, affecting consumer-safety standards, or reshaping the public’s perception of a societal issue.[1] Broader goals and more challenging cases require a strategist with a greater understanding of, and facility with, the tools of litigation strategy.
