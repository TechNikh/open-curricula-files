---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hard-working
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484584801
title: hard-working
categories:
    - Dictionary
---
hardworking
     adj : characterized by hard work and perseverance [syn: {industrious},
            {tireless}, {untiring}]
