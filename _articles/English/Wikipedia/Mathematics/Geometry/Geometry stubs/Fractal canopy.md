---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fractal_canopy
offline_file: ""
offline_thumbnail: ""
uuid: 451114cb-e949-4568-a13a-ece6317ad4bc
updated: 1484309362
title: Fractal canopy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Fractal_canopy.svg.png
categories:
    - Geometry stubs
---
In geometry, fractal canopies are one of the easiest-to-create types of fractals. They are created by splitting a line segment into two smaller segments at the end, and then splitting the two smaller segments and as well, and so on, infinitely.[1][2]
