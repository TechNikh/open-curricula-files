---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/erratic
offline_file: ""
offline_thumbnail: ""
uuid: dff2893b-6cf0-44a6-971a-a229d605cc0c
updated: 1484310170
title: erratic
categories:
    - Dictionary
---
erratic
     adj 1: having no fixed course; "an erratic comet"; "his life
            followed a wandering course"; "a planetary vagabond"
            [syn: {planetary}, {wandering}]
     2: liable to sudden unpredictable change; "erratic behavior";
        "fickle weather"; "mercurial twists of temperament"; "a
        quicksilver character, cool and willful at one moment,
        utterly fragile the next" [syn: {fickle}, {mercurial}, {quicksilver(a)}]
     3: likely to perform unpredictably; "erratic winds are the bane
        of a sailor"; "a temperamental motor; sometimes it would
        start and sometimes it wouldn't"; "that beautiful but
        temperamental instrument the flute"- ...
