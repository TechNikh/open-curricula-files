---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deep_freezer
offline_file: ""
offline_thumbnail: ""
uuid: 326e31ce-55b7-43ac-ac3f-3f7823812b9e
updated: 1484308372
title: Refrigerator
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Food_into_a_refrigerator_-_20111002.jpg
tags:
    - History
    - Refrigeration technology
    - Domestic refrigerator
    - Freezer
    - Commercial and domestic refrigerators
    - Styles of refrigerators
    - Production by country
    - General technical explanation
    - features
    - Types of domestic refrigerators
    - Energy efficiency
    - Effect on lifestyle
    - Temperature zones and ratings
categories:
    - Uncategorized
---
A refrigerator (colloquially fridge) is a popular household appliance that consists of a thermally insulated compartment and a heat pump (mechanical, electronic or chemical) that transfers heat from the inside of the fridge to its external environment so that the inside of the fridge is cooled to a temperature below the ambient temperature of the room. Refrigeration is an essential food storage technique in developed countries. The lower temperature lowers the reproduction rate of bacteria, so the refrigerator reduces the rate of spoilage. A refrigerator maintains a temperature a few degrees above the freezing point of water. Optimum temperature range for perishable food storage is 3 to ...
