---
version: 1
type: article
id: https://en.wikipedia.org/wiki/ABC@Home
offline_file: ""
offline_thumbnail: ""
uuid: 25191476-5da9-4116-accc-11027fbdfeac
updated: 1484308545
title: ABC@Home
categories:
    - Number theory
---
Using the Berkeley Open Infrastructure for Network Computing (BOINC) distributed computing platform. As of March 2011[update], there are more than 7,300 active participants from 114 countries with a total BOINC credit of more than 2.9 billion, reporting about 10 teraflops (10 trillion operations per second) of processing power.[2]
