---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ritually
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484623321
title: ritually
categories:
    - Dictionary
---
ritually
     adv : in a ceremonial manner; "he was ceremonially sworn in as
           President" [syn: {ceremonially}]
