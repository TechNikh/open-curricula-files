---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Squegging
offline_file: ""
offline_thumbnail: ""
uuid: 831a6290-0c66-4494-b4db-24779889ee19
updated: 1484308819
title: Squegging
categories:
    - Oscillation
---
Squegging is a radio engineering term. It is the abbreviation of self-quenching. A squegging or self-blocking oscillator produces an intermittent or changing output signal. Wildlife tags for birds and little mammals use squegging oscillators.[1] The Armstrong super-regenerative radio receiver uses a self-blocking oscillator, too. The receiver sensitivity rises while the oscillation builds up. The oscillation stops when the operation point no longer fulfills the Barkhausen stability criterion. The blocking oscillator recovers to the initial state and the cycle starts again.[2] The receive frequency of the Armstrong Super-Regenerative receiver was some hundred kilohertz. The self-quenching ...
