---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illustration
offline_file: ""
offline_thumbnail: ""
uuid: 77711624-f3f9-4639-96b8-020d71d3dd3f
updated: 1484310416
title: illustration
categories:
    - Dictionary
---
illustration
     n 1: artwork that helps make something clear or attractive
     2: showing by example [syn: {exemplification}]
     3: an item of information that is representative of a type;
        "this patient provides a typical example of the syndrome";
        "there is an example on page 10" [syn: {example}, {instance},
         {representative}]
     4: a visual representation (a picture or diagram) that is used
        make some subject more pleasing or easier to understand
