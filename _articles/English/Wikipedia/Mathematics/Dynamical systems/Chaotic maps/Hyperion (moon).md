---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperion_(moon)
offline_file: ""
offline_thumbnail: ""
uuid: 04db12f5-b7fc-49cd-b866-68443e396a01
updated: 1484308724
title: Hyperion (moon)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/270px-Hyperion_true.jpg
tags:
    - Name
    - Physical characteristics
    - Shape
    - Composition
    - Surface features
    - Rotation
    - Exploration
    - Notes
categories:
    - Chaotic maps
---
Hyperion (/haɪˈpɪəriən/;[b] Greek: Ὑπερίων), also known as Saturn VII (7), is a moon of Saturn discovered by William Cranch Bond, George Phillips Bond and William Lassell in 1848. It is distinguished by its irregular shape, its chaotic rotation, and its unexplained sponge-like appearance. It was the first non-round moon to be discovered.
