---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reaction
offline_file: ""
offline_thumbnail: ""
uuid: 666a42a1-5eb2-4183-859d-333ecfd703ed
updated: 1484310311
title: reaction
categories:
    - Dictionary
---
reaction
     n 1: a response that reveals a person's feelings or attitude; "he
          was pleased by the audience's reaction to his
          performance"; "John feared his mother's reaction when
          she saw the broken lamp"
     2: a bodily process occurring due to the effect of some
        foregoing stimulus or agent; "a bad reaction to the
        medicine"; "his responses have slowed with age" [syn: {response}]
     3: (chemistry) a process in which one or more substances are
        changed into others; "there was a chemical reaction of the
        lime with the ground water" [syn: {chemical reaction}]
     4: an idea evoked by some experience; "his reaction to the news
     ...
