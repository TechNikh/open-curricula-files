---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Official_statistics
offline_file: ""
offline_thumbnail: ""
uuid: d0d18468-d82f-47ca-b141-c54df29bddb9
updated: 1484308365
title: Official statistics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-UNECE_Germany_2010.gif
tags:
    - Aim
    - Various categories
    - Most common indicators used in official statistics
    - Users
    - Users with a general interest
    - Users with a business interest
    - Users with a research interest
    - Producers at the national level
    - Production Process
    - Data revision
    - Data sources
    - Statistical survey or sample survey
    - Census
    - Register
    - Official Statistics presentation
    - Release
    - Quality criteria to be respected
    - Relevance
    - Impartiality
    - Dissemination
    - Independence
    - Transparency
    - Confidentiality
    - International standards
    - External sources
categories:
    - Uncategorized
---
Official statistics are statistics published by government agencies or other public bodies such as international organizations as a public good. They provide quantitative or qualitative information on all major areas of citizens' lives, such as economic and social development,[1] living conditions,[2] health,[3] education,[4] and the environment.[5]
