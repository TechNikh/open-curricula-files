---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/requisition
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484528521
title: requisition
categories:
    - Dictionary
---
requisition
     n 1: the act of requiring; an authoritative request or demand,
          especially by a military or public authority that takes
          something over (usually temporarily) for military or
          public use
     2: an official form on which a request in made; "first you have
        to fill out the requisition" [syn: {requisition form}]
     3: seizing property that belongs to someone else and holding it
        until profits pay the demand for which it was seized [syn:
         {sequestration}]
     v 1: make a formal request for official services
     2: demand and take for use or service, especially by military
        or public authority for public service [ant: ...
