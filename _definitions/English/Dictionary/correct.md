---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/correct
offline_file: ""
offline_thumbnail: ""
uuid: 7334445b-88c2-4b40-92ff-3d64ca912220
updated: 1484310313
title: correct
categories:
    - Dictionary
---
correct
     adj 1: free from error; especially conforming to fact or truth;
            "the correct answer"; "the correct version"; "the
            right answer"; "took the right road"; "the right
            decision" [syn: {right}] [ant: {incorrect}, {incorrect}]
     2: socially right or correct; "it isn't right to leave the
        party without saying goodbye"; "correct behavior" [syn: {right}]
     3: in accord with accepted standards of usage or procedure;
        "what's the right word for this?"; "the right way to open
        oysters" [syn: {right}]
     4: correct in opinion or judgment; "time proved him right"
        [syn: {right}] [ant: {wrong}]
     v 1: make right or ...
