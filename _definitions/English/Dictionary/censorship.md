---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/censorship
offline_file: ""
offline_thumbnail: ""
uuid: 361d6d11-6286-4ae9-bab9-ccc9aa054829
updated: 1484310192
title: censorship
categories:
    - Dictionary
---
censorship
     n 1: counterintelligence achieved by banning or deleting any
          information of value to the enemy [syn: {censoring}, {security
          review}]
     2: deleting parts of publications or correspondence or
        theatrical performances [syn: {censoring}]
