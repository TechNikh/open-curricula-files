---
version: 1
type: video
provider: YouTube
id: dUMmoPdwBy4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/What%20is%20chemical%20equilibrium-%20-%20George%20Zaidan%20and%20Charles%20Morton.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f593e9e2-4063-4f0f-84b5-92b8bb515be8
updated: 1486069662
title: 'What is chemical equilibrium? - George Zaidan and Charles Morton'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/dUMmoPdwBy4/default.jpg
    - https://i3.ytimg.com/vi/dUMmoPdwBy4/1.jpg
    - https://i3.ytimg.com/vi/dUMmoPdwBy4/2.jpg
    - https://i3.ytimg.com/vi/dUMmoPdwBy4/3.jpg
---
