---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cultured
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415241
title: cultured
categories:
    - Dictionary
---
cultured
     adj : marked by refinement in taste and manners; "cultivated
           speech"; "cultured Bostonians"; "cultured tastes"; "a
           genteel old lady"; "polite society" [syn: {civilized},
           {civilised}, {cultivated}, {genteel}, {polite}]
