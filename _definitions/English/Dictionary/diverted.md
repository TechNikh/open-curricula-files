---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diverted
offline_file: ""
offline_thumbnail: ""
uuid: 9a0f1214-3e24-4da7-8411-021755538e66
updated: 1484310533
title: diverted
categories:
    - Dictionary
---
diverted
     adj : pleasantly occupied; "We are not amused" -Queen Victoria
           [syn: {amused}, {entertained}]
