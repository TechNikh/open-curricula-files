---
version: 1
type: video
provider: YouTube
id: m5cYLEyEm_w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bdd5d100-aea3-4a22-a08f-b495f720df69
updated: 1486069613
title: Metric units for weight
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/m5cYLEyEm_w/default.jpg
    - https://i3.ytimg.com/vi/m5cYLEyEm_w/1.jpg
    - https://i3.ytimg.com/vi/m5cYLEyEm_w/2.jpg
    - https://i3.ytimg.com/vi/m5cYLEyEm_w/3.jpg
---
