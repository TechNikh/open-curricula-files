---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/level
offline_file: ""
offline_thumbnail: ""
uuid: 48e9da6b-df0c-4f26-b23a-a865fa8e669f
updated: 1484310339
title: level
categories:
    - Dictionary
---
level
     adj 1: having a horizontal surface in which no part is higher or
            lower than another; "a flat desk"; "acres of level
            farmland"; "a plane surface" [syn: {flat}, {plane}]
     2: not showing abrupt variations; "spoke in a level voice";
        "she gave him a level look"- Louis Auchincloss [syn: {unwavering}]
     3: being on a precise horizontal plane; "a billiard table must
        be level"
     4: oriented at right angles to the plumb; "the picture is
        level"
     5: of the score in a contest; "the score is tied" [syn: {tied(p)},
         {even}, {level(p)}]
     n 1: a position on a scale of intensity or amount or quality; "a
          moderate ...
