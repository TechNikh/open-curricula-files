---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carry_(arithmetic)
offline_file: ""
offline_thumbnail: ""
uuid: f33ec17e-c7ad-4bf5-8aac-d5dc2cf27bb8
updated: 1484308921
title: Carry (arithmetic)
tags:
    - Manual arithmetic
    - Mathematics education
    - Higher mathematics
    - Computing
categories:
    - Elementary arithmetic
---
In elementary arithmetic, a carry is a digit that is transferred from one column of digits to another column of more significant digits. It is part of the standard algorithm to add numbers together by starting with the rightmost digits and working to the left. For example, when 6 and 7 are added to make 13, the "3" is written to the same column and the "1" is carried to the left. When used in subtraction the operation is called a borrow.
