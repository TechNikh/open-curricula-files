---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/participatory
offline_file: ""
offline_thumbnail: ""
uuid: 3491341f-ac8d-45ae-a420-c28535c16fa1
updated: 1484310166
title: participatory
categories:
    - Dictionary
---
participatory
     adj : affording the opportunity for individual participation;
           "participatory democracy"
