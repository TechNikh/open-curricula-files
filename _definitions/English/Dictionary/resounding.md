---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resounding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484580061
title: resounding
categories:
    - Dictionary
---
resounding
     adj : characterized by reverberation; "a resonant voice"; "hear
           the rolling thunder" [syn: {resonant}, {resonating}, {reverberating},
            {reverberative}, {rolling}]
