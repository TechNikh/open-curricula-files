---
version: 1
type: article
id: https://en.wikipedia.org/wiki/HUD_auction
offline_file: ""
offline_thumbnail: ""
uuid: e245f142-675e-4a76-939b-7454ac766b32
updated: 1484309017
title: HUD auction
categories:
    - Auctions
---
A HUD auction is a form of foreclosure auction except the original lender was a federal agency instead of a private lender. The United States Department of Housing and Urban Development (HUD), is the insurer of loans made through a variety of government programs, particularly FHA loans. When a lender forecloses on a government insured loan, HUD takes possession of the property.
