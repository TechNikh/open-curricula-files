---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Probabilistic_metric_space
offline_file: ""
offline_thumbnail: ""
uuid: ebedd5fc-2a27-4a05-9e40-fca1b6533aa7
updated: 1484309269
title: Probabilistic metric space
categories:
    - Metric geometry
---
A probabilistic metric space is a generalization of metric spaces where the distance is no longer valued in non-negative real numbers, but instead is valued in distribution functions.
