---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sorted
offline_file: ""
offline_thumbnail: ""
uuid: 91018163-8333-4936-ad79-477b4e0d104c
updated: 1484310238
title: sorted
categories:
    - Dictionary
---
sorted
     adj 1: arranged according to size
     2: arranged into groups [syn: {grouped}]
