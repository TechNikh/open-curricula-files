---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hoh
offline_file: ""
offline_thumbnail: ""
uuid: cc615e06-9971-41b7-b1c7-cb484112eb7a
updated: 1484308386
title: Hoh Indian Tribe of the Hoh Indian Reservation
categories:
    - Uncategorized
---
The Hoh or Chalá·at (′Those-Who-Live-on-the-Hoh River′ or ′People of the Hoh River′) are a Native American tribe in western Washington state in the United States. The tribe lives on the Pacific Coast of Washington on the Olympic Peninsula. The Hoh moved onto the Hoh Indian Reservation, 47°44′31″N 124°25′17″W﻿ / ﻿47.74194°N 124.42139°W﻿ / 47.74194; -124.42139 at the mouth of the Hoh River, on the Pacific Coast of Jefferson County, after the signing of the Quinault Treaty on July 1, 1855. The reservation has a land area of 1.929 square kilometres (477 acres) and a 2000 census resident population of 102 persons, 81 of whom were Native Americans. It lies about ...
