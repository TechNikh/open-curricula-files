---
version: 1
type: video
provider: YouTube
id: R-8QmxRLoMQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b7ebf2be-0899-46f1-b090-96665e8e94c9
updated: 1486069617
title: Subtracting 14-6
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/R-8QmxRLoMQ/default.jpg
    - https://i3.ytimg.com/vi/R-8QmxRLoMQ/1.jpg
    - https://i3.ytimg.com/vi/R-8QmxRLoMQ/2.jpg
    - https://i3.ytimg.com/vi/R-8QmxRLoMQ/3.jpg
---
