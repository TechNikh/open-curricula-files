---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Renal_artery
offline_file: ""
offline_thumbnail: ""
uuid: ceb69c2d-8561-439f-8803-63e809a9b940
updated: 1484308371
title: Renal artery
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray1121.png
tags:
    - Structure
    - Branches
    - Variation
    - Clinical significance
    - Additional images
categories:
    - Uncategorized
---
The renal arteries normally arise off the side of the abdominal aorta, immediately below the superior mesenteric artery, and supply the kidneys with blood. Each is directed across the crus of the diaphragm, so as to form nearly a right angle with the aorta.
