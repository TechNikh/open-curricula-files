---
version: 1
type: video
provider: YouTube
id: 7UISwx2Mr4c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Geometry/Congruence/Congruent%20legs%20and%20base%20angles%20of%20isosceles%20triangles.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 79eb591c-6d23-40e0-bd00-b5082d11fcad
updated: 1486069579
title: Congruent legs and base angles of isosceles triangles
categories:
    - Congruence
thumbnail_urls:
    - https://i3.ytimg.com/vi/7UISwx2Mr4c/default.jpg
    - https://i3.ytimg.com/vi/7UISwx2Mr4c/1.jpg
    - https://i3.ytimg.com/vi/7UISwx2Mr4c/2.jpg
    - https://i3.ytimg.com/vi/7UISwx2Mr4c/3.jpg
---
Showing that congruent legs imply equal base angles and vice versa

Practice this lesson yourself on KhanAcademy.org right now: 
https://www.khanacademy.org/math/geometry/congruent-triangles/isoscleles_equil/e/recognizing-triangles?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Watch the next lesson: https://www.khanacademy.org/math/geometry/congruent-triangles/isoscleles_equil/v/equilateral-triangle-sides-and-angles-congruent?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Missed the previous lesson? 
https://www.khanacademy.org/math/geometry/congruent-triangles/cong_triangle/v/problem-involving-angle-derived-from-square-and-circle?utm_source=YT&utm_medium=Desc&utm_campaign=Geometry

Geometry on Khan Academy: We are surrounded by space. And that space contains lots of things. And these things have shapes. In geometry we are concerned with the nature of these shapes, how we define them, and what they teach us about the world at large--from math to architecture to biology to astronomy (and everything in between). Learning geometry is about more than just taking your medicine ("It's good for you!"), it's at the core of everything that exists--including you. Having said all that, some of the specific topics we'll cover include angles, intersecting lines, right triangles, perimeter, area, volume, circles, triangles, quadrilaterals, analytic geometry, and geometric constructions. Wow. That's a lot. To summarize: it's difficult to imagine any area of math that is more widely used than geometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Geometry channel:
https://www.youtube.com/channel/UCD3OtKxPRUFw8kzYlhJXa1Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
