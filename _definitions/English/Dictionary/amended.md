---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amended
offline_file: ""
offline_thumbnail: ""
uuid: dcfe43c4-8dfe-4303-88ed-19afc807afb2
updated: 1484310599
title: amended
categories:
    - Dictionary
---
amended
     adj 1: of legislation [ant: {unamended}]
     2: modified for the better; "his amended ways"
