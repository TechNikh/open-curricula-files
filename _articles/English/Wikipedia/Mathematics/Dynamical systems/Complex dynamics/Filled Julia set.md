---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Filled_Julia_set
offline_file: ""
offline_thumbnail: ""
uuid: 1a0c2afd-ba06-4c5f-9642-8ea86a4e86fd
updated: 1484308728
title: Filled Julia set
tags:
    - Formal definition
    - Relation to the Fatou set
    - >
        Relation between Julia, filled-in Julia set and attractive
        basin of infinity
    - Spine
    - Images
    - Notes
categories:
    - Complex dynamics
---
The filled-in Julia set 
  
    
      
         
        K
        (
        f
        )
      
    
    {\displaystyle \ K(f)}
  
 of a polynomial 
  
    
      
         
        f
      
    
    {\displaystyle \ f}
  
 is :
