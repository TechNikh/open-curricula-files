---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reshuffle
offline_file: ""
offline_thumbnail: ""
uuid: 6cb48b11-b2ae-4cfc-9214-366996fde2ec
updated: 1484310411
title: reshuffle
categories:
    - Dictionary
---
reshuffle
     n 1: a redistribution of something; "there was a reshuffle of
          cabinet officers"
     2: shuffling again; "the gambler demanded a reshuffle" [syn: {reshuffling}]
     v 1: shuffle again; "So as to prevent cheating, he was asked to
          reshuffle the cards"
     2: reorganize and assign posts to different people; "The new
        Prime Minister reshuffled his cabinet"
