---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minimal_coupling
offline_file: ""
offline_thumbnail: ""
uuid: 81d55b5b-567c-4fa9-979e-12fc201e77ca
updated: 1484308802
title: Minimal coupling
categories:
    - Hamiltonian mechanics
---
In physics, minimal coupling refers to a coupling between fields which involves only the charge distribution and not higher multipole moments of the charge distribution. This minimal coupling is in contrast to, for example, Pauli coupling, which includes the magnetic moment of an electron directly in the Lagrangian.
