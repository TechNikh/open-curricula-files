---
version: 1
type: article
id: https://en.wikipedia.org/wiki/String_graph
offline_file: ""
offline_thumbnail: ""
uuid: 0e9db7b1-7b45-49b8-afa6-9a3c7e54a844
updated: 1484309479
title: String graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Planar_string_graph.svg.png
tags:
    - Background
    - Related graph classes
    - Other results
    - Notes
categories:
    - Topological graph theory
---
In graph theory, a string graph is an intersection graph of curves in the plane; each curve is called a "string". Given a graph G, G is a string graph if and only if there exists a set of curves, or strings, drawn in the plane such that no three strings intersect at a single point and such that the graph having a vertex for each curve and an edge for each intersecting pair of curves is isomorphic to G.
