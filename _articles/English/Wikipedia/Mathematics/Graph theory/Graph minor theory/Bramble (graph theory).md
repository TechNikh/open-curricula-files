---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bramble_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: ce0a953c-dd4f-484a-9289-7b98d401a384
updated: 1484309413
title: Bramble (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-3x3_grid_graph_haven.svg_0.png
tags:
    - Treewidth and havens
    - Size of brambles
    - Computational complexity
categories:
    - Graph minor theory
---
In graph theory, a bramble for an undirected graph G is a family of connected subgraphs of G that all touch each other: for every pair of disjoint subgraphs, there must exist an edge in G that has one endpoint in each subgraph. The order of a bramble is the smallest size of a hitting set, a set of vertices of G that has a nonempty intersection with each of the subgraphs. Brambles may be used to characterize the treewidth of G.[1]
