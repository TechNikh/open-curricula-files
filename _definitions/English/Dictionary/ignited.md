---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ignited
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484625601
title: ignited
categories:
    - Dictionary
---
ignited
     adj : set afire; "the ignited paper"; "a kindled fire" [syn: {enkindled},
            {kindled}]
