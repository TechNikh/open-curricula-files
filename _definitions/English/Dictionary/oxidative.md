---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oxidative
offline_file: ""
offline_thumbnail: ""
uuid: 0f7ce8d0-bf17-4ed5-8af4-a08653b68e10
updated: 1484310377
title: oxidative
categories:
    - Dictionary
---
oxidative
     adj : taking place in the presence of oxygen; "oxidative
           glycolysis"; "oxidative rancidity"
