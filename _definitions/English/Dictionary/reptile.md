---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reptile
offline_file: ""
offline_thumbnail: ""
uuid: 6be68ade-7ad5-41f0-8f71-122a94938e33
updated: 1484310277
title: reptile
categories:
    - Dictionary
---
reptile
     n : any cold-blooded vertebrate of the class Reptilia including
         tortoises turtles snakes lizards alligators crocodiles
         and extinct forms [syn: {reptilian}]
