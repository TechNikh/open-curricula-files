---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/operate
offline_file: ""
offline_thumbnail: ""
uuid: cfaa4f4f-440d-4ab0-a5f1-ec83ea6be02a
updated: 1484310238
title: operate
categories:
    - Dictionary
---
operate
     v 1: direct or control; projects, businesses, etc.; "She is
          running a relief operation in the Sudan" [syn: {run}]
     2: perform as expected when applied; "The washing machine won't
        go unless it's plugged in"; "Does this old car still run
        well?"; "This old radio doesn't work anymore" [syn: {function},
         {work}, {go}, {run}] [ant: {malfunction}]
     3: handle and cause to function; "do not operate machinery
        after imbibing alcohol"; "control the lever" [syn: {control}]
     4: perform a movement in military or naval tactics in order to
        secure an advantage in attack or defense [syn: {manoeuver},
         {maneuver}, {manoeuvre}]
  ...
