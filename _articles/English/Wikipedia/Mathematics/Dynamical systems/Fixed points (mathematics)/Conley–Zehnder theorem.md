---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Conley%E2%80%93Zehnder_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 54cebea7-7818-4d26-80fd-cdff05c1cb5a
updated: 1484308780
title: Conley–Zehnder theorem
categories:
    - Fixed points (mathematics)
---
In mathematics, the Conley–Zehnder theorem, named after Charles C. Conley and Eduard Zehnder, provides a lower bound for the number of fixed points of Hamiltonian diffeomorphisms of standard symplectic tori in terms of the topology of the underlying tori. The lower bound is one plus the cup-length of the torus (thus 2n+1, where 2n is the dimension of the considered torus), and it can be strengthen to the rank of the homology of the torus (which is 22n) provided all the fixed points are non-degenerate, this latter condition being generic in the C1-topology.
