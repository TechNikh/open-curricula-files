---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/colloidal
offline_file: ""
offline_thumbnail: ""
uuid: a338384f-e382-4fdc-8293-7663dcb4e03f
updated: 1484310426
title: colloidal
categories:
    - Dictionary
---
colloidal
     adj : of or relating to or having the properties of a colloid
