---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stars_and_bars_(combinatorics)
offline_file: ""
offline_thumbnail: ""
uuid: d2723b6b-a34c-45ba-a81d-b80088d5b1aa
updated: 1484308598
title: Stars and bars (combinatorics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/He1523a_0.jpg
tags:
    - Statements of theorems
    - Theorem one
    - Theorem two
    - Proofs via the method of stars and bars
    - Theorem one
    - Theorem two
    - Examples
categories:
    - Combinatorics
---
In the context of combinatorial mathematics, stars and bars is a graphical aid for deriving certain combinatorial theorems. It was popularized by William Feller in his classic book on probability. It can be used to solve many simple counting problems, such as how many ways there are to put n indistinguishable balls into k distinguishable bins.[1]
