---
version: 1
type: video
provider: YouTube
id: jvvkomcmyuo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Schrodinger%27s%20Equation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0e2ada1a-7979-4e78-a043-203c05bb5912
updated: 1486069656
title: "Schrodinger's Equation"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/jvvkomcmyuo/default.jpg
    - https://i3.ytimg.com/vi/jvvkomcmyuo/1.jpg
    - https://i3.ytimg.com/vi/jvvkomcmyuo/2.jpg
    - https://i3.ytimg.com/vi/jvvkomcmyuo/3.jpg
---
