---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hosoya_index
offline_file: ""
offline_thumbnail: ""
uuid: fdc093a8-177c-4953-9879-fbe24754e4aa
updated: 1484309422
title: Hosoya index
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-K4_matchings.svg.png
tags:
    - History
    - Example
    - Algorithms
    - Notes
categories:
    - Graph invariants
---
The Hosoya index, also known as the Z index, of a graph is the total number of matchings in it. The Hosoya index is always at least one, because the empty set of edges is counted as a matching for this purpose. Equivalently, the Hosoya index is the number of non-empty matchings plus one.
