---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exothermic
offline_file: ""
offline_thumbnail: ""
uuid: fa326a89-fe44-45c8-97db-17b84badb533
updated: 1484310371
title: exothermic
categories:
    - Dictionary
---
exothermic
     adj : (of a chemical reaction or compound) occurring or formed
           with evolution of heat [syn: {exothermal}, {heat-releasing}]
           [ant: {endothermic}]
