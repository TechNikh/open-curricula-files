---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heir
offline_file: ""
offline_thumbnail: ""
uuid: ead009a5-521c-4bc4-badf-b51fe83ec028
updated: 1484310401
title: heir
categories:
    - Dictionary
---
heir
     n 1: a person who is entitled by law or by the terms of a will to
          inherit the estate of another [syn: {inheritor}, {heritor}]
     2: a person who inherits some title or office [syn: {successor}]
