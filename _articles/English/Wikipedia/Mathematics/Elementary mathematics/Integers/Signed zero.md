---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Signed_zero
offline_file: ""
offline_thumbnail: ""
uuid: 8cdf1036-c8fc-44e3-8d55-1549c9f04a65
updated: 1484308938
title: Signed zero
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-IEEE_754_Single_Negative_Zero.svg.png
tags:
    - Representations
    - Properties and handling
    - Notation
    - Arithmetic
    - Comparisons
    - Scientific uses
categories:
    - Integers
---
Signed zero is zero with an associated sign. In ordinary arithmetic, the number 0 does not have a sign, so that −0, +0 and 0 are identical. However, in computing, some number representations allow for the existence of two zeros, often denoted by −0 (negative zero) and +0 (positive zero), regarded as equal by the numerical comparison operations but with possible different behaviors in particular operations. This occurs in the sign and magnitude and ones' complement signed number representations for integers, and in most floating-point number representations. The number 0 is usually encoded as +0, but can be represented by either +0 or −0.
