---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weaver
offline_file: ""
offline_thumbnail: ""
uuid: 376b8dce-52f6-4de6-9c80-def74093270c
updated: 1484310448
title: weaver
categories:
    - Dictionary
---
weaver
     n 1: a craftsman who weaves cloth
     2: finch-like African and Asian colonial birds noted for their
        elaborately woven nests [syn: {weaverbird}, {weaver finch}]
