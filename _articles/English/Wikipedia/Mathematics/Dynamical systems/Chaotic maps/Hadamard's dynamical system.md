---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hadamard%27s_dynamical_system'
offline_file: ""
offline_thumbnail: ""
uuid: a0a31344-86a1-4cde-a01b-98fa1a28e1a9
updated: 1484308713
title: "Hadamard's dynamical system"
categories:
    - Chaotic maps
---
In physics and mathematics, the Hadamard dynamical system or Hadamard's billiard is a chaotic dynamical system, a type of dynamical billiards. Introduced by Jacques Hadamard in 1898,[1] it is the first dynamical system to be proven chaotic.
