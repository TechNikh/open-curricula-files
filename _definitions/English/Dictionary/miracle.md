---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/miracle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484407381
title: miracle
categories:
    - Dictionary
---
miracle
     n 1: any amazing or wonderful occurrence
     2: a marvellous event manifesting a supernatural act of God
