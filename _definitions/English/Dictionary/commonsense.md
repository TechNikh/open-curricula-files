---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commonsense
offline_file: ""
offline_thumbnail: ""
uuid: 4e4f7520-9b6d-472b-92ac-05cdd4a8db31
updated: 1484310537
title: commonsense
categories:
    - Dictionary
---
common sense
     n : sound practical judgment; "I can't see the sense in doing it
         now"; "he hasn't got the sense God gave little green
         apples"; "fortunately she had the good sense to run away"
         [syn: {good sense}, {gumption}, {horse sense}, {sense}, {mother
         wit}]
