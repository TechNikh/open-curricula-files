---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/render
offline_file: ""
offline_thumbnail: ""
uuid: b63129e4-cf3b-4a8a-baf9-e8b2bd6ea5f5
updated: 1484310539
title: render
categories:
    - Dictionary
---
render
     n : a substance similar to stucco but exclusively applied to
         masonry walls
     v 1: cause to become; "The shot rendered her immobile"
     2: provide or furnish with; "We provided the room with an
        electrical heater" [syn: {supply}, {provide}, {furnish}]
     3: give an interpretation or rendition of; "The pianist
        rendered the Beethoven sonata beautifully" [syn: {interpret}]
     4: give or supply; "The cow brings in 5 liters of milk"; "This
        year's crop yielded 1,000 bushels of corn"; "The estate
        renders some revenue for the family" [syn: {yield}, {return},
         {give}, {generate}]
     5: pass down; "render a verdict"; "deliver a ...
