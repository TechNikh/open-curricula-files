---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Noncommutative_residue
offline_file: ""
offline_thumbnail: ""
uuid: b22e159c-655c-47a8-91b9-6f5f6d766acf
updated: 1484309290
title: Noncommutative residue
categories:
    - Noncommutative geometry
---
In mathematics, noncommutative residue, defined independently by M. Wodzicki (1984) and Guillemin (1985), is a certain trace on the algebra of pseudodifferential operators on a compact differentiable manifold that is expressed via a local density. In the case of the circle, the noncommutative residue had been studied earlier by M. Adler (1979) and Y. Manin (1978) in the context of one-dimensional integrable systems.
