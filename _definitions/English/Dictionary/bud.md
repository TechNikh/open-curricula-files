---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bud
offline_file: ""
offline_thumbnail: ""
uuid: d689dfbb-44a7-4365-a653-775a7d848e86
updated: 1484310341
title: bud
categories:
    - Dictionary
---
bud
     n 1: a partially opened flower
     2: a swelling on a plant stem consisting of overlapping
        immature leaves or petals
     v 1: develop buds; "The hibiscus is budding!"
     2: start to grow or develop; "a budding friendship"
     [also: {budding}, {budded}]
