---
version: 1
type: video
provider: YouTube
id: Rmo3O8yh3Xg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 98a287a5-3f3d-4a8c-a139-82be05c22130
updated: 1486069619
title: 'Proof- √2 is irrational'
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/Rmo3O8yh3Xg/default.jpg
    - https://i3.ytimg.com/vi/Rmo3O8yh3Xg/1.jpg
    - https://i3.ytimg.com/vi/Rmo3O8yh3Xg/2.jpg
    - https://i3.ytimg.com/vi/Rmo3O8yh3Xg/3.jpg
---
