---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indebted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484523541
title: indebted
categories:
    - Dictionary
---
indebted
     adj 1: under a legal obligation to someone [syn: {indebted(p)}]
     2: owing gratitude or recognition to another for help or favors
        etc [syn: {indebted(p)}]
