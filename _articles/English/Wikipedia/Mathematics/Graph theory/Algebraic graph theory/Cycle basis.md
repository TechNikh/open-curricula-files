---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cycle_basis
offline_file: ""
offline_thumbnail: ""
uuid: 6ef5cdb7-ee84-497b-a0e3-35af116117c8
updated: 1484309373
title: Cycle basis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Cycle_space_addition.svg.png
tags:
    - Definitions
    - Special cycle bases
    - Induced cycles
    - Fundamental cycles
    - Weakly fundamental cycles
    - Face cycles
    - Integral bases
    - Minimum weight
    - Polynomial time algorithms
    - NP-hardness
    - In planar graphs
    - Applications
categories:
    - Algebraic graph theory
---
In graph theory, a branch of mathematics, a cycle basis of an undirected graph is a set of simple cycles that forms a basis of the cycle space of the graph. That is, it is a minimal set of cycles that allows every Eulerian subgraph to be expressed as a symmetric difference of basis cycles.
