---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_homomorphism
offline_file: ""
offline_thumbnail: ""
uuid: 386b8598-33a5-42d7-9fc1-4afbf4a8c322
updated: 1484308627
title: Graph homomorphism
tags:
    - Definitions
    - Properties
    - Connection to coloring and girth
    - Complexity
    - Notes
categories:
    - Graph theory
---
In the mathematical field of graph theory a graph homomorphism is a mapping between two graphs that respects their structure. More concretely it maps adjacent vertices to adjacent vertices.
