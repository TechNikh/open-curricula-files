---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subsidy
offline_file: ""
offline_thumbnail: ""
uuid: 79649080-3d8c-4316-a2a8-8cbff341c953
updated: 1484310186
title: subsidy
categories:
    - Dictionary
---
subsidy
     n : a grant paid by a government to an enterprise that benefits
         the public; "a subsidy for research in artificial
         intelligence"
