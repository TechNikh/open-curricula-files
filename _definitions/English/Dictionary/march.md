---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/march
offline_file: ""
offline_thumbnail: ""
uuid: 39b5fd4e-bacd-4d90-977d-88f43ce83879
updated: 1484310238
title: march
categories:
    - Dictionary
---
March
     n 1: the month following February and preceding April [syn: {Mar}]
     2: the act of marching; walking with regular steps (especially
        in a procession of some kind); "it was a long march"; "we
        heard the sound of marching" [syn: {marching}]
     3: a steady advance; "the march of science"; "the march of
        time"
     4: a procession of people walking together; "the march went up
        Fifth Avenue"
     5: district consisting of the area on either side of a border
        or boundary of a country or an area; "the Welsh marches
        between England and Wales" [syn: {borderland}, {border
        district}, {marchland}]
     6: genre of music written for ...
