---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Packet_erasure_channel
offline_file: ""
offline_thumbnail: ""
uuid: 4a6f7da5-7467-45db-ac12-254be22bc3d6
updated: 1484308562
title: Packet erasure channel
categories:
    - Coding theory
---
The packet erasure channel is a communication channel model where sequential packets are either received or lost (at a known location). This channel model is closely related to the binary erasure channel.
