---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Scalar_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: ac95b7d4-93e7-406b-b4e5-ab0d2372f844
updated: 1484309170
title: Scalar (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Vector_components.svg.png
tags:
    - Etymology
    - Definitions and properties
    - Scalars of vector spaces
    - Scalars as vector components
    - Scalars in normed vector spaces
    - Scalars in modules
    - Scaling transformation
    - Scalar operations (computer science)
categories:
    - Analytic geometry
---
A scalar is an element of a field which is used to define a vector space. A quantity described by multiple scalars, such as having both direction and magnitude, is called a vector.[1]
