---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zero-symmetric_graph
offline_file: ""
offline_thumbnail: ""
uuid: a03b9e58-217d-4121-9a0d-ac5c1a5d3301
updated: 1484309374
title: Zero-symmetric graph
tags:
    - Examples
    - Properties
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory, a zero-symmetric graph is a connected graph in which all vertices are symmetric to each other, each vertex has exactly three incident edges, and these three edges are not symmetric to each other. More precisely, it is a connected vertex-transitive cubic graph whose edges are partitioned into three different orbits by the automorphism group.[1] In these graphs, for every two vertices u and v, there is exactly one graph automorphism that takes u into v.[2]
