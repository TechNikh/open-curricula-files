---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Star_(game_theory)
offline_file: ""
offline_thumbnail: ""
uuid: def4f4dd-cf62-4bce-8af3-fa8556b4c2f2
updated: 1484308528
title: Star (game theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/He1523a_4.jpg
tags:
    - 'Why * ≠ 0'
    - 'Example of a value-* game'
categories:
    - Combinatorial game theory
---
In combinatorial game theory, star, written as 
  
    
      
        ∗
      
    
    {\displaystyle *}
  
 or 
  
    
      
        ∗
        1
      
    
    {\displaystyle *1}
  
, is the value given to the game where both players have only the option of moving to the zero game. Star may also be denoted as the surreal form {0|0}. This game is an unconditional first-player win.
