---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/soluble
offline_file: ""
offline_thumbnail: ""
uuid: 9a003831-9a8d-45e0-9837-e6505b9c4bac
updated: 1484310375
title: soluble
categories:
    - Dictionary
---
soluble
     adj 1: (of a substance) capable of being dissolved in some solvent
            (usually water) [ant: {insoluble}]
     2: susceptible of solution or of being solved or explained;
        "the puzzle is soluble" [ant: {insoluble}]
