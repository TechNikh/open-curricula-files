---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mile
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484561821
title: mile
categories:
    - Dictionary
---
mile
     n 1: a unit of length equal to 1760 yards [syn: {statute mile}, {stat
          mi}, {land mile}, {mi}]
     2: a unit of length used in navigation; equivalent to the
        distance spanned by one minute of arc in latitude; 1,852
        meters [syn: {nautical mile}, {mi}, {naut mi}, {knot}, {international
        nautical mile}, {air mile}]
     3: a large distance; "he missed by a mile"
     4: a former British unit of length once used in navigation;
        equivalent to 1828.8 meters (6000 feet) [syn: {sea mile}]
     5: a British unit of length equivalent to 1,853.18 meters
        (6,082 feet) [syn: {nautical mile}, {naut mi}, {mi}, {geographical
        mile}, {Admiralty ...
