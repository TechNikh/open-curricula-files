---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abdominal_cavity
offline_file: ""
offline_thumbnail: ""
uuid: 077f1d59-e5ed-4c5d-94c6-56ca4a86e7a0
updated: 1484308371
title: Abdominal cavity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Scheme_body_cavities-en.svg_0.png
tags:
    - Structure
    - Organs
    - Peritoneum
    - Mesentery
    - Omenta
    - Clinical significance
    - Ascites
    - Inflammation
categories:
    - Uncategorized
---
The abdominal cavity is a large body cavity in humans[1] and many other animals that contains many organs. It is a part of the abdominopelvic cavity.[2] It is located below the thoracic cavity, and above the pelvic cavity. Its dome-shaped roof is the thoracic diaphragm, a thin sheet of muscle under the lungs, and its floor is the pelvic inlet , opening into the pelvis.
