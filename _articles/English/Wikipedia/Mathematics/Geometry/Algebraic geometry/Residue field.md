---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Residue_field
offline_file: ""
offline_thumbnail: ""
uuid: c8b8d10e-a5a1-4f46-8d12-34e4be7114a2
updated: 1484309130
title: Residue field
tags:
    - Definition
    - Example
    - Properties
categories:
    - Algebraic geometry
---
In mathematics, the residue field is a basic construction in commutative algebra. If R is a commutative ring and m is a maximal ideal, then the residue field is the quotient ring k = R/m, which is a field.[1] Frequently, R is a local ring and m is then its unique maximal ideal.
