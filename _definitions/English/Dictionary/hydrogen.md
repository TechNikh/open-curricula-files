---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydrogen
offline_file: ""
offline_thumbnail: ""
uuid: 07946632-db7e-42fa-8aff-176776bf9296
updated: 1484310212
title: hydrogen
categories:
    - Dictionary
---
hydrogen
     n : a nonmetallic univalent element that is normally a colorless
         and odorless highly flammable diatomic gas; the simplest
         and lightest and most abundant element in the universe
         [syn: {H}, {atomic number 1}]
