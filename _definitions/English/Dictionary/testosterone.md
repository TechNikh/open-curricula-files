---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/testosterone
offline_file: ""
offline_thumbnail: ""
uuid: a0dbfe97-bb68-401e-9406-d61ee2968b2f
updated: 1484310158
title: testosterone
categories:
    - Dictionary
---
testosterone
     n : a potent androgenic hormone produced chiefly by the testes;
         responsible for the development of male secondary sex
         characteristics
