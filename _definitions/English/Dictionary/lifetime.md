---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lifetime
offline_file: ""
offline_thumbnail: ""
uuid: d056697b-a6ce-47bb-a7d3-7830568ab382
updated: 1484310293
title: lifetime
categories:
    - Dictionary
---
lifetime
     n : the period during which something is functional (as between
         birth and death); "the battery had a short life"; "he
         lived a long and happy life" [syn: {life}, {lifespan}]
