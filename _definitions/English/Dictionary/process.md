---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/process
offline_file: ""
offline_thumbnail: ""
uuid: e0757953-3954-42dc-8633-6d24dc1748ad
updated: 1484310363
title: process
categories:
    - Dictionary
---
process
     n 1: a particular course of action intended to achieve a result;
          "the procedure of obtaining a driver's license"; "it was
          a process of trial and error" [syn: {procedure}]
     2: a sustained phenomenon or one marked by gradual changes
        through a series of states; "events now in process"; "the
        process of calcification begins later for boys than for
        girls"
     3: (psychology) the performance of some composite cognitive
        activity; an operation that affects mental contents; "the
        process of thinking"; "the cognitive operation of
        remembering" [syn: {cognitive process}, {mental process},
        {operation}, {cognitive ...
