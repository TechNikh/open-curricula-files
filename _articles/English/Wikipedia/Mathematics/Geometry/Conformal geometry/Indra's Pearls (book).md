---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Indra%27s_Pearls_(book)'
offline_file: ""
offline_thumbnail: ""
uuid: 66a2c8c3-f0a5-468d-a97a-aa70d8e8b5c9
updated: 1484309185
title: "Indra's Pearls (book)"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Apollonian_gasket.svg.png
tags:
    - Title
    - Contents
    - Importance
categories:
    - Conformal geometry
---
Indra's Pearls: The Vision of Felix Klein is a geometry book written by David Mumford, Caroline Series and David Wright, and published by Cambridge University Press in 2002.
