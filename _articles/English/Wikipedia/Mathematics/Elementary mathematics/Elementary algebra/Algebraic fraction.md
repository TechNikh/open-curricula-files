---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_fraction
offline_file: ""
offline_thumbnail: ""
uuid: a9551483-e463-4b94-a0f8-e77a2a6ce029
updated: 1484308903
title: Algebraic fraction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_0.png
tags:
    - Terminology
    - Rational fractions
    - Irrational fractions
    - Notes
categories:
    - Elementary algebra
---
In algebra, an algebraic fraction is a fraction whose numerator and denominator are algebraic expressions. Two examples of algebraic fractions are 
  
    
      
        
          
            
              3
              x
            
            
              
                x
                
                  2
                
              
              +
              2
              x
              −
              3
            
          
        
      
    
    {\displaystyle {\frac {3x}{x^{2}+2x-3}}}
  
 and 
  
    
      
        
          
            
              x
              +
              2
            
            
              
                x
        ...
