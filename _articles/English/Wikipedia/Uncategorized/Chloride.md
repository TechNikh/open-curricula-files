---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chlorides
offline_file: ""
offline_thumbnail: ""
uuid: 4de2dbe6-d237-4d27-a91d-19063859d09a
updated: 1484308389
title: Chloride
tags:
    - Electronic properties
    - Occurrence in nature
    - Role in commerce
    - Water quality and processing
    - Domestic uses
    - Corrosion
    - Role in biology
    - Reactions of chloride
    - Examples
    - Other oxyanions
categories:
    - Uncategorized
---
The chloride ion /ˈklɔəraɪd/[3] is the anion (negatively charged ion) Cl−. It is formed when the element chlorine (a halogen) gains an electron or when a compound such as hydrogen chloride is dissolved in water or other polar solvents. Chloride salts such as sodium chloride are often very soluble in water.[4] It is an essential electrolyte located in all body fluids responsible for maintaining acid/base balance, transmitting nerve impulses and regulating fluid in and out of cells.[5] Less frequently, the word chloride may also form part of the "common" name of chemical compounds in which one or more chlorine atoms are covalently bonded. For example, methyl chloride, with the standard ...
