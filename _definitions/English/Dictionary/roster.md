---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/roster
offline_file: ""
offline_thumbnail: ""
uuid: 27661ade-3efe-44e6-9e24-03650c9bf0c0
updated: 1484310140
title: roster
categories:
    - Dictionary
---
roster
     n : a list of names; "his name was struck off the rolls" [syn: {roll}]
