---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reservoir
offline_file: ""
offline_thumbnail: ""
uuid: d0c5ddbd-95df-44fe-8715-966cc210bee7
updated: 1484310256
title: reservoir
categories:
    - Dictionary
---
reservoir
     n 1: a large or extra supply of something; "a reservoir of
          talent"
     2: lake used to store water for community use [syn: {artificial
        lake}]
     3: tank used for collecting and storing a liquid (as water or
        oil)
     4: anything (a person or animal or plant or substance) in which
        an infectious agent normally lives and multiplies; "an
        infectious agent depends on a reservoir for its survival"
        [syn: {source}]
