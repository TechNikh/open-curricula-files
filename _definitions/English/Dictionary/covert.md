---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/covert
offline_file: ""
offline_thumbnail: ""
uuid: 4602bccc-fe19-4c12-b9b3-0674bce0c57a
updated: 1484310221
title: covert
categories:
    - Dictionary
---
covert
     adj 1: secret or hidden; not openly practiced or engaged in or
            shown or avowed; "covert actions by the CIA"; "covert
            funding for the rebels" [ant: {overt}]
     2: of a wife; under the protection of her husband
     n 1: a flock of coots
     2: a covering that serves to conceal or shelter something;
        "they crouched behind the screen"; "under cover of
        darkness" [syn: {screen}, {cover}, {concealment}]
