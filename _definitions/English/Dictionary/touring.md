---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/touring
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484536381
title: touring
categories:
    - Dictionary
---
touring
     adj : working for a short time in different places; "itinerant
           laborers"; "a road show"; "traveling salesman";
           "touring company" [syn: {itinerant}, {road}, {traveling}]
