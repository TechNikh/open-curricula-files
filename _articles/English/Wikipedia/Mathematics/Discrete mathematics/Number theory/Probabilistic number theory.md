---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Probabilistic_number_theory
offline_file: ""
offline_thumbnail: ""
uuid: 16b2b5d3-51b3-4620-961e-f2f824c44d22
updated: 1484308654
title: Probabilistic number theory
categories:
    - Number theory
---
Probabilistic number theory is a subfield of number theory, which explicitly uses probability to answer questions of number theory. One basic idea underlying it is that different prime numbers are, in some serious sense, like independent random variables. This however is not an idea that has a unique useful formal expression.
