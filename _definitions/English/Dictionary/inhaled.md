---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inhaled
offline_file: ""
offline_thumbnail: ""
uuid: 7e827d0e-3d49-4c7d-89c8-47e24b0031c6
updated: 1484310158
title: inhaled
categories:
    - Dictionary
---
inhaled
     adj : drawn into the lungs; breathed in; "inhaled smoke can damage
           the lungs" [ant: {exhaled}]
