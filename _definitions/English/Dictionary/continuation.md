---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/continuation
offline_file: ""
offline_thumbnail: ""
uuid: ec7610ed-ae00-44f5-a9e5-7237eb2b6cfb
updated: 1484310252
title: continuation
categories:
    - Dictionary
---
continuation
     n 1: the act of continuing an activity without interruption [syn:
           {continuance}] [ant: {discontinuance}, {discontinuance}]
     2: a part added to a book or play that continues and extends it
        [syn: {sequel}]
     3: a Gestalt principle of organization holding that there is an
        innate tendence to perceive a line as continuing its
        established direction [syn: {good continuation}, {law of
        continuation}]
     4: the consequence of being lengthened in duration [syn: {lengthiness},
         {prolongation}, {protraction}]
