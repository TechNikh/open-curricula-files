---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shortened
offline_file: ""
offline_thumbnail: ""
uuid: e2d30573-4731-46a5-a0c3-0444e78da5c4
updated: 1484310210
title: shortened
categories:
    - Dictionary
---
shortened
     adj 1: cut short; "a sawed-off shotgun"; "a sawed-off broomstick";
            "the shortened rope was easier to use" [syn: {sawed-off},
             {sawn-off}]
     2: cut short in duration; "the abbreviated speech"; "a
        curtailed visit"; "her shortened life was clearly the
        result of smoking"; "an unsatisfactory truncated
        conversation" [syn: {abbreviated}, {truncated}]
     3: shortened by or as if by means of parts that slide one
        within another or are crushed one into another; "a miracle
        that anyone survived in the telescoped cars"; "years that
        seemed telescoped like time in a dream" [syn: {telescoped}]
     4: with parts ...
