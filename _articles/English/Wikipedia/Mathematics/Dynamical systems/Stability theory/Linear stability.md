---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Linear_stability
offline_file: ""
offline_thumbnail: ""
uuid: 050a28de-f17f-47b0-81a1-9331bdad9780
updated: 1484308834
title: Linear stability
tags:
    - 'Example 1: ODE'
    - 'Example 2: NLS'
categories:
    - Stability theory
---
In mathematics, in the theory of differential equations and dynamical systems, a particular stationary or quasistationary solution to a nonlinear system is called linearly unstable if the linearization of the equation at this solution has the form 
  
    
      
        
          
            
              d
              r
            
            
              d
              t
            
          
        
        =
        A
        r
      
    
    {\displaystyle {\frac {dr}{dt}}=Ar}
  
, where A is a linear operator whose spectrum contains eigenvalues with positive real part. If all the eigenvalues have negative real part, then the solution is called linearly stable. Other ...
