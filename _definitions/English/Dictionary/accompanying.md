---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accompanying
offline_file: ""
offline_thumbnail: ""
uuid: 086fdc79-e06e-438d-b693-99d2c8b70bf7
updated: 1484310486
title: accompanying
categories:
    - Dictionary
---
accompanying
     adj : following as a consequence; "an excessive growth of
           bureaucracy, with related problems"; "snags incidental
           to the changeover in management" [syn: {attendant}, {concomitant},
            {incidental}, {incidental to(p)}]
