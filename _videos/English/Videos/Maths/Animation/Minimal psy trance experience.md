---
version: 1
type: video
provider: YouTube
id: kZiBtrXnWUI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 548fe45b-f1a1-41a6-909c-3f83c649b749
updated: 1486069649
title: Minimal psy trance experience
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/kZiBtrXnWUI/default.jpg
    - https://i3.ytimg.com/vi/kZiBtrXnWUI/1.jpg
    - https://i3.ytimg.com/vi/kZiBtrXnWUI/2.jpg
    - https://i3.ytimg.com/vi/kZiBtrXnWUI/3.jpg
---
