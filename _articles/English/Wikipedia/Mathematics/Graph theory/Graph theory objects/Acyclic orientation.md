---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Acyclic_orientation
offline_file: ""
offline_thumbnail: ""
uuid: 297c9cf6-a7d1-4bd9-9262-a8e32f6d337f
updated: 1484309442
title: Acyclic orientation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Acyclic_orientations_of_C4.svg.png
tags:
    - Construction
    - Relation to coloring
    - Duality
    - Edge flipping
    - Special cases
categories:
    - Graph theory objects
---
In graph theory, an acyclic orientation of an undirected graph is an assignment of a direction to each edge (an orientation) that does not form any directed cycle and therefore makes it into a directed acyclic graph. Every graph has an acyclic orientation.
