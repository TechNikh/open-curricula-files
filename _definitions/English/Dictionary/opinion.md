---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/opinion
offline_file: ""
offline_thumbnail: ""
uuid: 8d5881f2-5c1c-4a06-ad22-eb8636c6eb43
updated: 1484310448
title: opinion
categories:
    - Dictionary
---
opinion
     n 1: a personal belief or judgment that is not founded on proof
          or certainty; "my opinion differs from yours"; "what are
          your thoughts on Haiti?" [syn: {sentiment}, {persuasion},
           {view}, {thought}]
     2: a belief or sentiment shared by most people; the voice of
        the people; "he asked for a poll of public opinion" [syn:
        {public opinion}, {popular opinion}, {vox populi}]
     3: a message expressing a belief about something; the
        expression of a belief that is held with confidence but
        not substantiated by positive knowledge or proof; "his
        opinions appeared frequently on the editorial page" [syn:
        ...
