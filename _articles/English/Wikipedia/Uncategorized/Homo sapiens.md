---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homosapiens
offline_file: ""
offline_thumbnail: ""
uuid: 425875fc-2226-46bc-806b-95103720403f
updated: 1484308344
title: Homo sapiens
tags:
    - Name and taxonomy
    - Origin
    - Evolution
categories:
    - Uncategorized
---
Homo sapiens (Latin: "wise man") is the binomial nomenclature (also known as the scientific name) for the only extant human species. Homo is the human genus, which also includes Neanderthals and many other extinct species of hominid; H. sapiens is the only surviving species of the genus Homo. Modern humans are the subspecies Homo sapiens sapiens, which differentiates them from what has been argued to be their direct ancestor, Homo sapiens idaltu. The ingenuity and adaptability of Homo sapiens has led to its becoming the most influential species on the Earth; it is currently deemed of least concern on the Red List of endangered species by the International Union for Conservation of Nature.[1]
