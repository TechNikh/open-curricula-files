---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sympathise
offline_file: ""
offline_thumbnail: ""
uuid: 59fcac56-6792-4000-b964-dba724b6de75
updated: 1484310567
title: sympathise
categories:
    - Dictionary
---
sympathise
     v 1: share the feelings of; understand the sentiments of [syn: {sympathize}]
     2: to feel or express sympathy or compassion [syn: {commiserate},
         {sympathize}]
     3: be understanding of; "You don't need to explain--I
        understand!" [syn: {sympathize}, {empathize}, {empathise},
         {understand}]
