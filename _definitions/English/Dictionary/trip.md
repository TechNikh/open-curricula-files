---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trip
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484322361
title: trip
categories:
    - Dictionary
---
trip
     n 1: a journey for some purpose (usually including the return);
          "he took a trip to the shopping center"
     2: a hallucinatory experience induced by drugs; "an acid trip"
     3: an accidental misstep threatening (or causing) a fall; "he
        blamed his slip on the ice"; "the jolt caused many slips
        and a few spills" [syn: {slip}]
     4: an exciting or stimulting experience [syn: {head trip}]
     5: a catch mechanism that acts as a switch; "the pressure
        activates the tripper and releases the water" [syn: {tripper}]
     6: a light or nimble tread; "he heard the trip of women's feet
        overhead"
     7: an unintentional but embarrassing blunder; ...
