---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monkey
offline_file: ""
offline_thumbnail: ""
uuid: 964845b6-3160-4bdb-b7ff-3212af33f034
updated: 1484310168
title: monkey
categories:
    - Dictionary
---
monkey
     n 1: any of various long-tailed primates (excluding the
          prosimians)
     2: one who is playfully mischievous [syn: {imp}, {scamp}, {rascal},
         {rapscallion}, {scalawag}, {scallywag}]
     v 1: play around with or alter or falsify, usually secretively or
          dishonestly; "Someone tampered with the documents on my
          desk"; "The reporter fiddle with the facts" [syn: {tamper},
           {fiddle}]
     2: do random, unplanned work or activities or spend time idly;
        "The old lady is usually mucking about in her little
        house" [syn: {putter}, {mess around}, {potter}, {tinker},
        {monkey around}, {muck about}, {muck around}]
