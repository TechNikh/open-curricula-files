---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Enrique_Pujals
offline_file: ""
offline_thumbnail: ""
uuid: d9193067-98aa-4388-8467-de2d3aa8c999
updated: 1484308692
title: Enrique Pujals
categories:
    - Dynamical systems theorists
---
Enrique Ramiro Pujals is an Argentine-born Brazilian mathematician known for his contributions to the understanding of dynamical systems.[1][2] He currently works as full researcher at IMPA.[3]
