---
version: 1
type: video
provider: YouTube
id: xGmef7lFc5w
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Finding%20intercepts%20from%20an%20equation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b05c5515-a1e5-4451-8f8f-6acc2ddda3d4
updated: 1486069565
title: Finding intercepts from an equation
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/xGmef7lFc5w/default.jpg
    - https://i3.ytimg.com/vi/xGmef7lFc5w/1.jpg
    - https://i3.ytimg.com/vi/xGmef7lFc5w/2.jpg
    - https://i3.ytimg.com/vi/xGmef7lFc5w/3.jpg
---
