---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/days
offline_file: ""
offline_thumbnail: ""
uuid: 7c357729-f4d7-46c3-baad-f99a7dd55c1b
updated: 1484310290
title: days
categories:
    - Dictionary
---
days
     n : the time during which someone's life continues; "the
         monarch's last days"; "in his final years" [syn: {years}]
