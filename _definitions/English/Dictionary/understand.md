---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/understand
offline_file: ""
offline_thumbnail: ""
uuid: 2560e872-2979-435a-9227-d0988f8dec7d
updated: 1484310361
title: understand
categories:
    - Dictionary
---
understand
     v 1: know and comprehend the nature or meaning of; "She did not
          understand her husband"; "I understand what she means"
     2: perceive (an idea or situation) mentally; "Now I see!"; "I
        just can't see your point"; "Does she realize how
        important this decision is?"; "I don't understand the
        idea" [syn: {realize}, {realise}, {see}]
     3: make sense of a language; "She understands French"; "Can you
        read Greek?" [syn: {read}, {interpret}, {translate}]
     4: believe to be the case; "I understand you have no previous
        experience?" [syn: {infer}]
     5: be understanding of; "You don't need to explain--I
        understand!" [syn: ...
