---
version: 1
type: video
provider: YouTube
id: zwDJ1wVr7Is
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Forces%20and%20Newton%27s%20laws%20of%20motion/Introduction%20to%20tension%20%28part%202%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f2347c05-42fc-41f0-9e1c-281c7cdba3e9
updated: 1486069597
title: Introduction to tension (part 2)
categories:
    - "Forces and Newton's laws of motion"
thumbnail_urls:
    - https://i3.ytimg.com/vi/zwDJ1wVr7Is/default.jpg
    - https://i3.ytimg.com/vi/zwDJ1wVr7Is/1.jpg
    - https://i3.ytimg.com/vi/zwDJ1wVr7Is/2.jpg
    - https://i3.ytimg.com/vi/zwDJ1wVr7Is/3.jpg
---
A slightly more difficult tension problem. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/forces-newtons-laws/tension-tutorial/v/tension-in-an-accelerating-system-and-pie-in-the-face?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/forces-newtons-laws/tension-tutorial/v/introduction-to-tension?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
