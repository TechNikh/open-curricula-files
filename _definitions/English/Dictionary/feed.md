---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feed
offline_file: ""
offline_thumbnail: ""
uuid: c9108964-98cd-47bd-a455-816ac8e0d3b8
updated: 1484310268
title: feed
categories:
    - Dictionary
---
feed
     n : food for domestic livestock [syn: {provender}]
     v 1: provide as food; "Feed the guests the nuts"
     2: give food to; "Feed the starving children in India"; "don't
        give the child this tough meat" [syn: {give}] [ant: {starve}]
     3: feed into; supply; "Her success feeds her vanity"
     4: introduce continuously; "feed carrots into a food processor"
        [syn: {feed in}]
     5: support or promote; "His admiration fed her vanity"
     6: take in food; used of animals only; "This dog doesn't eat
        certain kinds of meat"; "What do whales eat?" [syn: {eat}]
     7: serve as food for; be the food for; "This dish feeds six"
     8: move along, of liquids; ...
