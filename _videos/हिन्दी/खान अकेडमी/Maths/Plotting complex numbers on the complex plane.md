---
version: 1
type: video
provider: YouTube
id: jOGqoZq9cHk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0416ef50-1fbc-4526-9ede-53c5026cacbf
updated: 1486069614
title: Plotting complex numbers on the complex plane
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/jOGqoZq9cHk/default.jpg
    - https://i3.ytimg.com/vi/jOGqoZq9cHk/1.jpg
    - https://i3.ytimg.com/vi/jOGqoZq9cHk/2.jpg
    - https://i3.ytimg.com/vi/jOGqoZq9cHk/3.jpg
---
