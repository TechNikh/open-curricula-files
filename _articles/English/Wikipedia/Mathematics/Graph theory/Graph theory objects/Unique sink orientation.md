---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unique_sink_orientation
offline_file: ""
offline_thumbnail: ""
uuid: ddd6f49f-0413-407f-9b53-da09d1aa1d4b
updated: 1484309447
title: Unique sink orientation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_21.png
categories:
    - Graph theory objects
---
In mathematics, a unique sink orientation is an orientation of the edges of a polytope such that, in every face of the polytope (including the whole polytope as one of the faces), there is exactly one vertex for which all adjoining edges are oriented inward (i.e. towards that vertex). If a polytope is given together with a linear objective function, and edges are oriented from vertices with smaller objective function values to vertices with larger objective values, the result is a unique sink orientation. Thus, unique sink orientations can be used to model linear programs as well as certain nonlinear programs such as the smallest circle problem.
