---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/returning
offline_file: ""
offline_thumbnail: ""
uuid: 710f32b3-832f-4c9f-85bd-0f9938a3d667
updated: 1484310266
title: returning
categories:
    - Dictionary
---
returning
     adj 1: tending to return to an earlier state [syn: {reverting}]
     2: tending to be turned back [syn: {returning(a)}, {reversive}]
