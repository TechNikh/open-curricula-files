---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mixed_volume
offline_file: ""
offline_thumbnail: ""
uuid: 3f77b1b1-23d7-40ac-8971-8e8af9c5ca8a
updated: 1484309197
title: Mixed volume
tags:
    - Definition
    - Properties
    - Quermassintegrals
    - Intrinsic volumes
    - "Hadwiger's characterization theorem"
    - Notes
categories:
    - Convex geometry
---
In mathematics, more specifically, in convex geometry, the mixed volume is a way to associate a non-negative number to an 
  
    
      
        n
      
    
    {\displaystyle n}
  
-tuple of convex bodies in the 
  
    
      
        n
      
    
    {\displaystyle n}
  
-dimensional space. This number depends on the size of the bodies and on their relative orientation to each other.
