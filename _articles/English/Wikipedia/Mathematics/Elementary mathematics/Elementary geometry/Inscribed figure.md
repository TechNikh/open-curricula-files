---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inscribed_figure
offline_file: ""
offline_thumbnail: ""
uuid: 0a35dfed-82ac-409d-80b0-318790d3eff6
updated: 1484308932
title: Inscribed figure
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/275px-Inscribed_circles.svg.png
categories:
    - Elementary geometry
---
In geometry, an inscribed planar shape or solid is one that is enclosed by and "fits snugly" inside another geometric shape or solid. To say that "figure F is inscribed in figure G" means precisely the same thing as "figure G is circumscribed about figure F". A circle or ellipse inscribed in a convex polygon (or a sphere or ellipsoid inscribed in a convex polyhedron) is tangent to every side or face of the outer figure (but see Inscribed sphere for semantic variants). A polygon inscribed in a circle, ellipse, or polygon (or a polyhedron inscribed in a sphere, ellipsoid, or polyhedron) has each vertex on the outer figure; if the outer figure is a polygon or polyhedron, there must be a vertex ...
