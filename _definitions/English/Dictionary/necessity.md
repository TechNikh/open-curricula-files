---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/necessity
offline_file: ""
offline_thumbnail: ""
uuid: 7f58c575-b18c-4cff-b170-548f98efad0f
updated: 1484310479
title: necessity
categories:
    - Dictionary
---
necessity
     n 1: the condition of being essential or indispensable
     2: anything indispensable; "food and shelter are necessities of
        life"; "the essentials of the good life"; "allow farmers
        to buy their requirements under favorable conditions"; "a
        place where the requisites of water fuel and fodder can be
        obtained" [syn: {essential}, {requirement}, {requisite}, {necessary}]
        [ant: {inessential}]
