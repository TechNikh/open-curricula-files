---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sadness
offline_file: ""
offline_thumbnail: ""
uuid: 419c368e-3c63-4c87-b489-e8469f5659ce
updated: 1484310545
title: sadness
categories:
    - Dictionary
---
sadness
     n 1: emotions experienced when not in a state of well-being [syn:
           {unhappiness}] [ant: {happiness}]
     2: the state of being sad; "she tired of his perpetual sadness"
        [syn: {sorrow}, {sorrowfulness}]
