---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/requirement
offline_file: ""
offline_thumbnail: ""
uuid: 5e0dc434-4986-404c-b2c8-d312805203ff
updated: 1484310464
title: requirement
categories:
    - Dictionary
---
requirement
     n 1: required activity; "the requirements of his work affected
          his health"; "there were many demands on his time" [syn:
           {demand}]
     2: anything indispensable; "food and shelter are necessities of
        life"; "the essentials of the good life"; "allow farmers
        to buy their requirements under favorable conditions"; "a
        place where the requisites of water fuel and fodder can be
        obtained" [syn: {necessity}, {essential}, {requisite}, {necessary}]
        [ant: {inessential}]
     3: something that is required in advance; "Latin was a
        prerequisite for admission" [syn: {prerequisite}]
