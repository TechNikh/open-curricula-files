---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pitiably
offline_file: ""
offline_thumbnail: ""
uuid: 1b382993-c70b-4837-8314-5da59a4735da
updated: 1484310537
title: pitiably
categories:
    - Dictionary
---
pitiably
     adv : in a manner arousing sympathy and compassion; "the sick
           child cried pathetically" [syn: {pathetically}]
