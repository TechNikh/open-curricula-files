---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bose_gas
offline_file: ""
offline_thumbnail: ""
uuid: e6c9ce2f-8a77-4072-81e9-e16d62c93a83
updated: 1484308837
title: Bose gas
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Bose_gas_quantities.png
tags:
    - Thomas–Fermi approximation
    - Inclusion of the ground state
    - Thermodynamics
categories:
    - Thermodynamics
---
An ideal Bose gas is a quantum-mechanical version of a classical ideal gas. It is composed of bosons, which have an integer value of spin, and obey Bose–Einstein statistics. The statistical mechanics of bosons were developed by Satyendra Nath Bose for photons, and extended to massive particles by Albert Einstein who realized that an ideal gas of bosons would form a condensate at a low enough temperature, unlike a classical ideal gas. This condensate is known as a Bose–Einstein condensate.
