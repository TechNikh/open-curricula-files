---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bihar
offline_file: ""
offline_thumbnail: ""
uuid: 047f58bc-f77e-4b21-b67f-26dfc2ed5b14
updated: 1484308420
title: Bihar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-BR_2000.png
tags:
    - Etymology
    - History
    - Ancient
    - Medieval
    - Colonial era
    - 'Pre- and post-Independence'
    - Geography and Climate
    - Flora and Fauna
    - Demographics
    - Government and administration
    - Politics
    - Economy
    - Agriculture
    - Industry
    - 'Income distribution: north-south divide'
    - culture
    - Language and literature
    - Performing arts
    - Cinema
    - religion
    - Media
    - Transportation
    - Airways
    - Inland Waterways
    - Tourism
    - Education
categories:
    - Uncategorized
---
Bihar (/bᵻˈhɑːr/; Hindustani pronunciation: [bɪˈɦaːr]) is a state in the eastern part of India.[7][8] It is the 13th-largest state of India, with an area of 94,163 km2 (36,357 sq mi). The third-largest state of India by population, it is contiguous with Uttar Pradesh to its west, Nepal to the north, the northern part of West Bengal to the east, with Jharkhand to the south. The Bihar plain is split by the river Ganges which flows from west to east.[7]
