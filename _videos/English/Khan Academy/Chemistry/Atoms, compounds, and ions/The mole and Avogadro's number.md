---
version: 1
type: video
provider: YouTube
id: AsqEkF7hcII
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Atoms%2C%20compounds%2C%20and%20ions/The%20mole%20and%20Avogadro%27s%20number.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7de53b43-ad3c-4bf8-b33c-c01eced65023
updated: 1486069583
title: "The mole and Avogadro's number"
categories:
    - Atoms, compounds, and ions
thumbnail_urls:
    - https://i3.ytimg.com/vi/AsqEkF7hcII/default.jpg
    - https://i3.ytimg.com/vi/AsqEkF7hcII/1.jpg
    - https://i3.ytimg.com/vi/AsqEkF7hcII/2.jpg
    - https://i3.ytimg.com/vi/AsqEkF7hcII/3.jpg
---
Introduction to the idea of a mole as a number (vs. an animal).

Watch the next lesson: https://www.khanacademy.org/science/chemistry/atomic-structure-and-properties/introduction-to-compounds/v/empirical-molecular-and-structural-formulas?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/atomic-structure-and-properties/introduction-to-the-atom/v/atomic-mass?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
