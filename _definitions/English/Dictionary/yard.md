---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yard
offline_file: ""
offline_thumbnail: ""
uuid: 90cd780a-ece1-42e2-ad40-54af14cb8e42
updated: 1484310522
title: yard
categories:
    - Dictionary
---
yard
     n 1: a unit of length equal to 3 feet; defined as 91.44
          centimeters; originally taken to be the average length
          of a stride [syn: {pace}]
     2: the enclosed land around a house or other building; "it was
        a small house with almost no yard" [syn: {grounds}, {curtilage}]
     3: a tract of land enclosed for particular activities
        (sometimes paved and usually associated with buildings);
        "they opened a repair yard on the edge of town"
     4: an area having a network of railway tracks and sidings for
        storage and maintenance of cars and engines [syn: {railway
        yard}]
     5: an enclosure for animals (as chicken or livestock)
    ...
