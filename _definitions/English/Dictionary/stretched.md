---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stretched
offline_file: ""
offline_thumbnail: ""
uuid: da9818cd-8b1d-4c20-be89-6d26285617ce
updated: 1484310283
title: stretched
categories:
    - Dictionary
---
stretched
     adj 1: (of muscles) relieved of stiffness by stretching;
            "well-stretched muscles are less susceptible to
            injury"
     2: (of the body) extended to full length; "he lay stretched out
        on the bed"; "stretched her calves before running" [syn: {stretched
        out}]
     3: extended or spread over a wide area or distance; "broad
        fields lay stretched on both sides of us"
