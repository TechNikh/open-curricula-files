---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/biology
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484385961
title: biology
categories:
    - Dictionary
---
biology
     n 1: the science that studies living organisms [syn: {biological
          science}]
     2: characteristic life processes and phenomena of living
        organisms; "the biology of viruses"
     3: all the plant and animal life of a particular region [syn: {biota}]
