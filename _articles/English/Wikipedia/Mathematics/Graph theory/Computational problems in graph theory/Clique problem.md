---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clique_problem
offline_file: ""
offline_thumbnail: ""
uuid: 9e444029-5db4-4463-aa68-e7da901c0218
updated: 1484309391
title: Clique problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Brute_force_Clique_algorithm.svg.png
tags:
    - History and applications
    - Definitions
    - Algorithms
    - Maximal versus maximum
    - Cliques of fixed size
    - Listing all maximal cliques
    - Finding maximum cliques in arbitrary graphs
    - Special classes of graphs
    - Approximation algorithms
    - Lower bounds
    - NP-completeness
    - Circuit complexity
    - Decision tree complexity
    - Fixed-parameter intractability
    - Hardness of approximation
    - Notes
categories:
    - Computational problems in graph theory
---
In computer science, the clique problem is any of a number of computational problems of finding cliques (subsets of vertices, all adjacent to each other, also called complete subgraphs) in a graph.
