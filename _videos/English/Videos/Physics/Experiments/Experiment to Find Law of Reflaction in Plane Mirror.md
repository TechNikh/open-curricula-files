---
version: 1
type: video
provider: YouTube
id: l62DHclurfk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6f702eea-a28c-42c9-9176-7926b7c5fc9f
updated: 1486069636
title: Experiment to Find Law of Reflaction in Plane Mirror
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/l62DHclurfk/default.jpg
    - https://i3.ytimg.com/vi/l62DHclurfk/1.jpg
    - https://i3.ytimg.com/vi/l62DHclurfk/2.jpg
    - https://i3.ytimg.com/vi/l62DHclurfk/3.jpg
---
