---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convinced
offline_file: ""
offline_thumbnail: ""
uuid: 19c42754-a963-4d67-abe2-4c3603d486c4
updated: 1484310575
title: convinced
categories:
    - Dictionary
---
convinced
     adj 1: having a strong belief or conviction; "a convinced and
            fanatical pacifist" [ant: {unconvinced}]
     2: persuaded of; very sure; "were convinced that it would be to
        their advantage to join"; "I am positive he is lying";
        "was confident he would win" [syn: {convinced(p)}, {positive(p)},
         {confident(p)}]
