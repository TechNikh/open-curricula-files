---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Catenary_ring
offline_file: ""
offline_thumbnail: ""
uuid: 18d676f2-7404-44ab-807a-9e1e1c42b04a
updated: 1484309138
title: Catenary ring
tags:
    - Dimension formula
    - Examples
    - A ring that is catenary but not universally catenary
categories:
    - Algebraic geometry
---
any two strictly increasing chains
