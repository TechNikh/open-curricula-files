---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Meghalaya
offline_file: ""
offline_thumbnail: ""
uuid: 0ef13faa-442a-42cf-99be-f97bd76fe0a0
updated: 1484308415
title: Meghalaya
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tea_Plantation_Agriculture_in_Meghalaya_India_on_the_way_to_Shillong.jpg
tags:
    - History
    - geography
    - Climate
    - Flora and Fauna
    - Demographics
    - population
    - religion
    - Languages
    - Districts
    - Education
    - Colleges
    - Government and politics
    - State government
    - Local-Self Government
    - Economy
    - Agriculture
    - Industry
    - Electricity infrastructure
    - Education infrastructure
    - Health infrastructure
    - Urban areas
    - New proposal for urban areas
    - Culture and society
    - Social institutions
    - Traditional political institutions
    - Festivals
    - Spirituality
    - Transport
    - Road Network
    - Railway
    - Aviation
    - Tourism
    - Major issues
    - Illegal immigration
    - Violence
    - Political Instability
    - Jhum farming
    - Media
    - Bibliography
categories:
    - Uncategorized
---
Meghalaya (/meɪɡˈɑːləjə/[3] or US /ˌmeɪɡəˈleɪə/;[4] /meːɡʱaːləj(ə)/[citation needed]) is a state in northeast India. The name means "the abode of clouds" in Sanskrit. The population of Meghalaya as of 2014 is estimated to be 3,211,474.[5] Meghalaya covers an area of approximately 22,430 square kilometers, with a length to breadth ratio of about 3:1.[6]
