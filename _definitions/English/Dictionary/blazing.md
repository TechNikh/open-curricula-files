---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blazing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484505781
title: blazing
categories:
    - Dictionary
---
blazing
     adj 1: shining intensely; "the blazing sun"; "blinding headlights";
            "dazzling snow"; "fulgent patterns of sunlight"; "the
            glaring sun" [syn: {blinding}, {dazzling}, {fulgent},
            {glaring}, {glary}]
     2: lighted up by or as by fire or flame; "forests set ablaze
        (or afire) by lightning"; "even the car's tires were
        aflame"; "a night aflare with fireworks"; "candles alight
        on the tables"; "blazing logs in the fireplace"; "a
        burning cigarette"; "a flaming crackling fire"; "houses on
        fire" [syn: {ablaze(p)}, {afire(p)}, {aflame(p)}, {aflare(p)},
         {alight(p)}, {burning}, {flaming}, {on fire(p)}]
     ...
