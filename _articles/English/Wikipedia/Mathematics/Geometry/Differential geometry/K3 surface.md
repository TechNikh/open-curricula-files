---
version: 1
type: article
id: https://en.wikipedia.org/wiki/K3_surface
offline_file: ""
offline_thumbnail: ""
uuid: a8dc1c55-3174-4f2b-a2aa-a16319b55ecc
updated: 1484309226
title: K3 surface
tags:
    - Definition
    - Calculation of the Betti numbers
    - Properties
    - The period map
    - Projective K3 surfaces
    - Relation to string duality
    - Examples
categories:
    - Differential geometry
---
In the Enriques–Kodaira classification of surfaces they form one of the 4 classes of surfaces of Kodaira dimension 0.
