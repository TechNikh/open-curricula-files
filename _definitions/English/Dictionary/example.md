---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/example
offline_file: ""
offline_thumbnail: ""
uuid: 40690c4c-8793-4330-b9c0-a74ecbc8eb74
updated: 1484310357
title: example
categories:
    - Dictionary
---
example
     n 1: an item of information that is representative of a type;
          "this patient provides a typical example of the
          syndrome"; "there is an example on page 10" [syn: {illustration},
           {instance}, {representative}]
     2: a representative form or pattern; "I profited from his
        example" [syn: {model}]
     3: something to be imitated; "an exemplar of success"; "a model
        of clarity"; "he is the very model of a modern major
        general" [syn: {exemplar}, {model}, {good example}]
     4: punishment intended as a warning to others; "they decided to
        make an example of him" [syn: {deterrent example}, {lesson},
         {object lesson}]
 ...
