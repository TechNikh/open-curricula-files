---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cheating
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484454661
title: cheating
categories:
    - Dictionary
---
cheating
     adj 1: not faithful to a spouse or lover; "adulterous husbands and
            wives"; "a two-timing boyfriend" [syn: {adulterous}, {cheating(a)},
             {two-timing(a)}]
     2: violating accepted standards or rules; "a dirty fighter";
        "used foul means to gain power"; "a nasty unsporting
        serve"; "fined for unsportsmanlike behavior" [syn: {cheating(a)},
         {dirty}, {foul}, {unsporting}, {unsportsmanlike}]
     n : a deception for profit to yourself [syn: {cheat}]
