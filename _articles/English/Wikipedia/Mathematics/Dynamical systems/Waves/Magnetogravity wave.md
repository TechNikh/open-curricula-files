---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magnetogravity_wave
offline_file: ""
offline_thumbnail: ""
uuid: 2be67ce4-f5b3-4719-bde4-acba8fca83ae
updated: 1484308887
title: Magnetogravity wave
categories:
    - Waves
---
A magnetogravity wave is a type of plasma wave. A magnetogravity wave is an acoustic gravity wave which is associated with fluctuations in the background magnetic field.[1] In this context, gravity wave refers to a classical fluid wave, and is completely unrelated to the relativistic gravitational wave.
