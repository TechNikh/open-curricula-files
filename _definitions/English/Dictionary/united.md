---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/united
offline_file: ""
offline_thumbnail: ""
uuid: e9121f45-01a5-4362-a104-b074dfcbb163
updated: 1484310254
title: united
categories:
    - Dictionary
---
united
     adj 1: characterized by unity; being or joined into a single
            entity; "presented a united front" [ant: {divided}]
     2: involving the joint activity of two or more; "the attack was
        met by the combined strength of two divisions"; "concerted
        action"; "the conjunct influence of fire and strong
        dring"; "the conjunctive focus of political opposition";
        "a cooperative effort"; "a united effort"; "joint military
        activities" [syn: {combined}, {concerted}, {conjunct}, {conjunctive},
         {cooperative}]
     3: of or relating to two people who are married to each other
        [syn: {joined}]
