---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curve
offline_file: ""
offline_thumbnail: ""
uuid: bfa94d3f-ffa4-40a7-b2ae-459b44905184
updated: 1484310218
title: curve
categories:
    - Dictionary
---
curve
     n 1: the trace of a point whose direction of motion changes [syn:
           {curved shape}] [ant: {straight line}]
     2: a line on a graph representing data
     3: a baseball thrown with spin so that its path curves as it
        approach the batter [syn: {curve ball}, {breaking ball}, {bender}]
     4: the property possessed by the curving of a line or surface
        [syn: {curvature}]
     5: curved segment (of a road or river or railroad track etc.)
        [syn: {bend}]
     v 1: turn sharply; change direction abruptly; "The car cut to the
          left at the intersection"; "The motorbike veered to the
          right" [syn: {swerve}, {sheer}, {trend}, {veer}, {slue},
 ...
