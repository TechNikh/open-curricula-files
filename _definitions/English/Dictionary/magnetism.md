---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnetism
offline_file: ""
offline_thumbnail: ""
uuid: 7292dcea-777d-4fc7-976a-1ef987ed52ba
updated: 1484310196
title: magnetism
categories:
    - Dictionary
---
magnetism
     n 1: attraction for iron; associated with electric currents as
          well as magnets; characterized by fields of force [syn:
          {magnetic attraction}, {magnetic force}]
     2: the branch of science that studies magnetism [syn: {magnetics}]
