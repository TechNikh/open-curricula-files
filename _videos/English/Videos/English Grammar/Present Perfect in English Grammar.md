---
version: 1
type: video
provider: YouTube
id: elfGbizcxEU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Present%20Perfect%20in%20English%20Grammar.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 59f9f79a-e013-445e-b637-849a274ebac1
updated: 1486069632
title: Present Perfect in English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/elfGbizcxEU/default.jpg
    - https://i3.ytimg.com/vi/elfGbizcxEU/1.jpg
    - https://i3.ytimg.com/vi/elfGbizcxEU/2.jpg
    - https://i3.ytimg.com/vi/elfGbizcxEU/3.jpg
---
