---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Amenable_number
offline_file: ""
offline_thumbnail: ""
uuid: 22e5c95d-bafd-4dcc-8c6e-1bc7a02bba4e
updated: 1484308535
title: Amenable number
categories:
    - Number theory
---
An amenable number is a positive integer for which there exists a multiset of as many integers as the original number that both add up to the original number and when multiplied together give the original number. To put it algebraically, for a positive integer n, there is a multiset of n integers {a1, ..., an}, for which the equalities
