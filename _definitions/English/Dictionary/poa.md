---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poa
offline_file: ""
offline_thumbnail: ""
uuid: c3bb2802-3f34-48ac-9f29-566ea1a15ccb
updated: 1484310154
title: poa
categories:
    - Dictionary
---
Poa
     n : chiefly perennial grasses of cool temperate regions [syn: {genus
         Poa}]
