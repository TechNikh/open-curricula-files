---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carbide
offline_file: ""
offline_thumbnail: ""
uuid: cc33f70e-82ab-49f1-89a1-4d41c5bd8666
updated: 1484310170
title: carbide
categories:
    - Dictionary
---
carbide
     n : a binary compound of carbon with a more electropositive
         element
