---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jelly_fish
offline_file: ""
offline_thumbnail: ""
uuid: 8d679cf5-c5ac-407b-9bf6-6a4d8fafddd3
updated: 1484308371
title: Jellyfish
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Olindias_formosa1.jpg
tags:
    - Terminology
    - Anatomy
    - Nervous system
    - Vision
    - Size
    - Taxonomy
    - Life history and behavior
    - Phases
    - Reproduction
    - Lifespan
    - Movement
    - Ecology
    - Diet
    - Predation
    - Blooms
    - population
    - habitats
    - Parasites
    - Relation to humans
    - Fisheries
    - Products
    - Biotechnology
    - Aquaria
    - Toxicity
    - Treatment of stings
    - Hazards
categories:
    - Uncategorized
---
Jellyfish, or jellies,[1] are softbodied free-swimming aquatic animals with a gelatinous umbrella-shaped bell and trailing tentacles. The bell can pulsate to acquire locomotion, while the stinging tentacles can be utilized to capture prey by emitting toxins. Jellyfish species are classified in the subphylum Medusozoa which makes up a major part of the phylum Cnidaria, although not all Medusozoa species are considered to be jellyfish.
