---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cooperative_game_theory
offline_file: ""
offline_thumbnail: ""
uuid: 0ea0f989-298c-4bdc-99b2-3825121c832a
updated: 1484308531
title: Cooperative game theory
tags:
    - Mathematical definition
    - Duality
    - Subgames
    - Properties for characterization
    - Superadditivity
    - Monotonicity
    - Properties for simple games
    - Relation with non-cooperative theory
    - Solution concepts
    - The stable set
    - Properties
    - The core
    - Properties
    - The core of a simple game with respect to preferences
    - The strong epsilon-core
    - The Shapley value
    - The kernel
    - The nucleolus
    - Properties
    - Convex cooperative games
    - Properties
    - Similarities and differences with combinatorial optimization
categories:
    - Cooperative games
---
In game theory, a cooperative game (or coalitional game) is a game with competition between groups of players ("coalitions") due to the possibility of external enforcement of cooperative behavior (e.g. through contract law). Those are opposed to non-cooperative games in which there is either no possibility to forge alliances or all agreements need to be self-enforcing (e.g. through credible threats).[1]
