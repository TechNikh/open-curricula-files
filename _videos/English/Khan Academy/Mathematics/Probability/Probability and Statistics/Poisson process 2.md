---
version: 1
type: video
provider: YouTube
id: Jkr4FSrNEVY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Probability%20and%20Statistics/Poisson%20process%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b30745cf-4189-406e-b0f1-800f9804ae67
updated: 1486069688
title: Poisson process 2
categories:
    - Probability and Statistics
thumbnail_urls:
    - https://i3.ytimg.com/vi/Jkr4FSrNEVY/default.jpg
    - https://i3.ytimg.com/vi/Jkr4FSrNEVY/1.jpg
    - https://i3.ytimg.com/vi/Jkr4FSrNEVY/2.jpg
    - https://i3.ytimg.com/vi/Jkr4FSrNEVY/3.jpg
---
