---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ever
offline_file: ""
offline_thumbnail: ""
uuid: 1936e694-b869-439e-8c8e-8e2400feaae1
updated: 1484310363
title: ever
categories:
    - Dictionary
---
ever
     adv 1: at any time; "did you ever smoke?"; "the best con man of all
            time" [syn: {of all time}]
     2: at all times; all the time and on every occasion; "I will
        always be there to help you"; "always arrives on time";
        "there is always some pollution in the air"; "ever hoping
        to strike it rich"; "ever busy" [syn: {always}, {e'er}]
        [ant: {never}]
     3: (intensifier for adjectives) very; "she was ever so
        friendly" [syn: {ever so}]
