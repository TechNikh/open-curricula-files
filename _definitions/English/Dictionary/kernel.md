---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kernel
offline_file: ""
offline_thumbnail: ""
uuid: 2abf35ed-ddca-4b8b-a4d3-2debbff4450d
updated: 1484310403
title: kernel
categories:
    - Dictionary
---
kernel
     n 1: the inner and usually edible part of a seed or grain or nut
          or fruit stone; "black walnut kernels are difficult to
          get out of the shell" [syn: {meat}]
     2: a single whole grain of a cereal; "a kernel of corn"
     3: the choicest or most essential or most vital part of some
        idea or experience; "the gist of the prosecutor's
        argument"; "the heart and soul of the Republican Party";
        "the nub of the story" [syn: {substance}, {core}, {center},
         {essence}, {gist}, {heart}, {heart and soul}, {inwardness},
         {marrow}, {meat}, {nub}, {pith}, {sum}, {nitty-gritty}]
     [also: {kernelling}, {kernelled}]
