---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stirred
offline_file: ""
offline_thumbnail: ""
uuid: f2746521-2584-474b-8c00-84b79864128f
updated: 1484310411
title: stirred
categories:
    - Dictionary
---
stir
     n 1: a disorderly outburst or tumult; "they were amazed by the
          furious disturbance they had caused" [syn: {disturbance},
           {disruption}, {commotion}, {flutter}, {hurly burly}, {to-do},
           {hoo-ha}, {hoo-hah}, {kerfuffle}]
     2: emotional agitation and excitement
     3: a rapid bustling commotion [syn: {bustle}, {hustle}, {flurry},
         {ado}, {fuss}]
     v 1: move an implement through with a circular motion; "stir the
          soup"; "stir my drink"
     2: move very slightly; "He shifted in his seat" [syn: {shift},
        {budge}, {agitate}]
     3: stir feelings in; "stimulate my appetite"; "excite the
        audience"; "stir emotions" [syn: ...
