---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/valid
offline_file: ""
offline_thumbnail: ""
uuid: cb38b253-c5ba-4984-84dc-5adbe49903f4
updated: 1484310249
title: valid
categories:
    - Dictionary
---
valid
     adj 1: well grounded in logic or truth or having legal force; "a
            valid inference"; "a valid argument"; "a valid
            contract"; "a valid license" [ant: {invalid}]
     2: still legally acceptable; "the license is still valid"
