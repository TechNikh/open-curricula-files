---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harass
offline_file: ""
offline_thumbnail: ""
uuid: 757bd919-8ad2-4ff0-a795-ceb9224bc00f
updated: 1484310192
title: harass
categories:
    - Dictionary
---
harass
     v 1: annoy continually or chronically; "He is known to harry his
          staff when he is overworked"; "This man harasses his
          female co-workers" [syn: {hassle}, {harry}, {chivy}, {chivvy},
           {chevy}, {chevvy}, {beset}, {plague}, {molest}, {provoke}]
     2: exhaust by attacking repeatedly; "harass the enemy"
