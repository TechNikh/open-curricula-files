---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/loyalty
offline_file: ""
offline_thumbnail: ""
uuid: 400b81e2-f6ba-4a2f-ab36-0017b490e99c
updated: 1484310563
title: loyalty
categories:
    - Dictionary
---
loyalty
     n 1: the quality of being loyal [ant: {disloyalty}]
     2: feelings of allegiance
     3: the act of binding yourself (intellectually or emotionally)
        to a course of action; "his long commitment to public
        service"; "they felt no loyalty to a losing team" [syn: {commitment},
         {allegiance}, {dedication}]
