---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/banyan
offline_file: ""
offline_thumbnail: ""
uuid: 620cc944-c4ca-464d-b4f3-0d3d15564eb7
updated: 1484310266
title: banyan
categories:
    - Dictionary
---
banyan
     n 1: East Indian tree that puts out aerial shoots that grow down
          into the soil forming additional trunks [syn: {banyan
          tree}, {banian}, {banian tree}, {Indian banyan}, {East
          Indian fig tree}, {Ficus bengalensis}]
     2: a loose fitting jacket; originally worn in India [syn: {banian}]
