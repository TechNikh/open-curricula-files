---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insist
offline_file: ""
offline_thumbnail: ""
uuid: fe637dab-3b7c-4726-b042-4534a54d5b7f
updated: 1484310585
title: insist
categories:
    - Dictionary
---
insist
     v 1: be insistent and refuse to budge; "I must insist!" [syn: {take
          a firm stand}]
     2: beg persistently and urgently; "I importune you to help
        them" [syn: {importune}]
     3: assert to be true; "The letter asserts a free society" [syn:
         {assert}]
