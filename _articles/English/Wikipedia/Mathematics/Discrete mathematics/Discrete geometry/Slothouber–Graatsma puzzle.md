---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Slothouber%E2%80%93Graatsma_puzzle'
offline_file: ""
offline_thumbnail: ""
uuid: 85a0b691-3dfe-4d6a-ad89-c134ed6a9ae6
updated: 1484308608
title: Slothouber–Graatsma puzzle
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Slothouber-Graatsma_%2528solution%2529.PNG'
tags:
    - Solution
    - Variations
categories:
    - Discrete geometry
---
The Slothouber–Graatsma puzzle is a packing problem that calls for packing six 1 × 2 × 2 blocks and three 1 × 1 × 1 blocks into a 3 × 3 × 3 box. The solution to this puzzle is unique (up to mirror reflections and rotations).
