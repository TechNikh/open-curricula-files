---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clockwise
offline_file: ""
offline_thumbnail: ""
uuid: 703804e4-f860-44c3-890b-be2a07ea0f1e
updated: 1484309290
title: Clockwise
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Clockwise_arrow.svg_0.png
tags:
    - Origin of the term
    - Usage
    - Shop-work
    - Mathematics
    - Games and activities
    - In humans
categories:
    - Orientation (geometry)
---
Circular motion can occur in two possible directions. A clockwise (typically abbreviated as CW) motion is one that proceeds in the same direction as a clock's hands: from the top to the right, then down and then to the left, and back up to the top. The opposite sense of rotation or revolution is (in Commonwealth English) anticlockwise (ACW), or (in North American English) counterclockwise (CCW). In a mathematical sense, a circle defined parametrically in a positive Cartesian plane by the equations x = cos t and y = sin t is traced anticlockwise as t increases in value.
