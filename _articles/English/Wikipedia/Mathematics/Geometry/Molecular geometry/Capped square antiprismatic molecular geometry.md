---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Capped_square_antiprismatic_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 524e5109-4d75-4191-920e-0576c2d1bc8f
updated: 1484309275
title: Capped square antiprismatic molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nonahydridorhenate-3D-balls.png
categories:
    - Molecular geometry
---
In chemistry, the capped square antiprismatic molecular geometry describes the shape of compounds where nine atoms, groups of atoms, or ligands are arranged around a central atom, defining the vertices of a gyroelongated square pyramid.
