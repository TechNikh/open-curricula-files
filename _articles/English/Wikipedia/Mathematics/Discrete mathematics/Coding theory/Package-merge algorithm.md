---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Package-merge_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: eca62801-67b7-4368-99db-716e31a30d8c
updated: 1484308551
title: Package-merge algorithm
tags:
    - "The coin collector's problem"
    - Description of the package-merge algorithm
    - "Reduction of length-limited Huffman coding to the coin collector's problem"
    - Performance improvements and generalizations
categories:
    - Coding theory
---
The package-merge algorithm is an O(nL)-time algorithm for finding an optimal length-limited Huffman code for a given distribution on a given alphabet of size n, where no code word is longer than L. It is a greedy algorithm, and a generalization of Huffman's original algorithm. Package-merge works by reducing the code construction problem to the binary coin collector's problem.[1]
