---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/summing
offline_file: ""
offline_thumbnail: ""
uuid: e2d54240-1b3a-4b82-a950-299358ddbb46
updated: 1484310448
title: summing
categories:
    - Dictionary
---
sum
     n 1: a quantity of money; "he borrowed a large sum"; "the amount
          he had in cash was insufficient" [syn: {sum of money}, {amount},
           {amount of money}]
     2: a quantity obtained by addition [syn: {amount}, {total}]
     3: the final aggregate; "the sum of all our troubles did not
        equal the misery they suffered" [syn: {summation}, {sum
        total}]
     4: the choicest or most essential or most vital part of some
        idea or experience; "the gist of the prosecutor's
        argument"; "the heart and soul of the Republican Party";
        "the nub of the story" [syn: {kernel}, {substance}, {core},
         {center}, {essence}, {gist}, {heart}, ...
