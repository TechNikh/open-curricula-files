---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/domestic
offline_file: ""
offline_thumbnail: ""
uuid: cf878b92-67a7-4ee8-a717-7081d849d71b
updated: 1484310252
title: domestic
categories:
    - Dictionary
---
domestic
     adj 1: of concern to or concerning the internal affairs of a
            nation; "domestic issues such as tax rate and highway
            construction" [ant: {foreign}]
     2: of or relating to the home; "domestic servant"; "domestic
        science"
     3: of or involving the home or family; "domestic worries";
        "domestic happiness"; "they share the domestic chores";
        "everything sounded very peaceful and domestic"; "an
        author of blood-and-thunder novels yet quite domestic in
        his taste" [ant: {undomestic}]
     4: converted or adapted to domestic use; "domestic animals";
        "domesticated plants like maize" [syn: {domesticated}]
     5: ...
