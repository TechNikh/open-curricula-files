---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stability
offline_file: ""
offline_thumbnail: ""
uuid: d5c3e473-e117-4596-83ea-741029ccd9d8
updated: 1484310260
title: stability
categories:
    - Dictionary
---
stability
     n 1: the quality or attribute of being firm and steadfast [syn: {stableness}]
          [ant: {instability}, {instability}]
     2: a stable order [ant: {instability}]
     3: the quality of being free from change or variation [syn: {constancy}]
        [ant: {inconstancy}]
