---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Low_(computability)
offline_file: ""
offline_thumbnail: ""
uuid: ad1ff8c6-0089-4126-9fbb-826862094ad8
updated: 1484309487
title: Low (computability)
categories:
    - Computability theory
---
In computability theory, a Turing degree [X] is low if the Turing jump [X′] is 0′. A set is low if it has low degree. Since every set is computable from its jump, any low set is computable in 0′, but the jump of sets computable in 0′ can bound any degree r.e. in 0′ (Schoenfield Jump Inversion). X being low says that its jump X′ has the least possible degree in terms of Turing reducibility for the jump of a set.
