---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/knock
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484422621
title: knock
categories:
    - Dictionary
---
knock
     n 1: the sound of knocking (as on a door or in an engine or
          bearing); "the knocking grew louder" [syn: {knocking}]
     2: negative criticism [syn: {roast}]
     3: a vigorous blow; "the sudden knock floored him"; "he took a
        bash right in his face"; "he got a bang on the head" [syn:
         {bash}, {bang}, {smash}, {belt}]
     4: a bad experience; "the school of hard knocks"
     5: the act of hitting vigorously; "he gave the table a whack"
        [syn: {belt}, {rap}, {whack}, {whang}]
     v 1: deliver a sharp blow or push :"He knocked the glass clear
          across the room" [syn: {strike hard}]
     2: rap with the knuckles; "knock on the door"
     3: ...
