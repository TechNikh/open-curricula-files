---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Jean-Fran%C3%A7ois_Mertens'
offline_file: ""
offline_thumbnail: ""
uuid: 2c4ccd7f-13a8-4ace-884d-596f74f36f5b
updated: 1484308509
title: Jean-François Mertens
tags:
    - Epistemic models
    - Repeated games with incomplete information
    - Stochastic games
    - 'Market games: limit price mechanism'
    - Shapley value
    - Refinements and Mertens-stable equilibria
    - Social choice theory and relative utilitarianism
    - Intergenerational equity in policy evaluation
categories:
    - Game theorists
---
Jean-François Mertens made some contributions to probability theory[2] and published articles on elementary topology,[3][4] but he was mostly active in economic theory. In particular, he contributed to order-book of market games, cooperative games, noncooperative games, repeated games, epistemic models of strategic behavior, and refinements of Nash equilibrium (see solution concept).
