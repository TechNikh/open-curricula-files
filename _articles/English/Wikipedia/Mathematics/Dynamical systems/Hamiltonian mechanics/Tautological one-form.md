---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tautological_one-form
offline_file: ""
offline_thumbnail: ""
uuid: b9252d3f-c2cb-4406-828a-2a9fd4b3d10f
updated: 1484308797
title: Tautological one-form
tags:
    - Coordinate-free definition
    - Properties
    - Action
    - On metric spaces
categories:
    - Hamiltonian mechanics
---
In mathematics, the tautological one-form is a special 1-form defined on the cotangent bundle T*Q of a manifold Q. The exterior derivative of this form defines a symplectic form giving T*Q the structure of a symplectic manifold. The tautological one-form plays an important role in relating the formalism of Hamiltonian mechanics and Lagrangian mechanics. The tautological one-form is sometimes also called the Liouville one-form, the Poincaré one-form, the canonical one-form, or the symplectic potential. A similar object is the canonical vector field on the tangent bundle. In algebraic geometry and complex geometry the term "canonical" is discouraged, due to confusion with the canonical ...
