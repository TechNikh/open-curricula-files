---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/harden
offline_file: ""
offline_thumbnail: ""
uuid: f7f3212c-aeac-4c34-a97b-169d3fa969fe
updated: 1484310283
title: harden
categories:
    - Dictionary
---
harden
     v 1: become hard or harder; "The wax hardened" [syn: {indurate}]
          [ant: {soften}]
     2: make hard or harder; "The cold hardened the butter" [syn: {indurate}]
        [ant: {soften}]
     3: harden by reheating and cooling in oil; "temper steel" [syn:
         {temper}]
     4: make fit; "This trip will season even the hardiest
        traveller" [syn: {season}]
     5: cause to accept or become hardened to; habituate; "He was
        inured to the cold" [syn: {inure}, {indurate}]
