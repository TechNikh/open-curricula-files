---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Spacetime_triangle_diagram_technique
offline_file: ""
offline_thumbnail: ""
uuid: 5c727d25-d596-48e6-abe1-5f3e7129cb94
updated: 1484308757
title: Spacetime triangle diagram technique
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/750px-Earth-moon.jpg
tags:
    - Basic stages
    - "STTD versus Green's function technique"
    - Drawbacks of the method
    - Most important concretizations
    - General considerations
    - Cartesian and cylindrical coordinates
    - Spherical coordinates
    - "Equivalence of the STTD (Riemann) and Green's function solutions"
    - References and notes
categories:
    - Electrodynamics
---
In physics and mathematics, the spacetime triangle diagram (STTD) technique, also known as the Smirnov method of incomplete separation of variables, is the direct space-time domain method for electromagnetic and scalar wave motion.
