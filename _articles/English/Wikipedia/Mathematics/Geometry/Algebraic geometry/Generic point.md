---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generic_point
offline_file: ""
offline_thumbnail: ""
uuid: 00cf8119-bee1-49a8-b4dd-fcc379954801
updated: 1484309149
title: Generic point
tags:
    - Definition and motivation
    - Examples
    - History
categories:
    - Algebraic geometry
---
In algebraic geometry, a generic point P of an algebraic variety X is, roughly speaking, a point at which all generic properties are true, a generic property being a property which is true for almost every point.
