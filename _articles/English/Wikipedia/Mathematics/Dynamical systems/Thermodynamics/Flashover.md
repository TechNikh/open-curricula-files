---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flashover
offline_file: ""
offline_thumbnail: ""
uuid: 2f00ba85-e6ce-48c9-9c36-4549b35c268b
updated: 1484308846
title: Flashover
tags:
    - Types
    - Dangers
    - Indicators
categories:
    - Thermodynamics
---
A flashover is the near-simultaneous ignition of most of the directly exposed combustible material in an enclosed area. When certain organic materials are heated, they undergo thermal decomposition and release flammable gases. Flashover occurs when the majority of the exposed surfaces in a space are heated to their autoignition temperature and emit flammable gases (see also flash point). Flashover normally occurs at 500 °C (932 °F) or 590 °C (1,100 °F) for ordinary combustibles, and an incident heat flux at floor level of 20 kilowatts per square metre (2.5 hp/sq ft).
