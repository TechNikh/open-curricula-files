---
title: Population Growth
youtube_id: b98JmQ0Cc3k
tags:
    - Environment
    - Environmental
    - population
    - growth
    - density
    - people
    - Earth
    - motion graphics
    - mograph
    - after effects
    - animated
    - Climate change
    - Infographic
    - Statistics
    - educational
    - mindtv
    - mind tv
    - sustainability
    - typography
    - kinetic
    - Earth Day
    - MiND
    - hot topics
categories:
    - Bamboo
---
