---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Formal_scheme
offline_file: ""
offline_thumbnail: ""
uuid: 380429dc-cd57-46db-80e3-b4255b3b9574
updated: 1484309147
title: Formal scheme
tags:
    - Definition
    - Morphisms between formal schemes
categories:
    - Algebraic geometry
---
In mathematics, specifically in algebraic geometry, a formal scheme is a type of space which includes data about its surroundings. Unlike an ordinary scheme, a formal scheme includes infinitesimal data that, in effect, points in a direction off of the scheme. For this reason, formal schemes frequently appear in topics such as deformation theory. But the concept is also used to prove a theorem such as the theorem on formal functions, which is used to deduce theorems of interest for usual schemes.
