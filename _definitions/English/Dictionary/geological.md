---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geological
offline_file: ""
offline_thumbnail: ""
uuid: 56058a20-3692-48b3-a50e-c04ad561ed7d
updated: 1484310289
title: geological
categories:
    - Dictionary
---
geological
     adj : of or relating to or based on geology; "geological
           formations"; "geologic forces" [syn: {geologic}]
