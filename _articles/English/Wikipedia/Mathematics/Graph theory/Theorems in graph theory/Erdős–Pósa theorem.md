---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93P%C3%B3sa_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 1d274c8d-7fd6-46ba-ad71-8103fe9f2f6e
updated: 1484309457
title: Erdős–Pósa theorem
categories:
    - Theorems in graph theory
---
In the mathematical discipline of graph theory, the Erdős–Pósa theorem, named after Paul Erdős and Lajos Pósa, states that there is a function f(k) such that for each positive integer k, every graph either contains k vertex-disjoint circuits or it has a feedback vertex set of f(k) vertices that intersects every circuit. Furthermore, f(k) = O(k log k) in the sense of Big O notation. Because of this theorem, circuits are said to have the Erdős–Pósa property.
