---
version: 1
type: video
provider: YouTube
id: hHUhR8dKW5o
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f330329c-ea10-47cc-90a5-9b885931d653
updated: 1486069626
title: Visarga Sandhi
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/hHUhR8dKW5o/default.jpg
    - https://i3.ytimg.com/vi/hHUhR8dKW5o/1.jpg
    - https://i3.ytimg.com/vi/hHUhR8dKW5o/2.jpg
    - https://i3.ytimg.com/vi/hHUhR8dKW5o/3.jpg
---
