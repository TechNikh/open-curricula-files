---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mri
offline_file: ""
offline_thumbnail: ""
uuid: 48db387b-febc-419a-8bd0-71e0d96011ce
updated: 1484310479
title: mri
categories:
    - Dictionary
---
MRI
     n : the use of nuclear magnetic resonance of protons to produce
         proton density images [syn: {magnetic resonance imaging}]
