---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/correlate
offline_file: ""
offline_thumbnail: ""
uuid: 604d6ec2-bfcf-48db-8ff9-591c2511d454
updated: 1484310413
title: correlate
categories:
    - Dictionary
---
correlate
     adj : mutually related [syn: {correlative}, {correlated}]
     n : either of two correlated variables [syn: {correlative}]
     v 1: to bear a reciprocal or mutual relation; "Do these facts
          correlate?"
     2: bring into a mutual, complementary, or reciprocal relation;
        "I cannot correlate these two pieces of information"
