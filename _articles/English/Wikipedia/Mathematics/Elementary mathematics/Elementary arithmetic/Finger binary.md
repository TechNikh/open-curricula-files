---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Finger_binary
offline_file: ""
offline_thumbnail: ""
uuid: 78f6095c-3879-4140-ac01-0c15577f866f
updated: 1484308921
title: Finger binary
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Chinesische.Zahl.Acht.jpg
tags:
    - Mechanics
    - Examples
    - Right hand
    - Left hand
    - Negative numbers and non-integers
    - Negative numbers
    - Fractions
    - Dyadic fractions
    - Rational numbers
    - Decimal fractions and vulgar fractions
    - Finger ternary
categories:
    - Elementary arithmetic
---
Finger binary is a system for counting and displaying binary numbers on the fingers of one or more hands. It is possible to count from 0 to 31 (25−1) using the fingers of a single hand, or from 0 through 1023 (210−1) if both hands are used.
