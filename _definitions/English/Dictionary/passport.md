---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/passport
offline_file: ""
offline_thumbnail: ""
uuid: 323e16bf-ac18-470e-845a-dd6f6d2ee8d4
updated: 1484310517
title: passport
categories:
    - Dictionary
---
passport
     n 1: any authorization to pass or go somewhere; "the pass to
          visit had a strict time limit" [syn: {pass}]
     2: a document issued by a country to a citizen allowing that
        person to travel abroad and re-enter the home country
     3: any quality or characteristic that gains a person a
        favorable reception or acceptance or admission; "her
        pleasant personality is already a recommendation"; "his
        wealth was not a passport into the exclusive circles of
        society" [syn: {recommendation}]
