---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/penance
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484639521
title: penance
categories:
    - Dictionary
---
penance
     n 1: remorse for your past conduct [syn: {repentance}, {penitence}]
     2: a Catholic sacrament; repentance and confession and
        satisfaction and absolution
     3: voluntary self-punishment in order to atone for some
        wrongdoing [syn: {self-mortification}, {self-abasement}]
