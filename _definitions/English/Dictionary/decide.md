---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decide
offline_file: ""
offline_thumbnail: ""
uuid: 8f1362ba-e5a1-44db-9c3d-b083a8b32e7c
updated: 1484310220
title: decide
categories:
    - Dictionary
---
decide
     v 1: reach, make, or come to a decision about something; "We
          finally decided after lengthy deliberations" [syn: {make
          up one's mind}, {determine}]
     2: bring to an end; settle conclusively; "The case was
        decided"; "The judge decided the case in favor of the
        plaintiff"; "The father adjudicated when the sons were
        quarreling over their inheritance" [syn: {settle}, {resolve},
         {adjudicate}]
     3: cause to decide; "This new development finally decided me!"
     4: influence or determine; "The vote in New Hampshire often
        decides the outcome of the Presidential election"
