---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetric_game
offline_file: ""
offline_thumbnail: ""
uuid: 12670d43-3342-4be4-a7ef-7d1e7ac99e59
updated: 1484309040
title: Symmetric game
tags:
    - Symmetry in 2x2 games
    - Symmetry and equilibria
    - 'Uncorrelated asymmetries: payoff neutral asymmetries'
    - The general case
categories:
    - Game theory game classes
---
In game theory, a symmetric game is a game where the payoffs for playing a particular strategy depend only on the other strategies employed, not on who is playing them. If one can change the identities of the players without changing the payoff to the strategies, then a game is symmetric. Symmetry can come in different varieties. Ordinally symmetric games are games that are symmetric with respect to the ordinal structure of the payoffs. A game is quantitatively symmetric if and only if it is symmetric with respect to the exact payoffs.
