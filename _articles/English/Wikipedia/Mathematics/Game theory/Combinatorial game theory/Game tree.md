---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Game_tree
offline_file: ""
offline_thumbnail: ""
uuid: 82a8664c-a28d-4084-aaee-6f6f3cd0ad3a
updated: 1484308535
title: Game tree
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tic-tac-toe-game-tree.svg.png
tags:
    - Solving game trees
    - Deterministic Algorithm Version
    - Randomized Algorithms Version
categories:
    - Combinatorial game theory
---
In game theory, a game tree is a directed graph whose nodes are positions in a game and whose edges are moves. The complete game tree for a game is the game tree starting at the initial position and containing all possible moves from each position; the complete tree is the same tree as that obtained from the extensive-form game representation.
