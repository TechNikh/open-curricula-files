---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unemotional
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484461861
title: unemotional
categories:
    - Dictionary
---
unemotional
     adj 1: unsusceptible to or destitute of or showing no emotion [ant:
             {emotional}]
     2: cool and formal in manner [syn: {reserved}, {restrained}, {reticent}]
