---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cardamom
offline_file: ""
offline_thumbnail: ""
uuid: 2961f5ef-673a-473c-9e6e-224dbca98fbd
updated: 1484308422
title: Cardamom
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cardamom_Seeds_BNC.jpg
tags:
    - Etymology
    - Types and distribution
    - Uses
    - Food and beverage
    - Composition
    - World production
    - Photo gallery
    - Bibliography
categories:
    - Uncategorized
---
Cardamom (/ˈkɑːrdəməm/), sometimes Cardamon or Cardamum,[1] is a spice made from the seeds of several plants in the genera Elettaria and Amomum in the family Zingiberaceae. Both genera are native to India (the largest producer until the late 20th century), Pakistan, Bangladesh, Bhutan, Indonesia and Nepal. They are recognised by their small seed pods: triangular in cross-section and spindle-shaped, with a thin papery outer shell and small black seeds; Elettaria pods are light green and smaller, while Amomum pods are larger and dark brown.
