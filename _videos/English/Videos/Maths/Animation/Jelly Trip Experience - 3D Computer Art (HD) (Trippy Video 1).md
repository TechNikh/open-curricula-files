---
version: 1
type: video
provider: YouTube
id: btVGCqpIoyk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 461c88b3-183a-4c33-98e2-59b134dacd17
updated: 1486069649
title: 'Jelly Trip Experience - 3D Computer Art (HD) (Trippy Video 1)'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/btVGCqpIoyk/default.jpg
    - https://i3.ytimg.com/vi/btVGCqpIoyk/1.jpg
    - https://i3.ytimg.com/vi/btVGCqpIoyk/2.jpg
    - https://i3.ytimg.com/vi/btVGCqpIoyk/3.jpg
---
