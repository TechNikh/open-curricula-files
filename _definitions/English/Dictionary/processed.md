---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/processed
offline_file: ""
offline_thumbnail: ""
uuid: d5938a61-1bff-4306-a794-a54a4beb9bed
updated: 1484310268
title: processed
categories:
    - Dictionary
---
processed
     adj 1: subjected to a special process or treatment; "prepared
            ergot"; "processed cheeses are easy to spread"
     2: freed from impurities by processing; "refined sugar";
        "refined oil"; "to gild refined gold"- Shakespeare [syn: {refined}]
        [ant: {unrefined}]
     3: prepared or converted from a natural state by subjecting to
        a special process; "processed ores" [ant: {unprocessed}]
