---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leading
offline_file: ""
offline_thumbnail: ""
uuid: 172a92bb-bc92-4976-801b-46beb124aeee
updated: 1484310279
title: leading
categories:
    - Dictionary
---
leading
     adj 1: indicating the most important performer or role; "the
            leading man"; "prima ballerina"; "prima donna"; "a
            star figure skater"; "the starring role"; "a stellar
            role"; "a stellar performance" [syn: {leading(p)}, {prima(p)},
             {star(p)}, {starring(p)}, {stellar(a)}]
     2: going or proceeding or going in advance; showing the way;
        "we rode in the leading car"; "the leading edge of
        technology" [ant: {following}]
     3: greatest in importance or degree or significance or
        achievement; "our greatest statesmen"; "the country's
        leading poet"; "a preeminent archeologist" [syn: {greatest},
         ...
