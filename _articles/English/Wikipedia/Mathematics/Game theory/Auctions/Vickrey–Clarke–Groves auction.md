---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Vickrey%E2%80%93Clarke%E2%80%93Groves_auction'
offline_file: ""
offline_thumbnail: ""
uuid: c2956af4-4564-4796-9d9b-9ca48a56dff6
updated: 1484309013
title: Vickrey–Clarke–Groves auction
tags:
    - Intuitive description
    - Formal description
    - Examples
    - 'Example #1'
    - 'Example #2'
    - 'Example #3'
    - Optimality of Truthful Bidding
categories:
    - Auctions
---
In auction theory, a Vickrey–Clarke–Groves (VCG) auction is a type of sealed-bid auction of multiple items. Bidders submit bids that report their valuations for the items, without knowing the bids of the other people in the auction. The auction system assigns the items in a socially optimal manner: it charges each individual the harm they cause to other bidders.[1] It also gives bidders an incentive to bid their true valuations, by ensuring that the optimal strategy for each bidder is to bid their true valuations of the items. It is a generalization of a Vickrey auction for multiple items.
