---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stretch
offline_file: ""
offline_thumbnail: ""
uuid: 70179327-dec9-47d5-8f02-318946242e11
updated: 1484310150
title: stretch
categories:
    - Dictionary
---
stretch
     adj 1: having an elongated seating area; "a stretch limousine"
            [syn: {stretch(a)}]
     2: easily stretched; "stretch hosiery"
     n 1: a large and unbroken expanse or distance; "a stretch of
          highway"; "a stretch of clear water"
     2: the act of physically reaching or thrusting out [syn: {reach},
         {reaching}]
     3: a straightaway section of a racetrack
     4: exercise designed to extend the limbs and muscles to their
        full extent [syn: {stretching}]
     5: extension to or beyond the ordinary limit; "running at full
        stretch"; "by no stretch of the imagination"; "beyond any
        stretch of his understanding"
     6: an ...
