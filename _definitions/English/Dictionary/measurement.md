---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/measurement
offline_file: ""
offline_thumbnail: ""
uuid: 6767eb83-438d-4a29-9369-c2901d09b0ed
updated: 1484310200
title: measurement
categories:
    - Dictionary
---
measurement
     n : the act or process of measuring; "the measurements were
         carefully done"; "his mental measurings proved remarkably
         accurate" [syn: {measuring}, {measure}, {mensuration}]
