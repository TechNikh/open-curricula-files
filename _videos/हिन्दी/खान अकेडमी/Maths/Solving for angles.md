---
version: 1
type: video
provider: YouTube
id: WZ5ODUK56Zw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4797a8f3-b744-4a9b-928b-cbdd356ab287
updated: 1486069614
title: Solving for angles
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/WZ5ODUK56Zw/default.jpg
    - https://i3.ytimg.com/vi/WZ5ODUK56Zw/1.jpg
    - https://i3.ytimg.com/vi/WZ5ODUK56Zw/2.jpg
    - https://i3.ytimg.com/vi/WZ5ODUK56Zw/3.jpg
---
