---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/torso
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484406601
title: torso
categories:
    - Dictionary
---
torso
     n : the body excluding the head and neck and limbs; "they moved
         their arms and legs and bodies" [syn: {trunk}, {body}]
     [also: {torsi} (pl)]
