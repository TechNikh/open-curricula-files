---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Assortativity
offline_file: ""
offline_thumbnail: ""
uuid: bbdb9315-6328-445d-aa96-1dfce7d8a9b2
updated: 1484309432
title: Assortativity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Scale-free_networks_for_different_degrees_of_assortativity.jpg
tags:
    - Measuring assortativity
    - Assortativity coefficient
    - Neighbor connectivity
    - 'Local Assortativity[8]'
    - Assortative mixing patterns of real networks
    - Application
    - Structural disassortativity
categories:
    - Network theory
---
Assortativity, or assortative mixing is a preference for a network's nodes to attach to others that are similar in some way. Though the specific measure of similarity may vary, network theorists often examine assortativity in terms of a node's degree.[1] The addition of this characteristic to network models more closely approximates the behaviors of many real world networks.
