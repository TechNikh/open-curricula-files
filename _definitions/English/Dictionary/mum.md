---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mum
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484409181
title: mum
categories:
    - Dictionary
---
mum
     adj : failing to speak or communicate etc when expected to; "the
           witness remained silent" [syn: {silent}]
     n 1: of China [syn: {florist's chrysanthemum}, {florists'
          chrysanthemum}, {Dendranthema grandifloruom}, {Chrysanthemum
          morifolium}]
     2: informal terms for a mother [syn: {ma}, {mama}, {mamma}, {mom},
         {momma}, {mommy}, {mammy}, {mummy}, {mater}]
     3: secrecy; "mum's the word"
     [also: {mumming}, {mummed}]
