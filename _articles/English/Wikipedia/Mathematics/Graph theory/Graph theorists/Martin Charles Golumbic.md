---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Martin_Charles_Golumbic
offline_file: ""
offline_thumbnail: ""
uuid: eb732087-2737-4f45-a018-53f337c018d5
updated: 1484309463
title: Martin Charles Golumbic
tags:
    - Biography
    - Scientific Contributions
    - Honors and awards
    - Bibliography
categories:
    - Graph theorists
---
Martin Charles Golumbic (born September 30, 1948) is a mathematician and computer scientist, best known for his work in algorithmic graph theory and in artificial intelligence. He is the founding editor-in-chief of the journal Annals of Mathematics and Artificial Intelligence.[citation needed]
