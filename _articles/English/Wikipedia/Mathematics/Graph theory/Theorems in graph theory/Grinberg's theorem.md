---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Grinberg%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 20dc02a2-7306-408e-b19c-465a72acf115
updated: 1484309457
title: "Grinberg's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Grinberg_5CEC_Nonhamiltonian_graph.svg.png
tags:
    - Formulation
    - Applications
    - Limitations
categories:
    - Theorems in graph theory
---
In graph theory, Grinberg's theorem is a necessary condition for a planar graph to contain a Hamiltonian cycle, based on the lengths of its face cycles. The result has been widely used to construct non-Hamiltonian planar graphs with further properties, such as to give new counterexamples to Tait's conjecture (originally disproved by W.T. Tutte in 1946). This theorem was proved by Latvian mathematician Emanuel Grinberg in 1968.
