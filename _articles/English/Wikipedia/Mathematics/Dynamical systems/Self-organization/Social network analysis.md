---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Social_network_analysis
offline_file: ""
offline_thumbnail: ""
uuid: e79eee99-2de5-46aa-b965-8dd2a6bd3e29
updated: 1484308825
title: Social network analysis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kencf0618FacebookNetwork.jpg
tags:
    - History
    - Metrics
    - Connections
    - Distributions
    - Segmentation
    - Modelling and visualization of networks
    - Social networking potential
    - Practical applications
    - Organizations
    - Peer-reviewed journals
    - Textbooks and educational resources
    - Data sets
categories:
    - Self-organization
---
Social network analysis (SNA) is the process of investigating social structures through the use of network and graph theories.[1] It characterizes networked structures in terms of nodes (individual actors, people, or things within the network) and the ties, edges, or links (relationships or interactions) that connect them. Examples of social structures commonly visualized through social network analysis include social media networks,[2] friendship and acquaintance networks, collaboration graphs, kinship, disease transmission, and sexual relationships.[3][4] These networks are often visualized through sociograms in which nodes are represented as points and ties are represented as lines.
