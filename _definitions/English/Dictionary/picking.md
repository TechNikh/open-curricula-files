---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/picking
offline_file: ""
offline_thumbnail: ""
uuid: 038c4b3e-c299-4ce8-a197-abd95c988688
updated: 1484310349
title: picking
categories:
    - Dictionary
---
picking
     n 1: the quantity of a crop that is harvested; "he sent the first
          picking of berries to the market"; "it was the biggest
          peach pick in years" [syn: {pick}]
     2: the act of picking (crops or fruit or hops etc.)
