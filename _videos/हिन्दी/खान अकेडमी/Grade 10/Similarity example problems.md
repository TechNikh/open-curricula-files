---
version: 1
type: video
provider: YouTube
id: WqAFH1EiXso
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 097bf2d7-bc7b-4c3e-93aa-ddc02886912b
updated: 1486069619
title: Similarity example problems
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/WqAFH1EiXso/default.jpg
    - https://i3.ytimg.com/vi/WqAFH1EiXso/1.jpg
    - https://i3.ytimg.com/vi/WqAFH1EiXso/2.jpg
    - https://i3.ytimg.com/vi/WqAFH1EiXso/3.jpg
---
