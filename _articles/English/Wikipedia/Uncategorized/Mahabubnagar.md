---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mahabubnagar
offline_file: ""
offline_thumbnail: ""
uuid: d98b08f0-ccc7-4e46-ac48-d781636f6e80
updated: 1484308444
title: Mahabubnagar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mahabubnagar_Railway_Station.JPG
tags:
    - Etymology
    - geography
    - Demographics
    - Governance
    - Transport
    - Notable people
    - Gallery
categories:
    - Uncategorized
---
Mahabubnagar is a city in Mahabubnagar district of the Indian state of Telangana. It is the headquarters of Mahabubnagar mandal in Mahabubnagar revenue division.[3] The city is also the largest in the district with an area of 98.64 km2 (38.09 sq mi) and 7th most populous in the state.[4]
