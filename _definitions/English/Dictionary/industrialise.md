---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/industrialise
offline_file: ""
offline_thumbnail: ""
uuid: a9dbefa4-a299-4196-a212-b2bec34b03f6
updated: 1484310573
title: industrialise
categories:
    - Dictionary
---
industrialise
     v 1: organize the production of into an industry; "The Chinese
          industrialized textile production" [syn: {industrialize}]
     2: develop industry; become industrial; "The nations of South
        East Asia will quickly industrialize and catch up with the
        West" [syn: {industrialize}]
