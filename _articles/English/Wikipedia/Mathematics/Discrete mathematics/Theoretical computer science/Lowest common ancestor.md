---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lowest_common_ancestor
offline_file: ""
offline_thumbnail: ""
uuid: c60bfee1-30f4-4bb2-93a5-5cf9fee116cb
updated: 1484308678
title: Lowest common ancestor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/140px-Lowest_common_ancestor.svg.png
tags:
    - History
    - Extension to directed acyclic graphs
    - Applications
categories:
    - Theoretical computer science
---
In graph theory and computer science, the lowest common ancestor (LCA) of two nodes v and w in a tree or directed acyclic graph (DAG) T is the lowest (i.e. deepest) node that has both v and w as descendants, where we define each node to be a descendant of itself (so if v has a direct connection from w, w is the lowest common ancestor).
