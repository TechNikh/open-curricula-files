---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Steven_Kerckhoff
offline_file: ""
offline_thumbnail: ""
uuid: e92a1f79-b85e-4f33-855c-fcff713450fc
updated: 1484309088
title: Steven Kerckhoff
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Steven_Kerckhoff_Stanford_September_2010.jpg
categories:
    - Geometers
---
He received his Ph.D. in mathematics from Princeton University in 1978, under the direction of William Thurston. Among his most famous results is his resolution of the Nielsen realization problem, a 1932 conjecture by Jakob Nielsen. Along with William J. Floyd, he wrote large parts of Thurston's influential Princeton lecture notes, and he is well known for his work (some of which is joint with Craig Hodgson) in exploring and clarifying Thurston's hyperbolic Dehn surgery.
