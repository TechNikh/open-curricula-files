---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lichen
offline_file: ""
offline_thumbnail: ""
uuid: 74bd1998-a318-4d42-aa25-049162a7f87c
updated: 1484310379
title: lichen
categories:
    - Dictionary
---
lichen
     n 1: any of several eruptive skin diseases characterized by hard
          thick lesions grouped together and resembling lichens
          growing on rocks
     2: any thallophytic plant of the division Lichenes; occur as
        crusty patches or bushy growths on tree trunks or rocks or
        bare ground etc.
