---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Toothpick_sequence
offline_file: ""
offline_thumbnail: ""
uuid: c04e6daa-5593-4b92-b8b2-f0d0a649b2fa
updated: 1484308595
title: Toothpick sequence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Margolus_toothpick_animated.gif
categories:
    - Combinatorics
---
In geometry, the toothpick sequence is a sequence of 2-dimensional patterns which can be formed by repeatedly adding line segments ("toothpicks") to the previous pattern in the sequence.
