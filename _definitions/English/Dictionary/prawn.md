---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prawn
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484587201
title: prawn
categories:
    - Dictionary
---
prawn
     n 1: any of various edible decapod crustaceans [syn: {shrimp}]
     2: shrimp-like decapod crustacean having two pairs of pincers;
        most are edible
     v : fish for prawns
