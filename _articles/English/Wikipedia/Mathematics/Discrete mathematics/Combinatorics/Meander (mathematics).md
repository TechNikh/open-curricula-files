---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Meander_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 98ae5fa1-b17b-4d03-aad6-5d300f604087
updated: 1484308585
title: Meander (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Meanderperm.svg.png
tags:
    - Meander
    - Examples
    - Meandric numbers
    - Meandric permutations
    - Open meander
    - Examples
    - Open meandric numbers
    - Semi-meander
    - Examples
    - Semi-meandric numbers
    - Properties of meandric numbers
categories:
    - Combinatorics
---
In mathematics, a meander or closed meander is a self-avoiding closed curve which intersects a line a number of times. Intuitively, a meander can be viewed as a road crossing a river through a number of bridges.
