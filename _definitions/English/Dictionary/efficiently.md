---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/efficiently
offline_file: ""
offline_thumbnail: ""
uuid: ff25fe89-a9fa-45d8-97c8-5f108ba29b69
updated: 1484310289
title: efficiently
categories:
    - Dictionary
---
efficiently
     adv : in an efficient manner; "he functions efficiently" [syn: {expeditiously},
            {with efficiency}] [ant: {inefficiently}]
