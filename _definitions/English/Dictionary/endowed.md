---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/endowed
offline_file: ""
offline_thumbnail: ""
uuid: 428c9a78-c9e9-4354-bcfe-27583330e055
updated: 1484310597
title: endowed
categories:
    - Dictionary
---
endowed
     adj : provided or supplied or equipped with (especially as by
           inheritance or nature); "an well-endowed college";
           "endowed with good eyesight"; "endowed by their Creator
           with certain unalienable rights" [ant: {unendowed}]
