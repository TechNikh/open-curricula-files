---
version: 1
type: video
provider: YouTube
id: TFlVWf8JX4A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bbca7749-b08d-4eba-a20e-651ffa354c80
updated: 1486069682
title: 'Electric Charge- Crash Course Physics #25'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/TFlVWf8JX4A/default.jpg
    - https://i3.ytimg.com/vi/TFlVWf8JX4A/1.jpg
    - https://i3.ytimg.com/vi/TFlVWf8JX4A/2.jpg
    - https://i3.ytimg.com/vi/TFlVWf8JX4A/3.jpg
---
