---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exceeding
offline_file: ""
offline_thumbnail: ""
uuid: 8ba5d711-fd9e-44f3-852e-5dd3a3f4dd84
updated: 1484310527
title: exceeding
categories:
    - Dictionary
---
exceeding
     adj : far beyond what is usual in magnitude or degree; "a night of
           exceeding darkness"; "an exceptional memory"; "olympian
           efforts to save the city from bankruptcy"; "the young
           Mozart's prodigious talents" [syn: {exceptional}, {olympian},
            {prodigious}, {surpassing}]
