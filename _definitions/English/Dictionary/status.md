---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/status
offline_file: ""
offline_thumbnail: ""
uuid: 44556eb9-508f-4414-88f8-3aeeb3aba435
updated: 1484310262
title: status
categories:
    - Dictionary
---
status
     n 1: the relative position or standing of things or especially
          persons in a society; "he had the status of a minor";
          "the novel attained the status of a classic"; "atheists
          do not enjoy a favorable position in American life"
          [syn: {position}]
     2: a state at a particular time; "a condition (or state) of
        disrepair"; "the current status of the arms negotiations"
        [syn: {condition}]
