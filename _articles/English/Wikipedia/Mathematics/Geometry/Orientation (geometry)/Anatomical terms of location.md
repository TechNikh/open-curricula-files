---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anatomical_terms_of_location
offline_file: ""
offline_thumbnail: ""
uuid: a3312631-f226-43c5-94df-563ad1404057
updated: 1484309290
title: Anatomical terms of location
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-1303_Human_Neuroaxis.jpg
tags:
    - Introduction
    - Standard anatomical position
    - Combined terms
    - Planes
    - Axes
    - Main terms
    - Superior and inferior
    - Anterior and posterior
    - Medial and lateral
    - Proximal and distal
    - 'Chemical & molecular physics'
    - Central and peripheral
    - Superficial and deep
    - Dorsal and ventral
    - Cranial and caudal
    - Other terms and special cases
    - Anatomical landmarks
    - Mouth and teeth
    - Hands and feet
    - Rotational direction
    - Other directional terms
    - Specific animals and other organisms
    - Humans
    - Asymmetrical and spherical organisms
    - Elongated organisms
    - Radially symmetrical organisms
    - Spiders
    - Citations
    - Sources
categories:
    - Orientation (geometry)
---
All vertebrates (including humans) have the same basic body plan –  they are strictly bilaterally symmetrical in early embryonic stages and largely bilaterally symmetrical in adulthood.[1] That is, they have mirror-image left and right halves if divided down the centre.[2] For these reasons, the basic directional terms can be considered to be those used in vertebrates. By extension, the same terms are used for many other (invertebrate) organisms as well.
