---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Afghanistan
offline_file: ""
offline_thumbnail: ""
uuid: 59f5ec4f-3ea5-43ec-8a5f-380dcc9a143e
updated: 1484308347
title: Afghanistan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-BamyanBuddha_Smaller_1.jpg
tags:
    - Etymology
    - History
    - Pre-Islamic period
    - Islamization and Mongol invasion
    - Hotak dynasty and Durrani Empire
    - Western influence
    - Marxist revolution and Soviet war
    - Civil war
    - Taliban Emirate and Northern Alliance
    - Recent history (2002–present)
    - geography
    - Demographics
    - Ethnic groups
    - Languages
    - Gender
    - Religions
    - Governance
    - Elections and parties
    - Administrative divisions
    - Foreign relations and military
    - Law enforcement
    - Economy
    - mining
    - Transport
    - air
    - Rail
    - Roads
    - Communication
    - Health
    - Education
    - culture
    - Media and entertainment
    - Sports
    - Notes
categories:
    - Uncategorized
---
Afghanistan i/æfˈɡænᵻstæn/ (Pashto/Dari: افغانستان, Afġānistān), officially the Islamic Republic of Afghanistan, is a landlocked country located within South Asia and Central Asia.[9][10] It has a population of approximately 32 million, making it the 42nd most populous country in the world. It is bordered by Pakistan in the south and east; Iran in the west; Turkmenistan, Uzbekistan, and Tajikistan in the north; and China in the far northeast. Its territory covers 652,000 km2 (252,000 sq mi), making it the 41st largest country in the world.
