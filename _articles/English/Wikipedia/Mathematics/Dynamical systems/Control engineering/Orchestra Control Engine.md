---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Orchestra_Control_Engine
offline_file: ""
offline_thumbnail: ""
uuid: 9a92fa97-fc09-45b7-8002-ace78836afc6
updated: 1484308738
title: Orchestra Control Engine
tags:
    - Main features
    - Suite components
    - Solutions
    - Release history
categories:
    - Control engineering
---
Orchestra Control Engine is a suite of software components (based on Linux/RTAI) used for the planning, development and deployment of real-time control applications for industrial machines and robots.
