---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyprinus
offline_file: ""
offline_thumbnail: ""
uuid: c0e54d79-666e-45fe-93b0-42bdf59b26c2
updated: 1484308340
title: Cyprinus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Six_koi.jpg
tags:
    - species
    - Fossil Species
    - Footnotes
categories:
    - Uncategorized
---
Cyprinus is the genus of typical carps in family Cyprinidae. They are of East Asian origin and closely related to some more barb-like genera, such as Cyclocheilichthys and the recently established Barbonymus (tinfoils). The crucian carps (Carassius) of western Eurasia, which include the goldfish (C. auratus), are apparently not as closely related.[1]
