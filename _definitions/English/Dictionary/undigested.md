---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/undigested
offline_file: ""
offline_thumbnail: ""
uuid: 64f3d35a-8715-4a1a-82e1-5475e10f0cc3
updated: 1484310319
title: undigested
categories:
    - Dictionary
---
undigested
     adj 1: not thought over and arranged systematically in the mind;
            not absorbed or assimilated mentally; "an undigested
            mass of facts gathered at random"
     2: not digested; "undigested food"
