---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/summarise
offline_file: ""
offline_thumbnail: ""
uuid: f174d696-7e59-4c9e-8ca4-2ba530cba624
updated: 1484310518
title: summarise
categories:
    - Dictionary
---
summarise
     v 1: be a summary of; "The abstract summarizes the main ideas in
          the paper" [syn: {summarize}, {sum}, {sum up}]
     2: give a summary (of); "he summed up his results"; "I will now
        summarize" [syn: {sum up}, {summarize}, {resume}]
