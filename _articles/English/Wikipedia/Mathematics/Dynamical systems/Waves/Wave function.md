---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Wave_function
offline_file: ""
offline_thumbnail: ""
uuid: 18ed9eb0-4c6f-4623-97dd-48ade453f19d
updated: 1484308905
title: Wave function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/QuantumHarmonicOscillatorAnimation.gif
tags:
    - Historical background
    - Wave functions and wave equations in modern theories
    - Definition (one spinless particle in 1d)
    - Position-space wave functions
    - Momentum-space wave functions
    - Relations between position and momentum representations
    - Definitions (other cases)
    - One-particle states in 3d position space
    - Many particle states in 3d position space
    - Probability interpretation
    - Time dependence
    - Non-relativistic examples
    - Particle in a box
    - Finite potential barrier
    - Quantum harmonic oscillator
    - Hydrogen atom
    - Wave functions and function spaces
    - Vector space structure
    - Representations
    - Inner product
    - Hilbert space
    - Common Hilbert spaces
    - Simplified description
    - More on wave functions and abstract state space
    - Ontology
    - Remarks
    - Notes
categories:
    - Waves
---
A wave function in quantum mechanics is a description of the quantum state of a system. The wave function is a complex-valued probability amplitude, and the probabilities for the possible results of measurements made on the system can be derived from it. The most common symbols for a wave function are the Greek letters ψ or Ψ (lower-case and capital psi).
