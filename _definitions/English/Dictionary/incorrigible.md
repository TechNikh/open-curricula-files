---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/incorrigible
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484440861
title: incorrigible
categories:
    - Dictionary
---
incorrigible
     adj : impervious to correction by punishment [ant: {corrigible}]
