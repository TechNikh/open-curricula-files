---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mistook
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484484361
title: mistook
categories:
    - Dictionary
---
mistake
     n 1: a wrong action attributable to bad judgment or ignorance or
          inattention; "he made a bad mistake"; "she was quick to
          point out my errors"; "I could understand his English in
          spite of his grammatical faults" [syn: {error}, {fault}]
     2: an understanding of something that is not correct; "he
        wasn't going to admit his mistake"; "make no mistake about
        his intentions"; "there must be some misunderstanding--I
        don't have a sister" [syn: {misunderstanding}, {misapprehension}]
     3: part of a statement that is not correct; "the book was full
        of errors" [syn: {error}]
     v 1: identify incorrectly; "Don't mistake her ...
