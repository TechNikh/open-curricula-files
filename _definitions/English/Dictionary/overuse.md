---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overuse
offline_file: ""
offline_thumbnail: ""
uuid: 3ebaf957-0794-482d-b824-675d99eaa994
updated: 1484310521
title: overuse
categories:
    - Dictionary
---
overuse
     n : exploitation to the point of diminishing returns [syn: {overexploitation},
          {overutilization}, {overutilisation}]
     v : make use of too often or too extensively [syn: {overdrive}]
