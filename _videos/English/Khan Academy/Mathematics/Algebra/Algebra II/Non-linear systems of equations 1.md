---
version: 1
type: video
provider: YouTube
id: hjigR_rHKDI
offline_file: ""
offline_thumbnail: ""
uuid: 7bced859-fddb-45cd-9198-f2659cb33f79
updated: 1484040398
title: Non-linear systems of equations 1
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/hjigR_rHKDI/default.jpg
    - https://i3.ytimg.com/vi/hjigR_rHKDI/1.jpg
    - https://i3.ytimg.com/vi/hjigR_rHKDI/2.jpg
    - https://i3.ytimg.com/vi/hjigR_rHKDI/3.jpg
---
