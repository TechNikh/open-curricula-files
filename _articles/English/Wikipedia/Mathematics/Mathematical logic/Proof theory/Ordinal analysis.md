---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ordinal_analysis
offline_file: ""
offline_thumbnail: ""
uuid: 452a8bdc-67ec-45af-90bd-78fa4b581cf4
updated: 1484309497
title: Ordinal analysis
tags:
    - Definition
    - Examples
    - Theories with proof theoretic ordinal ω2
    - Theories with proof theoretic ordinal ω3
    - Theories with proof theoretic ordinal ωn
    - Theories with proof theoretic ordinal ωω
    - Theories with proof theoretic ordinal ε0
    - >
        Theories with proof theoretic ordinal the Feferman-Schütte
        ordinal Γ0
    - >
        Theories with proof theoretic ordinal the Bachmann-Howard
        ordinal
    - Theories with larger proof theoretic ordinals
categories:
    - Proof theory
---
In proof theory, ordinal analysis assigns ordinals (often large countable ordinals) to mathematical theories as a measure of their strength. The field was formed when Gerhard Gentzen in 1934 used cut elimination to prove, in modern terms, that the proof theoretic ordinal of Peano arithmetic is ε0.
