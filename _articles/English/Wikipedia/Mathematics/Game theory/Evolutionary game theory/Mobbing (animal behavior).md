---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mobbing_(animal_behavior)
offline_file: ""
offline_thumbnail: ""
uuid: 95874b2f-6eaf-46db-a1b9-a72559c1d605
updated: 1484308524
title: Mobbing (animal behavior)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Redtail_hawk_chased_by_crows_4391.jpg
tags:
    - In birds
    - In other animals
    - Mobbing calls
    - Evolution
categories:
    - Evolutionary game theory
---
Mobbing in animals is an antipredator adaptation in which individuals of prey species mob a predator by cooperatively attacking or harassing it, usually to protect their offspring. A simple definition of mobbing is an assemblage of individuals around a potentially dangerous predator.[1] This is most frequently seen in birds, though it is also known to occur in many other animals such as the meerkat. While mobbing has evolved independently in many species, it only tends to be present in those whose young are frequently preyed upon. This behavior may complement cryptic adaptations in the offspring themselves, such as camouflage and hiding. Mobbing calls may be used to summon nearby ...
