---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cord
offline_file: ""
offline_thumbnail: ""
uuid: 520dd768-5eb4-40cd-8e38-eea987d6fd33
updated: 1484310321
title: cord
categories:
    - Dictionary
---
cord
     n 1: a line made of twisted fibers or threads; "the bundle was
          tied with a cord"
     2: a unit of amount of wood cut for burning; 128 cubic feet
     3: a light insulated conductor for household use [syn: {electric
        cord}]
     4: a cut pile fabric with vertical ribs; usually made of cotton
        [syn: {corduroy}]
     v 1: stack in cords; "cord firewood"
     2: bind or tie with a cord
