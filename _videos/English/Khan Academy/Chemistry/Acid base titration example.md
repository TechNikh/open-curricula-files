---
version: 1
type: video
provider: YouTube
id: XjFNmfLv9_Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Acid%20base%20titration%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7ceedc84-f2c9-461d-b7db-de50cad21370
updated: 1486069589
title: Acid base titration example
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/XjFNmfLv9_Q/default.jpg
    - https://i3.ytimg.com/vi/XjFNmfLv9_Q/1.jpg
    - https://i3.ytimg.com/vi/XjFNmfLv9_Q/2.jpg
    - https://i3.ytimg.com/vi/XjFNmfLv9_Q/3.jpg
---
Using acid-base titration to find mass of oxalic acid, a weak acid. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/acid-base-equilibrium/titrations/v/redox-titration?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/acid-base-equilibrium/titrations/v/titration-roundup?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
