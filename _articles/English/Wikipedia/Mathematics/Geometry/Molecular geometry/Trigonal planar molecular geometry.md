---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Trigonal_planar_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: aa2cbc9e-bb79-4200-8c8b-79d21277df84
updated: 1484309283
title: Trigonal planar molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-AX3E0-3D-balls.png
categories:
    - Molecular geometry
---
In chemistry, trigonal planar is a molecular geometry model with one atom at the center and three atoms at the corners of an equilateral triangle, called peripheral atoms, all in one plane.[1] In an ideal trigonal planar species, all three ligands are identical and all bond angles are 120°. Such species belong to the point group D3h. Molecules where the three ligands are not identical, such as H2CO, deviate from this idealized geometry. Examples of molecules with trigonal planar geometry include boron trifluoride (BF3), formaldehyde (H2CO), phosgene (COCl2), and sulfur trioxide (SO3). Some ions with trigonal planar geometry include nitrate (NO−
3), carbonate (CO2−
3), and guanidinium ...
