---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bogomolny_equations
offline_file: ""
offline_thumbnail: ""
uuid: a01bad0d-f5c2-4fba-88a0-12c20c044703
updated: 1484309204
title: Bogomolny equations
categories:
    - Differential geometry
---
In mathematics, the Bogomolny equations for magnetic monopoles are the equations FA = *DAφ, where FA is the curvature of a connection A on a G-bundle over a 3-manifold M, φ is a section of the corresponding adjoint bundle and * is the Hodge star operator on M. These equations are named after E. B. Bogomolny.
