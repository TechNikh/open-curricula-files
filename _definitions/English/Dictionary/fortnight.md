---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fortnight
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484449681
title: fortnight
categories:
    - Dictionary
---
fortnight
     n : a period of fourteen consecutive days; "most major tennis
         tournaments last a fortnight" [syn: {two weeks}]
