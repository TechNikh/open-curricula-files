---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transparent
offline_file: ""
offline_thumbnail: ""
uuid: cca9930b-7dbd-4775-8425-ce8fd16baab6
updated: 1484310303
title: transparent
categories:
    - Dictionary
---
transparent
     adj 1: transmitting light; able to be seen through with clarity;
            "the cold crystalline water of melted snow"; "crystal
            clear skies"; "could see the sand on the bottom of the
            limpid pool"; "lucid air"; "a pellucid brook";
            "transparent cristal" [syn: {crystalline}, {crystal
            clear}, {limpid}, {lucid}, {pellucid}]
     2: so thin as to transmit light; "a hat with a diaphanous
        veil"; "filmy wings of a moth"; "gauzy clouds of dandelion
        down"; "gossamer cobwebs"; "sheer silk stockings";
        "transparent chiffon"; "vaporous silks" [syn: {diaphanous},
         {filmy}, {gauzy}, {gossamer}, {see-through}, ...
