---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Veblen_function
offline_file: ""
offline_thumbnail: ""
uuid: fcb1c141-0d90-4c58-b956-6132d7b442b7
updated: 1484309499
title: Veblen function
tags:
    - The Veblen hierarchy
    - Fundamental sequences for the Veblen hierarchy
    - The Γ function
    - Generalizations
    - Finitely many variables
    - Transfinitely many variables
categories:
    - Proof theory
---
In mathematics, the Veblen functions are a hierarchy of normal functions (continuous strictly increasing functions from ordinals to ordinals), introduced by Oswald Veblen in Veblen (1908). If φ0 is any normal function, then for any non-zero ordinal α, φα is the function enumerating the common fixed points of φβ for β<α. These functions are all normal.
