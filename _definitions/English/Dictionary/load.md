---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/load
offline_file: ""
offline_thumbnail: ""
uuid: bf0601ca-021a-4a73-8adf-6e88b531b84e
updated: 1484310459
title: load
categories:
    - Dictionary
---
load
     n 1: weight to be borne or conveyed [syn: {loading}, {burden}]
     2: a quantity that can be processed or transported at one time;
        "the system broke down under excessive loads" [syn: {loading}]
     3: goods carried by a large vehicle [syn: {cargo}, {lading}, {freight},
         {loading}, {payload}, {shipment}, {consignment}]
     4: an amount of alcohol sufficient to intoxicate; "he got a
        load on and started a brawl"
     5: the power output of a generator or power plant
     6: an onerous or difficult concern; "the burden of
        responsibility"; "that's a load off my mind" [syn: {burden},
         {encumbrance}, {incumbrance}, {onus}]
     7: a deposit of ...
