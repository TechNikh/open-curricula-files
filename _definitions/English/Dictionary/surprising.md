---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surprising
offline_file: ""
offline_thumbnail: ""
uuid: 8e41f13f-31a0-422f-b705-57aeef9b3681
updated: 1484310447
title: surprising
categories:
    - Dictionary
---
surprising
     adj : causing surprise or wonder or amazement; "the report shows a
           surprising lack of hard factual data"; "leaped up with
           surprising agility"; "she earned a surprising amount of
           money" [ant: {unsurprising}]
