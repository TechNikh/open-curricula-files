---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ordinarily
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484500621
title: ordinarily
categories:
    - Dictionary
---
ordinarily
     adv : under normal conditions; "usually she was late" [syn: {normally},
            {usually}, {unremarkably}, {commonly}] [ant: {unusually}]
