---
version: 1
type: article
id: https://en.wikipedia.org/wiki/First-price_sealed-bid_auction
uuid: 6bea241e-06c3-44e1-9743-84e67e0b0ea5
updated: 1480455020
title: First-price sealed-bid auction
tags:
    - Strategic analysis
    - Example
    - Generalization
    - Incentive-compatible variant
    - Comparison to second-price auction
    - Comparison to other auctions
categories:
    - Mechanism design
---
A first-price sealed-bid auction (FPSBA) is a common type of auction. It is also known as blind auction,.[1] In this type of auction, all bidders simultaneously submit sealed bids, so that no bidder knows the bid of any other participant. The highest bidder pays the price they submitted.[2]:p2[3]
