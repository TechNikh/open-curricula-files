---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/varying
offline_file: ""
offline_thumbnail: ""
uuid: fe385038-d337-4ffd-b48f-570bac97f919
updated: 1484310258
title: varying
categories:
    - Dictionary
---
varying
     adj : marked by diversity or difference; "the varying angles of
           roof slope"; "nature is infinitely variable" [syn: {variable}]
