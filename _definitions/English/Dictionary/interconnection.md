---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interconnection
offline_file: ""
offline_thumbnail: ""
uuid: 1ff3de06-692d-4b5e-be30-467dc14f948e
updated: 1484310529
title: interconnection
categories:
    - Dictionary
---
interconnection
     n 1: a state of being connected reciprocally; "an interconnection
          between the two buildings" [syn: {interconnectedness}]
     2: (computer science) the act of interconnecting (wires or
        computers or theories etc.)
