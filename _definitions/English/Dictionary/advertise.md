---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advertise
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484546221
title: advertise
categories:
    - Dictionary
---
advertise
     v 1: call attention to; "Please don't advertise the fact that he
          has AIDS" [syn: {publicize}, {advertize}, {publicise}]
     2: make publicity for; try to sell (a product); "The salesman
        is aggressively pushing the new computer model"; "The
        company is heavily advertizing their new laptops" [syn: {advertize},
         {promote}, {push}]
