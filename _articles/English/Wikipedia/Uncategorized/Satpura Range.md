---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Satpura
offline_file: ""
offline_thumbnail: ""
uuid: b0823f0e-6d3d-4ec4-bb11-aaf55ba397e7
updated: 1484308420
title: Satpura Range
tags:
    - Etymology
    - geography
    - Ecology
    - Tourism
    - Other
categories:
    - Uncategorized
---
The Satpura Range is a range of hills in central India. The range rises in eastern Gujarat state running east through the border of Maharashtra and Madhya Pradesh to the east till Chhattisgarh. The range parallels the Vindhya Range to the north, and these two east-west ranges divide Indian Subcontinent into the Indo-Gangetic plain of northern India and the Deccan Plateau of the south. The Narmada River originates from north-eastern end of Satpura and runs in the depression between the Satpura and Vindhya ranges, draining the northern slope of the Satpura range, running west towards the Arabian Sea. The Tapti River originates in the eastern-central part of Satpura, crossing the range in the ...
