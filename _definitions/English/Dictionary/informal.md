---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/informal
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484461381
title: informal
categories:
    - Dictionary
---
informal
     adj 1: not formal; "conservative people unaccustomed to informal
            dress"; "an informal free-and-easy manner"; "an
            informal gathering of friends" [ant: {formal}]
     2: not officially recognized or controlled; "an informal
        agreement"; "a loose organization of the local farmers"
        [syn: {loose}]
     3: used of spoken and written language [ant: {formal}]
     4: having or fostering a warm or friendly atmosphere;
        especially through smallness and informality; "had a cozy
        chat"; "a relaxed informal manner"; "an intimate cocktail
        lounge"; "the small room was cozy and intimate" [syn: {cozy},
         {intimate}]
