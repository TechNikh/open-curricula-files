---
version: 1
type: video
provider: YouTube
id: 2v_8e6XnlAY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 90de6b8e-e6d1-4369-b7d7-0e867d86571f
updated: 1486069618
title: Subset, strict subset, and superset
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/2v_8e6XnlAY/default.jpg
    - https://i3.ytimg.com/vi/2v_8e6XnlAY/1.jpg
    - https://i3.ytimg.com/vi/2v_8e6XnlAY/2.jpg
    - https://i3.ytimg.com/vi/2v_8e6XnlAY/3.jpg
---
