---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/algebraic
offline_file: ""
offline_thumbnail: ""
uuid: af266053-3935-4373-85c2-08d588c19d90
updated: 1484310194
title: algebraic
categories:
    - Dictionary
---
algebraic
     adj : of or relating to algebra; "algebraic geometry" [syn: {algebraical}]
