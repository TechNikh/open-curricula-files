---
version: 1
type: video
provider: YouTube
id: 7T1L7lThd_s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 49cff509-f36a-4a6a-b8da-3616b5fec0d8
updated: 1486069658
title: "Science Projects For Kids- Bernoulli's Theorem - Golf Ball Demonstration for Bernoulli's  Experiment"
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/7T1L7lThd_s/default.jpg
    - https://i3.ytimg.com/vi/7T1L7lThd_s/1.jpg
    - https://i3.ytimg.com/vi/7T1L7lThd_s/2.jpg
    - https://i3.ytimg.com/vi/7T1L7lThd_s/3.jpg
---
