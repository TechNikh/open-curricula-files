---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/losses
offline_file: ""
offline_thumbnail: ""
uuid: c091ec2c-d606-41ec-9386-650141a88797
updated: 1484310605
title: losses
categories:
    - Dictionary
---
losses
     n : something lost (especially money lost at gambling) [syn: {losings}]
         [ant: {winnings}]
