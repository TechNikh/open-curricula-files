---
version: 1
type: video
provider: YouTube
id: T7u6LwQJVCQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f46979f5-9ef1-45c2-825b-2169ba8a83cb
updated: 1486069637
title: Fiber Optics Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/T7u6LwQJVCQ/default.jpg
    - https://i3.ytimg.com/vi/T7u6LwQJVCQ/1.jpg
    - https://i3.ytimg.com/vi/T7u6LwQJVCQ/2.jpg
    - https://i3.ytimg.com/vi/T7u6LwQJVCQ/3.jpg
---
