---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immaterial
offline_file: ""
offline_thumbnail: ""
uuid: 38ee391a-50d4-4c0c-b6dc-03705250eafc
updated: 1484310138
title: immaterial
categories:
    - Dictionary
---
immaterial
     adj 1: of no importance or relevance especially to a law case; "an
            objection that is immaterial after the fact" [ant: {material}]
     2: without material form or substance; "an incorporeal spirit"
        [syn: {incorporeal}] [ant: {corporeal}]
     3: not consisting of matter; "immaterial apparitions"; "ghosts
        and other immaterial entities" [syn: {nonmaterial}] [ant:
        {material}]
     4: not pertinent to the matter under consideration; "an issue
        extraneous to the debate"; "the price was immaterial";
        "mentioned several impertinent facts before finally coming
        to the point" [syn: {extraneous}, {impertinent}, {orthogonal}]
    ...
