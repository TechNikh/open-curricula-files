---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tree-depth
offline_file: ""
offline_thumbnail: ""
uuid: 6e2e25b6-dd84-4967-89e7-ea74c53d5e95
updated: 1484309387
title: Tree-depth
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/370px-Tree-depth.svg.png
tags:
    - Definitions
    - Examples
    - Depth of trees and relation to treewidth
    - Graph minors
    - Induced subgraphs
    - Complexity
    - Notes
categories:
    - Graph coloring
---
In graph theory, the tree-depth of a connected undirected graph G is a numerical invariant of G, the minimum height of a Trémaux tree for a supergraph of G. This invariant and its close relatives have gone under many different names in the literature, including vertex ranking number, ordered chromatic number, and minimum elimination tree height; it is also closely related to the cycle rank of directed graphs and the star height of regular languages.[1] Intuitively, where the treewidth graph width parameter measures how far a graph is from being a tree, this parameter measures how far a graph is from being a star.
