---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Large_numbers
offline_file: ""
offline_thumbnail: ""
uuid: 5f0a49b5-7dd3-4977-9bbe-fcdee52f5d63
updated: 1484308365
title: Large numbers
tags:
    - Using scientific notation to handle large and small numbers
    - Large numbers in the everyday world
    - Astronomically large numbers
    - Computers and computational complexity
    - Examples
    - Systematically creating ever faster increasing sequences
    - Standardized system of writing very large numbers
    - Examples of numbers in numerical order
    - Comparison of base values
    - Accuracy
    - Accuracy for very large numbers
    - Approximate arithmetic for very large numbers
    - Large numbers in some noncomputable sequences
    - Infinite numbers
    - Notations
    - Notes and references
categories:
    - Uncategorized
---
Large numbers are numbers that are significantly larger than those ordinarily used in everyday life, for instance in simple counting or in monetary transactions. The term typically refers to large positive integers, or more generally, large positive real numbers, but it may also be used in other contexts.
