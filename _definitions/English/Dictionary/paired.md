---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paired
offline_file: ""
offline_thumbnail: ""
uuid: 801af169-9742-4cc9-881d-dd926a9edc62
updated: 1484310303
title: paired
categories:
    - Dictionary
---
paired
     adj 1: used of gloves, socks, etc. [syn: {mated}]
     2: of leaves etc; growing in pairs on either side of a stem;
        "opposite leaves" [syn: {opposite}] [ant: {alternate}]
     3: mated sexually
