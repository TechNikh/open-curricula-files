---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nitrogen
offline_file: ""
offline_thumbnail: ""
uuid: c1435e7b-18c0-4df1-97af-7f3243661058
updated: 1484310373
title: nitrogen
categories:
    - Dictionary
---
nitrogen
     n : a common nonmetallic element that is normally a colorless
         odorless tasteless inert diatomic gas; constitutes 78
         percent of the atmosphere by volume; a constituent of all
         living tissues [syn: {N}, {atomic number 7}]
