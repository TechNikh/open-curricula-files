---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Massera%27s_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 37acf646-bba3-4d87-85d6-b01952c54108
updated: 1484308837
title: "Massera's lemma"
tags:
    - "Massera's original lemma"
    - Extension to multivariable functions
    - Footnotes
categories:
    - Stability theory
---
In stability theory and nonlinear control, Massera's lemma, named after José Luis Massera, deals with the construction of the Lyapunov function to prove the stability of a dynamical system.[1] The lemma appears in (Massera 1949, p. 716) as the first lemma in section 12, and in more general form in (Massera 1956, p. 195) as lemma 2. In 2004, Massera's original lemma for single variable functions was extended to the multivariable case, and the resulting lemma was used to prove the stability of switched dynamical systems, where a common Lyapunov function describes the stability of multiple modes and switching signals.
