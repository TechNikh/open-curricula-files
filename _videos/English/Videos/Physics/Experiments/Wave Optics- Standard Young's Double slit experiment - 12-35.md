---
version: 1
type: video
provider: YouTube
id: GyJTA-12vWQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c14a8d42-4b4c-4021-ba88-30de48156ce2
updated: 1486069640
title: "Wave Optics- Standard Young's Double slit experiment - 12/35"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/GyJTA-12vWQ/default.jpg
    - https://i3.ytimg.com/vi/GyJTA-12vWQ/1.jpg
    - https://i3.ytimg.com/vi/GyJTA-12vWQ/2.jpg
    - https://i3.ytimg.com/vi/GyJTA-12vWQ/3.jpg
---
