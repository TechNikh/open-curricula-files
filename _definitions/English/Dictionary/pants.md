---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pants
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484315101
title: pants
categories:
    - Dictionary
---
pants
     n 1: (usually in the plural) a garment extending from the waist
          to the knee or ankle, covering each leg separately; "he
          had a sharp crease in his trousers" [syn: {trousers}]
     2: (usually in the plural) underpants worn by women; "she was
        afraid that her bloomers might have been showing" [syn: {bloomers},
         {drawers}, {knickers}]
