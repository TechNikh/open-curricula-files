---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/division
offline_file: ""
offline_thumbnail: ""
uuid: 177cca31-44fa-4f3e-bbf2-6ac4918a617e
updated: 1484310371
title: division
categories:
    - Dictionary
---
division
     n 1: an army unit large enough to sustain combat; "two infantry
          divisions were held in reserve"
     2: one of the portions into which something is regarded as
        divided and which together constitute a whole; "the
        written part of the exam"; "the finance section of the
        company"; "the BBC's engineering division" [syn: {part}, {section}]
     3: the act or process of dividing
     4: an administrative unit in government or business
     5: an arithmetic operation that is the inverse of
        multiplication; the quotient of two numbers is computed
     6: discord that splits a group [syn: {variance}]
     7: a league ranked by quality; "he played ...
