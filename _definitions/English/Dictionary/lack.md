---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lack
offline_file: ""
offline_thumbnail: ""
uuid: 0370206f-c374-4bde-b69f-f3289e5f94ac
updated: 1484310405
title: lack
categories:
    - Dictionary
---
lack
     n : the state of needing something that is absent or
         unavailable; "there is a serious lack of insight into the
         problem"; "water is the critical deficiency in desert
         regions"; "for want of a nail the shoe was lost" [syn: {deficiency},
          {want}]
     v : be without; "This soup lacks salt"; "There is something
         missing in my jewellery box!" [syn: {miss}] [ant: {have}]
