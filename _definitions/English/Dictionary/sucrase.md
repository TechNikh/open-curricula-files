---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sucrase
offline_file: ""
offline_thumbnail: ""
uuid: b3384b6b-42fd-45ef-b69c-68acabb20a9e
updated: 1484310313
title: sucrase
categories:
    - Dictionary
---
sucrase
     n : an enzyme that catalyzes the hydrolysis of sucrose into
         glucose and fructose [syn: {invertase}, {saccharase}]
