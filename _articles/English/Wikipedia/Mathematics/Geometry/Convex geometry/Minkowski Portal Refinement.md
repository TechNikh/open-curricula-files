---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Minkowski_Portal_Refinement
offline_file: ""
offline_thumbnail: ""
uuid: fec61a7d-ab62-4699-8c91-4c0306e61cdf
updated: 1484309197
title: Minkowski Portal Refinement
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/550px-XenoCollide.jpg
categories:
    - Convex geometry
---
The algorithm was created by Gary Snethen in 2006 and was first published in Game Programming Gems 7. The algorithm was used in Tomb Raider: Underworld and other games created by Crystal Dynamics and its sister studios within Eidos Interactive.
