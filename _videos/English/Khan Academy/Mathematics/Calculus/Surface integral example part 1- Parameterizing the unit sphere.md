---
version: 1
type: video
provider: YouTube
id: E_Hwhp74Rhc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Surface%20integral%20example%20part%201-%20Parameterizing%20the%20unit%20sphere.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f5202251-a239-430e-805a-401c22a3220a
updated: 1486069692
title: 'Surface integral example part 1- Parameterizing the unit sphere'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/E_Hwhp74Rhc/default.jpg
    - https://i3.ytimg.com/vi/E_Hwhp74Rhc/1.jpg
    - https://i3.ytimg.com/vi/E_Hwhp74Rhc/2.jpg
    - https://i3.ytimg.com/vi/E_Hwhp74Rhc/3.jpg
---
