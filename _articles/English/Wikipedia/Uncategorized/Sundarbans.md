---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sundarban
offline_file: ""
offline_thumbnail: ""
uuid: 0d528683-a6b4-4628-ab36-59de54d73964
updated: 1484308425
title: Sundarbans
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sundarban_police_boat.jpg
tags:
    - Etymology
    - History
    - geography
    - Physiography
    - Ecoregions
    - Sundarbans freshwater swamp forests
    - Sundarbans Mangroves
    - Ecological succession
    - Flora
    - Fauna
    - Predators
    - Avifauna
    - Aqua fauna
    - Reptiles
    - Endangered and extinct species
    - Climate change impact
    - Hazards
    - natural hazards
    - Man made hazards
    - Economy
    - Habitation
    - Administration
    - Protection
    - Sundarban National Park
    - Sundarbans West Wildlife Sanctuary
    - Sundarbans East Wildlife Sanctuary
    - Sundarbans South Wildlife Sanctuary
    - Sajnakhali Wildlife Sanctuary
    - In popular culture
    - Footnotes and references
    - Sources
categories:
    - Uncategorized
---
The Sundarbans (Bengali: সুন্দরবন, Shundôrbôn) is a natural region comprising southern Bangladesh and a part in the Indian state of West Bengal. It is the largest single block of tidal halophytic mangrove forest in the world.[2][3][4] The Sundarbans covers approximately 10,000 square kilometres (3,900 sq mi) most of which is in Bangladesh with the remainder in India.[5][6] The Sundarbans is a UNESCO World Heritage Site.[5]
