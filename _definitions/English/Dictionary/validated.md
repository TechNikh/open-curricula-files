---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/validated
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484385241
title: validated
categories:
    - Dictionary
---
validated
     adj : declared or made legally valid; "a validated claim"
