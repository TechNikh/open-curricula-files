---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transversal_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: bd4b2f8d-3197-4b0e-bac6-d6a91b622f37
updated: 1484308932
title: Transversal (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Paralelni_transverzala_cor.svg.png
tags:
    - Angles of a transversal
    - Corresponding angles
    - Alternate angles
    - Consecutive interior angles
    - Other characteristics of transversals
    - Related theorems
categories:
    - Elementary geometry
---
In geometry, a transversal is a line that passes through two lines in the same plane at two distinct points. Transversals play a role in establishing whether two other lines in the Euclidean plane are parallel. The intersections of a transversal with two lines create various types of pairs of angles: consecutive interior angles, corresponding angles, and alternate angles. By Euclid's parallel postulate, if the two lines are parallel, consecutive interior angles are supplementary, corresponding angles are equal, and alternate angles are equal.
