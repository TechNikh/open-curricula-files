---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bengaluru
offline_file: ""
offline_thumbnail: ""
uuid: 8f51b58e-2146-4f2f-a3a0-65f41eb7cc81
updated: 1484308450
title: Bangalore
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-BangaloreLake_0.jpg
tags:
    - Etymology
    - History
    - Early and medieval history
    - Foundation and early modern history
    - Later modern and contemporary history
    - geography
    - Climate
    - Demographics
    - Civic administration
    - Pollution control
    - Slums
    - Waste management
    - Economy
    - Transport
    - air
    - Rail
    - Road
    - culture
    - Art and literature
    - Theatre, music, and dance
    - Education
    - Media
    - Sports
    - City based clubs
    - Location
    - Sister cities
categories:
    - Uncategorized
---
Bangalore /bæŋɡəˈlɔːr/, officially known as Bengaluru[8] ([ˈbeŋɡəɭuːɾu] ( listen)), is the capital of the Indian state of Karnataka. It has a population of about 8.42 million and a metropolitan population of about 8.52 million, making it the third most populous city and fifth most populous urban agglomeration in India.[5] Located in southern India on the Deccan Plateau, at a height of over 900 m (3,000 ft) above sea level, Bangalore is known for its pleasant climate throughout the year. Its elevation is the highest among the major cities of India.[9]
