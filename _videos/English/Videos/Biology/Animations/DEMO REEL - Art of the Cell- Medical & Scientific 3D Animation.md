---
version: 1
type: video
provider: YouTube
id: 2JxBxLiIcTU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 77050265-71e3-473d-95f9-7c394442453b
updated: 1486069670
title: 'DEMO REEL - Art of the Cell- Medical & Scientific 3D Animation'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/2JxBxLiIcTU/default.jpg
    - https://i3.ytimg.com/vi/2JxBxLiIcTU/1.jpg
    - https://i3.ytimg.com/vi/2JxBxLiIcTU/2.jpg
    - https://i3.ytimg.com/vi/2JxBxLiIcTU/3.jpg
---
