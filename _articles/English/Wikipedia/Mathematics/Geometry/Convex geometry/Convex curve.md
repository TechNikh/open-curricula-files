---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_curve
offline_file: ""
offline_thumbnail: ""
uuid: 47ad3518-b36e-4941-ac91-a30d362daed5
updated: 1484309189
title: Convex curve
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Convex_polygon_illustration1.svg.png
tags:
    - Definitions
    - Definition by supporting lines
    - Definition by convex sets
    - Strictly convex curve
    - Properties
    - Parallel tangents
    - Monotonicity of turning angle
    - Related shapes
categories:
    - Convex geometry
---
The boundary of a convex set is always a convex curve.
