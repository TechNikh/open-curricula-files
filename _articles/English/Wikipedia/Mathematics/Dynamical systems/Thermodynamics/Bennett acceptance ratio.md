---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bennett_acceptance_ratio
offline_file: ""
offline_thumbnail: ""
uuid: 75ad164a-be4b-4950-b435-bd17da21f81b
updated: 1484308832
title: Bennett acceptance ratio
tags:
    - Preliminaries
    - The general case
    - The basic case
    - The most efficient case
    - Multistate Bennett acceptance ratio
    - Relation to other methods
    - The perturbation theory method
    - The exact (infinite order) result
    - The second order (approximate) result
    - The first order inequalities
    - The thermodynamic integration method
    - Implementation
categories:
    - Thermodynamics
---
The Bennett acceptance ratio method (sometimes abbreviated to BAR) is an algorithm for estimating the difference in free energy between two systems (usually the systems will be simulated on the computer). It was suggested by Charles H. Bennett in 1976.[1]
