---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bazaar
offline_file: ""
offline_thumbnail: ""
uuid: 7e4ea6c6-2953-4035-982c-c039ec9c3488
updated: 1484310393
title: bazaar
categories:
    - Dictionary
---
bazaar
     n 1: a shop where a variety of goods are sold [syn: {bazar}]
     2: a street of small shops (especially in Orient) [syn: {bazar}]
     3: a sale of miscellany; often for charity; "the church bazaar"
        [syn: {fair}]
