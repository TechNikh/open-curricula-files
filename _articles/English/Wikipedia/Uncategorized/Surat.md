---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Surat
offline_file: ""
offline_thumbnail: ""
uuid: 14fcc2b7-ad19-4ffd-86b6-00b5b9a24762
updated: 1484308455
title: Surat
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Baghdadi_Jews_Cemetery_2.jpg
tags:
    - History
    - geography
    - Climate
    - Finance and economy
    - Diamond polishing
    - Textiles
    - Corporate industries
    - Information technology
    - Diamond Research And Mercantile City (DREAM)
    - Government and politics
    - Municipal institutions
    - Public safety
    - Transport
    - air
    - Rail
    - Water
    - Transport
    - Local transport
    - Infrastructure
    - Demographics
    - Culture, food and festivals
    - Food
    - Festivals
    - Media
    - Education
    - Universities
    - 'Engineering, IT & management institutes'
    - Medical
    - Other institutions
    - schools
    - Stadiums
    - Sports grounds
    - Other sports clubs
    - Points of interest
categories:
    - Uncategorized
---
Surat, is a port city previously known as Suryapur. It is the economical capital and former princely state in the Indian state of Gujarat. It is the eighth largest city and ninth largest urban agglomeration (after Pune) in India. Surat is the 3rd "cleanest city of India",[8] and 4th fastest growing city of the world.[9] Surat is famous for its food.
