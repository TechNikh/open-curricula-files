---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/size
offline_file: ""
offline_thumbnail: ""
uuid: 976dd340-e014-48dc-b913-5d4f65e351b0
updated: 1484310337
title: size
categories:
    - Dictionary
---
size
     adj : (used in combination) sized; "the economy-size package";
           "average-size house"
     n 1: the physical magnitude of something (how big it is); "a wolf
          is about the size of a large dog"
     2: the property resulting from being one of a series of
        graduated measurements (as of clothing); "he wears a size
        13 shoe"
     3: any glutinous material used to fill pores in surfaces or to
        stiffen fabrics; "size gives body to a fabric" [syn: {sizing}]
     4: the actual state of affairs; "that's the size of the
        situation"; "she hates me, that's about the size of it"
        [syn: {size of it}]
     5: a large magnitude; "he blanched ...
