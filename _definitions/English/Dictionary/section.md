---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/section
offline_file: ""
offline_thumbnail: ""
uuid: 3047cec1-57a3-44c2-8b36-2d3adf018ec2
updated: 1484310301
title: section
categories:
    - Dictionary
---
section
     n 1: a self-contained part of a larger composition (written or
          musical); "he always turns first to the business
          section"; "the history of this work is discussed in the
          next section" [syn: {subdivision}]
     2: a very thin slice (of tissue or mineral or other substance)
        for examination under a microscope; "sections from the
        left ventricle showed diseased tissue"
     3: a distinct region or subdivision of a territorial or
        political area or community or group of people; "no
        section of the nation is more ardent than the South";
        "there are three synagogues in the Jewish section"
     4: one of several parts or ...
