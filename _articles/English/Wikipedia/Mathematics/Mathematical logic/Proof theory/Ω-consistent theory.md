---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/%CE%A9-consistent_theory'
offline_file: ""
offline_thumbnail: ""
uuid: 16090d36-d030-4bd3-b377-a8742dd4d89a
updated: 1484309495
title: Ω-consistent theory
tags:
    - Definition
    - Examples
    - Consistent, ω-inconsistent theories
    - Arithmetically sound, ω-inconsistent theories
    - Arithmetically unsound, ω-consistent theories
    - ω-logic
    - Relation to other consistency principles
    - Notes
    - Bibliography
categories:
    - Proof theory
---
In mathematical logic, an ω-consistent (or omega-consistent, also called numerically segregative[1]) theory is a theory (collection of sentences) that is not only (syntactically) consistent (that is, does not prove a contradiction), but also avoids proving certain infinite combinations of sentences that are intuitively contradictory. The name is due to Kurt Gödel, who introduced the concept in the course of proving the incompleteness theorem.[2]
