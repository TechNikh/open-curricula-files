---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/constituent
offline_file: ""
offline_thumbnail: ""
uuid: 4c1e8595-36cb-4bee-8b9e-47ef506f338c
updated: 1484310363
title: constituent
categories:
    - Dictionary
---
constituent
     adj : constitutional in the structure of something (especially
           your physical makeup) [syn: {constituent(a)}, {constitutional},
            {constitutive(a)}, {organic}]
     n 1: an artifact that is one of the individual parts of which a
          composite entity is made up; especially a part that can
          be separated from or attached to a system; "spare
          components for cars"; "a component or constituent
          element of a system" [syn: {component}, {element}]
     2: a member of a constituency; a citizen who is represented in
        a government by officials for whom he or she votes; "needs
        continued support by constituents to be ...
