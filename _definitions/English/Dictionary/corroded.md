---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corroded
offline_file: ""
offline_thumbnail: ""
uuid: 7dd9432d-078e-4829-bd23-6cf5ee5ac61e
updated: 1484310384
title: corroded
categories:
    - Dictionary
---
corroded
     adj : eaten away as by acid or oxidation
