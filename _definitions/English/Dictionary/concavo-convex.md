---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/concavo-convex
offline_file: ""
offline_thumbnail: ""
uuid: f04a4a5a-9593-4741-af46-3d6a82e6cdb4
updated: 1484310208
title: concavo-convex
categories:
    - Dictionary
---
concavo-convex
     adj : concave on one side and convex on the other with the
           concavity being greater than the convexity
