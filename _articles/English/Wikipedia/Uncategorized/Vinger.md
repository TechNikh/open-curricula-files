---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vinger
offline_file: ""
offline_thumbnail: ""
uuid: c4fb2d5d-b169-4245-b64a-51a561bbea1e
updated: 1484308312
title: Vinger
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Vinger_kirke_Kongsvinger.JPG
categories:
    - Uncategorized
---
Vinger is a traditional district and a parish as well as being a former municipality in Kongsvinger in Hedmark, Norway.[1] The parish of Vinger (Vinger sogn) was established as a municipality on 1 January 1838 (see formannskapsdistrikt).[2]
