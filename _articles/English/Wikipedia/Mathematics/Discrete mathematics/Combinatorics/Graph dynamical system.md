---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_dynamical_system
offline_file: ""
offline_thumbnail: ""
uuid: 82902a5b-039e-4e96-9e51-c4376bddb742
updated: 1484308585
title: Graph dynamical system
tags:
    - Formal definition
    - Generalized cellular automata (GCA)
    - Sequential dynamical systems (SDS)
    - Stochastic graph dynamical systems
    - Applications
categories:
    - Combinatorics
---
In mathematics, the concept of graph dynamical systems can be used to capture a wide range of processes taking place on graphs or networks. A major theme in the mathematical and computational analysis of GDSs is to relate their structural properties (e.g. the network connectivity) and the global dynamics that result.
