---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hitlers
offline_file: ""
offline_thumbnail: ""
uuid: fac901b2-1bd5-4279-a9f6-bc9e6f6610d8
updated: 1484308476
title: Adolf Hitler
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hitler_house_in_Leonding.jpg
tags:
    - Early years
    - Ancestry
    - Childhood and education
    - Early adulthood in Vienna and Munich
    - World War I
    - Entry into politics
    - Beer Hall Putsch
    - Rebuilding the NSDAP
    - Rise to power
    - Brüning administration
categories:
    - Uncategorized
---
Adolf Hitler (German: [ˈadɔlf ˈhɪtlɐ] ( listen); 20 April 1889 – 30 April 1945) was a German politician who was the leader of the Nazi Party (Nationalsozialistische Deutsche Arbeiterpartei; NSDAP), Chancellor of Germany from 1933 to 1945, and Führer ("leader") of Nazi Germany from 1934 to 1945. As dictator of the German Reich, he initiated World War II in Europe with the invasion of Poland in September 1939 and was central to the Holocaust.
