---
version: 1
type: video
provider: YouTube
id: wDq1MGDkIUg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 111390a4-0260-41b1-9a9d-4b92331ee1c6
updated: 1486069636
title: Modulation Experiment 1 O2
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/wDq1MGDkIUg/default.jpg
    - https://i3.ytimg.com/vi/wDq1MGDkIUg/1.jpg
    - https://i3.ytimg.com/vi/wDq1MGDkIUg/2.jpg
    - https://i3.ytimg.com/vi/wDq1MGDkIUg/3.jpg
---
