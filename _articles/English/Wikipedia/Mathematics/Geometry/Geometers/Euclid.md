---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euclid
offline_file: ""
offline_thumbnail: ""
uuid: 91d37ed4-fa84-4f88-a3bb-76e8a99d6bbd
updated: 1484309078
title: Euclid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/lossy-page1-220px-Euclidis_quae_supersunt_omnia.tif.jpg
tags:
    - life
    - Elements
    - Other works
    - Lost works
    - Notes
categories:
    - Geometers
---
Euclid (/ˈjuːklᵻd/; Greek: Εὐκλείδης, Eukleidēs Ancient Greek: [eu̯.klěː.dɛːs]; fl. 300 BCE), sometimes called Euclid of Alexandria to distinguish him from Euclides of Megara, was a Greek mathematician, often referred to as the "father of geometry". He was active in Alexandria during the reign of Ptolemy I (323–283 BCE). His Elements is one of the most influential works in the history of mathematics, serving as the main textbook for teaching mathematics (especially geometry) from the time of its publication until the late 19th or early 20th century.[1][2][3] In the Elements, Euclid deduced the principles of what is now called Euclidean geometry from a small set of ...
