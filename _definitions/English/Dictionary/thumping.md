---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thumping
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484458621
title: thumping
categories:
    - Dictionary
---
thumping
     adj : (used informally) very large; "a thumping loss" [syn: {humongous},
            {banging}, {whopping}, {walloping}]
     n : a heavy dull sound (as made by impact of heavy objects)
         [syn: {thump}, {clump}, {clunk}, {thud}]
