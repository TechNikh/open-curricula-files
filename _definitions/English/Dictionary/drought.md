---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drought
offline_file: ""
offline_thumbnail: ""
uuid: 36177fe9-7574-43d3-a011-ce7bdc34409b
updated: 1484310249
title: drought
categories:
    - Dictionary
---
drought
     n 1: a temporary shortage of rainfall
     2: a prolonged shortage
