---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Barth_surface
offline_file: ""
offline_thumbnail: ""
uuid: 2910080d-d4e1-472b-a442-7c3648c3915f
updated: 1484309352
title: Barth surface
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-BarthSextic.png
categories:
    - Geometry stubs
---
In algebraic geometry, a Barth surface is one of the complex nodal surfaces in 3 dimensions with large numbers of double points found by Wolf Barth (1996). Two examples are the Barth sextic of degree 6 with 65 double points, and the Barth decic of degree 10 with 345 double points.
