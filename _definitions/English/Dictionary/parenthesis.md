---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parenthesis
offline_file: ""
offline_thumbnail: ""
uuid: f5f44618-6c88-4a02-aa8c-7bf208de241c
updated: 1484310399
title: parenthesis
categories:
    - Dictionary
---
parenthesis
     n 1: either of two punctuation marks (or) used to enclose textual
          material
     2: a message that departs from the main subject [syn: {digression},
         {aside}, {excursus}, {divagation}]
     [also: {parentheses} (pl)]
