---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Methylated_spirit
offline_file: ""
offline_thumbnail: ""
uuid: e2a5c058-49be-447a-8e38-304e98b3febf
updated: 1484308356
title: Denatured alcohol
tags:
    - Purpose
    - Formulations
    - Uses
    - Consumption and toxicity
categories:
    - Uncategorized
---
Denatured alcohol, also called methylated spirits or denatured rectified spirit, is ethanol that has additives to make it poisonous, bad tasting, foul smelling or nauseating, to discourage recreational consumption. In some cases it is also dyed.
