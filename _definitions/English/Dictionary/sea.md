---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sea
offline_file: ""
offline_thumbnail: ""
uuid: f5ff5a3d-946c-41f4-8a72-da4dafd95bf4
updated: 1484310268
title: sea
categories:
    - Dictionary
---
sea
     adj : relating to or characteristic of or occurring on the sea or
           ships; "sea stories"; "sea smells"; "sea traffic" [syn:
            {sea(a)}] [ant: {air(a)}, {land(a)}]
     n 1: a division of an ocean or a large body of salt water
          partially enclosed by land
     2: anything apparently limitless in quantity or volume [syn: {ocean}]
     3: turbulent water with swells of considerable size; "heavy
        seas"
