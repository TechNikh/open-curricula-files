---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Random_geometric_graph
offline_file: ""
offline_thumbnail: ""
uuid: 488826fa-42cb-45d4-901a-5916391477ec
updated: 1484309452
title: Random geometric graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Random_geometric_graph.svg.png
categories:
    - Random graphs
---
In graph theory, a random geometric graph (RGG) is the mathematically simplest spatial network, namely an undirected graph constructed by randomly placing N nodes in some metric space (according to a specified probability distribution) and connecting two nodes by a link if and only if their distance is in a given range, e.g. smaller than a certain neighborhood radius, r.
