---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/utilisation
offline_file: ""
offline_thumbnail: ""
uuid: 9fb4689b-96c8-48a1-946c-6b6bf90c4d0c
updated: 1484310252
title: utilisation
categories:
    - Dictionary
---
utilisation
     n : the act of using; "he warned against the use of narcotic
         drugs"; "skilled in the utilization of computers" [syn: {use},
          {usage}, {utilization}, {employment}, {exercise}]
