---
version: 1
type: video
provider: YouTube
id: z7dVWmXdUgA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0d46866f-5948-48a0-99c8-4ca54d0b637f
updated: 1486069639
title: >
    Focal Length of a Concave Lens Cbse Class 12 Practical
    Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/z7dVWmXdUgA/default.jpg
    - https://i3.ytimg.com/vi/z7dVWmXdUgA/1.jpg
    - https://i3.ytimg.com/vi/z7dVWmXdUgA/2.jpg
    - https://i3.ytimg.com/vi/z7dVWmXdUgA/3.jpg
---
