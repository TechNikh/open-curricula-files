---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Riordan_array
offline_file: ""
offline_thumbnail: ""
uuid: 5494be99-dc02-4669-9e4b-b4eb8108bf4f
updated: 1484308608
title: Riordan array
categories:
    - Combinatorics
---
A Riordan array is an infinite lower triangular matrix, 
  
    
      
        D
      
    
    {\displaystyle D}
  
, constructed out of two formal power series, 
  
    
      
        d
        (
        t
        )
      
    
    {\displaystyle d(t)}
  
 and 
  
    
      
        h
        (
        t
        )
      
    
    {\displaystyle h(t)}
  
, in such a way that 
  
    
      
        
          d
          
            n
            ,
            k
          
        
        =
        [
        
          t
          
            n
          
        
        ]
        d
        (
        t
        )
        
          
            (
            t
            h
         ...
