---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dense
offline_file: ""
offline_thumbnail: ""
uuid: 1ec62693-f0fb-4f91-b160-50a3c04e7a99
updated: 1484310212
title: dense
categories:
    - Dictionary
---
dense
     adj 1: permitting little if any light to pass through because of
            denseness of matter; "dense smoke"; "heavy fog";
            "impenetrable gloom" [syn: {heavy}, {impenetrable}]
     2: closely crowded together; "a compact shopping center"; "a
        dense population"; "thick crowds" [syn: {compact}, {thick}]
     3: hard to pass through because of dense growth; "dense
        vegetation"; "thick woods" [syn: {thick}]
     4: having high relative density or specific gravity; "dense as
        lead"
     5: slow to learn or understand; lacking intellectual acuity;
        "so dense he never understands anything I say to him";
        "never met anyone quite so dim"; ...
