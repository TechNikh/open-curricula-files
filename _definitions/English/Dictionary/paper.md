---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paper
offline_file: ""
offline_thumbnail: ""
uuid: 9a2282ea-7c45-4d7a-bc89-015185a4b6b4
updated: 1484310346
title: paper
categories:
    - Dictionary
---
paper
     adj : made of paper; "they wore paper hats at the party"
     n 1: a material made of cellulose pulp derived mainly from wood
          or rags or certain grasses
     2: an essay (especially one written as an assignment); "he got
        an A on his composition" [syn: {composition}, {report}, {theme}]
     3: a daily or weekly publication on folded sheets; contains
        news and articles and advertisements; "he read his
        newspaper at breakfast" [syn: {newspaper}]
     4: a scholarly article describing the results of observations
        or stating hypotheses; "he has written many scientific
        papers"
     5: medium for written communication; "the notion of an ...
