---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jordan
offline_file: ""
offline_thumbnail: ""
uuid: 34a79c95-466d-4f99-9355-b4765ce76b85
updated: 1484310148
title: jordan
categories:
    - Dictionary
---
Jordan
     n 1: a river in Palestine that empties into the Dead Sea; John
          the Baptist baptized Jesus in the Jordan [syn: {Jordan
          River}]
     2: an Arab kingdom in southwestern Asia on the Red Sea [syn: {Hashemite
        Kingdom of Jordan}]
