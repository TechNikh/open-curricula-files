---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/establish
offline_file: ""
offline_thumbnail: ""
uuid: 1218131b-8f4b-432f-adc3-d296351d1519
updated: 1484310208
title: establish
categories:
    - Dictionary
---
establish
     v 1: set up or found; "She set up a literacy program" [syn: {set
          up}, {found}, {launch}] [ant: {abolish}]
     2: set up or lay the groundwork for; "establish a new
        department" [syn: {found}, {plant}, {constitute}, {institute}]
     3: establish the validity of something, as by an example,
        explanation or experiment; "The experiment demonstrated
        the instability of the compound"; "The mathematician
        showed the validity of the conjecture" [syn: {prove}, {demonstrate},
         {show}, {shew}] [ant: {disprove}]
     4: institute, enact, or establish; "make laws" [syn: {lay down},
         {make}]
     5: bring about; "The trompe ...
