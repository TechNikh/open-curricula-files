---
version: 1
type: video
provider: YouTube
id: ddzJ4NGHzuM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9225b7e7-9d0b-4de5-8f23-1bb5a7e27cc6
updated: 1486069636
title: Soap Film Experiment 1
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/ddzJ4NGHzuM/default.jpg
    - https://i3.ytimg.com/vi/ddzJ4NGHzuM/1.jpg
    - https://i3.ytimg.com/vi/ddzJ4NGHzuM/2.jpg
    - https://i3.ytimg.com/vi/ddzJ4NGHzuM/3.jpg
---
