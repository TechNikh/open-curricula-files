---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/K%C3%B6nig%27s_theorem_(set_theory)'
offline_file: ""
offline_thumbnail: ""
uuid: 9f177e2d-7c20-40da-8511-07ef87cf5d29
updated: 1484308997
title: "König's theorem (set theory)"
tags:
    - Details
    - "Corollaries of König's theorem"
    - Axiom of choice
    - "König's theorem and cofinality"
    - "A proof of König's theorem"
    - Notes
categories:
    - Theorems in the foundations of mathematics
---
In set theory, König's theorem states that if the axiom of choice holds, I is a set, mi and ni are cardinal numbers for every i in I, and 
  
    
      
        
          m
          
            i
          
        
        <
        
          n
          
            i
          
        
        
      
    
    {\displaystyle m_{i}<n_{i}\!}
  
 for every i in I then
