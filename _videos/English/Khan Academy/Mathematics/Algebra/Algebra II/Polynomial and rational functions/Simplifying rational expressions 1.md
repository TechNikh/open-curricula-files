---
version: 1
type: video
provider: YouTube
id: XChok8XlF90
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Polynomial%20and%20rational%20functions/Simplifying%20rational%20expressions%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 97961aea-66e6-40c8-9b1b-a7bcdde377fb
updated: 1486069557
title: Simplifying rational expressions 1
categories:
    - Polynomial and rational functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/XChok8XlF90/default.jpg
    - https://i3.ytimg.com/vi/XChok8XlF90/1.jpg
    - https://i3.ytimg.com/vi/XChok8XlF90/2.jpg
    - https://i3.ytimg.com/vi/XChok8XlF90/3.jpg
---
