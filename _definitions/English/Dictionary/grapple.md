---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grapple
offline_file: ""
offline_thumbnail: ""
uuid: 490fe349-ce4d-4d15-b820-2cbdada9d4f0
updated: 1484310181
title: grapple
categories:
    - Dictionary
---
grapple
     n 1: a tool consisting of several hooks for grasping and holding;
          often thrown with a rope [syn: {grapnel}, {grappler}, {grappling
          hook}, {grappling iron}]
     2: a dredging bucket with hinges like the shell of a clam [syn:
         {clamshell}]
     3: the act of engaging in close hand-to-hand combat; "they had
        a fierce wrestle"; "we watched his grappling and wrestling
        with the bully" [syn: {wrestle}, {wrestling}, {grappling},
         {hand-to-hand struggle}]
     v 1: come to terms or deal successfully with; "We got by on just
          a gallon of gas"; "They made do on half a loaf of bread
          every day" [syn: {cope}, {get by}, ...
