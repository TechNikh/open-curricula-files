---
version: 1
type: video
provider: YouTube
id: zpaHPXVR8WU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/What%20are%20Ionic%20Bonds-%20%20-%20The%20Chemistry%20Journey%20-%20The%20Fuse%20School.webm"
offline_file: ""
offline_thumbnail: ""
uuid: deed044c-19ab-4bea-8402-00ec0add6f9b
updated: 1486069664
title: 'What are Ionic Bonds?  | The Chemistry Journey | The Fuse School'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/zpaHPXVR8WU/default.jpg
    - https://i3.ytimg.com/vi/zpaHPXVR8WU/1.jpg
    - https://i3.ytimg.com/vi/zpaHPXVR8WU/2.jpg
    - https://i3.ytimg.com/vi/zpaHPXVR8WU/3.jpg
---
