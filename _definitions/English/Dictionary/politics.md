---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/politics
offline_file: ""
offline_thumbnail: ""
uuid: 1b898a61-5836-4aa4-a84f-d429f05a97cb
updated: 1484310448
title: politics
categories:
    - Dictionary
---
politics
     n 1: social relations involving authority or power [syn: {political
          relation}]
     2: the study of government of states and other political units
        [syn: {political science}, {government}]
     3: the profession devoted to governing and to political affairs
     4: the opinion you hold with respect to political questions
        [syn: {political sympathies}]
