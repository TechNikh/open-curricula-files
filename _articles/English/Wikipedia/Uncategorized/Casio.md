---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Casio
offline_file: ""
offline_thumbnail: ""
uuid: 6fcb1217-4ade-460b-ba1d-9dfe6c8767e2
updated: 1484308391
title: Casio
tags:
    - History
    - Products
    - Calculators
    - Scientific calculators
    - Basic calculators
    - Watches
    - Musical instruments
    - Other
    - Gallery
categories:
    - Uncategorized
---
Casio Computer Co., Ltd. (カシオ計算機株式会社, Kashio Keisanki Kabushiki-gaisha?) is a multinational consumer electronics and commercial electronics manufacturing company headquartered in Shibuya, Tokyo, Japan. Its products include calculators, mobile phones, digital cameras, electronic musical instruments and digital watches. It was founded in 1946, and in 1957 released the world's first entirely electric compact calculator. Casio was an early digital camera innovator. During the 1980s and 1990s, Casio developed numerous affordable home electronic keyboards for musicians.
