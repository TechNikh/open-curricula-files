---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/virtual
offline_file: ""
offline_thumbnail: ""
uuid: adc2e204-4e02-4942-8b7d-c49cedf59605
updated: 1484310216
title: virtual
categories:
    - Dictionary
---
virtual
     adj 1: being actually such in almost every respect; "a practical
            failure"; "the once elegant temple lay in virtual
            ruin" [syn: {virtual(a)}, {practical(a)}]
     2: being such in essence or effect though not in actual fact;
        "a virtual dependence on charity"; "a virtual revolution";
        "virtual reality" [syn: {virtual(a)}]
