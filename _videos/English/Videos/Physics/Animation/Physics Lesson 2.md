---
version: 1
type: video
provider: YouTube
id: WE-1MhVUK-Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 24c8d8f8-1d5a-4cd6-98a6-2e9b4fa6dcca
updated: 1486069653
title: Physics Lesson 2
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/WE-1MhVUK-Q/default.jpg
    - https://i3.ytimg.com/vi/WE-1MhVUK-Q/1.jpg
    - https://i3.ytimg.com/vi/WE-1MhVUK-Q/2.jpg
    - https://i3.ytimg.com/vi/WE-1MhVUK-Q/3.jpg
---
