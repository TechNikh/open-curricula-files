---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tessellation
offline_file: ""
offline_thumbnail: ""
uuid: 6c0a1f3c-b844-44f1-a7f5-e61f276177fa
updated: 1484309319
title: Tessellation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Ceramic_Tile_Tessellations_in_Marrakech.jpg
tags:
    - History
    - Etymology
    - Overview
    - In mathematics
    - Introduction to tessellations
    - Wallpaper groups
    - Aperiodic tilings
    - Tessellations and colour
    - Tessellations with polygons
    - Voronoi tilings
    - Tessellations in higher dimensions
    - Tessellations in non-Euclidean geometries
    - In art
    - In manufacturing
    - In nature
    - In puzzles and recreational mathematics
    - Examples
    - Footnotes
    - Sources
categories:
    - Symmetry
---
A tessellation of a flat surface is the tiling of a plane using one or more geometric shapes, called tiles, with no overlaps and no gaps. In mathematics, tessellations can be generalized to higher dimensions and a variety of geometries.
