---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/colourless
offline_file: ""
offline_thumbnail: ""
uuid: 2f6013e8-dfd3-4bf3-8d98-33aacf5a39d7
updated: 1484310371
title: colourless
categories:
    - Dictionary
---
colourless
     adj 1: lacking in variety and interest; "a colorless and
            unimaginative person"; "a colorless description of the
            parade" [syn: {colorless}] [ant: {colorful}]
     2: weak in color; not colorful [syn: {colorless}] [ant: {colorful}]
