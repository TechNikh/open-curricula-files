---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cartan%E2%80%93Hadamard_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 3634c532-7859-4d2d-80fb-22c59c25188a
updated: 1484309261
title: Cartan–Hadamard theorem
tags:
    - Riemannian geometry
    - Metric geometry
    - Generalization to locally convex spaces
    - Significance
categories:
    - Metric geometry
---
In mathematics, the Cartan–Hadamard theorem is a statement in Riemannian geometry concerning the structure of complete Riemannian manifolds of non-positive sectional curvature. The theorem states that the universal cover of such a manifold is diffeomorphic to a Euclidean space via the exponential map at any point. It was first proved by Hans Carl Friedrich von Mangoldt for surfaces in 1881, and independently by Jacques Hadamard in 1898. Élie Cartan generalized the theorem to Riemannian manifolds in 1928 (Helgason 1978; do Carmo 1992; Kobayashi & Nomizu 1969). The theorem was further generalized to a wide class of metric spaces by Mikhail Gromov in 1987; detailed proofs were published by ...
