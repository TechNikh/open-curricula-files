---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/value
offline_file: ""
offline_thumbnail: ""
uuid: 3c4b8d8d-9b20-467a-890f-250191d30672
updated: 1484310234
title: value
categories:
    - Dictionary
---
value
     n 1: a numerical quantity measured or assigned or computed; "the
          value assigned was 16 milliseconds"
     2: the quality (positive or negative) that renders something
        desirable or valuable; "the Shakespearean Shylock is of
        dubious value in the modern world"
     3: the amount (of money or goods or services) that is
        considered to be a fair equivalent for something else; "he
        tried to estimate the value of the produce at normal
        prices" [syn: {economic value}]
     4: relative darkness or lightness of a color; "I establish the
        colors and principal values by organizing the painting
        into three values--dark, medium...and ...
