---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distinction
offline_file: ""
offline_thumbnail: ""
uuid: 2ed9287c-2473-4f0f-9be7-390d4c5f4309
updated: 1484310448
title: distinction
categories:
    - Dictionary
---
distinction
     n 1: a discrimination between things as different and distinct;
          "it is necessary to make a distinction between love and
          infatuation" [syn: {differentiation}]
     2: high status importance owing to marked superiority; "a
        scholar of great eminence" [syn: {eminence}, {preeminence},
         {note}]
     3: a distinguishing quality; "it has the distinction of being
        the cheapest restaurant in town"
     4: a distinguishing difference; "he learned the distinction
        between gold and lead"
