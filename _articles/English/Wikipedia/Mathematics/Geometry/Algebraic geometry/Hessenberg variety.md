---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hessenberg_variety
offline_file: ""
offline_thumbnail: ""
uuid: 46b2200b-65cb-4f3e-a10e-12d39a4737f5
updated: 1484309156
title: Hessenberg variety
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Hesse.svg.png
categories:
    - Algebraic geometry
---
In geometry, Hessenberg varieties, first studied by De Mari, Procesi, and Shayman, are a family of subvarieties of the full flag variety which are defined by a Hessenberg function h and a linear transformation X. The study of Hessenberg varieties was first motivated by questions in numerical analysis in relation to algorithms for computing eigenvalues and eigenspaces of the linear operator X. Later work by Springer, Peterson, Kostant, among others, found connections with combinatorics, representation theory and cohomology.
