---
version: 1
type: video
provider: YouTube
id: CAAZrzJFkJs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a5bc4ca3-9a5c-4c46-9ea7-fb6e92bc946d
updated: 1486069631
title: 'Easy Grammar- Articles'
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/CAAZrzJFkJs/default.jpg
    - https://i3.ytimg.com/vi/CAAZrzJFkJs/1.jpg
    - https://i3.ytimg.com/vi/CAAZrzJFkJs/2.jpg
    - https://i3.ytimg.com/vi/CAAZrzJFkJs/3.jpg
---
