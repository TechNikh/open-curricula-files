---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appear
offline_file: ""
offline_thumbnail: ""
uuid: f288cfca-8180-468c-b871-209bddd7fe26
updated: 1484310303
title: appear
categories:
    - Dictionary
---
appear
     v 1: give a certain impression or have a certain outward aspect;
          "She seems to be sleeping"; "This appears to be a very
          difficult problem"; "This project looks fishy"; "They
          appeared like people who had not eaten or slept for a
          long time" [syn: {look}, {seem}]
     2: come into sight or view; "He suddenly appeared at the
        wedding"; "A new star appeared on the horizon" [ant: {disappear}]
     3: be issued or published; "Did your latest book appear yet?";
        "The new Woody Allen film hasn't come out yet" [syn: {come
        out}]
     4: seem to be true, probable, or apparent; "It seems that he is
        very gifted"; "It ...
