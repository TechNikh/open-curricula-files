---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/human
offline_file: ""
offline_thumbnail: ""
uuid: eed0031e-34eb-4674-a337-1c707e029f6e
updated: 1484310361
title: human
categories:
    - Dictionary
---
human
     adj 1: characteristic of humanity; "human nature"
     2: relating to a person; "the experiment was conducted on 6
        monkeys and 2 human subjects"
     3: having human form or attributes as opposed to those of
        animals or divine beings; "human beings"; "the human
        body"; "human kindness"; "human frailty" [ant: {nonhuman}]
     n 1: a human being; "there was too much for one person to do"
          [syn: {person}, {individual}, {someone}, {somebody}, {mortal},
           {soul}]
     2: any living or extinct member of the family Hominidae [syn: {homo},
         {man}, {human being}]
