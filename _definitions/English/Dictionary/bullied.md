---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bullied
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484409541
title: bullied
categories:
    - Dictionary
---
bullied
     adj : frightened into submission or compliance [syn: {browbeaten},
            {cowed}, {hangdog}, {intimidated}]
