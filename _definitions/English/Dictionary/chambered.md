---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chambered
offline_file: ""
offline_thumbnail: ""
uuid: 22d14e3d-2e4c-450c-8069-c3870e55750c
updated: 1484310160
title: chambered
categories:
    - Dictionary
---
chambered
     adj : having compartmental chambers; "a spiral chambered seashell"
