---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/threatening
offline_file: ""
offline_thumbnail: ""
uuid: 92172c27-a1d6-4d25-866f-894ac07162ae
updated: 1484310181
title: threatening
categories:
    - Dictionary
---
threatening
     adj 1: threatening or foreshadowing evil or tragic developments; "a
            baleful look"; "forbidding thunderclouds"; "his tone
            became menacing"; "ominous rumblings of discontent";
            "sinister storm clouds"; "a sinister smile"; "his
            threatening behavior"; "ugly black clouds"; "the
            situation became ugly" [syn: {baleful}, {forbidding},
            {menacing}, {minacious}, {minatory}, {ominous}, {sinister},
             {ugly}]
     2: darkened by clouds; "a heavy sky" [syn: {heavy}, {lowering},
         {sullen}]
