---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Chern%E2%80%93Simons_form'
offline_file: ""
offline_thumbnail: ""
uuid: ba546517-18f5-4f9d-a7a7-80f1c6f2f49d
updated: 1484309209
title: Chern–Simons form
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Cher_Assinatura.png
categories:
    - Differential geometry
---
In mathematics, the Chern–Simons forms are certain secondary characteristic classes. They have been found to be of interest in gauge theory, and they (especially the 3-form) define the action of Chern–Simons theory. The theory is named for Shiing-Shen Chern and James Harris Simons, co-authors of a 1974 paper entitled "Characteristic Forms and Geometric Invariants," from which the theory arose. See Chern and Simons (1974)
