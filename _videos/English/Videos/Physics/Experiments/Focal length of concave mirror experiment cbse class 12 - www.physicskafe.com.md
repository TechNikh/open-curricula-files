---
version: 1
type: video
provider: YouTube
id: ZcixhCGkVxk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4dd47f94-4583-471e-8040-4e9cde30c2ba
updated: 1486069637
title: >
    Focal length of concave mirror experiment cbse class 12 ||
    www.physicskafe.com
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZcixhCGkVxk/default.jpg
    - https://i3.ytimg.com/vi/ZcixhCGkVxk/1.jpg
    - https://i3.ytimg.com/vi/ZcixhCGkVxk/2.jpg
    - https://i3.ytimg.com/vi/ZcixhCGkVxk/3.jpg
---
