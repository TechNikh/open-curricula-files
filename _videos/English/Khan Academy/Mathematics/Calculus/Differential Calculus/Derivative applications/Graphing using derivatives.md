---
version: 1
type: video
provider: YouTube
id: hIgnece9ins
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Derivative%20applications/Graphing%20using%20derivatives.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 11abca37-ca43-447e-b563-485e07df6beb
updated: 1486069689
title: Graphing using derivatives
categories:
    - Derivative applications
thumbnail_urls:
    - https://i3.ytimg.com/vi/hIgnece9ins/default.jpg
    - https://i3.ytimg.com/vi/hIgnece9ins/1.jpg
    - https://i3.ytimg.com/vi/hIgnece9ins/2.jpg
    - https://i3.ytimg.com/vi/hIgnece9ins/3.jpg
---
