---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/herbert
offline_file: ""
offline_thumbnail: ""
uuid: 3a8df565-ffe6-4fc6-8d00-5b86af5cc6b4
updated: 1484310581
title: herbert
categories:
    - Dictionary
---
Herbert
     n : United States musician and composer and conductor noted for
         his comic operas (1859-1924) [syn: {Victor Herbert}]
