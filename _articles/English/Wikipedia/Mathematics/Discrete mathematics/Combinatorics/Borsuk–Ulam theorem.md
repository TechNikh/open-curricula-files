---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Borsuk%E2%80%93Ulam_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: fdd7ad83-ad4a-41a7-91ae-b26b11a2d34a
updated: 1484308573
title: Borsuk–Ulam theorem
tags:
    - History
    - Equivalent statements
    - With odd functions
    - With retractions
    - Proofs
    - 1-dimensional case
    - 'General case - algebraic topology proof'
    - 'General case - combinatorial proof'
    - Corollaries
    - Equivalent results
    - Generalizations
    - Notes
categories:
    - Combinatorics
---
In mathematics, the Borsuk–Ulam theorem (BUT), states that every continuous function from an n-sphere into Euclidean n-space maps some pair of antipodal points to the same point. Here, two points on a sphere are called antipodal if they are in exactly opposite directions from the sphere's center.
