---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Turbulence
offline_file: ""
offline_thumbnail: ""
uuid: fdefbcf3-26a3-494e-9d5b-2b1ccce03052
updated: 1484308712
title: Turbulence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Los_Angeles_attack_sub_2.jpg
tags:
    - features
    - Turbulence and Reynolds number
    - Examples of turbulence
    - Heat and momentum transfer
    - "Kolmogorov's theory of 1941"
    - References and notes
    - General
    - Original scientific research papers and classic monographs
categories:
    - Chaos theory
---
In fluid dynamics, turbulence or turbulent flow is a flow regime characterized by chaotic changes in pressure and flow velocity. In contrast to laminar flow, turbulence is associated with high Reynolds numbers,[1]:2 where inertial forces dominate viscous forces.
