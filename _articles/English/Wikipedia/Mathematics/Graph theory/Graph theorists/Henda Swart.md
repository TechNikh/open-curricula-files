---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Henda_Swart
offline_file: ""
offline_thumbnail: ""
uuid: e99cf353-90f5-4d02-8d63-e0f7634c948d
updated: 1484309475
title: Henda Swart
categories:
    - Graph theorists
---
Hendrika Cornelia Scott (Henda) Swart FRSSAf (born 1939, died 24 February 2016 [age 77-78])[citation needed] was a South African mathematician, a professor emeritus of mathematics at the University of KwaZulu-Natal and a professor at the University of Cape Town[1][2]
