---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Champernowne_constant
offline_file: ""
offline_thumbnail: ""
uuid: 2a60f1cb-4d8c-4dad-a509-b64d57f0d2df
updated: 1484308629
title: Champernowne constant
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Champernowne_constant.svg.png
tags:
    - Normality
    - Continued fraction expansion
    - Irrationality measure
categories:
    - Number theory
---
In mathematics, the Champernowne constant C10 is a transcendental real constant whose decimal expansion has important properties. It is named after economist and mathematician D. G. Champernowne, who published it as an undergraduate in 1933.
