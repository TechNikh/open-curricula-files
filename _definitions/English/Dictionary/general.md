---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/general
offline_file: ""
offline_thumbnail: ""
uuid: cab956b2-4936-44e3-a6ba-77c35d85ff53
updated: 1484310344
title: general
categories:
    - Dictionary
---
general
     adj 1: applying to all or most members of a category or group; "the
            general public"; "general assistance"; "a general
            rule"; "in general terms"; "comprehensible to the
            general reader" [ant: {specific}]
     2: not specialized or limited to one class of things; "general
        studies"; "general knowledge"
     3: of national scope; "a general election"
     4: prevailing among and common to the general public; "the
        general discontent"
     5: affecting the entire body; "a general anesthetic"; "general
        symptoms" [ant: {local}]
     6: somewhat indefinite; "bearing a general resemblance to the
        original"; "a general ...
