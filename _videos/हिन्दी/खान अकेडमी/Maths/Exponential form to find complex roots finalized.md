---
version: 1
type: video
provider: YouTube
id: ydiD6EaJP6g
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cf68d493-8088-4484-9260-689983425f61
updated: 1486069607
title: Exponential form to find complex roots finalized
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ydiD6EaJP6g/default.jpg
    - https://i3.ytimg.com/vi/ydiD6EaJP6g/1.jpg
    - https://i3.ytimg.com/vi/ydiD6EaJP6g/2.jpg
    - https://i3.ytimg.com/vi/ydiD6EaJP6g/3.jpg
---
