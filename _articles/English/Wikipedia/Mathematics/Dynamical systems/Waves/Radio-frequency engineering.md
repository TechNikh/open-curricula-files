---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Radio-frequency_engineering
offline_file: ""
offline_thumbnail: ""
uuid: 3e8c9e26-ae6e-438c-823d-a329638438bd
updated: 1484308894
title: Radio-frequency engineering
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Radio_icon_1.png
tags:
    - Radio electronics
    - Duties
    - Early radio-frequency engineers
categories:
    - Waves
---
Radio-frequency engineering is a subset of electrical engineering that deals with devices that are designed to operate in the radio frequency (RF) spectrum. These devices operate within the range of about 3 kHz up to 300 GHz.
