---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Usa
offline_file: ""
offline_thumbnail: ""
uuid: ff7e091e-f06b-4a44-a117-080a6f39c794
updated: 1484308444
title: United States
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Chromesun_kincaid_site_01.jpg
tags:
    - Etymology
    - History
    - Indigenous and European contact
    - Settlements
    - Independence and expansion (1776–1865)
    - Civil War and Reconstruction Era
    - Industrialization
    - World War I, Great Depression, and World War II
    - Cold War and civil rights era
    - Contemporary history
    - Geography, climate, and environment
    - wildlife
    - Demographics
    - population
    - language
    - religion
    - Family structure
    - Government and politics
    - Political divisions
    - Parties and elections
    - Foreign relations
    - Government finance
    - Military
    - Law enforcement and crime
    - Economy
    - Income, poverty and wealth
    - Infrastructure
    - Transportation
    - Energy
    - Water supply and sanitation
    - Education
    - culture
    - Food
    - Literature, philosophy, and the arts
    - Music
    - Cinema
    - Sports
    - Media
    - science and technology
    - Health
    - Notes
    - Bibliography and further reading
    - Website sources
categories:
    - Uncategorized
---
The United States of America (USA), commonly referred to as the United States (U.S.) or America, is a federal republic composed of 50 states, a federal district, five major self-governing territories, and various possessions.[fn 1] Forty-eight of the fifty states and the federal district are contiguous and located in North America between Canada and Mexico. The state of Alaska is in the far northwestern corner of North America, with a land border to the east with Canada and separated by the Bering Strait from Russia. The state of Hawaii is an archipelago in the mid-Pacific. The territories of the U.S are scattered about the Pacific Ocean and the Caribbean Sea. Nine time zones are covered. ...
