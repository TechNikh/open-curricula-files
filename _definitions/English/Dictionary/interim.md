---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interim
offline_file: ""
offline_thumbnail: ""
uuid: c97f3768-a068-4dd6-8550-b594da9ebde7
updated: 1484310591
title: interim
categories:
    - Dictionary
---
interim
     adj : serving during an intermediate interval of time; "an interim
           agreement"
     n : the time between one event, process, or period and another
         [syn: {lag}]
