---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hereditary_set
offline_file: ""
offline_thumbnail: ""
uuid: 46c12c09-2fc0-4c86-89d2-3dbd315179c0
updated: 1484309506
title: Hereditary set
tags:
    - Examples
    - In formulations of set theory
    - Assumptions
categories:
    - Set theory
---
In set theory, a hereditary set (or pure set) is a set whose elements are all hereditary sets. That is, all elements of the set are themselves sets, as are all elements of the elements, and so on.
