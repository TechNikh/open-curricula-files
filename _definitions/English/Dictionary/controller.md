---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/controller
offline_file: ""
offline_thumbnail: ""
uuid: 95389e61-39cd-449e-882e-cf7438edc255
updated: 1484310611
title: controller
categories:
    - Dictionary
---
controller
     n 1: someone who maintains and audits business accounts [syn: {accountant},
           {comptroller}]
     2: a person who directs and restrains [syn: {restrainer}]
     3: a mechanism that controls the operation of a machine; "the
        speed control on his turntable was not working properly";
        "I turned the controls over to her" [syn: {control}]
