---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tennis
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484392441
title: tennis
categories:
    - Dictionary
---
tennis
     n : a game played with rackets by two or four players who hit a
         ball back and forth over a net that divides the court
         [syn: {lawn tennis}]
