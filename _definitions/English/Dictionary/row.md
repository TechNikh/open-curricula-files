---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/row
offline_file: ""
offline_thumbnail: ""
uuid: ca0d696c-191c-4d40-aa38-53c850450aaf
updated: 1484310393
title: row
categories:
    - Dictionary
---
row
     n 1: an arrangement of objects or people side by side in a line;
          "a row of chairs"
     2: an angry dispute; "they had a quarrel"; "they had words"
        [syn: {quarrel}, {wrangle}, {words}, {run-in}, {dustup}]
     3: a long continuous strip (usually running horizontally); "a
        mackerel sky filled with rows of clouds"; "rows of barbed
        wire protected the trenches"
     4: (construction) a layer of masonry; "a course of bricks"
        [syn: {course}]
     5: a linear array of numbers side by side
     6: a continuous chronological succession without an
        interruption; "they won the championship three years in a
        row"
     7: the act of rowing ...
