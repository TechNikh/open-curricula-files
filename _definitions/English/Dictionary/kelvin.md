---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kelvin
offline_file: ""
offline_thumbnail: ""
uuid: 505da1ec-4f88-4e6c-8180-5df520e0a6bc
updated: 1484310232
title: kelvin
categories:
    - Dictionary
---
kelvin
     n 1: the basic unit of thermodynamic temperature adopted under
          the Systeme International d'Unites [syn: {K}]
     2: British physicist who invented the Kelvin scale of
        temperature and pioneered undersea telegraphy (1824-1907)
        [syn: {First Baron Kelvin}, {William Thompson}]
