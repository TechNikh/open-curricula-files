---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lambda_transition
offline_file: ""
offline_thumbnail: ""
uuid: f3609aba-e276-4916-a0b6-05386093ea8a
updated: 1484308749
title: Lambda transition
tags:
    - Books
    - Journal articles
categories:
    - Critical phenomena
---
The λ (lambda) universality class is probably the most important group in condensed matter physics. It regroups several systems possessing strong analogies, namely, superfluids, superconductors and smectics (liquid crystals). All these systems are expected to belong to the same universality class for the thermodynamic critical properties of the phase transition. While these systems are quite different at the first glance, they all are described by similar formalisms and their typical phase diagrams are identical.
