---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drunkenness
offline_file: ""
offline_thumbnail: ""
uuid: 4a93a0ee-2ed2-4239-9645-faea9384fff5
updated: 1484310424
title: drunkenness
categories:
    - Dictionary
---
drunkenness
     n 1: a temporary state resulting from excessive consumption of
          alcohol [syn: {inebriation}, {inebriety}, {intoxication},
           {tipsiness}] [ant: {soberness}]
     2: Habitual intoxication; prolonged and excessive intake of
        alcoholic drinks leading to a breakdown in health and an
        addiction to alcohol such that abrupt deprivation leads to
        severe withdrawal symptoms [syn: {alcoholism}, {alcohol
        addiction}, {inebriation}]
     3: the act of drinking alcoholic beverages to excess; "drink
        was his downfall" [syn: {drink}, {drinking}, {boozing}, {crapulence}]
