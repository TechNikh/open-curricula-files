---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/International_Society_for_the_Interdisciplinary_Study_of_Symmetry
offline_file: ""
offline_thumbnail: ""
uuid: dd7ce7cf-4f52-405f-b529-6e272b358cf7
updated: 1484309312
title: >
    International Society for the Interdisciplinary Study of
    Symmetry
categories:
    - Symmetry
---
The International Society for the Interdisciplinary Study of Symmetry ("International Symmetry Society") provides a central forum for the scholarly study of symmetry. The Society's community comprises several branches of science and art, while symmetry studies have gained the rank of an individual interdisciplinary field in the judgement of the scientific community. The Society has members in all continents, in over forty countries.
