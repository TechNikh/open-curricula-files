---
version: 1
type: article
id: https://en.wikipedia.org/wiki/DockNET
offline_file: ""
offline_thumbnail: ""
uuid: d37f3b99-c8fe-494e-b219-e802c61f5b66
updated: 1484308727
title: DockNET
categories:
    - Control engineering
---
The control system was created for being able to analyze, communicate, supervise, direct, record and to document everything that happens in and around a loading door hole in a modern cargo terminal. Cargo terminals often suffer damage to loading houses and vehicles incurred in connection with drive into and away from the loading house. This is usually not just mistake out of the driver, but lack of communication between driver and loading dock. DockNET solves this lack of communication and secures while running the work environment and cargo handling. The result is increased efficiency where technology makes everything measurable around the loading space.
