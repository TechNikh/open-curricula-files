---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bartell_mechanism
offline_file: ""
offline_thumbnail: ""
uuid: 8b3d8f4e-cb93-4978-8153-f09eee869539
updated: 1484309277
title: Bartell mechanism
categories:
    - Molecular geometry
---
The Bartell mechanism is a pseudorotational mechanism similar to the Berry mechanism. It occurs only in molecules with a pentagonal bipyramidal molecular geometry, such as IF7. This mechanism was first predicted by H. B. Bartell. The mechanism exchanges the axial atoms with one pair of the equatorial atoms with an energy requirement of about 2.7 kcal/mol. Similarly to the Berry mechanism in square planar molecules, the symmetry of the intermediary phase of the vibrational mode is "chimeric"[1][2][3] of other mechanisms; it displays characteristics of the Berry mechanism, a "lever" mechanism seen in pseudorotation of disphenoidal molecules, and a "turnstile" mechanism (which can be seen in ...
