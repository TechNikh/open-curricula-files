---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flask
offline_file: ""
offline_thumbnail: ""
uuid: d96a8467-5de3-493f-9dcf-90a5e7017fc1
updated: 1484310371
title: flask
categories:
    - Dictionary
---
flask
     n 1: bottle that has a narrow neck
     2: the quantity a flask will hold [syn: {flaskful}]
