---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ester
offline_file: ""
offline_thumbnail: ""
uuid: 2b6a5c98-d6e7-4452-8889-d298bc9f8723
updated: 1484310419
title: ester
categories:
    - Dictionary
---
ester
     n : formed by reaction between an acid and an alcohol with
         elimination of water
