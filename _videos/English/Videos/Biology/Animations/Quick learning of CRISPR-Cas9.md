---
version: 1
type: video
provider: YouTube
id: 0dRT7slyGhs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Quick%20learning%20of%20CRISPR-Cas9.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ce66755d-aaf9-4904-9fd0-13348633573a
updated: 1486069672
title: Quick learning of CRISPR/Cas9
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/0dRT7slyGhs/default.jpg
    - https://i3.ytimg.com/vi/0dRT7slyGhs/1.jpg
    - https://i3.ytimg.com/vi/0dRT7slyGhs/2.jpg
    - https://i3.ytimg.com/vi/0dRT7slyGhs/3.jpg
---
