---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trihydroxy
offline_file: ""
offline_thumbnail: ""
uuid: dd186426-49eb-4d92-9cd7-738e90c69df2
updated: 1484310426
title: trihydroxy
categories:
    - Dictionary
---
trihydroxy
     adj : containing three hydroxyl groups
