---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/protection
offline_file: ""
offline_thumbnail: ""
uuid: da8f9003-1b83-4010-8bfb-1cb8ed535996
updated: 1484310363
title: protection
categories:
    - Dictionary
---
protection
     n 1: the activity of protecting someone or something; "the
          witnesses demanded police protection"
     2: a covering that is intend to protect from damage or injury;
        "they had no protection from the fallout"; "wax provided
        protection for the floors" [syn: {protective covering}, {protective
        cover}]
     3: defense against financial failure; financial independence;
        "his pension gave him security in his old age"; "insurance
        provided protection against loss of wages due to illness"
        [syn: {security}]
     4: the condition of being protected; "they were huddled
        together for protection"; "he enjoyed a sense of peace ...
