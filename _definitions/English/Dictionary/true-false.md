---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/true-false
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484393161
title: true-false
categories:
    - Dictionary
---
true-false
     adj : offering a series of statements each of which is to be
           judged as true or false; "a true-false test" [ant: {multiple-choice}]
