---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eminent
offline_file: ""
offline_thumbnail: ""
uuid: 037a5953-07b6-499e-9d6a-115941645222
updated: 1484310431
title: eminent
categories:
    - Dictionary
---
eminent
     adj 1: (used of persons) standing above others in character or
            attainment or reputation; "our distinguished
            professor"; "an eminent scholar"; "a great statesman"
            [syn: {distinguished}, {great}]
     2: standing above others in quality or position; "people in
        high places"; "the high priest"; "eminent members of the
        community" [syn: {high}]
     3: having achieved eminence; "an eminent physician"
     4: of imposing height; especially standing out above others;
        "an eminent peak"; "lofty mountains"; "the soaring spires
        of the cathedral"; "towering iceburgs" [syn: {lofty}, {soaring},
         {towering}]
