---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/firing
offline_file: ""
offline_thumbnail: ""
uuid: 070d3026-1cc3-4acc-a5fb-e4256e79500a
updated: 1484310557
title: firing
categories:
    - Dictionary
---
firing
     n 1: the act of firing weapons or artillery at an enemy; "hold
          your fire until you can see the whites of their eyes";
          "they retreated in the face of withering enemy fire"
          [syn: {fire}]
     2: the act of discharging a gun [syn: {discharge}, {firing off}]
     3: the act of setting on fire or catching fire [syn: {ignition},
         {lighting}, {kindling}, {inflammation}]
     4: the termination of someone's employment (leaving them free
        to depart) [syn: {dismissal}, {dismission}, {discharge}, {liberation},
         {release}, {sack}, {sacking}]
