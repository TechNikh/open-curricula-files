---
version: 1
type: video
provider: YouTube
id: 32QvIrKD_cA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 042bb203-21c9-4684-8f20-06cdda372492
updated: 1486069607
title: Example exercises on ways to add two digit numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/32QvIrKD_cA/default.jpg
    - https://i3.ytimg.com/vi/32QvIrKD_cA/1.jpg
    - https://i3.ytimg.com/vi/32QvIrKD_cA/2.jpg
    - https://i3.ytimg.com/vi/32QvIrKD_cA/3.jpg
---
