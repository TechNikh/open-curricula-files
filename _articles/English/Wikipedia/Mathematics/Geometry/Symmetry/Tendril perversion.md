---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tendril_perversion
offline_file: ""
offline_thumbnail: ""
uuid: 36a0a86b-0016-4d44-9b2d-11a266d124b5
updated: 1484309317
title: Tendril perversion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-PSM_V17_D663_A_caught_tendril_of_bryonia_dioica.jpg
categories:
    - Symmetry
---
Tendril perversion, often referred to in context as simply perversion, is a geometric phenomenon found in helical structures such as plant tendrils, in which a helical structure forms that is divided into two sections of opposite chirality, with a transition between the two in the middle.[1] A similar phenomenon can often be observed in kinked helical cables such as telephone handset cords.[2]
