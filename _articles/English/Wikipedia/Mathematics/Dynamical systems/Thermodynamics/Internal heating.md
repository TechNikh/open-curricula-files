---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Internal_heating
uuid: 53fe83cf-a449-4019-8b45-fef01cbc58ad
updated: 1480455144
title: Internal heating
tags:
    - Small celestial objects
    - Planets
    - Terrestrial planets
    - Gas giants
    - Brown dwarfs
    - Stars
categories:
    - Thermodynamics
---
Internal heat is the heat source from the interior of celestial objects, such as stars, brown dwarfs, planets, moons, dwarf planets, and (in the early history of the Solar System) even asteroids such as Vesta, resulting from contraction caused by gravity (the Kelvin–Helmholtz mechanism), nuclear fusion, tidal heating, core solidification (heat of fusion released as molten core material solidifies), and radioactive decay. The amount of internal heating depends on mass; the more massive the object, the more internal heat it has; also, for a given density, the more massive the object, the greater the ratio of mass to surface area, and thus the greater the retention of internal heat.[citation ...
