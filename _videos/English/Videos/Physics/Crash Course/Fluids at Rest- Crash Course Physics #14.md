---
version: 1
type: video
provider: YouTube
id: b5SqYuWT4-4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b02336bf-200a-4e46-abef-ec3af5c27781
updated: 1486069682
title: 'Fluids at Rest- Crash Course Physics #14'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/b5SqYuWT4-4/default.jpg
    - https://i3.ytimg.com/vi/b5SqYuWT4-4/1.jpg
    - https://i3.ytimg.com/vi/b5SqYuWT4-4/2.jpg
    - https://i3.ytimg.com/vi/b5SqYuWT4-4/3.jpg
---
