---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Lusin%27s_separation_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: ee588495-e2a3-4489-9930-339d3c99e425
updated: 1484309001
title: "Lusin's separation theorem"
categories:
    - Theorems in the foundations of mathematics
---
In descriptive set theory and mathematical logic, Lusin's separation theorem states that if A and B are disjoint analytic subsets of Polish space, then there is a Borel set C in the space such that A ⊆ C and B ∩ C = ∅.[1] It is named after Nikolai Luzin, who proved it in 1927.[2]
