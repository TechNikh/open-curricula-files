---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Divide_and_rule
offline_file: ""
offline_thumbnail: ""
uuid: 5aa6746a-c3af-4003-a1f9-a9625e4dfaca
updated: 1484309063
title: Divide and rule
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Philip-ii-of-macedon.jpg
tags:
    - In the workplace
    - Historical examples
    - Africa
    - Asia
    - Mongolian Empire
    - Indian subcontinent
    - Middle East
    - Europe
    - Mexico
categories:
    - Strategy
---
Divide and rule (or divide and conquer, from Latin dīvide et īmpera) in politics and sociology is gaining and maintaining power by breaking up larger concentrations of power into pieces that individually have less power than the one implementing the strategy. The concept refers to a strategy that breaks up existing power structures, and especially prevents smaller power groups from linking up, causing rivalries and fomenting discord among the people.[1]
