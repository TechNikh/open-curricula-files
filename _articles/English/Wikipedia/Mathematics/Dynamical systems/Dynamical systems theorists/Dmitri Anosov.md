---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dmitri_Anosov
offline_file: ""
offline_thumbnail: ""
uuid: fad8a680-0da2-4cea-a5f3-ea52431fcd96
updated: 1484308690
title: Dmitri Anosov
categories:
    - Dynamical systems theorists
---
Dmitri Victorovich Anosov (Russian: Дми́трий Ви́кторович Ано́сов; November 30, 1936 – August 5, 2014) was a Soviet and Russian mathematician, known for his contributions to dynamical systems theory.[1]
