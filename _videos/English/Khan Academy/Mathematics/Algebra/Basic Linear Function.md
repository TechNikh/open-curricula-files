---
version: 1
type: video
provider: YouTube
id: YB1XuQ1Pc5s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Basic%20Linear%20Function.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 58d89add-0ec9-4be1-83c3-bc9cf04c418e
updated: 1486069557
title: Basic Linear Function
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/YB1XuQ1Pc5s/default.jpg
    - https://i3.ytimg.com/vi/YB1XuQ1Pc5s/1.jpg
    - https://i3.ytimg.com/vi/YB1XuQ1Pc5s/2.jpg
    - https://i3.ytimg.com/vi/YB1XuQ1Pc5s/3.jpg
---
