---
version: 1
type: video
provider: YouTube
id: 1b4mr3vPNbk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/How%20RNA%20is%20transcripted%20from%20DNA.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c364c169-4008-4262-b6cf-c1be34c5cac2
updated: 1486069672
title: How RNA is transcripted from DNA?
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/1b4mr3vPNbk/default.jpg
    - https://i3.ytimg.com/vi/1b4mr3vPNbk/1.jpg
    - https://i3.ytimg.com/vi/1b4mr3vPNbk/2.jpg
    - https://i3.ytimg.com/vi/1b4mr3vPNbk/3.jpg
---
