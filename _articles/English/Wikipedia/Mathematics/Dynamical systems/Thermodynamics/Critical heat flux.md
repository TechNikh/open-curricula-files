---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Critical_heat_flux
offline_file: ""
offline_thumbnail: ""
uuid: 1bb6a540-9e94-4f46-bb07-275cd454056d
updated: 1484308840
title: Critical heat flux
tags:
    - Description
    - Correlations for critical heat flux
    - Applications in heat transfer
    - Terminology
categories:
    - Thermodynamics
---
Critical heat flux describes the thermal limit of a phenomenon where a phase change occurs during heating (such as bubbles forming on a metal surface used to heat water), which suddenly decreases the efficiency of heat transfer, thus causing localised overheating of the heating surface.
