---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fenugreek
offline_file: ""
offline_thumbnail: ""
uuid: 899fffa1-fd8e-4e5b-bbb8-000c23d70132
updated: 1484310353
title: fenugreek
categories:
    - Dictionary
---
fenugreek
     n 1: annual herb or southern Europe and eastern Asia having
          off-white flowers and aromatic seeds used medicinally
          and in curry [syn: {Greek clover}, {Trigonella
          foenumgraecum}]
     2: aromatic seeds used as seasoning especially in curry [syn: {fenugreek
        seed}]
