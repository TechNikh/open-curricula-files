---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bloch_wave
offline_file: ""
offline_thumbnail: ""
uuid: bbdae8c5-ad6d-4652-abcf-567f4b6b96dd
updated: 1484308887
title: Bloch wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-BlochWave_in_Silicon.png
tags:
    - Applications and consequences
    - Applicability
    - Meaning and non-uniqueness of the k-vector
    - Detailed example
    - "Proof of Bloch's theorem"
    - 'Preliminaries: Crystal symmetries, lattice, and reciprocal lattice'
    - Lemma about translation operators
    - Proof
    - History and related equations
categories:
    - Waves
---
A Bloch wave (also called Bloch state or Bloch function or Bloch wave function), named after Swiss physicist Felix Bloch, is a type of wavefunction for a particle in a periodically-repeating environment, most commonly an electron in a crystal. A wavefunction ψ is a Bloch wave if it has the form:[1]
