---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/realise
offline_file: ""
offline_thumbnail: ""
uuid: dba2b395-0835-4477-ac6c-7ff637409b6d
updated: 1484310206
title: realise
categories:
    - Dictionary
---
realise
     v 1: earn on some commercial or business transaction; earn as
          salary or wages; "How much do you make a month in your
          new job?"; "She earns a lot in her new job"; "this
          merger brought in lots of money"; "He clears $5,000 each
          month" [syn: {gain}, {take in}, {clear}, {make}, {earn},
           {realize}, {pull in}, {bring in}]
     2: convert into cash; of goods and property [syn: {realize}]
     3: expand or complete (a thorough-based part in a piece of
        baroque music) by supplying the harmonies indicated in the
        figured bass [syn: {realize}]
     4: make real or concrete; give reality or substance to; "our
        ideas must ...
