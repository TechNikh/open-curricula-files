---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recovering
offline_file: ""
offline_thumbnail: ""
uuid: bb009d71-cfe6-4116-8303-d8b23f7ef06b
updated: 1484310567
title: recovering
categories:
    - Dictionary
---
recovering
     adj : returning to health after illness or debility; "convalescent
           children are difficult to keep in bed" [syn: {convalescent}]
