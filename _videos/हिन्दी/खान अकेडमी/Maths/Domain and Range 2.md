---
version: 1
type: video
provider: YouTube
id: kS_WVgLMjDg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 55d80528-8360-4a83-ab6e-f3aba509fe56
updated: 1486069617
title: Domain and Range 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/kS_WVgLMjDg/default.jpg
    - https://i3.ytimg.com/vi/kS_WVgLMjDg/1.jpg
    - https://i3.ytimg.com/vi/kS_WVgLMjDg/2.jpg
    - https://i3.ytimg.com/vi/kS_WVgLMjDg/3.jpg
---
