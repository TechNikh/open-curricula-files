---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hunting_oscillation
offline_file: ""
offline_thumbnail: ""
uuid: 4756a175-39c5-4377-81a7-a3192dc6cc0b
updated: 1484308823
title: Hunting oscillation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Newkinematics.png
tags:
    - Railway wheelsets
    - Kinematic analysis
    - Assumptions and non-mathematical description
    - Mathematical analysis
    - Energy balance
    - Limitation of simplified analysis
    - Road-Rail vehicles
    - Notes
categories:
    - Oscillation
---
Hunting oscillation is a self-oscillation, usually unwanted, about an equilibrium.[1] The expression came into use in the 19th century and describes how a system "hunts" for equilibrium.[1] The expression is used to describe phenomena in such diverse fields as electronics, aviation, biology, and railway engineering.[1]
