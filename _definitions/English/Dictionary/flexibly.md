---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flexibly
offline_file: ""
offline_thumbnail: ""
uuid: fe6b1a78-4fa0-486f-93dc-2bf6c541f462
updated: 1484310529
title: flexibly
categories:
    - Dictionary
---
flexibly
     adv : with flexibility; "`Come whenever you are free,' he said
           flexibly" [ant: {inflexibly}]
