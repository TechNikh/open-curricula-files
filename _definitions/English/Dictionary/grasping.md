---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grasping
offline_file: ""
offline_thumbnail: ""
uuid: 34b055be-0dc7-438f-a8de-ff8ead818f11
updated: 1484310283
title: grasping
categories:
    - Dictionary
---
grasping
     adj : immoderately desirous of acquiring e.g. wealth; "they are
           avaricious and will do anything for money"; "casting
           covetous eyes on his neighbor's fields"; "a grasping
           old miser"; "grasping commercialism"; "greedy for money
           and power"; "grew richer and greedier"; "prehensile
           employers stingy with raises for their employees" [syn:
            {avaricious}, {covetous}, {grabby}, {greedy}, {prehensile}]
     n 1: understanding with difficulty; "the lecture was beyond his
          most strenuous graspings"
     2: the act of gripping something firmly with the hands [syn: {taking
        hold}, {seizing}, {prehension}]
