---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resultant
offline_file: ""
offline_thumbnail: ""
uuid: f7974f4c-7b09-4d13-ab3b-ca915c7f405e
updated: 1484310365
title: resultant
categories:
    - Dictionary
---
resultant
     adj : following as an effect or result; "the period of tension and
           consequent need for military preparedness"; "the
           ensuant response to his appeal"; "the resultant savings
           were considerable"; "the health of the plants and the
           resulting flowers" [syn: {consequent}, {ensuant}, {resulting(a)},
            {sequent}]
     n 1: the final point in a process [syn: {end point}]
     2: something that results; "he listened for the results on the
        radio" [syn: {result}, {final result}, {outcome}, {termination}]
     3: a vector that is the sum of two or more other vectors [syn:
        {vector sum}]
