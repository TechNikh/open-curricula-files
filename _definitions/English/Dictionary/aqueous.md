---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aqueous
offline_file: ""
offline_thumbnail: ""
uuid: 84571c1b-3891-49a9-b306-b562a8711cd4
updated: 1484310373
title: aqueous
categories:
    - Dictionary
---
aqueous
     adj : similar to or containing or dissolved in water; "aqueous
           solutions"
