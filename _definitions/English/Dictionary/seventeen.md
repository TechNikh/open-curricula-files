---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seventeen
offline_file: ""
offline_thumbnail: ""
uuid: 3a52fd17-d7b9-400b-9512-21225ef5d6e5
updated: 1484310395
title: seventeen
categories:
    - Dictionary
---
seventeen
     adj : being one more than sixteen [syn: {17}, {xvii}]
     n : the cardinal number that is the sum of sixteen and one [syn:
          {17}, {XVII}]
