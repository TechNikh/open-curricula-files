---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Buyer%27s_premium'
offline_file: ""
offline_thumbnail: ""
uuid: 5e806636-5f1e-4081-8c91-18d37173c42f
updated: 1484309010
title: "Buyer's premium"
categories:
    - Auctions
---
In auctions, the buyer's premium is a percentage additional charge on the hammer price (winning bid at auction) of the lot that must be paid by the winner. It is charged by the auctioneer to cover administrative expenses. The buyer's premium goes directly to the auction house and not to the seller.
