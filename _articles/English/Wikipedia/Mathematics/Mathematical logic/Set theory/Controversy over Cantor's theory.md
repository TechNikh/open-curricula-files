---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Controversy_over_Cantor%27s_theory'
offline_file: ""
offline_thumbnail: ""
uuid: 1a876573-a83d-498b-935f-b019407b85d7
updated: 1484309504
title: "Controversy over Cantor's theory"
tags:
    - "Cantor's argument"
    - Reception of the argument
    - Objection to the axiom of infinity
    - Notes
categories:
    - Set theory
---
In mathematical logic, the theory of infinite sets was first developed by Georg Cantor. Although this work has become a thoroughly standard fixture of classical set theory, it has been criticized in several areas by mathematicians and philosophers.
