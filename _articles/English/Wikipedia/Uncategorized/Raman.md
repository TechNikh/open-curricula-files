---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Raman
offline_file: ""
offline_thumbnail: ""
uuid: fc1b683a-4db0-4dc4-8485-11dda4d9159f
updated: 1484308409
title: Raman
tags:
    - people
    - Family name
    - 'Given name: Indian origin'
    - 'Given name: Belarusian origin'
    - 'Given name: Other or uncertain origin'
    - Places, structures and organisations
    - Other uses
categories:
    - Uncategorized
---
Raman is a name of Indian origin, used both as a family name and as both a feminine and a masculine given name. Raman is a masculine given name of Belarusian origin. There may be other origins also.
