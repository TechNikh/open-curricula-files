---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peaceful
offline_file: ""
offline_thumbnail: ""
uuid: 54b1e589-f476-4ffb-8452-6857613dfa38
updated: 1484310249
title: peaceful
categories:
    - Dictionary
---
peaceful
     adj 1: not disturbed by strife or turmoil or war; "a peaceful
            nation"; "peaceful times"; "a far from peaceful
            Christmas"; "peaceful sleep" [ant: {unpeaceful}]
     2: peacefully resistant in response to injustice; "passive
        resistance" [syn: {passive}]
     3: (of groups) not violent or disorderly; "the right of
        peaceful assembly" [syn: {law-abiding}]
