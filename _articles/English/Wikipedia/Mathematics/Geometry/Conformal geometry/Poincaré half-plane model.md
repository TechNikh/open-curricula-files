---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poincar%C3%A9_half-plane_model'
offline_file: ""
offline_thumbnail: ""
uuid: 8c0077e4-8e01-409d-a9cc-db200cb179af
updated: 1484309185
title: Poincaré half-plane model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Parallel_rays_in_Poincare_model_of_hyperbolic_geometry.svg.png
tags:
    - Metric
    - Distance calculation
    - Special points and curves
    - Compass and straightedge constructions
    - Creating the line through two existing points
    - >
        Creating the circle through one point with center another
        point
    - Given a circle find its (hyperbolic) center
    - Other constructions
    - Symmetry groups
    - Isometric symmetry
    - Geodesics
    - The model in three dimensions
    - The model in n dimensions
categories:
    - Conformal geometry
---
In non-Euclidean geometry, the Poincaré half-plane model is the upper half-plane, denoted below as H 
  
    
      
        {
        (
        x
        ,
        y
        )
        
          |
        
        y
        >
        0
        ;
        x
        ,
        y
        ∈
        
          R
        
        }
      
    
    {\displaystyle \{(x,y)|y>0;x,y\in \mathbb {R} \}}
  
 , together with a metric, the Poincaré metric, that makes it a model of two-dimensional hyperbolic geometry.
