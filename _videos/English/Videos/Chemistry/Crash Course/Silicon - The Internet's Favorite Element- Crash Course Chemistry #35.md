---
version: 1
type: video
provider: YouTube
id: kdy3RsZk7As
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Silicon%20-%20The%20Internet%27s%20Favorite%20Element-%20Crash%20Course%20Chemistry%20%2335.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6a4c1d45-ea68-4350-89fa-e8463913543e
updated: 1486069678
title: "Silicon - The Internet's Favorite Element- Crash Course Chemistry #35"
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/kdy3RsZk7As/default.jpg
    - https://i3.ytimg.com/vi/kdy3RsZk7As/1.jpg
    - https://i3.ytimg.com/vi/kdy3RsZk7As/2.jpg
    - https://i3.ytimg.com/vi/kdy3RsZk7As/3.jpg
---
