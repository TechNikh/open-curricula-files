---
version: 1
type: video
provider: YouTube
id: 2M9cVwj3AtI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/The%20Apostrophe/Advanced%20%28plural%29%20possession.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 41f916f0-b62b-468a-a6b1-dfc73422f307
updated: 1486069601
title: Advanced (plural) possession
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The Apostrophe
thumbnail_urls:
    - https://i3.ytimg.com/vi/2M9cVwj3AtI/default.jpg
    - https://i3.ytimg.com/vi/2M9cVwj3AtI/1.jpg
    - https://i3.ytimg.com/vi/2M9cVwj3AtI/2.jpg
    - https://i3.ytimg.com/vi/2M9cVwj3AtI/3.jpg
---
Paige and David talk about using apostrophes to show that something belongs to multiple people. 

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/e/advanved--plural--possesion

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/apostrophes-and-plurals-the-apostrophe-punctuation-khan-academy

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/punctuation/the-apostrophe/v/possession-for-words-ending-in-s-the-apostrophe-punctuation-khan-academy

Punctuation on Khan Academy: Punctuation is the collection of squiggles, dots, and lines that we use to separate sentences and their parts.  Is a question mark punctuation?  Yes, and so is an exclamation point!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
