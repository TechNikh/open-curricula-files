---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/G%C3%B6del%27s_completeness_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 4dd390c9-b296-4ffb-8354-f67514c22c15
updated: 1484309001
title: "Gödel's completeness theorem"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Completude%25CC%25A0_logique_premier_ordre.png'
tags:
    - Statement of the theorem
    - Preliminaries
    - "Gödel's original formulation"
    - Model existence theorem
    - More general form
    - As a theorem of arithmetic
    - Consequences
    - Relationship to the incompleteness theorem
    - Relationship to the compactness theorem
    - Completeness in other logics
    - Proofs
categories:
    - Theorems in the foundations of mathematics
---
Gödel's completeness theorem is a fundamental theorem in mathematical logic that establishes a correspondence between semantic truth and syntactic provability in first-order logic. It makes a close link between model theory that deals with what is true in different models, and proof theory that studies what can be formally proven in particular formal systems.
