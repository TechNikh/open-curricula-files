---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enter
offline_file: ""
offline_thumbnail: ""
uuid: 775404e0-30ff-4a05-98b8-f9c727c9713e
updated: 1484310264
title: enter
categories:
    - Dictionary
---
enter
     v 1: to come or go into; "the boat entered an area of shallow
          marshes" [syn: {come in}, {get into}, {get in}, {go into},
           {go in}, {move into}] [ant: {exit}]
     2: become a participant; be involved in; "enter a race"; "enter
        an agreement"; "enter a drug treatment program"; "enter
        negotiations" [syn: {participate}] [ant: {drop out}]
     3: register formally as a participant or member; "The party
        recruited many new members" [syn: {enroll}, {inscribe}, {enrol},
         {recruit}]
     4: be or play a part of or in; "Elections figure prominently in
        every government program"; "How do the elections figure in
        the current ...
