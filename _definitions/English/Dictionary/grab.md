---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grab
offline_file: ""
offline_thumbnail: ""
uuid: 404e7431-86e4-452c-b49e-fe2a3e26921b
updated: 1484310366
title: grab
categories:
    - Dictionary
---
grab
     n 1: a mechanical device for gripping an object
     2: the act of catching an object with the hands; "Mays made the
        catch with his back to the plate"; "he made a grab for the
        ball before it landed"; "Martin's snatch at the bridle
        failed and the horse raced away"; "the infielder's snap
        and throw was a single motion" [syn: {catch}, {snatch}, {snap}]
     v 1: take hold of so as to seize or restrain or stop the motion
          of; "Catch the ball!"; "Grab the elevator door!" [syn: {catch},
           {take hold of}]
     2: get hold of or seize quickly and easily; "I snapped up all
        the good buys during the garage sale" [syn: {snap up}, ...
