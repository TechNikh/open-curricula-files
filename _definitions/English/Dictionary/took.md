---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/took
offline_file: ""
offline_thumbnail: ""
uuid: 5515fd88-5b8f-414e-808a-9afd209aa1bc
updated: 1484310453
title: took
categories:
    - Dictionary
---
take
     n 1: the income arising from land or other property; "the average
          return was about 5%" [syn: {return}, {issue}, {proceeds},
           {takings}, {yield}, {payoff}]
     2: the act of photographing a scene or part of a scene without
        interruption
     v 1: carry out; "take action"; "take steps"; "take vengeance"
     2: as of time or space; "It took three hours to get to work
        this morning"; "This event occupied a very short time"
        [syn: {occupy}, {use up}]
     3: take somebody somewhere; "We lead him to our chief"; "can
        you take me to the main entrance?"; "He conducted us to
        the palace" [syn: {lead}, {direct}, {conduct}, {guide}]
   ...
