---
version: 1
type: video
provider: YouTube
id: jexMSlSDubM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Parametric%20equations%20and%20polar%20coordinates/Polar%20coordinates%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2c5e622a-e55c-47f9-8230-3acffcbabb4e
updated: 1486069696
title: Polar coordinates 1
categories:
    - Parametric equations and polar coordinates
thumbnail_urls:
    - https://i3.ytimg.com/vi/jexMSlSDubM/default.jpg
    - https://i3.ytimg.com/vi/jexMSlSDubM/1.jpg
    - https://i3.ytimg.com/vi/jexMSlSDubM/2.jpg
    - https://i3.ytimg.com/vi/jexMSlSDubM/3.jpg
---
