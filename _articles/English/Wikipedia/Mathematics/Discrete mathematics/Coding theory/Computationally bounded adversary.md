---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Computationally_bounded_adversary
offline_file: ""
offline_thumbnail: ""
uuid: f55224a1-3427-4b3b-85f3-24b4883c7f30
updated: 1484308547
title: Computationally bounded adversary
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Illustration_of_Proof_for_Computationally_Bounded_Adversary.png
tags:
    - Comparison to other models
    - Worst-case model
    - Stochastic noise model
    - Applications
    - Comparison to stochastic noise channel
    - Specific applications
categories:
    - Coding theory
---
In information theory, the computationally bounded adversary problem is a different way of looking at the problem of sending data over a noisy channel. In previous models the best that could be done was ensuring correct decoding for up to d/2 errors, where d was the Hamming distance of the code. The problem with doing it this way is that it does not take into consideration the actual amount of computing power available to the adversary. Rather, it only concerns itself with how many bits of a given code word can change and still have the message decode properly. In the computationally bounded adversary model the channel – the adversary – is restricted to only being able to perform a ...
