---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hypercone
offline_file: ""
offline_thumbnail: ""
uuid: 2896484b-e988-42e6-9dec-fb5285d2e67b
updated: 1484309285
title: Hypercone
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Spherical_cone_lines.png
tags:
    - Parametric form
    - Geometrical interpretation
    - Construction
    - Temporal interpretation
categories:
    - Multi-dimensional geometry
---
It is a quadric surface, and is one of the possible 3-manifolds which are 4-dimensional equivalents of the conical surface in 3 dimensions. It is also named spherical cone because its intersections with hyperplanes perpendicular to the w-axis are spheres. A four-dimensional right spherical hypercone can be thought of as a sphere which expands with time, starting its expansion from a single point source, such that the center of the expanding sphere remains fixed. An oblique spherical hypercone would be a sphere which expands with time, again starting its expansion from a point source, but such that the center of the expanding sphere moves with a uniform velocity.
