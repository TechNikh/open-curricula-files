---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abstract
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484469361
title: abstract
categories:
    - Dictionary
---
abstract
     adj 1: existing only in the mind; separated from embodiment;
            "abstract words like `truth' and `justice'" [ant: {concrete}]
     2: not representing or imitating external reality or the
        objects of nature; "a large abstract painting" [syn: {abstractionist},
         {nonfigurative}, {nonobjective}]
     3: based on specialized theory; "a theoretical analysis" [syn:
        {theoretical}]
     4: dealing with a subject in the abstract without practical
        purpose or intention; "abstract reasoning"; "abstract
        science"
     n 1: a concept or idea not associated with any specific instance;
          "he loved her only in the abstract--not in person" ...
