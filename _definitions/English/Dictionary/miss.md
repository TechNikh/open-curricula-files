---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/miss
offline_file: ""
offline_thumbnail: ""
uuid: 727acc54-3914-4fb1-8879-44264d025e47
updated: 1484310152
title: miss
categories:
    - Dictionary
---
miss
     n 1: a young woman; "a young lady of 18" [syn: {girl}, {missy}, {young
          lady}, {young woman}, {fille}]
     2: a failure to hit (or meet or find etc) [syn: {misfire}]
     v 1: fail to perceive or to catch with the senses or the mind; "I
          missed that remark"; "She missed his point"; "We lost
          part of what he said" [syn: {lose}]
     2: feel or suffer from the lack of; "He misses his mother"
     3: fail to attend an event or activity; "I missed the concert";
        "He missed school for a week" [ant: {attend}]
     4: leave undone or leave out; "How could I miss that typo?";
        "The workers on the conveyor belt miss one out of ten"
        [syn: ...
