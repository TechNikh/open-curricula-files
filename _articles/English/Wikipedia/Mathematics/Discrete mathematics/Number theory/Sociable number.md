---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sociable_number
offline_file: ""
offline_thumbnail: ""
uuid: 1a34291e-ad35-4f6e-9e49-0b025f7dd901
updated: 1484308661
title: Sociable number
tags:
    - Example
    - List of known sociable numbers
    - Searching for sociable numbers
categories:
    - Number theory
---
Sociable numbers are numbers whose aliquot sums form a cyclic sequence that begins and ends with the same number. They are generalizations of the concepts of amicable numbers and perfect numbers. The first two sociable sequences, or sociable chains, were discovered and named by the Belgian mathematician Paul Poulet in 1918. In a set of sociable numbers, each number is the sum of the proper factors of the preceding number, i.e., the sum excludes the preceding number itself. For the sequence to be sociable, the sequence must be cyclic and return to its starting point.
