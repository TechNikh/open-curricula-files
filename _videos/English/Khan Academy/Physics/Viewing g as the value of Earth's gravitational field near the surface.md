---
version: 1
type: video
provider: YouTube
id: 1E3Z_R5AHdg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Viewing%20g%20as%20the%20value%20of%20Earth%27s%20gravitational%20field%20near%20the%20surface.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bba13624-b94c-4315-b6dd-1b63a572c12f
updated: 1486069591
title: "Viewing g as the value of Earth's gravitational field near the surface"
categories:
    - Physics
thumbnail_urls:
    - https://i3.ytimg.com/vi/1E3Z_R5AHdg/default.jpg
    - https://i3.ytimg.com/vi/1E3Z_R5AHdg/1.jpg
    - https://i3.ytimg.com/vi/1E3Z_R5AHdg/2.jpg
    - https://i3.ytimg.com/vi/1E3Z_R5AHdg/3.jpg
---
Viewing g as the value of Earth's gravitational field near the surface rather than the acceleration due to gravity near Earth's surface for an object in freefall. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/one-dimensional-motion/old-projectile-motion/v/projectile-motion-part-1?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/one-dimensional-motion/kinematic-formulas/v/impact-velocity-from-given-height?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
