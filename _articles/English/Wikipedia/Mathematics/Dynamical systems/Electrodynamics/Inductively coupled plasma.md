---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Inductively_coupled_plasma
offline_file: ""
offline_thumbnail: ""
uuid: 32523bc3-cb02-47af-a637-9a4cce2c3e71
updated: 1484308759
title: Inductively coupled plasma
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Inductively_Coupled_Plasma.jpg
tags:
    - Operation
    - Applications
categories:
    - Electrodynamics
---
An inductively coupled plasma (ICP) or transformer coupled plasma (TCP)[1] is a type of plasma source in which the energy is supplied by electric currents which are produced by electromagnetic induction, that is, by time-varying magnetic fields.[2]
