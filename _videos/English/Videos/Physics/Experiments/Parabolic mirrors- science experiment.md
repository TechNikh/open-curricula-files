---
version: 1
type: video
provider: YouTube
id: I1IcR7vZwkA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c1cccab5-afbb-45c8-bbca-00da02514a5c
updated: 1486069637
title: 'Parabolic mirrors- science experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/I1IcR7vZwkA/default.jpg
    - https://i3.ytimg.com/vi/I1IcR7vZwkA/1.jpg
    - https://i3.ytimg.com/vi/I1IcR7vZwkA/2.jpg
    - https://i3.ytimg.com/vi/I1IcR7vZwkA/3.jpg
---
