---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nancy_Hingston
offline_file: ""
offline_thumbnail: ""
uuid: 714c51c5-77f2-4b51-8875-600adc62ae33
updated: 1484309085
title: Nancy Hingston
categories:
    - Geometers
---
Nancy Burgess Hingston is a mathematician whose research applies algebraic topology and functional analysis to differential geometry. She is a professor of mathematics at The College of New Jersey.[1]
