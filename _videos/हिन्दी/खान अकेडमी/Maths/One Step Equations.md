---
version: 1
type: video
provider: YouTube
id: pdT1E7GIqRg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9f415dcc-3b7b-43c1-96f8-dadb7a611058
updated: 1486069614
title: One Step Equations
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/pdT1E7GIqRg/default.jpg
    - https://i3.ytimg.com/vi/pdT1E7GIqRg/1.jpg
    - https://i3.ytimg.com/vi/pdT1E7GIqRg/2.jpg
    - https://i3.ytimg.com/vi/pdT1E7GIqRg/3.jpg
---
