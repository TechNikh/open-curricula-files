---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bounded_quantifier
offline_file: ""
offline_thumbnail: ""
uuid: 013eab19-594e-4382-8cf1-7bb9d5f84229
updated: 1484309483
title: Bounded quantifier
tags:
    - Bounded quantifiers in arithmetic
    - Bounded quantifiers in set theory
categories:
    - Computability theory
---
In the study of formal theories in mathematical logic, bounded quantifiers are often added to a language in addition to the standard quantifiers "∀" and "∃". Bounded quantifiers differ from "∀" and "∃" in that bounded quantifiers restrict the range of the quantified variable. The study of bounded quantifiers is motivated by the fact that determining whether a sentence with only bounded quantifiers is true is often not as difficult as determining whether an arbitrary sentence is true.
