---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sister
offline_file: ""
offline_thumbnail: ""
uuid: a030b797-d0d1-4ddb-8019-c966f2b08deb
updated: 1484310212
title: sister
categories:
    - Dictionary
---
sister
     n 1: a female person who has the same parents as another person;
          "my sister married a musician" [syn: {sis}] [ant: {brother}]
     2: (Roman Catholic Church) a title given to a nun (and used as
        a form of address); "the Sisters taught her to love God"
     3: a female person who is a fellow member of a sorority or
        labor union or other group; "none of her sisters would
        betray her"
     4: sometimes used as a term of address for attractive young
        women [syn: {baby}]
