---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Price_of_stability
offline_file: ""
offline_thumbnail: ""
uuid: 0894f5c3-382e-49e7-955e-66e74bee8298
updated: 1484308794
title: Price of stability
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Network-design-poa.svg.png
tags:
    - Examples
    - Background and milestones
    - Network design games
    - Setup
    - Price of anarchy
    - Lower bound on price of stability
    - Upper bound on price of stability
categories:
    - Fixed points (mathematics)
---
In game theory, the price of stability (PoS) of a game is the ratio between the best objective function value of one of its equilibria and that of an optimal outcome. The PoS is relevant for games in which there is some objective authority that can influence the players a bit, and maybe help them converge to a good Nash equilibrium. When measuring how efficient a Nash equilibrium is in a specific game we often time also talk about the price of anarchy (PoA).
