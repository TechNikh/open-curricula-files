---
version: 1
type: video
provider: YouTube
id: cNlwi6lUCEM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/8th%20grade/Systems%20of%20equations/Linear%20systems%20word%20problem%20with%20substitution.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4cdaf7ee-74c3-4398-a9f7-bbf481b08fba
updated: 1486069557
title: Linear systems word problem with substitution
categories:
    - Systems of equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/cNlwi6lUCEM/default.jpg
    - https://i3.ytimg.com/vi/cNlwi6lUCEM/1.jpg
    - https://i3.ytimg.com/vi/cNlwi6lUCEM/2.jpg
    - https://i3.ytimg.com/vi/cNlwi6lUCEM/3.jpg
---
