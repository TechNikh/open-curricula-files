---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spacing
offline_file: ""
offline_thumbnail: ""
uuid: 2fa8fe69-c3ff-4c65-9d35-33bd718fe78c
updated: 1484310194
title: spacing
categories:
    - Dictionary
---
spacing
     n 1: the time between occurrences of a repeating event; "some
          women do not control the spacing of their children"
     2: the property possessed by an array of things that have space
        between them [syn: {spatial arrangement}]
