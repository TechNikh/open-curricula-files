---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Robert_W._Rosenthal
offline_file: ""
offline_thumbnail: ""
uuid: ec2f05f3-6511-4f3d-8f84-7dea2b1a65bf
updated: 1484309025
title: Robert W. Rosenthal
categories:
    - Game theorists
---
He obtained a B.A. in political economy from Johns Hopkins University (1966), M.S. (1968) and Ph.D. (1971) in operations research from Stanford University, advised by Robert B. Wilson.[1] He worked as assistant professor in the department of Industrial Engineering and management science at Northwestern University (1970–76), was member of the technical staff at Bell Labs (1976–83), was professor of economics at Virginia Polytechnic Institute and State University (1983–84), State University of New York at Stony Brook (1984–87) and Boston University where worked until his death from a heart attack (1987–2002).[2] He also had appointments with Massachusetts Institute of Technology ...
