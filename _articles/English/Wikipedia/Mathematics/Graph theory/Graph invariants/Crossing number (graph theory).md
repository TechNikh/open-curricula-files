---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Crossing_number_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 0463baea-e800-4b56-96d8-35ada2c7e2ed
updated: 1484309420
title: Crossing number (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-3-crossing_Heawood_graph.svg.png
tags:
    - Definitions
    - Special cases
    - Complete bipartite graphs
    - Complete graphs and graph coloring
    - Cubic graphs
    - Known crossing numbers
    - Complexity
    - The crossing number inequality
    - Variations
categories:
    - Graph invariants
---
In graph theory, the crossing number cr(G) of a graph G is the lowest number of edge crossings of a plane drawing of the graph G. For instance, a graph is planar if and only if its crossing number is zero.
