---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/staple
offline_file: ""
offline_thumbnail: ""
uuid: 434ac1a1-2e15-4081-8387-207402417286
updated: 1484310531
title: staple
categories:
    - Dictionary
---
staple
     adj : necessary foods or commodities; "wheat is a staple crop"
     n 1: (usually plural) a necessary commodity for which demand is
          constant [syn: {basic}]
     2: material suitable for manufacture or use or finishing [syn:
        {raw material}]
     3: a short U-shaped wire nail for securing cables
     4: paper fastener consisting of a short length of U-shaped wire
        that can fasten papers together
     v : secure or fasten with a staple or staples; "staple the
         papers together" [ant: {unstaple}]
