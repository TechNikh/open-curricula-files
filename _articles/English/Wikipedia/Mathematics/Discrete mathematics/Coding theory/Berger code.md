---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Berger_code
offline_file: ""
offline_thumbnail: ""
uuid: 094f0114-582c-4265-98f3-013503a9d545
updated: 1484308549
title: Berger code
categories:
    - Coding theory
---
In telecommunication, a Berger code is a unidirectional error detecting code, named after its inventor, J. M. Berger. Berger codes can detect all unidirectional errors. Unidirectional errors are errors that only flip ones into zeroes or only zeroes into ones, such as in asymmetric channels. The check bits of Berger codes are computed by summing all the zeroes in the information word, and expressing that sum in natural binary. If the information word consists of n{\displaystyle n} bits, then the Berger code needs k=⌈log2⁡(n+1)⌉{\displaystyle k=\lceil \log _{2}(n+1)\rceil } "check bits", giving a Berger code of length k+n. (In other words, the k{\displaystyle k} check bits are enough to ...
