---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lounge
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484508061
title: lounge
categories:
    - Dictionary
---
lounge
     n 1: an upholstered seat for more than one person [syn: {sofa}, {couch}]
     2: a public room (as in a hotel or airport) with seating where
        people can wait [syn: {waiting room}, {waiting area}]
     v 1: sit or recline comfortably; "He was lounging on the sofa"
     2: be about; "The high school students like to loiter in the
        Central Square"; "Who is this man that is hanging around
        the department?" [syn: {loiter}, {footle}, {lollygag}, {loaf},
         {lallygag}, {hang around}, {mess about}, {tarry}, {linger},
         {lurk}, {mill about}, {mill around}]
