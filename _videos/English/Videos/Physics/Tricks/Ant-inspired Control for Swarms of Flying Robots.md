---
version: 1
type: video
provider: YouTube
id: 5DbWVEQ-JzA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ff291384-55b2-4d39-90c8-35817090f65b
updated: 1486069660
title: Ant-inspired Control for Swarms of Flying Robots
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/5DbWVEQ-JzA/default.jpg
    - https://i3.ytimg.com/vi/5DbWVEQ-JzA/1.jpg
    - https://i3.ytimg.com/vi/5DbWVEQ-JzA/2.jpg
    - https://i3.ytimg.com/vi/5DbWVEQ-JzA/3.jpg
---
