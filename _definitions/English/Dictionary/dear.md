---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dear
offline_file: ""
offline_thumbnail: ""
uuid: 49eaed38-a7f5-41b0-b070-a54a568ce1fc
updated: 1484310541
title: dear
categories:
    - Dictionary
---
dear
     adj 1: dearly loved [syn: {beloved}, {darling}]
     2: with or in a close or intimate relationship; "a good
        friend"; "my sisters and brothers are near and dear" [syn:
         {good}, {near}]
     3: earnest; "one's dearest wish"; "devout wishes for their
        success"; "heartfelt condolences" [syn: {devout}, {earnest},
         {heartfelt}]
     4: having a high price; "costly jewelry"; "high-priced
        merchandise"; "much too dear for my pocketbook"; "a pricey
        restaurant" [syn: {costly}, {dear(p)}, {high-priced}, {pricey},
         {pricy}]
     n 1: a beloved person; used as terms of endearment [syn: {beloved},
           {dearest}, {loved one}, {honey}, ...
