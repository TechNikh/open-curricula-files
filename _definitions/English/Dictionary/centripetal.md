---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/centripetal
offline_file: ""
offline_thumbnail: ""
uuid: 06d93648-bb4e-42ba-a1a2-f6ca3272146d
updated: 1484310365
title: centripetal
categories:
    - Dictionary
---
centripetal
     adj 1: tending to move toward a center; "centripetal force" [ant: {centrifugal}]
     2: tending to unify [syn: {unifying(a)}]
     3: of a nerve fiber or impulse originating outside and passing
        toward the central nervous system; "sensory neurons" [syn:
         {receptive}, {sensory(a)}]
