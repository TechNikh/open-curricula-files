---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Maximum_common_subgraph_isomorphism_problem
offline_file: ""
offline_thumbnail: ""
uuid: 0747a4e2-603f-434d-b739-896f88ce6cdd
updated: 1484309394
title: Maximum common subgraph isomorphism problem
categories:
    - Computational problems in graph theory
---
In complexity theory, maximum common subgraph-isomorphism (MCS) is an optimization problem that is known to be NP-hard. The formal description of the problem is as follows:
