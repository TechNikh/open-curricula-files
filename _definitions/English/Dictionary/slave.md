---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slave
offline_file: ""
offline_thumbnail: ""
uuid: 3870923f-79a9-4ab4-8ba9-4a528fe67926
updated: 1484310577
title: slave
categories:
    - Dictionary
---
slave
     adj : held in servitude; "he was born of slave parents" [syn: {slave(a)}]
           [ant: {free}]
     n 1: a person who is owned by someone
     2: someone who works as hard as a slave [syn: {striver}, {hard
        worker}]
     3: someone entirely dominated by some influence or person; "a
        slave to fashion"; "a slave to cocaine"; "his mother was
        his abject slave"
     v : work very hard, like a slave [syn: {break one's back}, {buckle
         down}, {knuckle down}]
