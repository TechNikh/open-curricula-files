---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Privacy-preserving_computational_geometry
offline_file: ""
offline_thumbnail: ""
uuid: d9243742-1b31-44f6-b174-499a8870fcc8
updated: 1484309179
title: Privacy-preserving computational geometry
categories:
    - Computational geometry
---
Privacy-preserving computational geometry is the research area on the intersection of the domains of secure multi-party computation (SMC) and computational geometry. Classical problems of computational geometry reconsidered from the point of view of SMC include shape intersection, private point inclusion problem, range searching, convex hull,[1] and more.[2]
