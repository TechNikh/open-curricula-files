---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/citizenship
offline_file: ""
offline_thumbnail: ""
uuid: a7538a0e-18a4-449e-9823-6fe0f9c1fbc1
updated: 1484310445
title: citizenship
categories:
    - Dictionary
---
citizenship
     n 1: the status of a citizen with rights and duties
     2: conduct as a citizen; "award for good citizenship"
