---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Green_pigment
offline_file: ""
offline_thumbnail: ""
uuid: 6c1e8202-2648-4586-965e-a832e8a8f168
updated: 1484308372
title: Shades of green
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Color_icon_green.svg.png
tags:
    - Green in nature
    - Artichoke
    - Artichoke green (Pantone)
    - Asparagus
    - Avocado
    - Dark green
    - Fern green
    - Forest green
    - "Hooker's green"
    - Jungle green
    - Laurel green
    - Light green
    - Mantis
    - Moss green
    - Dark moss green
    - Myrtle green
    - Mint green
    - Pine green
    - Sap green
    - Shamrock green (Irish green)
    - Tea green
    - Teal
    - Olive
    - Computer web color greens
    - Green
    - Green (HTML/CSS color)
    - Dark green (X11)
    - Additional definitions of green
    - Green (CMYK) (pigment green)
    - Green (NCS) (psychological primary green)
    - Green (Munsell)
    - Green (Pantone)
    - Green (Crayola)
    - Other notable green colors
    - Army green
    - Bottle green
    - Bright green
    - Bright mint
    - Brunswick green
    - Cal Poly Pomona green
    - Castleton green
    - Celadon
    - Celadon green
    - Dark pastel green
    - Dartmouth green
    - Emerald
    - Feldgrau
    - GO Transit green
    - Green-yellow
    - Harlequin
    - Hunter green
    - India green
    - Islamic green
    - Jade
    - Kelly green
    - Malachite
    - Midnight green
    - MSU green
    - Neon green
    - Office green
    - Pakistan green (X11 Dark green)
    - Paris green
    - Persian green
    - Rifle green
    - Russian green
    - Sacramento State green
    - "Screamin' green"
    - Sea green
    - Spanish green
    - UP forest green
    - Green in human culture
categories:
    - Uncategorized
---
Varieties of the color green may differ in hue, chroma (also called saturation or intensity) or lightness (or value, tone, or brightness), or in two or three of these qualities. Variations in value are also called tints and shades, a tint being a green or other hue mixed with white, a shade being mixed with black. A large selection of these various colors is shown below.
