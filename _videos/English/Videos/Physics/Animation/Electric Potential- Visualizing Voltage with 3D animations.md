---
version: 1
type: video
provider: YouTube
id: -Rb9guSEeVE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Electric%20Potential-%20Visualizing%20Voltage%20with%203D%20animations.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 16c13254-2e8f-4dc8-86a5-de24acbe5849
updated: 1486069657
title: 'Electric Potential- Visualizing Voltage with 3D animations'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/-Rb9guSEeVE/default.jpg
    - https://i3.ytimg.com/vi/-Rb9guSEeVE/1.jpg
    - https://i3.ytimg.com/vi/-Rb9guSEeVE/2.jpg
    - https://i3.ytimg.com/vi/-Rb9guSEeVE/3.jpg
---
