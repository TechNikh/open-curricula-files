---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dictator_game
offline_file: ""
offline_thumbnail: ""
uuid: 5f18b43e-4705-4ea7-a3e2-3c70b948b0d4
updated: 1484309050
title: Dictator game
tags:
    - Description
    - "Adults' and children's behavior"
    - Challenges
    - Trust game
categories:
    - Non-cooperative games
---
The dictator game is a game in experimental economics, similar to the ultimatum game, first developed by Daniel Kahneman and colleagues.[1] Experimental results offer evidence against the rationally self-interested individual (sometimes called the homo economicus) concept of economic behavior,[2] though precisely what to conclude from the evidence is controversial.[3]
