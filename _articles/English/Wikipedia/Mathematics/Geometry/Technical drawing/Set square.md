---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Set_square
offline_file: ""
offline_thumbnail: ""
uuid: 5b5c91ed-f1fc-4420-a0d3-e7c25bc04a7a
updated: 1484309331
title: Set square
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Reglas.svg.png
categories:
    - Technical drawing
---
A set square or triangle (American English) is an object used in engineering and technical drawing, with the aim of providing a straightedge at a right angle or other particular planar angle to a baseline.
