---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Null_set
offline_file: ""
offline_thumbnail: ""
uuid: 648091aa-2f13-41d7-9de8-8174553aec03
updated: 1484309510
title: Null set
tags:
    - Definition
    - Properties
    - Lebesgue measure
    - Uses
    - A subset of the Cantor set which is not Borel measurable
    - Haar null
categories:
    - Set theory
---
In set theory, a null set N ⊂ R is a set that can be covered by a countable union of intervals of arbitrarily small total length. The notion of null set in set theory anticipates the development of Lebesgue measure since a null set necessarily has measure zero.
