---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/International_Academy_of_Mathematical_Chemistry
offline_file: ""
offline_thumbnail: ""
uuid: 78b54bcc-7892-4888-91fb-a32c985001c2
updated: 1484309429
title: International Academy of Mathematical Chemistry
tags:
    - Governing Body of the IAMC
    - IAMC yearly meetings
categories:
    - Mathematical chemistry
---
The International Academy of Mathematical Chemistry (IAMC) was founded in Dubrovnik (Croatia) in 2005 by Milan Randić.[1] It is an organization for chemistry and mathematics avocation, and its predecessors have been around since the 1930s.[1] The Academy Members are 88 (2011) from all over the world (27 countries), comprising six scientists awarded the Nobel Prize.[1]
