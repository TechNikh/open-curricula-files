---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meant
offline_file: ""
offline_thumbnail: ""
uuid: 9c2f1322-5c1f-440c-9154-a8d0d8ff8d97
updated: 1484310389
title: meant
categories:
    - Dictionary
---
mean
     adj 1: approximating the statistical norm or average or expected
            value; "the average income in New England is below
            that of the nation"; "of average height for his age";
            "the mean annual rainfall" [syn: {average}, {mean(a)}]
     2: characterized by malice; "a hateful thing to do"; "in a mean
        mood" [syn: {hateful}]
     3: having or showing an ignoble lack of honor or morality;
        "that liberal obedience without which your army would be a
        base rabble"- Edmund Burke; "taking a mean advantage";
        "chok'd with ambition of the meaner sort"- Shakespeare;
        "something essentially vulgar and meanspirited in
        ...
