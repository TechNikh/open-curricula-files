---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Furuta_pendulum
offline_file: ""
offline_thumbnail: ""
uuid: 38a53384-4af8-4b0e-a3aa-05c91cc38e6d
updated: 1484308738
title: Furuta pendulum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-RotaryInvertedPendulum.JPG
tags:
    - Equations of motion
    - Definitions
    - Assumptions
    - Non-linear Equations of Motion
    - Simplifications
categories:
    - Control engineering
---
The Furuta pendulum, or rotational inverted pendulum, consists of a driven arm which rotates in the horizontal plane and a pendulum attached to that arm which is free to rotate in the vertical plane. It was invented in 1992 at Tokyo Institute of Technology by Katsuhisa Furuta[1][2][3][4] and his colleagues. It is an example of a complex nonlinear oscillator of interest in control system theory. The pendulum is underactuated and extremely non-linear due to the gravitational forces and the coupling arising from the Coriolis and centripetal forces. Since then, dozens, possibly hundreds of papers and theses have used the system to demonstrate linear and non-linear control laws.[5][6][7] The ...
