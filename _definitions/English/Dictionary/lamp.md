---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lamp
offline_file: ""
offline_thumbnail: ""
uuid: 4bd1720a-ecbb-40d5-9fba-0bd50e3f027a
updated: 1484310206
title: lamp
categories:
    - Dictionary
---
lamp
     n 1: an artificial source of visible illumination
     2: a piece of furniture holding one or more electric light
        bulbs
