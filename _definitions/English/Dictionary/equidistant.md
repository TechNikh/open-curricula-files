---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equidistant
offline_file: ""
offline_thumbnail: ""
uuid: 53139096-7cf8-4be7-b07e-169a2934aaa0
updated: 1484310365
title: equidistant
categories:
    - Dictionary
---
equidistant
     adj : the same distance apart at every point
