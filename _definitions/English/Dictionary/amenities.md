---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/amenities
offline_file: ""
offline_thumbnail: ""
uuid: a4f87749-2338-48fa-8d9e-21c6bcde5563
updated: 1484310475
title: amenities
categories:
    - Dictionary
---
amenities
     n : things that make you comfortable and at ease; "all the
         comforts of home" [syn: {comforts}, {creature comforts},
         {conveniences}]
