---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Berge%27s_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: abcc8bc3-9922-4ca4-a44a-b85c15ea8775
updated: 1484309423
title: "Berge's lemma"
tags:
    - Proof
    - Corollaries
    - Corollary 1
    - Corollary 2
categories:
    - Matching
---
In graph theory, Berge's lemma states that a matching M in a graph G is maximum (contains the largest possible number of edges) if and only if there is no augmenting path (a path that starts and ends on free (unmatched) vertices, and alternates between edges in and not in the matching) with M.
