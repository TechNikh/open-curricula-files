---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spring
offline_file: ""
offline_thumbnail: ""
uuid: b5aa7899-46f9-4674-a9af-0b257dc1804d
updated: 1484310293
title: spring
categories:
    - Dictionary
---
spring
     n 1: the season of growth; "the emerging buds were a sure sign of
          spring"; "he will hold office until the spring of next
          year" [syn: {springtime}]
     2: a natural flow of ground water [syn: {fountain}, {outflow},
        {outpouring}, {natural spring}]
     3: a metal elastic device that returns to its shape or position
        when pushed or pulled or pressed; "the spring was broken"
     4: a light springing movement upwards or forwards [syn: {leap},
         {leaping}, {saltation}, {bound}, {bounce}]
     5: the elasticity of something that can be stretched and
        returns to its original length [syn: {give}, {springiness}]
     6: a point at which ...
