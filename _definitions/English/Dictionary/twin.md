---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/twin
offline_file: ""
offline_thumbnail: ""
uuid: 2199d2c5-c3a2-425b-b6e1-5a4c547660f2
updated: 1484310459
title: twin
categories:
    - Dictionary
---
twin
     adj 1: being two identical [syn: {duplicate}, {matching}, {twin(a)},
             {twinned}]
     2: very similar [syn: {siamese}]
     n 1: either of two offspring born at the same time from the same
          pregnancy
     2: (astrology) a person who is born while the sun in in Gemini
        [syn: {Gemini}]
     3: a waterfall in the Snake River in southern Idaho [syn: {Twin
        Falls}]
     4: a duplicate copy [syn: {counterpart}, {similitude}]
     v 1: duplicate or match; "The polished surface twinned his face
          and chest in reverse" [syn: {duplicate}, {parallel}]
     2: bring two objects, ideas, or people together; "This fact is
        coupled to the other ...
