---
version: 1
type: video
provider: YouTube
id: hSZkSzvRx8w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 53798668-1e95-41f0-81a7-1b0a2107e163
updated: 1486069636
title: >
    Cbse Class 12 focal length Convex Mirror Physics Practical
    Experiment Video
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/hSZkSzvRx8w/default.jpg
    - https://i3.ytimg.com/vi/hSZkSzvRx8w/1.jpg
    - https://i3.ytimg.com/vi/hSZkSzvRx8w/2.jpg
    - https://i3.ytimg.com/vi/hSZkSzvRx8w/3.jpg
---
