---
version: 1
type: video
provider: YouTube
id: ew4YHmNigRs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Changing%20Direct%20into%20Indirect%20Speech.webm"
offline_file: ""
offline_thumbnail: ""
uuid: faefff6e-827e-4eac-8006-81d702965e9e
updated: 1486069631
title: Changing Direct into Indirect Speech
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/ew4YHmNigRs/default.jpg
    - https://i3.ytimg.com/vi/ew4YHmNigRs/1.jpg
    - https://i3.ytimg.com/vi/ew4YHmNigRs/2.jpg
    - https://i3.ytimg.com/vi/ew4YHmNigRs/3.jpg
---
