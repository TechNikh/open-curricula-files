---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hardship
offline_file: ""
offline_thumbnail: ""
uuid: 7bbdd508-c7ea-4cb1-8db6-f20068157755
updated: 1484310181
title: hardship
categories:
    - Dictionary
---
hardship
     n 1: a state of misfortune or affliction; "debt-ridden farmers
          struggling with adversity"; "a life of hardship" [syn: {adversity},
           {hard knocks}]
     2: something hard to endure; "the asperity of northern winters"
        [syn: {asperity}, {grimness}, {rigor}, {rigour}, {severity},
         {rigorousness}]
     3: something that causes or entails suffering; "I cannot think
        it a hardship that more indulgence is allowed to men than
        to women"- James Boswell; "the many hardships of frontier
        life"
