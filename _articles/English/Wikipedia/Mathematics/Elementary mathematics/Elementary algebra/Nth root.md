---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nth_root
offline_file: ""
offline_thumbnail: ""
uuid: e16175a3-8319-4a1b-aeaf-7c6382fbaa40
updated: 1484308918
title: Nth root
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Roots_chart.svg.png
tags:
    - Etymology
    - Origin of the root symbol
    - Etymology of "surd"
    - History
    - Definition and notation
    - Square roots
    - Cube roots
    - Identities and properties
    - Simplified form of a radical expression
    - Infinite series
    - Computing principal roots
    - nth root algorithm
    - >
        Digit-by-digit calculation of principal roots of decimal
        (base 10) numbers
    - Examples
    - Logarithmic computation
    - Geometric constructibility
    - Complex roots
    - Square roots
    - Roots of unity
    - nth roots
    - Solving polynomials
categories:
    - Elementary algebra
---
In mathematics, an nth root of a number x, where n is usually assumed to be a positive integer, is a number r which, when raised to the power n yields x
