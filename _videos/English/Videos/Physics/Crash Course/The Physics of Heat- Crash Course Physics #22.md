---
version: 1
type: video
provider: YouTube
id: tuSC0ObB-qY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3003b510-16fe-4e07-8107-124b10ba4830
updated: 1486069682
title: 'The Physics of Heat- Crash Course Physics #22'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/tuSC0ObB-qY/default.jpg
    - https://i3.ytimg.com/vi/tuSC0ObB-qY/1.jpg
    - https://i3.ytimg.com/vi/tuSC0ObB-qY/2.jpg
    - https://i3.ytimg.com/vi/tuSC0ObB-qY/3.jpg
---
