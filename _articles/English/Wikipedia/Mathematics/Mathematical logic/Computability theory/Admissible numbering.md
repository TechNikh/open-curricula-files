---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Admissible_numbering
offline_file: ""
offline_thumbnail: ""
uuid: ad4e5748-1597-41c5-b6f3-60954a7b70fd
updated: 1484309481
title: Admissible numbering
tags:
    - Definition
    - "Rogers' equivalence theorem"
categories:
    - Computability theory
---
In computability theory, the admissible numberings are enumerations (numberings) of the set of partial computable functions that can be converted to and from the standard numbering. These numberings are also called acceptable numberings and acceptable programming systems.
