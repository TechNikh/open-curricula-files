---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Virtual_valuation
offline_file: ""
offline_thumbnail: ""
uuid: 5f066617-b3f5-42e1-abeb-26edadda513f
updated: 1484309050
title: Virtual valuation
tags:
    - Applications
    - Examples
    - Regularity
categories:
    - Mechanism design
---
In auction theory, particularly Bayesian-optimal mechanism design, a virtual valuation of an agent is a function that measures the surplus that can be extracted from that agent.
