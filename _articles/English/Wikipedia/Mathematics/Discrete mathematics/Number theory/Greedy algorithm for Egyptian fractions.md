---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Greedy_algorithm_for_Egyptian_fractions
offline_file: ""
offline_thumbnail: ""
uuid: aefe1d79-35e5-4e25-a56b-99fcc297ae49
updated: 1484308644
title: Greedy algorithm for Egyptian fractions
tags:
    - Algorithm and examples
    - "Sylvester's sequence and closest approximation"
    - Maximum-length expansions and congruence conditions
    - Approximation of polynomial roots
    - Other integer sequences
    - Related expansions
categories:
    - Number theory
---
In mathematics, the greedy algorithm for Egyptian fractions is a greedy algorithm, first described by Fibonacci, for transforming rational numbers into Egyptian fractions. An Egyptian fraction is a representation of an irreducible fraction as a sum of unit fractions, as e.g. 5/6 = 1/2 + 1/3. As the name indicates, these representations have been used as long ago as ancient Egypt, but the first published systematic method for constructing such expansions is described in the Liber Abaci (1202) of Leonardo of Pisa (Fibonacci). It is called a greedy algorithm because at each step the algorithm chooses greedily the largest possible unit fraction that can be used in any representation of the ...
