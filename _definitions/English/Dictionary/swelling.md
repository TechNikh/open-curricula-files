---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swelling
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484615521
title: swelling
categories:
    - Dictionary
---
swelling
     adj : becoming puffy as from internal bleeding or accumulation of
           other fluids; "put ice on the swelling ankle"
     n 1: abnormal protuberance or localized enlargement [syn: {puffiness},
           {lump}]
     2: the swelling of certain substances when they are heated
        (often accompanied by release of water) [syn: {intumescence},
         {intumescency}]
