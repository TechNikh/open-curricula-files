---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Duality_(electrical_circuits)
offline_file: ""
offline_thumbnail: ""
uuid: 8778a881-0d0f-466b-a95d-13f9e7f2dd6d
updated: 1484309236
title: Duality (electrical circuits)
tags:
    - History
    - Examples
    - Constitutive relations
    - Voltage division — current division
    - Impedance and admittance
categories:
    - Duality theories
---
In electrical engineering, electrical terms are associated into pairs called duals. A dual of a relationship is formed by interchanging voltage and current in an expression. The dual expression thus produced is of the same form, and the reason that the dual is always a valid statement can be traced to the duality of electricity and magnetism.
