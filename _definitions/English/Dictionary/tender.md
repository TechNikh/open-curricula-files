---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tender
offline_file: ""
offline_thumbnail: ""
uuid: 7c513431-d95b-497c-a0d3-20f93e511c5e
updated: 1484310152
title: tender
categories:
    - Dictionary
---
tender
     adj 1: given to sympathy or gentleness or sentimentality; "a tender
            heart"; "a tender smile"; "tender loving care";
            "tender memories"; "a tender mother" [ant: {tough}]
     2: hurting; "the tender spot on his jaw" [syn: {sensitive}, {sore}]
     3: susceptible to physical or emotional injury; "at a tender
        age" [syn: {vulnerable}]
     4: having or displaying warmth or affection; "affectionate
        children"; "caring parents"; "a fond embrace"; "fond of
        his nephew"; "a tender glance"; "a warm embrace" [syn: {affectionate},
         {caring}, {fond}, {lovesome}, {warm}]
     5: easy to cut or chew; "tender beef" [ant: {tough}]
     6: ...
