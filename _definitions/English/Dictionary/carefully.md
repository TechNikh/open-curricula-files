---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carefully
offline_file: ""
offline_thumbnail: ""
uuid: 90c8f85e-9665-40d2-82b3-d39324205e75
updated: 1484310328
title: carefully
categories:
    - Dictionary
---
carefully
     adv 1: taking care or paying attention; "they watched carefully"
     2: with caution or prudence or tact; "she ventured cautiously
        downstairs"; "they handled the incident with kid gloves"
        [syn: {cautiously}, {with kid gloves}] [ant: {incautiously},
         {incautiously}]
