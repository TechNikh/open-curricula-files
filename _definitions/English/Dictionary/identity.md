---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/identity
offline_file: ""
offline_thumbnail: ""
uuid: 5aa9d277-7619-47f2-96d4-5765f99757b0
updated: 1484310275
title: identity
categories:
    - Dictionary
---
identity
     n 1: the distinct personality of an individual regarded as a
          persisting entity; "you can lose your identity when you
          join the army" [syn: {personal identity}, {individuality}]
     2: the individual characteristics by which a thing or person is
        recognized or known; "geneticists only recently discovered
        the identity of the gene that causes it"; "it was too dark
        to determine his identity"; "she guessed the identity of
        his lover"
     3: an operator that leaves unchanged the element on which it
        operates; "the identity under numerical multiplication is
        1" [syn: {identity element}, {identity operator}]
     4: ...
