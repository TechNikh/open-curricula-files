---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yoav_Shoham
offline_file: ""
offline_thumbnail: ""
uuid: 7eb613a6-04bf-4bd8-9932-343b1cf0799a
updated: 1484309026
title: Yoav Shoham
categories:
    - Game theorists
---
Yoav Shoham (Hebrew: יואב שהם‎‎; born 22 January 1956) is a computer scientist at Stanford University.[1] He received his Ph.D. at Yale University in 1987.[2] Shoham co-teaches a popular game theory course on Coursera.org,[3] along with Matthew O. Jackson and Kevin Leyton-Brown and Advanced game theory on Stanford-online.[4] He is also a Fellow of the Association for the Advancement of Artificial Intelligence (AAAI),[5] and a charter member of the Game Theory Society (GTS).[6] He is a co-winner of the 2012 ACM - AAAI Allen Newell Award for "fundamental contributions at the intersection of computer science, game theory, and economics, most particularly in multi-agent systems and ...
