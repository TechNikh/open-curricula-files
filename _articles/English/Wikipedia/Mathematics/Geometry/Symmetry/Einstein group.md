---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Einstein_group
offline_file: ""
offline_thumbnail: ""
uuid: 4a52d314-a8b1-44e8-ad1f-cae21f80e2ae
updated: 1484309309
title: Einstein group
categories:
    - Symmetry
---
The Poincaré group, the transformation group of special relativity, being orthogonal, the inverse of a transformation equals its transpose, introducing discrete reflections. This, in turn, violates Einstein's dictum for a group "no less general than that of the continuous transformations of the four coordinates." Specifically, any pair of Euler angles θk and −θk are not independent, nor are any pair of boosts vk/c and −vk/c. Available parameters are thus reduced, from the 16 needed to express all transformations in a curved spacetime, per the general principle of relativity, ∂xμ′/∂xν, to the 10 of the Poincaré group.
