---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/negotiation
offline_file: ""
offline_thumbnail: ""
uuid: 175f6739-79e2-4abb-b5d6-8d141b30f6ae
updated: 1484310555
title: negotiation
categories:
    - Dictionary
---
negotiation
     n 1: a discussion intended to produce an agreement; "the buyout
          negotiation lasted several days"; "they disagreed but
          kept an open dialogue"; "talks between Israelis and
          Palestinians" [syn: {dialogue}, {talks}]
     2: the activity or business of negotiating an agreement; coming
        to terms
