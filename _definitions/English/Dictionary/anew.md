---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anew
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484424361
title: anew
categories:
    - Dictionary
---
anew
     adv : again but in a new or different way; "start afresh"; "wanted
           to write the story anew"; "starting life anew in a
           fresh place" [syn: {afresh}]
