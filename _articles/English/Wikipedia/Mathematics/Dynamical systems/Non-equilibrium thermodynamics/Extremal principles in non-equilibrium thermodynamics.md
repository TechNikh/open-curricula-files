---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Extremal_principles_in_non-equilibrium_thermodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 2f404ba2-18f8-412d-b98f-033d87df9d8b
updated: 1484308808
title: Extremal principles in non-equilibrium thermodynamics
tags:
    - "Fluctuations, entropy, 'thermodynamics forces', and reproducible dynamical structure"
    - Local thermodynamic equilibrium
    - Linear and non-linear processes
    - Continuous and discontinuous motions of fluids
    - Historical development
    - W. Thomson, Baron Kelvin
    - Helmholtz
    - J.W. Strutt, Baron Rayleigh
    - Korteweg
    - Onsager
    - Prigogine
    - Casimir
    - Ziman
    - Ziegler
    - Gyarmati
    - Paltridge
    - >
        Speculated thermodynamic extremum principles for energy
        dissipation and entropy production
    - >
        Prigogine’s proposed theorem of minimum entropy production
        for very slow purely diffusive transfer
    - 'Faster transfer with convective circulation: second entropy'
    - >
        Speculated principles of maximum entropy production and
        minimum energy dissipation
    - Prospects
categories:
    - Non-equilibrium thermodynamics
---
Energy dissipation and entropy production extremal principles are ideas developed within non-equilibrium thermodynamics that attempt to predict the likely steady states and dynamical structures that a physical system might show. The search for extremum principles for non-equilibrium thermodynamics follows their successful use in other branches of physics.[1][2][3][4][5][6] According to Kondepudi (2008),[7] and to Grandy (2008),[8] there is no general rule that provides an extremum principle that governs the evolution of a far-from-equilibrium system to a steady state. According to Glansdorff and Prigogine (1971, page 16),[9] irreversible processes usually are not governed by global extremal ...
