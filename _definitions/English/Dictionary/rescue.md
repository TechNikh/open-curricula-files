---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rescue
offline_file: ""
offline_thumbnail: ""
uuid: a2598ff6-f061-4399-8682-c94ac9769e06
updated: 1484310163
title: rescue
categories:
    - Dictionary
---
rescue
     n : recovery or preservation from loss or danger; "work is the
         deliverance of mankind"; "a surgeon's job is the saving
         of lives" [syn: {deliverance}, {delivery}, {saving}]
     v 1: free from harm or evil [syn: {deliver}]
     2: take forcibly from legal custody; "rescue prisoners"
