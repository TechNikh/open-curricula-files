---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ribbon
offline_file: ""
offline_thumbnail: ""
uuid: aa76a6ff-176c-4a02-b989-7d19faa49ae3
updated: 1484310373
title: ribbon
categories:
    - Dictionary
---
ribbon
     n 1: any long object resembling a thin line; "a mere ribbon of
          land"; "the lighted ribbon of traffic"; "from the air
          the road was a gray thread"; "a thread of smoke climbed
          upward" [syn: {thread}]
     2: an award for winning a championship or commemorating some
        other event [syn: {decoration}, {laurel wreath}, {medal},
        {medallion}, {palm}]
     3: a long strip of inked material for making characters on
        paper with a typewriter [syn: {typewriter ribbon}]
     4: notion consisting of a narrow strip of fine material used
        for trimming
