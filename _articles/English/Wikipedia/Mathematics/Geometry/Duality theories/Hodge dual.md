---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hodge_dual
offline_file: ""
offline_thumbnail: ""
uuid: 54bbb4ef-9c08-4ce7-ae20-c3717a47dfa8
updated: 1484309234
title: Hodge dual
tags:
    - Dimensions and algebra
    - Extensions
    - Formal definition of the Hodge star of k-vectors
    - Explanation
    - Computation of the Hodge star
    - Index notation for the star operator
    - Examples
    - Two dimensions
    - Three-dimensions
    - Four dimensions
    - Inner product of k-vectors
    - Duality
    - Hodge star on manifolds
    - The codifferential
    - Derivatives in three dimensions
    - Notes
categories:
    - Duality theories
---
In mathematics, the Hodge star operator or Hodge dual is an important linear map introduced in general by W. V. D. Hodge. It is defined on the exterior algebra of a finite-dimensional oriented inner product space.
