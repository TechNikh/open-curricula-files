---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deflection
offline_file: ""
offline_thumbnail: ""
uuid: 61b5d010-f9e8-4621-8902-0519a97ddc35
updated: 1484310196
title: deflection
categories:
    - Dictionary
---
deflection
     n 1: a twist or aberration; especially a perverse or abnormal way
          of judging or acting [syn: {warp}]
     2: the amount by which a propagating wave is bent [syn: {deflexion},
         {refraction}]
     3: the movement of the pointer or pen of a measuring instrument
        from its zero position [syn: {deflexion}]
     4: the property of being bent or deflected [syn: {deflexion}, {bending}]
     5: a turning aside (of your course or attention or concern); "a
        diversion from the main highway"; "a digression into
        irrelevant details"; "a deflection from his goal" [syn: {diversion},
         {deviation}, {digression}, {deflexion}, {divagation}]
