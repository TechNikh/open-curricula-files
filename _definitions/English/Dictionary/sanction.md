---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sanction
offline_file: ""
offline_thumbnail: ""
uuid: fc7a6afb-e314-41e7-af1f-2d436929034c
updated: 1484310555
title: sanction
categories:
    - Dictionary
---
sanction
     n 1: formal and explicit approval; "a Democrat usually gets the
          union's endorsement" [syn: {countenance}, {endorsement},
           {indorsement}, {warrant}, {imprimatur}]
     2: a mechanism of social control for enforcing a society's
        standards
     3: official permission or approval; "authority for the program
        was renewed several times" [syn: {authority}, {authorization},
         {authorisation}]
     4: the act of final authorization; "it had the sanction of the
        church"
     v 1: give sanction to; "I approve of his educational policies"
          [syn: {approve}, {O.K.}, {okay}] [ant: {disapprove}]
     2: give authority or permission to
  ...
