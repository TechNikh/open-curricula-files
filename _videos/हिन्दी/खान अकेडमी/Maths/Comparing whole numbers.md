---
version: 1
type: video
provider: YouTube
id: 9pIQUa64K48
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 67090693-bc15-402c-a40c-d26e3d99220e
updated: 1486069613
title: Comparing whole numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/9pIQUa64K48/default.jpg
    - https://i3.ytimg.com/vi/9pIQUa64K48/1.jpg
    - https://i3.ytimg.com/vi/9pIQUa64K48/2.jpg
    - https://i3.ytimg.com/vi/9pIQUa64K48/3.jpg
---
