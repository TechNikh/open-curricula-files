---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Acousto-optics
offline_file: ""
offline_thumbnail: ""
uuid: 2c5f7214-c3f5-48d0-a88e-188b72923ccc
updated: 1484308885
title: Acousto-optics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Beugungsbild.jpg
tags:
    - Introduction
    - Acousto-optic effect
    - Acousto-optic devices
    - Acousto-optic modulator
    - Acousto-optic filter
    - Acousto-optic deflectors
    - Materials
categories:
    - Waves
---
Acousto-optics is a branch of physics that studies the interactions between sound waves and light waves, especially the diffraction of laser light by ultrasound (or sound in general) through an ultrasonic grating.
