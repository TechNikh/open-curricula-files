---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arrival
offline_file: ""
offline_thumbnail: ""
uuid: 52095ca4-78ae-4bc6-a093-ed20bec3048f
updated: 1484310577
title: arrival
categories:
    - Dictionary
---
arrival
     n 1: accomplishment of an objective [syn: {reaching}]
     2: the act of arriving at a certain place; "they awaited her
        arrival"
     3: someone who arrives (or has arrived) [syn: {arriver}, {comer}]
