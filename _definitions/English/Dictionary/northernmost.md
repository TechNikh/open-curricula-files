---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/northernmost
offline_file: ""
offline_thumbnail: ""
uuid: c7322cae-6d35-49ea-be8e-b87d3c2ce7a5
updated: 1484310439
title: northernmost
categories:
    - Dictionary
---
northernmost
     adj : situated farthest north; "Alaska is our northernmost state"
           [syn: {northmost}]
