---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Backdraft
offline_file: ""
offline_thumbnail: ""
uuid: 6e695ad0-ae13-43b8-8196-b24af6d975fa
updated: 1484308840
title: Backdraft
tags:
    - How backdrafts occur
    - Backdrafts and flashovers
    - In popular culture
categories:
    - Thermodynamics
---
A backdraft is a dramatic event caused by a fire, resulting from rapid re-introduction of oxygen to combustion in an oxygen-depleted environment; for example, the breaking of a window or opening of a door to an enclosed space. Backdrafts present a serious threat to firefighters.[1] There is some controversy concerning whether backdrafts should be considered a type of flashover (see below).
