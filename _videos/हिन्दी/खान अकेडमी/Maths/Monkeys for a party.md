---
version: 1
type: video
provider: YouTube
id: v64BosVNATY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 448cd984-c426-4e33-b7ea-16bfa7236e6d
updated: 1486069609
title: Monkeys for a party
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/v64BosVNATY/default.jpg
    - https://i3.ytimg.com/vi/v64BosVNATY/1.jpg
    - https://i3.ytimg.com/vi/v64BosVNATY/2.jpg
    - https://i3.ytimg.com/vi/v64BosVNATY/3.jpg
---
