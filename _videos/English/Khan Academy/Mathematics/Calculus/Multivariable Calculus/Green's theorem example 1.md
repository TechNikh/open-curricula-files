---
version: 1
type: video
provider: YouTube
id: gGXnILbrhsM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Green%27s%20theorem%20example%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f3f261d5-9399-43fd-9961-f996ef7147d6
updated: 1486069690
title: "Green's theorem example 1"
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/gGXnILbrhsM/default.jpg
    - https://i3.ytimg.com/vi/gGXnILbrhsM/1.jpg
    - https://i3.ytimg.com/vi/gGXnILbrhsM/2.jpg
    - https://i3.ytimg.com/vi/gGXnILbrhsM/3.jpg
---
