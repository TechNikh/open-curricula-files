---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/studied
offline_file: ""
offline_thumbnail: ""
uuid: c6007554-0207-404e-b087-4101d4a0bbc2
updated: 1484310355
title: studied
categories:
    - Dictionary
---
studied
     adj 1: produced or marked by conscious design or premeditation; "a
            studied smile"; "a note of biting irony and studied
            insult"- V.L.Parrington [syn: {deliberate}] [ant: {unstudied}]
     2: carefully practiced or designed or premeditated; "a studied
        reply" [syn: {designed(a)}, {studied(a)}]
