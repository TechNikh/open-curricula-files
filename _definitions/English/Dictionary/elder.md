---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elder
offline_file: ""
offline_thumbnail: ""
uuid: 2a794dbb-4ad9-45d2-9a79-11976a287719
updated: 1484310212
title: elder
categories:
    - Dictionary
---
elder
     adj 1: used of the older of two persons of the same name especially
            used to distinguish a father from his son; "Bill
            Adams, Sr." [syn: {older}, {sr.}]
     2: older brother or sister; "big sister" [syn: {big(a)}, {older}]
        [ant: {little(a)}]
     n 1: a person who is older than you are [syn: {senior}]
     2: any of numerous shrubs or small trees of temperate and
        subtropical northern hemisphere having white flowers and
        berrylike fruit [syn: {elderberry bush}]
     3: any of various church officers
