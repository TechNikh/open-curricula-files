---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/focussed
offline_file: ""
offline_thumbnail: ""
uuid: 310c22e6-9d31-4d8b-964b-368b304c99b4
updated: 1484310605
title: focussed
categories:
    - Dictionary
---
focussed
     adj 1: being in focus or brought into focus [syn: {focused}] [ant:
            {unfocused}]
     2: (of light rays) converging on a point; "focused light rays
        can set something afire" [syn: {focused}]
