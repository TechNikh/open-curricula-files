---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cob
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484326201
title: cob
categories:
    - Dictionary
---
cob
     n 1: nut of any of several trees of the genus Corylus [syn: {hazelnut},
           {filbert}, {cobnut}]
     2: stocky short-legged harness horse
     3: white gull having a black back and wings [syn: {black-backed
        gull}, {great black-backed gull}, {Larus marinus}]
     4: adult male swan
