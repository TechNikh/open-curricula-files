---
version: 1
type: video
provider: YouTube
id: IV4IUsholjg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Electrochemistry-%20Crash%20Course%20Chemistry%20%2336.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9361eaae-8860-4da7-8a03-5096d40e3b6d
updated: 1486069678
title: 'Electrochemistry- Crash Course Chemistry #36'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/IV4IUsholjg/default.jpg
    - https://i3.ytimg.com/vi/IV4IUsholjg/1.jpg
    - https://i3.ytimg.com/vi/IV4IUsholjg/2.jpg
    - https://i3.ytimg.com/vi/IV4IUsholjg/3.jpg
---
