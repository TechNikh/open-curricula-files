---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_H._Smith_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: a4ece0c5-dba4-4ea2-ba4e-0f22f34f71c9
updated: 1484309476
title: John H. Smith (mathematician)
categories:
    - Graph theorists
---
John Howard Smith is an American mathematician, a retired professor of mathematics at Boston College.[1] He received his Ph.D. from the Massachusetts Institute of Technology in 1963, under the supervision of Kenkichi Iwasawa.[1][2] In voting theory, he is known for the Smith set, the smallest nonempty set of candidates such that, in every pairwise matchup between a member and a non-member, the member is the winner, and for the Smith criterion, a property of certain election systems in which the winner is guaranteed to belong to the Smith set.[3] He has also made contributions to spectral graph theory[4] and additive number theory.[5]
