---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Samuel_L._Greitzer
offline_file: ""
offline_thumbnail: ""
uuid: c7a34a4c-8e56-42c6-9956-c4c8f7287417
updated: 1484309083
title: Samuel L. Greitzer
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Greitzer-Sam.jpg
tags:
    - Biography
    - Selected publications
categories:
    - Geometers
---
Samuel L. Greitzer (August 10, 1905 – February 22, 1988) was an American mathematician, the founding chairman of the United States of America Mathematical Olympiad, and the publisher of the precollege mathematics journal Arbelos.[1] Together with H.S.M. Coxeter in 1967, Greitzer coauthored the well-received textbook Geometry Revisited, which has remained in print for more than 40 years.[2]
