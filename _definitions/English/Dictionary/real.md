---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/real
offline_file: ""
offline_thumbnail: ""
uuid: 2b77e5c5-0792-4687-b694-6aa7a185b2d3
updated: 1484310218
title: real
categories:
    - Dictionary
---
real
     adj 1: being or occurring in fact or actuality; having verified
            existence; not illusory; "real objects"; "real people;
            not ghosts"; "a film based on real life"; "a real
            illness"; "real humility"; "Life is real! Life is
            earnest!"- Longfellow [syn: {existent}] [ant: {unreal}]
     2: no less than what is stated; worthy of the name; "the real
        reason"; "real war"; "a real friend"; "a real woman";
        "meat and potatoes--I call that a real meal"; "it's time
        he had a real job"; "it's no penny-ante job--he's making
        real money" [syn: {real(a)}] [ant: {unreal}]
     3: being or reflecting the essential or genuine ...
