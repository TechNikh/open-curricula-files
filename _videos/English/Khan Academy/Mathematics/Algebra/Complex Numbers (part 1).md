---
version: 1
type: video
provider: YouTube
id: kpywdu1afas
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Complex%20Numbers%20%28part%201%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4aa8b831-79ad-4c0b-ac8e-3c5bedd5daca
updated: 1486069569
title: Complex Numbers (part 1)
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/kpywdu1afas/default.jpg
    - https://i3.ytimg.com/vi/kpywdu1afas/1.jpg
    - https://i3.ytimg.com/vi/kpywdu1afas/2.jpg
    - https://i3.ytimg.com/vi/kpywdu1afas/3.jpg
---
Introduction to complex numbers. Adding, subtracting and multiplying complex numbers.
