---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chordal_bipartite_graph
offline_file: ""
offline_thumbnail: ""
uuid: 8ccebc50-9b64-4494-aedf-d1009ee434a6
updated: 1484308616
title: Chordal bipartite graph
tags:
    - Characterizations
    - Recognition
    - Complexity of problems
    - Related graph classes
    - Notes
categories:
    - Graph theory
---
In the mathematical area of graph theory, a chordal bipartite graph is a bipartite graph B = (X,Y,E) in which every cycle of length at least 6 in B has a chord, i.e., an edge that connects two vertices that are a distance > 1 apart from each other in the cycle. [1] A better name would be weakly chordal and bipartite since chordal bipartite graphs are in general not chordal as the induced cycle of length 4 shows.
