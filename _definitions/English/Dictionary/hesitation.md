---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hesitation
offline_file: ""
offline_thumbnail: ""
uuid: bf87be03-0823-46e6-9994-d4eeb7e0932a
updated: 1484310186
title: hesitation
categories:
    - Dictionary
---
hesitation
     n 1: indecision in speech or action [syn: {vacillation}, {wavering}]
     2: a certain degree of unwillingness; "a reluctance to commit
        himself"; "after some hesitation he agreed" [syn: {reluctance},
         {hesitancy}, {disinclination}, {indisposition}]
     3: the act of pausing uncertainly; "there was a hesitation in
        his speech" [syn: {waver}, {falter}, {faltering}]
