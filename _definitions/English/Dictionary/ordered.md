---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ordered
offline_file: ""
offline_thumbnail: ""
uuid: ad729f30-312d-4226-8a90-0a41ec5b70fb
updated: 1484310202
title: ordered
categories:
    - Dictionary
---
ordered
     adj 1: having a systematic arrangement; especially having elements
            succeeding in order according to rule; "an ordered
            sequence" [ant: {disordered}]
     2: marked by system or regularity or discipline; "a quiet
        ordered house"; "an orderly universe"; "a well regulated
        life" [syn: {orderly}, {regulated}]
     3: disposed or placed in a particular kind of order; "the
        carefully arranged chessmen"; "haphazardly arranged
        interlobular septa"; "comfortable chairs arranged around
        the fireplace" [syn: {arranged}] [ant: {disarranged}]
     4: marked by an orderly, logical, and aesthetically consistent
        relation of ...
