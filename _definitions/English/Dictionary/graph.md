---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/graph
offline_file: ""
offline_thumbnail: ""
uuid: 4cc06819-1961-4ad2-ae99-cccda2381049
updated: 1484310236
title: graph
categories:
    - Dictionary
---
graph
     n : a drawing illustrating the relations between certain
         quantities plotted with reference to a set of axes [syn:
         {graphical record}]
     v 1: represent by means of a graph; "chart the data" [syn: {chart}]
     2: plot upon a graph
