---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Global_cascades_model
offline_file: ""
offline_thumbnail: ""
uuid: 636762d3-08ec-48eb-b699-5eda01f82167
updated: 1484309435
title: Global cascades model
tags:
    - Model description
    - Global cascades condition
    - Relations with other contagion models
categories:
    - Network theory
---
Global cascades models are a class of models aiming to model large and rare cascades that are triggered by exogenous perturbations which are relatively small compared with the size of the system. The phenomenon occurs ubiquitously in various systems, like information cascades in social systems, stock market crashes in economic systems, and cascading failure in physics infrastructure networks. The models capture some essential properties of such phenomenon.
