---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rly
offline_file: ""
offline_thumbnail: ""
uuid: eace9dbf-d36f-479b-9ab6-5ffadcf6e711
updated: 1484308440
title: O RLY?
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-O_RLY.jpg
tags:
    - History
    - Hoots computer worm
categories:
    - Uncategorized
---
O RLY? is an Internet phenomenon, typically presented as an image macro featuring a snowy owl.[1] The phrase "O RLY?", an abbreviated form of "Oh, really?", is popularly used in Internet forums in a sarcastic manner, often in response to an obvious, predictable,[2][3] or blatantly false statement. Similar owl image macros followed the original to present different views, including images with the phrases "YA RLY" (Yeah, really.) and "NO WAI!!" (No way!).[4][5]
