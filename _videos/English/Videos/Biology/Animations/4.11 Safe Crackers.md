---
version: 1
type: video
provider: YouTube
id: _tw7v2rbbk4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/4.11%20Safe%20Crackers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: afbc4a33-3533-4b1a-a0c5-47fc041013a9
updated: 1486069674
title: 4.11 Safe Crackers
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/_tw7v2rbbk4/default.jpg
    - https://i3.ytimg.com/vi/_tw7v2rbbk4/1.jpg
    - https://i3.ytimg.com/vi/_tw7v2rbbk4/2.jpg
    - https://i3.ytimg.com/vi/_tw7v2rbbk4/3.jpg
---
