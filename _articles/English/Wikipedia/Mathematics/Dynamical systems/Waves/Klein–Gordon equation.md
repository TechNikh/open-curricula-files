---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Klein%E2%80%93Gordon_equation'
offline_file: ""
offline_thumbnail: ""
uuid: 8b3324cf-37a6-4788-a1e6-a904711c312b
updated: 1484308893
title: Klein–Gordon equation
tags:
    - Statement
    - History
    - Derivation
    - Klein–Gordon equation in a potential
    - Conserved current
    - Relativistic free particle solution
    - Action
    - Electromagnetic interaction
    - Gravitational interaction
    - Remarks
    - Notes
categories:
    - Waves
---
The Klein–Gordon equation (Klein–Fock–Gordon equation or sometimes Klein–Gordon–Fock equation) is a relativistic wave equation, related to the Schrödinger equation. It is second order in space and time and manifestly Lorentz covariant. It is a quantized version of the relativistic energy-momentum relation. Its solutions include a quantum scalar or pseudoscalar field, a field whose quanta are spinless particles. Its theoretical relevance is similar to that of the Dirac equation.[1] Electromagnetic interactions can be incorporated, forming the topic of scalar electrodynamics, but because common spinless particles like the pi-mesons are unstable and also experience the strong ...
