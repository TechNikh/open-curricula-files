---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/periodically
offline_file: ""
offline_thumbnail: ""
uuid: 79ecedde-d468-4788-8744-2019b741d77b
updated: 1484310369
title: periodically
categories:
    - Dictionary
---
periodically
     adv : in a sporadic manner; "he only works sporadically" [syn: {sporadically}]
