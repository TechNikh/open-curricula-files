---
version: 1
type: video
provider: YouTube
id: gyqodNhM3EU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Probability/Precalculus/Probability%20and%20combinatorics/Mega%20millions%20jackpot%20probability.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e59bb925-b596-4845-b687-46bf12fa4740
updated: 1486069689
title: Mega millions jackpot probability
categories:
    - Probability and combinatorics
thumbnail_urls:
    - https://i3.ytimg.com/vi/gyqodNhM3EU/default.jpg
    - https://i3.ytimg.com/vi/gyqodNhM3EU/1.jpg
    - https://i3.ytimg.com/vi/gyqodNhM3EU/2.jpg
    - https://i3.ytimg.com/vi/gyqodNhM3EU/3.jpg
---
