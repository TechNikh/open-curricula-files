---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/postpone
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484549761
title: postpone
categories:
    - Dictionary
---
postpone
     v : hold back to a later time; "let's postpone the exam" [syn: {prorogue},
          {hold over}, {put over}, {table}, {shelve}, {set back},
         {defer}, {remit}, {put off}]
