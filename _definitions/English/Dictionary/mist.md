---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mist
offline_file: ""
offline_thumbnail: ""
uuid: 6f4c9559-d6cb-494c-806d-f02bbc7bed35
updated: 1484310224
title: mist
categories:
    - Dictionary
---
mist
     n : a thin fog with condensation near the ground
     v 1: become covered with mist; "The windshield misted over" [syn:
           {mist over}]
     2: make less visible or unclear; "The stars are obscured by the
        clouds" [syn: {obscure}, {befog}, {becloud}, {obnubilate},
         {haze over}, {fog}, {cloud}]
     3: spray finely or cover with mist
