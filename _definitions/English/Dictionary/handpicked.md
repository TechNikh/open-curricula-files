---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/handpicked
offline_file: ""
offline_thumbnail: ""
uuid: cef247a6-ab7f-42a8-9398-3700b2ab95c1
updated: 1484310409
title: handpicked
categories:
    - Dictionary
---
hand-picked
     adj : carefully selected; "a hand-picked jury"
