---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/debate
offline_file: ""
offline_thumbnail: ""
uuid: 8ccec053-7d73-4124-8a0a-753fff50184e
updated: 1484310445
title: debate
categories:
    - Dictionary
---
debate
     n 1: a discussion in which reasons are advanced for and against
          some proposition or proposal; "the argument over foreign
          aid goes on and on" [syn: {argument}, {argumentation}]
     2: the formal presentation of and opposition to a stated
        proposition (usually followed by a vote) [syn: {disputation},
         {public debate}]
     v 1: argue with one another; "We debated the question of
          abortion"; "John debated Mary"
     2: think about carefully; weigh; "They considered the
        possibility of a strike"; "Turn the proposal over in your
        mind" [syn: {consider}, {moot}, {turn over}, {deliberate}]
     3: discuss the pros and cons of ...
