---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/older
offline_file: ""
offline_thumbnail: ""
uuid: 8fdb4f0e-6a3e-4fc8-9d59-d616f701ccc2
updated: 1484310447
title: older
categories:
    - Dictionary
---
older
     adj 1: advanced in years; (`aged' is pronounced as two syllables);
            "aged members of the society"; "elderly residents
            could remember the construction of the first
            skyscraper"; "senior citizen" [syn: {aged}, {elderly},
             {senior}]
     2: older brother or sister; "big sister" [syn: {big(a)}, {elder}]
        [ant: {little(a)}]
     3: used of the older of two persons of the same name especially
        used to distinguish a father from his son; "Bill Adams,
        Sr." [syn: {elder}, {sr.}]
     4: old in experience; "an old offender"; "the older soldiers"
        [syn: {old}]
