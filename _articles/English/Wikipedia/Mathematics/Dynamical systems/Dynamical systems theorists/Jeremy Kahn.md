---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jeremy_Kahn
offline_file: ""
offline_thumbnail: ""
uuid: 629da589-cef6-4049-b764-cdeb8bf4014f
updated: 1484308692
title: Jeremy Kahn
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jeremy_Kahn_and_Vladimir_Markovic.jpg
categories:
    - Dynamical systems theorists
---
Kahn grew up in New York City. He studied mathematics for his bachelor's degree at Harvard University then received his Ph.D. from the University of California, Berkeley in 1995 under Curtis McMullen with thesis Holomorphic removability of quadratic polynomial Julia sets.[1] As a postdoc he was at the University of Toronto. He was assistant professor at Caltech and at the State University of New York at Stony Brook. After that he worked for the investment firm Highbridge Capital Management as an analyst in financial mathematics. Since 2011 he has been a professor at Brown University.
