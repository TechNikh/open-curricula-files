---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/happening
offline_file: ""
offline_thumbnail: ""
uuid: 82b56a4f-4e25-46c9-940e-0c862060a10c
updated: 1484310447
title: happening
categories:
    - Dictionary
---
happening
     adj : taking place; "the parade is still happening"
     n : an event that happens [syn: {occurrence}, {natural event}]
