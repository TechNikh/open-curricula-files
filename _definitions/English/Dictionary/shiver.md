---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shiver
offline_file: ""
offline_thumbnail: ""
uuid: 158789a8-99c7-4caf-9c96-1b30ed3a3e90
updated: 1484310168
title: shiver
categories:
    - Dictionary
---
shiver
     n 1: reflex shaking caused by cold or fear or excitement [syn: {tremble},
           {shake}]
     2: an almost pleasurable sensation of fright; "a frisson of
        surprise shot through him" [syn: {frisson}, {chill}, {quiver},
         {shudder}, {thrill}, {tingle}]
     v 1: tremble convulsively, as from fear or excitement [syn: {shudder},
           {throb}, {thrill}]
     2: shake, as from cold; "The children are shivering--turn on
        the heat!" [syn: {shudder}]
