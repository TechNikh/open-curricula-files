---
version: 1
type: video
provider: YouTube
id: Lq7gzGjSWuQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 47a5c174-1ae4-457f-8076-d1af8f37a922
updated: 1486069643
title: Fractal zoom Mandelbrot Julia trap HQ
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Lq7gzGjSWuQ/default.jpg
    - https://i3.ytimg.com/vi/Lq7gzGjSWuQ/1.jpg
    - https://i3.ytimg.com/vi/Lq7gzGjSWuQ/2.jpg
    - https://i3.ytimg.com/vi/Lq7gzGjSWuQ/3.jpg
---
