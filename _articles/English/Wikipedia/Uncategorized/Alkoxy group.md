---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Alkoxy
offline_file: ""
offline_thumbnail: ""
uuid: 7c1d4542-e829-48f1-a995-1aca438a45b4
updated: 1484308401
title: Alkoxy group
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Alkoxy_group.svg.png
categories:
    - Uncategorized
---
In chemistry, the alkoxy group is an alkyl (carbon and hydrogen chain) group singular bonded to oxygen thus: R–O. The range of alkoxy groups is great, the simplest being methoxy (CH3O–). An ethoxy group (CH3CH2O–) is found in the organic compound ethyl phenyl ether, C6H5OCH2CH3 which is also known as ethoxybenzene. Related to alkoxy groups are aryloxy groups, which have an aryl group singular bonded to oxygen such as the phenoxy group (C6H5O–).
