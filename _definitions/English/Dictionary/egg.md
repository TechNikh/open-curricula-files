---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/egg
offline_file: ""
offline_thumbnail: ""
uuid: c38d7887-ea5c-47e4-902c-9da3d0fa8cff
updated: 1484310286
title: egg
categories:
    - Dictionary
---
egg
     n 1: animal reproductive body consisting of an ovum or embryo
          together with nutritive and protective envelopes;
          especially the thin-shelled reproductive body laid by
          e.g. female birds
     2: oval reproductive body of a fowl (especially a hen) used as
        food [syn: {eggs}]
     3: one of the two male reproductive glands that produce
        spermatozoa and secrete androgens; "she kicked him in the
        balls and got away" [syn: {testis}, {testicle}, {orchis},
        {ball}, {ballock}, {bollock}, {nut}]
     v 1: throw eggs at
     2: coat with beaten egg; "egg a schnitzel"
