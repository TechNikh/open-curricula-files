---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pascal%27s_rule'
offline_file: ""
offline_thumbnail: ""
uuid: fdea3236-5c67-4963-b623-91d6d9dd0e63
updated: 1484308592
title: "Pascal's rule"
tags:
    - Combinatorial proof
    - Algebraic proof
    - Generalization
    - Sources
categories:
    - Combinatorics
---
where 
  
    
      
        
          
            
              (
            
            
              n
              k
            
            
              )
            
          
        
      
    
    {\displaystyle {n \choose k}}
  
 is a binomial coefficient. This is also commonly written
