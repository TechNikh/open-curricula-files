---
version: 1
type: video
provider: YouTube
id: jgxPlHxff3I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 484f7616-54b9-43cd-932d-7c8eb66c771f
updated: 1486069643
title: 'A Journey Through Fantasy - Vorticity'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/jgxPlHxff3I/default.jpg
    - https://i3.ytimg.com/vi/jgxPlHxff3I/1.jpg
    - https://i3.ytimg.com/vi/jgxPlHxff3I/2.jpg
    - https://i3.ytimg.com/vi/jgxPlHxff3I/3.jpg
---
