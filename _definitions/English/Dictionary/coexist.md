---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coexist
offline_file: ""
offline_thumbnail: ""
uuid: 18eb6d8d-c490-48be-bf04-e948cbef6f02
updated: 1484310252
title: coexist
categories:
    - Dictionary
---
coexist
     v 1: coexist peacefully, as of nations
     2: exist together
