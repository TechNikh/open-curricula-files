---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cultural
offline_file: ""
offline_thumbnail: ""
uuid: a4d54bde-e9c9-4a96-9af3-8a05eafe690e
updated: 1484310451
title: cultural
categories:
    - Dictionary
---
cultural
     adj 1: of or relating to the arts and manners that a group favors;
            "cultural events"; "a person of broad cultural
            interests"
     2: denoting or deriving from or distinctive of the ways of
        living built up by a group of people; "influenced by
        ethnic and cultural ties"- J.F.Kennedy; "ethnic food"
        [syn: {ethnic}, {ethnical}]
     3: of or relating to the shared knowledge and values of a
        society; "cultural roots"
     4: relating to the raising of plants or animals; "a cultural
        variety"
