---
version: 1
type: article
id: https://en.wikipedia.org/wiki/No-wandering-domain_theorem
offline_file: ""
offline_thumbnail: ""
uuid: e4fbe601-c276-4553-85d5-a0ddd0791ced
updated: 1484308690
title: No-wandering-domain theorem
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Wandering_domains_for_the_entire_function_f%2528z%2529%253Dz%252B2%25CF%2580sin%2528z%2529.png'
categories:
    - Theorems in dynamical systems
---
The theorem states that a rational map f : Ĉ → Ĉ with deg(f) ≥ 2 does not have a wandering domain, where Ĉ denotes the Riemann sphere. More precisely, for every component U in the Fatou set of f, the sequence
