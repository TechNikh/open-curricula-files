---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fractal_dimension_on_networks
offline_file: ""
offline_thumbnail: ""
uuid: 5edf0671-2711-4878-9508-3a71238c164e
updated: 1484309434
title: Fractal dimension on networks
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Clustergrow.jpg
tags:
    - Self-similarity of complex networks
    - The methods for calculation of the dimension
    - The box counting method
    - The cluster growing method
    - Fractal scaling in scale-free networks
    - Box-counting and renormalization
    - Skeleton and fractal scaling
    - Real-world fractal networks
    - Other definitions for network dimensions
categories:
    - Network theory
---
