---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gibbs_paradox
offline_file: ""
offline_thumbnail: ""
uuid: 86fefd08-75d0-48ff-8555-e04239bdc83a
updated: 1484308767
title: Gibbs paradox
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Ideal_gas_3_particles.gif
tags:
    - Illustration of the problem
    - >
        Calculating the entropy of ideal gas, and making it
        extensive
    - The mixing paradox
    - Non-extensive entropy of two ideal gases and how to fix it
    - Setup
    - "Gibb's paradox in a one-dimensional gas"
    - Two standard ways to make the classical entropy extensive
    - "Swendsen's particle-exchange approach"
    - >
        Visualizing the particle-exchange approach in three
        dimensions
categories:
    - Entropy
---
In statistical mechanics, a semi-classical derivation of the entropy that does not take into account the indistinguishability of particles, yields an expression for the entropy which is not extensive (is not proportional to the amount of substance in question). This leads to a paradox known as the Gibbs paradox, after Josiah Willard Gibbs. The paradox allows for the entropy of closed systems to decrease, violating the second law of thermodynamics. A related paradox is the "mixing paradox". If one takes the perspective that the definition of entropy must be changed so as to ignore particle permutation, the paradox is averted.
