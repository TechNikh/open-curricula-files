---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assortment
offline_file: ""
offline_thumbnail: ""
uuid: 099fb1ab-5ba9-41b7-bf43-01dfab60b728
updated: 1484310299
title: assortment
categories:
    - Dictionary
---
assortment
     n 1: a collection containing a variety of sorts of things; "a
          great assortment of cars was on display"; "he had a
          variety of disorders"; "a veritable smorgasbord of
          religions" [syn: {mixture}, {mixed bag}, {miscellany}, {miscellanea},
           {variety}, {salmagundi}, {smorgasbord}, {potpourri}, {motley}]
     2: the act of distributing things into classes or categories of
        the same type [syn: {categorization}, {categorisation}, {classification},
         {compartmentalization}, {compartmentalisation}]
