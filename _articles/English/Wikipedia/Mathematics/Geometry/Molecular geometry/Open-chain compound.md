---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Open-chain_compound
offline_file: ""
offline_thumbnail: ""
uuid: d8dd0789-fe1f-407d-b2d5-bbf522cc1ac9
updated: 1484309279
title: Open-chain compound
categories:
    - Molecular geometry
---
In chemistry, an open-chain compound (also spelled as open chain compound) or acyclic compound (Greek prefix "α", without and "κύκλος", cycle) is a compound with a linear structure, rather than a cyclic one. [1] An open-chain compound having no side chains is called a straight-chain compound (also spelled as straight chain compound). [2] [3] Many of the simple molecules of organic chemistry, such as the alkanes and alkenes, have both linear and ring isomers, that is, both acyclic and cyclic, with the latter often classified as aromatic. For those with 4 or more carbons, the linear forms can have straight-chain or branched-chain isomers. The lowercase prefix n- denotes the ...
