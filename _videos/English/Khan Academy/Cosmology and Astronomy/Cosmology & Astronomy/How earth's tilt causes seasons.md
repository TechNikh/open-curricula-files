---
version: 1
type: video
provider: YouTube
id: 05qDIjKevJo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/How%20earth%27s%20tilt%20causes%20seasons.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 46a37361-1778-41d0-ac96-a52356c2d682
updated: 1486069685
title: "How earth's tilt causes seasons"
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/05qDIjKevJo/default.jpg
    - https://i3.ytimg.com/vi/05qDIjKevJo/1.jpg
    - https://i3.ytimg.com/vi/05qDIjKevJo/2.jpg
    - https://i3.ytimg.com/vi/05qDIjKevJo/3.jpg
---
