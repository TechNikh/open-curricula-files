---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resent
offline_file: ""
offline_thumbnail: ""
uuid: 4fa9d1aa-5348-4ed9-89c9-5854285ddee0
updated: 1484310441
title: resent
categories:
    - Dictionary
---
resent
     v 1: feel bitter or indignant about; "She resents being paid less
          than her co-workers"
     2: wish ill or allow unwillingly [syn: {begrudge}] [ant: {wish}]
