---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Low-pressure_area
offline_file: ""
offline_thumbnail: ""
uuid: 6dc5119d-f240-4840-a9f4-d513a6faeebe
updated: 1484308875
title: Low-pressure area
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Southern_hemisphere_extratropical_cyclone.jpg
tags:
    - Formation
    - Climatology
    - Mid-latitudes and subtropics
    - Monsoon trough
    - Tropical cyclone
    - Associated weather
categories:
    - Vortices
---
A low-pressure area, low or depression, is a region where the atmospheric pressure is lower than that of surrounding locations. Low-pressure systems form under areas of wind divergence that occur in the upper levels of the troposphere. The formation process of a low-pressure area is known as cyclogenesis. Within the field of meteorology, atmospheric divergence aloft occurs in two areas. The first area is on the east side of upper troughs, which form half of a Rossby wave within the Westerlies (a trough with large wavelength that extends through the troposphere). A second area of wind divergence aloft occurs ahead of embedded shortwave troughs, which are of smaller wavelength. Diverging ...
