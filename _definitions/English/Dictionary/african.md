---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/african
offline_file: ""
offline_thumbnail: ""
uuid: 743121c1-c03d-4049-97b0-07ce7da6d534
updated: 1484310279
title: african
categories:
    - Dictionary
---
African
     adj : of or relating to the nations of Africa or their peoples;
           "African languages"
     n : a native or inhabitant of Africa
