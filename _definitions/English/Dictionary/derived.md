---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/derived
offline_file: ""
offline_thumbnail: ""
uuid: 1aa030b6-dd48-43f4-b0d6-edd5e8196b9a
updated: 1484310271
title: derived
categories:
    - Dictionary
---
derived
     adj 1: determined by mathematical computation; "the calculated
            velocity of a bullet"; "a derived value" [syn: {calculated}]
     2: formed or developed from something else; not original; "the
        belief that classes and organizations are secondary and
        derived"- John Dewey [ant: {underived}]
