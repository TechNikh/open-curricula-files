---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alphabetical
offline_file: ""
offline_thumbnail: ""
uuid: 5f21cf9d-a83f-431b-adcc-2fbcb144c3fc
updated: 1484310421
title: alphabetical
categories:
    - Dictionary
---
alphabetical
     adj 1: relating to or expressed by a writing system that uses an
            alphabet; "alphabetical writing system" [syn: {alphabetic}]
            [ant: {analphabetic}]
     2: arranged in order according to the alphabet; "an alphabetic
        arrangement"; "dictionaries list words in alphabetical
        order" [syn: {alphabetic}] [ant: {analphabetic}]
