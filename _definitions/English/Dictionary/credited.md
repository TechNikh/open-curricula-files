---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/credited
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484427661
title: credited
categories:
    - Dictionary
---
credited
     adj : (usually followed by `to') given credit for; "an invention
           credited to Edison" [syn: {credited(p)}]
