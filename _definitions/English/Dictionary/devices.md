---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/devices
offline_file: ""
offline_thumbnail: ""
uuid: 95dcb2fe-cc4a-45b5-a51e-4cfe5ab394dc
updated: 1484310240
title: devices
categories:
    - Dictionary
---
devices
     n : an inclination or desire; used in the plural in the phrase
         `left to your own devices'; "eventually the family left
         the house to the devices of this malevolent force"; "the
         children were left to their own devices"
