---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/desire
offline_file: ""
offline_thumbnail: ""
uuid: c1ad6ac1-21c8-4b95-b4a5-90d33f73b69b
updated: 1484310443
title: desire
categories:
    - Dictionary
---
desire
     n 1: the feeling that accompanies an unsatisfied state
     2: an inclination to want things; "a man of many desires"
     3: something that is desired
     v 1: feel or have a desire for; want strongly; "I want to go home
          now"; "I want my own room" [syn: {want}]
     2: expect and wish; "I trust you will behave better from now
        on"; "I hope she understands that she cannot expect a
        raise" [syn: {hope}, {trust}]
     3: express a desire for
