---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diathermal_wall
offline_file: ""
offline_thumbnail: ""
uuid: a8549365-b523-4017-a064-e3030e6e543b
updated: 1484308838
title: Diathermal wall
tags:
    - Definitions of transfer of heat
    - Thermodynamic stream of thinking
    - Mechanical stream of thinking
    - Accounts of the diathermal wall
    - Bibliography
categories:
    - Thermodynamics
---
The diathermal wall is important because, in thermodynamics, it is customary to assume a priori, for a closed system, the physical existence of transfer of energy across a wall that is impermeable to matter but is not adiabatic, transfer which is called transfer of energy as heat, though it is not customary to label this assumption separately as an axiom or numbered law.[1]
