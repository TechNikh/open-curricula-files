---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/traced
offline_file: ""
offline_thumbnail: ""
uuid: 32b8c0be-c090-46ec-8f0f-8ce924e63947
updated: 1484310279
title: traced
categories:
    - Dictionary
---
traced
     adj : derived by copying something else; especially by following
           lines seen through a transparent sheet [syn: {copied}]
