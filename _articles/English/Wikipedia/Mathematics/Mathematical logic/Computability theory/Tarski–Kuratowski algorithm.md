---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tarski%E2%80%93Kuratowski_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 47b14b9f-f743-4102-a40f-49f751efff90
updated: 1484309490
title: Tarski–Kuratowski algorithm
categories:
    - Computability theory
---
In computability theory and mathematical logic the Tarski–Kuratowski algorithm is a non-deterministic algorithm which provides an upper bound for the complexity of formulas in the arithmetical hierarchy and analytical hierarchy.
