---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_polytope
offline_file: ""
offline_thumbnail: ""
uuid: 691fd71e-a182-4dd9-b0e5-bfed5132079c
updated: 1484309192
title: Convex polytope
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-3dpoly.svg.png
tags:
    - Examples
    - Definitions
    - Vertex representation (Convex hull)
    - Intersection of half-spaces
    - Finite basis theorem
    - Properties
    - The face lattice
    - Topological properties
    - Simplicial decomposition
    - Algorithmic problems for a convex polytope
    - Construction of representations
categories:
    - Convex geometry
---
A convex polytope is a special case of a polytope, having the additional property that it is also a convex set of points in the n-dimensional space Rn.[1] Some authors use the terms "convex polytope" and "convex polyhedron" interchangeably, while others prefer to draw a distinction between the notions of a polyhedron and a polytope.
