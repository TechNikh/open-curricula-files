---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Feferman%E2%80%93Sch%C3%BCtte_ordinal'
offline_file: ""
offline_thumbnail: ""
uuid: 4c7d4d26-d7da-4562-ac7e-31686e67c04a
updated: 1484309495
title: Feferman–Schütte ordinal
categories:
    - Proof theory
---
In mathematics, the Feferman–Schütte ordinal Γ0 is a large countable ordinal. It is the proof theoretic ordinal of several mathematical theories, such as arithmetical transfinite recursion. It is named after Solomon Feferman and Kurt Schütte.
