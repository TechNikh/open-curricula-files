---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dietary
offline_file: ""
offline_thumbnail: ""
uuid: 2fe64c75-a052-4bbd-b406-d81c101d6725
updated: 1484310535
title: dietary
categories:
    - Dictionary
---
dietary
     adj : of or relating to the diet; "dietary restrictions" [syn: {dietetic},
            {dietetical}]
     n : a regulated daily food allowance
