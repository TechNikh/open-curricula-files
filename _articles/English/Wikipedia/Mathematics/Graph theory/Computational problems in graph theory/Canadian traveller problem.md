---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Canadian_traveller_problem
offline_file: ""
offline_thumbnail: ""
uuid: 4dd301ce-fc77-418b-bdc6-0f853461a66d
updated: 1484309391
title: Canadian traveller problem
tags:
    - Problem description
    - Variants
    - Formal definition
    - Complexity
    - Applications
    - Open problems
    - Notes
categories:
    - Computational problems in graph theory
---
In computer science and graph theory, the Canadian Traveller Problem (CTP) is a generalization of the shortest path problem to graphs that are partially observable. In other words, the graph is revealed while it is being explored, and explorative edges are charged even if they do not contribute to the final path.
