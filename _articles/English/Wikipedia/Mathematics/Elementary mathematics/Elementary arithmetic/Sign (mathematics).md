---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sign_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 7d5547e8-4b78-4550-914b-ce950e5b6770
updated: 1484308925
title: Sign (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-PlusMinus.svg.png
tags:
    - Sign of a number
    - Sign of zero
    - Terminology for signs
    - Sign convention
    - Sign function
    - Meanings of sign
    - Sign of an angle
    - Sign of a change
    - Sign of a direction
    - Signedness in computing
    - Other meanings
categories:
    - Elementary arithmetic
---
In mathematics, the concept of sign originates from the property of every non-zero real number to be positive or negative. Zero itself is signless, although in some contexts it makes sense to consider a signed zero. Along its application to real numbers, "change of sign" is used throughout mathematics and physics to denote the additive inverse (multiplication to −1), even for quantities which are not real numbers (so, which are not prescribed to be either positive, negative, or zero). Also, the word "sign" can indicate aspects of mathematical objects that resemble positivity and negativity, such as the sign of a permutation (see below).
