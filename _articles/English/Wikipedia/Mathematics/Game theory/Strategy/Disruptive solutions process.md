---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Disruptive_solutions_process
offline_file: ""
offline_thumbnail: ""
uuid: 98e28baa-43e4-48d6-affe-b85abf5767b0
updated: 1484309063
title: Disruptive solutions process
tags:
    - Overview
    - Basic process
    - History and program successes
    - In the news
    - Citations and notes
categories:
    - Strategy
---
The disruptive solutions process (DSP) is a concept for innovation execution applied to the mishap prevention part of the combat operations process, often at tactical or operational level, primarily in Air National Guard applications. However, it has been used successfully in other government agencies and in the private sector. At its core is the notion of iterative, low cost, first-to-market development. The term 'disruptive' was borrowed from the marketing term disruptive technologies. DSP was created in 2005 by fighter pilot and United States Air Force/Air National Guard Colonel Edward Vaughan.[1]
