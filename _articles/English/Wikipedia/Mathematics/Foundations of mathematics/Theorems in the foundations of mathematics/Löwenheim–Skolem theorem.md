---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/L%C3%B6wenheim%E2%80%93Skolem_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 08ce44fe-1cfe-487c-90f0-443dbacd593e
updated: 1484308994
title: Löwenheim–Skolem theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lowenheim-skolem.svg.png
tags:
    - Background
    - Precise statement
    - Examples and consequences
    - Proof Sketch
    - Downward part
    - Upward part
    - Historical notes
    - Historical publications
    - Secondary sources
categories:
    - Theorems in the foundations of mathematics
---
In mathematical logic, the Löwenheim–Skolem theorem, named for Leopold Löwenheim and Thoralf Skolem, states that if a countable first-order theory has an infinite model, then for every infinite cardinal number κ it has a model of size κ. The result implies that first-order theories are unable to control the cardinality of their infinite models, and that no first-order theory with an infinite model can have a unique model up to isomorphism.
