---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Seed_dormancy
offline_file: ""
offline_thumbnail: ""
uuid: 063e04bf-128c-41d8-b6f2-b00ba1f11709
updated: 1484308363
title: Seed dormancy
tags:
    - Overview
    - Exogenous dormancy
    - Physical dormancy
    - Mechanical dormancy
    - Chemical dormancy
    - Endogenous dormancy
    - Physiological dormancy
    - Morphological dormancy
    - Combined dormancy
    - Combinational dormancy
    - Secondary dormancy
categories:
    - Uncategorized
---
A dormant seed is one that is unable to germinate in a specified period of time under a combination of environmental factors that are normally suitable for the germination of the non-dormant seed.[1] Dormancy is a mechanism to prevent germination during unsuitable ecological conditions, when the probability of seedling survival is low.[2]
