---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/density
offline_file: ""
offline_thumbnail: ""
uuid: 108ed4a6-9504-49d4-a807-00e881405004
updated: 1484310224
title: density
categories:
    - Dictionary
---
density
     n 1: the amount per unit size [syn: {denseness}]
     2: the spatial property of being crowded together [syn: {concentration},
         {denseness}, {compactness}] [ant: {distribution}]
