---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/morph
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484529661
title: morph
categories:
    - Dictionary
---
morph
     v 1: cause to change shape in a computer animation; "The computer
          programmer morphed the image"
     2: change shape as via computer animation; "In the video,
        Michael Jackson morphed into a panther"
