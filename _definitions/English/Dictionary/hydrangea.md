---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydrangea
offline_file: ""
offline_thumbnail: ""
uuid: 04eb55e3-82aa-4353-8fcb-4b9418f28e4e
updated: 1484310381
title: hydrangea
categories:
    - Dictionary
---
hydrangea
     n : any of various deciduous or evergreen shrubs of the genus
         Hydrangea
