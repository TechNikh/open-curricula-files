---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blockade
offline_file: ""
offline_thumbnail: ""
uuid: d2db1784-fbaa-46b4-a24a-ea41ddd40f5d
updated: 1484310571
title: blockade
categories:
    - Dictionary
---
blockade
     n 1: a war measure that isolates some area of importance to the
          enemy [syn: {encirclement}]
     2: prevents access or progress
     v 1: hinder or prevent the progress or accomplishment of; "His
          brother blocked him at every turn" [syn: {obstruct}, {block},
           {hinder}, {stymie}, {stymy}, {embarrass}]
     2: render unsuitable for passage; "block the way"; "barricade
        the streets"; "stop the busy road" [syn: {barricade}, {block},
         {stop}, {block off}, {block up}, {bar}]
     3: obstruct access to [syn: {block off}]
     4: impose a blockade on [syn: {seal off}]
