---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/minority
offline_file: ""
offline_thumbnail: ""
uuid: aa28d3fe-a234-4e57-9210-392228a27197
updated: 1484310565
title: minority
categories:
    - Dictionary
---
minority
     n 1: a group of people who differ racially or politically from a
          larger group of which it is a part
     2: being or relating to the smaller in number of two parts;
        "when the vote was taken they were in the minority"; "he
        held a minority position" [ant: {majority}]
     3: any age prior to the legal age [syn: {nonage}] [ant: {majority}]
