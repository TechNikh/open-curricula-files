---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Surya_Siddhanta
offline_file: ""
offline_thumbnail: ""
uuid: d00864cd-385d-48e6-99b0-3d3ff444ceaf
updated: 1484309247
title: Surya Siddhanta
tags:
    - Textual history and influence
    - Contents
    - Astronomy
    - Time cycles
    - Planetary diameters
    - Trigonometry
    - Calendrical uses
    - Editions
    - Notes
categories:
    - History of geometry
---
The Surya Siddhanta is the name of multiple treatises (siddhanta) in Indian astronomy. The extant text as translated by Burgess (1860) is medieval (c. 12th century), but it is clearly based on older versions, thought to have been composed in the early 6th century AD.
