---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/superscript
offline_file: ""
offline_thumbnail: ""
uuid: 64579fee-1c92-4fc4-8610-13d5f9e2010b
updated: 1484310393
title: superscript
categories:
    - Dictionary
---
superscript
     adj : written or printed above and to one side of another
           character [syn: {superior}] [ant: {subscript}, {adscript}]
     n : a character or symbol set or printed or written above and
         immediately to one side of another character [syn: {superior}]
         [ant: {subscript}]
