---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/floor
offline_file: ""
offline_thumbnail: ""
uuid: 8b258099-5b65-4bec-b995-264491ad1603
updated: 1484310230
title: floor
categories:
    - Dictionary
---
floor
     n 1: the inside lower horizontal surface (as of a room or
          hallway); "they needed rugs to cover the bare floors"
          [syn: {flooring}]
     2: structure consisting of a room or set of rooms comprising a
        single level of a multilevel building; "what level is the
        office on?" [syn: {level}, {storey}, {story}]
     3: a lower limit; "the government established a wage floor"
        [syn: {base}]
     4: the ground on which people and animals move about; "the fire
        spared the forest floor"
     5: the bottom surface of any a cave or lake etc.
     6: the occupants of a floor; "the whole floor complained about
        the lack of heat"
     7: the ...
