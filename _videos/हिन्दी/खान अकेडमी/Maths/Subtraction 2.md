---
version: 1
type: video
provider: YouTube
id: gyVWjlTm6K4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c13b8e6e-065b-4947-9071-a92463ef157c
updated: 1486069605
title: Subtraction 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/gyVWjlTm6K4/default.jpg
    - https://i3.ytimg.com/vi/gyVWjlTm6K4/1.jpg
    - https://i3.ytimg.com/vi/gyVWjlTm6K4/2.jpg
    - https://i3.ytimg.com/vi/gyVWjlTm6K4/3.jpg
---
