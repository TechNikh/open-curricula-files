---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/divorce
offline_file: ""
offline_thumbnail: ""
uuid: 98c0af60-aad4-4967-bd98-9b3a65cf133b
updated: 1484310571
title: divorce
categories:
    - Dictionary
---
divorce
     n : the legal dissolution of a marriage [syn: {divorcement}]
     v 1: part; cease or break association with; "She disassociated
          herself from the organization when she found out the
          identity of the president" [syn: {disassociate}, {dissociate},
           {disunite}, {disjoint}]
     2: get a divorce; formally terminate a marriage; "The couple
        divorced after only 6 months" [syn: {split up}]
