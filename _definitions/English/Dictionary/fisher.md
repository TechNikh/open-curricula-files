---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fisher
offline_file: ""
offline_thumbnail: ""
uuid: d5639c07-bc8d-41a4-8b29-e000db16eebc
updated: 1484310439
title: fisher
categories:
    - Dictionary
---
fisher
     n 1: someone whose occupation is catching fish [syn: {fisherman}]
     2: large dark brown North American arboreal carnivorous mammal
        [syn: {pekan}, {fisher cat}, {black cat}, {Martes pennanti}]
