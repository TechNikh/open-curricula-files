---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deserve
offline_file: ""
offline_thumbnail: ""
uuid: 63bb9eb9-d697-4f3c-871b-92f7d09987f5
updated: 1484310140
title: deserve
categories:
    - Dictionary
---
deserve
     v : be worthy or deserving; "You deserve a promotion after all
         the hard work you have done" [syn: {merit}]
