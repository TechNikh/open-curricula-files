---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/olm
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484366161
title: olm
categories:
    - Dictionary
---
olm
     n : European cave-dwelling aquatic salamander with permanent
         external gills [syn: {Proteus anguinus}]
