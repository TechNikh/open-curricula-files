---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neighbour
offline_file: ""
offline_thumbnail: ""
uuid: 1e15c72f-e665-4d2f-9d7e-0a4a66f26d9c
updated: 1484310464
title: neighbour
categories:
    - Dictionary
---
neighbour
     adj : situated near one another; "neighbor states" [syn: {neighbor},
            {neighboring(a)}, {neighbouring(a)}]
     n 1: a person who lives (or is located) near another [syn: {neighbor}]
     2: a nearby object of the same kind; "Fort Worth is a neighbor
        of Dallas"; "what is the closest neighbor to the Earth?"
        [syn: {neighbor}]
     v 1: live or be located as a neighbor; "the neighboring house"
          [syn: {neighbor}]
     2: be located near or adjacent to; "Pakistan neighbors India"
        [syn: {neighbor}]
