---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pia
offline_file: ""
offline_thumbnail: ""
uuid: fe4b9f32-f7ba-48bb-8fbb-4d04564b1829
updated: 1484310216
title: pia
categories:
    - Dictionary
---
pia
     n : perennial herb of East Indies to Polynesia and Australia;
         cultivated for its large edible root yielding Otaheite
         arrowroot starch [syn: {Indian arrowroot}, {Tacca
         leontopetaloides}, {Tacca pinnatifida}]
