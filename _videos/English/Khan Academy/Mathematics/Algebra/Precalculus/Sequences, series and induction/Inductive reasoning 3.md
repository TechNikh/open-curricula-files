---
version: 1
type: video
provider: YouTube
id: pgfWkaySFGY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Sequences%2C%20series%20and%20induction/Inductive%20reasoning%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7bbae013-ebe1-4c58-bb63-bb4091a48d11
updated: 1486069557
title: Inductive reasoning 3
categories:
    - Sequences, series and induction
thumbnail_urls:
    - https://i3.ytimg.com/vi/pgfWkaySFGY/default.jpg
    - https://i3.ytimg.com/vi/pgfWkaySFGY/1.jpg
    - https://i3.ytimg.com/vi/pgfWkaySFGY/2.jpg
    - https://i3.ytimg.com/vi/pgfWkaySFGY/3.jpg
---
