---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/short-circuit
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484424961
title: short-circuit
categories:
    - Dictionary
---
short circuit
     n : accidental contact between two points in an electric circuit
         that have a potential difference [syn: {short}]
