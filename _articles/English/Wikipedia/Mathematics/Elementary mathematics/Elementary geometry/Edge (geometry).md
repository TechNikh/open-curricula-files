---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edge_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 8d5f7d6e-9207-41cd-bd82-213f1a982a0d
updated: 1484308927
title: Edge (geometry)
tags:
    - Relation to edges in graphs
    - Number of edges in a polyhedron
    - Incidences with other faces
    - Alternative terminology
categories:
    - Elementary geometry
---
In geometry, an edge is a particular type of line segment joining two vertices in a polygon, polyhedron, or higher-dimensional polytope.[1] In a polygon, an edge is a line segment on the boundary,[2] and is often called a side. In a polyhedron or more generally a polytope, an edge is a line segment where two faces meet.[3] A segment joining two vertices while passing through the interior or exterior is not an edge but instead is called a diagonal.
