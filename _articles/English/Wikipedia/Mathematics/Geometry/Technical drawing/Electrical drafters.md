---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electrical_drafters
offline_file: ""
offline_thumbnail: ""
uuid: be19c43d-366a-495e-ac76-1e6df407647e
updated: 1484309324
title: Electrical drafters
categories:
    - Technical drawing
---
An electrical drawing is a technical drawing of a Building services drawing (or building project) that falls within the definition of Electrical. Electrical drawings are used by Electrical engineers and others for a number of purposes: to develop a design idea into a coherent proposal, to communicate ideas and concepts, to convince clients of the merits of a design, to enable a building contractor to construct it, as a record of the completed work, and to make a record of a building that already exists.
