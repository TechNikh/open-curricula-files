---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mycielskian
offline_file: ""
offline_thumbnail: ""
uuid: 3f3a8aa5-7c01-428f-a25a-03e78a1fc41e
updated: 1484309448
title: Mycielskian
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Gr%25C3%25B6tzsch_graph_as_a_Mycielskian.svg.png'
tags:
    - Construction
    - Example
    - Iterated Mycielskians
    - Properties
    - Cones over graphs
categories:
    - Graph operations
---
In the mathematical area of graph theory, the Mycielskian or Mycielski graph of an undirected graph is a larger graph formed from it by a construction of Jan Mycielski (1955). The construction preserves the property of being triangle-free but increases the chromatic number; by applying the construction repeatedly to a triangle-free starting graph, Mycielski showed that there exist triangle-free graphs with arbitrarily large chromatic number.
