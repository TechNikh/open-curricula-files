---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_chaos
offline_file: ""
offline_thumbnail: ""
uuid: e08f43ed-00b1-41a2-9a16-7bcc3da68afd
updated: 1484308709
title: Quantum chaos
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Quantum_Chaos.jpg
tags:
    - History
    - Approaches
    - Quantum mechanics in non-perturbative regimes
    - >
        Correlating statistical descriptions of quantum mechanics
        with classical behavior
    - Semiclassical methods
    - Periodic orbit theory
    - >
        Rough sketch on how to arrive at the Gutzwiller trace
        formula
    - Closed orbit theory
    - One-dimensional systems and potential
    - Recent directions in quantum chaos
    - Berry–Tabor conjecture
    - Notes
categories:
    - Chaos theory
---
Quantum chaos is a branch of physics which studies how chaotic classical dynamical systems can be described in terms of quantum theory. The primary question that quantum chaos seeks to answer is: "What is the relationship between quantum mechanics and classical chaos?" The correspondence principle states that classical mechanics is the classical limit of quantum mechanics. If this is true, then there must be quantum mechanisms underlying classical chaos; although this may not be a fruitful way of examining classical chaos. If quantum mechanics does not demonstrate an exponential sensitivity to initial conditions, how can exponential sensitivity to initial conditions arise in classical ...
