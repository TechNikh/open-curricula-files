---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spirit
offline_file: ""
offline_thumbnail: ""
uuid: 67996d55-d4ca-4299-9ee9-e68d9888cbaf
updated: 1484310226
title: spirit
categories:
    - Dictionary
---
spirit
     n 1: the vital principle or animating force within living things
     2: the general atmosphere of a place or situation and the
        effect that it has on people; "the feel of the city
        excited him"; "a clergyman improved the tone of the
        meeting"; "it had the smell of treason" [syn: {tone}, {feel},
         {feeling}, {flavor}, {flavour}, {look}, {smell}]
     3: a fundamental emotional and activating principle determining
        one's character
     4: any incorporeal supernatural being that can become visible
        (or audible) to human beings [syn: {disembodied spirit}]
     5: the state of a person's emotions (especially with regard to
        pleasure ...
