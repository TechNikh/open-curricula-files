---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pillar
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484366521
title: pillar
categories:
    - Dictionary
---
pillar
     n 1: a fundamental principle or practice; "science eroded the
          pillars of superstition"
     2: anything tall and thin approximating the shape of a column
        or tower; "the test tube held a column of white powder";
        "a tower of dust rose above the horizon"; "a thin pillar
        of smoke betrayed their campsite" [syn: {column}, {tower}]
     3: a prominent supporter; "he is a pillar of the community"
        [syn: {mainstay}]
     4: a vertical structure standing alone and not supporting
        anything (as a monument or a column of air) [syn: {column}]
     5: (architeture) a tall cylindrical vertical upright and used
        to support a structure [syn: ...
