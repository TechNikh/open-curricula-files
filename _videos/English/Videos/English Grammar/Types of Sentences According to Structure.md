---
version: 1
type: video
provider: YouTube
id: LEKkxk0WgVY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bfcc3fe8-5571-48c4-802a-3f768e81d4fb
updated: 1486069632
title: Types of Sentences According to Structure
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/LEKkxk0WgVY/default.jpg
    - https://i3.ytimg.com/vi/LEKkxk0WgVY/1.jpg
    - https://i3.ytimg.com/vi/LEKkxk0WgVY/2.jpg
    - https://i3.ytimg.com/vi/LEKkxk0WgVY/3.jpg
---
