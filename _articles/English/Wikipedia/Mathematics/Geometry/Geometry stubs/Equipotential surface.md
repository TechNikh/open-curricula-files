---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equipotential_surface
offline_file: ""
offline_thumbnail: ""
uuid: 6d3166f7-71ad-4cc0-b931-6ccfeb9a3d15
updated: 1484309360
title: Equipotential surface
categories:
    - Geometry stubs
---
Equipotential surfaces are surfaces of constant scalar potential. They are used to visualize an (n)-dimensional scalar potential function in (n-1) dimensional space. The gradient of the potential, denoting the direction of greatest increase, is perpendicular to the surface.
