---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spherical
offline_file: ""
offline_thumbnail: ""
uuid: ec18c65b-ca5c-4afd-898f-aa27795159a9
updated: 1484308327
title: Sphere
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sphere_wireframe_10deg_6r.svg_0.png
tags:
    - Surface area
    - Enclosed volume
    - Equations in three-dimensional space
    - Terminology
    - Hemisphere
    - Generalizations
    - Dimensionality
    - Metric spaces
    - Topology
    - Spherical geometry
    - Eleven properties of the sphere
    - Cubes in relation to spheres
categories:
    - Uncategorized
---
A sphere (from Greek σφαῖρα — sphaira, "globe, ball"[1]) is a perfectly round geometrical object in three-dimensional space that is the surface of a completely round ball, (viz., analogous to a circular object in two dimensions).[2]
