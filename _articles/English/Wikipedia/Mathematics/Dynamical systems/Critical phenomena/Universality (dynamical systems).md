---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Universality_(dynamical_systems)
offline_file: ""
offline_thumbnail: ""
uuid: fa40f021-5c40-4ba2-a824-c78d4bde8a97
updated: 1484308746
title: Universality (dynamical systems)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_4.png
tags:
    - Universality in statistical mechanics
    - Examples
    - Theoretical overview
    - Applications to other fields
categories:
    - Critical phenomena
---
In statistical mechanics, universality is the observation that there are properties for a large class of systems that are independent of the dynamical details of the system. Systems display universality in a scaling limit, when a large number of interacting parts come together. The modern meaning of the term was introduced by Leo Kadanoff in the 1960s,[citation needed] but a simpler version of the concept was already implicit in the van der Waals equation and in the earlier Landau theory of phase transitions, which did not incorporate scaling correctly.[citation needed]
