---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enforced
offline_file: ""
offline_thumbnail: ""
uuid: 45b3289d-a074-44f1-8242-ca0247743125
updated: 1484310457
title: enforced
categories:
    - Dictionary
---
enforced
     adj : forced or compelled or put in force; "a life of enforced
           inactivity"; "enforced obedience" [syn: {implemented}]
           [ant: {unenforced}]
