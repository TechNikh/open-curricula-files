---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Branko_Gr%C3%BCnbaum'
offline_file: ""
offline_thumbnail: ""
uuid: 2a339089-e3f8-42a1-b809-32895aa05b72
updated: 1484309080
title: Branko Grünbaum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Symmetrical_5-set_Venn_diagram.svg.png
tags:
    - Selected publications
    - Notes
categories:
    - Geometers
---
Branko Grünbaum (Hebrew: ברנקו גרונבאום‎‎; born 2 October 1929) is a Yugoslavian-born mathematician and a professor emeritus at the University of Washington in Seattle. He received his Ph.D. in 1957 from Hebrew University of Jerusalem in Israel.[1] He has authored over 200 papers, mostly in discrete geometry, an area in which he is known for various classification theorems. He has written on the theory of abstract polyhedra.
