---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiscroll_attractor
offline_file: ""
offline_thumbnail: ""
uuid: 693f981b-f2db-4c7a-bbfe-c3490941b1b1
updated: 1484308709
title: Multiscroll attractor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-DoubleScrollAttractor3D.svg.png
tags:
    - Multiscroll attractors
    - Lu Chen attractor
    - Modified Lu Chen attractor
    - Modified Chua chaotic attractor
    - PWL Duffing chaotic attractor
    - Modified Lorenz chaotic system
categories:
    - Chaos theory
---
In the mathematics of dynamical systems, the double-scroll attractor (sometimes known as Chua's attractor) is a strange attractor observed from a physical electronic chaotic circuit (generally, Chua's circuit) with a single nonlinear resistor (see Chua's Diode). The double-scroll system is often described by a system of three nonlinear ordinary differential equations and a 3-segment piecewise-linear equation (see Chua's equations). This makes the system easily simulated numerically and easily manifested physically due to Chua's circuits' simple design.
