---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/watch
offline_file: ""
offline_thumbnail: ""
uuid: c76deae7-31ad-4c64-b973-c5ce32a057fb
updated: 1484310341
title: watch
categories:
    - Dictionary
---
watch
     n 1: a small portable timepiece [syn: {ticker}]
     2: a period of time (4 or 2 hours) during which some of a
        ship's crew are on duty
     3: a purposeful surveillance to guard or observe [syn: {vigil}]
     4: the period during which someone (especially a guard) is on
        duty
     5: a person employed to watch for something to happen [syn: {lookout},
         {lookout man}, {sentinel}, {sentry}, {spotter}, {scout},
        {picket}]
     6: a devotional watch (especially on the eve of a religious
        festival) [syn: {vigil}]
     v 1: look attentively; "watch a basketball game"
     2: follow with the eyes or the mind; "Keep an eye on the baby,
        ...
