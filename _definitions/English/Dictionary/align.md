---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/align
offline_file: ""
offline_thumbnail: ""
uuid: 02d5d97b-35d6-48d2-b9e5-9013f4207cf4
updated: 1484310531
title: align
categories:
    - Dictionary
---
align
     v 1: place in a line or arrange so as to be parallel or straight;
          "align the car with the curb"; "align the sheets of
          paper on the table" [syn: {aline}, {line up}, {adjust}]
          [ant: {skew}]
     2: align with; be or come into adjustment
     3: align oneself with a group or a way of thinking [syn: {array}]
     4: bring (components or parts) into proper or desirable
        coordination correlation; "align the wheels of my car";
        "ordinate similar parts" [syn: {ordinate}, {coordinate}]
