---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Rabinovich%E2%80%93Fabrikant_equations'
offline_file: ""
offline_thumbnail: ""
uuid: f27a25cf-c968-4815-8de5-857814271177
updated: 1484308728
title: Rabinovich–Fabrikant equations
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-RabFab800.png
tags:
    - System description
    - Equilibrium points
    - γ = 0.87, α = 1.1
    - γ = 0.1
categories:
    - Chaotic maps
---
The Rabinovich–Fabrikant equations are a set of three coupled ordinary differential equations exhibiting chaotic behavior for certain values of the parameters. They are named after Mikhail Rabinovich and Anatoly Fabrikant, who described them in 1979.
