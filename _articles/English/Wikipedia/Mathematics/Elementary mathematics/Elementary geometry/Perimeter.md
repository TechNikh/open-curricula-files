---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perimeter
offline_file: ""
offline_thumbnail: ""
uuid: e7b138da-dcb2-4ca6-8a27-b68ffed83f7c
updated: 1484308938
title: Perimeter
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Perimiters.svg.png
tags:
    - Formulas
    - Polygons
    - Circumference of a circle
    - Perception of perimeter
    - Isoperimetry
categories:
    - Elementary geometry
---
A perimeter is a path that surrounds a two-dimensional shape. The word comes from the Greek peri (around) and meter (measure). The term may be used either for the path or its length&endash;it can be thought of as the length of the outline of a shape. The perimeter of a circle or ellipse is called its circumference.
