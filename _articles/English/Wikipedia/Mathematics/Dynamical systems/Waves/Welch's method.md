---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Welch%27s_method'
offline_file: ""
offline_thumbnail: ""
uuid: bae9fe31-0c34-495d-9cca-cf6e36b3dea9
updated: 1484308900
title: "Welch's method"
tags:
    - Definition and procedure
    - Related approaches
categories:
    - Waves
---
In physics, engineering, and applied mathematics, Welch's method, named after P.D. Welch, is used for estimating the power of a signal at different frequencies: that is, it is an approach to spectral density estimation. The method is based on the concept of using periodogram spectrum estimates, which are the result of converting a signal from the time domain to the frequency domain. Welch's method is an improvement on the standard periodogram spectrum estimating method and on Bartlett's method, in that it reduces noise in the estimated power spectra in exchange for reducing the frequency resolution. Due to the noise caused by imperfect and finite data, the noise reduction from Welch's ...
