---
version: 1
type: video
provider: YouTube
id: DvlpAsDwXPY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0cc6478e-6589-400b-af8d-ee8978d1a0bd
updated: 1486069658
title: Basics of Electric Charge
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/DvlpAsDwXPY/default.jpg
    - https://i3.ytimg.com/vi/DvlpAsDwXPY/1.jpg
    - https://i3.ytimg.com/vi/DvlpAsDwXPY/2.jpg
    - https://i3.ytimg.com/vi/DvlpAsDwXPY/3.jpg
---
