---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transformation_(function)
offline_file: ""
offline_thumbnail: ""
uuid: f720dc22-1d2c-4dc4-a6a6-1b34fe52e984
updated: 1484309336
title: Transformation (function)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/330px-A_code_snippet_for_a_rhombic_repetitive_pattern.svg.png
tags:
    - Translation
    - Reflection
    - Glide reflection
    - Rotation
    - Scaling
    - Shear
    - More generally
    - Partial transformations
    - Algebraic structures
    - Combinatorics
categories:
    - Transformation (function)
---
In mathematics, particularly in semigroup theory, a transformation is a function f that maps a set X to itself, i.e. f : X → X.[1][2][3] In other areas of mathematics, a transformation may simply be any function, regardless of domain and codomain.[4] This wider sense shall not be considered in this article; refer instead to the article on function for that sense.
