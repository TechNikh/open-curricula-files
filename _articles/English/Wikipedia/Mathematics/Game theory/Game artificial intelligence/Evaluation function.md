---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Evaluation_function
offline_file: ""
offline_thumbnail: ""
uuid: ec87294a-0308-4a0a-ae7f-45f234ff481e
updated: 1484309001
title: Evaluation function
tags:
    - In chess
    - In Go
categories:
    - Game artificial intelligence
---
An evaluation function, also known as a heuristic evaluation function or static evaluation function, is a function used by game-playing programs to estimate the value or goodness of a position in the minimax and related algorithms. The evaluation function is typically designed to prioritize speed over accuracy; the function looks only at the current position and does not explore possible moves (therefore static).
