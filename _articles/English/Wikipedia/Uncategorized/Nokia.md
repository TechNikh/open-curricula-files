---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nokia
offline_file: ""
offline_thumbnail: ""
uuid: b67d6cdd-8d7b-4de3-94ed-0395146b514e
updated: 1484308460
title: Nokia
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Nokia_Toilet_paper.JPG
tags:
    - History
    - 1865–1967
    - 1967–1990
    - 1990–2014
    - 2014–present
    - Operations
    - Nokia Networks
    - Nokia Technologies
    - Alcatel-Lucent
    - Nokia Bell Labs
categories:
    - Uncategorized
---
Nokia Corporation[4] (Finnish: Nokia Oyj, Finnish pronunciation: [ˈnokiɑ], UK /ˈnɒkiə/, US /ˈnoʊkiə/), stylised as NOKIA, is a Finnish multinational communications and information technology company, founded in 1865. Nokia is headquartered in Espoo, Uusimaa, in the greater Helsinki metropolitan area.[1] In 2014, Nokia employed 61,656 people across 120 countries, did business in more than 150 countries and reported annual revenues of around €12.73 billion.[2] Nokia is a public limited company listed on the Helsinki Stock Exchange and New York Stock Exchange.[5] It is the world's 274th-largest company measured by 2013 revenues according to the Fortune Global 500 and is a component ...
