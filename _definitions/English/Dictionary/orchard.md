---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/orchard
offline_file: ""
offline_thumbnail: ""
uuid: 2f98e838-8054-4795-8bf7-544a1a34922c
updated: 1484310559
title: orchard
categories:
    - Dictionary
---
orchard
     n : garden consisting of a small cultivated wood without
         undergrowth [syn: {grove}, {woodlet}, {plantation}]
