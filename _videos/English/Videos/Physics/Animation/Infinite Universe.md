---
version: 1
type: video
provider: YouTube
id: IFdfrtzo4SY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Infinite%20Universe.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4f007cff-1ce6-4c83-bdbb-e84f016f1b2f
updated: 1486069656
title: Infinite Universe?
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/IFdfrtzo4SY/default.jpg
    - https://i3.ytimg.com/vi/IFdfrtzo4SY/1.jpg
    - https://i3.ytimg.com/vi/IFdfrtzo4SY/2.jpg
    - https://i3.ytimg.com/vi/IFdfrtzo4SY/3.jpg
---
