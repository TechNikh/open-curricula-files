---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strontium
offline_file: ""
offline_thumbnail: ""
uuid: 5c7f162e-f563-4cfe-a232-18738eb3f3d7
updated: 1484310391
title: strontium
categories:
    - Dictionary
---
strontium
     n : a soft silver-white or yellowish metallic element of the
         alkali metal group; turns yellow in air; occurs in
         celestite and strontianite [syn: {Sr}, {atomic number 38}]
