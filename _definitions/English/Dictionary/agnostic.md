---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agnostic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484522521
title: agnostic
categories:
    - Dictionary
---
agnostic
     adj : uncertain of all claims to knowledge [syn: {agnostical}]
           [ant: {gnostic}]
     n : a person who doubts truth of religion [syn: {doubter}]
