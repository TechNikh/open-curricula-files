---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/garbage
offline_file: ""
offline_thumbnail: ""
uuid: 3a5bafc5-9dcc-415a-bef3-800c62be7efa
updated: 1484310473
title: garbage
categories:
    - Dictionary
---
garbage
     n 1: food that is discarded (as from a kitchen) [syn: {refuse}, {food
          waste}, {scraps}]
     2: a worthless message [syn: {drivel}]
     3: a receptacle where garbage is discarded; "she tossed the
        moldy bread into the garbage"
