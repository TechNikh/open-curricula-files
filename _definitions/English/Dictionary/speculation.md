---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/speculation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484473141
title: speculation
categories:
    - Dictionary
---
speculation
     n 1: a message expressing an opinion based on incomplete evidence
          [syn: {guess}, {conjecture}, {supposition}, {surmise}, {surmisal},
           {hypothesis}]
     2: a hypothesis that has been formed by speculating or
        conjecturing (usually with little hard evidence);
        "speculations about the outcome of the election"; "he
        dismissed it as mere conjecture" [syn: {conjecture}]
     3: an investment that is very risky but could yield great
        profits; "he knew the stock was a speculation when he
        bought it" [syn: {venture}]
     4: continuous and profound contemplation or musing on a subject
        or series of subjects of a deep or ...
