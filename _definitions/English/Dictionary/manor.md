---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/manor
offline_file: ""
offline_thumbnail: ""
uuid: b9b7c388-21d0-4eec-9d6d-f86d17af63bf
updated: 1484310559
title: manor
categories:
    - Dictionary
---
manor
     n 1: the mansion of the lord of the manor [syn: {manor house}]
     2: the landed estate of a lord (including the house on it)
