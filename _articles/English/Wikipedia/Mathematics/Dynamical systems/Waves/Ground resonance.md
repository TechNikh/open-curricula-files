---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ground_resonance
offline_file: ""
offline_thumbnail: ""
uuid: e05e5bcf-d8d1-4ed3-8ea3-464df3a261ba
updated: 1484308889
title: Ground resonance
tags:
    - Causes and consequences
    - mitigation
categories:
    - Waves
---
Ground resonance is an imbalance in the rotation of a helicopter rotor when the blades become bunched up on one side of their rotational plane and cause an oscillation in phase with the frequency of the rocking of the helicopter on its landing gear. The effect is similar to the behavior of a washing machine when the clothes are concentrated in one place during the spin cycle. It occurs when the landing gear is prevented from freely moving about on the horizontal plane, typically when the aircraft is on the ground.
