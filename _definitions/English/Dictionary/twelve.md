---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/twelve
offline_file: ""
offline_thumbnail: ""
uuid: 70fbd5ff-8248-4703-845c-c2914a941e48
updated: 1484310293
title: twelve
categories:
    - Dictionary
---
twelve
     adj : denoting a quantity consisting of 12 items or units [syn: {12},
            {xii}, {dozen}]
     n : the cardinal number that is the sum of eleven and one [syn:
         {12}, {XII}, {dozen}]
