---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consumption
offline_file: ""
offline_thumbnail: ""
uuid: 055f7338-540b-47c6-8874-0242cc78e53a
updated: 1484310254
title: consumption
categories:
    - Dictionary
---
consumption
     n 1: the process of taking food into the body through the mouth
          (as by eating) [syn: {ingestion}, {intake}, {uptake}]
     2: involving the lungs with progressive wasting of the body
        [syn: {pulmonary tuberculosis}, {phthisis}, {wasting
        disease}, {white plague}]
     3: (economics) the utilization of economic goods to satisfy
        needs or in manufacturing; "the consumption of energy has
        increased steadily" [syn: {economic consumption}, {usance},
         {use}, {use of goods and services}]
     4: the act of consuming something [syn: {using up}, {expenditure}]
