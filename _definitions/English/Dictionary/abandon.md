---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abandon
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484572801
title: abandon
categories:
    - Dictionary
---
abandon
     n 1: the trait of lacking restraint or control; freedom from
          inhibition or worry; "she danced with abandon" [syn: {wantonness},
           {unconstraint}]
     2: a feeling of extreme emotional intensity; "the wildness of
        his anger" [syn: {wildness}]
     v 1: forsake, leave behind; "We abandoned the old car in the
          empty parking lot"
     2: stop maintaining or insisting on; of ideas, claims, etc.;
        "He abandoned the thought of asking for her hand in
        marriage"; "Both sides have to give up some calims in
        these negociations" [syn: {give up}]
     3: give up with the intent of never claiming again; "Abandon
        your life to ...
