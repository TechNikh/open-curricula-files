---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clockwise
offline_file: ""
offline_thumbnail: ""
uuid: 11dc64a4-8e31-42ab-812b-b75786ed90b4
updated: 1484310365
title: clockwise
categories:
    - Dictionary
---
clockwise
     adj : in the same direction as the rotating hands of a clock [ant:
            {counterclockwise}]
     adv : in the direction that the hands of a clock move; "please
           move clockwise in a circle" [ant: {counterclockwise}]
