---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conway_puzzle
offline_file: ""
offline_thumbnail: ""
uuid: b4997073-a3f3-4aaf-aadf-6936fa30ae34
updated: 1484308611
title: Conway puzzle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Conway_puzzle_bricks.png
tags:
    - Solution
categories:
    - Discrete geometry
---
Conway's puzzle, or Blocks-in-a-Box, is a packing problem using rectangular blocks, named after its inventor, mathematician John Conway. It calls for packing thirteen 1 × 2 × 4 blocks, one 2 × 2 × 2 block, one 1 × 2 × 2 block, and three 1 × 1 × 3 blocks into a 5 × 5 × 5 box.[1]
