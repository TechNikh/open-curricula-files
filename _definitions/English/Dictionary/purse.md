---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/purse
offline_file: ""
offline_thumbnail: ""
uuid: 843881e1-edb0-444e-8782-aebd8dccdf64
updated: 1484310591
title: purse
categories:
    - Dictionary
---
purse
     n 1: a bag used for carrying money and small personal items or
          accessories (especially by women); "she reached into her
          bag and found a comb" [syn: {bag}, {handbag}, {pocketbook}]
     2: a sum of money spoken of as the contents of a money purse;
        "he made the contribution out of his own purse"; "he and
        his wife shared a common purse"
     3: a small bag for carrying money
     4: a sum of money offered as a prize; "the purse barely covered
        the winner's expenses"
     v 1: contract one's lips into a rounded shape
     2: gather or contract into wrinkles or folds; pucker; "purse
        ones's lips" [syn: {wrinkle}]
