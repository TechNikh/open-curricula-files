---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stock
offline_file: ""
offline_thumbnail: ""
uuid: e68356bc-4eb2-43c0-9267-8076a8985703
updated: 1484310461
title: stock
categories:
    - Dictionary
---
stock
     adj 1: repeated too often; overfamiliar through overuse; "bromidic
            sermons"; "his remarks were trite and commonplace";
            "hackneyed phrases"; "a stock answer"; "repeating
            threadbare jokes"; "parroting some timeworn axiom";
            "the trite metaphor `hard as nails'" [syn: {banal}, {commonplace},
             {hackneyed}, {old-hat}, {shopworn}, {stock(a)}, {threadbare},
             {timeworn}, {tired}, {trite}, {well-worn}]
     2: routine; "a stock answer"
     3: regularly and widely used or sold; "a standard size"; "a
        stock item" [syn: {standard}]
     n 1: the capital raised by a corporation through the issue of
          shares ...
