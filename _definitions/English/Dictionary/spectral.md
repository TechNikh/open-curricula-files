---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spectral
offline_file: ""
offline_thumbnail: ""
uuid: 47136da6-89c1-49fe-af8e-87a3bd66cfaf
updated: 1484310395
title: spectral
categories:
    - Dictionary
---
spectral
     adj 1: of or relating to a spectrum; "spectral colors"; "spectral
            analysis"
     2: like or being a phantom; "a ghostly face at the window"; "a
        phantasmal presence in the room"; "spectral emanations";
        "spiritual tappings at a seance" [syn: {apparitional}, {ghostlike},
         {ghostly}, {phantasmal}, {spiritual}]
