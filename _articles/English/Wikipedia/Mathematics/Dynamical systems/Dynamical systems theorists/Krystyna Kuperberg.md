---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Krystyna_Kuperberg
offline_file: ""
offline_thumbnail: ""
uuid: fbea72c3-0efb-423e-809c-6ace04219547
updated: 1484308690
title: Krystyna Kuperberg
categories:
    - Dynamical systems theorists
---
Krystyna M. Kuperberg (born Krystyna M. Trybulec; 17 July 1944) is a Polish-American mathematician who currently works as a professor of mathematics at Auburn University and is the former Alumni Professor of Mathematics there.[1][2][3]
