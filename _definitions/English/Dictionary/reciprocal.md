---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reciprocal
offline_file: ""
offline_thumbnail: ""
uuid: 8160bee3-4f72-4783-ad86-2b72756fe6f2
updated: 1484310198
title: reciprocal
categories:
    - Dictionary
---
reciprocal
     adj 1: concerning each of two or more persons or things; especially
            given or done in return; "reciprocal aid"; "reciprocal
            trade"; "mutual respect"; "reciprocal privileges at
            other clubs" [syn: {mutual}] [ant: {nonreciprocal}]
     2: of or relating to or suggestive of complementation;
        "interchangeable electric outlets" [syn: {complementary},
        {interchangeable}]
     3: of or relating to the multiplicative inverse of a quantity
        or function; "the reciprocal ratio of a:b is b:a"
     n 1: something (a term or expression or concept) that has a
          reciprocal relation to something else; "risk is the
          ...
