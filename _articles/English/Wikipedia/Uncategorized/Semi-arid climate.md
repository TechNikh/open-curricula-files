---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Semi-arid
offline_file: ""
offline_thumbnail: ""
uuid: 0dcb604d-a180-4480-952e-301651460c65
updated: 1484308333
title: Semi-arid climate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Koppen_World_Map_BSh_BSk.png
tags:
    - Defining attributes of semi-arid climates
    - Hot semi-arid climates
    - Cold semi-arid climates
    - Regions of varying classification
    - Charts of selected cities
categories:
    - Uncategorized
---
A semi-arid climate or steppe climate is the climate of a region that receives precipitation below potential evapotranspiration, but not extremely. There are different kinds of semi-arid climates, depending on such variables as temperature, and they give rise to different classes of ecology.
