---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convincing
offline_file: ""
offline_thumbnail: ""
uuid: cd477e34-ddef-4d60-989b-a93caa03b902
updated: 1484310429
title: convincing
categories:
    - Dictionary
---
convincing
     adj 1: causing one to believe the truth of something; "a convincing
            story"; "a convincing manner" [ant: {unconvincing}]
     2: capable of convincing or persuading; "a convincing argument"
