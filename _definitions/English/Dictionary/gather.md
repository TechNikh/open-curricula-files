---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gather
offline_file: ""
offline_thumbnail: ""
uuid: 8c096aa6-ee0a-40e9-9794-a8c3e521de77
updated: 1484310152
title: gather
categories:
    - Dictionary
---
gather
     n 1: sewing consisting of small folds or puckers made by pulling
          tight a thread in a line of stitching [syn: {gathering}]
     2: the act of gathering something [syn: {gathering}]
     v 1: assemble or get together; "gather some stones"; "pull your
          thoughts together" [syn: {garner}, {collect}, {pull
          together}] [ant: {spread}]
     2: collect in one place; "We assembled in the church basement";
        "Let's gather in the dining room" [syn: {meet}, {assemble},
         {forgather}, {foregather}]
     3: collect or gather; "Journals are accumulating in my office";
        "The work keeps piling up" [syn: {accumulate}, {cumulate},
         ...
