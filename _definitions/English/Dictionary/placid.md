---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/placid
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484416201
title: placid
categories:
    - Dictionary
---
placid
     adj 1: free from disturbance; "a ribbon of sand between the angry
            sea and the placid bay"; "the quiet waters of a
            lagoon"; "a lake of tranquil blue water reflecting a
            tranquil blue sky"; "a smooth channel crossing";
            "scarcely a ripple on the still water"; "unruffled
            water" [syn: {quiet}, {still}, {tranquil}, {unruffled}]
     2: not easily irritated; "an equable temper"; "not everyone
        shared his placid temperament"; "remained placid despite
        the repeated delays" [syn: {equable}, {even-tempered}, {good-tempered}]
     3: without untoward incident or disruption; "a placid
        existence"; "quiet times" ...
