---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Moishezon_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 0897c59c-1093-47c5-a744-1d12251d814d
updated: 1484309162
title: Moishezon manifold
categories:
    - Analytic geometry
---
In mathematics, a Moishezon manifold M is a compact complex manifold such that the field of meromorphic functions on each component M has transcendence degree equal the complex dimension of the component:
