---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bush
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484615401
title: bush
categories:
    - Dictionary
---
bush
     adj : not of the highest quality or sophistication [syn: {bush-league}]
     n 1: a low woody perennial plant usually having several major
          branches [syn: {shrub}]
     2: a large wilderness area
     3: dense vegetation consisting of stunted trees or bushes [syn:
         {scrub}, {chaparral}]
     4: 43rd President of the United States; son of George Herbert
        Walker Bush (born in 1946) [syn: {George Bush}, {George W.
        Bush}, {George Walker Bush}, {President Bush}, {President
        George W. Bush}, {Dubyuh}, {Dubya}]
     5: United States electrical engineer who designed an early
        analogue computer and who led the scientific program of
        the ...
