---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Neil_Robertson_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: b47c64a4-afaa-4b7b-b9fd-5e03d5260aac
updated: 1484309475
title: Neil Robertson (mathematician)
tags:
    - Biography
    - Research
    - Awards and honors
categories:
    - Graph theorists
---
George Neil Robertson (born November 30, 1938) is a mathematician working mainly in topological graph theory, currently a distinguished professor[1] emeritus[2] at the Ohio State University. He earned his B.Sc. from Brandon College in 1959, and his Ph.D. in 1969 at the University of Waterloo under his doctoral advisor William Tutte.[3][4]
