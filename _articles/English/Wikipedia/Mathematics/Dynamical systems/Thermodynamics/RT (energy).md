---
version: 1
type: article
id: https://en.wikipedia.org/wiki/RT_(energy)
offline_file: ""
offline_thumbnail: ""
uuid: 8fb9b6fc-edfb-473b-935a-e15fdf956c1a
updated: 1484308859
title: RT (energy)
categories:
    - Thermodynamics
---
RT is the product of the molar gas constant, R, and the temperature, T. This product is used in physics as a scaling factor for energy values in macroscopic scale (sometimes it is used as a pseudo-unit of energy), as many processes and phenomena depend not on the energy alone, but on the ratio of energy and RT, i.e. E/RT. The SI units for RT are joules per mole (J/mol).
