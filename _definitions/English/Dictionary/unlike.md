---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unlike
offline_file: ""
offline_thumbnail: ""
uuid: 2d330e49-5b8e-4a77-9b83-35696742631e
updated: 1484310371
title: unlike
categories:
    - Dictionary
---
unlike
     adj 1: not like; marked by dissimilarity; "for twins they are very
            unlike"; "people are profoundly different" [syn: {dissimilar},
             {different}] [ant: {like}]
     2: not equal in amount; "they distributed unlike (or unequal)
        sums to the various charities" [syn: {unequal}] [ant: {like}]
