---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Simulation_preorder
offline_file: ""
offline_thumbnail: ""
uuid: 999a802a-4e37-4601-93f6-484c6cbc9bfd
updated: 1484308683
title: Simulation preorder
tags:
    - Formal definition
    - Similarity of separate transition systems
categories:
    - Theoretical computer science
---
In theoretical computer science a simulation preorder is a relation between state transition systems associating systems which behave in the same way in the sense that one system simulates the other.
