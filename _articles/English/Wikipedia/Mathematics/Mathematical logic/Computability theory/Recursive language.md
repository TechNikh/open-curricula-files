---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Recursive_language
offline_file: ""
offline_thumbnail: ""
uuid: bc17b411-c5fd-4c26-99c2-1b4cdb6f817a
updated: 1484309490
title: Recursive language
tags:
    - Definitions
    - Examples
    - Closure properties
categories:
    - Computability theory
---
In mathematics, logic and computer science, a formal language (a set of finite sequences of symbols taken from a fixed alphabet) is called recursive if it is a recursive subset of the set of all possible finite sequences over the alphabet of the language. Equivalently, a formal language is recursive if there exists a total Turing machine (a Turing machine that halts for every given input) that, when given a finite sequence of symbols as input, accepts it if belongs to the language and rejects it otherwise. Recursive languages are also called decidable.
