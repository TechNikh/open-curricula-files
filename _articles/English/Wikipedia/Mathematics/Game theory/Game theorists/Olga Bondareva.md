---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Olga_Bondareva
offline_file: ""
offline_thumbnail: ""
uuid: 73756621-0a5c-4b2f-bcbf-8cdab893a252
updated: 1484308520
title: Olga Bondareva
tags:
    - Biography
    - Scientific work
    - Bibliography
    - Sources
categories:
    - Game theorists
---
Olga Nikolaevna Bondareva (April 27, 1937 – December 9, 1991) was a distinguished Soviet mathematician and economist. She contributed to the fields of mathematical economics and especially game theory, and is best known as one of the two independent discoverers of the Bondareva–Shapley theorem Bondareva, Olga N. (1963). "Some applications of linear programming methods to the theory of cooperative games (In Russian)" (PDF). Problemy Kybernetiki. 10: 119–139. 
