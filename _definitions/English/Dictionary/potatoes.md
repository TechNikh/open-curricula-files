---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/potatoes
offline_file: ""
offline_thumbnail: ""
uuid: ab468623-446f-4f27-a892-637287cfd3b3
updated: 1484310333
title: potatoes
categories:
    - Dictionary
---
potato
     n 1: an edible tuber native to South America; a staple food of
          Ireland [syn: {white potato}, {Irish potato}, {murphy},
          {spud}, {tater}]
     2: annual native to South America having underground stolons
        bearing edible starchy tubers; widely cultivated as a
        garden vegetable; vines are poisonous [syn: {white potato},
         {white potato vine}, {Solanum tuberosum}]
     [also: {potatoes} (pl)]
