---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Golomb_ruler
offline_file: ""
offline_thumbnail: ""
uuid: f4b7a46c-b56d-4d87-8907-e9c026d2a675
updated: 1484308641
title: Golomb ruler
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Golomb_Ruler-4.svg.png
tags:
    - Definitions
    - Golomb rulers as sets
    - Golomb rulers as functions
    - Optimality
    - Practical applications
    - Information theory and error correction
    - Radio frequency selection
    - Radio antenna placement
    - Current Transformers
    - Methods of construction
    - Erdős–Turan construction
    - Known optimal Golomb rulers
categories:
    - Number theory
---
In mathematics, a Golomb ruler is a set of marks at integer positions along an imaginary ruler such that no two pairs of marks are the same distance apart. The number of marks on the ruler is its order, and the largest distance between two of its marks is its length. Translation and reflection of a Golomb ruler are considered trivial, so the smallest mark is customarily put at 0 and the next mark at the smaller of its two possible values.
