---
version: 1
type: video
provider: YouTube
id: qV4lR9EWGlY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8d158981-b81b-4549-998a-212297430cc5
updated: 1486069682
title: 'Sound- Crash Course Physics #18'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/qV4lR9EWGlY/default.jpg
    - https://i3.ytimg.com/vi/qV4lR9EWGlY/1.jpg
    - https://i3.ytimg.com/vi/qV4lR9EWGlY/2.jpg
    - https://i3.ytimg.com/vi/qV4lR9EWGlY/3.jpg
---
