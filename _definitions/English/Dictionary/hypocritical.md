---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hypocritical
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484461561
title: hypocritical
categories:
    - Dictionary
---
hypocritical
     adj : professing feelings or virtues one does not have;
           "hypocritical praise"
