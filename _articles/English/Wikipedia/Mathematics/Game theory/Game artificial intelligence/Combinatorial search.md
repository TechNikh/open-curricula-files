---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorial_search
offline_file: ""
offline_thumbnail: ""
uuid: 012bd08c-2b0f-46c9-b233-f036c9115baf
updated: 1484309002
title: Combinatorial search
tags:
    - Examples
    - Lookahead
categories:
    - Game artificial intelligence
---
In computer science and artificial intelligence, combinatorial search studies search algorithms for solving instances of problems that are believed to be hard in general, by efficiently exploring the usually large solution space of these instances. Combinatorial search algorithms achieve this efficiency by reducing the effective size of the search space or employing heuristics. Some algorithms are guaranteed to find the optimal solution, while others may only return the best solution found in the part of the state space that was explored.
