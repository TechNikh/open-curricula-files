---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Olfactory_receptors
offline_file: ""
offline_thumbnail: ""
uuid: 0ed61c92-d5d8-435a-8b63-23604054fa4b
updated: 1484308363
title: Olfactory receptor
tags:
    - Expression
    - Mechanism
    - Diversity
    - Families
    - Evolution
    - discovery
categories:
    - Uncategorized
---
Olfactory receptors (ORs), also known as odorant receptors, are expressed in the cell membranes of olfactory receptor neurons and are responsible for the detection of odorants (i.e., compounds that have an odor) which give rise to the sense of smell. Activated olfactory receptors trigger nerve impulses which transmit information about odor to the brain. These receptors are members of the class A rhodopsin-like family of G protein-coupled receptors (GPCRs).[1][2] The olfactory receptors form a multigene family consisting of around 800 genes in humans and 1400 genes in mice.[3]
