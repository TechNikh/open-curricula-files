---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impacted
offline_file: ""
offline_thumbnail: ""
uuid: 6599d51e-1546-4bc4-9ed4-a7a11b60016f
updated: 1484310170
title: impacted
categories:
    - Dictionary
---
impacted
     adj : wedged or packed in together; "an impacted tooth" [syn: {wedged}]
