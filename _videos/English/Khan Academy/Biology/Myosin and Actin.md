---
version: 1
type: video
provider: YouTube
id: zopoN2i7ALQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Myosin%20and%20Actin.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 60f4f879-3d36-43bb-baa1-f9533d6b4dd3
updated: 1486069583
title: Myosin and Actin
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/zopoN2i7ALQ/default.jpg
    - https://i3.ytimg.com/vi/zopoN2i7ALQ/1.jpg
    - https://i3.ytimg.com/vi/zopoN2i7ALQ/2.jpg
    - https://i3.ytimg.com/vi/zopoN2i7ALQ/3.jpg
---
How myosin and actin interact to produce mechanical force.
More free lessons at: http://www.khanacademy.org/video?v=zopoN2i7ALQ
