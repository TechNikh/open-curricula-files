---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/island
offline_file: ""
offline_thumbnail: ""
uuid: 3ad03b7a-c114-43f1-9f57-859b0369af7e
updated: 1484310286
title: island
categories:
    - Dictionary
---
island
     n 1: a land mass (smaller than a continent) that is surrounded by
          water
     2: a zone or area resembling an island
