---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Grigory_Margulis
offline_file: ""
offline_thumbnail: ""
uuid: 71dce92b-ae9c-4707-bff7-8ecb18abf049
updated: 1484308684
title: Grigory Margulis
tags:
    - Short biography
    - Mathematical contributions
    - Selected publications
    - Books
    - Lectures
    - Papers
categories:
    - Dynamical systems theorists
---
Gregori Aleksandrovich Margulis (Russian: Григо́рий Алекса́ндрович Маргу́лис, first name often given as Gregory, Grigori or Grigory; born February 24, 1946) is a Russian-American[2] mathematician known for his work on lattices in Lie groups, and the introduction of methods from ergodic theory into diophantine approximation. He was awarded a Fields Medal in 1978 and a Wolf Prize in Mathematics in 2005, becoming the seventh mathematician to receive both prizes. In 1991, he joined the faculty of Yale University, where he is currently the Erastus L. DeForest Professor of Mathematics.[3]
