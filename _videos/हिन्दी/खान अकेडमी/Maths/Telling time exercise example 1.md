---
version: 1
type: video
provider: YouTube
id: RUL5PdeJeDU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 492d5c2d-3f2b-4948-af33-7cce82e2a697
updated: 1486069618
title: Telling time exercise example 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/RUL5PdeJeDU/default.jpg
    - https://i3.ytimg.com/vi/RUL5PdeJeDU/1.jpg
    - https://i3.ytimg.com/vi/RUL5PdeJeDU/2.jpg
    - https://i3.ytimg.com/vi/RUL5PdeJeDU/3.jpg
---
