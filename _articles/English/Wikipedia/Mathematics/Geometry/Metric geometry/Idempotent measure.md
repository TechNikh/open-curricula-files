---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Idempotent_measure
offline_file: ""
offline_thumbnail: ""
uuid: 598dc3fa-d1c1-47e3-bcd0-298325e437a2
updated: 1484309266
title: Idempotent measure
categories:
    - Metric geometry
---
In mathematics, an idempotent measure on a metric group is a probability measure that equals its convolution with itself; in other words, an idempotent measure is an idempotent element in the topological semigroup of probability measures on the given metric group.
