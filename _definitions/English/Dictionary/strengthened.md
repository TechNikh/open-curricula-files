---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/strengthened
offline_file: ""
offline_thumbnail: ""
uuid: 96fb6c6e-2e8e-4d41-ba52-30b12a84945c
updated: 1484310557
title: strengthened
categories:
    - Dictionary
---
strengthened
     adj : given added strength or support; "reinforced concrete
           contains steel bars or metal netting" [syn: {reinforced}]
