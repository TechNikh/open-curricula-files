---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dispersed
offline_file: ""
offline_thumbnail: ""
uuid: e2e6975a-89bd-4f2e-8b84-f32a1aa9eacd
updated: 1484310426
title: dispersed
categories:
    - Dictionary
---
dispersed
     adj : distributed or spread over a considerable extent; "has ties
           with many widely dispersed friends"; "eleven million
           Jews are spread throughout Europe" [syn: {spread}]
