---
version: 1
type: video
provider: YouTube
id: 8m6LvVQ-DQ4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/English%20Grammar-%20Stative%20Verbs.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1dc08ed6-b065-42ab-853c-cf36782af386
updated: 1486069632
title: 'English Grammar- Stative Verbs'
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/8m6LvVQ-DQ4/default.jpg
    - https://i3.ytimg.com/vi/8m6LvVQ-DQ4/1.jpg
    - https://i3.ytimg.com/vi/8m6LvVQ-DQ4/2.jpg
    - https://i3.ytimg.com/vi/8m6LvVQ-DQ4/3.jpg
---
