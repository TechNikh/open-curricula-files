---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/warm
offline_file: ""
offline_thumbnail: ""
uuid: a4ceb8b5-618b-4fec-8ea6-fec1c8fa248e
updated: 1484310226
title: warm
categories:
    - Dictionary
---
warm
     adj 1: having or producing a comfortable and agreeable degree of
            heat or imparting or maintaining heat; "a warm body";
            "a warm room"; "a warm climate"; "a warm coat" [ant: {cool}]
     2: psychologically warm; friendly and responsive; "a warm
        greeting"; "a warm personality"; "warm support" [ant: {cool}]
     3: (color) inducing the impression of warmth; used especially
        of reds and oranges and yellows; "warm reds and yellows
        and orange" [ant: {cool}]
     4: having or displaying warmth or affection; "affectionate
        children"; "caring parents"; "a fond embrace"; "fond of
        his nephew"; "a tender glance"; "a warm embrace" ...
