---
version: 1
type: video
provider: YouTube
id: MjJwA1Qm6nE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 783cc8bc-11d6-4708-b414-e5d01106db8f
updated: 1486069644
title: 'Dubstep - Impulslogic - Dark Areas & HD fractal animation'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/MjJwA1Qm6nE/default.jpg
    - https://i3.ytimg.com/vi/MjJwA1Qm6nE/1.jpg
    - https://i3.ytimg.com/vi/MjJwA1Qm6nE/2.jpg
    - https://i3.ytimg.com/vi/MjJwA1Qm6nE/3.jpg
---
