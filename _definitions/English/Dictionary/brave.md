---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brave
offline_file: ""
offline_thumbnail: ""
uuid: c270927d-e712-4be2-8bb2-5ac0a0cc78ab
updated: 1484310246
title: brave
categories:
    - Dictionary
---
brave
     adj 1: possessing or displaying courage; able to face and deal with
            danger or fear without flinching; "Familiarity with
            danger makes a brave man braver but less daring"-
            Herman Melville; "a frank courageous heart...triumphed
            over pain"- William Wordsworth; "set a courageous
            example by leading them safely into and out of
            enemy-held territory" [syn: {courageous}, {fearless}]
            [ant: {cowardly}]
     2: invulnerable to fear or intimidation; "audacious explorers";
        "fearless reporters and photographers"; "intrepid
        pioneers" [syn: {audacious}, {dauntless}, {fearless}, {intrepid},
         ...
