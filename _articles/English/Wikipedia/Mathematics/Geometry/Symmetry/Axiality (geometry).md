---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Axiality_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: a5bba570-05b8-4ba0-9738-68a279f26b92
updated: 1484309305
title: Axiality (geometry)
tags:
    - Upper and lower bounds
    - Algorithms
    - Related concepts
categories:
    - Symmetry
---
In the geometry of the Euclidean plane, axiality is a measure of how much axial symmetry a shape has. It is defined as the ratio of areas of the largest axially symmetric subset of the shape to the whole shape. Equivalently it is the largest fraction of the area of the shape that can be covered by a mirror reflection of the shape (with any orientation).
