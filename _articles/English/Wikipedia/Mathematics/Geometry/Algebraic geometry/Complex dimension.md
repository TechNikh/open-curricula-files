---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_dimension
offline_file: ""
offline_thumbnail: ""
uuid: 2f29374b-f07f-4fc4-acd7-cdb40f9ccc3b
updated: 1484309138
title: Complex dimension
categories:
    - Algebraic geometry
---
In mathematics, complex dimension usually refers to the dimension of a complex manifold M, or a complex algebraic variety V.[1] If the complex dimension is d, the real dimension will be 2d.[2] That is, the smooth manifold M has dimension 2d; and away from any singular point V will also be a smooth manifold of dimension 2d.
