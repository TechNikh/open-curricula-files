---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resemble
offline_file: ""
offline_thumbnail: ""
uuid: c232ffb8-671d-4784-be97-201843016ecd
updated: 1484310279
title: resemble
categories:
    - Dictionary
---
resemble
     v : appear like; be similar or bear a likeness to; "She
         resembles her mother very much"; "This paper resembles my
         own work"
