---
version: 1
type: video
provider: YouTube
id: zqGvUbvVQXg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Electric%20charge%2C%20electric%20force%2C%20and%20voltage/Voltage.webm"
offline_file: ""
offline_thumbnail: ""
uuid: abbcec12-5986-478e-8374-e7405cb57352
updated: 1486069591
title: Voltage
categories:
    - Electric charge, electric force, and voltage
thumbnail_urls:
    - https://i3.ytimg.com/vi/zqGvUbvVQXg/default.jpg
    - https://i3.ytimg.com/vi/zqGvUbvVQXg/1.jpg
    - https://i3.ytimg.com/vi/zqGvUbvVQXg/2.jpg
    - https://i3.ytimg.com/vi/zqGvUbvVQXg/3.jpg
---
Sal explains the difference between electrical potential (voltage) and electrical potential energy. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/electric-charge-electric-force-and-voltage/electric-potential-voltage/v/electric-potential-at-a-point-in-space?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/electric-charge-electric-force-and-voltage/electric-potential-voltage/v/electric-potential-energy-part-2-involves-calculus?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
