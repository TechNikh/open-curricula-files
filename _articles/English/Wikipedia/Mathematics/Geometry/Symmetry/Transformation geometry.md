---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transformation_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 77246f1c-888b-4e6e-91a1-2e592609800b
updated: 1484309322
title: Transformation geometry
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Simx2%253Dtransl_OK.svg.png'
categories:
    - Symmetry
---
In mathematics, transformation geometry (or transformational geometry) is the name of a mathematical and pedagogic take on the study of geometry by focusing on groups of geometric transformations, and that are invariant under them. It is opposed to the classical synthetic geometry approach of Euclidean geometry, that focuses on geometric constructions.
