---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unethical
offline_file: ""
offline_thumbnail: ""
uuid: 9fe6dee2-cfa0-44d3-abe6-70c4c26fcd51
updated: 1484310170
title: unethical
categories:
    - Dictionary
---
unethical
     adj 1: not conforming to approved standards of social or
            professional behavior; "unethical business practices"
            [ant: {ethical}]
     2: not adhering to ethical or moral principles; "base and
        unpatriotic motives"; "a base, degrading way of life";
        "cheating is dishonorable"; "they considered colonialism
        immoral"; "unethical practices in handling public funds"
        [syn: {base}, {dishonorable}, {dishonourable}, {immoral}]
