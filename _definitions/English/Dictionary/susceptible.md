---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/susceptible
offline_file: ""
offline_thumbnail: ""
uuid: a15edfb2-752c-4fdd-8652-336cd76bccdc
updated: 1484310581
title: susceptible
categories:
    - Dictionary
---
susceptible
     adj 1: (often followed by `of' or `to') yielding readily to or
            capable of; "susceptible to colds"; "susceptible of
            proof" [ant: {unsusceptible}]
     2: easily impressed emotionally
