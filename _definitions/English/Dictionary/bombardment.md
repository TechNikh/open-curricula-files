---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bombardment
offline_file: ""
offline_thumbnail: ""
uuid: b0a861e0-23cc-445e-a052-dd418aedfcd0
updated: 1484310399
title: bombardment
categories:
    - Dictionary
---
bombardment
     n 1: the act (or an instance) of subjecting a body or substance
          to the impact of high-energy particles (as electrons or
          alpha rays)
     2: the heavy fire of artillery to saturate an area rather than
        hit a specific target; "they laid down a barrage in front
        of the advancing troops"; "the shelling went on for hours
        without pausing" [syn: {barrage}, {barrage fire}, {battery},
         {shelling}]
     3: an attack by dropping bombs [syn: {bombing}]
