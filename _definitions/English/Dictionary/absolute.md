---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/absolute
offline_file: ""
offline_thumbnail: ""
uuid: d84dca44-f990-4894-9cb9-30258f9cb0b7
updated: 1484310252
title: absolute
categories:
    - Dictionary
---
absolute
     adj 1: perfect or complete or pure; "absolute loyalty"; "absolute
            silence"; "absolute truth"; "absolute alcohol" [ant: {relative}]
     2: complete and without restriction or qualification; sometimes
        used informally as intensifiers; "absolute freedom"; "an
        absolute dimwit"; "a downright lie"; "out-and-out mayhem";
        "an out-and-out lie"; "a rank outsider"; "many right-down
        vices"; "got the job through sheer persistence"; "sheer
        stupidity" [syn: {downright}, {out-and-out(a)}, {rank(a)},
         {right-down}, {sheer(a)}]
     3: not limited by law; "an absolute monarch"
     4: expressing finality with no implication of possible ...
