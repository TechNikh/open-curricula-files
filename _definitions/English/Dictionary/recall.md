---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recall
offline_file: ""
offline_thumbnail: ""
uuid: b1519bc6-cbd6-4e02-a2d1-d585d22fa665
updated: 1484310361
title: recall
categories:
    - Dictionary
---
recall
     n 1: a request by the manufacturer of a defective product to
          return the product (as for replacement or repair) [syn:
          {callback}]
     2: a call to return; "the recall of our ambassador"
     3: a bugle call that signals troops to return
     4: the process of remembering (especially the process of
        recovering information by mental effort); "he has total
        recall of the episode" [syn: {recollection}, {reminiscence}]
     5: the act of removing an official by petition
     v 1: recall knowledge from memory; have a recollection; "I can't
          remember saying any such thing"; "I can't think what her
          last name was"; "can you remember ...
