---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Abraham%E2%80%93Lorentz_force'
offline_file: ""
offline_thumbnail: ""
uuid: 5d3f02dd-1828-4560-8091-b6d92924cae7
updated: 1484308749
title: Abraham–Lorentz force
tags:
    - Definition and description
    - Background
    - Derivation
    - Signals from the future
    - Abraham–Lorentz–Dirac Force
    - Definition
    - Paradoxes
categories:
    - Electrodynamics
---
In the physics of electromagnetism, the Abraham–Lorentz force (also Lorentz–Abraham force) is the recoil force on an accelerating charged particle caused by the particle emitting electromagnetic radiation. It is also called the radiation reaction force or the self force.
