---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stockinged
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484453041
title: stockinged
categories:
    - Dictionary
---
stockinged
     adj : wearing stockings; "walks about in his stockinged (or
           stocking) feet" [syn: {stocking}]
