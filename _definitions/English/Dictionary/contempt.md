---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contempt
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484593021
title: contempt
categories:
    - Dictionary
---
contempt
     n 1: lack of respect accompanied by a feeling of intense dislike;
          "he was held in contempt"; "the despite in which
          outsiders were held is legendary" [syn: {disdain}, {scorn},
           {despite}]
     2: a manner that is generally disrespectful and contemptuous
        [syn: {disrespect}]
     3: open disrespect for a person or thing [syn: {scorn}]
     4: a willful disobedience to or disrespect for the authority of
        a court or legislative body
