---
version: 1
type: video
provider: YouTube
id: ysvFgARjjBM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2a087f79-54ba-4399-af4a-d26c62cf0c6c
updated: 1486069618
title: Area between curves
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/ysvFgARjjBM/default.jpg
    - https://i3.ytimg.com/vi/ysvFgARjjBM/1.jpg
    - https://i3.ytimg.com/vi/ysvFgARjjBM/2.jpg
    - https://i3.ytimg.com/vi/ysvFgARjjBM/3.jpg
---
