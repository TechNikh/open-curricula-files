---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electrochemical_gradient
offline_file: ""
offline_thumbnail: ""
uuid: 669f8c66-55fa-477c-8faf-3e754cad5ad4
updated: 1484308840
title: Electrochemical gradient
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Membrane_potential_ions_en.svg.png
tags:
    - Overview
    - chemistry
    - Biological context
    - Ion gradients
    - Proton gradients
    - Bacteriorhodopsin
    - Photophosphorylation
    - Oxidative Phosphorylation
categories:
    - Thermodynamics
---
An electrochemical gradient is a gradient of electrochemical potential, usually for an ion that can move across a membrane. The gradient consists of two parts, the chemical gradient, or difference in solute concentration across a membrane, and the electrical gradient, or difference in charge across a membrane. When there are unequal concentrations of an ion across a permeable membrane, the ion will move across the membrane from the area of higher concentration to the area of lower concentration through simple diffusion. Ions also carry an electric charge that forms an electric potential across a membrane. If there is an unequal distribution of charges across the membrane, then the ...
