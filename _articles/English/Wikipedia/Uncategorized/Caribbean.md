---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Caribbean
offline_file: ""
offline_thumbnail: ""
uuid: 900966d9-436a-41ed-b4ce-acf43e355cbd
updated: 1484308447
title: Caribbean
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-CIA_map_of_the_Caribbean.png
tags:
    - Etymology and pronunciation
    - Definition
    - Geography and geology
    - Climate
    - Island groups
    - Historical groupings
    - Modern-day island territories
    - Continental countries with Caribbean coastlines and islands
    - biodiversity
    - Plants and animals of the Caribbean
    - Demographics
    - Indigenous groups
    - language
    - religion
    - Politics
    - Regionalism
    - United States effects on regionalism
    - European Union effects on regionalism
    - "Venezuela's effects on regionalism"
    - Regional institutions
    - Cuisine
    - Favorite or national dishes
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
The Caribbean (/ˌkærᵻˈbiːən/ or /kəˈrɪbiən/) is a region that consists of the Caribbean Sea, its islands (some surrounded by the Caribbean Sea and some bordering both the Caribbean Sea and the North Atlantic Ocean) and the surrounding coasts. The region is southeast of the Gulf of Mexico and the North American mainland, east of Central America, and north of South America.
