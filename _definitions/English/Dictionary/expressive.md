---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expressive
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484555101
title: expressive
categories:
    - Dictionary
---
expressive
     adj : characterized by expression; "a very expressive face"
