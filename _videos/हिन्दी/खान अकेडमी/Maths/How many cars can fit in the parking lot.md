---
version: 1
type: video
provider: YouTube
id: mGydNPcYDIE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b5633a8d-0a1d-4746-8bea-7140da8abb85
updated: 1486069617
title: How many cars can fit in the parking lot
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/mGydNPcYDIE/default.jpg
    - https://i3.ytimg.com/vi/mGydNPcYDIE/1.jpg
    - https://i3.ytimg.com/vi/mGydNPcYDIE/2.jpg
    - https://i3.ytimg.com/vi/mGydNPcYDIE/3.jpg
---
