---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Petersen%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5da735cc-ed20-49d7-a7b2-89bcb8e7802a
updated: 1484309423
title: "Petersen's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Petersen-graph-factors.svg.png
tags:
    - Proof
    - History
    - Applications
    - Extensions
    - Number of perfect matchings in cubic bridgeless graphs
    - Algorithmic versions
    - Higher degree
    - Notes
categories:
    - Matching
---
In the mathematical discipline of graph theory, Petersen's theorem, named after Julius Petersen, is one of the earliest results in graph theory and can be stated as follows:
