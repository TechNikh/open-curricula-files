---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Incentive-centered_design
offline_file: ""
offline_thumbnail: ""
uuid: 59d2ce9f-a0ba-4d66-9b6c-cc6aac511122
updated: 1484309046
title: Incentive-centered design
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nike-nikeplus-plus-3912690-o.jpg
tags:
    - History
    - Interdisciplinary concepts
    - Information security
    - User-generated content
    - Reputation systems
    - Social computing
    - Recommender systems
    - Online auction design
    - Current research
    - Practical applications
    - Nike+iPod Sports Kit
    - Ford Hybrid Car
    - Class grading system at Indiana University
    - Achievements
    - Future applications
categories:
    - Mechanism design
---
Incentive-centered design (ICD) is the science of designing a system or institution according to the alignment of individual and user incentives with the goals of the system. Using incentive-centered design, system designers can observe systematic and predictable tendencies in users in response to motivators to provide or manage incentives to induce a greater amount and more valuable participation.[1] ICD is often considered when designing a system to induce desirable behaviors from users, such as participation and cooperation. It draws from principles in various areas such as economics, psychology, sociology, design, and engineering. ICD has been gaining attention in research communities ...
