---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-magnetic
offline_file: ""
offline_thumbnail: ""
uuid: d74bf680-7112-49c7-bbec-51cdba515c68
updated: 1484308391
title: Magnetism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Magnetic_quadrupole_moment.svg.png
tags:
    - History
    - Sources of magnetism
    - Materials
    - Diamagnetism
    - Paramagnetism
    - Ferromagnetism
    - Magnetic domains
    - Antiferromagnetism
    - Ferrimagnetism
    - Superparamagnetism
    - Other types of magnetism
    - Electromagnet
    - Magnetism, electricity, and special relativity
    - Magnetic fields in a material
    - Magnetic force
    - Magnetic dipoles
    - Magnetic monopoles
    - Quantum-mechanical origin of magnetism
    - Units
    - SI
    - Other
    - Living things
categories:
    - Uncategorized
---
Magnetism is a class of physical phenomena that are mediated by magnetic fields. Electric currents and the magnetic moments of elementary particles give rise to a magnetic field, which acts on other currents and magnetic moments. The most familiar effects occur in ferromagnetic materials, which are strongly attracted by magnetic fields and can be magnetized to become permanent magnets, producing magnetic fields themselves. Only a few substances are ferromagnetic; the most common ones are iron, nickel and cobalt and their alloys. The prefix ferro- refers to iron, because permanent magnetism was first observed in lodestone, a form of natural iron ore called magnetite, Fe3O4.
