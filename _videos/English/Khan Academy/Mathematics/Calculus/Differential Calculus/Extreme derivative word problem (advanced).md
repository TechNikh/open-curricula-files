---
version: 1
type: video
provider: YouTube
id: viaPc8zDcRI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Extreme%20derivative%20word%20problem%20%28advanced%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 373d6df1-f9a1-442f-9d4b-4569b6a6f62f
updated: 1486069693
title: Extreme derivative word problem (advanced)
categories:
    - Differential Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/viaPc8zDcRI/default.jpg
    - https://i3.ytimg.com/vi/viaPc8zDcRI/1.jpg
    - https://i3.ytimg.com/vi/viaPc8zDcRI/2.jpg
    - https://i3.ytimg.com/vi/viaPc8zDcRI/3.jpg
---
