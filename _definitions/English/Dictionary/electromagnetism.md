---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electromagnetism
offline_file: ""
offline_thumbnail: ""
uuid: b4772d78-15c9-47a9-941d-e68663d2e76d
updated: 1484310198
title: electromagnetism
categories:
    - Dictionary
---
electromagnetism
     n 1: magnetism produced by an electric current; "electromagnetism
          was discovered when it was observed that a copper wire
          carrying an electric current can magnetize pieces of
          iron or steel near it"
     2: the branch of physics concerned with electromagnetic
        phenomena [syn: {electromagnetics}]
