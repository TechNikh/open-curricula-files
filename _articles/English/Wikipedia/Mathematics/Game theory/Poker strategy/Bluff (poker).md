---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bluff_(poker)
offline_file: ""
offline_thumbnail: ""
uuid: 7db013c3-8dab-47ea-821f-de6846284859
updated: 1484309062
title: Bluff (poker)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Holdem.jpg
tags:
    - Pure bluff
    - Semi-bluff
    - Bluffing circumstances
    - Optimal bluffing frequency
    - Bluffing in other games
    - Artificial intelligence
    - Notes
categories:
    - Poker strategy
---
In the card game of poker, a bluff is a bet or raise made with a hand which is not thought to be the best hand. To bluff is to make such a bet. The objective of a bluff is to induce a fold by at least one opponent who holds a better hand. The size and frequency of a bluff determines its profitability to the bluffer. By extension, the phrase "calling somebody's bluff" is often used outside the context of poker to describe cases where one person "demand[s] that someone prove a claim" or prove that he or she "is not being deceptive."[1]
