---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disgusted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484551381
title: disgusted
categories:
    - Dictionary
---
disgusted
     adj : having a strong distaste from surfeit; "grew more and more
           disgusted"; "fed up with their complaints"; "sick of it
           all"; "sick to death of flattery"; "gossip that makes
           one sick"; "tired of the noise and smoke" [syn: {fed
           up(p)}, {sick(p)}, {sick of(p)}, {tired of(p)}]
