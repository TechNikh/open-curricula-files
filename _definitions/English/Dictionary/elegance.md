---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elegance
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484520181
title: elegance
categories:
    - Dictionary
---
elegance
     n : a refined quality of gracefulness and good taste [ant: {inelegance}]
