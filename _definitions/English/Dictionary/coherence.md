---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coherence
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484630761
title: coherence
categories:
    - Dictionary
---
coherence
     n 1: the state of cohering or sticking together [syn: {coherency},
           {cohesion}, {cohesiveness}] [ant: {incoherence}]
     2: logical and orderly and consistent relation of parts [syn: {coherency}]
