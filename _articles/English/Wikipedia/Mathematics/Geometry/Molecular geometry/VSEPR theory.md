---
version: 1
type: article
id: https://en.wikipedia.org/wiki/VSEPR_theory
offline_file: ""
offline_thumbnail: ""
uuid: 198a34aa-8d36-4f6f-8940-33155453ba76
updated: 1484309283
title: VSEPR theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-VSEPR_acqua.png
tags:
    - History
    - Overview
    - Degree of repulsion
    - AXE method
    - Examples
    - Exceptions
    - Some AX2E0 molecules
    - Some AX2E2 molecules
    - Some AX6E1 and AX8E1 molecules
    - Transition metal molecules
    - Odd-electron molecules
categories:
    - Molecular geometry
---
Valence shell electron pair repulsion (VSEPR) theory is a model used in chemistry to predict the geometry of individual molecules from the number of electron pairs surrounding their central atoms.[1] It is also named the Gillespie-Nyholm theory after its two main developers. The acronym "VSEPR" is pronounced either "ves-per"[2]:410 or "vuh-seh-per"[3] by some chemists.
