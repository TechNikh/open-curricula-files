---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/standing
offline_file: ""
offline_thumbnail: ""
uuid: 2bafea51-1d54-4320-aa83-da45d9a58614
updated: 1484310244
title: standing
categories:
    - Dictionary
---
standing
     adj 1: having a supporting base; "a standing lamp" [syn: {standing(a)}]
     2: (of fluids) not moving or flowing; "mosquitoes breed in
        standing water" [syn: {standing(a)}] [ant: {running(a)}]
     3: not created for a particular occasion; "a standing
        committee" [syn: {standing(a)}]
     4: maintaining an erect position; "standing timber"; "many
        buildings were still standing" [syn: {upright}] [ant: {falling}]
     5: executed in or initiated from a standing position; "a
        standing ovation"; "race from a standing start"; "a
        standing jump"; "a standing ovation" [syn: {standing(a)}]
        [ant: {running(a)}]
     6: (of persons) on the ...
