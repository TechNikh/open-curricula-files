---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peacefully
offline_file: ""
offline_thumbnail: ""
uuid: 7bbd9297-ef5c-40a5-9009-25694aa83033
updated: 1484310557
title: peacefully
categories:
    - Dictionary
---
peacefully
     adv : in a peaceful manner; "the hen settled herself on the nest
           most peacefully"
