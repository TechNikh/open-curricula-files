---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Semigroup_action
offline_file: ""
offline_thumbnail: ""
uuid: 9b02c752-7521-4c3b-a9b8-a3202d83ba7f
updated: 1484308684
title: Semigroup action
tags:
    - Formal definitions
    - Terminology and notation
    - Acts and transformations
    - S-homomorphisms
    - S-Act and M-Act
    - Transformation semigroups
    - Applications to computer science
    - Semiautomata
    - Krohn–Rhodes theory
    - Notes
categories:
    - Theoretical computer science
---
In algebra and theoretical computer science, an action or act of a semigroup on a set is a rule which associates to each element of the semigroup a transformation of the set in such a way that the product of two elements of the semigroup (using the semigroup operation) is associated with the composite of the two corresponding transformations. The terminology conveys the idea that the elements of the semigroup are acting as transformations of the set. From an algebraic perspective, a semigroup action is a generalization of the notion of a group action in group theory. From the computer science point of view, semigroup actions are closely related to automata: the set models the state of the ...
