---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Light%27s_associativity_test'
offline_file: ""
offline_thumbnail: ""
uuid: 9fbb6621-3c4f-4d0d-90a2-fa0cc447de43
updated: 1484308913
title: "Light's associativity test"
tags:
    - Description of the procedure
    - Example
    - A further simplification
    - "Program for Light's associativity test"
    - "Extension of Light's associativity test"
    - More advanced algorithms
categories:
    - Elementary algebra
---
In mathematics, Light's associativity test is a procedure invented by F. W. Light for testing whether a binary operation defined in a finite set by a Cayley multiplication table is associative. The naive procedure for verification of the associativity of a binary operation specified by a Cayley table, which compares the two products that can be formed from each triple of elements, is cumbersome. Light's associativity test simplifies the task in some instances (although it does not improve the worst-case runtime of the naive algorithm, namely O(n^3) for sets of size n).
