---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/salty
offline_file: ""
offline_thumbnail: ""
uuid: 9d3cf400-ec9c-4052-a7d6-fd6aad621efb
updated: 1484310346
title: salty
categories:
    - Dictionary
---
salty
     adj 1: engagingly stimulating or provocative; "a piquant wit";
            "salty language" [syn: {piquant}]
     2: containing salt; "a saline solution"; "salty tears" [syn: {saline}]
     3: one of the four basic taste sensations; like the taste of
        sea water [syn: {salt}]
     [also: {saltiest}, {saltier}]
