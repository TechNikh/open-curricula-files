---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Factorial
offline_file: ""
offline_thumbnail: ""
uuid: de44d4a3-07ae-4072-8cdd-1be0367a61e4
updated: 1484308577
title: Factorial
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Log-factorial.svg.png
tags:
    - Definition
    - Applications
    - Rate of growth and approximations for large n
    - Computation
    - Number theory
    - Series of reciprocals
    - Extension of factorial to non-integer values of argument
    - The Gamma and Pi functions
    - Applications of the Gamma function
    - Factorial at the complex plane
    - Approximations of factorial
    - Non-extendability to negative integers
    - Factorial-like products and functions
    - Double factorial
    - Multifactorials
    - Alternative extension of the multifactorial
    - Primorial
    - Quadruple factorial
    - Superfactorial
    - Alternative definition
    - Hyperfactorial
    - Notes
categories:
    - Combinatorics
---
In mathematics, the factorial of a non-negative integer n, denoted by n!, is the product of all positive integers less than or equal to n. For example,
