---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mail
offline_file: ""
offline_thumbnail: ""
uuid: 378061c0-d211-4a30-a117-52402c3ff113
updated: 1484310527
title: mail
categories:
    - Dictionary
---
mail
     n 1: the bags of letters and packages that are transported by the
          postal service
     2: the system whereby messages are transmitted via the post
        office; "the mail handles billions of items every day";
        "he works for the United States mail service"; "in England
        they call mail `the post'" [syn: {mail service}, {postal
        service}, {post}]
     3: a conveyance that transports mail
     4: any particular collection of letters or packages that is
        delivered; "your mail is on the table"; "is there any post
        for me?"; "she was opening her post" [syn: {post}]
     5: (Middle Ages) flexible armor made of interlinked metal rings
        ...
