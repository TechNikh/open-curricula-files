---
version: 1
type: video
provider: YouTube
id: iDGwrS7WuC4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Complex%20Sentences%20in%20English.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cf33c1ba-8e1f-4e85-a74c-90f33641b5b9
updated: 1486069631
title: Complex Sentences in English
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/iDGwrS7WuC4/default.jpg
    - https://i3.ytimg.com/vi/iDGwrS7WuC4/1.jpg
    - https://i3.ytimg.com/vi/iDGwrS7WuC4/2.jpg
    - https://i3.ytimg.com/vi/iDGwrS7WuC4/3.jpg
---
