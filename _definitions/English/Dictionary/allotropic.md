---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/allotropic
offline_file: ""
offline_thumbnail: ""
uuid: 1179bc10-395a-4167-b1ab-305acc587e88
updated: 1484310414
title: allotropic
categories:
    - Dictionary
---
allotropic
     adj : of or related to or exhibiting allotropism; "carbon and
           sulfur and phosphorus are allotropic elements" [syn: {allotropical}]
