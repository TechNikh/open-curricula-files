---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Time-varying_network
offline_file: ""
offline_thumbnail: ""
uuid: f3269c12-540f-4bfb-bd24-ed4f4571389a
updated: 1484309443
title: Time-varying network
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/688px-MontreGousset001_2.jpg
tags:
    - Applicability
    - Representations
    - Properties
    - Time respecting paths
    - Reachability
    - Latency
    - Centrality measures
    - Temporal patterns
    - Dynamics
    - Burstiness
categories:
    - Network theory
---
A time-varying network, also known as a temporal network, is a network whose links are active only at certain points in time. Each link carries information on when it is active, along with other possible characteristics such as a weight. Time-varying networks are of particular relevance to spreading processes, like the spread of information and disease, since each link is a contact opportunity and the time ordering of contacts is included.
