---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Frucht%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 97f48aa0-2694-491e-b01d-960dc2548afa
updated: 1484309374
title: "Frucht's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Frucht_graph.dot.svg.png
tags:
    - Proof idea
    - Graph size
    - Special families of graphs
    - Infinite groups and graphs
    - Notes
categories:
    - Algebraic graph theory
---
Frucht's theorem is a theorem in algebraic graph theory conjectured by Dénes Kőnig in 1936 and proved by Robert Frucht in 1939. It states that every finite group is the group of symmetries of a finite undirected graph. More strongly, for any finite group G there exist infinitely many non-isomorphic simple connected graphs such that the automorphism group of each of them is isomorphic to G.
