---
version: 1
type: video
provider: YouTube
id: mMLOBkJltIw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 94ec9b9b-b475-47f3-8fe6-ec998c7dce29
updated: 1486069648
title: '3D Mandelbrot zoom (HD) - Split Point II'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/mMLOBkJltIw/default.jpg
    - https://i3.ytimg.com/vi/mMLOBkJltIw/1.jpg
    - https://i3.ytimg.com/vi/mMLOBkJltIw/2.jpg
    - https://i3.ytimg.com/vi/mMLOBkJltIw/3.jpg
---
