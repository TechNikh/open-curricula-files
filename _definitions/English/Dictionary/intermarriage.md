---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intermarriage
offline_file: ""
offline_thumbnail: ""
uuid: 80d8b975-2377-4f13-a66c-ebe023b296fe
updated: 1484310479
title: intermarriage
categories:
    - Dictionary
---
intermarriage
     n 1: marriage to a person belonging to a tribe or group other
          than your own as required by custom or law [syn: {exogamy}]
          [ant: {endogamy}]
     2: marriage within one's own tribe or group as required by
        custom or law [syn: {endogamy}, {inmarriage}] [ant: {exogamy}]
