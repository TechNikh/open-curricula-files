---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sullivan_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 7f23bd26-cb3c-48b7-8ec0-bce6c8ddb0c7
updated: 1484308787
title: Sullivan conjecture
categories:
    - Fixed points (mathematics)
---
In mathematics, Sullivan conjecture can refer to any of several results and conjectures prompted by homotopy theory work of Dennis Sullivan. A basic theme and motivation concerns the fixed point set in group actions of a finite group 
  
    
      
        G
      
    
    {\displaystyle G}
  
. The most elementary formulation, however, is in terms of the classifying space 
  
    
      
        B
        G
      
    
    {\displaystyle BG}
  
 of such a group. Roughly speaking, it is difficult to map such a space 
  
    
      
        B
        G
      
    
    {\displaystyle BG}
  
 continuously into a finite CW complex 
  
    
      
        X
      
    
    {\displaystyle X}
  ...
