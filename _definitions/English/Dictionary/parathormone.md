---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parathormone
offline_file: ""
offline_thumbnail: ""
uuid: 935780b9-07af-4174-8cf6-8dcc355ecd0c
updated: 1484310163
title: parathormone
categories:
    - Dictionary
---
parathormone
     n : hormone synthesized and released into the blood stream by
         the parathyroid glands; regulates phosphorus and calcium
         in the body and functions in neuromuscular excitation and
         blood clotting [syn: {parathyroid hormone}]
