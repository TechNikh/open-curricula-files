---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Genus_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: da4df670-f97f-4275-aaa4-83c77e006c0a
updated: 1484309420
title: Genus (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Double_torus_illustration.png
tags:
    - Topology
    - Orientable surface
    - Non-orientable surfaces
    - Knot
    - Handlebody
    - Graph theory
    - Algebraic geometry
categories:
    - Graph invariants
---
