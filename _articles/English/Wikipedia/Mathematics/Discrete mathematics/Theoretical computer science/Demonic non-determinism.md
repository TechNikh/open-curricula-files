---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Demonic_non-determinism
offline_file: ""
offline_thumbnail: ""
uuid: 7342bf0f-a4b9-4a5f-8654-b85dd1e7d162
updated: 1484308675
title: Demonic non-determinism
categories:
    - Theoretical computer science
---
A term coined by C.A.R Hoare[citation needed], which describes the execution of a non-deterministic program where all choices that are made favour non-termination.[citation needed]
