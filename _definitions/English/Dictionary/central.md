---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/central
offline_file: ""
offline_thumbnail: ""
uuid: 0f4df02b-cf51-494a-983b-aa4c8d7a0b3f
updated: 1484310363
title: central
categories:
    - Dictionary
---
central
     adj 1: serving as an essential component; "a cardinal rule"; "the
            central cause of the problem"; "an example that was
            fundamental to the argument"; "computers are
            fundamental to modern industrial structure" [syn: {cardinal},
             {fundamental}, {key}, {primal}]
     2: in or near a center or constituting a center; the inner
        area; "a central position"; "central heating and air
        conditioning" [ant: {peripheral}]
     3: used in the description of a place that in the middle of
        another place; "the people of Central and Northern
        Europe"; "country in central Africa"
     4: centrally located and easy to reach; ...
