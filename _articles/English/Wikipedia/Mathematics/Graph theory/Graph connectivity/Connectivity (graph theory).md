---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Connectivity_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 6531e0af-980a-458a-94b5-30ba8f4ef1b3
updated: 1484309402
title: Connectivity (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Network_Community_Structure.svg.png
tags:
    - Connected graph
    - Definitions of components, cuts and connectivity
    - "Menger's theorem"
    - Computational aspects
    - Number of connected graphs
    - Examples
    - Bounds on connectivity
    - Other properties
categories:
    - Graph connectivity
---
In mathematics and computer science, connectivity is one of the basic concepts of graph theory: it asks for the minimum number of elements (nodes or edges) that need to be removed to disconnect the remaining nodes from each other.[1] It is closely related to the theory of network flow problems. The connectivity of a graph is an important measure of its resilience as a network.
