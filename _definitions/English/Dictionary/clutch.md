---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clutch
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484610841
title: clutch
categories:
    - Dictionary
---
clutch
     n 1: the act of grasping; "he released his clasp on my arm"; "he
          has a strong grip for an old man"; "she kept a firm hold
          on the railing" [syn: {clasp}, {clench}, {clutches}, {grasp},
           {grip}, {hold}]
     2: a tense critical situation; "he is a good man in the clutch"
     3: a number of birds hatched at the same time
     4: a collection of things or persons to be handled together
        [syn: {batch}]
     5: a pedal that operates a clutch [syn: {clutch pedal}]
     6: a coupling that connects or disconnects driving and driven
        parts of a driving mechanism
     v 1: take hold of; grab; "The salesclerk quickly seized the money
          on ...
