---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/voltage
offline_file: ""
offline_thumbnail: ""
uuid: c2c382ab-e3b6-4c3d-b80a-c07c5bd191df
updated: 1484310202
title: voltage
categories:
    - Dictionary
---
voltage
     n 1: the rate at which energy is drawn from a source that
          produces a flow of electricity in a circuit; expressed
          in volts [syn: {electromotive force}, {emf}]
     2: the difference in electrical charge between two points in a
        circuit expressed in volts [syn: {electric potential}, {potential},
         {potential difference}, {potential drop}]
