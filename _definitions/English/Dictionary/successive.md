---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/successive
offline_file: ""
offline_thumbnail: ""
uuid: 640ec87b-38c6-4e87-9a1d-e4ea9d980881
updated: 1484310271
title: successive
categories:
    - Dictionary
---
successive
     adj : in regular succession without gaps; "serial concerts" [syn:
           {consecutive}, {sequent}, {sequential}, {serial}]
