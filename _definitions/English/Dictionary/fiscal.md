---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fiscal
offline_file: ""
offline_thumbnail: ""
uuid: 908bb0c6-ef37-4c45-881f-581b47778e29
updated: 1484310517
title: fiscal
categories:
    - Dictionary
---
fiscal
     adj : involving financial matters; "fiscal responsibility" [syn: {financial}]
           [ant: {nonfinancial}]
