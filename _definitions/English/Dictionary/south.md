---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/south
offline_file: ""
offline_thumbnail: ""
uuid: a72437fd-aedc-463f-829e-2b3d3738fa8f
updated: 1484310279
title: south
categories:
    - Dictionary
---
south
     adj : situated in or facing or moving toward or coming from the
           south; "the south entrance" [ant: {north}]
     n 1: the region of the United States lying south of the
          Mason-Dixon Line
     2: the southern states that seceded from the United States in
        1861 [syn: {Confederacy}, {Confederate States}, {Confederate
        States of America}, {Dixie}, {Dixieland}]
     3: the cardinal compass point that is at 180 degrees [syn: {due
        south}, {S}]
     4: any region lying in or toward the south [syn: {southland}]
     adv : in a southern direction; "we moved south" [syn: {to the
           south}, {in the south}]
