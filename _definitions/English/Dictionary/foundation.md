---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foundation
offline_file: ""
offline_thumbnail: ""
uuid: 027900b1-7b17-4be6-9602-28b511e7b3b6
updated: 1484310553
title: foundation
categories:
    - Dictionary
---
foundation
     n 1: the basis on which something is grounded; "there is little
          foundation for his objections"
     2: an institution supported by an endowment
     3: the fundamental assumptions from which something is begun or
        developed or calculated or explained; "the whole argument
        rested on a basis of conjecture" [syn: {basis}, {base}, {fundament},
         {groundwork}, {cornerstone}]
     4: lowest support of a structure; "it was built on a base of
        solid rock"; "he stood at the foot of the tower" [syn: {base},
         {fundament}, {foot}, {groundwork}, {substructure}, {understructure}]
     5: education or instruction in the fundamentals of a field ...
