---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/palaeontology
offline_file: ""
offline_thumbnail: ""
uuid: d608b96f-28ea-44f3-915b-6ef7ab11c098
updated: 1484310281
title: palaeontology
categories:
    - Dictionary
---
palaeontology
     n : the earth science that studies fossil organisms and related
         remains [syn: {paleontology}, {fossilology}]
