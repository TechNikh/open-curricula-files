---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/creatine
offline_file: ""
offline_thumbnail: ""
uuid: d580680e-7178-4798-8e0e-8fa7cfb019ef
updated: 1484310158
title: creatine
categories:
    - Dictionary
---
creatine
     n : an amino acid that does not occur in proteins but is found
         in the muscle tissue of vertebrates both in the free form
         and as phosphocreatine; supplies energy for muscle
         contraction [syn: {creatin}]
