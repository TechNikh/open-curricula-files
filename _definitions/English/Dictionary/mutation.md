---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mutation
offline_file: ""
offline_thumbnail: ""
uuid: 884f9a0f-7d3e-478d-a585-8fdd68b4d437
updated: 1484310289
title: mutation
categories:
    - Dictionary
---
mutation
     n 1: (biology) an organism that has characteristics resulting
          from chromosomal alteration [syn: {mutant}, {variation},
           {sport}]
     2: (genetics) any event that changes genetic structure; any
        alteration in the inherited nucleic acid sequence of the
        genotype of an organism [syn: {genetic mutation}, {chromosomal
        mutation}]
     3: a change or alteration in form or qualities
