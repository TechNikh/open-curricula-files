---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shared
offline_file: ""
offline_thumbnail: ""
uuid: 61826367-0457-46a2-bf4c-266a263111bf
updated: 1484310232
title: shared
categories:
    - Dictionary
---
shared
     adj 1: have in common; held or experienced in common; "two shared
            valence electrons forming a bond between adjacent
            nuclei"; "a shared interest in philately" [ant: {unshared}]
     2: distributed in portions (often equal) on the basis of a plan
        or purpose [syn: {divided}, {divided up}, {shared out}]
