---
title: Monsoon
article_ref: https://en.wikipedia.org/wiki/Monsoon
tags:
    - Etymology
    - History
    - Strength of impact
    - Process
    - Global monsoons
    - Africa
    - North America
    - Asia
    - South Asian monsoon
    - Southwest monsoon
    - Northeast monsoon
    - East Asian Monsoon
    - Australia
    - Europe
categories:
    - Water harvesting
---
