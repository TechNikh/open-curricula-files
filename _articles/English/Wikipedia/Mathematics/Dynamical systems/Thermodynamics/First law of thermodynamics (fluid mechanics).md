---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/First_law_of_thermodynamics_(fluid_mechanics)
offline_file: ""
offline_thumbnail: ""
uuid: 8205b452-51d8-4997-b58a-5c554f028a68
updated: 1484308845
title: First law of thermodynamics (fluid mechanics)
tags:
    - Note
    - Compressible fluid
    - Integral form
    - Alternative representation
    - Alternative form data
categories:
    - Thermodynamics
---
In physics, the first law of thermodynamics is an expression of the conservation of total energy of a system. The increase of the energy of a system is equal to the sum of work done on the system and the heat added to that system:
