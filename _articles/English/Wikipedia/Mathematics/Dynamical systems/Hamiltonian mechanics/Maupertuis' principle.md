---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Maupertuis%27_principle'
offline_file: ""
offline_thumbnail: ""
uuid: 265b226c-6a81-4d46-a505-8218b3025041
updated: 1484308795
title: "Maupertuis' principle"
tags:
    - Mathematical formulation
    - "Jacobi's formulation"
    - "Comparison with Hamilton's principle"
    - History
categories:
    - Hamiltonian mechanics
---
In classical mechanics, Maupertuis' principle (named after Pierre Louis Maupertuis), is that the path followed by a physical system is the one of least length (with a suitable interpretation of path and length). It is a special case of the more generally stated principle of least action. Using the calculus of variations, it results in an integral equation formulation of the equations of motion for the system.
