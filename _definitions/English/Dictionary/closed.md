---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/closed
offline_file: ""
offline_thumbnail: ""
uuid: 6366cc53-cb96-43be-ad50-eb1ec1a019b4
updated: 1484310328
title: closed
categories:
    - Dictionary
---
closed
     adj 1: not open or affording passage or access; "the many closed
            streets made travel difficult"; "our neighbors peeped
            from behind closed curtains" [ant: {open}]
     2: of a curve or surface; having no end points or boundary
        curves; of a set; having members that can be produced by a
        specific operation on other members of the same set; of an
        interval; containing both its endpoints [ant: {open}]
     3: not open; "the door slammed shut" [syn: {shut}, {unopen}]
        [ant: {open}]
     4: used especially of mouth or eyes; "he sat quietly with
        closed eyes"; "his eyes were shut against the sunlight"
        [syn: {shut}] ...
