---
version: 1
type: video
provider: YouTube
id: 3UHdQn_-DZE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5b72501f-72a1-4ce6-827b-59dbb3b8b8b7
updated: 1486069605
title: Finding the greatest common factor of two monomials
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/3UHdQn_-DZE/default.jpg
    - https://i3.ytimg.com/vi/3UHdQn_-DZE/1.jpg
    - https://i3.ytimg.com/vi/3UHdQn_-DZE/2.jpg
    - https://i3.ytimg.com/vi/3UHdQn_-DZE/3.jpg
---
