---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corresponding
offline_file: ""
offline_thumbnail: ""
uuid: baedb4eb-11f6-4162-af71-4d3abbc1d383
updated: 1484310264
title: corresponding
categories:
    - Dictionary
---
corresponding
     adj 1: accompanying; "all rights carry with them corresponding
            responsibilities"
     2: similar especially in position or purpose; "a number of
        corresponding diagonal points"
     3: conforming in every respect; "boxes with corresponding
        dimensions"; "the like period of the preceding year" [syn:
         {comparable}, {like}]
     4: agreeing in amount, magnitude, or degree; "the figures are
        large but the corresponding totals next year will be
        larger" [syn: {proportionate}, {in proportion to}]
