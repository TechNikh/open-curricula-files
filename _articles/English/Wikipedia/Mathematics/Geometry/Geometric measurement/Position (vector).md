---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Position_(vector)
offline_file: ""
offline_thumbnail: ""
uuid: 136ac6ba-0090-43fc-b959-242786415e89
updated: 1484309242
title: Position (vector)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Space_curve.svg.png
tags:
    - Definition
    - Three dimensions
    - n dimensions
    - Applications
    - Differential geometry
    - Mechanics
    - Derivatives of position
    - Relationship to displacement vectors
    - Notes
categories:
    - Geometric measurement
---
In geometry, a position or position vector, also known as location vector or radius vector, is a Euclidean vector that represents the position of a point P in space in relation to an arbitrary reference origin O. Usually denoted x, r, or s, it corresponds to the straight-line distances along each axis from O to P:[1]
