---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/motional
offline_file: ""
offline_thumbnail: ""
uuid: 7c543038-6de6-4294-8c4b-b66cedf173a3
updated: 1484310366
title: motional
categories:
    - Dictionary
---
motional
     adj : of or relating to or characterized by motion
