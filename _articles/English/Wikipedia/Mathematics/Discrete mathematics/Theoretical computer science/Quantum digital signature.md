---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_digital_signature
offline_file: ""
offline_thumbnail: ""
uuid: 79053866-4756-4657-9a9a-8337bb4e9811
updated: 1484308678
title: Quantum digital signature
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/QDS_Swap_test.jpg
tags:
    - Classical public-key method
    - Quantum Digital Signature
    - Requirements for a good and usable signature scheme
    - Differences between classical and quantum one-way functions
    - Nature of the one-way function
    - Copying the public key
    - >
        Public Key should be the same for every recipient (Swap
        Test)
    - >
        An example of a signing-validation process using a
        simplified Gottesman-Chuang scheme
    - Signing Process
    - Validation Process
    - Avoid a message to be validated differently
categories:
    - Theoretical computer science
---
A Quantum Digital Signature (QDS) refers to the quantum mechanical equivalent of either a classical digital signature or, more generally, a handwritten signature on a paper document. Like a handwritten signature, a digital signature is used to protect a document, such as a digital contract, against forgery by another party or by one of the participating parties.
