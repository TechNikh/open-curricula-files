---
version: 1
type: video
provider: YouTube
id: 1s6G4iCiIHA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 21a3cea5-88b6-44f0-b698-018c11b6331f
updated: 1486069635
title: 'Teaching tips- CD Spectroscope - Science Snacks activity'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/1s6G4iCiIHA/default.jpg
    - https://i3.ytimg.com/vi/1s6G4iCiIHA/1.jpg
    - https://i3.ytimg.com/vi/1s6G4iCiIHA/2.jpg
    - https://i3.ytimg.com/vi/1s6G4iCiIHA/3.jpg
---
