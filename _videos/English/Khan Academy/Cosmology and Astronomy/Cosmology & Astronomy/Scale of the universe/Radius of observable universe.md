---
version: 1
type: video
provider: YouTube
id: 6nVysrZQnOQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Radius%20of%20observable%20universe.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ea7db68d-20e9-461a-8adc-579ca1f149bb
updated: 1486069685
title: Radius of observable universe
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/6nVysrZQnOQ/default.jpg
    - https://i3.ytimg.com/vi/6nVysrZQnOQ/1.jpg
    - https://i3.ytimg.com/vi/6nVysrZQnOQ/2.jpg
    - https://i3.ytimg.com/vi/6nVysrZQnOQ/3.jpg
---
