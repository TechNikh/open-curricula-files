---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vagus
offline_file: ""
offline_thumbnail: ""
uuid: e53193a7-ea45-45af-ad18-0fe57f9a3d1d
updated: 1484310353
title: vagus
categories:
    - Dictionary
---
vagus
     n : a mixed nerve that supplies the pharynx and larynx and lungs
         and heart and esophagus and stomach and most of the
         abdominal viscera [syn: {vagus nerve}, {nervus vagus}, {pneumogastric},
          {pneumogastric nerve}, {tenth cranial nerve}, {wandering
         nerve}]
     [also: {vagi} (pl)]
