---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Narmada
offline_file: ""
offline_thumbnail: ""
uuid: 028f4e46-2094-43d6-8c93-8e0c0703b46f
updated: 1484308420
title: Narmada River
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dhuandhar_falls4.JPG
tags:
    - River regime
    - Narmada basin
    - Geology
    - Religious significance
    - Facts of the valley
    - Forests and sanctuaries
    - Anthropological and archaeological sites
    - Narmada river development (NRD)
    - Early background and dispute
    - Sardar Sarovar Dam
    - Indirasagar Dam
    - Navigation along the river
    - Gallery
    - Bibliography
categories:
    - Uncategorized
---
The Narmada, also called the Rewa, is a river in central India and the fifth longest river in the Indian subcontinent. It is the third longest river that flows entirely within India, after the Godavari, and the Krishna. It is also known as "Life Line of Madhya Pradesh" for its huge contribution to the state of Madhya Pradesh in many ways. It forms the traditional boundary between North India and South India and flows westwards over a length of 1,312 km (815.2 mi) before draining through the Gulf of Cambay into the Arabian Sea, 30 km (18.6 mi) west of Bharuch city of Gujarat.[3]
