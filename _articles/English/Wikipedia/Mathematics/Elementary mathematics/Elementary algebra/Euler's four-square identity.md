---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Euler%27s_four-square_identity'
offline_file: ""
offline_thumbnail: ""
uuid: 61f0fc87-9b84-45d3-b344-6c88cd0c23bf
updated: 1484308907
title: "Euler's four-square identity"
tags:
    - Algebraic identity
    - "Pfister's identity"
categories:
    - Elementary algebra
---
In mathematics, Euler's four-square identity says that the product of two numbers, each of which is a sum of four squares, is itself a sum of four squares.
