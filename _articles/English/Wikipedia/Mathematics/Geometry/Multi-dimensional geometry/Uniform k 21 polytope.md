---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Uniform_k_21_polytope
offline_file: ""
offline_thumbnail: ""
uuid: e894fd2e-a8a6-4437-984a-6af24f76967c
updated: 1484309289
title: Uniform k 21 polytope
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_18.png
tags:
    - Family members
    - Elements
categories:
    - Multi-dimensional geometry
---
In geometry, a uniform k21 polytope is a polytope in k + 4 dimensions constructed from the En Coxeter group, and having only regular polytope facets. The family was named by their Coxeter symbol k21 by its bifurcating Coxeter–Dynkin diagram, with a single ring on the end of the k-node sequence.
