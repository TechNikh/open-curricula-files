---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/butler
offline_file: ""
offline_thumbnail: ""
uuid: 927f4457-e733-442b-8f76-4c9ef9ba8cff
updated: 1484310144
title: butler
categories:
    - Dictionary
---
butler
     n : a manservant (usually the head servant of a household) who
         has charge of wines and the table [syn: {pantryman}]
