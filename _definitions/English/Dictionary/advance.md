---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advance
offline_file: ""
offline_thumbnail: ""
uuid: 5c002c73-9ea4-4177-912f-0a297f80ad84
updated: 1484310395
title: advance
categories:
    - Dictionary
---
advance
     adj 1: being ahead of time or need; "gave advance warning"; "was
            beforehand with her report" [syn: {advance(a)}, {beforehand(p)}]
     2: situated ahead or going before; "an advance party"; "at that
        time the most advanced outpost was still east of the
        Rockies" [syn: {advance(a)}, {advanced(a)}, {in advance(p)}]
     n 1: a movement forward; "he listened for the progress of the
          troops" [syn: {progress}, {progression}]
     2: a change for the better; progress in development [syn: {improvement},
         {betterment}]
     3: a tentative suggestion designed to elicit the reactions of
        others; "she rejected his advances" [syn: ...
