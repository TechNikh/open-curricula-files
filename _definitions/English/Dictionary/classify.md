---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/classify
offline_file: ""
offline_thumbnail: ""
uuid: 39efde91-428a-4927-81ea-918693669fa7
updated: 1484310200
title: classify
categories:
    - Dictionary
---
classify
     v 1: arrange or order by classes or categories; "How would you
          classify these pottery shards--are they prehistoric?"
          [syn: {class}, {sort}, {assort}, {sort out}, {separate}]
     2: declare unavailable, as for security reasons; "Classify
        these documents" [ant: {declassify}]
     3: assign to a class or kind; "How should algae be
        classified?"; "People argue about how to relegate certain
        mushrooms" [syn: {relegate}]
     [also: {classified}]
