---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sequent_calculus
offline_file: ""
offline_thumbnail: ""
uuid: b9612f0e-ee5c-48ec-893f-99f68e0680f4
updated: 1484309499
title: Sequent calculus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sequent_calculus_proof_tree_example.png
tags:
    - Introduction
    - Hilbert-style deduction systems
    - Natural deduction systems
    - Sequent calculus systems
    - Distinction between natural deduction and sequent calculus
    - Origin of word "sequent"
    - Proving logical formulas
    - Reduction trees
    - Cut rule
    - Relation to standard axiomatizations
    - The system LK
    - Inference rules
    - An intuitive explanation
    - Example derivations
    - Relation to analytic tableaux
    - Structural rules
    - Properties of the system LK
    - Variants
    - Minor structural alternatives
    - Absurdity
    - Substructural logics
    - 'Intuitionistic sequent calculus: System LJ'
    - Notes
categories:
    - Proof theory
---
Sequent calculus is, in essence, a style of formal logical argumentation where every line of a proof is a conditional tautology (called a sequent by Gerhard Gentzen) instead of an unconditional tautology. Each conditional tautology is inferred from other conditional tautologies on earlier lines in a formal argument according to rules and procedures of inference, giving a better approximation to the style of natural deduction used by mathematicians than David Hilbert's earlier style of formal logic where every line was an unconditional tautology. There may be more subtle distinctions to be made; for example, there may be non-logical axioms upon which all propositions are implicitly ...
