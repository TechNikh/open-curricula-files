---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/revealed
offline_file: ""
offline_thumbnail: ""
uuid: c8a8bc68-56e5-48c7-adc8-b07c69b0d7cf
updated: 1484310539
title: revealed
categories:
    - Dictionary
---
revealed
     adj : no longer concealed; uncovered as by opening a curtain;
           `discovered' is archaic and primarily a theater term;
           "the scene disclosed was of a moonlit forest" [syn: {discovered},
            {disclosed}]
