---
version: 1
type: video
provider: YouTube
id: IUMF8rTNXCk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7d0f9c3c-73cd-4954-a123-0769a78693b5
updated: 1486069613
title: Absolute value word problems
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/IUMF8rTNXCk/default.jpg
    - https://i3.ytimg.com/vi/IUMF8rTNXCk/1.jpg
    - https://i3.ytimg.com/vi/IUMF8rTNXCk/2.jpg
    - https://i3.ytimg.com/vi/IUMF8rTNXCk/3.jpg
---
