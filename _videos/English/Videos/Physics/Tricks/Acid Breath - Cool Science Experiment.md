---
version: 1
type: video
provider: YouTube
id: w9Zw0X4uJuM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d0c4186c-0988-4b77-bcb8-776891599ea4
updated: 1486069660
title: 'Acid Breath - Cool Science Experiment'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/w9Zw0X4uJuM/default.jpg
    - https://i3.ytimg.com/vi/w9Zw0X4uJuM/1.jpg
    - https://i3.ytimg.com/vi/w9Zw0X4uJuM/2.jpg
    - https://i3.ytimg.com/vi/w9Zw0X4uJuM/3.jpg
---
