---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/merge
offline_file: ""
offline_thumbnail: ""
uuid: 8c501d7f-aefa-44cc-b325-ba1a9b5a57a5
updated: 1484310192
title: merge
categories:
    - Dictionary
---
merge
     v 1: become one; "Germany unified officially in 1990"; "Will the
          two Koreas unify?" [syn: {unify}, {unite}] [ant: {disunify}]
     2: mix together different elements; "The colors blend well"
        [syn: {blend}, {flux}, {mix}, {conflate}, {commingle}, {immix},
         {fuse}, {coalesce}, {meld}, {combine}]
     3: join or combine; "We merged our resources" [syn: {unite}, {unify}]
