---
version: 1
type: video
provider: YouTube
id: ANl-GDoNNsA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fc775098-7f91-4b3b-b4d5-2eb7f0333867
updated: 1486069621
title: Using expression structure to solve quadratic
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/ANl-GDoNNsA/default.jpg
    - https://i3.ytimg.com/vi/ANl-GDoNNsA/1.jpg
    - https://i3.ytimg.com/vi/ANl-GDoNNsA/2.jpg
    - https://i3.ytimg.com/vi/ANl-GDoNNsA/3.jpg
---
