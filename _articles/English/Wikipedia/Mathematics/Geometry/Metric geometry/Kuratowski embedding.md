---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kuratowski_embedding
offline_file: ""
offline_thumbnail: ""
uuid: 2b4a3e87-d643-4ed6-a330-e01e63e9a954
updated: 1484309269
title: Kuratowski embedding
categories:
    - Metric geometry
---
Specifically, if (X,d) is a metric space, x0 is a point in X, and Cb(X) denotes the Banach space of all bounded continuous real-valued functions on X with the supremum norm, then the map
