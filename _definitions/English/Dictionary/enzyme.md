---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enzyme
offline_file: ""
offline_thumbnail: ""
uuid: 4e818cca-f7b5-4756-9dc2-9c9be57d9f8c
updated: 1484310339
title: enzyme
categories:
    - Dictionary
---
enzyme
     n : any of several complex proteins that are produced by cells
         and act as catalysts in specific biochemical reactions
