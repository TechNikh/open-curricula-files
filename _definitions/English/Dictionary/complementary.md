---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/complementary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484318341
title: complementary
categories:
    - Dictionary
---
complementary
     adj 1: acting as or providing a complement (something that
            completes the whole) [syn: {complemental}, {completing}]
     2: of words or propositions so related that each is the
        negation of the other; "`male' and `female' are
        complementary terms"
     3: of or relating to or suggestive of complementation;
        "interchangeable electric outlets" [syn: {interchangeable},
         {reciprocal}]
     n : either one of two chromatic colors that when mixed together
         give white (in the case of lights) or gray (in the case
         of pigments); "yellow and blue are complementaries" [syn:
          {complementary color}]
