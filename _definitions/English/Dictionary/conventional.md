---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conventional
offline_file: ""
offline_thumbnail: ""
uuid: 2a3149e1-5c5c-4c8d-8a3f-7ced7f6fd579
updated: 1484310234
title: conventional
categories:
    - Dictionary
---
conventional
     adj 1: following accepted customs and proprieties; "conventional
            wisdom"; "she had strayed from the path of
            conventional behavior"; "conventional forms of
            address" [ant: {unconventional}, {unconventional}]
     2: conforming with accepted standards; "a conventional view of
        the world" [syn: {established}]
     3: (weapons) using non-nuclear energy for propulsion or
        destruction; "conventional warfare"; "conventional
        weapons" [ant: {nuclear}]
     4: unimaginative and conformist; "conventional bourgeois
        lives"; "conventional attitudes" [ant: {unconventional}]
     5: represented in simplified or symbolic form ...
