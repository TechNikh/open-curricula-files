---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/popular
offline_file: ""
offline_thumbnail: ""
uuid: b40c7741-1a15-4d3d-bb6a-8c101661a127
updated: 1484310194
title: popular
categories:
    - Dictionary
---
popular
     adj 1: regarded with great favor, approval, or affection especially
            by the general public; "a popular tourist attraction";
            "a popular girl"; "cabbage patch dolls are no longer
            popular" [ant: {unpopular}]
     2: carried on by or for the people (or citizens) at large; "the
        popular vote"; "popular representation"; "institutions of
        popular government"
     3: representing or appealing to or adapted for the benefit of
        the people at large; "democratic art forms"; "a democratic
        or popular movement"; "popular thought"; "popular
        science"; "popular fiction" [syn: {democratic}]
     4: comprehensible to the ...
