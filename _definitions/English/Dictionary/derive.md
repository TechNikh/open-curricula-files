---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/derive
offline_file: ""
offline_thumbnail: ""
uuid: 396cfdb8-acbb-4448-9388-77f607828834
updated: 1484310271
title: derive
categories:
    - Dictionary
---
derive
     v 1: reason by deduction; establish by deduction [syn: {deduce},
          {infer}, {deduct}]
     2: obtain; "derive pleasure from one's garden" [syn: {gain}]
     3: come from; "The present name derives from an older form"
     4: develop or evolve, especially from a latent or potential
        state [syn: {educe}]
     5: come from; be connected by a relationship of blood, for
        example; "She was descended from an old Italian noble
        family"; "he comes from humble origins" [syn: {come}, {descend}]
