---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometric_separator
offline_file: ""
offline_thumbnail: ""
uuid: 10f08812-87ed-4fae-a00d-cab2fa67b4e7
updated: 1484309173
title: Geometric separator
tags:
    - Separators that are closed shapes
    - Proof
    - Application example
    - Generalizations
    - Optimality
    - Separators that are hyperplanes
    - Proof
    - Optimality
    - Generalizations
    - Algorithmic versions
    - >
        Separators that are width-bounded strips between parallel
        hyperplanes
    - Proof Sketch
    - Applications
    - Geometric separators and planar graph separators
    - Notes
categories:
    - Computational geometry
---
A geometric separator is a line (or other shape) that partitions a collection of geometric shapes into two subsets, such that proportion of shapes in each subset is bounded, and the number of shapes that do not belong to any subset (i.e. the shapes intersected by the separator itself) is small.
