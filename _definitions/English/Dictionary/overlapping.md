---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overlapping
offline_file: ""
offline_thumbnail: ""
uuid: 54b82de4-229c-4bb8-bde8-10faf55add0d
updated: 1484310407
title: overlapping
categories:
    - Dictionary
---
overlapping
     adj 1: related by having something in common with or coinciding
            with; "having overlapping duties"; "found unexpected
            overlapping areas of interest"
     2: laid overlapping (not flush) [syn: {lap-jointed}, {lap-strake},
         {lap-straked}, {lap-streak}, {lap-streaked}]
     n : covering with a design in which one element covers a part of
         another (as with tiles or shingles) [syn: {imbrication},
         {lapping}]
