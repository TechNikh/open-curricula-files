---
version: 1
type: video
provider: YouTube
id: ihNZlp7iUHE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/One-dimensional%20motion/Intro%20to%20vectors%20%26%20scalars.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 32c6904f-c8b2-4e53-b57f-7c297fee7266
updated: 1486069597
title: 'Intro to vectors & scalars'
categories:
    - One-dimensional motion
thumbnail_urls:
    - https://i3.ytimg.com/vi/ihNZlp7iUHE/default.jpg
    - https://i3.ytimg.com/vi/ihNZlp7iUHE/1.jpg
    - https://i3.ytimg.com/vi/ihNZlp7iUHE/2.jpg
    - https://i3.ytimg.com/vi/ihNZlp7iUHE/3.jpg
---
Distance, displacement, speed and velocity. Difference between vectors and scalars. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/one-dimensional-motion/displacement-velocity-time/v/calculating-average-velocity-or-speed?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/one-dimensional-motion/displacement-velocity-time/v/introduction-to-physics?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
