---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Checker_board
offline_file: ""
offline_thumbnail: ""
uuid: 560fa861-1411-446e-9ae6-43fa4d98ef63
updated: 1484308377
title: Checkerboard
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Checkerboard_pattern.svg.png
categories:
    - Uncategorized
---
A checkerboard or chequerboard (see spelling differences) is a board of chequered pattern on which English draughts (checkers) is played.[1] It consists of 64 squares (8×8) of alternating dark and light color, often black and white.
