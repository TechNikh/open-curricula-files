---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Japanese_theorem_for_cyclic_polygons
offline_file: ""
offline_thumbnail: ""
uuid: 5e0e63c8-77df-49b8-a21e-a9ed4d21919f
updated: 1484309116
title: Japanese theorem for cyclic polygons
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Japan.svg_0.png
tags:
    - Proof
    - Notes
categories:
    - Theorems in geometry
---
Conversely, if the sum of inradii independent from the triangulation, then the polygon is cyclic. The Japanese theorem follows from Carnot's theorem; it is a Sangaku problem.
