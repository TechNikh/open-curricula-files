---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Dubins%E2%80%93Spanier_theorems'
offline_file: ""
offline_thumbnail: ""
uuid: d91727f5-f30d-4370-bad0-331232b2de0e
updated: 1484308524
title: Dubins–Spanier theorems
tags:
    - Setting
    - Statements
    - Corollaries
    - Consensus partition
    - Super-proportional division
    - Utilitarian-optimal division
    - Leximin-optimal division
    - Further developments
categories:
    - Fair division
---
The Dubins–Spanier theorems are several theorems in the theory of fair cake-cutting. They were published by Lester Dubins and Edwin Spanier in 1961.[1] Although the original motivation for these theorems is fair division, they are in fact general theorems in measure theory.
