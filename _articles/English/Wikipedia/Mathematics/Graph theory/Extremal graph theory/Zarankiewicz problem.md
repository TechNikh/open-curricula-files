---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zarankiewicz_problem
offline_file: ""
offline_thumbnail: ""
uuid: ed9429f6-a072-4ce9-982f-0fa68006cdc5
updated: 1484309409
title: Zarankiewicz problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Zarankiewicz-4-3.svg.png
tags:
    - Problem statement
    - Examples
    - Upper bounds
    - Lower bounds
    - Non-bipartite graphs
    - Applications
categories:
    - Extremal graph theory
---
The Zarankiewicz problem, an unsolved problem in mathematics, asks for the largest possible number of edges in a bipartite graph that has a given number of vertices but has no complete bipartite subgraphs of a given size.[1] It belongs to the field of extremal graph theory, a branch of combinatorics, and is named after the Polish mathematician Kazimierz Zarankiewicz, who proposed several special cases of the problem in 1951.[2]
