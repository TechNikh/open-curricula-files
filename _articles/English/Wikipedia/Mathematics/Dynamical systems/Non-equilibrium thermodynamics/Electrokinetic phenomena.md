---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electrokinetic_phenomena
offline_file: ""
offline_thumbnail: ""
uuid: ab70f43c-8259-4957-8557-705ea3df84d0
updated: 1484308805
title: Electrokinetic phenomena
tags:
    - Family of electrokinetic phenomena
categories:
    - Non-equilibrium thermodynamics
---
Electrokinetic phenomena are a family of several different effects that occur in heterogeneous fluids, or in porous bodies filled with fluid, or in a fast flow over a flat surface. The term heterogeneous here means a fluid containing particles. Particles can be solid, liquid or gas bubbles with sizes on the scale of a micrometer or nanometer.[citation needed] There is a common source of all these effects — the so-called interfacial 'double layer' of charges. Influence of an external force on the diffuse layer generates tangential motion of a fluid with respect to an adjacent charged surface. This force might be electric, pressure gradient, concentration gradient, or gravity. In addition, ...
