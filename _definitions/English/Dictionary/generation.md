---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/generation
offline_file: ""
offline_thumbnail: ""
uuid: b7d26f0e-db17-41cf-8759-cdd6c34a834b
updated: 1484310307
title: generation
categories:
    - Dictionary
---
generation
     n 1: all the people living at the same time or of approximately
          the same age [syn: {coevals}, {contemporaries}]
     2: group of genetically related organisms constituting a single
        step in the line of descent
     3: the normal time between successive generations; "they had to
        wait a generation for that prejudice to fade"
     4: a stage of technological development or innovation; "the
        third generation of computers"
     5: a coming into being [syn: {genesis}]
     6: the production of heat or electricity; "dams were built for
        the generation of electricity"
     7: the act of producing offspring or multiplying by such
        ...
