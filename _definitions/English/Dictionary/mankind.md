---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mankind
offline_file: ""
offline_thumbnail: ""
uuid: 996dd339-6113-4760-aa61-0fca767ee400
updated: 1484310369
title: mankind
categories:
    - Dictionary
---
mankind
     n : all of the inhabitants of the earth; "all the world loves a
         lover"; "she always used `humankind' because `mankind'
         seemed to slight the women" [syn: {world}, {human race},
         {humanity}, {humankind}, {human beings}, {humans}, {man}]
