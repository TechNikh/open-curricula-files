---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Entropy_in_thermodynamics_and_information_theory
offline_file: ""
offline_thumbnail: ""
uuid: ce0532bf-cb3f-4a6c-95fe-d14d5dc8cc96
updated: 1484308770
title: Entropy in thermodynamics and information theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Zentralfriedhof_Vienna_-_Boltzmann.JPG
tags:
    - Equivalence of form of the defining expressions
    - Theoretical relationship
    - Information is physical
    - "Szilard's engine"
    - "Landauer's principle"
    - Negentropy
    - Black holes
    - Quantum theory
    - The fluctuation theorem
    - Criticism
    - Topics of recent research
    - Is information quantized?
    - >
        Extracting work from quantum information in a Szilárd
        engine
    - Additional references
categories:
    - Entropy and information
---
There are close parallels between the mathematical expressions for the thermodynamic entropy, usually denoted by S, of a physical system in the statistical thermodynamics established by Ludwig Boltzmann and J. Willard Gibbs in the 1870s, and the information-theoretic entropy, usually expressed as H, of Claude Shannon and Ralph Hartley developed in the 1940s. Shannon, although not initially aware of this similarity, commented on it upon publicizing information theory in A Mathematical Theory of Communication.
