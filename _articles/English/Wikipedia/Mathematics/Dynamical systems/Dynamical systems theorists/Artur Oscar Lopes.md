---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Artur_Oscar_Lopes
offline_file: ""
offline_thumbnail: ""
uuid: 74ecd327-3aba-42f2-8fb8-81b6a1e1abba
updated: 1484308690
title: Artur Oscar Lopes
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_2.jpg
categories:
    - Dynamical systems theorists
---
Artur Oscar Lopes (born on 17 October 1950 in the city of Rio de Janeiro) is a Brazilian mathematician working on dynamical systems and ergodic theory.[1] He is a professor at UFRGS, Porto Alegre.[2]
