---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adiabatic_wall
offline_file: ""
offline_thumbnail: ""
uuid: 261068ac-83a7-4a94-a53c-262b13fb34a4
updated: 1484308843
title: Adiabatic wall
tags:
    - Construction of the concept of an adiabatic enclosure
    - Definitions of transfer of heat
    - Thermodynamic stream of thinking
    - Mechanical stream of thinking
    - Accounts of the adiabatic wall
    - Bibliography
categories:
    - Thermodynamics
---
In theoretical investigations, it is sometimes assumed that one of the two systems is the surroundings of the other. Then it is assumed that the work transferred is reversible within the surroundings, but in thermodynamics it is not assumed that the work transferred is reversible within the system. The assumption of reversibility in the surroundings has the consequence that the quantity of work transferred is well defined by macroscopic variables in the surroundings. Accordingly, the surroundings are sometimes said to have a reversible work reservoir.
