---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/barber
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484500201
title: barber
categories:
    - Dictionary
---
Barber
     n 1: United States composer (1910-1981) [syn: {Samuel Barber}]
     2: a hairdresser who cuts hair and shaves beards as a trade
     v : perform the services of a barber: cut the hair and/or beard
         of
