---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edge_dominating_set
offline_file: ""
offline_thumbnail: ""
uuid: d8862a6a-4d57-4975-adf5-cde3449dd167
updated: 1484309389
title: Edge dominating set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/140px-Edge-dominating-set.svg.png
tags:
    - Properties
    - Algorithms and computational complexity
categories:
    - Computational problems in graph theory
---
In graph theory, an edge dominating set for a graph G = (V, E) is a subset D ⊆ E such that every edge not in D is adjacent to at least one edge in D. An edge dominating set is also known as a line dominating set. Figures (a)–(d) are examples of edge dominating sets (thick red lines).
