---
version: 1
type: video
provider: YouTube
id: PdACg8uhPps
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1a1462f3-691f-4b51-9280-2146b9556bc7
updated: 1486069640
title: Projection Optics Experiment 1
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/PdACg8uhPps/default.jpg
    - https://i3.ytimg.com/vi/PdACg8uhPps/1.jpg
    - https://i3.ytimg.com/vi/PdACg8uhPps/2.jpg
    - https://i3.ytimg.com/vi/PdACg8uhPps/3.jpg
---
