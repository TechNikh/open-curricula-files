---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conformation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484465101
title: conformation
categories:
    - Dictionary
---
conformation
     n 1: a symmetrical arrangement of the parts of a thing
     2: any spatial attributes (especially as defined by outline);
        "he could barely make out their shapes through the smoke"
        [syn: {shape}, {form}, {configuration}, {contour}]
     3: acting according to certain accepted standards [syn: {conformity},
         {compliance}, {abidance}] [ant: {disobedience}, {nonconformity}]
