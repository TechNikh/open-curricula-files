---
version: 1
type: video
provider: YouTube
id: n6wpNhyreDE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/The%20chemistry%20of%20cookies%20-%20Stephanie%20Warren.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d20b7083-551c-4575-a13b-ea98fc333618
updated: 1486069662
title: 'The chemistry of cookies - Stephanie Warren'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/n6wpNhyreDE/default.jpg
    - https://i3.ytimg.com/vi/n6wpNhyreDE/1.jpg
    - https://i3.ytimg.com/vi/n6wpNhyreDE/2.jpg
    - https://i3.ytimg.com/vi/n6wpNhyreDE/3.jpg
---
