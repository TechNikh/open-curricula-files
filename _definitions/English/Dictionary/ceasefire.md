---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ceasefire
offline_file: ""
offline_thumbnail: ""
uuid: f2dc1c36-9c5b-46ec-befb-9d04540fd236
updated: 1484310178
title: ceasefire
categories:
    - Dictionary
---
cease-fire
     n : a state of peace agreed to between opponents so they can
         discuss peace terms [syn: {armistice}, {truce}]
