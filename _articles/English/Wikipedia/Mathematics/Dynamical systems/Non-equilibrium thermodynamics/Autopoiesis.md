---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Autopoiesis
offline_file: ""
offline_thumbnail: ""
uuid: ebc7571e-bd15-4830-9076-4de9a1840a47
updated: 1484308811
title: Autopoiesis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-3D-SIM-4_Anaphase_3_color.jpg
tags:
    - Meaning
    - Relation to complexity
    - Relation to cognition
    - Relation to consciousness
    - Criticism
    - Notes and references
categories:
    - Non-equilibrium thermodynamics
---
The term "autopoiesis" (from Greek αὐτo- (auto-), meaning "self", and ποίησις (poiesis), meaning "creation, production") refers to a system capable of reproducing and maintaining itself. The term was introduced in 1972 by Chilean biologists Humberto Maturana and Francisco Varela to define the self-maintaining chemistry of living cells. Since then the concept has been also applied to the fields of systems theory and sociology.
