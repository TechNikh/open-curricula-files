---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conventionally
offline_file: ""
offline_thumbnail: ""
uuid: a9191ad9-774e-4e02-9a90-e518683867ce
updated: 1484310198
title: conventionally
categories:
    - Dictionary
---
conventionally
     adv : in a conventional manner; "he usually behaves rather
           conventionally" [ant: {unconventionally}]
