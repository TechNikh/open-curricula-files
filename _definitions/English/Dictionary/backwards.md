---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/backwards
offline_file: ""
offline_thumbnail: ""
uuid: 34d486f1-671d-40ce-860b-cb6f2234fee9
updated: 1484310277
title: backwards
categories:
    - Dictionary
---
backwards
     adv 1: at or to or toward the back or rear; "he moved back";
            "tripped when he stepped backward"; "she looked
            rearward out the window of the car" [syn: {back}, {backward},
             {rearward}, {rearwards}] [ant: {forward}]
     2: in a manner or order or direction the reverse of normal;
        "it's easy to get the `i' and the `e' backward in words
        like `seize' and `siege'"; "the child put her jersey on
        backward" [syn: {backward}]
