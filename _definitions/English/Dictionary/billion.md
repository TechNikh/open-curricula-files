---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/billion
offline_file: ""
offline_thumbnail: ""
uuid: f55bea3d-3853-4566-b1ae-33edfed3e0a8
updated: 1484310363
title: billion
categories:
    - Dictionary
---
billion
     adj 1: denoting a quantity consisting of one thousand million items
            or units in the United States [syn: {a billion}]
     2: denoting a quantity consisting of one million million items
        or units in Great Britain [syn: {a billion}]
     n 1: the number that is represented as a one followed by 12
          zeros; in the United Kingdom the usage followed in the
          United States is frequently seen [syn: {one million
          million}, {1000000000000}]
     2: the number that is represented as a one followed by 9 zeros
        [syn: {one thousand million}, {1000000000}]
