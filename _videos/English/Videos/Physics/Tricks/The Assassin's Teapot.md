---
version: 1
type: video
provider: YouTube
id: kkrgUT70Mbo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: cfa29db0-90d7-4175-b5b7-cc10fce06c84
updated: 1486069660
title: "The Assassin's Teapot"
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/kkrgUT70Mbo/default.jpg
    - https://i3.ytimg.com/vi/kkrgUT70Mbo/1.jpg
    - https://i3.ytimg.com/vi/kkrgUT70Mbo/2.jpg
    - https://i3.ytimg.com/vi/kkrgUT70Mbo/3.jpg
---
