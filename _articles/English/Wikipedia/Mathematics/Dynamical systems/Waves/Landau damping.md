---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Landau_damping
offline_file: ""
offline_thumbnail: ""
uuid: e80b06f3-1258-4307-a12e-38a37f157204
updated: 1484308887
title: Landau damping
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Maxwell_dist_ress_partic_landau.svg.png
tags:
    - 'Wave-particle interactions[6]'
    - Physical interpretation
    - 'Theoretical physics: perturbation theory'
    - 'Mathematical theory: the Cauchy problem for perturbative solutions'
    - Notes and references
categories:
    - Waves
---
In physics, Landau damping, named after its discoverer,[1] the eminent Soviet physicist Lev Landau (1908-68), is the effect of damping (exponential decrease as a function of time) of longitudinal space charge waves in plasma or a similar environment.[2] This phenomenon prevents an instability from developing, and creates a region of stability in the parameter space. It was later argued by Donald Lynden-Bell that a similar phenomenon was occurring in galactic dynamics,[3] where the gas of electrons interacting by electrostatic forces is replaced by a "gas of stars" interacting by gravitation forces.[4] Landau damping can be manipulated exactly in numerical simulations such as ...
