---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uniqueness
offline_file: ""
offline_thumbnail: ""
uuid: fd9d9a93-474c-419c-bd0d-37d318dc4b97
updated: 1484310142
title: uniqueness
categories:
    - Dictionary
---
uniqueness
     n : the quality of being one of a kind; "that singularity
         distinguished him from all his companions" [syn: {singularity}]
