---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agency
offline_file: ""
offline_thumbnail: ""
uuid: d04bf432-3cef-4be4-93c1-01b6f85d2e2e
updated: 1484310533
title: agency
categories:
    - Dictionary
---
agency
     n 1: an administrative unit of government; "the Central
          Intelligence Agency"; "the Census Bureau"; "Office of
          Management and Budget"; "Tennessee Valley Authority"
          [syn: {federal agency}, {government agency}, {bureau}, {office},
           {authority}]
     2: a business that serves other businesses
     3: the state of being in action or exerting power; "the agency
        of providence"; "she has free agency"
     4: the state of serving as an official and authorized delegate
        or agent [syn: {representation}, {delegacy}]
     5: how a result is obtained or an end is achieved; "a means of
        control"; "an example is the best agency of ...
