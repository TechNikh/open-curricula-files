---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/organism
offline_file: ""
offline_thumbnail: ""
uuid: 1fcca45d-fd37-41ca-8472-e773d1722f7e
updated: 1484310313
title: organism
categories:
    - Dictionary
---
organism
     n 1: a living thing that has (or can develop) the ability to act
          or function independently [syn: {being}]
     2: a system considered analogous in structure or function to a
        living body; "the social organism"
