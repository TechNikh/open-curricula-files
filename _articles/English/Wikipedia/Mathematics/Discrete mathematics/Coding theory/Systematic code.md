---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Systematic_code
offline_file: ""
offline_thumbnail: ""
uuid: 7bb1f0a5-d946-4ac5-8055-ed57acb2c724
updated: 1484308563
title: Systematic code
tags:
    - Properties
    - Examples
    - Notes
categories:
    - Coding theory
---
In coding theory, a systematic code is any error-correcting code in which the input data is embedded in the encoded output. Conversely, in a non-systematic code the output does not contain the input symbols.
