---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spite
offline_file: ""
offline_thumbnail: ""
uuid: be07a63d-ac09-410e-bb42-52f2374a507f
updated: 1484308517
title: Spite
tags:
    - In game theory
    - In industrial relations
    - In behavioral ecology
categories:
    - Fair division
---
In fair division problems, spite is a phenomenon that occurs when a player's value of an allocation decreases when one or more other players' valuation increases. Thus, other things being equal, a player exhibiting spite will prefer an allocation in which other players receive less than more (if more of the good is desirable).
