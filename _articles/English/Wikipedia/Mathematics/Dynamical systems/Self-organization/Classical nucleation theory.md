---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Classical_nucleation_theory
offline_file: ""
offline_thumbnail: ""
uuid: 442522b2-d750-4178-9398-7e92487fbd49
updated: 1484308819
title: Classical nucleation theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Plot_of_classical_nucleation_theory_prediction_for_the_free_energy_of_a_nucleus_as_a_function_of_radius.png
tags:
    - Outline of classical nucleation theory
    - Homogeneous nucleation
    - Heterogeneous nucleation
    - Comparison between CNT and computer simulation
categories:
    - Self-organization
---
Nucleation is the first step in the formation of either a new thermodynamic phase or a new structure via self-assembly or self-organisation. Nucleation is typically defined to be the process that determines how long we have to wait before the new phase or self-organised structure appears. Classical nucleation theory (CNT) is the most common theoretical model used to understand why nucleation may take hours or years, or in effect never happen.[1][2][3]
