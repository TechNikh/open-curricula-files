---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dust_devil
offline_file: ""
offline_thumbnail: ""
uuid: 47f96f0b-2584-450f-9887-af6574705c09
updated: 1484308874
title: Dust devil
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dust_devil.jpg
tags:
    - Names
    - Formation
    - Intensity and duration
    - Electrical activities
    - Martian dust devils
    - Related phenomena
categories:
    - Vortices
---
A dust devil is a strong, well-formed, and relatively long-lived whirlwind, ranging from small (half a meter wide and a few meters tall) to large (more than 10 meters wide and more than 1000 meters tall). The primary vertical motion is upward. Dust devils are usually harmless, but can on rare occasions grow large enough to pose a threat to both people and property.[1]
