---
version: 1
type: video
provider: YouTube
id: PqNjq05plIE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f564e3bc-a6e9-4d90-a49a-b9f436160c51
updated: 1486069603
title: యాంత్రిక బలం (part 3)
tags:
    - telugu
    - Physics
    - Mechanics
    - mechanical advantage
categories:
    - భౌతిక శాస్త్రం
thumbnail_urls:
    - https://i3.ytimg.com/vi/PqNjq05plIE/default.jpg
    - https://i3.ytimg.com/vi/PqNjq05plIE/1.jpg
    - https://i3.ytimg.com/vi/PqNjq05plIE/2.jpg
    - https://i3.ytimg.com/vi/PqNjq05plIE/3.jpg
---
Translated from original video
Title - Mechanical Advantage ( part 3)
URL -http://www.khanacademy.org/science/physics/mechanics/v/mechanical-advantage--part-3
