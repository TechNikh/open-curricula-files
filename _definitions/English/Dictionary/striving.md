---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/striving
offline_file: ""
offline_thumbnail: ""
uuid: 3e8c0f6d-e185-40bd-a3bb-ea0264d39f63
updated: 1484310571
title: striving
categories:
    - Dictionary
---
striving
     n : an effortful attempt to attain a goal [syn: {nisus}, {pains},
          {strain}]
