---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/around
offline_file: ""
offline_thumbnail: ""
uuid: 15ae15fb-385c-4d04-88f0-e9d4ddcbaa17
updated: 1484310341
title: around
categories:
    - Dictionary
---
around
     adv 1: in the area or vicinity; "a few spectators standing about";
            "hanging around"; "waited around for the next flight"
            [syn: {about}]
     2: by a circular or circuitous route; "He came all the way
        around the base"; "the road goes around the pond"
     3: to or among many different places or in no particular
        direction; "wandering about with no place to go"; "people
        were rushing about"; "news gets around (or about)";
        "traveled around in Asia"; "he needs advice from someone
        who's been around"; "she sleeps around" [syn: {about}]
     4: in a circle or circular motion; "The wheels are spinning
        around"
     5: ...
