---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vouch
offline_file: ""
offline_thumbnail: ""
uuid: 65d2aa8c-bd99-45ee-9c9a-594d295bd1d9
updated: 1484310561
title: vouch
categories:
    - Dictionary
---
vouch
     v 1: give personal assurance; guarantee; "Will he vouch for me?"
     2: give surety or assume responsibility; "I vouch for the
        quality of my products" [syn: {guarantee}]
     3: summon (a vouchee) into court to warrant or defend a title
     4: give supporting evidence; "He vouched his words by his
        deeds"
