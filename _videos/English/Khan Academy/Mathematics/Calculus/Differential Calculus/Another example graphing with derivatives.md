---
version: 1
type: video
provider: YouTube
id: zC_dTaEY2AY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Another%20example%20graphing%20with%20derivatives.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a2d358b6-e380-432d-8f56-ae16d6b66073
updated: 1486069692
title: Another example graphing with derivatives
categories:
    - Differential Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/zC_dTaEY2AY/default.jpg
    - https://i3.ytimg.com/vi/zC_dTaEY2AY/1.jpg
    - https://i3.ytimg.com/vi/zC_dTaEY2AY/2.jpg
    - https://i3.ytimg.com/vi/zC_dTaEY2AY/3.jpg
---
