---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rampur
offline_file: ""
offline_thumbnail: ""
uuid: d46ed016-8991-44e3-8e6a-b4f52f15ab3e
updated: 1484308456
title: Rampur
tags:
    - Places
    - Bangladesh
    - India
    - Gujarat
    - Karnataka
    - Maharashtra
    - Uttar Pradesh
    - Other
    - Nepal
    - Pakistan
    - Animals
categories:
    - Uncategorized
---
The name Rampur ("City of Rama"), together with the variations Ramapur, Ramapura may refer to:
