---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/populate
offline_file: ""
offline_thumbnail: ""
uuid: 60436ef6-9cff-4b9a-9e8f-36aa099180e7
updated: 1484310246
title: populate
categories:
    - Dictionary
---
populate
     v 1: make one's home or live in; "She resides officially in
          Iceland"; "I live in a 200-year old house"; "These
          people inhabited all the islands that are now deserted";
          "The plains are sparsely populated" [syn: {dwell}, {shack},
           {reside}, {live}, {inhabit}, {people}, {domicile}, {domiciliate}]
     2: fill with people or supply with inhabitants; "people a
        room"; "The government wanted to populate the remote area
        of the country" [syn: {people}]
