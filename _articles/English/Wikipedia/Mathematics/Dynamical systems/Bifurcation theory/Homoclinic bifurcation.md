---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homoclinic_bifurcation
offline_file: ""
offline_thumbnail: ""
uuid: a0f632bf-15f6-4dd4-a0fc-ff13cb49cf14
updated: 1484308696
title: Homoclinic bifurcation
categories:
    - Bifurcation theory
---
The image below shows a phase portrait before, at, and after a homoclinic bifurcation in 2D. The periodic orbit grows until it collides with the saddle point. At the bifurcation point the period of the periodic orbit has grown to infinity and it has become a homoclinic orbit. After the bifurcation there is no longer a periodic orbit.
