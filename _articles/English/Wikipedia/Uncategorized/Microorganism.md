---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Micro-organisms
offline_file: ""
offline_thumbnail: ""
uuid: b2a78970-2e54-4665-bec2-b4b527a87164
updated: 1484308471
title: Microorganism
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-E_coli_at_10000x%252C_original.jpg'
tags:
    - Evolution
    - Pre-microbiology
    - History of discovery
    - Classification and structure
    - Prokaryotes
    - Bacteria
    - Archaea
    - Eukaryotes
    - Protists
    - Animals
categories:
    - Uncategorized
---
A microorganism or microbe is a microscopic living organism, which may be single-celled[1] or multicellular. The study of microorganisms is called microbiology, a subject that began with the discovery of microorganisms in 1674 by Antonie van Leeuwenhoek, using a microscope of his own design.
