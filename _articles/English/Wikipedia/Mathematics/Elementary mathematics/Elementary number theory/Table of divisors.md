---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Table_of_divisors
offline_file: ""
offline_thumbnail: ""
uuid: 4d5dd73a-70ef-4872-b729-efd738264dab
updated: 1484308941
title: Table of divisors
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Highly_composite_numbers.svg.png
tags:
    - Key to the tables
    - 1 to 100
    - 101 to 200
    - 201 to 300
    - 301 to 400
    - 401 to 500
    - 501 to 600
    - 601 to 700
    - 701 to 800
    - 801 to 900
    - 901 to 1000
categories:
    - Elementary number theory
---
A divisor of an integer n is an integer m, for which n/m is again an integer (which is necessarily also a divisor of n). For example, 3 is a divisor of 21, since 21/7 = 3 (and 7 is also a divisor of 21).
