---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radio
offline_file: ""
offline_thumbnail: ""
uuid: d68cebce-056d-4bc4-93b5-db0cea05a4aa
updated: 1484310194
title: radio
categories:
    - Dictionary
---
radio
     adj : indicating radiation or radioactivity; "radiochemistry"
     n 1: medium for communication [syn: {radiocommunication}, {wireless}]
     2: an electronic receiver that detects and demodulates and
        amplifies transmitted signals [syn: {radio receiver}, {receiving
        set}, {radio set}, {tuner}, {wireless}]
     3: a communication system based on broadcasting electromagnetic
        waves [syn: {wireless}]
     v : transmit messages via radio waves; "he radioed for help"
