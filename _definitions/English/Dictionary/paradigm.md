---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paradigm
offline_file: ""
offline_thumbnail: ""
uuid: eb07c879-fc3e-4a1d-8d47-8efe10b08fcb
updated: 1484310168
title: paradigm
categories:
    - Dictionary
---
paradigm
     n 1: systematic arrangement of all the inflected forms of a word
     2: a standard or typical example; "he is the prototype of good
        breeding"; "he provided America with an image of the good
        father" [syn: {prototype}, {epitome}, {image}]
     3: the class of all items that can be substituted into the same
        position (or slot) in a grammatical sentence (are in
        paradigmatic relation with one another) [syn: {substitution
        class}]
     4: the generally accepted perspective of a particular
        discipline at a given time; "he framed the problem within
        the psychoanalytic paradigm"
