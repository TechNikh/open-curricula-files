---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Constraint_counting
offline_file: ""
offline_thumbnail: ""
uuid: 16654627-0ddf-4a8c-bdc7-3111c2444360
updated: 1484308572
title: Constraint counting
tags:
    - Partial differential equations
    - Linear equations
    - General solution of initial value problem
    - Quasilinear equations
categories:
    - Combinatorics
---
In mathematics, constraint counting is counting the number of constraints in order to compare it with the number of variables, parameters, etc. that are free to be determined, the idea being that in most cases the number of independent choices that can be made is the excess of the latter over the former.
