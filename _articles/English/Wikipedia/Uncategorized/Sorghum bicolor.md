---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jowar
offline_file: ""
offline_thumbnail: ""
uuid: c99c3775-f386-4906-84cf-6b6f579d07bd
updated: 1484308432
title: Sorghum bicolor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sorghum_head_in_India.jpg
tags:
    - Cultivation
    - Uses
    - Agricultural uses
    - Research
    - Genome
    - Parasite
categories:
    - Uncategorized
---
Sorghum bicolor, commonly called sorghum[2] (/ˈsɔːrɡəm/) and also known as great millet,[3] durra, jowari, or milo, is a grass species cultivated for its grain, which is used for food, both for animals and humans, and for ethanol production. Sorghum originated in northern Africa, and is now cultivated widely in tropical and subtropical regions.[4] Sorghum is the world's fifth most important cereal crop after rice, wheat, maize and barley. S. bicolor is typically an annual, but some cultivars are perennial. It grows in clumps that may reach over 4 m high. The grain is small, ranging from 2 to 4 mm in diameter. Sweet sorghums are sorghum cultivars that are primarily grown for foliage, ...
