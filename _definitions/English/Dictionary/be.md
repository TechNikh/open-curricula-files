---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/be
offline_file: ""
offline_thumbnail: ""
uuid: 88feb899-f5ff-4672-9c04-45bc1979e79b
updated: 1484310361
title: be
categories:
    - Dictionary
---
Be
     n : a light strong brittle gray toxic bivalent metallic element
         [syn: {beryllium}, {glucinium}, {atomic number 4}]
     v 1: have the quality of being; (copula, used with an adjective
          or a predicate noun); "John is rich"; "This is not a
          good answer"
     2: be identical to; be someone or something; "The president of
        the company is John Smith"; "This is my house"
     3: occupy a certain position or area; be somewhere; "Where is
        my umbrella?" "The toolshed is in the back"; "What is
        behind this behavior?"
     4: have an existence, be extant; "Is there a God?" [syn: {exist}]
     5: happen, occur, take place; "I lost my wallet; this ...
