---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uphill
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484588101
title: uphill
categories:
    - Dictionary
---
uphill
     adj 1: sloping upward [syn: {acclivitous}, {rising}]
     2: hard to overcome or surmount; "a stiff hike"; "a stiff
        exam"; "an uphill battle against a popular incumbant"
        [syn: {stiff}]
     n : the upward slope of a hill
     adv 1: against difficulties; "she was talking uphill"
     2: upward on a hill or incline; "this street lay uphill"
