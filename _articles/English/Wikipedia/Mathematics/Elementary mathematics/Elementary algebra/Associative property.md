---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Associative_property
offline_file: ""
offline_thumbnail: ""
uuid: 5f733bc9-94d1-424a-92cd-554ca19e70c7
updated: 1484308905
title: Associative property
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Semigroup_associative.svg.png
tags:
    - Definition
    - Generalized associative law
    - Examples
    - Propositional logic
    - Rule of replacement
    - Truth functional connectives
    - Non-associativity
    - Nonassociativity of floating point calculation
    - Notation for non-associative operations
categories:
    - Elementary algebra
---
In mathematics, the associative property[1] is a property of some binary operations. In propositional logic, associativity is a valid rule of replacement for expressions in logical proofs.
