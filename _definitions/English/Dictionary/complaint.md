---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/complaint
offline_file: ""
offline_thumbnail: ""
uuid: 660b9729-397e-46a6-88d8-75e41e9248db
updated: 1484310518
title: complaint
categories:
    - Dictionary
---
complaint
     n 1: an often persistent bodily disorder or disease; a cause for
          complaining [syn: {ailment}, {ill}]
     2: (formerly) a loud cry (or repeated cries) of pain or rage or
        sorrow
     3: an expression of grievance or resentment
     4: (civil law) the first pleading of the plaintiff setting out
        the facts on which the claim for relief is based
     5: (criminal law) a pleading describing some wrong or offense;
        "he was arrested on a charge of larceny" [syn: {charge}]
