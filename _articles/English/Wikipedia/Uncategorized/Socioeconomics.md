---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Socio-economic
offline_file: ""
offline_thumbnail: ""
uuid: 40c6c4f2-9782-407d-8df8-2b9462bafdd5
updated: 1484308335
title: Socioeconomics
tags:
    - Overview
    - Notes
categories:
    - Uncategorized
---
Socioeconomics (also known as social economics) is the social science that studies how economic activity affects and is shaped by social processes. In general it analyzes how societies progress, stagnate, or regress because of their local or regional economy, or the global economy.
