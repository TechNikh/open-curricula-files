---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/specified
offline_file: ""
offline_thumbnail: ""
uuid: 7a9ab4ba-fbf8-4e32-bae7-e0b63c69da0a
updated: 1484310204
title: specified
categories:
    - Dictionary
---
specified
     adj : clearly and explicitly stated; "meals are at specified
           times" [ant: {unspecified}]
