---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bird%E2%80%93Meertens_formalism'
offline_file: ""
offline_thumbnail: ""
uuid: 36a97394-5da9-4e52-a8d6-1a6faa2b658b
updated: 1484308667
title: Bird–Meertens formalism
tags:
    - Basic examples and notations
    - >
        The homomorphism lemma and its applications to parallel
        implementations
categories:
    - Theoretical computer science
---
The Bird–Meertens formalism is a calculus for deriving programs from specifications (in a functional-programming setting) by a process of equational reasoning. It was devised by Richard Bird and Lambert Meertens as part of their work within IFIP Working Group 2.1.
