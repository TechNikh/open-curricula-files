---
version: 1
type: video
provider: YouTube
id: rAof9Ld5sOg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Differential%20Calculus/Taking%20derivatives/Calculus-%20Derivatives%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d3e7427d-58ea-428a-955b-249dabeeeefc
updated: 1486069693
title: 'Calculus- Derivatives 1'
categories:
    - Taking derivatives
thumbnail_urls:
    - https://i3.ytimg.com/vi/rAof9Ld5sOg/default.jpg
    - https://i3.ytimg.com/vi/rAof9Ld5sOg/1.jpg
    - https://i3.ytimg.com/vi/rAof9Ld5sOg/2.jpg
    - https://i3.ytimg.com/vi/rAof9Ld5sOg/3.jpg
---
