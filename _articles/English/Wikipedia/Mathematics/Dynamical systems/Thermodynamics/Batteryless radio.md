---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Batteryless_radio
offline_file: ""
offline_thumbnail: ""
uuid: d5b6edae-823f-4b04-9f27-e0e9e4d065b8
updated: 1484308838
title: Batteryless radio
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Clockwork_Radio.JPG
tags:
    - History
    - Carrier-powered radio
categories:
    - Thermodynamics
---
Originally this referred to units which could be used directly by AC mains supply (mains radio); it can also refer to units which do not require a power source at all, except for the power that they receive from radio waves.
