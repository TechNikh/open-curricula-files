---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jacobi_point
offline_file: ""
offline_thumbnail: ""
uuid: 0ca0ea80-1bb5-45cd-bef0-6e33435ffd52
updated: 1484309365
title: Jacobi point
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-GeneralisationOfNapoleonPoint.svg.png
categories:
    - Geometry stubs
---
A Jacobi point is a point in the Euclidean plane determined by a triangle ABC together with a triple of angles α, β, and γ. This information is sufficient to determine three points P, Q, and R such that ∠RAB = ∠QAC = α, ∠PBC = ∠RBA = β, and ∠QCA = ∠PCB = γ. Then, by a theorem of Karl Friedrich Andreas Jacobi (de), the lines AP, BQ, and CR are concurrent,[1][2][3] at a point N called the Jacobi point.[3]
