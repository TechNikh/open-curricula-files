---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sub
offline_file: ""
offline_thumbnail: ""
uuid: fa7d250d-7e1c-44e8-9522-81fcc0deafe5
updated: 1484310391
title: sub
categories:
    - Dictionary
---
sub
     n 1: a large sandwich made of a long crusty roll split lengthwise
          and filled with meats and cheese (and tomato and onion
          and lettuce and condiments); different names are used in
          different sections of the United States [syn: {bomber},
          {grinder}, {hero}, {hero sandwich}, {hoagie}, {hoagy}, {Cuban
          sandwich}, {Italian sandwich}, {poor boy}, {submarine},
          {submarine sandwich}, {torpedo}, {wedge}, {zep}]
     2: a submersible warship usually armed with torpedoes [syn: {submarine},
         {pigboat}, {U-boat}]
     v : be a substitute; "The young teacher had to substitute for
         the sick colleague"; "The skim milk ...
