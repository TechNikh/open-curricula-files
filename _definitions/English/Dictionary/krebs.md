---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/krebs
offline_file: ""
offline_thumbnail: ""
uuid: f14d4aba-6b52-4c9c-92b8-0414f54702a6
updated: 1484310158
title: krebs
categories:
    - Dictionary
---
Krebs
     n : English biochemist (born in Germany) who discovered the
         Krebs cycle (1900-1981) [syn: {Hans Adolf Krebs}, {Sir
         Hans Adolf Krebs}]
