---
version: 1
type: video
provider: YouTube
id: X9uTUs9Wm7g
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Galilean%20Relativity%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 371155f7-6ebe-44c8-b7fb-983a27427d14
updated: 1486069651
title: Galilean Relativity 2
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/X9uTUs9Wm7g/default.jpg
    - https://i3.ytimg.com/vi/X9uTUs9Wm7g/1.jpg
    - https://i3.ytimg.com/vi/X9uTUs9Wm7g/2.jpg
    - https://i3.ytimg.com/vi/X9uTUs9Wm7g/3.jpg
---
