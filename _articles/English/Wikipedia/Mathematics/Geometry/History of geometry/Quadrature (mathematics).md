---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quadrature_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 3ced402c-2665-4a66-b9b5-94d461f9dc1f
updated: 1484309247
title: Quadrature (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Parabola_and_inscribed_triangle.svg.png
tags:
    - History
    - Notes
categories:
    - History of geometry
---
In mathematics, quadrature is a historical term which means determining area. Quadrature problems served as one of the main sources of problems in the development of calculus, and introduce important topics in mathematical analysis.
