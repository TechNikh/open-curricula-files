---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Atriphtaloid
offline_file: ""
offline_thumbnail: ""
uuid: 4127b5a7-8514-4d06-acfa-4333f9a3cc63
updated: 1484309354
title: Atriphtaloid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Atriph05.svg.png
categories:
    - Geometry stubs
---
where a and b are positive numbers.
