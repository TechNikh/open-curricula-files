---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/User:Johannes_nordstrom/sandbox
offline_file: ""
offline_thumbnail: ""
uuid: e2fb52c4-827f-4c8d-8a31-c32f5fafce60
updated: 1484309156
title: User:Johannes nordstrom/sandbox
tags:
    - Definition
    - The Laplacians on Kähler manifolds
    - Calibrations
    - Local coordinate expression
    - Kähler classes
    - Kähler potentials
    - Ricci tensor and Kähler manifolds
    - Riemannian holonomy
    - Examples
categories:
    - Algebraic geometry
---
In mathematics and especially differential geometry, a Kähler manifold is a manifold with three mutually compatible structures; a complex structure, a Riemannian structure, and a symplectic structure.
