---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/washer
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484426821
title: washer
categories:
    - Dictionary
---
washer
     n 1: someone who washes things for a living
     2: seal consisting of a flat disk placed to prevent leakage
     3: a home appliance for washing clothes and linens
        automatically [syn: {automatic washer}, {washing machine}]
