---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/precisely
offline_file: ""
offline_thumbnail: ""
uuid: 710ac109-68fb-4410-be98-1821d58a1fd7
updated: 1484310559
title: precisely
categories:
    - Dictionary
---
precisely
     adv 1: indicating exactness or preciseness; "he was doing precisely
            (or exactly) what she had told him to do"; "it was
            just as he said--the jewel was gone"; "it has just
            enough salt" [syn: {exactly}, {just}]
     2: in a precise manner; "she always expressed herself
        precisely" [syn: {incisively}, {exactly}] [ant: {imprecisely},
         {imprecisely}]
     3: just as it should be; "`Precisely, my lord,' he said" [syn:
        {exactly}, {on the nose}, {on the dot}, {on the button}]
