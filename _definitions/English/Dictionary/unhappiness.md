---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unhappiness
offline_file: ""
offline_thumbnail: ""
uuid: 2c35568a-e774-4a71-a7f5-1036d4bebf04
updated: 1484310587
title: unhappiness
categories:
    - Dictionary
---
unhappiness
     n 1: emotions experienced when not in a state of well-being [syn:
           {sadness}] [ant: {happiness}]
     2: state characterized by emotions ranging from mild
        discontentment to deep grief [ant: {happiness}]
