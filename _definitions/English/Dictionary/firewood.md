---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/firewood
offline_file: ""
offline_thumbnail: ""
uuid: ea32e43f-fbae-486c-b4a0-0c718af9a357
updated: 1484310249
title: firewood
categories:
    - Dictionary
---
firewood
     n : wood used for fuel; "they collected and cut their own
         firewood"
