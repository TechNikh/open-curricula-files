---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rehabilitate
offline_file: ""
offline_thumbnail: ""
uuid: 80ffafd2-9b56-4297-9980-84a1004c5316
updated: 1484310170
title: rehabilitate
categories:
    - Dictionary
---
rehabilitate
     v 1: reinstall politically; "Deng Xiao Ping was rehabilitated
          several times throughout his lifetime" [ant: {purge}]
     2: restore to a state of good condition or operation
     3: help to re-adapt, as to a former state of health or good
        repute; "The prisoner was successfully rehabilitated";
        "After a year in the mental clinic, the patient is now
        rehabilitated"
