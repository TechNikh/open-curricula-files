---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Soundness
offline_file: ""
offline_thumbnail: ""
uuid: 9a94f4b4-feb2-460b-834a-2dce93ac5874
updated: 1484309499
title: Soundness
tags:
    - Of arguments
    - Logical systems
    - Soundness
    - Strong soundness
    - Arithmetic soundness
    - Relation to completeness
categories:
    - Proof theory
---
In mathematical logic, a logical system has the soundness property if and only if its inference rules prove only formulas that are valid with respect to its semantics. In most cases, this comes down to its rules having the property of preserving truth, but this is not the case in general.
