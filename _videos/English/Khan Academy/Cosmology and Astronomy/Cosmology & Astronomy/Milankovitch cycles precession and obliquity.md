---
version: 1
type: video
provider: YouTube
id: ZD8THEz18gc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Milankovitch%20cycles%20precession%20and%20obliquity.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 121d486e-6e81-46bc-9cf0-4f01730f4f5e
updated: 1486069688
title: Milankovitch cycles precession and obliquity
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/ZD8THEz18gc/default.jpg
    - https://i3.ytimg.com/vi/ZD8THEz18gc/1.jpg
    - https://i3.ytimg.com/vi/ZD8THEz18gc/2.jpg
    - https://i3.ytimg.com/vi/ZD8THEz18gc/3.jpg
---
