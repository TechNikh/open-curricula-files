---
version: 1
type: video
provider: YouTube
id: XF3nTCXcJT0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c0fc8b7c-7da2-4e0f-b68f-07684adf7313
updated: 1486069657
title: 'Cool Science Experiments for Kids- Balloon Hovercraft - Making the Hole for the Balloon Hovercraft'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/XF3nTCXcJT0/default.jpg
    - https://i3.ytimg.com/vi/XF3nTCXcJT0/1.jpg
    - https://i3.ytimg.com/vi/XF3nTCXcJT0/2.jpg
    - https://i3.ytimg.com/vi/XF3nTCXcJT0/3.jpg
---
