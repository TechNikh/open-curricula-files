---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/splitting
offline_file: ""
offline_thumbnail: ""
uuid: f4dce2ed-d049-4f5d-b414-18adfa89a4c2
updated: 1484310391
title: splitting
categories:
    - Dictionary
---
split
     adj 1: being divided or separated; "split between love and hate"
     2: having been divided; having the unity destroyed;
        "Congress...gave the impression of...a confusing sum of
        disconnected local forces"-Samuel Lubell; "a league of
        disunited nations"- E.B.White; "a fragmented coalition";
        "a split group" [syn: {disconnected}, {disunited}, {fragmented}]
     3: broken or burst apart longitudinally; "after the
        thunderstorm we found a tree with a split trunk"; "they
        tore big juicy chunks from the heart of the split
        watermelon"
     4: having a long rip or tear; "a split lip" [syn: {cut}]
     5: (especially of wood) cut or ...
