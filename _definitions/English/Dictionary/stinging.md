---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stinging
offline_file: ""
offline_thumbnail: ""
uuid: d3f3d1c8-c9a0-4806-9272-2c26e5c5080b
updated: 1484310384
title: stinging
categories:
    - Dictionary
---
stinging
     adj 1: causing or experiencing a painful shivering feeling as from
            many tiny pricks; "a prickling blush of
            embarrassment"; "the tingling feeling in a foot that
            has gone to sleep"; "a stinging nettle"; "the stinging
            windblown sleet" [syn: {prickling}, {tingling}]
     2: having a sting or the capacity to sting; "stinging insects";
        "stinging nettles" [ant: {stingless}]
     3: (of speech) harsh or hurtful in tone or character; "cutting
        remarks"; "edged satire"; "a stinging comment" [syn: {cutting},
         {edged}]
     n : a kind of pain; something as sudden and painful as being
         stung; "the sting of ...
