---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computer_Go
offline_file: ""
offline_thumbnail: ""
uuid: b8055d95-6c5d-493b-a92c-c8ce122ee109
updated: 1484309002
title: Computer Go
tags:
    - Performance
    - Early decades
    - 21st century
    - Obstacles to high-level performance
    - Size of board
    - Number of Move Options
    - Techniques in chess that cannot be applied to Go
    - Evaluation function
    - Combinatorial problems
    - Endgame
    - Order of play
    - Tactical search
    - State representation
    - System design
    - New approaches to problems
    - Design philosophies
    - Minimax tree search
    - Knowledge-based systems
    - Monte-Carlo methods
    - Machine learning
    - AlphaGo
    - List of Go-playing computer programs
    - Competitions among computer Go programs
    - History
    - Rule Formalization Problems in computer-computer games
    - Testing
categories:
    - Game artificial intelligence
---
Computer Go is the field of artificial intelligence (AI) dedicated to creating a computer program that plays the traditional board game Go. The game of Go has been a fertile subject of artificial intelligence research for decades, culminating in 2016 with the best computer program beating one of the top human players.
