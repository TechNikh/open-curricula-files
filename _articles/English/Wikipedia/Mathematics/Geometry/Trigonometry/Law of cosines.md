---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Law_of_cosines
offline_file: ""
offline_thumbnail: ""
uuid: d922884b-b573-45a3-96ff-03df4234a270
updated: 1484309341
title: Law of cosines
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/512px-Johnny-automatic-scales-of-justice.svg.png
tags:
    - History
    - Applications
    - Proofs
    - Using the distance formula
    - Using trigonometry
    - Using the Pythagorean theorem
    - Case of an obtuse angle
    - Case of an acute angle
    - Another proof in the acute case
    - "Using Ptolemy's theorem"
    - By comparing areas
    - Using geometry of the circle
    - Using the law of sines
    - Isosceles case
    - Analog for tetrahedra
    - Version suited to small angles
    - Law of cosines in non-Euclidean geometry
categories:
    - Trigonometry
---
In trigonometry, the law of cosines (also known as the cosine formula or cosine rule) relates the lengths of the sides of a triangle to the cosine of one of its angles. Using notation as in Fig. 1, the law of cosines states
