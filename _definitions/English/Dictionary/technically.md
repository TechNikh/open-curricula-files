---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/technically
offline_file: ""
offline_thumbnail: ""
uuid: 3bfaa38a-eadd-4949-925c-70ada8f6b38b
updated: 1484310319
title: technically
categories:
    - Dictionary
---
technically
     adv 1: with regard to technique; "technically lagging behind the
            Japanese"
     2: with regard to technical skill; "a technically brilliant
        solution"
