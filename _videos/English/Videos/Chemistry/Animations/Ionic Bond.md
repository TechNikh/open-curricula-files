---
version: 1
type: video
provider: YouTube
id: DEdRcfyYnSQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Ionic%20Bond.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8ae92e5c-1cdd-4a9d-80e2-ccc8189b081d
updated: 1486069662
title: Ionic Bond
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/DEdRcfyYnSQ/default.jpg
    - https://i3.ytimg.com/vi/DEdRcfyYnSQ/1.jpg
    - https://i3.ytimg.com/vi/DEdRcfyYnSQ/2.jpg
    - https://i3.ytimg.com/vi/DEdRcfyYnSQ/3.jpg
---
