---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/muster
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484518441
title: muster
categories:
    - Dictionary
---
muster
     n 1: a gathering of military personnel for duty; "he was thrown
          in the brig for missing muster"
     2: compulsory military service [syn: {conscription}, {draft}, {selective
        service}]
     v 1: gather or bring together; "muster the courage to do
          something"; "she rallied her intellect"; "Summon all
          your courage" [syn: {rally}, {summon}, {come up}, {muster
          up}]
     2: call to duty, military service, jury duty, etc.
