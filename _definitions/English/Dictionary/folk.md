---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/folk
offline_file: ""
offline_thumbnail: ""
uuid: 67176057-e92d-40a3-8dc7-a35d187bfe66
updated: 1484310533
title: folk
categories:
    - Dictionary
---
folk
     n 1: people in general; "they're just country folk"; "the common
          people determine the group character and preserve its
          customs from one generation to the next" [syn: {common
          people}]
     2: a social division of (usually preliterate) people [syn: {tribe}]
     3: people descended from a common ancestor; "his family has
        lived in Massachusetts since the Mayflower" [syn: {family},
         {family line}, {kinfolk}, {kinsfolk}, {sept}, {phratry}]
     4: the traditional and typically anonymous music that is an
        expression of the life of people in a community [syn: {folk
        music}, {ethnic music}]
