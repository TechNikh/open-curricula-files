---
version: 1
type: video
provider: YouTube
id: Hlal9ME2Aig
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Pre-Algebra/Negative%20numbers%20and%20absolute%20value/Negative%20numbers%20introduction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 959c4fdb-c41a-4120-b77a-6058360ca117
updated: 1486069569
title: Negative numbers introduction
categories:
    - Negative numbers and absolute value
thumbnail_urls:
    - https://i3.ytimg.com/vi/Hlal9ME2Aig/default.jpg
    - https://i3.ytimg.com/vi/Hlal9ME2Aig/1.jpg
    - https://i3.ytimg.com/vi/Hlal9ME2Aig/2.jpg
    - https://i3.ytimg.com/vi/Hlal9ME2Aig/3.jpg
---
Mysterious negative numbers! What ARE they? They are numbers less than zero. If you understand the nature of below zero temperatures, you can understand negative numbers. We'll help.

Practice this lesson yourself on KhanAcademy.org right now: https://www.khanacademy.org/math/pre-algebra/negatives-absolute-value-pre-alg/add-sub-negatives-pre-alg/e/number_line_2?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Watch the next lesson: https://www.khanacademy.org/math/pre-algebra/negatives-absolute-value-pre-alg/add-sub-negatives-pre-alg/v/number-line-3-exercise-example?utm_source=YT&utm_medium=Desc&utm_campaign=PreAlgebra

Pre-Algebra on Khan Academy: No way, this isn't your run of the mill arithmetic. This is Pre-algebra. You're about to play with the professionals. Think of pre-algebra as a runway. You're the airplane and algebra is your sunny vacation destination. Without the runway you're not going anywhere. Seriously, the foundation for all higher mathematics is laid with many of the concepts that we will introduce to you here: negative numbers, absolute value, factors, multiples, decimals, and fractions to name a few. So buckle up and move your seat into the upright position. We're about to take off!

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy’s Pre-Algebra channel:: https://www.youtube.com/channel/UCIMlYkATtXOFswVoCZN7nAA?sub_confirmation=1
Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
