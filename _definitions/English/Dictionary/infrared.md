---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/infrared
offline_file: ""
offline_thumbnail: ""
uuid: 6ba523b0-e84e-49e0-b896-5fef69564436
updated: 1484310391
title: infrared
categories:
    - Dictionary
---
infrared
     adj : having or employing wavelengths longer than light but
           shorter than radio waves; lying outside the visible
           spectrum at its red end; "infrared radiation";
           "infrared photography"
     n 1: the infrared part of the electromagnetic spectrum;
          electromagnetic wave frequencies below the visible
          range; "they could sense radiation in the infrared"
          [syn: {infrared frequency}]
     2: electromagnetic radiation with wavelengths longer than
        visible light but shorter than radio waves [syn: {infrared
        light}, {infrared radiation}, {infrared emission}]
