---
version: 1
type: video
provider: YouTube
id: 6M1Bs6f61Ss
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bbee9023-63f9-4922-b169-c9120d000d79
updated: 1486069613
title: Adding and subtracting a decimal percentage and fraction
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/6M1Bs6f61Ss/default.jpg
    - https://i3.ytimg.com/vi/6M1Bs6f61Ss/1.jpg
    - https://i3.ytimg.com/vi/6M1Bs6f61Ss/2.jpg
    - https://i3.ytimg.com/vi/6M1Bs6f61Ss/3.jpg
---
