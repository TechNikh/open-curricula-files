---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equaliser_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: c1e734e3-eb95-43de-99de-25da7a3ce6e5
updated: 1484309504
title: Equaliser (mathematics)
tags:
    - Definitions
    - Difference kernels
    - In category theory
    - Notes
categories:
    - Set theory
---
In mathematics, an equaliser is a set of arguments where two or more functions have equal values. An equaliser is the solution set of an equation. In certain contexts, a difference kernel is the equaliser of exactly two functions.
