---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/philosopher
offline_file: ""
offline_thumbnail: ""
uuid: d82334a8-8677-4c01-8fbc-a07d95eaabd9
updated: 1484310389
title: philosopher
categories:
    - Dictionary
---
philosopher
     n 1: a specialist in philosophy
     2: a wise person who is calm and rational; someone who lives a
        life of reason with equanimity
