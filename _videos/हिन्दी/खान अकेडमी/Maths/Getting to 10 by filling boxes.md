---
version: 1
type: video
provider: YouTube
id: VD0PfTWCvLc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 21c30d53-678d-41d8-9307-f6e8871deeff
updated: 1486069613
title: Getting to 10 by filling boxes
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/VD0PfTWCvLc/default.jpg
    - https://i3.ytimg.com/vi/VD0PfTWCvLc/1.jpg
    - https://i3.ytimg.com/vi/VD0PfTWCvLc/2.jpg
    - https://i3.ytimg.com/vi/VD0PfTWCvLc/3.jpg
---
