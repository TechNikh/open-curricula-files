---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mulgara_(software)
offline_file: ""
offline_thumbnail: ""
uuid: 3234d587-893e-4c9c-bdfd-f79dc0ebf3f8
updated: 1484309404
title: Mulgara (software)
tags:
    - History
    - Internals
categories:
    - Graph databases
---
Mulgara is a triplestore and fork of the original Kowari project. It is Open Source, scalable, and transaction-safe.[1] Mulgara instances can be queried via the iTQL query language and the SPARQL query language.[2]
