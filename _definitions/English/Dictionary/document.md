---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/document
offline_file: ""
offline_thumbnail: ""
uuid: 5b8b2b3c-1f8d-43cc-a3b2-3e75da377496
updated: 1484310597
title: document
categories:
    - Dictionary
---
document
     n 1: writing that provides information (especially information of
          an official nature) [syn: {written document}, {papers}]
     2: anything serving as a representation of a person's thinking
        by means of symbolic marks
     3: a written account of ownership or obligation
     4: (computer science) a computer file that contains text (and
        possibly formatting instructions) using 7-bit ASCII
        characters [syn: {text file}]
     v 1: record in detail; "The parents documented every step of
          their child's development"
     2: support or supply with references; "Can you document your
        claims?"
