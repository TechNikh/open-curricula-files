---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermodynamic_process_path
offline_file: ""
offline_thumbnail: ""
uuid: ff17c74e-6b70-48cc-8711-3dbec709498a
updated: 1484308872
title: Thermodynamic process path
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gax_expanding_doing_work_on_a_piston_in_a_cylinder.jpg
categories:
    - Thermodynamics
---
A thermodynamic process path is the path or series of states through which a system passes from an initial equilibrium state to a final equilibrium state[1] and can be viewed graphically on a pressure-volume (P-V), pressure-temperature (P-T), and temperature-entropy (T-s) diagrams.[2]
