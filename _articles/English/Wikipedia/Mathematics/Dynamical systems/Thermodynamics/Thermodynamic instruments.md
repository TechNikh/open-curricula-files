---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermodynamic_instruments
offline_file: ""
offline_thumbnail: ""
uuid: 5985e977-c60a-4b8c-9bd6-f0855dffd551
updated: 1484308874
title: Thermodynamic instruments
tags:
    - Overview
    - Thermodynamic meters
    - Thermodynamic reservoirs
    - Theory
categories:
    - Thermodynamics
---
A thermodynamic instrument is any device which facilitates the quantitative measurement of thermodynamic systems. In order for a thermodynamic parameter to be truly defined, a technique for its measurement must be specified. For example, the ultimate definition of temperature is "what a thermometer reads". The question follows - what is a thermometer?
