---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fibrant_object
offline_file: ""
offline_thumbnail: ""
uuid: 989a99b6-9501-42b9-86da-30ad16ee5c72
updated: 1484309360
title: Fibrant object
categories:
    - Geometry stubs
---
In mathematics, specifically in homotopy theory in the context of a model category M, a fibrant object A of M is an object that has a fibration to the terminal object of the category.
