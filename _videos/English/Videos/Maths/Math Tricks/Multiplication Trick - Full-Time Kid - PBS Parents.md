---
version: 1
type: video
provider: YouTube
id: Wu3JSnRaaV0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d19a560f-9c7f-4f30-b727-3975c4f108db
updated: 1486069625
title: Multiplication Trick | Full-Time Kid | PBS Parents
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/Wu3JSnRaaV0/default.jpg
    - https://i3.ytimg.com/vi/Wu3JSnRaaV0/1.jpg
    - https://i3.ytimg.com/vi/Wu3JSnRaaV0/2.jpg
    - https://i3.ytimg.com/vi/Wu3JSnRaaV0/3.jpg
---
