---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agree
offline_file: ""
offline_thumbnail: ""
uuid: 2ece6668-fe3e-4825-b228-d592f4a80010
updated: 1484310289
title: agree
categories:
    - Dictionary
---
agree
     v 1: be in accord; be in agreement; "We agreed on the terms of
          the settlement"; "I can't agree with you!"; "I hold with
          those who say life is sacred"; "Both philosophers
          concord on this point" [syn: {hold}, {concur}, {concord}]
          [ant: {disagree}]
     2: consent or assent to a condition, or agree to do something;
        "She agreed to all my conditions"; "He agreed to leave her
        alone"
     3: be compatible, similar or consistent; coincide in their
        characteristics; "The two stories don't agree in many
        details"; "The handwriting checks with the signature on
        the check"; "The suspect's fingerprints don't match ...
