---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cyprinus_carpio
offline_file: ""
offline_thumbnail: ""
uuid: 815d8558-cc83-44a8-9382-5e36edf910b9
updated: 1484308342
title: Common carp
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cyprinus_carpio_1879.jpg
tags:
    - Taxonomy
    - History
    - Physiology
    - Habitat
    - Diet
    - Reproduction
    - Predation
    - Introduction into other habitats
    - As food and sport
categories:
    - Uncategorized
---
The common carp or European carp (Cyprinus carpio) is a widespread freshwater fish of eutrophic waters in lakes and large rivers in Europe and Asia.[2][3] The native wild populations are considered vulnerable to extinction by the IUCN,[1] but the species has also been domesticated and introduced into environments worldwide, and is often considered a destructive invasive species,[2] being included in the list of the world's 100 worst invasive species. It gives its name to the carp family Cyprinidae.
