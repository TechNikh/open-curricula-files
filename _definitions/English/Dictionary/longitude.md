---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/longitude
offline_file: ""
offline_thumbnail: ""
uuid: 51d430f7-93be-4ad9-be71-9c0f387dc57b
updated: 1484310433
title: longitude
categories:
    - Dictionary
---
longitude
     n : an imaginary great circle on the surface of the earth
         passing through the north and south poles at right angles
         to the equator; "all points on the same meridian have the
         same longitude" [syn: {meridian}, {line of longitude}]
