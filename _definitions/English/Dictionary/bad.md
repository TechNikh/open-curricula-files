---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bad
offline_file: ""
offline_thumbnail: ""
uuid: 15499695-c3e6-42b6-a56f-258b5dc70b13
updated: 1484310569
title: bad
categories:
    - Dictionary
---
bad
     adj 1: having undesirable or negative qualities; "a bad report
            card"; "his sloppy appearance made a bad impression";
            "a bad little boy"; "clothes in bad shape"; "a bad
            cut"; "bad luck"; "the news was very bad"; "the
            reviews were bad"; "the pay is bad"; "it was a bad
            light for reading"; "the movie was a bad choice" [ant:
             {good}]
     2: very intense; "a bad headache"; "in a big rage"; "had a big
        (or bad) shock"; "a bad earthquake"; "a bad storm" [syn: {big}]
     3: feeling physical discomfort or pain (`tough' is occasionally
        used colloquially for `bad'); "my throat feels bad"; "she
        felt ...
