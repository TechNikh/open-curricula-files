---
version: 1
type: video
provider: YouTube
id: BTiZD7p_oTc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d6f198b8-e891-4154-a1ca-87b712e1efa7
updated: 1486069647
title: Best fractals zoom ever
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/BTiZD7p_oTc/default.jpg
    - https://i3.ytimg.com/vi/BTiZD7p_oTc/1.jpg
    - https://i3.ytimg.com/vi/BTiZD7p_oTc/2.jpg
    - https://i3.ytimg.com/vi/BTiZD7p_oTc/3.jpg
---
