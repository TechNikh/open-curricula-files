---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/European_Terrestrial_Reference_System_1989
offline_file: ""
offline_thumbnail: ""
uuid: 0a96d9a4-da9b-4b90-8dd8-5703c8a63256
updated: 1484309295
title: European Terrestrial Reference System 1989
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Europe_green_light_1.png
categories:
    - Orientation (geometry)
---
The European Terrestrial Reference System 1989 (ETRS89) is an ECEF (Earth-Centered, Earth-Fixed) geodetic Cartesian reference frame, in which the Eurasian Plate as a whole is static. The coordinates and maps in Europe based on ETRS89 are not subject to change due to the continental drift.
