---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tram
offline_file: ""
offline_thumbnail: ""
uuid: 04b1ecff-d3dc-4c09-b20b-57b555a3c1d3
updated: 1484310152
title: tram
categories:
    - Dictionary
---
tram
     n 1: a conveyance that transports passengers or freight in
          carriers suspended from cables and supported by a series
          of towers [syn: {tramway}, {aerial tramway}, {cable
          tramway}, {ropeway}]
     2: a four-wheeled wagon that runs on tracks in a mine; "a
        tramcar carries coal out of a coal mine" [syn: {tramcar}]
     3: a wheeled vehicle that runs on rails and is propelled by
        electricity; "`tram' and `tramcar' are British terms"
        [syn: {streetcar}, {tramcar}, {trolley}, {trolley car}]
     [also: {tramming}, {trammed}]
