---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stance
offline_file: ""
offline_thumbnail: ""
uuid: 4b4daeb7-54c3-4410-aed7-2d621d0f3331
updated: 1484310551
title: stance
categories:
    - Dictionary
---
stance
     n 1: standing posture
     2: a rationalized mental attitude [syn: {position}, {posture}]
