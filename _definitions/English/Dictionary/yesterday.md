---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yesterday
offline_file: ""
offline_thumbnail: ""
uuid: 82cb340f-a4a2-458d-a9bd-0b6ed2cbec12
updated: 1484310154
title: yesterday
categories:
    - Dictionary
---
yesterday
     n 1: the day immediately before today; "it was in yesterday's
          newspapers"
     2: the recent past; "yesterday's solutions are not good
        enough"; "we shared many yesterdays"
     adv 1: on the day preceding today; "yesterday the weather was
            beautiful"
     2: in the recent past; only a short time ago; "I was not born
        yesterday!"
