---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/footwear
offline_file: ""
offline_thumbnail: ""
uuid: 5bea4291-541d-45b2-bfd7-89b8b89a42d2
updated: 1484310471
title: footwear
categories:
    - Dictionary
---
footwear
     n 1: clothing worn on a person's feet
     2: covering for a person's feet [syn: {footgear}]
