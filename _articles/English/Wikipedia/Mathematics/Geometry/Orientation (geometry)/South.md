---
version: 1
type: article
id: https://en.wikipedia.org/wiki/South
offline_file: ""
offline_thumbnail: ""
uuid: 192ce88a-5c3a-4b91-b4c9-0f7408878ca8
updated: 1484309302
title: South
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Compass_Rose_English_South.svg.png
tags:
    - Etymology
    - Navigation
    - South Pole
    - geography
    - Other uses
categories:
    - Orientation (geometry)
---
South is a noun, adjective, or adverb indicating direction or geography. It is one of the four cardinal directions or compass points. South is the polar opposite of north and is perpendicular to east and west.
