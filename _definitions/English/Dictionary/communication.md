---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/communication
offline_file: ""
offline_thumbnail: ""
uuid: 016f5760-4dae-46c7-8b6e-0196d0e165df
updated: 1484310210
title: communication
categories:
    - Dictionary
---
communication
     n 1: the activity of communicating; the activity of conveying
          information; "they could not act without official
          communication from Moscow" [syn: {communicating}]
     2: something that is communicated by or to or between people or
        groups
     3: a connection allowing access between persons or places; "how
        many lines of communication can there be among four
        people?"; "a secret passageway provided communication
        between the two rooms"
