---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Behrend_function
offline_file: ""
offline_thumbnail: ""
uuid: e6a06e11-613c-4f8d-97f3-5cac3036f85c
updated: 1484309352
title: Behrend function
categories:
    - Geometry stubs
---
such that if X is a quasi-projective proper moduli scheme carrying a symmetric obstruction theory, then the weighted Euler characteristic
