---
version: 1
type: video
provider: YouTube
id: UXp3GVtSTJA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f99915ca-1deb-424d-b474-311f71b9027f
updated: 1486069635
title: Experiment to Measure the Angle of Deviation of a Prism
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/UXp3GVtSTJA/default.jpg
    - https://i3.ytimg.com/vi/UXp3GVtSTJA/1.jpg
    - https://i3.ytimg.com/vi/UXp3GVtSTJA/2.jpg
    - https://i3.ytimg.com/vi/UXp3GVtSTJA/3.jpg
---
