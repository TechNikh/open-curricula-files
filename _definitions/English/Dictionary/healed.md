---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/healed
offline_file: ""
offline_thumbnail: ""
uuid: ac1513a5-212e-4da3-b7c3-2414dd785fdb
updated: 1484310313
title: healed
categories:
    - Dictionary
---
healed
     adj : freed from illness or injury; "the patient appears cured";
           "the incision is healed"; "appears to be entirely
           recovered"; "when the recovered patient tries to
           remember what occurred during his delirium"- Normon
           Cameron [syn: {cured}, {recovered}]
