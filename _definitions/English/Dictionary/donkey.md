---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/donkey
offline_file: ""
offline_thumbnail: ""
uuid: 047dda0f-47e8-4c4d-bc3e-b44fdd175b26
updated: 1484310204
title: donkey
categories:
    - Dictionary
---
donkey
     n 1: the symbol of the Democratic Party; introduced in cartoons
          by Thomas Nast in 1874
     2: domestic beast of burden descended from the African wild
        ass; patient but stubborn [syn: {domestic ass}, {Equus
        asinus}]
