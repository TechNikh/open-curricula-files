---
version: 1
type: video
provider: YouTube
id: IGs7IB48Fvg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Example%202-%20Modeling%20with%20polynomials.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 691b90c0-601f-48a8-b354-fc7108c51c8e
updated: 1486069557
title: 'Example 2- Modeling with polynomials'
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/IGs7IB48Fvg/default.jpg
    - https://i3.ytimg.com/vi/IGs7IB48Fvg/1.jpg
    - https://i3.ytimg.com/vi/IGs7IB48Fvg/2.jpg
    - https://i3.ytimg.com/vi/IGs7IB48Fvg/3.jpg
---
