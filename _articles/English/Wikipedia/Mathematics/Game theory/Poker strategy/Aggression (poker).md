---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aggression_(poker)
offline_file: ""
offline_thumbnail: ""
uuid: 4c7f5329-0038-48a8-82be-4e7e0aa0ad08
updated: 1484309059
title: Aggression (poker)
categories:
    - Poker strategy
---
In the game of poker, opens and raises are considered aggressive plays, while calls and checks are considered passive (though a check-raise would be considered a very aggressive play). It is said[by whom?] that "aggression has its own value", meaning that often aggressive plays can make money with weak hands because of bluff value. In general, opponents must respond to aggressive play by playing more loosely, which offers more opportunities to make mistakes.
