---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Straight_skeleton
offline_file: ""
offline_thumbnail: ""
uuid: 887c4dd9-9740-443d-8e42-39ffa0338f34
updated: 1484308608
title: Straight skeleton
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/StraightSkeletonDefinition.png
tags:
    - Definition
    - Algorithms
    - Applications
    - Higher dimensions
categories:
    - Discrete geometry
---
In geometry, a straight skeleton is a method of representing a polygon by a topological skeleton. It is similar in some ways to the medial axis but differs in that the skeleton is composed of straight line segments, while the medial axis of a polygon may involve parabolic curves.
