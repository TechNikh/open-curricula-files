---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bygone
offline_file: ""
offline_thumbnail: ""
uuid: 1247e345-6286-44b9-9c7f-38465bd64f6c
updated: 1484310383
title: bygone
categories:
    - Dictionary
---
bygone
     adj : well in the past; former; "bygone days"; "dreams of foregone
           times"; "sweet memories of gone summers"; "relics of a
           departed era" [syn: {bypast}, {departed}, {foregone}, {gone}]
     n : past events to be put aside; "let bygones be bygones" [syn:
         {water under the bridge}]
