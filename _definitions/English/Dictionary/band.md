---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/band
offline_file: ""
offline_thumbnail: ""
uuid: db63fc9a-e743-44f8-999b-e896ef1171e1
updated: 1484310391
title: band
categories:
    - Dictionary
---
band
     n 1: an unofficial association of people or groups; "the smart
          set goes there"; "they were an angry lot" [syn: {set}, {circle},
           {lot}]
     2: instrumentalists not including string players
     3: a stripe of contrasting color; "chromosomes exhibit
        characteristic bands" [syn: {stria}, {striation}]
     4: a strip or stripe of a contrasting color or material [syn: {banding},
         {stripe}]
     5: a group of musicians playing popular music for dancing [syn:
         {dance band}, {dance orchestra}]
     6: a range of frequencies between two limits
     7: something elongated that is worn around the body or one of
        the limbs
     8: jewelry ...
