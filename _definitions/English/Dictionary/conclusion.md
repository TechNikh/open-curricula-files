---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conclusion
offline_file: ""
offline_thumbnail: ""
uuid: fde695e5-2221-481a-b78d-b7696d375bb2
updated: 1484310284
title: conclusion
categories:
    - Dictionary
---
conclusion
     n 1: a position or opinion or judgment reached after
          consideration; "a decision unfavorable to the
          opposition"; "his conclusion took the evidence into
          account"; "satisfied with the panel's determination"
          [syn: {decision}, {determination}]
     2: an intuitive assumption; "jump to a conclusion"
     3: the temporal end; the concluding time; "the stopping point
        of each round was signaled by a bell"; "the market was up
        at the finish"; "they were playing better at the close of
        the season" [syn: {stopping point}, {finale}, {finis}, {finish},
         {last}, {close}]
     4: event whose occurrence ends something; ...
