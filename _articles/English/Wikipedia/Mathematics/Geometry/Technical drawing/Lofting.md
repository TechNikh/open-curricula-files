---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lofting
offline_file: ""
offline_thumbnail: ""
uuid: 120153bd-974b-4e6a-be67-493de88b3663
updated: 1484309329
title: Lofting
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cecil_Beaton_Photographs-_Tyneside_Shipyards%252C_1943_DB88.jpg'
categories:
    - Technical drawing
---
Lofting is a drafting technique (sometimes using mathematical tables) whereby curved lines are generated, to be used in plans for streamlined objects such as aircraft and boats. The lines may be drawn on wood and the wood then cut for advanced woodworking. The technique can be as simple as bending a flexible object, such as a long strip of thin wood or thin plastic, so that it passes over three non-linear points and scribing the resultant curved line, or plotting the line using computers or mathematical tables.
