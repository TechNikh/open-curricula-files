---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/balloon
offline_file: ""
offline_thumbnail: ""
uuid: 69bff865-d37a-47c0-a77d-93a0a32ef9dc
updated: 1484310375
title: balloon
categories:
    - Dictionary
---
balloon
     n 1: small thin inflatable rubber bag with narrow neck
     2: large tough non-rigid bag filled with gas or heated air
     v 1: ride in a hot-air balloon; "He tried to balloon around the
          earth but storms forced him to land in China"
     2: become inflated; "The sails ballooned" [syn: {inflate}, {billow}]
