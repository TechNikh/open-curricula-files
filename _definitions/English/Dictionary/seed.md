---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seed
offline_file: ""
offline_thumbnail: ""
uuid: 406be7b4-1c8a-4bdd-b226-901a83281b6d
updated: 1484310307
title: seed
categories:
    - Dictionary
---
seed
     n 1: a small hard fruit
     2: a mature fertilized plant ovule consisting of an embryo and
        its food source and having a protective coat or testa
     3: one of the outstanding players in a tournament [syn: {seeded
        player}]
     4: anything that provides inspiration for later work [syn: {source},
         {germ}]
     5: the thick white fluid containing spermatozoa that is
        ejaculated by the male genital tract [syn: {semen}, {seminal
        fluid}, {ejaculate}, {cum}]
     v 1: go to seed; shed seeds; "The dandelions went to seed"
     2: help (an enterprise) in its early stages of development by
        providing seed money
     3: bear seeds
     4: place ...
