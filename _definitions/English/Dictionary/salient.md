---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/salient
offline_file: ""
offline_thumbnail: ""
uuid: 239b5108-81c7-4503-ac2e-920cf29d3962
updated: 1484310395
title: salient
categories:
    - Dictionary
---
salient
     adj 1: having a quality that thrusts itself into attention; "an
            outstanding fact of our time is that nations poisoned
            by anti semitism proved less fortunate in regard to
            their own freedom"; "a new theory is the most
            prominent feature of the book"; "salient traits"; "a
            spectacular rise in prices"; "a striking thing about
            Picadilly Circus is the statue of Eros in the center";
            "a striking resemblance between parent and child"
            [syn: {outstanding}, {prominent}, {spectacular}, {striking}]
     2: (of angles) pointing outward at an angle of less than 180
        degrees [ant: {re-entrant}]
 ...
