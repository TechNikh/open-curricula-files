---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cling
offline_file: ""
offline_thumbnail: ""
uuid: e79bfa95-0f13-41da-baf6-85b158ba3d52
updated: 1484310607
title: cling
categories:
    - Dictionary
---
cling
     n : fruit (especially peach) whose flesh adheres strongly to the
         pit [syn: {clingstone}]
     v 1: come or be in close contact with; stick or hold together and
          resist separation; "The dress clings to her body"; "The
          label stuck to the box"; "The sushi rice grains cohere"
          [syn: {cleave}, {adhere}, {stick}, {cohere}]
     2: to remain emotionally or intellectually attached; "He clings
        to the idea that she might still love him."
     3: hold on tightly or tenaciously; "hang on to your father's
        hands"; "The child clung to his mother's apron" [syn: {hang}]
     [also: {clung}]
