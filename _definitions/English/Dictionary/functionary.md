---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/functionary
offline_file: ""
offline_thumbnail: ""
uuid: b5af32dc-f8fa-4e9d-bcc7-abcb28c95a02
updated: 1484310597
title: functionary
categories:
    - Dictionary
---
functionary
     n : a worker who holds or is invested with an office [syn: {official}]
