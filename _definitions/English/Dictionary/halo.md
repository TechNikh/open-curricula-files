---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/halo
offline_file: ""
offline_thumbnail: ""
uuid: f0c5a057-05d8-4ac1-8780-87d389b2c99a
updated: 1484310419
title: halo
categories:
    - Dictionary
---
halo
     n 1: an indication of radiant light drawn around the head of a
          saint [syn: {aura}, {aureole}, {nimbus}, {glory}, {gloriole}]
     2: a toroidal shape; "a ring of ships in the harbor"; "a halo
        of smoke" [syn: {ring}, {annulus}, {anulus}, {doughnut}, {anchor
        ring}]
     3: a circle of light around the sun or moon
     [also: {haloes} (pl)]
