---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/percolate
offline_file: ""
offline_thumbnail: ""
uuid: 3b5832d9-81cd-4f2d-a95c-3b2327de24e4
updated: 1484310461
title: percolate
categories:
    - Dictionary
---
percolate
     n : the product of percolation
     v 1: permeate or penetrate gradually; "the fertilizer leached
          into the ground" [syn: {leach}]
     2: spread gradually; "Light percolated into our house in the
        morning"
     3: prepare in a percolator; "percolate coffee"
     4: cause (a solvent) to pass through a permeable substance in
        order to extract a soluble constituent
     5: pass through; "Water permeates sand easily" [syn: {sink in},
         {permeate}, {filter}]
     6: gain or regain energy; "I picked up after a nap" [syn: {perk
        up}, {perk}, {pick up}, {gain vigor}]
