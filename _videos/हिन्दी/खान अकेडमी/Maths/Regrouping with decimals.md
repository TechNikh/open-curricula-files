---
version: 1
type: video
provider: YouTube
id: FoP-pOQBFjY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5a0df4af-a4c0-4cb1-b62b-76af732e59bc
updated: 1486069614
title: Regrouping with decimals
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/FoP-pOQBFjY/default.jpg
    - https://i3.ytimg.com/vi/FoP-pOQBFjY/1.jpg
    - https://i3.ytimg.com/vi/FoP-pOQBFjY/2.jpg
    - https://i3.ytimg.com/vi/FoP-pOQBFjY/3.jpg
---
