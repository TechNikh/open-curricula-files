---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sometime
offline_file: ""
offline_thumbnail: ""
uuid: 02e30647-9ea0-458c-990b-611b28f1031e
updated: 1484310375
title: sometime
categories:
    - Dictionary
---
sometime
     adj : belonging to some prior time; "erstwhile friend"; "our
           former glory"; "the once capital of the state"; "her
           quondam lover" [syn: {erstwhile(a)}, {former(a)}, {once(a)},
            {onetime(a)}, {quondam(a)}, {sometime(a)}]
     adv : at some indefinite or unstated time; "let's get together
           sometime"; "everything has to end sometime"; "It was to
           be printed sometime later"
