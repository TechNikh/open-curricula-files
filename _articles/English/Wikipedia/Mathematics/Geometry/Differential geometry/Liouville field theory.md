---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Liouville_field_theory
offline_file: ""
offline_thumbnail: ""
uuid: 5da350a4-d8f8-4cb0-ba54-69fa65e06799
updated: 1484309226
title: Liouville field theory
categories:
    - Differential geometry
---
In physics, Liouville field theory (or simply Liouville theory) is a two-dimensional quantum field theory whose classical equation of motion resembles the Joseph Liouville's non-linear second order differential equation that appears in the classical geometrical problem of uniformizing Riemann surfaces.[1]
