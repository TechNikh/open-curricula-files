---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/played
offline_file: ""
offline_thumbnail: ""
uuid: 60c055e3-831d-403a-b326-64a1bc92e3db
updated: 1484310194
title: played
categories:
    - Dictionary
---
played
     adj : (of games) engaged in; "the loosely played game"
