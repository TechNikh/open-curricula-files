---
version: 1
type: article
id: https://en.wikipedia.org/wiki/14_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 5acc3008-7e6f-47d1-8672-2f193fab43d6
updated: 1484308948
title: 14 (number)
tags:
    - In mathematics
    - In science
    - chemistry
    - Astronomy
    - In religion and mythology
    - Christianity
    - Mormonism
    - Islam
    - Hinduism
    - Mythology
    - Age 14
    - In sports
    - In other fields
categories:
    - Integers
---
In speech, the numbers 14 and 40 are often confused. When carefully enunciated, they differ in which syllable is stressed: 14 i/fɔərˈtiːn/ vs 40 /ˈfɔːrti/.[1] In relation to the word "four" (4), 14 is spelled "fourteen".
