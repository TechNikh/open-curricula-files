---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unorganised
offline_file: ""
offline_thumbnail: ""
uuid: 7f1ea22d-7185-47bc-b01c-d6f9e5e58995
updated: 1484310455
title: unorganised
categories:
    - Dictionary
---
unorganised
     adj 1: not having or belonging to a structured whole; "unorganized
            territories lack a formal government" [syn: {unorganized}]
            [ant: {organized}]
     2: not affiliated in a trade union; "the workers in the plant
        were unorganized" [syn: {unorganized}, {nonunionized}, {nonunionised}]
