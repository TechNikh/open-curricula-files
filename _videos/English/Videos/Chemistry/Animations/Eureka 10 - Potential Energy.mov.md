---
version: 1
type: video
provider: YouTube
id: EM7FjGzUCR4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Eureka%2010%20-%20Potential%20Energy.mov.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 79686b6c-49e0-4dbc-80d7-62c46720087d
updated: 1486069665
title: 'Eureka 10 - Potential Energy.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/EM7FjGzUCR4/default.jpg
    - https://i3.ytimg.com/vi/EM7FjGzUCR4/1.jpg
    - https://i3.ytimg.com/vi/EM7FjGzUCR4/2.jpg
    - https://i3.ytimg.com/vi/EM7FjGzUCR4/3.jpg
---
