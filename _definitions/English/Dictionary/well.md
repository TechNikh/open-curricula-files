---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/well
offline_file: ""
offline_thumbnail: ""
uuid: b030adfe-cb52-4f62-a0d3-341339f4c11c
updated: 1484310357
title: well
categories:
    - Dictionary
---
well
     adj 1: in good health especially after having suffered illness or
            injury; "appears to be entirely well"; "the wound is
            nearly well"; "a well man"; "I think I'm well; at
            least I feel well" [ant: {ill}]
     2: resulting favorably; "its a good thing that I wasn't there";
        "it is good that you stayed"; "it is well that no one saw
        you"; "all's well that ends well" [syn: {good}, {well(p)}]
     3: wise or advantageous and hence advisable; "it would be well
        to start early" [syn: {well(p)}]
     n 1: a deep hole or shaft dug or drilled to obtain water or oil
          or gas or brine
     2: a cavity or vessel used to contain ...
