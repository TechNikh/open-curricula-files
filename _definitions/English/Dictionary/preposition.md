---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preposition
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484533441
title: preposition
categories:
    - Dictionary
---
preposition
     n 1: a function word that combines with a noun or pronoun or noun
          phrase to form a prepositional phrase that can have an
          adverbial or adjectival relation to some other word
     2: (linguistics) the placing of one linguistic element before
        another (as placing a modifier before the word it modifies
        in a sentence or placing an affix before the base to which
        it is attached)
