---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/teaching
offline_file: ""
offline_thumbnail: ""
uuid: eb4a93d6-0e77-4a36-b4d5-e1174fdcd6d9
updated: 1484310575
title: teaching
categories:
    - Dictionary
---
teaching
     n 1: the profession of a teacher; "he prepared for teaching while
          still in college"; "pedagogy is recognized as an
          important profession" [syn: {instruction}, {pedagogy}]
     2: a doctrine that is taught; "the teachings of religion"; "he
        believed all the Christian precepts" [syn: {precept}, {commandment}]
     3: the activities of educating or instructing or teaching;
        activities that impart knowledge or skill; "he received no
        formal education"; "our instruction was carefully
        programmed"; "good teaching is seldom rewarded" [syn: {education},
         {instruction}, {pedagogy}, {educational activity}]
