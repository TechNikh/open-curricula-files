---
version: 1
type: video
provider: YouTube
id: Io-HXZTepH4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Waves-%20Light%2C%20Sound%2C%20and%20the%20nature%20of%20Reality.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0e6fdca7-421d-4720-801b-9df92695f59a
updated: 1486069656
title: 'Waves- Light, Sound, and the nature of Reality'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/Io-HXZTepH4/default.jpg
    - https://i3.ytimg.com/vi/Io-HXZTepH4/1.jpg
    - https://i3.ytimg.com/vi/Io-HXZTepH4/2.jpg
    - https://i3.ytimg.com/vi/Io-HXZTepH4/3.jpg
---
