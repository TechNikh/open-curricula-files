---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/troupe
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484525821
title: troupe
categories:
    - Dictionary
---
troupe
     n : organization of performers and associated personnel
         (especially theatrical); "the traveling company all
         stayed at the same hotel" [syn: {company}]
