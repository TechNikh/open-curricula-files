---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dust
offline_file: ""
offline_thumbnail: ""
uuid: d9dfae52-b80b-491a-9c68-beb7c57cc8e2
updated: 1484310228
title: dust
categories:
    - Dictionary
---
dust
     n 1: fine powdery material such as dry earth or pollen that can
          be blown about in the air; "the furniture was covered
          with dust"
     2: the remains of something that has been destroyed or broken
        up [syn: {debris}, {junk}, {rubble}, {detritus}]
     3: free microscopic particles of solid material; "astronomers
        say that the empty space between planets actually contains
        measurable amounts of dust"
     v 1: remove the dust from; "dust the cabinets"
     2: rub the dust over a surface so as to blur the outlines of a
        shape; "The artist dusted the charcoal drawing down to a
        faint image"
     3: cover with a light dusting of a ...
