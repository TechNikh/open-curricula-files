---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/motivation
offline_file: ""
offline_thumbnail: ""
uuid: c8615469-1d81-44d6-b16d-9bc71632a35d
updated: 1484310357
title: motivation
categories:
    - Dictionary
---
motivation
     n 1: the psychological feature that arouses an organism to action
          toward a desired goal; the reason for the action; that
          which gives purpose and direction to behavior; "we did
          not understand his motivation"; "he acted with the best
          of motives" [syn: {motive}, {need}]
     2: the condition of being motivated; "his motivation was at a
        high level"
     3: the act of motivating; providing incentive [syn: {motivating}]
