---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Charles_Colbourn
offline_file: ""
offline_thumbnail: ""
uuid: cb5e7970-e13d-4f7e-b818-3017d946c4b1
updated: 1484309461
title: Charles Colbourn
categories:
    - Graph theorists
---
Charles Joseph Colbourn (born October 24, 1953) is a Canadian computer scientist and mathematician, whose research concerns graph algorithms, combinatorial designs, and their applications. From 1996 to 2001 he was the Dorothean Professor of Computer Science at the University of Vermont; since then he has been a professor of Computer Science and Engineering at Arizona State University.[1][2][3]
