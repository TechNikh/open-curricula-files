---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Digital_root
offline_file: ""
offline_thumbnail: ""
uuid: ba8196bb-62ef-4b63-9bfb-8e450ba4d2b6
updated: 1484308635
title: Digital root
tags:
    - Significance and formula of the digital root
    - Abstract multiplication of digital roots
    - Formal definition
    - Example
    - Proof that a constant value exists
    - Congruence formula
    - Some properties of digital roots
    - In other bases
categories:
    - Number theory
---
The digital root (also repeated digital sum) of a non-negative integer is the (single digit) value obtained by an iterative process of summing digits, on each iteration using the result from the previous iteration to compute a digit sum. The process continues until a single-digit number is reached.
