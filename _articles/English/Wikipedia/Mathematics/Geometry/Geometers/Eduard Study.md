---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eduard_Study
offline_file: ""
offline_thumbnail: ""
uuid: 3dd27828-3063-4b43-a314-c7fab6a9afa6
updated: 1484309103
title: Eduard Study
tags:
    - Career
    - Euclidean space group and dual quaternions
    - Hypercomplex numbers
    - Ruled surfaces
    - Hermitian form metric
    - Valence theory
    - Cited publications
categories:
    - Geometers
---
Eduard Study, more properly Christian Hugo Eduard Study (March 23, 1862 – January 6, 1930), was a German mathematician known for work on invariant theory of ternary forms (1889) and for the study of spherical trigonometry. He is also known for contributions to space geometry, hypercomplex numbers, and criticism of early physical chemistry.
