---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partial_k-tree
offline_file: ""
offline_thumbnail: ""
uuid: f24636a5-8c0a-4923-aa60-7ef7c542d842
updated: 1484309413
title: Partial k-tree
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Partial_3-tree_forbidden_minors.svg.png
tags:
    - Graph minors
    - Dynamic programming
    - Related families of graphs
    - Notes
categories:
    - Graph minor theory
---
In graph theory, a partial k-tree is a type of graph, defined either as a subgraph of a k-tree or as a graph with treewidth at most k. Many NP-hard combinatorial problems on graphs are solvable in polynomial time when restricted to the partial k-trees, for bounded values of k.
