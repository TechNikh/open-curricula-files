---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/classification
offline_file: ""
offline_thumbnail: ""
uuid: bdb56080-1ae4-4123-9013-50fbb64dcade
updated: 1484310307
title: classification
categories:
    - Dictionary
---
classification
     n 1: the act of distributing things into classes or categories of
          the same type [syn: {categorization}, {categorisation},
          {compartmentalization}, {compartmentalisation}, {assortment}]
     2: a group of people or things arranged by class or category
        [syn: {categorization}, {categorisation}]
     3: the basic cognitive process of arranging into classes or
        categories [syn: {categorization}, {categorisation}, {sorting}]
     4: restriction imposed by the government on documents or
        weapons that are available only to certain authorized
        people [ant: {declassification}]
