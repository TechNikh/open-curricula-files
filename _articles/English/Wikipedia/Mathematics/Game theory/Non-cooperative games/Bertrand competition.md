---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bertrand_competition
offline_file: ""
offline_thumbnail: ""
uuid: a41d720e-4369-4072-a990-8d292c71daf7
updated: 1484309055
title: Bertrand competition
tags:
    - The Bertrand duopoly equilibrium
    - Calculating the classic Bertrand model
    - Critical analysis of the Bertrand model
    - Bertrand competition versus Cournot competition
categories:
    - Non-cooperative games
---
Bertrand competition is a model of competition used in economics, named after Joseph Louis François Bertrand (1822–1900). It describes interactions among firms (sellers) that set prices and their customers (buyers) that choose quantities at the prices set. The model was formulated in 1883 by Bertrand in a review of Antoine Augustin Cournot's book Recherches sur les Principes Mathematiques de la Theorie des Richesses (1838) in which Cournot had put forward the Cournot model.[1] Cournot argued that when firms choose quantities, the equilibrium outcome involves firms pricing above marginal cost and hence the competitive price. In his review, Bertrand argued that if firms chose prices rather ...
