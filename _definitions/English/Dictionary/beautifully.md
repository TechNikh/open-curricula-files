---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beautifully
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484583901
title: beautifully
categories:
    - Dictionary
---
beautifully
     adv : in a beautiful manner; "her face was beautifully made up"
           [syn: {attractively}] [ant: {unattractively}]
