---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kerchief
offline_file: ""
offline_thumbnail: ""
uuid: 9017bfc8-b5e1-43ce-b923-748d29c7431c
updated: 1484310349
title: kerchief
categories:
    - Dictionary
---
kerchief
     n : a square scarf that is folded into a triangle and worn over
         the head or about the neck
