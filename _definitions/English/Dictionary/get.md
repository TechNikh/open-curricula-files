---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/get
offline_file: ""
offline_thumbnail: ""
uuid: 45396a49-1376-4081-839f-d1f7f5f38b8d
updated: 1484310359
title: get
categories:
    - Dictionary
---
get
     v 1: come into the possession of something concrete or abstract;
          "She got a lot of paintings from her uncle"; "They
          acquired a new pet"; "Get your results the next day";
          "Get permission to take a few days off from work" [syn:
          {acquire}]
     2: enter or assume a certain state or condition; "He became
        annoyed when he heard the bad news"; "It must be getting
        more serious"; "her face went red with anger"; "She went
        into ecstasy"; "Get going!" [syn: {become}, {go}]
     3: cause to move; cause to be in a certain position or
        condition; "He got his squad on the ball"; "This let me in
        for a big surprise"; "He ...
