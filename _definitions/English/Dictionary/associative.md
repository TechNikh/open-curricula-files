---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/associative
offline_file: ""
offline_thumbnail: ""
uuid: 335fcb71-d526-4197-9b68-052b8a79a376
updated: 1484310142
title: associative
categories:
    - Dictionary
---
associative
     adj 1: relating to or resulting from association; "associative
            recall"
     2: characterized by or causing or resulting from association;
        "associative learning" [syn: {associatory}] [ant: {nonassociative}]
