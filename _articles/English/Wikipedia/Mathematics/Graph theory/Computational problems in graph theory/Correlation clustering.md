---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Correlation_clustering
offline_file: ""
offline_thumbnail: ""
uuid: ccede304-9b7a-489b-9f31-5d5753a78fb1
updated: 1484309389
title: Correlation clustering
tags:
    - Description of the problem
    - Algorithms
    - Optimal number of clusters
    - Correlation clustering (data mining)
categories:
    - Computational problems in graph theory
---
Clustering is the problem of partitioning data points into groups based on their similarity. Correlation clustering provides a method for clustering a set of objects into the optimum number of clusters without specifying that number in advance.[1]
