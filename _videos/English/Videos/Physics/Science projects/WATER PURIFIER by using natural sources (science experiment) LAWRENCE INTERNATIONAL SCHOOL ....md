---
version: 1
type: video
provider: YouTube
id: m2Qg9ax6hoA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 55d1fb2d-ea34-4340-9b7a-2bd45ca8caeb
updated: 1486069634
title: >
    WATER PURIFIER by using natural sources (science experiment)
    LAWRENCE INTERNATIONAL SCHOOL ...
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/m2Qg9ax6hoA/default.jpg
    - https://i3.ytimg.com/vi/m2Qg9ax6hoA/1.jpg
    - https://i3.ytimg.com/vi/m2Qg9ax6hoA/2.jpg
    - https://i3.ytimg.com/vi/m2Qg9ax6hoA/3.jpg
---
