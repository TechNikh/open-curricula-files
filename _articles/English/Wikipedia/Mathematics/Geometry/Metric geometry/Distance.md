---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distance
offline_file: ""
offline_thumbnail: ""
uuid: fc92fc57-1b12-4a8a-a401-1d06c08efd57
updated: 1484309262
title: Distance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Greatcircle_Jetstream_routes.svg.png
tags:
    - Overview and definitions
    - Physical distances
    - Theoretical distances
    - Distance versus directed distance and displacement
    - Directed distance
    - Displacement
    - Mathematics
    - Geometry
    - Distance in Euclidean space
    - Variational formulation of distance
    - Generalization to higher-dimensional objects
    - Algebraic distance
    - General metric
    - Distances between sets and between a point and a set
    - Graph theory
    - Other mathematical "distances"
categories:
    - Metric geometry
---
Distance is a numerical description of how far apart objects are. In physics or everyday usage, distance may refer to a physical length, or an estimation based on other criteria (e.g. "two counties over"). In most cases, "distance from A to B" is interchangeable with "distance from B to A". In mathematics, a distance function or metric is a generalization of the concept of physical distance. A metric is a function that behaves according to a specific set of rules, and is a way of describing what it means for elements of some space to be "close to" or "far away from" each other.
