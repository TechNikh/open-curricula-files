---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/preamble
offline_file: ""
offline_thumbnail: ""
uuid: 764bd1ef-981c-4722-8559-d8bd5e5b95cd
updated: 1484310589
title: preamble
categories:
    - Dictionary
---
preamble
     n : a preliminary introduction to a statute or constitution
         (usually explaining its purpose)
     v : make a preliminary introduction, usually to a formal
         document
