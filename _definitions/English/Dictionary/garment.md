---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/garment
offline_file: ""
offline_thumbnail: ""
uuid: e8ac050f-03ff-4680-aeb3-b777fd310494
updated: 1484310459
title: garment
categories:
    - Dictionary
---
garment
     n : an article of clothing; "garments of the finest silk"
     v : provide with clothes or put clothes on; "Parents must feed
         and dress their child" [syn: {dress}, {clothe}, {enclothe},
          {garb}, {raiment}, {tog}, {habilitate}, {fit out}, {apparel}]
         [ant: {undress}]
