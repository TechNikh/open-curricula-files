---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imminent
offline_file: ""
offline_thumbnail: ""
uuid: 83eb99a1-7797-41d9-a7c7-ab0f81d0548b
updated: 1484310178
title: imminent
categories:
    - Dictionary
---
imminent
     adj : close in time; about to occur; "retribution is at hand";
           "some people believe the day of judgment is close at
           hand"; "in imminent danger"; "his impending retirement"
           [syn: {at hand(p)}, {close at hand(p)}, {impending}]
