---
version: 1
type: video
provider: YouTube
id: nri6d415EJE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e854e79f-43e5-4fbc-9222-51903ba3342c
updated: 1486069607
title: Addition and subtraction within 10
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/nri6d415EJE/default.jpg
    - https://i3.ytimg.com/vi/nri6d415EJE/1.jpg
    - https://i3.ytimg.com/vi/nri6d415EJE/2.jpg
    - https://i3.ytimg.com/vi/nri6d415EJE/3.jpg
---
