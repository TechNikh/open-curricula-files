---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/experiment
offline_file: ""
offline_thumbnail: ""
uuid: 78c38307-02e0-44d2-ae12-685b151d978e
updated: 1484310341
title: experiment
categories:
    - Dictionary
---
experiment
     n 1: the act of conducting a controlled test or investigation
          [syn: {experimentation}]
     2: the testing of an idea; "it was an experiment in living";
        "not all experimentation is done in laboratories" [syn: {experimentation}]
     3: a venture at something new or different; "as an experiment
        he decided to grow a beard"
     v 1: to conduct a test or investigation; "We are experimenting
          with the new drug in order to fight this disease"
     2: try something new, as in order to gain experience; "Students
        experiment sexually"; "The composer experimented with a
        new style" [syn: {try out}]
