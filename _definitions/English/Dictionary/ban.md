---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ban
offline_file: ""
offline_thumbnail: ""
uuid: 82df79dc-99ab-42ee-a8b0-ba76273c6c36
updated: 1484310464
title: ban
categories:
    - Dictionary
---
ban
     n 1: a decree that prohibits something [syn: {prohibition}, {proscription}]
     2: 100 bani equal 1 leu
     3: 100 bani equal 1 leu
     4: an official prohibition or edict against something [syn: {banning},
         {forbiddance}, {forbidding}]
     5: a bachelor's degree in nursing [syn: {Bachelor of Arts in
        Nursing}]
     v 1: prohibit especially by legal means or social pressure;
          "Smoking is banned in this building"
     2: forbid the public distribution of ( a movie or a newspaper)
        [syn: {censor}]
     3: ban from a place of residence, as for punishment [syn: {banish}]
     4: expel from a community or group [syn: {banish}, {ostracize},
         ...
