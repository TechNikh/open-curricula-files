---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Finite_topological_space
offline_file: ""
offline_thumbnail: ""
uuid: def11b8d-8633-4e65-a13d-39ca9738acfa
updated: 1484308580
title: Finite topological space
tags:
    - Topologies on a finite set
    - As a bounded sublattice
    - Specialization preorder
    - Examples
    - 0 or 1 points
    - 2 points
    - 3 points
    - 4 points
    - Properties
    - Compactness and countability
    - Separation axioms
    - Connectivity
    - Additional structure
    - Algebraic topology
    - Number of topologies on a finite set
categories:
    - Combinatorics
---
In mathematics, a finite topological space is a topological space for which the underlying point set is finite. That is, it is a topological space for which there are only finitely many points.
