---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sparse_ruler
offline_file: ""
offline_thumbnail: ""
uuid: eae07640-b538-44b3-84ca-f5f7bcb0d5ec
updated: 1484308595
title: Sparse ruler
tags:
    - Wichmann rulers
    - Examples
    - Incomplete sparse rulers
categories:
    - Combinatorics
---
A sparse ruler is a ruler in which some of the distance marks may be missing. More abstractly, a sparse ruler of length 
  
    
      
        L
      
    
    {\displaystyle L}
  
 with 
  
    
      
        m
      
    
    {\displaystyle m}
  
 marks is a sequence of integers 
  
    
      
        
          a
          
            1
          
        
        ,
        
          a
          
            2
          
        
        ,
        .
        .
        .
        ,
        
          a
          
            m
          
        
      
    
    {\displaystyle a_{1},a_{2},...,a_{m}}
  
 where 
  
    
      
        0
        =
        
          a
          
         ...
