---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Recursive_indexing
offline_file: ""
offline_thumbnail: ""
uuid: e9da4495-3425-4715-82ff-91090c50e6f4
updated: 1484308558
title: Recursive indexing
tags:
    - Encoding
    - Example
    - Decoding
    - Example
    - Uses
categories:
    - Coding theory
---
When number (generally large number) is represented in a finite alphabet set, and it cannot be represented by just one member of the set, Recursive indexing is used.
