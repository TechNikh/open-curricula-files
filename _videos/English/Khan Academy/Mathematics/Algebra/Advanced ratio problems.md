---
version: 1
type: video
provider: YouTube
id: PASSD2OcU0c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Advanced%20ratio%20problems.webm"
offline_file: ""
offline_thumbnail: ""
uuid: caff6341-6bc6-415f-838d-03b370256c77
updated: 1486069575
title: Advanced ratio problems
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/PASSD2OcU0c/default.jpg
    - https://i3.ytimg.com/vi/PASSD2OcU0c/1.jpg
    - https://i3.ytimg.com/vi/PASSD2OcU0c/2.jpg
    - https://i3.ytimg.com/vi/PASSD2OcU0c/3.jpg
---
More advanced ratio problems
More free lessons at: http://www.khanacademy.org/video?v=PASSD2OcU0c
