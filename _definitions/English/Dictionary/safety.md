---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/safety
offline_file: ""
offline_thumbnail: ""
uuid: d40cde49-3b62-4639-ac2b-60c76d2fa0df
updated: 1484310441
title: safety
categories:
    - Dictionary
---
safety
     n 1: the state of being certain that adverse effects will not be
          caused by some agent under defined conditions; "insure
          the safety of the children"; "the reciprocal of safety
          is risk" [ant: {danger}]
     2: a safe place; "He ran to safety" [syn: {refuge}]
     3: a device designed to prevent injury [syn: {guard}, {safety
        device}]
     4: (baseball) the successful act of striking a baseball in such
        a way that the batter reaches base safely [syn: {base hit},
         {bingle}]
     5: contraceptive device consisting of a thin rubber or latex
        sheath worn over the penis during intercourse [syn: {condom},
         {rubber}, ...
