---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wake
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484609761
title: wake
categories:
    - Dictionary
---
wake
     n 1: the consequences of an event (especially a catastrophic
          event); "the aftermath of war"; "in the wake of the
          accident no one knew how many had been injured" [syn: {aftermath},
           {backwash}]
     2: an island in the western Pacific between Guam and Hawaii
        [syn: {Wake Island}]
     3: the wave that spreads behind a boat as it moves forward;
        "the motorboat's wake capsized the canoe" [syn: {backwash}]
     4: a vigil held over a corpse the night before burial; "there's
        no weeping at an Irish wake" [syn: {viewing}]
     v 1: be awake, be alert, be there [ant: {sleep}]
     2: stop sleeping; "She woke up to the sound of the alarm ...
