---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shun
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484486041
title: shun
categories:
    - Dictionary
---
shun
     v 1: avoid and stay away from deliberately; stay clear of [syn: {eschew}]
     2: expel from a community or group [syn: {banish}, {ban}, {ostracize},
         {ostracise}, {cast out}, {blackball}]
     [also: {shunning}, {shunned}]
