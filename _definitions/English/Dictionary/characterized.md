---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/characterized
offline_file: ""
offline_thumbnail: ""
uuid: 5ef99572-4d29-4776-aec9-850f376d8698
updated: 1484310194
title: characterized
categories:
    - Dictionary
---
characterized
     adj : of the meaning of words or concepts; stated precisely [syn:
           {characterised}]
