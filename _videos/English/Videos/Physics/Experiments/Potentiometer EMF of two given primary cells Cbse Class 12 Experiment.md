---
version: 1
type: video
provider: YouTube
id: 8CSNlc2Xy68
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ab5ef7ad-d81d-44a3-8c2b-a09f51818ced
updated: 1486069636
title: >
    Potentiometer EMF of two given primary cells Cbse Class 12
    Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/8CSNlc2Xy68/default.jpg
    - https://i3.ytimg.com/vi/8CSNlc2Xy68/1.jpg
    - https://i3.ytimg.com/vi/8CSNlc2Xy68/2.jpg
    - https://i3.ytimg.com/vi/8CSNlc2Xy68/3.jpg
---
