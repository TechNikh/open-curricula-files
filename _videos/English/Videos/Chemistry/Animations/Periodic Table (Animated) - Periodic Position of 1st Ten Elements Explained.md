---
version: 1
type: video
provider: YouTube
id: e6cmGw03F80
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Periodic%20Table%20%28Animated%29%20-%20Periodic%20Position%20of%201st%20Ten%20Elements%20Explained.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 11910723-62ed-4326-b80d-4beec5c3cac1
updated: 1486069662
title: 'Periodic Table (Animated) - Periodic Position of 1st Ten Elements Explained'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/e6cmGw03F80/default.jpg
    - https://i3.ytimg.com/vi/e6cmGw03F80/1.jpg
    - https://i3.ytimg.com/vi/e6cmGw03F80/2.jpg
    - https://i3.ytimg.com/vi/e6cmGw03F80/3.jpg
---
