---
version: 1
type: video
provider: YouTube
id: Tq0mCZxjKXk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2b805e44-23ab-4adf-84b5-666c2ea82889
updated: 1486069611
title: Solving and graphing linear inequalities in two variables 1
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Tq0mCZxjKXk/default.jpg
    - https://i3.ytimg.com/vi/Tq0mCZxjKXk/1.jpg
    - https://i3.ytimg.com/vi/Tq0mCZxjKXk/2.jpg
    - https://i3.ytimg.com/vi/Tq0mCZxjKXk/3.jpg
---
