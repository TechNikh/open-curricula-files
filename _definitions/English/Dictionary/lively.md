---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lively
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484417101
title: lively
categories:
    - Dictionary
---
lively
     adj 1: full of life and energy; "a lively discussion"; "lively and
            attractive parents"; "a lively party" [ant: {dull}]
     2: full of zest or vigor; "a racy literary style" [syn: {racy}]
     3: quick and energetic; "a brisk walk in the park"; "a lively
        gait"; "a merry chase"; "traveling at a rattling rate"; "a
        snappy pace"; "a spanking breeze" [syn: {brisk}, {merry},
        {rattling}, {snappy}, {spanking}, {zippy}]
     4: rebounds readily; "clean bouncy hair"; "a lively tennis
        ball"; "as resiliant as seasoned hickory"; "springy turf"
        [syn: {bouncy}, {live}, {resilient}, {springy}, {whippy}]
     5: filled with events or activity; ...
