---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/method
offline_file: ""
offline_thumbnail: ""
uuid: 1e000e0b-bab7-47b2-a473-ecb109de8cdc
updated: 1484310313
title: method
categories:
    - Dictionary
---
method
     n : a way of doing something, especially a systematic way;
         implies an orderly logical arrangement (usually in steps)
