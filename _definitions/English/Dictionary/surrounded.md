---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surrounded
offline_file: ""
offline_thumbnail: ""
uuid: e4c46df2-3236-43ec-95ed-43e050d544d4
updated: 1484310405
title: surrounded
categories:
    - Dictionary
---
surrounded
     adj : confined on all sides; "a camp surrounded by enemies"; "the
           encircled pioneers" [syn: {encircled}]
