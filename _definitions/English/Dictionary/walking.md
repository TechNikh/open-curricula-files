---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/walking
offline_file: ""
offline_thumbnail: ""
uuid: 9c749efa-f22d-456a-84ef-26874448b0b2
updated: 1484310212
title: walking
categories:
    - Dictionary
---
walking
     adj : close enough to be walked to; "walking distance"; "the
           factory with the big parking lot...is more convenient
           than the walk-to factory" [syn: {walk-to(a)}, {walking(a)}]
     n : the act of traveling by foot; "walking is a healthy form of
         exercise" [syn: {walk}]
