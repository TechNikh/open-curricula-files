---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apologize
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484622421
title: apologize
categories:
    - Dictionary
---
apologize
     v 1: acknowledge faults or shortcomings or failing; "I apologized
          for being late"; "He apologized for the many typoes"
          [syn: {apologise}]
     2: defend, explain, clear away, or make excuses for by
        reasoning; "rationalize the child's seemingly crazy
        behavior"; "he rationalized his lack of success" [syn: {apologise},
         {excuse}, {justify}, {rationalize}, {rationalise}]
