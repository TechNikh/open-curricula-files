---
version: 1
type: video
provider: YouTube
id: 0LM6wPpypxM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Conjugate%20acids%20and%20bases.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a31362f8-a008-4ae7-969c-dc7002e5ee67
updated: 1486069583
title: Conjugate acids and bases
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/0LM6wPpypxM/default.jpg
    - https://i3.ytimg.com/vi/0LM6wPpypxM/1.jpg
    - https://i3.ytimg.com/vi/0LM6wPpypxM/2.jpg
    - https://i3.ytimg.com/vi/0LM6wPpypxM/3.jpg
---
Introduction to conjugate acids and bases. Created by Sal Khan.

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
