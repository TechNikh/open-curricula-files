---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poincar%C3%A9_duality'
offline_file: ""
offline_thumbnail: ""
uuid: d26f3e67-14ee-40b1-9ec3-9c29121ad271
updated: 1484309239
title: Poincaré duality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Dually007.png
tags:
    - History
    - Modern formulation
    - Dual cell structures
    - Naturality
    - Bilinear pairings formulation
    - Thom Isomorphism Formulation
    - Generalizations and related results
categories:
    - Duality theories
---
In mathematics, the Poincaré duality theorem, named after Henri Poincaré, is a basic result on the structure of the homology and cohomology groups of manifolds. It states that if M is an n-dimensional oriented closed manifold (compact and without boundary), then the kth cohomology group of M is isomorphic to the (n − k)th homology group of M, for all integers k
