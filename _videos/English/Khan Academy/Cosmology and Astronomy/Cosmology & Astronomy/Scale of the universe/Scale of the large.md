---
version: 1
type: video
provider: YouTube
id: 5FEjrStgcF8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Scale%20of%20the%20universe/Scale%20of%20the%20large.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2a6fadaf-ea98-403f-9656-356dc3bde712
updated: 1486069686
title: Scale of the large
categories:
    - Scale of the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/5FEjrStgcF8/default.jpg
    - https://i3.ytimg.com/vi/5FEjrStgcF8/1.jpg
    - https://i3.ytimg.com/vi/5FEjrStgcF8/2.jpg
    - https://i3.ytimg.com/vi/5FEjrStgcF8/3.jpg
---
