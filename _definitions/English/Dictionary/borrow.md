---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/borrow
offline_file: ""
offline_thumbnail: ""
uuid: 709d62f2-5b7f-4b6e-801c-00ab3e4fe8d3
updated: 1484310459
title: borrow
categories:
    - Dictionary
---
borrow
     v 1: get temporarily; "May I borrow your lawn mower?" [ant: {lend}]
     2: take up and practice as one's own [syn: {adopt}, {take over},
         {take up}]
