---
version: 1
type: article
id: https://en.wikipedia.org/wiki/You-Dong_Liang
offline_file: ""
offline_thumbnail: ""
uuid: 0d06bb9f-56b3-4d94-9d16-6a46eff97d81
updated: 1484309090
title: You-Dong Liang
categories:
    - Geometers
---
You-Dong Liang was born on July 19, 1935, in Fuzhou, Fujian Province, China. Liang pursued his graduate degree in Fudan University, where he worked under the supervision of Professor Su Buqing and specialized in geometric theory. After graduating in 1960, he joined the mathematics teaching faculty at Zhejiang University, where he actively promoted the development of geometric design and graphics. From 1984-1990, he was the chairman of the mathematics department, and on several occasions, was a visiting scholar and visiting professor at the University of California at Berkeley, University of Utah, and University of Berlin. Liang helped form the Computational Geometry Collaborative Group in ...
