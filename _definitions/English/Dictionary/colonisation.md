---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/colonisation
offline_file: ""
offline_thumbnail: ""
uuid: 6424542f-6d67-437f-986b-36245f5724fd
updated: 1484310471
title: colonisation
categories:
    - Dictionary
---
colonisation
     n : the act of colonizing; the establishment of colonies; "the
         British colonization of America" [syn: {colonization}, {settlement}]
