---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mills
offline_file: ""
offline_thumbnail: ""
uuid: 569abed9-cb4b-4e42-a02f-93fd489d3df2
updated: 1484310589
title: mills
categories:
    - Dictionary
---
Mills
     n : United States architect who was the presidentially appointed
         architect of Washington D.C. (1781-1855) [syn: {Robert
         Mills}]
