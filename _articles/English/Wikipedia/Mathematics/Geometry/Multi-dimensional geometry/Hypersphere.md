---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hypersphere
offline_file: ""
offline_thumbnail: ""
uuid: 3a44f231-e91e-4d0c-9907-1b058b1306a1
updated: 1484309283
title: Hypersphere
categories:
    - Multi-dimensional geometry
---
In geometry of higher dimensions, a hypersphere is the set of points at a constant distance from a given point called its center. It is a manifold of codimension one (i.e. with one dimension less than that of the ambient space). As the radius increases the curvature of the hypersphere decreases; in the limit a hypersphere approaches the zero curvature of a hyperplane. Hyperplanes and hyperspheres are examples of hypersurfaces.
