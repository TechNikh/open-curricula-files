---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Benacerraf%27s_identification_problem'
offline_file: ""
offline_thumbnail: ""
uuid: 415501df-7559-4898-8b32-9387e0068ebc
updated: 1484309501
title: "Benacerraf's identification problem"
tags:
    - Historical motivations
    - Description
    - Bibliography
categories:
    - Set theory
---
Benacerraf's identification problem is a philosophical argument, developed by Paul Benacerraf, against set-theoretic platonism.[1] In 1965, Benacerraf published a paradigm changing article entitled "What Numbers Could Not Be".[1][2] Historically, the work became a significant catalyst in motivating the development of structuralism in the philosophy of mathematics.[3] The identification problem argues that there exists a fundamental problem in reducing natural numbers to pure sets. Since there exists an infinite number of ways of identifying the natural numbers with pure sets, no particular set-theoretic method can be determined as the "true" reduction. Benacerraf infers that any attempt to ...
