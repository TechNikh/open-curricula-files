---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sequential_dynamical_system
offline_file: ""
offline_thumbnail: ""
uuid: 7f0b42f6-3fdf-49a8-8259-fe9ce437e0a7
updated: 1484308585
title: Sequential dynamical system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-SDSphase1.png
tags:
    - Definition
    - Example
categories:
    - Combinatorics
---
Sequential dynamical systems (SDSs) are a class of graph dynamical systems. They are discrete dynamical systems which generalize many aspects of for example classical cellular automata, and they provide a framework for studying asynchronous processes over graphs. The analysis of SDSs uses techniques from combinatorics, abstract algebra, graph theory, dynamical systems and probability theory.
