---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Medicinemen
offline_file: ""
offline_thumbnail: ""
uuid: dd1462da-9738-465f-b45b-3b29563b2e3e
updated: 1484308470
title: Medicine man
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ojibweherbalistmedicine.png
tags:
    - The medicine man and woman in North America
    - Cultural context
    - Medicine Men and Women in the Cherokee Tribe
    - History
    - Training
    - Modern Day
    - Notes
categories:
    - Uncategorized
---
A medicine man or medicine woman is a traditional healer and spiritual leader who serves a community of Indigenous people. Individual cultures have their own names, in their respective Indigenous languages, for the spiritual healers and ceremonial leaders in their particular cultures.
