---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bioaccumulation
offline_file: ""
offline_thumbnail: ""
uuid: 63b76477-7a06-4535-bd95-4e6c692ddf72
updated: 1484308337
title: Bioaccumulation
tags:
    - Examples
categories:
    - Uncategorized
---
Bioaccumulation refers to the accumulation of substances, such as pesticides, or other chemicals in an organism.[1] Bioaccumulation occurs when an organism absorbs a - possibly toxic - substance at a rate faster than that at which the substance is lost by catabolism and excretion. Thus, the longer the biological half-life of a toxic substance the greater the risk of chronic poisoning, even if environmental levels of the toxin are not very high.[2] Bioaccumulation, for example in fish, can be predicted by models.[3] Hypotheses for molecular size cutoff criteria for use as bioaccumulation potential indicators are not supported by data.[4] Biotransformation can strongly modify bioaccumulation ...
