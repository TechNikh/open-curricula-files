---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometric_combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: dc7c1869-c1c3-482a-9baf-ef624b44e7b8
updated: 1484308590
title: Geometric combinatorics
categories:
    - Combinatorics
---
Geometric combinatorics is a branch of mathematics in general and combinatorics in particular. It includes a number of subareas such as polyhedral combinatorics (the study of faces of convex polyhedra), convex geometry (the study of convex sets, in particular combinatorics of their intersections), and discrete geometry, which in turn has many applications to computational geometry. Other important areas include metric geometry of polyhedra, such as the Cauchy theorem on rigidity of convex polytopes. The study of regular polytopes, Archimedean solids, and kissing numbers is also a part of geometric combinatorics. Special polytopes are also considered, such as the permutohedron, associahedron ...
