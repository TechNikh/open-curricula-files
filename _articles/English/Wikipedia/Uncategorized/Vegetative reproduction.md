---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vegetative_propagation
offline_file: ""
offline_thumbnail: ""
uuid: 4df49e94-92dc-4eba-bd18-303788278baa
updated: 1484308366
title: Vegetative reproduction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kalanchoe_veg.jpg
tags:
    - Types
    - Vegetative structures
    - Natural vegetative propagation
    - Artificial vegetative propagation
    - Cultivated plants propagated by vegetative methods
categories:
    - Uncategorized
---
Vegetative reproduction (vegetative propagation, vegetative multiplication, vegetative cloning) is a form of asexual reproduction in plants. It is a process by which new organisms arise without production of seeds or spores.[citation needed] It can occur naturally or be induced by horticulturists.
