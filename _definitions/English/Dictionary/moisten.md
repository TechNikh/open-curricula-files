---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moisten
offline_file: ""
offline_thumbnail: ""
uuid: e7f7b025-46fd-4793-a7df-bec07050c1e3
updated: 1484310335
title: moisten
categories:
    - Dictionary
---
moisten
     v 1: make moist; "The dew moistened the meadows" [syn: {wash}, {dampen}]
     2: moisten with fine drops; "drizzle the meat with melted
        butter" [syn: {drizzle}]
