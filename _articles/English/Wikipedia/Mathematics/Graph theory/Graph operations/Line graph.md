---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Line_graph
offline_file: ""
offline_thumbnail: ""
uuid: d165110e-c3d8-40a9-9b49-2c049c96014a
updated: 1484309448
title: Line graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/120px-Diamond_graph.svg.png
tags:
    - Formal definition
    - Example
    - Properties
    - Translated properties of the underlying graph
    - Whitney isomorphism theorem
    - Strongly regular and perfect line graphs
    - Other related graph families
    - Characterization and recognition
    - Clique partition
    - Forbidden subgraphs
    - Algorithms
    - Iterating the line graph operator
    - Generalizations
    - Medial graphs and convex polyhedra
    - Total graphs
    - Multigraphs
    - Line digraphs
    - Weighted line graphs
    - Line graphs of hypergraphs
    - Notes
categories:
    - Graph operations
---
In the mathematical discipline of graph theory, the line graph of an undirected graph G is another graph L(G) that represents the adjacencies between edges of G. The name line graph comes from a paper by Harary & Norman (1960) although both Whitney (1932) and Krausz (1943) used the construction before this.[1] Other terms used for the line graph include the covering graph, the derivative, the edge-to-vertex dual, the conjugate, the representative graph, and the ϑ-obrazom,[1] as well as the edge graph, the interchange graph, the adjoint graph, and the derived graph.[2]
