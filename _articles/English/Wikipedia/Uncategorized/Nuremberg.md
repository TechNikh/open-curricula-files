---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nuremberg
offline_file: ""
offline_thumbnail: ""
uuid: ca0416bb-0742-4c8e-8942-0f792b37c04d
updated: 1484308482
title: Nuremberg
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Nuremberg_chronicles_-_Nuremberga.png
tags:
    - History
    - Middle Ages
    - Early modern age
    - After the Great French War
    - Nazi era
    - Nuremberg trials
    - Geography and Climate
    - Demographics
    - Economy
    - culture
categories:
    - Uncategorized
---
Nuremberg (/ˈnjʊərəmbɜːrɡ/; German: Nürnberg; pronounced [ˈnʏɐ̯nbɛɐ̯k] ( listen)[2]) is a city on the river Pegnitz and the Rhine–Main–Danube Canal in the German state of Bavaria, in the administrative region of Middle Franconia, about 170 kilometres (110 mi) north of Munich. It is the second-largest city in Bavaria (after Munich), and the largest in Franconia (Franken). The population as of February 2015, is 517,498, which makes it Germany's fourteenth-largest city. The urban area also includes Fürth, Erlangen and Schwabach with a total population of 763,854. The "European Metropolitan Area Nuremberg" has ca. 3.5 million inhabitants.[3]
