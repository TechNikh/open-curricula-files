---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/superconductivity
offline_file: ""
offline_thumbnail: ""
uuid: ddc7be4b-1e72-4813-8ad6-ee1442c23a4c
updated: 1484310431
title: superconductivity
categories:
    - Dictionary
---
superconductivity
     n : the disappearance of electrical resistance at very low
         temperatures
