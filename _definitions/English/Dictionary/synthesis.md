---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/synthesis
offline_file: ""
offline_thumbnail: ""
uuid: 90e0b3aa-a04c-4533-86cc-c210a85da1c7
updated: 1484310264
title: synthesis
categories:
    - Dictionary
---
synthesis
     n 1: the process of producing a chemical compound (usually by the
          union of simpler chemical compounds)
     2: the combination of ideas into a complex whole [syn: {synthetic
        thinking}] [ant: {analysis}]
     3: reasoning from the general to the particular (or from cause
        to effect) [syn: {deduction}, {deductive reasoning}]
     [also: {syntheses} (pl)]
