---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suspension
offline_file: ""
offline_thumbnail: ""
uuid: 13149188-0470-4bb7-978e-6a9b2680ebe4
updated: 1484310426
title: suspension
categories:
    - Dictionary
---
suspension
     n 1: a mixture in which fine particles are suspended in a fluid
          where they are supported by buoyancy
     2: a time interval during which there is a temporary cessation
        of something [syn: {pause}, {intermission}, {break}, {interruption}]
     3: temporary cessation or suspension [syn: {abeyance}]
     4: an interruption in the intensity or amount of something
        [syn: {respite}, {reprieve}, {hiatus}, {abatement}]
     5: a mechanical system of springs or shock absorbers connecting
        the wheels and axles to the chassis of a wheeled vehicle
        [syn: {suspension system}]
     6: the act of suspending something (hanging it from above so it
      ...
