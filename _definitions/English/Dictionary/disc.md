---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disc
offline_file: ""
offline_thumbnail: ""
uuid: 6c4b4540-918f-485a-9119-02252704a3e5
updated: 1484310210
title: disc
categories:
    - Dictionary
---
disc
     n 1: sound recording consisting of a disc with continuous
          grooves; formerly used to reproduce music by rotating
          while a phonograph needle tracked in the grooves [syn: {phonograph
          record}, {phonograph recording}, {record}, {disk}, {platter}]
     2: something with a round shape like a flat circular plate
        [syn: {disk}, {saucer}]
     3: (computer science) a memory device consisting of a flat disk
        covered with a magnetic coating on which information is
        stored [syn: {magnetic disk}, {magnetic disc}, {disk}]
     4: a flat circular plate [syn: {disk}]
