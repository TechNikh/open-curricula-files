---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/code
offline_file: ""
offline_thumbnail: ""
uuid: 1470a19e-1a17-4855-9f6f-e37803a34d0a
updated: 1484310459
title: code
categories:
    - Dictionary
---
code
     n 1: a set of rules or principles or laws (especially written
          ones) [syn: {codification}]
     2: a coding system used for transmitting messages requiring
        brevity or secrecy
     3: (computer science) the symbolic arrangement of data or
        instructions in a computer program or the set of such
        instructions [syn: {computer code}]
     v 1: attach a code to; "Code the pieces with numbers so that you
          can identify them later"
     2: convert ordinary language into code; "We should encode the
        message for security reasons" [syn: {encode}, {encipher},
        {cipher}, {cypher}, {encrypt}, {inscribe}, {write in code}]
        [ant: ...
