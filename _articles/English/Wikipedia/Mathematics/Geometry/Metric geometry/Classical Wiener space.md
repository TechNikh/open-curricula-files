---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Classical_Wiener_space
offline_file: ""
offline_thumbnail: ""
uuid: 36ce906c-6936-4d82-8a88-7972f4fae4ec
updated: 1484309261
title: Classical Wiener space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Norbert_wiener.jpg
tags:
    - Definition
    - Properties of classical Wiener space
    - Uniform topology
    - Separability and completeness
    - Tightness in classical Wiener space
    - Classical Wiener measure
categories:
    - Metric geometry
---
In mathematics, classical Wiener space is the collection of all continuous functions on a given domain (usually a sub-interval of the real line), taking values in a metric space (usually n-dimensional Euclidean space). Classical Wiener space is useful in the study of stochastic processes whose sample paths are continuous functions. It is named after the American mathematician Norbert Wiener.
