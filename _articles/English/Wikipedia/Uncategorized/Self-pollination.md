---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-pollinating
offline_file: ""
offline_thumbnail: ""
uuid: 49908c05-477c-403e-ae83-7ee65c9aa4d6
updated: 1484308347
title: Self-pollination
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ophrys_apifera_flower2.jpg
tags:
    - Occurrence
    - Types of flowers that self pollinate
    - Advantages of self-pollination
    - Disadvantages of self-pollination
    - Mixed mating
    - Self-pollinating species
    - Paphiopedilum parishii
    - Holcoglossum amesianum
    - Caulokaempferia coenobialis
    - Capsella rubella
    - Arabidopsis thaliana
    - Possible long-term benefit of meiosis
categories:
    - Uncategorized
---
Self-pollination is when pollen from the same plant arrives at the stigma of a flower (in flowering plants) or at the ovule (in Gymnosperms). There are two types of self-pollination: In autogamy, pollen is transferred to the stigma of the same flower. In geitonogamy, pollen is transferred from the anther of one flower to the stigma of another flower on the same flowering plant, or from microsporangium to ovule within a single (monoecious) Gymnosperm. Some plants have mechanisms that ensure autogamy, such as flowers that do not open (cleistogamy), or stamens that move to come into contact with the stigma.
