---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/police
offline_file: ""
offline_thumbnail: ""
uuid: 8966d634-9c8b-4e18-8002-89173a5137a8
updated: 1484310424
title: police
categories:
    - Dictionary
---
police
     n : the force of policemen and officers; "the law came looking
         for him" [syn: {police force}, {constabulary}, {law}]
     v : maintain the security of by carrying out a control [syn: {patrol}]
