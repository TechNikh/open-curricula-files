---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supervisor
offline_file: ""
offline_thumbnail: ""
uuid: 06723574-dccf-409d-abd8-67fa9449e2a7
updated: 1484310521
title: supervisor
categories:
    - Dictionary
---
supervisor
     n 1: one who supervises or has charge and direction of
     2: a program that controls the execution of other programs
        [syn: {supervisory program}, {executive program}]
