---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prove
offline_file: ""
offline_thumbnail: ""
uuid: e22ad040-bbb0-4f37-aaa8-3b071e5a4a8a
updated: 1484310319
title: prove
categories:
    - Dictionary
---
prove
     v 1: be shown or be found to be; "She proved to be right"; "The
          medicine turned out to save her life"; "She turned up
          HIV positive" [syn: {turn out}, {turn up}]
     2: establish the validity of something, as by an example,
        explanation or experiment; "The experiment demonstrated
        the instability of the compound"; "The mathematician
        showed the validity of the conjecture" [syn: {demonstrate},
         {establish}, {show}, {shew}] [ant: {disprove}]
     3: provide evidence for; "The blood test showed that he was the
        father"; "Her behavior testified to her incompetence"
        [syn: {testify}, {bear witness}, {evidence}, {show}]
    ...
