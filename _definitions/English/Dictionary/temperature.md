---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/temperature
offline_file: ""
offline_thumbnail: ""
uuid: 762087cd-74d9-4e91-b002-b63f47c2081f
updated: 1484310346
title: temperature
categories:
    - Dictionary
---
temperature
     n 1: the degree of hotness or coldness of a body or environment
          (corresponding to its molecular activity)
     2: the somatic sensation of cold or heat
