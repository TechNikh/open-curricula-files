---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/homo
offline_file: ""
offline_thumbnail: ""
uuid: cdfda105-6a32-4b4b-ae5f-fb5894d21c91
updated: 1484310277
title: homo
categories:
    - Dictionary
---
homo
     n 1: someone who practices homosexuality; having a sexual
          attraction to persons of the same sex [syn: {homosexual},
           {gay}]
     2: any living or extinct member of the family Hominidae [syn: {man},
         {human being}, {human}]
