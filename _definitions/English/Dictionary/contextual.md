---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contextual
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484497381
title: contextual
categories:
    - Dictionary
---
contextual
     adj : relating to or determined by or in context; "contextual
           information"
