---
version: 1
type: video
provider: YouTube
id: I6hz8mhaRB0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Understanding%20Logical%20Statements%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b60a1384-342c-45e5-8aa6-11ab7dddb22e
updated: 1486069563
title: Understanding Logical Statements 1
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/I6hz8mhaRB0/default.jpg
    - https://i3.ytimg.com/vi/I6hz8mhaRB0/1.jpg
    - https://i3.ytimg.com/vi/I6hz8mhaRB0/2.jpg
    - https://i3.ytimg.com/vi/I6hz8mhaRB0/3.jpg
---
