---
version: 1
type: video
provider: YouTube
id: sKsapSEuJz8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 594ddefd-5259-4701-958e-79f9ab3c86a0
updated: 1486069651
title: Katana Kut
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/sKsapSEuJz8/default.jpg
    - https://i3.ytimg.com/vi/sKsapSEuJz8/1.jpg
    - https://i3.ytimg.com/vi/sKsapSEuJz8/2.jpg
    - https://i3.ytimg.com/vi/sKsapSEuJz8/3.jpg
---
