---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homodyne_detection
offline_file: ""
offline_thumbnail: ""
uuid: 62b307cf-b957-450b-896d-270e5047b1fa
updated: 1484308893
title: Homodyne detection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-HomodyneDetection2.png
tags:
    - In optics
    - Radio technology
    - Applications
categories:
    - Waves
---
Homodyne detection is a method of detecting frequency-modulated radiation by non-linear mixing with radiation of a reference frequency, the same principle as for heterodyne detection.[clarification needed]
