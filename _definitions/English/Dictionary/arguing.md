---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arguing
offline_file: ""
offline_thumbnail: ""
uuid: d6cfbf83-72e9-405a-8bac-e6769e07bcd9
updated: 1484310369
title: arguing
categories:
    - Dictionary
---
arguing
     n : a contentious speech act; a dispute where there is strong
         disagreement; "they were involved in a violent argument"
         [syn: {controversy}, {contention}, {contestation}, {disputation},
          {disceptation}, {tilt}, {argument}]
