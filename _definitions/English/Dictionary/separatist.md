---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/separatist
offline_file: ""
offline_thumbnail: ""
uuid: 48141607-d5ed-403a-a9e0-0b0b0d830af1
updated: 1484310587
title: separatist
categories:
    - Dictionary
---
separatist
     adj : having separated or advocating separation from another
           entity or policy or attitude; "a breakaway faction"
           [syn: {breakaway}, {fissiparous}]
     n : an advocate of secession or separation from a larger group
         (such as an established church or a national union) [syn:
          {separationist}]
