---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/till
offline_file: ""
offline_thumbnail: ""
uuid: ed233252-77e6-4b6e-b16a-57847fbbcd3d
updated: 1484310344
title: till
categories:
    - Dictionary
---
till
     n 1: unstratified soil deposited by a glacier; consists of sand
          and clay and gravel and boulders mixed together [syn: {boulder
          clay}]
     2: a treasury for government funds [syn: {public treasury}, {trough}]
     3: a strongbox for holding cash [syn: {cashbox}, {money box}]
     v : work land as by ploughing, harrowing, and manuring, in order
         to make it ready for cultivation; "till the soil"
