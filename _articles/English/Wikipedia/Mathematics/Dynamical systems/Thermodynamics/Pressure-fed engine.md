---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pressure-fed_engine
offline_file: ""
offline_thumbnail: ""
uuid: e4fc8f26-4766-4ae9-8fdb-d71735e6d1a3
updated: 1484308859
title: Pressure-fed engine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Pressure_fed_rocket_cycle.png
categories:
    - Thermodynamics
---
The pressure-fed engine is a class of rocket engine designs. A separate gas supply, usually helium, pressurizes the propellant tanks to force fuel and oxidizer to the combustion chamber. To maintain adequate flow, the tank pressures must exceed the combustion chamber pressure.
