---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thimmapuram
offline_file: ""
offline_thumbnail: ""
uuid: a466879d-bf76-4cbb-9470-ed8dc6b8084d
updated: 1484308451
title: Thimmapuram
categories:
    - Uncategorized
---
Thimmapuram is a village in Tuni Mandal in East Godavari District of Andhra Pradesh State, India. Its nearest town is Tuni which is 14 km far.Mostly people depend on farming and the nearby villages sell their produce using the railway station present.It is 1 km away from the village Tetagunta where it has better educational and other facilities. Kakinada and Rajahmundry are the nearby Cities to Thimmapuram.
