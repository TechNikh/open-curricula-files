---
version: 1
type: article
id: https://en.wikipedia.org/wiki/GIT_quotient
offline_file: ""
offline_thumbnail: ""
uuid: d74044e7-7ddd-458f-9d79-0fba28f0e981
updated: 1484309149
title: GIT quotient
categories:
    - Algebraic geometry
---
In algebraic geometry, an affine GIT quotient, or affine geometric invariant theory quotient, of an affine scheme 
  
    
      
        Spec
        ⁡
        A
      
    
    {\displaystyle \operatorname {Spec} A}
  
 with action by a group scheme G is the affine scheme 
  
    
      
        Spec
        ⁡
        (
        
          A
          
            G
          
        
        )
      
    
    {\displaystyle \operatorname {Spec} (A^{G})}
  
, the prime spectrum of the ring of invariants of A, and is denoted by 
  
    
      
        X
        
          /
        
        
        
          /
        
        G
      
    
    {\displaystyle X/\!/G}
  
. A GIT ...
