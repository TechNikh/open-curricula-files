---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamming_space
offline_file: ""
offline_thumbnail: ""
uuid: 37f53d04-f482-4d6f-a510-86e45ac4ba38
updated: 1484308553
title: Hamming space
categories:
    - Coding theory
---
In statistics and coding theory, a Hamming space is usually the set of all 
  
    
      
        
          2
          
            N
          
        
      
    
    {\displaystyle 2^{N}}
  
 binary strings of length N.[1][2] It is used in the theory of coding signals and transmission.
