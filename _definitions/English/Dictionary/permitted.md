---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/permitted
offline_file: ""
offline_thumbnail: ""
uuid: 363ba8a3-e133-4a4c-9549-5de5a6eaff55
updated: 1484310464
title: permitted
categories:
    - Dictionary
---
permitted
     adj : possible to allow; "a degree of freedom allowable among
           friends" [syn: {allowable}]
