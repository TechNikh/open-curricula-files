---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/zone
offline_file: ""
offline_thumbnail: ""
uuid: ebb4ddeb-ca36-4a25-968b-4c895c417ebf
updated: 1484310414
title: zone
categories:
    - Dictionary
---
zone
     n 1: a circumscribed geographical region characterized by some
          distinctive features
     2: any of the regions of the surface of the Earth loosely
        divided according to latitude or longitude [syn: {geographical
        zone}]
     3: an area or region distinguished from adjacent parts by a
        distinctive feature or characteristic
     4: (anatomy) any encircling or beltlike structure [syn: {zona}]
     v 1: regulate housing in; of certain areas of towns [syn: {district}]
     2: separate or apportion into sections; "partition a room off"
        [syn: {partition}]
