---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/element
offline_file: ""
offline_thumbnail: ""
uuid: bcd4c7da-0fe6-4613-ba58-870d9f211dc4
updated: 1484310206
title: element
categories:
    - Dictionary
---
element
     n 1: an abstract part of something; "jealousy was a component of
          his character"; "two constituents of a musical
          composition are melody and harmony"; "the grammatical
          elements of a sentence"; "a key factor in her success";
          "humor: an effective ingredient of a speech" [syn: {component},
           {constituent}, {factor}, {ingredient}]
     2: any of the more than 100 known substances (of which 92 occur
        naturally) that cannot be separated into simpler
        substances and that singly or in combination constitute
        all matter [syn: {chemical element}]
     3: an artifact that is one of the individual parts of which a
        ...
