---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/armed
offline_file: ""
offline_thumbnail: ""
uuid: f34312b3-6c48-424d-a1d2-4bb237d41142
updated: 1484310553
title: armed
categories:
    - Dictionary
---
armed
     adj 1: (used of persons or the military) characterized by having or
            bearing arms; "armed robbery" [ant: {unarmed}]
     2: having arms or arms as specified; used especially in
        combination; "the many-armed goddess Shiva" [ant: {armless}]
     3: used of plants and animals [ant: {unarmed}]
