---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cumin
offline_file: ""
offline_thumbnail: ""
uuid: 06a5663f-aad0-454c-ae7c-8f407d491d06
updated: 1484308378
title: Cumin
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sa_cumin_0.jpg
tags:
    - Etymology
    - Description
    - History
    - Cultivation and production
    - Cultivation areas
    - Climatic requirements
    - Cultivation parameters
    - Cultivation management
    - Breeding of cumin
    - Uses
    - Traditional uses
    - Volatiles
    - Nutritional value
    - Confusion with other spices
    - Aroma profile
    - Images
categories:
    - Uncategorized
---
Cumin (/ˈkjuːmᵻn/ or UK /ˈkʌmᵻn/, US /ˈkuːmᵻn/) (Cuminum cyminum) is a flowering plant in the family Apiaceae, native from the east Mediterranean to South Asia.
