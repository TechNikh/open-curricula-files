---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/herewith
offline_file: ""
offline_thumbnail: ""
uuid: 14683c8f-326c-47a3-abfd-1f2e01d28d40
updated: 1484310593
title: herewith
categories:
    - Dictionary
---
herewith
     adv : (formal) by means of this; "I hereby declare you man and
           wife" [syn: {hereby}]
