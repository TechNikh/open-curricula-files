---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Palani
offline_file: ""
offline_thumbnail: ""
uuid: 54a7f3fc-3e41-4b6f-b31e-6ce3a64ec2b5
updated: 1484308422
title: Palani
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Gopuramgold.JPG
tags:
    - Etymology
    - History
    - geography
    - Demographics
    - Temples
    - Transportation
    - Road
    - Rail
    - air
categories:
    - Uncategorized
---
(Tamil:பழநி) Palani (or Pazhani) is a town and a municipality in Dindigul district, Tamil Nadu, located about 100 km north-west of Madurai and 100 km South-east of Coimbatore and 60 km west of Dindigul. The Palani Murugan Temple dedicated to Hindu war God Kartikeya is situated on a hill overlooking the town. The temple is visited by more than 7 million pilgrims each year. As of 2011, the town had a population of 70,467.
