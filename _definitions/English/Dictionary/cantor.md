---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cantor
offline_file: ""
offline_thumbnail: ""
uuid: 049c57f5-678c-407a-89a3-3eb97972c370
updated: 1484310140
title: cantor
categories:
    - Dictionary
---
cantor
     n 1: the musical director of a choir [syn: {choirmaster}, {precentor}]
     2: the official of a synagogue who conducts the liturgical part
        of the service and sings or chants the prayers intended to
        be performed as solos [syn: {hazan}]
