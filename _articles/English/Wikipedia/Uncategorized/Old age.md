---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Senior_citizens
offline_file: ""
offline_thumbnail: ""
uuid: 8decbae4-5433-4481-8bab-808e7df3332d
updated: 1484308371
title: Old age
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Maes_Old_Woman_Dozing.jpg
tags:
    - Definitions
    - Official definitions
    - Sub-group definitions
    - Dimensions of old age
    - Senior citizen
    - Age qualifications
    - Marks of old age
    - Physical marks of old age
    - Mental marks of old age
    - Perceptions of old age
    - Old age from a middle-age perspective
    - Old age from an old-age perspective
    - Old age from society’s perspective
    - "Old people from society's perspective"
    - Old age from simulated perspective
    - Old age frailty
    - Prevalence of frailty
    - Markers of frailty
    - Misconceptions of frail people
    - Care and costs
    - Death and frailty
    - Religiosity in old age
    - Demographic changes
    - Psychosocial aspects
    - Theories of old age
    - Life expectancy
    - Old age benefits
    - 'Assistance: devices and personal'
    - Depictions in art
categories:
    - Uncategorized
---
Old age refers to ages nearing or surpassing the life expectancy of human beings, and is thus the end of the human life cycle. In October 2016, scientists identified the maximum human lifespan at an average age of 115, with an absolute upper limit of 125 years.[1][2] Terms and euphemisms for old people include, old people (worldwide usage), seniors (American usage), senior citizens (British and American usage), older adults (in the social sciences[3]), the elderly, and elders (in many cultures—including the cultures of aboriginal people).
