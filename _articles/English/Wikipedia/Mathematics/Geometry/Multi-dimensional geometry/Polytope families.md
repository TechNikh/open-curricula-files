---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polytope_families
offline_file: ""
offline_thumbnail: ""
uuid: 4c84d531-63fd-4e55-952c-8d844b38d5fb
updated: 1484309289
title: Polytope families
categories:
    - Multi-dimensional geometry
---
There are several families of symmetric polytopes with irreducible symmetry which have a member in more than one dimensionality. These are tabulated here with Petrie polygon projection graphs and Coxeter-Dynkin diagrams.
