---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inquire
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484578141
title: inquire
categories:
    - Dictionary
---
inquire
     v 1: inquire about; "I asked about their special today"; "He had
          to ask directions several times" [syn: {ask}, {enquire}]
     2: have a wish or desire to know something; "He wondered who
        had built this beautiful church" [syn: {wonder}, {enquire}]
     3: conduct an inquiry or investigation of; "The district
        attorney's office investigated reports of possible
        irregularities"; "inquire into the disappearance of the
        rich old lady" [syn: {investigate}, {enquire}]
