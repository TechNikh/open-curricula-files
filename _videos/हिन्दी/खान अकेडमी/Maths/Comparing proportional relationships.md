---
version: 1
type: video
provider: YouTube
id: NlpA31yz6N4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4df84cdc-5c8c-4b42-b201-e623d6240c23
updated: 1486069616
title: Comparing proportional relationships
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/NlpA31yz6N4/default.jpg
    - https://i3.ytimg.com/vi/NlpA31yz6N4/1.jpg
    - https://i3.ytimg.com/vi/NlpA31yz6N4/2.jpg
    - https://i3.ytimg.com/vi/NlpA31yz6N4/3.jpg
---
