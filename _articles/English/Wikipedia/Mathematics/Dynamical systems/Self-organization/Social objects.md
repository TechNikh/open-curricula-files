---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Social_objects
offline_file: ""
offline_thumbnail: ""
uuid: 8bee6b7b-c232-45ad-b434-fa6b8ce994a6
updated: 1484308823
title: Social objects
categories:
    - Self-organization
---
Social objects are objects around which social networks form. The concept was put forward by Jyri Engeström in 2005 as part of the explanation of why some social networks succeed and some fail. Engeström maintained that "Social network theory fails to recognise such real-world dynamics because its notion of sociality is limited to just people." Instead, he proposed what he called "object centered sociality," citing the work of the sociologist Karin Knorr-Cetina. For example, Engeström maintained that much of the success of the popular photo-sharing site Flickr was because photographs serve as social objects around which conversations of social networks form.[1]
