---
version: 1
type: article
id: https://en.wikipedia.org/wiki/House_plan
offline_file: ""
offline_thumbnail: ""
uuid: bfe99393-4612-4a3f-9220-edcc68630624
updated: 1484309329
title: House plan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/270px-Putnam_House_-_floor_plans.jpg
tags:
    - Drawing Set
    - Lines and symbols
    - Spaces and rooms
    - Open floorplan
categories:
    - Technical drawing
---
A house plan is a set of construction or working drawings (sometimes still called blueprints) that define all the construction specifications of a residential house such as dimensions, materials, layouts, installation methods and techniques.
