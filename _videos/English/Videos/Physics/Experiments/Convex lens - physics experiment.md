---
version: 1
type: video
provider: YouTube
id: MadCcYUoHMg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 6b167199-7df9-4400-921a-8b608123c9c6
updated: 1486069637
title: 'Convex lens - physics experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/MadCcYUoHMg/default.jpg
    - https://i3.ytimg.com/vi/MadCcYUoHMg/1.jpg
    - https://i3.ytimg.com/vi/MadCcYUoHMg/2.jpg
    - https://i3.ytimg.com/vi/MadCcYUoHMg/3.jpg
---
