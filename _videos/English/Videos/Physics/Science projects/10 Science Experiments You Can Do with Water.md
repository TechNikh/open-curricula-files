---
version: 1
type: video
provider: YouTube
id: O9s_6G1aJgc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d4dd32f7-9c05-4d6a-ae01-163a0bbd50bf
updated: 1486069632
title: 10 Science Experiments You Can Do with Water
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/O9s_6G1aJgc/default.jpg
    - https://i3.ytimg.com/vi/O9s_6G1aJgc/1.jpg
    - https://i3.ytimg.com/vi/O9s_6G1aJgc/2.jpg
    - https://i3.ytimg.com/vi/O9s_6G1aJgc/3.jpg
---
