---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Air_vortex_cannon
offline_file: ""
offline_thumbnail: ""
uuid: 760fc758-a320-4072-a404-6281ef09aac3
updated: 1484308872
title: Air vortex cannon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/ArtificialFictionBrain_0.png
categories:
    - Vortices
---
An air vortex cannon is a device that releases doughnut-shaped air vortices — similar to smoke rings but larger, stronger and invisible. The vortices are able to ruffle hair, disturb papers or blow out candles after travelling several metres.
