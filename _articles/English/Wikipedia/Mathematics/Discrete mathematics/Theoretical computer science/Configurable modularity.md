---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Configurable_modularity
offline_file: ""
offline_thumbnail: ""
uuid: 7667048a-bd3d-4d32-bfff-4d5af3e09847
updated: 1484308667
title: Configurable modularity
categories:
    - Theoretical computer science
---
Configurable modularity is a term coined by Raoul de Campo of IBM Research and later expanded on by Nate Edwards of the same organization, denoting the ability to reuse independent components by changing their interconnections, but not their internals.[1] In Edwards' view this characterizes all successful reuse systems, and indeed all systems which can be described as "engineered".
