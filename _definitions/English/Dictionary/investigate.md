---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/investigate
offline_file: ""
offline_thumbnail: ""
uuid: 52eb3237-caae-4289-b6e4-d80ebae53244
updated: 1484310381
title: investigate
categories:
    - Dictionary
---
investigate
     v 1: investigate scientifically; "Let's investigate the syntax of
          Chinese" [syn: {look into}]
     2: conduct an inquiry or investigation of; "The district
        attorney's office investigated reports of possible
        irregularities"; "inquire into the disappearance of the
        rich old lady" [syn: {inquire}, {enquire}]
