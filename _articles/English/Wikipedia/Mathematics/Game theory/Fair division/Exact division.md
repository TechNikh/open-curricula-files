---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exact_division
offline_file: ""
offline_thumbnail: ""
uuid: 8426110c-ec06-4e75-a698-88b8fc42f784
updated: 1484308518
title: Exact division
tags:
    - Definitions
    - Near-exact division
    - Exact division with arbitrary number of cuts
    - Piecewise-homogeneous cake, many partners, any weights
    - General cake, many partners, any weights
    - Exact division with a minimal number of cuts
    - Interval cake, two partners, many subsets, any weights
    - Interval cake, many partners, two subsets, equal weights
    - Interval cake, many partners, many subsets, equal weights
    - Interval cake, many partners, two subsets, arbitrary weights
    - Tightness proof
    - >
        Multi-dimensional cake, many partners, many subsets, equal
        weights
    - Near-Exact division procedures
    - Crumb-and-pack procedure
    - Truthful mechanisms
    - Impossibility
    - Comparison with other criteria
    - Summary table
categories:
    - Fair division
---
An exact division, also called even division or consensus division, is a division of a heterogeneous resource ("cake") to several subsets such that each of n people with different tastes agree about the valuations of the pieces.[1]:127
