---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/youngster
offline_file: ""
offline_thumbnail: ""
uuid: 45377e38-bb4e-418e-81ae-b2d3583fe6f5
updated: 1484310154
title: youngster
categories:
    - Dictionary
---
youngster
     n : a young person of either sex; "she writes books for
         children"; "they're just kids"; "`tiddler' is a British
         term for youngsters" [syn: {child}, {kid}, {minor}, {shaver},
          {nipper}, {small fry}, {tiddler}, {tike}, {tyke}, {fry},
          {nestling}]
