---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distributed_control_system
offline_file: ""
offline_thumbnail: ""
uuid: 309c2cc5-c0a5-4d19-b1da-8087357e48f7
updated: 1484308732
title: Distributed control system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/500px-Functional_levels_of_a_Distributed_Control_System.svg.png
tags:
    - Structure
    - Typical applications
    - History
    - Overview of control evolution
    - Origins
    - Development
    - The network-centric era of the 1980s
    - The application-centric era of the 1990s
    - Modern systems (2010 onwards)
categories:
    - Control engineering
---
A Distributed Control System (DCS) is a computerised control system for a process or plant, wherein control elements (controllers) are distributed throughout the system. This is in contrast to non-distributed systems that use discrete controllers. In a DCS, a hierarchy of controllers is connected by communication networks, allowing both centralised control rooms and local on-plant monitoring and control.
