---
version: 1
type: video
provider: YouTube
id: tpxNu3mg2jg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ea287ad8-d901-48ae-bc95-3b9fa3605dae
updated: 1486069645
title: Picasso falling down a Staircase (HD)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/tpxNu3mg2jg/default.jpg
    - https://i3.ytimg.com/vi/tpxNu3mg2jg/1.jpg
    - https://i3.ytimg.com/vi/tpxNu3mg2jg/2.jpg
    - https://i3.ytimg.com/vi/tpxNu3mg2jg/3.jpg
---
