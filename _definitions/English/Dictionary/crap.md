---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crap
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484596741
title: crap
categories:
    - Dictionary
---
crap
     n 1: obscene terms for feces [syn: {dirt}, {shit}, {shite}, {poop},
           {turd}]
     2: obscene words for unacceptable behavior; "I put up with a
        lot of bullshit from that jerk"; "what he said was mostly
        bull" [syn: {bullshit}, {bull}, {Irish bull}, {horseshit},
         {shit}, {dogshit}]
     v : have a bowel movement; "The dog had made in the flower beds"
         [syn: {stool}, {defecate}, {shit}, {take a shit}, {take a
         crap}, {ca-ca}, {make}]
     [also: {crapping}, {crapped}]
