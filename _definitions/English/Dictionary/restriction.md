---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/restriction
offline_file: ""
offline_thumbnail: ""
uuid: 57c953c5-9372-4ef4-96ae-6345b5907fcb
updated: 1484310527
title: restriction
categories:
    - Dictionary
---
restriction
     n 1: a principle that limits the extent of something; "I am
          willing to accept certain restrictions on my movements"
          [syn: {limitation}]
     2: an act of limiting or restricting (as by regulation) [syn: {limitation}]
     3: the act of keeping something within specified bounds (by
        force if necessary)
