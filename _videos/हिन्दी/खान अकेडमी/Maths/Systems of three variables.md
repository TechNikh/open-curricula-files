---
version: 1
type: video
provider: YouTube
id: QLPiY-sGe0w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8f14cd8e-736d-42d7-8bbb-9017ebdf7918
updated: 1486069617
title: Systems of three variables
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/QLPiY-sGe0w/default.jpg
    - https://i3.ytimg.com/vi/QLPiY-sGe0w/1.jpg
    - https://i3.ytimg.com/vi/QLPiY-sGe0w/2.jpg
    - https://i3.ytimg.com/vi/QLPiY-sGe0w/3.jpg
---
