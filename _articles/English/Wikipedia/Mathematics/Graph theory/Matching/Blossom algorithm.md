---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Blossom_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: 716e0e1e-3ab8-4ad4-899a-61e73f8a8bb5
updated: 1484309426
title: Blossom algorithm
tags:
    - Augmenting paths
    - Blossoms and contractions
    - Finding an augmenting path
    - Examples
    - Analysis
    - Bipartite matching
    - Weighted matching
categories:
    - Matching
---
The blossom algorithm is an algorithm in graph theory for constructing maximum matchings on graphs. The algorithm was developed by Jack Edmonds in 1961,[1] and published in 1965.[2] Given a general graph G = (V, E), the algorithm finds a matching M such that each vertex in V is incident with at most one edge in M and |M| is maximized. The matching is constructed by iteratively improving an initial empty matching along augmenting paths in the graph. Unlike bipartite matching, the key new idea is that an odd-length cycle in the graph (blossom) is contracted to a single vertex, with the search continuing iteratively in the contracted graph.
