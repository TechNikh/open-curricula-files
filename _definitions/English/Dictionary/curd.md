---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curd
offline_file: ""
offline_thumbnail: ""
uuid: 88f48ee5-11a1-4051-9cc8-77b1bf8adabe
updated: 1484310264
title: curd
categories:
    - Dictionary
---
curd
     n 1: a coagulated liquid resembling milk curd; "bean curd";
          "lemon curd"
     2: coagulated milk; used to made cheese; "Little Miss Muffet
        sat on a tuffet eating some curds and whey"
