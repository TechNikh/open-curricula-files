---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/typical
offline_file: ""
offline_thumbnail: ""
uuid: a007ce1a-4bae-4afe-8136-a15d9f7a6387
updated: 1484310208
title: typical
categories:
    - Dictionary
---
typical
     adj 1: exhibiting the qualities or characteristics that identify a
            group or kind or category; "a typical American girl";
            "a typical suburban community"; "the typical car owner
            drives 10,000 miles a year"; "a painting typical of
            the Impressionist school"; "a typical romantic poem";
            "a typical case of arteritis" [ant: {atypical}]
     2: of a feature that helps to distinguish a person or thing;
        "Jerusalem has a distinctive Middle East flavor"- Curtis
        Wilkie; "that is typical of you!" [syn: {distinctive}]
     3: conforming to a type; "the typical (or normal) American";
        "typical teenage behavior"
