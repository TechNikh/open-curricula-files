---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meow
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484597521
title: meow
categories:
    - Dictionary
---
meow
     n : the sound made by a cat (or any sound resembling this) [syn:
          {mew}, {miaou}, {miaow}]
     v : cry like a cat; "the cat meowed" [syn: {mew}]
