---
version: 1
type: video
provider: YouTube
id: uaaIT8Z48Fo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c8e48d10-2118-4e93-adf9-1f46eac7d7f1
updated: 1486069643
title: A Quick Animation of the Fibonacci Spiral Made with Doceri
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/uaaIT8Z48Fo/default.jpg
    - https://i3.ytimg.com/vi/uaaIT8Z48Fo/1.jpg
    - https://i3.ytimg.com/vi/uaaIT8Z48Fo/2.jpg
    - https://i3.ytimg.com/vi/uaaIT8Z48Fo/3.jpg
---
