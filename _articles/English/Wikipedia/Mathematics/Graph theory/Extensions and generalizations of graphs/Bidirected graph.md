---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bidirected_graph
offline_file: ""
offline_thumbnail: ""
uuid: ada7508d-09b5-4c0f-a63f-32b5a5fa0ea5
updated: 1484309408
title: Bidirected graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/360px-Bidirected_graph_features.svg.png
categories:
    - Extensions and generalizations of graphs
---
In the mathematical domain of graph theory, a bidirected graph (introduced by Edmonds & Johnson 1970)[1] is a graph in which each edge is given an independent orientation (or direction, or arrow) at each end. Thus, there are three kinds of bidirected edges: those where the arrows point outward, towards the vertices, at both ends; those where both arrows point inward, away from the vertices; and those in which one arrow points away from its vertex and towards the opposite end, while the other arrow points in the same direction as the first, away from the opposite end and towards its own vertex.
