---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_contact_conductance
offline_file: ""
offline_thumbnail: ""
uuid: 44bd035c-8790-4178-8b15-e92ee9ffce97
updated: 1484308864
title: Thermal contact conductance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/295px-Contact_conductance.svg.png
tags:
    - Definition
    - Importance
    - Factors influencing contact conductance
    - Contact pressure
    - Interstitial materials
    - Surface roughness, waviness and flatness
    - Surface deformations
    - Surface cleanliness
    - Measurement of thermal contact conductance
    - Thermal boundary conductance
categories:
    - Thermodynamics
---
In physics, thermal contact conductance is the study of heat conduction between solid bodies in thermal contact. The thermal contact conductance coefficient, 
  
    
      
        
          h
          
            c
          
        
      
    
    {\displaystyle h_{c}}
  
, is a property indicating the thermal conductivity, or ability to conduct heat, between two bodies in contact. The inverse of this property is termed thermal contact resistance.
