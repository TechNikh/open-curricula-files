---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sharecroppers
offline_file: ""
offline_thumbnail: ""
uuid: be58a077-86f6-4154-80bc-088449beddc3
updated: 1484308428
title: Sharecropping
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Greene_Co_Ga1941_Delano.jpg
tags:
    - Overview
    - Advantages
    - Disadvantages
    - Regions
    - Africa
    - United States
    - Sharecropping agreements
    - "Farmer's cooperatives"
    - Economic theories of share tenancy
categories:
    - Uncategorized
---
Sharecropping is a form of agriculture in which a landowner allows a tenant to use the land in return for a share of the crops produced on their portion of land. Sharecropping has a long history and there are a wide range of different situations and types of agreements that have used a form of the system. Some are governed by tradition, and others by law. Legal contract systems such as the Italian mezzadria, the French métayage, the Spanish mediero, or the Islamic system of muqasat, occur widely.[citation needed]
