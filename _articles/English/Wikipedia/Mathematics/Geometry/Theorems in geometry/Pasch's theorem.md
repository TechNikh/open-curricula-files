---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pasch%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: ab9ee67e-3065-4d4f-b7ce-7afb96452711
updated: 1484309123
title: "Pasch's theorem"
tags:
    - Notes
categories:
    - Theorems in geometry
---
In geometry, Pasch's theorem, stated in 1882 by the German mathematician Moritz Pasch,[1] is a result in plane geometry which cannot be derived from Euclid's postulates.
