---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comma
offline_file: ""
offline_thumbnail: ""
uuid: a0d9add4-d410-4d52-8beb-251a945f01ae
updated: 1484310421
title: comma
categories:
    - Dictionary
---
comma
     n 1: a punctuation mark (,) used to indicate the separation of
          elements within the grammatical structure of a sentence
     2: anglewing butterfly with a comma-shaped mark on the
        underside of each hind wing [syn: {comma butterfly}, {Polygonia
        comma}]
