---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/studying
offline_file: ""
offline_thumbnail: ""
uuid: 3407e3b1-c8c1-4642-9200-d0fdd5486739
updated: 1484310271
title: studying
categories:
    - Dictionary
---
studying
     n : reading carefully with intent to remember [syn: {perusal}, {perusing},
          {poring over}]
