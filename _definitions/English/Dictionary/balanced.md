---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/balanced
offline_file: ""
offline_thumbnail: ""
uuid: 0642ce90-d649-4ebe-a250-0df025d08641
updated: 1484310204
title: balanced
categories:
    - Dictionary
---
balanced
     adj 1: being in a state of proper balance or equilibrium; "the
            carefully balanced seesaw"; "a properly balanced
            symphony orchestra"; "a balanced assessment of
            intellectual and cultural history"; "a balanced blend
            of whiskeys"; "the educated man shows a balanced
            development of all his powers" [ant: {unbalanced}]
     2: total debits and credits are equal; "the books looked
        balanced"
