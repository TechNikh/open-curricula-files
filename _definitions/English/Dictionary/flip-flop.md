---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flip-flop
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484496421
title: flip-flop
categories:
    - Dictionary
---
flip-flop
     n 1: a decision to reverse an earlier decision [syn: {reversal},
          {change of mind}, {turnabout}, {turnaround}]
     2: a backless sandal held to the foot by a thong between the
        big toe and the second toe
     3: an electronic circuit that can assume either of two stable
        states
     4: a backward somersault
     v : reverse (a direction, attitude, or course of action) [syn: {interchange},
          {tack}, {switch}, {alternate}, {flip}]
     [also: {flip-flopping}, {flip-flopped}]
