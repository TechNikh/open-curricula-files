---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Squaring_the_square
offline_file: ""
offline_thumbnail: ""
uuid: 6cc3ea71-d6ff-4f3c-9660-6c42921ee3c3
updated: 1484308502
title: Squaring the square
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Sprague_squared_square.svg.png
tags:
    - Perfect squared squares
    - Simple squared squares
    - "Mrs. Perkins's quilt"
    - No more than two different sizes
    - Squaring the plane
    - Cubing the cube
categories:
    - Discrete geometry
---
Squaring the square is the problem of tiling an integral square using only other integral squares. (An integral square is a square whose sides have integer length.) The name was coined in a humorous analogy with squaring the circle. Squaring the square is an easy task unless additional conditions are set. The most studied restriction is that the squaring be perfect, meaning that the sizes of the smaller squares are all different. A related problem is squaring the plane, which can be done even with the restriction that each natural number occurs exactly once as a size of a square in the tiling. The order of a squared square is its number of constituent squares.
