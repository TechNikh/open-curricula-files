---
version: 1
type: video
provider: YouTube
id: KIA9YCu4Qyo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 60602aef-f51e-4e94-9e17-e98e7e91652f
updated: 1486069613
title: 'Math patterns example 2  Applying mathematical reasoning'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/KIA9YCu4Qyo/default.jpg
    - https://i3.ytimg.com/vi/KIA9YCu4Qyo/1.jpg
    - https://i3.ytimg.com/vi/KIA9YCu4Qyo/2.jpg
    - https://i3.ytimg.com/vi/KIA9YCu4Qyo/3.jpg
---
