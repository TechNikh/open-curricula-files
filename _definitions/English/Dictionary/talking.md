---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/talking
offline_file: ""
offline_thumbnail: ""
uuid: 67a6c2bf-ec04-4c08-bbba-b4143e6b474b
updated: 1484310168
title: talking
categories:
    - Dictionary
---
talking
     adj : uttering speech; "talking heads" [syn: {talking(a)}]
     n : an exchange of ideas via conversation; "let's have more work
         and less talk around here" [syn: {talk}]
