---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Fr%C3%B6berg_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: c42e0d9b-4ce9-4bc6-9059-7dcd3e910328
updated: 1484309147
title: Fröberg conjecture
categories:
    - Algebraic geometry
---
In algebraic geometry, the Fröberg conjecture is a conjecture about the possible Hilbert functions of a set of forms. It is named after Ralf Fröberg, who introduced it in Fröberg (1985, page 120). The Fröberg–Iarrobino conjecture is a generalization introduced by Iarrobino (1997).
