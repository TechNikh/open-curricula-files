---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aerogel
offline_file: ""
offline_thumbnail: ""
uuid: d6490689-acf4-40ed-84ee-417bc2a2c6c9
updated: 1484308392
title: Aerogel
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Aerogel_hand.jpg
tags:
    - Properties
    - Knudsen effect
    - Structure
    - Waterproofing
    - Porosity of aerogel
    - Materials
    - Silica
    - carbon
    - Metal oxide
    - Other
    - Applications
    - Production
    - Safety
categories:
    - Uncategorized
---
Aerogel is a synthetic porous ultralight material derived from a gel, in which the liquid component of the gel has been replaced with a gas.[1] The result is a solid with extremely low density[2] and low thermal conductivity. Nicknames include frozen smoke,[3] solid smoke, solid air, or blue smoke owing to its translucent nature and the way light scatters in the material. It feels like fragile expanded polystyrene to the touch. Aerogels can be made from a variety of chemical compounds.[4]
