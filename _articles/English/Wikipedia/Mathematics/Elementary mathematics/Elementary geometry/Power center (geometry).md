---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Power_center_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 62db6eb1-c26e-4438-9dd2-61a3c1fb0e8a
updated: 1484308932
title: Power center (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Radical_center.svg.png
categories:
    - Elementary geometry
---
In geometry, the power center of three circles, also called the radical center, is the intersection point of the three radical axes of the pairs of circles. If the radical center lies outside of all three circles, then it is the center of the unique circle (the radical circle) that intersects the three given circles orthogonally; the construction of this orthogonal circle corresponds to Monge's problem. This is a special case of the three conics theorem.
