---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/out
offline_file: ""
offline_thumbnail: ""
uuid: 8e2343be-7670-4d9c-a429-a1be70f899dc
updated: 1484310365
title: out
categories:
    - Dictionary
---
out
     adj 1: not allowed to continue to bat or run; "he was tagged out at
            second on a close play"; "he fanned out" [syn: {out(p)},
             {retired}] [ant: {safe(p)}]
     2: of a fire; being out or having grown cold; "threw his
        extinct cigarette into the stream";  "the fire is out"
        [syn: {extinct}, {out(p)}]
     3: not worth considering as a possibility; "a picnic is out
        because of the weather" [syn: {out(p)}]
     4: out of power; especially having been unsuccessful in an
        election; "now the Democrats are out" [syn: {out(a)}]
     5: excluded from use or mention; "forbidden fruit"; "in our
        house dancing and playing cards were ...
