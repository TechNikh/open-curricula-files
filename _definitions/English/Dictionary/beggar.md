---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beggar
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484451721
title: beggar
categories:
    - Dictionary
---
beggar
     n : a pauper who lives by begging [syn: {mendicant}]
     v 1: be beyond the resources of; "This beggars description!"
     2: reduce to beggary [syn: {pauperize}, {pauperise}]
