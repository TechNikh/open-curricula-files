---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/separation
offline_file: ""
offline_thumbnail: ""
uuid: 15f550c5-e154-4c05-90d3-d285638baae8
updated: 1484310216
title: separation
categories:
    - Dictionary
---
separation
     n 1: the act of dividing or disconnecting
     2: coming apart [syn: {breakup}, {detachment}]
     3: the state of lacking unity [ant: {union}]
     4: the distance between things; "fragile items require
        separation and cushioning" [syn: {interval}]
     5: sorting one thing from others; "the separation of wheat from
        chaff"; "the separation of mail by postal zones"
     6: the social act of separating or parting company; "the
        separation of church and state"
     7: the space where a division or parting occurs; "he hid in the
        separation between walls"
     8: termination of employment (by resignation or dismissal)
     9: (law) the cessation of ...
