---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 489d7304-ca82-49e3-9f35-d86cdba10287
updated: 1484310232
title: equilibrium
categories:
    - Dictionary
---
equilibrium
     n 1: a chemical reaction and its reverse proceed at equal rates
          [syn: {chemical equilibrium}]
     2: a stable situation in which forces cancel one another [ant:
        {disequilibrium}]
     3: equality of distribution [syn: {balance}, {equipoise}, {counterbalance}]
     4: a sensory system located in structures of the inner ear that
        registers the orientation of the head [syn: {labyrinthine
        sense}, {vestibular sense}, {sense of balance}, {sense of
        equilibrium}]
     [also: {equilibria} (pl)]
