---
version: 1
type: video
provider: YouTube
id: HUSDvSknIgI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Immune%20System%20-%20Fighting%20Infection%20by%20Clonal%20Selection.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 822d6590-5557-48d1-b3a0-f671dbfe1077
updated: 1486069669
title: 'Immune System - Fighting Infection by Clonal Selection'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/HUSDvSknIgI/default.jpg
    - https://i3.ytimg.com/vi/HUSDvSknIgI/1.jpg
    - https://i3.ytimg.com/vi/HUSDvSknIgI/2.jpg
    - https://i3.ytimg.com/vi/HUSDvSknIgI/3.jpg
---
