---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sickle
offline_file: ""
offline_thumbnail: ""
uuid: b49eeb5c-ec5e-471b-89f1-00795d341346
updated: 1484310587
title: sickle
categories:
    - Dictionary
---
sickle
     n : an edge tool for cutting grass or crops; has a curved blade
         and a short handle [syn: {reaping hook}, {reap hook}]
