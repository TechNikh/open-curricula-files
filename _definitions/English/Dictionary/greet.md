---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greet
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484568841
title: greet
categories:
    - Dictionary
---
greet
     v 1: express greetings upon meeting someone [syn: {recognize}, {recognise}]
     2: send greetings to
     3: react to in a certain way; "The President was greeted with
        catcalls"
     4: be perceived by; "Loud music greeted him when he entered the
        apartment"
