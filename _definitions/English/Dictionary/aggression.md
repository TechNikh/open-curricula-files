---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aggression
offline_file: ""
offline_thumbnail: ""
uuid: e31bd239-b7c1-4660-a022-f9f1137b474e
updated: 1484310557
title: aggression
categories:
    - Dictionary
---
aggression
     n 1: a disposition to behave aggressively
     2: a feeling of hostility that arouses thoughts of attack [syn:
         {aggressiveness}]
     3: violent action that is hostile and usually unprovoked [syn:
        {hostility}]
     4: the act of initiating hostilities
     5: deliberately unfriendly behavior
