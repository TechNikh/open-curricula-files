---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/R%C3%BCchardt_experiment'
offline_file: ""
offline_thumbnail: ""
uuid: d70f93ff-068a-45c7-af5c-1c7179b58055
updated: 1484308860
title: Rüchardt experiment
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ruechardt_experiment_riediker.jpg
tags:
    - Background
    - Experiment
    - List of various versions of the Rüchardt experiment
categories:
    - Thermodynamics
---
The Rüchardt experiment,[1] [2][3] invented by Eduard Rüchardt, is a famous experiment in thermodynamics, which determines the ratio of the molar heat capacities of a gas, i.e. the ratio of 
  
    
      
        
          C
          
            p
          
        
      
    
    {\displaystyle C_{p}}
  
 (heat capacity at constant pressure) and 
  
    
      
        
          C
          
            V
          
        
      
    
    {\displaystyle C_{V}}
  
 (heat capacity at constant volume) and is denoted by 
  
    
      
        γ
      
    
    {\displaystyle \gamma }
  
 (gamma, for ideal gas) or 
  
    
      
        κ
      
    
    {\displaystyle \kappa }
  ...
