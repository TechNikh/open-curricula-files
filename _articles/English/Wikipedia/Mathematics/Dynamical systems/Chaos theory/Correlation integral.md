---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Correlation_integral
offline_file: ""
offline_thumbnail: ""
uuid: 131b2b4a-e43a-4cd7-b9aa-bdcf7e29a0c0
updated: 1484308705
title: Correlation integral
categories:
    - Chaos theory
---
where 
  
    
      
        N
      
    
    {\displaystyle N}
  
 is the number of considered states 
  
    
      
        
          
            
              x
              →
            
          
        
        (
        i
        )
      
    
    {\displaystyle {\vec {x}}(i)}
  
, 
  
    
      
        ε
      
    
    {\displaystyle \varepsilon }
  
 is a threshold distance, 
  
    
      
        
          |
        
        
          |
        
        ⋅
        
          |
        
        
          |
        
      
    
    {\displaystyle ||\cdot ||}
  
 a norm (e.g. Euclidean norm) and 
  
    
      
        Θ
        (
        ⋅
        )
      
   ...
