---
version: 1
type: video
provider: YouTube
id: VrWgDnkIjE8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Eureka%201%20-%20Inertia.mov.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5e6a6d23-0a98-4a30-b892-b0070b63469b
updated: 1486069665
title: 'Eureka 1 - Inertia.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/VrWgDnkIjE8/default.jpg
    - https://i3.ytimg.com/vi/VrWgDnkIjE8/1.jpg
    - https://i3.ytimg.com/vi/VrWgDnkIjE8/2.jpg
    - https://i3.ytimg.com/vi/VrWgDnkIjE8/3.jpg
---
