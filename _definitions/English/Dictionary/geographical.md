---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/geographical
offline_file: ""
offline_thumbnail: ""
uuid: 0be570e0-d92d-4250-bd99-199dc4b544d2
updated: 1484310433
title: geographical
categories:
    - Dictionary
---
geographical
     adj 1: of or relating to the science of geography [syn: {geographic}]
     2: determined by geography; "the north and south geographic
        poles" [syn: {geographic}] [ant: {magnetic}]
