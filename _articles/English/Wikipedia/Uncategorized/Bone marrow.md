---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bone_marrow
offline_file: ""
offline_thumbnail: ""
uuid: 972ec638-b60e-473f-b853-4d7ca8b5f8d7
updated: 1484308371
title: Bone marrow
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gray72-en.svg.png
tags:
    - Structure
    - Types of bone marrow
    - Stroma
    - Cellular components
    - Function
    - Mesenchymal stem cells
    - Bone marrow barrier
    - Lymphatic role
    - Compartmentalization
    - Society and culture
    - Clinical significance
    - Disease
    - Imaging
    - Histology
    - Donation and transplantation
    - harvesting
    - Fossil record
categories:
    - Uncategorized
---
Bone marrow is the flexible tissue in the interior of bones. In humans, red blood cells are produced by cores of bone marrow in the heads of long bones in a process known as hematopoiesis.[2] On average, bone marrow constitutes 4% of the total body mass of humans; in an adult having 65 kilograms of mass (143 lbs), bone marrow typically accounts for approximately 2.6 kilograms (5.7 lb). The hematopoietic component of bone marrow produces approximately 500 billion blood cells per day, which use the bone marrow vasculature as a conduit to the body's systemic circulation.[3] Bone marrow is also a key component of the lymphatic system, producing the lymphocytes that support the body's immune ...
