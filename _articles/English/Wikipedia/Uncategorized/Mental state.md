---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mental_state
offline_file: ""
offline_thumbnail: ""
uuid: 10d1c53d-1072-4611-8fdd-fe3d54a330bf
updated: 1484308351
title: Mental state
tags:
    - Mental states and academia
    - Epistemology
categories:
    - Uncategorized
---
A mental state is a state of mind that an agent is in. Most simplistically, a mental state is a mental condition. It is a relation that connects the agent with a proposition. Several of these states are a combination of mental representations and propositional attitudes. There are several paradigmatic states of mind that an agent has: love, hate, pleasure and pain, and attitudes toward propositions such as: believing that, conceiving that, hoping and fearing that, etc.
