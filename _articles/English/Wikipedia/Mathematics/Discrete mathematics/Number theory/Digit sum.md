---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Digit_sum
offline_file: ""
offline_thumbnail: ""
uuid: df95374e-09dc-4ec2-8858-0f1dce3cc8ae
updated: 1484308639
title: Digit sum
categories:
    - Number theory
---
In mathematics, the digit sum of a given integer is the sum of all its digits (e.g. the digit sum of 84001 is calculated as 8+4+0+0+1 = 13). Digit sums are most often computed using the decimal representation of the given number, but they may be calculated in any other base. Different bases give different digit sums, with the digit sums for binary being on average smaller than those for any other base.[1]
