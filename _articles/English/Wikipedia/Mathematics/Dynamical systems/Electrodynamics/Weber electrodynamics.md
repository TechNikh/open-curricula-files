---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weber_electrodynamics
offline_file: ""
offline_thumbnail: ""
uuid: 90a946e0-884a-4ca7-a39d-4f76fd2d14f9
updated: 1484308767
title: Weber electrodynamics
tags:
    - Mathematical description
    - "Newton's third law in Maxwell and Weber electrodynamics"
    - Predictions
    - Limitations
    - Experimental tests
    - Velocity Dependent Tests
    - Acceleration Dependent Tests
    - Relation to quantum electrodynamics
categories:
    - Electrodynamics
---
Weber electrodynamics is an alternative to Maxwell electrodynamics developed by Wilhelm Eduard Weber. In this theory, Coulomb's Law becomes velocity dependent. The theory is not in widespread use by contemporary physicists, and is not mentioned in mainstream textbooks on classical electromagnetism.[1]
