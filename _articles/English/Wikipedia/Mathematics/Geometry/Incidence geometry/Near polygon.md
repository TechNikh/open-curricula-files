---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Near_polygon
offline_file: ""
offline_thumbnail: ""
uuid: c09becec-14d9-42b2-975b-8313542b9373
updated: 1484309251
title: Near polygon
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/240px-GQ%25282%252C2%2529%252C_the_Doily.svg.png'
tags:
    - Definition
    - Examples
    - Regular near polygons
    - Notes
categories:
    - Incidence geometry
---
In mathematics, a near polygon is an incidence geometry introduced by Ernest E. Shult and Arthur Yanushka in 1980.[1] Shult and Yanushka showed the connection between the so-called tetrahedrally closed line-systems in Euclidean spaces and a class of point-line geometries which they called near polygons. These structures generalise the notion of generalized polygon as every generalized 2n-gon is a near 2n-gon of a particular kind. Near polygons were extensively studied and connection between them and dual polar space [2] was shown in 1980s and early 1990s. Some sporadic simple groups, for example the Hall-Janko group and the Mathieu groups, act as automorphism groups of near polygons.
