---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/midpoint
offline_file: ""
offline_thumbnail: ""
uuid: 598585e6-aac6-40fa-99f3-665d7fcb3999
updated: 1484310208
title: midpoint
categories:
    - Dictionary
---
midpoint
     n : a point equidistant from the ends of a line or the
         extremities of a figure [syn: {center}, {centre}]
