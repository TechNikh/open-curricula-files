---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suck
offline_file: ""
offline_thumbnail: ""
uuid: cd657a47-c8bf-4dc4-8d90-f7260d0b6b32
updated: 1484310268
title: suck
categories:
    - Dictionary
---
suck
     n : the act of sucking [syn: {sucking}, {suction}]
     v 1: draw into the mouth by creating a practical vacuum in the
          mouth; "suck the poison from the place where the snake
          bit"; "suck on a straw"; "the baby sucked on the
          mother's breast"
     2: draw something in by or as if by a vacuum; "Mud was sucking
        at her feet"
     3: attract by using an inexorable force, inducement, etc.; "The
        current boom in the economy sucked many workers in from
        abroad" [syn: {suck in}]
     4: take in, also metaphorically; "The sponge absorbs water
        well"; "She drew strength from the minister's words" [syn:
         {absorb}, {imbibe}, ...
