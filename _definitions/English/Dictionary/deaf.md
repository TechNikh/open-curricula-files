---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deaf
offline_file: ""
offline_thumbnail: ""
uuid: 43ccf897-123b-4dc7-ab0e-19d99e857567
updated: 1484310150
title: deaf
categories:
    - Dictionary
---
deaf
     adj 1: lacking or deprive of the sense of hearing wholly or in part
            [ant: {hearing(a)}]
     2: (usually followed by `to') unwilling or refusing to pay
        heed; "deaf to her warnings" [syn: {deaf(p)}, {indifferent(p)}]
     n : people who have severe hearing impairments; "many of the
         deaf use sign language"
     v : make or render deaf; "a deafening noise" [syn: {deafen}]
