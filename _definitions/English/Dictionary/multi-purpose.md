---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/multi-purpose
offline_file: ""
offline_thumbnail: ""
uuid: 3335d96d-5d9e-4281-ac9b-4606d78d6ea1
updated: 1484310170
title: multi-purpose
categories:
    - Dictionary
---
multipurpose
     adj : having multiple uses; "a multipurpose tool"
