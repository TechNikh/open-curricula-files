---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Divisibility_rule
offline_file: ""
offline_thumbnail: ""
uuid: 2e2c2ed1-aa9d-4b8b-8ebc-c925c56e5e8b
updated: 1484308944
title: Divisibility rule
tags:
    - Divisibility rules for numbers 1–30
    - Step-by-step examples
    - Divisibility by 2
    - Divisibility by 3 or 9
    - Divisibility by 4
    - Divisibility by 5
    - Divisibility by 6
    - Divisibility by 7
    - Divisibility by 13
    - Beyond 30
    - Composite divisors
    - Prime divisors
    - Notable examples
    - Generalized divisibility rule
    - Proofs
    - Proof using basic algebra
    - Proof using modular arithmetic
    - Sources
categories:
    - Elementary number theory
---
A divisibility rule is a shorthand way of determining whether a given number is divisible by a fixed divisor without performing the division, usually by examining its digits. Although there are divisibility tests for numbers in any radix, or base, and they are all different, this article presents rules and examples only for decimal, or base 10, numbers. Martin Gardner explained and popularized these rules in his September 1962 "Mathematical Games" column in Scientific American.
