---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/companion
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484470561
title: companion
categories:
    - Dictionary
---
companion
     n 1: a person who is frequently in the company of another;
          "drinking companions"; "comrades in arms" [syn: {comrade},
           {fellow}, {familiar}, {associate}]
     2: a traveler who accompanies you [syn: {fellow traveler}, {fellow
        traveller}]
     3: one paid to accompany or assist or live with another
     v : be a companion to somebody [syn: {company}, {accompany}, {keep
         company}]
