---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indicator
offline_file: ""
offline_thumbnail: ""
uuid: 23e6f64e-2a5d-49c3-a1e3-29c2ac5f85a2
updated: 1484310230
title: indicator
categories:
    - Dictionary
---
indicator
     n 1: a number or ratio (a value on a scale of measurement)
          derived from a series of observed facts; can reveal
          relative changes as a function of time [syn: {index}, {index
          number}, {indicant}]
     2: a signal for attracting attention
     3: a device for showing the operating condition of some system
     4: (chemistry) a substance that changes color to indicate the
        presence of some ion or substance; can be used to indicate
        the completion of a chemical reaction or (in medicine) to
        test for a particular reaction
