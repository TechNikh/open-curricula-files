---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Concept-driven_strategy
offline_file: ""
offline_thumbnail: ""
uuid: 10657a39-a20b-4d9e-b896-abe89e0daf9b
updated: 1484309065
title: Concept-driven strategy
tags:
    - Background
    - Underlying philosophy
    - Method
    - Other terminology
    - Other literature
categories:
    - Strategy
---
A concept-driven strategy is a process for formulating strategy that draws on the explanation of how humans inquire provided by linguistic pragmatic philosophy. This argues that thinking starts by selecting (explicitly or implicitly) a set of concepts (frames, patterns, lens, principles, etc.) gained from our past experiences. These are used to reflect on whatever happens, or is done, in the future.[1]
