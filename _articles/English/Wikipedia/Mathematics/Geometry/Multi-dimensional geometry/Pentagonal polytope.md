---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pentagonal_polytope
offline_file: ""
offline_thumbnail: ""
uuid: 2fd77fa2-681e-423b-aefd-5ba48c3b6205
updated: 1484309287
title: Pentagonal polytope
tags:
    - Family members
    - Dodecahedral
    - Icosahedral
    - Related star polytopes and honeycombs
    - Notes
categories:
    - Multi-dimensional geometry
---
In geometry, a pentagonal polytope is a regular polytope in n dimensions constructed from the Hn Coxeter group. The family was named by H. S. M. Coxeter, because the two-dimensional pentagonal polytope is a pentagon. It can be named by its Schläfli symbol as {5, 3n − 2} (dodecahedral) or {3n − 2, 5} (icosahedral).
