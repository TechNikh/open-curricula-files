---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Uttaranchal
offline_file: ""
offline_thumbnail: ""
uuid: 1bb7f5b4-a38b-4759-be66-e28e5929d356
updated: 1484308426
title: Uttarakhand
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Jageshwar_main_0.JPG
tags:
    - Etymology
    - History
    - geography
    - Demographics
    - Government and politics
    - Sub-divisions
    - culture
    - Economy
    - Flora and Fauna
    - Transport
    - Tourism
    - Education
    - Sports
    - Sports Stadiums
categories:
    - Uncategorized
---
Uttarakhand (i/ˌʊtəˈrɑːkʌnd/),[3] (Hindi: उत्तराखण्ड, Uttarākhaṇḍ), formerly known as Uttaranchal,[4] is a state in the northern part of India. It is often referred to as the Devbhumi (literally: "Land of the Gods") due to many Hindu temples and pilgrimage centres found throughout the state. Uttarakhand is known for its natural beauty of the Himalayas, the Bhabhar and the Terai. On 9 November 2000, this 27th state of the Republic of India was created from the Himalayan and adjoining northwestern districts of Uttar Pradesh.[5] It borders the Tibet on the north; the Mahakali Zone of the Far-Western Region, Nepal on the east; and the Indian states of Uttar ...
