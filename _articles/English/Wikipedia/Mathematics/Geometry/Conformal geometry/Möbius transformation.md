---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/M%C3%B6bius_transformation'
offline_file: ""
offline_thumbnail: ""
uuid: a7e94082-e200-4c1e-bbd9-1bd064782a00
updated: 1484309187
title: Möbius transformation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Apollonian_circles.svg_0.png
tags:
    - Overview
    - Definition
    - Decomposition and elementary properties
    - Preservation of angles and generalized circles
    - Cross-ratio preservation
    - Conjugation
    - Projective matrix representations
    - Specifying a transformation by three points
    - Mapping first to 0, 1, ∞
    - Explicit determinant formula
    - Classification
    - Parabolic transforms
    - Characteristic constant
    - Elliptic transforms
    - Hyperbolic transforms
    - Loxodromic transforms
    - General classification
    - The real case and a note on terminology
    - Fixed points
    - Determining the fixed points
    - Topological proof
    - Normal form
    - Geometric interpretation of the characteristic constant
    - Elliptic transformations
    - Hyperbolic transformations
    - Loxodromic transformations
    - Stereographic projection
    - Iterating a transformation
    - Poles of the transformation
    - Lorentz transformation
    - Hyperbolic space
    - Subgroups of the Möbius group
    - Higher dimensions
    - Notes
categories:
    - Conformal geometry
---
of one complex variable z; here the coefficients a, b, c, d are complex numbers satisfying ad − bc ≠ 0.
