---
version: 1
type: video
provider: YouTube
id: rabHKjfpJuU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5e3cfefb-40e2-4a19-922d-6615869ebb97
updated: 1486069609
title: Systems With Elimination Practice
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/rabHKjfpJuU/default.jpg
    - https://i3.ytimg.com/vi/rabHKjfpJuU/1.jpg
    - https://i3.ytimg.com/vi/rabHKjfpJuU/2.jpg
    - https://i3.ytimg.com/vi/rabHKjfpJuU/3.jpg
---
