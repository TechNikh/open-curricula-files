---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stone
offline_file: ""
offline_thumbnail: ""
uuid: 4d936f87-ac82-4ab6-b190-00a49b3f7037
updated: 1484310256
title: stone
categories:
    - Dictionary
---
stone
     adj : of any of various dull tannish-gray colors
     n 1: a lump or mass of hard consolidated mineral matter; "he
          threw a rock at me" [syn: {rock}]
     2: material consisting of the aggregate of minerals like those
        making up the Earth's crust; "that mountain is solid
        rock"; "stone is abundant in New England and there are
        many quarries" [syn: {rock}]
     3: building material consisting of a piece of rock hewn in a
        definite shape for a special purpose; "he wanted a special
        stone to mark the site"
     4: a crystalline rock that can be cut and polished for jewelry;
        "he had the gem set in a ring for his wife"; "she had
     ...
