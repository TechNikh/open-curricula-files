---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/selfish
offline_file: ""
offline_thumbnail: ""
uuid: cb5b1a41-3f45-4bb2-b834-2406dac35136
updated: 1484310152
title: selfish
categories:
    - Dictionary
---
selfish
     adj : concerned chiefly or only with yourself; "Selfish men
           were...trying to make capital for themselves out of the
           sacred cause of civil rights"- Maria Weston Chapman
           [ant: {unselfish}]
