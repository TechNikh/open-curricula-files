---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/banner
offline_file: ""
offline_thumbnail: ""
uuid: f79c81b4-9a9d-4233-ac96-9022b2348573
updated: 1484310236
title: banner
categories:
    - Dictionary
---
banner
     adj : unusually good; outstanding; "a banner year for the company"
     n 1: long strip of cloth for decoration or advertising [syn: {streamer}]
     2: a newspaper headline that runs across the full page [syn: {streamer}]
