---
version: 1
type: video
provider: YouTube
id: XgSqR1hsYkM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d68fe909-ded1-4ec3-a653-4c2932676cbe
updated: 1486069635
title: 'Experiment- Image formed by plane mirror 02'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/XgSqR1hsYkM/default.jpg
    - https://i3.ytimg.com/vi/XgSqR1hsYkM/1.jpg
    - https://i3.ytimg.com/vi/XgSqR1hsYkM/2.jpg
    - https://i3.ytimg.com/vi/XgSqR1hsYkM/3.jpg
---
