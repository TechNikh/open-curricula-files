---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/afghan
offline_file: ""
offline_thumbnail: ""
uuid: e9074959-35d9-4d73-99b1-9a9b9e9b0b16
updated: 1484310585
title: afghan
categories:
    - Dictionary
---
Afghan
     adj : of or relating to or characteristic of Afghanistan or its
           people [syn: {Afghani}, {Afghanistani}]
     n 1: a blanket knitted or crocheted in strips or squares;
          sometimes used as a shawl
     2: a native or inhabitant of Afghanistan [syn: {Afghanistani}]
     3: an Iranian language spoken in Afghanistan and Pakistan; the
        official language of Afghanistan [syn: {Pashto}, {Pashtu},
         {Paxto}, {Afghani}]
     4: a coat made of sheepskin [syn: {sheepskin coat}]
     5: tall graceful breed of hound with a long silky coat; native
        to the Near East [syn: {Afghan hound}]
