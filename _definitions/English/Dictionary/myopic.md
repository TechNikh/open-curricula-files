---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/myopic
offline_file: ""
offline_thumbnail: ""
uuid: 0893b743-4254-4b19-8d56-f7d9b8a73889
updated: 1484310156
title: myopic
categories:
    - Dictionary
---
myopic
     adj 1: unable to see distant objects clearly [syn: {nearsighted}]
            [ant: {farsighted}]
     2: lacking foresight or scope; "a short view of the problem";
        "shortsighted policies"; "shortsighted critics derided the
        plan"; "myopic thinking" [syn: {short}, {shortsighted}, {unforesightful}]
