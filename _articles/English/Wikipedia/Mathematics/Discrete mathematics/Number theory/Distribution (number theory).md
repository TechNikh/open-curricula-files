---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distribution_(number_theory)
offline_file: ""
offline_thumbnail: ""
uuid: f842bda9-4437-45fe-8579-0832b3b5c3b5
updated: 1484308635
title: Distribution (number theory)
tags:
    - Examples
    - Hurwitz zeta function
    - Bernoulli distribution
    - Cyclotomic units
    - Universal distribution
    - Stickelberger distributions
    - p-adic measures
    - Hecke operators and measures
categories:
    - Number theory
---
In algebra and number theory, a distribution is a function on a system of finite sets into an abelian group which is analogous to an integral: it is thus the algebraic analogue of a distribution in the sense of generalised function.
