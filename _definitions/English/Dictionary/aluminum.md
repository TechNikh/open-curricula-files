---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aluminum
offline_file: ""
offline_thumbnail: ""
uuid: c43827b5-97f6-41b2-9145-af9418630b2c
updated: 1484310240
title: aluminum
categories:
    - Dictionary
---
aluminum
     n : a silvery ductile metallic element found primarily in
         bauxite [syn: {aluminium}, {Al}, {atomic number 13}]
