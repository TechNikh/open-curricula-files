---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extremist
offline_file: ""
offline_thumbnail: ""
uuid: 456404a4-7c11-4a13-8aa0-b09daf0a8731
updated: 1484310176
title: extremist
categories:
    - Dictionary
---
extremist
     adj : (used of opinions and actions) far beyond the norm;
           "extremist political views"; "radical opinions on
           education"; "an ultra conservative" [syn: {radical}, {ultra}]
     n : a person who holds extreme views
