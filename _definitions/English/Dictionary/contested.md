---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contested
offline_file: ""
offline_thumbnail: ""
uuid: 8f31ff64-46cb-4660-b66f-aafd03ea3bbe
updated: 1484310537
title: contested
categories:
    - Dictionary
---
contested
     adj : disputed or made the object of contention or competition; "a
           contested election" [ant: {uncontested}]
