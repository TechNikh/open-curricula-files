---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hero
offline_file: ""
offline_thumbnail: ""
uuid: 233604d6-f131-4aa3-b57a-6ef700d813aa
updated: 1484310605
title: hero
categories:
    - Dictionary
---
hero
     n 1: a man distinguished by exceptional courage and nobility and
          strength; "RAF pilots were the heroes of the Battle of
          Britain"
     2: the principal character in a play or movie or novel or poem
     3: someone who fights for a cause [syn: {champion}, {fighter},
        {paladin}]
     4: Greek mathematician and inventor who devised a way to
        determine the area of a triangle and who described various
        mechanical devices (first century) [syn: {Heron}, {Hero of
        Alexandria}]
     5: (classical mythology) a being of great strength and courage
        celebrated for bold exploits; often the offspring of a
        mortal and a god
     6: ...
