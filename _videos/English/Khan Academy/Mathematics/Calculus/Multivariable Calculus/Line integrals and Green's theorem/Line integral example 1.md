---
version: 1
type: video
provider: YouTube
id: uXjQ8yc9Pdg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Line%20integrals%20and%20Green%27s%20theorem/Line%20integral%20example%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 896d926c-01c5-464d-80e9-b975d715ebfd
updated: 1486069696
title: Line integral example 1
categories:
    - "Line integrals and Green's theorem"
thumbnail_urls:
    - https://i3.ytimg.com/vi/uXjQ8yc9Pdg/default.jpg
    - https://i3.ytimg.com/vi/uXjQ8yc9Pdg/1.jpg
    - https://i3.ytimg.com/vi/uXjQ8yc9Pdg/2.jpg
    - https://i3.ytimg.com/vi/uXjQ8yc9Pdg/3.jpg
---
