---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hopf%E2%80%93Rinow_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: c861d8b5-fb6f-4c97-8474-a7b47b63e1af
updated: 1484309268
title: Hopf–Rinow theorem
tags:
    - Statement of the theorem
    - Variations and generalizations
    - Notes
categories:
    - Metric geometry
---
Hopf–Rinow theorem is a set of statements about the geodesic completeness of Riemannian manifolds. It is named after Heinz Hopf and his student Willi Rinow, who published it in 1931.[1]
