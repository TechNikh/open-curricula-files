---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rajastan
offline_file: ""
offline_thumbnail: ""
uuid: e178be84-e380-4eff-bd74-9638d9f426f8
updated: 1484308464
title: Rajasthan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mehrangarh_Fort_0.jpg
tags:
    - Etymology
    - History
    - Ancient
    - Classical
    - Gurjars
    - Gurjara-Pratihara
    - Medieval and Early Modern
    - Historical tribes
    - Major Rulers
    - Rajput era
categories:
    - Uncategorized
---
Rajasthan (/ˈrɑːdʒəstæn/ Hindustani pronunciation: [raːdʒəsˈt̪ʰaːn] ( listen); literally, "Land of Kings")[4] is India's largest state by area (342,239 square kilometres (132,139 sq mi) or 10.4% of India's total area). It is located on the western side of the country, where it comprises most of the wide and inhospitable Thar Desert (also known as the "Rajasthan Desert" and "Great Indian Desert") and shares a border with the Pakistani provinces of Punjab to the northwest and Sindh to the west, along the Sutlej-Indus river valley. Elsewhere it is bordered by the other Indian states: Punjab to the north; Haryana and Uttar Pradesh to the northeast; Madhya Pradesh to the ...
