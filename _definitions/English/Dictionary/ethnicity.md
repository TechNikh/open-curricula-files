---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ethnicity
offline_file: ""
offline_thumbnail: ""
uuid: 1abd11e9-1393-402e-bbff-ff802845372a
updated: 1484310581
title: ethnicity
categories:
    - Dictionary
---
ethnicity
     n : an ethnic quality or affiliation resulting from racial or
         cultural ties; "ethnicity has a strong influence on
         community status relations"
