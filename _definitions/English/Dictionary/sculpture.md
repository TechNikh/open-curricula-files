---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sculpture
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484639341
title: sculpture
categories:
    - Dictionary
---
sculpture
     n 1: a three-dimensional work of plastic art
     2: creating figures or designs in three dimensions [syn: {carving}]
     v 1: create by shaping stone or wood or any other hard material;
          "sculpt a swan out of a block of ice" [syn: {sculpt}]
     2: shape (a material like stone or wood) by whittling away at
        it; "She is sculpting the block of marble into an image of
        her husband" [syn: {sculpt}, {grave}]
