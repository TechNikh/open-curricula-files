---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jean_Paul_de_Gua_de_Malves
offline_file: ""
offline_thumbnail: ""
uuid: ab154c3b-6319-47e9-8b51-f499e91e41fe
updated: 1484309083
title: Jean Paul de Gua de Malves
categories:
    - Geometers
---
Jean Paul de Gua de Malves (1713, Malves-en-Minervois (Aude) – June 2, 1785, Paris[1]) was a French mathematician who published in 1740 a work on analytical geometry in which he applied it, without the aid of differential calculus, to find the tangents, asymptotes, and various singular points of an algebraic curve.
