---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bhabar
offline_file: ""
offline_thumbnail: ""
uuid: e5d8ba8e-8c83-4e50-9b09-dc3a14d34dec
updated: 1484308420
title: Bhabar
tags:
    - Etymology
    - Overview
    - History
categories:
    - Uncategorized
---
Bhabar (Hindi and Nepali: भाबर, bhābar) is the region south of the Lower Himalayas and the Shiwalik Hills.[1] It is the alluvial apron of sediments washed down from the Siwaliks along the northern edge of the Indo-Gangetic Plain.
