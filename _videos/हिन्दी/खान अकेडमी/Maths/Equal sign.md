---
version: 1
type: video
provider: YouTube
id: 8iNqCRIadVQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1e5a9727-58d5-4dfb-a355-8bf8fce0b9ee
updated: 1486069616
title: Equal sign
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/8iNqCRIadVQ/default.jpg
    - https://i3.ytimg.com/vi/8iNqCRIadVQ/1.jpg
    - https://i3.ytimg.com/vi/8iNqCRIadVQ/2.jpg
    - https://i3.ytimg.com/vi/8iNqCRIadVQ/3.jpg
---
