---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overweight
offline_file: ""
offline_thumbnail: ""
uuid: cb4db367-8f1d-492c-89b0-6e76668979dc
updated: 1484310535
title: overweight
categories:
    - Dictionary
---
overweight
     adj : usually describes a large person who is fat but has a large
           frame to carry it [syn: {fleshy}, {heavy}]
     n : the property of excessive fatness [syn: {corpulence}, {stoutness},
          {adiposis}]
