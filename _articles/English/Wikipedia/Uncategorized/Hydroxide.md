---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hydroxyl_ion
offline_file: ""
offline_thumbnail: ""
uuid: 1bb11af6-0cb2-42c3-a49e-f62fcd83c917
updated: 1484308372
title: Hydroxide
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Bihydoxide.png
tags:
    - Hydroxide ion
    - Vibrational spectra
    - Applications
    - Inorganic hydroxides
    - Alkali metals
    - Alkaline earth metals
    - Boron group elements
    - Carbon group elements
    - Other main-group elements
    - Transition and post-transition metals
    - Basic salts containing hydroxide
    - Structural chemistry
    - In organic reactions
    - Base catalysis
    - As a nucleophilic reagent
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
Hydroxide is a diatomic anion with chemical formula OH−. It consists of an oxygen and hydrogen atom held together by a covalent bond, and carries a negative electric charge. It is an important but usually minor constituent of water. It functions as a base, a ligand, a nucleophile and a catalyst. The hydroxide ion forms salts, some of which dissociate in aqueous solution, liberating solvated hydroxide ions. Sodium hydroxide is a multi-million-ton per annum commodity chemical. A hydroxide attached to a strongly electropositive center may itself ionize,[citation needed] liberating a hydrogen cation (H+), making the parent compound an acid.
