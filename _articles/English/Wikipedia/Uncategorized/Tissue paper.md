---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tissue_paper
offline_file: ""
offline_thumbnail: ""
uuid: 1884fab6-1d3a-4322-8ce3-6920c54ae52e
updated: 1484308365
title: Tissue paper
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Taschentuch.JPG
tags:
    - Properties
    - Production
    - Applications
    - Hygienic tissue paper
    - Facial tissues
    - Paper towels
    - Wrapping tissue
    - Toilet tissue
    - Table napkins
    - Acoustic disrupter
    - Packing industry
    - The industry
    - Companies
    - sustainability
    - Types of eco-labels
categories:
    - Uncategorized
---
