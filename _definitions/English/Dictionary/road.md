---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/road
offline_file: ""
offline_thumbnail: ""
uuid: 304d1cd5-81a0-4aff-b843-f7ef3ca3a966
updated: 1484310212
title: road
categories:
    - Dictionary
---
road
     adj 1: taking place over public roads; "road racing" [syn: {road(a)}]
            [ant: {cross-country}]
     2: working for a short time in different places; "itinerant
        laborers"; "a road show"; "traveling salesman"; "touring
        company" [syn: {itinerant}, {touring}, {traveling}]
     n 1: an open way (generally public) for travel or transportation
          [syn: {route}]
     2: a way or means to achieve something; "the road to fame"
