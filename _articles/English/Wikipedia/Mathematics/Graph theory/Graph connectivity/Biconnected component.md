---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biconnected_component
offline_file: ""
offline_thumbnail: ""
uuid: a4dc3c0a-432c-4f4a-a2bb-e3cf7c80d426
updated: 1484309396
title: Biconnected component
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Graph-Biconnected-Components.svg.png
tags:
    - Algorithms
    - Linear time depth first search
    - Pseudocode
    - Other algorithms
    - Related structures
    - Equivalence relation
    - Block graph
    - Block-cut tree
    - Notes
categories:
    - Graph connectivity
---
In graph theory, a biconnected component (also known as a block or 2-connected component) is a maximal biconnected subgraph. Any connected graph decomposes into a tree of biconnected components called the block-cut tree of the graph. The blocks are attached to each other at shared vertices called cut vertices or articulation points. Specifically, a cut vertex is any vertex whose removal increases the number of connected components.
