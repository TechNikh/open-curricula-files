---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spot
offline_file: ""
offline_thumbnail: ""
uuid: ac755e2b-0490-4f1e-b94d-f7885d2cc374
updated: 1484310218
title: spot
categories:
    - Dictionary
---
spot
     n 1: a point located with respect to surface features of some
          region; "this is a nice place for a picnic" [syn: {topographic
          point}, {place}]
     2: a short section or illustration (as between radio or tv
        programs or in a magazine) that is often used for
        advertising
     3: a blemish made by dirt; "he had a smudge on his cheek" [syn:
         {smudge}, {blot}, {daub}, {smear}, {smirch}, {slur}]
     4: a small contrasting part of something; "a bald spot"; "a
        leopard's spots"; "a patch of clouds"; "patches of thin
        ice"; "a fleck of red" [syn: {speckle}, {dapple}, {patch},
         {fleck}, {maculation}]
     5: a section of an ...
