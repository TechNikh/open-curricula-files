---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/democratically
offline_file: ""
offline_thumbnail: ""
uuid: 7454d51e-6dcc-4b94-bac5-7482e774e4b6
updated: 1484310176
title: democratically
categories:
    - Dictionary
---
democratically
     adv : in a democratic manner; based on democratic principles; "it
           was decided democratically"; "democratically elected
           government" [ant: {undemocratically}]
