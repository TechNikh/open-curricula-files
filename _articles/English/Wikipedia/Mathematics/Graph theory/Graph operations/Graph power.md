---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_power
offline_file: ""
offline_thumbnail: ""
uuid: 9e04914a-5887-4c7e-98bf-4b62d1f9b051
updated: 1484309448
title: Graph power
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Square_of_a_graph.svg.png
tags:
    - Properties
    - Coloring
    - Hamiltonicity
    - Computational complexity
    - Induced subgraphs
categories:
    - Graph operations
---
In graph theory, a branch of mathematics, the kth power Gk of an undirected graph G is another graph that has the same set of vertices, but in which two vertices are adjacent when their distance in G is at most k. Powers of graphs are referred to using terminology similar to that for exponentiation of numbers: G2 is called the square of G, G3 is called the cube of G, etc.[1]
