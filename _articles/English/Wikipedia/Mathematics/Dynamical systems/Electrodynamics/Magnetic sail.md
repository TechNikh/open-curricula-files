---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Magnetic_sail
offline_file: ""
offline_thumbnail: ""
uuid: 73b42205-8e44-47f2-8093-7fd419bd47de
updated: 1484308757
title: Magnetic sail
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Magsail-particle-wind.png
tags:
    - History
    - Principles of operation and design
    - Solar wind example
    - Mini-magnetospheric plasma propulsion (M2P2)
    - Modes of operation
    - In a plasma wind
    - Inside a planetary magnetosphere
    - Interstellar travel
    - Fictional uses
categories:
    - Electrodynamics
---
A magnetic sail or magsail is a proposed method of spacecraft propulsion which would use a static magnetic field to deflect charged particles radiated by the Sun as a plasma wind, and thus impart momentum to accelerate the spacecraft.[1][2] A magnetic sail could also thrust directly against planetary and solar magnetospheres.
