---
version: 1
type: article
id: https://en.wikipedia.org/wiki/P-adic_gamma_function
offline_file: ""
offline_thumbnail: ""
uuid: 13d234d1-c2a3-4c40-a645-a3e7c9aa2c0f
updated: 1484308654
title: P-adic gamma function
categories:
    - Number theory
---
In mathematics, the p-adic gamma function Γp(s) is a function of a p-adic variable s analogous to the gamma function. It was first explicitly defined by Morita (1975), though Boyarsky (1980) pointed out that Dwork (1964) implicitly used the same function. Diamond (1977) defined a p-adic analog Gp(s) of log Γ(s). Overholtzer (1952) had previously given a definition of a different p-adic analogue of the gamma function, but his function does not have satisfactory properties and is not used much.
