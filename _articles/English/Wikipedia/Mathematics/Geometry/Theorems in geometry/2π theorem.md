---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/2%CF%80_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: c1e52cfc-a1bd-4c98-97af-e6edca61f68c
updated: 1484309105
title: 2π theorem
categories:
    - Theorems in geometry
---
In mathematics, the 2π theorem of Gromov and Thurston states a sufficient condition for Dehn filling on a cusped hyperbolic 3-manifold to result in a negatively curved 3-manifold.
