---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dramatic
offline_file: ""
offline_thumbnail: ""
uuid: dd0c3348-ece6-49d9-bc11-a1f97f27b7db
updated: 1484310587
title: dramatic
categories:
    - Dictionary
---
dramatic
     adj 1: suitable to or characteristic of drama; "a dramatic entrance
            in a swirling cape"; "a dramatic rescue at sea" [ant:
            {undramatic}]
     2: sensational in appearance or thrilling in effect; "a
        dramatic sunset"; "a dramatic pause"; "a spectacular
        display of northern lights"; "it was a spectacular play";
        "his striking good looks always created a sensation" [syn:
         {spectacular}, {striking}]
     3: pertaining to or characteristic of drama; "dramatic arts"
     4: used of a singer or singing voice that is marked by power
        and expressiveness and a histrionic or theatrical style;
        "a dramatic tenor"; "a ...
