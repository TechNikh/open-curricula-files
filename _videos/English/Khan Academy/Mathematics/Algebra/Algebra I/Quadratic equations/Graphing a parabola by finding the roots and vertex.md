---
version: 1
type: video
provider: YouTube
id: TgKBc3Igx1I
offline_file: ""
offline_thumbnail: ""
uuid: 2c9cbcc9-4d35-4a14-9b34-aca670122a3f
updated: 1484040398
title: Graphing a parabola by finding the roots and vertex
categories:
    - Quadratic equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/TgKBc3Igx1I/default.jpg
    - https://i3.ytimg.com/vi/TgKBc3Igx1I/1.jpg
    - https://i3.ytimg.com/vi/TgKBc3Igx1I/2.jpg
    - https://i3.ytimg.com/vi/TgKBc3Igx1I/3.jpg
---
