---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Finsler%E2%80%93Hadwiger_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 063a29d8-cec7-4acb-b5d5-8c8a38c07f77
updated: 1484309114
title: Finsler–Hadwiger theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/280px-Hadwiger_finsler_theorem.svg.png
categories:
    - Theorems in geometry
---
The Finsler–Hadwiger theorem is statement in Euclidean plane geometry that describes a third square derived from any two squares that share a vertex. The theorem is named after Paul Finsler and Hugo Hadwiger, who published it in 1937 as part of the same paper in which they published the Hadwiger–Finsler inequality relating the side lengths and area of a triangle.[1]
