---
version: 1
type: video
provider: YouTube
id: r8KHIxscCkg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Beginning%20sentences%20with%20conjunctions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d06a9555-1363-4b87-bd80-fbc8e6e2f5c0
updated: 1486069601
title: Beginning sentences with conjunctions
tags:
    - Education
    - online learning
    - learning
    - lessons
    - sentence-initial conjunctions
    - sentences with conjunctions
    - can I start a sentence with but
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/r8KHIxscCkg/default.jpg
    - https://i3.ytimg.com/vi/r8KHIxscCkg/1.jpg
    - https://i3.ytimg.com/vi/r8KHIxscCkg/2.jpg
    - https://i3.ytimg.com/vi/r8KHIxscCkg/3.jpg
---
Is it okay to begin a sentence with a conjunction like "but" or "because"? Yes. David explains why.

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/conjunctions/v/correlative-conjunctions?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
