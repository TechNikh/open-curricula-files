---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sentence
offline_file: ""
offline_thumbnail: ""
uuid: 6fc32a06-80b2-4358-badb-e34e0cbec2e5
updated: 1484310371
title: sentence
categories:
    - Dictionary
---
sentence
     n 1: a string of words satisfying the grammatical rules of a
          language; "he always spoke in grammatical sentences"
     2: (criminal law) a final judgment of guilty in a criminal case
        and the punishment that is imposed; "the conviction came
        as no surprise" [syn: {conviction}, {judgment of
        conviction}, {condemnation}] [ant: {acquittal}]
     3: the period of time a prisoner is imprisoned; "he served a
        prison term of 15 months"; "his sentence was 5 to 10
        years"; "he is doing time in the county jail" [syn: {prison
        term}, {time}]
     v : pronounce a sentence on (somebody) in a court of law; "He
         was condemned to ten ...
