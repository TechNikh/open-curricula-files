---
version: 1
type: video
provider: YouTube
id: kaSIjIzAtYA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Mitosis%2C%20Meiosis%20and%20Sexual%20Reproduction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: dbd8a013-6a86-48ca-a2f0-70befd57c99b
updated: 1486069581
title: Mitosis, Meiosis and Sexual Reproduction
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/kaSIjIzAtYA/default.jpg
    - https://i3.ytimg.com/vi/kaSIjIzAtYA/1.jpg
    - https://i3.ytimg.com/vi/kaSIjIzAtYA/2.jpg
    - https://i3.ytimg.com/vi/kaSIjIzAtYA/3.jpg
---
Mitosis, meiosis and sexual reproduction.  Understanding gametes, zygotes,  and haploid / diploid numbers.
