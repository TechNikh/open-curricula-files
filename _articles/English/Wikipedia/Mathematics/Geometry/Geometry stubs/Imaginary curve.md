---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Imaginary_curve
offline_file: ""
offline_thumbnail: ""
uuid: 6719bc18-7011-4da2-a9a1-e0235e72a6da
updated: 1484309363
title: Imaginary curve
categories:
    - Geometry stubs
---
For example, the set of pairs of complex numbers 
  
    
      
        (
        x
        ,
        y
        )
      
    
    {\displaystyle (x,y)}
  
 satisfying the equation 
  
    
      
        
          x
          
            2
          
        
        +
        
          y
          
            2
          
        
        =
        −
        1
      
    
    {\displaystyle x^{2}+y^{2}=-1}
  
 forms an imaginary circle, containing points such as 
  
    
      
        (
        i
        ,
        0
        )
      
    
    {\displaystyle (i,0)}
  
 and 
  
    
      
        (
        
          
            
              5
              i
            
         ...
