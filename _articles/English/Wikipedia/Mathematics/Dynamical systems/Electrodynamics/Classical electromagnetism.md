---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Classical_electromagnetism
offline_file: ""
offline_thumbnail: ""
uuid: f7526a8b-e521-4314-bd81-45fa31c1f0db
updated: 1484308749
title: Classical electromagnetism
tags:
    - History
    - Lorentz force
    - The electric field E
    - Electromagnetic waves
    - General field equations
    - Models
categories:
    - Electrodynamics
---
Classical electromagnetism or classical electrodynamics is a branch of theoretical physics that studies the interactions between electric charges and currents using an extension of the classical Newtonian model. The theory provides an excellent description of electromagnetic phenomena whenever the relevant length scales and field strengths are large enough that quantum mechanical effects are negligible. For small distances and low field strengths, such interactions are better described by quantum electrodynamics.
