---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stature
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484519041
title: stature
categories:
    - Dictionary
---
stature
     n 1: high level of respect gained by impressive development or
          achievement; "a man of great stature"
     2: natural height of a person or animal in an upright position
        [syn: {height}]
