---
version: 1
type: video
provider: YouTube
id: EJqo90lNYLs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2acedb4f-22e3-415f-91ac-ef52cc1ec4e4
updated: 1486069651
title: Trippy Animation courtesy of Anthony Francisco Schepperd
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/EJqo90lNYLs/default.jpg
    - https://i3.ytimg.com/vi/EJqo90lNYLs/1.jpg
    - https://i3.ytimg.com/vi/EJqo90lNYLs/2.jpg
    - https://i3.ytimg.com/vi/EJqo90lNYLs/3.jpg
---
