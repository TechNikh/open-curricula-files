---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drowned
offline_file: ""
offline_thumbnail: ""
uuid: 46933795-aea6-4e2e-a0dc-cc87f349a8a9
updated: 1484310541
title: drowned
categories:
    - Dictionary
---
drowned
     adj : dead by drowning; "poor drowned sailors"
