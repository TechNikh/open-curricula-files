---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Configuration_graph
offline_file: ""
offline_thumbnail: ""
uuid: a3b57a49-f663-4a56-aa20-68df872d6c5a
updated: 1484308616
title: Configuration graph
tags:
    - Definition
    - Useful property
    - Size of the graph
    - Use of this object
    - Bibliography
categories:
    - Graph theory
---
Configuration graphs are a theoretical tool used in computational complexity theory to prove a relation between graph reachability and complexity classes.[citation needed]
