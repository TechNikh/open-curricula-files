---
version: 1
type: video
provider: YouTube
id: m6c6dlmUT1c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Parametric%20equations%20and%20polar%20coordinates/Parametric%20equations%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2a7bf67f-dd94-433d-9e8a-010acb5f7ff9
updated: 1486069694
title: Parametric equations 1
categories:
    - Parametric equations and polar coordinates
thumbnail_urls:
    - https://i3.ytimg.com/vi/m6c6dlmUT1c/default.jpg
    - https://i3.ytimg.com/vi/m6c6dlmUT1c/1.jpg
    - https://i3.ytimg.com/vi/m6c6dlmUT1c/2.jpg
    - https://i3.ytimg.com/vi/m6c6dlmUT1c/3.jpg
---
