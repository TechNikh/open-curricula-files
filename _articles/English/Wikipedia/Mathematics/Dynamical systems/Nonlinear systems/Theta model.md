---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Theta_model
offline_file: ""
offline_thumbnail: ""
uuid: 2e26672d-f148-4222-8919-ab7b5d196e35
updated: 1484308813
title: Theta model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-SNIC.png
tags:
    - Background and history
    - Characteristics of the model
    - General equations
    - Model equations and properties
    - Example
    - Derivation
    - Phase response curve
    - Similar models
    - "Plant's model"
    - Quadratic integrate-and-fire
    - Applications
    - Neuroscience
    - Lobster stomatogastric ganglion
    - Visual cortex
    - Theta networks
    - Artificial intelligence
    - Steepest gradient descent learning rule
    - Limitations
categories:
    - Nonlinear systems
---
The theta model (or Ermentrout-Kopell canonical model) is a biological neuron model originally developed to model neurons in the animal Aplysia, and later used in various fields of computational neuroscience. The model is particularly well suited to describe neuron bursting, which are rapid oscillations in the membrane potential of a neuron interrupted by periods of relatively little oscillation. Bursts are often found in neurons responsible for controlling and maintaining steady rhythms. For example, breathing is controlled by a small network of bursting neurons in the brain stem. Of the three main classes of bursting neurons (square wave bursting, parabolic bursting, and elliptic ...
