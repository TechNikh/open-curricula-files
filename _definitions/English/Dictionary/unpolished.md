---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unpolished
offline_file: ""
offline_thumbnail: ""
uuid: 83247754-bd47-4036-b62b-d01df0cae90b
updated: 1484310537
title: unpolished
categories:
    - Dictionary
---
unpolished
     adj 1: not carefully reworked or perfected or made smooth by
            polishing; "dull unpolished shoes" [ant: {polished}]
     2: lacking social polish; "too gauche to leave the room when
        the conversation became intimate"; "their excellent
        manners always may be feel gauche" [syn: {gauche}, {graceless}]
