---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/optical
offline_file: ""
offline_thumbnail: ""
uuid: da0d1bad-4727-4c7c-beec-6b4c69821d5e
updated: 1484310212
title: optical
categories:
    - Dictionary
---
optical
     adj 1: relating to or using sight; "ocular inspection"; "an optical
            illusion"; "visual powers"; "visual navigation" [syn:
            {ocular}, {optic}, {visual}]
     2: of or relating to or involving light or optics; "optical
        supplies"
     3: of or relating to or resembling the eye; "ocular muscles";
        "an ocular organ"; "ocular diseases"; "the optic (or
        optical) axis of the eye"; "an ocular spot is a pigmented
        organ or part believed to be sensitive to light" [syn: {ocular},
         {optic}, {opthalmic}]
