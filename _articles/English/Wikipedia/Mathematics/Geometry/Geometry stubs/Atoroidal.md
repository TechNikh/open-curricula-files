---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Atoroidal
offline_file: ""
offline_thumbnail: ""
uuid: dd3586fd-10d2-433a-a0f7-41866c7cd9ee
updated: 1484309352
title: Atoroidal
categories:
    - Geometry stubs
---
In mathematics, an atoroidal 3-manifold is one that does not contain an essential torus. There are two major variations in this terminology: a torus may be defined geometrically, as an embedded, non-boundary parallel, incompressible torus, or it may be defined algebraically, as a subgroup 
  
    
      
        
          Z
        
        ×
        
          Z
        
      
    
    {\displaystyle \mathbb {Z} \times \mathbb {Z} }
  
 of its fundamental group that is not conjugate to a peripheral subgroup (i.e. the image of the map on fundamental group induced by an inclusion of a boundary component). The terminology is not standardized, and different authors require atoroidal ...
