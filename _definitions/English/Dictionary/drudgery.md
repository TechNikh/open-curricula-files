---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drudgery
offline_file: ""
offline_thumbnail: ""
uuid: 5b5f52ae-557c-43a6-b965-6e421d714beb
updated: 1484310539
title: drudgery
categories:
    - Dictionary
---
drudgery
     n : hard monotonous routine work [syn: {plodding}, {grind}, {donkeywork}]
