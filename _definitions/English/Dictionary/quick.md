---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quick
offline_file: ""
offline_thumbnail: ""
uuid: c6d4037d-f04d-4515-9f27-a7a6a613ad40
updated: 1484310309
title: quick
categories:
    - Dictionary
---
quick
     adj 1: accomplished rapidly and without delay; "was quick to make
            friends"; "his quick reaction prevented an accident";
            "hoped for a speedy resolution of the problem"; "a
            speedy recovery"; "he has a right to a speedy trial"
            [syn: {speedy}]
     2: hurried and brief; "paid a flying visit"; "took a flying
        glance at the book"; "a quick inspection"; "a fast visit"
        [syn: {flying}, {fast}]
     3: moving quickly and lightly; "sleek and agile as a gymnast";
        "as nimble as a deer"; "nimble fingers"; "quick of foot";
        "the old dog was so spry it was halfway up the stairs
        before we could stop it" [syn: ...
