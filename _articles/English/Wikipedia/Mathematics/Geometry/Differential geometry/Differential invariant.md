---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Differential_invariant
offline_file: ""
offline_thumbnail: ""
uuid: 7d7f4ea8-4d05-4cbf-b15c-01043f785da5
updated: 1484309218
title: Differential invariant
tags:
    - Definition
    - Applications
    - Notes
categories:
    - Differential geometry
---
In mathematics, a differential invariant is an invariant for the action of a Lie group on a space that involves the derivatives of graphs of functions in the space. Differential invariants are fundamental in projective differential geometry, and the curvature is often studied from this point of view.[1] Differential invariants were introduced in special cases by Sophus Lie in the early 1880s and studied by Georges Henri Halphen at the same time. Lie (1884) was the first general work on differential invariants, and established the relationship between differential invariants, invariant differential equations, and invariant differential operators.
