---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Blocking_set
offline_file: ""
offline_thumbnail: ""
uuid: 168020f1-98fb-4249-b2f8-0907c8c00307
updated: 1484308573
title: Blocking set
tags:
    - Definition
    - Examples
    - Size
    - History
    - In hypergraphs
    - Complete k-arcs
    - Rédei blocking sets
    - Affine blocking sets
    - Notes
categories:
    - Combinatorics
---
In geometry, specifically projective geometry, a blocking set is a set of points in a projective plane which every line intersects and which does not contain an entire line. The concept can be generalized in several ways. Instead of talking about points and lines, one could deal with n-dimensional subspaces and m-dimensional subspaces, or even more generally, objects of type 1 and objects of type 2 when some concept of intersection makes sense for these objects. A second way to generalize would be to move into more abstract settings than projective geometry. One can define a blocking set of a hypergraph as a set that meets all edges of the hypergraph.
