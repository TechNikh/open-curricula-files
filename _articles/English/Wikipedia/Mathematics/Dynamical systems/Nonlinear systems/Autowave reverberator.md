---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Autowave_reverberator
offline_file: ""
offline_thumbnail: ""
uuid: 2b58ad7d-02a3-46f6-9ea8-b94e1cc7e6ba
updated: 1484308813
title: Autowave reverberator
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Diagram_of_reverberator_evolution_in_FitzHugh-Nagumo_model.jpg
tags:
    - Introduction
    - Basic information
    - "Historical" definition
    - On the question of terminology
    - Types of reverberator behaviour
    - The "classical" regimes
    - Regimes of induced drift
    - Autowave lacet
    - >
        The reasons for distinguishing between variants of rotating
        autowaves
    - Notes
categories:
    - Nonlinear systems
---
A reverberator appears a result of a rupture in the front of a plane autowave. Such a rupture may occur, for example, via collision of the front with a nonexcitable obstacle. In this case, depending on the conditions, either of two phenomena may arise: a spiral wave, which rotates around the obstacle, or an autowave reverberator which rotates with its tip free.
