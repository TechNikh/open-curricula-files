---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Honey-bee
offline_file: ""
offline_thumbnail: ""
uuid: 8300f715-a150-4c88-b03b-aa865d9e2ee1
updated: 1484308312
title: Honey bee
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gaint_Honey_Bee_%2528Apis_dorsata%2529_on_Tribulus_terrestris_W_IMG_1020.jpg'
tags:
    - Etymology and name
    - Origin, systematics and distribution
    - Genetics
    - Micrapis
    - Megapis
    - Apis
    - Africanized bee
    - Beekeeping
    - Colony collapse disorder
    - Lifecycle
    - Winter survival
    - Pollination
    - Nutrition
    - Bee products
    - Honey
    - Nectar
    - Beeswax
    - Pollen
    - Bee bread
    - Propolis
    - Sexes and castes
    - Drones
    - Workers
    - Queens
    - Defense
    - Competition
    - Communication
    - Symbolism
categories:
    - Uncategorized
---
A honey bee (or honeybee) is any bee member of the genus Apis, primarily distinguished by the production and storage of honey and the construction of perennial, colonial nests from wax. Currently, only seven species of honey bee are recognized, with a total of 44 subspecies,[1] though historically, from six to eleven species have been recognized. The best known honey bee is the Western honey bee which has been domesticated for honey production and crop pollination. Honey bees represent only a small fraction of the roughly 20,000[2] known species of bees. Some other types of related bees produce and store honey, including the stingless honey bees, but only members of the genus Apis are true ...
