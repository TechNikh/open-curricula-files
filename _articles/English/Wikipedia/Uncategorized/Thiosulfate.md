---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thiosulphate
offline_file: ""
offline_thumbnail: ""
uuid: 63889b0e-0baf-423d-89c3-9f17538bf483
updated: 1484308309
title: Thiosulfate
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Thiosulfate-ion-2D-dimensions.png
tags:
    - Formation
    - Reactions
    - Nomenclature
    - Biochemistry
categories:
    - Uncategorized
---
The prefix thio- indicates that the thiosulfate ion is a sulfate ion with one oxygen replaced by sulfur. Thiosulfate has a tetrahedral molecular shape with C3v symmetry. Thiosulfate occurs naturally and is produced by certain biochemical processes. It rapidly dechlorinates water and is notable for its use to halt bleaching in the paper-making industry. Thiosulfate is also useful in smelting silver ore, in producing leather goods, and to set dyes in textiles.
