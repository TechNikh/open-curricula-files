---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gromov_product
offline_file: ""
offline_thumbnail: ""
uuid: e17b6e42-b446-4525-8350-24ebc8eb4729
updated: 1484309266
title: Gromov product
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tripod_%2528graph%2529.svg.png'
tags:
    - Definition
    - Motivation
    - Properties
    - Points at infinity
    - δ-hyperbolic spaces and divergence of geodesics
categories:
    - Metric geometry
---
In mathematics, the Gromov product is a concept in the theory of metric spaces named after the mathematician Mikhail Gromov. The Gromov product can also be used to define δ-hyperbolic metric spaces in the sense of Gromov.
