---
version: 1
type: video
provider: YouTube
id: hlXc_eEtBHA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Hydrocarbon%20Derivatives%20-%20Crash%20Course%20Chemistry%20%2343.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 97a1f835-a2dd-4d13-bdb0-5d54e3638494
updated: 1486069680
title: 'Hydrocarbon Derivatives - Crash Course Chemistry #43'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/hlXc_eEtBHA/default.jpg
    - https://i3.ytimg.com/vi/hlXc_eEtBHA/1.jpg
    - https://i3.ytimg.com/vi/hlXc_eEtBHA/2.jpg
    - https://i3.ytimg.com/vi/hlXc_eEtBHA/3.jpg
---
