---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/workhouse
offline_file: ""
offline_thumbnail: ""
uuid: da92861b-fbed-410d-b21c-f1fc6f286065
updated: 1484310148
title: workhouse
categories:
    - Dictionary
---
workhouse
     n 1: a poorhouse where able-bodied poor are compelled to labor
     2: a county jail that holds prisoners for periods up to 18
        months
