---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Harmonic_divisor_number
offline_file: ""
offline_thumbnail: ""
uuid: 7b1043ed-4236-4dbd-b24f-5cb173c546e2
updated: 1484308645
title: Harmonic divisor number
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Perfect_number_Cuisenaire_rods_6.png
tags:
    - Examples
    - Factorization of the harmonic mean
    - Harmonic divisor numbers and perfect numbers
    - Bounds and computer searches
categories:
    - Number theory
---
In mathematics, a harmonic divisor number, or Ore number (named after Øystein Ore who defined it in 1948), is a positive integer whose divisors have a harmonic mean that is an integer. The first few harmonic divisor numbers are
