---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Variation_of_information
offline_file: ""
offline_thumbnail: ""
uuid: cc94a65e-29bf-4116-93ac-99e93ad9a5d1
updated: 1484308773
title: Variation of information
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-VennDiagramIncludingVI.svg.png
tags:
    - Definition
    - Identities
categories:
    - Entropy and information
---
In probability theory and information theory, the variation of information or shared information distance is a measure of the distance between two clusterings (partitions of elements). It is closely related to mutual information; indeed, it is a simple linear expression involving the mutual information. Unlike the mutual information, however, the variation of information is a true metric, in that it obeys the triangle inequality.[1]
