---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chloride
offline_file: ""
offline_thumbnail: ""
uuid: 1fea7f75-8ae8-4b1a-9326-a26cc1e1df60
updated: 1484310371
title: chloride
categories:
    - Dictionary
---
chloride
     n 1: any compound containing a chlorine atom
     2: any salt of hydrochloric acid (containing the chloride ion)
