---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rough
offline_file: ""
offline_thumbnail: ""
uuid: d77863ba-2a00-45a8-af9c-cb17f9e5273e
updated: 1484310469
title: rough
categories:
    - Dictionary
---
rough
     adj 1: having or caused by an irregular surface; "trees with rough
            bark"; "rough ground"; "a rough ride"; "rough skin";
            "rough blankets"; "his unsmooth face"; "unsmooth
            writing" [syn: {unsmooth}] [ant: {smooth}]
     2: (of persons or behavior) lacking refinement or finesse; "she
        was a diamond in the rough"; "rough manners"
     3: not quite exact or correct; "the approximate time was 10
        o'clock"; "a rough guess"; "a ballpark estimate" [syn: {approximate},
         {approximative}]
     4: full of hardship or trials; "the rocky road to success";
        "they were having a rough time" [syn: {rocky}]
     5: violently agitated ...
