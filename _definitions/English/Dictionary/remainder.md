---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/remainder
offline_file: ""
offline_thumbnail: ""
uuid: a407e45a-457d-40db-a7d3-73f80eb1d7b6
updated: 1484310138
title: remainder
categories:
    - Dictionary
---
remainder
     n 1: something left after other parts have been taken away;
          "there was no remainder"; "he threw away the rest"; "he
          took what he wanted and I got the balance" [syn: {balance},
           {residual}, {residue}, {residuum}, {rest}]
     2: the part of the dividend that is left over when the dividend
        is not evenly divisible by the divisor
     3: the number that remains after subtraction; the number that
        when added to the subtrahend gives the minuend [syn: {difference}]
     4: a piece of cloth that is left over after the rest has been
        used or sold [syn: {end}, {remnant}, {oddment}]
     v : sell cheaply as remainders; "The publisher ...
