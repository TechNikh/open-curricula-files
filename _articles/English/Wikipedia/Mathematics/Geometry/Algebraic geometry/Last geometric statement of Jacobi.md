---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Last_geometric_statement_of_Jacobi
offline_file: ""
offline_thumbnail: ""
uuid: 08a3f094-029c-4261-8c5a-f56943cb0038
updated: 1484309159
title: Last geometric statement of Jacobi
categories:
    - Algebraic geometry
---
In differential geometry and algebraic geometry, the last geometric statement of Jacobi is a conjecture named after Carl Gustav Jacob Jacobi. According to this conjecture, every caustic from any point 
  
    
      
        p
      
    
    {\displaystyle p}
  
 on an ellipsoid other than umbilical points has exactly four cusps.
