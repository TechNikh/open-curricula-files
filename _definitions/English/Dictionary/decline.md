---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decline
offline_file: ""
offline_thumbnail: ""
uuid: d1d21d3f-2ba9-4aa6-a8b5-cd2457cbc68d
updated: 1484310260
title: decline
categories:
    - Dictionary
---
decline
     n 1: change toward something smaller or lower [syn: {diminution}]
     2: a condition inferior to an earlier condition; a gradual
        falling off from a better state [syn: {declination}] [ant:
         {improvement}]
     3: a gradual decrease; as of stored charge or current [syn: {decay}]
     4: a downward slope or bend [syn: {descent}, {declivity}, {fall},
         {declination}, {declension}, {downslope}] [ant: {ascent}]
     v 1: grow worse; "Conditions in the slum worsened" [syn: {worsen}]
          [ant: {better}]
     2: refuse to accept; "He refused my offer of hospitality" [syn:
         {refuse}, {reject}, {pass up}, {turn down}] [ant: {accept}]
     3: show ...
