---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tropical_regions
offline_file: ""
offline_thumbnail: ""
uuid: 5efb1eae-d46e-4251-881e-5558b3348d78
updated: 1484308372
title: Tropics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-World_map_indicating_tropics_and_subtropics.png
tags:
    - Seasons and climate
    - Tropical ecosystems
    - Tropicality
    - Image gallery
categories:
    - Uncategorized
---
The tropics are a region of the Earth surrounding the equator. They are delimited in latitude by the Tropic of Cancer in the Northern Hemisphere at 23°26′13.5″ (or 23.4371°) N and the Tropic of Capricorn in the Southern Hemisphere at 23°26′13.5″ (or 23.4371°) S; these latitudes correspond to the axial tilt of the Earth. The tropics are also referred to as the tropical zone and the torrid zone (see geographical zone). The tropics include all the areas on the Earth where the Sun is at a point directly overhead at least once during the solar year (which is a subsolar point).
