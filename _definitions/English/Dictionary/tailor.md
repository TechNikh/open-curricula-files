---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tailor
offline_file: ""
offline_thumbnail: ""
uuid: 6748a5f3-503c-4f68-9c80-61ae89cad036
updated: 1484310451
title: tailor
categories:
    - Dictionary
---
tailor
     n : a person whose occupation is making and altering garments
         [syn: {seamster}, {sartor}]
     v 1: make fit for a specific purpose [syn: {shoehorn}]
     2: style and tailor in a certain fashion; "cut a dress" [syn: {cut}]
     3: create (clothes) with cloth; "Can the seamstress sew me a
        suit by next week?" [syn: {sew}, {tailor-make}]
