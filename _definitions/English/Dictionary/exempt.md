---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exempt
offline_file: ""
offline_thumbnail: ""
uuid: c8e5db75-940b-4e0f-b94c-732c9bfce4bb
updated: 1484310464
title: exempt
categories:
    - Dictionary
---
exempt
     adj 1: (of persons) freed from or not subject to an obligation or
            liability (as e.g. taxes) to which others or other
            things are subject; "a beauty somehow exempt from the
            aging process"; "exempt from jury duty"; "only the
            very poorest citizens should be exempt from income
            taxes" [ant: {nonexempt}]
     2: (of goods or funds) not subject to taxation; "the funds of
        nonprofit organizations are nontaxable"; "income exempt
        from taxation" [syn: {nontaxable}] [ant: {taxable}]
     v 1: grant relief or an exemption from a rule or requirement to;
          "She exempted me from the exam" [syn: {relieve}, {free}]
 ...
