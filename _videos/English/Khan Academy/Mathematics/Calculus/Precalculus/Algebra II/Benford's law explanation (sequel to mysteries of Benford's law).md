---
version: 1
type: video
provider: YouTube
id: SZUDoEdjTzg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Algebra%20II/Benford%27s%20law%20explanation%20%28sequel%20to%20mysteries%20of%20Benford%27s%20law%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 1546c01e-7925-4773-bfda-487803c865aa
updated: 1486069694
title: "Benford's law explanation (sequel to mysteries of Benford's law)"
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/SZUDoEdjTzg/default.jpg
    - https://i3.ytimg.com/vi/SZUDoEdjTzg/1.jpg
    - https://i3.ytimg.com/vi/SZUDoEdjTzg/2.jpg
    - https://i3.ytimg.com/vi/SZUDoEdjTzg/3.jpg
---
