---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Problem_of_Apollonius
offline_file: ""
offline_thumbnail: ""
uuid: 21b0690e-70b0-4cd2-8c8e-4e42f0e0a811
updated: 1484309187
title: Problem of Apollonius
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Apollonius_problem_typical_solution.svg.png
tags:
    - Statement of the problem
    - History
    - Solution methods
    - Intersecting hyperbolas
    - "Viète's reconstruction"
    - Algebraic solutions
    - Lie sphere geometry
    - Inversive methods
    - Pairs of solutions by inversion
    - Inversion to an annulus
    - Resizing and inversion
    - Shrinking one given circle to a point
    - Resizing two given circles to tangency
    - "Gergonne's solution"
    - Special cases
    - Ten combinations of points, circles, and lines
    - Number of solutions
    - "Mutually tangent given circles: Soddy's circles and Descartes' theorem"
    - Generalizations
    - Applications
categories:
    - Conformal geometry
---
In Euclidean plane geometry, Apollonius's problem is to construct circles that are tangent to three given circles in a plane (Figure 1). Apollonius of Perga (ca. 262 BC – ca. 190 BCE) posed and solved this famous problem in his work Ἐπαφαί (Epaphaí, "Tangencies"); this work has been lost, but a 4th-century report of his results by Pappus of Alexandria has survived. Three given circles generically have eight different circles that are tangent to them (Figure 2) and each solution circle encloses or excludes the three given circles in a different way: in each solution, a different subset of the three circles is enclosed (its complement is excluded) and there are 8 subsets of a set ...
