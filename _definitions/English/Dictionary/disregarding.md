---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disregarding
offline_file: ""
offline_thumbnail: ""
uuid: bfd5e991-3b59-48c4-9e80-97b8f5902de3
updated: 1484310168
title: disregarding
categories:
    - Dictionary
---
disregarding
     adv : in spite of everything; without regard to drawbacks; "he
           carried on regardless of the difficulties" [syn: {regardless},
            {irrespective}, {disregardless}, {no matter}]
