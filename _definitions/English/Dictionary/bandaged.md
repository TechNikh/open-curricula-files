---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bandaged
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484608381
title: bandaged
categories:
    - Dictionary
---
bandaged
     adj : covered or wrapped with a bandage; "the bandaged wound on
           the back of his head"; "an injury bound in fresh gauze"
           [syn: {bound}]
