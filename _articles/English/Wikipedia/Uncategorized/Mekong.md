---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mekong
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1486069201
title: Mekong
thumbnail_urls:
    - http://eschool2go.org/
tags:
    - Names
    - Course
    - Drainage basin
    - Upper basin
    - Lower basin
    - Water flow along its course
    - River modifications
    - Natural history
    - The fisheries
    - Navigation
categories:
    - Uncategorized
---
The Mekong is a trans-boundary river in Southeast Asia. It is the world's 12th-longest river[2] and the 7th-longest in Asia. Its estimated length is 4,350 km (2,703 mi),[2] and it drains an area of 795,000 km2 (307,000 sq mi), discharging 475 km3 (114 cu mi) of water annually.[3]
