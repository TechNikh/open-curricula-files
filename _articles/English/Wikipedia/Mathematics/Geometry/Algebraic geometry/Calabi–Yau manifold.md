---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Calabi%E2%80%93Yau_manifold'
offline_file: ""
offline_thumbnail: ""
uuid: c3092eb2-9bc5-4fa7-9a23-60f6c3b6eab8
updated: 1484309136
title: Calabi–Yau manifold
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-CalabiYau5.jpg
tags:
    - Definitions
    - Examples
    - Applications in superstring theory
    - Citations
    - Bibliography
categories:
    - Algebraic geometry
---
A Calabi–Yau manifold, also known as a Calabi–Yau space, is a special type of manifold that is described in certain branches of mathematics such as algebraic geometry. The Calabi–Yau manifold's properties, such as Ricci flatness, also yield applications in theoretical physics. Particularly in superstring theory, the extra dimensions of spacetime are sometimes conjectured to take the form of a 6-dimensional Calabi–Yau manifold, which led to the idea of mirror symmetry.
