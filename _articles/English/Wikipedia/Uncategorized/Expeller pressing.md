---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Expell
offline_file: ""
offline_thumbnail: ""
uuid: e584fce8-de03-4c2f-88a4-e39b8b37e225
updated: 1484308378
title: Expeller pressing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Expeller.jpg
tags:
    - Overview
    - Efficiency
    - Design
    - Continuous screw
    - Interrupted screw
    - Resistor teeth
    - Expanded applications
categories:
    - Uncategorized
---
Expeller pressing (also called oil pressing) is a mechanical method for extracting oil from raw materials trademarked by Anderson International Corp. Mr. Valerius D. Anderson founded the V. D. Anderson Company in Cleveland, Ohio in 1888. In 1900, Mr. V. D. Anderson created the first successful continuously operated Expeller® press.[1] The raw materials are squeezed under high pressure in a single step. When used for the extraction of food oils, typical raw materials are nuts, seeds and algae, which are supplied to the press in a continuous feed. As the raw material is pressed, friction causes it to heat up; in the case of harder nuts (which require higher pressures) the material can exceed ...
