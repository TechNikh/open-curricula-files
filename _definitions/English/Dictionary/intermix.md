---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intermix
offline_file: ""
offline_thumbnail: ""
uuid: dc82e992-d65f-431b-9096-85d43d1fca2a
updated: 1484310407
title: intermix
categories:
    - Dictionary
---
intermix
     v : combine into one; "blend the nuts and raisins together"; "he
         blends in with the crowd"; "We don't intermingle much"
         [syn: {blend}, {immingle}, {intermingle}]
