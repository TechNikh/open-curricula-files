---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parks
offline_file: ""
offline_thumbnail: ""
uuid: f3d8c52d-1a3b-4cf3-943c-abf8ab4c3fcd
updated: 1484310240
title: parks
categories:
    - Dictionary
---
Parks
     n : United States civil rights leader who refused to give up her
         seat on a bus to a white man in Montgomery (Alabama) and
         so triggered the national civil rights movement (born in
         1913) [syn: {Rosa Parks}]
