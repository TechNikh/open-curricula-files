---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Degree-preserving_randomization
offline_file: ""
offline_thumbnail: ""
uuid: 2b6a7691-c5ab-49f1-aa4b-f6f03abbefcb
updated: 1484309434
title: Degree-preserving randomization
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Degree_Preserving_Randomization_rewire_step.svg.png
tags:
    - Background
    - Uses
    - Example
categories:
    - Network theory
---
Degree Preserving Randomization is a technique used in Network Science that aims to assess whether or not variations observed in a given graph could simply be an artifact of the graph's inherent structural properties rather than properties unique to the nodes, in an observed network.
