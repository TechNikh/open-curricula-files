---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heparin
offline_file: ""
offline_thumbnail: ""
uuid: 6053d7e3-617b-4ef0-a1a3-efe1cf76c7af
updated: 1484310160
title: heparin
categories:
    - Dictionary
---
heparin
     n : a polysaccharide produced in basophils (especially in the
         lung and liver) and that inhibit the activity of thrombin
         in coagulation of the blood; heparin sodium (trade names
         Lipo-Hepin and Liquaemin) is used as an anticoagulant in
         the treatment of thrombosis and in heart surgery [syn: {Lipo-Hepin},
          {Liquaemin}]
