---
version: 1
type: article
id: https://en.wikipedia.org/wiki/News_bulletin
offline_file: ""
offline_thumbnail: ""
uuid: 824ee19a-9309-4695-be6f-7acfc82028a5
updated: 1484308363
title: Breaking news
tags:
    - Formats
    - Television
    - Radio
    - Usage
    - Criticism
categories:
    - Uncategorized
---
Breaking news, interchangeably termed latebreaking news and also known as a special report or special coverage or news bulletin, is a current issue that broadcasters feel warrants the interruption of scheduled programming and/or current news in order to report its details. Its use is also assigned to the most significant story of the moment or a story that is being covered live. It could be a story that is simply of wide interest to viewers and has little impact otherwise.[1][2] Many times, breaking news is used after the news organization has already reported on the story. When a story has not been reported on previously, the graphic and phrase "Just In" is sometimes used instead.
