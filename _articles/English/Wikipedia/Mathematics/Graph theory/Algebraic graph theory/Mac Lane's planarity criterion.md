---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Mac_Lane%27s_planarity_criterion'
offline_file: ""
offline_thumbnail: ""
uuid: c64b8615-be46-47f2-8d01-99be0b6c2a01
updated: 1484309376
title: "Mac Lane's planarity criterion"
tags:
    - Statement
    - Existence of a 2-basis for planar graphs
    - Necessity of planarity when a 2-basis exists
    - Application
categories:
    - Algebraic graph theory
---
In graph theory, Mac Lane's planarity criterion is a characterisation of planar graphs in terms of their cycle spaces, named after Saunders Mac Lane, who published it in 1937. It states that a finite undirected graph is planar if and only if the cycle space of the graph (taken modulo 2) has a cycle basis in which each edge of the graph participates in at most two basis vectors.
