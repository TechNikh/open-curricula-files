---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arnold_tongue
offline_file: ""
offline_thumbnail: ""
uuid: 56dd6cd6-0046-4be9-ad44-052498e898bb
updated: 1484308715
title: Arnold tongue
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Circle_map_poincare_recurrence.jpeg
tags:
    - Standard circle map
    - Mode locking
    - Chirikov standard map
    - Applications
categories:
    - Chaotic maps
---
In mathematics, particularly in dynamical systems theory, an Arnold tongue of a finite-parameter family of circle maps, named after Vladimir Arnold, is a region in the space of parameters where the map has locally-constant rational rotation number. In other words, it is a level set of a rotation number with nonempty interior.
