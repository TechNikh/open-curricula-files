---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/abnormal
offline_file: ""
offline_thumbnail: ""
uuid: 27705719-f43c-4f13-9e70-4daaa17a3484
updated: 1484310189
title: abnormal
categories:
    - Dictionary
---
abnormal
     adj 1: not normal; not typical or usual or regular or conforming to
            a norm; "abnormal powers of concentration"; "abnormal
            amounts of rain"; "abnormal circumstances"; "an
            abnormal interest in food" [ant: {normal}]
     2: departing from the normal in e.g. intelligence and
        development; "they were heartbroken when they learned
        their child was abnormal"; "an abnormal personality" [ant:
         {normal}]
     3: much greater than the normal; "abnormal profits"; "abnormal
        ambition"
