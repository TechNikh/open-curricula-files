---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pot
offline_file: ""
offline_thumbnail: ""
uuid: 4f52e5ac-b7e3-40b9-a6f2-edf952ec0f56
updated: 1484310148
title: pot
categories:
    - Dictionary
---
pot
     n 1: metal or earthenware cooking vessel that is usually round
          and deep; often has a handle and lid
     2: a plumbing fixture for defecation and urination [syn: {toilet},
         {can}, {commode}, {crapper}, {potty}, {stool}, {throne}]
     3: the quantity contained in a pot [syn: {potful}]
     4: a container in which plants are cultivated [syn: {flowerpot}]
     5: (often followed by `of') a large number or amount or extent;
        "a batch of letters"; "a deal of trouble"; "a lot of
        money"; "he made a mint on the stock market"; "it must
        have cost plenty" [syn: {batch}, {deal}, {flock}, {good
        deal}, {great deal}, {hatful}, {heap}, {lot}, ...
