---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reduction
offline_file: ""
offline_thumbnail: ""
uuid: 4f410332-1337-4708-8241-aff059781e8a
updated: 1484310252
title: reduction
categories:
    - Dictionary
---
reduction
     n 1: the act of decreasing or reducing something [syn: {decrease},
           {diminution}, {step-down}] [ant: {increase}]
     2: any process in which electrons are added to an atom or ion
        (as by removing oxygen or adding hydrogen); always occurs
        accompanied by oxidation of the reducing agent [syn: {reducing}]
     3: the act of reducing complexity [syn: {simplification}]
