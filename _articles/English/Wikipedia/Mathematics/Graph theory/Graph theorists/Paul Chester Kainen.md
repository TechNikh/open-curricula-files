---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paul_Chester_Kainen
offline_file: ""
offline_thumbnail: ""
uuid: 4d24a7e4-159d-49ae-8c78-60676c2b5010
updated: 1484309466
title: Paul Chester Kainen
tags:
    - Biography
    - Selected publications
categories:
    - Graph theorists
---
Paul Chester Kainen is an American mathematician, an adjunct associate professor of mathematics and director of the Lab for Visual Mathematics at Georgetown University.[1] Kainen is the author of a popular book on the four color theorem,[2] and is also known for his work on book embeddings of graphs.[3]
