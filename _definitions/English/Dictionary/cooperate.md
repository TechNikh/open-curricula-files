---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cooperate
offline_file: ""
offline_thumbnail: ""
uuid: a387aabd-e426-4a2e-8dad-d55f44e17531
updated: 1484310405
title: cooperate
categories:
    - Dictionary
---
cooperate
     v : work together on a common enterprise of project; "The
         soprano and the pianist did not get together very well";
         "We joined forces with another research group" [syn: {collaborate},
          {join forces}, {get together}]
