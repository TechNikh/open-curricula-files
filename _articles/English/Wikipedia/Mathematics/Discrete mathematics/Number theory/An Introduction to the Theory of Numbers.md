---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/An_Introduction_to_the_Theory_of_Numbers
offline_file: ""
offline_thumbnail: ""
uuid: 12bbd586-c6d7-4882-9c46-58f8ae8ef2bb
updated: 1484308649
title: An Introduction to the Theory of Numbers
categories:
    - Number theory
---
The book grew out of a series of lectures by Hardy and Wright and was first published in 1938.
