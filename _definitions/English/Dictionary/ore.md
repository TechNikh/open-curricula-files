---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ore
offline_file: ""
offline_thumbnail: ""
uuid: 660bb5f5-e245-42fe-9547-aa6be22a6b87
updated: 1484310399
title: ore
categories:
    - Dictionary
---
ore
     n 1: a metal-bearing mineral valuable enough to be mined
     2: a monetary subunit in Denmark and Norway and Sweden; 100 ore
        equal 1 krona
