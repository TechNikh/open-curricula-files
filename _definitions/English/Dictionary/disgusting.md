---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disgusting
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484535781
title: disgusting
categories:
    - Dictionary
---
disgusting
     adj : highly offensive; arousing aversion or disgust; "a
           disgusting smell"; "distasteful language"; "a loathsome
           disease"; "the idea of eating meat is repellent to me";
           "revolting food"; "a wicked stench" [syn: {disgustful},
            {distasteful}, {foul}, {loathly}, {loathsome}, {repellent},
            {repellant}, {repelling}, {revolting}, {skanky}, {wicked},
            {yucky}]
