---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weyl_distance_function
offline_file: ""
offline_thumbnail: ""
uuid: fa9c72ab-5724-4da5-a463-98587352859e
updated: 1484309275
title: Weyl distance function
tags:
    - Definitions
    - Properties
    - Abstract characterization of buildings
categories:
    - Metric geometry
---
In combinatorial geometry, the Weyl distance function is a function that behaves in some ways like the distance function of a metric space, but instead of taking values in the positive real numbers, it takes values in a group of reflections, called the Weyl group (named for Hermann Weyl). This distance function is defined on the collection of chambers in a mathematical structure known as a building, and its value on a pair of chambers a minimal sequence of reflections (in the Weyl group) to go from one chamber to the other. An adjacent sequence of chambers in a building is known as a gallery, so the Weyl distance function is a way of encoding the information of a minimal gallery between two ...
