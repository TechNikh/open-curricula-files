---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swampy
offline_file: ""
offline_thumbnail: ""
uuid: fb475cf7-995f-4e98-be1b-3c22732e639f
updated: 1484310437
title: swampy
categories:
    - Dictionary
---
swampy
     adj : (of soil) soft and watery; "the ground was boggy under
           foot"; "a marshy coastline"; "miry roads"; "wet mucky
           lowland"; "muddy barnyard"; "quaggy terrain"; "the
           sloughy edge of the pond"; "swampy bayous" [syn: {boggy},
            {marshy}, {miry}, {mucky}, {muddy}, {quaggy}, {sloughy}]
