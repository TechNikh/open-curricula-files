---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brilliant
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484505721
title: brilliant
categories:
    - Dictionary
---
brilliant
     adj 1: of surpassing excellence; "a brilliant performance"; "a
            superb actor" [syn: {superb}]
     2: having or marked by unusual and impressive intelligence;
        "some men dislike brainy women"; "a brilliant mind"; "a
        brilliant solution to the problem" [syn: {brainy}, {smart
        as a whip}]
     3: characterized by or attended with brilliance or grandeur;
        "the brilliant court life at Versailles"; "a glorious work
        of art"; "magnificent cathedrals"; "the splendid
        coronation ceremony" [syn: {glorious}, {magnificent}, {splendid}]
     4: having striking color; "bright greens"; "brilliant
        tapestries"; "a bird with vivid ...
