---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inverted
offline_file: ""
offline_thumbnail: ""
uuid: 786fec7c-ae91-4bef-8115-ec1e0496a6ff
updated: 1484310268
title: inverted
categories:
    - Dictionary
---
inverted
     adj 1: being in such a position that top and bottom are reversed;
            "a quotation mark is sometimes called an inverted
            comma"; "an upside-down cake" [syn: {upside-down}]
     2: (of a plant ovule) completely inverted; turned back 180
        degrees on its stalk [syn: {anatropous}] [ant: {amphitropous}]
