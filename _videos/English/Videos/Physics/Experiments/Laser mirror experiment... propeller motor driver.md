---
version: 1
type: video
provider: YouTube
id: jqaA42wOMnk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a5868cf8-cfb0-4823-9deb-9941b85dd878
updated: 1486069639
title: Laser mirror experiment... propeller motor driver
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/jqaA42wOMnk/default.jpg
    - https://i3.ytimg.com/vi/jqaA42wOMnk/1.jpg
    - https://i3.ytimg.com/vi/jqaA42wOMnk/2.jpg
    - https://i3.ytimg.com/vi/jqaA42wOMnk/3.jpg
---
