---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fatty
offline_file: ""
offline_thumbnail: ""
uuid: 0fa122e0-a31b-4570-a986-ba2e5172dab2
updated: 1484310423
title: fatty
categories:
    - Dictionary
---
fatty
     adj : containing or composed of fat; "fatty food"; "fat tissue"
           [syn: {fat}] [ant: {nonfat}]
     n : a rotund individual [syn: {fatso}, {fat person}, {roly-poly},
          {butterball}] [ant: {thin person}]
     [also: {fattiest}, {fattier}]
