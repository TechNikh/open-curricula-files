---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Kinetic_smallest_enclosing_disk
offline_file: ""
offline_thumbnail: ""
uuid: ffc94a8a-2043-48c0-b4b8-06b2a7124744
updated: 1484309173
title: Kinetic smallest enclosing disk
tags:
    - 2D
    - Approximate 2D
    - Higher dimensions
categories:
    - Computational geometry
---
