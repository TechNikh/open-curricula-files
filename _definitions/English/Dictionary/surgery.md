---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surgery
offline_file: ""
offline_thumbnail: ""
uuid: 6bf220bb-2202-486c-b24c-a20ab33beb62
updated: 1484310479
title: surgery
categories:
    - Dictionary
---
surgery
     n 1: the branch of medical science that treats disease or injury
          by operative procedures; "he is professor of surgery at
          the Harvard Medical School"
     2: a room where a doctor or dentist can be consulted; "he read
        the warning in the doctor's surgery"
     3: a room in a hospital equipped for the performance of
        surgical operations; "great care is taken to keep the
        operating rooms aseptic" [syn: {operating room}, {OR}, {operating
        theater}, {operating theatre}]
     4: a medical procedure involving an incision with instruments;
        performed to repair damage or arrest disease in a living
        body; "they will schedule ...
