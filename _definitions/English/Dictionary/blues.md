---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blues
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484386501
title: blues
categories:
    - Dictionary
---
blues
     n 1: a type of folksong that originated among Black Americans at
          the beginning of the 20th century; has a melancholy
          sound from repeated use of blue notes
     2: a state of depression; "he had a bad case of the blues"
        [syn: {blue devils}, {megrims}, {vapors}, {vapours}]
