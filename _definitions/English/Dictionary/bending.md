---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bending
offline_file: ""
offline_thumbnail: ""
uuid: 50a847f9-3d8a-42b0-a547-50b75b6849fd
updated: 1484310216
title: bending
categories:
    - Dictionary
---
bending
     adj : not remaining rigid or straight; "tried to support his
           weight on a bending cane"
     n 1: movement that causes the formation of a curve [syn: {bend}]
     2: the property of being bent or deflected [syn: {deflection},
        {deflexion}]
     3: the act of bending something
