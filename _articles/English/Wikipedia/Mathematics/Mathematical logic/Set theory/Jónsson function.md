---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/J%C3%B3nsson_function'
offline_file: ""
offline_thumbnail: ""
uuid: 4b6acc05-c973-4670-9d3f-7db22dc9b828
updated: 1484309506
title: Jónsson function
categories:
    - Set theory
---
In mathematical set theory, an ω-Jónsson function for a set x of ordinals is a function 
  
    
      
        f
        :
        [
        x
        
          ]
          
            ω
          
        
        →
        x
      
    
    {\displaystyle f:[x]^{\omega }\to x}
  
 with the property that, for any subset y of x with the same cardinality as x, the restriction of 
  
    
      
        f
      
    
    {\displaystyle f}
  
 to 
  
    
      
        [
        y
        
          ]
          
            ω
          
        
      
    
    {\displaystyle [y]^{\omega }}
  
 is surjective on 
  
    
      
        x
      
    
    {\displaystyle x}
  
. Here 
  ...
