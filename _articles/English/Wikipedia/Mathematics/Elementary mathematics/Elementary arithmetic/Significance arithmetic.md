---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Significance_arithmetic
offline_file: ""
offline_thumbnail: ""
uuid: cab12b76-cf5f-41fc-8712-cfc7af4eb337
updated: 1484308921
title: Significance arithmetic
tags:
    - Multiplication and division using significance arithmetic
    - Addition and subtraction using significance arithmetic
    - Transcendental functions
    - Rounding rules
    - Disagreements about importance
categories:
    - Elementary arithmetic
---
Significance arithmetic is a set of rules (sometimes called significant figure rules) for approximating the propagation of uncertainty in scientific or statistical calculations. These rules can be used to find the appropriate number of significant figures to use to represent the result of a calculation. If a calculation is done without analysis of the uncertainty involved, a result that is written with too many significant figures can be taken to imply a higher precision than is known, and a result that is written with too few significant figures results in an avoidable loss of precision. Understanding these rules requires a good understanding of the concept of significant and insignificant ...
