---
version: 1
type: article
id: https://en.wikipedia.org/wiki/K-tree
offline_file: ""
offline_thumbnail: ""
uuid: cd2aa0a2-afdf-4c9d-accb-6f12784be3e4
updated: 1484309411
title: K-tree
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Goldner-Harary_graph.svg.png
categories:
    - Graph minor theory
---
In graph theory, a k-tree is an undirected graph formed by starting with a (k + 1)-vertex complete graph and then repeatedly adding vertices in such a way that each added vertex has exactly k neighbors that, together, the k + 1 vertices form a clique.[1][2]
