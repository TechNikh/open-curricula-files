---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/critical
offline_file: ""
offline_thumbnail: ""
uuid: bda5d1a3-d0d9-4852-b234-286d15b3b7b0
updated: 1484310246
title: critical
categories:
    - Dictionary
---
critical
     adj 1: marked by a tendency to find and call attention to errors
            and flaws; "a critical attitude" [ant: {uncritical}]
     2: at or of a point at which a property or phenomenon suffers
        an abrupt change especially having enough mass to sustain
        a chain reaction; "a critical temperature of water is 100
        degrees C--its boiling point at standard atmospheric
        pressure"; "critical mass"; "go critical" [ant: {noncritical}]
     3: characterized by careful evaluation and judgment; "a
        critical reading"; "a critical dissertation"; "a critical
        analysis of Melville's writings" [ant: {uncritical}]
     4: urgently needed; absolutely ...
