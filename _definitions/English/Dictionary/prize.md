---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prize
offline_file: ""
offline_thumbnail: ""
uuid: 9255508e-1c71-40aa-a8af-114a88aaf335
updated: 1484310299
title: prize
categories:
    - Dictionary
---
prize
     adj : of superior grade; "choice wines"; "prime beef"; "prize
           carnations"; "quality paper"; "select peaches" [syn: {choice},
            {prime(a)}, {quality}, {select}]
     n 1: something given for victory or superiority in a contest or
          competition or for winning a lottery; "the prize was a
          free trip to Europe" [syn: {award}]
     2: goods or money obtained illegally [syn: {loot}, {booty}, {pillage},
         {plunder}, {swag}, {dirty money}]
     3: something given as a token of victory [syn: {trophy}]
     v 1: hold dear; "I prize these old photographs" [syn: {value}, {treasure},
           {appreciate}]
     2: to move or force, especially in ...
