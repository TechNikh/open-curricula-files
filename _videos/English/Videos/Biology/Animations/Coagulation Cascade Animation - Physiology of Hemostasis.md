---
version: 1
type: video
provider: YouTube
id: cy3a__OOa2M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 829573d5-3b48-4ce8-a519-e282733221a0
updated: 1486069670
title: 'Coagulation Cascade Animation - Physiology of Hemostasis'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/cy3a__OOa2M/default.jpg
    - https://i3.ytimg.com/vi/cy3a__OOa2M/1.jpg
    - https://i3.ytimg.com/vi/cy3a__OOa2M/2.jpg
    - https://i3.ytimg.com/vi/cy3a__OOa2M/3.jpg
---
