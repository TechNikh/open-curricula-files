---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/afflicted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484500741
title: afflicted
categories:
    - Dictionary
---
afflicted
     adj 1: grievously affected especially by disease [syn: {stricken}]
     2: mentally or physically unfit [syn: {impaired}]
