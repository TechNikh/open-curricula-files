---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/victorious
offline_file: ""
offline_thumbnail: ""
uuid: 9d10bc0c-7370-47af-a1f0-15e4d7c38559
updated: 1484310553
title: victorious
categories:
    - Dictionary
---
victorious
     adj 1: having won; "the victorious entry"; "the winning team" [syn:
             {winning}]
     2: experiencing triumph [syn: {triumphant}]
