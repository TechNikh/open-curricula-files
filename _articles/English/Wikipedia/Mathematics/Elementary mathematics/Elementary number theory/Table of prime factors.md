---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Table_of_prime_factors
offline_file: ""
offline_thumbnail: ""
uuid: fc0cdff4-2460-4310-b0a4-599d301a0101
updated: 1484308942
title: Table of prime factors
tags:
    - Properties
    - 1 to 100
    - 101 to 200
    - 201 to 300
    - 301 to 400
    - 401 to 500
    - 501 to 600
    - 601 to 700
    - 701 to 800
    - 801 to 900
    - 901 to 1000
categories:
    - Elementary number theory
---
When n is a prime number, the prime factorization is just n itself, written in bold below.
