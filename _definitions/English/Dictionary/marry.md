---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484463061
title: marry
categories:
    - Dictionary
---
marry
     v 1: take in marriage [syn: {get married}, {wed}, {conjoin}, {hook
          up with}, {get hitched with}, {espouse}]
     2: perform a marriage ceremony; "The minister married us on
        Saturday"; "We were wed the following week"; "The couple
        got spliced on Hawaii" [syn: {wed}, {tie}, {splice}]
     [also: {married}]
