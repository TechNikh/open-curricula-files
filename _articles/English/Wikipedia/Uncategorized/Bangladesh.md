---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bangladesh
offline_file: ""
offline_thumbnail: ""
uuid: 0f638b29-3427-4975-a7d4-7b001e5692e7
updated: 1484308413
title: Bangladesh
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Somapura_Mahavihara%252C_Bangladesh.jpg'
tags:
    - Etymology
    - History
    - Ancient and classical Bengal
    - Islamic Bengal
    - British Bengal
    - Eastern wing of Pakistan
    - Genocide and war of independence
    - Bangladeshi Republic
    - geography
    - Climate
    - biodiversity
    - Politics
    - government
    - Foreign affairs
    - Military
    - Human rights and corruption
    - Administrative divisions
    - Economy
    - Primary
    - secondary
    - Tertiary
    - Transport
    - Energy
    - Water
    - science and technology
    - IT Outsourcing
    - Demographics
    - Urban centres
    - Languages
    - religion
    - Education
    - Health
    - culture
    - Literature
    - Women in Bangladesh
    - Architecture
    - Performance arts
    - Textiles
    - Cuisine
    - Festivals
    - Sports
    - Media and cinema
    - Rickshaws
    - Museums and libraries
    - Cited sources
categories:
    - Uncategorized
---
Bangladesh (i/ˌbæŋɡləˈdɛʃ/; /ˌbɑːŋɡləˈdɛʃ/; বাংলাদেশ, pronounced: [ˈbaŋlad̪eʃ] ( listen), lit. "The country of Bengal"), officially the People's Republic of Bangladesh (গণপ্রজাতন্ত্রী বাংলাদেশ Gônôprôjatôntri Bangladesh), is a sovereign state in South Asia. It forms the largest and eastern portion the ethno-linguistic region of Bengal. Located at the apex of the Bay of Bengal, the country is bordered by India and Myanmar and is separated from Nepal and Bhutan by the narrow Siliguri Corridor.[8] With a population of 170 million, it is the world's eighth-most populous country, the fifth-most populous in ...
