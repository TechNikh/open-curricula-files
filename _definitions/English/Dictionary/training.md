---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/training
offline_file: ""
offline_thumbnail: ""
uuid: ad1b17f7-fda5-43d9-ab63-69af77073dcf
updated: 1484310484
title: training
categories:
    - Dictionary
---
training
     n 1: activity leading to skilled behavior [syn: {preparation}, {grooming}]
     2: the result of good upbringing (especially knowledge of
        correct social behavior); "a woman of breeding and
        refinement" [syn: {education}, {breeding}]
