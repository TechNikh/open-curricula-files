---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/booklet
offline_file: ""
offline_thumbnail: ""
uuid: 89f99ae6-c7a9-4a93-a0ea-99a0d9383014
updated: 1484310158
title: booklet
categories:
    - Dictionary
---
booklet
     n : a small book usually having a paper cover [syn: {brochure},
         {folder}, {leaflet}, {pamphlet}]
