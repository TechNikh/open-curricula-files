---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deliberate
offline_file: ""
offline_thumbnail: ""
uuid: 5c94bd3e-7ab1-4c62-bbf4-d9cf59c88b45
updated: 1484310475
title: deliberate
categories:
    - Dictionary
---
deliberate
     adj 1: by conscious design or purpose; "intentional damage"; "a
            knowing attempt to defraud"; "a willful waste of time"
            [syn: {intentional}, {knowing}, {willful}, {wilful}]
     2: with care and dignity; "walking at the same measured pace";
        "with all deliberate speed" [syn: {careful}, {measured}]
     3: produced or marked by conscious design or premeditation; "a
        studied smile"; "a note of biting irony and studied
        insult"- V.L.Parrington [syn: {studied}] [ant: {unstudied}]
     4: marked by careful consideration or reflection; "a deliberate
        decision"
     5: carefully thought out in advance; "a calculated insult";
       ...
