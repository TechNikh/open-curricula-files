---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strange_nonchaotic_attractor
offline_file: ""
offline_thumbnail: ""
uuid: 77f77473-9148-4e0e-8414-25b54d73512f
updated: 1484308802
title: Strange nonchaotic attractor
categories:
    - Limit sets
---
In mathematics, a strange nonchaotic attractor (SNA) is a form of attractor, which while converging to a limit, is strange, because it is not piecewise differentiable, and also non-chaotic, in that its Lyapunov exponents are non-positive.[1] SNAs were introduced as a topic of study by Grebogi et al. in 1984.[1][2] SNAs can be distinguished from periodic, quasiperiodic and chaotic attractors using the 0-1 test for chaos.[3]
