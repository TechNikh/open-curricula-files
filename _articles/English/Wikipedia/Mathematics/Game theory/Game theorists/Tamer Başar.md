---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tamer_Ba%C5%9Far'
offline_file: ""
offline_thumbnail: ""
uuid: df23a2bf-4972-4678-81b0-8bd316308331
updated: 1484308520
title: Tamer Başar
tags:
    - Education
    - Academic life
    - Honorary degrees and chairs
    - Research areas
categories:
    - Game theorists
---
Tamer Başar, (born January 19, 1946[7] in Istanbul, Turkey) is a Turkish control theorist who holds the Swanlund Endowed Chair[8] of the Center for Advanced Study[9][10] and Professor[11] in the Department of Electrical and Computer Engineering[12] at the University of Illinois at Urbana-Champaign, USA.
