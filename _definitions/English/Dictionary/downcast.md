---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/downcast
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484621941
title: downcast
categories:
    - Dictionary
---
downcast
     adj 1: directed downward; "a downcast glance"
     2: low in spirits; "lonely and blue in a strange city";
        "depressed by the loss of his job"; "a dispirited and
        resigned expression on her face"; "downcast after his
        defeat"; "feeling discouraged and downhearted" [syn: {blue},
         {depressed}, {dispirited}, {down(p)}, {downhearted}, {down
        in the mouth}, {low}, {low-spirited}]
     n : a ventilation shaft through which air enters a mine
