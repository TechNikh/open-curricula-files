---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Colonies
offline_file: ""
offline_thumbnail: ""
uuid: 4a59d34a-9f4b-40a9-8c6f-a1c1234a2f82
updated: 1484308476
title: Colony
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Non-Self-Governing.png
tags:
    - Definitions
    - Colonies in ancient civilizations (examples)
    - Modern colonies (historical examples)
    - Current colonies
categories:
    - Uncategorized
---
In politics and history, a colony is a territory under the immediate political control of a state, distinct from the home territory of the sovereign. For colonies in antiquity, city-states would often found their own colonies. Some colonies were historically countries, while others were territories without definite statehood from their inception.
