---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/candle
offline_file: ""
offline_thumbnail: ""
uuid: 00218638-1cec-46c5-a9c1-8d891cf8139b
updated: 1484310221
title: candle
categories:
    - Dictionary
---
candle
     n 1: stick of wax with a wick in the middle [syn: {taper}, {wax
          light}]
     2: the basic unit of luminous intensity adopted under the
        Systeme International d'Unites; equal to 1/60 of the
        luminous intensity per square centimeter of a black body
        radiating at the temperature of 2,046 degrees Kelvin [syn:
         {candela}, {cd}, {standard candle}]
     v : examine eggs for freshness by holding them against a light
