---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marker
offline_file: ""
offline_thumbnail: ""
uuid: 95bef7ec-667d-430b-bff3-2ed8e551bf47
updated: 1484310607
title: marker
categories:
    - Dictionary
---
marker
     n 1: some conspicuous object used to distinguish or mark
          something; "the buoys were markers for the channel"
     2: a distinguishing symbol; "the owner's mark was on all the
        sheep" [syn: {marking}, {mark}]
     3: a writing implement for making a mark
