---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Narasimha
offline_file: ""
offline_thumbnail: ""
uuid: ffb39b8b-5f03-4fdc-a439-4cdcf58a7910
updated: 1484308428
title: Narasimha
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Narasimha_oil_colour.jpg
tags:
    - Etymology
    - Scriptural sources
    - References from the Vedas
    - Lord Narasiṃha and Prahlāda
    - Narasiṃha and Ādi Śaṅkara
    - Mode of worship
    - Prayers
    - Symbolism
    - Significance
    - Forms of Narasiṃha
    - Early images
    - Cultural Tradition of Procession (Śrī Nṛsiṃha Yātrā)
    - Temples dedicated to Narasiṃha
    - In Andhra Pradesh and Telangana
    - In Karnataka
    - In Maharashtra and Goa
    - In Tamil Nadu
    - In Kerala
    - In Rajasthan
    - In other places
categories:
    - Uncategorized
---
Narsingh (Sanskrit: IAST: Narasiṃha, lit. man-lion), Narasingh, and Narasingha and Narasimhar in Dravidian languages, is an avatar of the Hindu god Vishnu, who is regarded as the supreme God in Vaishnavism and a popular deity in the broader Hinduism. The avatar of Narasimha is evidenced in early epics, iconography, and temple and festival worship for over a millennium.[1]
