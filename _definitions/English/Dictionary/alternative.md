---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alternative
offline_file: ""
offline_thumbnail: ""
uuid: 8dec9262-a11f-4cf4-871f-badf97f324a4
updated: 1484310266
title: alternative
categories:
    - Dictionary
---
alternative
     adj 1: allowing a choice; "an alternative plan" [syn: {alternate}]
     2: necessitating a choice between mutually exclusive
        possibilities; "`either' and `or' in `either this or
        that'" [syn: {mutually exclusive}]
     3: pertaining to unconventional choices; "an alternative life
        style"
     n : one of a number of things from which only one can be chosen;
         "what option did I have?"; "there no other alternative";
         "my only choice is to refuse" [syn: {option}, {choice}]
