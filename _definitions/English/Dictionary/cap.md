---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cap
offline_file: ""
offline_thumbnail: ""
uuid: 0a57c496-661c-4103-9721-f7f3740e6b04
updated: 1484310224
title: cap
categories:
    - Dictionary
---
cap
     n 1: a tight-fitting headdress
     2: a top (as for a bottle)
     3: a mechanical or electrical explosive device or a small
        amount of explosive; can be used to initiate the reaction
        of a disrupting explosive [syn: {detonator}, {detonating
        device}]
     4: something serving as a cover or protection
     5: a fruiting structure resembling an umbrella that forms the
        top of a stalked fleshy fungus such as a mushroom [syn: {pileus}]
     6: an upper limit on what is allowed; "they established a cap
        for prices" [syn: {ceiling}]
     7: dental appliance consisting of an artificial crown for a
        tooth [syn: {crownwork}]
     8: the upper part ...
