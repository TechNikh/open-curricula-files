---
version: 1
type: video
provider: YouTube
id: HXOok3mfMLM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d3647d97-9af2-496f-8a86-474992a26d13
updated: 1486069682
title: 'Electric Current- Crash Course Physics #28'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/HXOok3mfMLM/default.jpg
    - https://i3.ytimg.com/vi/HXOok3mfMLM/1.jpg
    - https://i3.ytimg.com/vi/HXOok3mfMLM/2.jpg
    - https://i3.ytimg.com/vi/HXOok3mfMLM/3.jpg
---
