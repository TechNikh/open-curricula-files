---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tarski%27s_theorem_about_choice'
offline_file: ""
offline_thumbnail: ""
uuid: dbb21b59-80b0-4999-9b19-0b78b8a48765
updated: 1484309001
title: "Tarski's theorem about choice"
categories:
    - Theorems in the foundations of mathematics
---
In mathematics, the Tarski's theorem, proved by Alfred Tarski (1924), states that in ZF the theorem "For every infinite set 
  
    
      
        A
      
    
    {\displaystyle A}
  
, there is a bijective map between the sets 
  
    
      
        A
      
    
    {\displaystyle A}
  
 and 
  
    
      
        A
        ×
        A
      
    
    {\displaystyle A\times A}
  
" implies the axiom of choice. The opposite direction was already known, thus the theorem and axiom of choice are equivalent.
