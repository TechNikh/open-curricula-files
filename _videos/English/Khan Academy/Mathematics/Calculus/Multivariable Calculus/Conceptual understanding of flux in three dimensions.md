---
version: 1
type: video
provider: YouTube
id: ivg3dLTarbs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Conceptual%20understanding%20of%20flux%20in%20three%20dimensions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2644dc94-dc44-4302-8b09-cc1692145bc3
updated: 1486069689
title: Conceptual understanding of flux in three dimensions
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/ivg3dLTarbs/default.jpg
    - https://i3.ytimg.com/vi/ivg3dLTarbs/1.jpg
    - https://i3.ytimg.com/vi/ivg3dLTarbs/2.jpg
    - https://i3.ytimg.com/vi/ivg3dLTarbs/3.jpg
---
