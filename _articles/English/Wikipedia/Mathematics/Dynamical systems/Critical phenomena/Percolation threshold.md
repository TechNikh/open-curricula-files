---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Percolation_threshold
offline_file: ""
offline_thumbnail: ""
uuid: 41ae3f9f-1f35-4b0c-bb54-fb911b9257a6
updated: 1484308753
title: Percolation threshold
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-2D_continuum_percolation_with_disks.jpg
tags:
    - Percolation models
    - Thresholds on Archimedean lattices
    - Square lattice with complex neighborhoods
    - Approximate formulas for thresholds of Archimedean lattices
    - Formulas for site-bond percolation
    - Archimedean Duals (Laves Lattices)
    - 2-Uniform Lattices
    - Inhomogeneous 2-Uniform Lattice
    - Thresholds on 2D bow-tie and martini lattices
    - Thresholds on 2D covering, medial, and matching lattices
    - Thresholds on 2D chimera non-planar lattices
    - Thresholds on subnet lattices
    - Thresholds of dimers a square lattice
    - >
        Thresholds of full dimer coverings of two dimensional
        lattices
    - Thresholds of polymers (random walks) on a square lattice
    - >
        Thresholds of self-avoiding walks of length k added by
        random sequential adsorption
    - Thresholds on 2D inhomogeneous lattices
    - Thresholds for 2D continuum models
    - Thresholds on 2D random and quasi-lattices
    - Thresholds on slabs
    - Thresholds on 3D lattices
    - Dimer percolation in 3D
    - Thresholds for 3D continuum models
    - Thresholds on 3D random and quasi-lattices
    - Continuum models in higher dimensions
    - Thresholds on hypercubic lattices
    - Thresholds in higher-dimensional lattices
    - Thresholds on hyperbolic, hierarchical, and tree lattices
    - Thresholds for directed percolation
    - Exact critical manifolds of inhomogeneous systems
    - Percolation thresholds for graphs
categories:
    - Critical phenomena
---
Percolation threshold is a mathematical concept related to percolation theory, which is the formation of long-range connectivity in random systems. Below the threshold a giant connected component does not exist; while above it, there exists a giant component of the order of system size. In engineering and coffee making, percolation represents the flow of fluids through porous media, but in the mathematics and physics worlds it generally refers to simplified lattice models of random systems or networks (graphs), and the nature of the connectivity in them. The percolation threshold is the critical value of the occupation probability p, or more generally a critical surface for a group of ...
