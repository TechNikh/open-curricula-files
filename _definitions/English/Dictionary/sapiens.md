---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sapiens
offline_file: ""
offline_thumbnail: ""
uuid: c1d46140-f4d7-475d-95f8-036d9c11b339
updated: 1484310281
title: sapiens
categories:
    - Dictionary
---
sapiens
     adj : of or relating to or characteristic of Homo sapiens
