---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suggested
offline_file: ""
offline_thumbnail: ""
uuid: 6f8259eb-db2f-4a7c-a158-d4fdc468d7c9
updated: 1484310313
title: suggested
categories:
    - Dictionary
---
suggested
     adj : mentioned as worthy of acceptance; "the recommended
           medicine"; "the suggested course of study" [syn: {recommended}]
