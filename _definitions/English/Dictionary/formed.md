---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formed
offline_file: ""
offline_thumbnail: ""
uuid: 340b0f26-d828-4df0-bcd6-2dc366c533c0
updated: 1484310311
title: formed
categories:
    - Dictionary
---
formed
     adj 1: clearly defined; "I have no formed opinion about the chances
            of success" [syn: {defined}, {settled}]
     2: having or given a form or shape [ant: {unformed}]
     3: formed in the mind [syn: {conceived}]
     4: having taken on a definite arrangement; "cheerleaders were
        formed into letters"; "we saw troops formed into columns"
     5: fully developed as by discipline or training; "a fully
        formed literary style"
