---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regulation
offline_file: ""
offline_thumbnail: ""
uuid: 64f44d62-d779-471b-b050-d350d6c797c7
updated: 1484310464
title: regulation
categories:
    - Dictionary
---
regulation
     adj : prescribed by or according to regulation; "regulation army
           equipment"
     n 1: an authoritative rule [syn: {ordinance}]
     2: a principle or condition that customarily governs behavior;
        "it was his rule to take a walk before breakfast"; "short
        haircuts were the regulation" [syn: {rule}]
     3: the state of being controlled or governed
     4: (embryology) the ability of an early embryo to continue
        normal development after its structure has been somehow
        damaged or altered
     5: the act of bringing to uniformity; making regular [syn: {regularization},
         {regularisation}]
     6: the act of controlling or directing ...
