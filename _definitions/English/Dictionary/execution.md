---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/execution
offline_file: ""
offline_thumbnail: ""
uuid: 7e77f5f8-648c-4f01-86e3-724f4e288ea3
updated: 1484310571
title: execution
categories:
    - Dictionary
---
execution
     n 1: putting a condemned person to death [syn: {executing}, {capital
          punishment}, {death penalty}]
     2: the act of performing; of doing something successfully;
        using knowledge as distinguished from merely possessing
        it; "they criticised his performance as mayor";
        "experience generally improves performance" [syn: {performance},
         {carrying out}, {carrying into action}]
     3: (computer science) the process of carrying out an
        instruction by a computer [syn: {instruction execution}]
     4: (law) the completion of a legal instrument (such as a
        contract or deed) by signing it (and perhaps sealing and
        delivering ...
