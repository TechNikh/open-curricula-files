---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dictum
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484521681
title: dictum
categories:
    - Dictionary
---
dictum
     n 1: an authoritative declaration [syn: {pronouncement}, {say-so}]
     2: an opinion voiced by a judge on a point of law not directly
        bearing on the case in question and therefore not binding
        [syn: {obiter dictum}]
     [also: {dicta} (pl)]
