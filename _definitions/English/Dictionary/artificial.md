---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/artificial
offline_file: ""
offline_thumbnail: ""
uuid: 130b6754-b454-40ba-8f9c-083f109a5edc
updated: 1484310387
title: artificial
categories:
    - Dictionary
---
artificial
     adj 1: contrived by art rather than nature; "artificial flowers";
            "artificial flavoring"; "an artificial diamond";
            "artificial fibers"; "artificial sweeteners" [syn: {unreal}]
            [ant: {natural}]
     2: artificially formal; "that artificial humility that her
        husband hated"; "contrived coyness"; "a stilted letter of
        acknowledgment"; "when people try to correct their speech
        they develop a stilted pronunciation" [syn: {contrived}, {hokey},
         {stilted}]
     3: not arising from natural growth or characterized by vital
        processes
