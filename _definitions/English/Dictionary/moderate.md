---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moderate
offline_file: ""
offline_thumbnail: ""
uuid: ac54b041-b6d0-48c1-a861-aa7e0e904029
updated: 1484310230
title: moderate
categories:
    - Dictionary
---
moderate
     adj 1: being within reasonable or average limits; not excessive or
            extreme; "moderate prices"; "a moderate income"; "a
            moderate fine"; "moderate demands"; "a moderate
            estimate"; "a moderate eater"; "moderate success"; "a
            kitchen of moderate size"; "the X-ray showed moderate
            enlargement of the heart" [ant: {immoderate}]
     2: not extreme; "a moderate penalty"; "temperate in his
        response to criticism" [syn: {temperate}]
     3: marked by avoidance of extravagance or extremes; "moderate
        in his demands"; "restrained in his response" [syn: {restrained}]
     n : a person who takes a position in the ...
