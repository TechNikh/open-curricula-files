---
version: 1
type: article
id: https://en.wikipedia.org/wiki/David_Sumner
offline_file: ""
offline_thumbnail: ""
uuid: c2c422a7-ff7b-4619-b1c2-3c4c12c8ce00
updated: 1484309476
title: David Sumner
categories:
    - Graph theorists
---
David P. Sumner is an American mathematician known for his research in graph theory. He formulated Sumner's conjecture that tournaments are universal graphs for polytrees in 1971,[1] and showed in 1974 that all claw-free graphs with an even number of vertices have perfect matchings.[2]
