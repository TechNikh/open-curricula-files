---
title: Environmental impact of irrigation
article_ref: https://en.wikipedia.org/wiki/Environmental_impact_of_irrigation
tags:
    - Direct effects
    - Indirect Effects
    - Adverse impacts
    - Reduced river flow
    - Increased groundwater recharge, waterlogging, soil salinity
    - Reduced downstream river water quality
    - Affected downstream water users
    - Lost land use opportunities
    - Groundwater mining with wells, land subsidence
    - Simulation and prediction
    - Case studies
    - Reduced downstream drainage and groundwater quality
    - Mitigation of adverse effects
    - Delayed environmental impacts
    - Potential benefits outweigh the potential disadvantages
categories:
    - Water harvesting
---
