---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trench
offline_file: ""
offline_thumbnail: ""
uuid: 40ac2cd2-60ec-421c-a406-a137fd9d0d57
updated: 1484310262
title: trench
categories:
    - Dictionary
---
trench
     n 1: a ditch dug as a fortification having a parapet of the
          excavated earth
     2: a long steep-sided depression in the ocean floor [syn: {deep},
         {oceanic abyss}]
     3: any long ditch cut in the ground
     v 1: impinge or infringe upon; "This impinges on my rights as an
          individual"; "This matter entrenches on other domains"
          [syn: {impinge}, {encroach}, {entrench}]
     2: fortify by surrounding with trenches; "He trenched his
        military camp"
     3: cut or carve deeply into; "letters trenched into the stone"
     4: set, plant, or bury in a trench; "trench the fallen
        soldiers"; "trench the vegetables"
     5: cut a trench ...
