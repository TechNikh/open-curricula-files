---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fundamental_polygon
offline_file: ""
offline_thumbnail: ""
uuid: c779e0ef-295d-4db1-8200-9f3ba3d23ca2
updated: 1484309187
title: Fundamental polygon
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fundamental_parallelogram.png
tags:
    - Examples
    - Group generators
    - Standard fundamental polygons
    - Fundamental polygon of a compact Riemann surface
    - Metric fundamental polygon
    - Standard fundamental polygon
    - Example
    - area
    - Explicit form for standard polygons
    - Generalizations
categories:
    - Conformal geometry
---
In mathematics, each closed surface in the sense of geometric topology can be constructed from an even-sided oriented polygon, called a fundamental polygon, by pairwise identification of its edges.
