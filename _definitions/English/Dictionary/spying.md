---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spying
offline_file: ""
offline_thumbnail: ""
uuid: bc56023c-2960-40c1-8561-66b07786faa1
updated: 1484310180
title: spying
categories:
    - Dictionary
---
spying
     n 1: keeping a secret or furtive watch
     2: the act of keeping a secret watch for intelligence purposes
        [syn: {undercover work}]
     3: the act of detecting something; catching sight of something
        [syn: {detection}, {catching}, {espial}, {spotting}]
