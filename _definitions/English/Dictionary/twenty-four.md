---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/twenty-four
offline_file: ""
offline_thumbnail: ""
uuid: 390567f3-a6b2-4275-a4b5-017141927071
updated: 1484310431
title: twenty-four
categories:
    - Dictionary
---
twenty-four
     adj : being four more than twenty [syn: {24}, {xxiv}]
     n : the cardinal number that is the sum of twenty-three and one
         [syn: {24}, {XXIV}, {two dozen}]
