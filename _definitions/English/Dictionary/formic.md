---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formic
offline_file: ""
offline_thumbnail: ""
uuid: 6eb906f2-e728-4c7c-a6c0-26252486ea79
updated: 1484310419
title: formic
categories:
    - Dictionary
---
formic
     adj 1: of or relating to or derived from ants
     2: of or containing or derived from formic acid
