---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impurity
offline_file: ""
offline_thumbnail: ""
uuid: 6adff3ef-4f8e-4a9f-93f8-a6848b920ad3
updated: 1484310409
title: impurity
categories:
    - Dictionary
---
impurity
     n 1: worthless material that should be removed; "there were
          impurities in the water" [syn: {dross}]
     2: the condition of being impure [syn: {impureness}] [ant: {purity}]
