---
version: 1
type: article
id: https://en.wikipedia.org/wiki/18_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 2eb64313-4968-44f8-a27f-08aacfd0701c
updated: 1484308955
title: 18 (number)
tags:
    - In mathematics
    - In science
    - chemistry
    - Astronomy
    - In religion and literature
    - As lucky or unlucky number
    - Age 18
    - In sports
    - In other fields
    - Notes
categories:
    - Integers
---
In speech, the numbers 18 and 80 are sometimes confused. When carefully enunciated, they differ in which syllable is stressed: 18 /eɪtˈtiːn/ vs 80 /ˈeɪti/. However, in dates such as 1864, or when contrasting numbers in the teens, such as 17, 18, 19, the stress shifts to the first syllable: 18 /ˈeɪttiːn/. In some dialects, such as General American, there is little confusion because the single t sound in 80 becomes a d-like flap [ɾ], whereas the double t sound in 18 does not.
