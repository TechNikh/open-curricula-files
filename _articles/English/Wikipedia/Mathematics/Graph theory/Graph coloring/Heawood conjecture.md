---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Heawood_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: fb14d9ef-e5ce-4348-9a16-4360e65ff2af
updated: 1484309384
title: Heawood conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Franklin_graph.svg.png
tags:
    - Formal statement
    - Example
categories:
    - Graph coloring
---
In graph theory, the Heawood conjecture or Ringel–Youngs theorem gives a lower bound for the number of colors that are necessary for graph coloring on a surface of a given genus. For surfaces of genus 0, 1, 2, 3, 4, 5, 6, 7, ..., the required number of colors is 4, 7, 8, 9, 10, 11, 12, 12, ....  A000934, the chromatic number or Heawood number.
