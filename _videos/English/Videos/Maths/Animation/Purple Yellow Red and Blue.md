---
version: 1
type: video
provider: YouTube
id: g9GWc7htvxU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d2764da4-31be-461d-8757-e8d6e87169f7
updated: 1486069648
title: Purple Yellow Red and Blue
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/g9GWc7htvxU/default.jpg
    - https://i3.ytimg.com/vi/g9GWc7htvxU/1.jpg
    - https://i3.ytimg.com/vi/g9GWc7htvxU/2.jpg
    - https://i3.ytimg.com/vi/g9GWc7htvxU/3.jpg
---
