---
version: 1
type: video
provider: YouTube
id: uYoX_Qyknao
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Punctuation/Commas%20in%20dialogue.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0f6d1bed-e83e-494d-b14a-855341b3baf9
updated: 1486069599
title: Commas in dialogue
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Punctuation
thumbnail_urls:
    - https://i3.ytimg.com/vi/uYoX_Qyknao/default.jpg
    - https://i3.ytimg.com/vi/uYoX_Qyknao/1.jpg
    - https://i3.ytimg.com/vi/uYoX_Qyknao/2.jpg
    - https://i3.ytimg.com/vi/uYoX_Qyknao/3.jpg
---
“Today we’re going to talk about using commas in dialogue,” said David and Paige, KA’s resident grammarians. 

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/e/commas-in-dialogue?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/v/appositives-the-comma-punctuation-khan-academy?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/punctuation/the-comma/v/more-uses-for-commas-the-comma-punctuation-khan-academy?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
