---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/artificially
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484576881
title: artificially
categories:
    - Dictionary
---
artificially
     adv : not according to nature; not by natural means; "artificially
           induced conditions" [syn: {unnaturally}, {by artificial
           means}] [ant: {naturally}]
