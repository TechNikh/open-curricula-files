---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nerve
offline_file: ""
offline_thumbnail: ""
uuid: dd52d402-2108-40cb-9e62-4309862d512e
updated: 1484310349
title: nerve
categories:
    - Dictionary
---
nerve
     n 1: any bundle of nerve fibers running to various organs and
          tissues of the body [syn: {nervus}]
     2: the courage to carry on; "he kept fighting on pure spunk";
        "you haven't got the heart for baseball" [syn: {heart}, {mettle},
         {spunk}]
     3: impudent aggressiveness; "I couldn't believe her boldness";
        "he had the effrontery to question my honesty" [syn: {boldness},
         {brass}, {face}, {cheek}]
     v : get ready for something difficult or unpleasant [syn: {steel}]
