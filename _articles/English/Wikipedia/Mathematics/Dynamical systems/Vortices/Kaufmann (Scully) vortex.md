---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kaufmann_(Scully)_vortex
offline_file: ""
offline_thumbnail: ""
uuid: 0c991963-8df3-49dd-9547-31319ea4490c
updated: 1484308878
title: Kaufmann (Scully) vortex
categories:
    - Vortices
---
The Kaufmann vortex, also known as the Scully model,[1] is a mathematical model for a vortex taking account of viscosity. It uses an algebraic velocity profile.[2]
