---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forested
offline_file: ""
offline_thumbnail: ""
uuid: f95a3c3a-1439-4eed-ba3d-37ead6b81286
updated: 1484310172
title: forested
categories:
    - Dictionary
---
forested
     adj : covered with forest; "efforts to protect forested lands of
           the northwest"
