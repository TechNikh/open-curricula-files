---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Closed_geodesic
offline_file: ""
offline_thumbnail: ""
uuid: d6ffc867-1857-4d7b-9f44-b0be89543c99
updated: 1484309209
title: Closed geodesic
tags:
    - Definition
    - Examples
categories:
    - Differential geometry
---
In differential geometry and dynamical systems, a closed geodesic on a Riemannian manifold is a geodesic that forms a simple closed curve. It may be formalized as the projection of a closed orbit of the geodesic flow on the tangent space of the manifold.
