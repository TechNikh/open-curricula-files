---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/self-expression
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484522341
title: self-expression
categories:
    - Dictionary
---
self-expression
     n : the expression of one's individuality (usually through
         creative activities)
