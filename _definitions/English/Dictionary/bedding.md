---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bedding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484485621
title: bedding
categories:
    - Dictionary
---
bed
     n 1: a piece of furniture that provides a place to sleep; "he sat
          on the edge of the bed"; "the room had only a bed and
          chair"
     2: a plot of ground in which plants are growing; "the gardener
        planted a bed of roses"
     3: a depression forming the ground under a body of water; "he
        searched for treasure on the ocean bed" [syn: {bottom}]
     4: (geology) a stratum of rock (especially sedimentary rock);
        "they found a bed of standstone"
     5: a stratum of ore or coal thick enough to be mined with
        profit; "he worked in the coal beds" [syn: {seam}]
     6: single thickness of usually some homogeneous substance;
        "slices of ...
