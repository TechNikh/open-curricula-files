---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Angel_problem
offline_file: ""
offline_thumbnail: ""
uuid: 72d8f6f3-ae8f-4308-bc4d-4a9702f8de15
updated: 1484309020
title: Angel problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Angel_problem.svg.png
tags:
    - "Basic strategies and why they don't work"
    - History
    - Further unsolved questions
    - Proof sketches
    - "Guardian" proof
    - "Máthé's 2-angel proof"
    - "Bowditch's 4-angel proof"
categories:
    - Combinatorial game theory
---
The angel problem is a question in game theory proposed by John Horton Conway. The game is commonly referred to as the Angels and Devils game.[1] The game is played by two players called the angel and the devil. It is played on an infinite chessboard (or equivalently the points of a 2D lattice). The angel has a power k (a natural number 1 or higher), specified before the game starts. The board starts empty with the angel at the origin. On each turn, the angel jumps to a different empty square which could be reached by at most k moves of a chess king, i.e. the distance from the starting square is at most k in the infinity norm. The devil, on its turn, may add a block on any single square not ...
