---
version: 1
type: video
provider: YouTube
id: oQpKtm5TtxU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Probability%20and%20combinatorics/Example-%20Ways%20to%20arrange%20colors.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9df926d4-b966-4098-9f6c-e5a90d3d4941
updated: 1486069561
title: 'Example- Ways to arrange colors'
categories:
    - Probability and combinatorics
thumbnail_urls:
    - https://i3.ytimg.com/vi/oQpKtm5TtxU/default.jpg
    - https://i3.ytimg.com/vi/oQpKtm5TtxU/1.jpg
    - https://i3.ytimg.com/vi/oQpKtm5TtxU/2.jpg
    - https://i3.ytimg.com/vi/oQpKtm5TtxU/3.jpg
---
