---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extremely
offline_file: ""
offline_thumbnail: ""
uuid: 5a55d4a5-afe9-4868-beaf-f5985e869b21
updated: 1484310273
title: extremely
categories:
    - Dictionary
---
extremely
     adv 1: to a high degree or extent; favorably or with much respect;
            "highly successful"; "He spoke highly of her"; "does
            not think highly of his writing"; "extremely
            interesting" [syn: {highly}]
     2: to an extreme degree; "extremely cold"; "extremely
        unpleasant"
     3: to an extreme degree; "the house was super clean for
        Mother's visit" [syn: {super}]
     4: to an extreme degree or extent; "his eyesight was
        exceedingly defective" [syn: {exceedingly}, {passing}]
