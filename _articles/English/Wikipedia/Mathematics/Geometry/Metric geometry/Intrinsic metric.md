---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Intrinsic_metric
offline_file: ""
offline_thumbnail: ""
uuid: 88fffe96-8e45-483c-b531-c07d55c1fb30
updated: 1484309270
title: Intrinsic metric
tags:
    - Definitions
    - Examples
    - Properties
categories:
    - Metric geometry
---
In the mathematical study of metric spaces, one can consider the arclength of paths in the space. If two points are at a given distance from each other, it is natural to expect that one should be able to get from the first point to the second along a path whose arclength is equal to (or very close to) that distance. The distance between two points of a metric space relative to the intrinsic metric is defined as the infimum of the lengths of all paths from the first point to the second. A metric space is a length metric space if the intrinsic metric agrees with the original metric of the space.
