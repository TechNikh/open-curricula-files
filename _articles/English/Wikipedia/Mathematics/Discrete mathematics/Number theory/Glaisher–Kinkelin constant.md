---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Glaisher%E2%80%93Kinkelin_constant'
offline_file: ""
offline_thumbnail: ""
uuid: a2904df3-28a5-43f2-a196-ed84dbb8b394
updated: 1484308641
title: Glaisher–Kinkelin constant
categories:
    - Number theory
---
In mathematics, the Glaisher–Kinkelin constant or Glaisher's constant, typically denoted A, is a mathematical constant, related to the K-function and the Barnes G-function. The constant appears in a number of sums and integrals, especially those involving Gamma functions and zeta functions. It is named after mathematicians James Whitbread Lee Glaisher and Hermann Kinkelin.
