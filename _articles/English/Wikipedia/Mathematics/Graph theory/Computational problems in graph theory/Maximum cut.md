---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maximum_cut
offline_file: ""
offline_thumbnail: ""
uuid: c0d0da0f-0864-4800-8c35-0550a3759989
updated: 1484309394
title: Maximum cut
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Max-cut.svg.png
tags:
    - Computational complexity
    - Polynomial-time algorithms
    - Approximation algorithms
    - Notes
categories:
    - Computational problems in graph theory
---
For a graph, a maximum cut is a cut whose size is at least the size of any other cut. The problem of finding a maximum cut in a graph is known as the Max-Cut Problem.
