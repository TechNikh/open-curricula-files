---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Percolation_theory
offline_file: ""
offline_thumbnail: ""
uuid: 205eeb9c-5a8e-40d9-b9a8-2a4e8dbe50de
updated: 1484309511
title: Percolation theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Perc-wiki.png
tags:
    - Introduction
    - Universality
    - Phases
    - Subcritical and supercritical
    - Critical
    - Different models
categories:
    - Combinatorics
---
In statistical physics and mathematics, percolation theory describes the behaviour of connected clusters in a random graph. The applications of percolation theory to materials science and other domains are discussed in the article percolation.
