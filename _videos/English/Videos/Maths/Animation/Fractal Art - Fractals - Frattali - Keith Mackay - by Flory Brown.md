---
version: 1
type: video
provider: YouTube
id: nCeBKnyF-zI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 32475ed4-8a9b-4d1e-b7e9-e5431601a9a2
updated: 1486069641
title: 'Fractal Art - Fractals - Frattali - Keith Mackay - by Flory Brown'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/nCeBKnyF-zI/default.jpg
    - https://i3.ytimg.com/vi/nCeBKnyF-zI/1.jpg
    - https://i3.ytimg.com/vi/nCeBKnyF-zI/2.jpg
    - https://i3.ytimg.com/vi/nCeBKnyF-zI/3.jpg
---
