---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Double_vector_bundle
offline_file: ""
offline_thumbnail: ""
uuid: 95364378-daee-42b3-bb1b-94429710f668
updated: 1484309218
title: Double vector bundle
tags:
    - Definition and first consequences
    - Double vector bundle morphism
    - Examples
categories:
    - Differential geometry
---
In mathematics, a double vector bundle is the combination of two compatible vector bundle structures, which contains in particular the double tangent 
  
    
      
        T
        E
      
    
    {\displaystyle TE}
  
 of a vector bundle 
  
    
      
        E
      
    
    {\displaystyle E}
  
 and the double tangent bundle 
  
    
      
        
          T
          
            2
          
        
        M
      
    
    {\displaystyle T^{2}M}
  
.
