---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Percolation
offline_file: ""
offline_thumbnail: ""
uuid: e611be83-b942-4da0-a7a1-ad8dcfbc2759
updated: 1484308605
title: Percolation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Manual_coffee_preperation.jpg
tags:
    - Background
    - Examples
categories:
    - Combinatorics
---
In physics, chemistry and materials science, percolation (from Latin percōlāre, "to filter" or "trickle through") refers to the movement and filtering of fluids through porous materials.
