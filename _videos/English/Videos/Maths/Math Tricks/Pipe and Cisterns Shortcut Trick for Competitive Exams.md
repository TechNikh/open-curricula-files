---
version: 1
type: video
provider: YouTube
id: anoYJez30Vk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: be315ce2-abcd-4523-90a3-b99571c09a03
updated: 1486069622
title: Pipe and Cisterns Shortcut Trick for Competitive Exams
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/anoYJez30Vk/default.jpg
    - https://i3.ytimg.com/vi/anoYJez30Vk/1.jpg
    - https://i3.ytimg.com/vi/anoYJez30Vk/2.jpg
    - https://i3.ytimg.com/vi/anoYJez30Vk/3.jpg
---
