---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Carl_Anton_Bretschneider
offline_file: ""
offline_thumbnail: ""
uuid: ef3fcca8-ee68-441a-9bd3-365f12e1a9aa
updated: 1484309075
title: Carl Anton Bretschneider
categories:
    - Geometers
---
Carl Anton Bretschneider (27 May 1808 – 6 November 1878) was a mathematician from Gotha, Germany. Bretschneider worked in geometry, number theory, and history of geometry. He also worked on logarithmic integrals and mathematical tables. He was one of the first mathematicians to use the symbol γ for Euler's constant when he published his 1837 paper. He is best known for his discovery of Bretschneider's formula.
