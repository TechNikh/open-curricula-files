---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marble
offline_file: ""
offline_thumbnail: ""
uuid: 27f3900b-e993-45e7-87a1-aa4874f6ddb4
updated: 1484310373
title: marble
categories:
    - Dictionary
---
marble
     n 1: a hard crystalline metamorphic rock that takes a high
          polish; used for sculpture and as building material
     2: a small ball of glass that is used in various games
     3: a sculpture carved from marble
     v : paint or stain like marble; "marble paper"
