---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/services
offline_file: ""
offline_thumbnail: ""
uuid: d501cb5e-0425-473c-adb4-e61ba80629ad
updated: 1484310262
title: services
categories:
    - Dictionary
---
services
     n : performance of duties or provision of space and equipment
         helpful to others; "the mayor tried to maintain city
         services"; "the medical services are excellent"
