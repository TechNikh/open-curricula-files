---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/foodgrain
offline_file: ""
offline_thumbnail: ""
uuid: f0877900-1b10-4f50-94df-2140301b0fe3
updated: 1484310533
title: foodgrain
categories:
    - Dictionary
---
food grain
     n : foodstuff prepared from the starchy grains of cereal grasses
         [syn: {grain}, {cereal}]
