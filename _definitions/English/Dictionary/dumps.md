---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dumps
offline_file: ""
offline_thumbnail: ""
uuid: 754cecbf-7930-4fb0-9301-f217816d94e2
updated: 1484310443
title: dumps
categories:
    - Dictionary
---
dumps
     n : an informal expression for a mildly depressed state; "in the
         dumps"; "have the mopes" [syn: {mopes}]
