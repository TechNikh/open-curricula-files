---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bother
offline_file: ""
offline_thumbnail: ""
uuid: 714a708c-1e8e-4502-89ca-4899300d0706
updated: 1484310561
title: bother
categories:
    - Dictionary
---
bother
     n 1: an angry disturbance; "he didn't want to make a fuss"; "they
          had labor trouble"; "a spot of bother" [syn: {fuss}, {trouble},
           {hassle}]
     2: something or someone that causes trouble; a source of
        unhappiness; "washing dishes was a nuisance before we got
        a dish washer"; "a bit of a bother"; "he's not a friend,
        he's an infliction" [syn: {annoyance}, {botheration}, {pain},
         {infliction}, {pain in the neck}, {pain in the ass}]
     v 1: take the trouble to do something; concern oneself; "He did
          not trouble to call his mother on her birthday"; "Don't
          bother, please" [syn: {trouble oneself}, {trouble}, ...
