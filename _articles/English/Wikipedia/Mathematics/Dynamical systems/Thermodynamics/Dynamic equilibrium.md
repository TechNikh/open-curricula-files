---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dynamic_equilibrium
offline_file: ""
offline_thumbnail: ""
uuid: 5ce6ff11-adfb-4bb9-897c-532ebe139df3
updated: 1484308842
title: Dynamic equilibrium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dynamic_equilibrium.png
tags:
    - Examples
    - Relationship between equilibrium and rate constants
categories:
    - Thermodynamics
---
In chemistry, a dynamic equilibrium exists once a reversible reaction ceases to change its ratio of reactants/products, but substances move between the chemicals at an equal rate, meaning there is no net change It is a particular example of a system in a steady state. In thermodynamics a closed system is in thermodynamic equilibrium when reactions occur at such rates that the composition of the mixture does not change with time. Reactions do in fact occur, sometimes vigorously, but to such an extent that changes in composition cannot be observed. Equilibrium constants can be expressed in terms of the rate constants for elementary reactions.
