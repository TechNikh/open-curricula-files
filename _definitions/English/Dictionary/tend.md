---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tend
offline_file: ""
offline_thumbnail: ""
uuid: 0a77d93c-6e9d-4617-9d4e-61cbacdc25f8
updated: 1484310286
title: tend
categories:
    - Dictionary
---
tend
     v 1: have a tendency or disposition to do or be something; be
          inclined; "She tends to be nervous before her lectures";
          "These dresses run small"; "He inclined to corpulence"
          [syn: {be given}, {lean}, {incline}, {run}]
     2: have care of or look after; "She tends to the children"
     3: manage or run; "tend a store"
