---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/notion
offline_file: ""
offline_thumbnail: ""
uuid: 6303e0b9-7351-466e-a708-c2e7e425837b
updated: 1484310281
title: notion
categories:
    - Dictionary
---
notion
     n 1: a vague idea in which some confidence is placed; "his
          impression of her was favorable"; "what are your
          feelings about the crisis?"; "it strengthened my belief
          in his sincerity"; "I had a feeling that she was lying"
          [syn: {impression}, {feeling}, {belief}, {opinion}]
     2: a general inclusive concept
     3: an odd or fanciful or capricious idea; "the theatrical
        notion of disguise is associated with disaster in his
        stories"; "he had a whimsy about flying to the moon";
        "whimsy can be humorous to someone with time to enjoy it"
        [syn: {whim}, {whimsy}, {whimsey}]
     4: (usually plural) small personal ...
