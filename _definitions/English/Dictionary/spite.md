---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spite
offline_file: ""
offline_thumbnail: ""
uuid: cea2b010-48d8-4e96-bfd7-82ceb8e191d8
updated: 1484310309
title: spite
categories:
    - Dictionary
---
spite
     n 1: feeling a need to see others suffer [syn: {malice}, {maliciousness},
           {spitefulness}, {venom}]
     2: malevolence by virtue of being malicious or spiteful or
        nasty [syn: {cattiness}, {bitchiness}, {spitefulness}, {nastiness}]
     v : hurt the feelings of; "She hurt me when she did not include
         me among her guests"; "This remark really bruised me ego"
         [syn: {hurt}, {wound}, {injure}, {bruise}, {offend}]
