---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cinnabar
offline_file: ""
offline_thumbnail: ""
uuid: 396ac584-23c9-4afe-9c96-59b230ea0a81
updated: 1484308389
title: Cinnabar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Cinnabarit_01.jpg
tags:
    - Etymology
    - Properties and structure
    - Properties
    - Structure
    - Occurrence
    - Mining and extraction of mercury
    - Toxicity
    - Decorative use
    - Other forms
categories:
    - Uncategorized
---
Cinnabar (/ˈsɪnəbɑːr/) and cinnabarite (/sɪnəˈbɑːraɪt/), likely deriving from the Ancient Greek: κιννάβαρι[6] (kinnabari), refer to the common bright scarlet to brick-red form of mercury(II) sulfide, formula HgS, that is the most common source ore for refining elemental mercury, and is the historic source for the brilliant red or scarlet pigment termed vermilion and associated red mercury pigments.
