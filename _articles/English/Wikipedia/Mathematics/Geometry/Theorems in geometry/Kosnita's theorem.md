---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Kosnita%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 59de1052-9ba4-46c7-be36-e8f70b8c2d83
updated: 1484309117
title: "Kosnita's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Kosnita_points.svg.png
categories:
    - Theorems in geometry
---
Let 
  
    
      
        A
        B
        C
      
    
    {\displaystyle ABC}
  
 be an arbitrary triangle, 
  
    
      
        O
      
    
    {\displaystyle O}
  
 its circumcenter and 
  
    
      
        
          O
          
            a
          
        
        ,
        
          O
          
            b
          
        
        ,
        
          O
          
            c
          
        
      
    
    {\displaystyle O_{a},O_{b},O_{c}}
  
 are the circumcenters of three triangles 
  
    
      
        O
        B
        C
      
    
    {\displaystyle OBC}
  
, 
  
    
      
        O
        C
        A
      
    
    {\displaystyle OCA}
 ...
