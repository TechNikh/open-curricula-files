---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/virtue
offline_file: ""
offline_thumbnail: ""
uuid: 2930924f-a454-4ed3-9217-56e3c3c943e2
updated: 1484310573
title: virtue
categories:
    - Dictionary
---
virtue
     n 1: the quality of doing what is right and avoiding what is
          wrong [syn: {virtuousness}, {moral excellence}]
     2: any admirable quality or attribute; "work of great merit"
        [syn: {merit}] [ant: {demerit}]
     3: morality with respect to sexual relations [syn: {chastity},
        {sexual morality}]
     4: a particular moral excellence
