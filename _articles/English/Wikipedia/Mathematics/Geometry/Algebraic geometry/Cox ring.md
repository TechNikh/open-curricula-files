---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cox_ring
offline_file: ""
offline_thumbnail: ""
uuid: 0f2bf84c-bfdb-4b0d-8a51-b8e3316e423a
updated: 1484309138
title: Cox ring
categories:
    - Algebraic geometry
---
In algebraic geometry, a Cox ring is a sort of universal homogeneous coordinate ring for a projective variety, and is (roughly speaking) a direct sum of the spaces of sections of all isomorphism classes of line bundles. Cox rings were introduced by Hu & Keel (2000), based on an earlier construction by Cox (1995) for toric varieties.
