---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/english
offline_file: ""
offline_thumbnail: ""
uuid: 488dcf8a-6fa5-4bc6-9dad-18e068a29c7d
updated: 1484310525
title: english
categories:
    - Dictionary
---
English
     adj : of or relating to or characteristic of England or its
           culture; "English history"; "the English landed
           aristocracy"; "English literature"
     n 1: an Indo-European language belonging to the West Germanic
          branch; the official language of Britain and the United
          States and most of the Commonwealth countries [syn: {English
          language}]
     2: the people of England [syn: {English people}, {the English}]
     3: the discipline that studies the English language and
        literature
     4: (sports) the spin given to a ball by striking it on one side
        or releasing it with a sharp twist [syn: {side}]
