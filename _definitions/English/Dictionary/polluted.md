---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polluted
offline_file: ""
offline_thumbnail: ""
uuid: 987fee97-1145-468c-be6d-68f3f994a69e
updated: 1484310252
title: polluted
categories:
    - Dictionary
---
polluted
     adj : rendered unwholesome by contaminants and pollution; "had to
           boil the contaminated water"; "polluted lakes and
           streams" [syn: {contaminated}]
