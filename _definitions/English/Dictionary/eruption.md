---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eruption
offline_file: ""
offline_thumbnail: ""
uuid: b108b75f-456c-400b-9f93-5ad7f472c41e
updated: 1484310439
title: eruption
categories:
    - Dictionary
---
eruption
     n 1: the sudden occurrence of a violent discharge of steam and
          volcanic material [syn: {volcanic eruption}]
     2: symptom consisting of a breaking out and becoming visible
     3: (of volcanos) pouring out fumes or lava (or a deposit so
        formed) [syn: {eructation}, {extravasation}]
     4: a sudden violent spontaneous occurrence (usually of some
        undesirable condition); "the outbreak of hostilities"
        [syn: {outbreak}, {irruption}]
     5: a sudden very loud noise [syn: {bang}, {clap}, {blast}, {loud
        noise}]
     6: the emergence of a tooth as it breaks through the gum
