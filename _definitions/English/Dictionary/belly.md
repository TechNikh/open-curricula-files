---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/belly
offline_file: ""
offline_thumbnail: ""
uuid: 8d5c4c86-3c84-46d3-b407-bf2d81be954b
updated: 1484310543
title: belly
categories:
    - Dictionary
---
belly
     n 1: the region of the body of a vertebrate between the thorax
          and the pelvis [syn: {abdomen}, {venter}, {stomach}]
     2: a protruding abdomen [syn: {paunch}]
     3: a part that bulges deeply; "the belly of a sail"
     4: the hollow inside of something; "in the belly of the ship"
     5: the underpart of the body of certain vertebrates such as
        snakes or fish
     v : swell out or bulge out [syn: {belly out}]
     [also: {bellied}]
