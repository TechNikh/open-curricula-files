---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Sandi_Klav%C5%BEar'
offline_file: ""
offline_thumbnail: ""
uuid: c954a588-6740-4f6d-89c1-6f395d59f84d
updated: 1484309429
title: Sandi Klavžar
tags:
    - Education
    - Research
    - Awards and honors
categories:
    - Mathematical chemistry
---
Sandi Klavžar(born 5 February 1962) is a Slovenian mathematician working in the area of graph theory and its applications. He is a professor of mathematics at the University of Ljubljana.
