---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contents
offline_file: ""
offline_thumbnail: ""
uuid: 98ed61dc-7963-497f-af45-b77d99eef1ed
updated: 1484310327
title: contents
categories:
    - Dictionary
---
contents
     n : a list of divisions (chapters or articles) and the pages on
         which they start [syn: {table of contents}]
