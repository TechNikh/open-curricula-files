---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/smaller
offline_file: ""
offline_thumbnail: ""
uuid: 763fcc4c-dc46-47cd-83d9-b88e41b1dca9
updated: 1484310339
title: smaller
categories:
    - Dictionary
---
smaller
     adj : small or little relative to something else [syn: {littler}]
