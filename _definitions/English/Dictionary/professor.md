---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/professor
offline_file: ""
offline_thumbnail: ""
uuid: 735f2d73-adcc-4b26-9380-ea3448865cb1
updated: 1484310403
title: professor
categories:
    - Dictionary
---
professor
     n : someone who is a member of the faculty at a college or
         university [syn: {prof}]
