---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coal
offline_file: ""
offline_thumbnail: ""
uuid: cda90b1b-41ef-4936-b091-43a766ae48e1
updated: 1484310271
title: coal
categories:
    - Dictionary
---
coal
     n 1: fossil fuel consisting of carbonized vegetable matter
          deposited in the Carboniferous period
     2: a hot glowing or smouldering fragment of wood or coal left
        from a fire [syn: {ember}]
     v 1: burn to charcoal; "Without a drenching rain, the forest fire
          will char everything" [syn: {char}]
     2: supply with coal
     3: take in coal; "The big ship coaled"
