---
version: 1
type: video
provider: YouTube
id: OXvbsBN3YWg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Classical%20Physics.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 798115a4-49b5-4470-a880-0ac137befe31
updated: 1486069654
title: Classical Physics
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/OXvbsBN3YWg/default.jpg
    - https://i3.ytimg.com/vi/OXvbsBN3YWg/1.jpg
    - https://i3.ytimg.com/vi/OXvbsBN3YWg/2.jpg
    - https://i3.ytimg.com/vi/OXvbsBN3YWg/3.jpg
---
