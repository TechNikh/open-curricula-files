---
version: 1
type: video
provider: YouTube
id: v9NLtiVt3XY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Probability%20and%20combinatorics/Example-%20Ways%20to%20pick%20officers.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 46466608-2abb-4eee-9d8e-3c35270c6fb9
updated: 1486069565
title: 'Example- Ways to pick officers'
categories:
    - Probability and combinatorics
thumbnail_urls:
    - https://i3.ytimg.com/vi/v9NLtiVt3XY/default.jpg
    - https://i3.ytimg.com/vi/v9NLtiVt3XY/1.jpg
    - https://i3.ytimg.com/vi/v9NLtiVt3XY/2.jpg
    - https://i3.ytimg.com/vi/v9NLtiVt3XY/3.jpg
---
