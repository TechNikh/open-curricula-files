---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nutrient
offline_file: ""
offline_thumbnail: ""
uuid: 50718e55-8a95-4119-9c21-681a031942f3
updated: 1484310328
title: nutrient
categories:
    - Dictionary
---
nutrient
     adj : of or providing nourishment; "good nourishing stew" [syn: {alimentary},
            {alimental}, {nourishing}, {nutritious}, {nutritive}]
     n : any substance that can be metabolized by an organism to give
         energy and build tissue [syn: {food}]
