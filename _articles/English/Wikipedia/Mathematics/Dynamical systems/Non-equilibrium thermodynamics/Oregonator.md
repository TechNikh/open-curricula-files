---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oregonator
offline_file: ""
offline_thumbnail: ""
uuid: 5a8ea8f4-95c0-45cc-96c6-5f4c95d8305b
updated: 1484308805
title: Oregonator
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/750px-Flag_of_Oregon.svg.png
categories:
    - Non-equilibrium thermodynamics
---
The Oregonator is a theoretical model for a type of autocatalytic reaction. The Oregonator (Orygunator) is the simplest realistic model of the chemical dynamics of the oscillatory Belousov-Zhabotinsky reaction.[1] It was created by Richard Field and Richard M. Noyes at the University of Oregon.[2] It is a portmanteau of Oregon and oscillator.
