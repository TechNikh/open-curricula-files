---
version: 1
type: video
provider: YouTube
id: WtXWro5mNPE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5033ec20-0f63-42b2-8885-174b23fa93ee
updated: 1486069614
title: Counting in pictures
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/WtXWro5mNPE/default.jpg
    - https://i3.ytimg.com/vi/WtXWro5mNPE/1.jpg
    - https://i3.ytimg.com/vi/WtXWro5mNPE/2.jpg
    - https://i3.ytimg.com/vi/WtXWro5mNPE/3.jpg
---
