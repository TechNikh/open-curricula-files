---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sociable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484413801
title: sociable
categories:
    - Dictionary
---
sociable
     adj 1: inclined to or conducive to companionship with others; "a
            sociable occasion"; "enjoyed a sociable chat"; "a
            sociable conversation"; "Americans are sociable and
            gregarious" [ant: {unsociable}]
     2: friendly and pleasant; "a sociable gathering"
     n : a party of people assembled to promote sociability and
         communal activity [syn: {social}, {mixer}]
