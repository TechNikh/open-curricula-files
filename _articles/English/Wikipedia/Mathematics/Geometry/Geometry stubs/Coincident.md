---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coincident
offline_file: ""
offline_thumbnail: ""
uuid: ea91f6a0-c95b-440e-b5de-43618b3f019b
updated: 1484309357
title: Coincident
categories:
    - Geometry stubs
---
In geometry, two points are called coincident when they are actually the same point as each other.[1] The same word has also been used more generally to other forms of incidence or special position between geometric objects.[2]
