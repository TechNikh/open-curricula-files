---
version: 1
type: video
provider: YouTube
id: AElLVGW9kxQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0c37a37b-efc7-43ea-a438-e02f4b5012c3
updated: 1486069636
title: >
    To determine the focal length of a convex lens by focusing a
    distant object
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/AElLVGW9kxQ/default.jpg
    - https://i3.ytimg.com/vi/AElLVGW9kxQ/1.jpg
    - https://i3.ytimg.com/vi/AElLVGW9kxQ/2.jpg
    - https://i3.ytimg.com/vi/AElLVGW9kxQ/3.jpg
---
