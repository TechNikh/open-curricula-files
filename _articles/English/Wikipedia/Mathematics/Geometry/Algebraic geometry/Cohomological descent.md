---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cohomological_descent
offline_file: ""
offline_thumbnail: ""
uuid: 76796b6d-1058-4b4b-a3a1-41dc16960743
updated: 1484309136
title: Cohomological descent
categories:
    - Algebraic geometry
---
In algebraic geometry, a cohomological descent is, roughly, a "derived" version of a fully faithful descent in the classical descent theory. This point is made precise by the below: the following are equivalent:[1] in an appropriate setting, given a map a from a simplicial space X to a space S,
