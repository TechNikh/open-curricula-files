---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Seked
offline_file: ""
offline_thumbnail: ""
uuid: cd1908b6-8978-466a-84b7-bf71d49cc667
updated: 1484309244
title: Seked
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Geometrical_diagram_showing_Egyptian_seked_system.jpg
tags:
    - Pyramid slopes
categories:
    - Geometric measurement
---
The seked (or seqed) was an ancient Egyptian unit of measure for the inclination of the triangular faces of a right pyramid.[1] The system was based on the Egyptian's length measure known as the royal cubit. The royal cubit was subdivided into seven palms and each palm was further divided into four digits. The inclination of measured slopes was therefore expressed as the number of palms and digits moved horizontally for each royal cubit rise.
