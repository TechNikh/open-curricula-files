---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/junction
offline_file: ""
offline_thumbnail: ""
uuid: 77d64e0a-50f2-4188-aac7-bd161c2cbb7a
updated: 1484310198
title: junction
categories:
    - Dictionary
---
junction
     n 1: the place where two or more things come together
     2: the state of being joined together [syn: {conjunction}, {conjugation},
         {colligation}]
     3: the shape or manner in which things come together and a
        connection is made [syn: {articulation}, {join}, {joint},
        {juncture}]
     4: something that joins or connects [syn: {conjunction}]
     5: an act of joining or adjoining things [syn: {adjunction}]
