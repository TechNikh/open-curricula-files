---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Differential_graded_scheme
offline_file: ""
offline_thumbnail: ""
uuid: b00198d5-ae86-46a6-b289-d6c251f1c4de
updated: 1484309142
title: Differential graded scheme
categories:
    - Algebraic geometry
---
In algebraic geometry, a differential graded scheme is obtained by gluing affine differential graded schemes, with respect to étale topology.[1] It was introduced by Maxim Kontsevich[2] "as the first approach to derived algebraic geometry."[3] and was developed further by Mikhail Kapranov and Ionut Ciocan-Fontanine.
