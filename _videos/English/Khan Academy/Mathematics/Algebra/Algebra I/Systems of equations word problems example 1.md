---
version: 1
type: video
provider: YouTube
id: z1hz8-Kri1E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Systems%20of%20equations%20word%20problems%20example%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 57f1c61d-07ac-4507-9a78-19198af2b740
updated: 1486069563
title: Systems of equations word problems example 1
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/z1hz8-Kri1E/default.jpg
    - https://i3.ytimg.com/vi/z1hz8-Kri1E/1.jpg
    - https://i3.ytimg.com/vi/z1hz8-Kri1E/2.jpg
    - https://i3.ytimg.com/vi/z1hz8-Kri1E/3.jpg
---
