---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/modest
offline_file: ""
offline_thumbnail: ""
uuid: f199717d-ec1e-4666-9924-84c920765020
updated: 1484310521
title: modest
categories:
    - Dictionary
---
modest
     adj 1: marked by simplicity; having a humble opinion of yourself;
            "a modest apartment"; "too modest to wear his medals"
            [ant: {immodest}]
     2: not large but sufficient in size or amount; "a modest
        salary"; "modest inflation"; "helped in my own small way"
        [syn: {small}]
     3: free from pomp or affectation; "comfortable but modest
        cottages"; "a simple rectangular brick building"; "a
        simple man with simple tastes"
     4: not offensive to sexual mores in conduct or appearance [ant:
         {immodest}]
     5: low or inferior in station or quality; "a humble cottage";
        "a lowly parish priest"; "a modest man of the ...
