---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/conclude
offline_file: ""
offline_thumbnail: ""
uuid: 1aa05e78-917f-43da-b69e-0936e71054f4
updated: 1484310353
title: conclude
categories:
    - Dictionary
---
conclude
     v 1: decide by reasoning; draw or come to a conclusion; "We
          reasoned that it was cheaper to rent than to buy a
          house" [syn: {reason}, {reason out}]
     2: bring to a close; "The committee concluded the meeting"
     3: reach a conclusion after a discussion or deliberation [syn:
        {resolve}]
     4: come to a close; "The concert closed with a nocturne by
        Chopin" [syn: {close}]
     5: reach agreement on; "They concluded an economic agreement";
        "We concluded a cease-fire"
