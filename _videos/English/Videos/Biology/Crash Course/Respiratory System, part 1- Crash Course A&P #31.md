---
version: 1
type: video
provider: YouTube
id: bHZsvBdUC2I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 10b4f39b-5618-4338-8c25-10d4494ab1b0
updated: 1486069681
title: 'Respiratory System, part 1- Crash Course A&P #31'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/bHZsvBdUC2I/default.jpg
    - https://i3.ytimg.com/vi/bHZsvBdUC2I/1.jpg
    - https://i3.ytimg.com/vi/bHZsvBdUC2I/2.jpg
    - https://i3.ytimg.com/vi/bHZsvBdUC2I/3.jpg
---
