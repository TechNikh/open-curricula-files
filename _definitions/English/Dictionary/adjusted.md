---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adjusted
offline_file: ""
offline_thumbnail: ""
uuid: b7da67fa-b5f5-46a8-810a-7fb4a3e6c7e2
updated: 1484310451
title: adjusted
categories:
    - Dictionary
---
adjusted
     adj 1: altered to accommodate to certain requirements or bring into
            a proper relation; "an adjusted insurance claim"; "the
            car runs more smoothly with the timing adjusted" [ant:
             {unadjusted}]
     2: adjusted to demands of daily living; showing emotional
        stability [ant: {maladjusted}]
     3: having achieved a comfortable relationship with your
        environment [syn: {familiarized}, {familiarised}]
     4: (especially of garments) having the fit or style adjusted;
        "for my wedding I had my mother's wedding dress altered to
        fit me"
