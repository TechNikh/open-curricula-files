---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arc_(projective_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: a2c7556a-4f11-4869-bf74-b1c1602fbace
updated: 1484309251
title: Arc (projective geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hyperoval_in_Fano_plane.svg.png
tags:
    - k-arcs in a projective plane
    - k-arcs in a projective space
    - (k, d)-arcs in a projective plane
    - Notes
categories:
    - Incidence geometry
---
An (simple) arc in finite projective geometry is a set of points which satisfies, in an intuitive way, a feature of curved figures in continuous geometries. Loosely speaking, they are sets of points that are far from "line-like" in a plane or far from "plane-like" in a three-dimensional space. In this finite setting it is typical to include the number of points in the set in the name, so these simple arcs are called k-arcs. An important generalization of the k-arc concept, also referred to as arcs in the literature, are the (k, d)-arcs.
