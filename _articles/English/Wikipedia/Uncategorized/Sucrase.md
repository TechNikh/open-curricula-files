---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sucrase
offline_file: ""
offline_thumbnail: ""
uuid: 6d71f879-c3c4-4909-a5b0-66e26a270cfe
updated: 1484308349
title: Sucrase
tags:
    - Types
    - Physiology
    - Use in chemical analysis
categories:
    - Uncategorized
---
Sucrase is a digestive enzyme secreted in the mouth. Sucrase enzymes are located on the brush border of the small intestine. The enzymes catalyze the hydrolysis of sucrose to fructose and glucose. The sucrase enzyme invertase, which occurs more commonly in plants, also hydrolyzes sucrose but by a different mechanism.[1]
