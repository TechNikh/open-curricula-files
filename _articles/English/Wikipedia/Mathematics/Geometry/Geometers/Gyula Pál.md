---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gyula_P%C3%A1l'
offline_file: ""
offline_thumbnail: ""
uuid: dc8b7b44-04cb-497b-824c-9464e9a48d2e
updated: 1484309093
title: Gyula Pál
categories:
    - Geometers
---
Gyula Pál (1881– September 6, 1946) was a noted Hungarian-Danish mathematician.[1] He is known for his work on Jordan curves both in plane and space, and on the Kakeya problem. He proved that every locally connected planar continuum with at least two points is the orthogonal projection of a closed Jordan curve of the Euclidean 3-space.
