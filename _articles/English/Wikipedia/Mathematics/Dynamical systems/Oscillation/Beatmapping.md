---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Beatmapping
offline_file: ""
offline_thumbnail: ""
uuid: 5b9edd38-6c2e-41a8-a22a-bc1531168aab
updated: 1484308814
title: Beatmapping
categories:
    - Oscillation
---
Beatmapping is the detection of a beat or tempo in music using software. Beatmapping visually lays out/displays the tempo (speed) of music throughout the entirety or portion of a song or music piece. This "mapping" is done with software specifically designed for beatmapping.
