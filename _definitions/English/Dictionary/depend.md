---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/depend
offline_file: ""
offline_thumbnail: ""
uuid: 4d35d4fb-cea2-426d-9efa-f8864e180400
updated: 1484310254
title: depend
categories:
    - Dictionary
---
depend
     v 1: be contingent upon (something that is ellided); "That
          depends"
     2: have faith or confidence in; "you can count on me to help
        you any time"; "Look to your friends for support"; "You
        can bet on that!"; "Depend on your family in times of
        crisis" [syn: {count}, {bet}, {look}, {calculate}, {reckon}]
