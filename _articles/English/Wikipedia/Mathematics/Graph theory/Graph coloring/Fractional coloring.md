---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fractional_coloring
offline_file: ""
offline_thumbnail: ""
uuid: f9e8d62b-0780-48a3-808a-ae340f67330d
updated: 1484309382
title: Fractional coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Graph_fractional_coloring.svg.png
tags:
    - Definitions
    - Linear Programming (LP) Formulation
    - Applications
    - Comparison with traditional graph coloring
    - Notes
categories:
    - Graph coloring
---
Fractional coloring is a topic in a young branch of graph theory known as fractional graph theory. It is a generalization of ordinary graph coloring. In a traditional graph coloring, each vertex in a graph is assigned some color, and adjacent vertices — those connected by edges — must be assigned different colors. In a fractional coloring however, a set of colors is assigned to each vertex of a graph. The requirement about adjacent vertices still holds, so if two vertices are joined by an edge, they must have no colors in common.
