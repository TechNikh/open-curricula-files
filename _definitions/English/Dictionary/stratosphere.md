---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stratosphere
offline_file: ""
offline_thumbnail: ""
uuid: 627f1a84-f884-4231-8506-be0e06e22e01
updated: 1484310162
title: stratosphere
categories:
    - Dictionary
---
stratosphere
     n : the atmospheric layer between the troposphere and the
         mesosphere
