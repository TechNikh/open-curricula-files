---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brush
offline_file: ""
offline_thumbnail: ""
uuid: 9d46da74-2924-4cf1-98fe-ea150ea8ea10
updated: 1484310369
title: brush
categories:
    - Dictionary
---
brush
     n 1: a dense growth of bushes [syn: {brushwood}, {coppice}, {copse},
           {thicket}]
     2: an implement that has hairs or bristles firmly set into a
        handle
     3: momentary contact [syn: {light touch}]
     4: conducts current between rotating and stationary parts of a
        generator or motor
     5: a minor short-term fight [syn: {clash}, {encounter}, {skirmish}]
     6: the act of brushing your teeth; "the dentist recommended two
        brushes a day" [syn: {brushing}]
     7: the act of brushing your hair; "he gave his hair a quick
        brush" [syn: {brushing}]
     8: contact with something dangerous or undesirable; "I had a
        brush with danger ...
