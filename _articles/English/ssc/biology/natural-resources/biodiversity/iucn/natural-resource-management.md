---
title: Natural resource management
article_ref: https://en.wikipedia.org/wiki/Natural_resource_management
tags:
    - History
    - Ownership regimes
    - State property regime
    - Private property regime
    - Common property regimes
    - Non-property regimes (open access)
    - Hybrid regimes
    - Stakeholder analysis
    - Management approaches
    - Community-based natural resource management
    - Adaptive management
    - Integrated natural resource management
    - Frameworks and modelling
    - Other elements
categories:
    - IUCN
---
