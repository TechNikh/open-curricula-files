---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unhealthy
offline_file: ""
offline_thumbnail: ""
uuid: f3b84cd8-bcd7-4db1-84c9-555ff1496af2
updated: 1484310533
title: unhealthy
categories:
    - Dictionary
---
unhealthy
     adj 1: not in or exhibiting good health in body or mind; "unhealthy
            ulcers" [ant: {healthy}]
     2: detrimental to health [syn: {insalubrious}, {unhealthful}]
     3: not conducive to good health; "an unhealthy diet of fast
        foods"; "an unhealthy climate"
     [also: {unhealthiest}, {unhealthier}]
