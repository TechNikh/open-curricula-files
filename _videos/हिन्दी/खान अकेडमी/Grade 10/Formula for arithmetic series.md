---
version: 1
type: video
provider: YouTube
id: ElRtd1Z2FRc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5579fa11-13bc-4e5c-bb55-7c8fed83b416
updated: 1486069619
title: Formula for arithmetic series
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/ElRtd1Z2FRc/default.jpg
    - https://i3.ytimg.com/vi/ElRtd1Z2FRc/1.jpg
    - https://i3.ytimg.com/vi/ElRtd1Z2FRc/2.jpg
    - https://i3.ytimg.com/vi/ElRtd1Z2FRc/3.jpg
---
