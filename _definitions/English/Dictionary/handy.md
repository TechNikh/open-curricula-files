---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/handy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484508541
title: handy
categories:
    - Dictionary
---
handy
     adj 1: easy to reach; "found a handy spot for the can opener" [syn:
             {convenient}, {ready to hand(p)}]
     2: easy to use; "a handy gadget"
     3: skillful with the hands; "handy with an axe"
     n : United States blues musician who transcribed and published
         traditional blues music (1873-1958) [syn: {W. C. Handy},
         {William Christopher Handy}]
     [also: {handiest}, {handier}]
