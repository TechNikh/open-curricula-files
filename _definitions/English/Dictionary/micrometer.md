---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/micrometer
offline_file: ""
offline_thumbnail: ""
uuid: c4b6b510-dc8f-4a38-b8b9-b7d18935194b
updated: 1484310210
title: micrometer
categories:
    - Dictionary
---
micrometer
     n 1: caliper for measuring small distances [syn: {micrometer
          gauge}, {micrometer caliper}]
     2: a metric unit of length equal to one millionth of a meter
        [syn: {micron}]
