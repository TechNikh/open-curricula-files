---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Barbier%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: fab54c39-baa9-497d-81f3-8a2f7b2f5d46
updated: 1484309114
title: "Barbier's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Reuleaux_polygons.svg.png
tags:
    - Examples
    - Proofs
    - Higher dimensions
categories:
    - Theorems in geometry
---
In geometry, Barbier's theorem states that every curve of constant width has perimeter π times its width, regardless of its precise shape.[1] This theorem was first published by Joseph-Émile Barbier in 1860.[2]
