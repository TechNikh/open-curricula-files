---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hurl
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484535721
title: hurl
categories:
    - Dictionary
---
hurl
     n : a violent throw [syn: {cast}]
     v 1: throw forcefully [syn: {hurtle}, {cast}]
     2: make a thrusting forward movement [syn: {lunge}, {hurtle}, {thrust}]
     3: utter with force; utter vehemently; "hurl insults"; "throw
        accusations at someone" [syn: {throw}]
