---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edge_space
offline_file: ""
offline_thumbnail: ""
uuid: b866e124-78d3-4a39-ba48-321a66dffdbb
updated: 1484309373
title: Edge space
tags:
    - Definition
    - Properties
categories:
    - Algebraic graph theory
---
In the mathematical discipline of graph theory, the edge space and vertex space of an undirected graph are vector spaces defined in terms of the edge and vertex sets, respectively. These vector spaces make it possible to use techniques of linear algebra in studying the graph.
