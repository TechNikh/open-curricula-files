---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maximal_set
offline_file: ""
offline_thumbnail: ""
uuid: a25d229d-e637-4593-9956-96dd6256ddd0
updated: 1484309489
title: Maximal set
categories:
    - Computability theory
---
In recursion theory, the mathematical theory of computability, a maximal set is a coinfinite recursively enumerable subset A of the natural numbers such that for every further recursively enumerable subset B of the natural numbers, either B is cofinite or B is a finite variant of A or B is not a superset of A. This gives an easy definition within the lattice of the recursively enumerable sets.
