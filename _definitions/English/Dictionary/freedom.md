---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/freedom
offline_file: ""
offline_thumbnail: ""
uuid: cfa70abd-6410-4abf-8f7a-a8c52894446f
updated: 1484310387
title: freedom
categories:
    - Dictionary
---
freedom
     n 1: the condition of being free; the power to act or speak or
          think without externally imposed restraints
     2: immunity from an obligation or duty [syn: {exemption}]
