---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Brahmagupta%27s_formula'
offline_file: ""
offline_thumbnail: ""
uuid: b5760b92-4546-42a5-afc3-f26464b55dfe
updated: 1484309111
title: "Brahmagupta's formula"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/281px-Brahmaguptas_formula.svg.png
tags:
    - Formula
    - Proof
    - Trigonometric proof
    - Non-trigonometric proof
    - Extension to non-cyclic quadrilaterals
    - Related theorems
categories:
    - Theorems in geometry
---
In Euclidean geometry, Brahmagupta's formula finds the area of any cyclic quadrilateral (one that can be inscribed in a circle) given the lengths of the sides.
