---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uncontrolled
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484505961
title: uncontrolled
categories:
    - Dictionary
---
uncontrolled
     adj 1: not being under control; out of control; "the greatest
            uncontrolled health problem is AIDS"; "uncontrolled
            growth" [ant: {controlled}]
     2: extravagant or extreme; "the inhumanity of his untempered
        principles"- M.S.Dworkin [syn: {unrestrained}, {untempered}]
