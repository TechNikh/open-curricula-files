---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fetch
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448061
title: fetch
categories:
    - Dictionary
---
fetch
     v 1: go or come after and bring or take back; "Get me those books
          over there, please"; "Could you bring the wine?"; "The
          dog fetched the hat" [syn: {bring}, {get}, {convey}]
          [ant: {take away}]
     2: be sold for a certain price; "The painting brought $10,000";
        "The old print fetched a high price at the auction" [syn:
        {bring in}, {bring}]
     3: take away or remove; "The devil will fetch you!"
