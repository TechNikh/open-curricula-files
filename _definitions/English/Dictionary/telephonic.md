---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/telephonic
offline_file: ""
offline_thumbnail: ""
uuid: 74019d96-c0bc-4e8c-9111-b5319d790ce7
updated: 1484310186
title: telephonic
categories:
    - Dictionary
---
telephonic
     adj : of or relating to telephony; "telephonic connection"
