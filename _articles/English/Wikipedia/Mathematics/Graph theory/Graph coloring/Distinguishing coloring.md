---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distinguishing_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 18d8b045-5374-47cd-b14b-2c59d10b3e5e
updated: 1484309379
title: Distinguishing coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Distinguishing_4-hypercube.svg.png
tags:
    - Examples
    - Computational complexity
    - Additional properties
    - Variations
categories:
    - Graph coloring
---
In graph theory, a distinguishing coloring or distinguishing labeling of a graph is an assignment of colors or labels to the vertices of the graph that destroys all of the nontrivial symmetries of the graph. The coloring does not need to be a proper coloring: adjacent vertices are allowed to be given the same color. For the colored graph, there should not exist any one-to-one mapping of the vertices to themselves that preserves both adjacency and coloring. The minimum number of colors in a distinguishing coloring is called the distinguishing number of the graph.
