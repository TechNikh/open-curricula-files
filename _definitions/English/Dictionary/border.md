---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/border
offline_file: ""
offline_thumbnail: ""
uuid: 8a0aeab2-feb3-4d17-a4cb-82ae0a978e13
updated: 1484310212
title: border
categories:
    - Dictionary
---
border
     n 1: a line that indicates a boundary [syn: {boundary line}, {borderline},
           {delimitation}, {mete}]
     2: the boundary line or the area immediately inside the
        boundary [syn: {margin}, {perimeter}]
     3: the boundary of a surface [syn: {edge}]
     4: a decorative recessed or relieved surface on an edge [syn: {molding},
         {moulding}]
     5: a strip forming the outer edge of something; "the rug had a
        wide blue border"
     v 1: extend on all sides of simultaneously; encircle; "The forest
          surrounds my property" [syn: {surround}, {skirt}]
     2: form the boundary of; be contiguous to [syn: {bound}]
     3: enclose in or as if in a ...
