---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Evaporative_cooling_(atomic_physics)
offline_file: ""
offline_thumbnail: ""
uuid: e490ced0-ecf8-486e-be30-23c2703622ac
updated: 1484308845
title: Evaporative cooling (atomic physics)
tags:
    - RF evaporation
    - History
categories:
    - Thermodynamics
---
Atoms trapped in optical or magnetic traps are cooled as the trap depth is decreased and the hottest atoms (with the highest kinetic energy) leave the trap. The hot atoms leaving the trap are on the tail of the Maxwell-Boltzmann distribution and therefore carry away a significant amount of kinetic energy, mitigating the loss of atoms by an overall increase in phase space density. The technique is analogous to cooling a hot cup of coffee by blowing on it.
