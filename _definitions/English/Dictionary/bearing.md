---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bearing
offline_file: ""
offline_thumbnail: ""
uuid: 8c6a6549-1582-4435-a0c6-0cc346cbef54
updated: 1484310518
title: bearing
categories:
    - Dictionary
---
bearing
     adj 1: (of a structural member) withstanding a weight or strain
            [syn: {bearing(a)}] [ant: {nonbearing}]
     2: producing or yielding; "an interest-bearing note";
        "fruit-bearing trees"
     n 1: relevant relation or interconnection; "those issues have no
          bearing on our situation"
     2: the direction or path along which something moves or along
        which it lies [syn: {heading}, {aim}]
     3: dignified manner or conduct [syn: {comportment}, {presence},
         {mien}]
     4: characteristic way of bearing one's body; "stood with good
        posture" [syn: {carriage}, {posture}]
     5: heraldry consisting of a design or image depicted on a
 ...
