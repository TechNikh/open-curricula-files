---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Karakoram
offline_file: ""
offline_thumbnail: ""
uuid: ad884398-9ac7-4476-87e5-58b2b96708a2
updated: 1484308426
title: Karakoram
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Un-kashmir-jammu.png
tags:
    - Name
    - Exploration
    - Geology and glaciers
    - The Karakoram during the Ice Age
    - Highest peaks
    - K-names
    - Subranges
    - Passes
    - Cultural references
    - Notes
categories:
    - Uncategorized
---
The Karakoram, or Karakorum is a large mountain range spanning the borders between Afghanistan, China, India, Pakistan and Tajikistan, located in the regions of Gilgit–Baltistan (Pakistan), Ladakh (India), southern Xinjiang (China) and the northeastern frontier of the Wakhan Corridor (Afghanistan). A part of the complex of ranges from the Hindu Kush to the Himalayan Range,[1][2] it is one of the Greater Ranges of Asia. The Karakoram is home to the four most closely located peaks over 8000m in height on earth:[3][4] K2, the second highest peak in the world at 8,611 m (28,251 ft), Gasherbrum I, Broad Peak and Gasherbrum II.
