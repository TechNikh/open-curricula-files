---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/protein
offline_file: ""
offline_thumbnail: ""
uuid: 60f1eeed-f755-44d9-9268-c649e53c62b6
updated: 1484310333
title: protein
categories:
    - Dictionary
---
protein
     n : any of a large group of nitrogenous organic compounds that
         are essential constituents of living cells; consist of
         polymers of amino acids; essential in the diet of animals
         for growth and for repair of tissues; can be obtained
         from meat and eggs and milk and legumes; "a diet high in
         protein"
