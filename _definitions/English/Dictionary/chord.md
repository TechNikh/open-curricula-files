---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chord
offline_file: ""
offline_thumbnail: ""
uuid: 0a742bb0-dfb0-4091-976a-5e80df326530
updated: 1484310154
title: chord
categories:
    - Dictionary
---
chord
     n 1: a straight line connecting two points on a curve
     2: a combination of three or more notes that blend harmoniously
        when sounded together
     v 1: play chords on (a string instrument)
     2: bring into consonance, harmony, or accord while making music
        or singing [syn: {harmonize}, {harmonise}]
