---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pudding
offline_file: ""
offline_thumbnail: ""
uuid: 096f97d8-5e34-4c31-b1d5-ce6573bf4193
updated: 1484310537
title: pudding
categories:
    - Dictionary
---
pudding
     n 1: any of various soft thick unsweetened baked dishes; "corn
          pudding"
     2: (British) the dessert course of a meal (`pud' is used
        informally) [syn: {pud}]
     3: any of various soft sweet desserts thickened usually with
        flour and baked or boiled or steamed
