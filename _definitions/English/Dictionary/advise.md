---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advise
offline_file: ""
offline_thumbnail: ""
uuid: 6bd7fb8b-a728-4129-9a01-3d20c5376c84
updated: 1484310543
title: advise
categories:
    - Dictionary
---
advise
     v 1: give advice to; "The teacher counsels troubled students";
          "The lawyer counselled me when I was accused of tax
          fraud" [syn: {counsel}]
     2: give information or notice to; "I advised him that the rent
        was due" [syn: {notify}, {give notice}, {send word}, {apprise},
         {apprize}]
     3: make a proposal, declare a plan for something [syn: {propose},
         {suggest}]
