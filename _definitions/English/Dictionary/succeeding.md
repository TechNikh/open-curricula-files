---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/succeeding
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484327221
title: succeeding
categories:
    - Dictionary
---
succeeding
     adj 1: coming after or following [syn: {succeeding(a)}] [ant: {preceding(a)}]
     2: (of elected officers) elected but not yet serving; "our next
        president" [syn: {future(a)}, {next}, {succeeding(a)}]
