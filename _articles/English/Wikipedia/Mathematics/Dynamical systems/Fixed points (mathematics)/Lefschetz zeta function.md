---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lefschetz_zeta_function
offline_file: ""
offline_thumbnail: ""
uuid: a1f58844-734b-45d4-8d96-456f66fc1c72
updated: 1484308784
title: Lefschetz zeta function
tags:
    - Examples
    - Formula
    - Connections
categories:
    - Fixed points (mathematics)
---
In mathematics, the Lefschetz zeta-function is a tool used in topological periodic and fixed point theory, and dynamical systems. Given a mapping f, the zeta-function is defined as the formal series
