---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Indentured
offline_file: ""
offline_thumbnail: ""
uuid: 1ddb0623-9417-41f3-9e0c-341c2d0e5c1f
updated: 1484308460
title: Indenture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Indenture_1723.jpg
tags:
    - Historical usage
    - Modern usage
categories:
    - Uncategorized
---
An indenture is a legal contract that reflects or covers a debt or purchase obligation. It specifically refers to two types of practices: in historical usage, an indentured servant status, and in modern usage, it is an instrument used for commercial debt or real estate transaction.
