---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unnoticed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484433961
title: unnoticed
categories:
    - Dictionary
---
unnoticed
     adj : not noticed; "hoped his departure had passed unnoticed"
           [ant: {noticed}]
