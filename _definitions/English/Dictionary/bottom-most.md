---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bottom-most
offline_file: ""
offline_thumbnail: ""
uuid: efc1d710-972f-463f-af5b-3195f384b9a8
updated: 1484310479
title: bottom-most
categories:
    - Dictionary
---
bottommost
     adj : farthest down; "bottommost shelf" [syn: {lowermost}, {nethermost}]
