---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quality
offline_file: ""
offline_thumbnail: ""
uuid: ccfe189e-a47f-407a-b578-37b661dc2f79
updated: 1484310240
title: quality
categories:
    - Dictionary
---
quality
     adj 1: of superior grade; "choice wines"; "prime beef"; "prize
            carnations"; "quality paper"; "select peaches" [syn: {choice},
             {prime(a)}, {prize}, {select}]
     2: of high social status; "people of quality"; "a quality
        family"
     n 1: an essential and distinguishing attribute of something or
          someone; "the quality of mercy is not
          strained"--Shakespeare
     2: a degree or grade of excellence or worth; "the quality of
        students has risen"; "an executive of low caliber" [syn: {caliber},
         {calibre}]
     3: a characteristic property that defines the apparent
        individual nature of something; "each town has ...
