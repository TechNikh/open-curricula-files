---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Agnesium
offline_file: ""
offline_thumbnail: ""
uuid: d22f7db4-3e37-4733-b704-9155cba26119
updated: 1484308722
title: Agnesium
categories:
    - Complex dynamics
---
Agnesium is a neologism coined to describe a powerful, efficient and effective organization as an entity.[1] Agnesium is a combination of Agnes (Latin for powerful, efficient and effective) and the suffix -ium [2] (indicating a biological structure).
