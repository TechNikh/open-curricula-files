---
version: 1
type: video
provider: YouTube
id: OTJnLMOP3Oo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 55995aaf-887d-44d8-a068-c7eb4cf177a5
updated: 1486069616
title: "Order doesn't matter when purely multiplying"
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/OTJnLMOP3Oo/default.jpg
    - https://i3.ytimg.com/vi/OTJnLMOP3Oo/1.jpg
    - https://i3.ytimg.com/vi/OTJnLMOP3Oo/2.jpg
    - https://i3.ytimg.com/vi/OTJnLMOP3Oo/3.jpg
---
