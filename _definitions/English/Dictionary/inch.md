---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inch
offline_file: ""
offline_thumbnail: ""
uuid: 6aa51906-2e55-4817-834b-e733cec2653a
updated: 1484310315
title: inch
categories:
    - Dictionary
---
inch
     n 1: a unit of length equal to one twelfth of a foot [syn: {in}]
     2: a unit of measurement for advertising space [syn: {column
        inch}]
     v : advance slowly, as if by inches; "He edged towards the car"
         [syn: {edge}]
