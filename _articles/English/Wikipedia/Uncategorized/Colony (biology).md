---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bacterial_colony
offline_file: ""
offline_thumbnail: ""
uuid: 1f73389c-c7b6-4bd3-9be4-50b10dc69c7b
updated: 1484308366
title: Colony (biology)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Colony_Northern_Gannet_Morus_bassanus.jpg
tags:
    - Colony types
    - Social colonies
    - Modular organisms
    - Microbial colonies
    - Life history
categories:
    - Uncategorized
---
In biology, a colony is composed of two or more conspecific individuals living in close association with, or connected to, one another, usually for mutual benefit such as stronger defense or the ability to attack bigger prey.[1] In contrast, a solitary organism is one in which all individuals live independently and have all of the functions needed to survive and reproduce.
