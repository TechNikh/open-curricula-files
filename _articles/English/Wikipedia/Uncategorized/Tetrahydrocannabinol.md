---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thc
offline_file: ""
offline_thumbnail: ""
uuid: 740b7e11-a0ee-4fd4-b2a0-55e4d063ae5f
updated: 1484308342
title: Tetrahydrocannabinol
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tetrahydrocannabinol.svg.png
tags:
    - Medical uses
    - Adverse effects
    - Acute toxicity
    - Pharmacology
    - Mechanism of action
    - Pharmacokinetics
    - Physical and chemical properties
    - Discovery and structure identification
    - Solubility
    - Biosynthesis
    - Detection in body fluids
    - History
    - Society and culture
    - Brand names
    - Comparisons with medical cannabis
    - Research
    - Multiple sclerosis symptoms
    - Neurodegenerative disorders
    - Other neurological disorders
categories:
    - Uncategorized
---
Tetrahydrocannabinol (THC, dronabinol by INN), or more precisely its main isomer (−)-trans-Δ9-tetrahydrocannabinol, is the principal psychoactive constituent (or cannabinoid) of cannabis. It can be an amber or gold colored glassy solid when cold, which becomes viscous and sticky if warmed.
