---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cargo_Sous_Terrain
offline_file: ""
offline_thumbnail: ""
uuid: 1a49d56c-5e61-49ed-a817-4bf7ad2a95c1
updated: 1484308754
title: Cargo Sous Terrain
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Mittelland.JPG
tags:
    - Description
    - Network
    - Costs (first phase)
categories:
    - Electrodynamics
---
Cargo Sous Terrain (abbreviated to CST; French language, literally: "Underground Cargo") is a planned underground logistics system in Switzerland. As of January 2016, the company respectively association (Förderverein Cargo sous terrain) is based at the seat of the Swiss retailer Coop in Basel.[1]
