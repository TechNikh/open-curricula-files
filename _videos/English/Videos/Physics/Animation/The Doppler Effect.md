---
version: 1
type: video
provider: YouTube
id: RsiY8VdDlDQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/The%20Doppler%20Effect.webm"
offline_file: ""
offline_thumbnail: ""
uuid: efbe5bf9-7f74-4787-84c9-b3e75b32effe
updated: 1486069651
title: The Doppler Effect
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/RsiY8VdDlDQ/default.jpg
    - https://i3.ytimg.com/vi/RsiY8VdDlDQ/1.jpg
    - https://i3.ytimg.com/vi/RsiY8VdDlDQ/2.jpg
    - https://i3.ytimg.com/vi/RsiY8VdDlDQ/3.jpg
---
