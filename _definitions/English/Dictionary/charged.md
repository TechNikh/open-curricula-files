---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/charged
offline_file: ""
offline_thumbnail: ""
uuid: 2dd807f1-60a2-4840-a3b5-48d5aae48b02
updated: 1484310202
title: charged
categories:
    - Dictionary
---
charged
     adj 1: of a particle or body or system; having a net amount of
            positive or negative electric charge; "charged
            particles"; "a charged battery" [ant: {uncharged}]
     2: fraught with great emotion; "an atmosphere charged with
        excitement"; "an emotionally charged speech" [syn: {supercharged}]
     3: supplied with carbon dioxide [syn: {aerated}]
     4: capable of producing violent emotion or arousing
        controversy; "the highly charged issue of abortion"
