---
version: 1
type: article
id: https://en.wikipedia.org/wiki/John_N._Mather
offline_file: ""
offline_thumbnail: ""
uuid: 1a8a789b-a2dd-479e-a083-e611f12cebad
updated: 1484308688
title: John N. Mather
categories:
    - Dynamical systems theorists
---
John Norman Mather (born June 9, 1942 in Los Angeles, California) is a mathematician at Princeton University known for his work on singularity theory and Hamiltonian dynamics. He is descended from Atherton Mather (1663-1734), a cousin of Cotton Mather.[1]
