---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pappus%27s_hexagon_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: dfe5ce04-3353-45fd-b036-090125df42c5
updated: 1484309119
title: "Pappus's hexagon theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pappusconfig.svg.png
tags:
    - Proof
    - Origins
    - Other statements of the theorem
    - Notes
categories:
    - Theorems in geometry
---
In mathematics, Pappus's hexagon theorem (attributed to Pappus of Alexandria) states that given one set of collinear points A, B, C, and another set of collinear points a, b, c, then the intersection points X, Y, Z of line pairs Ab and aB, Ac and aC, Bc and bC are collinear, lying on the Pappus line. These three points are the points of intersection of the "opposite" sides of the hexagon AbCaBc. It holds in a projective plane over any field, but fails for projective planes over any noncommutative division ring.[1] Projective planes in which the "theorem" is valid are called pappian planes.
