---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kustodiev
offline_file: ""
offline_thumbnail: ""
uuid: 65d7fc7e-a63a-4ccf-bd2b-d9e0e5d7b652
updated: 1484308473
title: Boris Kustodiev
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Kustodiev_self_portrait.jpg
tags:
    - Early life
    - Art studies
    - Career
    - Stage design
    - Selected works
categories:
    - Uncategorized
---
Boris Mikhaylovich Kustodiev (Russian: Бори́с Миха́йлович Кусто́диев; 7 March [O.S. 23 February] 1878 – 28 May 1927) was a Russian painter and stage designer.[1][2]
