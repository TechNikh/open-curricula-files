---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/antiseptic
offline_file: ""
offline_thumbnail: ""
uuid: 303a1b8a-3e63-4d5f-937e-8c23093b80de
updated: 1484310387
title: antiseptic
categories:
    - Dictionary
---
antiseptic
     adj 1: thoroughly clean and free of or destructive to
            disease-causing organisms; "doctors in antiseptic
            green coats"; "the antiseptic effect of alcohol"; "it
            is said that marjoram has antiseptic qualities" [ant:
            {septic}]
     2: clean and honest; "antiseptic financial practices"
     3: freeing from error or corruption; "the antiseptic effect of
        sturdy criticism"
     4: made free from live bacteria or other microorganisms;
        "sterilized instruments" [syn: {sterilized}, {sterilised}]
     5: (extended sense) of exceptionally clean language; "lyrics as
        antiseptic as Sunday School"
     n : a substance that ...
