---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thirty
offline_file: ""
offline_thumbnail: ""
uuid: 5272101f-d0d2-4160-ae77-df476763acc6
updated: 1484310399
title: thirty
categories:
    - Dictionary
---
thirty
     adj : being ten more than twenty [syn: {30}, {xxx}]
     n : the cardinal number that is the product of ten and three
         [syn: {30}, {XXX}]
