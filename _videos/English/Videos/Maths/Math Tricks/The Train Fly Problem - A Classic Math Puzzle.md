---
version: 1
type: video
provider: YouTube
id: -DkXXOGtVDU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ca811150-5c4d-4809-a3d7-d413efb9ce45
updated: 1486069623
title: 'The Train Fly Problem - A Classic Math Puzzle'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/-DkXXOGtVDU/default.jpg
    - https://i3.ytimg.com/vi/-DkXXOGtVDU/1.jpg
    - https://i3.ytimg.com/vi/-DkXXOGtVDU/2.jpg
    - https://i3.ytimg.com/vi/-DkXXOGtVDU/3.jpg
---
