---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/excellent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484435341
title: excellent
categories:
    - Dictionary
---
excellent
     adj : of the highest quality; "made an excellent speech"; "the
           school has excellent teachers"; "a first-class mind"
           [syn: {first-class}, {fantabulous}]
