---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Long_axis
offline_file: ""
offline_thumbnail: ""
uuid: 848e28f1-b583-47dd-8a9f-80f9bdde2c3c
updated: 1484308375
title: Flight control surfaces
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-ControlSurfaces.gif
tags:
    - Development
    - Axes of motion
    - Lateral axis
    - Longitudinal axis
    - Vertical axis
    - Main control surfaces
    - Ailerons
    - Elevator
    - Rudder
    - Secondary effects of controls
    - Ailerons
    - Rudder
    - Turning the aircraft
    - Alternate main control surfaces
    - Secondary control surfaces
    - Spoilers
    - Flaps
    - Slats
    - Air brakes
    - Control trimming surfaces
    - Elevator trim
    - Trimming tail plane
    - Control horn
    - Spring trim
    - Rudder and aileron trim
    - Notes
categories:
    - Uncategorized
---
Development of an effective set of flight control surfaces was a critical advance in the development of aircraft. Early efforts at fixed-wing aircraft design succeeded in generating sufficient lift to get the aircraft off the ground, but once aloft, the aircraft proved uncontrollable, often with disastrous results. The development of effective flight controls is what allowed stable flight.
