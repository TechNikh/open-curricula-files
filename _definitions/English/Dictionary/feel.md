---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feel
offline_file: ""
offline_thumbnail: ""
uuid: d862efcf-57fc-4cc2-b89b-056595d06d4c
updated: 1484310359
title: feel
categories:
    - Dictionary
---
feel
     n 1: an intuitive awareness; "he has a feel for animals" or "it's
          easy when you get the feel of it";
     2: the general atmosphere of a place or situation and the
        effect that it has on people; "the feel of the city
        excited him"; "a clergyman improved the tone of the
        meeting"; "it had the smell of treason" [syn: {spirit}, {tone},
         {feeling}, {flavor}, {flavour}, {look}, {smell}]
     3: a property perceived by touch [syn: {tactile property}]
     4: manual-genital stimulation for sexual pleasure; "the girls
        hated it when he tried to sneak a feel"
     v 1: undergo an emotional sensation; "She felt resentful"; "He
          felt ...
