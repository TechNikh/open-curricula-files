---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regrettable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484449321
title: regrettable
categories:
    - Dictionary
---
regrettable
     adj : deserving regret; "regrettable remarks"; "it's regrettable
           that she didn't go to college"; "it's too bad he had no
           feeling himself for church" [syn: {too bad}]
