---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fellow
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484416321
title: fellow
categories:
    - Dictionary
---
fellow
     n 1: a boy or man; "that chap is your host"; "there's a fellow at
          the door"; "he's a likable cuss" [syn: {chap}, {feller},
           {lad}, {gent}, {fella}, {blighter}, {cuss}]
     2: a person who is frequently in the company of another;
        "drinking companions"; "comrades in arms" [syn: {companion},
         {comrade}, {familiar}, {associate}]
     3: a person who is member of your class or profession; "the
        surgeon consulted his colleagues"; "he sent e-mail to his
        fellow hackers" [syn: {colleague}, {confrere}]
     4: an informal form of address for a man; "Say, fellow, what
        are you doing?"; "Hey buster, what's up?" [syn: {buster}]
     ...
