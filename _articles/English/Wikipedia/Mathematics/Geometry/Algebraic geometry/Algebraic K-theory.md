---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_K-theory
offline_file: ""
offline_thumbnail: ""
uuid: 29ee4061-39de-483c-8995-a8744919707c
updated: 1484309134
title: Algebraic K-theory
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_9.png
tags:
    - History
    - K0, K1, and K2
    - Higher K-groups
    - Applications of algebraic K-theory in topology
    - >
        Algebraic topology and algebraic geometry in algebraic
        K-theory
    - Lower K-groups
    - K0
    - Examples
    - Relative K0
    - K0 as a ring
    - K1
    - Relative K1
    - Commutative rings and fields
    - Central simple algebras
    - K2
    - "Matsumoto's theorem"
    - Long exact sequences
    - Pairing
    - Milnor K-theory
    - Higher K-theory
    - The +-construction
    - The Q-construction
    - The S-construction
    - Examples
    - Algebraic K-groups of finite fields
    - Algebraic K-groups of rings of integers
    - Applications and open questions
    - Notes
    - Historical references
categories:
    - Algebraic geometry
---
Algebraic K-theory is a subject area in mathematics with connections to geometry, topology, ring theory, and number theory. Geometric, algebraic, and arithmetic objects are assigned objects called K-groups. These are groups in the sense of abstract algebra. They contain detailed information about the original object but are notoriously difficult to compute; for example, an important outstanding problem is to compute the K-groups of the integers.
