---
version: 1
type: video
provider: YouTube
id: aguCWnbRETU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Creating%20a%20Longitudinal%20Wave.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f591dc75-a2e5-4547-a022-4e61ea16bf43
updated: 1486069652
title: Creating a Longitudinal Wave
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/aguCWnbRETU/default.jpg
    - https://i3.ytimg.com/vi/aguCWnbRETU/1.jpg
    - https://i3.ytimg.com/vi/aguCWnbRETU/2.jpg
    - https://i3.ytimg.com/vi/aguCWnbRETU/3.jpg
---
