---
version: 1
type: video
provider: YouTube
id: -L5HodT0FFU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 86962545-3b13-470e-a5a5-62b7c2a35539
updated: 1486069622
title: 'How to trick your math teacher 11 Tutorial - Math tricks revealed'
categories:
    - Math Magic Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/-L5HodT0FFU/default.jpg
    - https://i3.ytimg.com/vi/-L5HodT0FFU/1.jpg
    - https://i3.ytimg.com/vi/-L5HodT0FFU/2.jpg
    - https://i3.ytimg.com/vi/-L5HodT0FFU/3.jpg
---
