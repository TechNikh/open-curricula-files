---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diarrhoea
offline_file: ""
offline_thumbnail: ""
uuid: 2d3558c8-90b9-421f-bce9-4449724e2459
updated: 1484310443
title: diarrhoea
categories:
    - Dictionary
---
diarrhoea
     n : frequent and watery bowel movements; can be a symptom of
         infection or food poisoning or colitis or a
         gastrointestinal tumor [syn: {diarrhea}, {looseness of
         the bowels}]
