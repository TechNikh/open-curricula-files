---
version: 1
type: video
provider: YouTube
id: ExaQ8shhkw8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/The%20Plants%20%26%20The%20Bees-%20Plant%20Reproduction%20-%20CrashCourse%20Biology%20%2338.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 12613b4d-e1c4-4a99-85f6-e92647466643
updated: 1486069678
title: 'The Plants & The Bees- Plant Reproduction - CrashCourse Biology #38'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/ExaQ8shhkw8/default.jpg
    - https://i3.ytimg.com/vi/ExaQ8shhkw8/1.jpg
    - https://i3.ytimg.com/vi/ExaQ8shhkw8/2.jpg
    - https://i3.ytimg.com/vi/ExaQ8shhkw8/3.jpg
---
