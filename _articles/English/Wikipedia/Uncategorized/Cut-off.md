---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cut-off
offline_file: ""
offline_thumbnail: ""
uuid: aebc8739-1479-4239-8932-5c7b97bfd925
updated: 1484308335
title: Cut-off
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Gypsy_Joker_Protest_Run_5.jpg
categories:
    - Uncategorized
---
A cut-off, also known as a kutte or "battle jacket" / "battlevest" in heavy metal subcultures, is a type of vest or jacket which originated in the biker subculture and has now found popularity in the punk and various heavy metal subcultures. Biker, metal and punk subcultures differ in how the garment itself is prepared, what decorations are applied and how this is done.
