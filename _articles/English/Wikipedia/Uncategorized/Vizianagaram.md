---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vizianagaram
offline_file: ""
offline_thumbnail: ""
uuid: faca1e7b-9e47-4f7e-b335-91260ade49a0
updated: 1484308447
title: Vizianagaram
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-West_Entrance_of_the_Vizianagaram_fort_in_Andhra_Pradesh.jpg
tags:
    - History
    - geography
    - Climate
    - Demographics
    - Governance
    - Civic administration
    - Utility services
    - Economy
    - Industries
    - Transport
    - Roadways
    - Railways
    - Airport
    - educational
    - Sports
categories:
    - Uncategorized
---
Vizianagaram is a city and district headquarters of Vizianagaram district in the Indian state of Andhra Pradesh. It is a municipal corporation and also the mandal headquarters of Vizianagaram mandal.[4] It is located 18 km inland from the Bay of Bengal and 42 km to the northeast of Visakhapatnam.
