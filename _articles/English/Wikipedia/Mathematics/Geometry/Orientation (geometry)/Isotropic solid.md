---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isotropic_solid
offline_file: ""
offline_thumbnail: ""
uuid: 89648284-1793-44e3-a507-6764f04bc547
updated: 1484309293
title: Isotropic solid
categories:
    - Orientation (geometry)
---
An isotropic solid is a solid material in which physical properties do not depend on its orientation. It is an example of isotropy which can also apply to fluids, or other non material concepts. The properties could be felt or seen such as the index of refraction or mechanical. For example an isotropic solid will expand equally in all directions when heated.[1] Heat will conduct equally well in any direction, and sound will pass at the same speed.[1] The physics of these solids are much easier to describe. Some examples are glass with random arrangements that average out to uniform, or the cubic crystal system. The opposite is anisotropic solids. Most crystal structures are actually ...
