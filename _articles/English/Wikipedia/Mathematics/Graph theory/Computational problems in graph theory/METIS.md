---
version: 1
type: article
id: https://en.wikipedia.org/wiki/METIS
offline_file: ""
offline_thumbnail: ""
uuid: 151a66c3-eaa0-450a-a2b0-fe9d185ce3a5
updated: 1484309392
title: METIS
categories:
    - Computational problems in graph theory
---
METIS' multilevel approach has three phases and comes with several algorithms for each phase:
