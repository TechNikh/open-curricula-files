---
version: 1
type: video
provider: YouTube
id: 3F9BEHc8Bqg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bada9e67-2569-4198-a93d-6ff6dc313cdb
updated: 1486069618
title: Introduction to line plots
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/3F9BEHc8Bqg/default.jpg
    - https://i3.ytimg.com/vi/3F9BEHc8Bqg/1.jpg
    - https://i3.ytimg.com/vi/3F9BEHc8Bqg/2.jpg
    - https://i3.ytimg.com/vi/3F9BEHc8Bqg/3.jpg
---
