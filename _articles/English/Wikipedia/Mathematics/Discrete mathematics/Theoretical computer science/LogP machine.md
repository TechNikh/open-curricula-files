---
version: 1
type: article
id: https://en.wikipedia.org/wiki/LogP_machine
offline_file: ""
offline_thumbnail: ""
uuid: f2cddc73-05fb-4f81-b03b-e150d1a2a932
updated: 1484308684
title: LogP machine
categories:
    - Theoretical computer science
---
The LogP machine is a model for parallel computation.[1] It aims at being more practical than the PRAM model while still allowing for easy analysis of computation. The name is not related to the mathematical logarithmic function: Instead, the machine is described by the four parameters 
  
    
      
        L
      
    
    {\displaystyle L}
  
, 
  
    
      
        o
      
    
    {\displaystyle o}
  
, 
  
    
      
        g
      
    
    {\displaystyle g}
  
 and 
  
    
      
        P
      
    
    {\displaystyle P}
  
.
