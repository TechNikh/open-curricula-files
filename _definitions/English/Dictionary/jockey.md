---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jockey
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484470981
title: jockey
categories:
    - Dictionary
---
jockey
     n 1: someone employed to ride horses in horse races
     2: an operator of some vehicle or machine or apparatus; "he's a
        truck jockey"; "a computer jockey"; "a disc jockey"
     v 1: defeat someone in an expectation through trickery or deceit
          [syn: {cheat}, {chouse}, {shaft}, {screw}, {chicane}]
     2: compete (for an advantage or a position)
     3: ride a race-horse as a professional jockey
