---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outlet
offline_file: ""
offline_thumbnail: ""
uuid: f490c4c8-846c-4011-b9b1-5ea7ae8182c7
updated: 1484310413
title: outlet
categories:
    - Dictionary
---
outlet
     n 1: a place of business for retailing goods [syn: {mercantile
          establishment}, {retail store}, {sales outlet}]
     2: receptacle providing a place in a wiring system where
        current can be taken to run electrical devices [syn: {wall
        socket}, {wall plug}, {electric outlet}, {electrical
        outlet}, {electric receptacle}]
     3: an opening that permits escape or release; "he blocked the
        way out"; "the canyon had only one issue" [syn: {exit}, {issue},
         {way out}]
     4: activity that releases or expresses creative energy or
        emotion; "she had no other outlet for her feelings"; "he
        gave vent to his anger" [syn: {release}, ...
