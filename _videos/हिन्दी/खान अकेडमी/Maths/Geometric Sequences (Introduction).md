---
version: 1
type: video
provider: YouTube
id: 0wwg3mS7lGk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 005ad172-de75-4d13-9bb6-31d50ee1f411
updated: 1486069605
title: Geometric Sequences (Introduction)
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/0wwg3mS7lGk/default.jpg
    - https://i3.ytimg.com/vi/0wwg3mS7lGk/1.jpg
    - https://i3.ytimg.com/vi/0wwg3mS7lGk/2.jpg
    - https://i3.ytimg.com/vi/0wwg3mS7lGk/3.jpg
---
