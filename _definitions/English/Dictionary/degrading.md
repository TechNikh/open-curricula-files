---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/degrading
offline_file: ""
offline_thumbnail: ""
uuid: 665d094b-7a2c-434c-ba73-bc37948c09fe
updated: 1484310166
title: degrading
categories:
    - Dictionary
---
degrading
     adj 1: harmful to the mind or morals; "corrupt judges and their
            corrupting influence"; "the vicious and degrading cult
            of violence" [syn: {corrupting}]
     2: used of conduct; characterized by dishonor [syn: {debasing}]
