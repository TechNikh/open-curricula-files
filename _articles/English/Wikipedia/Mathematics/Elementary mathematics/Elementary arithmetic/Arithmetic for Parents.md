---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arithmetic_for_Parents
offline_file: ""
offline_thumbnail: ""
uuid: aaff549c-d675-410c-813a-c801e9397352
updated: 1484308915
title: Arithmetic for Parents
categories:
    - Elementary arithmetic
---
The author, Ron Aharoni, is a professor of mathematics at the Technion; he wrote the book based on his experiences teaching elementary mathematics to Israeli schoolchildren.[2] The book was originally written in Hebrew and was translated to English, Portuguese and Dutch.
