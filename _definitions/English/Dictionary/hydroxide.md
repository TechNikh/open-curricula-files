---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydroxide
offline_file: ""
offline_thumbnail: ""
uuid: c54aed3a-4fdf-46fc-b0ad-a4ef2efb8618
updated: 1484310371
title: hydroxide
categories:
    - Dictionary
---
hydroxide
     n 1: a compound of an oxide with water [syn: {hydrated oxide}]
     2: a chemical compound containing the hydroxyl group
