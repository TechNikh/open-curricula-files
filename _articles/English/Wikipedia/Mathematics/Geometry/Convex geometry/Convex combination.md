---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_combination
offline_file: ""
offline_thumbnail: ""
uuid: 39d590f8-7f63-41c3-9969-a776ccd7c40a
updated: 1484309192
title: Convex combination
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Convex_combination_illustration.svg.png
categories:
    - Convex geometry
---
In convex geometry, a convex combination is a linear combination of points (which can be vectors, scalars, or more generally points in an affine space) where all coefficients are non-negative and sum to 1.
