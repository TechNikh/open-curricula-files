---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unesco
offline_file: ""
offline_thumbnail: ""
uuid: a440f4ad-4a04-44d4-867a-27c150de63c9
updated: 1484308476
title: UNESCO
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-UNESCO_Bras%25C3%25ADlia_Office.jpg'
tags:
    - History
    - activities
    - Media
    - Official UNESCO NGOs
    - Institutes and centres
    - Prizes
    - Inactive prizes
    - International Days observed at UNESCO
    - Member states
    - Governing bodies
categories:
    - Uncategorized
---
The United Nations Educational, Scientific and Cultural Organization (UNESCO)[2] (In French: Organisation des Nations unies pour l'éducation, la science et la culture) is a specialized agency of the United Nations (UN) based in Paris. Its declared purpose is to contribute to peace and security by promoting international collaboration through educational, scientific, and cultural reforms in order to increase universal respect for justice, the rule of law, and human rights along with fundamental freedom proclaimed in the United Nations Charter.[1] It is the heir of the League of Nations' International Committee on Intellectual Cooperation.
