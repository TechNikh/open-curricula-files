---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/destruction
offline_file: ""
offline_thumbnail: ""
uuid: f9011138-4fef-49fa-8758-eab2a22c46e9
updated: 1484310246
title: destruction
categories:
    - Dictionary
---
destruction
     n 1: termination by an act of destruction [syn: {devastation}]
     2: an event (or the result of an event) that completely
        destroys something [syn: {demolition}, {wipeout}]
     3: a final state; "he came to a bad end"; "the so-called
        glorious experiment came to an inglorious end" [syn: {end},
         {death}]
