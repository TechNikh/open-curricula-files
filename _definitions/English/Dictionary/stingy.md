---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stingy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484414581
title: stingy
categories:
    - Dictionary
---
stingy
     adj 1: not generous; "she practices economy without being stingy";
            "an ungenerous response to the appeal for funds" [syn:
             {ungenerous}] [ant: {generous}]
     2: selfishly unwilling to share with others [syn: {ungenerous}]
     [also: {stingiest}, {stingier}]
