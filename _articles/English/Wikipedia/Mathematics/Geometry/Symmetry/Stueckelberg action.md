---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stueckelberg_action
offline_file: ""
offline_thumbnail: ""
uuid: 97158a30-9898-4b99-903c-3107a763cd96
updated: 1484309317
title: Stueckelberg action
categories:
    - Symmetry
---
In field theory, the Stueckelberg action (named after Ernst Stueckelberg (1938), "Die Wechselwirkungskräfte in der Elektrodynamik und in der Feldtheorie der Kräfte", Helv. Phys. Acta. 11: 225) describes a massive spin-1 field as an R (the real numbers are the Lie algebra of U(1)) Yang–Mills theory coupled to a real scalar field φ. This scalar field takes on values in a real 1D affine representation of R with m as the coupling strength.
