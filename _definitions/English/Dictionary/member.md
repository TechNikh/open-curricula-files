---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/member
offline_file: ""
offline_thumbnail: ""
uuid: 22ab4a79-d63a-431d-aa31-041162510e63
updated: 1484310196
title: member
categories:
    - Dictionary
---
member
     n 1: one of the persons who compose a social group (especially
          individuals who have joined and participates in a group
          organization); "only members will be admitted"; "a
          member of the faculty"; "she was introduced to all the
          members of his family" [ant: {nonmember}]
     2: an organization that is a member of another organization
        (especially a state that belongs to a group of nations);
        "the library was a member of the interlibrary loan
        association"; "Canada is a member of the United Nations"
     3: an external body part that projects from the body; "it is
        important to keep the extremities warm" [syn: ...
