---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hardware
offline_file: ""
offline_thumbnail: ""
uuid: 057d4a89-fade-4f16-aacb-25153272feac
updated: 1484310549
title: hardware
categories:
    - Dictionary
---
hardware
     n 1: major items of military weaponry (as tanks or missile)
     2: instrumentalities (tools or implements) made of metal [syn:
        {ironware}]
     3: (computer science) the mechanical, magnetic, electronic, and
        electrical components making up a computer system [syn: {computer
        hardware}] [ant: {software}]
