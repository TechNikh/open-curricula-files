---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/photocopy
offline_file: ""
offline_thumbnail: ""
uuid: f5bb08f6-3954-4ea1-b731-b832ff1763cb
updated: 1484310297
title: photocopy
categories:
    - Dictionary
---
photocopy
     n : a photographic copy of written or printed or graphic work
     v : reproduce by xerography [syn: {run off}, {xerox}]
     [also: {photocopied}]
