---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algebraic_operation
offline_file: ""
offline_thumbnail: ""
uuid: 54099096-ed7c-46c0-9223-047f7097928b
updated: 1484308907
title: Algebraic operation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_1.png
tags:
    - Notation
    - Arithmetic vs algebraic operations
    - Properties of arithmetic and algebraic operations
categories:
    - Elementary algebra
---
In mathematics, a basic algebraic operation is any one of the traditional operations of arithmetic, which are addition, subtraction, multiplication, division, raising to an integer power, and taking roots (fractional power). These operations may be performed on numbers, in which case they are often called arithmetic operations. They may also be performed, in a similar way, on variables, algebraic expressions,[1] and, more generally on elements of algebraic structures, such as groups and fields.[2]
