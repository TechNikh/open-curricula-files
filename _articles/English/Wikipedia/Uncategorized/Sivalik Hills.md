---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shivaliks
offline_file: ""
offline_thumbnail: ""
uuid: 060a4809-1825-4069-a8a6-53d388aefa29
updated: 1484308415
title: Sivalik Hills
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kalimpongtown.jpg
tags:
    - Geology
    - Pre-history
    - Demographics
    - Additional information
categories:
    - Uncategorized
---
The Sivalik Hills is a mountain range of the outer Himalayas identified as Manak Parbat[1] in some Sanskrit texts. Shivalik literally means 'tresses of Shiva’.[2] This range is about 2,400 km (1,500 mi) long enclosing an area that starts almost from the Indus and ends close to the Brahmaputra, with a gap of about 90 kilometres (56 mi) between the Teesta and Raidak rivers in Assam. The width of the Sivalik Hills varies from 10 to 50 km (6.2 to 31.1 mi), their average elevation is 1,500 to 2,000 m (4,900 to 6,600 ft).[3]
