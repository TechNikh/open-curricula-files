---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fixed-point_property
offline_file: ""
offline_thumbnail: ""
uuid: 7a588d78-fdec-4c74-8462-2f1f7fcc5908
updated: 1484308786
title: Fixed-point property
tags:
    - Definition
    - Examples
    - The closed interval
    - The closed disc
    - Topology
categories:
    - Fixed points (mathematics)
---
A mathematical object X has the fixed-point property if every suitably well-behaved mapping from X to itself has a fixed point. The term is most commonly used to describe topological spaces on which every continuous mapping has a fixed point. But another use is in order theory, where a partially ordered set P is said to have the fixed point property if every increasing function on P has a fixed point.
