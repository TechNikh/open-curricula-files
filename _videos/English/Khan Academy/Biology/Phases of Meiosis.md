---
version: 1
type: video
provider: YouTube
id: ijLc52LmFQg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Phases%20of%20Meiosis.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9cc6421c-982c-4bb6-8424-e1f9f5806531
updated: 1486069583
title: Phases of Meiosis
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/ijLc52LmFQg/default.jpg
    - https://i3.ytimg.com/vi/ijLc52LmFQg/1.jpg
    - https://i3.ytimg.com/vi/ijLc52LmFQg/2.jpg
    - https://i3.ytimg.com/vi/ijLc52LmFQg/3.jpg
---
The phases of Meiosis.
More free lessons at: http://www.khanacademy.org/video?v=ijLc52LmFQg
