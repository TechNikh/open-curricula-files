---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Massai
offline_file: ""
offline_thumbnail: ""
uuid: 356b108e-b6cc-401f-8668-dfa3f354dfef
updated: 1484308471
title: Massai
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Massai_Apache_1880.jpg
categories:
    - Uncategorized
---
Massai (also known as: Massa, Massi, Masai, Wasse or Massey; c.1847-1906,[1] 1911[2]) was a member of the Mimbres /Mimbreños local group of the Chihenne band of the Chiricahua Apache. He was a warrior who escaped from a train that was sending the scouts and renegades to Florida to be held with Geronimo and Chihuahua.
