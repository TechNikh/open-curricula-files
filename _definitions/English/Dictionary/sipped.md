---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sipped
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484467682
title: sipped
categories:
    - Dictionary
---
sip
     n : a small drink [syn: {nip}]
     v : drink in sips; "She was sipping her tea"
     [also: {sipping}, {sipped}]
