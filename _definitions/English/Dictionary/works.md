---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/works
offline_file: ""
offline_thumbnail: ""
uuid: c80ff1b7-f0f7-4d5f-ad59-50efb9fefd52
updated: 1484310236
title: works
categories:
    - Dictionary
---
works
     n 1: buildings for carrying on industrial labor; "they built a
          large plant to manufacture automobiles" [syn: {plant}, {industrial
          plant}]
     2: everything available; usually preceded by `the'; "we saw the
        whole shebang"; "a hotdog with the works"; "we took on the
        whole caboodle"; "for $10 you get the full treatment"
        [syn: {whole shebang}, {whole kit and caboodle}, {kit and
        caboodle}, {whole kit and boodle}, {kit and boodle}, {whole
        kit}, {whole caboodle}, {whole works}, {full treatment}]
     3: performance of moral or religious acts; "salvation by
        deeds"; "the reward for good works" [syn: {deeds}]
     4: the ...
