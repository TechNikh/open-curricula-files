---
version: 1
type: video
provider: YouTube
id: hNeMM9itRVA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 454578a3-79fe-4a93-95ec-1fb750265333
updated: 1486069641
title: 'CPU 3D Fractal Zoom - Spiral Motion!'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/hNeMM9itRVA/default.jpg
    - https://i3.ytimg.com/vi/hNeMM9itRVA/1.jpg
    - https://i3.ytimg.com/vi/hNeMM9itRVA/2.jpg
    - https://i3.ytimg.com/vi/hNeMM9itRVA/3.jpg
---
