---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/steady
offline_file: ""
offline_thumbnail: ""
uuid: 82497138-3cfe-4bcf-9802-ff0cf1f745f4
updated: 1484310311
title: steady
categories:
    - Dictionary
---
steady
     adj 1: not subject to change or variation especially in behavior;
            "a steady beat"; "a steady job"; "a steady breeze"; "a
            steady increase"; "a good steady ballplayer" [ant: {unsteady}]
     2: persistent in occurrence and unvarying in nature;
        "maintained a constant temperature"; "a constant beat";
        "principles of unvarying validity"; "a steady breeze"
        [syn: {changeless}, {constant}, {invariant}, {unvarying}]
     3: not liable to fluctuate or especially to fall; "stocks are
        still firm" [syn: {firm}, {unfluctuating}]
     4: securely in position; not shaky; "held the ladder steady"
     5: marked by firm determination or ...
