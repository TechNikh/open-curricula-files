---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Renc
offline_file: ""
offline_thumbnail: ""
uuid: 16879eee-bf93-4ba6-b663-95acd5e771db
updated: 1484308321
title: Rrencë
categories:
    - Uncategorized
---
Rrencë is a village in the former municipality Guri i Zi, northern Albania. At the 2015 local government reform it became part of the municipality Shkodër.[1] It is located 1.5 km east from the city of Shkodër and consists of 860 inhabitants and 236 houses.
