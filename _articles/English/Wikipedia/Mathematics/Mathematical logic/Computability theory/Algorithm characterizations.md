---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Algorithm_characterizations
offline_file: ""
offline_thumbnail: ""
uuid: 7ef28759-5bed-45a6-9d69-9db94d0ba283
updated: 1484309481
title: Algorithm characterizations
tags:
    - The problem of definition
    - Chomsky hierarchy
    - Characterizations of the notion of "algorithm"
    - "1881 John Venn's negative reaction to W. Stanley Jevons's Logical Machine of 1870"
    - "1943, 1952 Stephen Kleene's characterization"
    - |
        1943 "Thesis I", 1952 "Church's Thesis"
    - |
        1952 "Turing's thesis"
    - 1952 Church–Turing Thesis
    - |
        A note of dissent: "There's more to algorithm..." Blass and Gurevich (2003)
    - "1954 A. A. Markov's characterization"
    - "1936, 1963, 1964 Gödel's characterization"
    - "1967 Minsky's characterization"
    - "1967 Rogers' characterization"
    - "1968, 1973 Knuth's characterization"
    - "1972 Stone's characterization"
    - "1995 Soare's characterization"
    - "2000 Berlinski's characterization"
    - "2000, 2002 Gurevich's characterization"
    - "2003 Blass and Gurevich's characterization"
    - '1995 – Daniel Dennett: evolution as an algorithmic process'
    - "2002 John Searle adds a clarifying caveat to Dennett's characterization"
    - '2002: Boolos-Burgess-Jeffrey specification of Turing machine calculation'
    - "2006: Sipser's assertion and his three levels of description"
    - Notes
categories:
    - Computability theory
---
Algorithm characterizations are attempts to formalize the word algorithm. Algorithm does not have a generally accepted formal definition. Researchers[1] are actively working on this problem. This article will present some of the "characterizations" of the notion of "algorithm" in more detail.
