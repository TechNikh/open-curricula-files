---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Staedtler
offline_file: ""
offline_thumbnail: ""
uuid: f0d0e3d5-7f9c-45bb-872b-c7ef3d645482
updated: 1484309332
title: Staedtler
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Olovke_staedtler.JPG
tags:
    - History
    - Products
    - Awards
    - Logos
    - Notes and references
categories:
    - Technical drawing
---
Staedtler Mars GmbH & Co. KG is a German fine writing instrument company and a manufacturer and supplier of writing, artist, and engineering drawing instruments. The firm was founded by J.S. Staedtler in 1835 and produces a large variety of writing instruments, including drafting pencils, propelling pencils, professional pens and standard wooden pencils.[1] It also produces plastic erasers, rulers, compasses and other drawing/writing accessories. Staedtler claims to be the largest European manufacturer of wood-cased pencils, OHP pens, mechanical pencil leads, erasers, and modelling clays.[2] Staedtler has over 20 global subsidiaries and seven manufacturing facilities.[3] Over 85% of the ...
