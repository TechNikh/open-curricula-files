---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Promoting_adversaries
offline_file: ""
offline_thumbnail: ""
uuid: 4461e94e-c723-426c-9172-fd5597c83029
updated: 1484308819
title: Promoting adversaries
tags:
    - Promoting adversaries in military, politics, and economics
    - Promoting adversaries in pop culture and public relations
    - Notes
categories:
    - Self-organization
---
Promoting adversaries refers to an unofficiated self-organizing tactical relationship between opposing organizations, in which both opposing sides benefit by attacking each other. The relationship typically relies on either side never fully defeating the other, because the whole time their 'conflict' helps both sides (while each side also simultaneously takes occasional 'acceptable' losses). Promoting adversaries requires neither side to be finally defeated throughout the relationship, because both sides actually prefer the relationship to continue and thus both sides to keep existing and fighting each other.
