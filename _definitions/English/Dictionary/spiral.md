---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spiral
offline_file: ""
offline_thumbnail: ""
uuid: 08c6f9ee-712a-45c5-aee9-7b3a5988cc3f
updated: 1484310297
title: spiral
categories:
    - Dictionary
---
spiral
     adj : in the shape of a coil [syn: {coiling}, {helical}, {spiraling},
            {volute}, {voluted}, {whorled}, {turbinate}]
     n 1: a plane curve traced by a point circling about the center
          but at ever-greater distances from it
     2: a curve that lies on the surface of a cylinder or cone and
        cuts the element at a constant angle [syn: {helix}]
     3: ornament consisting of a curve on a plane that winds around
        a center with an increasing distance from the center [syn:
         {volute}]
     4: a structure consisting of something wound in a continuous
        series of loops; "a coil of rope" [syn: {coil}, {volute},
        {whorl}, {helix}]
     ...
