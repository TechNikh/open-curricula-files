---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ware
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484332921
title: ware
categories:
    - Dictionary
---
ware
     n : articles of the same kind or material; usually used in
         combination: silverware; software
     v : spend extravagantly; "waste not, want not" [syn: {consume},
         {squander}, {waste}]
