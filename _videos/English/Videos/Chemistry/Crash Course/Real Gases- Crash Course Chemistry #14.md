---
version: 1
type: video
provider: YouTube
id: GIPrsWuSkQc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Crash%20Course/Real%20Gases-%20Crash%20Course%20Chemistry%20%2314.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ce18a825-f114-43cc-8cbd-962bef14ce9a
updated: 1486069678
title: 'Real Gases- Crash Course Chemistry #14'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/GIPrsWuSkQc/default.jpg
    - https://i3.ytimg.com/vi/GIPrsWuSkQc/1.jpg
    - https://i3.ytimg.com/vi/GIPrsWuSkQc/2.jpg
    - https://i3.ytimg.com/vi/GIPrsWuSkQc/3.jpg
---
