---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Duality_(mechanical_engineering)
offline_file: ""
offline_thumbnail: ""
uuid: b1e7dfc0-621c-43f8-8af9-53e030f0a42e
updated: 1484309236
title: Duality (mechanical engineering)
tags:
    - Examples
    - Constitutive relation
categories:
    - Duality theories
---
In mechanical engineering, many terms are associated into pairs called duals. A dual of a relationship is formed by interchanging force (stress) and deformation (strain) in an expression.
