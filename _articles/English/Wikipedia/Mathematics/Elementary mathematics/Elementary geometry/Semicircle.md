---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Semicircle
offline_file: ""
offline_thumbnail: ""
uuid: 073b246c-22bf-41f8-991d-0a3bb2f2e8b7
updated: 1484308935
title: Semicircle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/198px-Semicircle.svg.png
tags:
    - Uses
    - Equation
    - Arbelos
categories:
    - Elementary geometry
---
In mathematics (and more specifically geometry), a semicircle is a one-dimensional locus of points that forms half of a circle. The full arc of a semicircle always measures 180° (equivalently, π radians, or a half-turn). It has only one line of symmetry (reflection symmetry). In non-technical usage, the term "semicircle" is sometimes used to refer to a half-disk, which is a two-dimensional geometric shape that also includes the diameter segment from one end of the arc to the other as well as all the interior points.
