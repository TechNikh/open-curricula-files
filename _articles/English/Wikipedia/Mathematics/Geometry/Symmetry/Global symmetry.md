---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Global_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 767f6bd9-87ae-4b94-8da1-f847a3decf2e
updated: 1484309307
title: Global symmetry
categories:
    - Symmetry
---
In physics, a global symmetry is a symmetry that holds at all points in the spacetime under consideration, as opposed to a local symmetry which varies from point to point.
