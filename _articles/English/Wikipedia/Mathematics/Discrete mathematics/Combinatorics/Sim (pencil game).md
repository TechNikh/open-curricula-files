---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sim_(pencil_game)
offline_file: ""
offline_thumbnail: ""
uuid: ced2bde8-f74b-4500-9459-83a405e7c264
updated: 1484308601
title: Sim (pencil game)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Complete_graph_K6.svg.png
categories:
    - Combinatorics
---
Two players take turns coloring any uncolored lines. One player colors in one color, and the other colors in another color, with each player trying to avoid the creation of a triangle made solely of their color (only triangles with the dots as corners count; intersections of lines are not relevant); the player who completes such a triangle loses immediately.
