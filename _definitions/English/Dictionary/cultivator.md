---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cultivator
offline_file: ""
offline_thumbnail: ""
uuid: d3af8e85-1c82-481d-ab8a-be8f914d2dd6
updated: 1484310451
title: cultivator
categories:
    - Dictionary
---
cultivator
     n 1: someone concerned with the science or art or business of
          cultivating the soil [syn: {agriculturist}, {grower}, {raiser}]
     2: a farm implement used to break up the surface of the soil
        (for aeration and weed control and conservation of
        moisture) [syn: {tiller}]
