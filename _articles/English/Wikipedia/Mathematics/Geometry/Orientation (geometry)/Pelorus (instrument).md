---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pelorus_(instrument)
offline_file: ""
offline_thumbnail: ""
uuid: 65825354-42e4-4c70-aff2-196325a0abf2
updated: 1484309299
title: Pelorus (instrument)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-HMS_Belfast_-_Wheelhouse_-_Pelorus.jpg
tags:
    - Modern use
    - Ancient instrument
categories:
    - Orientation (geometry)
---
In marine navigation, a pelorus is a reference tool for maintaining bearing of a vessel at sea. It is a "dumb compass" without a directive element, suitably mounted and provided with vanes to permit observation of relative bearings.[1]
