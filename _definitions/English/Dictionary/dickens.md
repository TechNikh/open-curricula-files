---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dickens
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484456401
title: dickens
categories:
    - Dictionary
---
dickens
     n 1: a word used in exclamations of confusion; "what the devil";
          "the deuce with it"; "the dickens you say" [syn: {devil},
           {deuce}]
     2: English writer whose novels depicted and criticized social
        injustice (1812-1870) [syn: {Charles Dickens}, {Charles
        John Huffam Dickens}]
