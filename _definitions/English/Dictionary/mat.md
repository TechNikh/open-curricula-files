---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mat
offline_file: ""
offline_thumbnail: ""
uuid: 479ae40f-a21f-4019-9cfe-fac56b1df4b6
updated: 1484310486
title: mat
categories:
    - Dictionary
---
mat
     adj : not reflecting light; not glossy; "flat wall paint"; "a
           photograph with a matte finish" [syn: {flat}, {matt}, {matte},
            {matted}]
     n 1: a thick flat pad used as a floor covering
     2: mounting consisting of a border or background for a picture
        [syn: {matting}]
     3: sports equipment consisting of a piece of thick padding on
        the floor for gymnastic sports [syn: {gym mat}]
     4: a master's degree in teaching [syn: {Master of Arts in
        Teaching}]
     5: the property of having little or no contrast; lacking
        highlights or gloss [syn: {flatness}, {lusterlessness}, {lustrelessness},
         {matt}, {matte}]
     6: a ...
