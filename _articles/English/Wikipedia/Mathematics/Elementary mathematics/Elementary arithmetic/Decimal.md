---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Decimal
offline_file: ""
offline_thumbnail: ""
uuid: 0cf66929-26af-4993-bf21-63ac7a404d66
updated: 1484308915
title: Decimal
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Decimal_multiplication_table.JPG
tags:
    - Decimal notation
    - Decimal fractions
    - Other rational numbers
    - Real numbers
    - Non-uniqueness of decimal representation
    - Decimal computation
    - History
    - History of decimal fractions
    - Natural languages
    - Other bases
categories:
    - Elementary arithmetic
---
The decimal numeral system (also called base 10 or occasionally denary) has ten as its base. It is the numerical base most widely used by modern civilizations.[1]
