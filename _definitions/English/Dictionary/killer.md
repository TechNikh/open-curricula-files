---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/killer
offline_file: ""
offline_thumbnail: ""
uuid: 266cf96f-fea1-4546-bee4-400a18539db5
updated: 1484310577
title: killer
categories:
    - Dictionary
---
killer
     n 1: someone who causes the death of a person or animal [syn: {slayer}]
     2: the causal agent resulting in death; "heart disease is the
        biggest killer in the United States" [syn: {cause of death}]
     3: a difficulty that is hard to deal with; "that exam was a
        real killer"
     4: predatory black-and-white toothed whale with large dorsal
        fin; common in cold seas [syn: {killer whale}, {orca}, {grampus},
         {sea wolf}, {Orcinus orca}]
