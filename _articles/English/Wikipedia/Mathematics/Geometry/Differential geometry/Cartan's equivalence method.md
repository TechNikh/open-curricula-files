---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cartan%27s_equivalence_method'
offline_file: ""
offline_thumbnail: ""
uuid: 1221e06e-3824-4fe9-b939-18dbef9009d1
updated: 1484309209
title: "Cartan's equivalence method"
categories:
    - Differential geometry
---
In mathematics, Cartan's equivalence method is a technique in differential geometry for determining whether two geometrical structures are the same up to a diffeomorphism. For example, if M and N are two Riemannian manifolds with metrics g and h, respectively, when is there a diffeomorphism
