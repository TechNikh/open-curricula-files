---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slow
offline_file: ""
offline_thumbnail: ""
uuid: 06bb0e29-6e2a-440a-a91d-f6f85b39fd20
updated: 1484310309
title: slow
categories:
    - Dictionary
---
slow
     adj 1: not moving quickly; taking a comparatively long time; "a
            slow walker"; "the slow lane of traffic"; "her steps
            were slow"; "he was slow in reacting to the news";
            "slow but steady growth" [ant: {fast}]
     2: at a slow tempo; "the band played a slow waltz" [ant: {fast}]
     3: slow to learn or understand; lacking intellectual acuity;
        "so dense he never understands anything I say to him";
        "never met anyone quite so dim"; "although dull at
        classical learning, at mathematics he was uncommonly
        quick"- Thackeray; "dumb officials make some really dumb
        decisions"; "he was either normally stupid or being
   ...
