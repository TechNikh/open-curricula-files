---
version: 1
type: video
provider: YouTube
id: t7xJNZGwg_w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: aa8e25f5-224b-4084-8e8e-8bf6fe4b4503
updated: 1486069616
title: Rational number word problem example 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/t7xJNZGwg_w/default.jpg
    - https://i3.ytimg.com/vi/t7xJNZGwg_w/1.jpg
    - https://i3.ytimg.com/vi/t7xJNZGwg_w/2.jpg
    - https://i3.ytimg.com/vi/t7xJNZGwg_w/3.jpg
---
