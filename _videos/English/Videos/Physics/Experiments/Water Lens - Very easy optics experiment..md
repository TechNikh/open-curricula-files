---
version: 1
type: video
provider: YouTube
id: FFWNvcjEjDQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 12fb46fe-df59-4b9d-bdbc-d9747f2cdc2a
updated: 1486069639
title: 'Water Lens - Very easy optics experiment.'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/FFWNvcjEjDQ/default.jpg
    - https://i3.ytimg.com/vi/FFWNvcjEjDQ/1.jpg
    - https://i3.ytimg.com/vi/FFWNvcjEjDQ/2.jpg
    - https://i3.ytimg.com/vi/FFWNvcjEjDQ/3.jpg
---
