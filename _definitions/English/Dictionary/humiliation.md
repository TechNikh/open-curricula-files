---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humiliation
offline_file: ""
offline_thumbnail: ""
uuid: 35d07788-a6db-42ca-bb3a-507e64d87523
updated: 1484310571
title: humiliation
categories:
    - Dictionary
---
humiliation
     n 1: state of disgrace or loss of self-respect
     2: strong feelings of embarrassment [syn: {chagrin}, {mortification}]
     3: an instance in which you are caused to lose your prestige or
        self-respect; "he had to undergo one humiliation after
        another" [syn: {mortification}]
     4: depriving one of self-esteem [syn: {abasement}]
