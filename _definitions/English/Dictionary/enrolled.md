---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enrolled
offline_file: ""
offline_thumbnail: ""
uuid: 1b68f128-10d5-4a52-a28a-e690727903c5
updated: 1484310448
title: enrolled
categories:
    - Dictionary
---
enrol
     v : register formally as a participant or member; "The party
         recruited many new members" [syn: {enroll}, {inscribe}, {enter},
          {recruit}]
     [also: {enrolling}, {enrolled}]
