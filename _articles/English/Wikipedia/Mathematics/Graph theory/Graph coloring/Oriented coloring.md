---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oriented_coloring
offline_file: ""
offline_thumbnail: ""
uuid: c2ac4393-ebc1-41f1-b46f-02f285f23f70
updated: 1484309386
title: Oriented coloring
categories:
    - Graph coloring
---
In graph theory, oriented graph coloring is a special type of graph coloring. Namely, it is an assignment of colors to vertices of an oriented graph that
