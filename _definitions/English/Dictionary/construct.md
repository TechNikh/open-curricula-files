---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/construct
offline_file: ""
offline_thumbnail: ""
uuid: 888bdba6-3922-4471-9a4e-20da76fbb2f2
updated: 1484310218
title: construct
categories:
    - Dictionary
---
construct
     n : an abstract or general idea inferred or derived from
         specific instances [syn: {concept}, {conception}] [ant: {misconception}]
     v 1: make by combining materials and parts; "this little pig made
          his house out of straw"; "Some eccentric constructed an
          electric brassiere warmer" [syn: {build}, {make}]
     2: put together out of components or parts; "the company
        fabricates plastic chairs"; "They manufacture small toys"
        [syn: {manufacture}, {fabricate}]
     3: draw with suitable instruments and under specified
        conditions; "construct an equilateral triangle"
     4: create by linking linguistic units; "construct a ...
