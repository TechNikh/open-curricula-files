---
version: 1
type: article
id: https://en.wikipedia.org/wiki/English_auction
offline_file: ""
offline_thumbnail: ""
uuid: 3ede1beb-d651-4ea2-8ba7-3321c0dc2e2f
updated: 1484309013
title: English auction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/English.gif
tags:
    - Variations
    - Candle auction
    - Japanese Auction
    - Selling more than one item
    - Notes and references
categories:
    - Auctions
---
Unlike sealed-bid auctions (such as First-price sealed-bid auction or Vickrey auction), an English auction is "open" or fully transparent, as the identity of all bidders is disclosed to each other during the auction. More generally, an auction mechanism is considered "English" if it involves an iterative process of adjusting the price in a direction that is unfavorable to the bidders (increasing in price if the item is being sold to competing buyers or decreasing in price in a reverse auction with competing sellers). In contrast, a Dutch auction would adjust the price in a direction that favored the bidders (lowering the price if the item is being sold to competing buyers, increasing it, if ...
