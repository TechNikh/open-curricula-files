---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/badly
offline_file: ""
offline_thumbnail: ""
uuid: 33dfa0f9-bece-4dab-af12-1024fa285d8e
updated: 1484310603
title: badly
categories:
    - Dictionary
---
badly
     adv 1: to a severe or serious degree; "fingers so badly frozen they
            had to be amputated"; "badly injured"; "a severely
            impaired heart"; "is gravely ill"; "was seriously ill"
            [syn: {severely}, {gravely}, {seriously}]
     2: (`ill' is often used as a combining form) in a poor or
        improper or unsatisfactory manner; not well; "he was ill
        prepared"; "it ill befits a man to betray old friends";
        "the car runs badly"; "he performed badly on the exam";
        "the team played poorly"; "ill-fitting clothes"; "an
        ill-conceived plan" [syn: {ill}, {poorly}] [ant: {well}]
     3: evilly or wickedly; "treated his parents ...
