---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Prime_signature
offline_file: ""
offline_thumbnail: ""
uuid: 6d1e5f81-07f2-454a-b76b-eb6b58761cb5
updated: 1484308654
title: Prime signature
tags:
    - Numbers with same prime signature
    - Sequences defined by their prime signature
categories:
    - Number theory
---
The prime signature of a number is the multiset of exponents of its prime factorisation. The prime signature of a number having prime factorisation 
  
    
      
        
          p
          
            1
          
          
            
              m
              
                1
              
            
          
        
        
          p
          
            2
          
          
            
              m
              
                2
              
            
          
        
        …
        
          p
          
            n
          
          
            
              m
              
                n
              
            
          ...
