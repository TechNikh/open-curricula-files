---
version: 1
type: article
id: https://en.wikipedia.org/wiki/N-sphere
offline_file: ""
offline_thumbnail: ""
uuid: 0c0e60da-21e8-4d88-8c1f-51f2eb67a054
updated: 1484309289
title: N-sphere
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sphere_wireframe.svg_0.png
tags:
    - Description
    - Euclidean coordinates in (n + 1)-space
    - n-ball
    - Topological description
    - Volume and surface area
    - Examples
    - Recurrences
    - Closed forms
    - Other relations
    - Spherical coordinates
    - Spherical volume element
    - Stereographic projection
    - Generating random points
    - Uniformly at random from the (n − 1)-sphere
    - Examples
    - Alternatives
    - Uniformly at random from the n-ball
    - Specific spheres
    - Notes
categories:
    - Multi-dimensional geometry
---
In mathematics, the n-sphere is the generalization of the ordinary sphere to spaces of arbitrary dimension. It is an n-dimensional manifold that can be embedded in Euclidean (n + 1)-space.
