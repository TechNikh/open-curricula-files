---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/celebrate
offline_file: ""
offline_thumbnail: ""
uuid: cd89392a-21a5-47af-b521-c607540b2424
updated: 1484310545
title: celebrate
categories:
    - Dictionary
---
celebrate
     v 1: celebrate, as of holidays or rites; "Keep the commandments";
          "celebrate Christmas"; "Observe Yom Kippur" [syn: {observe},
           {keep}]
     2: have a celebration; "They were feting the patriarch of the
        family"; "After the exam, the students were celebrating"
        [syn: {fete}]
     3: assign great social importance to; "The film director was
        celebrated all over Hollywood"; "The tenor was lionized in
        Vienna" [syn: {lionize}, {lionise}]
