---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abscisic_acid
offline_file: ""
offline_thumbnail: ""
uuid: 4021330a-8b06-46aa-9fab-f49ef7817956
updated: 1484308361
title: Abscisic acid
tags:
    - In plants
    - Function
    - Homeostasis
    - Biosynthesis
    - Location and timing of ABA biosynthesis
    - Inactivation
    - Effects
    - In Fungi
    - In animals
    - Measurement of ABA Concentration
categories:
    - Uncategorized
---
Abscisic acid (ABA), also known as Dormin, Dormic acid (DMA), is best known as a plant hormone. ABA functions in many plant developmental processes, including bud dormancy. It is degraded by the enzyme (+)-abscisic acid 8'-hydroxylase into phaseic acid.
