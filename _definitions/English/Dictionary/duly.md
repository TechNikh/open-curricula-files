---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/duly
offline_file: ""
offline_thumbnail: ""
uuid: 781f85bf-fa7b-411b-bc05-71dee4e4eaaa
updated: 1484310591
title: duly
categories:
    - Dictionary
---
duly
     adv : at the proper time; "she was duly apprised of the raise"
           [syn: {punctually}]
