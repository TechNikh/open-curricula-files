---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/library
offline_file: ""
offline_thumbnail: ""
uuid: 347751e0-5374-45d7-a534-0360d788342b
updated: 1484310315
title: library
categories:
    - Dictionary
---
library
     n 1: a room where books are kept; "they had brandy in the
          library"
     2: a collection of literary documents or records kept for
        reference or borrowing
     3: a depository built to contain books and other materials for
        reading and study [syn: {depository library}]
     4: (computing) a collection of standard programs and
        subroutines that are stored and available for immediate
        use [syn: {program library}, {subroutine library}]
     5: a building that houses a collection of books and other
        materials
