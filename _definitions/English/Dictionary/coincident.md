---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coincident
offline_file: ""
offline_thumbnail: ""
uuid: d62b58fd-c551-4db8-a710-a946824d3c9b
updated: 1484310156
title: coincident
categories:
    - Dictionary
---
coincident
     adj 1: occurring or operating at the same time; "a series of
            coincident events" [syn: {coincidental}, {coinciding},
             {concurrent}, {cooccurring}, {simultaneous}]
     2: matching point for point; "coincident circles" [syn: {coinciding}]
