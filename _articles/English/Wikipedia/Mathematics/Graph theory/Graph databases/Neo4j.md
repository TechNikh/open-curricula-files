---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Neo4j
offline_file: ""
offline_thumbnail: ""
uuid: 672d1ea2-6fec-44ca-b0df-42c5b2453030
updated: 1484309404
title: Neo4j
tags:
    - Licensing and editions
    - Data structure
    - Neo technology
    - Bibliography
categories:
    - Graph databases
---
Neo4j is a graph database management system developed by Neo Technology, Inc. Described by its developers as an ACID-compliant transactional database with native graph storage and processing,[3] Neo4j is the most popular graph database according to db-engines.com.[4]
