---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arabian
offline_file: ""
offline_thumbnail: ""
uuid: 55fdb1be-6ce0-4f4a-a06b-4fdfc81f4edb
updated: 1484310435
title: arabian
categories:
    - Dictionary
---
Arabian
     adj 1: relating to or associated with Arabia or its people;
            "Arabian Nights"; "Arabian Sea"
     2: of or relating to Arabian horses
     n 1: a member of a Semitic people originally from the Arabian
          peninsula and surrounding territories who speaks Arabic
          and who inhabits much of the Middle East and northern
          Africa [syn: {Arab}]
     2: a spirited graceful and intelligent riding horse native to
        Arabia [syn: {Arab}]
