---
version: 1
type: article
id: https://en.wikipedia.org/wiki/William_Edge_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: f5e1d707-b2af-49c2-bd4f-d9fdb7131e2d
updated: 1484309078
title: William Edge (mathematician)
categories:
    - Geometers
---
Prof William Leonard Edge FRSE (November 8, 1904 – September 27, 1997) was a British mathematician most known for his work in finite geometry. Students knew him as WLE.
