---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eka-silicon
offline_file: ""
offline_thumbnail: ""
uuid: 19563e65-59f5-40de-b3f8-d52d7d68c45c
updated: 1484308383
title: "Mendeleev's predicted elements"
tags:
    - Prefixes
    - Original predictions
    - Other predictions
    - Later predictions
categories:
    - Uncategorized
---
Dmitri Mendeleev published the first periodic table of the chemical elements in 1869 based on properties which appeared with some regularity as he laid out the elements from lightest to heaviest.[1] When Mendeleev proposed his periodic table, he noted gaps in the table, and predicted that as-then-unknown elements existed with properties appropriate to fill those gaps.
