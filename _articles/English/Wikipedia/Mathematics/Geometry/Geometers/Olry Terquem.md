---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Olry_Terquem
offline_file: ""
offline_thumbnail: ""
uuid: 49690d2e-b6a6-41fa-bcc6-3e757e04f9a3
updated: 1484309105
title: Olry Terquem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Triangle.NinePointCircle.svg.png
tags:
    - Education and career
    - Mathematics
    - Jewish activism
categories:
    - Geometers
---
Olry Terquem (16 June 1782 – 6 May 1862) was a French mathematician. He is known for his works in geometry and for founding two scientific journals, one of which was the first journal about the history of mathematics. He was also the pseudonymous author (as Tsarphati) of a sequence of letters advocating radical Reform in Judaism.[1] He was French Jewish.
