---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/proximity
offline_file: ""
offline_thumbnail: ""
uuid: 17656024-3abe-431d-9023-99b3cd130d23
updated: 1484310525
title: proximity
categories:
    - Dictionary
---
proximity
     n 1: the property of being close together [syn: {propinquity}]
     2: the region close around a person or thing
     3: a Gestalt principle of organization holding that (other
        things being equal) objects or events that are near to one
        another (in space or time) are perceived as belonging
        together as a unit [syn: {law of proximity}]
