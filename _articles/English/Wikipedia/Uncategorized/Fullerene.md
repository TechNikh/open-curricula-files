---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fullerenes
offline_file: ""
offline_thumbnail: ""
uuid: f6ba88c5-f1bf-431b-bbf8-5d2804f1a903
updated: 1484308392
title: Fullerene
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Fullerene_c540_0.png
tags:
    - History
    - Naming
    - Types of fullerene
    - Buckyballs
    - Buckminsterfullerene
    - Boron buckyball
    - Other buckyballs
    - Carbon nanotubes
    - Carbon nanobuds
    - Fullerite
    - Inorganic fullerenes
    - Properties
    - Aromaticity
    - chemistry
    - Solubility
    - Quantum mechanics
    - Superconductivity
    - Chirality
    - Construction
    - Production technology
    - Applications
    - Tumor research
    - Safety and toxicity
    - Popular culture
categories:
    - Uncategorized
---
A fullerene is a molecule of carbon in the form of a hollow sphere, ellipsoid, tube, and many other shapes. Spherical fullerenes, also referred to as Buckminsterfullerenes (buckyballs), resemble the balls used in football (soccer). Cylindrical fullerenes are also called carbon nanotubes (buckytubes). Fullerenes are similar in structure to graphite, which is composed of stacked graphene sheets of linked hexagonal rings; they may also contain pentagonal (or sometimes heptagonal) rings.[1]
