---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Functional_renormalization_group
offline_file: ""
offline_thumbnail: ""
uuid: 1c351a41-306f-49f7-abde-6918d9a07077
updated: 1484308787
title: Functional renormalization group
tags:
    - The flow equation for the effective action
    - Aspects of functional renormalization
    - >
        Functional renormalization-group for Wick-ordered effective
        interaction
    - Applications
    - Papers
    - Pedagogic reviews
categories:
    - Fixed points (mathematics)
---
In theoretical physics, functional renormalization group (FRG) is an implementation of the renormalization group (RG) concept which is used in quantum and statistical field theory, especially when dealing with strongly interacting systems. The method combines functional methods of quantum field theory with the intuitive renormalization group idea of Kenneth G. Wilson. This technique allows to interpolate smoothly between the known microscopic laws and the complicated macroscopic phenomena in physical systems. In this sense, it bridges the transition from simplicity of microphysics to complexity of macrophysics. Figuratively speaking, FRG acts as a microscope with a variable resolution. One ...
