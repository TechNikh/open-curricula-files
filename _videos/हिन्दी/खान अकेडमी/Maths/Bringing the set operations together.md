---
version: 1
type: video
provider: YouTube
id: dPM1eeVFFRw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 021ef05c-19aa-45ea-b05e-b934d938234c
updated: 1486069605
title: Bringing the set operations together
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/dPM1eeVFFRw/default.jpg
    - https://i3.ytimg.com/vi/dPM1eeVFFRw/1.jpg
    - https://i3.ytimg.com/vi/dPM1eeVFFRw/2.jpg
    - https://i3.ytimg.com/vi/dPM1eeVFFRw/3.jpg
---
