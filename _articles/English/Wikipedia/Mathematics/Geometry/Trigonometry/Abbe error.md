---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abbe_error
offline_file: ""
offline_thumbnail: ""
uuid: 7d0472cc-6925-4194-a13f-74f85e4a3082
updated: 1484309340
title: Abbe error
categories:
    - Trigonometry
---
Abbe error, named after Ernst Abbe, also called sine error, describes the magnification of angular error over distance. For example, when one measures a point that is 1 meter away at 45 degrees, an angular error of 1 degree corresponds to a positional error of over 1.745 cm, equivalent to a distance-measurement error of 1.745%.
