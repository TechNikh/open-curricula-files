---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adrenal
offline_file: ""
offline_thumbnail: ""
uuid: c38520d1-d2b3-4112-bfcf-dc9eb9421ea1
updated: 1484310162
title: adrenal
categories:
    - Dictionary
---
adrenal
     adj 1: of or pertaining to the adrenal glands or their secretions
     2: near the kidneys
     n : either of a pair of complex endocrine glands situated near
         the kidney [syn: {adrenal gland}, {suprarenal gland}]
