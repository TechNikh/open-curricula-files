---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/send
offline_file: ""
offline_thumbnail: ""
uuid: 16dc6253-f313-4fbf-b4a9-5441d29aede6
updated: 1484310357
title: send
categories:
    - Dictionary
---
send
     v 1: cause to go somewhere; "The explosion sent the car flying in
          the air"; "She sent her children to camp"; "He directed
          all his energies into his dissertation" [syn: {direct}]
     2: to cause or order to be taken, directed, or transmitted to
        another place; "He had sent the dispatches downtown to the
        proper people and had slept" [syn: {send out}]
     3: cause to be directed or transmitted to another place; "send
        me your latest results"; "I'll mail you the paper when
        it's written" [syn: {mail}, {post}]
     4: transport commercially [syn: {transport}, {ship}]
     5: assign to a station [syn: {station}, {post}, {base}, ...
