---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Toida%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 78ec0598-76df-4a71-80c1-20769dae8dbc
updated: 1484308595
title: "Toida's conjecture"
categories:
    - Combinatorics
---
In combinatorial mathematics, Toida's conjecture, due to Shunichi Toida in 1977,[1] is a refinement of the disproven Ádám's conjecture in 1967. Toida's conjecture states formally:
