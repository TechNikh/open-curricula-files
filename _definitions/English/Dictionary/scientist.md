---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/scientist
offline_file: ""
offline_thumbnail: ""
uuid: 3bb0193c-bebc-4069-92ed-b0c18f05cd78
updated: 1484310351
title: scientist
categories:
    - Dictionary
---
scientist
     n : a person with advanced knowledge of one of more sciences
         [syn: {man of science}]
