---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pradeep_Dubey
offline_file: ""
offline_thumbnail: ""
uuid: 2d5d1b23-816f-4ad8-9755-d65057d031ac
updated: 1484308512
title: Pradeep Dubey
categories:
    - Game theorists
---
Pradeep Dubey, born January 9, 1951 in Patna, Bihar, India, is an American game theorist. He is Professor of Economics at State University of New York, Stony Brook and a member[1] of the Stony Brook Center for Game Theory. He also holds a visiting position [2] at Cowles Foundation, Yale University. He did his schooling from the St. Columba's School, Delhi. He received his Ph.D. in Applied Mathematics from Cornell University and B.Sc. (with Honors in Physics) from the University of Delhi. His areas of research interests are game theory and mathematical economics. He has published, among others, in Econometrica, Games and Economic Behavior, Journal of Economic Theory and Quarterly Journal of ...
