---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Brian_Skyrms
offline_file: ""
offline_thumbnail: ""
uuid: fbfe3040-7a12-47a2-bc2c-241c4cbf863b
updated: 1484309029
title: Brian Skyrms
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/175px-BrianSkyrms.jpg
categories:
    - Game theorists
---
Brian Skyrms (born 1938) is a Distinguished Professor of Logic and Philosophy of Science and Economics at the University of California, Irvine and a Professor of Philosophy at Stanford University. He has worked on problems in the philosophy of science, causation, decision theory, game theory, and the foundations of probability. Most recently, his work has focused on the evolution of social norms using evolutionary game theory. His two recent books Evolution of the Social Contract and The Stag Hunt are both on this topic. These books use arguments and examples from evolutionary game theory to cover topics of interest to political philosophy, philosophy of social science, philosophy of ...
