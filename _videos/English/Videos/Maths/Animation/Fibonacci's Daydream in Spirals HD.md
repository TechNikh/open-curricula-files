---
version: 1
type: video
provider: YouTube
id: mHNhlAi6gew
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1ff4dbe7-f633-4e7c-8ba4-636217d62da8
updated: 1486069645
title: "Fibonacci's Daydream in Spirals HD"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/mHNhlAi6gew/default.jpg
    - https://i3.ytimg.com/vi/mHNhlAi6gew/1.jpg
    - https://i3.ytimg.com/vi/mHNhlAi6gew/2.jpg
    - https://i3.ytimg.com/vi/mHNhlAi6gew/3.jpg
---
