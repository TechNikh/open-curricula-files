---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outer
offline_file: ""
offline_thumbnail: ""
uuid: 5f991c3a-3787-4208-9cd3-2747f91763ba
updated: 1484310328
title: outer
categories:
    - Dictionary
---
outer
     adj 1: being on the outside or further from a center; "spent hours
            adorning the outer man"; "the outer suburbs" [syn: {outer(a)}]
            [ant: {inner(a)}]
     2: located outside; "outer reality"
     3: being on or toward the outside of the body; "the outer ear"
