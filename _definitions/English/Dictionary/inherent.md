---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inherent
offline_file: ""
offline_thumbnail: ""
uuid: 0aa398ff-3664-47cc-8c2c-3d167778256e
updated: 1484310591
title: inherent
categories:
    - Dictionary
---
inherent
     adj 1: existing as an essential constituent or characteristic; "the
            Ptolemaic system with its built-in concept of
            periodicity"; "a constitutional inability to tell the
            truth" [syn: {built-in}, {constitutional}, {inbuilt},
            {integral}]
     2: present at birth but not necessarily hereditary; acquired
        during fetal development [syn: {congenital}, {inborn}, {innate}]
     3: in the nature of something though not readily apparent;
        "shortcomings inherent in our approach"; "an underlying
        meaning" [syn: {implicit in(p)}, {underlying}]
