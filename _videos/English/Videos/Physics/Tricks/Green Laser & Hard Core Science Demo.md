---
version: 1
type: video
provider: YouTube
id: trpL0ldEVFM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 03bf8e0b-ab3c-4360-99fe-ce8ef94a856f
updated: 1486069660
title: 'Green Laser & Hard Core Science Demo'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/trpL0ldEVFM/default.jpg
    - https://i3.ytimg.com/vi/trpL0ldEVFM/1.jpg
    - https://i3.ytimg.com/vi/trpL0ldEVFM/2.jpg
    - https://i3.ytimg.com/vi/trpL0ldEVFM/3.jpg
---
