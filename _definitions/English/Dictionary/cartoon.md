---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cartoon
offline_file: ""
offline_thumbnail: ""
uuid: 4ff88bff-076d-4856-b022-51c01a1240ff
updated: 1484310316
title: cartoon
categories:
    - Dictionary
---
cartoon
     n 1: a humorous or satirical drawing published in a newspaper or
          magazine [syn: {sketch}]
     2: a film made by photographing a series of cartoon drawings to
        give the illusion of movement when projected in rapid
        sequence [syn: {animated cartoon}]
     v : draw cartoons of
