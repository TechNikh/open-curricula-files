---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Truth-table_reduction
offline_file: ""
offline_thumbnail: ""
uuid: 3c601516-2ac3-4af5-b8cb-73dc823d8733
updated: 1484309490
title: Truth-table reduction
categories:
    - Computability theory
---
In computability theory, a truth-table reduction is a reduction from one set of natural numbers to another. As a "tool", it is weaker than Turing reduction, since not every Turing reduction between sets can be performed by a truth-table reduction, but every truth-table reduction can be performed by a Turing reduction. For the same reason it is said to be a stronger reducibility than Turing reducibility, because it implies Turing reducibility. A weak truth-table reduction is a related type of reduction which is so named because it weakens the constraints placed on a truth-table reduction, and provides a weaker equivalence classification; as such, a "weak truth-table reduction" can actually ...
