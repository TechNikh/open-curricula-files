---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crashing
offline_file: ""
offline_thumbnail: ""
uuid: b9f1fa8d-45a4-4185-8fd4-3dc66a8581d7
updated: 1484310559
title: crashing
categories:
    - Dictionary
---
crashing
     adj : (used of persons) informal intensifiers; "what a bally (or
           blinking) nuisance"; "a bloody fool"; "a crashing
           bore"; "you flaming idiot" [syn: {bally(a)}, {blinking(a)},
            {bloody(a)}, {blooming(a)}, {crashing(a)}, {flaming(a)},
            {fucking(a)}]
