---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/balcony
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484378281
title: balcony
categories:
    - Dictionary
---
balcony
     n 1: an upper floor projecting from the rear over the main floor
          in an auditorium
     2: a platform projecting from the wall of a building and
        surrounded by a balustrade or railing or parapet
