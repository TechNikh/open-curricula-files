---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cymatics
offline_file: ""
offline_thumbnail: ""
uuid: 560bd261-d27d-49e3-887b-0a89b36edcf1
updated: 1484309309
title: Cymatics
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Resonance_Chladni_Soundboard_Harpsichord_Clavecin.jpg
tags:
    - History
    - Work of Hans Jenny
    - Cymatics in Healing
    - Influences on art and music
    - Influences in engineering
categories:
    - Symmetry
---
Cymatics, from Greek: κῦμα, meaning "wave", is a subset of modal vibrational phenomena. The term was coined by Hans Jenny (1904-1972), a Swiss follower of the philosophical school known as anthroposophy. Typically the surface of a plate, diaphragm or membrane is vibrated, and regions of maximum and minimum displacement are made visible in a thin coating of particles, paste or liquid.[1] Different patterns emerge in the excitatory medium depending on the geometry of the plate and the driving frequency.
