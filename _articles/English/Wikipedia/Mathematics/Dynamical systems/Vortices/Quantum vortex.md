---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantum_vortex
offline_file: ""
offline_thumbnail: ""
uuid: d074d2ea-0fbf-4705-af07-955e4bb2ea03
updated: 1484308875
title: Quantum vortex
tags:
    - Vortex-quantisation in a superfluid
    - Vortex-quantization in a superconductor
    - Constrained vortices in ferromagnets and antiferromagnets
    - Statistical mechanics of vortex lines
categories:
    - Vortices
---
In physics, a quantum vortex is a topological defect exhibited in superfluids and superconductors. The existence of quantum vortices was predicted by Lars Onsager in 1947 in connection with superfluid helium. Onsager also pointed out that quantum vortices describe the circulation of superfluid and conjectured that their excitations are responsible for superfluid phase transitions. These ideas of Onsager were further developed by Richard Feynman in 1955[1] and in 1957 were applied to describe the magnetic phase diagram of type-II superconductors by Alexei Alexeyevich Abrikosov.[2]
