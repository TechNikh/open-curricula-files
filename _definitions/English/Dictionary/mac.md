---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mac
offline_file: ""
offline_thumbnail: ""
uuid: 4cbe9438-68b2-4f86-b1b5-bce984df2f08
updated: 1484310163
title: mac
categories:
    - Dictionary
---
mac
     n : a waterproof raincoat made of rubberized fabric [syn: {macintosh},
          {mackintosh}, {mack}]
