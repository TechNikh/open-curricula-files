---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chaos_computing
offline_file: ""
offline_thumbnail: ""
uuid: fb00a3b7-f098-48ef-b2b8-728d7739b043
updated: 1484308664
title: Chaos computing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Ditto_Chaos_Computing_Example_1.jpg
tags:
    - Introduction
    - Chaotic Morphing
    - ChaoGate
    - Research
categories:
    - Theoretical computer science
---
Chaos computing is the idea of using chaotic systems for computation. In particular, chaotic systems can be made to produce all types of logic gates and further allow them to be morphed into each other.
