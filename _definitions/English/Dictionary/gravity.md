---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gravity
offline_file: ""
offline_thumbnail: ""
uuid: b3aac62b-6292-4ada-af5a-53e0d2f00405
updated: 1484310366
title: gravity
categories:
    - Dictionary
---
gravity
     n 1: (physics) the force of attraction between all masses in the
          universe; especially the attraction of the earth's mass
          for bodies near its surface; "the more remote the body
          the less the gravity"; "the gravitation between two
          bodies is proportional to the product of their masses
          and inversely proportional to the square of the distance
          between them"; "gravitation cannot be held responsible
          for people falling in love"--Albert Einstein [syn: {gravitation},
           {gravitational attraction}, {gravitational force}]
     2: a manner that is serious and solemn [syn: {graveness}, {sobriety},
         ...
