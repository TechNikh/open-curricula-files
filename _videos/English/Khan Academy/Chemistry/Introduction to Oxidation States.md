---
version: 1
type: video
provider: YouTube
id: _fNNQfGGYr4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Introduction%20to%20Oxidation%20States.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7c4231a8-8fdf-4b85-9f21-44953c5a107a
updated: 1486069581
title: Introduction to Oxidation States
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/_fNNQfGGYr4/default.jpg
    - https://i3.ytimg.com/vi/_fNNQfGGYr4/1.jpg
    - https://i3.ytimg.com/vi/_fNNQfGGYr4/2.jpg
    - https://i3.ytimg.com/vi/_fNNQfGGYr4/3.jpg
---
Oxidation and reduction.  Oxidation states.
