---
version: 1
type: video
provider: YouTube
id: eNVBDudkHW4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ef61ad35-8ea8-419b-bccf-97dba23ccc76
updated: 1486069618
title: Subtracting two digit numbers without regrouping example 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/eNVBDudkHW4/default.jpg
    - https://i3.ytimg.com/vi/eNVBDudkHW4/1.jpg
    - https://i3.ytimg.com/vi/eNVBDudkHW4/2.jpg
    - https://i3.ytimg.com/vi/eNVBDudkHW4/3.jpg
---
