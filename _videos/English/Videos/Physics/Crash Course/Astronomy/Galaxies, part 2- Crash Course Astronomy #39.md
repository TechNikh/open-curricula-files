---
version: 1
type: video
provider: YouTube
id: _O2sg-PGhEg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Galaxies%2C%20part%202-%20Crash%20Course%20Astronomy%20%2339.webm"
offline_file: ""
offline_thumbnail: ""
uuid: de5947ec-34eb-41df-9b25-fa674dc80150
updated: 1486069684
title: 'Galaxies, part 2- Crash Course Astronomy #39'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/_O2sg-PGhEg/default.jpg
    - https://i3.ytimg.com/vi/_O2sg-PGhEg/1.jpg
    - https://i3.ytimg.com/vi/_O2sg-PGhEg/2.jpg
    - https://i3.ytimg.com/vi/_O2sg-PGhEg/3.jpg
---
