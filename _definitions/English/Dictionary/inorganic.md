---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inorganic
offline_file: ""
offline_thumbnail: ""
uuid: 52df3485-f339-467b-8e9f-d32420c9d206
updated: 1484310264
title: inorganic
categories:
    - Dictionary
---
inorganic
     adj 1: relating or belonging to the class of compounds not having a
            carbon basis; "hydrochloric and sulfuric acids are
            called inorganic substances" [ant: {organic}]
     2: lacking the properties characteristic of living organisms
        [ant: {organic}]
