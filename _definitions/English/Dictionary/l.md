---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/l
offline_file: ""
offline_thumbnail: ""
uuid: 003b79f7-2e3d-4823-9173-1aa96289b685
updated: 1484310140
title: l
categories:
    - Dictionary
---
l
     adj : being ten more than forty [syn: {fifty}, {50}]
     n 1: a metric unit of capacity equal to the volume of 1 kilogram
          of pure water at 4 degrees centigrade and 760 mm of
          mercury (or approximately 1.76 pints) [syn: {liter}, {litre},
           {cubic decimeter}, {cubic decimetre}]
     2: the cardinal number that is the product of ten and five
        [syn: {fifty}, {50}]
     3: a cgs unit of illumination equal to the brightness of a
        perfectly diffusing surface that emits or reflects one
        lumen per square centimeter [syn: {lambert}]
     4: the 12th letter of the Roman alphabet
