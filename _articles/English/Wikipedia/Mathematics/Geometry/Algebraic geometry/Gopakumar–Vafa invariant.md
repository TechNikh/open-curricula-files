---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gopakumar%E2%80%93Vafa_invariant'
offline_file: ""
offline_thumbnail: ""
uuid: 69900109-1ecb-4f79-b909-57176e9fa84b
updated: 1484309149
title: Gopakumar–Vafa invariant
categories:
    - Algebraic geometry
---
In theoretical physics Rajesh Gopakumar and Cumrun Vafa introduced new topological invariants, which named Gopakumar–Vafa invariant, that represent the number of BPS states on Calabi–Yau 3-fold, in a series of papers. (see Gopakumar & Vafa (1998a),Gopakumar & Vafa (1998b) and also see Gopakumar & Vafa (1998c), Gopakumar & Vafa (1998d).)　They lead the following formula generating function for the Gromov–Witten invariant on Calabi–Yau 3-fold M.
