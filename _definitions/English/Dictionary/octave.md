---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/octave
offline_file: ""
offline_thumbnail: ""
uuid: 5fa6fc55-72bd-4871-95fe-bf86b3217552
updated: 1484310397
title: octave
categories:
    - Dictionary
---
octave
     n 1: a feast day and the seven days following it
     2: a musical interval of eight tones [syn: {musical octave}]
     3: a rhythmic group of eight lines of verse
