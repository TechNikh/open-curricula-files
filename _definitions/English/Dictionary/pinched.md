---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pinched
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484462881
title: pinched
categories:
    - Dictionary
---
pinched
     adj 1: sounding as if the nose were pinched; "a whining nasal
            voice" [syn: {adenoidal}, {nasal}]
     2: very thin especially from disease or hunger or cold;
        "emaciated bony hands"; "a nightmare population of gaunt
        men and skeletal boys"; "eyes were haggard and cavernous";
        "small pinched faces"; "kept life in his wasted frame only
        by grim concentration" [syn: {bony}, {cadaverous}, {emaciated},
         {gaunt}, {haggard}, {skeletal}, {wasted}]
     3: not having enough money to pay for necessities [syn: {hard
        up}, {impecunious}, {in straitened circumstances(p)}, {penniless},
         {penurious}]
     4: as if squeezed ...
