---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/seemingly
offline_file: ""
offline_thumbnail: ""
uuid: 22459c86-516c-4613-a35a-2cef9008a66e
updated: 1484310309
title: seemingly
categories:
    - Dictionary
---
seemingly
     adv : from appearances alone; "irrigation often produces bumper
           crops from apparently desert land"; "the child is
           seemingly healthy but the doctor is concerned"; "had
           been ostensibly frank as to his purpose while really
           concealing it"-Thomas Hardy; "on the face of it the
           problem seems minor" [syn: {apparently}, {ostensibly},
           {on the face of it}]
