---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Alternated_order-4_hexagonal_tiling
offline_file: ""
offline_thumbnail: ""
uuid: cebda356-c1f5-4fb3-8cda-ccf0179bbeab
updated: 1484309352
title: Alternated order-4 hexagonal tiling
tags:
    - Uniform constructions
    - Related polyhedra and tiling
categories:
    - Geometry stubs
---
In geometry, the alternated order-4 hexagonal tiling or ditetragonal tritetratrigonal tiling is a uniform tiling of the hyperbolic plane. It has Schläfli symbol of (3,4,4), h{6,4}, and hr{6,6}.
