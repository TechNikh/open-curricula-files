---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/delicate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484447521
title: delicate
categories:
    - Dictionary
---
delicate
     adj 1: exquisitely fine and subtle and pleasing; susceptible to
            injury; "a delicate violin passage"; "delicate china";
            "a delicate flavor"; "the delicate wing of a
            butterfly" [ant: {rugged}]
     2: marked by great skill especially in meticulous technique; "a
        surgeon's delicate touch"
     3: easily broken or damaged or destroyed; "a kite too delicate
        to fly safely"; "fragile porcelain plates"; "fragile old
        bones"; "a frail craft" [syn: {fragile}, {frail}]
     4: easily hurt; "soft hands"; "a baby's delicate skin" [syn: {soft}]
     5: developed with extreme delicacy and subtlety; "the satire
        touches with ...
