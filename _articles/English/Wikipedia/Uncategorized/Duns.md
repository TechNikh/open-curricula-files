---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Duns
offline_file: ""
offline_thumbnail: ""
uuid: 3d6cffa9-624b-4e3f-9778-586219cbe2ab
updated: 1484308415
title: Duns
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/125px-The_Covenanter%2527s_Stone_on_Duns_Law_-_geograph.org.uk_-_1265154.jpg'
tags:
    - History
    - Early history
    - Middle Ages
    - Early modern
    - Today
    - Castle
    - Country houses
    - Polish War Memorial
    - Transport
    - Education
    - The Ba game of Duns
    - Notable people
    - Twin towns
    - Notes
    - Sources
categories:
    - Uncategorized
---
Duns (historically Scots: Dunse) is the county town of the historic county of Berwickshire, within the Scottish Borders.
