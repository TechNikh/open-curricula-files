---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perspective
offline_file: ""
offline_thumbnail: ""
uuid: df61c1ff-1979-4377-886d-ec35966f9374
updated: 1484310467
title: perspective
categories:
    - Dictionary
---
perspective
     n 1: a way of regarding situations or topics etc.; "consider what
          follows from the positivist view" [syn: {position}, {view}]
     2: the appearance of things relative to one another as
        determined by their distance from the viewer [syn: {linear
        perspective}]
