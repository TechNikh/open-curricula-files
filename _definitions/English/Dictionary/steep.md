---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/steep
offline_file: ""
offline_thumbnail: ""
uuid: 14fc34f9-1caf-4c92-b099-5b1ca33b7b99
updated: 1484310435
title: steep
categories:
    - Dictionary
---
steep
     adj 1: having a sharp inclination; "the steep attic stairs"; "steep
            cliffs" [ant: {gradual}]
     2: greatly exceeding bounds of reason or moderation;
        "exorbitant rent"; "extortionate prices"; "spends an
        outrageous amount on entertainment"; "usorious interest
        rate"; "unconscionable spending" [syn: {exorbitant}, {extortionate},
         {outrageous}, {unconscionable}, {usurious}]
     3: of a slope; set at a high angle; "note the steep incline";
        "a steep roof sheds snow"
     n : a steep place (as on a hill)
     v 1: engross (oneself) fully; "He immersed himself into his
          studies" [syn: {immerse}, {engulf}, {plunge}, ...
