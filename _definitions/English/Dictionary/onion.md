---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/onion
offline_file: ""
offline_thumbnail: ""
uuid: 34e2651f-a312-45de-93f3-5c2cddc45f46
updated: 1484310379
title: onion
categories:
    - Dictionary
---
onion
     n 1: edible bulb of an onion plant
     2: bulbous plant having hollow leaves cultivated worldwide for
        its rounded edible bulb [syn: {onion plant}, {Allium cepa}]
     3: an aromatic flavorful bulb
