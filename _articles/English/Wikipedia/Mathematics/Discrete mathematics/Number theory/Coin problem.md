---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coin_problem
offline_file: ""
offline_thumbnail: ""
uuid: 3821fa70-96b4-43a4-9260-64df3e7467fe
updated: 1484308639
title: Coin problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Chicken_McNuggets.jpg
tags:
    - Statement
    - Frobenius numbers for small n
    - n = 1
    - n = 2
    - n = 3
    - Frobenius numbers for special sets
    - Arithmetic sequences
    - Geometric sequences
    - Examples
    - McNugget numbers
    - Other examples
categories:
    - Number theory
---
The coin problem (also referred to as the Frobenius coin problem or Frobenius problem, after the mathematician Ferdinand Frobenius) is a mathematical problem that asks for the largest monetary amount that cannot be obtained using only coins of specified denominations.[1] For example, the largest amount that cannot be obtained using only coins of 3 and 5 units is 7 units. The solution to this problem for a given set of coin denominations is called the Frobenius number of the set.
