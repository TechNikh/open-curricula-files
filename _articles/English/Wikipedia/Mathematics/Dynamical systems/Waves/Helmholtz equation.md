---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Helmholtz_equation
offline_file: ""
offline_thumbnail: ""
uuid: eb307619-f0c2-4d06-8ce1-4070b5d65807
updated: 1484308887
title: Helmholtz equation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Helmholtz_source.png
tags:
    - Motivation and uses
    - Solving the Helmholtz equation using separation of variables
    - Vibrating membrane
    - Three-dimensional solutions
    - Paraxial approximation
    - Inhomogeneous Helmholtz equation
    - Notes
categories:
    - Waves
---
where ∇2 is the Laplacian, k is the wavenumber, and A is the amplitude.
