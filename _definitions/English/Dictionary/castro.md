---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/castro
offline_file: ""
offline_thumbnail: ""
uuid: 74de461a-c7fe-4c1e-a932-707db533f536
updated: 1484310180
title: castro
categories:
    - Dictionary
---
Castro
     n : Cuban socialist leader who overthrew a dictator in 1959 and
         established a Marxist socialist state in Cuba (born in
         1927) [syn: {Fidel Castro}, {Fidel Castro Ruz}]
