---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Motty_Perry
offline_file: ""
offline_thumbnail: ""
uuid: 89f26700-0dd6-4453-b16b-9bc5003761fc
updated: 1484309026
title: Motty Perry
tags:
    - Biography
    - Political activism
    - Published works
categories:
    - Game theorists
---
Motty Perry (Hebrew: מוטי פרי) (born on October 2, 1949) is an Israeli professor of Economics at University of Warwick, England and emeritus Don Patinkin Professor of Economics at Hebrew University of Jerusalem, Israel.[1]
