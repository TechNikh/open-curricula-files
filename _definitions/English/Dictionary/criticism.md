---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/criticism
offline_file: ""
offline_thumbnail: ""
uuid: bb1e671e-ece2-469d-a783-d5f03c3a6007
updated: 1484310289
title: criticism
categories:
    - Dictionary
---
criticism
     n 1: disapproval expressed by pointing out faults or
          shortcomings; "the senator received severe criticism
          from his opponent" [syn: {unfavorable judgment}]
     2: a serious examination and judgment of something;
        "constructive criticism is always appreciated" [syn: {critique}]
     3: a written evaluation of a work of literature [syn: {literary
        criticism}]
