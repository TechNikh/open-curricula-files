---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/factorize
offline_file: ""
offline_thumbnail: ""
uuid: 91bd9642-798f-45e0-9edf-7b4f620c0cb4
updated: 1484310142
title: factorize
categories:
    - Dictionary
---
factorize
     v : resolve (a polynomial) into factors [syn: {factorise}]
