---
version: 1
type: video
provider: YouTube
id: tvO0358YUYM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/States%20of%20matter%20and%20intermolecular%20forces/Change%20of%20state%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 566d4316-3301-432f-b03b-52fc960d5ed6
updated: 1486069587
title: Change of state example
categories:
    - States of matter and intermolecular forces
thumbnail_urls:
    - https://i3.ytimg.com/vi/tvO0358YUYM/default.jpg
    - https://i3.ytimg.com/vi/tvO0358YUYM/1.jpg
    - https://i3.ytimg.com/vi/tvO0358YUYM/2.jpg
    - https://i3.ytimg.com/vi/tvO0358YUYM/3.jpg
---
Specific heat capacity and enthalpy of vaporization example: calculating how much energy it takes to vaporize 1.00 kg of ethanol starting from 20 degrees C. 

Watch the next lesson: https://www.khanacademy.org/science/chemistry/states-of-matter-and-intermolecular-forces/states-of-matter/v/vapor-pressure?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/states-of-matter-and-intermolecular-forces/states-of-matter/v/chilling-water-problem?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
