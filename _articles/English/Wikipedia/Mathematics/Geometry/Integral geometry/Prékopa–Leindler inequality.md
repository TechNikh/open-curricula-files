---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pr%C3%A9kopa%E2%80%93Leindler_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: be679f5f-e129-4e34-96be-4b886c7a9c00
updated: 1484309257
title: Prékopa–Leindler inequality
tags:
    - Statement of the inequality
    - Essential form of the inequality
    - Relationship to the Brunn–Minkowski inequality
    - Applications in probability and statistics
    - Notes
categories:
    - Integral geometry
---
In mathematics, the Prékopa–Leindler inequality is an integral inequality closely related to the reverse Young's inequality, the Brunn–Minkowski inequality and a number of other important and classical inequalities in analysis. The result is named after the Hungarian mathematicians András Prékopa and László Leindler.
