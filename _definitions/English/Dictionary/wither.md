---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wither
offline_file: ""
offline_thumbnail: ""
uuid: 829cd59f-525b-4e4e-8ab6-7718bda7efcd
updated: 1484310462
title: wither
categories:
    - Dictionary
---
wither
     v 1: wither, especially with a loss of moisture; "The fruit dried
          and shriveled" [syn: {shrivel}, {shrivel up}, {shrink}]
     2: lose freshness, vigor, or vitality; "Her bloom was fading"
        [syn: {fade}]
