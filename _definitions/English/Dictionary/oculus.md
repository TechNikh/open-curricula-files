---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oculus
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484529601
title: oculus
categories:
    - Dictionary
---
oculus
     n : the organ of sight [syn: {eye}, {optic}]
     [also: {oculi} (pl)]
