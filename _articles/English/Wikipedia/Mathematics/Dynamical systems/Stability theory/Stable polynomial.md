---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stable_polynomial
offline_file: ""
offline_thumbnail: ""
uuid: 9cdab8db-4212-4a09-a4d9-36fcc08e7d01
updated: 1484308832
title: Stable polynomial
tags:
    - Properties
    - Examples
categories:
    - Stability theory
---
The first condition provides stability for (or continuous-time) linear systems, and the second case relates to stability of discrete-time linear systems. A polynomial with the first property is called at times a Hurwitz polynomial and with the second property a Schur polynomial. Stable polynomials arise in control theory and in mathematical theory of differential and difference equations. A linear, time-invariant system (see LTI system theory) is said to be BIBO stable if every bounded input produces bounded output. A linear system is BIBO stable if its characteristic polynomial is stable. The denominator is required to be Hurwitz stable if the system is in continuous-time and Schur stable ...
