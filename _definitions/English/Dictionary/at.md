---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/at
offline_file: ""
offline_thumbnail: ""
uuid: aad62f71-9c70-4bdf-b7cb-f72e6a4e07dd
updated: 1484310363
title: at
categories:
    - Dictionary
---
At
     n 1: a highly unstable radioactive element (the heaviest of the
          halogen series); a decay product of uranium and thorium
          [syn: {astatine}, {atomic number 85}]
     2: 100 at equal 1 kip
