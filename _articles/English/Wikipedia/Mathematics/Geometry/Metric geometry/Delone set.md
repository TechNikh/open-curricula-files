---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Delone_set
offline_file: ""
offline_thumbnail: ""
uuid: c8d3126d-1e44-4093-a2ab-a6773a60f6f0
updated: 1484309265
title: Delone set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Metric_epsilon-net.svg.png
tags:
    - Definitions
    - Construction of ε-nets
    - Applications
    - Coding theory
    - Approximation algorithms
    - Crystallography
categories:
    - Metric geometry
---
In the mathematical theory of metric spaces, ε-nets, ε-packings, ε-coverings, uniformly discrete sets, relatively dense sets, and Delone sets (named after Boris Delone) are several closely related definitions of well-spaced sets of points, and the packing radius and covering radius of these sets measure how well-spaced they are. These sets have applications in coding theory, approximation algorithms, and the theory of quasicrystals.
