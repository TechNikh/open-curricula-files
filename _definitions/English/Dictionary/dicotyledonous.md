---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dicotyledonous
offline_file: ""
offline_thumbnail: ""
uuid: c29217f3-6217-414a-873e-c0d04ae764b3
updated: 1484310162
title: dicotyledonous
categories:
    - Dictionary
---
dicotyledonous
     adj : (of a flowering plant) having two cotyledons in the seed
           [ant: {monocotyledonous}]
