---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/explore
offline_file: ""
offline_thumbnail: ""
uuid: 9c8f9b8f-655a-409b-9bb5-d9dc4da9f3a8
updated: 1484310309
title: explore
categories:
    - Dictionary
---
explore
     v 1: inquire into [syn: {research}, {search}]
     2: travel to or penetrate into; "explore unknown territory in
        biology"
     3: examine minutely
     4: examine (organs etc.) for diagnostic purposes
