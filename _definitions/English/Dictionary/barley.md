---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/barley
offline_file: ""
offline_thumbnail: ""
uuid: 58e8128e-df88-49b2-9f39-5cf083063d7b
updated: 1484310424
title: barley
categories:
    - Dictionary
---
barley
     n 1: a grain of barley [syn: {barleycorn}]
     2: cultivated since prehistoric times; grown for forage and
        grain
