---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Incidence_geometry
offline_file: ""
offline_thumbnail: ""
uuid: b566c355-4753-46e8-a9cb-808a9513d97e
updated: 1484309249
title: Incidence geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sample_Incidence.jpg
tags:
    - Incidence structures
    - Distance in an incidence structure
    - Partial linear spaces
    - Fundamental geometric examples
    - Projective planes
    - Fano plane
    - Affine planes
    - Hesse configuration
    - Partial geometries
    - Generalized polygons
    - Near polygons
    - Möbius planes
    - Incidence theorems in the Euclidean plane
    - The Sylvester-Gallai theorem
    - The de Bruijn–Erdős theorem
    - The Szemerédi–Trotter theorem
    - "Beck's theorem"
    - More examples
    - Notes
categories:
    - Incidence geometry
---
In mathematics, incidence geometry is the study of incidence structures. A geometry such as the Euclidean plane is a complicated object that involves concepts such as length, angles, continuity, betweenness, and incidence. An incidence structure is what is obtained when all other concepts are removed and all that remains is the data about which points lie on which lines. Even with this severe limitation, theorems can be proved and interesting facts emerge concerning this structure. Such fundamental results remain valid when additional concepts are added to form a richer geometry. It sometimes happens that authors blur the distinction between a study and the objects of that study, so it is ...
