---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kudremukh
offline_file: ""
offline_thumbnail: ""
uuid: e3578abe-962d-4ae8-817e-df3b906311b8
updated: 1484308438
title: Kudremukh
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kudremukh_hillside.jpg
tags:
    - Location
    - geography
    - History
    - National park
    - Opposition to national park
    - Conservation
    - Ecology
    - Mining town
    - Demographics
    - Stay at Kudremukh
    - Trekking in Kudremukh
    - Hanumana Gundi Waterfalls
    - Threats
    - Gallery
categories:
    - Uncategorized
---
Kuduremukha is a mountain range and name of a peak located in Chikkamagaluru district, in Karnataka, India. It is also the name of a small hill station cum mining town situated near the mountain, about 48 kilometres from Karkala and around 20 kilometres from Kalasa. The name Kuduremukha literally means 'horse-face' (in the Kannada language) and refers to a particular picturesque view of a side of the mountain that resembles a horse's face. It was also referred to as 'Samseparvata', historically since it was approached from Samse village. Kuduremukh is Karnataka's 3rd highest peak after Mullayangiri and Baba Budangiri. The nearest airport is Mangalore International Airport at Mangalore which ...
