---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Damped_sine_wave
offline_file: ""
offline_thumbnail: ""
uuid: 3b125544-3245-4c7f-a41d-047989e7cb3a
updated: 1484308887
title: Damped sine wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-DampedSine.png
tags:
    - Definition
    - Equations
categories:
    - Waves
---
Damped sine waves are commonly seen in science and engineering, wherever a harmonic oscillator is losing energy faster than it is being supplied.
