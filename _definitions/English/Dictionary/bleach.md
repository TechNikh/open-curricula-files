---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bleach
offline_file: ""
offline_thumbnail: ""
uuid: 3721a9a6-569e-420f-9207-58aef6cb5b99
updated: 1484310383
title: bleach
categories:
    - Dictionary
---
bleach
     n 1: the whiteness that results from removing the color from
          something; "a complete bleach usually requires several
          applications"
     2: an agent that makes things white or colorless [syn: {bleaching
        agent}, {blanching agent}, {whitener}]
     3: the act of whitening something by bleaching it (exposing it
        to sunlight or using a chemical bleaching agent)
     v 1: remove color from; "The sun bleached the red shirt" [syn: {bleach
          out}, {decolor}, {decolour}, {decolorize}, {decolourize},
           {decolorise}, {decolourise}, {discolorize}, {discolourise},
           {discolorise}]
     2: make whiter or lighter; "bleach the laundry"
