---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/supplementary
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484317982
title: supplementary
categories:
    - Dictionary
---
supplementary
     adj 1: added to complete or make up a deficiency; "produced
            supplementary volumes"; "additional reading" [syn: {supplemental},
             {additional}]
     2: functioning in a subsidiary or supporting capacity; "the
        main library and its auxiliary branches" [syn: {auxiliary},
         {subsidiary}, {supplemental}]
