---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Windward_and_leeward
offline_file: ""
offline_thumbnail: ""
uuid: 17a06c81-79cd-4f6b-8459-5f45253fdaf5
updated: 1484309304
title: Windward and leeward
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Upwind_downwind_example.png
tags:
    - Pronunciation
    - Origin
    - Meteorological significance
    - Other significance
    - Nautical and naval
    - Notes
categories:
    - Orientation (geometry)
---
Windward is the direction upwind (toward where the wind is coming from) from the point of reference. Leeward is the direction downwind (or downward) from the point of reference. The side of a ship that is towards the leeward is its lee side. If the vessel is heeling under the pressure of the wind, this will be the "lower side". During the age of sail, the term weather was used as a synonym for windward in some contexts, as in the weather gage.
