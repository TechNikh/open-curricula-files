---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mill
offline_file: ""
offline_thumbnail: ""
uuid: 3c9834ea-c009-436c-bfaf-9f958025618f
updated: 1484310313
title: mill
categories:
    - Dictionary
---
mill
     n 1: a plant consisting of buildings with facilities for
          manufacturing [syn: {factory}, {manufacturing plant}, {manufactory}]
     2: Scottish philosopher who expounded Bentham's utilitarianism;
        father of John Stuart Mill (1773-1836) [syn: {James Mill}]
     3: English philosopher and economist remembered for his
        interpretations of empiricism and utilitarianism
        (1806-1873) [syn: {John Mill}, {John Stuart Mill}]
     4: machine that processes materials by grinding or crushing
        [syn: {grinder}]
     5: the act of grinding to a powder or dust [syn: {grind}, {pulverization},
         {pulverisation}]
     v 1: move about in a confused manner ...
