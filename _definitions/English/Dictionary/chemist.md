---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chemist
offline_file: ""
offline_thumbnail: ""
uuid: b08f1c12-3e59-4f0b-b9c6-025971c6024f
updated: 1484310393
title: chemist
categories:
    - Dictionary
---
chemist
     n 1: a scientist who specializes in chemistry
     2: a health professional trained in the art of preparing and
        dispensing drugs [syn: {pharmacist}, {druggist}, {apothecary},
         {pill pusher}, {pill roller}]
