---
version: 1
type: video
provider: YouTube
id: TCsPPU99KRI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Apostrophe%20To%20Show%20Possession%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a6f48c7c-f7e8-4e58-8b5b-b9fd9f239f4f
updated: 1486069632
title: Apostrophe To Show Possession 2
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/TCsPPU99KRI/default.jpg
    - https://i3.ytimg.com/vi/TCsPPU99KRI/1.jpg
    - https://i3.ytimg.com/vi/TCsPPU99KRI/2.jpg
    - https://i3.ytimg.com/vi/TCsPPU99KRI/3.jpg
---
