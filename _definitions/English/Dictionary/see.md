---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/see
offline_file: ""
offline_thumbnail: ""
uuid: 04d18bff-f4dd-4e19-b0c1-1d75a6c8ee50
updated: 1484310344
title: see
categories:
    - Dictionary
---
see
     n : the seat within a bishop's diocese where his cathedral is
         located
     adv : compare (used in texts to point the reader to another
           location in the text) [syn: {cf.}, {cf}, {confer}, {see
           also}]
     v 1: perceive by sight or have the power to perceive by sight;
          "You have to be a good observer to see all the details";
          "Can you see the bird in that tree?"; "He is blind--he
          cannot see"
     2: perceive (an idea or situation) mentally; "Now I see!"; "I
        just can't see your point"; "Does she realize how
        important this decision is?"; "I don't understand the
        idea" [syn: {understand}, {realize}, ...
