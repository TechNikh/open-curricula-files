---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leak
offline_file: ""
offline_thumbnail: ""
uuid: 6b8823f2-8597-4d1d-a517-4dc383d49808
updated: 1484310240
title: leak
categories:
    - Dictionary
---
leak
     n 1: an accidental hole that allows something (fluid or light
          etc.) to enter or escape; "one of the tires developed a
          leak"
     2: soft watery rot in fruits and vegetables caused by fungi
     3: a euphemism for urination; "he had to take a leak" [syn: {wetting},
         {making water}, {passing water}]
     4: the unwanted discharge of a fluid from some container; "they
        tried to stop the escape of gas from the damaged pipe";
        "he had to clean up the leak" [syn: {escape}, {leakage}, {outflow}]
     5: unauthorized (especially deliberate) disclosure of
        confidential information [syn: {news leak}]
     v 1: tell anonymously; "The news were ...
