---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/college
offline_file: ""
offline_thumbnail: ""
uuid: 68492732-7b71-4de8-a596-2e27bb6a1fa9
updated: 1484310431
title: college
categories:
    - Dictionary
---
college
     n 1: the body of faculty and students of a college
     2: an institution of higher education created to educate and
        grant degrees; often a part of a university
     3: British slang for prison
     4: a complex of buildings in which a college is housed
