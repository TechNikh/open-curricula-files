---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flat
offline_file: ""
offline_thumbnail: ""
uuid: 35cc61ff-e6f9-4777-b703-e1f8a2859abd
updated: 1484310341
title: flat
categories:
    - Dictionary
---
flat
     adj 1: having a horizontal surface in which no part is higher or
            lower than another; "a flat desk"; "acres of level
            farmland"; "a plane surface" [syn: {level}, {plane}]
     2: having no depth or thickness
     3: not modified or restricted by reservations; "a categorical
        denial"; "a flat refusal" [syn: {categoric}, {categorical},
         {unconditional}]
     4: stretched out and lying at full length along the ground;
        "found himself lying flat on the floor" [syn: {prostrate}]
     5: lacking contrast or shading between tones [ant: {contrasty}]
     6: lowered in pitch by one chromatic semitone; "B flat" [ant: {natural},
         {sharp}]
  ...
