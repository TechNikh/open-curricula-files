---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Internal_pressure
offline_file: ""
offline_thumbnail: ""
uuid: 1b001315-3e62-4ac5-b799-c0c74ba2c429
updated: 1484308375
title: Internal pressure
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Internal_pressure_gases.png
tags:
    - Thermodynamic equation of state
    - Perfect gas
    - Real gases
    - The Joule experiment
categories:
    - Uncategorized
---
Internal pressure is a measure of how the internal energy of a system changes when it expands or contracts at constant temperature. It has the same dimensions as pressure, the SI unit of which is the pascal.
