---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strict_differentiability
offline_file: ""
offline_thumbnail: ""
uuid: 84bfdc26-a482-4e41-8572-394c09529ea6
updated: 1484308654
title: Strict differentiability
tags:
    - Basic definition
    - Motivation from p-adic analysis
    - Definition in p-adic case
categories:
    - Number theory
---
In mathematics, strict differentiability is a modification of the usual notion of differentiability of functions that is particularly suited to p-adic analysis. In short, the definition is made more restrictive by allowing both points used in the difference quotient to "move".
