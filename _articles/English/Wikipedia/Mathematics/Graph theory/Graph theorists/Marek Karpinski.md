---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Marek_Karpinski
offline_file: ""
offline_thumbnail: ""
uuid: 62b65ebd-0074-4f39-8cba-34eeb200ba92
updated: 1484309466
title: Marek Karpinski
categories:
    - Graph theorists
---
Marek Karpinski[1] is a computer scientist and mathematician known for his research in the theory of algorithms and their applications, combinatorial optimization, computational complexity, and mathematical foundations. He is a recipient of several research prizes in the above areas.
