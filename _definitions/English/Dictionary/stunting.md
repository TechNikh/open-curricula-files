---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stunting
offline_file: ""
offline_thumbnail: ""
uuid: 018b1b55-345e-41f5-879a-e77f96a7f5a1
updated: 1484310535
title: stunting
categories:
    - Dictionary
---
stunting
     n : the performance of stunts while in flight in an aircraft
         [syn: {acrobatics}, {aerobatics}, {stunt flying}]
