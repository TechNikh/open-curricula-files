---
version: 1
type: video
provider: YouTube
id: Zoa485PqK_M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Applying%20Quadratic%20Functions%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6dcf4066-ce7f-4f5d-a1db-db99aeff7e15
updated: 1486069557
title: Applying Quadratic Functions 1
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/Zoa485PqK_M/default.jpg
    - https://i3.ytimg.com/vi/Zoa485PqK_M/1.jpg
    - https://i3.ytimg.com/vi/Zoa485PqK_M/2.jpg
    - https://i3.ytimg.com/vi/Zoa485PqK_M/3.jpg
---
