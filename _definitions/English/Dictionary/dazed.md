---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dazed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484642762
title: dazed
categories:
    - Dictionary
---
dazed
     adj 1: in a state of mental numbness especially as resulting from
            shock; "he had a dazed expression on his face"; "lay
            semiconscious, stunned (or stupefied) by the blow";
            "was stupid from fatigue" [syn: {stunned}, {stupefied},
             {stupid(p)}]
     2: stunned or confused and slow to react (as from blows or
        drunkenness or exhaustion) [syn: {foggy}, {groggy}, {logy},
         {stuporous}]
