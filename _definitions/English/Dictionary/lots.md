---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lots
offline_file: ""
offline_thumbnail: ""
uuid: 125166e7-0978-4b8d-a5e0-619681d0a1fe
updated: 1484310152
title: lots
categories:
    - Dictionary
---
lots
     n : a large number or amount; "made lots of new friends"; "she
         amassed a mountain of newspapers" [syn: {tons}, {dozens},
          {heaps}, {mountain}, {piles}, {scores}, {stacks}, {loads},
          {rafts}, {slews}, {wads}, {oodles}, {gobs}, {scads}, {lashings}]
