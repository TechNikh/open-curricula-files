---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/redistribute
offline_file: ""
offline_thumbnail: ""
uuid: 1801a12c-7ae0-4d30-9164-292e6061db44
updated: 1484310407
title: redistribute
categories:
    - Dictionary
---
redistribute
     v : distribute anew; "redistribute the troops more
         strategically"
