---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exotic_affine_space
offline_file: ""
offline_thumbnail: ""
uuid: c5b56e82-71f9-466c-a1f3-636bfd393e93
updated: 1484309362
title: Exotic affine space
categories:
    - Geometry stubs
---
In algebraic geometry, an exotic affine space is a complex algebraic variety that is diffeomorphic to 
  
    
      
        
          
            R
          
          
            2
            n
          
        
      
    
    {\displaystyle \mathbb {R} ^{2n}}
  
 for some n, but is not isomorphic as an algebraic variety to 
  
    
      
        
          
            C
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {C} ^{n}}
  
.[1][2][3] An example of an exotic 
  
    
      
        
          
            C
          
          
            3
          
        
      
    
    {\displaystyle \mathbb {C} ^{3}}
  
 is the ...
