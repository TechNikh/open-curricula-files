---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hereditary
offline_file: ""
offline_thumbnail: ""
uuid: 0f2065ec-9088-4971-b22f-896ce84ee49e
updated: 1484310183
title: hereditary
categories:
    - Dictionary
---
hereditary
     adj 1: tending to occur among members of a family usually by
            heredity; "an inherited disease"; "familial traits";
            "genetically transmitted features" [syn: {familial}, {genetic},
             {inherited}, {transmitted}, {transmissible}]
     2: inherited or inheritable by established rules (usually legal
        rules) of descent; "ancestral home"; "ancestral lore";
        "hereditary monarchy"; "patrimonial estate";
        "transmissible tradition" [syn: {ancestral}, {patrimonial},
         {transmissible}]
