---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hafnian
offline_file: ""
offline_thumbnail: ""
uuid: 279e0b82-e272-47ad-9460-df1f274f8fe2
updated: 1484308588
title: Hafnian
categories:
    - Combinatorics
---
In mathematics, the hafnian of an adjacency matrix of a graph is the number of perfect matchings in the graph. It was so named by Eduardo R. Caianiello "to mark the fruitful period of stay in Copenhagen (Hafnia in Latin)."[1]
