---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Probabilistic_bisimulation
offline_file: ""
offline_thumbnail: ""
uuid: ed95ed5c-2e66-440b-bcac-edfc9381d42f
updated: 1484308683
title: Probabilistic bisimulation
categories:
    - Theoretical computer science
---
In theoretical computer science, probabilistic bisimulation is an extension of the concept of bisimulation for fully probabilistic transition systems first described by K.G. Larsen and A. Skou.[1]
