---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unchanging
offline_file: ""
offline_thumbnail: ""
uuid: dd9bb5e5-b03e-4a42-ad9a-5bb5b404d78d
updated: 1484310531
title: unchanging
categories:
    - Dictionary
---
unchanging
     adj 1: not active or moving; "a static village community and a
            completely undynamic type of agriculture"; "static
            feudal societies" [syn: {static}]
     2: conforming to the same principles or course of action over
        time
     3: showing little if any change; "a static population" [syn: {static},
         {stable}]
