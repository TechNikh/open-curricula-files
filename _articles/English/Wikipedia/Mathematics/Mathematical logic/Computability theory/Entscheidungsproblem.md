---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Entscheidungsproblem
offline_file: ""
offline_thumbnail: ""
uuid: b16238f1-44b7-42ec-b1c3-e76b98e00400
updated: 1484309485
title: Entscheidungsproblem
tags:
    - History of the problem
    - Negative answer
    - Practical decision procedures
    - Notes
categories:
    - Computability theory
---
In mathematics and computer science, the Entscheidungsproblem (pronounced [ɛntˈʃaɪ̯dʊŋspʁoˌbleːm], German for 'decision problem') is a challenge posed by David Hilbert in 1928.[1] The Entscheidungsproblem asks for an algorithm that takes as input a statement of a first-order logic (possibly with a finite number of axioms beyond the usual axioms of first-order logic) and answers "Yes" or "No" according to whether the statement is universally valid, i.e., valid in every structure satisfying the axioms. By the completeness theorem of first-order logic, a statement is universally valid if and only if it can be deduced from the axioms, so the Entscheidungsproblem can also be viewed as ...
