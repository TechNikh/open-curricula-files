---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/applied
offline_file: ""
offline_thumbnail: ""
uuid: a576c680-c581-4241-809c-cb5e8bd18eaf
updated: 1484310299
title: applied
categories:
    - Dictionary
---
applied
     adj 1: that are used; "an isotropic resonance shift...to lower
            applied fields"
     2: concerned with concrete problems or data rather than with
        fundamental principles; opposed to theoretical; "applied
        physics"; "applied psychology"; "technical problems in
        medicine, engineering, economics and other applied
        disciplines"- Sidney Hook [ant: {theoretical}]
     3: put into practice or put to use; "applied physics"
