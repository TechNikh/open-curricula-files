---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/towel
offline_file: ""
offline_thumbnail: ""
uuid: fc41c0d5-dcdc-45eb-8cff-d89eed2048e1
updated: 1484310226
title: towel
categories:
    - Dictionary
---
towel
     n : a rectangular piece of absorbent cloth (or paper) for drying
         or wiping
     v : wipe with a towel; "towel your hair dry"
     [also: {towelling}, {towelled}]
