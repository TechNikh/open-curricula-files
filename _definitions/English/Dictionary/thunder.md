---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/thunder
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484476621
title: thunder
categories:
    - Dictionary
---
thunder
     n 1: a deep prolonged loud noise [syn: {boom}, {roar}, {roaring}]
     2: a booming or crashing noise caused by air expanding along
        the path of a bolt of lightning
     3: street names for heroin [syn: {big H}, {hell dust}, {nose
        drops}, {smack}]
     v 1: move fast, noisily, and heavily; "The bus thundered down the
          road"
     2: utter words loudly and forcefully; "`Get out of here,' he
        roared" [syn: {roar}]
     3: be the case that thunder is being heard; "Whenever it
        thunders, my dog crawls under the bed" [syn: {boom}]
     4: to make or produce a loud noise; "The river thundered
        below"; "The engine roared as the driver pushed ...
