---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tartar
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484513521
title: tartar
categories:
    - Dictionary
---
tartar
     n 1: a salt used especially in baking powder [syn: {cream of
          tartar}, {potassium hydrogen tartrate}]
     2: a fiercely vigilant and unpleasant woman [syn: {dragon}]
     3: a member of the Mongolian people of central Asia who invaded
        Russia in the 13th century [syn: {Tatar}, {Mongol Tatar}]
     4: an incrustation that forms on the teeth and gums [syn: {calculus},
         {tophus}]
