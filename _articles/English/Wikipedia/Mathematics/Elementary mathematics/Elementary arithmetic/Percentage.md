---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Percentage
offline_file: ""
offline_thumbnail: ""
uuid: ca99ede2-8459-4d1a-8aa4-bdc8f9aa0add
updated: 1484308915
title: Percentage
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Web-browser_usage_on_Wikimedia.svg.png
tags:
    - Examples
    - History
    - Percent sign
    - Calculations
    - Percentage increase and decrease
    - Compounding percentages
    - Word and symbol
    - Other uses
    - Related units
    - Practical applications
categories:
    - Elementary arithmetic
---
In mathematics, a percentage is a number or ratio expressed as a fraction of 100. It is often denoted using the percent sign, "%", or the abbreviations "pct.", "pct"; sometimes the abbreviation "pc" is also used.[1] A percentage is a dimensionless number (pure number).
