---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/costly
offline_file: ""
offline_thumbnail: ""
uuid: dee3dea4-c66f-4b93-9095-baf13bb46db8
updated: 1484310577
title: costly
categories:
    - Dictionary
---
costly
     adj 1: entailing great loss or sacrifice; "a dearly-won victory"
            [syn: {dearly-won}]
     2: having a high price; "costly jewelry"; "high-priced
        merchandise"; "much too dear for my pocketbook"; "a pricey
        restaurant" [syn: {dear(p)}, {high-priced}, {pricey}, {pricy}]
     [also: {costliest}, {costlier}]
