---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recast
offline_file: ""
offline_thumbnail: ""
uuid: ebbf07a3-d2cf-44da-a061-4231a934d421
updated: 1484310196
title: recast
categories:
    - Dictionary
---
recast
     v 1: cast again, in a different role; "He was recast as Iago"
     2: cast again; "The bell cracked and had to be recast" [syn: {remold},
         {remould}]
     3: cast or model anew; "She had to recast her image to please
        the electorate in her home state" [syn: {reforge}, {remodel}]
