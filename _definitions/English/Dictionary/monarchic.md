---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/monarchic
offline_file: ""
offline_thumbnail: ""
uuid: 3d9d5201-afec-4510-a013-55b6c579eb9a
updated: 1484310569
title: monarchic
categories:
    - Dictionary
---
monarchic
     adj 1: of or relating to or befitting a monarch or monarchy;
            "monarchal (or monarchical) government"; "a country
            that was monarchial in tradition"; "reconciled to
            monarchic rule"; "monarchical systems" [syn: {monarchal},
             {monarchial}, {monarchical}]
     2: ruled by or having the supreme power resting with a monarch;
        "monarchal government"; "monarchical systems" [syn: {monarchal},
         {monarchical}]
