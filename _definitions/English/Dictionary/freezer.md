---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/freezer
offline_file: ""
offline_thumbnail: ""
uuid: aa210fb6-a39b-4912-aae5-5b1d1c80e5b9
updated: 1484310224
title: freezer
categories:
    - Dictionary
---
freezer
     n : electric refrigerator (trade name Deepfreeze) in which food
         is frozen and stored for long periods of time [syn: {deep-freeze},
          {Deepfreeze}, {deep freezer}]
