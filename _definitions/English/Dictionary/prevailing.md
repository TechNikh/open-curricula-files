---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prevailing
offline_file: ""
offline_thumbnail: ""
uuid: f1e5de23-c7d0-450e-a01a-458ad009ea84
updated: 1484310246
title: prevailing
categories:
    - Dictionary
---
prevailing
     adj 1: most frequent or common; "prevailing winds" [syn: {predominant}]
     2: encountered generally especially at the present time; "the
        prevailing opinion was that a trade war could be averted";
        "the most prevalent religion in our area"; "speculation
        concerning the books author was rife" [syn: {prevalent}, {rife}]
