---
version: 1
type: video
provider: YouTube
id: tdO4UOLW9d8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Simplifying%20expressions%20with%20exponents.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 141c6669-3d33-4633-8030-bc1a38565955
updated: 1486069563
title: Simplifying expressions with exponents
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/tdO4UOLW9d8/default.jpg
    - https://i3.ytimg.com/vi/tdO4UOLW9d8/1.jpg
    - https://i3.ytimg.com/vi/tdO4UOLW9d8/2.jpg
    - https://i3.ytimg.com/vi/tdO4UOLW9d8/3.jpg
---
