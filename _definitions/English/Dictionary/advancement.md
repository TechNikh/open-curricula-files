---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advancement
offline_file: ""
offline_thumbnail: ""
uuid: 26379a7c-5876-4628-ad5e-f5682f65ad1b
updated: 1484310431
title: advancement
categories:
    - Dictionary
---
advancement
     n 1: encouragement of the progress or growth or acceptance of
          something [syn: {promotion}, {furtherance}]
     2: the act of moving forward toward a goal [syn: {progress}, {progression},
         {procession}, {advance}, {forward motion}, {onward motion}]
     3: gradual improvement or growth or development; "advancement
        of knowledge"; "great progress in the arts" [syn: {progress}]
