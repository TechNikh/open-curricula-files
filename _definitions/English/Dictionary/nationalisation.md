---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nationalisation
offline_file: ""
offline_thumbnail: ""
uuid: 66206120-3c61-4f94-a33b-9be6cc061066
updated: 1484310559
title: nationalisation
categories:
    - Dictionary
---
nationalisation
     n 1: the action of forming or becoming a nation [syn: {nationalization}]
     2: the action of rendering national in character [syn: {nationalization}]
     3: changing something from private to state ownership or
        control [syn: {nationalization}, {communization}, {communisation}]
        [ant: {denationalization}]
