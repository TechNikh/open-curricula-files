---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jenga
offline_file: ""
offline_thumbnail: ""
uuid: ea46a0b8-1542-41eb-b56d-7ff5c75d6455
updated: 1484308535
title: Jenga
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Jenga.JPG
tags:
    - Rules
    - Origins
    - Tallest tower
    - Official variants
    - Video games
    - Ta-Ka-Radi
    - Notes
categories:
    - Combinatorial game theory
---
Jenga is a game of physical and mental skill created by Leslie Scott, and currently marketed by Parker Brothers, a division of Hasbro. During the game, players take turns removing one block at a time from a tower constructed of 54 blocks. Each block removed is then balanced on top of the tower, creating a progressively taller but less stable structure.
