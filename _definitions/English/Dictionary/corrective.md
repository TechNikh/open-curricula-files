---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/corrective
offline_file: ""
offline_thumbnail: ""
uuid: 12001ecc-90c8-485c-9652-6ae1256c1aa9
updated: 1484310603
title: corrective
categories:
    - Dictionary
---
corrective
     adj 1: designed to promote discipline; "the teacher's action was
            corrective rather than instructional"; "disciplinal
            measures"; "the mother was stern and disciplinary"
            [syn: {disciplinary}, {disciplinal}]
     2: tending or intended to correct or counteract or restore to a
        normal condition; "corrective measures"; "corrective
        lenses"
     n : a device for treating injury or disease [syn: {restorative}]
