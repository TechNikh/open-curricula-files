---
version: 1
type: video
provider: YouTube
id: DrDiqVhaghw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 27a45d5f-0dbf-4a01-bedf-384ef3c15689
updated: 1486069644
title: Exploring Mandelbulb. 3D Fractal Animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/DrDiqVhaghw/default.jpg
    - https://i3.ytimg.com/vi/DrDiqVhaghw/1.jpg
    - https://i3.ytimg.com/vi/DrDiqVhaghw/2.jpg
    - https://i3.ytimg.com/vi/DrDiqVhaghw/3.jpg
---
