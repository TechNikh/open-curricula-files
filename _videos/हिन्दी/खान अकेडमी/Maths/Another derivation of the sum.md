---
version: 1
type: video
provider: YouTube
id: nglI_t7K8UY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 319c0d6f-da24-4fdf-9d1f-6f3a573343fc
updated: 1486069605
title: Another derivation of the sum
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/nglI_t7K8UY/default.jpg
    - https://i3.ytimg.com/vi/nglI_t7K8UY/1.jpg
    - https://i3.ytimg.com/vi/nglI_t7K8UY/2.jpg
    - https://i3.ytimg.com/vi/nglI_t7K8UY/3.jpg
---
