---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tibetan
offline_file: ""
offline_thumbnail: ""
uuid: 45dbc2aa-4ebf-4d48-88b2-7e7d2ecbe683
updated: 1484310433
title: tibetan
categories:
    - Dictionary
---
Tibetan
     adj : of or relating to or characteristic of Tibet or its people
           or their language; "Tibetan monks"
     n 1: Himalayish language spoken in Tibet
     2: a native or inhabitant of Tibet
