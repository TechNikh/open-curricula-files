---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/testis
offline_file: ""
offline_thumbnail: ""
uuid: f3cea2cc-9a0a-4465-bfe9-22d302ee3302
updated: 1484310162
title: testis
categories:
    - Dictionary
---
testis
     n : one of the two male reproductive glands that produce
         spermatozoa and secrete androgens; "she kicked him in the
         balls and got away" [syn: {testicle}, {orchis}, {ball}, {ballock},
          {bollock}, {nut}, {egg}]
     [also: {testes} (pl)]
