---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Distance_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 03afb983-0bbe-4d77-b480-b555c5a4eb79
updated: 1484309265
title: Distance matrix
tags:
    - Metric distance
    - Non-metric distance
    - Applications
    - Hierarchical Clustering
    - Phylogenetic Analysis
    - Other uses
    - Examples
categories:
    - Metric geometry
---
In mathematics, computer science and especially graph theory, a distance matrix is a matrix (two-dimensional array) containing the distances, taken pairwise, between the elements of a set. Depending upon the application involved, the distance being used to define this matrix may or may not be a metric. If there are N elements, this matrix will have size N×N. In graph-theoretic applications the elements are more often referred to as points, nodes or vertices.
