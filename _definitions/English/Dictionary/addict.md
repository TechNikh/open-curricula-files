---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/addict
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484604661
title: addict
categories:
    - Dictionary
---
addict
     n 1: someone who is so ardently devoted to something that it
          resembles an addiction; "a golf addict"; "a car nut"; "a
          news junkie" [syn: {nut}, {freak}, {junkie}, {junky}]
     2: someone who is physiologically dependent on a substance;
        abrupt deprivation of the substance produces withdrawal
        symptoms
     v : to cause (someone or oneself) to become dependent (on
         something, especially a narcotic drug) [syn: {hook}]
