---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dual_abelian_variety
offline_file: ""
offline_thumbnail: ""
uuid: 8ccaccd6-88ab-4d6f-bdb1-a4979b60653a
updated: 1484309232
title: Dual abelian variety
tags:
    - Definition
    - History
    - Dual isogeny (elliptic curve case)
    - Construction of the dual isogeny
    - Poincaré line bundle
    - Notes
categories:
    - Duality theories
---
