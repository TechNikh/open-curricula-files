---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-assembly
offline_file: ""
offline_thumbnail: ""
uuid: 1446ee54-925c-44d9-87d3-57af0af59f4d
updated: 1484308828
title: Self-assembly
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/260px-Iron_oxide_nanocube.jpg
tags:
    - Self-assembly in chemistry and materials science
    - Distinctive features
    - Order
    - Interactions
    - Building blocks
    - Examples
    - Properties
    - Self-assembly at the macroscopic scale
    - Consistent concepts of self-organization and self-assembly
    - External links and further reading
categories:
    - Self-organization
---
Self-assembly is a process in which a disordered system of pre-existing components forms an organized structure or pattern as a consequence of specific, local interactions among the components themselves, without external direction. When the constitutive components are molecules, the process is termed molecular self-assembly.
