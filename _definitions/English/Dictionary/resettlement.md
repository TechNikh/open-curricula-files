---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resettlement
offline_file: ""
offline_thumbnail: ""
uuid: e63063dd-790b-4b46-894d-c650898f73a9
updated: 1484310475
title: resettlement
categories:
    - Dictionary
---
resettlement
     n : the transportation of people (as a family or colony) to a
         new settlement (as after an upheaval of some kind) [syn:
         {transplantation}, {relocation}]
