---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emanate
offline_file: ""
offline_thumbnail: ""
uuid: 5c1e90d3-de47-475e-b65c-6c091a606a7a
updated: 1484310208
title: emanate
categories:
    - Dictionary
---
emanate
     v 1: proceed or issue forth, as from a source; "Water emanates
          from this hole in the ground"
     2: give out (breath or an odor); "The chimney exhales a thick
        smoke" [syn: {exhale}, {give forth}]
