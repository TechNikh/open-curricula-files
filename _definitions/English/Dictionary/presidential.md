---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/presidential
offline_file: ""
offline_thumbnail: ""
uuid: 313cf783-47a9-4625-a6f8-294d711f7cc0
updated: 1484310579
title: presidential
categories:
    - Dictionary
---
presidential
     adj 1: relating to a president or presidency; "presidential aides";
            "presidential veto"
     2: befitting a president; "criticized the candidate for not
        looking presidential" [ant: {unpresidential}]
