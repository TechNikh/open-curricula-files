---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/country
offline_file: ""
offline_thumbnail: ""
uuid: 33aabfe3-3a48-4afb-a497-9dd067fd6a4c
updated: 1484310271
title: country
categories:
    - Dictionary
---
country
     n 1: the territory occupied by a nation; "he returned to the land
          of his birth"; "he visited several European countries"
          [syn: {state}, {land}]
     2: a politically organized body of people under a single
        government; "the state has elected a new president";
        "African nations"; "students who had come to the nation's
        capitol"; "the country's largest manufacturer"; "an
        industrialized land" [syn: {state}, {nation}, {land}, {commonwealth},
         {res publica}, {body politic}]
     3: the people who live in a nation or country; "a statement
        that sums up the nation's mood"; "the news was announced
        to the nation"; ...
