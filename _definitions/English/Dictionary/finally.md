---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/finally
offline_file: ""
offline_thumbnail: ""
uuid: d8d1a12a-8da2-4dde-adf3-bd4e6155bd5b
updated: 1484310339
title: finally
categories:
    - Dictionary
---
finally
     adv 1: after a long period of time or an especially long delay; "at
            length they arrived" [syn: {eventually}, {at length}]
     2: the item at the end; "last, I'll discuss family values"
        [syn: {last}, {lastly}, {in conclusion}]
     3: as the end result of a succession or process; "ultimately he
        had to give in"; "at long last the winter was over" [syn:
        {ultimately}, {in the end}, {at last}, {at long last}]
