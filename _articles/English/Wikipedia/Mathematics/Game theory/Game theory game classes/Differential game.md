---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Differential_game
offline_file: ""
offline_thumbnail: ""
uuid: 84bd3f17-c4e0-4b2e-b20b-5dd9c7491be5
updated: 1484309032
title: Differential game
tags:
    - Connection to optimal control
    - History
    - Random time horizon
    - Applications
    - Notes
    - Textbooks and general references
categories:
    - Game theory game classes
---
In game theory, differential games are a group of problems related to the modeling and analysis of conflict in the context of a dynamical system. More specifically, a state variable or variables evolve over time according to a differential equation. Early analyses reflected military interests, considering two actors - the pursuer and the evader - with diametrically opposed goals. More recent analyses have reflected engineering or economic considerations.
