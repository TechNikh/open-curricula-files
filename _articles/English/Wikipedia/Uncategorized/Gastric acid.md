---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gastric_juice
offline_file: ""
offline_thumbnail: ""
uuid: 438cf53b-f165-4b73-b9b0-b74de6768c99
updated: 1484308358
title: Gastric acid
tags:
    - Secretion
    - Regulation of secretion
    - Neutralization
    - Role in disease
    - Pharmacology
    - History
    - Notes
categories:
    - Uncategorized
---
Gastric acid, gastric juice or stomach acid, is a digestive fluid, formed in the stomach and is composed of hydrochloric acid (HCl) .05–0.1 M (roughly 5,000–10,000 parts per million or 0.5-1%)[1] potassium chloride (KCl) and sodium chloride (NaCl). The acid plays a key role in digestion of proteins, by activating digestive enzymes, and making ingested proteins unravel so that digestive enzymes break down the long chains of amino acids. Gastric acid is produced by cells in the lining of the stomach, which are coupled in feedback systems to increase acid production when needed. Other cells in the stomach produce bicarbonate, a base, to buffer the fluid, ensuring that it does not become ...
