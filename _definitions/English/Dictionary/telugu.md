---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/telugu
offline_file: ""
offline_thumbnail: ""
uuid: 5863f6d7-52fd-41ca-88e7-67ce18d5c95d
updated: 1484310605
title: telugu
categories:
    - Dictionary
---
Telugu
     n 1: a member of the people in southeastern India (Andhra
          Pradesh) who speak the Telugu language
     2: a Dravidian language spoken by the Telugu people in
        southeastern India
