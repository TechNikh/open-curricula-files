---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inner
offline_file: ""
offline_thumbnail: ""
uuid: 70053124-8e6c-4fa9-8654-ed1dfc4ebba0
updated: 1484310335
title: inner
categories:
    - Dictionary
---
inner
     adj 1: located inward; "Beethoven's manuscript looks like a bloody
            record of a tremendous inner battle"- Leonard
            Bernstein; "she thinks she has no soul, no interior
            life, but the truth is that she has no access to it"-
            David Denby; "an internal sense of rightousness"-
            A.R.Gurney,Jr. [syn: {interior}, {internal}]
     2: located or occurring within or closer to a center; "an inner
        room" [syn: {inner(a)}] [ant: {outer(a)}]
     3: innermost or essential; "the inner logic of Cubism"; "the
        internal contradictions of the theory"; "the intimate
        structure of matter" [syn: {internal}, {intimate}]
     4: ...
