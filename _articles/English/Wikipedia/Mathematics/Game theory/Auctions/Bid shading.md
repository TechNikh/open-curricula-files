---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bid_shading
offline_file: ""
offline_thumbnail: ""
uuid: c59a2606-c95a-4978-8a8a-bb666c168792
updated: 1484309010
title: Bid shading
categories:
    - Auctions
---
Bid shading is used for one of two purposes. In a common value auction with incomplete information, bid shading is used to compensate for the winner's curse. In such auctions, the good is worth the same amount to all bidders, but bidders don't know the value of the good and must independently estimate it. Since all bidders value the good equally, the winner will generally be the bidder whose estimate of the value is largest. But if we assume that in general bidders estimate the value accurately, then the highest bidder has overestimated the good's value and will end up paying more than it is worth. In other words, winning the auction carries bad news about a bidder's value estimate. A savvy ...
