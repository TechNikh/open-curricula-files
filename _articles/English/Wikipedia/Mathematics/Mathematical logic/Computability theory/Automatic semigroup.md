---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Automatic_semigroup
offline_file: ""
offline_thumbnail: ""
uuid: 5d31260e-4ae4-4a52-b761-4a6810f0efdc
updated: 1484309481
title: Automatic semigroup
tags:
    - Decision problems
    - Geometric characterization
    - Examples of automatic semigroups
categories:
    - Computability theory
---
In mathematics, an automatic semigroup is a finitely generated semigroup equipped with several regular languages over an alphabet representing a generating set. One of these languages determines "canonical forms" for the elements of the semigroup, the other languages determine if two canonical forms represent elements that differ by multiplication by a generator.
