---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/storage
offline_file: ""
offline_thumbnail: ""
uuid: 71340efa-348b-4c0c-90e4-9cd0ee509729
updated: 1484310319
title: storage
categories:
    - Dictionary
---
storage
     n 1: the act of storing something
     2: a depository for goods; "storehouses were built close to the
        docks" [syn: {storehouse}, {depot}, {entrepot}, {store}]
     3: the commercial enterprise of storing goods and materials
     4: (computer science) the process of storing information in a
        computer memory or on a magnetic tape or disk
     5: an electronic memory device; "a memory and the CPU form the
        central part of a computer to which peripherals are
        attached" [syn: {memory}, {computer memory}, {computer
        storage}, {store}, {memory board}]
     6: depositing in a warehouse [syn: {repositing}, {reposition},
        {warehousing}]
