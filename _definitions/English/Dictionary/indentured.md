---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/indentured
offline_file: ""
offline_thumbnail: ""
uuid: 280915ff-796b-42d8-934d-c1d1a2e12616
updated: 1484310525
title: indentured
categories:
    - Dictionary
---
indentured
     adj : bound by contract [syn: {apprenticed}, {articled}, {bound}]
