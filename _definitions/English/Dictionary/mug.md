---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mug
offline_file: ""
offline_thumbnail: ""
uuid: e5518b42-bd74-44a6-90f2-a9355d034d5a
updated: 1484310212
title: mug
categories:
    - Dictionary
---
mug
     n 1: the quantity that can be held in a mug [syn: {mugful}]
     2: a person who is gullible and easy to take advantage of [syn:
         {chump}, {fool}, {gull}, {mark}, {patsy}, {fall guy}, {sucker},
         {soft touch}]
     3: the human face (`kisser' and `smiler' and `mug' are informal
        terms for `face' and `phiz' is British) [syn: {countenance},
         {physiognomy}, {phiz}, {visage}, {kisser}, {smiler}]
     4: with handle and usually cylindrical
     v : rob at gunpoint or with the threat of violence; "I was
         mugged in the streets of New York last night"
     [also: {mugging}, {mugged}]
