---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prone
offline_file: ""
offline_thumbnail: ""
uuid: fba3c691-496c-44c1-8ebe-2079a11e883a
updated: 1484310459
title: prone
categories:
    - Dictionary
---
prone
     adj 1: lying face downward [syn: {prostrate}]
     2: having a tendency (to); often used in combination; "a child
        prone to mischief"; "failure-prone"
