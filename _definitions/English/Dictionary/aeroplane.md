---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aeroplane
offline_file: ""
offline_thumbnail: ""
uuid: ccfaa7db-d70d-4893-ac3b-18ed1f031ca9
updated: 1484310156
title: aeroplane
categories:
    - Dictionary
---
aeroplane
     n : an aircraft that has a fixed wing and is powered by
         propellers or jets; "the flight was delayed due to
         trouble with the airplane" [syn: {airplane}, {plane}]
