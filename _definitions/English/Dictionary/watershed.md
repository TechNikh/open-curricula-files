---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/watershed
offline_file: ""
offline_thumbnail: ""
uuid: b60d52f5-c85b-4b93-a66a-9f240a53a7fd
updated: 1484310258
title: watershed
categories:
    - Dictionary
---
watershed
     n 1: a ridge of land that separates two adjacent river systems
          [syn: {water parting}, {divide}]
     2: an event marking a unique or important historical change of
        course or one on which important developments depend; "the
        agreement was a watershed in the history of both nations"
        [syn: {landmark}, {turning point}]
