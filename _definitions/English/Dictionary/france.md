---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/france
offline_file: ""
offline_thumbnail: ""
uuid: 1a5b38eb-a7bf-4bdb-852f-413509eb783f
updated: 1484310529
title: france
categories:
    - Dictionary
---
France
     n 1: a republic in western Europe; the largest country wholly in
          Europe [syn: {French Republic}]
     2: French writer of sophisticated novels and short stories
        (1844-1924) [syn: {Anatole France}, {Jacques Anatole
        Francois Thibault}]
