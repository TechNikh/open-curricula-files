---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/livelihood
offline_file: ""
offline_thumbnail: ""
uuid: 1d9c95b8-c96f-4bac-9343-055f27075b4a
updated: 1484310260
title: livelihood
categories:
    - Dictionary
---
livelihood
     n : the financial means whereby one lives; "each child was
         expected to pay for their keep"; "he applied to the state
         for support"; "he could no longer earn his own
         livelihood" [syn: {support}, {keep}, {living}, {bread and
         butter}, {sustenance}]
