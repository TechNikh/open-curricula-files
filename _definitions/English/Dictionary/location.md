---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/location
offline_file: ""
offline_thumbnail: ""
uuid: 71304f26-a508-4a93-aaae-0fd8598b190e
updated: 1484310401
title: location
categories:
    - Dictionary
---
location
     n 1: a point or extent in space
     2: the act of putting something in a certain place or location
        [syn: {placement}, {locating}, {position}, {positioning},
        {emplacement}]
     3: a determination of the location of something; "he got a good
        fix on the target" [syn: {localization}, {localisation}, {locating},
         {fix}]
