---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lux
offline_file: ""
offline_thumbnail: ""
uuid: f6d44a6d-eb4a-4ad4-a05d-d2cbe45ff2f7
updated: 1484310140
title: lux
categories:
    - Dictionary
---
lux
     n : a unit of illumination equal to 1 lumen per square meter;
         0.0929 foot candle [syn: {lx}]
