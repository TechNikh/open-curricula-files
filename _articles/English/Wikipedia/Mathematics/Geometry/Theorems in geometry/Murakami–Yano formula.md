---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Murakami%E2%80%93Yano_formula'
offline_file: ""
offline_thumbnail: ""
uuid: f64b97fa-cd43-4bac-8d54-df8d7fb260eb
updated: 1484309117
title: Murakami–Yano formula
categories:
    - Theorems in geometry
---
In geometry, the Murakami–Yano formula, introduced by Murakami & Yano (2005), is a formula for the volume of a hyperbolic or spherical tetrahedron given in terms of its dihedral angles.
