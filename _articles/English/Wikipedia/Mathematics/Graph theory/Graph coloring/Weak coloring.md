---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Weak_coloring
offline_file: ""
offline_thumbnail: ""
uuid: 9fb0d759-f842-4aed-bcd6-a2146a00d3cc
updated: 1484309389
title: Weak coloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/100px-Weak-2-coloring.svg.png
categories:
    - Graph coloring
---
In graph theory, a weak coloring is a special case of a graph labeling. A weak k-coloring of a graph G = (V, E) assigns a color c(v) ∈ {1, 2, ..., k} to each vertex v ∈ V, such that each non-isolated vertex is adjacent to at least one vertex with different color. In notation, for each non-isolated v ∈ V, there is a vertex u ∈ U with {u, v} ∈ E and c(u) ≠ c(v).
