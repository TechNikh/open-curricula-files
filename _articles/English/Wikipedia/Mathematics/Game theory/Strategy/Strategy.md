---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strategy
offline_file: ""
offline_thumbnail: ""
uuid: 7f135418-c01f-4862-9f1a-c8131cb0c2fa
updated: 1484309063
title: Strategy
tags:
    - Components of strategy
    - Formulating and implementing strategy
    - Military theory
    - Management theory
    - Strategies in game theory
categories:
    - Strategy
---
Strategy (from Greek στρατηγία stratēgia, "art of troop leader; office of general, command, generalship"[1]) is a high level plan to achieve one or more goals under conditions of uncertainty. In the sense of the "art of the general", which included several subsets of skills including "tactics", siegecraft, logistics etc., the term came into use in the 6th century C.E. in East Roman terminology, and was translated into Western vernacular languages only in the 18th century. From then until the 20th century, the word "strategy" came to denote "a comprehensive way to try to pursue political ends, including the threat or actual use of force, in a dialectic of wills" in a military ...
