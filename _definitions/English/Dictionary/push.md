---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/push
offline_file: ""
offline_thumbnail: ""
uuid: 25b571c5-a2fe-459c-b3bb-891158fe696b
updated: 1484310335
title: push
categories:
    - Dictionary
---
push
     n 1: the act of applying force in order to move something away;
          "he gave the door a hard push"; "the pushing is good
          exercise" [syn: {pushing}]
     2: the force used in pushing; "the push of the water on the
        walls of the tank"; "the thrust of the jet engines" [syn:
        {thrust}]
     3: enterprising or ambitious drive; "Europeans often laugh at
        American energy" [syn: {energy}, {get-up-and-go}]
     4: an electrical switch operated by pressing a button; "the
        elevator was operated by push buttons"; "the push beside
        the bed operated a buzzer at the desk" [syn: {push button},
         {button}]
     5: an effort to advance; "the ...
