---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/existence
offline_file: ""
offline_thumbnail: ""
uuid: aa9d8728-f82f-4342-bdf0-0dfb1dba3693
updated: 1484310289
title: existence
categories:
    - Dictionary
---
existence
     n 1: the state or fact of existing; "a point of view gradually
          coming into being"; "laws in existence for centuries"
          [syn: {being}, {beingness}] [ant: {nonexistence}, {nonbeing}]
     2: everything that exists anywhere; "they study the evolution
        of the universe"; "the biggest tree in existence" [syn: {universe},
         {creation}, {world}, {cosmos}, {macrocosm}]
