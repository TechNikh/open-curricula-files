---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Chua%27s_circuit'
offline_file: ""
offline_thumbnail: ""
uuid: fdeb9d8a-998b-44df-989d-5d2ba32b6125
updated: 1484308722
title: "Chua's circuit"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Chua%2527s_circuit.svg.png'
tags:
    - Chaotic criteria
    - Models
    - Notes
categories:
    - Chaotic maps
---
Chua's circuit (also known as a Chua circuit) is a simple electronic circuit that exhibits classic chaos theory behavior. This means roughly that it is a "nonperiodic oscillator"; it produces an oscillating waveform that, unlike an ordinary electronic oscillator, never "repeats". It was invented in 1983 by Leon O. Chua, who was a visitor at Waseda University in Japan at that time.[1] The ease of construction of the circuit has made it a ubiquitous real-world example of a chaotic system, leading some to declare it "a paradigm for chaos".[2]
