---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secular
offline_file: ""
offline_thumbnail: ""
uuid: 52808d2e-efb3-4e54-b588-23e7a45d2ab6
updated: 1484310557
title: secular
categories:
    - Dictionary
---
secular
     adj : concerning those not members of the clergy; "set his collar
           in laic rather than clerical position"; "the lay
           ministry"; "the choir sings both sacred and secular
           music" [syn: {laic}, {lay}]
