---
version: 1
type: video
provider: YouTube
id: EuIowrQ62w8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7a338553-3026-43f6-901d-147e538ca50a
updated: 1486069607
title: Ordering numeric expressions
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/EuIowrQ62w8/default.jpg
    - https://i3.ytimg.com/vi/EuIowrQ62w8/1.jpg
    - https://i3.ytimg.com/vi/EuIowrQ62w8/2.jpg
    - https://i3.ytimg.com/vi/EuIowrQ62w8/3.jpg
---
