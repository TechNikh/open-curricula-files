---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anna_Nagurney
offline_file: ""
offline_thumbnail: ""
uuid: a4e67785-758a-4044-877a-10567fc03ce7
updated: 1484308506
title: Anna Nagurney
categories:
    - Game theorists
---
Anna Nagurney is a Ukrainian-American mathematician, economist, educator and author in the field of Operations Management. Nagurney holds the John F. Smith Memorial Professorship in the Isenberg School of Management at the University of Massachusetts Amherst at Amherst.[1] Nagurney's Doctoral Advisor at Brown University was Stella Dafermos. Anna has contributed to many different areas of operations research, and enjoys spending her time studying the Braess paradox.
