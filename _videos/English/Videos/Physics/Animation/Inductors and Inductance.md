---
version: 1
type: video
provider: YouTube
id: ukBFPrXiKWA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Inductors%20and%20Inductance.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 79b28534-8464-4860-90d6-58737236de5c
updated: 1486069656
title: Inductors and Inductance
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/ukBFPrXiKWA/default.jpg
    - https://i3.ytimg.com/vi/ukBFPrXiKWA/1.jpg
    - https://i3.ytimg.com/vi/ukBFPrXiKWA/2.jpg
    - https://i3.ytimg.com/vi/ukBFPrXiKWA/3.jpg
---
