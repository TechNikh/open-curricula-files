---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ram
offline_file: ""
offline_thumbnail: ""
uuid: fd85a70f-13f6-434d-8a41-ef335ad40fec
updated: 1484310254
title: ram
categories:
    - Dictionary
---
RAM
     n 1: the most common computer memory which can be used by
          programs to perform necessary tasks while the computer
          is on; an integrated circuit memory chip allows
          information to be stored or accessed in any order and
          all storage locations are equally accessible [syn: {random-access
          memory}, {random access memory}, {random memory}, {read/write
          memory}]
     2: (astrology) a person who is born while the sun is in Aries
        [syn: {Aries}]
     3: the first sign of the zodiac which the sun enters at the
        vernal equinox; the sun is in this sign from about March
        21 to April 19 [syn: {Aries}, {Aries the Ram}]
    ...
