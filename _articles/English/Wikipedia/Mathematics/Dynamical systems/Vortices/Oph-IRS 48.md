---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oph-IRS_48
offline_file: ""
offline_thumbnail: ""
uuid: a4f23cd3-b285-43b3-9168-91664094ab5a
updated: 1484308878
title: Oph-IRS 48
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Artist%25E2%2580%2599s_impression_of_the_comet_factory_seen_by_ALMA.jpg'
categories:
    - Vortices
---
The disk has revolutionized the view of planet formation in astronomy. Studies have shown that the millimeter dust particles are gathered in a crescent shape, while the gas (traced by CO molecules) and small dust grains follow a full disk ring structure .[2][3][4] The centimeter grains are even more concentrated inside the crescent.[5] This structure is consistent with theoretical predictions of dust trapping. Also the chemical composition has been studied, with molecules like H2CO being present.[6] The dust trap is thought to be conducting the process of planet formation in this young system.
