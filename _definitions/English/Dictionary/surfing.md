---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/surfing
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484411281
title: surfing
categories:
    - Dictionary
---
surfing
     n : the sport of riding a surfboard toward the shore on the
         crest of a wave [syn: {surfboarding}, {surfriding}]
