---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/web
offline_file: ""
offline_thumbnail: ""
uuid: bb9e018b-92f2-4277-9fbf-bd9d0f5baa1d
updated: 1484310273
title: web
categories:
    - Dictionary
---
web
     n 1: an intricate network suggesting something that was formed by
          weaving or interweaving; "the trees cast a delicate web
          of shadows over the lawn"
     2: an intricate trap that entangles or ensnares its victim
        [syn: {entanglement}]
     3: the flattened weblike part of a feather consisting of a
        series of barbs on either side of the shaft [syn: {vane}]
     4: an interconnected system of things or people; "he owned a
        network of shops"; "retirement meant dropping out of a
        whole network of people who had been part of my life";
        "tangled in a web of cloth" [syn: {network}]
     5: computer network consisting of a collection ...
