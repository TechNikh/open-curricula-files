---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Liouville%27s_theorem_(conformal_mappings)'
offline_file: ""
offline_thumbnail: ""
uuid: 4ebdad3b-31fe-4282-bd69-a9fd387290b8
updated: 1484309116
title: "Liouville's theorem (conformal mappings)"
categories:
    - Theorems in geometry
---
In mathematics, Liouville's theorem, proved by Joseph Liouville in 1850, is a rigidity theorem about conformal mappings in Euclidean space. It states that any smooth conformal mapping on a domain of Rn, where n > 2, can be expressed as a composition of translations, similarities, orthogonal transformations and inversions: they are Möbius transformations (in n dimensions).[1][2] This theorem severely limits the variety of possible conformal mappings in R3 and higher-dimensional spaces. By contrast, conformal mappings in R2 can be much more complicated – for example, all simply connected planar domains are conformally equivalent, by the Riemann mapping theorem.
