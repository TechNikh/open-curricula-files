---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/graded
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484592961
title: graded
categories:
    - Dictionary
---
graded
     adj : arranged in a sequence of grades or ranks; "stratified areas
           of the distribution" [syn: {ranked}, {stratified}]
