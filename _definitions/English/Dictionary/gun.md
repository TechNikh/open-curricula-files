---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gun
offline_file: ""
offline_thumbnail: ""
uuid: 65fa90ab-0b52-451e-87f8-db64a498e140
updated: 1484310313
title: gun
categories:
    - Dictionary
---
gun
     n 1: a weapon that discharges a missile at high velocity
          (especially from a metal tube or barrel)
     2: large but transportable armament [syn: {artillery}, {heavy
        weapon}, {ordnance}]
     3: a person who shoots a gun (as regards their ability) [syn: {gunman}]
     4: a professional killer who uses a gun [syn: {gunman}, {gunslinger},
         {hired gun}, {gun for hire}, {triggerman}, {hit man}, {hitman},
         {torpedo}, {shooter}]
     5: a hand-operated pump that resembles a gun; forces grease
        into parts of a machine [syn: {grease-gun}]
     6: a pedal that controls the throttle valve; "he stepped on the
        gas" [syn: {accelerator}, ...
