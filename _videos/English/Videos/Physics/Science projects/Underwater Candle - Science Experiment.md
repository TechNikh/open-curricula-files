---
version: 1
type: video
provider: YouTube
id: 8XSzJibma1s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 00783590-4002-4a8b-87ae-38cb03ed6570
updated: 1486069634
title: 'Underwater Candle - Science Experiment'
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/8XSzJibma1s/default.jpg
    - https://i3.ytimg.com/vi/8XSzJibma1s/1.jpg
    - https://i3.ytimg.com/vi/8XSzJibma1s/2.jpg
    - https://i3.ytimg.com/vi/8XSzJibma1s/3.jpg
---
