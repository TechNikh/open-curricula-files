---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/begging
offline_file: ""
offline_thumbnail: ""
uuid: 5610dd31-3169-4f14-9244-f647cb8a23c5
updated: 1484310603
title: begging
categories:
    - Dictionary
---
beg
     v 1: call upon in supplication; entreat; "I beg you to stop!"
          [syn: {implore}, {pray}]
     2: make a solicitation or entreaty for something; request
        urgently or persistently; "Henry IV solicited the Pope for
        a divorce"; "My neighbor keeps soliciting money for
        different charities" [syn: {solicit}, {tap}]
     3: ask to obtain free; "beg money and food"
     [also: {begging}, {begged}]
