---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/French_Institute_for_Research_in_Computer_Science_and_Automation
offline_file: ""
offline_thumbnail: ""
uuid: 05a5bdaa-de34-40e9-a4af-c2491fca2ceb
updated: 1484308675
title: >
    French Institute for Research in Computer Science and
    Automation
tags:
    - Administrative status
    - Research
categories:
    - Theoretical computer science
---
The French Institute for Research in Computer Science and Automation (French: Institut national de recherche en informatique et en automatique) is a French national research institution focusing on computer science and applied mathematics. It was created under the name Institut de recherche en informatique et en automatique (IRIA) in 1967 at Rocquencourt near Paris, part of Plan Calcul. Its first site was the historical premises of SHAPE (central command of NATO military forces). In 1979 IRIA became INRIA.[1] Since 2011, it has been styled inria.
