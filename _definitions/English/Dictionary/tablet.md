---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tablet
offline_file: ""
offline_thumbnail: ""
uuid: 6d91590e-f6f0-458d-a8e4-f8db3c1eb259
updated: 1484310381
title: tablet
categories:
    - Dictionary
---
tablet
     n 1: a slab of stone or wood suitable for bearing an inscription
     2: a number of sheets of paper fastened together along one edge
        [syn: {pad}, {pad of paper}]
     3: a small flat compressed cake of some substance; "a tablet of
        soap"
     4: a dose of medicine in the form of a small pellet [syn: {pill},
         {lozenge}, {tab}]
