---
version: 1
type: article
id: https://en.wikipedia.org/wiki/L._W._Beineke
offline_file: ""
offline_thumbnail: ""
uuid: c5a35067-8bd7-482d-8c25-99ace2728daa
updated: 1484309457
title: L. W. Beineke
categories:
    - Graph theorists
---
Lowell Wayne Beineke (born 1939) is a professor of graph theory at Indiana University – Purdue University Fort Wayne. Beineke is known for his elegant characterization of line graphs (derived graph) in terms of the nine Forbidden graph characterization.
