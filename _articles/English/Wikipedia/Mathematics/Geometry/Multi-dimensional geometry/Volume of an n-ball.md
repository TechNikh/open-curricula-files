---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Volume_of_an_n-ball
offline_file: ""
offline_thumbnail: ""
uuid: 2a1c85ce-85b2-4c91-a360-531813e002b1
updated: 1484309292
title: Volume of an n-ball
tags:
    - Formulas
    - The volume
    - Recursions
    - Low dimensions
    - High dimensions
    - Relation with surface area
    - Proofs
    - The volume is proportional to the nth power of the radius
    - The two-dimension recursion formula
    - The one-dimension recursion formula
    - Direct integration in spherical coordinates
    - Gaussian integrals
    - Balls in Lp norms
categories:
    - Multi-dimensional geometry
---
In geometry, a ball is a region in space consisting of all points within a fixed distance from a fixed point. An n-ball is a ball in n-dimensional Euclidean space. The volume of an n-ball is an important constant that occurs in formulas throughout mathematics.
