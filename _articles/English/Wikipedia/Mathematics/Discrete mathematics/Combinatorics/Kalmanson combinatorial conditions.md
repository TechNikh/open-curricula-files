---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Kalmanson_combinatorial_conditions
offline_file: ""
offline_thumbnail: ""
uuid: f8bae1f5-e082-49a0-83e4-53af81d1f849
updated: 1484308583
title: Kalmanson combinatorial conditions
categories:
    - Combinatorics
---
In mathematics, the Kalmanson combinatorial conditions are a set of conditions on the distance matrix used in determining the solvability of the traveling salesman problem. These conditions apply to a special kind of cost matrix, the Kalmanson matrix, and are named after Kenneth Kalmanson.
