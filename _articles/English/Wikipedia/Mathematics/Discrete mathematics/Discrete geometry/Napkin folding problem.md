---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Napkin_folding_problem
offline_file: ""
offline_thumbnail: ""
uuid: 2c6310c2-b343-4395-8382-25ec3a91a1f1
updated: 1484308616
title: Napkin folding problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Napkin_folding_problem_Lang_N_5.svg.png
tags:
    - Formulations
    - Folding along a straight line
    - Where only the result matters
    - Folding without stretching
    - Solutions
    - "Lang's solutions"
    - History
categories:
    - Discrete geometry
---
The napkin folding problem is a problem in geometry and the mathematics of paper folding that explores whether folding a square or a rectangular napkin can increase its perimeter. The problem is known under several names, including the Margulis napkin problem, suggesting it is due to Grigory Margulis, and the Arnold's rouble problem referring to Vladimir Arnold and the folding of a Russian ruble bank note. Some versions of the problem were solved by Robert J. Lang, Svetlana Krat, Alexey S. Tarasov, and Ivan Yaschenko. One form of the problem remains open.
