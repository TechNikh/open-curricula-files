---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conic_bundle
offline_file: ""
offline_thumbnail: ""
uuid: 05eee4a8-50f7-40b9-85f6-d6e4765c3de6
updated: 1484309138
title: Conic bundle
tags:
    - A naive point of view
    - The fiber c
categories:
    - Algebraic geometry
---
Theoretically, it can be considered as a Severi–Brauer surface, or more precisely as a Châtelet surface. This can be a double covering of a ruled surface. Through an isomorphism, it can be associated with a symbol 
  
    
      
        (
        a
        ,
        P
        )
      
    
    {\displaystyle (a,P)}
  
 in the second Galois cohomology of the field 
  
    
      
        k
      
    
    {\displaystyle k}
  
.
