---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exponential_stability
offline_file: ""
offline_thumbnail: ""
uuid: 1a5196f1-465a-4246-988d-b579341bfc4f
updated: 1484308830
title: Exponential stability
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-AsymptoticStabilityImpulseScilab.png
tags:
    - Practical consequences
    - Example exponentially stable LTI systems
    - Real-world example
categories:
    - Stability theory
---
In control theory, a continuous linear time-invariant system (LTI) is exponentially stable if and only if the system has eigenvalues (i.e., the poles of input-to-output systems) with strictly negative real parts. (i.e., in the left half of the complex plane).[1] A discrete-time input-to-output LTI system is exponentially stable if and only if the poles of its transfer function lie strictly within the unit circle centered on the origin of the complex plane. Exponential stability is a form of asymptotic stability. Systems that are not LTI are exponentially stable if their convergence is bounded by exponential decay.
