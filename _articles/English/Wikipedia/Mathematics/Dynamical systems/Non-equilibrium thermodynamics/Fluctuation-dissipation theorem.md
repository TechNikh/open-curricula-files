---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Fluctuation-dissipation_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 7c14286c-f5c8-48f4-b33a-b5bc37f295e9
updated: 1484308811
title: Fluctuation-dissipation theorem
tags:
    - Qualitative overview and examples
    - Examples in detail
    - Brownian motion
    - Thermal noise in a resistor
    - General formulation
    - Derivation
    - Violations in glassy systems
    - Notes
categories:
    - Non-equilibrium thermodynamics
---
The fluctuation-dissipation theorem (FDT) is a powerful tool in statistical physics for predicting the behavior of systems that obey detailed balance. Given that a system obeys detailed balance, the theorem is a general proof that thermal fluctuations in a physical variable predict the response quantified by the admittance or impedance of the same physical variable, and vice versa. The fluctuation-dissipation theorem applies both to classical and quantum mechanical systems.
