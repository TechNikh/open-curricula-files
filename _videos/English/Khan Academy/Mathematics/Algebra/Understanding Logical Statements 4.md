---
version: 1
type: video
provider: YouTube
id: mLCD0ez-yO0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Understanding%20Logical%20Statements%204.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4755f0da-76d7-46b8-882c-9b97122fcfd4
updated: 1486069567
title: Understanding Logical Statements 4
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/mLCD0ez-yO0/default.jpg
    - https://i3.ytimg.com/vi/mLCD0ez-yO0/1.jpg
    - https://i3.ytimg.com/vi/mLCD0ez-yO0/2.jpg
    - https://i3.ytimg.com/vi/mLCD0ez-yO0/3.jpg
---
