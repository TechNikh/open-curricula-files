---
version: 1
type: video
provider: YouTube
id: CzGGtJnbd1A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 04e3559d-94da-4215-8197-9a08dc0e9151
updated: 1486069614
title: Quotient rule from product rule
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/CzGGtJnbd1A/default.jpg
    - https://i3.ytimg.com/vi/CzGGtJnbd1A/1.jpg
    - https://i3.ytimg.com/vi/CzGGtJnbd1A/2.jpg
    - https://i3.ytimg.com/vi/CzGGtJnbd1A/3.jpg
---
