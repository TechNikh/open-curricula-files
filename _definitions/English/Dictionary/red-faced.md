---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/red-faced
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484445121
title: red-faced
categories:
    - Dictionary
---
red-faced
     adj 1: (especially of the face) reddened or suffused with or as if
            with blood from emotion or exertion; "crimson with
            fury"; "turned red from exertion"; "with puffy
            reddened eyes"; "red-faced and violent"; "flushed (or
            crimson) with embarrassment" [syn: {crimson}, {red}, {reddened},
             {flushed}]
     2: having a red face from embarrassment or shame or agitation
        or emotional upset; "the blushing boy was brought before
        the Principal"; "her blushful beau"; "was red-faced with
        anger" [syn: {blushful}, {blushing(a)}]
