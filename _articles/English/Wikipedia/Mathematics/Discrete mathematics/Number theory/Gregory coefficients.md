---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gregory_coefficients
offline_file: ""
offline_thumbnail: ""
uuid: e61b51a1-2158-48f3-b3b6-5bc41905fb7c
updated: 1484308644
title: Gregory coefficients
tags:
    - Computation and representations
    - Bounds and asymptotic behavior
    - Series with Gregory coefficients
    - Generalizations
categories:
    - Number theory
---
Gregory coefficients Gn, also known as reciprocal logarithmic numbers, Bernoulli numbers of the second kind and the Cauchy numbers of the first kind,[1][2][3][4][5][6][7][8][9][10][11][12][13] are the rational numbers
