---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Busy_beaver
offline_file: ""
offline_thumbnail: ""
uuid: de2fc236-4130-44e2-8139-c4e113351f52
updated: 1484309483
title: Busy beaver
tags:
    - The game
    - Related functions
    - The busy beaver function Σ
    - Non-computability
    - Complexity and unprovability of Σ
    - Max shifts function S
    - Known values for Σ and S
    - Proof for uncomputability of S(n) and Σ(n)
    - Exact values and lower bounds
    - Generalizations
    - Applications
    - Examples
    - Notes
categories:
    - Computability theory
---
The Busy Beaver Game consists of designing a halting, binary-alphabet Turing Machine which writes the most 1s on the tape, using only a limited set of states. The rules for the 2-state game are as follows: (i) the machine must have two states in addition to the halting state, and (ii) the tape starts with 0s only. As the player, you should conceive each state aiming for the maximum output of 1s on the tape while making sure the machine will halt eventually.
