---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emperor
offline_file: ""
offline_thumbnail: ""
uuid: 98c9a779-2559-48e9-a65e-cbbcd56ef877
updated: 1484310573
title: emperor
categories:
    - Dictionary
---
Emperor
     n 1: the male ruler of an empire
     2: red table grape of California
     3: large moth of temperate forests of Eurasia having heavily
        scaled transparent wings [syn: {emperor moth}, {Saturnia
        pavonia}]
     4: large richly colored butterfly [syn: {emperor butterfly}]
