---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ring_flip
offline_file: ""
offline_thumbnail: ""
uuid: ad6c07fa-94b7-425e-8682-4f9402e5b4f0
updated: 1484309282
title: Ring flip
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/450px-Ring-Flip.png
categories:
    - Molecular geometry
---
Ring flipping (also known as ring inversion or ring reversal) is a phenomenon involving the interconversion (by rotation) about single bonds of cyclic conformers having equivalent ring shapes but not necessarily equivalent spatial positions of substituent atoms.
