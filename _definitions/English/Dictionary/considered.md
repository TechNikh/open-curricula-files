---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/considered
offline_file: ""
offline_thumbnail: ""
uuid: c7a5ac27-e694-4eb2-a9cb-be5d7b22bdf7
updated: 1484310220
title: considered
categories:
    - Dictionary
---
considered
     adj 1: resulting from careful thought; "the paper was well thought
            out" [syn: {reasoned}, {well thought out(p)}]
     2: carefully considered; "a considered opinion" [syn: {wise}]
