---
version: 1
type: video
provider: YouTube
id: BvZcjQivfEs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: af5a0cfc-1bb8-4c6a-a07d-9c38800a07e1
updated: 1486069609
title: Multiplying Complex Numbers
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/BvZcjQivfEs/default.jpg
    - https://i3.ytimg.com/vi/BvZcjQivfEs/1.jpg
    - https://i3.ytimg.com/vi/BvZcjQivfEs/2.jpg
    - https://i3.ytimg.com/vi/BvZcjQivfEs/3.jpg
---
