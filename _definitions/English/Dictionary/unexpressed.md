---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unexpressed
offline_file: ""
offline_thumbnail: ""
uuid: 51cd34c1-62a2-4d73-a801-1db311d84b4e
updated: 1484310299
title: unexpressed
categories:
    - Dictionary
---
unexpressed
     adj : not made explicit; "the unexpressed terms of the agreement";
           "things left unsaid"; "some kind of unspoken
           agreement"; "his action is clear but his reason remains
           unstated" [syn: {unsaid}, {unstated}, {unuttered}, {unverbalized},
            {unverbalised}, {unvoiced}, {unspoken}]
