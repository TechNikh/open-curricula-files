---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wrong
offline_file: ""
offline_thumbnail: ""
uuid: 7210b2d5-95f5-4250-9ef5-26e9c8f014ce
updated: 1484310204
title: wrong
categories:
    - Dictionary
---
wrong
     adj 1: not correct; not in conformity with fact or truth; "an
            incorrect calculation"; "the report in the paper is
            wrong"; "your information is wrong"; "the clock showed
            the wrong time"; "found themselves on the wrong road";
            "based on the wrong assumptions" [syn: {incorrect}]
            [ant: {correct}, {correct}]
     2: contrary to conscience or morality or law; "it is wrong for
        the rich to take advantage of the poor"; "cheating is
        wrong"; "it is wrong to lie" [ant: {right}]
     3: not appropriate for a purpose or occasion; "unsuitable
        attire for the office"; "said all the wrong things" [syn:
        ...
