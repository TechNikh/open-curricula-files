---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cashew
offline_file: ""
offline_thumbnail: ""
uuid: 56ab75e2-b4c0-48d5-84b8-f77419202c35
updated: 1484310541
title: cashew
categories:
    - Dictionary
---
cashew
     n 1: tropical American evergreen tree bearing kidney-shaped nuts
          that are edible only when roasted [syn: {cashew tree}, {Anacardium
          occidentale}]
     2: kidney-shaped nut edible only when roasted [syn: {cashew nut}]
