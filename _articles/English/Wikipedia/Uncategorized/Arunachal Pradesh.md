---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arunachal
offline_file: ""
offline_thumbnail: ""
uuid: 5eb39285-989c-4563-a0e1-8524fcd02952
updated: 1484308415
title: Arunachal Pradesh
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-British_Indian_Empire_1909_Imperial_Gazetteer_of_India.jpg
tags:
    - History
    - Prehistory
    - Early history
    - Drawing of McMahon line
    - Sino-Indian War
    - Tawang
    - Current name
    - geography
    - Climate
    - biodiversity
    - Districts
    - Economy
    - Tourism
    - Demographics
    - religion
    - Languages
    - Transport
    - air
    - Roads
    - Railway
    - Education
    - State symbols
categories:
    - Uncategorized
---
Arunachal Pradesh /ˌɑːrəˌnɑːtʃəl prəˈdɛʃ/ is one of the twenty-nine states of India. Located in northeast India, it holds the most north-eastern position among the states in the north-east region of India. Arunachal Pradesh borders the states of Assam and Nagaland to the south, and shares international borders with Bhutan in the west, Myanmar in the east and China in the north. Itanagar is the capital of the state. Arunachal Pradesh has territorial disputes with both the PRC and ROC due to its cultural, ethnic and geographic proximity to Tibet.
