---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phasor
offline_file: ""
offline_thumbnail: ""
uuid: 4a7c1ab8-6cef-4dde-a9fe-e4fd595d3f3b
updated: 1484309345
title: Phasor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Wykres_wektorowy_by_Zureks.svg.png
tags:
    - Definition
    - Phasor arithmetic
    - Multiplication by a constant (scalar)
    - Differentiation and integration
    - Addition
    - Phasor diagrams
    - Applications
    - Circuit laws
    - Power engineering
    - Footnotes
categories:
    - Trigonometry
---
In physics and engineering, a phasor (a portmanteau of phase vector[1][2]), is a complex number representing a sinusoidal function whose amplitude (A), angular frequency (ω), and initial phase (θ) are time-invariant. It is related to a more general concept called analytic representation,[3] which decomposes a sinusoid into the product of a complex constant and a factor that encapsulates the frequency and time dependence. The complex constant, which encapsulates amplitude and phase dependence, is known as phasor, complex amplitude,[4][5] and (in older texts) sinor[6] or even complexor.[6]
