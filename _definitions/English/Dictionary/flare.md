---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flare
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484589241
title: flare
categories:
    - Dictionary
---
flare
     n 1: a shape that spreads outward; "the skirt had a wide flare"
          [syn: {flair}]
     2: a sudden burst of flame
     3: a burst of light used to communicate or illuminate [syn: {flash}]
     4: reddening of the skin spreading outward from a focus of
        infection or irritation
     5: a sudden recurrence or worsening of symptoms; "a colitis
        flare"; "infection can cause a lupus flare"
     6: a sudden eruption of intense high-energy radiation from the
        sun's surface; associated with sunspots and radio
        interference [syn: {solar flare}]
     7: am unwanted reflection in an optical system (or the fogging
        of an image that is caused by such a ...
