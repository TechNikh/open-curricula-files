---
version: 1
type: video
provider: YouTube
id: x5EJG_rAtkY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Absolute%20inequalities%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 589df3ee-99f6-4475-b15b-c8793b7a0a4e
updated: 1486069557
title: Absolute inequalities 2
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/x5EJG_rAtkY/default.jpg
    - https://i3.ytimg.com/vi/x5EJG_rAtkY/1.jpg
    - https://i3.ytimg.com/vi/x5EJG_rAtkY/2.jpg
    - https://i3.ytimg.com/vi/x5EJG_rAtkY/3.jpg
---
