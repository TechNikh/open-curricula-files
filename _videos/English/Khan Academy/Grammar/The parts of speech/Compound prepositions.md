---
version: 1
type: video
provider: YouTube
id: dNwf-DoSsF0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/The%20parts%20of%20speech/Compound%20prepositions.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9768c7ba-7941-47aa-a4a2-0906a23e7156
updated: 1486069599
title: Compound prepositions
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - The parts of speech
thumbnail_urls:
    - https://i3.ytimg.com/vi/dNwf-DoSsF0/default.jpg
    - https://i3.ytimg.com/vi/dNwf-DoSsF0/1.jpg
    - https://i3.ytimg.com/vi/dNwf-DoSsF0/2.jpg
    - https://i3.ytimg.com/vi/dNwf-DoSsF0/3.jpg
---
These prepositions are called compound prepositions, or multisyllabic prepositions. Like most prepositions, they have both literal and figurative meanings. David explains. 

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-preposition/e/multi-syllable-prepositions?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/partsofspeech/the-preposition/v/prepositional-phrases?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Missed the previous lesson? https://www.khanacademy.org/humanities/grammar/partsofspeech/the-preposition/v/prepositions-of-neither-space-nor-time?utm_source=YT&utm_medium=Desc&utm_campaign=grammar

Grammar on Khan Academy: Grammar is the collection of rules and conventions that make languages go. This section is about Standard American English, but there's something here for everyone.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Grammar channel: https://www.youtube.com/channel/UC8JT97hQjMVWeO0B-x8eVxQ?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy"
