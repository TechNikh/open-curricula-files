---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/global
offline_file: ""
offline_thumbnail: ""
uuid: 51f19067-6868-42c9-9136-0b99e5036402
updated: 1484310246
title: global
categories:
    - Dictionary
---
global
     adj 1: involving the entire earth; not limited or provincial in
            scope; "global war"; "global monetary policy";
            "neither national nor continental but planetary"; "a
            world crisis"; "of worldwide significance" [syn: {planetary},
             {world(a)}, {worldwide}]
     2: having the shape of a sphere or ball; "a spherical object";
        "nearly orbicular in shape"; "little globular houses like
        mud-wasp nests"- Zane Grey [syn: {ball-shaped}, {globose},
         {globular}, {orbicular}, {spheric}, {spherical}]
