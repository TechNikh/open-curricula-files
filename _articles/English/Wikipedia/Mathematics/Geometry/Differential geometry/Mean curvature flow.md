---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mean_curvature_flow
offline_file: ""
offline_thumbnail: ""
uuid: 1f5fa767-0036-4965-b75b-cce3cae3f224
updated: 1484309228
title: Mean curvature flow
tags:
    - Physical examples
    - Properties
    - Mean curvature flow of a three-dimensional surface
categories:
    - Differential geometry
---
In the field of differential geometry in mathematics, mean curvature flow is an example of a geometric flow of hypersurfaces in a Riemannian manifold (for example, smooth surfaces in 3-dimensional Euclidean space). Intuitively, a family of surfaces evolves under mean curvature flow if the normal component of the velocity of which a point on the surface moves is given by the mean curvature of the surface. For example, a round sphere evolves under mean curvature flow by shrinking inward uniformly (since the mean curvature vector of a sphere points inward). Except in special cases, the mean curvature flow develops singularities.
