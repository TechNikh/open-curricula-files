---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/centroid
offline_file: ""
offline_thumbnail: ""
uuid: d6cc74f9-046e-435b-857b-9c13c4139f92
updated: 1484310156
title: centroid
categories:
    - Dictionary
---
centroid
     n : the center of mass of an object of uniform density
