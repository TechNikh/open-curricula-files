---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/happen
offline_file: ""
offline_thumbnail: ""
uuid: e0c6ff01-90f8-4a23-80bd-671abc452758
updated: 1484310343
title: happen
categories:
    - Dictionary
---
happen
     v 1: come to pass; "What is happening?"; "The meeting took place
          off without an incidence"; "Nothing occurred that seemed
          important" [syn: {hap}, {go on}, {pass off}, {occur}, {pass},
           {fall out}, {come about}, {take place}]
     2: happen, occur, or be the case in the course of events or by
        chance; "It happens that today is my birthday"; "These
        things befell" (Santayana) [syn: {befall}, {bechance}]
     3: chance to be or do something, without intention or
        causation; "I happen to have just what you need!"
     4: come into being; become reality; "Her dream really
        materialized" [syn: {materialize}, {materialise}] ...
