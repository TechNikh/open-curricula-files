---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/screaming
offline_file: ""
offline_thumbnail: ""
uuid: 543478af-5f2b-432f-bc55-587a0fec3f03
updated: 1484310214
title: screaming
categories:
    - Dictionary
---
screaming
     adj 1: loud and sustained; shrill and piercing; "hordes of
            screaming fans"; "a screaming jet plane"; "a screaming
            fury of sound"; "a screeching parrot"; "screeching
            brakes"; "a horde of shrieking fans"; "shrieking
            winds" [syn: {screaming(a)}, {screeching(a)}, {shrieking(a)}]
     2: so extremely intense as to evoke screams; "in screaming
        agony"; "a screaming rage" [syn: {screaming(a)}]
     3: resembling a scream in effect; "screaming headlines";
        "screaming colors and designs" [syn: {screaming(a)}]
     4: marked by or causing boisterous merriment or convulsive
        laughter; "hilarious broad comedy"; "a ...
