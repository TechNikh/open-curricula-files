---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Degree_distribution
offline_file: ""
offline_thumbnail: ""
uuid: 481aeceb-e0f7-4ab8-ae94-38613220f6b5
updated: 1484308620
title: Degree distribution
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Enwiki-degree-distribution.png
tags:
    - Definition
    - Observed degree distributions
categories:
    - Graph theory
---
In the study of graphs and networks, the degree of a node in a network is the number of connections it has to other nodes and the degree distribution is the probability distribution of these degrees over the whole network.
