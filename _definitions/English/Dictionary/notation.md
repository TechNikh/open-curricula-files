---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/notation
offline_file: ""
offline_thumbnail: ""
uuid: e44a8ebf-f9de-461f-ba63-539c1f118660
updated: 1484310393
title: notation
categories:
    - Dictionary
---
notation
     n 1: a technical system of symbols used to represent special
          things [syn: {notational system}]
     2: a comment or instruction (usually added); "his notes were
        appended at the end of the article"; "he added a short
        notation to the address on the envelope" [syn: {note}, {annotation}]
     3: the activity of representing something by a special system
        of marks or characters
