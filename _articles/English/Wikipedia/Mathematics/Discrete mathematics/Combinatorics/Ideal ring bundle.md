---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ideal_ring_bundle
offline_file: ""
offline_thumbnail: ""
uuid: 0a555869-6a60-4ffb-b37b-c3f14e34464a
updated: 1484308577
title: Ideal ring bundle
categories:
    - Combinatorics
---
Ideal ring bundle (IRB) is a mathematical term which means an n-stage cyclic sequence of semi-measured terms, e.g. integers for which the set of all circular sums enumerates row of natural numbers by fixed times. The circular sum is called a sum of consecutive terms in the n-sequence of any number of terms (from 1 to n − 1).
