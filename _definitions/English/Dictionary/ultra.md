---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ultra
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484613302
title: ultra
categories:
    - Dictionary
---
ultra
     adj : (used of opinions and actions) far beyond the norm;
           "extremist political views"; "radical opinions on
           education"; "an ultra conservative" [syn: {extremist},
           {radical}]
