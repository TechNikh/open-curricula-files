---
version: 1
type: article
id: https://en.wikipedia.org/wiki/NodeXL
offline_file: ""
offline_thumbnail: ""
uuid: 22ecea9e-ef70-4744-9257-2122a5895d7f
updated: 1484309438
title: NodeXL
tags:
    - Codebase
    - features
    - Data Import
    - Data representation
    - Graph Analysis
    - Graph Visualization
    - Research
    - Notes
    - RESOURCES
categories:
    - Network theory
---
NodeXL Basic is a free and open-source network analysis and visualization software package for Microsoft Excel 2007/2010/2013/2016.[2][3] NodeXL Pro is a fee based fully featured version of NodeXL that includes access to social media network data importers, advanced network metrics, and automation. It is a popular[n 1] package similar to other network visualization tools such as Pajek, UCINet, and Gephi.[5]
