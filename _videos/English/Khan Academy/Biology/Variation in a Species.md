---
version: 1
type: video
provider: YouTube
id: DuArVnT1i-E
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Variation%20in%20a%20Species.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 114cf9bd-7b4c-4e2a-8065-5dc819b74b1c
updated: 1486069583
title: Variation in a Species
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/DuArVnT1i-E/default.jpg
    - https://i3.ytimg.com/vi/DuArVnT1i-E/1.jpg
    - https://i3.ytimg.com/vi/DuArVnT1i-E/2.jpg
    - https://i3.ytimg.com/vi/DuArVnT1i-E/3.jpg
---
How variation can be introduced into a species.
More free lessons at: http://www.khanacademy.org/video?v=DuArVnT1i-E
