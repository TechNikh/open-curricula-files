---
version: 1
type: video
provider: YouTube
id: GwzUMEuGZHs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b69aa146-5581-4ba8-bc19-9b521cfaaf9e
updated: 1486069637
title: TFT Polarisation Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/GwzUMEuGZHs/default.jpg
    - https://i3.ytimg.com/vi/GwzUMEuGZHs/1.jpg
    - https://i3.ytimg.com/vi/GwzUMEuGZHs/2.jpg
    - https://i3.ytimg.com/vi/GwzUMEuGZHs/3.jpg
---
