---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polyphenol
offline_file: ""
offline_thumbnail: ""
uuid: 1c029463-2639-48d0-8c4d-63ddf5428992
updated: 1484308313
title: Polyphenol
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-Tannic_acid.svg.png
tags:
    - Definition of the term polyphenol
    - Original "WBSSH" definition of polyphenols
    - Proposed Quideau definition of polyphenols
    - Defining chemical reactions of the polyphenol class
    - Chemical structure and synthesis
    - Structural features
    - Chemical synthesis
    - Chemical properties and uses
    - Chemical properties
    - Chemical uses
    - Biology
    - Biological role in plants
    - Occurrence in nature
    - Metabolism
    - Biosynthesis and metabolism
    - Content in food
    - Potential health effects
    - Traditional medicine
    - Research techniques
    - Sensory properties
    - Analysis
    - Extraction
    - Analysis techniques
    - Microscopy analysis
    - Quantification
categories:
    - Uncategorized
---
Polyphenols[1][2] (noun, pronunciation of the singular /pɒliˈfiːnəl/[3] or /pɒliˈfɛnəl/, also known as polyhydroxyphenols) are a structural class of mainly natural, but also synthetic or semisynthetic, organic chemicals characterized by the presence of large multiples of phenol structural units. The number and characteristics of these phenol structures underlie the unique physical, chemical, and biological (metabolic, toxic, therapeutic, etc.) properties of particular members of the class. Examples include tannic acid (image at right) and ellagitannin (image below). The historically important chemical class of tannins is a subset of the polyphenols.[1][4]
