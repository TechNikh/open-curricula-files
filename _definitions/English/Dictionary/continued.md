---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/continued
offline_file: ""
offline_thumbnail: ""
uuid: 787fac1d-7415-43af-8f82-8484195b4bc8
updated: 1484310194
title: continued
categories:
    - Dictionary
---
continued
     adj : without stop or interruption; "to insure the continued
           success of the war"; "the continued existence of
           nationalism"; "the continued popularity of Westerns"
           [ant: {discontinued}]
