---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Independent_assortment
offline_file: ""
offline_thumbnail: ""
uuid: a105fec0-3f80-4890-b369-abd818f899ac
updated: 1484308375
title: Mendelian inheritance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Gregor_Mendel.png
tags:
    - History
    - "Mendel's laws"
    - Law of Segregation of genes (the "First Law")
    - Law of Independent Assortment (the "Second Law")
    - Law of Dominance (the "Third Law")
    - Mendelian trait
    - Non-Mendelian inheritance
    - Notes
    - Notes
categories:
    - Uncategorized
---
Mendelian inheritance[help 1] is a type of biological inheritance that follows the laws originally proposed by Gregor Mendel in 1865 and 1866 and re-discovered in 1900. These laws were initially very controversial. When Mendel's theories were integrated with the Boveri–Sutton chromosome theory of inheritance by Thomas Hunt Morgan in 1915, they became the core of classical genetics. Ronald Fisher later combined these ideas with the theory of natural selection in his 1930 book The Genetical Theory of Natural Selection, putting evolution onto a mathematical footing and forming the basis for population genetics and the modern evolutionary synthesis.[1]
