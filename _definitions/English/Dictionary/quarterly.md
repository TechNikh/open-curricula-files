---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quarterly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484509921
title: quarterly
categories:
    - Dictionary
---
quarterly
     adj : of or relating to or consisting of a quarter; "quarterly
           report"
     n : a periodical that is published every quarter
     adv 1: in diagonally opposed quarters of an escutcheon; "two coats
            of arms borne quarterly"
     2: in three month intervals; "interest is compounded quarterly"
        [syn: {every quarter}]
