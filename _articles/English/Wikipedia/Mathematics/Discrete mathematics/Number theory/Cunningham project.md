---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cunningham_project
offline_file: ""
offline_thumbnail: ""
uuid: c0982649-1cac-478a-b395-dca209dd323c
updated: 1484308644
title: Cunningham project
tags:
    - Factors of Cunningham numbers
    - Algebraic factors
    - Aurifeuillian factors
    - Other factors
    - Notation
categories:
    - Number theory
---
The Cunningham project is a project, started in 1925, to factor numbers of the form bn ± 1 for b = 2, 3, 5, 6, 7, 10, 11, 12 and large n. The project is named after Allan Joseph Champneys Cunningham, who published the first version of the table together with Herbert J. Woodall.[1] There are three printed versions of the table, the most recent published in 2002,[2] as well as an online version.[3]
