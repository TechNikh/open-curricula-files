---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convex
offline_file: ""
offline_thumbnail: ""
uuid: 61ea6471-218d-466d-a9fa-4fc3c91c8318
updated: 1484310218
title: convex
categories:
    - Dictionary
---
convex
     adj : curving or bulging outward [syn: {bulging}] [ant: {concave}]
