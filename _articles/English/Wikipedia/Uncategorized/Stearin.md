---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tristearin
offline_file: ""
offline_thumbnail: ""
uuid: 64f272ce-cd49-4b53-8bde-e23462018d3d
updated: 1484308405
title: Stearin
tags:
    - Occurrence
    - Uses
categories:
    - Uncategorized
---
Stearin /ˈstɪərᵻn/, or tristearin, or glyceryl tristearate is a triglyceride derived from three units of stearic acid. Most triglycerides are derived from at least two and more commonly three different fatty acids.[7] Like other triglycerides, stearin can crystallise in three polymorphs. For stearin, these melt at 54 (α-form), 65, and 72.5 °C (β-form).[3]
