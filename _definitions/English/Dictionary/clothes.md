---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clothes
offline_file: ""
offline_thumbnail: ""
uuid: bafce7b0-ef9b-4610-92f8-fbdb091d2c20
updated: 1484310226
title: clothes
categories:
    - Dictionary
---
clothes
     n : clothing in general; "she was refined in her choice of
         apparel"; "he always bought his clothes at the same
         store"; "fastidious about his dress" [syn: {apparel}, {wearing
         apparel}, {dress}]
