---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/precocious
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484453701
title: precocious
categories:
    - Dictionary
---
precocious
     adj 1: characterized by or characteristic of exceptionally early
            development or maturity (especially in mental
            aptitude); "a precocious child"; "a precocious
            achievement" [ant: {retarded}]
     2: appearing or developing early; "precocious flowers appear
        before the leaves as in some species of magnolias"
