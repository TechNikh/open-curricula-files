---
version: 1
type: video
provider: YouTube
id: xTTgUcumYv0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Crystal%20Field%20Theory%20-%20Chemistry%20Animation%20Energy%20Video%20-%20Lecture%20on%20Crystal%20Field%20Splitting%20Theory.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9ab171c6-1505-49e1-8649-a279d197eaa9
updated: 1486069664
title: >
    Crystal Field Theory | Chemistry Animation Energy Video |
    Lecture on Crystal Field Splitting Theory
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/xTTgUcumYv0/default.jpg
    - https://i3.ytimg.com/vi/xTTgUcumYv0/1.jpg
    - https://i3.ytimg.com/vi/xTTgUcumYv0/2.jpg
    - https://i3.ytimg.com/vi/xTTgUcumYv0/3.jpg
---
