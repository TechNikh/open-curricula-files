---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drift
offline_file: ""
offline_thumbnail: ""
uuid: c611c30f-d15a-46c8-8be8-4543b1a1474d
updated: 1484310293
title: drift
categories:
    - Dictionary
---
drift
     n 1: a force that moves something along [syn: {impetus}, {impulsion}]
     2: the gradual departure from an intended course due to
        external influences (as a ship or plane)
     3: a process of linguistic change over a period of time
     4: something that is heaped up by the wind or by water currents
     5: a general tendency to change (as of opinion); "not openly
        liberal but that is the trend of the book"; "a broad
        movement of the electorate to the right" [syn: {trend}, {movement}]
     6: general meaning or tenor; "caught the drift of the
        conversation" [syn: {purport}]
     7: a horizontal (or nearly horizontal) passageway in a mine;
        ...
