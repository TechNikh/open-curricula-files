---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/blood
offline_file: ""
offline_thumbnail: ""
uuid: 732f5889-3fad-4f5c-ac88-2b0301eb109c
updated: 1484310357
title: blood
categories:
    - Dictionary
---
blood
     n 1: the fluid (red in vertebrates) that is pumped by the heart;
          "blood carries oxygen and nutrients to the tissues and
          carries waste products away"; "the ancients believed
          that blood was the seat of the emotions"
     2: the descendants of one individual; "his entire lineage has
        been warriors" [syn: {lineage}, {line}, {line of descent},
         {descent}, {bloodline}, {blood line}, {pedigree}, {ancestry},
         {origin}, {parentage}, {stemma}, {stock}]
     3: the shedding of blood resulting in murder; "he avenged the
        blood of his kinsmen" [syn: {bloodshed}, {gore}]
     4: temperament or disposition; "a person of hot blood"
     ...
