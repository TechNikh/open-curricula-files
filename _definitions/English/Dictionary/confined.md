---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confined
offline_file: ""
offline_thumbnail: ""
uuid: 035b5a94-873e-4e55-ba09-c98e258111ad
updated: 1484310365
title: confined
categories:
    - Dictionary
---
confined
     adj 1: not free to move about [ant: {unconfined}]
     2: enclosed by a confining fence [syn: {fenced in}, {penned}]
     3: not invading healthy tissue [ant: {invasive}]
     4: deprived of liberty; especially placed under arrest or
        restraint
     5: in captivity [syn: {captive}, {imprisoned}, {jailed}]
