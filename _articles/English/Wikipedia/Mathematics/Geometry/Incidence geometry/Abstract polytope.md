---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abstract_polytope
offline_file: ""
offline_thumbnail: ""
uuid: b6e75c94-b62e-4930-8f97-0261fec058ec
updated: 1484309253
title: Abstract polytope
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/340px-Pyramid_abstract_polytope.svg.png
tags:
    - Traditional versus abstract polytopes
    - Introductory concepts
    - Polytopes as posets
    - Least and greatest faces
    - A simple example
    - The Hasse diagram
    - Rank
    - The line segment
    - Flags
    - Sections
    - Vertex figures
    - Connectedness
    - Formal definition
    - Notes
    - The simplest polytopes
    - 'Rank < 2'
    - Rank 2
    - The digon
    - Examples of higher rank
    - Hosohedra and hosotopes
    - Projective polytopes
    - Duality
    - Abstract regular polytopes
    - An irregular example
    - Realisations
    - The amalgamation problem and universal polytopes
    - The 11-cell and the 57-cell
    - Local topology
    - Exchange maps
    - Incidence matrices
    - History
    - Notes
categories:
    - Incidence geometry
---
In mathematics, an abstract polytope, informally speaking, is a structure which considers only the combinatorial properties of a traditional polytope, ignoring many of its other properties, such as angles, edge lengths, etc. No space, such as Euclidean space, is required to contain it. The abstract formulation embodies the combinatorial properties as a partially ordered set or poset.
