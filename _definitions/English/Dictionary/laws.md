---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laws
offline_file: ""
offline_thumbnail: ""
uuid: 1b64e920-73fc-46b9-ba93-186759cd89dc
updated: 1484310275
title: laws
categories:
    - Dictionary
---
Laws
     n : the first of three divisions of the Hebrew Scriptures
         comprising the first five books of the Hebrew Bible
         considered as a unit [syn: {Torah}, {Pentateuch}]
