---
version: 1
type: video
provider: YouTube
id: hTBQUibtnBk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Systems%20and%20rate%20problems%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4b01dd1a-d10d-4290-8783-9d259021e4fa
updated: 1486069565
title: Systems and rate problems 3
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/hTBQUibtnBk/default.jpg
    - https://i3.ytimg.com/vi/hTBQUibtnBk/1.jpg
    - https://i3.ytimg.com/vi/hTBQUibtnBk/2.jpg
    - https://i3.ytimg.com/vi/hTBQUibtnBk/3.jpg
---
