---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comedian
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484530801
title: comedian
categories:
    - Dictionary
---
comedian
     n 1: a professional performer who tells jokes and performs
          comical acts [syn: {comic}]
     2: an actor in a comedy
