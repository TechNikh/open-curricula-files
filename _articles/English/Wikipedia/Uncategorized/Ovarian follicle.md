---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graafian_follicles
offline_file: ""
offline_thumbnail: ""
uuid: cf288f11-1939-4ea0-b073-49871d6b2a55
updated: 1484308368
title: Ovarian follicle
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Graafian_Follicle%252C_Human_Ovary_%25283595817584%2529.jpg'
tags:
    - Structure
    - Oocyte
    - Cumulus oophorus
    - Membrana granulosa
    - Granulosa cell
    - Theca of follicle
    - Development
    - Development of oocytes in ovarian follicles
    - Clinical significance
    - Additional images
categories:
    - Uncategorized
---
An ovarian follicle is a roughly spheroid cellular aggregation set found in the ovaries. It secretes hormones that influence stages of the menstrual cycle. Women begin puberty with about 400,000 follicles,[1] each with the potential to release an egg cell (ovum) at ovulation for fertilization.[2] These eggs are developed only once every menstrual cycle.
