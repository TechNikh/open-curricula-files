---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ideological
offline_file: ""
offline_thumbnail: ""
uuid: c7006b7f-13ad-4d7d-9aa0-86305c54a6df
updated: 1484310181
title: ideological
categories:
    - Dictionary
---
ideological
     adj : concerned with or suggestive of ideas; "an ideological
           argument"; "ideological application of a theory"; "the
           drama's symbolism was very ideological" [syn: {ideologic}]
