---
version: 1
type: video
provider: YouTube
id: 3z1d6t5grLg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0771989c-43eb-4feb-bf64-6685a3c71f28
updated: 1486069640
title: Experiment Preparation on the Optical Dilatometer DIL806
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/3z1d6t5grLg/default.jpg
    - https://i3.ytimg.com/vi/3z1d6t5grLg/1.jpg
    - https://i3.ytimg.com/vi/3z1d6t5grLg/2.jpg
    - https://i3.ytimg.com/vi/3z1d6t5grLg/3.jpg
---
