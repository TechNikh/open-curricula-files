---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/history
offline_file: ""
offline_thumbnail: ""
uuid: 1d1ec878-af37-4dfc-a6ec-2bd1c03b6d2e
updated: 1484310289
title: history
categories:
    - Dictionary
---
history
     n 1: the aggregate of past events; "a critical time in the
          school's history"
     2: the continuum of events occurring in succession leading from
        the past to the present and even into the future; "all of
        human history"
     3: a record or narrative description of past events; "a history
        of France"; "he gave an inaccurate account of the plot to
        kill the president"; "the story of exposure to lead" [syn:
         {account}, {chronicle}, {story}]
     4: the discipline that records and interprets past events
        involving human beings; "he teaches Medieval history";
        "history takes the long view"
     5: all that is remembered of ...
