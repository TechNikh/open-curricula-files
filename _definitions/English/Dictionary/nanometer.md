---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nanometer
offline_file: ""
offline_thumbnail: ""
uuid: 790c4e3a-3008-474c-9d74-4d522abeab64
updated: 1484310405
title: nanometer
categories:
    - Dictionary
---
nanometer
     n : a metric unit of length equal to one billionth of a meter
         [syn: {nanometre}, {nm}, {millimicron}, {micromillimeter},
          {micromillimetre}]
