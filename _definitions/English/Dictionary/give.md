---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/give
offline_file: ""
offline_thumbnail: ""
uuid: b9e00e56-1417-42ef-b9e4-43a1d64556c3
updated: 1484310361
title: give
categories:
    - Dictionary
---
give
     n : the elasticity of something that can be stretched and
         returns to its original length [syn: {spring}, {springiness}]
     v 1: cause to have, in the abstract sense or physical sense; "She
          gave him a black eye"; "The draft gave me a cold"
     2: be the cause or source of; "He gave me a lot of trouble";
        "Our meeting afforded much interesting information" [syn:
        {yield}, {afford}]
     3: transfer possession of something concrete or abstract to
        somebody; "I gave her my money"; "can you give me
        lessons?"; "She gave the children lots of love and tender
        loving care" [ant: {take}]
     4: convey or reveal information; "Give ...
