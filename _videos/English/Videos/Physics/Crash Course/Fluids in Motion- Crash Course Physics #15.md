---
version: 1
type: video
provider: YouTube
id: fJefjG3xhW0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 47526015-e86e-4022-93a2-5a727f21d5c3
updated: 1486069682
title: 'Fluids in Motion- Crash Course Physics #15'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/fJefjG3xhW0/default.jpg
    - https://i3.ytimg.com/vi/fJefjG3xhW0/1.jpg
    - https://i3.ytimg.com/vi/fJefjG3xhW0/2.jpg
    - https://i3.ytimg.com/vi/fJefjG3xhW0/3.jpg
---
