---
version: 1
type: video
provider: YouTube
id: BVXlrg3r3Os
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4723095d-f1f6-435c-b780-248ff50b82b1
updated: 1486069611
title: Absolute value smallest to biggest
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/BVXlrg3r3Os/default.jpg
    - https://i3.ytimg.com/vi/BVXlrg3r3Os/1.jpg
    - https://i3.ytimg.com/vi/BVXlrg3r3Os/2.jpg
    - https://i3.ytimg.com/vi/BVXlrg3r3Os/3.jpg
---
