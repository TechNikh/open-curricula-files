---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/terrible
offline_file: ""
offline_thumbnail: ""
uuid: 450f93cd-2ee9-49b9-8669-8294958a7aa1
updated: 1484310152
title: terrible
categories:
    - Dictionary
---
terrible
     adj 1: causing fear or dread or terror; "the awful war"; "an awful
            risk"; "dire news"; "a career or vengeance so direful
            that London was shocked"; "the dread presence of the
            headmaster"; "polio is no longer the dreaded disease
            it once was"; "a dreadful storm"; "a fearful howling";
            "horrendous explosions shook the city"; "a terrible
            curse" [syn: {awful}, {dire}, {direful}, {dread(a)}, {dreaded},
             {dreadful}, {fearful}, {fearsome}, {frightening}, {horrendous},
             {horrific}]
     2: exceptionally bad or displeasing; "atrocious taste";
        "abominable workmanship"; "an awful voice"; ...
