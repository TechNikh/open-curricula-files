---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cerium
offline_file: ""
offline_thumbnail: ""
uuid: 108fa732-7fdb-4356-a707-b6f19e923caa
updated: 1484308383
title: Cerium
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cerium2.jpg
tags:
    - characteristics
    - Physical
    - Isotopes
    - chemistry
    - Cerium(IV)
    - History
    - Occurrence and production
    - Applications
    - Biological role and precautions
    - Bibliography
categories:
    - Uncategorized
---
Cerium is a soft, ductile, silvery-white metallic chemical element with symbol Ce and atomic number 58. Tarnishing rapidly when exposed to air, it is soft enough to be cut with a knife. Cerium is the second element in the lanthanide series, and while it often shows the +3 state characteristic of the series, it also exceptionally has a stable +4 state that does not oxidise water. It is also traditionally considered to be one of the rare earth elements. Cerium has no biological role, and is not very toxic.
