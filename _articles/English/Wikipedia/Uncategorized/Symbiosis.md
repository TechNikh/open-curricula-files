---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symbiotic_relationship
offline_file: ""
offline_thumbnail: ""
uuid: 9ce984dd-0845-43de-8c2c-561960545cd1
updated: 1484308366
title: Symbiosis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Common_clownfish_curves_dnsmpl.jpg
tags:
    - Physical interaction
    - Mutualism
    - Mutualism and endosymbiosis
    - Commensalism
    - Parasitism
    - Amensalism
    - Synnecrosis
    - Symbiosis and evolution
    - Vascular plants
    - Symbiogenesis
    - Co-evolution
    - List of symbioses
    - Bibliography
categories:
    - Uncategorized
---
Symbiosis (from Greek συμβίωσις "living together", from σύν "together" and βίωσις "living")[2] is a close and often long-term interaction between two different biological species. In 1877 Albert Bernhard Frank used the word symbiosis (which previously had been used to depict people living together in community) to describe the mutualistic relationship in lichens.[3] In 1879, the German mycologist Heinrich Anton de Bary defined it as "the living together of unlike organisms."[4][5]
