---
version: 1
type: video
provider: YouTube
id: qOcFJKQPZfo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7b5d6bd2-742c-40b7-8784-d5dc13158669
updated: 1486069656
title: Divergence and Curl
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/qOcFJKQPZfo/default.jpg
    - https://i3.ytimg.com/vi/qOcFJKQPZfo/1.jpg
    - https://i3.ytimg.com/vi/qOcFJKQPZfo/2.jpg
    - https://i3.ytimg.com/vi/qOcFJKQPZfo/3.jpg
---
