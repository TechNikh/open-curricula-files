---
version: 1
type: video
provider: YouTube
id: LDlR4I4wibQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 58603753-dd3c-445f-8235-f1ec269dd7f1
updated: 1486069658
title: Busting the popcorn from cell phones myth
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/LDlR4I4wibQ/default.jpg
    - https://i3.ytimg.com/vi/LDlR4I4wibQ/1.jpg
    - https://i3.ytimg.com/vi/LDlR4I4wibQ/2.jpg
    - https://i3.ytimg.com/vi/LDlR4I4wibQ/3.jpg
---
