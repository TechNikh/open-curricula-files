---
version: 1
type: video
provider: YouTube
id: nNx9YcM3wQU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 43db7806-b2a2-4601-bcdc-1b3bb51fdfdc
updated: 1486069619
title: Making predictions with probability
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/nNx9YcM3wQU/default.jpg
    - https://i3.ytimg.com/vi/nNx9YcM3wQU/1.jpg
    - https://i3.ytimg.com/vi/nNx9YcM3wQU/2.jpg
    - https://i3.ytimg.com/vi/nNx9YcM3wQU/3.jpg
---
