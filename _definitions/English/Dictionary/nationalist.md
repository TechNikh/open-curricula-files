---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nationalist
offline_file: ""
offline_thumbnail: ""
uuid: 54beefec-36dd-4c31-8e19-c78c34673b61
updated: 1484310575
title: nationalist
categories:
    - Dictionary
---
nationalist
     adj : devotion to the interests or culture of a particular nation
           including promoting the interests of one country over
           those of others; "nationalist aspirations"; "minor
           nationalistic differences" [syn: {nationalistic}]
     n 1: one who loves and defends his or her country [syn: {patriot}]
     2: an advocate of national independence of or a strong national
        government
