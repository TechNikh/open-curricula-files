---
version: 1
type: video
provider: YouTube
id: OBVGQt1Eeug
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Mixture%20problems%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c5cac7a4-7f2c-4e19-b9d8-2cf8f8a74e62
updated: 1486069557
title: Mixture problems 2
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/OBVGQt1Eeug/default.jpg
    - https://i3.ytimg.com/vi/OBVGQt1Eeug/1.jpg
    - https://i3.ytimg.com/vi/OBVGQt1Eeug/2.jpg
    - https://i3.ytimg.com/vi/OBVGQt1Eeug/3.jpg
---
