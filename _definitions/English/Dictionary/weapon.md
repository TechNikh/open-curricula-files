---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weapon
offline_file: ""
offline_thumbnail: ""
uuid: 64cdfb71-0b8b-4c96-8155-c1b5ad722537
updated: 1484310607
title: weapon
categories:
    - Dictionary
---
weapon
     n 1: any instrument or instrumentality used in fighting or
          hunting; "he was licensed to carry a weapon" [syn: {arm},
           {weapon system}]
     2: a means of persuading or arguing; "he used all his
        conversational weapons" [syn: {artillery}]
