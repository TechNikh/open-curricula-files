---
version: 1
type: video
provider: YouTube
id: kDTMbM2Z3x4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0d9c1105-8492-40fc-bd62-eb3063510614
updated: 1486069634
title: >
    SCIENCE STUDY experiment shows how to find the focal length
    of concave lens
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/kDTMbM2Z3x4/default.jpg
    - https://i3.ytimg.com/vi/kDTMbM2Z3x4/1.jpg
    - https://i3.ytimg.com/vi/kDTMbM2Z3x4/2.jpg
    - https://i3.ytimg.com/vi/kDTMbM2Z3x4/3.jpg
---
