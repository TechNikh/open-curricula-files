---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Andries_Brouwer
offline_file: ""
offline_thumbnail: ""
uuid: ce436d6c-0c22-49e3-aa88-28dacd359328
updated: 1484309457
title: Andries Brouwer
tags:
    - Biography
    - Work
    - Hack
    - Linux kernel
    - Selected publications
categories:
    - Graph theorists
---
