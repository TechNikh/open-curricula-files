---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Caso
offline_file: ""
offline_thumbnail: ""
uuid: 5c5cab2d-95e3-413f-940d-5affc5630791
updated: 1484308389
title: Caso
tags:
    - Access
    - Parishes
    - Representative fauna
    - Representative flora
    - Feasts
    - Tourist attractions
categories:
    - Uncategorized
---
Caso (Asturian: Casu) is a municipality in the Spanish Principality of Asturias. It shares a boundary to the North with Piloña; to the East with Ponga; to the South with León and to the West with Sobrescobio and Laviana.
