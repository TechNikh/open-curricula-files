---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shame
offline_file: ""
offline_thumbnail: ""
uuid: edc6ccb8-3b6a-4e90-b7fb-88558eec7ac5
updated: 1484310186
title: shame
categories:
    - Dictionary
---
shame
     n 1: a painful emotion resulting from an awareness of inadequacy
          or guilt
     2: a state of dishonor; "one mistake brought shame to all his
        family"; "suffered the ignominy of being sent to prison"
        [syn: {disgrace}, {ignominy}]
     3: an unfortunate development; "it's a pity he couldn't do it"
        [syn: {pity}]
     v 1: bring shame or dishonor upon; "he dishonored his family by
          committing a serious crime" [syn: {dishonor}, {disgrace},
           {dishonour}, {attaint}] [ant: {honor}]
     2: compel through a sense of shame; "She shamed him into making
        amends"
     3: cause to be ashamed
     4: surpass or beat by a wide margin
