---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Zonal_and_poloidal
offline_file: ""
offline_thumbnail: ""
uuid: 2597e3ed-c213-4e16-b083-99c07fa01676
updated: 1484309305
title: Zonal and poloidal
categories:
    - Orientation (geometry)
---
In magnetic confinement fusion the zonal direction primarily connotes the poloidal direction (i.e. the short way around the torus), the corresponding coordinate being denoted by y in the slab approximation or θ in magnetic coordinates. However, in the fusion context, usage is restricted to the context of zonal plasma flows and there will in general be a toroidal component in such flows as well. Thus, although the term zonal has come into use in plasma physics to emphasize an analogy with zonal flows in geophysics, it does not uniquely identify the direction of flow, unlike the case in geophysics.
