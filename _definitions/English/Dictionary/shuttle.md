---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shuttle
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484363042
title: shuttle
categories:
    - Dictionary
---
shuttle
     n 1: badminton equipment consisting of a ball of cork or rubber
          with a crown of feathers [syn: {shuttlecock}, {bird}, {birdie}]
     2: public transport that consists of a bus or train or airplane
        that plies back and forth between two points
     3: bobbin that passes the weft thread between the warp threads
     v : travel back and forth between two points
