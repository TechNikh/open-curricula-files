---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rotation
offline_file: ""
offline_thumbnail: ""
uuid: 11a513ce-af4f-4168-9d6b-d1359cd3f842
updated: 1484309299
title: Rotation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Rotating_Sphere.gif
tags:
    - Mathematics
    - Astronomy
    - Rotation and revolution
    - Retrograde rotation
    - Physics
    - Cosmological principle
    - Euler rotations
    - Flight dynamics
    - Amusement rides
    - Sports
categories:
    - Orientation (geometry)
---
A rotation is a circular movement of an object around a center (or point) of rotation . A three-dimensional object always rotates around an imaginary line called a rotation axis. If the axis passes through the body's center of mass, the body is said to rotate upon itself, or spin. A rotation about an external point, e.g. the Earth about the Sun, is called a revolution or orbital revolution, typically when it is produced by gravity. The axis is called a pole.
