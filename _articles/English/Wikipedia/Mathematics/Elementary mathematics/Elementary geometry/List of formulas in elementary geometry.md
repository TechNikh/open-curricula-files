---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_formulas_in_elementary_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 10727250-221d-4b53-b0e1-f8f4c28ad0a2
updated: 1484308921
title: List of formulas in elementary geometry
tags:
    - Two-dimensional shapes
    - Three-dimensional shapes
    - LaTeX markup (for writer/editors)
    - Circle (area and circumference)
    - Sphere (area and volume)
categories:
    - Elementary geometry
---
