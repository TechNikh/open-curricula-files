---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pioneer
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484437801
title: pioneer
categories:
    - Dictionary
---
pioneer
     n 1: someone who helps to open up a new line of research or
          technology or art [syn: {innovator}, {trailblazer}, {groundbreaker}]
     2: one the first colonists or settler in a new territory; "they
        went west as pioneers with only the possessions they could
        carry with them"
     v 1: open up an area or prepare a way; "She pioneered a graduate
          program for women students" [syn: {open up}]
     2: take the lead or initiative in; participate in the
        development of; "This South African surgeon pioneered
        heart transplants" [syn: {initiate}]
     3: open up and explore a new area; "pioneer space"
