---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pineapple
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484326321
title: pineapple
categories:
    - Dictionary
---
pineapple
     n 1: a tropical American plant bearing a large fleshy edible
          fruit with a terminal tuft of stiff leaves; widely
          cultivated in the tropics [syn: {pineapple plant}, {Ananas
          comosus}]
     2: large sweet fleshy tropical fruit with a terminal tuft of
        stiff leaves; widely cultivated [syn: {ananas}]
