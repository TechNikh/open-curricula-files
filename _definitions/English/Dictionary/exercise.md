---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exercise
offline_file: ""
offline_thumbnail: ""
uuid: a1651fd7-792a-401d-ac39-01a6e0e46597
updated: 1484310469
title: exercise
categories:
    - Dictionary
---
exercise
     n 1: the activity of exerting your muscles in various ways to
          keep fit; "the doctor recommended regular exercise"; "he
          did some exercising"; "the physical exertion required by
          his work kept him fit" [syn: {exercising}, {physical
          exercise}, {physical exertion}, {workout}]
     2: the act of using; "he warned against the use of narcotic
        drugs"; "skilled in the utilization of computers" [syn: {use},
         {usage}, {utilization}, {utilisation}, {employment}]
     3: systematic training by multiple repetitions; "practice makes
        perfect" [syn: {practice}, {drill}, {practice session}, {recitation}]
     4: a task performed or ...
