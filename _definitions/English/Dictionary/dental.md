---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dental
offline_file: ""
offline_thumbnail: ""
uuid: 27989673-df42-468a-90f4-e1020a281bf4
updated: 1484310341
title: dental
categories:
    - Dictionary
---
dental
     adj 1: of or relating to the teeth; "dental floss"
     2: of or relating to dentistry; "dental student"
