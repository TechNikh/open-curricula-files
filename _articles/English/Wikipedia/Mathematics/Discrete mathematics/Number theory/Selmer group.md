---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Selmer_group
offline_file: ""
offline_thumbnail: ""
uuid: fd38eab3-d1ce-472b-8ad4-4e3ecf27a5d4
updated: 1484308657
title: Selmer group
categories:
    - Number theory
---
In arithmetic geometry, the Selmer group, named in honor of the work of Selmer (1951) by Cassels (1962), is a group constructed from an isogeny of abelian varieties.
