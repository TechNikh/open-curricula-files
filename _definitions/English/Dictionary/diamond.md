---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diamond
offline_file: ""
offline_thumbnail: ""
uuid: 6e516f49-4c30-4c6e-b147-c4cddac45976
updated: 1484310212
title: diamond
categories:
    - Dictionary
---
diamond
     n 1: a transparent piece of diamond that has been cut and
          polished and is valued as a precious gem
     2: very hard native crystalline carbon valued as a gem [syn: {adamant}]
     3: a playing card in the minor suit of diamonds
     4: the area of a baseball field that is enclosed by 3 bases and
        home plate [syn: {baseball diamond}, {infield}] [ant: {outfield}]
     5: the baseball playing field [syn: {ball field}, {baseball
        field}]
