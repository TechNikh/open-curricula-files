---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unwilling
offline_file: ""
offline_thumbnail: ""
uuid: 71072306-2217-48b7-9fc0-8a1ff0c1aa00
updated: 1484310443
title: unwilling
categories:
    - Dictionary
---
unwilling
     adj 1: not disposed or inclined toward; "an unwilling assistant"
            [ant: {willing}]
     2: in spite of contrary volition
