---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chess_opening
offline_file: ""
offline_thumbnail: ""
uuid: b5449ace-1ea3-4cfb-aaf3-d5441105b6ef
updated: 1484309017
title: Chess opening
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Nuvola_apps_package_games_strategy_4.png
tags:
    - Aims of the opening
    - Common aims in opening play
    - Top-level objectives
    - Opening repertoires
    - Opening nomenclature
    - Classification of chess openings
    - 'Open games: 1.e4 e5'
    - 'Semi-open games: 1.e4, Black plays other than 1...e5'
    - 'Closed games: 1.d4 d5'
    - 'Indian Defense Systems: 1.d4 Nf6'
    - Other Black responses to 1.d4
    - "Flank openings (including English, Réti, Bird's, and White fianchettos)"
    - Unusual first moves for White
categories:
    - Chess theory
---
A chess opening is the group of initial moves of a chess game. Recognized sequences of initial moves are referred to as openings by White, or defenses by Black, but opening is also used as the general term. There are many dozens of different openings, and hundreds of named variants. The Oxford Companion to Chess lists 1,327 named openings and variants.[1] These vary widely in character from quiet positional play to wild tactical play. In addition to referring to specific move sequences, the opening is the first phase of a chess game, the other phases being the middlegame and the endgame.
