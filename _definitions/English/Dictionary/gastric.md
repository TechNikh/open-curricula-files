---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gastric
offline_file: ""
offline_thumbnail: ""
uuid: 96bd73d3-16f8-402b-a666-834dc1b77ea8
updated: 1484310335
title: gastric
categories:
    - Dictionary
---
gastric
     adj : relating to or involving the stomach; "gastric ulcer" [syn:
           {stomachic}, {stomachal}]
