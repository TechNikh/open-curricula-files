---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/snake
offline_file: ""
offline_thumbnail: ""
uuid: eb727a21-e24a-4907-a732-b82869130fb7
updated: 1484310277
title: snake
categories:
    - Dictionary
---
snake
     n 1: limbless scaly elongate reptile; some are venomous [syn: {serpent},
           {ophidian}]
     2: a deceitful or treacherous person [syn: {snake in the grass}]
     3: a tributary of the Columbia River that rises in Wyoming and
        flows westward; discovered in 1805 by the Lewis and Clark
        Expedition [syn: {Snake River}]
     4: a long faint constellation in the southern hemisphere near
        the equator stretching between Virgo and Cancer [syn: {Hydra}]
     5: something resembling a snake
     v 1: move smoothly and sinuously, like a snake
     2: form a snake-like pattern; "The river snakes through the
        valley"
     3: move along a winding path; "The ...
