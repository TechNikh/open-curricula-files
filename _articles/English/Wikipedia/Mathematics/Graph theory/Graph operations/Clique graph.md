---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clique_graph
offline_file: ""
offline_thumbnail: ""
uuid: b810bd0f-4800-45e7-83b7-d2b55c93e549
updated: 1484309448
title: Clique graph
tags:
    - Formal definition
    - Characterization
categories:
    - Graph operations
---
Clique graphs were discussed at least as early as 1968,[1] and a characterization of clique graphs was given in 1971.[2]
