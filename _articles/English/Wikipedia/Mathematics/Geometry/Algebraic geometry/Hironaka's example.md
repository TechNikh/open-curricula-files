---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hironaka%27s_example'
offline_file: ""
offline_thumbnail: ""
uuid: 1a86136d-7839-46dd-89ee-a0b9d28ba357
updated: 1484309156
title: "Hironaka's example"
tags:
    - "Hironaka's example"
    - A complete abstract variety that is not projective
    - An effective cycle algebraically equivalent to 0
    - >
        A deformation of Kähler manifolds that is not a Kähler
        manifold
    - A smooth algebraic space that is not a scheme
    - A Moishezon manifold that is not an abstract variety
    - >
        The quotient of a scheme by a free action of a finite group
        need not be a scheme
    - >
        A finite subset of a variety need not be contained in an
        open affine subvariety
    - A variety with no Hilbert scheme
    - >
        Descent can fail for proper smooth morphisms of proper
        schemes
    - >
        A scheme of finite type over a field such that not every
        line bundle comes from a divisor
categories:
    - Algebraic geometry
---
In geometry, Hironaka's example is a non-Kähler complex manifold that is a deformation of Kähler manifolds found by Hironaka (1960, 1962). Hironaka's example can be used to show that several other plausible statements holding for smooth varieties of dimension at most 2 fail for smooth varieties of dimension at least 3.
