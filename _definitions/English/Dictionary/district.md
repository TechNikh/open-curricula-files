---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/district
offline_file: ""
offline_thumbnail: ""
uuid: 13068b7e-d800-48ac-b2c1-fb93fa85c610
updated: 1484310284
title: district
categories:
    - Dictionary
---
district
     n : a region marked off for administrative or other purposes
         [syn: {territory}, {territorial dominion}, {dominion}]
     v : regulate housing in; of certain areas of towns [syn: {zone}]
