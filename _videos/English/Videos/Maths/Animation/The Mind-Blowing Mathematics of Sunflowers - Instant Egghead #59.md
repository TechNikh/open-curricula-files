---
version: 1
type: video
provider: YouTube
id: z9d1mxgZ0ag
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e09b7e01-5b4f-4c2a-b8cf-22bc89e743e0
updated: 1486069643
title: 'The Mind-Blowing Mathematics of Sunflowers - Instant Egghead #59'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/z9d1mxgZ0ag/default.jpg
    - https://i3.ytimg.com/vi/z9d1mxgZ0ag/1.jpg
    - https://i3.ytimg.com/vi/z9d1mxgZ0ag/2.jpg
    - https://i3.ytimg.com/vi/z9d1mxgZ0ag/3.jpg
---
