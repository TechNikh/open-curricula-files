---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/environmentalist
offline_file: ""
offline_thumbnail: ""
uuid: 2ab2eb44-1362-42cd-912d-87565d51298e
updated: 1484310579
title: environmentalist
categories:
    - Dictionary
---
environmentalist
     n : someone who works to protect the environment from
         destruction or pollution [syn: {conservationist}]
