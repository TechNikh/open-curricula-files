---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tamarind
offline_file: ""
offline_thumbnail: ""
uuid: 67f50382-1f81-4007-8f86-324933d53b85
updated: 1484310351
title: tamarind
categories:
    - Dictionary
---
tamarind
     n 1: long-lived tropical evergreen tree with a spreading crown
          and feathery evergreen foliage and fragrant flowers
          yielding hard yellowish wood and long pods with edible
          chocolate-colored acidic pulp [syn: {tamarind tree}, {tamarindo},
           {Tamarindus indica}]
     2: large tropical seed pod with very tangy pulp that is eaten
        fresh or cooked with rice and fish or preserved for
        curries and chutneys [syn: {tamarindo}]
