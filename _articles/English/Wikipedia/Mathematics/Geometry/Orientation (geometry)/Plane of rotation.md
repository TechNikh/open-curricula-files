---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plane_of_rotation
offline_file: ""
offline_thumbnail: ""
uuid: af1bedd5-b72c-463f-91d9-62991921ff92
updated: 1484309299
title: Plane of rotation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-PlaneRotation.svg.png
tags:
    - Definitions
    - Plane
    - Plane of rotation
    - Two dimensions
    - Three dimensions
    - Four dimensions
    - Simple rotations
    - Double rotations
    - Isoclinic rotations
    - Higher dimensions
    - Mathematical properties
    - Reflections
    - Bivectors
    - Eigenvalues and eigenplanes
    - Notes
categories:
    - Orientation (geometry)
---
In geometry, a plane of rotation is an abstract object used to describe or visualise rotations in space. In three dimensions it is an alternative to the axis of rotation, but unlike the axis of rotation it can be used in other dimensions, such as two, four or more dimensions.
