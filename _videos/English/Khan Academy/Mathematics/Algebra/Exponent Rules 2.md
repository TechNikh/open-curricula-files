---
version: 1
type: video
provider: YouTube
id: 6ND4nKwyCEc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Exponent%20Rules%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 88663a4a-f7fd-4595-8129-2139bb73cc6c
updated: 1486069565
title: Exponent Rules 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/6ND4nKwyCEc/default.jpg
    - https://i3.ytimg.com/vi/6ND4nKwyCEc/1.jpg
    - https://i3.ytimg.com/vi/6ND4nKwyCEc/2.jpg
    - https://i3.ytimg.com/vi/6ND4nKwyCEc/3.jpg
---
