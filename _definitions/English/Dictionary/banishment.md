---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/banishment
offline_file: ""
offline_thumbnail: ""
uuid: 0b947f00-d810-46de-a6d9-b770a6732fd8
updated: 1484310593
title: banishment
categories:
    - Dictionary
---
banishment
     n 1: the state of being banished or ostracized (excluded from
          society by general consent); "the association should get
          rid of its elderly members--not by euthanasia, of
          course, but by Coventry" [syn: {ostracism}, {Coventry}]
     2: rejection by means of an act of banishing or proscribing
        someone [syn: {proscription}]
