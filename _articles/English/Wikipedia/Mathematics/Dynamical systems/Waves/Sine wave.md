---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sine_wave
offline_file: ""
offline_thumbnail: ""
uuid: 382a8627-6d0b-4918-be9e-60788de1beee
updated: 1484308900
title: Sine wave
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Sine_and_Cosine.svg.png
tags:
    - General form
    - Occurrences
    - Fourier series
    - Traveling and standing waves
categories:
    - Waves
---
A sine wave or sinusoid is a mathematical curve that describes a smooth repetitive oscillation. It is named after the function sine, of which it is the graph. It occurs often in pure and applied mathematics, as well as physics, engineering, signal processing and many other fields. Its most basic form as a function of time (t) is:
