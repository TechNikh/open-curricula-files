---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/setback
offline_file: ""
offline_thumbnail: ""
uuid: 5d88bb32-fb5a-42a8-9fd4-3235eb2045f1
updated: 1484310178
title: setback
categories:
    - Dictionary
---
set back
     v 1: hold back to a later time; "let's postpone the exam" [syn: {postpone},
           {prorogue}, {hold over}, {put over}, {table}, {shelve},
           {defer}, {remit}, {put off}]
     2: slow down the progress of; hinder; "His late start set him
        back"
     3: cost a certain amount; "My daughter's wedding set me back
        $20,000" [syn: {knock back}, {put back}]
