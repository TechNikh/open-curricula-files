---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comprise
offline_file: ""
offline_thumbnail: ""
uuid: d4c23af9-94bd-4a34-9c8a-960cc9f6eeba
updated: 1484310435
title: comprise
categories:
    - Dictionary
---
comprise
     v 1: be composed of; "The land he conquered comprised several
          provinces"; "What does this dish consist of?" [syn: {consist}]
     2: include or contain; have as a component; "A totally new idea
        is comprised in this paper"; "The record contains many old
        songs from the 1930's" [syn: {incorporate}, {contain}]
     3: form or compose; "This money is my only income"; "The stone
        wall was the backdrop for the performance"; "These
        constitute my entire belonging"; "The children made up the
        chorus"; "This sum represents my entire income for a
        year"; "These few men comprise his entire army" [syn: {constitute},
         ...
