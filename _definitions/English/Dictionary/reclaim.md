---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reclaim
offline_file: ""
offline_thumbnail: ""
uuid: 061323a0-2340-44f3-bd6a-364c1dd8ed24
updated: 1484310543
title: reclaim
categories:
    - Dictionary
---
reclaim
     v 1: claim back [syn: {repossess}]
     2: of materials from waste products [syn: {recover}]
     3: bring, lead, or force to abandon a wrong or evil course of
        life, conduct, and adopt a right one; "The Church reformed
        me"; "reform your conduct" [syn: {reform}, {regenerate}, {rectify}]
     4: make useful again; transform from a useless or uncultivated
        state; "The people reclaimed the marshes"
     5: overcome the wildness of; make docile and tractable; "He
        tames lions for the circus"; "reclaim falcons" [syn: {domesticate},
         {domesticize}, {domesticise}, {tame}]
