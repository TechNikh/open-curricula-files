---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/modernisation
offline_file: ""
offline_thumbnail: ""
uuid: 946f1740-cb26-4608-82de-e3ff260dd8a3
updated: 1484310529
title: modernisation
categories:
    - Dictionary
---
modernisation
     n : making modern in appearance or behavior; "the modernization
         of Nigeria will be a long process" [syn: {modernization}]
