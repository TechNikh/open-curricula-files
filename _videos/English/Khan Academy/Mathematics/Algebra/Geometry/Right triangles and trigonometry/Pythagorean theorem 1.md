---
version: 1
type: video
provider: YouTube
id: JVrkLIcA2qw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Geometry/Right%20triangles%20and%20trigonometry/Pythagorean%20theorem%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 62e58952-8bf6-48ef-a464-6f5378c76f5c
updated: 1486069565
title: Pythagorean theorem 1
categories:
    - Right triangles and trigonometry
thumbnail_urls:
    - https://i3.ytimg.com/vi/JVrkLIcA2qw/default.jpg
    - https://i3.ytimg.com/vi/JVrkLIcA2qw/1.jpg
    - https://i3.ytimg.com/vi/JVrkLIcA2qw/2.jpg
    - https://i3.ytimg.com/vi/JVrkLIcA2qw/3.jpg
---
