---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chloroform
offline_file: ""
offline_thumbnail: ""
uuid: 3e6ddc55-8798-4449-a371-f2e43a883a9b
updated: 1484310384
title: chloroform
categories:
    - Dictionary
---
chloroform
     n : a volatile liquid haloform (CHCl3); formerly used as an
         anesthetic; "chloroform was the first inhalation
         anesthetic" [syn: {trichloromethane}]
     v : anesthetize with chloroform; "Doctors used to put people
         under by chloroforming them"
