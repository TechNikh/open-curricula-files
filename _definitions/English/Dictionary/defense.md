---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defense
offline_file: ""
offline_thumbnail: ""
uuid: f2208ada-5b78-42d2-bce4-976be58ecd91
updated: 1484310383
title: defense
categories:
    - Dictionary
---
defense
     n 1: (sports) the team that is trying to prevent the other team
          from scoring; "his teams are always good on defense"
          [syn: {defence}, {defending team}] [ant: {offense}, {offense}]
     2: military action or resources protecting a country against
        potential enemies; "they died in the defense of
        Stalingrad"; "they were developed for the defense program"
        [syn: {defence}, {defensive measure}]
     3: the defendant and his legal advisors collectively; "the
        defense called for a mistrial" [syn: {defence}, {defense
        team}, {defense lawyers}] [ant: {prosecution}]
     4: protection from harm; "sanitation is the best defense
      ...
