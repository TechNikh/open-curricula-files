---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dominated
offline_file: ""
offline_thumbnail: ""
uuid: ee918b6e-b7d1-436d-836d-f3293a3b75a7
updated: 1484310579
title: dominated
categories:
    - Dictionary
---
dominated
     adj 1: controlled or ruled by superior authority or power
     2: harassed by persistent nagging [syn: {henpecked}]
