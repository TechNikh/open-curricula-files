---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hardy_hierarchy
offline_file: ""
offline_thumbnail: ""
uuid: 06db3db5-73fd-4916-993b-c9442acbf64f
updated: 1484309487
title: Hardy hierarchy
categories:
    - Computability theory
---
In computability theory, computational complexity theory and proof theory, the Hardy hierarchy, named after G. H. Hardy, is an ordinal-indexed family of functions hα: N → N (where N is the set of natural numbers, {0, 1, ...}). It is related to the fast-growing hierarchy and slow-growing hierarchy. The hierarchy was first described in Hardy's 1904 paper, "A theorem concerning the infinite cardinal numbers".
