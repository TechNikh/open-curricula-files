---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Journal_of_Number_Theory
offline_file: ""
offline_thumbnail: ""
uuid: d32ca74d-34b4-4804-bbfd-8e160bddf9f8
updated: 1484308649
title: Journal of Number Theory
categories:
    - Number theory
---
The Journal of Number Theory is a mathematics journal that publishes a broad spectrum of original research in number theory. The journal was established in 1969 by R.P. Bambah, P. Roquette, A. Ross, A. Woods, and H. Zassenhaus at Ohio State University. It is currently published monthly by Elsevier, with 6 volumes per year. The editor-in-chief is David Goss (Ohio State University).
