---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/morals
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484498401
title: morals
categories:
    - Dictionary
---
morals
     n : motivation based on ideas of right and wrong [syn: {ethical
         motive}, {ethics}, {morality}]
