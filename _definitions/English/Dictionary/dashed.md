---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dashed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484586061
title: dashed
categories:
    - Dictionary
---
dashed
     adj : having gaps or spaces; "sign on the dotted line" [syn: {dotted}]
