---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Japanese_theorem_for_cyclic_quadrilaterals
offline_file: ""
offline_thumbnail: ""
uuid: cf7940cb-31e9-4848-931a-12bae02ac526
updated: 1484309116
title: Japanese theorem for cyclic quadrilaterals
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Japan.svg_1.png
categories:
    - Theorems in geometry
---
In geometry, the Japanese theorem states that the centers of the incircles of certain triangles inside a cyclic quadrilateral are vertices of a rectangle.
