---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Control-Lyapunov_function
offline_file: ""
offline_thumbnail: ""
uuid: d3b160b3-6c53-4fbf-9aa1-66119eb5a532
updated: 1484308830
title: Control-Lyapunov function
tags:
    - Example
    - Notes
categories:
    - Stability theory
---
In control theory, a control-Lyapunov function[1] is a Lyapunov function 
  
    
      
        V
        (
        x
        )
      
    
    {\displaystyle V(x)}
  
 for a system with control inputs. The ordinary Lyapunov function is used to test whether a dynamical system is stable (more restrictively, asymptotically stable). That is, whether the system starting in a state 
  
    
      
        x
        ≠
        0
      
    
    {\displaystyle x\neq 0}
  
 in some domain D will remain in D, or for asymptotic stability will eventually return to 
  
    
      
        x
        =
        0
      
    
    {\displaystyle x=0}
  
. The control-Lyapunov function is used to test ...
