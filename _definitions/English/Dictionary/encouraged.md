---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/encouraged
offline_file: ""
offline_thumbnail: ""
uuid: f81cbb77-60fb-4e04-9197-f96c2e60fc9c
updated: 1484310258
title: encouraged
categories:
    - Dictionary
---
encouraged
     adj : inspired with confidence; "felt bucked up by his success"
           [syn: {bucked up(p)}]
