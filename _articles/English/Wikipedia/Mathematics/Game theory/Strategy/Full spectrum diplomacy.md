---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Full_spectrum_diplomacy
offline_file: ""
offline_thumbnail: ""
uuid: 4ac2d3ad-2d55-4754-b71d-bd93e7744bbf
updated: 1484309065
title: Full spectrum diplomacy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Gorbachev_and_Reagan_1987-2.jpg
tags:
    - Origin
    - Components
    - Orchestra analogy
    - Purpose
    - Use of the concept in academia and journalism
categories:
    - Strategy
---
Full spectrum diplomacy is a combination of traditional, government-to-government diplomacy with the many components of public diplomacy as well as the integration of these two functions with other instruments of statecraft. The term was coined by Dr. John Lenczowski, the Founder and President of the Institute of World Politics in Washington, D.C. in his book Full Spectrum Diplomacy and Grand Strategy: Reforming the Structure and Culture of U.S. Foreign Policy which was released in May, 2011.
