---
version: 1
type: video
provider: YouTube
id: 8ROHpZ0A70I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 725a4e7f-efac-4f6f-9e4e-847209fbc268
updated: 1486069664
title: 'The uncertain location of electrons - George Zaidan and Charles Morton'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/8ROHpZ0A70I/default.jpg
    - https://i3.ytimg.com/vi/8ROHpZ0A70I/1.jpg
    - https://i3.ytimg.com/vi/8ROHpZ0A70I/2.jpg
    - https://i3.ytimg.com/vi/8ROHpZ0A70I/3.jpg
---
