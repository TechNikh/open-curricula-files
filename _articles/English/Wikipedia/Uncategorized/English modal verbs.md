---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Couldn%27t'
offline_file: ""
offline_thumbnail: ""
uuid: 0536a42e-dae4-44cc-b491-de5263a138fb
updated: 1484308323
title: English modal verbs
tags:
    - Modal verbs and their features
    - Etymology
    - Syntax
    - Past forms
    - Conditional sentences
    - Replacements for defective forms
    - Contractions and reduced pronunciation
    - Usage of specific verbs
    - Can and could
    - May and might
    - Shall and should
    - Will and would
    - Must and had to
    - Ought to and had better
    - Dare and need
    - Used to
    - Deduction
    - Double modals
    - Comparison with other Germanic languages
    - Notes
categories:
    - Uncategorized
---
The modal verbs of English are a small class of auxiliary verbs used mostly to express modality (properties such as possibility, obligation, etc.). They can be distinguished from other verbs by their defectiveness (they do not have participle or infinitive forms) and by the fact that they do not take the ending -(e)s in the third-person singular.
