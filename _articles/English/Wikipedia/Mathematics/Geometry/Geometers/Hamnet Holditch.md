---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hamnet_Holditch
offline_file: ""
offline_thumbnail: ""
uuid: b6a50d5d-8c45-401c-bd06-78d9253aea50
updated: 1484309085
title: Hamnet Holditch
categories:
    - Geometers
---
Rev. Hamnet Holditch, also spelled Hamnett Holditch (1800 – 12 December 1867), was an English mathematician who was president of Gonville and Caius College, Cambridge.
