---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/partially
offline_file: ""
offline_thumbnail: ""
uuid: e3e2c8d1-eddb-4997-a8dd-ccd8ec3efebc
updated: 1484310327
title: partially
categories:
    - Dictionary
---
partially
     adv : in part; in some degree; not wholly; "I felt partly to
           blame"; "He was partially paralyzed" [syn: {partly}, {part}]
           [ant: {wholly}]
