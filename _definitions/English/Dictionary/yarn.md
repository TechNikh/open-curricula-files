---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yarn
offline_file: ""
offline_thumbnail: ""
uuid: e343bc5d-a98c-4899-a111-aa324f1c47f4
updated: 1484310521
title: yarn
categories:
    - Dictionary
---
yarn
     n 1: the act of giving an account describing incidents or a
          course of events; "his narration was hesitant" [syn: {narration},
           {recital}]
     2: a fine cord of twisted fibers (of cotton or silk or wool or
        nylon etc.) used in sewing and weaving [syn: {thread}]
     v : tell or spin a yarn
