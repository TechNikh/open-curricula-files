---
version: 1
type: video
provider: YouTube
id: w3BhzYI6zXU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0d6aa3a8-e9f1-442b-93c4-d2a7cec4faff
updated: 1486069682
title: 'Vectors and 2D Motion- Crash Course Physics #4'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/w3BhzYI6zXU/default.jpg
    - https://i3.ytimg.com/vi/w3BhzYI6zXU/1.jpg
    - https://i3.ytimg.com/vi/w3BhzYI6zXU/2.jpg
    - https://i3.ytimg.com/vi/w3BhzYI6zXU/3.jpg
---
