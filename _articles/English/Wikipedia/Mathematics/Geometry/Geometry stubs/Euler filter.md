---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euler_filter
offline_file: ""
offline_thumbnail: ""
uuid: e83ab88d-cdd7-460f-a86b-daf5adf8011e
updated: 1484309362
title: Euler filter
categories:
    - Geometry stubs
---
In computer graphics, an Euler filter is a filter intended to prevent gimbal lock and related discontinuities in animation data sets in which rotation is expressed in terms of Euler angles.
