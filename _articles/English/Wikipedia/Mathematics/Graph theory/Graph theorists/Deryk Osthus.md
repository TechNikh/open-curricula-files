---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deryk_Osthus
offline_file: ""
offline_thumbnail: ""
uuid: 18ed2d15-7688-4e27-a422-8630c8a9f0f0
updated: 1484309472
title: Deryk Osthus
tags:
    - Career
    - Awards and honours
categories:
    - Graph theorists
---
Deryk Osthus is the Professor of Graph Theory at the School of Mathematics, University of Birmingham. He is known for his research in combinatorics, predominantly in extremal and probabilistic graph theory.[1]
