---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/khaki
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484645101
title: khaki
categories:
    - Dictionary
---
khaki
     adj : of a yellowish brown color
     n : a sturdy twilled cloth of a yellowish brown color used
         especially for military uniforms
