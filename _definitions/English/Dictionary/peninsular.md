---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peninsular
offline_file: ""
offline_thumbnail: ""
uuid: 50b23a9d-45fc-4bac-a017-29115af1ac09
updated: 1484310433
title: peninsular
categories:
    - Dictionary
---
peninsular
     adj : of or forming or resembling a peninsula; "peninsular
           isolation"
