---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disguise
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484557321
title: disguise
categories:
    - Dictionary
---
disguise
     n 1: an outward semblance that misrepresents the true nature of
          something; "the theatrical notion of disguise is always
          associated with catastrophe in his stories" [syn: {camouflage}]
     2: any attire that modifies the appearance in order to conceal
        the wearer's identity
     3: the act of concealing the identity of something by modifying
        its appearance; "he is a master of disguise" [syn: {camouflage}]
     v : make unrecognizable; "The herb disguises the garlic taste";
         "We disguised our faces before robbing the bank"
