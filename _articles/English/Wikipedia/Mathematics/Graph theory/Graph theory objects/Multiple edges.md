---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiple_edges
offline_file: ""
offline_thumbnail: ""
uuid: 9fb47842-50f0-4779-bb39-4a38e882b287
updated: 1484309445
title: Multiple edges
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Multiple_edges.png
categories:
    - Graph theory objects
---
In graph theory, multiple edges (also called parallel edges or a multi-edge), are two or more edges that are incident to the same two vertices. A simple graph has no multiple edges.
