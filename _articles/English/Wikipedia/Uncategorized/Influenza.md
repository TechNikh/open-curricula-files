---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flu
offline_file: ""
offline_thumbnail: ""
uuid: 9c4d3754-f438-49ae-9e0f-5c74b7e29835
updated: 1484308450
title: Influenza
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/226px-EM_of_influenza_virus.jpg
tags:
    - Signs and symptoms
    - Virology
    - Types of virus
    - Influenzavirus A
    - Influenzavirus B
    - Influenzavirus C
    - Structure, properties, and subtype nomenclature
    - Replication
    - Mechanism
    - Transmission
    - Pathophysiology
    - Prevention
    - Vaccination
    - Infection control
    - Treatment
    - Antivirals
    - Neuraminidase inhibitors
    - M2 inhibitors
    - Prognosis
    - Epidemiology
    - Seasonal variations
    - Epidemic and pandemic spread
    - History
    - Etymology
    - Pandemics
    - Society and culture
    - Research
    - Other animals
    - Bird flu
    - Swine flu
categories:
    - Uncategorized
---
Influenza, commonly known as "the flu", is an infectious disease caused by an influenza virus.[1] Symptoms can be mild to severe.[2] The most common symptoms include: a high fever, runny nose, sore throat, muscle pains, headache, coughing, and feeling tired. These symptoms typically begin two days after exposure to the virus and most last less than a week. The cough, however, may last for more than two weeks.[1] In children, there may be nausea and vomiting, but these are not common in adults. Nausea and vomiting occur more commonly in the unrelated infection gastroenteritis, which is sometimes inaccurately referred to as "stomach flu" or "24-hour flu".[3] Complications of influenza may ...
