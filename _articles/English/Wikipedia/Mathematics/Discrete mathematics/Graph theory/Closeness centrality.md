---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Closeness_centrality
offline_file: ""
offline_thumbnail: ""
uuid: 247047c5-515c-43ac-bac4-543fb872aaaa
updated: 1484308502
title: Closeness centrality
tags:
    - In disconnected graphs
    - Variants
categories:
    - Graph theory
---
In a connected graph, the closeness centrality (or closeness) of a node is a measure of centrality in a network, calculated as the sum of the length of the shortest paths between the node and all other nodes in the graph. Thus the more central a node is, the closer it is to all other nodes.
