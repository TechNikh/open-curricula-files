---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/letting
offline_file: ""
offline_thumbnail: ""
uuid: 13d1fef0-9577-4dd7-9599-75c233d32994
updated: 1484310344
title: letting
categories:
    - Dictionary
---
letting
     n : property that is leased or rented out or let [syn: {lease},
         {rental}]
