---
version: 1
type: video
provider: YouTube
id: MZl6Mna0leQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Example%204-%20Factor%20a%20polynomial%20with%20two%20variables%20by%20taking%20a%20common%20factor.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9389a3a1-e4bf-4d3a-a718-450f791e318c
updated: 1486069565
title: 'Example 4- Factor a polynomial with two variables by taking a common factor'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/MZl6Mna0leQ/default.jpg
    - https://i3.ytimg.com/vi/MZl6Mna0leQ/1.jpg
    - https://i3.ytimg.com/vi/MZl6Mna0leQ/2.jpg
    - https://i3.ytimg.com/vi/MZl6Mna0leQ/3.jpg
---
