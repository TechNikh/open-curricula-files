---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/replacing
offline_file: ""
offline_thumbnail: ""
uuid: 586994a3-af23-4aff-8556-1242a821c825
updated: 1484310196
title: replacing
categories:
    - Dictionary
---
replacing
     n : the act of furnishing an equivalent person or thing in the
         place of another; "replacing the star will not be easy"
         [syn: {replacement}]
