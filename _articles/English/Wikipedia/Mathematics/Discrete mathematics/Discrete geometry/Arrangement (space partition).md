---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arrangement_(space_partition)
offline_file: ""
offline_thumbnail: ""
uuid: 3719999c-1edf-4a0d-b18b-82de07425bec
updated: 1484308595
title: Arrangement (space partition)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Complete-quads.svg.png
categories:
    - Discrete geometry
---
In discrete geometry, an arrangement is the decomposition of the d-dimensional linear, affine, or projective space into connected open cells of lower dimensions, induced by a finite collection of geometric objects. Sometimes these objects are of the same type, such as hyperplanes or spheres. An interest in the study of arrangements was driven by advances in computational geometry, where the arrangements were unifying structures for many problems. Advances in study of more complicated objects, such as algebraic surfaces, contributed to "real-world" applications, such as motion planning and computer vision.[1]
