---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sexually
offline_file: ""
offline_thumbnail: ""
uuid: 49c006cb-c5b7-4f5c-aba2-17a76e956b86
updated: 1484310290
title: sexually
categories:
    - Dictionary
---
sexually
     adv 1: with respect to sexuality; "sexually ambiguous"
     2: by sexual means; "reproduce sexually"
