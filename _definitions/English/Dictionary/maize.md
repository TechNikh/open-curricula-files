---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/maize
offline_file: ""
offline_thumbnail: ""
uuid: 2e115075-92dc-452b-8dd2-4de2b59adb2d
updated: 1484310254
title: maize
categories:
    - Dictionary
---
maize
     n 1: tall annual cereal grass bearing kernels on large ears:
          widely cultivated in America in many varieties; the
          principal cereal in Mexico and Central and South America
          since pre-Columbian times [syn: {corn}, {Indian corn}, {Zea
          mays}]
     2: a strong yellow color [syn: {gamboge}, {lemon}, {lemon
        yellow}]
