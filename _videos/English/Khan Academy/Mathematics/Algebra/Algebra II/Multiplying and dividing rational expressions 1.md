---
version: 1
type: video
provider: YouTube
id: 3GL69IA2q4s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Multiplying%20and%20dividing%20rational%20expressions%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f9e337b3-cb3f-40eb-96d6-4b0af1b0094e
updated: 1486069563
title: Multiplying and dividing rational expressions 1
categories:
    - Algebra II
thumbnail_urls:
    - https://i3.ytimg.com/vi/3GL69IA2q4s/default.jpg
    - https://i3.ytimg.com/vi/3GL69IA2q4s/1.jpg
    - https://i3.ytimg.com/vi/3GL69IA2q4s/2.jpg
    - https://i3.ytimg.com/vi/3GL69IA2q4s/3.jpg
---
