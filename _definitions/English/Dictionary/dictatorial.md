---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dictatorial
offline_file: ""
offline_thumbnail: ""
uuid: b114b238-de08-4da9-a959-5ac2f2080879
updated: 1484310577
title: dictatorial
categories:
    - Dictionary
---
dictatorial
     adj 1: of or characteristic of a dictator; "dictatorial powers"
     2: likened to a dictator in severity [syn: {authoritarian}]
     3: expecting unquestioning obedience; "he was imperious and
        dictatorial"; "the timid child of authoritarian parents";
        "insufferably overbearing behavior toward the waiter"
        [syn: {authoritarian}, {overbearing}]
     4: characteristic of an absolute ruler or absolute rule; having
        absolute sovereignty; "an authoritarian regime";
        "autocratic government"; "despotic rulers"; "a dictatorial
        rule that lasted for the duration of the war"; "a
        tyrannical government" [syn: {authoritarian}, ...
