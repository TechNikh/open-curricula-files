---
version: 1
type: video
provider: YouTube
id: c6vVg-7-2H4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d1a8426b-659d-4c50-874a-fe53db32a526
updated: 1486069607
title: Adding and subtracting on number line word problems
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/c6vVg-7-2H4/default.jpg
    - https://i3.ytimg.com/vi/c6vVg-7-2H4/1.jpg
    - https://i3.ytimg.com/vi/c6vVg-7-2H4/2.jpg
    - https://i3.ytimg.com/vi/c6vVg-7-2H4/3.jpg
---
