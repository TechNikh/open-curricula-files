---
version: 1
type: video
provider: YouTube
id: 83F7ShZ28s0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4022fb52-c820-43b2-901d-9c2ce6f5da2e
updated: 1486069621
title: Compound interest Shortcut Trick for Competitive Exams
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/83F7ShZ28s0/default.jpg
    - https://i3.ytimg.com/vi/83F7ShZ28s0/1.jpg
    - https://i3.ytimg.com/vi/83F7ShZ28s0/2.jpg
    - https://i3.ytimg.com/vi/83F7ShZ28s0/3.jpg
---
