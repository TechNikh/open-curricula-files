---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Casas-Alvero_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: 911d6b3c-adac-4b38-863b-2ccd92b5aacf
updated: 1484308639
title: Casas-Alvero conjecture
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Dr._Casas-Alvero_talking_about_Casas-Alvero_Conjecture.jpg
tags:
    - Formal statement
    - Analog in non-zero characteristic
    - Special cases
categories:
    - Number theory
---
In mathematics, the Casas-Alvero conjecture is an open problem about polynomials which have factors in common with their derivatives, proposed by Eduardo Casas-Alvero in 2001.
