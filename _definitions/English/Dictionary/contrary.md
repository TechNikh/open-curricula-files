---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contrary
offline_file: ""
offline_thumbnail: ""
uuid: 9eaa5314-9be7-4fb3-8765-fb87c169680d
updated: 1484310313
title: contrary
categories:
    - Dictionary
---
contrary
     adj 1: very opposed in nature or character or purpose; "acts
            contrary to our code of ethics"; "the facts point to a
            contrary conclusion"
     2: of words or propositions so related that both cannot be true
        but both may be false; "`hot' and `cold' are contrary
        terms"
     3: resistant to guidance or discipline; "Mary Mary quite
        contrary"; "an obstinate child with a violent temper"; "a
        perverse mood"; "wayward behavior" [syn: {obstinate}, {perverse},
         {wayward}]
     4: in an opposing direction; "adverse currents"; "a contrary
        wind" [syn: {adverse}]
     n 1: a relation of direct opposition; "we thought Sue ...
