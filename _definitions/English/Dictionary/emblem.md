---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emblem
offline_file: ""
offline_thumbnail: ""
uuid: 27abcc87-0871-4e1e-b5f8-5cdc06803544
updated: 1484310181
title: emblem
categories:
    - Dictionary
---
emblem
     n 1: special design or visual object representing a quality,
          type, group, etc.
     2: a visible symbol representing an abstract idea [syn: {allegory}]
