---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/davy
offline_file: ""
offline_thumbnail: ""
uuid: b893ab12-9a0b-4f9e-8699-2635acaeee9b
updated: 1484310403
title: davy
categories:
    - Dictionary
---
Davy
     n : English chemist who was a pioneer in electrochemistry and
         who used it to isolate elements sodium and potassium and
         barium and boron and calcium and magnesium and chlorine
         (1778-1829) [syn: {Humphrey Davy}, {Sir Humphrey Davy}]
