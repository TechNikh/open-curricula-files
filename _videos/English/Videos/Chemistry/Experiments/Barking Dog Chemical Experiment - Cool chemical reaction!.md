---
version: 1
type: video
provider: YouTube
id: 2DeeMICweiw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Experiments/Barking%20Dog%20Chemical%20Experiment%20-%20Cool%20chemical%20reaction%21.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ce5fa8bd-f313-4567-b4f7-c8fcc9df2727
updated: 1486069661
title: 'Barking Dog Chemical Experiment - Cool chemical reaction!'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/2DeeMICweiw/default.jpg
    - https://i3.ytimg.com/vi/2DeeMICweiw/1.jpg
    - https://i3.ytimg.com/vi/2DeeMICweiw/2.jpg
    - https://i3.ytimg.com/vi/2DeeMICweiw/3.jpg
---
