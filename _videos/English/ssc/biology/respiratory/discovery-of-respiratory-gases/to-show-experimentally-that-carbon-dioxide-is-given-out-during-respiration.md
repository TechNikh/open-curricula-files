---
title: To show experimentally that carbon dioxide is given out during respiration
youtube_id: 34ESzqzf_Uo
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - discovery-of-respiratory-gases
---
