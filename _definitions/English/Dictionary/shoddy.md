---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shoddy
offline_file: ""
offline_thumbnail: ""
uuid: d268c78c-9d20-4761-b0f1-be1cb600b8bc
updated: 1484310150
title: shoddy
categories:
    - Dictionary
---
shoddy
     adj 1: cheap and shoddy; "cheapjack moviemaking...that feeds on the
            low taste of the mob"- Judith Crist [syn: {cheapjack},
             {tawdry}]
     2: of inferior workmanship and materials; "mean little
        jerry-built houses" [syn: {jerry-built}]
     n : reclaimed wool fiber
     [also: {shoddiest}, {shoddier}]
