---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Circuit_rank
offline_file: ""
offline_thumbnail: ""
uuid: bf46f49a-6af2-45ab-8983-8f4efc18c766
updated: 1484309417
title: Circuit rank
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/290px-6n-graf.svg.png
tags:
    - Matroid rank and construction of a minimum feedback edge set
    - The number of independent cycles
    - Applications
    - Meshedness coefficient
    - Ear decomposition
    - Almost-trees
    - Generalizations to directed graphs
    - Related concepts
categories:
    - Graph invariants
---
In graph theory, a branch of mathematics, the circuit rank of an undirected graph is the minimum number of edges that must be removed from the graph to break all its cycles, making it into a tree or forest. Alternatively, it can be interpreted as the number of independent cycles in the graph. Unlike the corresponding feedback arc set problem for directed graphs, the circuit rank r is easily computed using the formula
