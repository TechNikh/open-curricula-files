---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/B*'
offline_file: ""
offline_thumbnail: ""
uuid: a962657a-af2a-4543-9188-646455d4ffe3
updated: 1484308999
title: 'B*'
tags:
    - Summary
    - Details
    - Interval evaluations rather than estimates
    - Backup process
    - Termination of search
    - Expansion
    - Strategy selection
    - Strategy selection at non-root nodes
    - Robustness
    - Extension to two-player games
    - Applications
categories:
    - Game artificial intelligence
---
In computer science, B* (pronounced "B star") is a best-first graph search algorithm that finds the least-cost path from a given initial node to any goal node (out of one or more possible goals). First published by Hans Berliner in 1979, it is related to the A* search algorithm.
