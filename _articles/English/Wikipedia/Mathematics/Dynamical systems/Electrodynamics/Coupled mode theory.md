---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coupled_mode_theory
offline_file: ""
offline_thumbnail: ""
uuid: f5fadf8f-232d-43e6-afa3-f606eca6928f
updated: 1484308751
title: Coupled mode theory
tags:
    - History
    - Overview
    - Formulation
categories:
    - Electrodynamics
---
Coupled mode theory (CMT) is a perturbational approach for analyzing the coupling of vibrational systems (mechanical, optical, electrical, etc.) in space or in time. Coupled mode theory allows a wide range of devices and systems to be modeled as one or more coupled resonators. In optics, such systems include laser cavities, photonic crystal slabs, metamaterials, and ring resonators.
