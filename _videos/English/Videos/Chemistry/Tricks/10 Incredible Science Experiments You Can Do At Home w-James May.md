---
version: 1
type: video
provider: YouTube
id: V-VKCVyidrc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3ec25440-03d1-42aa-931b-dad6a3491674
updated: 1486069661
title: >
    10 Incredible Science Experiments You Can Do At Home w/James
    May
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/V-VKCVyidrc/default.jpg
    - https://i3.ytimg.com/vi/V-VKCVyidrc/1.jpg
    - https://i3.ytimg.com/vi/V-VKCVyidrc/2.jpg
    - https://i3.ytimg.com/vi/V-VKCVyidrc/3.jpg
---
