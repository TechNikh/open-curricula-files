---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ozone
offline_file: ""
offline_thumbnail: ""
uuid: ea41eee1-7880-44a3-864f-1d6290fea06a
updated: 1484310170
title: ozone
categories:
    - Dictionary
---
ozone
     n : a colorless gas (O3) soluble in alkalis and cold water; a
         strong oxidizing agent; can be produced by electric
         discharge in oxygen or by the action of ultraviolet
         radiation on oxygen in the stratosphere (where it acts as
         a screen for ultraviolet radiation)
