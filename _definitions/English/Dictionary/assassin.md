---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assassin
offline_file: ""
offline_thumbnail: ""
uuid: d52295d0-a143-495a-829c-0b38c15444ab
updated: 1484310591
title: assassin
categories:
    - Dictionary
---
assassin
     n 1: a murderer (especially one who kills a prominent political
          figure) who kills by a treacherous surprise attack and
          often is hired to do the deed; "his assassins were
          hunted down like animals"; "assassinators of kings and
          emperors" [syn: {assassinator}, {bravo}]
     2: a member of a secret order of Muslims (founded in the 12th
        century) who terroriszed and killed Christian Crusaders
