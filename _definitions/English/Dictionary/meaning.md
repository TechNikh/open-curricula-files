---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meaning
offline_file: ""
offline_thumbnail: ""
uuid: 17da81d2-96cf-4679-95f6-8e6a200e06cb
updated: 1484310289
title: meaning
categories:
    - Dictionary
---
meaning
     adj : rich in significance or implication; "a meaning look";
           "pregnant with meaning" [syn: {meaning(a)}, {pregnant},
            {significant}]
     n 1: the message that is intended or expressed or signified;
          "what is the meaning of this sentence"; "the
          significance of a red traffic light"; "the signification
          of Chinese characters"; "the import of his announcement
          was ambigtuous" [syn: {significance}, {signification}, {import}]
     2: the idea that is intended; "What is the meaning of this
        proverb?" [syn: {substance}]
