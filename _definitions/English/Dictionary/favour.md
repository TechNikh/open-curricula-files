---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/favour
offline_file: ""
offline_thumbnail: ""
uuid: 5a53491d-a297-467c-a400-f52631e13f00
updated: 1484310469
title: favour
categories:
    - Dictionary
---
favour
     n 1: a feeling of favorable regard [syn: {favor}]
     2: an inclination to approve; "that style is in favor this
        season" [syn: {favor}]
     3: an advantage to the benefit of someone or something; "the
        outcome was in his favor" [syn: {favor}]
     4: souvenir consisting of a small gift given to a guest at a
        party [syn: {party favor}, {party favour}, {favor}]
     5: an act of gracious kindness [syn: {favor}]
     v 1: treat gently or carefully [syn: {favor}]
     2: bestow a privilege upon [syn: {privilege}, {favor}]
     3: promote over another; "he favors his second daughter" [syn:
        {prefer}, {favor}]
     4: consider as the favorite; "The local ...
