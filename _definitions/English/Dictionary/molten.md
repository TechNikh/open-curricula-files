---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/molten
offline_file: ""
offline_thumbnail: ""
uuid: fbbcc133-e706-4ccd-9693-7ed527dd7d2a
updated: 1484310407
title: molten
categories:
    - Dictionary
---
melt
     n : the process whereby heat changes something from a solid to a
         liquid; "the power failure caused a refrigerator melt
         that was a disaster"; "the thawing of a frozen turkey
         takes several hours" [syn: {thaw}, {thawing}, {melting}]
     v 1: reduce or cause to be reduced from a solid to a liquid
          state, usually by heating; "melt butter"; "melt down
          gold"; "The wax melted in the sun" [syn: {run}, {melt
          down}]
     2: become or cause to become soft or liquid; "The sun melted
        the ice"; "the ice thawed"; "the ice cream melted"; "The
        heat melted the wax"; "The giant iceberg dissolved over
        the years during the ...
