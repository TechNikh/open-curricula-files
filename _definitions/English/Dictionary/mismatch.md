---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mismatch
offline_file: ""
offline_thumbnail: ""
uuid: 843872ce-5a19-4172-a2b7-332262bfcf19
updated: 1484310464
title: mismatch
categories:
    - Dictionary
---
mismatch
     v : match badly; match two objects or people that do not go
         together
