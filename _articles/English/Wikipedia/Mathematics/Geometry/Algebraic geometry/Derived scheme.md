---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Derived_scheme
offline_file: ""
offline_thumbnail: ""
uuid: df7c669d-c67b-442d-a628-370daab4a119
updated: 1484309142
title: Derived scheme
tags:
    - Connection with differential graded rings
    - Generalizations
    - Notes
categories:
    - Algebraic geometry
---
In algebraic geometry, a derived scheme is a pair 
  
    
      
        (
        X
        ,
        
          
            O
          
        
        )
      
    
    {\displaystyle (X,{\mathcal {O}})}
  
 consisting of a topological space X and a sheaf 
  
    
      
        
          
            O
          
        
      
    
    {\displaystyle {\mathcal {O}}}
  
 of commutative ring spectra [1] on X such that (1) the pair 
  
    
      
        (
        X
        ,
        
          π
          
            0
          
        
        
          
            O
          
        
        )
      
    
    {\displaystyle (X,\pi _{0}{\mathcal {O}})}
  
 is a scheme and ...
