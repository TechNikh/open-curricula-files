---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gram
offline_file: ""
offline_thumbnail: ""
uuid: a6f79b06-2dc7-4ce8-8913-a0e7c8db526b
updated: 1484310232
title: gram
categories:
    - Dictionary
---
gram
     n 1: a metric unit of weight equal to one thousandth of a
          kilogram [syn: {gramme}, {gm}, {g}]
     2: Danish physician and bacteriologist who developed a method
        of staining bacteria to distinguish among them (1853-1938)
        [syn: {Hans C. J. Gram}]
