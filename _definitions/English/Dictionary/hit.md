---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hit
offline_file: ""
offline_thumbnail: ""
uuid: 0d841ff8-029c-42b1-9727-c7ec3d350d28
updated: 1484310533
title: hit
categories:
    - Dictionary
---
hit
     n 1: (baseball) a successful stroke in an athletic contest
          (especially in baseball); "he came all the way around on
          Williams' hit"
     2: the act of contacting one thing with another; "repeated
        hitting raised a large bruise"; "after three misses she
        finally got a hit" [syn: {hitting}, {striking}]
     3: a conspicuous success; "that song was his first hit and
        marked the beginning of his career"; "that new Broadway
        show is a real smasher"; "the party went with a bang"
        [syn: {smash}, {smasher}, {strike}, {bang}]
     4: (physics) an brief event in which two or more bodies come
        together; "the collision of the ...
