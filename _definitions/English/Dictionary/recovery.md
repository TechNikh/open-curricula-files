---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recovery
offline_file: ""
offline_thumbnail: ""
uuid: ef4dc725-cc43-42b2-a648-6bfc84af5400
updated: 1484310377
title: recovery
categories:
    - Dictionary
---
recovery
     n 1: return to an original state; "the recovery of the forest
          after the fire was surprisingly rapid"
     2: gradual healing (through rest) after sickness or injury
        [syn: {convalescence}, {recuperation}]
     3: the act of regaining or saving something lost (or in danger
        of becoming lost) [syn: {retrieval}]
