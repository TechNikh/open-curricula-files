---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/financial
offline_file: ""
offline_thumbnail: ""
uuid: f737d3d3-0277-44f1-afbe-4af3a6cda5a6
updated: 1484310453
title: financial
categories:
    - Dictionary
---
financial
     adj : involving financial matters; "fiscal responsibility" [syn: {fiscal}]
           [ant: {nonfinancial}]
