---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/near
offline_file: ""
offline_thumbnail: ""
uuid: 984fe382-4bb2-406e-a01e-c4f48d28bcbd
updated: 1484310330
title: near
categories:
    - Dictionary
---
near
     adj 1: not far distant in time or space or degree or circumstances;
            "near neighbors"; "in the near future"; "they are near
            equals"; "his nearest approach to success"; "a very
            near thing"; "a near hit by the bomb"; "she was near
            tears"; "she was close to tears"; "had a close call"
            [syn: {close}] [ant: {far}]
     2: being on the left side; "the near or nigh horse is the one
        on the left"; "the animal's left side is its near or nigh
        side" [syn: {near(a)}, {nigh(a)}]
     3: closely resembling the genuine article; "near beer"; "a
        dress of near satin"
     4: giving or spending with reluctance; "our ...
