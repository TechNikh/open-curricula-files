---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/decrease
offline_file: ""
offline_thumbnail: ""
uuid: 0b3aaf57-5d21-4aaf-9d4c-35123cae5e71
updated: 1484310327
title: decrease
categories:
    - Dictionary
---
decrease
     n 1: a change downward; "there was a decrease in his temperature
          as the fever subsided"; "there was a sharp drop-off in
          sales" [syn: {lessening}, {drop-off}] [ant: {increase}]
     2: a process of becoming smaller or shorter [syn: {decrement}]
        [ant: {increase}, {increase}]
     3: the amount by which something decreases [syn: {decrement}]
        [ant: {increase}]
     4: the act of decreasing or reducing something [syn: {diminution},
         {reduction}, {step-down}] [ant: {increase}]
     v 1: decrease in size, extent, or range; "The amount of homework
          decreased towards the end of the semester"; "The cabin
          pressure fell ...
