---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Marginal_stability
offline_file: ""
offline_thumbnail: ""
uuid: 306ec94c-b6d5-46be-aebc-91a6b203b78e
updated: 1484308828
title: Marginal stability
tags:
    - Continuous time
    - Discrete time
    - System response
categories:
    - Stability theory
---
In the theory of dynamical systems, and control theory, a linear time-invariant system is marginally stable if it is neither asymptotically stable nor unstable. Marginal stability is sometimes referred to as neutral stability.[1]
