---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Network_simplex_algorithm
offline_file: ""
offline_thumbnail: ""
uuid: 29666cb6-32d8-43ce-84b9-51dd089f22a1
updated: 1484309394
title: Network simplex algorithm
tags:
    - History
    - Overview
    - Applications
categories:
    - Computational problems in graph theory
---
In mathematical optimization, the network simplex algorithm is a graph theoretic specialization of the simplex algorithm. The algorithm is usually formulated in terms of a standard problem, minimum-cost flow problem and can be efficiently solved in polynomial time. The network simplex method works very well in practice, typically 200 to 300 times faster than the simplex method applied to general linear program of same dimensions.[citation needed]
