---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dead
offline_file: ""
offline_thumbnail: ""
uuid: 76698de5-5f29-43b5-a287-f7bd138ba325
updated: 1484310284
title: dead
categories:
    - Dictionary
---
dead
     adj 1: no longer having or seeming to have or expecting to have
            life; "the nerve is dead"; "a dead pallor"; "he was
            marked as a dead man by the assassin" [ant: {alive(p)}]
     2: not showing characteristics of life especially the capacity
        to sustain life; no longer exerting force or having energy
        or heat; "Mars is a dead planet"; "a dead battery"; "dead
        soil"; "dead coals"; "the fire is dead" [ant: {live}]
     3: very tired; "was all in at the end of the day"; "so beat I
        could flop down and go to sleep anywhere"; "bushed after
        all that exercise"; "I'm dead after that long trip" [syn:
        {all in(p)}, {beat(p)}, ...
