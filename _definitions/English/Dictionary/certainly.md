---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/certainly
offline_file: ""
offline_thumbnail: ""
uuid: cd27cd39-9cb5-4998-a7ee-23f110e71e48
updated: 1484310441
title: certainly
categories:
    - Dictionary
---
certainly
     adv : definitely or positively (`sure' is sometimes used
           informally for `surely'); "the results are surely
           encouraging"; "she certainly is a hard worker"; "it's
           going to be a good day for sure"; "they are coming, for
           certain"; "they thought he had been killed sure
           enough"; "he'll win sure as shooting"; "they sure smell
           good"; "sure he'll come" [syn: {surely}, {sure}, {for
           sure}, {for certain}, {sure enough}, {sure as shooting}]
