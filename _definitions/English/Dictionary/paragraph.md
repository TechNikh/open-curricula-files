---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/paragraph
offline_file: ""
offline_thumbnail: ""
uuid: 6c23e905-f2a2-48ed-92c9-2a12785396c2
updated: 1484310271
title: paragraph
categories:
    - Dictionary
---
paragraph
     n : one of several distinct subdivisions of a text intended to
         separate ideas; the beginning is usually marked by a new
         indented line
     v 1: divide into paragraphs, as of text; "This story is well
          paragraphed"
     2: write about in a paragraph; "All her friends were
        paragraphed in last Monday's paper"
     3: write paragraphs; work as a paragrapher
