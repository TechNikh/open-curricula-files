---
version: 1
type: video
provider: YouTube
id: F00H76WgxLA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d3b3cbd3-564c-4b54-b3b7-eb34a2bc7cf2
updated: 1486069618
title: Extending arithmetic sequences
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/F00H76WgxLA/default.jpg
    - https://i3.ytimg.com/vi/F00H76WgxLA/1.jpg
    - https://i3.ytimg.com/vi/F00H76WgxLA/2.jpg
    - https://i3.ytimg.com/vi/F00H76WgxLA/3.jpg
---
