---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nod
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484477581
title: nod
categories:
    - Dictionary
---
nod
     n 1: a sign of assent or salutation or command
     2: the act of nodding the head
     v 1: express or signify by nodding; "He nodded his approval"
     2: lower and raise the head, as to indicate assent or agreement
        or confirmation; "The teacher nodded when the student gave
        the right answer"
     3: let the head fall forward through drowsiness; "The old man
        was nodding in his chair"
     4: sway gently back and forth, as is in a nodding motion; "the
        flowers were nodding in the breeze"
     5: be almost asleep; "The old man sat nodding by the fireplace"
     [also: {nodding}, {nodded}]
