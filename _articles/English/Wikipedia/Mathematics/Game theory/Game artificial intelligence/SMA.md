---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/SMA*'
offline_file: ""
offline_thumbnail: ""
uuid: 6290c837-2e53-49ac-9406-70e82b53c1ff
updated: 1484309006
title: 'SMA*'
tags:
    - Process
    - Properties
    - Implementation
categories:
    - Game artificial intelligence
---
SMA* or Simplified Memory Bounded A* is a shortest path algorithm based on the A* algorithm. The main advantage of SMA* is that it uses a bounded memory, while the A* algorithm might need exponential memory. All other characteristics of SMA* are inherited from A*.
