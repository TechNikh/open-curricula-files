---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gdp
offline_file: ""
offline_thumbnail: ""
uuid: 697959ab-c85a-4767-90e2-57aa6158c342
updated: 1484308432
title: Gross domestic product
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/370px-Countries_by_GDP_%2528Nominal%2529_in_2014.svg.png'
tags:
    - Definition
    - History
    - Determining gross domestic product (GDP)
    - Production approach
    - Income approach
    - Expenditure approach
    - Components of GDP by expenditure
    - GDP vs GNI
    - International standards
    - National measurement
    - Nominal GDP and adjustments to GDP
    - Cross-border comparison and purchasing power parity
    - 'Standard of living and GDP: Wealth distribution and externalities'
    - Limitations and criticisms
    - Lists of countries by their GDP
    - Notes and references
    - Global
    - Data
    - Articles and books
categories:
    - Uncategorized
---
Gross domestic product (GDP) is a monetary measure of the market value of all final goods and services produced in a period (quarterly or yearly). Nominal GDP estimates are commonly used to determine the economic performance of a whole country or region, and to make international comparisons. Nominal GDP per capita does not, however, reflect differences in the cost of living and the inflation rates of the countries; therefore using a GDP PPP per capita basis is arguably more useful when comparing differences in living standards between nations.
