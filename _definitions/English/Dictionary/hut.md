---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hut
offline_file: ""
offline_thumbnail: ""
uuid: 392cda10-f835-4645-8a1c-04d9418b84aa
updated: 1484310486
title: hut
categories:
    - Dictionary
---
hut
     n 1: temporary military shelter [syn: {army hut}, {field hut}]
     2: small crude shelter used as a dwelling [syn: {hovel}, {hutch},
         {shack}, {shanty}]
