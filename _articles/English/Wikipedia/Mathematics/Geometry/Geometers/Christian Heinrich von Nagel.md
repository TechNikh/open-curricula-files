---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Christian_Heinrich_von_Nagel
offline_file: ""
offline_thumbnail: ""
uuid: f074c1b7-a422-4d23-bcf4-0c14ba4d0ae6
updated: 1484309093
title: Christian Heinrich von Nagel
categories:
    - Geometers
---
After attending the gymnasium, Nagel went in 1817 to Evangelical Seminaries of Maulbronn and Blaubeuren. From 1821 to 1825, he took a four-year course of theology at the Tübinger Stift. Soon after his graduation, he became interested in mathematics. He became mathematics and science teacher at the Lyceum and at the Secondary school in Tübingen. Already in 1826, he earned doctorate at the local Faculty of Philosophy on a theme De triangulis rectangulis ex algebraica aequatione construendis (About right triangles construable from an algebraic equation). Until 1830, he held post of a private lecturer in Tübingen. In that year, he moved to Ulm where he had a better-paid job as a teacher at ...
