---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Medak
offline_file: ""
offline_thumbnail: ""
uuid: 6688ea45-c3a8-4596-beb3-8ab6921e0fa0
updated: 1484308444
title: Medak
tags:
    - geography
    - History
    - Demographics
    - Governance
    - culture
    - Economy
    - Tourism
categories:
    - Uncategorized
---
Medak is a town in Medak district of the Indian state of Telangana. It is a municipality and the headquarters of Medak mandal in Medak revenue division.[3]
