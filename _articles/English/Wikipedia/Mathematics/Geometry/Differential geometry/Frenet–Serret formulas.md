---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Frenet%E2%80%93Serret_formulas'
offline_file: ""
offline_thumbnail: ""
uuid: 7426f2d8-6dcd-4278-bc03-82173974aff1
updated: 1484309221
title: Frenet–Serret formulas
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Frenet.png
tags:
    - Definitions
    - Formulas in n dimensions
    - Proof
    - Applications and interpretation
    - Kinematics of the frame
    - Graphical Illustrations
    - Frenet–Serret formulas in calculus
    - Taylor expansion
    - Ribbons and tubes
    - Congruence of curves
    - Other expressions of the frame
    - Special cases
    - Plane curves
    - Notes
categories:
    - Differential geometry
---
In differential geometry, the Frenet–Serret formulas describe the kinematic properties of a particle moving along a continuous, differentiable curve in three-dimensional Euclidean space ℝ3, or the geometric properties of the curve itself irrespective of any motion. More specifically, the formulas describe the derivatives of the so-called tangent, normal, and binormal unit vectors in terms of each other. The formulas are named after the two French mathematicians who independently discovered them: Jean Frédéric Frenet, in his thesis of 1847, and Joseph Alfred Serret in 1851. Vector notation and linear algebra currently used to write these formulas were not yet in use at the time of ...
