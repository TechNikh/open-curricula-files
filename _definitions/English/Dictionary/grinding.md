---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grinding
offline_file: ""
offline_thumbnail: ""
uuid: 356d8075-63be-43a0-a4e0-22e10d11b11d
updated: 1484310343
title: grinding
categories:
    - Dictionary
---
grinding
     n 1: matter resulting from the process of grinding; "vegetable
          grindings clogged the drain"
     2: a harsh and strident sound (as of the grinding of gears)
     3: the wearing down of rock particles by friction due to water
        or wind or ice [syn: {abrasion}, {attrition}, {detrition}]
