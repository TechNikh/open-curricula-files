---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Efficient_cake-cutting
offline_file: ""
offline_thumbnail: ""
uuid: 3a2df8e3-3b9b-4e3b-868a-33c017515794
updated: 1484308520
title: Efficient cake-cutting
tags:
    - Assumptions
    - Example cake
    - Efficiency
    - Combining efficiency and fairness
    - PEPR division
    - PEEF division
categories:
    - Fair division
---
Efficient cake-cutting is a problem in economics and computer science. It involves a heterogenous resource, such as a cake with different toppings or a land with different coverings, that is assumed to be divisible - it is possible to cut arbitrarily small pieces of it without destroying their value. The resource has to be divided among several partners who have different preferences over different parts of the cake, i.e., some people prefer the chocolate toppings, some prefer the cherries, some just want as large a piece as possible, etc. The division should be Pareto-efficient.
