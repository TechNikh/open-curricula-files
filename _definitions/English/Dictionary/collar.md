---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collar
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484493181
title: collar
categories:
    - Dictionary
---
collar
     n 1: a band that fits around the neck and is usually folded over
          [syn: {neckband}]
     2: a band of leather or rope that is placed around an animal's
        neck as a harness or to identify it
     3: necklace that fits tightly around a woman's neck [syn: {choker},
         {dog collar}, {neckband}]
     4: a figurative restraint; "asked for a collar on program
        trading in the stock market"; "kept a tight leash on his
        emotions"; "he's always gotten a long leash" [syn: {leash}]
     5: the act of apprehending (especially apprehending a
        criminal); "the policeman on the beat got credit for the
        collar" [syn: {apprehension}, {arrest}, ...
