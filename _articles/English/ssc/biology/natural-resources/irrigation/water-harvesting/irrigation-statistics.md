---
title: Irrigation statistics
article_ref: https://en.wikipedia.org/wiki/Irrigation_statistics
tags:
    - Irrigated area
    - Area per application method at field level
    - Areal growth
    - Water use
    - Economical significance
categories:
    - Water harvesting
---
