---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deteriorate
offline_file: ""
offline_thumbnail: ""
uuid: 05d9752f-0933-4ddd-841a-bf1552a00d4d
updated: 1484310547
title: deteriorate
categories:
    - Dictionary
---
deteriorate
     v 1: become worse or disintegrate; "His mind deteriorated"
     2: grow worse; "Her condition deteriorated"; "Conditions in the
        slums degenerated"; "The discussion devolved into a
        shouting match" [syn: {devolve}, {drop}, {degenerate}]
        [ant: {recuperate}]
