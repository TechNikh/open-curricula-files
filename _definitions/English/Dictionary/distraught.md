---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distraught
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484408341
title: distraught
categories:
    - Dictionary
---
distraught
     adj : deeply agitated especially from emotion; "distraught with
           grief" [syn: {overwrought}]
