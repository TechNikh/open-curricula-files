---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/participation
offline_file: ""
offline_thumbnail: ""
uuid: a928e269-6ff3-452e-9c01-5c580a594e41
updated: 1484310553
title: participation
categories:
    - Dictionary
---
participation
     n 1: the act of sharing in the activities of a group; "the
          teacher tried to increase his students' engagement in
          class activities" [syn: {engagement}, {involvement}, {involution}]
          [ant: {non-engagement}, {non-engagement}, {non-engagement}]
     2: the condition of sharing in common with others (as fellows
        or partners etc.) [syn: {involvement}]
