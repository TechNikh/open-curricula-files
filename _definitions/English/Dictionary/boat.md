---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/boat
offline_file: ""
offline_thumbnail: ""
uuid: 2b5a049c-bc6e-44d1-926e-b0ccc45cb077
updated: 1484310212
title: boat
categories:
    - Dictionary
---
boat
     n 1: a small vessel for travel on water
     2: a dish (often boat-shaped) for serving gravy or sauce [syn:
        {gravy boat}, {gravy holder}, {sauceboat}]
     v : ride in a boat on water
