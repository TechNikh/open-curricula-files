---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/academic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484330941
title: academic
categories:
    - Dictionary
---
academic
     adj 1: associated with academia or an academy; "the academic
            curriculum"; "academic gowns"
     2: hypothetical or theoretical and not expected to produce an
        immediate or practical result; "an academic discussion";
        "an academic question"
     3: marked by a narrow focus on or display of learning
        especially its trivial aspects [syn: {donnish}, {pedantic}]
     n : an educator who works at a college or university [syn: {academician},
          {faculty member}]
