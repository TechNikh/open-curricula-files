---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/worse
offline_file: ""
offline_thumbnail: ""
uuid: dbe21f9f-bdc2-47f7-84e0-e0a615c8a3bf
updated: 1484310459
title: worse
categories:
    - Dictionary
---
worse
     adj 1: (comparative of `bad') inferior to another in quality or
            condition or desirability; "this road is worse than
            the first one we took"; "the road is in worse shape
            than it was"; "she was accused of worse things than
            cheating and lying" [ant: {better}]
     2: changed for the worse in health or fitness; "I feel worse
        today"; "her cold is worse" [syn: {worsened}] [ant: {better}]
     n : something inferior in quality or condition or effect; "for
         better or for worse"; "accused of cheating and lying and
         worse"
     adv : (comparative of `ill') in a less effective or successful or
           desirable ...
