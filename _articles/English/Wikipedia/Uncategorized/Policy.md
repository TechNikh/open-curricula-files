---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Policymakers
offline_file: ""
offline_thumbnail: ""
uuid: cbb9dd68-2b10-4167-8652-326340c8fa92
updated: 1484308471
title: Policy
tags:
    - Impact
    - Intended effects
    - Unintended effects
    - Policy cycle
    - Content
    - Typologies
    - Distributive policies
    - Regulatory policies
    - Constituent policies
    - Miscellaneous policies
categories:
    - Uncategorized
---
A policy is a deliberate system of principles to guide decisions and achieve rational outcomes. A policy is a statement of intent, and is implemented as a procedure or protocol. Policies are generally adopted by the Board of or senior governance body within an organization where as procedures or protocols would be developed and adopted by senior executive officers. Policies can assist in both subjective and objective decision making. Policies to assist in subjective decision making would usually assist senior management with decisions that must consider the relative merits of a number of factors before making decisions and as a result are often hard to objectively test e.g. work-life ...
