---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refined
offline_file: ""
offline_thumbnail: ""
uuid: 1ab28a73-a961-4027-9c8d-29e76f0fca76
updated: 1484310411
title: refined
categories:
    - Dictionary
---
refined
     adj 1: used of persons and their behavior; cultivated and genteel;
            "she was delicate and refined and unused to hardship";
            "refined people with refined taste" [ant: {unrefined}]
     2: freed from impurities by processing; "refined sugar";
        "refined oil"; "to gild refined gold"- Shakespeare [syn: {processed}]
        [ant: {unrefined}]
     3: showing a high degree of refinement and the assurance that
        comes from wide social experience; "his polished manner";
        "maintained an urbane tone in his letters" [syn: {polished},
         {svelte}, {urbane}]
     4: made pure [syn: {purified}, {sublimate}]
     5: suggesting taste, ease, and ...
