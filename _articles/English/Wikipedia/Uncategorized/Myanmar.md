---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Myanmar
offline_file: ""
offline_thumbnail: ""
uuid: c7c059a0-6313-4e6e-b077-e2997cb9e880
updated: 1484308425
title: Myanmar
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pyu_Realm.png
tags:
    - Etymology
    - History
    - Prehistory
    - Early city-states
    - Imperial Burma
    - Taungoo and colonialism
    - British Burma (1824–1948)
    - Independence (1948–1962)
    - Military rule (1962–2011)
    - Civil wars
    - Democratic reforms
    - 2015 Myanmar general elections
    - geography
    - Administrative divisions
    - Climate
    - Environment
    - wildlife
    - Government and politics
    - Political culture
    - Foreign relations
    - Military
    - Human rights and internal conflicts
    - Child soldiers
    - >
        Child/forced/slave labour, systematic sexual violence and
        human trafficking
    - Genocide allegations and crimes against Rohingya people
    - Rohingya fleeing by boat
    - 2012 Rakhine State riots
    - Freedom of speech
    - Praise for the 2011 government reforms
    - 2013 onwards
    - Nuclear weapons programme
    - Economy
    - Economic history
    - Agriculture
    - Drug production
    - natural resources
    - Tourism
    - Economic sanctions
    - Government stakeholders in business
    - Economic liberalisation, post–2011
    - Units of measurement
    - Society
    - Demographics
    - Largest cities
    - Ethnic groups
    - Languages
    - religion
    - Health
    - Education
    - Crime
    - culture
    - Cuisine
    - art
    - Media and communications
    - Internet
    - Film
    - Sport
    - Notes
    - Bibliography
categories:
    - Uncategorized
---
Myanmar (Burmese pronunciation: [mjəmà]),[nb 1][5][6][7][8][9][10] officially the Republic of the Union of Myanmar and also known as Burma, is a sovereign state in South East Asia bordered by Bangladesh, India, China, Laos and Thailand. About one third of Myanmar's total perimeter of 5,876 km (3,651 miles), forms an uninterrupted coastline of 1,930 km (1,200 miles) along the Bay of Bengal and the Andaman Sea. The country's 2014 census revealed a much lower population than expected, with 51 million people recorded.[11] Myanmar is 676,578 square kilometres (261,227 sq mi) in size. Its capital city is Naypyidaw and its largest city is Yangon (Rangoon).[1]
