---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/treaty
offline_file: ""
offline_thumbnail: ""
uuid: af5fb4eb-1b4c-4672-b1d8-e774a563b6b2
updated: 1484310553
title: treaty
categories:
    - Dictionary
---
treaty
     n : a written agreement between two states or sovereigns [syn: {pact},
          {accord}]
