---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Diocles_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 17fee59d-ea9e-4b16-be21-13ead5421a5a
updated: 1484309077
title: Diocles (mathematician)
categories:
    - Geometers
---
Although little is known about the life of Diocles, it is known that he was a contemporary of Apollonius and that he flourished sometime around the end of the 3rd century BC and the beginning of the 2nd century BC.
