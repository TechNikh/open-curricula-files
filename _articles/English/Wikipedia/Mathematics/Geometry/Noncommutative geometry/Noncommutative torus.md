---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Noncommutative_torus
offline_file: ""
offline_thumbnail: ""
uuid: fc87ad9b-7e3e-4683-891d-4a0f7d5063fe
updated: 1484309292
title: Noncommutative torus
tags:
    - Definition
    - Alternative characterizations
    - Classification and K-theory
categories:
    - Noncommutative geometry
---
In mathematics, and more specifically in the theory of C*-algebras, the noncommutative tori Aθ, also known as irrational rotation algebras for irrational values of θ, form a family of noncommutative C*-algebras which generalize the algebra of continuous functions on the 2-torus. Many topological and geometric properties of the classical 2-torus have algebraic analogues for the noncommutative tori, and as such they are fundamental examples of a noncommutative space in the sense of Alain Connes.
