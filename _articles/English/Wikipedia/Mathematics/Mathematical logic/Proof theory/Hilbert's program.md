---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hilbert%27s_program'
offline_file: ""
offline_thumbnail: ""
uuid: b4669fc7-a959-4d9c-a480-051c826ab144
updated: 1484309495
title: "Hilbert's program"
tags:
    - "Statement of Hilbert's Program"
    - "Gödel's Incompleteness Theorems"
    - "Hilbert's program after Gödel"
categories:
    - Proof theory
---
In mathematics, Hilbert's program, formulated by German mathematician David Hilbert, was a proposed solution to the foundational crisis of mathematics, when early attempts to clarify the foundations of mathematics were found to suffer from paradoxes and inconsistencies. As a solution, Hilbert proposed to ground all existing theories to a finite, complete set of axioms, and provide a proof that these axioms were consistent. Hilbert proposed that the consistency of more complicated systems, such as real analysis, could be proven in terms of simpler systems. Ultimately, the consistency of all of mathematics could be reduced to basic arithmetic.
