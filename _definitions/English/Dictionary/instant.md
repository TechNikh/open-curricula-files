---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/instant
offline_file: ""
offline_thumbnail: ""
uuid: 9d519744-0b67-41a5-9e46-0e0d2a18fa46
updated: 1484310206
title: instant
categories:
    - Dictionary
---
instant
     adj 1: occurring with no delay; "relief was instantaneous";
            "instant gratification" [syn: {instantaneous}, {instant(a)}]
     2: in or of the present month; "your letter of the 10th inst"
        [syn: {inst}]
     3: demanding attention; "clamant needs"; "a crying need";
        "regarded literary questions as exigent and momentous"-
        H.L.Mencken; "insistent hunger"; "an instant need" [syn: {clamant},
         {crying}, {exigent}, {insistent}]
     n 1: a very short time (as the time it takes the eye blink or the
          heart to beat); "if I had the chance I'd do it in a
          flash" [syn: {blink of an eye}, {flash}, {heartbeat}, {jiffy},
           ...
