---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pest
offline_file: ""
offline_thumbnail: ""
uuid: 66581024-d4ec-4594-b676-61421f7b65fc
updated: 1484310277
title: pest
categories:
    - Dictionary
---
pest
     n : a persistently annoying person [syn: {blighter}, {cuss}, {pesterer},
          {gadfly}]
