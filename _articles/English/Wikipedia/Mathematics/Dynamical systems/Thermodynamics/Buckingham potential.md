---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Buckingham_potential
offline_file: ""
offline_thumbnail: ""
uuid: 2b3c877a-f47e-43a1-90c1-3862250f6e08
updated: 1484308840
title: Buckingham potential
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Coulomb-Buckingham_Potential.png
categories:
    - Thermodynamics
---
The Buckingham potential is a formula proposed by Richard Buckingham which describes the Pauli repulsion energy and van der Waals energy 
  
    
      
        
          Φ
          
            12
          
        
        (
        r
        )
      
    
    {\displaystyle \Phi _{12}(r)}
  
 for the interaction of two atoms that are not directly bonded as a function of the interatomic distance 
  
    
      
        r
      
    
    {\displaystyle r}
  
. It is a variety of interatomic potentials.
