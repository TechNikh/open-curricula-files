---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inaction
offline_file: ""
offline_thumbnail: ""
uuid: 08ed6e5f-9a8e-4a83-8944-ae386ac424c4
updated: 1484310178
title: inaction
categories:
    - Dictionary
---
inaction
     n 1: the state of being inactive [syn: {inactivity}, {inactiveness}]
          [ant: {action}, {action}, {action}]
     2: a state of no activity [syn: {inactiveness}]
