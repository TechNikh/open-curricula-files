---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/frequent
offline_file: ""
offline_thumbnail: ""
uuid: d6427cee-51aa-43da-8286-a24b3f81eac2
updated: 1484310192
title: frequent
categories:
    - Dictionary
---
frequent
     adj 1: coming at short intervals or habitually; "a frequent guest";
            "frequent complaints" [ant: {infrequent}]
     2: frequently encountered; "a frequent (or common) error is
        using the transitive verb `lay' for the intransitive
        `lie'";
     v 1: do one's shopping at; do business with; be a customer or
          client of [syn: {patronize}, {patronise}, {shop}, {shop
          at}, {buy at}, {sponsor}] [ant: {boycott}, {boycott}]
     2: be a regular or frequent visitor to a certain place; "She
        haunts the ballet" [syn: {haunt}]
