---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Artin%E2%80%93Verdier_duality'
offline_file: ""
offline_thumbnail: ""
uuid: 7f9e3022-0395-4433-825c-5aa53ffb7855
updated: 1484309231
title: Artin–Verdier duality
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ballerina-icon_12.jpg
categories:
    - Duality theories
---
In mathematics, Artin–Verdier duality is a duality theorem for constructible abelian sheaves over the spectrum of a ring of algebraic numbers, introduced by Artin and Verdier (1964), that generalizes Tate duality.
