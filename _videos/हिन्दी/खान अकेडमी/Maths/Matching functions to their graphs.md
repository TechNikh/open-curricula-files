---
version: 1
type: video
provider: YouTube
id: bXYLpZzl16k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b02bd2e7-d895-46d2-8298-4cb98aa8ebfa
updated: 1486069611
title: Matching functions to their graphs
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/bXYLpZzl16k/default.jpg
    - https://i3.ytimg.com/vi/bXYLpZzl16k/1.jpg
    - https://i3.ytimg.com/vi/bXYLpZzl16k/2.jpg
    - https://i3.ytimg.com/vi/bXYLpZzl16k/3.jpg
---
