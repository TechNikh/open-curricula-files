---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/create
offline_file: ""
offline_thumbnail: ""
uuid: f9368b7c-3b9d-405d-aec2-58a1a04824a2
updated: 1484310277
title: create
categories:
    - Dictionary
---
create
     v 1: make or cause to be or to become; "make a mess in one's
          office"; "create a furor" [syn: {make}]
     2: bring into existence; "The company was created 25 years
        ago"; "He created a new movement in painting"
     3: pursue a creative activity; be engaged in a creative
        activity; "Don't disturb him--he is creating"
     4: invest with a new title, office, or rank; "Create one a
        peer"
     5: create by artistic means; "create a poem"; "Schoenberg
        created twelve-tone music"; "Picasso created Cubism";
        "Auden made verses" [syn: {make}]
     6: create or manufacture a man-made product; "We produce more
        cars than we can sell"; ...
