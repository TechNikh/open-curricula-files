---
version: 1
type: video
provider: YouTube
id: DI8Yt1AQrH8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c0aef115-f8c4-4221-84b8-32bd068721ce
updated: 1486069656
title: 'Voltage multiplier- Generating over 100,000 volts DC'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/DI8Yt1AQrH8/default.jpg
    - https://i3.ytimg.com/vi/DI8Yt1AQrH8/1.jpg
    - https://i3.ytimg.com/vi/DI8Yt1AQrH8/2.jpg
    - https://i3.ytimg.com/vi/DI8Yt1AQrH8/3.jpg
---
