---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/motivating
offline_file: ""
offline_thumbnail: ""
uuid: 3e35580a-6a15-494f-8200-f28bad6d8e4f
updated: 1484310384
title: motivating
categories:
    - Dictionary
---
motivating
     adj : impelling to action; "it may well be that ethical language
           has primarily a motivative function"- Arthur Pap;
           "motive pleas"; "motivating arguments" [syn: {motivative(a)},
            {motive(a)}]
     n : the act of motivating; providing incentive [syn: {motivation}]
