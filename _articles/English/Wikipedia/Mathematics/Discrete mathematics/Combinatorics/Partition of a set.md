---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partition_of_a_set
offline_file: ""
offline_thumbnail: ""
uuid: db70da18-443c-4876-9476-78f718354daa
updated: 1484308603
title: Partition of a set
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Indiabundleware.jpg
tags:
    - Definition
    - Examples
    - Partitions and equivalence relations
    - Refinement of partitions
    - Noncrossing partitions
    - Counting partitions
    - Notes
categories:
    - Combinatorics
---
In mathematics, a partition of a set is a grouping of the set's elements into non-empty subsets, in such a way that every element is included in one and only one of the subsets.
