---
version: 1
type: video
provider: YouTube
id: 4AxZ-6MOznY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Structure%20of%20the%20Earth.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8620d572-bfab-42aa-9d3a-3d99c0baa823
updated: 1486069685
title: Structure of the Earth
categories:
    - Cosmology and Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/4AxZ-6MOznY/default.jpg
    - https://i3.ytimg.com/vi/4AxZ-6MOznY/1.jpg
    - https://i3.ytimg.com/vi/4AxZ-6MOznY/2.jpg
    - https://i3.ytimg.com/vi/4AxZ-6MOznY/3.jpg
---
