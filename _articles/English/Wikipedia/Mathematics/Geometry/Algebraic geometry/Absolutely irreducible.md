---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Absolutely_irreducible
offline_file: ""
offline_thumbnail: ""
uuid: 6e605bcd-93f2-413f-b5fd-863c53f64901
updated: 1484309130
title: Absolutely irreducible
categories:
    - Algebraic geometry
---
In mathematics, a multivariate polynomial defined over the rational numbers is absolutely irreducible if it is irreducible over the complex field.[1][2][3] For example, 
  
    
      
        
          x
          
            2
          
        
        +
        
          y
          
            2
          
        
        −
        1
      
    
    {\displaystyle x^{2}+y^{2}-1}
  
 is absolutely irreducible, but while 
  
    
      
        
          x
          
            2
          
        
        +
        
          y
          
            2
          
        
      
    
    {\displaystyle x^{2}+y^{2}}
  
 is irreducible over the integers and the reals, it is ...
