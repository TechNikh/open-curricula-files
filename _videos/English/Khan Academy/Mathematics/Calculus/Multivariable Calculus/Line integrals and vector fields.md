---
version: 1
type: video
provider: YouTube
id: t3cJYNdQLYg
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Line%20integrals%20and%20vector%20fields.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 333b293e-1e01-4758-ae5c-7fa73859bb2b
updated: 1486069696
title: Line integrals and vector fields
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/t3cJYNdQLYg/default.jpg
    - https://i3.ytimg.com/vi/t3cJYNdQLYg/1.jpg
    - https://i3.ytimg.com/vi/t3cJYNdQLYg/2.jpg
    - https://i3.ytimg.com/vi/t3cJYNdQLYg/3.jpg
---
