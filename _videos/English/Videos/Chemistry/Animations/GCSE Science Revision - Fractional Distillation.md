---
version: 1
type: video
provider: YouTube
id: KCs1F_44dy4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/GCSE%20Science%20Revision%20-%20Fractional%20Distillation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5a69fe42-a20a-4764-97ad-1d511fc9a095
updated: 1486069664
title: 'GCSE Science Revision - Fractional Distillation'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/KCs1F_44dy4/default.jpg
    - https://i3.ytimg.com/vi/KCs1F_44dy4/1.jpg
    - https://i3.ytimg.com/vi/KCs1F_44dy4/2.jpg
    - https://i3.ytimg.com/vi/KCs1F_44dy4/3.jpg
---
