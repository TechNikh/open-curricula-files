---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interpretation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484314321
title: interpretation
categories:
    - Dictionary
---
interpretation
     n 1: a mental representation of the meaning or significance of
          something [syn: {reading}, {version}]
     2: the act of interpreting something as expressed in an
        artistic performance; "her rendition of Milton's verse was
        extraordinarily moving" [syn: {rendition}, {rendering}]
     3: an explanation that results from interpreting something;
        "the report included his interpretation of the forensic
        evidence"
     4: an explanation of something that is not immediately obvious;
        "the edict was subject to many interpretations"; "he
        annoyed us with his interpreting of parables"; "often
        imitations are extended to ...
