---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convex_geometry
offline_file: ""
offline_thumbnail: ""
uuid: f7e71a5d-5d9e-4437-bf8c-509086701f62
updated: 1484309189
title: Convex geometry
tags:
    - Classification
    - Historical note
    - Notes
categories:
    - Convex geometry
---
In mathematics, convex geometry is the branch of geometry studying convex sets, mainly in Euclidean space. Convex sets occur naturally in many areas: computational geometry, convex analysis, discrete geometry, functional analysis, geometry of numbers, integral geometry, linear programming, probability theory, etc.
