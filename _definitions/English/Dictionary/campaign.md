---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/campaign
offline_file: ""
offline_thumbnail: ""
uuid: 0e3333b3-89f2-43be-ba34-7147b10bf64d
updated: 1484310569
title: campaign
categories:
    - Dictionary
---
campaign
     n 1: a race between candidates for elective office; "I managed
          his campaign for governor"; "he is raising money for a
          Senate run" [syn: {political campaign}, {run}]
     2: a series of actions advancing a principle or tending toward
        a particular end; "he supported populist campaigns"; "they
        worked in the cause of world peace"; "the team was ready
        for a drive toward the pennant"; "the movement to end
        slavery"; "contributed to the war effort" [syn: {cause}, {crusade},
         {drive}, {movement}, {effort}]
     3: several related operations aimed at achieving a particular
        goal (usually within geographical and temporal
 ...
