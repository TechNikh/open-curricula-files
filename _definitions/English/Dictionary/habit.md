---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/habit
offline_file: ""
offline_thumbnail: ""
uuid: 4c5cd990-df97-468e-90af-33cb2fd88adc
updated: 1484310537
title: habit
categories:
    - Dictionary
---
habit
     n 1: an established custom; "it was their habit to dine at 7
          every evening" [syn: {wont}]
     2: a pattern of behavior acquired through frequent repetition;
        "she had a habit twirling the ends of her hair"; "long use
        had hardened him to it" [syn: {use}, {wont}]
     3: (religion) a distinctive attire (as the costume of a
        religious order)
     4: excessive use of drugs [syn: {substance abuse}, {drug abuse}]
     v : put a habit on
