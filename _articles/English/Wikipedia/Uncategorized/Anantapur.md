---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anantapur
offline_file: ""
offline_thumbnail: ""
uuid: 098e1dfc-170c-4dbe-b776-81be8245b1ec
updated: 1484308444
title: Anantapur
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sapatgiri_Circle.jpg
tags:
    - geography
    - Climate
    - Demographics
    - Governance
    - Transport
    - culture
    - Cuisine
    - Cityscape
    - Education
    - Sports
categories:
    - Uncategorized
---
Anantapur is a city in Anantapur district of the Indian state of Andhra Pradesh. It is the mandal headquarters of Anantapur mandal and also the divisional headquarters of Anantapur revenue division.[2] The city is located on National Highway 44. It was also the headquarters of the Datta Mandalam (Rayalaseema districts of Andhra Pradesh and Bellary district of Karnataka) in 1799. It was also a position of strategic importance for the British Indian Army during the Second World War.
