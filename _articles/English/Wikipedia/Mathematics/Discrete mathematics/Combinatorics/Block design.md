---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Block_design
offline_file: ""
offline_thumbnail: ""
uuid: b990fed0-4431-4988-a848-240e0c5ca4b9
updated: 1484308573
title: Block design
tags:
    - Definition of a BIBD (or 2-design)
    - Symmetric BIBDs
    - Projective planes
    - Biplanes
    - Hadamard 2-designs
    - Resolvable 2-designs
    - 'Generalization: t-designs'
    - Derived and extendable t-designs
    - Inversive planes
    - Steiner systems
    - Partially balanced designs (PBIBDs)
    - Example
    - Properties
    - Two associate class PBIBDs
    - Applications
    - Notes
categories:
    - Combinatorics
---
In combinatorial mathematics, a block design is a set together with a family of subsets (repeated subsets are allowed at times) whose members are chosen to satisfy some set of properties that are deemed useful for a particular application. These applications come from many areas, including experimental design, finite geometry, software testing, cryptography, and algebraic geometry. Many variations have been examined, but the most intensely studied are the balanced incomplete block designs (BIBDs or 2-designs) which historically were related to statistical issues in the design of experiments.[1][2]
