---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/familiar
offline_file: ""
offline_thumbnail: ""
uuid: 39a9fe2e-f696-4001-980c-e8802b3205ea
updated: 1484310273
title: familiar
categories:
    - Dictionary
---
familiar
     adj 1: well known or easily recognized; "a familiar figure";
            "familiar songs"; "familiar guests" [ant: {unfamiliar}]
     2: within normal everyday experience; common and ordinary; not
        strange; "familiar ordinary objects found in every home";
        "a familiar everyday scene"; "a familiar excuse"; "a day
        like any other filled with familiar duties and
        experiences" [ant: {strange}]
     3: (usually followed by `with') well informed about or knowing
        thoroughly; "conversant with business trends"; "familiar
        with the complex machinery"; "he was familiar with those
        roads" [syn: {conversant(p)}, {familiar(p)}]
     4: ...
