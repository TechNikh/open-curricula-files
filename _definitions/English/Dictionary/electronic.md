---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/electronic
offline_file: ""
offline_thumbnail: ""
uuid: f9e5a638-6be9-4371-9040-40825ab2ccb5
updated: 1484310200
title: electronic
categories:
    - Dictionary
---
electronic
     adj 1: of or relating to electronics; concerned with or using
            devices that operate on principles governing the
            behavior of electrons; "electronic devices"
     2: of or concerned with electrons; "electronic energy"
