---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guerrilla
offline_file: ""
offline_thumbnail: ""
uuid: c33064b5-9b3a-4684-b85d-10034d52ef7f
updated: 1484310172
title: guerrilla
categories:
    - Dictionary
---
guerrilla
     adj : used of independent armed resistance forces; "guerrilla
           warfare"; "partisan forces" [syn: {guerrilla(a)}, {guerilla(a)},
            {underground}, {irregular}]
     n : a member of an irregular armed force that fights a stronger
         force by sabotage and harassment [syn: {guerilla}, {irregular},
          {insurgent}]
