---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Clobber
offline_file: ""
offline_thumbnail: ""
uuid: 930525b4-2f14-4e0e-a562-2d1227edc0bd
updated: 1484309022
title: Clobber
tags:
    - Details
    - Variants
categories:
    - Combinatorial game theory
---
Clobber is an abstract strategy game invented in 2001 by combinatorial game theorists Michael H. Albert, J.P. Grossman and Richard Nowakowski. It has subsequently been studied by Elwyn Berlekamp and Erik Demaine among others. Since 2005, it has been one of the events in the Computer Olympiad.
