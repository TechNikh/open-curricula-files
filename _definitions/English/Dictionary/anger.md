---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/anger
offline_file: ""
offline_thumbnail: ""
uuid: a699e30a-404e-44eb-8172-8d8c7c30eb9b
updated: 1484310577
title: anger
categories:
    - Dictionary
---
anger
     n 1: a strong emotion; a feeling that is oriented toward some
          real or supposed grievance [syn: {choler}, {ire}]
     2: the state of being angry [syn: {angriness}]
     3: belligerence aroused by a real or supposed wrong
        (personified as one of the deadly sins) [syn: {wrath}, {ire},
         {ira}]
     v 1: make angry; "The news angered him"
     2: become angry; "He angers easily" [syn: {see red}]
