---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/popularisation
offline_file: ""
offline_thumbnail: ""
uuid: 9d74fab1-0bae-4664-8265-f8292526ab50
updated: 1484310168
title: popularisation
categories:
    - Dictionary
---
popularisation
     n 1: an interpretation that easily understandable and acceptable
          [syn: {popularization}]
     2: the act of making something attractive to the general public
        [syn: {popularization}, {vulgarization}, {vulgarisation}]
