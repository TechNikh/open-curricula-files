---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saponification
offline_file: ""
offline_thumbnail: ""
uuid: ea5fec9c-f396-4bc1-b977-a01e9a2b8216
updated: 1484310426
title: saponification
categories:
    - Dictionary
---
saponification
     n : a chemical reaction in which an ester is heated with an
         alkali (especially the alkaline hydrolysis of a fat or
         oil to make soap)
