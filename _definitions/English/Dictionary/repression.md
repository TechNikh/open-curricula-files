---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/repression
offline_file: ""
offline_thumbnail: ""
uuid: 4d33f186-5158-4217-828a-b903ba45b000
updated: 1484310609
title: repression
categories:
    - Dictionary
---
repression
     n 1: a state of forcible subjugation; "the long repression of
          Christian sects"
     2: (psychiatry) the classical defense mechanism that protects
        you from impulses or ideas that would cause anxiety by
        preventing them from becoming conscious
     3: the act of repressing; control by holding down; "his goal
        was the repression of insolence"
