---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unfortunately
offline_file: ""
offline_thumbnail: ""
uuid: 8db67f85-b065-45e1-a13c-c94bb199990a
updated: 1484310252
title: unfortunately
categories:
    - Dictionary
---
unfortunately
     adv : by bad luck; "unfortunately it rained all day"; "alas, I
           cannot stay" [syn: {unluckily}, {regrettably}, {alas}]
           [ant: {fortunately}, {fortunately}]
