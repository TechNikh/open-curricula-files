---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assorted
offline_file: ""
offline_thumbnail: ""
uuid: ac8c97f1-5170-412a-9478-976569b8a83c
updated: 1484310277
title: assorted
categories:
    - Dictionary
---
assorted
     adj 1: consisting of a haphazard assortment of different kinds
            (even to the point of incongruity); "an arrangement of
            assorted spring flowers"; "assorted sizes";
            "miscellaneous accessories"; "a mixed program of
            baroque and contemporary music"; "a motley crew";
            "sundry sciences commonly known as social"-
            I.A.Richards [syn: {miscellaneous}, {mixed}, {motley},
             {sundry(a)}]
     2: of many different kinds purposefully arranged but lacking
        any uniformity; "assorted sizes"; "his disguises are many
        and various"; "various experiments have failed to disprove
        the theory"; "cited ...
