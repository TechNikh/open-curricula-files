---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plug
offline_file: ""
offline_thumbnail: ""
uuid: 4798f843-9bf4-47aa-9d04-3f186f98dbcb
updated: 1484310383
title: plug
categories:
    - Dictionary
---
plug
     n 1: blockage consisting of an object designed to fill a hole
          tightly [syn: {stopper}, {stopple}]
     2: a wad of something chewable as tobacco [syn: {chew}, {chaw},
         {cud}, {quid}, {wad}]
     3: blatant or sensational promotion [syn: {ballyhoo}, {hoopla},
         {hype}]
     4: electrical device that fits into the cylinder head of an
        internal-combustion engine and ignites the gas by means of
        an electric spark [syn: {spark plug}, {sparking plug}]
     5: an electrical device with two or three pins that is inserted
        in a socket to make an electrical connection [syn: {male
        plug}]
     6: an upright hydrant for drawing water to use ...
