---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/regulatory
offline_file: ""
offline_thumbnail: ""
uuid: 3a5a0cbf-5507-4df4-8dd7-e5a4c1081217
updated: 1484310545
title: regulatory
categories:
    - Dictionary
---
regulatory
     adj : restricting according to rules or principles; "a regulatory
           gene" [syn: {regulative}]
