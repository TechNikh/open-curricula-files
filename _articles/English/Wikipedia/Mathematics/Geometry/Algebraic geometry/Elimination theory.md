---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Elimination_theory
offline_file: ""
offline_thumbnail: ""
uuid: 90cc4acb-89e1-489e-98c0-7befe955da36
updated: 1484309142
title: Elimination theory
tags:
    - Connection to modern theories
    - History
    - Connection with logic
categories:
    - Algebraic geometry
---
In commutative algebra and algebraic geometry, elimination theory is the classical name for algorithmic approaches to eliminating some variables between polynomials of several variables.
