---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/genotype
offline_file: ""
offline_thumbnail: ""
uuid: ab72848c-e954-4cb0-88f6-505198528276
updated: 1484310299
title: genotype
categories:
    - Dictionary
---
genotype
     n 1: a group of organisms sharing a specific genetic constitution
     2: the particular alleles at specified loci present in an
        organism [syn: {genetic constitution}]
