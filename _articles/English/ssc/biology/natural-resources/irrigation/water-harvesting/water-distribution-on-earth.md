---
title: Water distribution on Earth
article_ref: https://en.wikipedia.org/wiki/Water_distribution_on_Earth
tags:
    - Distribution of saline and fresh water
    - Distribution of river water
    - Area, Volume, and Depth of the World Ocean
    - Variability of water availability
    - Water in Earth's mantle
categories:
    - Water harvesting
---
