---
version: 1
type: video
provider: YouTube
id: mfgCcFXUZRk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Electron%20Transport%20Chain.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cc62a80b-b768-4706-b81e-cf083ee04cb2
updated: 1486069583
title: Electron Transport Chain
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/mfgCcFXUZRk/default.jpg
    - https://i3.ytimg.com/vi/mfgCcFXUZRk/1.jpg
    - https://i3.ytimg.com/vi/mfgCcFXUZRk/2.jpg
    - https://i3.ytimg.com/vi/mfgCcFXUZRk/3.jpg
---
Overview of the Electron Transport Chain
More free lessons at: http://www.khanacademy.org/video?v=mfgCcFXUZRk
