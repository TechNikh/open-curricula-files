---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spillage
offline_file: ""
offline_thumbnail: ""
uuid: f09e98ae-4e1b-486a-b3c3-f3ceee8e005d
updated: 1484310579
title: spillage
categories:
    - Dictionary
---
spillage
     n 1: the amount that has spilled
     2: the act of allowing a fluid to escape [syn: {spill}, {release}]
