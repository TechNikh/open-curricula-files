---
version: 1
type: video
provider: YouTube
id: MJu9dL7a3ZI
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/DNA%20-%20Episode%203%20of%205%20-%20The%20Human%20Race%20-%20PBS%20Documentary.webm"
offline_file: ""
offline_thumbnail: ""
uuid: be52513a-e49b-465c-a0f2-192aaf976ee3
updated: 1486069673
title: 'DNA - Episode 3 of 5 - The Human Race - PBS Documentary'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/MJu9dL7a3ZI/default.jpg
    - https://i3.ytimg.com/vi/MJu9dL7a3ZI/1.jpg
    - https://i3.ytimg.com/vi/MJu9dL7a3ZI/2.jpg
    - https://i3.ytimg.com/vi/MJu9dL7a3ZI/3.jpg
---
