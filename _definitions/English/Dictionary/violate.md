---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/violate
offline_file: ""
offline_thumbnail: ""
uuid: b5b3ffc3-eb04-42ba-b2ab-61f239ff4781
updated: 1484310192
title: violate
categories:
    - Dictionary
---
violate
     v 1: fail to agree with; be in violation of; as of rules or
          patterns; "This sentence violates the rules of syntax"
          [syn: {go against}, {break}] [ant: {conform to}]
     2: act in disregard of laws and rules; "offend all laws of
        humanity"; "violate the basic laws or human civilization";
        "break a law" [syn: {transgress}, {offend}, {infract}, {go
        against}, {breach}, {break}]
     3: destroy; "Don't violate my garden"; "violate my privacy"
     4: violate the sacred character of a place or language;
        "desecrate a cemetary"; "violate the sanctity of the
        church"; "profane the name of God" [syn: {desecrate}, {profane},
        ...
