---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gentzen%27s_consistency_proof'
offline_file: ""
offline_thumbnail: ""
uuid: fd3856b8-8ad3-41b6-b79f-739d56211347
updated: 1484309495
title: "Gentzen's consistency proof"
tags:
    - "Gentzen's theorem"
    - "Relation to Hilbert's program and Gödel's theorem"
    - Other consistency proofs of arithmetic
    - "Work initiated by Gentzen's proof"
    - Notes
categories:
    - Proof theory
---
Gentzen's consistency proof is a result of proof theory in mathematical logic, published by Gerhard Gentzen in 1936. It shows that the Peano axioms of first-order arithmetic do not contain a contradiction (i.e. are "consistent"), as long as a certain other system used in the proof does not contain any contradictions either. This other system, today called "primitive recursive arithmetic with the additional principle of quantifier-free transfinite induction up to the ordinal ε0", is neither weaker nor stronger than the system of Peano axioms. Gentzen argued that it avoids the questionable modes of inference contained in Peano arithmetic and that its consistency is therefore less ...
