---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Voltage_graph
offline_file: ""
offline_thumbnail: ""
uuid: 1df49ac6-f4b7-4a8b-a143-822c9dc77807
updated: 1484309406
title: Voltage graph
tags:
    - Definition
    - The derived graph
    - Examples
    - Notes
categories:
    - Extensions and generalizations of graphs
---
In graph-theoretic mathematics, a voltage graph is a directed graph whose edges are labelled invertibly by elements of a group. It is formally identical to a gain graph, but it is generally used in topological graph theory as a concise way to specify another graph called the derived graph of the voltage graph.
