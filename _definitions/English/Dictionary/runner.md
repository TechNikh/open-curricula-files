---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/runner
offline_file: ""
offline_thumbnail: ""
uuid: 3aaedb13-c114-4dfb-999d-30cd875fd534
updated: 1484310284
title: runner
categories:
    - Dictionary
---
runner
     n 1: someone who imports or exports without paying duties [syn: {smuggler},
           {contrabandist}, {moon curser}, {moon-curser}]
     2: someone who travels on foot by running
     3: a person who is employed to deliver messages or documents;
        "he sent a runner over with the contract"
     4: a baseball player on the team at bat who is on base (or
        attempting to reach a base) [syn: {base runner}]
     5: a horizontal branch from the base of plant that produces new
        plants from buds at its tips [syn: {stolon}, {offset}]
     6: a trained athlete who competes in foot races
     7: a long narrow carpet
     8: device consisting of the parts on which ...
