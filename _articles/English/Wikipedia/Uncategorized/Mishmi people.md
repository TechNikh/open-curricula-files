---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mishmi
offline_file: ""
offline_thumbnail: ""
uuid: c50fad7f-fe32-4fee-91e7-f9a0697b1605
updated: 1484308415
title: Mishmi people
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Idu_Mishmi_diorama.JPG
tags:
    - In China
    - Origins
categories:
    - Uncategorized
---
The Mishmi or Deng people of Tibet and Arunachal Pradesh are an ethnic group comprising mainly three tribes: Idu Mishmi (Idu Lhoba); Digaro tribe (Taraon, Darang Deng), and Miju Mishmi (Kaman Deng). The Mishmis occupy the northeastern tip of the central Arunachal Pradesh in Upper and Lower Dibang Valley, Lohit and Anjaw Districts/Medog County. The three sub-divisions of the tribe emerged due to the geographical distribution, but racially all the three groups are of the same stock. The Idu are also known as Yidu Lhoba in Tibet and often referred as Chulikatas in Assam. The Idus are primarily concentrated in the Upper Dibang Valley and Lower Dibang Valley district and parts of the northern ...
