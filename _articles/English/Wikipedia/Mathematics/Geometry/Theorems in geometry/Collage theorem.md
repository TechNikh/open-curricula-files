---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Collage_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 78e8a3b1-9f22-4a07-9ea2-eba67ac3136a
updated: 1484309111
title: Collage theorem
tags:
    - Statement of the theorem
categories:
    - Theorems in geometry
---
In mathematics, the collage theorem characterises an iterated function system whose attractor is close, relative to the Hausdorff metric, to a given set. The IFS described is composed of contractions whose images, as a collage or union when mapping the given set, are arbitrarily close to the given set. It is typically used in fractal compression.
