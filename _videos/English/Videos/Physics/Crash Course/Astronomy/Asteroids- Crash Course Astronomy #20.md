---
version: 1
type: video
provider: YouTube
id: auxpcdQimCs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Asteroids-%20Crash%20Course%20Astronomy%20%2320.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 20af993c-775d-4321-a7e1-6f841a6b1d5e
updated: 1486069686
title: 'Asteroids- Crash Course Astronomy #20'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/auxpcdQimCs/default.jpg
    - https://i3.ytimg.com/vi/auxpcdQimCs/1.jpg
    - https://i3.ytimg.com/vi/auxpcdQimCs/2.jpg
    - https://i3.ytimg.com/vi/auxpcdQimCs/3.jpg
---
