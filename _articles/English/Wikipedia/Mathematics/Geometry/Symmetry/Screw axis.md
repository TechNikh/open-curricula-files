---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Screw_axis
offline_file: ""
offline_thumbnail: ""
uuid: bb1d08d1-72f7-4961-a0e1-d72e7575bebe
updated: 1484309313
title: Screw axis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pure_screw.svg.png
tags:
    - History
    - Screw axis symmetry
    - Screw axis of a spatial displacement
    - Geometric argument
    - Computing a point on the screw axis
    - Dual quaternion
    - Mechanics
    - Biomechanics
categories:
    - Symmetry
---
A screw axis (helical axis or twist axis) is a line that is simultaneously the axis of rotation and the line along which translation of a body occurs. Chasles' theorem shows that each Euclidean displacement in three-dimensional space has a screw axis, and the displacement can be decomposed into a rotation about and a slide along this screw axis.[1][2]
