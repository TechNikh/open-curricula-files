---
version: 1
type: video
provider: YouTube
id: z__LVQSZXqQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 89c1a76c-5c0e-4b0f-94cb-e128aa46014a
updated: 1486069669
title: Model of Nuclear Pore Complex transport selectivity
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/z__LVQSZXqQ/default.jpg
    - https://i3.ytimg.com/vi/z__LVQSZXqQ/1.jpg
    - https://i3.ytimg.com/vi/z__LVQSZXqQ/2.jpg
    - https://i3.ytimg.com/vi/z__LVQSZXqQ/3.jpg
---
