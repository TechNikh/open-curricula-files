---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Julius_Petersen
offline_file: ""
offline_thumbnail: ""
uuid: 997c6a8d-49a2-46c8-a315-15f386327506
updated: 1484309473
title: Julius Petersen
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Petersen_Julius.jpg
tags:
    - Biography
    - Early life and education
    - Work
    - Last years
categories:
    - Graph theorists
---
Julius Peter Christian Petersen (16 June 1839, Sorø, West Zealand – 5 August 1910, Copenhagen) was a Danish mathematician. His contributions to the field of mathematics led to the birth of graph theory.
