---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/laugh
offline_file: ""
offline_thumbnail: ""
uuid: 315f75f6-48cf-43fa-bc08-6e48794b6620
updated: 1484310146
title: laugh
categories:
    - Dictionary
---
laugh
     n 1: the sound of laughing [syn: {laughter}]
     2: a facial expression characteristic of a person laughing;
        "his face wrinkled in a silent laugh of derision"
     3: a humorous anecdote or remark intended to provoke laughter;
        "he told a very funny joke"; "he knows a million gags";
        "thanks for the laugh"; "he laughed unpleasantly at hisown
        jest"; "even a schoolboy's jape is supposed to have some
        ascertainable point" [syn: {joke}, {gag}, {jest}, {jape}]
     v : produce laughter [syn: {express joy}, {express mirth}] [ant:
          {cry}]
