---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Animal_products
offline_file: ""
offline_thumbnail: ""
uuid: 718e2e07-da2b-4460-a841-e8472efeff06
updated: 1484308375
title: Animal product
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-%2527Duck%252C_Duck%252C_Duck%2527.png'
tags:
    - Slaughterhouse waste
    - Food
    - Non-food animal products
    - Notes
categories:
    - Uncategorized
---
An animal product is any material derived from the body of an animal. Examples are fat, flesh, blood, milk, eggs, and lesser known products, such as isinglass and rennet.[1]
