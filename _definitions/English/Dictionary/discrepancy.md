---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discrepancy
offline_file: ""
offline_thumbnail: ""
uuid: 99c16a50-f2da-4a67-8f48-762b4165c074
updated: 1484310407
title: discrepancy
categories:
    - Dictionary
---
discrepancy
     n 1: a difference between conflicting facts or claims or
          opinions; "a growing divergence of opinion" [syn: {disagreement},
           {divergence}, {variance}]
     2: an event that departs from expectations [syn: {variance}, {variant}]
