---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/non-linear
offline_file: ""
offline_thumbnail: ""
uuid: 052f89b1-dc01-4b36-bf78-b561048859b2
updated: 1484310200
title: non-linear
categories:
    - Dictionary
---
nonlinear
     adj : designating or involving an equation whose terms are not of
           the first degree [ant: {linear}]
