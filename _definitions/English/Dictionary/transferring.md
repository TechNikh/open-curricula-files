---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transferring
offline_file: ""
offline_thumbnail: ""
uuid: 389dec96-5c41-49cc-a3b6-129e0bf0ad30
updated: 1484310337
title: transferring
categories:
    - Dictionary
---
transfer
     n 1: the act of transporting something from one location to
          another [syn: {transportation}, {transferral}, {conveyance}]
     2: someone who transfers or is transferred from one position to
        another; "the best student was a transfer from LSU" [syn:
        {transferee}]
     3: the act of transfering something from one form to another;
        "the transfer of the music from record to tape suppressed
        much of the background noise" [syn: {transference}]
     4: a ticket that allows a passenger to change conveyances
     5: application of a skill learned in one situation to a
        different but similar situation [syn: {transfer of
        training}, ...
