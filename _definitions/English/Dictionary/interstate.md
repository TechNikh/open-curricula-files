---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interstate
offline_file: ""
offline_thumbnail: ""
uuid: 679835cd-7023-4854-818e-a86cf78bcc87
updated: 1484310238
title: interstate
categories:
    - Dictionary
---
interstate
     adj : involving and relating to the mutual relations of states
           especially of the US; "Interstate Highway Commission";
           "interstate highways"; "Interstate Commerce
           Commission"; "interstate commerce" [ant: {intrastate}]
