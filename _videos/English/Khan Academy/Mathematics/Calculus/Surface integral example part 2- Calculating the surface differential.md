---
version: 1
type: video
provider: YouTube
id: tyVCA_8MUV4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Surface%20integral%20example%20part%202-%20Calculating%20the%20surface%20differential.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fc5ba815-d348-4cbf-8e87-7904d575495c
updated: 1486069690
title: 'Surface integral example part 2- Calculating the surface differential'
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/tyVCA_8MUV4/default.jpg
    - https://i3.ytimg.com/vi/tyVCA_8MUV4/1.jpg
    - https://i3.ytimg.com/vi/tyVCA_8MUV4/2.jpg
    - https://i3.ytimg.com/vi/tyVCA_8MUV4/3.jpg
---
