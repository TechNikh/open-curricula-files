---
version: 1
type: video
provider: YouTube
id: vD83JsfXq-s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2c465146-6569-4aa5-ac35-664563fd7ee2
updated: 1486069623
title: 'Math Trick- Multiply Any 3-Digit Number By 7x11x13 In Your Head'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/vD83JsfXq-s/default.jpg
    - https://i3.ytimg.com/vi/vD83JsfXq-s/1.jpg
    - https://i3.ytimg.com/vi/vD83JsfXq-s/2.jpg
    - https://i3.ytimg.com/vi/vD83JsfXq-s/3.jpg
---
