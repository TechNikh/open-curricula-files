---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/profitable
offline_file: ""
offline_thumbnail: ""
uuid: 56682234-284a-4eba-94ed-81dc7f20c56d
updated: 1484310258
title: profitable
categories:
    - Dictionary
---
profitable
     adj 1: yielding material gain or profit; "profitable speculation on
            the stock market" [ant: {unprofitable}]
     2: promoting benefit or gain; "a profitable meeting to resolve
        difficulties"
     3: providing profit; "a profitable conversation"
     4: productive of profit; "a profitable enterprise"; "a fruitful
        meeting" [syn: {fruitful}]
