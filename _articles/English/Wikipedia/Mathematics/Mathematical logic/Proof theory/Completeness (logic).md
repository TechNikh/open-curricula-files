---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Completeness_(logic)
offline_file: ""
offline_thumbnail: ""
uuid: 16100728-606c-4a2f-9883-fc0e1700234e
updated: 1484309490
title: Completeness (logic)
tags:
    - Other properties related to completeness
    - Forms of completeness
    - Expressive completeness
    - Functional completeness
    - Semantic completeness
    - Strong completeness
    - Refutation completeness
    - Syntactical completeness
    - Structural completeness
categories:
    - Proof theory
---
In mathematical logic and metalogic, a formal system is called complete with respect to a particular property if every formula having the property can be derived using that system, i.e. is one of its theorems; otherwise the system is said to be incomplete. The term "complete" is also used without qualification, with differing meanings depending on the context, mostly referring to the property of semantical validity. Intuitively, a system is called complete in this particular sense, if it can derive every formula that is true. Kurt Gödel, Leon Henkin, and Emil Leon Post all published proofs of completeness. (See History of the Church–Turing thesis.)
