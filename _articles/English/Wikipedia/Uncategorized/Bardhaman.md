---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bardhaman
offline_file: ""
offline_thumbnail: ""
uuid: 3bb11c12-f8d6-41f7-91b9-b25d62c98e43
updated: 1484308456
title: Bardhaman
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/181px-Sarbamangala_temple.jpg
tags:
    - geography
    - History
    - Demographics
    - culture
    - Places of interest
    - Locality In Bardhaman
    - Restaurants in Burdwan
    - Shopping Malls in Bardhaman
    - Cinema Halls
    - Foods
    - Transport
    - Road
    - Rail
    - Rickshaws
    - Education
    - University
    - Colleges
    - schools
    - Climate
categories:
    - Uncategorized
---
Bardhaman (Pron: ˈbɑ:dəˌmən) (Bengali): বর্ধমান, (Hindi): बर्दवान is a city of West Bengal state in eastern India. It is the headquarters of Bardhaman district, having become a district capital during the period of British rule. Burdwan, an alternative name for the city, has remained in use since that period.
