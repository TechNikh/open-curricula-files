---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/baby
offline_file: ""
offline_thumbnail: ""
uuid: 17adee30-75c0-4edc-99a0-8cf43b75f7b3
updated: 1484310297
title: baby
categories:
    - Dictionary
---
baby
     n 1: a very young child (birth to 1 year) who has not yet begun
          to walk or talk; "isn't she too young to have a baby?"
          [syn: {babe}, {infant}]
     2: sometimes used as a term of address for attractive young
        women [syn: {sister}]
     3: a very young mammal; "baby rabbits"
     4: the youngest member of a group (not necessarily young); "the
        baby of the family"; "the baby of the Supreme Court"
     5: an immature childish person; "he remained a child in
        practical matters as long as he lived"; "stop being a
        baby!" [syn: {child}]
     6: a project of personal concern to someone; "this project is
        his baby"
     v : treat with ...
