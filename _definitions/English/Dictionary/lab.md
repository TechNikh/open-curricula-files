---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lab
offline_file: ""
offline_thumbnail: ""
uuid: 7d7d7bba-323d-4b5f-91b2-743328248b8f
updated: 1484310327
title: lab
categories:
    - Dictionary
---
lab
     n : a workplace for the conduct of scientific research [syn: {laboratory},
          {research lab}, {research laboratory}, {science lab}, {science
         laboratory}]
