---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yield
offline_file: ""
offline_thumbnail: ""
uuid: bc5458cb-fb8e-4887-9f9a-8e0b5a9f97b7
updated: 1484310371
title: yield
categories:
    - Dictionary
---
yield
     n 1: production of a certain amount [syn: {output}]
     2: an amount of a product [syn: {fruit}]
     3: the income arising from land or other property; "the average
        return was about 5%" [syn: {return}, {issue}, {proceeds},
        {take}, {takings}, {payoff}]
     4: the quantity of something (as a commodity) that is created
        (usually within a given period of time); "production was
        up in the second quarter" [syn: {output}, {production}]
     v 1: be the cause or source of; "He gave me a lot of trouble";
          "Our meeting afforded much interesting information"
          [syn: {give}, {afford}]
     2: end resistance, especially under pressure or ...
