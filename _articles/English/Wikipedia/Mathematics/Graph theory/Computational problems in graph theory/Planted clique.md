---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Planted_clique
offline_file: ""
offline_thumbnail: ""
uuid: 53ee31e2-1658-4793-b973-b90b22b7331c
updated: 1484309396
title: Planted clique
tags:
    - Definition
    - Algorithms
    - Large cliques
    - Quasipolynomial time
    - As a hardness assumption
categories:
    - Computational problems in graph theory
---
In computational complexity theory, a planted clique or hidden clique in an undirected graph is a clique formed from another graph by selecting a subset of vertices and adding edges between each pair of vertices in the subset. The planted clique problem is the algorithmic problem of distinguishing random graphs from graphs that have a planted clique. This is a variation of the clique problem; it may be solved in quasi-polynomial time but is conjectured not to be solvable in polynomial time for intermediate values of the clique size. The conjecture that no polynomial time solution exists is called the planted clique conjecture; it has been used as a computational hardness assumption.
