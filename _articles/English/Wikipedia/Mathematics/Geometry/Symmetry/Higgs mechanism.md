---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Higgs_mechanism
offline_file: ""
offline_thumbnail: ""
uuid: 32baa458-bbc1-4c58-95c5-f5346d24aa5f
updated: 1484309312
title: Higgs mechanism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Andersonphoto.jpg
tags:
    - Standard Model
    - Structure of the Higgs field
    - The photon as the part that remains massless
    - Consequences for fermions
    - History of research
    - Background
    - discovery
    - Examples
    - Landau model
    - Abelian Higgs mechanism
    - Nonabelian Higgs mechanism
    - Affine Higgs mechanism
categories:
    - Symmetry
---
In the Standard Model of particle physics, the Higgs mechanism is essential to explain the generation mechanism of the property "mass" for gauge bosons. Without the Higgs mechanism, all bosons (a type of fundamental particle) would be massless, but measurements show that the W+, W−, and Z bosons actually have relatively large masses of around 80 GeV/c2. The Higgs field resolves this conundrum. The simplest description of the mechanism adds a quantum field (the Higgs field) that permeates all space, to the Standard Model. Below some extremely high temperature, the field causes spontaneous symmetry breaking during interactions. The breaking of symmetry triggers the Higgs mechanism, causing ...
