---
version: 1
type: video
provider: YouTube
id: BNzdgVyAOXs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5b7c5443-6a0a-4f09-b908-084e99fd7ca0
updated: 1486069635
title: prism colour experiment.mp4
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/BNzdgVyAOXs/default.jpg
    - https://i3.ytimg.com/vi/BNzdgVyAOXs/1.jpg
    - https://i3.ytimg.com/vi/BNzdgVyAOXs/2.jpg
    - https://i3.ytimg.com/vi/BNzdgVyAOXs/3.jpg
---
