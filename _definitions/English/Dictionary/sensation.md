---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sensation
offline_file: ""
offline_thumbnail: ""
uuid: d3cac96f-e414-4c8f-85ca-d1c5f21f0e4e
updated: 1484310355
title: sensation
categories:
    - Dictionary
---
sensation
     n 1: an unelaborated elementary awareness of stimulation; "a
          sensation of touch" [syn: {sense experience}, {sense
          impression}, {sense datum}]
     2: someone who is dazzlingly skilled in any field [syn: {ace},
        {adept}, {champion}, {maven}, {mavin}, {virtuoso}, {genius},
         {hotshot}, {star}, {superstar}, {whiz}, {whizz}, {wizard},
         {wiz}]
     3: a general feeling of excitement and heightened interest;
        "anticipation produced in me a sensation somewhere between
        hope and fear"
     4: a state of widespread public excitement and interest; "the
        news caused a sensation"
     5: the faculty through which the external ...
