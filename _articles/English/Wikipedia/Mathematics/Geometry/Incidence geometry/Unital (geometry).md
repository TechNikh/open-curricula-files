---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unital_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 59b270ac-915d-4ac6-87fa-e1a09e720d98
updated: 1484309255
title: Unital (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_16.png
tags:
    - Classical unitals
    - Ree unitals
    - Isomorphic versus equivalent unitals
    - Embeddable versus non-embeddable
    - Notes
categories:
    - Incidence geometry
---
In geometry, a unital is a set of n3 + 1 points arranged into subsets of size n + 1 so that every pair of distinct points of the set are contained in exactly one subset. n ≥ 3 is required by some authors to avoid small exceptional cases.[1] This is equivalent to saying that a unital is a 2-(n3 + 1, n + 1, 1) block design. Some unitals may be embedded in a projective plane of order n2 (the subsets of the design become sets of collinear points in the projective plane). In this case of embedded unitals, every line of the plane intersects the unital in either 1 or n + 1 points. In the Desarguesian planes, PG(2,q2), the classical examples of unitals are given by nondegenerate Hermitian curves. ...
