---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Group_delay_dispersion
offline_file: ""
offline_thumbnail: ""
uuid: 9bc3e326-decf-4239-8fb6-8f2e0f9d4b2e
updated: 1484308887
title: Group delay dispersion
categories:
    - Waves
---
The group delay dispersion (GDD) of an optical element is the derivative of the group delay with respect to angular frequency, and also the second derivative of the optical phase. 
  
    
      
        
          D
          
            2
          
        
        (
        ω
        )
        =
        −
        
          
            
              ∂
              
                T
                
                  g
                
              
            
            
              d
              ω
            
          
        
        =
        
          
            
              
                d
                
                  2
                
          ...
