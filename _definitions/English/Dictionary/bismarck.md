---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bismarck
offline_file: ""
offline_thumbnail: ""
uuid: 0ece7647-a90a-41f5-b18b-49da781bc94c
updated: 1484310553
title: bismarck
categories:
    - Dictionary
---
Bismarck
     n 1: German statesman under whose leadership Germany was united
          (1815-1898) [syn: {von Bismarck}, {Otto von Bismarck}, {Prince
          Otto von Bismarck}, {Prince Otto Eduard Leopold von
          Bismarck}, {Iron Chancellor}]
     2: capital of the state of North Dakota; located in south
        central North Dakota overlooking the Missouri river [syn:
        {capital of North Dakota}]
