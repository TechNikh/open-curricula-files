---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mukhopadhyaya_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 7e082617-8498-45ae-93a5-3f1ed3d65c68
updated: 1484309117
title: Mukhopadhyaya theorem
categories:
    - Theorems in geometry
---
In geometry Mukhopadhyaya's theorem may refer to one of several closely related theorems about the number of vertices of a curve due to Mukhopadhyaya (1909). One version, called the Four-vertex theorem, states that a simple convex curve in the plane has at least 4 vertices, and another version states that a simple convex curve in the affine plane has at least 6 affine vertices.
