---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Set_packing
offline_file: ""
offline_thumbnail: ""
uuid: 187d5aed-086b-4ffd-b16a-e1b68a9d5c0b
updated: 1484308583
title: Set packing
tags:
    - Integer linear program formulation
    - Examples
    - Weighted version
    - Heuristics
    - Complexity
    - Equivalent problems
    - Special cases
    - Other related problems
    - Notes
categories:
    - Combinatorics
---
Suppose we have a finite set S and a list of subsets of S. Then, the set packing problem asks if some k subsets in the list are pairwise disjoint (in other words, no two of them share an element).
