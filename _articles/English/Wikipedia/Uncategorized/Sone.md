---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sone
offline_file: ""
offline_thumbnail: ""
uuid: 975fc7ac-a39b-4d2f-9670-ef49e612cfe0
updated: 1484308420
title: Sone
tags:
    - >
        Examples of sound pressure, sound pressure levels and
        loudness in sone
    - Formulae
categories:
    - Uncategorized
---
The sone (pronunciation: /ˈsoʊn/) is a unit of how loud a sound is perceived. The sone scale is linear. Doubling the perceived loudness doubles the sone value. Proposed by Stanley Smith Stevens in 1936, it is a non-SI unit.
