---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Exploded-view_drawing
offline_file: ""
offline_thumbnail: ""
uuid: 6dbe755c-533e-45c7-8f25-94d12c795339
updated: 1484309325
title: Exploded-view drawing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Gear_pump_exploded.png
tags:
    - Overview
    - History
categories:
    - Technical drawing
---
An exploded view drawing is a diagram, picture, schematic or technical drawing of an object, that shows the relationship or order of assembly of various parts.[1]
