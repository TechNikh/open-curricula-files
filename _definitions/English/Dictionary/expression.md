---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expression
offline_file: ""
offline_thumbnail: ""
uuid: 8427db4b-65ef-4e9a-854c-3c7e8a6f9677
updated: 1484310295
title: expression
categories:
    - Dictionary
---
expression
     n 1: the expression on a person's face; "a sad expression"; "a
          look of triumph"; "an angry face" [syn: {look}, {aspect},
           {facial expression}, {face}]
     2: expression without words; "tears are an expression of
        grief"; "the pulse is a reflection of the heart's
        condition" [syn: {manifestation}, {reflection}, {reflexion}]
     3: the communication (in speech or writing) of your beliefs or
        opinions; "expressions of good will"; "he helped me find
        expression for my ideas"
     4: a word or phrase that particular people use in particular
        situations; "pardon the expression" [syn: {saying}, {locution}]
     5: the style ...
