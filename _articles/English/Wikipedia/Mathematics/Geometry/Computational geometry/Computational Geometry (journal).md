---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Computational_Geometry_(journal)
offline_file: ""
offline_thumbnail: ""
uuid: 61cdec5c-6bf2-4713-8ad3-da5aaf0ffad1
updated: 1484309175
title: Computational Geometry (journal)
categories:
    - Computational geometry
---
Computational Geometry, also known as Computational Geometry: Theory and Applications, is a peer-reviewed mathematics journal for research in theoretical and applied computational geometry, its applications, techniques, and design and analysis of geometric algorithms. All aspects of computational geometry are covered, including the numerical, graph theoretical and combinatorial aspects, as well as fundamental problems in various areas of application of computational geometry: in computer graphics, pattern recognition, image processing, robotics, electronic design automation, CAD/CAM, and geographical information systems.
