---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dispose
offline_file: ""
offline_thumbnail: ""
uuid: 80aaa067-fe3b-4f37-b146-7681419ddaf8
updated: 1484310443
title: dispose
categories:
    - Dictionary
---
dispose
     v 1: give, sell, or transfer to another; "She disposed of her
          parents' possessions"
     2: throw or cast away; "Put away your worries" [syn: {discard},
         {fling}, {toss}, {toss out}, {toss away}, {chuck out}, {cast
        aside}, {throw out}, {cast out}, {throw away}, {cast away},
         {put away}]
     3: make receptive or willing towards an action or attitude or
        belief; "Their language inclines us to believe them" [syn:
         {incline}] [ant: {indispose}]
     4: make fit or prepared; "Your education qualifies you for this
        job" [syn: {qualify}] [ant: {disqualify}]
