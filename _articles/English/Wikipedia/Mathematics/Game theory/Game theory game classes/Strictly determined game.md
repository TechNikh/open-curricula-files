---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strictly_determined_game
offline_file: ""
offline_thumbnail: ""
uuid: 6d39dd55-be77-41b0-bb6b-d0d7e3433478
updated: 1484309038
title: Strictly determined game
categories:
    - Game theory game classes
---
In game theory, a strictly determined game is a two-player zero-sum game that has at least one Nash equilibrium with both players using pure strategies. The value of a strictly determined game is equal to the value of the equilibrium outcome.[1][2][3][4][5]
