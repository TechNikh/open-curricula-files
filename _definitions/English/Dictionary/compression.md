---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/compression
offline_file: ""
offline_thumbnail: ""
uuid: ac9105c2-6e99-4be0-ad21-8db75a86475a
updated: 1484310433
title: compression
categories:
    - Dictionary
---
compression
     n 1: an increase in the density of something [syn: {compaction},
          {concretion}, {densification}]
     2: the process or result of becoming smaller or pressed
        together; "the contraction of a gas on cooling" [syn: {condensation},
         {contraction}]
     3: encoding information while reducing the bandwidth or bits
        required [ant: {decompression}]
     4: applying pressure [syn: {compressing}] [ant: {decompression}]
