---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/above
offline_file: ""
offline_thumbnail: ""
uuid: a0122b16-0d3a-42a3-9576-2fe9d3c886d5
updated: 1484310346
title: above
categories:
    - Dictionary
---
above
     adj : appearing earlier in the same text; "flaws in the above
           interpretation"
     adv 1: at an earlier place; "see above" [syn: {supra}] [ant: {below}]
     2: in or to a place that is higher [syn: {higher up}, {in a
        higher place}, {to a higher place}] [ant: {below}]
