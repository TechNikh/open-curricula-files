---
version: 1
type: video
provider: YouTube
id: cknArdpvwPQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0a43ddb3-a2e3-4751-96c8-93ffbf175b8a
updated: 1486069617
title: Dimensions from volume of box
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/cknArdpvwPQ/default.jpg
    - https://i3.ytimg.com/vi/cknArdpvwPQ/1.jpg
    - https://i3.ytimg.com/vi/cknArdpvwPQ/2.jpg
    - https://i3.ytimg.com/vi/cknArdpvwPQ/3.jpg
---
