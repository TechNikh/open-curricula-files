---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deterring
offline_file: ""
offline_thumbnail: ""
uuid: 21d23372-ec63-44f5-ab3b-749d97af0dbd
updated: 1484310172
title: deterring
categories:
    - Dictionary
---
deter
     v 1: try to prevent; show opposition to; "We should discourage
          this practice among our youth" [syn: {discourage}]
     2: turn away from by persuasion; "Negative campaigning will
        only dissuade people" [syn: {dissuade}] [ant: {persuade}]
     [also: {deterring}, {deterred}]
