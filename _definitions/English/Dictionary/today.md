---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/today
offline_file: ""
offline_thumbnail: ""
uuid: 76e6b200-bbb8-4c79-b8ad-82871e2c57d7
updated: 1484310252
title: today
categories:
    - Dictionary
---
today
     n 1: the present time or age; "the world of today"; "today we
          have computers"
     2: the day that includes the present moment (as opposed to
        yesterday or tomorrow); "Today is beautiful"; "did you see
        today's newspaper?"
     adv 1: in these times; "it is solely by their language that the
            upper classes nowadays are distinguished"- Nancy
            Mitford; "we now rarely see horse-drawn vehicles on
            city streets"; "today almost every home has
            television" [syn: {nowadays}, {now}]
     2: on this day as distinct from yesterday or tomorrow; "I can't
        meet with you today"
