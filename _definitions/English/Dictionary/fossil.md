---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fossil
offline_file: ""
offline_thumbnail: ""
uuid: f286abcb-ba2c-4a5a-ac9a-4f706d478cb2
updated: 1484310284
title: fossil
categories:
    - Dictionary
---
fossil
     adj : characteristic of a fossil
     n 1: someone whose style is out of fashion [syn: {dodo}, {fogy},
          {fogey}]
     2: the remains (or an impression) of a plant or animal that
        existed in a past geological age and that has been
        excavated from the soil
