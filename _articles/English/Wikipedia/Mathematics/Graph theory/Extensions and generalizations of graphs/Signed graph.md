---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Signed_graph
offline_file: ""
offline_thumbnail: ""
uuid: 05af2355-dd89-4e5c-a125-8d4f0771ab13
updated: 1484309406
title: Signed graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pox.jpg
tags:
    - Examples
    - Adjacency Matrix
    - Orientation
    - Incidence matrix
    - Switching
    - Fundamental theorem
    - Frustration
    - Matroid theory
    - Other kinds of "signed graph"
    - Signed digraph
    - Coloring
    - Applications
    - Social psychology
    - Spin glasses
    - Data clustering
    - Generalizations
    - Notes
categories:
    - Extensions and generalizations of graphs
---
A signed graph is balanced if the product of edge signs around every cycle is positive. Three fundamental questions about a signed graph are: Is it balanced? What is the largest size of a balanced edge set in it? What is the smallest number of vertices that must be deleted to make it balanced? The first question is easy to solve quickly; the second and third are computationally intractable (technically, they are NP-hard).
