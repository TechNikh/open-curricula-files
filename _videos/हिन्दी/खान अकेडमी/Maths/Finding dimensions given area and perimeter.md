---
version: 1
type: video
provider: YouTube
id: 0PySjKHMokw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: f876973e-795b-4c0d-8cba-deb5d84514ae
updated: 1486069614
title: Finding dimensions given area and perimeter
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/0PySjKHMokw/default.jpg
    - https://i3.ytimg.com/vi/0PySjKHMokw/1.jpg
    - https://i3.ytimg.com/vi/0PySjKHMokw/2.jpg
    - https://i3.ytimg.com/vi/0PySjKHMokw/3.jpg
---
