---
version: 1
type: video
provider: YouTube
id: nJYk9tDNUyg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 975ccd51-43d4-4b61-add6-612bbaa179a2
updated: 1486069622
title: 'Profit and loss Shortcut tricks - Part 1'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/nJYk9tDNUyg/default.jpg
    - https://i3.ytimg.com/vi/nJYk9tDNUyg/1.jpg
    - https://i3.ytimg.com/vi/nJYk9tDNUyg/2.jpg
    - https://i3.ytimg.com/vi/nJYk9tDNUyg/3.jpg
---
