---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saying
offline_file: ""
offline_thumbnail: ""
uuid: 6c19a541-5b64-4287-978b-888e1237f77c
updated: 1484310369
title: saying
categories:
    - Dictionary
---
saying
     n : a word or phrase that particular people use in particular
         situations; "pardon the expression" [syn: {expression}, {locution}]
