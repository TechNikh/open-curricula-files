---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/innovate
offline_file: ""
offline_thumbnail: ""
uuid: ebbbf614-3085-47eb-9708-67a85a56cd63
updated: 1484310527
title: innovate
categories:
    - Dictionary
---
innovate
     v : bring something new to an environment; "A new word processor
         was introduced" [syn: {introduce}]
