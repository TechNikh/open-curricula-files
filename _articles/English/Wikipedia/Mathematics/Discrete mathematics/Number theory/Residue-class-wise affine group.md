---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Residue-class-wise_affine_group
offline_file: ""
offline_thumbnail: ""
uuid: 88cd947d-6a12-48a6-a945-37bfc80e1c13
updated: 1484308657
title: Residue-class-wise affine group
categories:
    - Number theory
---
In mathematics, specifically in group theory, residue-class-wise affine groups are certain permutation groups acting on 
  
    
      
        
          Z
        
      
    
    {\displaystyle \mathbb {Z} }
  
 (the integers), whose elements are bijective residue-class-wise affine mappings.
