---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beach
offline_file: ""
offline_thumbnail: ""
uuid: da9c6ec3-eac9-4366-9879-de11d905fdaa
updated: 1484310234
title: beach
categories:
    - Dictionary
---
beach
     n : an area of sand sloping down to the water of a sea or lake
     v : land on a beach; "the ship beached near the port"
