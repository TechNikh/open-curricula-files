---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insulator
offline_file: ""
offline_thumbnail: ""
uuid: ffa07c33-7dd8-44fb-bb9b-03ed1f70bd51
updated: 1484310377
title: insulator
categories:
    - Dictionary
---
insulator
     n : a material such as glass or porcelain with negligible
         electrical or thermal conductivity [syn: {dielectric}, {nonconductor}]
         [ant: {conductor}]
