---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sore
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484561881
title: sore
categories:
    - Dictionary
---
sore
     adj 1: hurting; "the tender spot on his jaw" [syn: {sensitive}, {tender}]
     2: causing misery or pain or distress; "it was a sore trial to
        him"; "the painful process of growing up" [syn: {afflictive},
         {painful}]
     3: roused to anger; "stayed huffy a good while"- Mark Twain;
        "she gets mad when you wake her up so early"; "mad at his
        friend"; "sore over a remark" [syn: {huffy}, {mad}]
     4: inflamed and painful; "his throat was raw"; "had a sore
        throat" [syn: {raw}]
     n : an open skin infection
