---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/imbalanced
offline_file: ""
offline_thumbnail: ""
uuid: 5e59421e-1767-49f3-ba6f-db62a5f95f6e
updated: 1484310601
title: imbalanced
categories:
    - Dictionary
---
imbalanced
     adj : being or thrown out of equilibrium [syn: {unbalanced}] [ant:
            {balanced}]
