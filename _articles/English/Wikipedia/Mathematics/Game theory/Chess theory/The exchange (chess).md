---
version: 1
type: article
id: https://en.wikipedia.org/wiki/The_exchange_(chess)
offline_file: ""
offline_thumbnail: ""
uuid: 67b58770-f65e-4e38-8304-4784a4b6b708
updated: 1484309017
title: The exchange (chess)
tags:
    - Value of the exchange
    - In the endgame
    - The exchange sacrifice
    - Sokolov vs. Kramnik
    - Reshevsky vs. Petrosian
    - Petrosian vs. Spassky
    - Kasparov vs. Shirov
    - Minor exchange
    - Notes
categories:
    - Chess theory
---
The exchange in chess refers to a situation in which one player loses a minor piece (i.e. a bishop or knight) but captures the opponent's rook. The side which wins the rook is said to have won the exchange, while the other player has lost the exchange, since the rook is usually more valuable. Alternatively, the side that has won the rook is up the exchange, and the other player is down the exchange. The opposing captures often happen on consecutive moves, although this is not strictly necessary. It is generally detrimental to lose the exchange, although occasionally one may find reason to purposely do so; the result is an exchange sacrifice (see below). The minor exchange is an uncommon ...
