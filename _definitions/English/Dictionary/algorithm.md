---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/algorithm
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484310721
title: algorithm
categories:
    - Dictionary
---
algorithm
     n : a precise rule (or set of rules) specifying how to solve
         some problem [syn: {algorithmic rule}, {algorithmic
         program}]
