---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aggregate
offline_file: ""
offline_thumbnail: ""
uuid: 832321fc-2d47-4481-9eae-600d939f2281
updated: 1484310426
title: aggregate
categories:
    - Dictionary
---
aggregate
     adj 1: gathered or tending to gather into a mass or whole;
            "aggregate expenses include expenses of all divisions
            combined for the entire year"; "the aggregated amount
            of indebtedness" [syn: {aggregated}, {aggregative}, {mass}]
     2: formed of separate units in a cluster; "raspberries are
        aggregate fruits"
     n 1: a sum total of many heterogenous things taken together [syn:
           {congeries}, {conglomeration}]
     2: the whole amount [syn: {sum}, {total}, {totality}]
     v 1: amount in the aggregate to
     2: gather in a mass, sum, or whole [syn: {combine}]
