---
version: 1
type: video
provider: YouTube
id: AW9bHqpJLOE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c3178f0e-a464-4113-ac79-844972bb8a6c
updated: 1486069637
title: '199705 - Virginia Tech - video experiment highlights - 03 - spiral lighter feedback w/refraction'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/AW9bHqpJLOE/default.jpg
    - https://i3.ytimg.com/vi/AW9bHqpJLOE/1.jpg
    - https://i3.ytimg.com/vi/AW9bHqpJLOE/2.jpg
    - https://i3.ytimg.com/vi/AW9bHqpJLOE/3.jpg
---
