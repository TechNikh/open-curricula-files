---
version: 1
type: video
provider: YouTube
id: fpHdPn0vW2s
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Tricks/Dry%20Ice%20ERUPTIONS%21%20How%20to%20Create%20Experimental%20Explosions%21%20-%20Joe%20Genius.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f6af4288-9a2f-4837-aa43-9d695793a3af
updated: 1486069661
title: 'Dry Ice ERUPTIONS! How to Create Experimental Explosions! - Joe Genius'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/fpHdPn0vW2s/default.jpg
    - https://i3.ytimg.com/vi/fpHdPn0vW2s/1.jpg
    - https://i3.ytimg.com/vi/fpHdPn0vW2s/2.jpg
    - https://i3.ytimg.com/vi/fpHdPn0vW2s/3.jpg
---
