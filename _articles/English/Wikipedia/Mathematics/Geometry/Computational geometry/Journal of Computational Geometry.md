---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Journal_of_Computational_Geometry
offline_file: ""
offline_thumbnail: ""
uuid: d4341e63-8daf-4f11-b346-02d73144aaf9
updated: 1484309175
title: Journal of Computational Geometry
categories:
    - Computational geometry
---
The Journal of Computational Geometry is an open access mathematics journal that was established in 2010. It covers research in all aspects of computational geometry. The current editors-in-chief are Kenneth L. Clarkson and Günter Rote.
