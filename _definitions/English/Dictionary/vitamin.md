---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vitamin
offline_file: ""
offline_thumbnail: ""
uuid: 1640b0bd-8018-4d36-8011-aa9c0215dfc9
updated: 1484310301
title: vitamin
categories:
    - Dictionary
---
vitamin
     n : any of a group of organic substances essential in small
         quantities to normal metabolism
