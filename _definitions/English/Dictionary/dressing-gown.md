---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dressing-gown
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484452981
title: dressing-gown
categories:
    - Dictionary
---
dressing gown
     n : a robe worn before dressing or while lounging [syn: {robe-de-chambre},
          {lounging robe}]
