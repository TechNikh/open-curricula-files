---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inorder
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484337901
title: inorder
categories:
    - Dictionary
---
in order
     adj 1: appropriate or even needed in the circumstances; "a change
            is in order" [syn: {in order(p)}, {called for}]
     2: marked by system; in good order; "everything is in order";
        "his books are always just so"; "things must be exactly
        so" [syn: {in order(p)}, {so(p)}]
     3: in a state of proper readiness or preparation or
        arrangement; "everything is in order for their arrival"
