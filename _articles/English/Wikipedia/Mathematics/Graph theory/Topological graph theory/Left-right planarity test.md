---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Left-right_planarity_test
offline_file: ""
offline_thumbnail: ""
uuid: 2fb0a71e-3825-4035-81b9-5824101057d9
updated: 1484309479
title: Left-right planarity test
tags:
    - T-alike and T-opposite edges
    - The characterization
    - Additional reading
categories:
    - Topological graph theory
---
In graph theory, a branch of mathematics, the left-right planarity test or de Fraysseix–Rosenstiehl planarity criterion[1] is a characterization of planar graphs based on the properties of the depth-first search trees, published by de Fraysseix and Rosenstiehl (1982, 1985)[2][3] and used by them with Patrice Ossona de Mendez to develop a linear time planarity testing algorithm.[4][5] In a 2003 experimental comparison of six planarity testing algorithms, this was one of the fastest algorithms tested.[6]
