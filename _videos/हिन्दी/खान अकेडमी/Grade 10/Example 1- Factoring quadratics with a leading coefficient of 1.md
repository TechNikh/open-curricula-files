---
version: 1
type: video
provider: YouTube
id: p1BGDNSNOdU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bc680d34-9140-4b6f-8103-88e69109a6c2
updated: 1486069621
title: 'Example 1- Factoring quadratics with a leading coefficient of 1'
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/p1BGDNSNOdU/default.jpg
    - https://i3.ytimg.com/vi/p1BGDNSNOdU/1.jpg
    - https://i3.ytimg.com/vi/p1BGDNSNOdU/2.jpg
    - https://i3.ytimg.com/vi/p1BGDNSNOdU/3.jpg
---
