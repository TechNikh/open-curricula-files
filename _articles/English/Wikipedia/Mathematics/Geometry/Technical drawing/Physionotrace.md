---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Physionotrace
offline_file: ""
offline_thumbnail: ""
uuid: cd285c21-a997-4fa2-b6f1-6ab5ddaec8ec
updated: 1484309327
title: Physionotrace
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pierre_Gaveaux_by_Edm%25C3%25A9_Quenedey_%25281821%2529.jpg'
categories:
    - Technical drawing
---
Physiognotrace or physionotrace: is an instrument designed to trace a person's physiognomy, most specifically the profile in the form of a silhouette: it originated in France where it is known as the physionotrace.[1]The instrument is a descendant of the pantograph, a drawing device that magnifies figures.
