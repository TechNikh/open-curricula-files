---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homicidal_chauffeur_problem
offline_file: ""
offline_thumbnail: ""
uuid: 6c7099f6-ec9b-472b-872c-982913337222
updated: 1484309063
title: Homicidal chauffeur problem
categories:
    - Pursuit-evasion
---
In game theory, the homicidal chauffeur problem is a mathematical pursuit problem which pits a hypothetical runner, who can only move slowly, but is highly maneuverable, against the driver of a motor vehicle, which is much faster but far less maneuverable, who is attempting to run him down. Both runner and driver are assumed to never tire. The question to be solved is: under what circumstances, and with what strategy, can the driver of the car guarantee that he can always catch the pedestrian, or the pedestrian guarantee that he can indefinitely elude the car?
