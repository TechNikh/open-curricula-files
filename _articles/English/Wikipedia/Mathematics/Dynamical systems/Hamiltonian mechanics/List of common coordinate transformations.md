---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_common_coordinate_transformations
offline_file: ""
offline_thumbnail: ""
uuid: d9207627-e56d-4310-8e11-f0135dc801aa
updated: 1484308794
title: List of common coordinate transformations
tags:
    - 2-Dimensional
    - From polar coordinates to Cartesian coordinates
    - To polar coordinates from Cartesian coordinates
    - To Cartesian coordinates from log-polar coordinates
    - To log-polar coordinates from Cartesian coordinates
    - To Cartesian coordinates from bipolar coordinates
    - To Cartesian coordinates from two-center bipolar coordinates
    - To polar coordinates from two-center bipolar coordinates
    - To Cartesian coordinates from Cesàro equation
    - Arc length and curvature from Cartesian coordinates
    - Arc length and curvature from polar coordinates
    - 3-Dimensional
    - To Cartesian coordinates
    - From spherical coordinates
    - From cylindrical coordinates
    - To Spherical coordinates
    - From Cartesian coordinates
    - From cylindrical coordinates
    - To Cylindrical Coordinates
    - From Cartesian coordinates
    - From Spherical Coordinates(Lana Rule)
    - Arc length, curvature and torsion from cartesian coordinates
categories:
    - Hamiltonian mechanics
---
