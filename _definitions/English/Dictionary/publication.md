---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/publication
offline_file: ""
offline_thumbnail: ""
uuid: 3e7205a1-32de-47d6-9962-0ed64ae45c77
updated: 1484310397
title: publication
categories:
    - Dictionary
---
publication
     n 1: a copy of a printed work offered for distribution
     2: the act of issuing printed materials [syn: {issue}]
     3: the business of publishing [syn: {publishing}]
