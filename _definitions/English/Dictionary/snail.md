---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/snail
offline_file: ""
offline_thumbnail: ""
uuid: 0edf5477-4665-4fe5-b640-e684c88b52c2
updated: 1484310162
title: snail
categories:
    - Dictionary
---
snail
     n 1: freshwater or marine or terrestrial gastropod mollusk
          usually having an external enclosing spiral shell
     2: edible terrestrial snail usually served in the shell with a
        sauce of melted butter and garlic [syn: {escargot}]
     v : gather snails; "We went snailing in the summer"
