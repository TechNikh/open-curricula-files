---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strategyproofness
offline_file: ""
offline_thumbnail: ""
uuid: 80adf454-b7b9-4155-9d75-f8f67e2ef629
updated: 1484309046
title: Strategyproofness
tags:
    - Examples
    - Notation
    - Characterization
    - Outcome-function characterization
    - Truthful mechanisms in single-parameter domains
    - Truthfulness with-high-probability
    - False-name-proofness
categories:
    - Mechanism design
---
In game theory, an asymmetric game where players have private information is said to be strategyproof (SP) if it is a weakly-dominant strategy for every player to reveal his/her private information,[1]:244 i.e you fare best or at least not worse by being truthful, regardless of what the others do.
