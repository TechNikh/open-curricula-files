---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lug
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484503801
title: lug
categories:
    - Dictionary
---
Lug
     n 1: ancient Celtic god [syn: {Lugh}]
     2: a sail with four corners that is hoisted from a yard that is
        oblique to the mast [syn: {lugsail}]
     3: a projecting piece that is used to lift or support or turn
        something
     4: marine worms having a row of tufted gills along each side of
        the back; often used for fishing bait [syn: {lugworm}, {lobworm}]
     v 1: carry with difficulty; "You'll have to lug this suitcase"
          [syn: {tote}, {tug}]
     2: obstruct; "My nose is all stuffed"; "Her arteries are
        blocked" [syn: {stuff}, {choke up}, {block}] [ant: {unstuff}]
