---
version: 1
type: video
provider: YouTube
id: 5NDdp8ucWGg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 78271da0-ae29-411d-9de0-a0607d94375c
updated: 1486069632
title: 8 Coin Science Experiments Compilation
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/5NDdp8ucWGg/default.jpg
    - https://i3.ytimg.com/vi/5NDdp8ucWGg/1.jpg
    - https://i3.ytimg.com/vi/5NDdp8ucWGg/2.jpg
    - https://i3.ytimg.com/vi/5NDdp8ucWGg/3.jpg
---
