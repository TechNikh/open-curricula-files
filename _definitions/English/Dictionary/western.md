---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/western
offline_file: ""
offline_thumbnail: ""
uuid: b67a4f0d-183c-4795-824d-f74d72a70941
updated: 1484310435
title: western
categories:
    - Dictionary
---
western
     adj 1: relating to or characteristic of regions of western parts of
            the world; "the Western Hemisphere"; "Western Europe";
            "the Western Roman Empire" [ant: {eastern}]
     2: lying in or toward the west [syn: {westerly}]
     3: of or characteristic of regions of the United States west of
        the Mississippi River; "a Western ranch" [ant: {eastern}]
     4: lying toward or situated in the west; "our company's western
        office"
     5: of wind; from the west [syn: {westerly}]
     n 1: a film about life in the western United States during the
          period of exploration and development [syn: {horse opera}]
     2: a sandwich made from a ...
