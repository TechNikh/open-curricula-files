---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/metamorphic
offline_file: ""
offline_thumbnail: ""
uuid: ba169c31-e5a0-48fb-853f-ac276622b86b
updated: 1484310437
title: metamorphic
categories:
    - Dictionary
---
metamorphic
     adj 1: of or relating to metamorphosis (especially of rocks);
            "metamorphic stage"; "marble is a metamorphic rock
            that takes a high polish" [syn: {metamorphous}]
     2: characterized by metamorphosis or change in physical form or
        substance [ant: {nonmetamorphic}]
