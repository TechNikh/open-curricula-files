---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Curry%E2%80%93Howard_correspondence'
offline_file: ""
offline_thumbnail: ""
uuid: b35065eb-c8ae-46ad-83c5-93ca785ae39e
updated: 1484309492
title: Curry–Howard correspondence
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Coq_plus_comm_screenshot.jpg
tags:
    - Origin, scope, and consequences
    - General formulation
    - Corresponding systems
    - Hilbert-style deduction systems and combinatory logic
    - Natural deduction and lambda calculus
    - Classical logic and control operators
    - Sequent calculus
    - Related proofs-as-programs correspondences
    - The role of de Bruijn
    - BHK interpretation
    - Realizability
    - Curry–Howard–Lambek correspondence
    - Examples
    - Other applications
    - Generalizations
    - Seminal references
    - Extensions of the correspondence
    - Philosophical interpretations
    - Synthetic papers
    - Books
categories:
    - Proof theory
---
In programming language theory and proof theory, the Curry–Howard correspondence (also known as the Curry–Howard isomorphism or equivalence, or the proofs-as-programs and propositions- or formulae-as-types interpretation) is the direct relationship between computer programs and mathematical proofs. It is a generalization of a syntactic analogy between systems of formal logic and computational calculi that was first discovered by the American mathematician Haskell Curry and logician William Alvin Howard.[1] It is the link between logic and computation that is usually attributed to Curry and Howard, although the idea is related to the operational interpretation of intuitionistic logic ...
