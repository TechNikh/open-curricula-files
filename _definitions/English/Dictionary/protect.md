---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/protect
offline_file: ""
offline_thumbnail: ""
uuid: a4cbc0d5-3de1-4d09-bfbc-fcb83012f718
updated: 1484310249
title: protect
categories:
    - Dictionary
---
protect
     v 1: shield from danger, injury, destruction, or damage;
          "Weatherbeater protects your roof from the rain"
     2: use tariffs to favor domestic industry
