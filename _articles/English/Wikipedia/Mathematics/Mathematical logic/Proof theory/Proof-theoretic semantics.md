---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Proof-theoretic_semantics
offline_file: ""
offline_thumbnail: ""
uuid: 911874dd-025a-4a44-86ed-844ed9fcd610
updated: 1484309497
title: Proof-theoretic semantics
categories:
    - Proof theory
---
Proof-theoretic semantics is an approach to the semantics of logic that attempts to locate the meaning of propositions and logical connectives not in terms of interpretations, as in Tarskian approaches to semantics, but in the role that the proposition or logical connective plays within the system of inference.
