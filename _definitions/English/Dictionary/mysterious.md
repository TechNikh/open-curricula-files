---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mysterious
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484478541
title: mysterious
categories:
    - Dictionary
---
mysterious
     adj 1: of an obscure nature; "the new insurance policy is written
            without cryptic or mysterious terms"; "a deep dark
            secret"; "the inscrutible workings of Providence"; "in
            its mysterious past it encompasses all the dim origins
            of life"- Rachel Carson; "rituals totally mystifying
            to visitors from other lands" [syn: {cryptic}, {cryptical},
             {deep}, {inscrutable}, {mystifying}]
     2: having an import not apparent to the senses nor obvious to
        the intelligence; beyond ordinary understanding;
        "mysterious symbols"; "the mystical style of Blake";
        "occult lore"; "the secret learning of ...
