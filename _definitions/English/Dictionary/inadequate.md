---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inadequate
offline_file: ""
offline_thumbnail: ""
uuid: 3f352a07-6582-4e70-91c0-e82afb2fbb8b
updated: 1484310447
title: inadequate
categories:
    - Dictionary
---
inadequate
     adj 1: (sometimes followed by `to') not meeting the requirements
            especially of a task; "inadequate training"; "the
            staff was inadequate"; "she was inadequate to the job"
            [ant: {adequate}]
     2: not sufficient to meet a need; "an inadequate income"; "a
        poor salary"; "money is short"; "on short rations"; "food
        is in short supply"; "short on experience" [syn: {poor}, {short}]
