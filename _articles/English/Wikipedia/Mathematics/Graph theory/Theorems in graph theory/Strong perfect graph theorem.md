---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strong_perfect_graph_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 3419d970-d605-4f99-8c1e-aea8595afb5d
updated: 1484309456
title: Strong perfect graph theorem
tags:
    - Theorem statement
    - Relation to the weak perfect graph theorem
    - Proof ideas
    - Notes
categories:
    - Theorems in graph theory
---
In graph theory, the strong perfect graph theorem is a forbidden graph characterization of the perfect graphs as being exactly the graphs that have neither odd holes (odd-length induced cycles) nor odd antiholes (complements of odd holes). It was conjectured by Claude Berge in 1961. A proof by Maria Chudnovsky, Neil Robertson, Paul Seymour, and Robin Thomas was announced in 2002[1] and published by them in 2006.
