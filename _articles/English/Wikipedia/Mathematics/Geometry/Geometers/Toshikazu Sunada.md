---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Toshikazu_Sunada
offline_file: ""
offline_thumbnail: ""
uuid: b434babd-43e9-453b-95cc-e8d59769985f
updated: 1484309103
title: Toshikazu Sunada
categories:
    - Geometers
---
Toshikazu Sunada (砂田 利一, Sunada Toshikazu?, born September 7, 1948) is a Japanese mathematician and author of many books and essays on mathematics and mathematical sciences. He is professor of mathematics at Meiji University, Tokyo, and is also professor emeritus of Tohoku University, Tohoku, Japan. Before he joined Meiji University in 2003, he was professor of mathematics at Nagoya University (1988–1991), at the University of Tokyo (1991–1993), and at Tohoku University (1993–2003). Sunada was involved in the creation of the School of Interdisciplinary Mathematical Sciences in Meiji University and is its first dean (2013-).
