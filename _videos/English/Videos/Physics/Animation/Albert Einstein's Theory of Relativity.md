---
version: 1
type: video
provider: YouTube
id: ev9zrt__lec
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 61abdd48-2543-48e4-a92f-f880d3d12d46
updated: 1486069657
title: "Albert Einstein's Theory of Relativity"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/ev9zrt__lec/default.jpg
    - https://i3.ytimg.com/vi/ev9zrt__lec/1.jpg
    - https://i3.ytimg.com/vi/ev9zrt__lec/2.jpg
    - https://i3.ytimg.com/vi/ev9zrt__lec/3.jpg
---
