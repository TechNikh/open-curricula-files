---
version: 1
type: video
provider: YouTube
id: sLdJTcBnO3c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Introduction%20to%20DNA.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4a72930e-61b0-470d-a2fd-a924532f8bc8
updated: 1486069673
title: Introduction to DNA
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/sLdJTcBnO3c/default.jpg
    - https://i3.ytimg.com/vi/sLdJTcBnO3c/1.jpg
    - https://i3.ytimg.com/vi/sLdJTcBnO3c/2.jpg
    - https://i3.ytimg.com/vi/sLdJTcBnO3c/3.jpg
---
