---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suffocating
offline_file: ""
offline_thumbnail: ""
uuid: bcb5caf1-8091-4d5a-aa94-feae8c40813a
updated: 1484310575
title: suffocating
categories:
    - Dictionary
---
suffocating
     adj : causing difficulty in breathing especially through lack of
           fresh air and presence of heat; "the choking June
           dust"; "the smothering soft voices"; "smothering heat";
           "the room was suffocating--hot and airless" [syn: {smothering},
            {suffocative}]
