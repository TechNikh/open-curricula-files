---
version: 1
type: video
provider: YouTube
id: UukRgqzk-KE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Why%20does%20ice%20float%20in%20water-%20-%20George%20Zaidan%20and%20Charles%20Morton.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 37e233db-09e5-40a4-84eb-3864948ba7c1
updated: 1486069665
title: 'Why does ice float in water? - George Zaidan and Charles Morton'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/UukRgqzk-KE/default.jpg
    - https://i3.ytimg.com/vi/UukRgqzk-KE/1.jpg
    - https://i3.ytimg.com/vi/UukRgqzk-KE/2.jpg
    - https://i3.ytimg.com/vi/UukRgqzk-KE/3.jpg
---
