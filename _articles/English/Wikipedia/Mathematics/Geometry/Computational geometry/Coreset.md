---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Coreset
offline_file: ""
offline_thumbnail: ""
uuid: f0820493-dcc0-43f4-8c67-6c5fc2bacadb
updated: 1484309170
title: Coreset
categories:
    - Computational geometry
---
In computational geometry, a coreset is a small set of points that approximates the shape of a larger point set, in the sense that applying some geometric measure to the two sets (such as their minimum bounding box volume) results in approximately equal numbers. Many natural geometric optimization problems have coresets that approximate an optimal solution to within a factor of 1 + ε, that can be found quickly (in linear time or near-linear time), and that have size bounded by a function of 1/ε independent of the input size, where ε is an arbitrary positive number. When this is the case, one obtains a linear-time or near-linear time approximation scheme, based on the idea of finding a ...
