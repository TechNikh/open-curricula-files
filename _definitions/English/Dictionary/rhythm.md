---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rhythm
offline_file: ""
offline_thumbnail: ""
uuid: 77f1530b-4769-4245-888c-7695e1e2a5fe
updated: 1484310144
title: rhythm
categories:
    - Dictionary
---
rhythm
     n 1: the basic rhythmic unit in a piece of music; "the piece has
          a fast rhythm"; "the conductor set the beat" [syn: {beat},
           {musical rhythm}]
     2: recurring at regular intervals [syn: {regular recurrence}]
     3: an interval during which a recurring sequence of events
        occurs; "the neverending cycle of the seasons" [syn: {cycle},
         {round}]
     4: the arrangement of spoken words alternating stressed and
        unstressed elements; "the rhythm of Frost's poetry" [syn:
        {speech rhythm}]
     5: natural family planning in which ovulation is assumed to
        occur 14 days before the onset of a period (the fertile
        period would ...
