---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Geometry_of_roots_of_real_polynomials
offline_file: ""
offline_thumbnail: ""
uuid: 3a4001e7-75b8-4ae9-8692-f91194e6953c
updated: 1484308907
title: Geometry of roots of real polynomials
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Crystal_Clear_app_3d.png
tags:
    - Complex roots of quadratic polynomials
categories:
    - Elementary algebra
---
Graphical methods provide a means of determining or approximating the roots of a polynomial—the values that make the polynomial equal to zero.[1] Practical tools for performing these include graph paper, graphical calculators and computer graphics.[2]
