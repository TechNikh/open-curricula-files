---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Samantha
offline_file: ""
offline_thumbnail: ""
uuid: 9b08029c-2e49-467b-a0eb-b0be9d011fd9
updated: 1484308463
title: Samantha
tags:
    - Translations
    - Notable people
    - Female
    - Male
    - Fictional characters
categories:
    - Uncategorized
---
Samantha is a feminine given name. It has been recorded in England in 1633 in Newton Regis, Warwickshire, England.[1] It was also recorded in the 18th century in New England, but its etymology is unknown.[2] Speculation (without evidence) has suggested an origin from the masculine given name Samuel[3] and anthos, the Greek word for "flower".[4] A variant of this speculation is that it may have been a feminine form of Samuel with the addition of the already existing feminine name Anthea.[3]
