---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dual_matroid
offline_file: ""
offline_thumbnail: ""
uuid: 1ce99468-0aed-4203-aa97-b5464c1bcf3b
updated: 1484309236
title: Dual matroid
tags:
    - Basic properties
    - Minors
    - Self-dual matroids
    - Matroid families
categories:
    - Duality theories
---
In matroid theory, the dual of a matroid 
  
    
      
        M
      
    
    {\displaystyle M}
  
 is another matroid 
  
    
      
        
          M
          
            ∗
          
        
      
    
    {\displaystyle M^{\ast }}
  
 that has the same elements as 
  
    
      
        M
      
    
    {\displaystyle M}
  
, and in which a set is independent if and only if 
  
    
      
        M
      
    
    {\displaystyle M}
  
 has a basis set disjoint from it.[1][2][3]
