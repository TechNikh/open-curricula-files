---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Verdier_duality
offline_file: ""
offline_thumbnail: ""
uuid: 5d0a64c2-3930-4bf3-9d70-e97888ae7428
updated: 1484309241
title: Verdier duality
tags:
    - Verdier duality
    - Poincaré duality
categories:
    - Duality theories
---
In mathematics, Verdier duality is a duality in sheaf theory that generalizes Poincaré duality for manifolds. Verdier duality was introduced by Verdier (1967, 1995) as an analog for locally compact spaces of the coherent duality for schemes due to Grothendieck. It is commonly encountered when studying constructible or perverse sheaves.
