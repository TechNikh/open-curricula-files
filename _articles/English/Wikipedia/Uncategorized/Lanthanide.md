---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lanthanoids
offline_file: ""
offline_thumbnail: ""
uuid: 22528484-eda5-4c19-a495-2b7183f0165c
updated: 1484308384
title: Lanthanide
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rareearthoxides.jpg
tags:
    - Etymology
    - Physical properties of the elements
    - Chemistry and compounds
    - Effect of 4f orbitals
    - Lanthanide oxidation states
    - Separation of lanthanides
    - Coordination chemistry and catalysis
    - Ln(III) compounds
    - Ln(II) and Ln(IV) compounds
    - Hydrides
    - Halides
    - Oxides and hydroxides
    - Chalcogenides (S, Se, Te)
    - Pnictides (group 15)
    - Carbides
    - Borides
    - Organometallic compounds
    - Physical properties
    - Magnetic and spectroscopic
    - Occurrence
    - Applications
    - Industrial
    - Life science
    - Upcoming Medical Uses
    - Biological effects
    - Cited sources
categories:
    - Uncategorized
---
The lanthanide /ˈlænθənaɪd/ or lanthanoid /ˈlænθənɔɪd/ series of chemical elements[1] comprises the fifteen metallic chemical elements with atomic numbers 57 through 71, from lanthanum through lutetium.[2][3][4] These fifteen lanthanide elements, along with the chemically similar elements scandium and yttrium, are often collectively known as the rare earth elements.
