---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geometric_phase_analysis
offline_file: ""
offline_thumbnail: ""
uuid: 340769eb-5a33-443c-9b58-63fbce66926e
updated: 1484309241
title: Geometric phase analysis
categories:
    - Geometric measurement
---
Geometric phase analysis is a method for measuring and mapping displacement fields and strain fields from periodic lattices such as those found in high-resolution electron microscope (HREM) images [1]
