---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laver_table
offline_file: ""
offline_thumbnail: ""
uuid: 230816ad-8f86-41cc-a2f9-bd5ea02d4740
updated: 1484308580
title: Laver table
tags:
    - Definition
    - Periodicity
categories:
    - Combinatorics
---
In mathematics, Laver tables (named after Richard Laver, who discovered them towards the end of the 1980s in connection with his works on set theory) are tables of numbers that have certain properties. They occur in the study of racks and quandles.
