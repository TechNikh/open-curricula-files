---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Q-Pochhammer_symbol
offline_file: ""
offline_thumbnail: ""
uuid: 5cfd2f05-420c-49b7-bbf0-d6ac6ffd15f1
updated: 1484308654
title: Q-Pochhammer symbol
tags:
    - Identities
    - Combinatorial interpretation
    - Multiple arguments convention
    - q-series
    - Relationship to other q-functions
categories:
    - Number theory
---
In mathematics, in the area of combinatorics, a q-Pochhammer symbol, also called a q-shifted factorial, is a q-analog of the common Pochhammer symbol. It is defined as
