---
version: 1
type: video
provider: YouTube
id: XiHVe8U5PhU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Voltage%2C%20Current%2C%20Electricity%2C%20Magnetism.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0bcb0e44-5702-406b-bfeb-e7129352022b
updated: 1486069657
title: Voltage, Current, Electricity, Magnetism
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/XiHVe8U5PhU/default.jpg
    - https://i3.ytimg.com/vi/XiHVe8U5PhU/1.jpg
    - https://i3.ytimg.com/vi/XiHVe8U5PhU/2.jpg
    - https://i3.ytimg.com/vi/XiHVe8U5PhU/3.jpg
---
