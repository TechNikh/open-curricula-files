---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/once
offline_file: ""
offline_thumbnail: ""
uuid: 14aef535-6fb3-4c2d-a3d1-21f641e4e3ef
updated: 1484310281
title: once
categories:
    - Dictionary
---
once
     adj : belonging to some prior time; "erstwhile friend"; "our
           former glory"; "the once capital of the state"; "her
           quondam lover" [syn: {erstwhile(a)}, {former(a)}, {once(a)},
            {onetime(a)}, {quondam(a)}, {sometime(a)}]
     adv 1: on one occasion; "once I ran into her" [syn: {one time}, {in
            one case}]
     2: as soon as; "once we are home, we can rest" [syn: {when}]
     3: at a previous time; "once he loved her"; "her erstwhile
        writing" [syn: {formerly}, {at one time}, {erstwhile}, {erst}]
