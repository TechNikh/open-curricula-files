---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fusion
offline_file: ""
offline_thumbnail: ""
uuid: b6db81fd-7ed1-4679-bc3f-600aae3259b0
updated: 1484310226
title: fusion
categories:
    - Dictionary
---
fusion
     n 1: an occurrence that involves the production of a union [syn:
          {merger}, {unification}]
     2: a nuclear reaction in which nuclei combine to form more
        massive nuclei with the simultaneous release of energy
        [syn: {nuclear fusion}, {nuclear fusion reaction}]
     3: the state of being combined into one body [syn: {coalition}]
     4: the merging of adjacent sounds or syllables or words
     5: the combining of images from the two eyes to form a single
        visual percept [syn: {optical fusion}]
     6: correction of an unstable part of the spine by joining two
        or more vertebrae; usually done surgically but sometimes
        done by traction ...
