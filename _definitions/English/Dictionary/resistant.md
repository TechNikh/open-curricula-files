---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/resistant
offline_file: ""
offline_thumbnail: ""
uuid: cfe241a2-ad07-489f-8772-604c6eb5f6aa
updated: 1484310414
title: resistant
categories:
    - Dictionary
---
resistant
     adj 1: relating to or conferring immunity (to disease or infection)
            [syn: {immune}]
     2: incapable of being affected; "resistant to persuasion"
     3: disposed to or engaged in defiance of established authority
        [syn: {insubordinate}, {resistive}]
     4: incapable of absorbing or mixing with; "a water-repellent
        fabric"; "plastic highly resistant to steam and water"
        [syn: {repellent}]
