---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/here
offline_file: ""
offline_thumbnail: ""
uuid: bc317d37-b27e-47aa-9b1b-8c0c4831c3a2
updated: 1484310341
title: here
categories:
    - Dictionary
---
here
     adj : being here now; "is everyone here?"; "present company
           excepted" [syn: {here(p)}]
     n 1: the present location; this place; "where do we go from
          here?" [ant: {there}]
     2: queen of the Olympian gods in ancient Greek mythology;
        sister and wife of Zeus remembered for her jealously of
        the many mortal women Zeus fell in love with; identified
        with Roman Juno [syn: {Hera}]
     adv 1: in or at this place; where the speaker or writer is; "I work
            here"; "turn here"; "radio waves received here on
            Earth" [ant: {there}]
     2: in this circumstance or respect or on this point or detail;
        "what do we have ...
