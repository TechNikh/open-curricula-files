---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Language_Acquisition_Device_(computer)
offline_file: ""
offline_thumbnail: ""
uuid: 9f8c5606-d48d-401c-b713-d139985f3003
updated: 1484309006
title: Language Acquisition Device (computer)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/128px-Globe_of_letters.svg.png
categories:
    - Game artificial intelligence
---
The Language Acquisition Device is a computer program developed by Lobal Technologies, a computer company in the United Kingdom, and scientists from King's College. It emulates the functions of the brain's frontal lobes where humans process language and emotion.[1]
