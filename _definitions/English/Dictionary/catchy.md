---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/catchy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484589541
title: catchy
categories:
    - Dictionary
---
catchy
     adj 1: having concealed difficulty; "a catchy question"; "a tricky
            recipe to follow" [syn: {tricky}]
     2: likely to attract attention; "a catchy title for a movie"
        [syn: {attention-getting}]
     [also: {catchiest}, {catchier}]
