---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overnight
offline_file: ""
offline_thumbnail: ""
uuid: b350cc3c-2a32-4ba9-8b8c-66711ee46715
updated: 1484310379
title: overnight
categories:
    - Dictionary
---
overnight
     adj : lasting through or extending over the whole night; "a
           nightlong vigil"; "an overnight trip" [syn: {nightlong}]
     adv : happening in a short time or with great speed; "these
           solutions cannot be found overnight!"
