---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Double-negation_translation
offline_file: ""
offline_thumbnail: ""
uuid: d2e76553-72e3-4c8a-a096-f731d34b6186
updated: 1484309495
title: Double-negation translation
tags:
    - Propositional logic
    - First-order logic
    - Variants
    - Results
    - Notes
categories:
    - Proof theory
---
In proof theory, a discipline within mathematical logic, double-negation translation, sometimes called negative translation, is a general approach for embedding classical logic into intuitionistic logic, typically by translating formulas to formulas which are classically equivalent but intuitionistically inequivalent. Particular instances of double-negation translation include Glivenko's translation for propositional logic, and the Gödel–Gentzen translation and Kuroda's translation for first-order logic.
