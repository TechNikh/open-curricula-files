---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Texture_mapping_unit
offline_file: ""
offline_thumbnail: ""
uuid: 7c17f604-04fb-48a5-9c96-d19f64af02ca
updated: 1484309185
title: Texture mapping unit
tags:
    - Background and history
    - Geometry
    - Texture Fill Rate
    - Details
    - Texture Mapping Units (TMUs)
    - Pipelines
    - Render Output Pipelines (ROPs)
    - Use in GPGPU
categories:
    - Geometry in computer vision
---
A texture mapping unit (TMU) is a component in modern graphics processing units (GPUs), historically it was a separate physical processor. A TMU is able to rotate, resize, and distort a bitmap image (performing texture sampling), to be placed onto an arbitrary plane of a given 3D model as a texture. This process is called texture mapping. In modern graphics cards it is implemented as a discrete stage in a graphics pipeline,[1] whereas when first introduced it was implemented as a separate processor, e.g. as seen on the Voodoo2 graphics card.
