---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kepert_model
offline_file: ""
offline_thumbnail: ""
uuid: ed45f919-e382-49f1-8af0-86c882237d60
updated: 1484309277
title: Kepert model
categories:
    - Molecular geometry
---
The Kepert model is a modification of VSEPR theory used to predict the 3-dimensional structures of transitional metal complexes. In the Kepert model, the ligands attached to the metal are considered to repel each other the same way that point charges repel each other in VSEPR theory. Unlike VSEPR theory, the Kepert model does not account for non-bonding electrons. Therefore, the geometry of the coordination complex is independent of the electronic configuration of the metal center. Thus [MLn]m+ has the same coordination geometry as [MLn]m−. The Kepert model cannot explain the formation of square planar complexes or distorted structures.
