---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/planning
offline_file: ""
offline_thumbnail: ""
uuid: fb8fa806-28cd-43cc-ba36-a6109f9687fc
updated: 1484310462
title: planning
categories:
    - Dictionary
---
plan
     n 1: a series of steps to be carried out or goals to be
          accomplished; "they drew up a six-step plan"; "they
          discussed plans for a new bond issue" [syn: {program}, {programme}]
     2: an arrangement scheme; "the awkward design of the keyboard
        made operation difficult"; "it was an excellent design for
        living"; "a plan for seating guests" [syn: {design}]
     3: scale drawing of a structure; "the plans for City Hall were
        on file" [syn: {architectural plan}]
     v 1: have the will and intention to carry out some action; "He
          plans to be in graduate school next year"; "The rebels
          had planned turmoil and confusion" [syn: ...
