---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flung
offline_file: ""
offline_thumbnail: ""
uuid: 09841625-891f-4f97-b918-ea9d28521d70
updated: 1484310545
title: flung
categories:
    - Dictionary
---
fling
     n 1: a usually brief attempt; "he took a crack at it"; "I gave it
          a whirl" [syn: {crack}, {go}, {pass}, {whirl}, {offer}]
     2: a brief indulgence of your impulses [syn: {spree}]
     3: the act of flinging
     v 1: throw with force or recklessness; "fling the frisbee"
     2: move in an abrupt or headlong manner; "He flung himself onto
        the sofa"
     3: indulge oneself; "I splurged on a new TV" [syn: {splurge}]
     4: throw or cast away; "Put away your worries" [syn: {discard},
         {toss}, {toss out}, {toss away}, {chuck out}, {cast aside},
         {dispose}, {throw out}, {cast out}, {throw away}, {cast
        away}, {put away}]
     [also: {flung}]
