---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electromagnetic_spectrum
offline_file: ""
offline_thumbnail: ""
uuid: 6708c69b-8e1e-46dc-83f8-9413be457664
updated: 1484308889
title: Electromagnetic spectrum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/324px-Light_spectrum.svg.png
tags:
    - History of electromagnetic spectrum discovery
    - Range of the spectrum
    - Rationale for spectrum regional names
    - Types of radiation
    - Boundaries
    - Regions of the spectrum
    - Radio frequency
    - Microwaves
    - Terahertz radiation
    - Infrared radiation
    - Visible radiation (light)
    - Ultraviolet radiation
    - X-rays
    - Gamma rays
    - Notes and references
categories:
    - Waves
---
The electromagnetic spectrum is the collective term for all known frequencies and their linked wavelengths of the known photons ( electromagnetic radiation ). The "electromagnetic spectrum" of an object has a different meaning, and is instead the characteristic distribution of electromagnetic radiation emitted or absorbed by that particular object.[citation needed]
