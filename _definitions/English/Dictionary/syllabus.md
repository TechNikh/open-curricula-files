---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/syllabus
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464141
title: syllabus
categories:
    - Dictionary
---
syllabus
     n : an integrated course of academic studies; "he was admitted
         to a new program at the university" [syn: {course of
         study}, {program}, {programme}, {curriculum}]
     [also: {syllabi} (pl)]
