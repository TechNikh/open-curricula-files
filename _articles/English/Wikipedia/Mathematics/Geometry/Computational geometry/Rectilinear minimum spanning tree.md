---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Rectilinear_minimum_spanning_tree
offline_file: ""
offline_thumbnail: ""
uuid: 632f12b9-080c-4db8-86ca-39bf55135317
updated: 1484309179
title: Rectilinear minimum spanning tree
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Rectilinear_minimum_spanning_tree.svg.png
tags:
    - Properties and algorithms
    - Planar case
    - Applications
    - Electronic design
categories:
    - Computational geometry
---
In graph theory, the rectilinear minimum spanning tree (RMST) of a set of n points in the plane (or more generally, in ℝd) is a minimum spanning tree of that set, where the weight of the edge between each pair of points is the rectilinear distance between those two points.
