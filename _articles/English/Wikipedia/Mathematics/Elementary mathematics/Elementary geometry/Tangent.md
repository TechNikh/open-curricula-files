---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tangent
offline_file: ""
offline_thumbnail: ""
uuid: d7d01631-95f7-41bb-a395-42dc90ae90be
updated: 1484308938
title: Tangent
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tangent_to_a_curve.svg.png
tags:
    - History
    - Tangent line to a curve
    - Analytical approach
    - Intuitive description
    - More rigorous description
    - How the method can fail
    - Equations
    - Normal line to a curve
    - Angle between curves
    - Multiple tangents at a point
    - Tangent circles
    - Surfaces and higher-dimensional manifolds
categories:
    - Elementary geometry
---
In geometry, the tangent line (or simply tangent) to a plane curve at a given point is the straight line that "just touches" the curve at that point. Leibniz defined it as the line through a pair of infinitely close points on the curve.[1] More precisely, a straight line is said to be a tangent of a curve y = f (x) at a point x = c on the curve if the line passes through the point (c, f (c)) on the curve and has slope f '(c) where f ' is the derivative of f. A similar definition applies to space curves and curves in n-dimensional Euclidean space.
