---
version: 1
type: video
provider: YouTube
id: vlSOESXQI7o
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Atoms%20and%20Molecules%20-Basics%20-Animation%20lesson%20for%20kids.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f14fcba6-60c5-45dc-9234-798f67e9fba5
updated: 1486069664
title: Atoms and Molecules -Basics -Animation lesson for kids
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/vlSOESXQI7o/default.jpg
    - https://i3.ytimg.com/vi/vlSOESXQI7o/1.jpg
    - https://i3.ytimg.com/vi/vlSOESXQI7o/2.jpg
    - https://i3.ytimg.com/vi/vlSOESXQI7o/3.jpg
---
