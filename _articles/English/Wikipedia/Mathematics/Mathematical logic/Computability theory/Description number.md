---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Description_number
offline_file: ""
offline_thumbnail: ""
uuid: 97f2aa47-d36b-42fa-a00e-cb07c1537de2
updated: 1484309485
title: Description number
tags:
    - An example of a description number
    - Application to undecidability proofs
categories:
    - Computability theory
---
Description numbers are numbers that arise in the theory of Turing machines. They are very similar to Gödel numbers, and are also occasionally called "Gödel numbers" in the literature. Given some universal Turing machine, every Turing machine can, given its encoding on that machine, be assigned a number. This is the machine's description number. These numbers play a key role in Alan Turing's proof of the undecidability of the halting problem, and are very useful in reasoning about Turing machines as well.
