---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ecology
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484604781
title: ecology
categories:
    - Dictionary
---
ecology
     n 1: the environment as it relates to living organisms; "it
          changed the ecology of the island"
     2: the branch of biology concerned with the relations between
        organisms and their environment [syn: {bionomics}, {environmental
        science}]
