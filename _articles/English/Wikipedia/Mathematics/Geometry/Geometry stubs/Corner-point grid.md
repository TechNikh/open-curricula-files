---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Corner-point_grid
offline_file: ""
offline_thumbnail: ""
uuid: 734f99ff-d57b-475d-94d8-acfcc81ae509
updated: 1484309360
title: Corner-point grid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-TwoCells.png
categories:
    - Geometry stubs
---
A set of straight lines defined by their end points define the pillars of the corner-point grid. The pillars have a lexicographical ordering that determines neighbouring pillars. On each pillar, a constant number of nodes (corner-points) is defined. A corner-point cell is now the volume between 4 neighbouring pillars and two neighbouring points on each pillar.
