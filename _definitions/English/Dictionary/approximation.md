---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/approximation
offline_file: ""
offline_thumbnail: ""
uuid: 1e88d024-3f9d-4ef8-9f1f-4b3ac107be4f
updated: 1484310206
title: approximation
categories:
    - Dictionary
---
approximation
     n 1: an approximate calculation of quantity or degree or worth;
          "an estimate of what it would cost"; "a rough idea how
          long it would take" [syn: {estimate}, {estimation}, {idea}]
     2: the quality of coming near to identity (especially close in
        quantity)
     3: an imprecise or incomplete account; "newspapers gave only an
        approximation of the actual events"
     4: the act of bringing near or bringing together especially the
        cut edges of tissue [syn: {bringing close together}]
