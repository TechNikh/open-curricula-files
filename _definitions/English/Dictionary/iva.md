---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/iva
offline_file: ""
offline_thumbnail: ""
uuid: 534b4f15-86c0-4892-b024-68bff39c23ca
updated: 1484310399
title: iva
categories:
    - Dictionary
---
iva
     n : any of various coarse shrubby plants of the genus Iva with
         small greenish flowers; common in moist areas (as coastal
         salt marshes) of eastern and central North America [syn:
         {marsh elder}]
