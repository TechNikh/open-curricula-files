---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/debris
offline_file: ""
offline_thumbnail: ""
uuid: 083b8931-13dd-4f7e-8352-6cd8268f3a20
updated: 1484310461
title: debris
categories:
    - Dictionary
---
debris
     n : the remains of something that has been destroyed or broken
         up [syn: {dust}, {junk}, {rubble}, {detritus}]
