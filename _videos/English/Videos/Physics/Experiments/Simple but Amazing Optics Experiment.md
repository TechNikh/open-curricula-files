---
version: 1
type: video
provider: YouTube
id: 5RBsm0r_EaU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8b2ad58d-18d0-48d7-b1f0-6dc0e36e4e2d
updated: 1486069639
title: Simple but Amazing Optics Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/5RBsm0r_EaU/default.jpg
    - https://i3.ytimg.com/vi/5RBsm0r_EaU/1.jpg
    - https://i3.ytimg.com/vi/5RBsm0r_EaU/2.jpg
    - https://i3.ytimg.com/vi/5RBsm0r_EaU/3.jpg
---
