---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Forest-fire_model
offline_file: ""
offline_thumbnail: ""
uuid: 0faaa66a-aa0c-46ab-a3a5-16b4ebfdf31b
updated: 1484308828
title: Forest-fire model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Forest_fire_model.gif
categories:
    - Self-organization
---
In applied mathematics, a forest-fire model is any of a number of dynamical systems displaying self-organized criticality. Note, however, that according to Pruessner et al. (2002, 2004) the forest-fire model does not behave critically on very large, i.e. physically relevant scales. Early versions go back to Henley (1989) and Drossel and Schwabl (1992). The model is defined as a cellular automaton on a grid with Ld cells. L is the sidelength of the grid and d is its dimension. A cell can be empty, occupied by a tree, or burning. The model of Drossel and Schwabl (1992) is defined by four rules which are executed simultaneously:
