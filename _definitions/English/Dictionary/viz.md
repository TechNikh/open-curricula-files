---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/viz
offline_file: ""
offline_thumbnail: ""
uuid: 9e50cd5f-fded-4ae1-aa7d-9e9aa058fea9
updated: 1484310553
title: viz
categories:
    - Dictionary
---
viz.
     adv : as follows [syn: {namely}, {that is to say}, {videlicet}]
