---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/succus
offline_file: ""
offline_thumbnail: ""
uuid: 6c8c0d98-76c2-4a6c-b3fe-ce1ada7bae5f
updated: 1484310327
title: succus
categories:
    - Dictionary
---
succus
     n : any of several liquids of the body; "digestive juices" [syn:
          {juice}]
