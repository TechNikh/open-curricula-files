---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/defeat
offline_file: ""
offline_thumbnail: ""
uuid: 89a94aff-e5f1-41c8-a420-e5555667a14c
updated: 1484310555
title: defeat
categories:
    - Dictionary
---
defeat
     n 1: an unsuccessful ending [syn: {licking}] [ant: {victory}]
     2: the feeling that accompanies an experience of being thwarted
        in attaining your goals [syn: {frustration}]
     v 1: win a victory over; "You must overcome all difficulties";
          "defeat your enemies"; "He overcame his shyness"; "She
          conquered here fear of mice"; "He overcame his
          infirmity"; "Her anger got the better of her and she
          blew up" [syn: {get the better of}, {overcome}]
     2: thwart the passage of; "kill a motion"; "he shot down the
        student's proposal" [syn: {kill}, {shoot down}, {vote down},
         {vote out}]
