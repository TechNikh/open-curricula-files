---
version: 1
type: video
provider: YouTube
id: pQp6bmJPU_0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3e36c439-1d07-4db1-9f3d-b7692d147e3e
updated: 1486069682
title: 'Induction - An Introduction- Crash Course Physics #34'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/pQp6bmJPU_0/default.jpg
    - https://i3.ytimg.com/vi/pQp6bmJPU_0/1.jpg
    - https://i3.ytimg.com/vi/pQp6bmJPU_0/2.jpg
    - https://i3.ytimg.com/vi/pQp6bmJPU_0/3.jpg
---
