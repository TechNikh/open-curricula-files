---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pressure
offline_file: ""
offline_thumbnail: ""
uuid: 1a664c8b-b675-4326-9115-a557c1bdef50
updated: 1484310262
title: pressure
categories:
    - Dictionary
---
pressure
     n 1: the force applied to a unit area of surface; measured in
          pascals (SI unit) or in dynes (cgs unit); "the
          compressed gas exerts an increased pressure" [syn: {pressure
          level}, {force per unit area}]
     2: a force that compels; "the public brought pressure to bear
        on the government"
     3: the act of pressing; the exertion of pressure; "he gave the
        button a press"; "he used pressure to stop the bleeding";
        "at the pressing of a button" [syn: {press}, {pressing}]
     4: the state of urgently demanding notice or attention; "the
        press of business matters" [syn: {imperativeness}, {insistence},
         {insistency}, ...
