---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shop_drawing
offline_file: ""
offline_thumbnail: ""
uuid: 32257cdf-c22a-457a-8dd9-1fa19156f291
updated: 1484309331
title: Shop drawing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Post_and_Beam_Shop_Drawing.jpg
tags:
    - Information required to be included in Shop Drawings
    - Comparison information for the architect and engineer
    - >
        Notes of changes or alterations from the construction
        documents
    - Information needed to fabricate the product
    - >
        Indication of dimensions needing verification from the
        jobsite
    - Placement or installation information
    - Samples
    - >
        Computer-aided Mechanical, Electrical and Plumbing Design
        Coordination
    - Reviews
    - Number of copies
    - Submittal of one or two copies of the shop drawing
    - Submittal of a copy that can be reproduced
    - Compatible CAD software
    - Shop drawings in concrete reinforcing
categories:
    - Technical drawing
---
A shop drawing is a drawing or set of drawings produced by the contractor, supplier, manufacturer, subcontractor, or fabricator.[1] Shop drawings are typically required for prefabricated components. Examples of these include: elevators, structural steel, trusses, pre-cast, windows, appliances, cabinets, air handling units, and millwork. Also critical are the installation and coordination shop drawings of the MEP trades such as sheet metal ductwork, piping, plumbing, fire protection, and electrical. Shop drawings are not produced by architects and engineers under their contract with the owner. The shop drawing is the manufacturer’s or the contractor’s drawn version of information shown ...
