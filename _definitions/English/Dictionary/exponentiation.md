---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exponentiation
offline_file: ""
offline_thumbnail: ""
uuid: a64533d6-0fe1-4c2f-8d5e-79a8e4f328d0
updated: 1484310140
title: exponentiation
categories:
    - Dictionary
---
exponentiation
     n : the process of raising a quantity to some assigned power
         [syn: {involution}]
