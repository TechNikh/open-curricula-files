---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/antiquity
offline_file: ""
offline_thumbnail: ""
uuid: a1dbf89a-0496-4e75-94a6-40edb88fce5b
updated: 1484310379
title: antiquity
categories:
    - Dictionary
---
antiquity
     n 1: the historic period preceding the Middle Ages in Europe
     2: extreme oldness [syn: {ancientness}]
     3: an artifact surviving from the past
