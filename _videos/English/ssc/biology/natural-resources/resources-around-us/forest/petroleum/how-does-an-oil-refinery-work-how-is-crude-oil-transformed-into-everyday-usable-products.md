---
title: How does an oil refinery work? How is crude oil transformed into everyday usable products?
youtube_id: MdLVzXf7v5E
tags:
    - Oil Refinery (Taxonomy Subject)
    - Refinery
categories:
    - Petroleum
---
