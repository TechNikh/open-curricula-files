---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Giovanni_Girolamo_Saccheri
offline_file: ""
offline_thumbnail: ""
uuid: 5552015c-6fb0-429d-a755-e7f1817db8da
updated: 1484309098
title: Giovanni Girolamo Saccheri
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Saccheri_1733_-_Euclide_Ab_Omni_Naevo_Vindicatus.gif
categories:
    - Geometers
---
Giovanni Girolamo Saccheri (Italian pronunciation: [dʒoˈvanni dʒiˈrɔlamo sakˈkɛri]; 5 September 1667 – 25 October 1733) was an Italian Jesuit priest, scholastic philosopher, and mathematician.
