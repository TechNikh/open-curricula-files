---
version: 1
type: video
provider: YouTube
id: FwuPXchH2rA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Precalculus/Imaginary%20and%20complex%20numbers/Basic%20complex%20analysis.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2b164c2f-1677-4573-8350-9402adbdeeb2
updated: 1486069694
title: Basic complex analysis
categories:
    - Imaginary and complex numbers
thumbnail_urls:
    - https://i3.ytimg.com/vi/FwuPXchH2rA/default.jpg
    - https://i3.ytimg.com/vi/FwuPXchH2rA/1.jpg
    - https://i3.ytimg.com/vi/FwuPXchH2rA/2.jpg
    - https://i3.ytimg.com/vi/FwuPXchH2rA/3.jpg
---
