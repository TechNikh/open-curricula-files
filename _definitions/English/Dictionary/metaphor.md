---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/metaphor
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484652361
title: metaphor
categories:
    - Dictionary
---
metaphor
     n : a figure of speech in which an expression is used to refer
         to something that it does not literally denote in order
         to suggest a similarity
