---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flat_function
offline_file: ""
offline_thumbnail: ""
uuid: 12de509c-1134-4df5-973d-0910a4e5c8bb
updated: 1484309147
title: Flat function
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-FBN_exp%2528-1x2%2529.jpeg'
categories:
    - Algebraic geometry
---
In mathematics, especially real analysis, a flat function is a smooth function ƒ : ℝ → ℝ all of whose derivatives vanish at a given point x0 ∈ ℝ. The flat functions are, in some sense, the antitheses of the analytic functions. An analytic function ƒ : ℝ → ℝ is given by a convergent power series close to some point x0 ∈ ℝ:
