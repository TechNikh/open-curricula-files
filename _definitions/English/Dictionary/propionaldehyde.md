---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/propionaldehyde
offline_file: ""
offline_thumbnail: ""
uuid: 61df6214-20ac-4d1a-b516-bd5fd4dd95f1
updated: 1484310419
title: propionaldehyde
categories:
    - Dictionary
---
propionaldehyde
     n : a colorless liquid aldehyde [syn: {propanal}]
