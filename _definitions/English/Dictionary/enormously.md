---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enormously
offline_file: ""
offline_thumbnail: ""
uuid: 61254e6e-ea5e-469a-bf1e-e1e6ab66986d
updated: 1484310249
title: enormously
categories:
    - Dictionary
---
enormously
     adv : extremely; "he was enormously popular" [syn: {tremendously},
            {hugely}, {staggeringly}]
