---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liberalism
offline_file: ""
offline_thumbnail: ""
uuid: 4e5edc3a-5868-4cdd-9dbc-f2f8d7195951
updated: 1484310174
title: liberalism
categories:
    - Dictionary
---
liberalism
     n 1: a political orientation that favors progress and reform
     2: an economic theory advocating free competition and a
        self-regulating market and the gold standard
