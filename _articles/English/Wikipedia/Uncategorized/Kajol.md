---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kajol
offline_file: ""
offline_thumbnail: ""
uuid: 43c4e403-554a-4066-a3fa-5f9028ec2414
updated: 1484308470
title: Kajol
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Kajol%252C_Tanuja%252C_Tanisha_Mukherjee_at_Esha_Deol%2527s_wedding_reception_12.jpg'
tags:
    - Early life and background
    - Career
    - '1992–96: Debut and breakthrough'
    - '1997–98: Widespread success'
    - '1999–2001: Commercial fluctuations'
    - '2006–10; 2015: Comeback and recent work'
    - Off-screen work
    - Managerial work
    - Stage performance and television
    - Social work
categories:
    - Uncategorized
---
Kajol (born Kajol Mukherjee; 5 August 1974), also known by her married name Kajol Devgan, is an Indian film actress, who predominantly works in Hindi cinema. Born into the Mukherjee-Samarth family, she is the daughter of actress Tanuja Samarth and late filmmaker Shomu Mukherjee. Regarded as one of India's most successful and highest-paid actresses, Kajol is the recipient of numerous accolades, including six Filmfare Awards, among twelve nominations. Along with her late aunt Nutan, she holds the record for most Best Actress wins at Filmfare, with five. In 2011, the Government of India awarded her with the Padma Shri, the fourth highest civilian honour of the country.
