---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/outcome
offline_file: ""
offline_thumbnail: ""
uuid: f7d546c7-20de-4d7c-b0bd-1bd0017c066d
updated: 1484310357
title: outcome
categories:
    - Dictionary
---
outcome
     n 1: something that results; "he listened for the results on the
          radio" [syn: {result}, {resultant}, {final result}, {termination}]
     2: a phenomenon that follows and is caused by some previous
        phenomenon; "the magnetic effect was greater when the rod
        was lengthwise"; "his decision had depressing consequences
        for business"; "he acted very wise after the event" [syn:
        {consequence}, {effect}, {result}, {event}, {issue}, {upshot}]
