---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/annoyance
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484461141
title: annoyance
categories:
    - Dictionary
---
annoyance
     n 1: the psychological state of being irritated or annoyed [syn:
          {irritation}, {vexation}, {botheration}]
     2: anger produced by some annoying irritation [syn: {chafe}, {vexation}]
     3: an unpleasant person who is annoying or exasperating [syn: {aggravator}]
     4: something or someone that causes trouble; a source of
        unhappiness; "washing dishes was a nuisance before we got
        a dish washer"; "a bit of a bother"; "he's not a friend,
        he's an infliction" [syn: {bother}, {botheration}, {pain},
         {infliction}, {pain in the neck}, {pain in the ass}]
     5: the act of troubling or annoying someone [syn: {annoying}, {irritation},
       ...
