---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/expulsion
offline_file: ""
offline_thumbnail: ""
uuid: 48963204-417a-4463-9355-433133c9fc05
updated: 1484310325
title: expulsion
categories:
    - Dictionary
---
expulsion
     n 1: the act of forcing out someone or something; "the ejection
          of troublemakers by the police"; "the child's expulsion
          from school" [syn: {ejection}, {exclusion}, {riddance}]
     2: squeezing out by applying pressure; "an unexpected extrusion
        of toothpaste from the bottom of the tube"; "the expulsion
        of pus from the pimple" [syn: {extrusion}]
     3: the act of expelling or projecting or ejecting [syn: {projection},
         {ejection}, {forcing out}]
