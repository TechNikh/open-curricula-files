---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/malaria
offline_file: ""
offline_thumbnail: ""
uuid: 5e6b8692-251b-4f31-ae7d-0e27843eecbf
updated: 1484310473
title: malaria
categories:
    - Dictionary
---
malaria
     n : an infective disease caused by sporozoan parasites that are
         transmitted through the bite of an infected Anopheles
         mosquito; marked by paroxysms of chills and fever
