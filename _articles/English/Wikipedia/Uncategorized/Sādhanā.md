---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sadhana
offline_file: ""
offline_thumbnail: ""
uuid: 31aa56d9-99fc-4210-b56a-5dc9bc3129fd
updated: 1484308451
title: Sādhanā
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Taki-gyo_8118084.jpg
tags:
    - Paths
    - Tantric sādhana
    - Buddhism
    - Notes
categories:
    - Uncategorized
---
Sādhana (Sanskrit: sādhana; Tibetan: སྒྲུབ་ཐབས་, THL druptap, Chinese: 修行), literally "a means of accomplishing something",[1] is an ego-transcending spiritual practice.[2] It includes a variety of disciplines in Hindu,[3] Buddhist,[4] Jain[5] and Sikh traditions that are followed in order to achieve various spiritual or ritual objectives.
