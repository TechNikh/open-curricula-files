---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chimney
offline_file: ""
offline_thumbnail: ""
uuid: 84e8ac91-46c0-4568-a4cd-7ab940ca55fa
updated: 1484310413
title: chimney
categories:
    - Dictionary
---
chimney
     n 1: a vertical flue that provides a path through which smoke
          from a fire is carried away through the wall or roof of
          a building
     2: a glass flue surrounding the wick of an oil lamp [syn: {lamp
        chimney}]
