---
version: 1
type: video
provider: YouTube
id: 499MvHFrqUU
offline_file: ""
offline_thumbnail: ""
uuid: 90036fd3-d56c-4328-9451-04a8cfd5bcfe
updated: 1484040398
title: 'Example 2- Factor a quadratic binomial with two variables by taking a common factor'
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/499MvHFrqUU/default.jpg
    - https://i3.ytimg.com/vi/499MvHFrqUU/1.jpg
    - https://i3.ytimg.com/vi/499MvHFrqUU/2.jpg
    - https://i3.ytimg.com/vi/499MvHFrqUU/3.jpg
---
