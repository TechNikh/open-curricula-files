---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Formal_verification
offline_file: ""
offline_thumbnail: ""
uuid: 39a39937-4307-4e12-b73b-6ab33feda3d6
updated: 1484308681
title: Formal verification
tags:
    - Approaches
    - Software
    - Verification and validation
    - Automated program repair
    - Industry use
categories:
    - Theoretical computer science
---
In the context of hardware and software systems, formal verification is the act of proving or disproving the correctness of intended algorithms underlying a system with respect to a certain formal specification or property, using formal methods of mathematics.[1]
