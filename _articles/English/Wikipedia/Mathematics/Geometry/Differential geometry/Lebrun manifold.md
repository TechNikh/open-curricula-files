---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lebrun_manifold
offline_file: ""
offline_thumbnail: ""
uuid: dea6c672-5281-42d5-ae36-f5cc915f76e0
updated: 1484309226
title: Lebrun manifold
categories:
    - Differential geometry
---
In mathematics, a Lebrun manifold is a connected sum of copies of the complex projective plane, equipped with an explicit self-dual metric. Here, self-dual means that the Weyl tensor is its own Hodge star. The metric is determined by the choice of a finite collection of points in hyperbolic 3-space. These metrics were discovered by Claude LeBrun (1991), and named after LeBrun by Atiyah and Witten (2002).
