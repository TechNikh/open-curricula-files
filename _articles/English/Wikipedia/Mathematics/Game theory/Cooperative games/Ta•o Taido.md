---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Ta%E2%80%A2o_Taido'
offline_file: ""
offline_thumbnail: ""
uuid: 92aa2adc-30da-4fa1-ab56-b4354dc1b2e6
updated: 1484308528
title: Ta•o Taido
tags:
    - Gameplay
    - Characters
    - Boss
categories:
    - Cooperative games
---
Ta•o Taido (タオ体道?) is a 1993 fighting arcade game developed and published by Video System. It was Video System's only attempt in the fighting game genre, and it was created during the fighting game trend of the 1990s that was popularized by Capcom's 1991 arcade hit Street Fighter II.
