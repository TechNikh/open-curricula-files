---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/called
offline_file: ""
offline_thumbnail: ""
uuid: 68da2901-a2f0-4596-a224-8a2566b01cff
updated: 1484310344
title: called
categories:
    - Dictionary
---
called
     adj 1: given or having a specified name; "they called his name
            Jesus"; "forces...which Empedocles called `love' and
            `hate'"; "an actor named Harold Lloyd"; "a building in
            Cardiff named the Temple of Peace" [syn: {named}]
     2: known or spoken of as; "though she is named Katherine, she
        is called Kitty" [syn: {known as}]
