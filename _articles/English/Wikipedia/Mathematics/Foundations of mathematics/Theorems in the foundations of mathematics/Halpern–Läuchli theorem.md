---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Halpern%E2%80%93L%C3%A4uchli_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: eca3d34d-fe38-40c1-a453-9c25cf9291bb
updated: 1484308994
title: Halpern–Läuchli theorem
categories:
    - Theorems in the foundations of mathematics
---
In mathematics, the Halpern–Läuchli theorem is a partition result about finite products of infinite trees. Its original purpose was to give a model for set theory in which the Boolean prime ideal theorem is true but the axiom of choice is false. It is often called the Halpern–Läuchli theorem, but the proper attribution for the theorem as it is formulated below is to Halpern–Läuchli–Laver–Pincus or HLLP (named after James D. Halpern, Hans Läuchli, Richard Laver, and David Pincus), following (Milliken 1979).
