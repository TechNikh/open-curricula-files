---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Foot_wear
offline_file: ""
offline_thumbnail: ""
uuid: 4766d8b7-b2e7-4ac4-9b78-797808158e58
updated: 1484308371
title: Footwear
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-2014_Erywa%25C5%2584%252C_Damskie_buty_na_wysokim_obcasie_na_witrynie_sklepowej.jpg'
tags:
    - History
    - Materials
    - Components
    - Types
    - Boots
    - Shoes
    - Sandals
    - Indoor footwear
    - Specific footwear
    - Traditional footwear
    - Socks
    - Footwear industry
    - Safety of footwear products
categories:
    - Uncategorized
---
Footwear refers to garments worn on the feet, which originally serves to purpose of protection against adversities of the environment, usually regarding ground textures and temperature. Footwear in the manner of shoes therefore primarily serves the purpose to ease the locomotion and prevent injuries. Secondly footwear can also be used for fashion and adornment as well as to indicate the status or rank of the person within a social structure. Socks and other hosiery are typically worn additionally between the feet and other footwear for further comfort and relief.
