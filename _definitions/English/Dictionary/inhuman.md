---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/inhuman
offline_file: ""
offline_thumbnail: ""
uuid: bde79ea1-f0c3-49cc-a0e7-d5dae93f4903
updated: 1484310166
title: inhuman
categories:
    - Dictionary
---
inhuman
     adj 1: without compunction or human feeling; "in cold blood";
            "cold-blooded killing"; "insensate destruction" [syn:
            {cold}, {cold-blooded}, {insensate}]
     2: belonging to or resembling something nonhuman; "something
        dark and inhuman in form"; "a babel of inhuman noises"
