---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cognizance
offline_file: ""
offline_thumbnail: ""
uuid: 163e6533-0c21-4970-a6d7-d78b150d71fa
updated: 1484310189
title: cognizance
categories:
    - Dictionary
---
cognizance
     n 1: having knowledge of; "he had no awareness of his mistakes";
          "his sudden consciousness of the problem he faced";
          "their intelligence and general knowingness was
          impressive" [syn: {awareness}, {consciousness}, {cognisance},
           {knowingness}]
     2: range of what one can know or understand; "beyond my ken"
        [syn: {ken}]
     3: range or scope of what is perceived
