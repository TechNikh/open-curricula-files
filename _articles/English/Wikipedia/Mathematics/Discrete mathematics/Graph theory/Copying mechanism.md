---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Copying_mechanism
offline_file: ""
offline_thumbnail: ""
uuid: f6ed2f93-1633-4065-94cb-dccc886122c1
updated: 1484308614
title: Copying mechanism
tags:
    - Motivation
    - Description
    - Examples
    - Undirected network models
    - Protein interaction networks
    - Proteome networks
    - Directed network models
    - Biological networks
    - WWW networks and citation networks
    - Growing network with copying (GNC)
    - Notes
categories:
    - Graph theory
---
In the study of scale-free networks, a copying mechanism is a process by which such a network can form and grow, by means of repeated steps in which nodes are duplicated with mutations from existing nodes. Several variations of copying mechanisms have been studied. In the general copying model, a growing network starts as a small initial graph and, at each time step, a new vertex is added with a given number k of new outgoing edges. As a result of a stochastic selection, the neighbors of the new vertex are either chosen randomly among the existing vertices, or one existing vertex is randomly selected and k of its neighbors are ‘copied’ as heads of the new edges.[1]
