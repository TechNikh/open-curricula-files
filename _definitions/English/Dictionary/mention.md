---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mention
offline_file: ""
offline_thumbnail: ""
uuid: f5d8ad5b-b2a2-4e36-926b-ef33ddf048cc
updated: 1484310279
title: mention
categories:
    - Dictionary
---
mention
     n 1: a remark that calls attention to something or someone; "she
          made frequent mention of her promotion"; "there was no
          mention of it"; "the speaker made several references to
          his wife" [syn: {reference}]
     2: a short note recognizing a source of information or of a
        quoted passage; "the student's essay failed to list
        several important citations"; "the acknowledgments are
        usually printed at the front of a book"; "the article
        includes mention of similar clinical cases" [syn: {citation},
         {acknowledgment}, {credit}, {reference}, {quotation}]
     3: an official recognition of merit; "although he didn't win
   ...
