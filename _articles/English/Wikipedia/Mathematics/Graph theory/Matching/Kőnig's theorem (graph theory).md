---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/K%C5%91nig%27s_theorem_(graph_theory)'
offline_file: ""
offline_thumbnail: ""
uuid: a9de5125-0962-4391-94c7-9d1b748b00db
updated: 1484309426
title: "Kőnig's theorem (graph theory)"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Koenigs-theorem-graph.svg.png
tags:
    - Setting
    - History
    - Statement of the theorem
    - Example
    - Proof
    - Algorithm
    - Connections with perfect graphs
    - Notes
categories:
    - Matching
---
In the mathematical area of graph theory, König's theorem, proved by Dénes Kőnig (1931), describes an equivalence between the maximum matching problem and the minimum vertex cover problem in bipartite graphs. It was discovered independently, also in 1931, by Jenő Egerváry in the more general case of weighted graphs.
