---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ene
offline_file: ""
offline_thumbnail: ""
uuid: 43b83d40-4b61-4cab-b853-af13281bff5a
updated: 1484308401
title: Ene River
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Rio_Ene.png
categories:
    - Uncategorized
---
The Ene is formed at 12°15′45″S 73°58′30″W﻿ / ﻿12.26250°S 73.97500°W﻿ / -12.26250; -73.97500 at the confluence of the Mantaro River and the Apurímac River, circa 400 m above sea level, where the three Peruvian Regions Junín, Cusco, and Ayacucho meet.
