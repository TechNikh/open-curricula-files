---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Pomeau%E2%80%93Manneville_scenario'
offline_file: ""
offline_thumbnail: ""
uuid: 846644b7-3320-4634-93ac-edd57844c424
updated: 1484308709
title: Pomeau–Manneville scenario
categories:
    - Chaos theory
---
In the theory of dynamical systems (or turbulent flow), the Pomeau–Manneville scenario is the transition to chaos (turbulence) due to intermittency.[1][2]
