---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Marathwada
offline_file: ""
offline_thumbnail: ""
uuid: 57f3a1b0-e695-4476-9530-743f7562f40f
updated: 1484308455
title: Marathwada
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Mir_osman_ali_khan.JPG
tags:
    - Etymology
    - Historical highlights
    - Demography
    - Districts
    - Major Cities
    - Tourism
    - Education
    - Marathwada Statutory Development Board
    - Major Findings of Annual Report
    - Suicide of farmers
    - Gallery
categories:
    - Uncategorized
---
Marathwada (IPA:Marāṭhvāḍā) is one of the five regions in Indian state of Maharashtra. The region coincides with the Aurangabad Division of Maharashtra.
