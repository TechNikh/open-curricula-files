---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mid
offline_file: ""
offline_thumbnail: ""
uuid: 397131c4-54a2-4394-9274-2a5fe5199490
updated: 1484310316
title: mid
categories:
    - Dictionary
---
mid
     adj : used in combination to denote the middle; "midmorning";
           "midsummer"; "in mid-1958"; "a mid-June wedding" [syn:
           {mid(a)}]
