---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cash
offline_file: ""
offline_thumbnail: ""
uuid: 6d051e12-55c5-42e1-aefe-94b82f9cd253
updated: 1484310464
title: cash
categories:
    - Dictionary
---
cash
     n 1: money in the form of bills or coins [syn: {hard cash}, {hard
          currency}]
     2: prompt payment for goods or services in currency or by check
        [syn: {immediate payment}] [ant: {credit}]
     v : exchange for cash; "I cashed the check as soon as it arrived
         in the mail" [syn: {cash in}]
