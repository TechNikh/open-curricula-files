---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consternation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484459941
title: consternation
categories:
    - Dictionary
---
consternation
     n : fear resulting from the awareness of danger [syn: {alarm}, {dismay}]
