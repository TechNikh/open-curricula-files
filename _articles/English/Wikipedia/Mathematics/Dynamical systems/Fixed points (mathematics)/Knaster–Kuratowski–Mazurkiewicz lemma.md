---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Knaster%E2%80%93Kuratowski%E2%80%93Mazurkiewicz_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 27e7fde4-33dd-4bf9-a262-645a56f3c8d6
updated: 1484308786
title: Knaster–Kuratowski–Mazurkiewicz lemma
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-KKM_example.png
tags:
    - Statement
    - Example
    - Equivalent results
categories:
    - Fixed points (mathematics)
---
The Knaster–Kuratowski–Mazurkiewicz lemma is a basic result in mathematical fixed-point theory published in 1929 by Knaster, Kuratowski and Mazurkiewicz.[1]
