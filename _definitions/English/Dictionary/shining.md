---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shining
offline_file: ""
offline_thumbnail: ""
uuid: 15bb86db-67ba-4b07-99d5-cad7cd81b673
updated: 1484310208
title: shining
categories:
    - Dictionary
---
shining
     adj 1: marked by exceptional merit; "had shining virtues and few
            faults"; "a shining example"
     2: made smooth and bright by or as if by rubbing; reflecting a
        sheen or glow; "bright silver candlesticks"; "a burnished
        brass knocker"; "she brushed her hair until it fell in
        lustrous auburn waves"; "rows of shining glasses"; "shiny
        black patents" [syn: {bright}, {burnished}, {lustrous}, {shiny}]
     3: abounding with sunlight; "a bright sunny day"; "one shining
        norming"- John Muir; "when it is warm and shiny" [syn: {bright},
         {shiny}, {sunshiny}, {sunny}]
     4: reflecting light; "glistening bodies of swimmers"; "the
 ...
