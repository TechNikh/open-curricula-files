---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fantastic
offline_file: ""
offline_thumbnail: ""
uuid: 2632131b-6539-4ffb-acbe-7318bc4df393
updated: 1484310309
title: fantastic
categories:
    - Dictionary
---
fantastic
     adj 1: ludicrously odd; "Hamlet's assumed antic disposition";
            "fantastic Halloween costumes"; "a grotesque
            reflection in the mirror" [syn: {antic}, {fantastical},
             {grotesque}]
     2: extraordinarily good; used especially as intensifiers; "a
        fantastic trip to the Orient"; "the film was fantastic!";
        "a howling success"; "a marvelous collection of rare
        books"; "had a rattling conversation about politics"; "a
        tremendous achievement" [syn: {howling(a)}, {marvelous}, {marvellous},
         {rattling(a)}, {terrific}, {tremendous}, {wonderful}, {wondrous}]
     3: extravagantly fanciful and unrealistic; foolish; ...
