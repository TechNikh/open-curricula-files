---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irritation
offline_file: ""
offline_thumbnail: ""
uuid: 51a539b7-5179-4b4f-a37a-96d74f441de4
updated: 1484310384
title: irritation
categories:
    - Dictionary
---
irritation
     n 1: the psychological state of being irritated or annoyed [syn:
          {annoyance}, {vexation}, {botheration}]
     2: a sudden outburst of anger; "his temper sparked like damp
        firewood" [syn: {pique}, {temper}]
     3: (pathology) abnormal sensitivity to stimulation; "any food
        produced irritation of the stomach"
     4: the neural or electrical arousal of an organ or muscle or
        gland [syn: {excitation}, {innervation}]
     5: an uncomfortable feeling in some part of the body [syn: {discomfort},
         {soreness}]
     6: unfriendly behavior that causes anger or resentment [syn: {aggravation},
         {provocation}]
     7: the act of troubling ...
