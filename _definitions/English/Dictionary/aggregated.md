---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aggregated
offline_file: ""
offline_thumbnail: ""
uuid: 34907f90-dc97-4729-8e94-5c1c78eb20b3
updated: 1484310426
title: aggregated
categories:
    - Dictionary
---
aggregated
     adj : gathered or tending to gather into a mass or whole;
           "aggregate expenses include expenses of all divisions
           combined for the entire year"; "the aggregated amount
           of indebtedness" [syn: {aggregate}, {aggregative}, {mass}]
