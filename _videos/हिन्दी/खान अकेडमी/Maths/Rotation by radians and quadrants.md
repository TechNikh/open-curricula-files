---
version: 1
type: video
provider: YouTube
id: G7GfjAESMjo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e355c94b-b92a-4d7b-9ee9-8b73afac6d28
updated: 1486069607
title: Rotation by radians and quadrants
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/G7GfjAESMjo/default.jpg
    - https://i3.ytimg.com/vi/G7GfjAESMjo/1.jpg
    - https://i3.ytimg.com/vi/G7GfjAESMjo/2.jpg
    - https://i3.ytimg.com/vi/G7GfjAESMjo/3.jpg
---
