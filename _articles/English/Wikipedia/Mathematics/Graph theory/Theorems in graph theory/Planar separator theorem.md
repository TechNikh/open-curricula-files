---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Planar_separator_theorem
offline_file: ""
offline_thumbnail: ""
uuid: d7cc4e59-80a7-4f58-b771-86d44f58bedc
updated: 1484309457
title: Planar separator theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Grid_separator.svg_0.png
tags:
    - Statement of the theorem
    - Example
    - Constructions
    - Breadth-first layering
    - Simple cycle separators
    - Circle separators
    - Spectral partitioning
    - Edge separators
    - Lower bounds
    - Separator hierarchies
    - Other classes of graphs
    - Applications
    - Divide and conquer algorithms
    - Exact solution of NP-hard optimization problems
    - Approximation algorithms
    - Graph compression
    - Universal graphs
    - Notes
categories:
    - Theorems in graph theory
---
In graph theory, the planar separator theorem is a form of isoperimetric inequality for planar graphs, that states that any planar graph can be split into smaller pieces by removing a small number of vertices. Specifically, the removal of O(√n) vertices from an n-vertex graph (where the O invokes big O notation) can partition the graph into disjoint subgraphs each of which has at most 2n/3 vertices.
