---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/memorandum
offline_file: ""
offline_thumbnail: ""
uuid: 6088b573-5a6c-4cd1-bd82-471ad410ef52
updated: 1484310480
title: memorandum
categories:
    - Dictionary
---
memorandum
     n : a written proposal or reminder [syn: {memo}, {memoranda}]
     [also: {memoranda} (pl)]
