---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Network_scheduler
offline_file: ""
offline_thumbnail: ""
uuid: def94a74-b35c-4d9f-acdf-43795c5470cf
updated: 1484309438
title: Network scheduler
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Data_Queue.svg.png
tags:
    - Network scheduling algorithms
    - Bufferbloat
    - Terminology
    - Implementations
    - Linux kernel
    - BSD
categories:
    - Network theory
---
A network scheduler, also called packet scheduler, is an arbiter program on a node in packet switching communication network. It manages the sequence of network packets in the transmit and receive queues of the network interface controller, which is a circular data buffer. There are several network schedulers available for the different operating system kernels, that implement many of the existing network scheduling algorithms.
