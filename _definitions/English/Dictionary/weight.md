---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/weight
offline_file: ""
offline_thumbnail: ""
uuid: dab62618-d5ff-444a-9b90-b99f2a4c0b8d
updated: 1484310295
title: weight
categories:
    - Dictionary
---
weight
     n 1: the vertical force exerted by a mass as a result of gravity
     2: sports equipment used in calisthenic exercises and
        weightlifting; a weight that is not attached to anything
        and is raised and lowered by use of the hands and arms
        [syn: {free weight}, {exercising weight}]
     3: the relative importance granted to something; "his opinion
        carries great weight"
     4: an artifact that is heavy
     5: an oppressive feeling of heavy force; "bowed down by the
        weight of responsibility"
     6: a system of units used to express the weight of something
        [syn: {system of weights}]
     7: a unit used to measure weight; "he placed two ...
