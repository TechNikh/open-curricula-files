---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uma
offline_file: ""
offline_thumbnail: ""
uuid: 08c8cdc1-8304-420f-8d1d-3cfd57c7d3c0
updated: 1484310156
title: uma
categories:
    - Dictionary
---
Uma
     n 1: a benevolent aspect of Devi; `splendor'
     2: fringe-toed lizard [syn: {genus Uma}]
