---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Number_bond
offline_file: ""
offline_thumbnail: ""
uuid: cc887162-133e-4b99-9f93-83aefc2970d5
updated: 1484308924
title: Number bond
tags:
    - History
categories:
    - Elementary arithmetic
---
In mathematics education at primary school level, a number bond (sometimes alternatively called an addition fact) is a simple addition sum which has become so familiar that a child can recognise it and complete it almost instantly, with recall as automatic as that of an entry from a multiplication table in multiplication.
