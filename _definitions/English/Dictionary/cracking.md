---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cracking
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484568781
title: cracking
categories:
    - Dictionary
---
cracking
     adj : very good; "he did a bully job"; "a neat sports car"; "had a
           great time at the party"; "you look simply smashing"
           [syn: {bang-up}, {bully}, {corking}, {dandy}, {great},
           {groovy}, {keen}, {neat}, {nifty}, {not bad(p)}, {peachy},
            {slap-up}, {swell}, {smashing}]
     n 1: a sudden sharp noise; "the crack of a whip"; "he heard the
          cracking of the ice"; "he can hear the snap of a twig"
          [syn: {crack}, {snap}]
     2: the act of cracking something [syn: {fracture}, {crack}]
     3: the process whereby heavy molecules of naphtha or petroleum
        are broken down into hydrocarbons of lower molecular
        ...
