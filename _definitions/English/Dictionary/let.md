---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/let
offline_file: ""
offline_thumbnail: ""
uuid: ba571d94-019c-4240-8f87-557a6a799fcb
updated: 1484310361
title: let
categories:
    - Dictionary
---
LET
     n 1: the most brutal terrorist group active in Kashmir; fights
          against India with the goal of restoring Islamic rule of
          India; "Lashkar-e-Toiba has committed mass murders of
          civilian Hindus" [syn: {Lashkar-e-Taiba}, {Lashkar-e-Toiba},
           {Lashkar-e-Tayyiba}, {Army of the Pure}, {Army of the
          Righteous}]
     2: a serve that strikes the net before falling into the
        receiver's court; the ball must be served again [syn: {net
        ball}]
     v 1: make it possible through a specific action or lack of action
          for something to happen; "This permits the water to rush
          in"; "This sealed door won't allow the water ...
