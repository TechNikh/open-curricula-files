---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Rado%27s_theorem_(Ramsey_theory)'
offline_file: ""
offline_thumbnail: ""
uuid: 6714874b-f9b7-4560-a8f1-f963668d53db
updated: 1484308669
title: "Rado's theorem (Ramsey theory)"
categories:
    - Theorems in discrete mathematics
---
Rado's theorem is a theorem from the branch of mathematics known as Ramsey theory. It is named for the German mathematician Richard Rado. It was proved in his thesis, Studien zur Kombinatorik.
