---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/honoured
offline_file: ""
offline_thumbnail: ""
uuid: ab7cf4db-6c02-4214-8884-8e4907d289dd
updated: 1484310593
title: honoured
categories:
    - Dictionary
---
honoured
     adj : worthy of honor; "an honored name"; "our honored dead" [syn:
            {honored}]
