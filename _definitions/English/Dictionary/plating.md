---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plating
offline_file: ""
offline_thumbnail: ""
uuid: d9453320-8d43-43ef-94a3-e1a3ab315636
updated: 1484310379
title: plating
categories:
    - Dictionary
---
plating
     n 1: a thin coating of metal deposited on a surface [syn: {metal
          plating}]
     2: the application of a thin coat of metal (as by electrolysis)
