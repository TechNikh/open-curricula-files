---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/applicable
offline_file: ""
offline_thumbnail: ""
uuid: 9b7d1370-54b6-4e2d-a3c0-d1db4afab525
updated: 1484310220
title: applicable
categories:
    - Dictionary
---
applicable
     adj 1: capable of being applied; having relevance; "gave applicable
            examples to support her argument"
     2: readily applicable or practical [syn: {applicative}, {applicatory}]
