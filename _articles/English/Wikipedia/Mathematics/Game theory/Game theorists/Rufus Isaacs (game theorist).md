---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rufus_Isaacs_(game_theorist)
offline_file: ""
offline_thumbnail: ""
uuid: 3517bd98-05fb-4d40-9933-4180f3937907
updated: 1484308512
title: Rufus Isaacs (game theorist)
tags:
    - Biography
    - Selected work(s)
    - The Isaacs Award
    - Notes
categories:
    - Game theorists
---
Rufus Philip Isaacs (June 11, 1914 – January 18, 1981) was a game theorist especially prominent in the 1950s and 1960s with his work on differential games.
