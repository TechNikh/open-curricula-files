---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Whiteprint
offline_file: ""
offline_thumbnail: ""
uuid: 151387ce-479e-4892-8028-97efa2717a96
updated: 1484309334
title: Whiteprint
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Heliographic_copy.jpg
tags:
    - The diazo printing process
    - Fading prints
    - Document control
    - Demise of the technology
categories:
    - Technical drawing
---
Whiteprint describes a document reproduction produced by using the diazo chemical process. It is also known as the blue-line process since the result is blue lines on a white background. It is a contact printing process which accurately reproduces the original in size, but cannot reproduce continuous tones or colors. The light-sensitivity of the chemicals used was known in the 1890s and several related printing processes were patented at that time. Whiteprinting replaced the blueprint process for reproducing architectural and engineering drawings because the process was simpler and involved fewer toxic chemicals. A blue-line print is not permanent and will fade if exposed to light for weeks ...
