---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Resource_productivity
offline_file: ""
offline_thumbnail: ""
uuid: eaa2fbe1-362c-4837-8580-7f8a729b0d80
updated: 1484308857
title: Resource productivity
categories:
    - Thermodynamics
---
Resource productivity is the quantity of good or service (outcome) that is obtained through the expenditure of unit resource.[1][2][3] This can be expressed in monetary terms as the monetary yield per unit resource.
