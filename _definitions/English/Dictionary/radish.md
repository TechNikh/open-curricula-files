---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/radish
offline_file: ""
offline_thumbnail: ""
uuid: 308d5aec-a308-4fd7-adad-6b938214f662
updated: 1484310148
title: radish
categories:
    - Dictionary
---
radish
     n 1: pungent fleshy edible root
     2: pungent edible root of any of various cultivated radish
        plants
     3: Eurasian plant widely cultivated for its edible pungent root
        usually eaten raw [syn: {Raphanus sativus}]
