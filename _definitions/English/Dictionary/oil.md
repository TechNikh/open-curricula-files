---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/oil
offline_file: ""
offline_thumbnail: ""
uuid: cbe5da0d-1c15-41da-b06c-61ce87318d6c
updated: 1484310335
title: oil
categories:
    - Dictionary
---
oil
     n 1: a slippery or viscous liquid or liquefiable substance not
          miscible with water
     2: oil paint used by an artist [syn: {oil color}]
     3: any of a group of liquid edible fats that are obtained from
        plants [syn: {vegetable oil}]
     v 1: cover with oil, as if by rubbing; "oil the wooden surface"
     2: administer an oil or ointment to ; often in a religious
        ceremony of blessing [syn: {anoint}, {inunct}, {anele}, {embrocate}]
