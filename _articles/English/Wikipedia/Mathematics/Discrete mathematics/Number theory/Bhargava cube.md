---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bhargava_cube
offline_file: ""
offline_thumbnail: ""
uuid: ffe60eed-afd2-4e4b-8cff-cef25cb9fa7c
updated: 1484308630
title: Bhargava cube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-BhargavaCube.png
tags:
    - Integer binary quadratic forms
    - Gauss composition of integer binary quadratic forms
    - Quadratic forms associated with the Bhargava cube
    - The three forms
    - Example
    - Further composition laws on forms
    - Composition of cubes
    - Composition of cubic forms
    - Composition of pairs of binary quadratic forms
categories:
    - Number theory
---
In mathematics, in number theory, Bhargava cube (also called Bhargava's cube) is a configuration consisting of eight integers placed at the eight corners of a cube.[1] This configuration was extensively used by Manjul Bhargava, an Indian-American Fields Medal winning mathematician, to study the composition laws of binary quadratic forms and other such forms. To each pair of opposite faces of a Bhargava cube one can associate an integer binary quadratic form thus getting three binary quadratic forms corresponding to the three pairs of opposite faces of the Bhargava cube.[2] These three quadratic forms all have the same discriminant and Manjul Bhargava proved that their composition in the ...
