---
version: 1
type: video
provider: YouTube
id: FnrqBgot3jM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Finding%20the%20inequality%20representing%20the%20graph%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 15c878d4-341d-46aa-97cd-5e5dc403a956
updated: 1486069557
title: Finding the inequality representing the graph example
categories:
    - Algebra I
thumbnail_urls:
    - https://i3.ytimg.com/vi/FnrqBgot3jM/default.jpg
    - https://i3.ytimg.com/vi/FnrqBgot3jM/1.jpg
    - https://i3.ytimg.com/vi/FnrqBgot3jM/2.jpg
    - https://i3.ytimg.com/vi/FnrqBgot3jM/3.jpg
---
