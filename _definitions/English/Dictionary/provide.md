---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/provide
offline_file: ""
offline_thumbnail: ""
uuid: 53279f67-9b82-4c92-b6d6-e3609bdc74a5
updated: 1484310264
title: provide
categories:
    - Dictionary
---
provide
     v 1: provide or furnish with; "We provided the room with an
          electrical heater" [syn: {supply}, {render}, {furnish}]
     2: provide what is desired or needed, especially support, food
        or sustenance; "The hostess provided lunch for all the
        guests" [syn: {supply}, {ply}, {cater}]
     3: determine (what is to happen in certain contingencies),
        especially by including a proviso condition or
        stipulation; "The will provides that each child should
        receive half of the money"; "The Constitution provides for
        the right to free speech"
     4: mount or put up; "put up a good fight"; "offer resistance"
        [syn: {put up}, ...
