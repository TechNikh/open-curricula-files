---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Landauer%27s_principle'
offline_file: ""
offline_thumbnail: ""
uuid: cdc0daf1-955c-414a-b493-221504d0567c
updated: 1484308771
title: "Landauer's principle"
tags:
    - History
    - Rationale
    - Equation
    - Challenges
categories:
    - Entropy and information
---
Landauer's principle is a physical principle pertaining to the lower theoretical limit of energy consumption of computation. It holds that "any logically irreversible manipulation of information, such as the erasure of a bit or the merging of two computation paths, must be accompanied by a corresponding entropy increase in non-information-bearing degrees of freedom of the information-processing apparatus or its environment".[1]
