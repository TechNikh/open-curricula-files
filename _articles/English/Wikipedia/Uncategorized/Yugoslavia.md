---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yugoslavia
offline_file: ""
offline_thumbnail: ""
uuid: 3c180d4e-c846-4a12-99d5-0aaaf8d41760
updated: 1484308476
title: Yugoslavia
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Yugoslavia_location_map.svg.png
tags:
    - Background
    - Kingdom of Yugoslavia
    - King Alexander
    - 1934–1941
    - World War II
    - SFR Yugoslavia
    - The 1948 Yugoslavia-Soviet split
    - Demographics
    - government
    - Ethnic tensions and economic crisis
categories:
    - Uncategorized
---
Yugoslavia (Serbo-Croatian: Jugoslavija/Југославија, Slovene: Jugoslavija, Macedonian: Југославија) was a country in Southeast Europe during most of the 20th century. It came into existence after World War I in 1918[i] under the name of the Kingdom of Serbs, Croats and Slovenes by the merger of the provisional State of Slovenes, Croats and Serbs (itself formed from territories of the former Austro-Hungarian Empire) with the formerly independent Kingdom of Serbia. The Serbian royal House of Karađorđević became the Yugoslav royal dynasty. Yugoslavia gained international recognition on 13 July 1922 at the Conference of Ambassadors in Paris.[2] The country was named ...
