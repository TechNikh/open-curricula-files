---
version: 1
type: video
provider: YouTube
id: NAaHPRsveJk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0e172e8d-bafb-41f5-9a99-7750b9d0a497
updated: 1486069637
title: Total Internal Reflection
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/NAaHPRsveJk/default.jpg
    - https://i3.ytimg.com/vi/NAaHPRsveJk/1.jpg
    - https://i3.ytimg.com/vi/NAaHPRsveJk/2.jpg
    - https://i3.ytimg.com/vi/NAaHPRsveJk/3.jpg
---
