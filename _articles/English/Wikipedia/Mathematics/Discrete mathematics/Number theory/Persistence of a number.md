---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Persistence_of_a_number
offline_file: ""
offline_thumbnail: ""
uuid: d1187ebc-a248-4239-b509-779de583cc95
updated: 1484308654
title: Persistence of a number
categories:
    - Number theory
---
In mathematics, the persistence of a number is the number of times one must apply a given operation to an integer before reaching a fixed point at which the operation no longer alters the number.
