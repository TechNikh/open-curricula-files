---
version: 1
type: article
id: https://en.wikipedia.org/wiki/De_Prospectiva_Pingendi
offline_file: ""
offline_thumbnail: ""
uuid: ebe22140-f722-4ff7-ae8e-5c214dd3d53c
updated: 1484309245
title: De Prospectiva Pingendi
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Piero%252C_proiezioni_di_una_testa_scorciata_dal_de_prospectiva_pingendi%252C_ante_1482%252C_milano%252C_biblioteca_ambrosiana.jpg'
tags:
    - The book
    - History
categories:
    - History of geometry
---
De Prospectiva pingendi (On the Perspective of painting) is the earliest and only pre–1500 Renaissance treatise solely devoted to the subject of perspective.[1] It was written by the Italian master Piero della Francesca late in his career but by c1474.[2] Despite its Latin title, the opus is written in Italian.
