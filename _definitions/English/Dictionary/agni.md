---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/agni
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484625061
title: agni
categories:
    - Dictionary
---
Agni
     n : (Sanskrit) god of fire in ancient and traditional India; one
         of the three chief deities of the Vedas
