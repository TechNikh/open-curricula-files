---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Rook_and_bishop_versus_rook_endgame
offline_file: ""
offline_thumbnail: ""
uuid: ad3219d3-1bd6-4fdb-a038-fd6cc4021bc0
updated: 1484309026
title: Rook and bishop versus rook endgame
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/Andr%25C3%25A9_Philidor_1.jpg'
tags:
    - History
    - Winning positions
    - Philidor position
    - Lolli position
    - Drawing defenses
    - Cochrane Defense
    - Budnikov versus Novik
    - Ljubojević versus Portisch
    - Second-rank defense
    - Norri versus Atalik
    - Carlsen versus van Wely
    - Szén position
    - Second Lolli position
    - Notes
categories:
    - Chess theory
---
The rook and bishop versus rook endgame is a chess endgame where one player has just a rook, bishop and king, and the other player has only a rook and king. It has been studied many times through the years. This combination of material is one of the most common pawnless chess endgames. It is generally a theoretical draw, but the rook and bishop have good winning chances in practice because the defense is difficult. Ulf Andersson won the position twice within a year, once against a grandmaster and once against a candidate grandmaster; and grandmaster Keith Arkell has won it 18 times out of 18 (Hawkins 2012:193).[1] In positions that have a forced win, up to 59 moves are required (Speelman, ...
