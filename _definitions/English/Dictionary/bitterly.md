---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bitterly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484524981
title: bitterly
categories:
    - Dictionary
---
bitterly
     adv 1: in a resentful manner; "she complained bitterly" [syn: {with
            bitterness}]
     2: indicating something hard to accept; "he was bitterly
        disappointed"
     3: extremely and sharply; "it was bitterly cold"; "bitter cold"
        [syn: {piercingly}, {bitingly}, {bitter}]
