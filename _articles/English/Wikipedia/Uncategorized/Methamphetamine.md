---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Meth
offline_file: ""
offline_thumbnail: ""
uuid: 025ede11-4754-4d34-a09e-b8c820d93547
updated: 1484308398
title: Methamphetamine
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Racemic_methamphetamine.svg.png
tags:
    - Uses
    - Medical
    - Recreational
    - Contraindications
    - Side effects
    - Physical
    - Meth mouth
    - Sexually transmitted infection
    - Psychological
    - Neurotoxicity and neuroimmune response
    - Overdose
    - Psychosis
    - Emergency treatment
    - Addiction
    - Treatment and management
    - Dependence and withdrawal
    - Interactions
    - Pharmacology
    - Pharmacodynamics
    - Pharmacokinetics
    - Detection in biological fluids
    - chemistry
    - Degradation
    - Synthesis
    - History, society, and culture
    - Legal status
    - Australia
    - Research
    - Notes
    - Reference notes
categories:
    - Uncategorized
---
Methamphetamine[note 1] (contracted from N-methylamphetamine) is a strong central nervous system (CNS) stimulant that is mainly used as a recreational drug and less commonly as a treatment for attention deficit hyperactivity disorder and obesity. Methamphetamine was discovered in 1893 and exists as two enantiomers: levo-methamphetamine and dextro-methamphetamine.[note 2] Methamphetamine properly refers to a specific chemical, the racemic free base, which is an equal mixture of levomethamphetamine and dextromethamphetamine in their pure amine forms. It is rarely prescribed due to concerns involving human neurotoxicity and potential for recreational use as an aphrodisiac and euphoriant, among ...
