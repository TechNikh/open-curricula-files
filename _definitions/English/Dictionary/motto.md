---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/motto
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484438041
title: motto
categories:
    - Dictionary
---
motto
     n : a favorite saying of a sect or political group [syn: {slogan},
          {catchword}, {shibboleth}]
     [also: {mottoes} (pl)]
