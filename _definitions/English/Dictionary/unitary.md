---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unitary
offline_file: ""
offline_thumbnail: ""
uuid: 0579e322-b799-4168-9cf3-9e737e4631c9
updated: 1484310597
title: unitary
categories:
    - Dictionary
---
unitary
     adj 1: relating to or characterized by or aiming toward unity; "the
            unitary principles of nationalism"; "a unitary
            movement in politics"
     2: of or pertaining to or involving the use of units; "a
        unitary method was applied"; "established a unitary
        distance on which to base subsequent calculations"
     3: characterized by or constituting a form of government in
        which power is held by one central authority; "a unitary
        as opposed to a federal form of government" [ant: {federal}]
     4: having the indivisible character of a unit; "a unitary
        action"; "spoke with one voice" [syn: {one(a)}]
