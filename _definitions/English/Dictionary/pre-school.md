---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pre-school
offline_file: ""
offline_thumbnail: ""
uuid: 3149d4a7-6482-49e0-a5f1-1da5c8650895
updated: 1484310535
title: pre-school
categories:
    - Dictionary
---
preschool
     n : an educational institution for children too young for
         elementary school
