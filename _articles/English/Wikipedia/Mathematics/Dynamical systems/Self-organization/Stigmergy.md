---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stigmergy
offline_file: ""
offline_thumbnail: ""
uuid: 0b8029c2-3e75-464e-b596-72087bb64572
updated: 1484308834
title: Stigmergy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Safari_ants.jpg
tags:
    - History
    - Stigmergic behavior in lower organisms
    - Analysis of human behavior
    - Analysis of human social movements
    - Stigmergic society
categories:
    - Self-organization
---
Stigmergy is a mechanism of indirect coordination, through the environment, between agents or actions.[1] The principle is that the trace left in the environment by an action stimulates the performance of a next action, by the same or a different agent. In that way, subsequent actions tend to reinforce and build on each other, leading to the spontaneous emergence of coherent, apparently systematic activity.
