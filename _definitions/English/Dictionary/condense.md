---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/condense
offline_file: ""
offline_thumbnail: ""
uuid: d627d0d0-2cd2-49a6-ad9e-901823221ad5
updated: 1484310226
title: condense
categories:
    - Dictionary
---
condense
     v 1: undergo condensation; change from a gaseous to a liquid
          state and fall in drops; "water condenses"; "The acid
          distills at a specific temperature" [syn: {distill}, {distil}]
     2: make more concise; "condense the contents of a book into a
        summary" [syn: {digest}, {concentrate}]
     3: remove water from; "condense the milk"
     4: cause a gas or vapor to change into a liquid; "The cold air
        condensed the steam"
     5: become more compact or concentrated; "Her feelings
        condensed"
     6: develop due to condensation; "All our planets condensed out
        of the same material"
     7: compress or concentrate; "Congress condensed ...
