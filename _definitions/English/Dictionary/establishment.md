---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/establishment
offline_file: ""
offline_thumbnail: ""
uuid: 40ebbe2f-15ed-4278-8187-2d24c24cd90e
updated: 1484310479
title: establishment
categories:
    - Dictionary
---
establishment
     n 1: the act of forming something; "the constitution of a PTA
          group last year"; "it was the establishment of his
          reputation"; "he still remembers the organization of the
          club" [syn: {constitution}, {formation}, {organization},
           {organisation}]
     2: an organization founded and united for a specific purpose
        [syn: {institution}]
     3: the persons (or committees or departments etc.) who make up
        a body for the purpose of administering something; "he
        claims that the present administration is corrupt"; "the
        governance of an association is responsible to its
        members"; "he quickly became ...
