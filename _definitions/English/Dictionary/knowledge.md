---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/knowledge
offline_file: ""
offline_thumbnail: ""
uuid: b960ad5d-cdff-4a1c-b0fc-3fd1664e3ba6
updated: 1484310349
title: knowledge
categories:
    - Dictionary
---
knowledge
     n : the psychological result of perception and learning and
         reasoning [syn: {cognition}, {noesis}]
