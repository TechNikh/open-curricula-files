---
version: 1
type: video
provider: YouTube
id: BsVX0-RAxME
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 400e57fe-a210-407e-9ca1-9e14fbbe1685
updated: 1486069621
title: 'How to Trick Your Math Teacher 5 TUTORIAL - Math Tricks Revealed'
categories:
    - Math Magic Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/BsVX0-RAxME/default.jpg
    - https://i3.ytimg.com/vi/BsVX0-RAxME/1.jpg
    - https://i3.ytimg.com/vi/BsVX0-RAxME/2.jpg
    - https://i3.ytimg.com/vi/BsVX0-RAxME/3.jpg
---
