---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sulfidation
offline_file: ""
offline_thumbnail: ""
uuid: 74f180f1-f075-45a8-8a7b-e0676c9e0f1a
updated: 1484308859
title: Sulfidation
categories:
    - Thermodynamics
---
Sulfidation is a process of installing sulfide ions in a material or molecule. The process is widely used to convert oxides to sulfides but is also related to corrosion and surface modification.
