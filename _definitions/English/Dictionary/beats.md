---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/beats
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484400541
title: beats
categories:
    - Dictionary
---
beats
     n : a United States youth subculture of the 1950s; rejected
         possessions or regular work or traditional dress; for
         communal living and psychedelic drugs and anarchism;
         favored modern forms of jazz (e.g., bebop) [syn: {beat
         generation}, {beatniks}]
