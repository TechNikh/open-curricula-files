---
version: 1
type: video
provider: YouTube
id: malkLGsE1Os
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5d901f0b-7931-48f8-8d05-33cbb5d56eb5
updated: 1486069601
title: >
    సాధించగల వాయు సమీకరణ
    PV=nRT
tags:
    - telugu
    - chemistry
    - Gases
    - ideal gas equation
    - Gas (Phase Of Matter)
categories:
    - రసాయన శాస్త్రం
thumbnail_urls:
    - https://i3.ytimg.com/vi/malkLGsE1Os/default.jpg
    - https://i3.ytimg.com/vi/malkLGsE1Os/1.jpg
    - https://i3.ytimg.com/vi/malkLGsE1Os/2.jpg
    - https://i3.ytimg.com/vi/malkLGsE1Os/3.jpg
---
Translated from original video
Title - Ideal Gas Equation: PV=nRT 
URL -http://www.khanacademy.org/science/chemistry/ideal-gas-laws/v/ideal-gas-equation--pv-nrt
