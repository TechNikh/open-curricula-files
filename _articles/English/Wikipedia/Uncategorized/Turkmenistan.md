---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Turkomania
offline_file: ""
offline_thumbnail: ""
uuid: 1c5a97a7-55e4-47ff-bc7e-91d602ff2a1b
updated: 1484094290
title: Turkmenistan
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Russians_attack_a_turkmen_caravan_1873.jpg
tags:
    - History
    - Politics
    - Foreign relations
    - Human rights
    - Restrictions on free and open communication
    - Administrative divisions
    - Climate
    - geography
    - Economy
    - Natural gas and export routes
categories:
    - Uncategorized
---
Turkmenistan (i/tɜːrkˈmɛnᵻstæn/ or i/tɜːrkmɛnᵻˈstɑːn/; Turkmen: Türkmenistan/Түркменистан/تۆركمهنيستآن, pronounced [tyɾkmeniˈθːaːn]); formerly known as Turkmenia[6] is a country in Central Asia, bordered by Kazakhstan to the northwest, Uzbekistan to the north and east, Afghanistan to the southeast, Iran to the south and southwest, and the Caspian Sea to the west.
