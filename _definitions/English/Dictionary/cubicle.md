---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cubicle
offline_file: ""
offline_thumbnail: ""
uuid: 654f4b0d-8df4-446e-99f0-b17e80dc7392
updated: 1484310156
title: cubicle
categories:
    - Dictionary
---
cubicle
     n 1: small room is which a monk or nun lives [syn: {cell}]
     2: small individual study area in a library [syn: {carrel}, {carrell},
         {stall}]
     3: small area set off by walls for special use [syn: {booth}, {stall},
         {kiosk}]
