---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trinity
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484561041
title: trinity
categories:
    - Dictionary
---
trinity
     n 1: the cardinal number that is the sum of one and one and one
          [syn: {three}, {3}, {III}, {trio}, {threesome}, {tierce},
           {leash}, {troika}, {triad}, {trine}, {ternary}, {ternion},
           {triplet}, {tercet}, {terzetto}, {trey}, {deuce-ace}]
     2: the union of the Father and Son and Holy Ghost in one
        Godhead [syn: {Holy Trinity}, {Blessed Trinity}, {Sacred
        Trinity}]
     3: three people considered as a unit [syn: {trio}, {threesome},
         {triad}]
