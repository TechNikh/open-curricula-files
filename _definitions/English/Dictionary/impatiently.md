---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impatiently
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484481541
title: impatiently
categories:
    - Dictionary
---
impatiently
     adv : in an impatient manner; "he answered her impatiently" [syn:
           {with impatience}] [ant: {patiently}]
