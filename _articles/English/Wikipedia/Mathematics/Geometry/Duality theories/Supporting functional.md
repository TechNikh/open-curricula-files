---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Supporting_functional
offline_file: ""
offline_thumbnail: ""
uuid: 19ea739a-c55b-4b66-afca-05a661a74dba
updated: 1484309239
title: Supporting functional
tags:
    - Mathematical definition
    - Relation to support function
    - Relation to supporting hyperplane
categories:
    - Duality theories
---
