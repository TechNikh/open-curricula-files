---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grace
offline_file: ""
offline_thumbnail: ""
uuid: 3dc03b82-76af-475a-99e3-2f877b9c4a37
updated: 1484310484
title: grace
categories:
    - Dictionary
---
grace
     n 1: (Bhristian theology) a state of sanctification by God; the
          state of one who under such divine influence; "the
          conception of grace developed alongside the conception
          of sin"; "it was debated whether saving grace could be
          obtained outside the membership of the church"; "the
          Virgin lived in a state of grace" [syn: {saving grace},
          {state of grace}]
     2: elegance and beauty of movement or expression [syn: {gracility}]
     3: a sense of propriety and consideration for others [syn: {seemliness}]
        [ant: {unseemliness}]
     4: a disposition to kindness and compassion; benign good will;
        "the victor's grace ...
