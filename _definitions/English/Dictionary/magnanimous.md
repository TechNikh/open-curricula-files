---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magnanimous
offline_file: ""
offline_thumbnail: ""
uuid: 058f1e2e-0db1-454f-8c36-7b099dec9804
updated: 1484310189
title: magnanimous
categories:
    - Dictionary
---
magnanimous
     adj 1: noble and generous in spirit; "a greathearted general"; "a
            magnanimous conqueror" [syn: {greathearted}]
     2: generous and understanding and tolerant; "a heart big enough
        to hold no grudges"; "that's very big of you to be so
        forgiving"; "a large and generous spirit"; "a large
        heart"; "magnanimous toward his enemies" [syn: {big}, {large}]
