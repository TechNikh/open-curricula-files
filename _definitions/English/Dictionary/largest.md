---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/largest
offline_file: ""
offline_thumbnail: ""
uuid: 08120440-31e2-474f-abc7-16b3ac18ebfe
updated: 1484310246
title: largest
categories:
    - Dictionary
---
largest
     adj 1: greatest in size of those under consideration [syn: {biggest},
             {greatest}]
     2: maximal in amount; "a maximal amount"; "an outside estimate"
        [syn: {outside}]
