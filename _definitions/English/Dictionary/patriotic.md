---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/patriotic
offline_file: ""
offline_thumbnail: ""
uuid: 53494d03-5d2b-42d5-865e-8a09c1da54ce
updated: 1484310573
title: patriotic
categories:
    - Dictionary
---
patriotic
     adj : inspired by love for your country [syn: {loyal}] [ant: {unpatriotic}]
