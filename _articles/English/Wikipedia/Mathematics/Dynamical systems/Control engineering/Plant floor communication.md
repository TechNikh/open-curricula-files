---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Plant_floor_communication
offline_file: ""
offline_thumbnail: ""
uuid: d8a0c08d-679c-4f86-ad92-5adf8702c714
updated: 1484308743
title: Plant floor communication
tags:
    - Machine to machine (M2M) communications
    - Machine to enterprise (M2E) communications
    - Integration
categories:
    - Control engineering
---
Plant floor communications refers to the control and data communications typically found in automation environments, on a manufacturing plant floor or process plant. The difference between manufacturing and process is typically the types of control involved, discrete control or continuous control (aka process control). Many plants offer a hybrid of both discrete and continuous control. The underlying commonality between them all is that the automation systems are often an integration of multi-vendor products to form one system. Each vendor product typically offers communication capability for programming, maintaining and collecting data from their products. A properly orchestrated plant ...
