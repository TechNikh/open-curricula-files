---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nurse
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484408701
title: nurse
categories:
    - Dictionary
---
nurse
     n 1: one skilled in caring for young children or the sick
          (usually under the supervision of a physician)
     2: a woman who is the custodian of children [syn: {nanny}, {nursemaid}]
     v 1: try to cure by special care of treatment, of an illness or
          injury; "He nursed his cold with Chinese herbs"
     2: maintain (a theory, thoughts, or feelings); "bear a grudge";
        "entertain interesting notions"; "harbor a resentment"
        [syn: {harbor}, {harbour}, {hold}, {entertain}]
     3: serve as a nurse; care for sick or handicapped people
     4: treat carefully; "He nursed his injured back by liyng in bed
        several hours every afternoon"; "He nursed ...
