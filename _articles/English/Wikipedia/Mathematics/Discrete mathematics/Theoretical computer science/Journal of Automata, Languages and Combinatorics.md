---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Journal_of_Automata,_Languages_and_Combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: a2848f9f-81f0-43be-b836-2be9eec01c29
updated: 1484308675
title: Journal of Automata, Languages and Combinatorics
categories:
    - Theoretical computer science
---
The Journal of Automata, Languages and Combinatorics is a peer-reviewed scientific journal of computer science, edited by Jürgen Dassow. It was established in 1965 as the Journal of Information Processing and Cybernetics/Elektronische Informationsverarbeitung und Kybernetik and obtained its current title in 1996 with volume numbering reset to 1. The main focus of the journal is on automata theory, formal language theory, and combinatorics.
