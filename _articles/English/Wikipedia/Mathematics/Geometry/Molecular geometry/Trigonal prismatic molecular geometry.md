---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Trigonal_prismatic_molecular_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 7508a1ca-07d6-4b6b-a44e-7ab7516fb8ec
updated: 1484309283
title: Trigonal prismatic molecular geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Prismatic_TrigonalP.png
categories:
    - Molecular geometry
---
In chemistry, the trigonal prismatic molecular geometry describes the shape of compounds where six atoms, groups of atoms, or ligands are arranged around a central atom, defining the vertices of a triangular prism.
