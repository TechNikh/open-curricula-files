---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Named_graph
offline_file: ""
offline_thumbnail: ""
uuid: 573d7af3-0489-486e-b65e-84d8e23db7ef
updated: 1484309417
title: Named graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Named-graphs-1.jpg
tags:
    - Named graphs and HTTP
    - Named graphs and RDF stores
    - Example
    - Named graphs and quads
    - Formal definition
    - Specifications
    - Proposed specifications
categories:
    - Graphs
---
Named graphs are a key concept of Semantic Web architecture in which a set of Resource Description Framework statements (a graph) are identified using a URI,[1] allowing descriptions to be made of that set of statements such as context, provenance information or other such metadata.
