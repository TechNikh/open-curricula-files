---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/exposure
offline_file: ""
offline_thumbnail: ""
uuid: 170b01f7-7d34-48ac-a9a3-9f21ba6acddb
updated: 1484310486
title: exposure
categories:
    - Dictionary
---
exposure
     n 1: vulnerability to the elements; to the action of heat or cold
          or wind or rain; "exposure to the weather" or "they died
          from exposure";
     2: the act of subjecting someone to an influencing experience;
        "she denounced the exposure of children to pornography"
     3: the disclosure of something secret; "they feared exposure of
        their campaign plans"
     4: aspect re light or wind; "the studio had a northern
        exposure"
     5: the state of being vulnerable or exposed; "his vulnerability
        to litigation"; "his exposure to ridicule" [syn: {vulnerability}]
     6: the intensity of light falling on a photographic film or
        ...
