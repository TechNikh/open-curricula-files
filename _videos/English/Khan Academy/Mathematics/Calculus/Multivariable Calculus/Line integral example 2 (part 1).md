---
version: 1
type: video
provider: YouTube
id: wyTjyQMVvc4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Line%20integral%20example%202%20%28part%201%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 14a08aa9-156f-469d-bc0e-5665630bb99a
updated: 1486069689
title: Line integral example 2 (part 1)
categories:
    - Multivariable Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/wyTjyQMVvc4/default.jpg
    - https://i3.ytimg.com/vi/wyTjyQMVvc4/1.jpg
    - https://i3.ytimg.com/vi/wyTjyQMVvc4/2.jpg
    - https://i3.ytimg.com/vi/wyTjyQMVvc4/3.jpg
---
