---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_of_diatomic_molecules
offline_file: ""
offline_thumbnail: ""
uuid: 5cd7fc54-66a1-473f-a55d-4fecbc190ce4
updated: 1484309319
title: Symmetry of diatomic molecules
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/248px-Axial.png
tags:
    - Symmetry and group theory
    - Groups
    - Groups, symmetry and conservation
    - Geometrical symmetries
    - Symmetry operations, symmetry elements and point group
    - Basic symmetry operations
    - Schoenflies notation
    - Symmetry groups in diatomic molecules
    - Summary examples
    - Complete set of commuting operators
    - Molecular term symbol, Λ-doubling
    - Angular momentum
    - Axial symmetry
    - Inversion symmetry
    - Spin and total angular momentum
    - Molecular term symbol
    - von Neumann-Wigner non-crossing rule
    - Effect of symmetry on the matrix elements of the Hamiltonian
    - Observable consequences
    - Rotational spectrum
    - Vibrational spectrum
    - Rovibrational spectrum
    - 'A special example: Hydrogen molecule ion'
    - Notes
categories:
    - Symmetry
---
Molecular symmetry in physics and chemistry describes the symmetry present in molecules and the classification of molecules according to their symmetry. Molecular symmetry is a fundamental concept in the application of Quantum Mechanics in physics and chemistry, for example it can be used to predict or explain many of a molecule's properties, such as its dipole moment and its allowed spectroscopic transitions (based on selection rules), without doing the exact rigorous calculations (which, in some cases, may not even be possible). Group theory is the predominant framework for analyzing molecular symmetry.
