---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/merry
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484450041
title: merry
categories:
    - Dictionary
---
merry
     adj 1: full of or showing high-spirited merriment; "when hearts
            were young and gay"; "a poet could not but be gay, in
            such a jocund company"- Wordsworth; "the jolly crowd
            at the reunion"; "jolly old Saint Nick"; "a jovial old
            gentleman"; "have a merry Christmas"; "peals of merry
            laughter"; "a mirthful laugh" [syn: {gay}, {jocund}, {jolly},
             {jovial}, {mirthful}]
     2: offering fun and gaiety; "a gala ball after the
        inauguration"; "a festive (or festal) occasion"; "gay and
        exciting night life"; "a merry evening" [syn: {gala(a)}, {gay},
         {festal}, {festive}]
     3: quick and ...
