---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/meditation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484640361
title: meditation
categories:
    - Dictionary
---
meditation
     n 1: continuous and profound contemplation or musing on a subject
          or series of subjects of a deep or abstruse nature; "the
          habit of meditation is the basis for all real knowledge"
          [syn: {speculation}]
     2: (religion) contemplation of spiritual matters (usually on
        religious or philosophical subjects)
