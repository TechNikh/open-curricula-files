---
version: 1
type: video
provider: YouTube
id: tS2YJPmKOFQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/pH%2C%20pOH%20of%20strong%20acids%20and%20bases.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6de666ca-581a-44e7-9741-604c455213b3
updated: 1486069583
title: pH, pOH of strong acids and bases
categories:
    - Chemistry
thumbnail_urls:
    - https://i3.ytimg.com/vi/tS2YJPmKOFQ/default.jpg
    - https://i3.ytimg.com/vi/tS2YJPmKOFQ/1.jpg
    - https://i3.ytimg.com/vi/tS2YJPmKOFQ/2.jpg
    - https://i3.ytimg.com/vi/tS2YJPmKOFQ/3.jpg
---
Deriving the relationship between pKw, pH, and pOH. Calculating the pH or pOH of strong acids and bases. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/chemistry/acids-and-bases-topic/acids-and-bases/v/strong-acids-and-strong-bases?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Missed the previous lesson? https://www.khanacademy.org/science/chemistry/acids-and-bases-topic/acids-and-bases/v/introduction-to-definition-of-ph?utm_source=YT&utm_medium=Desc&utm_campaign=chemistry

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
