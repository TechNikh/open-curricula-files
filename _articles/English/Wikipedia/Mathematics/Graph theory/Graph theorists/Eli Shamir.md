---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Eli_Shamir
offline_file: ""
offline_thumbnail: ""
uuid: 5b1c7626-40bb-4987-9654-89a0dcafb9c5
updated: 1484309475
title: Eli Shamir
tags:
    - Biography
    - Contributions
    - Awards and honors
    - Selected publications
categories:
    - Graph theorists
---
Eliahu (Eli) Shamir (Hebrew: אליהו שמיר‎‎) is an Israeli mathematician and computer scientist, the Jean and Helene Alfassa Professor Emeritus of Computer Science at the Hebrew University of Jerusalem.[1]
