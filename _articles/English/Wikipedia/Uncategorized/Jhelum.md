---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jhelum
offline_file: ""
offline_thumbnail: ""
uuid: 71951e55-5387-412c-a344-ba33c49edfc3
updated: 1484308419
title: Jhelum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-A_Fort_in_Jhelum_City.jpg
tags:
    - Etymology
    - History
    - Ancient
    - Medieval
    - Later periods
    - British Raj
    - Independence
    - Administration
    - Demography
    - City
    - Geography and Climate
    - Major floods
    - Important sites
    - Travel and tourism
    - Local
    - National
    - Railway
    - air
    - Telecommunication
    - Sports
    - Education
    - Higher and technical education
    - Universities
    - Hospitals
    - Major industries
    - Gallery
categories:
    - Uncategorized
---
Jhelum /ˈdʒeɪləm/ (Urdu, Punjabi: جہلم) (Greek: Αλεξάνδρια Βυκεφάλους Alexandria Bucephalous) is a city on the right bank of the Jhelum River, in the district of the same name in the north of Punjab province, Pakistan. Jhelum is known for providing a large number of soldiers to the British Army[4] before independence and later to the Pakistan armed forces due to which it is also known as city of soldiers or land of martyrs and warriors.[5][6] Jhelum is a few miles upstream from the site of the Battle of the Hydaspes between the armies of Alexander of Macedonia and Raja Porus. A city called Bucephala was founded nearby to commemorate the death of Alexander's ...
