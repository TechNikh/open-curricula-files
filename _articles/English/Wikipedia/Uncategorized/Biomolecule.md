---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Biomolecules
offline_file: ""
offline_thumbnail: ""
uuid: 9ab9f72b-4b1e-40d1-9379-ebea5930f7c4
updated: 1484308392
title: Biomolecule
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Myoglobin.png
tags:
    - Types of biomolecules
    - Nucleosides and nucleotides
    - DNA and RNA structure
    - Saccharides
    - Lignin
    - Lipids
    - Amino acids
    - Protein structure
    - Apoenzymes
    - Isoenzymes
categories:
    - Uncategorized
---
A biomolecule or biological molecule is molecule that is present in living organisms, including large macromolecules such as proteins, carbohydrates, lipids, and nucleic acids, as well as small molecules such as primary metabolites, secondary metabolites, and natural products. A more general name for this class of material is biological materials. Biomolecules are usually endogenous but may also be exogenous. For example, pharmaceutical drugs may be natural products or semisynthetic (biopharmaceuticals) or they may be totally synthetic.
