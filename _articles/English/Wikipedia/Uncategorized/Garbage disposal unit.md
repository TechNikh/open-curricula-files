---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Garbage_disposal
offline_file: ""
offline_thumbnail: ""
uuid: 67164924-271a-41e7-a72e-9f82e660ef09
updated: 1484308368
title: Garbage disposal unit
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Waste_disposer.JPG
tags:
    - History
    - Adoption
    - Rationale
    - Operation
    - Environmental impact
categories:
    - Uncategorized
---
A garbage disposal unit (also known as a garbage disposal, waste disposal unit, garbage disposer, or in Canadian English a garburator) is a device, usually electrically powered, installed under a kitchen sink between the sink's drain and the trap. The disposal unit shreds food waste into pieces small enough—generally less than 2 mm (0.079 in)—to pass through plumbing.[1]
