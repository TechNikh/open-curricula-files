---
version: 1
type: video
provider: YouTube
id: tIfsHPpkSPs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/Simple%20Animals-%20Sponges%2C%20Jellies%2C%20%26%20Octopuses%20-%20Crash%20Course%20Biology%20%2322.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4761ec4d-fd76-4a3e-8317-e88452762951
updated: 1486069678
title: 'Simple Animals- Sponges, Jellies, & Octopuses - Crash Course Biology #22'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/tIfsHPpkSPs/default.jpg
    - https://i3.ytimg.com/vi/tIfsHPpkSPs/1.jpg
    - https://i3.ytimg.com/vi/tIfsHPpkSPs/2.jpg
    - https://i3.ytimg.com/vi/tIfsHPpkSPs/3.jpg
---
