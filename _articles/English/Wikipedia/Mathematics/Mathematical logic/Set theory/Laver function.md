---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laver_function
offline_file: ""
offline_thumbnail: ""
uuid: 00290168-d3bb-4188-aa96-f9660638bf7f
updated: 1484309507
title: Laver function
categories:
    - Set theory
---
If κ is a supercompact cardinal, a Laver function is a function ƒ:κ → Vκ such that for every set x and every cardinal λ ≥ |TC(x)| + κ there is a supercompact measure U on [λ]<κ such that if j U is the associated elementary embedding then j U(ƒ)(κ) = x. (Here Vκ denotes the κ-th level of the cumulative hierarchy, TC(x) is the transitive closure of x)
