---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/simon
offline_file: ""
offline_thumbnail: ""
uuid: 78d7df83-d7c7-4152-a8a4-084448cc3509
updated: 1484310202
title: simon
categories:
    - Dictionary
---
Simon
     n 1: one of the twelve Apostles (first century) [syn: {St. Simon},
           {Simon Zelotes}, {Simon the Zealot}, {Simon the
          Canaanite}]
     2: United States singer and songwriter (born in 1942) [syn: {Paul
        Simon}]
     3: United States playwright noted for light comedies (born in
        1927) [syn: {Neil Simon}, {Marvin Neil Simon}]
     4: United States economist and psychologist who pioneered in
        the development of cognitive science (1916-2001) [syn: {Herb
        Simon}, {Herbert A. Simon}, {Herbert Alexander Simon}]
