---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Subtract_a_square
offline_file: ""
offline_thumbnail: ""
uuid: 04d11bf1-a268-4d0e-8f22-85c732076629
updated: 1484308533
title: Subtract a square
tags:
    - Illustration
    - Mathematical theory
    - Extensions
    - Misère game
categories:
    - Combinatorial game theory
---
Subtract-a-square (also referred to as take-a-square) is a two-player mathematical game of strategy starting with a positive integer and both players taking turns subtracting a non-zero square number not larger than the current value. The game is usually played as a normal play game, which means that the last person who can make a subtraction wins.
