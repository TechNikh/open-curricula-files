---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sat
offline_file: ""
offline_thumbnail: ""
uuid: 9e3e6fec-ae5c-4503-9916-a8827bdf404a
updated: 1484310531
title: sat
categories:
    - Dictionary
---
Sat
     n : the seventh and last day of the week; observed as the
         Sabbath by Jews and some Christians [syn: {Saturday}, {Sabbatum}]
