---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ellipsoid
offline_file: ""
offline_thumbnail: ""
uuid: 9a0bcc13-1328-4086-8c94-3ffd3bc439b1
updated: 1484310414
title: ellipsoid
categories:
    - Dictionary
---
ellipsoid
     adj : in the form of an ellipse [syn: {ellipsoidal}, {spheroidal},
            {non-circular}]
     n : a surface whose plane sections are all ellipses or circles;
         "the Earth is an ellipsoid"
