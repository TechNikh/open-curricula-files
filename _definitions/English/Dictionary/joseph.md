---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/joseph
offline_file: ""
offline_thumbnail: ""
uuid: ff964c36-5af4-4b17-8e63-465bf35cf721
updated: 1484310393
title: joseph
categories:
    - Dictionary
---
Joseph
     n 1: leader of the Nez Perce in their retreat from United States
          troops (1840-1904) [syn: {Chief Joseph}]
     2: (Old Testament) the 11th son of Jacob and one of the 12
        patriarchs of Israel; Jacob gave Joseph a coat of many
        colors, which made his brothers jealous and they sold him
        into slavery in Egypt
     3: (New Testament) husband of Mary and (in Christian belief)
        the foster father of Jesus
