---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Topological_combinatorics
offline_file: ""
offline_thumbnail: ""
uuid: 6b3138eb-af45-4f9b-999b-94bfe4b4ce35
updated: 1484308598
title: Topological combinatorics
categories:
    - Combinatorics
---
The discipline of combinatorial topology used combinatorial concepts in topology and in the early 20th century this turned into the field of algebraic topology.
