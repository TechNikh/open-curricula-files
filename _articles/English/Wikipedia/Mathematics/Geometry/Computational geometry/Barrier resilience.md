---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Barrier_resilience
offline_file: ""
offline_thumbnail: ""
uuid: 3d19bf40-700c-4fce-91c7-8401c88db9db
updated: 1484309170
title: Barrier resilience
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Barrier_resilience.svg.png
tags:
    - Definitions
    - Computational complexity
    - Notes
categories:
    - Computational geometry
---
Barrier resilience is an algorithmic optimization problem in computational geometry motivated by the design of wireless sensor networks, in which one seeks a path through a collection of barriers (often modeled as unit disks) that passes through as few barriers as possible.
