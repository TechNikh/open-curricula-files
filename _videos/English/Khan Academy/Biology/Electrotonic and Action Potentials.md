---
version: 1
type: video
provider: YouTube
id: gkQtRec2464
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Electrotonic%20and%20Action%20Potentials.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6d7c0f0b-8d37-44fc-9924-71b50097784a
updated: 1486069583
title: Electrotonic and Action Potentials
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/gkQtRec2464/default.jpg
    - https://i3.ytimg.com/vi/gkQtRec2464/1.jpg
    - https://i3.ytimg.com/vi/gkQtRec2464/2.jpg
    - https://i3.ytimg.com/vi/gkQtRec2464/3.jpg
---
How electrotonic and action potentials propagate down cells
