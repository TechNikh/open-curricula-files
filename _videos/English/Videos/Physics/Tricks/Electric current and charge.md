---
version: 1
type: video
provider: YouTube
id: AIkT78RRM5Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: a19993d1-b577-49d5-96de-9f07e33495e9
updated: 1486069658
title: Electric current and charge
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/AIkT78RRM5Q/default.jpg
    - https://i3.ytimg.com/vi/AIkT78RRM5Q/1.jpg
    - https://i3.ytimg.com/vi/AIkT78RRM5Q/2.jpg
    - https://i3.ytimg.com/vi/AIkT78RRM5Q/3.jpg
---
