---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Social_network
offline_file: ""
offline_thumbnail: ""
uuid: 047a07af-2cee-4aa0-9353-ba2b946efdd7
updated: 1484308828
title: Social network
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Moreno_Sociogram_2nd_Grade.png
tags:
    - Overview
    - History
    - Levels of analysis
    - Micro level
    - Meso level
    - Macro level
    - Theoretical links
    - Imported theories
    - Indigenous theories
    - Structural holes
    - Research clusters
    - Communications
    - Community
    - Complex networks
    - Criminal networks
    - Diffusion of innovations
    - Demography
    - Economic sociology
    - Health care
    - Human ecology
    - Language and linguistics
    - Literary networks
    - Organizational studies
    - Social capital
    - Mobility benefits
    - Social media
    - Organizations
    - Peer-reviewed journals
    - Textbooks and educational resources
    - Data sets
categories:
    - Self-organization
---
A social network is a social structure made up of a set of social actors (such as individuals or organizations), sets of dyadic ties, and other social interactions between actors. The social network perspective provides a set of methods for analyzing the structure of whole social entities as well as a variety of theories explaining the patterns observed in these structures.[1] The study of these structures uses social network analysis to identify local and global patterns, locate influential entities, and examine network dynamics.
