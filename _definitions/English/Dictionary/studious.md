---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/studious
offline_file: ""
offline_thumbnail: ""
uuid: 78ab8ae6-6360-4e45-9377-68e646d46958
updated: 1484310445
title: studious
categories:
    - Dictionary
---
studious
     adj 1: marked by care and effort; "made a studious attempt to fix
            the television set"
     2: characterized by diligent study and fondness for reading; "a
        bookish farmer who always had a book in his pocket"; "a
        quiet studious child" [syn: {bookish}]
