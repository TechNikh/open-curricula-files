---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Paley_graph
offline_file: ""
offline_thumbnail: ""
uuid: e0c1d19b-cd09-4852-a620-353fc2e642b6
updated: 1484308657
title: Paley graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Paley13.svg.png
tags:
    - Definition
    - Example
    - Properties
    - Applications
    - Paley digraphs
    - Genus
categories:
    - Number theory
---
In mathematics, Paley graphs are dense undirected graphs constructed from the members of a suitable finite field by connecting pairs of elements that differ by a quadratic residue. The Paley graphs form an infinite family of conference graphs, which yield an infinite family of symmetric conference matrices. Paley graphs allow graph-theoretic tools to be applied to the number theory of quadratic residues, and have interesting properties that make them useful in graph theory more generally.
