---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lie_derivative
offline_file: ""
offline_thumbnail: ""
uuid: 1b9dcea8-1887-43c8-b4fa-402d4d706431
updated: 1484309226
title: Lie derivative
tags:
    - Motivation
    - Definition
    - The (Lie) derivative of a function
    - The Lie derivative of a vector field
    - The Lie derivative of a tensor field
    - The Lie derivative of a differential form
    - Coordinate expressions
    - Examples
    - Properties
    - Generalizations
    - The Lie derivative of a spinor field
    - Covariant Lie derivative
    - Nijenhuis–Lie derivative
    - History
    - Notes
categories:
    - Differential geometry
---
In differential geometry, the Lie derivative /ˈliː/, named after Sophus Lie by Władysław Ślebodziński,[1][2] evaluates the change of a tensor field (including scalar function, vector field and one-form), along the flow of another vector field. This change is coordinate invariant and therefore the Lie derivative is defined on any differentiable manifold.
