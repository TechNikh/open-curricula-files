---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Atiyah%E2%80%93Hitchin%E2%80%93Singer_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 3ec71b2f-c086-4bf5-abf2-f3fe7b9d5534
updated: 1484309204
title: Atiyah–Hitchin–Singer theorem
categories:
    - Differential geometry
---
In differential geometry, the Atiyah–Hitchin–Singer theorem, introduced by Atiyah, Hitchin, and Singer (1977, 1978), states that the space of SU(2) anti self dual Yang–Mills fields on a 4-sphere with index k > 0 has dimension 8k – 3.
