---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/viii
offline_file: ""
offline_thumbnail: ""
uuid: 686dbfa4-9cda-44c6-a94e-d83a8cbaba11
updated: 1484310316
title: viii
categories:
    - Dictionary
---
viii
     adj : being one more than seven [syn: {eight}, {8}]
     n : the cardinal number that is the sum of seven and one [syn: {eight},
          {8}, {eighter}, {eighter from Decatur}, {octad}, {ogdoad},
          {octonary}, {octet}]
