---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ruth_Aaronson_Bari
offline_file: ""
offline_thumbnail: ""
uuid: 145191ed-826a-4838-b864-a29d024d1bdc
updated: 1484309459
title: Ruth Aaronson Bari
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Ruthbari.jpg
tags:
    - Career
    - Community and Family Life
categories:
    - Graph theorists
---
Ruth Aaronson Bari (November 17, 1917 – August 25, 2005) was an American mathematician known for her work in graph theory and algebraic homomorphisms. The daughter of Polish-Jewish immigrants to the United States, she was a professor at George Washington University beginning in 1966. She was the mother of environmental activist Judi Bari, science reporter Gina Kolata and art historian Martha Bari.
