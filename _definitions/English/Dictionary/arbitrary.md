---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arbitrary
offline_file: ""
offline_thumbnail: ""
uuid: 78d13b2d-a8d7-411b-91a4-4083cfd94cb6
updated: 1484310218
title: arbitrary
categories:
    - Dictionary
---
arbitrary
     adj : based on or subject to individual discretion or preference
           or sometimes impulse or caprice; "an arbitrary
           decision"; "the arbitrary rule of a dictator"; "an
           arbitrary penalty"; "of arbitrary size and shape"; "an
           arbitrary choice"; "arbitrary division of the group
           into halves" [ant: {nonarbitrary}]
