---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Herbrand%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 411ff86f-8ab7-4551-b07a-d53d2469d8e0
updated: 1484308994
title: "Herbrand's theorem"
tags:
    - Proof Sketch
    - "Generalizations of Herbrand's Theorem"
    - Notes
categories:
    - Theorems in the foundations of mathematics
---
Herbrand's theorem is a fundamental result of mathematical logic obtained by Jacques Herbrand (1930).[1] It essentially allows a certain kind of reduction of first-order logic to propositional logic. Although Herbrand originally proved his theorem for arbitrary formulas of first-order logic,[2] the simpler version shown here, restricted to formulas in prenex form containing only existential quantifiers, became more popular.
