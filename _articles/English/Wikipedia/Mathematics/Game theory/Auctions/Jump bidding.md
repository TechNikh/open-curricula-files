---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Jump_bidding
offline_file: ""
offline_thumbnail: ""
uuid: 1db29652-d9c9-49c9-9419-a775453aa715
updated: 1484309011
title: Jump bidding
tags:
    - Puzzle
    - Reducing bidding costs
    - Signaling
    - Numeric example
    - Manipulating the outcome using the discreteness of prices
    - Effects
categories:
    - Auctions
---
In auction theory, jump bidding is the practice of increasing the current price in an English auction, substantially more than the minimal allowed amount.
