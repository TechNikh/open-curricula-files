---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Expectiminimax_tree
offline_file: ""
offline_thumbnail: ""
uuid: b65baced-fefb-4485-9f1f-6f40d7c1e5ff
updated: 1484309002
title: Expectiminimax tree
categories:
    - Game artificial intelligence
---
An expectiminimax tree is a specialized variation of a minimax game tree for use in artificial intelligence systems that play two-player zero-sum games such as backgammon, in which the outcome depends on a combination of the player's skill and chance elements such as dice rolls. In addition to "min" and "max" nodes of the traditional minimax tree, this variant has "chance" ("move by nature") nodes, which take the expected value of a random event occurring.[1] In game theory terms, an expectiminimax tree is the game tree of an extensive-form game of perfect, but incomplete information.
