---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/categorised
offline_file: ""
offline_thumbnail: ""
uuid: a8409a4f-17d2-43e6-8175-796e7b4dd669
updated: 1484310451
title: categorised
categories:
    - Dictionary
---
categorised
     adj : arranged into categories [syn: {categorized}]
