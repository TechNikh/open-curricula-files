---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Surface_area
offline_file: ""
offline_thumbnail: ""
uuid: fde9a1b9-8036-4907-a2fb-fd12b81c9dde
updated: 1484308375
title: Surface area
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sphere_wireframe_10deg_6r.svg.png
tags:
    - Definition
    - Depth problems
    - Common formulas
    - >
        Ratio of surface areas of a sphere and cylinder of the same
        radius and height
    - In chemistry
    - In biology
categories:
    - Uncategorized
---
The surface area of a solid object is a measure of the total area that the surface of the object occupies. The mathematical definition of surface area in the presence of curved surfaces is considerably more involved than the definition of arc length of one-dimensional curves, or of the surface area for polyhedra (i.e., objects with flat polygonal faces), for which the surface area is the sum of the areas of its faces. Smooth surfaces, such as a sphere, are assigned surface area using their representation as parametric surfaces. This definition of surface area is based on methods of infinitesimal calculus and involves partial derivatives and double integration.
