---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Catenation
offline_file: ""
offline_thumbnail: ""
uuid: 0af2c223-18ac-4656-8ff8-3be31e13170b
updated: 1484308394
title: Catenation
categories:
    - Uncategorized
---
Catenation is the linkage of atoms of the same element into longer chains. Catenation occurs most readily in carbon, which forms covalent bonds with other carbon atoms to form longer chains and structures. This is the reason for the presence of the vast number of organic compounds in nature. Carbon is most well known for its properties of catenation, with organic chemistry essentially being the study of catenated carbon structures (and known as catenae). However, carbon is by no means the only element capable of forming such catenae, and several other main group elements are capable of forming an expansive range of catenae, including silicon, sulfur and boron.
