---
version: 1
type: video
provider: YouTube
id: w8YKjbkoicc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ae49fa58-71d2-499f-bc6f-e8d73f46ca12
updated: 1486069611
title: Rotating segment about origin example
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/w8YKjbkoicc/default.jpg
    - https://i3.ytimg.com/vi/w8YKjbkoicc/1.jpg
    - https://i3.ytimg.com/vi/w8YKjbkoicc/2.jpg
    - https://i3.ytimg.com/vi/w8YKjbkoicc/3.jpg
---
