---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Regular_map_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 55ab46e6-1fb1-4b6e-b9cb-04b75de368fd
updated: 1484308592
title: Regular map (graph theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hexagonal_Hosohedron.svg.png
tags:
    - Overview
    - Topological approach
    - Group-theoretical approach
    - Graph-theoretical approach
    - Examples
categories:
    - Discrete geometry
---
In mathematics, a regular map is a symmetric tessellation of a closed surface. More precisely, a regular map is a decomposition of a two-dimensional manifold such as a sphere, torus, or real projective plane into topological disks, such that every flag (an incident vertex-edge-face triple) can be transformed into any other flag by a symmetry of the decomposition. Regular maps are, in a sense, topological generalizations of Platonic solids. The theory of maps and their classification is related to the theory of Riemann surfaces, hyperbolic geometry, and Galois theory. Regular maps are classified according to either: the genus and orientability of the supporting surface, the underlying graph, ...
