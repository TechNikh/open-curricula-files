---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/authorities
offline_file: ""
offline_thumbnail: ""
uuid: 9add9c33-0464-45c9-aaa3-0e7ffbb8ded4
updated: 1484310477
title: authorities
categories:
    - Dictionary
---
authorities
     n : the organization that is the governing authority of a
         political unit; "the government reduced taxes"; "the
         matter was referred to higher authorities" [syn: {government},
          {regime}]
