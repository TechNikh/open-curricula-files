---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Oseledets_theorem
offline_file: ""
offline_thumbnail: ""
uuid: efee65e9-ae28-49f9-97ee-d641b742bd30
updated: 1484308692
title: Oseledets theorem
tags:
    - Cocycles
    - Examples
    - Statement of the theorem
    - Additive versus multiplicative ergodic theorems
categories:
    - Theorems in dynamical systems
---
In mathematics, the multiplicative ergodic theorem, or Oseledets theorem provides the theoretical background for computation of Lyapunov exponents of a nonlinear dynamical system. It was proved by Valery Oseledets (also spelled "Oseledec") in 1965 and reported at the International Mathematical Congress in Moscow in 1966. A conceptually different proof of the multiplicative ergodic theorem was found by M. S. Raghunathan. The theorem has been extended to semisimple Lie groups by V. A. Kaimanovich and further generalized in the works of David Ruelle, Gregory Margulis, Anders Karlsson, and F. Ledrappier.
