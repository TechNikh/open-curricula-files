---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interpret
offline_file: ""
offline_thumbnail: ""
uuid: a77333c4-cc4c-4163-ba41-83dc875a603b
updated: 1484310545
title: interpret
categories:
    - Dictionary
---
interpret
     v 1: make sense of; assign a meaning to; "What message do you see
          in this letter?"; "How do you interpret his behavior?"
          [syn: {construe}, {see}]
     2: give an interpretation or explanation to
     3: give an interpretation or rendition of; "The pianist
        rendered the Beethoven sonata beautifully" [syn: {render}]
     4: create an image or likeness of; "The painter represented his
        wife as a young girl" [syn: {represent}]
     5: restate (words) from one language into another language; "I
        have to translate when my in-laws from Austria visit the
        U.S."; "Can you interpret the speech of the visiting
        dignitaries?"; "She ...
