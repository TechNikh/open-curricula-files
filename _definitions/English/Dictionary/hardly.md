---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hardly
offline_file: ""
offline_thumbnail: ""
uuid: 11c532c1-9c78-4a36-a776-ff33044669e8
updated: 1484310533
title: hardly
categories:
    - Dictionary
---
hardly
     adv 1: by a small margin; "they could barely hear the speaker"; "we
            hardly knew them"; "just missed being hit"; "had
            scarcely rung the bell when the door flew open";
            "would have scarce arrived before she would have found
            some excuse to leave"- W.B.Yeats [syn: {barely}, {just},
             {scarcely}, {scarce}]
     2: almost not; "he hardly ever goes fishing"; "he was hardly
        more than sixteen years old"; "they scarcely ever used the
        emergency generator" [syn: {scarcely}]
