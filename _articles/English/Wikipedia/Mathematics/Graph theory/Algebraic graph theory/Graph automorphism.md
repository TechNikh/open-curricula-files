---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_automorphism
offline_file: ""
offline_thumbnail: ""
uuid: e4d59a1f-4a4b-4cfb-bd6b-5397f6c9617f
updated: 1484309374
title: Graph automorphism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Petersen1_tiny.svg.png
tags:
    - Computational complexity
    - Algorithms, software and applications
    - Symmetry display
    - Graph families defined by their automorphisms
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory, an automorphism of a graph is a form of symmetry in which the graph is mapped onto itself while preserving the edge–vertex connectivity.
