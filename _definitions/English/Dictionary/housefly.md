---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/housefly
offline_file: ""
offline_thumbnail: ""
uuid: 3b43701d-5a36-42a5-b105-e304e14d9449
updated: 1484310162
title: housefly
categories:
    - Dictionary
---
housefly
     n : common fly that frequents human habitations and spreads many
         diseases [syn: {Musca domestica}]
