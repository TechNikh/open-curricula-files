---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/J%C3%BCrgen_Moser'
offline_file: ""
offline_thumbnail: ""
uuid: 354aa33f-4346-413c-9dba-2638ae304d3f
updated: 1484308690
title: Jürgen Moser
tags:
    - Professional biography
    - Students
    - Awards and honours
    - Personal biography
    - Notes
categories:
    - Dynamical systems theorists
---
Jürgen Kurt Moser or Juergen Kurt Moser (July 4, 1928 – December 17, 1999) was an award-winning, German-American mathematician, honored for work spanning over 4 decades, including Hamiltonian dynamical systems and partial differential equations.
