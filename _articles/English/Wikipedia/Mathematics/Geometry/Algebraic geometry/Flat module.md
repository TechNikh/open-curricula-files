---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flat_module
offline_file: ""
offline_thumbnail: ""
uuid: cccb4799-1dee-4084-af2a-3432e5ce7b37
updated: 1484309149
title: Flat module
tags:
    - Definition
    - Commutative rings
    - General rings
    - Examples
    - Case of commutative rings
    - Categorical colimits
    - Homological algebra
    - Flat resolutions
    - Flat covers
    - In constructive mathematics
categories:
    - Algebraic geometry
---
In homological algebra and algebraic geometry, a flat module over a ring R is an R-module M such that taking the tensor product over R with M preserves exact sequences. A module is faithfully flat if taking the tensor product with a sequence produces an exact sequence if and only if the original sequence is exact.
