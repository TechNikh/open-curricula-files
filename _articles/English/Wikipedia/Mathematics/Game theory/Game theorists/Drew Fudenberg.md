---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Drew_Fudenberg
offline_file: ""
offline_thumbnail: ""
uuid: 83e4f0cd-058f-4e3d-b5ad-3c8a6ee02e6f
updated: 1484308513
title: Drew Fudenberg
tags:
    - Biography
    - Publications
categories:
    - Game theorists
---
Drew Fudenberg (born March 2, 1957 in New York City) is the Paul A. Samuelson Professor of Economics at MIT. His extensive research spans many aspects of game theory, including equilibrium theory, learning in games, evolutionary game theory, and many applications to other fields. Fudenberg was also one of the first to apply game theoretic analysis in industrial organization, bargaining theory, and contract theory. He has also authored papers on repeated games, reputation effects, and behavioral economics.
