---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Galapagos
offline_file: ""
offline_thumbnail: ""
uuid: 8fc9279d-6d30-44b8-870f-8bdea162718a
updated: 1484308345
title: Galápagos Islands
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Orthographic_projection_centred_over_the_Galapagos.png
tags:
    - Physical geography
    - Main islands
    - Minor islands
    - Climate
    - History
    - Politics
    - Demographics
    - travel
    - Environmental protection policy
    - Environmental threats
categories:
    - Uncategorized
---
The Galápagos Islands (official name: Archipiélago de Colón, other Spanish name: Islas Galápagos, Spanish pronunciation: [ˈizlaz ɣaˈlapaɣos]) are an archipelago of volcanic islands distributed on either side of the Equator in the Pacific Ocean surrounding the centre of the Western Hemisphere, 906 km (563 mi) west of continental Ecuador, of which they are a part. The islands are known for their vast number of endemic species and were studied by Charles Darwin during the voyage of the Beagle, as his observations and collections contributed to the inception of Darwin's theory of evolution by natural selection.
