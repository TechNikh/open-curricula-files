---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stranger
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484475781
title: stranger
categories:
    - Dictionary
---
stranger
     n : anyone who does not belong in the environment in which they
         are found [syn: {alien}, {unknown}]
