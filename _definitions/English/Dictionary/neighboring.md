---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/neighboring
offline_file: ""
offline_thumbnail: ""
uuid: 67a0b2d7-7a10-4907-93d1-fb0407694ef6
updated: 1484310467
title: neighboring
categories:
    - Dictionary
---
neighboring
     adj 1: situated near one another; "neighbor states" [syn: {neighbor},
             {neighbour}, {neighboring(a)}, {neighbouring(a)}]
     2: having a common boundary or edge; touching; "abutting lots";
        "adjoining rooms"; "Rhode Island has two bordering states;
        Massachusetts and Conncecticut"; "the side of Germany
        conterminous with France"; "Utah and the contiguous state
        of Idaho"; "neighboring cities" [syn: {abutting}, {adjacent},
         {adjoining}, {conterminous}, {contiguous}, {neighboring(a)}]
