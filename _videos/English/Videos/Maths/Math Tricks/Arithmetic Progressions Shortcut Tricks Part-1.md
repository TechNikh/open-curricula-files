---
version: 1
type: video
provider: YouTube
id: pDp8meqgWnI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2040751d-f511-4877-9e48-33c609078c6d
updated: 1486069621
title: Arithmetic Progressions Shortcut Tricks Part-1
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/pDp8meqgWnI/default.jpg
    - https://i3.ytimg.com/vi/pDp8meqgWnI/1.jpg
    - https://i3.ytimg.com/vi/pDp8meqgWnI/2.jpg
    - https://i3.ytimg.com/vi/pDp8meqgWnI/3.jpg
---
