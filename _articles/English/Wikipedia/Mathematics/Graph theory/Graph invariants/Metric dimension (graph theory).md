---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Metric_dimension_(graph_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 38478854-2b2e-498a-b04b-7fab2e24a312
updated: 1484309422
title: Metric dimension (graph theory)
tags:
    - Detailed definition
    - trees
    - Properties
    - Computational complexity
    - Decision complexity
    - Approximation complexity
categories:
    - Graph invariants
---
In graph theory, the metric dimension of a graph G is the minimum cardinality of a subset S of vertices such that all other vertices are uniquely determined by their distances to the vertices in S. Finding the metric dimension of a graph is an NP-hard problem; the decision version, determining whether the metric dimension is less than a given value, is NP-complete.
