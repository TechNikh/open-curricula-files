---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/while
offline_file: ""
offline_thumbnail: ""
uuid: 1af4e60b-b7db-49df-92ae-849adf196d15
updated: 1484310346
title: while
categories:
    - Dictionary
---
while
     n : a period of indeterminate length (usually short) marked by
         some action or condition; "he was here for a little
         while"; "I need to rest for a piece"; "a spell of good
         weather"; "a patch of bad weather" [syn: {piece}, {spell},
          {patch}]
