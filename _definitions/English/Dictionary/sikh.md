---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sikh
offline_file: ""
offline_thumbnail: ""
uuid: e978f2bf-1ee2-463c-9733-d4ee1fd3ece6
updated: 1484310589
title: sikh
categories:
    - Dictionary
---
Sikh
     adj : of or relating to the Sikhs or their religious beliefs and
           customs
     n : an adherent of Sikhism
