---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dinitz_conjecture
offline_file: ""
offline_thumbnail: ""
uuid: bc4da3ff-886b-4af1-9796-a94b9b985678
updated: 1484308570
title: Dinitz conjecture
categories:
    - Combinatorics
---
In combinatorics, the Dinitz Theorem (formerly known as Dinitz Conjecture) is a statement about the extension of arrays to partial Latin squares, proposed in 1979 by Jeff Dinitz, and proved in 1994 by Fred Galvin.
