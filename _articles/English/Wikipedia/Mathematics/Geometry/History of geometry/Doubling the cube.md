---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Doubling_the_cube
offline_file: ""
offline_thumbnail: ""
uuid: 64fbfa9d-8561-49d6-88db-be349c9a4700
updated: 1484309244
title: Doubling the cube
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Cube_and_doubled_cube.svg.png
tags:
    - Proof of impossibility
    - History
    - Solutions via means other than compass and straightedge
    - Using a marked ruler
categories:
    - History of geometry
---
Doubling the cube, also known as the Delian problem, is an ancient[1] geometric problem. Given the edge of a cube, the problem requires the construction of the edge of a second cube whose volume is double that of the first, using only the tools of a compass and straightedge. As with the related problems of squaring the circle and trisecting the angle, doubling the cube is now known to be impossible.
