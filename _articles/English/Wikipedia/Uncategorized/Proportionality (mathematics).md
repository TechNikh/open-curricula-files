---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Directly_proportional
offline_file: ""
offline_thumbnail: ""
uuid: b2eb8fe9-54d0-4feb-bc1f-a68d170ecbd8
updated: 1484308372
title: Proportionality (mathematics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Proportional_variables.svg.png
tags:
    - Direct proportionality
    - Examples
    - Properties
    - Inverse proportionality
    - Hyperbolic coordinates
    - Exponential and logarithmic proportionality
    - growth
    - Notes
categories:
    - Uncategorized
---
In mathematics, two variables are proportional if a change in one is always accompanied by a change in the other, and if the changes are always related by use of a constant multiplier. The constant is called the coefficient of proportionality or proportionality constant.
