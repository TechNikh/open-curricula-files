---
version: 1
type: video
provider: YouTube
id: dPKvHrD1eS4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/In%20Da%20Club%20-%20Membranes%20%26%20Transport-%20Crash%20Course%20Biology%20%235.webm"
offline_file: ""
offline_thumbnail: ""
uuid: e280a6ba-2ad5-4f73-8bbb-bac359ea184c
updated: 1486069677
title: 'In Da Club - Membranes & Transport- Crash Course Biology #5'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/dPKvHrD1eS4/default.jpg
    - https://i3.ytimg.com/vi/dPKvHrD1eS4/1.jpg
    - https://i3.ytimg.com/vi/dPKvHrD1eS4/2.jpg
    - https://i3.ytimg.com/vi/dPKvHrD1eS4/3.jpg
---
