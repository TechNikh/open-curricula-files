---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/furiously
offline_file: ""
offline_thumbnail: ""
uuid: 52a37d59-0f93-4d10-9185-b1befc750cd4
updated: 1484310557
title: furiously
categories:
    - Dictionary
---
furiously
     adv 1: (of the elements) in a wild and stormy manner; "winds were
            blowing furiously"
     2: in a manner marked by extreme or violent energy; "the boys
        fought furiously"; "she went peddling furiously up the
        narrow street"
     3: in an impassioned or very angry manner; "she screamed
        furiously at her tormentors"
