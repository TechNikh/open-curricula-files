---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Arnold%27s_cat_map'
offline_file: ""
offline_thumbnail: ""
uuid: 4d7dc3b0-e1d1-44f3-ab06-c8fe2af0121b
updated: 1484308712
title: "Arnold's cat map"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Arnoldcatmap.svg.png
tags:
    - Properties
    - The discrete cat map
categories:
    - Chaotic maps
---
In mathematics, Arnold's cat map is a chaotic map from the torus into itself, named after Vladimir Arnold, who demonstrated its effects in the 1960s using an image of a cat, hence the name.[1]
