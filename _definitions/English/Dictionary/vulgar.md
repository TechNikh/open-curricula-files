---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vulgar
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484445181
title: vulgar
categories:
    - Dictionary
---
vulgar
     adj 1: lacking refinement or cultivation or taste; "he had coarse
            manners but a first-rate mind"; "behavior that branded
            him as common"; "an untutored and uncouth human
            being"; "an uncouth soldier--a real tough guy";
            "appealing to the vulgar taste for violence"; "the
            vulgar display of the newly rich" [syn: {coarse}, {common},
             {rough-cut}, {uncouth}]
     2: of or associated with the great masses of people; "the
        common people in those days suffered greatly"; "behavior
        that branded him as common"; "his square plebeian nose";
        "a vulgar and objectionable person"; "the unwashed masses"
   ...
