---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mirage
offline_file: ""
offline_thumbnail: ""
uuid: d231892c-b2a2-4633-84a4-f0fef6c4b3b0
updated: 1484310214
title: mirage
categories:
    - Dictionary
---
mirage
     n 1: an optical illusion in which atmospheric refraction by a
          layer of hot air distorts or inverts reflections of
          distant objects
     2: something illusory and unattainable
