---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Fundamental_matrix_(computer_vision)
offline_file: ""
offline_thumbnail: ""
uuid: 56529da5-23f0-4fe3-82bc-f3b975651f0a
updated: 1484309184
title: Fundamental matrix (computer vision)
tags:
    - Introduction
    - Projective reconstruction theorem
    - Proof
    - Derivation of fundamental matrix using coplanarity condition
    - Properties
    - Notes
    - Toolboxes
categories:
    - Geometry in computer vision
---
In computer vision, the fundamental matrix 
  
    
      
        
          F
        
      
    
    {\displaystyle \mathbf {F} }
  
 is a 3×3 matrix which relates corresponding points in stereo images. In epipolar geometry, with homogeneous image coordinates, x and x′, of corresponding points in a stereo image pair, Fx describes a line (an epipolar line) on which the corresponding point x′ on the other image must lie. That means, for all pairs of corresponding points holds
