---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Galilean_electromagnetism
offline_file: ""
offline_thumbnail: ""
uuid: 4414235a-0193-4e54-95a7-45e7d281d0b4
updated: 1484308759
title: Galilean electromagnetism
tags:
    - Overview
    - History
    - Notes
categories:
    - Electrodynamics
---
Galilean electromagnetism is a formal electromagnetic field theory that is consistent with Galilean invariance. Galilean electromagnetism is useful for describing the electric and magnetic fields in the vicinity of charged bodies moving at non-relativistic speeds relative to the frame of reference. The resulting mathematical equations are simpler than the fully relativistic forms because certain coupling terms are neglected.[a]:12
