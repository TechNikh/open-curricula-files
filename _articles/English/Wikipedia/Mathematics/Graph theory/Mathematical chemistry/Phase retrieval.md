---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Phase_retrieval
offline_file: ""
offline_thumbnail: ""
uuid: de59f0f5-f04d-4b6d-859a-5002cb588fd2
updated: 1484309432
title: Phase retrieval
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-GS-diagram.png
tags:
    - Methods
    - Error reduction algorithm
    - Hybrid input-output algorithm
    - Shrinkwrap
categories:
    - Mathematical chemistry
---
Phase retrieval is the process of algorithmically finding solutions to the phase problem. Given a complex signal 
  
    
      
        F
        (
        k
        )
      
    
    {\displaystyle F(k)}
  
, of amplitude 
  
    
      
        
          |
        
        F
        (
        k
        )
        
          |
        
      
    
    {\displaystyle |F(k)|}
  
, and phase 
  
    
      
        ψ
        (
        k
        )
      
    
    {\displaystyle \psi (k)}
  
:
