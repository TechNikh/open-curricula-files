---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mixing_(physics)
offline_file: ""
offline_thumbnail: ""
uuid: 14016f24-454e-4fab-ac18-fac779ea13de
updated: 1484308780
title: Mixing (physics)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Ergodic_mixing_of_putty_ball_after_repeated_Smale_horseshoe_map.jpg
categories:
    - Ergodic theory
---
In physics, a dynamical system is said to be mixing if the phase space of the system becomes strongly intertwined, according to at least one of several mathematical definitions. For example, a measure-preserving transformation T is said to be strong mixing if
