---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Algebraic_geometry_and_analytic_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 3a443071-f3f2-43f5-bdb5-c278b1ceae2a
updated: 1484309130
title: Algebraic geometry and analytic geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Arithmetic_symbols.svg_3.png
tags:
    - Main statement
    - Background
    - Important results
    - "Riemann's existence theorem"
    - The Lefschetz principle
    - "Chow's theorem"
    - GAGA
    - Formal statement of GAGA
    - Notes
categories:
    - Algebraic geometry
---
In mathematics, algebraic geometry and analytic geometry are two closely related subjects. While algebraic geometry studies algebraic varieties, analytic geometry deals with complex manifolds and the more general analytic spaces defined locally by the vanishing of analytic functions of several complex variables. The deep relation between these subjects has numerous applications in which algebraic techniques are applied to analytic spaces and analytic techniques to algebraic varieties.
