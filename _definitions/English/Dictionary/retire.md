---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retire
offline_file: ""
offline_thumbnail: ""
uuid: 25d25ea8-7a4a-4fa7-bc4d-ff7c59dfc175
updated: 1484310457
title: retire
categories:
    - Dictionary
---
retire
     v 1: go into retirement; stop performing one's work or withdraw
          from one's position; "He retired at age 68"
     2: withdraw from active participation; "He retired from chess"
        [syn: {withdraw}]
     3: pull back or move away or backward; "The enemy withdrew";
        "The limo pulled away from the curb" [syn: {withdraw}, {retreat},
         {pull away}, {draw back}, {recede}, {pull back}, {move
        back}]
     4: move back and away from; "The enemy fell back" [syn: {recede},
         {fall back}] [ant: {advance}]
     5: withdraw from circulation or from the market, as of bills,
        shares, and bonds
     6: break from a meeting or gathering; "We ...
