---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/optically
offline_file: ""
offline_thumbnail: ""
uuid: d63c170f-3663-444e-9386-c23d48b0a213
updated: 1484310216
title: optically
categories:
    - Dictionary
---
optically
     adv : in an optical manner; "optically distorted"
