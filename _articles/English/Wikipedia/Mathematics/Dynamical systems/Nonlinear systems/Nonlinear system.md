---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nonlinear_system
offline_file: ""
offline_thumbnail: ""
uuid: 28e58083-3aea-452e-8f66-1cbb1dbd56cc
updated: 1484308811
title: Nonlinear system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-PendulumLayout.svg_0.png
tags:
    - Definition
    - Nonlinear algebraic equations
    - Nonlinear recurrence relations
    - Nonlinear differential equations
    - Ordinary differential equations
    - Partial differential equations
    - Pendula
    - Types of nonlinear behaviors
    - Examples of nonlinear equations
    - Software for solving nonlinear systems
categories:
    - Nonlinear systems
---
In physical sciences, a nonlinear system is a system in which the output is not directly proportional to the input. Nonlinear problems are of interest to engineers, physicists and mathematicians and many other scientists because most systems are inherently nonlinear in nature. Nonlinear systems may appear chaotic, unpredictable or counterintuitive, contrasting with the much simpler linear systems.
