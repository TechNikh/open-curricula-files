---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sand_bath
offline_file: ""
offline_thumbnail: ""
uuid: 4333fb8c-7b60-4d4b-99b7-38c250820ad8
updated: 1484308859
title: Sand bath
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Sand_bath.png
tags:
    - Notes
categories:
    - Thermodynamics
---
A sand bath is a common piece of laboratory equipment made from a container filled with heated sand. It is used to provide even heating for another container, most often during a chemical reaction.
