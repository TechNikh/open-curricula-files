---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/television
offline_file: ""
offline_thumbnail: ""
uuid: 51e6fbc3-e934-46a7-9b3f-26c8dac55ccc
updated: 1484310196
title: television
categories:
    - Dictionary
---
television
     n 1: broadcasting visual images of stationary or moving objects;
          "she is a star of screen and video"; "Television is a
          medium because it is neither rare nor well done" - Ernie
          Kovacs [syn: {telecasting}, {TV}, {video}]
     2: a receiver that displays television images; "the British
        call a tv set a telly" [syn: {television receiver}, {television
        set}, {tv}, {tv set}, {idiot box}, {boob tube}, {telly}, {goggle
        box}]
     3: a telecommunication system that transmits images of objects
        (stationary or moving) between distant points [syn: {television
        system}]
