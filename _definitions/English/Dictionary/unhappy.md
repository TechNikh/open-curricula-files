---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unhappy
offline_file: ""
offline_thumbnail: ""
uuid: fa50cb75-86b4-4825-a0e9-d5739961252f
updated: 1484310567
title: unhappy
categories:
    - Dictionary
---
unhappy
     adj 1: experiencing or marked by or causing sadness or sorrow or
            discontent; "unhappy over her departure"; "unhappy
            with her raise"; "after the argument they lapsed into
            an unhappy silence"; "had an unhappy time at school";
            "the unhappy (or sad) news"; "he looks so sad" [ant: {happy}]
     2: generalized feeling of distress [syn: {dysphoric}, {distressed}]
        [ant: {euphoric}]
     3: causing discomfort; "the unhappy truth"
     4: marked by or producing unhappiness; "infelicitous
        circumstances"; "unhappy caravans, straggling afoot
        through swamps and canebrakes"- American Guide Series
        [syn: ...
