---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ancestral
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484617741
title: ancestral
categories:
    - Dictionary
---
ancestral
     adj 1: inherited or inheritable by established rules (usually legal
            rules) of descent; "ancestral home"; "ancestral lore";
            "hereditary monarchy"; "patrimonial estate";
            "transmissible tradition" [syn: {hereditary}, {patrimonial},
             {transmissible}]
     2: of or belonging to or inherited from an ancestor
