---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula'
offline_file: ""
offline_thumbnail: ""
uuid: bb2fdb98-7dd3-47bb-8419-d7fc11ad4a5c
updated: 1484309300
title: "Rodrigues' rotation formula"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Rodrigues-formula.svg.png
tags:
    - Statement
    - Derivation
    - Matrix notation
categories:
    - Orientation (geometry)
---
In the theory of three-dimensional rotation, Rodrigues' rotation formula, named after Olinde Rodrigues, is an efficient algorithm for rotating a vector in space, given an axis and angle of rotation. By extension, this can be used to transform all three basis vectors to compute a rotation matrix in SO(3), the group of all rotation matrices, from an axis–angle representation. In other words, the Rodrigues' formula provides an algorithm to compute the exponential map from so(3), the Lie algebra of SO(3), to SO(3) without actually computing the full matrix exponential.
