---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/methanol
offline_file: ""
offline_thumbnail: ""
uuid: 4403d934-1936-4506-b2dc-b465b1185335
updated: 1484310424
title: methanol
categories:
    - Dictionary
---
methanol
     n : a light volatile flammable poisonous liquid alcohol; used as
         an antifreeze and solvent and fuel and as a denaturant
         for ethyl alcohol [syn: {methyl alcohol}, {wood alcohol},
          {wood spirit}]
