---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/alteration
offline_file: ""
offline_thumbnail: ""
uuid: 73268d7b-280a-4c00-9580-e8209e6efe8e
updated: 1484310311
title: alteration
categories:
    - Dictionary
---
alteration
     n 1: an event that occurs when something passes from one state or
          phase to another; "the change was intended to increase
          sales"; "this storm is certainly a change for the
          worse"; "the neighborhood had undergone few
          modifications since his last visit years ago" [syn: {change},
           {modification}]
     2: the act of making something different (as e.g. the size of a
        garment) [syn: {modification}, {adjustment}]
     3: the act of revising or altering (involving reconsideration
        and modification); "it would require a drastic revision of
        his opinion" [syn: {revision}]
