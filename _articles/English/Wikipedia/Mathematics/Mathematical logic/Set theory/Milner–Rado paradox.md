---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Milner%E2%80%93Rado_paradox'
offline_file: ""
offline_thumbnail: ""
uuid: 8eb2a688-453b-4e70-aa43-61af71b6415b
updated: 1484309506
title: Milner–Rado paradox
categories:
    - Set theory
---
In set theory, a branch of mathematics, the Milner – Rado paradox, found by Eric Charles Milner and Richard Rado (1965), states that every ordinal number α less than the successor κ+ of some cardinal number κ can be written as the union of sets X1,X2,... where Xn is of order type at most κn for n a positive integer.
