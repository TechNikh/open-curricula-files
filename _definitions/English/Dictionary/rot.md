---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rot
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484635081
title: rot
categories:
    - Dictionary
---
rot
     n 1: decay usually accompanied by an offensive odor [syn: {putrefaction}]
     2: (biology) decaying caused by bacterial or fungal action
        [syn: {decomposition}, {rotting}, {putrefaction}]
     3: unacceptable behavior (especially ludicrously false
        statements) [syn: {bunk}, {bunkum}, {buncombe}, {guff}, {hogwash}]
     v 1: break down; "The bodies decomposed in the heat" [syn: {decompose},
           {molder}, {moulder}]
     2: waste away; "Political prisoners are wasting away in many
        prisons all over the world" [syn: {waste}]
     [also: {rotting}, {rotted}]
