---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/synthetic
offline_file: ""
offline_thumbnail: ""
uuid: 5787e37f-1cfd-46a1-81ef-d7b8e2bfa04f
updated: 1484310284
title: synthetic
categories:
    - Dictionary
---
synthetic
     adj 1: not of natural origin; prepared or made artificially;
            "man-made fibers"; "synthetic leather" [syn: {man-made},
             {semisynthetic}]
     2: involving or of the nature of synthesis (combining separate
        elements to form a coherent whole) as opposed to analysis;
        "limnology is essentially a synthetic science composed of
        elements...that extend well beyond the limits of biology"-
        P.S.Welch [syn: {synthetical}] [ant: {analytic}]
     3: systematic combining of root and modifying elements into
        single words [ant: {analytic}]
     4: of a proposition whose truth value is determined by
        observation or facts; "`all ...
