---
version: 1
type: video
provider: YouTube
id: yj-wkw98j7Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 27219440-c553-4faf-a673-fb1a0b0f2ba1
updated: 1486069632
title: How to Make a Matchbox Microphone
categories:
    - Science projects
thumbnail_urls:
    - https://i3.ytimg.com/vi/yj-wkw98j7Q/default.jpg
    - https://i3.ytimg.com/vi/yj-wkw98j7Q/1.jpg
    - https://i3.ytimg.com/vi/yj-wkw98j7Q/2.jpg
    - https://i3.ytimg.com/vi/yj-wkw98j7Q/3.jpg
---
