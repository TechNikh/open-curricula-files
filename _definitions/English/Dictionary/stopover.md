---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stopover
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484515621
title: stopover
categories:
    - Dictionary
---
stop over
     v 1: interrupt a journey temporarily, e.g., overnight; "We had to
          stop over in Venezuela on our flight back from Brazil"
          [syn: {lay over}]
     2: interrupt a trip; "we stopped at Aunt Mary's house"; "they
        stopped for three days in Florence" [syn: {stop}]
