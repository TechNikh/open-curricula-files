---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Full_width_at_half_maximum
offline_file: ""
offline_thumbnail: ""
uuid: a36a62ee-3f5f-48f7-bb52-ee055ec196cd
updated: 1484308883
title: Full width at half maximum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-FWHM.svg.png
categories:
    - Waves
---
Full width at half maximum (FWHM) is an expression of the extent of a function given by the difference between the two extreme values of the independent variable at which the dependent variable is equal to half of its maximum value. In other words, it is the width of a spectrum curve measured between those points on the y-axis which are half the maximum amplitude.
