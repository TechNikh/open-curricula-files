---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dictator
offline_file: ""
offline_thumbnail: ""
uuid: 8e941a6e-8e81-4d2e-818f-5361818c9421
updated: 1484310174
title: dictator
categories:
    - Dictionary
---
dictator
     n 1: a speaker who dictates to a secretary or a recording machine
     2: a ruler who is unconstrained by law [syn: {potentate}]
     3: a person behaves in an tyrannical manner; "my boss is a
        dictator who makes everyone work overtime" [syn: {authoritarian}]
