---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cabri_Geometry
offline_file: ""
offline_thumbnail: ""
uuid: 7ce7720c-854a-4e96-8210-a64bb3e7a683
updated: 1484309352
title: Cabri Geometry
categories:
    - Geometry stubs
---
Cabri Geometry is a commercial interactive geometry software produced by the French company Cabrilog for teaching and learning geometry and trigonometry.[1][2] It was designed with ease-of-use in mind. The program allows the user to animate geometric figures, proving a significant advantage over those drawn on a blackboard. Relationships between points on a geometric object may easily be demonstrated, which can be useful in the learning process. There are also graphing and display functions which allow exploration of the connections between geometry and algebra. The program can be run under Windows or the Mac OS.
