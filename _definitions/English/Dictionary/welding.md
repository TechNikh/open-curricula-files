---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/welding
offline_file: ""
offline_thumbnail: ""
uuid: a90aaadf-880b-4cef-a8dc-e91bdb07e6cd
updated: 1484310426
title: welding
categories:
    - Dictionary
---
welding
     n : fastening two pieces of metal together by softening with
         heat and applying pressure
