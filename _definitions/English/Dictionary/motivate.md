---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/motivate
offline_file: ""
offline_thumbnail: ""
uuid: 70f4964e-0766-4cdd-8b54-2b6417e1dd75
updated: 1484310249
title: motivate
categories:
    - Dictionary
---
motivate
     v : give an incentive for action; "This moved me to sacrifice my
         career" [syn: {actuate}, {propel}, {move}, {prompt}, {incite}]
