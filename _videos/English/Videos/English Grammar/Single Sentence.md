---
version: 1
type: video
provider: YouTube
id: Wq9Xept-Qr0
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8503d8dd-9b86-47a0-9b75-46f28b7df3d0
updated: 1486069631
title: Single Sentence
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/Wq9Xept-Qr0/default.jpg
    - https://i3.ytimg.com/vi/Wq9Xept-Qr0/1.jpg
    - https://i3.ytimg.com/vi/Wq9Xept-Qr0/2.jpg
    - https://i3.ytimg.com/vi/Wq9Xept-Qr0/3.jpg
---
