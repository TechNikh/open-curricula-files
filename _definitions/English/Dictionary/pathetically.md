---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pathetically
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484514001
title: pathetically
categories:
    - Dictionary
---
pathetically
     adv 1: in a manner arousing sympathy and compassion; "the sick
            child cried pathetically" [syn: {pitiably}]
     2: arousing scornful pity; "they had pathetically little
        money"; "it was pathetically bad"
