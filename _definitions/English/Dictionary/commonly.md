---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/commonly
offline_file: ""
offline_thumbnail: ""
uuid: c90b198c-7715-417b-91f3-f2429281ad32
updated: 1484310216
title: commonly
categories:
    - Dictionary
---
commonly
     adv : under normal conditions; "usually she was late" [syn: {normally},
            {usually}, {unremarkably}, {ordinarily}] [ant: {unusually}]
