---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/B%C3%B8rge_Jessen'
offline_file: ""
offline_thumbnail: ""
uuid: 2680c4fd-5486-408b-9a29-11b7c86da0f0
updated: 1484309085
title: Børge Jessen
tags:
    - Early years
    - Career
categories:
    - Geometers
---
Børge Christian Jessen (19 June 1907 – 20 March 1993) was a Danish mathematician best known for his work in analysis, specifically on the Riemann zeta function, and in geometry, specifically on Hilbert's third problem.
