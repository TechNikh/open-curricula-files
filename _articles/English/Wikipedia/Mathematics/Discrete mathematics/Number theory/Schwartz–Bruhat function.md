---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Schwartz%E2%80%93Bruhat_function'
offline_file: ""
offline_thumbnail: ""
uuid: a0d25313-3b04-466c-8553-f82327eedda3
updated: 1484308657
title: Schwartz–Bruhat function
categories:
    - Number theory
---
In mathematics, a Schwartz–Bruhat function, named after Laurent Schwartz and François Bruhat, is a function on a locally compact abelian group, such as the adeles, that generalizes a Schwartz function on a real vector space. A tempered distribution is defined as a continuous linear functional on the space of Schwartz–Bruhat functions.
