---
version: 1
type: video
provider: YouTube
id: tKZs-U5nIBI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: be4c9f2d-1689-48dd-8fd8-0c876b5b1504
updated: 1486069635
title: >
    Concave Mirror Focal Length By 2 Needle method Class 12 CBSE
    Physics
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/tKZs-U5nIBI/default.jpg
    - https://i3.ytimg.com/vi/tKZs-U5nIBI/1.jpg
    - https://i3.ytimg.com/vi/tKZs-U5nIBI/2.jpg
    - https://i3.ytimg.com/vi/tKZs-U5nIBI/3.jpg
---
