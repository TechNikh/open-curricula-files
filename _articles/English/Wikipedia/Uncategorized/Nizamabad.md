---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nizamabad
offline_file: ""
offline_thumbnail: ""
uuid: c279380f-0db5-40e8-b4d8-040ee0fb60cb
updated: 1484308447
title: Nizamabad
tags:
    - India
    - Iran
    - Fars Province
    - Golestan Province
    - Hamadan Province
    - Hormozgan Province
    - Isfahan Province
    - Kerman Province
    - Kermanshah Province
    - Markazi Province
    - Mazandaran Province
    - Qazvin Province
    - Razavi Khorasan Province
    - Tehran Province
    - West Azerbaijan Province
    - Zanjan Province
    - Pakistan
categories:
    - Uncategorized
---
