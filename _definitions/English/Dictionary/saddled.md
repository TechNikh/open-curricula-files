---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/saddled
offline_file: ""
offline_thumbnail: ""
uuid: 4cebcb14-db7f-4eea-a294-142712ea2d52
updated: 1484310561
title: saddled
categories:
    - Dictionary
---
saddled
     adj 1: having a saddle on or being mounted on a saddled animal;
            "saddled and spurred and ready to ride" [ant: {unsaddled}]
     2: subject to an imposed burden; "left me saddled with the
        bill"; "found himself saddled with more responsibility
        than power"
