---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chrome
offline_file: ""
offline_thumbnail: ""
uuid: d385a7f5-3e47-4f56-839e-3b544d36211c
updated: 1484310377
title: chrome
categories:
    - Dictionary
---
chrome
     n : another word for chromium when it is used in dyes or
         pigments
     v 1: plate with chromium; "chrome bathroom fixtures" [syn: {chromium-plate}]
     2: treat with a chromium compound
