---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/List_of_nonlinear_partial_differential_equations
offline_file: ""
offline_thumbnail: ""
uuid: f66c3c1c-df4d-4fb0-8309-268d4d85e9f8
updated: 1484309230
title: List of nonlinear partial differential equations
tags:
    - >
        Methods for studying nonlinear partial differential
        equations
    - Existence and uniqueness of solutions
    - Singularities
    - Linear approximation
    - Moduli space of solutions
    - Exact solutions
    - Numerical solutions
    - Lax pair
    - Euler–Lagrange equations
    - Hamilton equations
    - Integrable systems
    - Symmetry
    - Look it up
    - List of equations
    - A–F
    - G–K
    - L–Q
    - R–Z, α–ω
categories:
    - Differential geometry
---
In mathematics and physics, nonlinear partial differential equations are (as their name suggests) partial differential equations with nonlinear terms. They describe many different physical systems, ranging from gravitation to fluid dynamics, and have been used in mathematics to solve problems such as the Poincaré conjecture and the Calabi conjecture. They are difficult to study: there are almost no general techniques that work for all such equations, and usually each individual equation has to be studied as a separate problem.
