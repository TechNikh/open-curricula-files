---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/clause
offline_file: ""
offline_thumbnail: ""
uuid: 1563a139-1576-4cde-ba88-d190768a3a18
updated: 1484310601
title: clause
categories:
    - Dictionary
---
clause
     n 1: (grammar) an expression including a subject and predicate
          but not constituting a complete sentence
     2: a separate section of a legal document (as a statute or
        contract or will) [syn: {article}]
