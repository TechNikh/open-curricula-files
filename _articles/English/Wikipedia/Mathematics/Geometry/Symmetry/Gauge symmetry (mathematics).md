---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gauge_symmetry_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: bad717d9-bdc6-4152-9c91-c88a461218fb
updated: 1484309312
title: Gauge symmetry (mathematics)
categories:
    - Symmetry
---
In mathematics, any Lagrangian system generally admits gauge symmetries, though it may happen that they are trivial. In theoretical physics, the notion of gauge symmetries depending on parameter functions is a cornerstone of contemporary field theory.
