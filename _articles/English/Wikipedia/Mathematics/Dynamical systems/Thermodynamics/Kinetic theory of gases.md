---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Kinetic_theory_of_gases
offline_file: ""
offline_thumbnail: ""
uuid: 6533ead0-8b81-4434-9a29-13e3806c30eb
updated: 1484308853
title: Kinetic theory of gases
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Translational_motion_0.gif
tags:
    - Assumptions
    - Properties
    - Pressure and kinetic energy
    - Temperature and kinetic energy
    - Collisions with container
    - Speed of molecules
    - Transport properties
    - History
    - Notes
categories:
    - Thermodynamics
---
The kinetic theory describe a gas as a large number of submicroscopic particles (atoms or molecules), all of which are in constant rapid motion that has randomness arising from their many collisions with each other and with the walls of the container.
