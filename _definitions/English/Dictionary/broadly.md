---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/broadly
offline_file: ""
offline_thumbnail: ""
uuid: 6e788797-97ce-458f-948d-52cac949839e
updated: 1484310437
title: broadly
categories:
    - Dictionary
---
broadly
     adv 1: without regard to specific details or exceptions; "he
            interprets the law broadly" [syn: {loosely}, {broadly
            speaking}, {generally}] [ant: {narrowly}]
     2: in a wide fashion; "he smiled broadly"
