---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/offer
offline_file: ""
offline_thumbnail: ""
uuid: 0727e9b0-9d07-4ec1-a716-c2e13a75fdba
updated: 1484310196
title: offer
categories:
    - Dictionary
---
offer
     n 1: the verbal act of offering; "a generous offer of assistance"
          [syn: {offering}]
     2: something offered (as a proposal or bid); "noteworthy new
        offerings for investors included several index funds"
        [syn: {offering}]
     3: a usually brief attempt; "he took a crack at it"; "I gave it
        a whirl" [syn: {crack}, {fling}, {go}, {pass}, {whirl}]
     v 1: make available or accessible, provide or furnish; "The
          conference center offers a health spa"; "The hotel
          offers private meeting rooms"
     2: present for acceptance or rejection; "She offered us all a
        cold drink" [syn: {proffer}]
     3: agree freely; "She ...
