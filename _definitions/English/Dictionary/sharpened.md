---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sharpened
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484366221
title: sharpened
categories:
    - Dictionary
---
sharpened
     adj 1: having the point made sharp; "a sharpened pencil"
     2: made sharp or sharper; "a sharpened knife cuts more cleanly"
