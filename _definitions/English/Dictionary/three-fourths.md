---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/three-fourths
offline_file: ""
offline_thumbnail: ""
uuid: 473401c9-3544-4ff8-912d-76a65837bdcb
updated: 1484310553
title: three-fourths
categories:
    - Dictionary
---
three-fourths
     n : three of four equal parts; "three-fourths of a pound"
