---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vortex_ring_state
offline_file: ""
offline_thumbnail: ""
uuid: 3da6e3fc-74dd-418e-af20-7abf78fde848
updated: 1484308878
title: Vortex ring state
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Vortex_ring_helicopter.jpg
tags:
    - Description
    - Occurrence
    - Detection and correction
    - Powering out of vortex ring state
    - Pilot or operator reaction
    - Tandem rotor helicopters
    - Radio control quadcopters
categories:
    - Vortices
---
The vortex ring state, also known as settling with power, is a dangerous condition that may arise in helicopter flight, when a vortex ring system engulfs the rotor causing severe loss of lift. The FAA sees these terms as synonymous, whereas Transport Canada sees them as two different phenomena.[1]
