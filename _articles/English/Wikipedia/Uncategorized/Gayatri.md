---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gayathri
offline_file: ""
offline_thumbnail: ""
uuid: d52eddca-efe8-49ec-ae94-f1020136df2b
updated: 1484308428
title: Gayatri
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Gayatri1.jpg
tags:
    - Portrayal
    - In popular culture
    - Notes
categories:
    - Uncategorized
---
Gayatri (Sanskrit: gāyatrī) is the feminine form of gāyatra, a Sanskrit word for a song or a hymn, having a Vedic meter of three padas, or lines, of eight syllables. In particular, it refers to the Gayatri Mantra and the Goddess Gāyatrī as that mantra personified.
