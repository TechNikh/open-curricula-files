---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/curious
offline_file: ""
offline_thumbnail: ""
uuid: 74164110-37d8-4c77-931d-ba91661faeb7
updated: 1484310607
title: curious
categories:
    - Dictionary
---
curious
     adj 1: beyond or deviating from the usual or expected; "a curious
            hybrid accent"; "her speech has a funny twang"; "they
            have some funny ideas about war"; "had an odd name";
            "the peculiar aromatic odor of cloves"; "something
            definitely queer about this town"; "what a rum
            fellow"; "singular behavior" [syn: {funny}, {odd}, {peculiar},
             {queer}, {rum}, {rummy}, {singular}]
     2: eager to investigate and learn or learn more (sometimes
        about others' concerns); "a curious child is a teacher's
        delight"; "a trap door that made me curious"; "curious
        investigators"; "traffic was slowed by ...
