---
version: 1
type: video
provider: YouTube
id: lsOsce_7gK8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dffcb550-fe6c-4542-ad84-36ffd7a4559e
updated: 1486069616
title: Inverse of a 2x2 matrix
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/lsOsce_7gK8/default.jpg
    - https://i3.ytimg.com/vi/lsOsce_7gK8/1.jpg
    - https://i3.ytimg.com/vi/lsOsce_7gK8/2.jpg
    - https://i3.ytimg.com/vi/lsOsce_7gK8/3.jpg
---
