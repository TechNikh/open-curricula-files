---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/privy
offline_file: ""
offline_thumbnail: ""
uuid: 096d871e-c5d8-4bc5-b7a2-8f4e594dca66
updated: 1484310591
title: privy
categories:
    - Dictionary
---
privy
     adj 1: hidden from general view or use; "a privy place to rest and
            think"; "a secluded romantic spot"; "a secret garden"
            [syn: {secluded}, {secret}]
     2: (followed by `to') informed about something secret or not
        generally known; "privy to the details of the conspiracy"
        [syn: {privy(p)}]
     n 1: a room equipped with toilet facilities [syn: {toilet}, {lavatory},
           {lav}, {can}, {john}, {bathroom}]
     2: a small outbuilding with a bench having holes through which
        a user can defecate [syn: {outhouse}, {earth-closet}, {jakes}]
     [also: {priviest}, {privier}]
