---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fourteen
offline_file: ""
offline_thumbnail: ""
uuid: 5486e171-dae6-462e-a4fb-74063ddcd702
updated: 1484310254
title: fourteen
categories:
    - Dictionary
---
fourteen
     adj : being one more than thirteen [syn: {14}, {xiv}]
     n : the cardinal number that is the sum of thirteen and one
         [syn: {14}, {XIV}]
