---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coulomb
offline_file: ""
offline_thumbnail: ""
uuid: dcdb3d1b-ae6b-4f55-80a7-8eeaaf498127
updated: 1484310204
title: coulomb
categories:
    - Dictionary
---
coulomb
     n 1: a unit of electrical charge equal to the amount of charge
          transferred by a current of 1 ampere in 1 second [syn: {C},
           {ampere-second}]
     2: French physicist famous for his discoveries in the field of
        electricity and magnetism; formulated Coulomb's Law
        (1736-1806) [syn: {Charles Augustin de Coulomb}]
