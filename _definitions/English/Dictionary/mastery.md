---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mastery
offline_file: ""
offline_thumbnail: ""
uuid: f7b034a4-113c-477f-9bc3-5efa8b37db79
updated: 1484310555
title: mastery
categories:
    - Dictionary
---
mastery
     n 1: great skillfulness and knowledge of some subject or
          activity; "a good command of French" [syn: {command}, {control}]
     2: power to dominate or defeat; "mastery of the seas" [syn: {domination},
         {supremacy}]
     3: the act of mastering or subordinating someone [syn: {subordination}]
