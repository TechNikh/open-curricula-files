---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overlap
offline_file: ""
offline_thumbnail: ""
uuid: 3ed4764e-087e-4353-baa3-066dfef0e7a8
updated: 1484310365
title: overlap
categories:
    - Dictionary
---
overlap
     n 1: a representation of common ground between theories or
          phenomena; "there was no overlap between their
          proposals" [syn: {convergence}, {intersection}]
     2: the property of partial coincidence in time
     3: a flap that lies over another part; "the lap of the shingles
        should be at least ten inches" [syn: {lap}]
     v 1: coincide partially or wholly; "Our vacations overlap"
     2: extend over and cover a part of; "The roofs of the houses
        overlap in this crowded city"
     [also: {overlapping}, {overlapped}]
