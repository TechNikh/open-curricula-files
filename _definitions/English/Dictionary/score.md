---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/score
offline_file: ""
offline_thumbnail: ""
uuid: 8e6f913f-cc9e-40ca-ae26-05a238c22e77
updated: 1484310172
title: score
categories:
    - Dictionary
---
score
     n 1: a number or letter indicating quality (especially of a
          student's performance); "she made good marks in
          algebra"; "grade A milk"; "what was your score on your
          homework?" [syn: {mark}, {grade}]
     2: a written form of a musical composition; parts for different
        instruments appear on separate staves on large pages; "he
        studied the score of the sonata" [syn: {musical score}]
     3: a number that expresses the accomplishment of a team or an
        individual in a game or contest; "the score was 7 to 0"
     4: a set of twenty members; "a score were sent out but only one
        returned"
     5: grounds; "don't do it on my ...
