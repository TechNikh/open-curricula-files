---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Anti-de_Sitter_space
offline_file: ""
offline_thumbnail: ""
uuid: 1bbfc82f-92c0-4316-abb3-f211344ef2cf
updated: 1484309200
title: Anti-de Sitter space
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-HyperboloidOfOneSheet.svg.png
tags:
    - Non-technical explanation
    - Technical terms translated
    - Spacetime in general relativity
    - de Sitter space in general relativity
    - Anti-de Sitter space distinguished from de Sitter space
    - >
        de Sitter space and anti-de Sitter space viewed as embedded
        in five dimensions
    - Caveats
    - Definition and properties
    - Closed timelike curves and the universal cover
    - Symmetries
    - Coordinate patches
    - As a homogeneous, symmetric space
    - >
        A simple definition for anti-de Sitter space and its
        properties
    - Global coordinates
    - Poincaré coordinates
    - Geometric properties
categories:
    - Differential geometry
---
In mathematics and physics, n-dimensional anti-de Sitter space (AdSn) is a maximally symmetric Lorentzian manifold with constant negative scalar curvature. It is the Lorentzian analogue of hyperbolic space, just as Minkowski space is the analogue of Euclidean space and de Sitter space is the analogue of spherical space.
