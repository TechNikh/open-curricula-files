---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fujiwhara_effect
offline_file: ""
offline_thumbnail: ""
uuid: fd94b403-8289-49c9-9052-4ec5ab29e4a6
updated: 1484308875
title: Fujiwhara effect
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-ParmaMelor_AMO_TMO_2009279_lrg.jpg
tags:
    - Description
    - Tropical cyclones
    - Examples
    - North Atlantic
    - Northeast Pacific
    - Northwest Pacific
    - Southwest Indian Ocean
    - South Pacific Ocean
    - Extratropical cyclones
categories:
    - Vortices
---
The Fujiwhara effect, sometimes referred to as Fujiwara interaction or binary interaction, is when two nearby cyclonic vortices orbit each other and close the distance between the circulations of their corresponding low-pressure areas. The effect is named after Sakuhei Fujiwhara, the Japanese meteorologist who initially described the effect. Binary interaction of smaller circulations can cause the development of a larger cyclone, or cause two cyclones to merge into one. Extratropical cyclones typically engage in binary interaction when within 2,000 kilometres (1,200 mi) of one another, while tropical cyclones typically interact within 1,400 kilometres (870 mi) of each other.
