---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wearing
offline_file: ""
offline_thumbnail: ""
uuid: 8aa2203a-98f4-469f-98f0-22b0e61d94dc
updated: 1484310264
title: wearing
categories:
    - Dictionary
---
wearing
     adj : producing exhaustion; "an exhausting march"; "the visit was
           especially wearing" [syn: {exhausting}, {tiring}, {wearying}]
     n 1: (geology) the mechanical process of wearing or grinding
          something down (as by particles washing over it) [syn: {erosion},
           {eroding}, {eating away}, {wearing away}]
     2: the act of having on your person as a covering or adornment;
        "she bought it for everyday wear" [syn: {wear}]
