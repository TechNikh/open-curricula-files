---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/announcement
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484450881
title: announcement
categories:
    - Dictionary
---
announcement
     n 1: a formal public statement; "the government made an
          announcement about changes in the drug war"; "a
          declaration of independence" [syn: {proclamation}, {annunciation},
           {declaration}]
     2: a public statement about something that is happening or
        going to happen; "the announcement appeared in the local
        newspaper"; "the promulgation was written in English"
        [syn: {promulgation}]
