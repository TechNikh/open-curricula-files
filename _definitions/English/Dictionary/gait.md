---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gait
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484466721
title: gait
categories:
    - Dictionary
---
gait
     n 1: the rate of moving (especially walking or running) [syn: {pace}]
     2: a horse's manner of moving
     3: a person's manner of walking
