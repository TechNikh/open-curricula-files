---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quadratic_integrate_and_fire
offline_file: ""
offline_thumbnail: ""
uuid: 1cc251ee-131e-4ee6-96e2-b943a5d855ed
updated: 1484308802
title: Quadratic integrate and fire
categories:
    - Nonlinear systems
---
The quadratic integrate and fire (QIF) model is a biological neuron model and a type of integrate-and-fire neuron which describes action potentials in neurons. In contrast to physiologically accurate but computationally expensive neuron models like the Hodgkin–Huxley model, the QIF model seeks only to produce action potential-like patterns and ignores subtleties like gating variables, which play an important role in generating action potentials in a real neuron. However, the QIF model is incredibly easy to implement and compute, and relatively straightforward to study and understand, thus has found ubiquitous use in computational neuroscience[citation needed].
