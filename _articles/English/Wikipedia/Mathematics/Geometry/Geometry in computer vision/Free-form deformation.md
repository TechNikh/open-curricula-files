---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Free-form_deformation
offline_file: ""
offline_thumbnail: ""
uuid: 27d059e6-3fe3-408c-8405-c61652b65e05
updated: 1484309184
title: Free-form deformation
categories:
    - Geometry in computer vision
---
In computer graphics, free-form deformation (FFD) is a geometric technique used to model simple deformations of rigid objects. It is based on the idea of enclosing an object within a cube or another hull object, and transforming the object within the hull as the hull is deformed. Deformation of the hull is based on the concept of so-called hyper-patches, which are three-dimensional analogs of parametric curves such as Bézier curves, B-splines, or NURBs. The technique was first described by Thomas W. Sederberg and Scott R. Parry in 1986,[1] and is based on an earlier technique by Alan Barr.[2] It was extended by Coquillart to a technique described as extended free-form deformation, which ...
