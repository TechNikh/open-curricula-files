---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aravali
offline_file: ""
offline_thumbnail: ""
uuid: 0648589a-b83f-47b7-b6d7-59d72daaee4a
updated: 1484308426
title: Aravalli Range
tags:
    - features
    - mining
    - Ganeshwar sunari Cultural Complex
    - Ecological concerns
    - Gallery
    - External sources
    - Notes
categories:
    - Uncategorized
---
The Aravali Range[1] (Hindi: अरावली, lit. 'line of peaks')[2] is a range of mountains in western India running approximately 692 km (430 mi) in a northeastern direction across the Indian states of Gujarat, Rajasthan, and Haryana, ending in Delhi.[3][4][5]
