---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dead_water
offline_file: ""
offline_thumbnail: ""
uuid: 5c7e82cc-118e-49db-9dca-65e8e40427e2
updated: 1484308885
title: Dead water
categories:
    - Waves
---
Dead water is the nautical term for a phenomenon which can occur when a layer of fresh or brackish water rests on top of denser salt water, without the two layers mixing. A ship powered by direct thrust under the waterline (such as a propeller), traveling in such conditions may be hard to maneuver or can even slow down almost to a standstill. Much of the energy from the ship's propeller only results in waves and turbulence between the two layers of water, leaving a ship capable of traveling at perhaps as little as 20% of its normal speed.[citation needed]
