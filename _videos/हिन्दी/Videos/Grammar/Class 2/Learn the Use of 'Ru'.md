---
version: 1
type: video
provider: YouTube
id: 7J8VghyPJ6w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 28ffc81b-445f-4254-9f5e-07b8579621e7
updated: 1486069630
title: "Learn the Use of 'Ru'"
categories:
    - Class 2
thumbnail_urls:
    - https://i3.ytimg.com/vi/7J8VghyPJ6w/default.jpg
    - https://i3.ytimg.com/vi/7J8VghyPJ6w/1.jpg
    - https://i3.ytimg.com/vi/7J8VghyPJ6w/2.jpg
    - https://i3.ytimg.com/vi/7J8VghyPJ6w/3.jpg
---
