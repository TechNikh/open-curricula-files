---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/res
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484377141
title: res
categories:
    - Dictionary
---
RES
     n : a widely distributed system consisting of all the cells able
         to ingest bacteria or colloidal particles etc, except for
         certain white blood cells [syn: {reticuloendothelial
         system}]
