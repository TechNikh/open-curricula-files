---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solubility
offline_file: ""
offline_thumbnail: ""
uuid: 06bb25a1-f12f-4082-ba8b-2eb3d4aece01
updated: 1484310226
title: solubility
categories:
    - Dictionary
---
solubility
     n 1: the quality of being soluble [ant: {insolubility}]
     2: the quantity of a particular substance that can dissolve in
        a particular solvent (yielding a saturated solution)
