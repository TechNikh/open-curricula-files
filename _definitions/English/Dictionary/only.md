---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/only
offline_file: ""
offline_thumbnail: ""
uuid: 331e63d9-8409-4d48-b103-99d22f1f6135
updated: 1484310355
title: only
categories:
    - Dictionary
---
only
     adj 1: being the only one; single and isolated from others; "the
            lone doctor in the entire county"; "a lonesome pine";
            "an only child"; "the sole heir"; "the sole example";
            "a solitary instance of cowardice"; "a solitary speck
            in the sky" [syn: {lone(a)}, {lonesome(a)}, {only(a)},
             {sole(a)}, {solitary(a)}]
     2: exclusive of anyone or anything else; "she alone believed
        him"; "cannot live by bread alone"; "I'll have this car
        and this car only" [syn: {alone(p)}]
     adv 1: and nothing more; "I was merely asking"; "it is simply a
            matter of time"; "just a scratch"; "he was only a
            ...
