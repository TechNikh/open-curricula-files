---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/depicted
offline_file: ""
offline_thumbnail: ""
uuid: 6af454b4-d4c7-4401-ac22-9e4db31cd28c
updated: 1484310403
title: depicted
categories:
    - Dictionary
---
depicted
     adj : represented graphically by sketch or design or lines [syn: {pictured},
            {portrayed}]
