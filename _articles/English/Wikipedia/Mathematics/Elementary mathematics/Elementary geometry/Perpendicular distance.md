---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Perpendicular_distance
offline_file: ""
offline_thumbnail: ""
uuid: 45833880-d8a6-4566-9bf5-28210fa4672d
updated: 1484308941
title: Perpendicular distance
categories:
    - Elementary geometry
---
In geometry, the perpendicular distance between two objects is the distance from one to the other, measured along a line that is perpendicular to one or both. In particular, see:
