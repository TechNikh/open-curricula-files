---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hope
offline_file: ""
offline_thumbnail: ""
uuid: 957f9509-d9b1-4eec-b253-8f4f7e692841
updated: 1484310467
title: hope
categories:
    - Dictionary
---
hope
     n 1: a specific instance of feeling hopeful; "it revived their
          hope of winning the pennant"
     2: the general feeling that some desire will be fulfilled; "in
        spite of his troubles he never gave up hope" [ant: {despair}]
     3: grounds for feeling hopeful about the future; "there is
        little or no promise that he will recover" [syn: {promise}]
     4: someone (or something) on which expectations are centered;
        "he was their best hope for a victory"
     5: United States comedian (born in England) who appeared in
        films with Bing Crosby (born in 1903) [syn: {Bob Hope}, {Leslie
        Townes Hope}]
     6: one of the three Christian virtues
  ...
