---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_line
offline_file: ""
offline_thumbnail: ""
uuid: 164bbfc7-2b48-4529-ba0f-96f17835a050
updated: 1484309356
title: Complex line
categories:
    - Geometry stubs
---
In mathematics, a complex line is a one-dimensional affine subspace of a vector space over the complex numbers.[1][2] A common point of confusion is that while a complex line has dimension one over C (hence the term "line"), it has dimension two over the real numbers R, and is topologically equivalent to a real plane, not a real line.[3]
