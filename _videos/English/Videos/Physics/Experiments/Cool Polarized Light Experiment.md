---
version: 1
type: video
provider: YouTube
id: Hx6ZArQum-A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 13217f2b-dbe6-46e5-aa62-5b2bee2be9a1
updated: 1486069634
title: Cool Polarized Light Experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/Hx6ZArQum-A/default.jpg
    - https://i3.ytimg.com/vi/Hx6ZArQum-A/1.jpg
    - https://i3.ytimg.com/vi/Hx6ZArQum-A/2.jpg
    - https://i3.ytimg.com/vi/Hx6ZArQum-A/3.jpg
---
