---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ganges
offline_file: ""
offline_thumbnail: ""
uuid: a7862749-7158-4054-b715-edf5535b77c0
updated: 1484310525
title: ganges
categories:
    - Dictionary
---
Ganges
     n : an Asian river; rises in the Himalayas and flows east into
         the Bay of Bengal; a sacred river of the Hindus [syn: {Ganges
         River}]
