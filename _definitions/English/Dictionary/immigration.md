---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immigration
offline_file: ""
offline_thumbnail: ""
uuid: ced0d2cc-4ac4-4929-b977-6ea051a46aa8
updated: 1484310517
title: immigration
categories:
    - Dictionary
---
immigration
     n 1: migration into a place (especially migration to a country of
          which you are not a native in order to settle there)
          [syn: {in-migration}]
     2: the body of immigrants arriving during a specified interval;
        "the increased immigration strengthened the colony"
