---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feminist
offline_file: ""
offline_thumbnail: ""
uuid: 0ca05b7e-ab36-4f20-8a9e-a1c7d2acccf9
updated: 1484310192
title: feminist
categories:
    - Dictionary
---
feminist
     adj : of or relating to or advocating equal rights for women;
           "feminist critique"
     n : a supporter of feminism [syn: {women's rightist}, {women's
         liberationist}, {libber}]
