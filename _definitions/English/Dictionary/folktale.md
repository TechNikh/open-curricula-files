---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/folktale
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484476321
title: folktale
categories:
    - Dictionary
---
folk tale
     n : a tale circulated by word of mouth among the common folk
         [syn: {folktale}]
