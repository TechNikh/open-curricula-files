---
version: 1
type: video
provider: YouTube
id: M5uOIy-JTmo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Physics/Thermodynamics/PV-diagrams%20and%20expansion%20work.webm"
offline_file: ""
offline_thumbnail: ""
uuid: fbfcc537-b87c-4c89-91c6-e86922fbba3c
updated: 1486069589
title: PV-diagrams and expansion work
categories:
    - Thermodynamics
thumbnail_urls:
    - https://i3.ytimg.com/vi/M5uOIy-JTmo/default.jpg
    - https://i3.ytimg.com/vi/M5uOIy-JTmo/1.jpg
    - https://i3.ytimg.com/vi/M5uOIy-JTmo/2.jpg
    - https://i3.ytimg.com/vi/M5uOIy-JTmo/3.jpg
---
Why work from expansion is the area under the curve of a PV-diagram. Why heat is not a state function and internal energy is a state function. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/thermodynamics/laws-of-thermodynamics/v/proof-u-3-2-pv-or-u-3-2-nrt?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/thermodynamics/laws-of-thermodynamics/v/work-from-expansion?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
