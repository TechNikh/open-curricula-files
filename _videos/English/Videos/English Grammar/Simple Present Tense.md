---
version: 1
type: video
provider: YouTube
id: m28BKjBDafU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/Simple%20Present%20Tense.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d517199b-7243-4858-9d0a-9e2b333f8b91
updated: 1486069632
title: Simple Present Tense
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/m28BKjBDafU/default.jpg
    - https://i3.ytimg.com/vi/m28BKjBDafU/1.jpg
    - https://i3.ytimg.com/vi/m28BKjBDafU/2.jpg
    - https://i3.ytimg.com/vi/m28BKjBDafU/3.jpg
---
