---
version: 1
type: video
provider: YouTube
id: SAqGKWz109M
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/RECOMBINATION.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a6056ad2-5d52-4a3b-9407-92875d33194e
updated: 1486069666
title: RECOMBINATION
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/SAqGKWz109M/default.jpg
    - https://i3.ytimg.com/vi/SAqGKWz109M/1.jpg
    - https://i3.ytimg.com/vi/SAqGKWz109M/2.jpg
    - https://i3.ytimg.com/vi/SAqGKWz109M/3.jpg
---
