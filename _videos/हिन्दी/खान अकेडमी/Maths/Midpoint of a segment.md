---
version: 1
type: video
provider: YouTube
id: X3fF4SnlkDU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d8556d01-16bd-4f3e-9f98-44584a8ddf82
updated: 1486069613
title: Midpoint of a segment
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/X3fF4SnlkDU/default.jpg
    - https://i3.ytimg.com/vi/X3fF4SnlkDU/1.jpg
    - https://i3.ytimg.com/vi/X3fF4SnlkDU/2.jpg
    - https://i3.ytimg.com/vi/X3fF4SnlkDU/3.jpg
---
