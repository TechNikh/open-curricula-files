---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Duality_(order_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 2c0ccb2e-c3e1-4855-81c5-c31e0637642d
updated: 1484309236
title: Duality (order theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Duale_Verbaende.svg.png
categories:
    - Duality theories
---
In the mathematical area of order theory, every partially ordered set P gives rise to a dual (or opposite) partially ordered set which is often denoted by Pop or Pd. This dual order Pop is defined to be the set with the inverse order, i.e. x ≤ y holds in Pop if and only if y ≤ x holds in P. It is easy to see that this construction, which can be depicted by flipping the Hasse diagram for P upside down, will indeed yield a partially ordered set. In a broader sense, two posets are also said to be duals if they are dually isomorphic, i.e. if one poset is order isomorphic to the dual of the other.
