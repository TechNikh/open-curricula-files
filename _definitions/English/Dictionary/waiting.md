---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/waiting
offline_file: ""
offline_thumbnail: ""
uuid: 5990d240-5d96-427a-9b11-3a4fa4a506b7
updated: 1484310152
title: waiting
categories:
    - Dictionary
---
waiting
     adj : being and remaining ready and available for use; "waiting
           cars and limousines lined the curb"; "found her mother
           waiting for them"; "an impressive array of food ready
           and waiting for the guests"; "military forces ready and
           waiting" [syn: {ready and waiting(p)}]
     n : the act of waiting (remaining inactive in one place while
         expecting something); "the wait was an ordeal for him"
         [syn: {wait}]
