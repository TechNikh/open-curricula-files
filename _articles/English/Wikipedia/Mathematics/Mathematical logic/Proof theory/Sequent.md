---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sequent
offline_file: ""
offline_thumbnail: ""
uuid: 9c477cca-9a09-48df-ac0c-4c0495564326
updated: 1484309497
title: Sequent
tags:
    - Introduction
    - The form and semantics of sequents
    - Syntax details
    - Properties
    - Effects of inserting and removing propositions
    - Consequences of empty lists of formulas
    - Examples
    - Rules
    - Interpretation
    - History of the meaning of sequent assertions
    - Intuitive meaning
    - Variations
    - Etymology
    - Notes
categories:
    - Proof theory
---
A sequent may have any number m of condition formulas Ai (called "antecedents") and any number n of asserted formulas Bj (called "succedents" or "consequents"). A sequent is understood to mean that if all of the antecedent conditions are true, then at least one of the consequent formulas is true. This style of conditional assertion is almost always associated with the conceptual framework of sequent calculus.
