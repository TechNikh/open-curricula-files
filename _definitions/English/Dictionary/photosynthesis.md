---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/photosynthesis
offline_file: ""
offline_thumbnail: ""
uuid: dfea6819-8fde-48c7-a11c-38322cc503ba
updated: 1484310266
title: photosynthesis
categories:
    - Dictionary
---
photosynthesis
     n : synthesis of compounds with the aid of radiant energy
         (especially in plants)
