---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/larva
offline_file: ""
offline_thumbnail: ""
uuid: eeeef0ba-a584-4c81-9c8f-f9e19587aacb
updated: 1484310160
title: larva
categories:
    - Dictionary
---
larva
     n : the immature free-living form of most invertebrates and
         amphibians and fish which at hatching from the egg is
         fundamentally unlike its parent and must metamorphose
     [also: {larvae} (pl)]
