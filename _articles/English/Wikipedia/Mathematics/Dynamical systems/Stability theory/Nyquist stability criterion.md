---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nyquist_stability_criterion
offline_file: ""
offline_thumbnail: ""
uuid: 1650f46b-c382-4cbf-b391-417b692e1226
updated: 1484308838
title: Nyquist stability criterion
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Nyquist_example.svg.png
tags:
    - Background
    - "Cauchy's argument principle"
    - The Nyquist criterion
    - >
        The Nyquist criterion for systems with poles on the
        imaginary axis
    - Mathematical Derivation
    - Summary
    - Notes
categories:
    - Stability theory
---
In control theory and stability theory, the Nyquist stability criterion, discovered by Swedish-American electrical engineer Harry Nyquist at Bell Telephone Laboratories in 1932,[1] is a graphical technique for determining the stability of a dynamical system. Because it only looks at the Nyquist plot of the open loop systems, it can be applied without explicitly computing the poles and zeros of either the closed-loop or open-loop system (although the number of each type of right-half-plane singularities must be known). As a result, it can be applied to systems defined by non-rational functions, such as systems with delays. In contrast to Bode plots, it can handle transfer functions with ...
