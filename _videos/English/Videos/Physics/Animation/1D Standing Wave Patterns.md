---
version: 1
type: video
provider: YouTube
id: V_KOpEOb1KE
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/1D%20Standing%20Wave%20Patterns.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f1316d33-5b7d-4286-88aa-e01941f91cc6
updated: 1486069653
title: 1D Standing Wave Patterns
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/V_KOpEOb1KE/default.jpg
    - https://i3.ytimg.com/vi/V_KOpEOb1KE/1.jpg
    - https://i3.ytimg.com/vi/V_KOpEOb1KE/2.jpg
    - https://i3.ytimg.com/vi/V_KOpEOb1KE/3.jpg
---
