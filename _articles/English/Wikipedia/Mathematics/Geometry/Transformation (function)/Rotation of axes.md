---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rotation_of_axes
offline_file: ""
offline_thumbnail: ""
uuid: 900f5af7-b071-4875-bb90-ec77c4a17073
updated: 1484309338
title: Rotation of axes
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/320px-Rotation_of_coordinates.svg.png
tags:
    - Motivation
    - Derivation
    - Examples in two dimensions
    - Example 1
    - Example 2
    - Rotation of conic sections
    - Identifying rotated conic sections
    - Generalization to several dimensions
    - Examples in several dimensions
    - Example 3
    - Notes
categories:
    - Transformation (function)
---
In mathematics, a rotation of axes in two dimensions is a mapping from an xy-Cartesian coordinate system to an x'y'-Cartesian coordinate system in which the origin is kept fixed and the x' and y' axes are obtained by rotating the x and y axes counterclockwise through an angle 
  
    
      
        θ
      
    
    {\displaystyle \theta }
  
. A point P has coordinates (x, y) with respect to the original system and coordinates (x', y') with respect to the new system.[1] In the new coordinate system, the point P will appear to have been rotated in the opposite direction, that is, clockwise through the angle 
  
    
      
        θ
      
    
    {\displaystyle \theta }
  
. A rotation ...
