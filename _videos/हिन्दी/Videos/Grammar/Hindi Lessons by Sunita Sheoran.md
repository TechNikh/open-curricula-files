---
version: 1
type: video
provider: YouTube
id: mYCdnhb8qjI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 0823ab9c-5c48-4ee6-ae78-4640f64f1949
updated: 1486069629
title: Hindi Lessons by Sunita Sheoran
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/mYCdnhb8qjI/default.jpg
    - https://i3.ytimg.com/vi/mYCdnhb8qjI/1.jpg
    - https://i3.ytimg.com/vi/mYCdnhb8qjI/2.jpg
    - https://i3.ytimg.com/vi/mYCdnhb8qjI/3.jpg
---
