---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Affine_plane_(incidence_geometry)
offline_file: ""
offline_thumbnail: ""
uuid: efa48773-58ef-4ba6-a1a1-462a969f570b
updated: 1484309251
title: Affine plane (incidence geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Hesse_configuration.svg.png
tags:
    - Finite affine planes
    - Relation with projective planes
    - Affine translation planes
    - 'Generalization: k-nets'
    - 'Example: translation nets'
    - Geometric codes
    - Affine spaces
    - Notes
categories:
    - Incidence geometry
---
In an affine plane, two lines are called parallel if they are equal or disjoint. Using this definition, Playfair's axiom above can be replaced by:[2]
