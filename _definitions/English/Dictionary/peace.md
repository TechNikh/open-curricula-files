---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peace
offline_file: ""
offline_thumbnail: ""
uuid: dc506d6c-1b5b-4d78-bbc5-28296b07d9a5
updated: 1484310413
title: peace
categories:
    - Dictionary
---
peace
     n 1: the state prevailing during the absence of war [ant: {war}]
     2: harmonious relations; freedom from disputes; "the roommates
        lived in peace together"
     3: the absence of mental stress or anxiety [syn: {peacefulness},
         {peace of mind}, {repose}, {serenity}, {heartsease}, {ataraxis}]
     4: the general security of public places; "he was arrested for
        disturbing the peace" [syn: {public security}]
     5: a treaty to cease hostilities; "peace came on November 11th"
        [syn: {peace treaty}, {pacification}]
