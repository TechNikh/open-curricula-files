---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Hilbert%27s_arithmetic_of_ends'
offline_file: ""
offline_thumbnail: ""
uuid: 7fe0da49-0509-4d07-bae8-2e054217992c
updated: 1484309153
title: "Hilbert's arithmetic of ends"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Theo3reflection.png
tags:
    - Definitions
    - Ends
    - Addition
    - Multiplication
    - Rigid motions
categories:
    - Algebraic geometry
---
In mathematics, specifically in the area of hyperbolic geometry, Hilbert's arithmetic of ends is a method for endowing a geometric set, the set of ideal points or "ends" of a hyperbolic plane, with an algebraic structure as a field. It was introduced by German mathematician David Hilbert.[1]
