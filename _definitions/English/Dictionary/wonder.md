---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wonder
offline_file: ""
offline_thumbnail: ""
uuid: 9bfcca48-2113-43ff-8ab8-e689e004c590
updated: 1484310419
title: wonder
categories:
    - Dictionary
---
wonder
     n 1: the feeling aroused by something strange and surprising
          [syn: {wonderment}, {admiration}]
     2: something that causes feelings of wonder; "the wonders of
        modern science" [syn: {marvel}]
     3: a state in which you want to learn more about something
        [syn: {curiosity}]
     v 1: have a wish or desire to know something; "He wondered who
          had built this beautiful church" [syn: {inquire}, {enquire}]
     2: place in doubt or express doubtful speculation; "I wonder
        whether this was the right thing to do"; "she wondered
        whether it would snow tonight" [syn: {question}]
     3: be amazed at; "We marvelled at the child's ...
