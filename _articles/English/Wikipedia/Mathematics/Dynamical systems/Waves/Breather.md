---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Breather
offline_file: ""
offline_thumbnail: ""
uuid: 48775b21-9d8e-4c7c-b6e1-be8fa54e1cf0
updated: 1484308887
title: Breather
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Breather-surface.png
tags:
    - Overview
    - Example of a breather solution for the sine-Gordon equation
    - >
        Example of a breather solution for the nonlinear
        Schrödinger equation
    - References and notes
categories:
    - Waves
---
In physics, a breather is a nonlinear wave in which energy concentrates in a localized and oscillatory fashion. This contradicts with the expectations derived from the corresponding linear system for infinitesimal amplitudes, which tends towards an even distribution of initially localized energy.
