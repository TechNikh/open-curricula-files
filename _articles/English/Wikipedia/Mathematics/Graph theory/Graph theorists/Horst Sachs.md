---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Horst_Sachs
offline_file: ""
offline_thumbnail: ""
uuid: 448f659a-1392-4d0b-83e2-ac3d9703cfb4
updated: 1484309475
title: Horst Sachs
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Horst_Sachs.jpg
categories:
    - Graph theorists
---
He earned the degree of Doctor of Science (Dr. rer. nat.) from the Martin-Luther-Universität Halle-Wittenberg in 1958.[2] Following his retirement in 1992, he was professor emeritus at the Institute of Mathematics of the Technische Universität Ilmenau.[3]
