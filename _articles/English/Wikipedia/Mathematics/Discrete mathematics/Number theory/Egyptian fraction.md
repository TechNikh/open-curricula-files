---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Egyptian_fraction
offline_file: ""
offline_thumbnail: ""
uuid: d34d1ca0-471c-4336-bcaa-3c53af99354e
updated: 1484308639
title: Egyptian fraction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Egypt.svg.png
tags:
    - Motivating applications
    - Early history
    - Notation
    - Calculation methods
    - Later usage
    - Modern number theory
    - Open problems
    - Notes
categories:
    - Number theory
---
An Egyptian fraction is a finite sum of distinct unit fractions, such as 
  
    
      
        
          
            
              1
              2
            
          
        
        +
        
          
            
              1
              3
            
          
        
        +
        
          
            
              1
              16
            
          
        
      
    
    {\displaystyle {\tfrac {1}{2}}+{\tfrac {1}{3}}+{\tfrac {1}{16}}}
  
. That is, each fraction in the expression has a numerator equal to 1 and a denominator that is a positive integer, and all the denominators differ from each other. The value of an expression of this type is a ...
