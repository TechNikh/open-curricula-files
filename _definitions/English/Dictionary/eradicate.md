---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eradicate
offline_file: ""
offline_thumbnail: ""
uuid: fc180f1c-172e-4d5d-9aeb-13950e72f269
updated: 1484310163
title: eradicate
categories:
    - Dictionary
---
eradicate
     v 1: kill in large numbers; "the plague wiped out an entire
          population" [syn: {eliminate}, {annihilate}, {extinguish},
           {wipe out}, {decimate}, {carry off}]
     2: destroy completely, as if down to the roots; "the vestiges
        of political democracy were soon uprooted" [syn: {uproot},
         {extirpate}, {exterminate}]
