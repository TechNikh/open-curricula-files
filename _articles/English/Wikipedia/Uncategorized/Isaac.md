---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isaac
offline_file: ""
offline_thumbnail: ""
uuid: 16d2147e-e7fb-428d-9285-c01211ee9265
updated: 1484308409
title: Isaac
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Beit_alfa02.jpg
tags:
    - Etymology
    - Genesis narrative
    - Birth
    - Binding
    - Family life
    - Migration
    - Birthright
    - Family tree
    - Burial site
    - Jewish views
    - Christian views
    - New Testament
    - Islamic views
    - "Qur'an"
    - Academic
    - Documentary hypothesis
    - In art
    - Notes
    - Citations
categories:
    - Uncategorized
---
Isaac (/ˈaɪzək/;[1] Hebrew: יִצְחָק, Modern Yitshak, Tiberian Yiṣḥāq, ISO 259-3 Yiçḥaq, "[he] will laugh"; Ancient Greek: Ἰσαάκ Isaak; Arabic: إسحاق‎‎ or إسحٰق[a] ʼIsḥāq) as described in the Hebrew Bible and the Qur'an, was the second son of Abraham, the only son Abraham had with his wife Sarah, and the father of Jacob and Esau. According to the Book of Genesis, Abraham was 100 years old when Isaac was born, and Sarah was past 90.
