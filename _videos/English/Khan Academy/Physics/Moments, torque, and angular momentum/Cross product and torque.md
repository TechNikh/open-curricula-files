---
version: 1
type: video
provider: YouTube
id: s38l6nmTrvM
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Moments%2C%20torque%2C%20and%20angular%20momentum/Cross%20product%20and%20torque.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 8751b884-f0ef-4f16-bfe7-992ab7afe383
updated: 1486069595
title: Cross product and torque
categories:
    - Moments, torque, and angular momentum
thumbnail_urls:
    - https://i3.ytimg.com/vi/s38l6nmTrvM/default.jpg
    - https://i3.ytimg.com/vi/s38l6nmTrvM/1.jpg
    - https://i3.ytimg.com/vi/s38l6nmTrvM/2.jpg
    - https://i3.ytimg.com/vi/s38l6nmTrvM/3.jpg
---
The cross product and the direction of torque. Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/oscillatory-motion/harmonic-motion/v/introduction-to-harmonic-motion?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/torque-angular-momentum/torque-tutorial/v/angular-momentum-of-an-extended-object?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
