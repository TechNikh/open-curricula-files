---
version: 1
type: article
id: https://en.wikipedia.org/wiki/47_(number)
offline_file: ""
offline_thumbnail: ""
uuid: f95c1018-652d-46a4-a9d0-a6b3800ce1a3
updated: 1484308952
title: 47 (number)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/180px-47_%2528Pro_Era%2529.svg.png'
tags:
    - In mathematics
    - In science
    - Astronomy
    - In popular culture
    - Calendar years
    - Other
categories:
    - Integers
---
