---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/car
offline_file: ""
offline_thumbnail: ""
uuid: 54fde954-97a9-495b-89d2-2d5a19e93497
updated: 1484310240
title: car
categories:
    - Dictionary
---
car
     n 1: 4-wheeled motor vehicle; usually propelled by an internal
          combustion engine; "he needs a car to get to work" [syn:
           {auto}, {automobile}, {machine}, {motorcar}]
     2: a wheeled vehicle adapted to the rails of railroad; "three
        cars had jumped the rails" [syn: {railcar}, {railway car},
         {railroad car}]
     3: a conveyance for passengers or freight on a cable railway;
        "they took a cable car to the top of the mountain" [syn: {cable
        car}]
     4: car suspended from an airship and carrying personnel and
        cargo and power plant [syn: {gondola}]
     5: where passengers ride up and down; "the car was on the top
        ...
