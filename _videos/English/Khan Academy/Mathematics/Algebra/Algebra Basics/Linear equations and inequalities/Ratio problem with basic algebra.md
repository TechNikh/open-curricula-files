---
version: 1
type: video
provider: YouTube
id: ITVQrzDSekU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20Basics/Linear%20equations%20and%20inequalities/Ratio%20problem%20with%20basic%20algebra.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ed47daf0-11ec-426e-a859-d1f4c1bc6002
updated: 1486069573
title: Ratio problem with basic algebra
categories:
    - Linear equations and inequalities
thumbnail_urls:
    - https://i3.ytimg.com/vi/ITVQrzDSekU/default.jpg
    - https://i3.ytimg.com/vi/ITVQrzDSekU/1.jpg
    - https://i3.ytimg.com/vi/ITVQrzDSekU/2.jpg
    - https://i3.ytimg.com/vi/ITVQrzDSekU/3.jpg
---
A slightly more involved ratio problem with algebra. Created by Sal Khan.

Practice this yourself on Khan Academy right now: https://www.khanacademy.org/math/algebra-basics/core-algebra-linear-equations-inequalities/ratios-core-algebra/e/constructing-proportions-to-solve-application-problems?utm_source=YT&utm_medium=Desc&utm_campaign=algebrabasics

Watch the next lesson: https://www.khanacademy.org/math/algebra-basics/core-algebra-linear-equations-inequalities/core-algebra-direct-inverse-variation/v/direct-and-inverse-variation?utm_source=YT&utm_medium=Desc&utm_campaign=algebrabasics

Missed the previous lesson? https://www.khanacademy.org/math/algebra-basics/core-algebra-linear-equations-inequalities/ratios-core-algebra/v/constructing-proportions-to-solve-application-problems?utm_source=YT&utm_medium=Desc&utm_campaign=algebrabasics

Algebra basics on Khan Academy: Topics covered in a traditional college level introductory macroeconomics course

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Macroeconomics channel: https://www.youtube.com/channel/UCBytY7pnP0GAHB3C8vDeXvg
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
