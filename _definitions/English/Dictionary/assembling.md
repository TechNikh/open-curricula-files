---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assembling
offline_file: ""
offline_thumbnail: ""
uuid: c2db5b10-e1ea-45be-a09b-5e221c5339c2
updated: 1484310531
title: assembling
categories:
    - Dictionary
---
assembling
     n : the act of gathering something together [syn: {collection},
         {collecting}, {aggregation}]
