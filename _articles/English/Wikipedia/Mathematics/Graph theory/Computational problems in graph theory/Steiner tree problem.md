---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Steiner_tree_problem
offline_file: ""
offline_thumbnail: ""
uuid: da9d323b-1332-4117-bc79-e832c91b328c
updated: 1484309396
title: Steiner tree problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Steiner_3_points.svg.png
tags:
    - Euclidean Steiner tree
    - Rectilinear Steiner tree
    - Generalization of minimum Steiner tree
    - Approximating the Steiner tree
    - Steiner ratio
    - Notes
categories:
    - Computational problems in graph theory
---
The Steiner tree problem, motorway problem, or minimum Steiner tree problem, named after Jakob Steiner, is a problem in combinatorial optimization, which may be formulated in a number of settings, with the common part being that it is required to find the shortest interconnect for a given set of objects.
