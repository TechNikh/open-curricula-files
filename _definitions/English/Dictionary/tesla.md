---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tesla
offline_file: ""
offline_thumbnail: ""
uuid: b838d52c-a3fb-4be5-b1df-83ede8186f98
updated: 1484310194
title: tesla
categories:
    - Dictionary
---
tesla
     n 1: a unit of magnetic flux density equal to one weber per
          square meter
     2: United States electrical engineer and inventor (born in
        Croatia but of Serbian descent) who discovered the
        principles of alternating currents and developed the first
        alternating-current induction motor and the Tesla coil and
        several forms of oscillators (1856-1943) [syn: {Nikola
        Tesla}]
