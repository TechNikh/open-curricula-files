---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tropical_cyclone
offline_file: ""
offline_thumbnail: ""
uuid: 4667b130-961d-4b73-84ec-c2bec9ed81c0
updated: 1484308881
title: Tropical cyclone
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Hurricane_Isabel_from_ISS_0.jpg
tags:
    - Physical structure
    - Wind field
    - Eye and center
    - Intensity
    - Size
    - Physics and energetics
    - 'Secondary circulation: a Carnot heat engine'
    - 'Primary circulation: rotating winds'
    - Maximum potential intensity
    - Derivation
    - Characteristic values and variability on Earth
    - Interaction with the upper ocean
    - Major basins and related warning centers
    - Formation
    - Times
    - Factors
    - Locations
    - Movement
    - Environmental steering
    - Beta drift
    - Multiple storm interaction
    - Interaction with the mid-latitude westerlies
    - Landfall
    - Dissipation
    - Factors
    - Artificial dissipation
    - Effects
    - Observation and forecasting
    - Observation
    - Forecasting
    - Classifications, terminology, and naming
    - Intensity classifications
    - Tropical depression
    - Tropical storm
    - Hurricane or typhoon
    - Origin of storm terms
    - Naming
    - Notable tropical cyclones
    - Changes caused by El Niño-Southern Oscillation
    - Long-term activity trends
    - Global warming
    - Related cyclone types
    - In popular culture
categories:
    - Vortices
---
A tropical cyclone is a rapidly rotating storm system characterized by a low-pressure center, a closed low-level atmospheric circulation, strong winds, and a spiral arrangement of thunderstorms that produce heavy rain. Depending on its location and strength, a tropical cyclone is referred to by names such as hurricane (/ˈhʌrᵻkən/ or /ˈhʌrᵻkeɪn/[1][2][3]), typhoon /taɪˈfuːn/, tropical storm, cyclonic storm, tropical depression, and simply cyclone.[4]
