---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/das
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484506922
title: das
categories:
    - Dictionary
---
das
     n : any of several small ungulate mammals of Africa and Asia
         with rodent-like incisors and feet with hooflike toes
         [syn: {hyrax}, {coney}, {cony}, {dassie}]
