---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Compartmental_modelling_of_dendrites
offline_file: ""
offline_thumbnail: ""
uuid: 88161f76-5c26-47c1-947d-26fc5afea752
updated: 1484308808
title: Compartmental modelling of dendrites
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Dendrites_Modelling.jpg
tags:
    - Introduction
    - Multiple compartments
    - >
        Increased computational accuracy in multi-compartmental
        cable models
    - Cable Theory
    - Some applications
    - Information processing
    - Midbrain dopaminergic neuron
    - Mode locking
    - Compartmental neural simulations with spatial adaptivity
    - Action potential (AP) initiation site
    - A finite-state automaton model
    - Constraining compartmental models
    - Multi-compartmental model of a CA1 pyramidal cell
    - Electrical compartmentalization
    - Robust coding in motion-sensitive neurons
    - Conductance-based neuron models
categories:
    - Nonlinear systems
---
Compartmental modelling of dendrites deals with multi-compartment modelling of the dendrites, to make the understanding of the electrical behavior of complex dendrites easier. Basically, compartmental modelling of dendrites is a very helpful tool to develop new biological neuron models. Dendrites are very important because they occupy the most membrane area in many of the neurons and give the neuron an ability to connect to thousands of other cells. Originally the dendrites were thought to have constant conductance and current but now it has been understood that they may have active Voltage-gated ion channels, which influences the firing properties of the neuron and also the response of ...
