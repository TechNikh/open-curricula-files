---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/launching
offline_file: ""
offline_thumbnail: ""
uuid: 0122e90d-2c2f-4826-add6-cb88e3c6f440
updated: 1484310242
title: launching
categories:
    - Dictionary
---
launching
     n 1: the act of moving a newly-built vessel into the water for
          the first time
     2: the act of beginning something new; "they looked forward to
        the debut of their new product line" [syn: {introduction},
         {debut}, {first appearance}, {unveiling}, {entry}]
     3: the act of propelling with force [syn: {launch}]
