---
version: 1
type: video
provider: YouTube
id: NU2r-ECmPr4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 19fe45da-4675-4d1f-94a0-8846201c7e51
updated: 1486069639
title: "Newton's fiddly prism experiment"
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/NU2r-ECmPr4/default.jpg
    - https://i3.ytimg.com/vi/NU2r-ECmPr4/1.jpg
    - https://i3.ytimg.com/vi/NU2r-ECmPr4/2.jpg
    - https://i3.ytimg.com/vi/NU2r-ECmPr4/3.jpg
---
