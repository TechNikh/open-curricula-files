---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/colonel
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484422261
title: colonel
categories:
    - Dictionary
---
colonel
     n : a commissioned military officer in the United States Army or
         Air Force or Marines who ranks above a lieutenant colonel
         and below a brigadier general
