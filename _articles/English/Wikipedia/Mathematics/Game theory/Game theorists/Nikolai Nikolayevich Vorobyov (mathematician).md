---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Nikolai_Nikolayevich_Vorobyov_(mathematician)
offline_file: ""
offline_thumbnail: ""
uuid: 222cd6a7-7f63-474d-809d-cc6a58af336c
updated: 1484309029
title: Nikolai Nikolayevich Vorobyov (mathematician)
categories:
    - Game theorists
---
Nikolai Nikolayevich Vorobyov (also Vorobiev) (Russian: Николай Николаевич Воробьёв, 18 September 1925, Leningrad — July 14, 1995) was a Soviet and Russian mathematician, an expert in the field of abstract algebra, mathematical logic and probability theory, the founder of the Soviet school of game theory. He is an author of two textbooks, three monographs, a large number of mathematical articles and a number of popular science books. He supervised over 30 kandidat and doktor dissertations.[1]
