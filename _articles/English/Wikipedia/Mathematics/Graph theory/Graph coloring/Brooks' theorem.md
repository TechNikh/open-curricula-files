---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Brooks%27_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 2c86a779-8ed8-43ca-8168-1e89d27c6cd7
updated: 1484309376
title: "Brooks' theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/179px-Graph_exact_coloring.svg.png
tags:
    - Formal statement
    - Proof
    - Extensions
    - Algorithms
    - Notes
categories:
    - Graph coloring
---
In graph theory, Brooks' theorem states a relationship between the maximum degree of a graph and its chromatic number. According to the theorem, in a connected graph in which every vertex has at most Δ neighbors, the vertices can be colored with only Δ colors, except for two cases, complete graphs and cycle graphs of odd length, which require Δ + 1 colors.
