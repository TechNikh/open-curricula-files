---
version: 1
type: video
provider: YouTube
id: 38mJxHSegRc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Team%20%28Motivation%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2249ee3a-80df-40a6-8e75-f4a2aa498014
updated: 1486069653
title: Team (Motivation)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/38mJxHSegRc/default.jpg
    - https://i3.ytimg.com/vi/38mJxHSegRc/1.jpg
    - https://i3.ytimg.com/vi/38mJxHSegRc/2.jpg
    - https://i3.ytimg.com/vi/38mJxHSegRc/3.jpg
---
