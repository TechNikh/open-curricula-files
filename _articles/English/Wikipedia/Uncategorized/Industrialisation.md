---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Industrialization
offline_file: ""
offline_thumbnail: ""
uuid: b3bc5fb3-97fb-41ff-bbc3-f67507b2278a
updated: 1484308337
title: Industrialisation
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Maddison_GDP_per_capita_1500-1950.svg.png
tags:
    - Background
    - Social consequences
    - Urbanisation
    - Exploitation
    - Changes in family structure
    - Current situation
categories:
    - Uncategorized
---
Industrialisation or industrialization is the period of social and economic change that transforms a human group from an agrarian society into an industrial one, involving the extensive re-organisation of an economy for the purpose of manufacturing.[2]
