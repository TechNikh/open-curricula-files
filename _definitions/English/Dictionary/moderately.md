---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/moderately
offline_file: ""
offline_thumbnail: ""
uuid: 529ad419-23b5-440f-b046-d9899fe4a560
updated: 1484310407
title: moderately
categories:
    - Dictionary
---
moderately
     adv 1: to a moderately sufficient extent or degree; "the shoes are
            priced reasonably"; "he is fairly clever with
            computers"; "they lived comfortably within reason"
            [syn: {reasonably}, {within reason}, {somewhat}, {fairly},
             {middling}, {passably}] [ant: {unreasonably}, {unreasonably}]
     2: in a moderate manner; "he drinks moderately" [syn: {with
        moderation}] [ant: {immoderately}]
