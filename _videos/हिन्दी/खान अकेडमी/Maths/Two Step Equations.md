---
version: 1
type: video
provider: YouTube
id: nQlz9NOK-ts
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: db258916-5c0e-4199-a73f-79a1bb713c9e
updated: 1486069617
title: Two Step Equations
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/nQlz9NOK-ts/default.jpg
    - https://i3.ytimg.com/vi/nQlz9NOK-ts/1.jpg
    - https://i3.ytimg.com/vi/nQlz9NOK-ts/2.jpg
    - https://i3.ytimg.com/vi/nQlz9NOK-ts/3.jpg
---
