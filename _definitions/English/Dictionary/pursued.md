---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pursued
offline_file: ""
offline_thumbnail: ""
uuid: 27bd6e59-2d9b-4eee-b4f6-ac8df5d4cda8
updated: 1484310416
title: pursued
categories:
    - Dictionary
---
pursued
     adj : followed with enmity as if to harm; "running and leaping
           like a herd of pursued antelopes"
     n : a person who is being chased; "the film jumped back and
         forth from the pursuer to the pursued" [syn: {chased}]
