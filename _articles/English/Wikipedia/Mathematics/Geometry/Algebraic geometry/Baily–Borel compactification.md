---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Baily%E2%80%93Borel_compactification'
offline_file: ""
offline_thumbnail: ""
uuid: ccccbe1d-67de-4d14-8be7-0ca1375ed150
updated: 1484309134
title: Baily–Borel compactification
categories:
    - Algebraic geometry
---
In mathematics, the Baily–Borel compactification is a compactification of a quotient of a Hermitian symmetric space by an arithmetic group, introduced by W.L. Baily and A. Borel (1964, 1966).
