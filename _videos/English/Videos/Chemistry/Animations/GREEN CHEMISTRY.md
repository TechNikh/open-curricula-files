---
version: 1
type: video
provider: YouTube
id: S_6JazDjSJ8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/GREEN%20CHEMISTRY.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 82280a19-c704-4c2c-857f-c2c101b26eca
updated: 1486069662
title: GREEN CHEMISTRY
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/S_6JazDjSJ8/default.jpg
    - https://i3.ytimg.com/vi/S_6JazDjSJ8/1.jpg
    - https://i3.ytimg.com/vi/S_6JazDjSJ8/2.jpg
    - https://i3.ytimg.com/vi/S_6JazDjSJ8/3.jpg
---
