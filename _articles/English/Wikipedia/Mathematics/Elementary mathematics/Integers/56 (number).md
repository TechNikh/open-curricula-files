---
version: 1
type: article
id: https://en.wikipedia.org/wiki/56_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 2ebae1be-eb08-4b47-a5ef-3c0957bd35df
updated: 1484308961
title: 56 (number)
tags:
    - Mathematics
    - Science, technology, and biology
    - Astronomy
    - Music
    - Television and film
    - Sports
    - Organizations
    - people
    - geography
    - Archaeology
    - Cosmogony
    - History
    - Occultism
categories:
    - Integers
---
