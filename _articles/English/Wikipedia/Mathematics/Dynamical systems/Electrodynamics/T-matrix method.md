---
version: 1
type: article
id: https://en.wikipedia.org/wiki/T-matrix_method
offline_file: ""
offline_thumbnail: ""
uuid: a97e2ba5-3b69-4c71-a280-f093f8196c56
updated: 1484308762
title: T-matrix method
categories:
    - Electrodynamics
---
The T-matrix method is a computational technique of light scattering by nonspherical particles originally formulated by P. C. Waterman (1928-2012) in 1965.[1] The technique is also known as null field method and extended boundary technique method (EBCM). [2] In the method, matrix elements are obtained by matching boundary conditions for solutions of Maxwell equations.
