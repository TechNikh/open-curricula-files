---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Maximum_entropy_probability_distribution
offline_file: ""
offline_thumbnail: ""
uuid: 0ed64caa-408f-460b-b4c2-1f1a9459803b
updated: 1484308770
title: Maximum entropy probability distribution
tags:
    - Definition of entropy and differential entropy
    - Distributions with measured constants
    - Continuous version
    - Discrete version
    - Proof
    - Caveats
    - Examples
    - Uniform and piecewise uniform distributions
    - 'Positive and specified mean: the exponential distribution'
    - 'Specified variance: the normal distribution'
    - Discrete distributions with specified mean
    - Circular random variables
    - Maximizer for specified mean, variance and skew
    - Other examples
    - Notes
categories:
    - Entropy and information
---
In statistics and information theory, a maximum entropy probability distribution has entropy that is at least as great as that of all other members of a specified class of probability distributions. According to the principle of maximum entropy, if nothing is known about a distribution except that it belongs to a certain class (usually defined in terms of specified properties or measures), then the distribution with the largest entropy should be chosen as the least-informative default. The motivation is twofold: first, maximizing entropy minimizes the amount of prior information built into the distribution; second, many physical systems tend to move towards maximal entropy configurations ...
