---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fatal
offline_file: ""
offline_thumbnail: ""
uuid: fbb17b2e-b15b-498b-a927-63f31e2c7ee0
updated: 1484310202
title: fatal
categories:
    - Dictionary
---
fatal
     adj 1: bringing death [ant: {nonfatal}]
     2: having momentous consequences; of decisive importance; "that
        fateful meeting of the U.N. when...it declared war on
        North Korea"- Saturday Rev; "the fatal day of the election
        finally arrived" [syn: {fateful}]
     3: (of events) having extremely unfortunate or dire
        consequences; bringing ruin; "the stock market crashed on
        Black Friday"; "a calamitous defeat"; "the battle was a
        disastrous end to a disastrous campaign"; "such doctrines,
        if true, would be absolutely fatal to my theory"- Charles
        Darwin; "it is fatal to enter any war without the will to
        win it"- ...
