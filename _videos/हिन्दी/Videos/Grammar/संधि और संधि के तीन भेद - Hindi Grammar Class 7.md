---
version: 1
type: video
provider: YouTube
id: QEjStUi4mhc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 8c87507f-07ff-4116-8c1e-2dfaf94c4c17
updated: 1486069630
title: >
    संधि और संधि के तीन भेद
    | Hindi Grammar Class 7
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/QEjStUi4mhc/default.jpg
    - https://i3.ytimg.com/vi/QEjStUi4mhc/1.jpg
    - https://i3.ytimg.com/vi/QEjStUi4mhc/2.jpg
    - https://i3.ytimg.com/vi/QEjStUi4mhc/3.jpg
---
