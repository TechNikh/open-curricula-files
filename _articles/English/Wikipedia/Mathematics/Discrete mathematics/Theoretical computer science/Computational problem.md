---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computational_problem
offline_file: ""
offline_thumbnail: ""
uuid: 49229141-e05c-457f-913e-68381016eca1
updated: 1484308671
title: Computational problem
categories:
    - Theoretical computer science
---
In theoretical computer science, a computational problem is a mathematical object representing a collection of questions that computers might be able to solve. For example, the problem of factoring
