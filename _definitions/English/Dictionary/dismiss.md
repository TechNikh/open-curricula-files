---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dismiss
offline_file: ""
offline_thumbnail: ""
uuid: 3a660b3c-8a62-491f-9ed5-f3a6e0e79a53
updated: 1484310595
title: dismiss
categories:
    - Dictionary
---
dismiss
     v 1: bar from attention or consideration; "She dismissed his
          advances" [syn: {disregard}, {brush aside}, {brush off},
           {discount}, {push aside}, {ignore}]
     2: cease to consider; put out of judicial consideration; "This
        case is dismissed!" [syn: {throw out}]
     3: stop associating with; "They dropped her after she had a
        child out of wedlock" [syn: {send packing}, {send away}, {drop}]
     4: terminate the employment of; "The boss fired his secretary
        today"; "The company terminated 25% of its workers" [syn:
        {fire}, {give notice}, {can}, {give the axe}, {send away},
         {sack}, {force out}, {give the sack}, ...
