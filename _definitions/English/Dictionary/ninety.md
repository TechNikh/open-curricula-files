---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ninety
offline_file: ""
offline_thumbnail: ""
uuid: 1dedc280-e281-48f3-b6a9-d5bc689c452c
updated: 1484310393
title: ninety
categories:
    - Dictionary
---
ninety
     adj : being ten more than eighty [syn: {90}, {xc}]
     n : the cardinal number that is the product of ten and nine
         [syn: {90}, {XC}]
