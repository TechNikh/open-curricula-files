---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Stearic
offline_file: ""
offline_thumbnail: ""
uuid: a48427c8-3c37-431a-aba1-8945388ec414
updated: 1484308405
title: Stearic acid
tags:
    - Production
    - Uses
    - Soaps, cosmetics, detergents
    - Lubricants, softening and release agents
    - Niche uses
    - Metabolism
categories:
    - Uncategorized
---
Stearic acid (/ˈstɪərɪk/ STEER-ik, /ˈstɪ.æ.rɪk/ stee-ARR-ik) is a saturated fatty acid with an 18-carbon chain and has the IUPAC name octadecanoic acid. It is a waxy solid and its chemical formula is C17H35CO2H. Its name comes from the Greek word στέαρ "stéar", which means tallow. The salts and esters of stearic acid are called stearates. As its ester, stearic acid is one of the most common saturated fatty acids found in nature following palmitic acid.[9] The triglyceride derived from three molecules of stearic acid is called stearin.
