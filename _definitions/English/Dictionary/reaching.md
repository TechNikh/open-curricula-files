---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reaching
offline_file: ""
offline_thumbnail: ""
uuid: e8afb43c-b274-415a-b4fe-9e79b1ae5e51
updated: 1484310313
title: reaching
categories:
    - Dictionary
---
reaching
     n 1: the act of physically reaching or thrusting out [syn: {reach},
           {stretch}]
     2: accomplishment of an objective [syn: {arrival}]
