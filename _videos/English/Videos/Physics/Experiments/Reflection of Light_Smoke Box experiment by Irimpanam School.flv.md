---
version: 1
type: video
provider: YouTube
id: 8q-oXhqIuIU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1fb4a885-4625-4cdb-9745-889255967a50
updated: 1486069640
title: >
    Reflection of Light_Smoke Box experiment by Irimpanam
    School.flv
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/8q-oXhqIuIU/default.jpg
    - https://i3.ytimg.com/vi/8q-oXhqIuIU/1.jpg
    - https://i3.ytimg.com/vi/8q-oXhqIuIU/2.jpg
    - https://i3.ytimg.com/vi/8q-oXhqIuIU/3.jpg
---
