---
version: 1
type: video
provider: YouTube
id: Orumw-PyNjw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Crash%20Course/The%20Integumentary%20System%2C%20Part%201%20-%20Skin%20Deep-%20Crash%20Course%20A%26P%20%236.webm"
offline_file: ""
offline_thumbnail: ""
uuid: add333ec-ad8a-4973-864c-3a6ed96c7deb
updated: 1486069681
title: 'The Integumentary System, Part 1 - Skin Deep- Crash Course A&P #6'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/Orumw-PyNjw/default.jpg
    - https://i3.ytimg.com/vi/Orumw-PyNjw/1.jpg
    - https://i3.ytimg.com/vi/Orumw-PyNjw/2.jpg
    - https://i3.ytimg.com/vi/Orumw-PyNjw/3.jpg
---
