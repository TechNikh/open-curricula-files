---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/edge
offline_file: ""
offline_thumbnail: ""
uuid: 205a676d-b34d-439c-af0c-4458d01dfa07
updated: 1484310226
title: edge
categories:
    - Dictionary
---
edge
     n 1: the boundary of a surface [syn: {border}]
     2: a sharp side formed by the intersection of two surfaces of
        an object; "he rounded the edges of the box"
     3: a line determining the limits of an area [syn: {boundary}, {bound}]
     4: the attribute of urgency; "his voice had an edge to it"
        [syn: {sharpness}]
     5: a slight competitive advantage; "he had an edge on the
        competition"
     6: a strip near the boundary of an object; "he jotted a note on
        the margin of the page" [syn: {margin}]
     v 1: advance slowly, as if by inches; "He edged towards the car"
          [syn: {inch}]
     2: provide with a border or edge; "edge the tablecloth ...
