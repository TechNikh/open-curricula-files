---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Thomas%27_cyclically_symmetric_attractor'
offline_file: ""
offline_thumbnail: ""
uuid: dec36abb-87e9-406f-91b0-86b10e028c74
updated: 1484308823
title: "Thomas' cyclically symmetric attractor"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Thomas%2527_cyclically_symmetric_attractor.png'
categories:
    - Nonlinear systems
---
In the dynamical systems theory, Thomas' cyclically symmetric attractor is a 3D strange attractor originally proposed by René Thomas.[1] It has a simple form which is cyclically symmetric in the x,y, and z variables and can be viewed as the trajectory of a frictionally dampened particle moving in a 3D lattice of forces.[2] The simple form has made it a popular example.
