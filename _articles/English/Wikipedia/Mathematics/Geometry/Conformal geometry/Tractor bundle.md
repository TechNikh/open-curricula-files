---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tractor_bundle
offline_file: ""
offline_thumbnail: ""
uuid: 60af2acc-f359-4a9b-b819-e2922009c576
updated: 1484309187
title: Tractor bundle
categories:
    - Conformal geometry
---
In conformal geometry, the tractor bundle is a particular vector bundle constructed on a conformal manifold whose fibres form an effective representation of the conformal group (see associated bundle).
