---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vance_Faber
offline_file: ""
offline_thumbnail: ""
uuid: d2ff3ae8-8c9b-4a88-b4d4-9879bfea2db9
updated: 1484309461
title: Vance Faber
categories:
    - Graph theorists
---
Vance Faber (born December 1, 1944 in Buffalo, New York) is a mathematician, known for his work in combinatorics, applied linear algebra and image processing.
