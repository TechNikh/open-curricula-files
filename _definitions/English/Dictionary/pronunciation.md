---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pronunciation
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484492821
title: pronunciation
categories:
    - Dictionary
---
pronunciation
     n 1: the manner in which someone utters a word; "they are always
          correcting my pronunciation"
     2: the way a word or a language is customarily  spoken; "the
        pronunciation of Chinese is difficult for foreigners";
        "that is the correct pronunciation" [syn: {orthoepy}]
