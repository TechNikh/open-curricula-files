---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unpopular
offline_file: ""
offline_thumbnail: ""
uuid: 9f07afb9-778b-4679-995e-e38bab22a72c
updated: 1484310611
title: unpopular
categories:
    - Dictionary
---
unpopular
     adj : regarded with disfavor or lacking general approval;
           "unpopular ideas"; "an unpopular war" [ant: {popular}]
