---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/greasy
offline_file: ""
offline_thumbnail: ""
uuid: 8c801d66-d95b-4921-980d-c21ac4f5878e
updated: 1484310426
title: greasy
categories:
    - Dictionary
---
greasy
     adj 1: containing an unusual amount of grease or oil; "greasy
            hamburgers"; "oily fried potatoes"; "oleaginous seeds"
            [syn: {oily}, {sebaceous}, {oleaginous}]
     2: smeared or soiled with grease or oil; "greasy coveralls";
        "get rid of rubbish and oily rags" [syn: {oily}]
     [also: {greasiest}, {greasier}]
