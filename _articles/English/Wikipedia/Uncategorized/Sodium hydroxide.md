---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Naoh
offline_file: ""
offline_thumbnail: ""
uuid: 29754205-98cc-49d4-af80-1a67a3cf802a
updated: 1484308317
title: Sodium hydroxide
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Lye.jpg
tags:
    - Properties
    - Physical properties
    - Chemical properties
    - Reaction with acids
    - Reaction with acidic oxides
    - Reaction with amphoteric metals and oxides
    - Precipitant
    - Saponification
    - Production
    - Uses
    - Chemical pulping
    - Tissue digestion
    - Dissolving amphoteric metals and compounds
    - Esterification and transesterification reagent
    - Food preparation
    - Cleaning agent
    - Water treatment
    - Historical uses
    - Experimental
    - Safety
    - History
    - Commercial brands
    - Bibliography
categories:
    - Uncategorized
---
Sodium hydroxide (NaOH), also known as lye and caustic soda,[1][2] is an inorganic compound. It is a white solid and highly caustic metallic base and alkali of sodium which is available in pellets, flakes, granules, and as prepared solutions at different concentrations.[10] Sodium hydroxide forms an approximately 50% (by mass) saturated solution with water.[11]
