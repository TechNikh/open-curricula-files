---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Discrete_symmetry
offline_file: ""
offline_thumbnail: ""
uuid: 82082064-1271-4848-b196-8f7eb4986d2f
updated: 1484309307
title: Discrete symmetry
categories:
    - Symmetry
---
In mathematics, a discrete symmetry is a symmetry that describes non-continuous changes in a system. For example, a square possesses discrete rotational symmetry, as only rotations by multiples of right angles will preserve the square's original appearance. Discrete symmetries sometimes involve some type of 'swapping', these swaps usually being called reflections or interchanges. In mathematics and theoretical physics, a discrete symmetry is a symmetry under the transformations of a discrete group—e.g. a topological group with a discrete topology whose elements form a finite or a countable set.
