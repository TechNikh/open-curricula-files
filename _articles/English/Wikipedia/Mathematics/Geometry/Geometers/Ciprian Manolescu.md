---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ciprian_Manolescu
offline_file: ""
offline_thumbnail: ""
uuid: f240a3cf-2b15-471c-ba55-3c06553195b1
updated: 1484309091
title: Ciprian Manolescu
tags:
    - Biography
    - Competitions
    - Selected works
categories:
    - Geometers
---
Ciprian Manolescu (born on December 24, 1978) is a Romanian-American[2] mathematician, working in gauge theory, symplectic geometry, and low-dimensional topology. He is currently a Professor of Mathematics at the University of California, Los Angeles.
