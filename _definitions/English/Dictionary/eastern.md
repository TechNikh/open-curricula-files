---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eastern
offline_file: ""
offline_thumbnail: ""
uuid: 8ea35c6b-03bb-40da-a1fe-d0c982ebce58
updated: 1484310435
title: eastern
categories:
    - Dictionary
---
eastern
     adj 1: lying toward or situated in the east; "the eastern end of
            the island"
     2: of or characteristic of eastern regions of the United
        States; "the Eastern establishment" [ant: {western}]
     3: lying in or toward the east; "the east side of NY"; "eastern
        cities" [syn: {easterly}]
     4: relating to or characteristic of regions of eastern parts of
        the world; "the Eastern Hemisphere"; "Eastern Europe";
        "the Eastern religions" [ant: {western}]
     5: from the east; used especially of winds; "an east wind";
        "the winds are easterly" [syn: {easterly}]
