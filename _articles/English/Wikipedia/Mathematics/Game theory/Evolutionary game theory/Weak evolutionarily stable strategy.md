---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Weak_evolutionarily_stable_strategy
offline_file: ""
offline_thumbnail: ""
uuid: e070796b-da4c-465d-b718-7bdc9e9055e9
updated: 1484308524
title: Weak evolutionarily stable strategy
categories:
    - Evolutionary game theory
---
A weak evolutionarily stable strategy (WESS) is a more broad form of evolutionarily stable strategy (ESS).[1] Like ESS, a WESS is able to defend against an invading "mutant" strategy. This means the WESS cannot be entirely eliminated from the population.
