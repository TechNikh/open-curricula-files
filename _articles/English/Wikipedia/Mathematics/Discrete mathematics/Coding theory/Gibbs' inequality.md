---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gibbs%27_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: d75f1df6-9778-4f05-a55a-6465fc5a6033
updated: 1484308539
title: "Gibbs' inequality"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Josiah_Willard_Gibbs_-from_MMS-.jpg
tags:
    - "Gibbs' inequality"
    - Proof
    - Alternative proofs
    - Corollary
categories:
    - Coding theory
---
In information theory, Gibbs' inequality is a statement about the mathematical entropy of a discrete probability distribution. Several other bounds on the entropy of probability distributions are derived from Gibbs' inequality, including Fano's inequality. It was first presented by J. Willard Gibbs in the 19th century.
