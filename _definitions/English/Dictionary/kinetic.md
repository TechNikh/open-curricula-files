---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kinetic
offline_file: ""
offline_thumbnail: ""
uuid: 48458f5c-8c58-433b-9b98-5406b2c99dac
updated: 1484310232
title: kinetic
categories:
    - Dictionary
---
kinetic
     adj 1: relating to the motion of material bodies and the forces
            associated therewith; "kinetic energy"
     2: characterized by motion; "modern dance has been called
        kinetic pantomime"
     3: supplying motive force; "the complex civilization of which
        Rome was the kinetic center"- H.O.Taylor [syn: {energizing},
         {energising}]
