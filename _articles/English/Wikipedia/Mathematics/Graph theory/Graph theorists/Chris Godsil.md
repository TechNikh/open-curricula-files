---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chris_Godsil
offline_file: ""
offline_thumbnail: ""
uuid: 7400d47d-1dee-4c79-b0bd-5992405a0f2b
updated: 1484309463
title: Chris Godsil
categories:
    - Graph theorists
---
Christopher David Godsil is a professor and the current Chair at the Department of Combinatorics and Optimization in the faculty of mathematics at the University of Waterloo. He wrote the popular textbook on algebraic graph theory, entitled Algebraic Graph Theory, with Gordon Royle,[1] His earlier textbook on algebraic combinatorics discussed distance-regular graphs and association schemes.[2] He started the Journal of Algebraic Combinatorics, and was the Editor-in-Chief of the Electronic Journal of Combinatorics from 2004 to 2008. He is also on the editorial board of the Journal of Combinatorial Theory Series B and Combinatorica.[3]
