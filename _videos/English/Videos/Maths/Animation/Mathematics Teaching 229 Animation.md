---
version: 1
type: video
provider: YouTube
id: MyK1RaOpnMw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: af241ac5-76a0-48d6-a1e2-f79beb012eac
updated: 1486069641
title: Mathematics Teaching 229 Animation
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/MyK1RaOpnMw/default.jpg
    - https://i3.ytimg.com/vi/MyK1RaOpnMw/1.jpg
    - https://i3.ytimg.com/vi/MyK1RaOpnMw/2.jpg
    - https://i3.ytimg.com/vi/MyK1RaOpnMw/3.jpg
---
