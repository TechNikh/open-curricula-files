---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Banach_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 0fee5821-c8d7-4ec2-99d4-5871205d4f3f
updated: 1484309206
title: Banach manifold
tags:
    - Definition
    - Examples
    - Classification up to homeomorphism
categories:
    - Differential geometry
---
In mathematics, a Banach manifold is a manifold modeled on Banach spaces. Thus it is a topological space in which each point has a neighbourhood homeomorphic to an open set in a Banach space (a more involved and formal definition is given below). Banach manifolds are one possibility of extending manifolds to infinite dimensions.
