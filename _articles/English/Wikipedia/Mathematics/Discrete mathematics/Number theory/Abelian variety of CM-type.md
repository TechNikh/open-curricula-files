---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Abelian_variety_of_CM-type
offline_file: ""
offline_thumbnail: ""
uuid: 90d901ad-8c9b-4c15-b3bc-86a0bc4765cb
updated: 1484308539
title: Abelian variety of CM-type
categories:
    - Number theory
---
In mathematics, an abelian variety A defined over a field K is said to have CM-type if it has a large enough commutative subring in its endomorphism ring End(A). The terminology here is from complex multiplication theory, which was developed for elliptic curves in the nineteenth century. One of the major achievements in algebraic number theory and algebraic geometry of the twentieth century was to find the correct formulations of the corresponding theory for abelian varieties of dimension d > 1. The problem is at a deeper level of abstraction, because it is much harder to manipulate analytic functions of several complex variables.
