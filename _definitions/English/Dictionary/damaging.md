---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/damaging
offline_file: ""
offline_thumbnail: ""
uuid: be42fa47-2e19-44a2-9f00-9eb0430a839e
updated: 1484310236
title: damaging
categories:
    - Dictionary
---
damaging
     adj 1: (sometimes followed by `to') causing harm or injury;
            "damaging to career and reputation"; "the reporter's
            coverage resulted in prejudicial publicity for the
            defendant" [syn: {detrimental}, {prejudicial}, {prejudicious}]
     2: designed or tending to discredit, especially without
        positive or helpful suggestions; "negative criticism"
        [syn: {negative}]
