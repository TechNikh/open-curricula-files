---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unitary_method
offline_file: ""
offline_thumbnail: ""
uuid: 249f5bf3-fd0b-41b1-9cfa-270a09d96292
updated: 1484308915
title: Unitary method
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg_9.png
categories:
    - Elementary algebra
---
The unitary method is a technique in mathematics for solving a problem by finding the value of single unit, i.e., 1, (by dividing) and then finding the necessary value by multiplying the single unit value. In essence, the unitary method is used to find the value of a unit from the value of a multiple (by dividing), and thence the value of a multiple (by multiplying).
