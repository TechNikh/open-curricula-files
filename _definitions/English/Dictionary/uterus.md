---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/uterus
offline_file: ""
offline_thumbnail: ""
uuid: d0d127ea-8c5c-43bb-bdb9-8ce737cdc171
updated: 1484310162
title: uterus
categories:
    - Dictionary
---
uterus
     n : a hollow muscular organ in the pelvic cavity of females;
         contains the developing fetus [syn: {womb}]
     [also: {uteri} (pl)]
