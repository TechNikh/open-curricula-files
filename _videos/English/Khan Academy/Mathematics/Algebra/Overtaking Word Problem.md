---
version: 1
type: video
provider: YouTube
id: LYWUHaWwY0c
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Overtaking%20Word%20Problem.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c04d6a23-8505-4c89-b77e-bec469442d22
updated: 1486069571
title: Overtaking Word Problem
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/LYWUHaWwY0c/default.jpg
    - https://i3.ytimg.com/vi/LYWUHaWwY0c/1.jpg
    - https://i3.ytimg.com/vi/LYWUHaWwY0c/2.jpg
    - https://i3.ytimg.com/vi/LYWUHaWwY0c/3.jpg
---
3 people overtaking each other at different rates
More free lessons at: http://www.khanacademy.org/video?v=LYWUHaWwY0c
