---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_network
offline_file: ""
offline_thumbnail: ""
uuid: 71b008ee-3718-4293-b679-56f95b2ab124
updated: 1484309432
title: Complex network
tags:
    - Definition
    - Scale-free networks
    - Small-world networks
    - Books
categories:
    - Network theory
---
In the context of network theory, a complex network is a graph (network) with non-trivial topological features—features that do not occur in simple networks such as lattices or random graphs but often occur in graphs modelling of real systems. The study of complex networks is a young and active area of scientific research (since 2000) inspired largely by the empirical study of real-world networks such as computer networks, technological networks, brain networks and social networks.
