---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pat
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484476861
title: pat
categories:
    - Dictionary
---
pat
     adj 1: having only superficial plausibility; "glib promises"; "a
            slick commercial" [syn: {glib}, {slick}]
     2: exactly suited to the occasion; "a pat reply"
     n 1: the sound made by a gentle blow [syn: {rap}, {tap}]
     2: a light touch or stroke [syn: {tap}, {dab}]
     adv : completely or perfectly; "he has the lesson pat"; "had the
           system down pat"
     v 1: pat or squeeze fondly or playfully, especially under the
          chin [syn: {chuck}]
     2: hit lightly; "pat him on the shoulder" [syn: {dab}]
     [also: {patting}, {patted}]
