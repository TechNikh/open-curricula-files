---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/onwards
offline_file: ""
offline_thumbnail: ""
uuid: bccd14ba-5266-45dc-b5f2-232c56a4900c
updated: 1484310471
title: onwards
categories:
    - Dictionary
---
onwards
     adv : in a forward direction; "go ahead"; "the train moved ahead
           slowly"; "the boat lurched ahead"; "moved onward into
           the forest"; "they went slowly forward in the mud"
           [syn: {ahead}, {onward}, {forward}, {forwards}, {forrader}]
