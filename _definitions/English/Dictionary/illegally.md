---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/illegally
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484427841
title: illegally
categories:
    - Dictionary
---
illegally
     adv : in an illegal manner; "they dumped the waste illegally"
           [syn: {illicitly}, {lawlessly}] [ant: {legitimately}]
