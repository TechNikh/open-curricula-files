---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/person
offline_file: ""
offline_thumbnail: ""
uuid: 3236d0fb-c9a5-4095-b109-01f5ec64be84
updated: 1484310319
title: person
categories:
    - Dictionary
---
person
     n 1: a human being; "there was too much for one person to do"
          [syn: {individual}, {someone}, {somebody}, {mortal}, {human},
           {soul}]
     2: a person's body (usually including their clothing); "a
        weapon was hidden on his person"
     3: a grammatical category of pronouns and verb forms; "stop
        talking about yourself in the third person"
