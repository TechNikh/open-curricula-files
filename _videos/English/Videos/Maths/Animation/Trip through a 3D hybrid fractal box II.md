---
version: 1
type: video
provider: YouTube
id: vST31O5Q46E
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2d0baaa9-a204-47c1-a426-5fd75df14bb1
updated: 1486069641
title: Trip through a 3D hybrid fractal box II
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/vST31O5Q46E/default.jpg
    - https://i3.ytimg.com/vi/vST31O5Q46E/1.jpg
    - https://i3.ytimg.com/vi/vST31O5Q46E/2.jpg
    - https://i3.ytimg.com/vi/vST31O5Q46E/3.jpg
---
