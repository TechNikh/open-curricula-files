---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Graph_equation
offline_file: ""
offline_thumbnail: ""
uuid: 45382007-0e6e-4044-9762-cda3ead736d4
updated: 1484308612
title: Graph equation
categories:
    - Graph theory
---
In graph theory, Graph equations are equations in which the unknowns are graphs. One of the central questions of graph theory concerns the notion of isomorphism. We ask: When are two graphs the same (i.e., graph isomorphism)? The graphs in question may be expressed differently in terms of graph equations.[1]
