---
version: 1
type: video
provider: YouTube
id: OYeXMMwh3Lk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Molecular%20Biology%20of%20Gene.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 181874f9-81ea-49fd-b84b-53b5b70f658f
updated: 1486069673
title: Molecular Biology of Gene
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/OYeXMMwh3Lk/default.jpg
    - https://i3.ytimg.com/vi/OYeXMMwh3Lk/1.jpg
    - https://i3.ytimg.com/vi/OYeXMMwh3Lk/2.jpg
    - https://i3.ytimg.com/vi/OYeXMMwh3Lk/3.jpg
---
