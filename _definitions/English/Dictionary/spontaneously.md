---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spontaneously
offline_file: ""
offline_thumbnail: ""
uuid: eb23eee6-45d0-4e56-a89d-c9da9247d32d
updated: 1484310375
title: spontaneously
categories:
    - Dictionary
---
spontaneously
     adv 1: in a spontaneous manner; "this shift occurs spontaneously"
     2: without advance preparation; "he spoke ad lib" [syn: {ad lib},
         {ad libitum}, {impromptu}]
