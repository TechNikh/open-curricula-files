---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Angle_trisection
offline_file: ""
offline_thumbnail: ""
uuid: baced555-6bf1-4fae-9eef-f2b2a99b08bd
updated: 1484309244
title: Angle trisection
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Neusis-trisection.svg.png
tags:
    - Background and problem statement
    - Proof of impossibility
    - Angles which can be trisected
    - Algebraic characterization
    - Other methods
    - Approximation by successive bisections
    - Using origami
    - Using a linkage
    - With an auxiliary curve
    - With a marked ruler
    - With a string
    - With a "tomahawk"
    - With interconnected compasses
    - Uses of angle trisection
    - Generalization
    - Additional references
    - Other means of trisection
categories:
    - History of geometry
---
Angle trisection is a classical problem of compass and straightedge constructions of ancient Greek mathematics. It concerns construction of an angle equal to one third of a given arbitrary angle, using only two tools: an unmarked straightedge, and a compass.
