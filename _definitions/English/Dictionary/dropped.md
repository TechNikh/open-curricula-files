---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dropped
offline_file: ""
offline_thumbnail: ""
uuid: 7152ab29-59e7-4868-8add-ee066c5195fe
updated: 1484310286
title: dropped
categories:
    - Dictionary
---
drop
     n 1: a small quantity (especially of a liquid); "one drop of each
          sample was analyzed"; "any child with a drop of negro
          blood was legally a negro"; "there is not a drop of pity
          in that man" [syn: {driblet}]
     2: a shape that is small and round; "he studied the shapes of
        low-viscosity drops"; "beads of sweat on his forehead"
        [syn: {bead}, {pearl}]
     3: a sudden sharp decrease in some quantity; "a drop of 57
        points on the Dow Jones index"; "there was a drop in
        pressure in the pulmonary artery"; "a dip in prices";
        "when that became known the price of their stock went into
        free fall" [syn: {dip}, ...
