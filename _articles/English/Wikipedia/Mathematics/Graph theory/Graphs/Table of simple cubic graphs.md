---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Table_of_simple_cubic_graphs
offline_file: ""
offline_thumbnail: ""
uuid: 1d89dd1c-39fc-4a66-8124-f8dfd5bc2aa5
updated: 1484309414
title: Table of simple cubic graphs
tags:
    - Connectivity
    - Pictures
    - LCF notation
    - Table
    - 4 nodes
    - 6 nodes
    - 8 nodes
    - 10 nodes
    - 12 nodes
    - Vector coupling coefficients
categories:
    - Graphs
---
