---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Half-integer
offline_file: ""
offline_thumbnail: ""
uuid: 9c2b3a90-b3a4-4856-b182-e6f5073228e4
updated: 1484308942
title: Half-integer
tags:
    - Notation and algebraic structure
    - Uses
    - Sphere packing
    - Physics
    - Sphere volume
categories:
    - Elementary number theory
---
where 
  
    
      
        n
      
    
    {\displaystyle n}
  
 is an integer. For example,
