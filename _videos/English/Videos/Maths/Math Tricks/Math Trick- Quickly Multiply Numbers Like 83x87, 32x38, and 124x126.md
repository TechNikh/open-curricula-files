---
version: 1
type: video
provider: YouTube
id: mmIjpcjdnPI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 5fcc1713-4677-4122-acc2-cffab0567cb5
updated: 1486069622
title: 'Math Trick- Quickly Multiply Numbers Like 83x87, 32x38, and 124x126'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/mmIjpcjdnPI/default.jpg
    - https://i3.ytimg.com/vi/mmIjpcjdnPI/1.jpg
    - https://i3.ytimg.com/vi/mmIjpcjdnPI/2.jpg
    - https://i3.ytimg.com/vi/mmIjpcjdnPI/3.jpg
---
