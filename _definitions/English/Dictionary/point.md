---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/point
offline_file: ""
offline_thumbnail: ""
uuid: f5c9f42b-66c0-4f2e-878e-1d2ed0d8431f
updated: 1484310293
title: point
categories:
    - Dictionary
---
point
     n 1: a geometric element that has position but no extension; "a
          point is defined by its coordinates"
     2: the precise location of something; a spatially limited
        location; "she walked to a point where she could survey
        the whole street"
     3: a brief version of the essential meaning of something; "get
        to the point"; "he missed the point of the joke"; "life
        has lost its point"
     4: a specific identifiable position in a continuum or series or
        especially in a process; "a remarkable degree of
        frankness"; "at what stage are the social sciences?" [syn:
         {degree}, {level}, {stage}]
     5: an isolated fact that is ...
