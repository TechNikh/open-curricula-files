---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/naturally
offline_file: ""
offline_thumbnail: ""
uuid: 69e177cf-0c87-4628-a86a-6f06a5878574
updated: 1484310293
title: naturally
categories:
    - Dictionary
---
naturally
     adv 1: as might be expected; "naturally, the lawyer sent us a huge
            bill" [syn: {of course}, {course}] [ant: {unnaturally}]
     2: through inherent nature; "he was naturally lazy" [syn: {by
        nature}]
     3: according to nature; by natural means; without artificial
        help; "naturally grown flowers" [ant: {artificially}]
     4: in a natural or normal manner; "speak naturally and easily"
        [ant: {unnaturally}]
