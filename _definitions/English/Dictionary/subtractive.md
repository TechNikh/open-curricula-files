---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subtractive
offline_file: ""
offline_thumbnail: ""
uuid: 60e102e2-9f46-4ad7-8bd3-ffbf36f21dd8
updated: 1484310423
title: subtractive
categories:
    - Dictionary
---
subtractive
     adj : constituting or involving subtraction; "a subtractive
           correction" [ant: {additive}]
