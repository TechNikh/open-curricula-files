---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dental_formula
offline_file: ""
offline_thumbnail: ""
uuid: 49e0d509-412f-4841-ba22-ee17b4720d45
updated: 1484308375
title: Dentition
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/335px-Upper_Jaw_Dentition.jpeg
tags:
    - Overview
    - Dental formula
    - Tooth naming discrepancies
    - Dental eruption sequence
    - Dental formulae examples
    - Dentition use in archaeology
    - Dinosaurs
    - Dentition discussions in other articles
    - Notes
categories:
    - Uncategorized
---
Dentition pertains to the development of teeth and their arrangement in the mouth. In particular, it is the characteristic arrangement, kind, and number of teeth in a given species at a given age.[1] That is, the number, type, and morpho-physiology (the physical shape) of the teeth of an animal.[2]
