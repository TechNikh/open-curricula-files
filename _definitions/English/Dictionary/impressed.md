---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/impressed
offline_file: ""
offline_thumbnail: ""
uuid: 09a8ba00-c48b-427d-8843-8b40ad5f3657
updated: 1484310585
title: impressed
categories:
    - Dictionary
---
impressed
     adj : deeply or markedly affected or influenced [syn: {impressed(p)}]
