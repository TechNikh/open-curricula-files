---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/faithful
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484579041
title: faithful
categories:
    - Dictionary
---
faithful
     adj 1: steadfast in affection or allegiance; "years of faithful
            service"; "faithful employees"; "we do not doubt that
            England has a faithful patriot in the Lord Chancellor"
            [ant: {unfaithful}]
     2: marked by fidelity to an original; "a close translation"; "a
        faithful copy of the portrait"; "a faithful rendering of
        the observed facts" [syn: {close}]
     3: not having sexual relations with anyone except your husband
        or wife, or your boyfriend or girlfriend; "he remained
        faithful to his wife" [ant: {unfaithful}]
     n 1: any loyal and steadfast following
     2: a group of people who adhere to a common faith ...
