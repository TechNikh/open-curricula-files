---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stored
offline_file: ""
offline_thumbnail: ""
uuid: 30ae2de0-d0c7-491e-8e78-c937d32dc635
updated: 1484310316
title: stored
categories:
    - Dictionary
---
stored
     adj : accumulated until needed; "stored energy in the condenser";
           "the computer can use stored information"
