---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/th
offline_file: ""
offline_thumbnail: ""
uuid: f504bba4-f8a3-41e2-bd64-b5d27613d1c9
updated: 1484310355
title: th
categories:
    - Dictionary
---
Th
     n 1: the fifth day of the week; the fourth working day [syn: {Thursday}]
     2: a soft silvery-white tetravalent radioactive metallic
        element; isotope 232 is used as a power source in nuclear
        reactors; occurs in thorite and in monazite sands [syn: {thorium},
         {atomic number 90}]
