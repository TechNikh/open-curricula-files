---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mixed_graph
offline_file: ""
offline_thumbnail: ""
uuid: a72d8059-f35a-491b-9432-8ecce68c06f2
updated: 1484308632
title: Mixed graph
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Mixed_Graph_Example.jpg
tags:
    - Definitions and Notation
    - Coloring
    - Example
    - Strong and weak coloring
    - Existence
    - Computing weak chromatic polynomials
    - Applications
    - Scheduling problem
    - Bayesian inference
    - Notes
categories:
    - Graph theory
---
A mixed graph G = (V, E, A) is a mathematical object consisting of a set of vertices (or nodes) V, a set of (undirected) edges E, and a set of directed edges (or arcs) A.[1]
