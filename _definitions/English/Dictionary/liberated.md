---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/liberated
offline_file: ""
offline_thumbnail: ""
uuid: b672ed56-08d5-487f-b1f6-c8a4b038af24
updated: 1484310373
title: liberated
categories:
    - Dictionary
---
liberated
     adj 1: (of a gas e.g.) released from chemical combination
     2: freed from bondage [syn: {emancipated}, {freed}]
     3: free from traditional social restraints; "an emancipated
        young woman pursuing her career"; "a liberated lifestyle"
        [syn: {emancipated}]
