---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chest
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484447701
title: chest
categories:
    - Dictionary
---
chest
     n 1: the part of the human body between the neck and the
          diaphragm or the corresponding part in other vertebrates
          [syn: {thorax}, {pectus}]
     2: box with a lid; used for storage; usually large and sturdy
     3: furniture with drawers for keeping clothes [syn: {chest of
        drawers}, {bureau}, {dresser}]
