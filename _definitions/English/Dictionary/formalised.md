---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formalised
offline_file: ""
offline_thumbnail: ""
uuid: 345c627e-aa51-4290-99f1-2aac7eb1f987
updated: 1484310180
title: formalised
categories:
    - Dictionary
---
formalised
     adj 1: concerned with or characterized by rigorous or adherence to
            recognized forms (especially in religion or art);
            "highly formalized plays like `Waiting for Godot'"
            [syn: {formalistic}, {formalized}]
     2: given formal standing or endorsement; made official or
        legitimate by the observance of proper procedures [syn: {formalized}]
