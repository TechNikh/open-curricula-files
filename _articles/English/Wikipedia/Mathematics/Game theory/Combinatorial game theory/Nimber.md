---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nimber
offline_file: ""
offline_thumbnail: ""
uuid: 7d63de20-40f1-4f28-9b09-83a45b98d8a0
updated: 1484308538
title: Nimber
tags:
    - Properties
    - Minimum excludant (mex)
    - Addition
    - Multiplication
    - Addition and multiplication tables
    - Notes
categories:
    - Combinatorial game theory
---
In mathematics, the nimbers, also called Grundy numbers, are introduced in combinatorial game theory, where they are defined as the values of nim heaps. They arise in a much larger class of games because of the Sprague–Grundy theorem. The nimbers are the ordinal numbers endowed with a new nimber addition and nimber multiplication, which are distinct from ordinal addition and ordinal multiplication. The minimum excludant operation is applied to sets of nimbers.
