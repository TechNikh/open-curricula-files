---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/creature
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484458561
title: creature
categories:
    - Dictionary
---
creature
     n 1: a living organism characterized by voluntary movement [syn:
          {animal}, {animate being}, {beast}, {brute}, {fauna}]
     2: a human being; `wight' is an archaic term [syn: {wight}]
     3: a person who is controlled by others and is used to perform
        unpleasant or dishonest tasks for someone else [syn: {tool},
         {puppet}]
