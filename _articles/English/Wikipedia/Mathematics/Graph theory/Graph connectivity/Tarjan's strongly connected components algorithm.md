---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm'
offline_file: ""
offline_thumbnail: ""
uuid: 4b43c17f-4d8e-48b0-a392-6f1d11f07b25
updated: 1484309405
title: "Tarjan's strongly connected components algorithm"
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Tarjan%2527s_Algorithm_Animation.gif'
tags:
    - Overview
    - Stack invariant
    - Bookkeeping
    - The algorithm in pseudocode
    - Complexity
    - Additional remarks
categories:
    - Graph connectivity
---
Tarjan's algorithm is an algorithm in graph theory for finding the strongly connected components of a graph. It runs in linear time, matching the time bound for alternative methods including Kosaraju's algorithm and the path-based strong component algorithm. Tarjan's Algorithm is named for its discoverer, Robert Tarjan.[1]
