---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computability_in_Europe
offline_file: ""
offline_thumbnail: ""
uuid: 0ea58bf0-b652-4393-af06-fb15a0e6c3b5
updated: 1484308667
title: Computability in Europe
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Computability_in_Europe_logo.jpg
tags:
    - Association
    - Conference series
    - 'Book series & Journal'
categories:
    - Theoretical computer science
---
Computability in Europe (CiE) is an international organization of mathematicians, logicians, computer scientists, philosophers, theoretical physicists and others interested in new developments in computability and in their underlying significance for the real world. CiE aims to widen understanding and appreciation of the importance of the concepts and techniques of computability theory, and to support the development of a vibrant multi-disciplinary community of researchers focused on computability-related topics. CiE positions itself at the interface between applied and fundamental research, prioritising mathematical approaches to computational barriers.
