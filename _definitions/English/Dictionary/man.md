---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/man
offline_file: ""
offline_thumbnail: ""
uuid: 22148fe2-d75d-4f62-ac10-a69ff5fa1cb2
updated: 1484310315
title: man
categories:
    - Dictionary
---
man
     n 1: an adult male person (as opposed to a woman); "there were
          two women and six men on the bus" [syn: {adult male}]
          [ant: {woman}]
     2: someone who serves in the armed forces; a member of a
        military force; "two men stood sentry duty" [syn: {serviceman},
         {military man}, {military personnel}] [ant: {civilian}]
     3: the generic use of the word to refer to any human being; "it
        was every man for himself"
     4: all of the inhabitants of the earth; "all the world loves a
        lover"; "she always used `humankind' because `mankind'
        seemed to slight the women" [syn: {world}, {human race}, {humanity},
         {humankind}, ...
