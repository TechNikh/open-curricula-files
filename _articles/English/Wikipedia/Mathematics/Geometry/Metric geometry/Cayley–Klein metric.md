---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Cayley%E2%80%93Klein_metric'
offline_file: ""
offline_thumbnail: ""
uuid: 930b8be6-2c86-4bf8-b828-1f7c097d4c0b
updated: 1484309262
title: Cayley–Klein metric
tags:
    - Definition
    - Foundations
    - Notes
categories:
    - Metric geometry
---
In mathematics, a Cayley–Klein metric is a metric on the complement of a fixed quadric in a projective space is defined using a cross-ratio. The construction originated with Arthur Cayley's essay "On the theory of distance"[1] where he calls the quadric the absolute. The construction was developed in further detail by Felix Klein in papers in 1871 and 1873, and in his book Vorlesungen über Nicht-Euklidischen Geometrie(1928). The Cayley–Klein metrics are a unifying idea in geometry since the method is used to provide metrics in hyperbolic geometry, elliptic geometry, and Euclidean geometry. The field of non-Euclidean geometry rests largely on the footing provided by Cayley–Klein ...
