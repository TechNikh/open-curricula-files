---
version: 1
type: article
id: https://en.wikipedia.org/wiki/2_(number)
offline_file: ""
offline_thumbnail: ""
uuid: bcf93f68-cffb-46c3-94b3-17ca7285aa2a
updated: 1484308944
title: 2 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-2_playing_cards.jpg
tags:
    - In mathematics
    - List of basic calculations
    - Evolution of the glyph
    - In science
    - Astronomy
    - In technology
    - In religion
    - Judaism
    - Numerological significance
    - In sports
    - In other fields
categories:
    - Integers
---
