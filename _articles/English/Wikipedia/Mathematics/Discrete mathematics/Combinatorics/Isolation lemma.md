---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isolation_lemma
offline_file: ""
offline_thumbnail: ""
uuid: fe71e593-480e-41bc-82a7-d609ff2ea793
updated: 1484308588
title: Isolation lemma
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Linear_optimization_in_a_2-dimensional_polytope.svg.png
tags:
    - The isolation lemma of Mulmuley, Vazirani, and Vazirani
    - Examples/applications
    - Notes
categories:
    - Combinatorics
---
In theoretical computer science, the term isolation lemma (or isolating lemma) refers to randomized algorithms that reduce the number of solutions to a problem to one, should a solution exist. This is achieved by constructing random constraints such that, with non-negligible probability, exactly one solution satisfies these additional constraints if the solution space is not empty. Isolation lemmas have important applications in computer science, such as the Valiant–Vazirani theorem and Toda's theorem in computational complexity theory.
