---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interrupted
offline_file: ""
offline_thumbnail: ""
uuid: 2084da5f-9cb1-4437-9f2e-b851cf57afc0
updated: 1484310377
title: interrupted
categories:
    - Dictionary
---
interrupted
     adj 1: discontinued temporarily; "we resumed the interrupted
            discussion"
     2: intermittently stopping and starting; "fitful (or
        interrupted) sleep"; "off-and-on static" [syn: {fitful}, {off-and-on(a)}]
