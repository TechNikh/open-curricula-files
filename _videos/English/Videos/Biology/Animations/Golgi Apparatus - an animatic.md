---
version: 1
type: video
provider: YouTube
id: bD4z27ASN1M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4366ffe5-c532-4c27-b538-f3fce59b002f
updated: 1486069668
title: 'Golgi Apparatus - an animatic'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/bD4z27ASN1M/default.jpg
    - https://i3.ytimg.com/vi/bD4z27ASN1M/1.jpg
    - https://i3.ytimg.com/vi/bD4z27ASN1M/2.jpg
    - https://i3.ytimg.com/vi/bD4z27ASN1M/3.jpg
---
