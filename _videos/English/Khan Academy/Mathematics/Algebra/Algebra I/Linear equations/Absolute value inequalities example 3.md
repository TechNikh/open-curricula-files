---
version: 1
type: video
provider: YouTube
id: y9MGpOGQVqQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Absolute%20value%20inequalities%20example%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d067b432-357e-4f7d-a327-ae29afad4517
updated: 1486069561
title: Absolute value inequalities example 3
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/y9MGpOGQVqQ/default.jpg
    - https://i3.ytimg.com/vi/y9MGpOGQVqQ/1.jpg
    - https://i3.ytimg.com/vi/y9MGpOGQVqQ/2.jpg
    - https://i3.ytimg.com/vi/y9MGpOGQVqQ/3.jpg
---
