---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Denjoy%E2%80%93Wolff_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5afa51db-ea0e-406e-81f6-c40d2af49ef9
updated: 1484308692
title: Denjoy–Wolff theorem
tags:
    - Statement
    - Proof of Theorem
    - Fixed point in the disk
    - No fixed points
    - Notes
categories:
    - Theorems in dynamical systems
---
In mathematics, the Denjoy–Wolff theorem is a theorem in complex analysis and dynamical systems concerning fixed points and iterations of holomorphic mappings of the unit disc in the complex numbers into itself. The result was proved independently in 1926 by the French mathematician Arnaud Denjoy and the Dutch mathematician Julius Wolff.
