---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/N%C3%A9el_temperature'
offline_file: ""
offline_thumbnail: ""
uuid: 7619821f-84f5-4821-b793-48409d428488
updated: 1484308749
title: Néel temperature
categories:
    - Critical phenomena
---
The Néel temperature or magnetic ordering temperature, TN, is the temperature above which an antiferromagnetic material becomes paramagnetic—that is, the thermal energy becomes large enough to destroy the microscopic magnetic ordering within the material.[1]
