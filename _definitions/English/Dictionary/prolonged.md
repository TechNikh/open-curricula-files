---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prolonged
offline_file: ""
offline_thumbnail: ""
uuid: 34b957cb-55a4-462a-8eaf-db56579b4fa5
updated: 1484310180
title: prolonged
categories:
    - Dictionary
---
prolonged
     adj 1: relatively long in duration; tediously protracted; "a
            drawn-out argument"; "an extended discussion"; "a
            lengthy visit from her mother-in-law"; "a prolonged
            and bitter struggle"; "protracted negotiations" [syn:
            {drawn-out}, {extended}, {lengthy}, {protracted}]
     2: drawn out or made longer spatially; "Picasso's elongated Don
        Quixote"; "lengthened skirts are fashionable this year";
        "the extended airport runways can accommodate larger
        planes"; "a prolonged black line across the page" [syn: {elongated},
         {extended}, {lengthened}]
     3: (of illness) developing slowly or of long duration
