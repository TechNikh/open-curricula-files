---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Additive_inverse
offline_file: ""
offline_thumbnail: ""
uuid: 57299af7-b780-416e-b419-31e19450d280
updated: 1484308905
title: Additive inverse
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-NegativeI2Root.svg.png
tags:
    - Common examples
    - Relation to subtraction
    - Other properties
    - Formal definition
    - Other examples
    - Non-examples
    - Footnotes
categories:
    - Elementary algebra
---
In mathematics, the additive inverse of a number a is the number that, when added to a, yields zero. This number is also known as the opposite (number),[1] sign change, and negation.[2] For a real number, it reverses its sign: the opposite to a positive number is negative, and the opposite to a negative number is positive. Zero is the additive inverse of itself.
