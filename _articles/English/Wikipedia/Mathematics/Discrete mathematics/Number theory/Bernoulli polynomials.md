---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bernoulli_polynomials
offline_file: ""
offline_thumbnail: ""
uuid: 8d187f4b-bcac-4c39-bacb-fb35a48379a2
updated: 1484308622
title: Bernoulli polynomials
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bernoulli_polynomials.svg.png
tags:
    - Representations
    - Explicit formula
    - Generating functions
    - Representation by a differential operator
    - Representation by an integral operator
    - Another explicit formula
    - Sums of pth powers
    - The Bernoulli and Euler numbers
    - Explicit expressions for low degrees
    - Maximum and minimum
    - Differences and derivatives
    - Translations
    - Symmetries
    - Fourier series
    - Inversion
    - Relation to falling factorial
    - Multiplication theorems
    - Integrals
    - Periodic Bernoulli polynomials
categories:
    - Number theory
---
In mathematics, the Bernoulli polynomials occur in the study of many special functions and in particular the Riemann zeta function and the Hurwitz zeta function. This is in large part because they are an Appell sequence, i.e. a Sheffer sequence for the ordinary derivative operator. Unlike orthogonal polynomials, the Bernoulli polynomials are remarkable in that the number of crossings of the x-axis in the unit interval does not go up as the degree of the polynomials goes up. In the limit of large degree, the Bernoulli polynomials, appropriately scaled, approach the sine and cosine functions.
