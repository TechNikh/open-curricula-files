---
version: 1
type: video
provider: YouTube
id: f_MZNsEqyQw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1fed45e4-de91-4958-8b44-e9e8b928e571
updated: 1486069656
title: 'Capacitors and Capacitance- Capacitor physics and circuit operation'
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/f_MZNsEqyQw/default.jpg
    - https://i3.ytimg.com/vi/f_MZNsEqyQw/1.jpg
    - https://i3.ytimg.com/vi/f_MZNsEqyQw/2.jpg
    - https://i3.ytimg.com/vi/f_MZNsEqyQw/3.jpg
---
