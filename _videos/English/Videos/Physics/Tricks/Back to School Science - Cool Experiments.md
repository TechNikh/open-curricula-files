---
version: 1
type: video
provider: YouTube
id: KSj0UcA7uY8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bd506ac2-545c-4822-adc0-2fe7b26211fe
updated: 1486069660
title: 'Back to School Science - Cool Experiments'
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/KSj0UcA7uY8/default.jpg
    - https://i3.ytimg.com/vi/KSj0UcA7uY8/1.jpg
    - https://i3.ytimg.com/vi/KSj0UcA7uY8/2.jpg
    - https://i3.ytimg.com/vi/KSj0UcA7uY8/3.jpg
---
