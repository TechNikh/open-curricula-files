---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Projection_body
offline_file: ""
offline_thumbnail: ""
uuid: 2fd6fd2f-d389-4e3d-b227-5189cd66b7ee
updated: 1484309193
title: Projection body
categories:
    - Convex geometry
---
In convex geometry, the projection body ΠK of a centrally symmetric body K in Euclidean space is the star body such that for any vector u, the support function of ΠK in the direction u is the (n – 1)-dimensional volume of the projection of K onto the hyperplane u⊥.
