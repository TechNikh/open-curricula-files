---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Daniel_Pedoe
offline_file: ""
offline_thumbnail: ""
uuid: 1972b18b-cde8-49fd-a3e9-5989053849f3
updated: 1484309098
title: Daniel Pedoe
tags:
    - Early life
    - Cambridge and Princeton
    - University of Southampton and Freeman Dyson
    - Birmingham University and Westfield College
    - Khartoum and Singapore
    - Purdue and Minnesota
    - San Gaku
    - Death
    - Archive
    - Books
categories:
    - Geometers
---
Dan Pedoe (29 October 1910, London – 27 October 1998, St Paul, Minnesota, USA[1]) was an English-born mathematician and geometer with a career spanning more than sixty years. In the course of his life he wrote approximately fifty research and expository papers in geometry. He is also the author of various core books on mathematics and geometry some of which have remained in print for decades and been translated into several languages. These books include the three-volume Methods of Algebraic Geometry (which he wrote in collaboration with W. V. D. Hodge), The Gentle Art of Mathematics, Circles: A Mathematical View, Geometry and the Visual Arts and most recently Japanese Temple Geometry ...
