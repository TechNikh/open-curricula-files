---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/licence
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484605621
title: licence
categories:
    - Dictionary
---
licence
     n 1: excessive freedom; lack of due restraint; "when liberty
          becomes license dictatorship is near"- Will Durant; "the
          intolerable license with which the newspapers
          break...the rules of decorum"- Edmund Burke [syn: {license}]
     2: freedom to deviate deliberately from normally applicable
        rules or practices (especially in behavior or speech)
        [syn: {license}]
     3: a legal document giving official permission to do something
        [syn: {license}, {permit}]
     v : authorize officially; "I am licensed to practice law in this
         state" [syn: {license}, {certify}] [ant: {decertify}]
