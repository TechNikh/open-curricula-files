---
version: 1
type: video
provider: YouTube
id: QVyvR5nq2nI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: aba6d851-625c-4bc9-be1c-8ac2dc970c9e
updated: 1486069639
title: 'Experiment- Rectilinear Propagation of Light (5th Grade)'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/QVyvR5nq2nI/default.jpg
    - https://i3.ytimg.com/vi/QVyvR5nq2nI/1.jpg
    - https://i3.ytimg.com/vi/QVyvR5nq2nI/2.jpg
    - https://i3.ytimg.com/vi/QVyvR5nq2nI/3.jpg
---
