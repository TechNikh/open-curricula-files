---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shoe
offline_file: ""
offline_thumbnail: ""
uuid: 51616c97-0124-45dd-8479-fd2def715ed7
updated: 1484310365
title: shoe
categories:
    - Dictionary
---
shoe
     n 1: footwear shaped to fit the foot (below the ankle) with a
          flexible upper of leather or plastic and a sole and heel
          of heavier material
     2: (card games) a case from which playing cards are dealt one
        at a time
     3: U-shaped plate nailed to underside of horse's hoof [syn: {horseshoe}]
     4: a restraint provided when the brake linings are moved
        hydraulically against the brake drum to retard the wheel's
        rotation [syn: {brake shoe}, {skid}]
     v : furnish with shoes; "the children were well shoed"
     [also: {shod}]
