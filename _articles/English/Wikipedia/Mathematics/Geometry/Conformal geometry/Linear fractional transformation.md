---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Linear_fractional_transformation
offline_file: ""
offline_thumbnail: ""
uuid: 2bdfecdb-4439-40ec-8dae-52b8c6a04ccb
updated: 1484309185
title: Linear fractional transformation
categories:
    - Conformal geometry
---
In mathematics, the phrase linear fractional transformation usually refers to a Möbius transformation, which is a homography on the complex projective line P(C) where C is the field of complex numbers.
