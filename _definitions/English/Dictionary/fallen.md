---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fallen
offline_file: ""
offline_thumbnail: ""
uuid: 6b4d697a-de67-47ae-85e0-b0c03e0f784f
updated: 1484310210
title: fallen
categories:
    - Dictionary
---
fall
     n 1: the season when the leaves fall from the trees; "in the fall
          of 1973" [syn: {autumn}]
     2: a sudden drop from an upright position; "he had a nasty
        spill on the ice" [syn: {spill}, {tumble}]
     3: the lapse of mankind into sinfulness because of the sin of
        Adam and Eve; "women have been blamed ever since the Fall"
     4: a downward slope or bend [syn: {descent}, {declivity}, {decline},
         {declination}, {declension}, {downslope}] [ant: {ascent}]
     5: a lapse into sin; a loss of innocence or of chastity; "a
        fall from virtue"
     6: a sudden decline in strength or number or importance; "the
        fall of the House of Hapsburg" ...
