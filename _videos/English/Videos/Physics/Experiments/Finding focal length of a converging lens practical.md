---
version: 1
type: video
provider: YouTube
id: wJriOCBBmuQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 529b116f-4493-4994-abcf-8b92b8d67028
updated: 1486069640
title: Finding focal length of a converging lens practical
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/wJriOCBBmuQ/default.jpg
    - https://i3.ytimg.com/vi/wJriOCBBmuQ/1.jpg
    - https://i3.ytimg.com/vi/wJriOCBBmuQ/2.jpg
    - https://i3.ytimg.com/vi/wJriOCBBmuQ/3.jpg
---
