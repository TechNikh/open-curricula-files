---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aloof
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484519162
title: aloof
categories:
    - Dictionary
---
aloof
     adj : remote in manner; "stood apart with aloof dignity"; "a
           distant smile"; "he was upstage with strangers" [syn: {distant},
            {upstage}]
     adv : in an aloof manner; "the local gentry and professional
           classes had held aloof for the school had accepted
           their sons readily enough"
