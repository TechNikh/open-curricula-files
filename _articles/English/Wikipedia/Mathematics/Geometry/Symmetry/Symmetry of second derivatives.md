---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_of_second_derivatives
offline_file: ""
offline_thumbnail: ""
uuid: 9b031fd0-7c36-4137-befc-7a8cbbf27864
updated: 1484309319
title: Symmetry of second derivatives
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Graph001.png
tags:
    - Hessian matrix
    - Formal expressions of symmetry
    - "Schwarz's theorem"
    - Sufficiency of twice-differentiability
    - Distribution theory formulation
    - Requirement of continuity
    - In Lie theory
categories:
    - Symmetry
---
In mathematics, the symmetry of second derivatives (also called the equality of mixed partials) refers to the possibility under certain conditions (see below) of interchanging the order of taking partial derivatives of a function
