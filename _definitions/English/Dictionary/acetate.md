---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/acetate
offline_file: ""
offline_thumbnail: ""
uuid: 6c7321de-4f1d-4436-93e8-1a4ec673de49
updated: 1484310384
title: acetate
categories:
    - Dictionary
---
acetate
     n 1: a salt or ester of acetic acid [syn: {ethanoate}]
     2: a fabric made from cellulose acetate fibers [syn: {acetate
        rayon}]
