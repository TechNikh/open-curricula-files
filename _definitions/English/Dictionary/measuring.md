---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/measuring
offline_file: ""
offline_thumbnail: ""
uuid: 951b6f3f-19c3-4c4f-822d-bff6677b6745
updated: 1484310218
title: measuring
categories:
    - Dictionary
---
measuring
     n : the act or process of measuring; "the measurements were
         carefully done"; "his mental measurings proved remarkably
         accurate" [syn: {measurement}, {measure}, {mensuration}]
