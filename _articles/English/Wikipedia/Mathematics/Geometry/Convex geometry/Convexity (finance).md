---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Convexity_(finance)
offline_file: ""
offline_thumbnail: ""
uuid: cbf8014b-8c99-41ff-9e81-ad92cf21a91d
updated: 1484309189
title: Convexity (finance)
tags:
    - Terminology
    - Mathematics
    - Interpretation
    - Convexity adjustments
categories:
    - Convex geometry
---
In mathematical finance, convexity refers to non-linearities in a financial model. In other words, if the price of an underlying variable changes, the price of an output does not change linearly, but depends on the second derivative (or, loosely speaking, higher-order terms) of the modeling function. Geometrically, the model is no longer flat but curved, and the degree of curvature is called the convexity.
