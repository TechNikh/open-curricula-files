---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/relative
offline_file: ""
offline_thumbnail: ""
uuid: d8112d5a-1a21-405f-a429-2c9ce089351e
updated: 1484310232
title: relative
categories:
    - Dictionary
---
relative
     adj 1: not absolute or complete; "a relative stranger" [ant: {absolute}]
     2: properly related in size or degree or other measurable
        characteristics; usually followed by `to'; "punishment
        oughtt to be proportional to the crime"; "earnings
        relative to production" [syn: {proportional}]
     n 1: a person related by blood or marriage; "police are searching
          for relatives of the deceased"; "he has distant
          relations back in New Jersey" [syn: {relation}]
     2: an animal or plant that bears a relationship to another (as
        related by common descent or by membership in the same
        genus) [syn: {congener}, {congenator}]
