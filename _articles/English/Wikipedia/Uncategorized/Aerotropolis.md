---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Aerotropolis
offline_file: ""
offline_thumbnail: ""
uuid: b5c1b165-2cfb-4e58-ac08-6d53681940ff
updated: 1484308451
title: Aerotropolis
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/Aerotropolis_Schematic_with_Airport_City_Core.jpg
tags:
    - Airports, connectivity, and development
    - Criticisms of the concept
    - List of aerotropolises
    - The African Aerotropolis
    - The California Aerotropolis
    - Failed St. Louis Aerotropolis proposal
categories:
    - Uncategorized
---
An aerotropolis is a metropolitan subregion where the layout, infrastructure, and economy are centered on an airport which serves as a multimodal "airport city" commercial core.[1][2] It is similar in form to a traditional metropolis, which contains a central city commercial core and commuter-linked suburbs.[3] The term was first used by New York commercial artist Nicholas DeSantis, whose drawing of a skyscraper rooftop airport in the city was presented in the November 1939 issue of Popular Science.[4] The term was repurposed by air commerce researcher John D. Kasarda in 2000 based on his prior research on airport-driven economic development.[5][6][7][8][9]
