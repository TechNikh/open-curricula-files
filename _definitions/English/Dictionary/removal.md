---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/removal
offline_file: ""
offline_thumbnail: ""
uuid: a72b4851-9906-4678-a378-518542522836
updated: 1484310319
title: removal
categories:
    - Dictionary
---
removal
     n 1: the act of removing; "he had surgery for the removal of a
          malignancy" [syn: {remotion}]
     2: dismissal from office
