---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sholapur
offline_file: ""
offline_thumbnail: ""
uuid: a3379269-ec56-4378-8f34-5d70fd8fa386
updated: 1484308455
title: Solapur
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Siddheshwar_Temple.JPG
tags:
    - Etymology and history
    - Toponomy
    - culture
    - Demographics
    - Geography and Climate
    - Civic administration
    - Education
    - University
    - Colleges
    - Science Museum
    - Tourism
    - Sports
    - Economy
    - Beedi Industry
    - Environment
    - Transport
    - Rail
    - Road
    - air
    - Utility services
    - Notable people
    - Notes and references
categories:
    - Uncategorized
---
Solapur (IPA: [Sōlāpūr]) ( pronunciation (help·info)) is a city located in the south-eastern region of the Indian state of Maharashtra.[6][7] Solapur is located on major road and rail routes between Mumbai and Hyderabad, with a branch line to the cities of Bijapur and Gadag in the neighbouring state of Karnataka.[8] It is classified as a 2 Tier and B-2 class city by House Rent Allowance (HRA) classification by the Government of India.[9] It is 49th-most-populous city in India and 43rd-largest urban agglomeration.[10]
