---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/demonstrator
offline_file: ""
offline_thumbnail: ""
uuid: 3d6b7ab3-f898-4fc6-851a-ec04e65bb717
updated: 1484310172
title: demonstrator
categories:
    - Dictionary
---
demonstrator
     n 1: a teacher or teacher's assistant who demonstrates the
          principles that are being taught
     2: someone who demonstrates an article to a prospective buyer
        [syn: {sales demonstrator}]
     3: someone who participates in a public display of group
        feeling [syn: {protester}]
