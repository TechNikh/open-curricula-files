---
version: 1
type: video
provider: YouTube
id: PfqijYvJ438
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 329dd800-f1de-4683-91b7-800e28d46232
updated: 1486069631
title: Homonyms and Homophones
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/PfqijYvJ438/default.jpg
    - https://i3.ytimg.com/vi/PfqijYvJ438/1.jpg
    - https://i3.ytimg.com/vi/PfqijYvJ438/2.jpg
    - https://i3.ytimg.com/vi/PfqijYvJ438/3.jpg
---
