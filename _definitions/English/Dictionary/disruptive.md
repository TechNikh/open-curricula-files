---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disruptive
offline_file: ""
offline_thumbnail: ""
uuid: 25bedebf-b217-475d-bb80-8d4e0ebe4eed
updated: 1484310603
title: disruptive
categories:
    - Dictionary
---
disruptive
     adj : characterized by unrest or disorder or insubordination;
           "effects of the struggle will be violent and
           disruptive"; "riotous times"; "these troubled areas";
           "the tumultuous years of his administration"; "a
           turbulent and unruly childhood" [syn: {riotous}, {troubled},
            {tumultuous}, {turbulent}]
