---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Correlation_sum
offline_file: ""
offline_thumbnail: ""
uuid: 8205231a-e571-47c5-8b81-8e96f773abc5
updated: 1484308705
title: Correlation sum
categories:
    - Chaos theory
---
In chaos theory, the correlation sum is the estimator of the correlation integral, which reflects the mean probability that the states at two different times are close:
