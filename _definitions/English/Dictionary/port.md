---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/port
offline_file: ""
offline_thumbnail: ""
uuid: 2c18702f-f0b5-4066-b16e-6c106c08cd2a
updated: 1484310200
title: port
categories:
    - Dictionary
---
port
     adj : located on the left side of a ship or aircraft [syn: {larboard}]
     n 1: a place (seaport or airport) where people and merchandise
          can enter or leave a country
     2: sweet dark-red dessert wine originally from Portugal [syn: {port
        wine}]
     3: an opening (in a wall or ship or armored vehicle) for firing
        through [syn: {embrasure}, {porthole}]
     4: the left side of a ship or aircraft to someone facing the
        bow or nose [syn: {larboard}] [ant: {starboard}]
     5: (computer science) computer circuit consisting of the
        hardware and associated circuitry that links one device
        with another (especially a computer and a hard ...
