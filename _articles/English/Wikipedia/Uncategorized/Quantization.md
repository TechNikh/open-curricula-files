---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quantized
offline_file: ""
offline_thumbnail: ""
uuid: 3777534c-ae18-4395-a810-c7b63b60bc4c
updated: 1484308307
title: Quantization
categories:
    - Uncategorized
---
Quantization is the procedure of constraining something from a continuous set of values (such as the real numbers) to a relatively small discrete set (such as the integers).
