---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Overheating_(electricity)
offline_file: ""
offline_thumbnail: ""
uuid: 950696ad-1620-46bc-bdf7-7683b216b877
updated: 1484308849
title: Overheating (electricity)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/426px-Failed_SMPS_controller_IC_ISL6251.jpg
tags:
    - Causes
    - Preventive measures
    - Use of circuit breaker or fuse
    - Use of heat-dissipating systems
    - Control within circuit-design
    - Proper manufacture
    - Sources
categories:
    - Thermodynamics
---
Overheating is a phenomenon of rising of temperature in an electric circuit (or portion of a circuit). Overheating causes potential damage to the circuit components, and can cause fire, explosion, or injury. The parts damages caused by overheating are commonly irreversible; i.e. the only way to repair, is to replace some components.
