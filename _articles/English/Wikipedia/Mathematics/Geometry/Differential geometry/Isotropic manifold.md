---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Isotropic_manifold
offline_file: ""
offline_thumbnail: ""
uuid: 97246dea-87c0-40ca-a27c-847e72bc07ee
updated: 1484309226
title: Isotropic manifold
categories:
    - Differential geometry
---
In mathematics, an isotropic manifold is a manifold in which the geometry does not depend on directions. Formally, we say that a Riemannian manifold 
  
    
      
        (
        M
        ,
        g
        )
      
    
    {\displaystyle (M,g)}
  
 is isotropic if for any point 
  
    
      
        p
        ∈
        M
      
    
    {\displaystyle p\in M}
  
 and unit vectors 
  
    
      
        v
        ,
        w
        ∈
        
          T
          
            p
          
        
        M
      
    
    {\displaystyle v,w\in T_{p}M}
  
, there is an isometry 
  
    
      
        φ
      
    
    {\displaystyle \varphi }
  
 of 
  
    
      
        ...
