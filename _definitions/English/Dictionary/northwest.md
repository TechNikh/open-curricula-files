---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/northwest
offline_file: ""
offline_thumbnail: ""
uuid: b4495eba-ebf2-4b9f-9f0d-ed68c073f8d9
updated: 1484310305
title: northwest
categories:
    - Dictionary
---
north-west
     adv : to, toward, or in the northwest [syn: {northwest}, {nor'-west}]
