---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/firm
offline_file: ""
offline_thumbnail: ""
uuid: ea87ab6e-defc-4441-a661-546fa73f234d
updated: 1484310543
title: firm
categories:
    - Dictionary
---
firm
     adj 1: marked by firm determination or resolution; not shakable;
            "firm convictions"; "a firm mouth"; "steadfast
            resolve"; "a man of unbendable perseverence";
            "unwavering loyalty" [syn: {steadfast}, {steady}, {unbendable},
             {unfaltering}, {unshakable}, {unwavering}]
     2: not soft or yielding to pressure; "a firm mattress"; "the
        snow was firm underfoot"; "solid ground" [syn: {solid}]
     3: strong and sure; "a firm grasp"; "gave a strong pull on the
        rope" [syn: {strong}]
     4: not subject to revision or change; "a firm contract"; "a
        firm offer"
     5: (of especially a person's physical features) not ...
