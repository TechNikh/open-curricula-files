---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unjust
offline_file: ""
offline_thumbnail: ""
uuid: 7abc4bed-b828-4cbb-99ba-5e945ed5c7fe
updated: 1484310545
title: unjust
categories:
    - Dictionary
---
unjust
     adj 1: not fair; marked by injustice or partiality or deception;
            "used unfair methods"; "it was an unfair trial"; "took
            an unfair advantage" [syn: {unfair}] [ant: {fair}]
     2: violating principles of justice; "unjust punishment"; "an
        unjust judge"; "an unjust accusation" [ant: {just}]
     3: not equitable or fair; "the inequitable division of wealth";
        "inequitable taxation" [syn: {inequitable}] [ant: {equitable}]
     4: not righteous; "`unjust' is an archaic term for
        `unrighteous'"
