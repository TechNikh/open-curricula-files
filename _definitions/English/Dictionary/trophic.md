---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trophic
offline_file: ""
offline_thumbnail: ""
uuid: 0d87edf9-1db4-482e-9632-7c2b0adb8978
updated: 1484310271
title: trophic
categories:
    - Dictionary
---
trophic
     adj : of or relating to nutrition; "a trophic level on the food
           chain"
