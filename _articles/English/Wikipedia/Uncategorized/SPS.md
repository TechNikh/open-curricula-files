---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sps
offline_file: ""
offline_thumbnail: ""
uuid: fa510995-814a-4782-a96e-2be837c2219c
updated: 1484308447
title: SPS
tags:
    - City
    - Computing
    - Educational, scholarly, and professional institutions
    - Medicine and psychology
    - Politics, treaties, political parties, and government uses
    - Science and engineering
    - Other uses
categories:
    - Uncategorized
---
The abbreviation SPS may stand for:
