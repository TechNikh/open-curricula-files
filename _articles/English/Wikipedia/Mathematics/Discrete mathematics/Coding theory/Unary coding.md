---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Unary_coding
offline_file: ""
offline_thumbnail: ""
uuid: effc2599-131e-4306-a1d7-131cd3e86192
updated: 1484308562
title: Unary coding
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/600px-UN_emblem_blue.svg.png
tags:
    - Unary code in use today
    - Unary coding in biological networks
    - Generalized unary coding
categories:
    - Coding theory
---
Unary coding, sometimes called thermometer code, is an entropy encoding that represents a natural number, n, with n ones followed by a zero (if natural number is understood as non-negative integer) or with n − 1 ones followed by a zero (if natural number is understood as strictly positive integer). For example 5 is represented as 111110 or 11110. Some representations use n or n − 1 zeros followed by a one. The ones and zeros are interchangeable without loss of generality. Unary coding is both a Prefix-free code and a Self-synchronizing code.
