---
version: 1
type: video
provider: YouTube
id: FXSuEIMrPQk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Inflammatory%20Response.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 09b0790d-74d3-4c6d-82f8-9912d54578e9
updated: 1486069583
title: Inflammatory Response
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/FXSuEIMrPQk/default.jpg
    - https://i3.ytimg.com/vi/FXSuEIMrPQk/1.jpg
    - https://i3.ytimg.com/vi/FXSuEIMrPQk/2.jpg
    - https://i3.ytimg.com/vi/FXSuEIMrPQk/3.jpg
---
Overview of the inflammatory response
More free lessons at: http://www.khanacademy.org/video?v=FXSuEIMrPQk
