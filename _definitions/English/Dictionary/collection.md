---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collection
offline_file: ""
offline_thumbnail: ""
uuid: 78844d3f-f5de-41b9-b074-27863b25b000
updated: 1484310365
title: collection
categories:
    - Dictionary
---
collection
     n 1: several things grouped together or considered as a whole
          [syn: {aggregation}, {accumulation}, {assemblage}]
     2: a publication containing a variety of works [syn: {compendium}]
     3: request for a sum of money; "an appeal to raise money for
        starving children" [syn: {solicitation}, {appeal}, {ingathering}]
     4: the act of gathering something together [syn: {collecting},
        {assembling}, {aggregation}]
