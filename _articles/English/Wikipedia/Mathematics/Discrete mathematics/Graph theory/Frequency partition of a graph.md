---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frequency_partition_of_a_graph
offline_file: ""
offline_thumbnail: ""
uuid: 8240c508-5292-43e2-bed9-b1d55ecb15c9
updated: 1484308605
title: Frequency partition of a graph
tags:
    - Frequency partitions of Eulerian graphs
    - >
        Frequency partition of trees, Hamiltonian graphs,
        tournaments and hypegraphs
    - Unsolved problems in frequency partitions
    - External section
categories:
    - Graph theory
---
In graph theory, a discipline within mathematics, the frequency partition of a graph (simple graph) is a partition of its vertices grouped by their degree. For example, the degree sequence of the left-hand graph below is (3, 3, 3, 2, 2, 1) and its frequency partition is 6 = 3 + 2 + 1. This indicates that it has 3 vertices with some degree, 2 vertices with some other degree, and 1 vertex with a third degree. The degree sequence of the bipartite graph in the middle below is (3, 2, 2, 2, 2, 2, 1, 1, 1) and its frequency partition is 9 = 5 + 3 + 1. The degree sequence of the right-hand graph below is (3, 3, 3, 3, 3, 3, 2) and its frequency partition is 7 = 6 + 1.
