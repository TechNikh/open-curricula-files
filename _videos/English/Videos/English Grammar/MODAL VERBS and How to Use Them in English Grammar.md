---
version: 1
type: video
provider: YouTube
id: cbc_TzxcNWs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: de1019f7-161f-4975-a4ed-0736f1a309ee
updated: 1486069632
title: MODAL VERBS and How to Use Them in English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/cbc_TzxcNWs/default.jpg
    - https://i3.ytimg.com/vi/cbc_TzxcNWs/1.jpg
    - https://i3.ytimg.com/vi/cbc_TzxcNWs/2.jpg
    - https://i3.ytimg.com/vi/cbc_TzxcNWs/3.jpg
---
