---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/genotypic
offline_file: ""
offline_thumbnail: ""
uuid: 471afffc-4085-400b-97c9-b5605f91aeac
updated: 1484310301
title: genotypic
categories:
    - Dictionary
---
genotypic
     adj : of or relating to or constituting a genotype; "genotypical
           pattern" [syn: {genotypical}]
