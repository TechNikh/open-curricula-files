---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/modify
offline_file: ""
offline_thumbnail: ""
uuid: 54fbd225-d8bf-4240-8171-c69b596d02d5
updated: 1484310196
title: modify
categories:
    - Dictionary
---
modify
     v 1: make less severe or harsh or extreme; "please modify this
          letter to make it more polite"; "he modified his views
          on same-gender marriage"
     2: add a modifier to a constituent [syn: {qualify}]
     3: cause to change; make different; cause a transformation;
        "The advent of the automobile may have altered the growth
        pattern of the city"; "The discussion has changed my
        thinking about the issue" [syn: {change}, {alter}]
     [also: {modified}]
