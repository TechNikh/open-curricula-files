---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transparency
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484547601
title: transparency
categories:
    - Dictionary
---
transparency
     n 1: permitting the free passage of electromagnetic radiation
          [syn: {transparence}] [ant: {opacity}]
     2: the quality of being clear and transparent [syn: {transparence},
         {transparentness}]
     3: picture consisting of a positive photograph or drawing on a
        transparent base; viewed with a projector [syn: {foil}]
