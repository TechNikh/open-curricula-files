---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/intro
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484529481
title: intro
categories:
    - Dictionary
---
intro
     n 1: formally making a person known to another or to the public
          [syn: {presentation}, {introduction}]
     2: a brief introductory passage to a piece of popular music
