---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/priest
offline_file: ""
offline_thumbnail: ""
uuid: 0c0970b6-2fe6-49bd-9f81-16bbdb9d00ee
updated: 1484310451
title: priest
categories:
    - Dictionary
---
priest
     n 1: a clergyman in Christian churches who has the authority to
          perform or administer various religious rites; one of
          the Holy Orders
     2: a spiritual leader in a non-Christian religion [syn: {non-Christian
        priest}]
