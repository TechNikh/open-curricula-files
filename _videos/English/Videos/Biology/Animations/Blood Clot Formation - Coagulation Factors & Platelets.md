---
version: 1
type: video
provider: YouTube
id: _yQD0U3ZtCs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 55cd730b-360f-457a-b05d-fe948eca87b2
updated: 1486069670
title: 'Blood Clot Formation - Coagulation Factors & Platelets'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/_yQD0U3ZtCs/default.jpg
    - https://i3.ytimg.com/vi/_yQD0U3ZtCs/1.jpg
    - https://i3.ytimg.com/vi/_yQD0U3ZtCs/2.jpg
    - https://i3.ytimg.com/vi/_yQD0U3ZtCs/3.jpg
---
