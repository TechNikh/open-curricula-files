---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Domestic_cat
offline_file: ""
offline_thumbnail: ""
uuid: 019ef5fc-1d24-4d85-9488-632376b7af8e
updated: 1484308363
title: Cat
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-AfricanWildCat.jpg
tags:
    - Taxonomy and evolution
    - Nomenclature and etymology
    - Biology
    - Anatomy
    - Physiology
    - Senses
    - Health
    - Diseases
    - Poisoning
    - Genetics
    - Behavior
    - Sociability
    - Communication
    - Grooming
    - Fighting
    - Hunting and feeding
    - Play
    - Reproduction
    - Ecology
    - habitats
    - Feral cats
    - Impact on prey species
    - Impact on birds
    - Interaction with humans
    - Infections transmitted from cats to humans
    - History and mythology
    - Depictions in art
categories:
    - Uncategorized
---
The domestic cat[1][5] (Latin: Felis catus) is a small, typically furry, carnivorous mammal. They are often called house cats when kept as indoor pets or simply cats when there is no need to distinguish them from other felids and felines.[6] Cats are often valued by humans for companionship and for their ability to hunt vermin. There are more than 70 cat breeds; different associations proclaim different numbers according to their standards.
