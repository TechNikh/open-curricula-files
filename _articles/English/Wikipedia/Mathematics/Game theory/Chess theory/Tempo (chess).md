---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Tempo_(chess)
offline_file: ""
offline_thumbnail: ""
uuid: 662d034e-4f5a-42d3-84a4-51a9a4880bf3
updated: 1484309026
title: Tempo (chess)
tags:
    - Gaining a tempo
    - Losing a tempo
    - Spare tempo
    - Reserve tempo
categories:
    - Chess theory
---
In chess, tempo refers to a "turn" or single move. When a player achieves a desired result in one fewer move, the player "gains a tempo"; and conversely when a player takes one more move than necessary, the player "loses a tempo". Similarly, when a player forces their opponent to make moves not according to their initial plan, one "gains tempo" because the opponent wastes moves. A move that gains a tempo is often called a move "with tempo".
