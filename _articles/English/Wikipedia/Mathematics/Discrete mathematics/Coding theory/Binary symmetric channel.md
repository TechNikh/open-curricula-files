---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Binary_symmetric_channel
offline_file: ""
offline_thumbnail: ""
uuid: 910469b1-6f9c-4a29-8f44-2755c1f97b37
updated: 1484308547
title: Binary symmetric channel
tags:
    - Description
    - Definition
    - Capacity of BSCp
    - "Shannon's channel capacity theorem for BSCp"
    - Noisy coding theorem for BSCp
    - "Converse of Shannon's capacity theorem"
    - Codes for BSCp
    - "Forney's code for BSCp"
    - 'Decoding error probability for C*'
    - Notes
categories:
    - Coding theory
---
A binary symmetric channel (or BSC) is a common communications channel model used in coding theory and information theory. In this model, a transmitter wishes to send a bit (a zero or a one), and the receiver receives a bit. It is assumed that the bit is usually transmitted correctly, but that it will be "flipped" with a small probability (the "crossover probability"). This channel is used frequently in information theory because it is one of the simplest channels to analyze.
