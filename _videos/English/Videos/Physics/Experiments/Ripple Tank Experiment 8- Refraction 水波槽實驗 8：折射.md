---
version: 1
type: video
provider: YouTube
id: 8PQOlHs8Z7Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 809a20bb-6334-4c62-9a23-da90be084c2c
updated: 1486069635
title: 'Ripple Tank Experiment 8- Refraction 水波槽實驗 8：折射'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/8PQOlHs8Z7Q/default.jpg
    - https://i3.ytimg.com/vi/8PQOlHs8Z7Q/1.jpg
    - https://i3.ytimg.com/vi/8PQOlHs8Z7Q/2.jpg
    - https://i3.ytimg.com/vi/8PQOlHs8Z7Q/3.jpg
---
