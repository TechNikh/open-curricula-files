---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Normal_form_(bifurcation_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 8a3bde54-094f-4ef2-8b40-6b6569124578
updated: 1484308705
title: Normal form (bifurcation theory)
categories:
    - Bifurcation theory
---
Normal forms are defined for local bifurcations. It is assumed that the system is first reduced to the center manifold of the equilibrium point at which the bifurcation takes place. The reduced system is then locally (around the equilibrium) topologically equivalent to the normal form of the bifurcation.
