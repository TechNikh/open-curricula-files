---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fao
offline_file: ""
offline_thumbnail: ""
uuid: f89cf1f8-8d04-4bef-8baf-eb54b3df8747
updated: 1484310163
title: fao
categories:
    - Dictionary
---
FAO
     n : the United Nations agency concerned with the international
         organization of food and agriculture [syn: {Food and
         Agriculture Organization}, {Food and Agriculture
         Organization of the United Nations}]
