---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/romance
offline_file: ""
offline_thumbnail: ""
uuid: ffc269e6-0d56-40af-938b-213d5cb7ee78
updated: 1484310569
title: romance
categories:
    - Dictionary
---
Romance
     adj : relating to languages derived from Latin; "Romance
           languages" [syn: {Latin}]
     n 1: a relationship between two lovers [syn: {love affair}]
     2: an exciting and mysterious quality (as of a heroic time or
        adventure) [syn: {romanticism}]
     3: the group of languages derived from Latin [syn: {Romance
        language}, {Latinian language}]
     4: a story dealing with love [syn: {love story}]
     5: a novel dealing with idealized events remote from everyday
        life
     v 1: make amorous advances towards; "John is courting Mary" [syn:
           {woo}, {court}, {solicit}]
     2: have a love affair with
     3: talk or behave amorously, ...
