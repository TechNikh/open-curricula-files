---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irritable
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484514481
title: irritable
categories:
    - Dictionary
---
irritable
     adj 1: easily irritated or annoyed; "an incorrigibly fractious
            young man"; "not the least nettlesome of his
            countrymen" [syn: {cranky}, {fractious}, {nettlesome},
             {peevish}, {peckish}, {pettish}, {petulant}, {testy},
             {tetchy}, {techy}]
     2: abnormally sensitive to a stimulus
     3: capable of responding to stimuli [syn: {excitable}]
