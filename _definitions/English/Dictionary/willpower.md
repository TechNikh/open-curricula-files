---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/willpower
offline_file: ""
offline_thumbnail: ""
uuid: e38a140e-6c58-4530-af76-eb1e57effc5d
updated: 1484310186
title: willpower
categories:
    - Dictionary
---
willpower
     n : the trait of resolutely controlling your own behavior [syn:
         {self-control}, {self-possession}, {possession}, {self-command},
          {self-will}]
