---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Implicit_function
offline_file: ""
offline_thumbnail: ""
uuid: 110d3b6d-d799-4653-b33b-0e959f70f5cd
updated: 1484309159
title: Implicit function
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Implicit_circle.svg.png
tags:
    - Examples
    - Inverse functions
    - Algebraic functions
    - Caveats
    - Implicit differentiation
    - Examples
    - General formula for derivative of implicit function
    - Implicit function theorem
    - In algebraic geometry
    - In differential equations
    - Applications in economics
    - Marginal rate of substitution
    - Marginal rate of technical substitution
    - Optimization
categories:
    - Algebraic geometry
---
In mathematics, an implicit equation is a relation of the form 
  
    
      
        R
        (
        
          x
          
            1
          
        
        ,
        …
        ,
        
          x
          
            n
          
        
        )
        =
        0
      
    
    {\displaystyle R(x_{1},\ldots ,x_{n})=0}
  
, where 
  
    
      
        R
      
    
    {\displaystyle R}
  
 is a function of several variables (often a polynomial). For example, the implicit equation of the unit circle is 
  
    
      
        
          x
          
            2
          
        
        +
        
          y
          
            2
          
        
   ...
