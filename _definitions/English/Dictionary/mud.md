---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mud
offline_file: ""
offline_thumbnail: ""
uuid: cc161fe7-bde7-4c72-8d74-b80d1a803a0a
updated: 1484310281
title: mud
categories:
    - Dictionary
---
mud
     n 1: water soaked soil; soft wet earth [syn: {clay}]
     2: slanderous remarks or charges
     v 1: soil with mud, muck, or mire; "The child mucked up his shirt
          while playing ball in the garden" [syn: {mire}, {muck},
          {muck up}]
     2: plaster with mud
     [also: {mudding}, {mudded}]
