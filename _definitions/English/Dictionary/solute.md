---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/solute
offline_file: ""
offline_thumbnail: ""
uuid: eb98fda3-2b50-4b09-8cdd-7a7d4c721ae8
updated: 1484310407
title: solute
categories:
    - Dictionary
---
solute
     n : the dissolved substance in a solution; the component of a
         solution that changes its state
