---
version: 1
type: article
id: https://en.wikipedia.org/wiki/St-connectivity
offline_file: ""
offline_thumbnail: ""
uuid: 8030c8ed-2a4e-43ef-a9f2-67e676d09cd3
updated: 1484309404
title: St-connectivity
categories:
    - Graph connectivity
---
In computer science and computational complexity theory, st-connectivity or STCON is a decision problem asking, for vertices s and t in a directed graph, if t is reachable from s.
