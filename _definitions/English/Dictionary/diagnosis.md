---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diagnosis
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484464321
title: diagnosis
categories:
    - Dictionary
---
diagnosis
     n : identifying the nature or cause of some phenomenon [syn: {diagnosing}]
     [also: {diagnoses} (pl)]
