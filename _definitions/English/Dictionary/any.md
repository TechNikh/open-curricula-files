---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/any
offline_file: ""
offline_thumbnail: ""
uuid: 73efa5b2-5715-4627-b108-bf935103facd
updated: 1484310359
title: any
categories:
    - Dictionary
---
any
     adj 1: (in negative statements) either every little or very great
            but unspecified; "can't stand any noise"; "could not
            endure chemotherapy for any length of time" [syn: {any(a)}]
     2: one or some or every or all without specification; "give me
        any peaches you don't want"; "not any milk is left"; "any
        child would know that"; "pick any card"; "any day now";
        "cars can be rented at almost any airport"; "at twilight
        or any other time"; "beyond any doubt"; "need any help we
        can get"; "give me whatever peaches you don't want"; "no
        milk whatsoever is left" [syn: {any(a)}, {whatever}, {whatsoever}]
     adv : to any ...
