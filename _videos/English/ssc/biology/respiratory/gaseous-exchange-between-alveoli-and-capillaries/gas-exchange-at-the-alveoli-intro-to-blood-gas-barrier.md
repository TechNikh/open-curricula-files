---
title: Gas Exchange at the Alveoli-Intro to Blood Gas Barrier
youtube_id: HR58DOK4YRk
tags:
    - SSC
    - Biology
    - Respiration
categories:
    - gaseous-exchange-between-alveoli-and-capillaries
---
