---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/appreciable
offline_file: ""
offline_thumbnail: ""
uuid: 15312180-00d1-4e96-932c-d4e18b318e55
updated: 1484310228
title: appreciable
categories:
    - Dictionary
---
appreciable
     adj : enough to be estimated or measured; "appreciable amounts of
           noxious wastes are dumped into the harbor"
