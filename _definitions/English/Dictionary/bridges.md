---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bridges
offline_file: ""
offline_thumbnail: ""
uuid: 9f935539-7e91-491f-9e8b-8cb7d416d859
updated: 1484310379
title: bridges
categories:
    - Dictionary
---
Bridges
     n : United States labor leader who organized the longshoremen
         (1901-1990) [syn: {Harry Bridges}]
