---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aerated
offline_file: ""
offline_thumbnail: ""
uuid: 5c1920af-ab1e-4cee-a135-dadc5c99540d
updated: 1484310384
title: aerated
categories:
    - Dictionary
---
aerated
     adj 1: (of a liquid) treated by having air passed or bubbled
            through it for purification
     2: used of tissues or especially blood; supplied with oxygen by
        respiration [syn: {oxygenated}]
     3: supplied with carbon dioxide [syn: {charged}]
