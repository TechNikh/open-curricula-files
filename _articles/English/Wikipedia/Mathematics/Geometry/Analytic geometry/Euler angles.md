---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euler_angles
offline_file: ""
offline_thumbnail: ""
uuid: 0980c548-3e3e-44fb-87bc-548b450c690e
updated: 1484309162
title: Euler angles
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Eulerangles.svg.png
tags:
    - Proper Euler angles
    - Geometrical definition
    - Definition by intrinsic rotations
    - Definition by extrinsic rotations
    - Conventions
    - Signs and ranges
    - Precession, nutation and intrinsic rotation
    - Tait–Bryan angles
    - Definitions
    - Conventions
    - Signs and ranges
    - Alternative names
    - Angles of a given frame
    - Proper Euler angles
    - Tait-Bryan angles
    - Last remarks
    - Conversion to other orientation representations
    - Rotation matrix
    - Properties
    - Geometric algebra
    - Higher dimensions
    - Applications
    - Vehicles and moving frames
    - Crystallographic texture
    - Others
    - Bibliography
categories:
    - Analytic geometry
---
The Euler angles are three angles introduced by Leonhard Euler to describe the orientation of a rigid body respect a fixed coordinate system.[1] They can also represent the orientation of a mobile frame of reference in physics or the orientation of a general basis in 3-dimensional linear algebra.
