---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/punctuation
offline_file: ""
offline_thumbnail: ""
uuid: 1e64d122-5148-44b8-aa1b-88b56832e945
updated: 1484310421
title: punctuation
categories:
    - Dictionary
---
punctuation
     n 1: something that makes repeated and regular interruptions or
          divisions
     2: the marks used to clarify meaning by indicating separation
        of words into sentences and clauses and phrases [syn: {punctuation
        mark}]
     3: the use of certain marks to clarify meaning of written
        material by grouping words grammatically into sentences
        and clauses and phrases
