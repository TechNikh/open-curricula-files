---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cartesian
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484313721
title: cartesian
categories:
    - Dictionary
---
Cartesian
     adj : of or relating to Rene Descartes or his works; "Cartesian
           linguistics"
