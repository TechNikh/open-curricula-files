---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Complex_convexity
offline_file: ""
offline_thumbnail: ""
uuid: 51fff67e-ad3f-42aa-bcc7-26956bddac69
updated: 1484309357
title: Complex convexity
categories:
    - Geometry stubs
---
A set 
  
    
      
        Ω
      
    
    {\displaystyle \Omega }
  
 in 
  
    
      
        
          
            C
          
          
            n
          
        
      
    
    {\displaystyle \mathbb {C} ^{n}}
  
 is called 
  
    
      
        
          C
        
      
    
    {\displaystyle \mathbb {C} }
  
-convex if its intersection with any complex line is contractible.[1]
