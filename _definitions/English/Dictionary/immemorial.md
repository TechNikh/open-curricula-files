---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/immemorial
offline_file: ""
offline_thumbnail: ""
uuid: c25d5816-d622-4a36-b0ac-9f9ded7e121d
updated: 1484310174
title: immemorial
categories:
    - Dictionary
---
immemorial
     adj : long past; beyond the limits of memory or tradition or
           recorded history; "time immemorial" [syn: {immemorial(ip)}]
