---
version: 1
type: video
provider: YouTube
id: m77N-ey4zlY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/How%20Blood%20Clots-%20-%20Blood%20Clotting%20Mechanism%20Animation%20-%20Blood%20Clot%20Formation%20Process.webm"
offline_file: ""
offline_thumbnail: ""
uuid: d767b450-ea46-44ef-bc44-039f7160bb48
updated: 1486069668
title: 'How Blood Clots? - Blood Clotting Mechanism Animation - Blood Clot Formation Process'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/m77N-ey4zlY/default.jpg
    - https://i3.ytimg.com/vi/m77N-ey4zlY/1.jpg
    - https://i3.ytimg.com/vi/m77N-ey4zlY/2.jpg
    - https://i3.ytimg.com/vi/m77N-ey4zlY/3.jpg
---
