---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/elevated
offline_file: ""
offline_thumbnail: ""
uuid: b86d9412-0d60-44de-a710-d13835e0a8fb
updated: 1484310439
title: elevated
categories:
    - Dictionary
---
elevated
     adj 1: raised above ground level; on elevated rails; "elevated
            railway"
     2: raised above the ground; "an elevated platform"
