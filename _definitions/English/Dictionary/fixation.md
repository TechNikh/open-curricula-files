---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fixation
offline_file: ""
offline_thumbnail: ""
uuid: dc9b71e9-8ab3-40c7-b95b-cde378fa8d22
updated: 1484310271
title: fixation
categories:
    - Dictionary
---
fixation
     n 1: an abnormal state in which development has stopped
          prematurely [syn: {arrested development}, {infantile
          fixation}, {regression}]
     2: an unhealthy and compulsive preoccupation with something or
        someone [syn: {obsession}]
     3: the activity of fastening something firmly in position
     4: (histology) the preservation and hardening of a tissue
        sample to retain as nearly as possible the same relations
        they had in the living body [syn: {fixing}]
