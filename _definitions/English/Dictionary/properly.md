---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/properly
offline_file: ""
offline_thumbnail: ""
uuid: bb270914-5692-42ba-8718-7be999d73be1
updated: 1484310328
title: properly
categories:
    - Dictionary
---
properly
     adv 1: in the right manner; "please do your job properly!"; "can't
            you carry me decent?" [syn: {decently}, {decent}, {in
            good order}, {right}, {the right way}] [ant: {improperly}]
     2: with reason or justice [syn: {by rights}]
