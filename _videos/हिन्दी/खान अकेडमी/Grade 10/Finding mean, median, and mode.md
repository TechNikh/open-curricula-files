---
version: 1
type: video
provider: YouTube
id: 7Gg107jiyi4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: faada33c-75d2-4d24-b015-767f67955c51
updated: 1486069618
title: Finding mean, median, and mode
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/7Gg107jiyi4/default.jpg
    - https://i3.ytimg.com/vi/7Gg107jiyi4/1.jpg
    - https://i3.ytimg.com/vi/7Gg107jiyi4/2.jpg
    - https://i3.ytimg.com/vi/7Gg107jiyi4/3.jpg
---
