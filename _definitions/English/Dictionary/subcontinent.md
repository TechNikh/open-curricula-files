---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subcontinent
offline_file: ""
offline_thumbnail: ""
uuid: 97194e97-13ad-40ae-9248-f0458caf7ca1
updated: 1484310439
title: subcontinent
categories:
    - Dictionary
---
subcontinent
     n : a large and distinctive landmass (as India or Greenland)
         that is a distinct part of some continent
