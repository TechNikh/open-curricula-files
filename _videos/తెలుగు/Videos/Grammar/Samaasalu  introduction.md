---
version: 1
type: video
provider: YouTube
id: nUS8IyoCOjo
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 3268f378-3eed-4eae-8a5c-9b363728874b
updated: 1486069626
title: 'Samaasalu  introduction'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/nUS8IyoCOjo/default.jpg
    - https://i3.ytimg.com/vi/nUS8IyoCOjo/1.jpg
    - https://i3.ytimg.com/vi/nUS8IyoCOjo/2.jpg
    - https://i3.ytimg.com/vi/nUS8IyoCOjo/3.jpg
---
