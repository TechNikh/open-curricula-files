---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Presburger_arithmetic
offline_file: ""
offline_thumbnail: ""
uuid: 65881d35-9bac-4583-9143-90276eb79f79
updated: 1484309497
title: Presburger arithmetic
tags:
    - Overview
    - Properties
    - Applications
    - Presburger-definable integer relation
    - "Muchnik's characterization"
categories:
    - Proof theory
---
Presburger arithmetic is the first-order theory of the natural numbers with addition, named in honor of Mojżesz Presburger, who introduced it in 1929. The signature of Presburger arithmetic contains only the addition operation and equality, omitting the multiplication operation entirely. The axioms include a schema of induction.
