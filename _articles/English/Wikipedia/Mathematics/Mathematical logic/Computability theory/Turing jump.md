---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Turing_jump
offline_file: ""
offline_thumbnail: ""
uuid: 8e45ef3a-6fe3-4265-ac12-6782850fdd16
updated: 1484309490
title: Turing jump
tags:
    - Definition
    - Examples
    - Properties
categories:
    - Computability theory
---
In computability theory, the Turing jump or Turing jump operator, named for Alan Turing, is an operation that assigns to each decision problem X a successively harder decision problem X ′ with the property that X ′ is not decidable by an oracle machine with an oracle for X.
