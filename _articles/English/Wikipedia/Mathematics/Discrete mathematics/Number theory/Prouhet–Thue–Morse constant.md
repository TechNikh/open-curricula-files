---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Prouhet%E2%80%93Thue%E2%80%93Morse_constant'
offline_file: ""
offline_thumbnail: ""
uuid: 1464b2cc-3126-4c58-ae79-3542a04d1894
updated: 1484308654
title: Prouhet–Thue–Morse constant
categories:
    - Number theory
---
In mathematics, the Prouhet–Thue–Morse constant, named for Eugène Prouhet, Axel Thue, and Marston Morse, is the number—denoted by 
  
    
      
        τ
      
    
    {\displaystyle \tau }
  
—whose binary expansion .01101001100101101001011001101001... is given by the Thue–Morse sequence. That is,
