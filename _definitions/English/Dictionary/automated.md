---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/automated
offline_file: ""
offline_thumbnail: ""
uuid: 0cb3db71-996d-472f-9d21-4d5ade5884e8
updated: 1484310521
title: automated
categories:
    - Dictionary
---
automated
     adj : operated by automation; "an automated stoker" [syn: {machine-controlled},
            {machine-driven}]
