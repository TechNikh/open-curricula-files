---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/integral
offline_file: ""
offline_thumbnail: ""
uuid: 1b59a82f-508e-4c97-89cc-f381bf87c5fd
updated: 1484310174
title: integral
categories:
    - Dictionary
---
integral
     adj 1: existing as an essential constituent or characteristic; "the
            Ptolemaic system with its built-in concept of
            periodicity"; "a constitutional inability to tell the
            truth" [syn: {built-in}, {constitutional}, {inbuilt},
            {inherent}]
     2: constituting the undiminished entirety; lacking nothing
        essential especially not damaged; "a local motion keepeth
        bodies integral"- Bacon; "was able to keep the collection
        entire during his lifetime"; "fought to keep the union
        intact" [syn: {entire}, {intact}]
     n : the result of a mathematical integration; F(x) is the
         integral of f(x) if dF/dx = ...
