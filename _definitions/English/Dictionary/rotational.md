---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rotational
offline_file: ""
offline_thumbnail: ""
uuid: 8ca01f08-ebee-4af8-bb38-d9239aa9e190
updated: 1484310230
title: rotational
categories:
    - Dictionary
---
rotational
     adj : of or pertaining to rotation; "rotational inertia"
