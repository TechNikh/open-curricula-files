---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/matched
offline_file: ""
offline_thumbnail: ""
uuid: e443d7a3-06b6-4914-8ce1-da2d0f9e4f55
updated: 1484310599
title: matched
categories:
    - Dictionary
---
matched
     adj 1: provided with a worthy adversary or competitor; "matched
            teams"
     2: going well together; possessing harmonizing qualities [ant:
        {mismatched}]
