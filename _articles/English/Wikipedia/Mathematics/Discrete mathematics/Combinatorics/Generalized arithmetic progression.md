---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Generalized_arithmetic_progression
offline_file: ""
offline_thumbnail: ""
uuid: bc57e3bc-88e0-4178-abae-4b48c23e42e4
updated: 1484308582
title: Generalized arithmetic progression
categories:
    - Combinatorics
---
In mathematics, a multiple arithmetic progression, generalized arithmetic progression, k-dimensional arithmetic progression or a linear set, is a set of integers or tuples of integers constructed as an arithmetic progression is, but allowing several possible differences. So, for example, we start at 17 and may add a multiple of 3 or of 5, repeatedly. In algebraic terms we look at integers
