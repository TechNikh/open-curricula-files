---
version: 1
type: video
provider: YouTube
id: tIGnMK4YtlE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 4ad4077b-2bd6-4a68-8926-21844ae4e732
updated: 1486069639
title: Experiments with polarizers
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/tIGnMK4YtlE/default.jpg
    - https://i3.ytimg.com/vi/tIGnMK4YtlE/1.jpg
    - https://i3.ytimg.com/vi/tIGnMK4YtlE/2.jpg
    - https://i3.ytimg.com/vi/tIGnMK4YtlE/3.jpg
---
