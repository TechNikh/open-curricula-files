---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Set_TSP_problem
offline_file: ""
offline_thumbnail: ""
uuid: 291eb9c5-7ef8-4695-a05a-6420239db303
updated: 1484309398
title: Set TSP problem
categories:
    - Computational problems in graph theory
---
In combinatorial optimization, the set TSP, also known as the, generalized TSP, group TSP, One-of-a-Set TSP, Multiple Choice TSP or Covering Salesman Problem, is a generalization of the Traveling salesman problem (TSP), whereby it is required to find a shortest tour in a graph which visits all specified subsets of the vertices of a graph. The ordinary TSP is a special case of the set TSP when all subsets to be visited are singletons. Therefore the set TSP is also NP-hard.
