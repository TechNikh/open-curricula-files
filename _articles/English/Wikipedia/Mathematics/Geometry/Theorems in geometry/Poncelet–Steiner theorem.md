---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Poncelet%E2%80%93Steiner_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: d752c884-2ce4-4d10-a8e6-d4640a5470ac
updated: 1484309127
title: Poncelet–Steiner theorem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Steiner_construction_of_a_parallel_to_a_diameter.gif
tags:
    - History
    - Notes
categories:
    - Theorems in geometry
---
In Euclidean geometry, the Poncelet–Steiner theorem concerning compass and straightedge constructions states that whatever can be constructed by straightedge and compass together can be constructed by straightedge alone, provided that a single circle and its centre are given. This result can not be weakened; if the centre of the circle is not given, it cannot be constructed by a straightedge alone. Also, the entire circle is not required. In 1904, Francesco Severi proved that any small arc together with the centre will suffice.[1]
