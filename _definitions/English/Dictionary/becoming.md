---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/becoming
offline_file: ""
offline_thumbnail: ""
uuid: d517b4a2-9770-4b51-abf3-8313ca8422f1
updated: 1484310254
title: becoming
categories:
    - Dictionary
---
becoming
     adj 1: according with custom or propriety; "her becoming modesty";
            "comely behavior"; "it is not comme il faut for a
            gentleman to be constantly asking for money"; "a
            decent burial"; "seemly behavior" [syn: {comely}, {comme
            il faut}, {decent}, {decorous}, {seemly}]
     2: displaying or setting off to best advantage; "a becoming new
        shade of rose"; "a becoming portrait"
