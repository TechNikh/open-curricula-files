---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermal_transmittance
offline_file: ""
offline_thumbnail: ""
uuid: d1a50a98-dbbb-4772-a2d0-5bf6db465ec9
updated: 1484308870
title: Thermal transmittance
categories:
    - Thermodynamics
---
Thermal transmittance, also known as U-value, is the rate of transfer of heat (in watts) through one square metre of a structure, divided by the difference in temperature across the structure. It is expressed in watts per square metre kelvin, or W/m²K. Well-insulated parts of a building have a low thermal transmittance whereas poorly insulated parts of a building have a high thermal transmittance. Losses due to thermal radiation, thermal convection and thermal conduction are taken into account in the U-value. Although it has the same units as heat transfer coefficient, thermal transmittance is different in that the heat transfer coefficient is used to solely describe heat transfer in ...
