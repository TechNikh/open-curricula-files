---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disturbance
offline_file: ""
offline_thumbnail: ""
uuid: b31e7dc3-ba1e-4d9c-8a83-d6ad1323393e
updated: 1484310238
title: disturbance
categories:
    - Dictionary
---
disturbance
     n 1: activity that is an intrusion or interruption; "he looked
          around for the source of the disturbance"; "there was a
          disturbance of neural function" [syn: {perturbation}]
     2: an unhappy and worried mental state; "there was too much
        anger and disturbance"; "she didn't realize the upset she
        caused me" [syn: {perturbation}, {upset}]
     3: a disorderly outburst or tumult; "they were amazed by the
        furious disturbance they had caused" [syn: {disruption}, {commotion},
         {stir}, {flutter}, {hurly burly}, {to-do}, {hoo-ha}, {hoo-hah},
         {kerfuffle}]
     4: a noisy fight [syn: {affray}, {fray}, {ruffle}]
     5: the ...
