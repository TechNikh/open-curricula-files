---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/backed
offline_file: ""
offline_thumbnail: ""
uuid: 2c3a4a1c-deab-48ca-a9b0-8df31e260a00
updated: 1484310577
title: backed
categories:
    - Dictionary
---
backed
     adj 1: having a back or backing, usually of a specified type [ant:
            {backless}]
     2: having backing; "a claim backed up by strong evidence" [syn:
         {backed up}]
     3: used of film that is coated on the side opposite the
        emulsion with a substance to absorb light
