---
version: 1
type: video
provider: YouTube
id: s9HPNwXd9fk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Biology/Chromosomes%2C%20Chromatids%2C%20Chromatin%2C%20etc..webm"
offline_file: ""
offline_thumbnail: ""
uuid: 67cd2390-ab46-47d5-980f-c5305c207587
updated: 1486069583
title: Chromosomes, Chromatids, Chromatin, etc.
categories:
    - Biology
thumbnail_urls:
    - https://i3.ytimg.com/vi/s9HPNwXd9fk/default.jpg
    - https://i3.ytimg.com/vi/s9HPNwXd9fk/1.jpg
    - https://i3.ytimg.com/vi/s9HPNwXd9fk/2.jpg
    - https://i3.ytimg.com/vi/s9HPNwXd9fk/3.jpg
---
The vocabulary of DNA: chromosomes, chromatids, chromatin, transcription, translation, and replication
More free lessons at: http://www.khanacademy.org/video?v=s9HPNwXd9fk
