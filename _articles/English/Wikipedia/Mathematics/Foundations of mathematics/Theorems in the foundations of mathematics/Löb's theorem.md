---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/L%C3%B6b%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 5fbacd12-6717-480b-a150-fe50f8566927
updated: 1484308994
title: "Löb's theorem"
tags:
    - "Löb's theorem in provability logic"
    - "Modal Proof of Löb's theorem"
    - Modal Formulas
    - Modal Fixed Points
    - Modal Rules of Inference
    - "Proof of Löb's theorem"
    - Examples
    - "Converse: Löb's theorem implies the existence of modal fixed points"
categories:
    - Theorems in the foundations of mathematics
---
In mathematical logic, Löb's theorem states that in any formal system F with Peano arithmetic (PA), for any formula P, if it is provable in F that "if P is provable in F then P is true", then P is provable in F. More formally, if Bew(#P) means that the formula P with Gödel number #P is provable (from the German "beweisbar"), then
