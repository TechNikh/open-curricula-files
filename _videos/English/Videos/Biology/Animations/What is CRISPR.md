---
version: 1
type: video
provider: YouTube
id: MnYppmstxIs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/What%20is%20CRISPR.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 6ccdf6c2-f7cb-4c9c-bee3-cbcdbd885989
updated: 1486069670
title: What is CRISPR?
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/MnYppmstxIs/default.jpg
    - https://i3.ytimg.com/vi/MnYppmstxIs/1.jpg
    - https://i3.ytimg.com/vi/MnYppmstxIs/2.jpg
    - https://i3.ytimg.com/vi/MnYppmstxIs/3.jpg
---
