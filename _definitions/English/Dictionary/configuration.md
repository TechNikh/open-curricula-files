---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/configuration
offline_file: ""
offline_thumbnail: ""
uuid: d5344340-6e51-44dc-af1c-839d01f66dd1
updated: 1484310391
title: configuration
categories:
    - Dictionary
---
configuration
     n 1: an arrangement of parts or elements; "the outcome depends on
          the configuration of influences at the time" [syn: {constellation}]
     2: any spatial attributes (especially as defined by outline);
        "he could barely make out their shapes through the smoke"
        [syn: {shape}, {form}, {contour}, {conformation}]
