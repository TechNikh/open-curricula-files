---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/formation
offline_file: ""
offline_thumbnail: ""
uuid: 2a7c8afa-19b4-49de-b319-588f9636c491
updated: 1484310289
title: formation
categories:
    - Dictionary
---
formation
     n 1: an arrangement of people or things acting as a unit; "a
          defensive formation"; "a formation of planes"
     2: a particular spatial arrangement
     3: the fabrication of something in a particular shape [syn: {shaping}]
     4: the act of forming something; "the constitution of a PTA
        group last year"; "it was the establishment of his
        reputation"; "he still remembers the organization of the
        club" [syn: {constitution}, {establishment}, {organization},
         {organisation}]
     5: (geology) the geological features of the earth [syn: {geological
        formation}]
     6: natural process that causes something to form; "the
        ...
