---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/carpenter
offline_file: ""
offline_thumbnail: ""
uuid: fd35e00d-22bb-4311-9374-9bb97e5f8704
updated: 1484310152
title: carpenter
categories:
    - Dictionary
---
carpenter
     n : a woodworker who makes or repairs wooden objects
     v : work as a carpenter
