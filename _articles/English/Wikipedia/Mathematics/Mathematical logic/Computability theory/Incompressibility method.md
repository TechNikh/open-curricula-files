---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Incompressibility_method
offline_file: ""
offline_thumbnail: ""
uuid: 232354c3-2a25-4196-bb93-715d9ba39f47
updated: 1484309485
title: Incompressibility method
tags:
    - History
    - Applications
    - Number theory
    - Graph theory
    - Combinatorics
    - Topological combinatorics
    - Probability
    - Turing machines time complexity
    - Theory of computation
    - Logic
    - Comparison with other methods
categories:
    - Computability theory
---
The incompressibility method is a proof method such as the probabilistic method, the counting method, or the pigeonhole principle. The method proceeds as follows: In order to prove that an object in a certain class on average satisfies a certain property, select an object of that class that is incompressible. Subsequently it is shown that if it does not satisfy the property then it can be compressed by clever computable coding. Since in general it can be proved that almost all objects of a given class are incompressible, the argument shows that almost all objects in the class have the property involved (and not just the average). To select an incompressible object is not effective: it ...
