---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/isomerism
offline_file: ""
offline_thumbnail: ""
uuid: fc0019ce-03d9-4164-8eb8-6cc2c31078cf
updated: 1484310416
title: isomerism
categories:
    - Dictionary
---
isomerism
     n : the state of being an isomer; the complex of chemical and
         physical phenomena characteristic of isomers
