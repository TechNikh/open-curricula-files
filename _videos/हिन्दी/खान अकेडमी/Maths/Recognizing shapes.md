---
version: 1
type: video
provider: YouTube
id: kBpFn5ubELA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9fc81c0e-c220-46ef-acf1-722a32cee5d2
updated: 1486069618
title: Recognizing shapes
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/kBpFn5ubELA/default.jpg
    - https://i3.ytimg.com/vi/kBpFn5ubELA/1.jpg
    - https://i3.ytimg.com/vi/kBpFn5ubELA/2.jpg
    - https://i3.ytimg.com/vi/kBpFn5ubELA/3.jpg
---
