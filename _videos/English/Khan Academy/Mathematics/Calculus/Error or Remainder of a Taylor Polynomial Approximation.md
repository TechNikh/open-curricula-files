---
version: 1
type: video
provider: YouTube
id: wgkRH5Uoavk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Error%20or%20Remainder%20of%20a%20Taylor%20Polynomial%20Approximation.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 61a35066-cc49-458f-8d82-4662973f5fb4
updated: 1486069690
title: Error or Remainder of a Taylor Polynomial Approximation
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/wgkRH5Uoavk/default.jpg
    - https://i3.ytimg.com/vi/wgkRH5Uoavk/1.jpg
    - https://i3.ytimg.com/vi/wgkRH5Uoavk/2.jpg
    - https://i3.ytimg.com/vi/wgkRH5Uoavk/3.jpg
---
