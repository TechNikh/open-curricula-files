---
version: 1
type: video
provider: YouTube
id: f_AvqnMTJjg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2a41175b-bc22-429a-a521-2776b829fce5
updated: 1486069661
title: 'Making Rayon Fiber - Artificial silk, chemical experiment!'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/f_AvqnMTJjg/default.jpg
    - https://i3.ytimg.com/vi/f_AvqnMTJjg/1.jpg
    - https://i3.ytimg.com/vi/f_AvqnMTJjg/2.jpg
    - https://i3.ytimg.com/vi/f_AvqnMTJjg/3.jpg
---
