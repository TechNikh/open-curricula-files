---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Richard_Canary
offline_file: ""
offline_thumbnail: ""
uuid: 6c4d63bd-9c8a-4787-9539-280dd0edd400
updated: 1484309073
title: Richard Canary
categories:
    - Geometers
---
Richard Douglas Canary (born in 1962) is an American mathematician working mainly on low-dimensional topology. He is a professor at the University of Michigan.[1]
