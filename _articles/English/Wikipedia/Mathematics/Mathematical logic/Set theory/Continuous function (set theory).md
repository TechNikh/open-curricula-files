---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Continuous_function_(set_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 24a7209e-bc19-4b9c-918f-ca2d339a7c57
updated: 1484309501
title: Continuous function (set theory)
categories:
    - Set theory
---
In mathematics, specifically set theory, a continuous function is a sequence of ordinals such that the values assumed at limit stages are the limits (limit suprema and limit infima) of all values at previous stages. More formally, let γ be an ordinal, and 
  
    
      
        s
        :=
        ⟨
        
          s
          
            α
          
        
        
          |
        
        α
        <
        γ
        ⟩
      
    
    {\displaystyle s:=\langle s_{\alpha }|\alpha <\gamma \rangle }
  
 be a γ-sequence of ordinals. Then s is continuous if at every limit ordinal β < γ,
