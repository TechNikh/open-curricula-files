---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tributary
offline_file: ""
offline_thumbnail: ""
uuid: c23b32fe-8711-4397-b1cd-5d6294d5b4a0
updated: 1484310462
title: tributary
categories:
    - Dictionary
---
tributary
     adj 1: of a stream; flowing into a larger stream
     2: paying tribute; "a tributary colony"
     3: tending to bring about; being partly responsible for;
        "working conditions are not conducive to productivity";
        "the seaport was a contributing factor in the growth of
        the city"; "a contributory factor" [syn: {conducive}, {contributing(a)},
         {contributive}, {contributory}]
     n : a branch that flows into the main stream [syn: {feeder}, {affluent}]
         [ant: {distributary}]
