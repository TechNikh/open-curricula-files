---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Partition_(number_theory)
offline_file: ""
offline_thumbnail: ""
uuid: 69a1f88d-e0ae-4e44-91ef-0cc2ee055527
updated: 1484308595
title: Partition (number theory)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Ferrer_partitioning_diagrams.svg.png
tags:
    - Examples
    - Representations of partitions
    - Ferrers diagram
    - Young diagram
    - Partition function
    - Generating function
    - Congruences
    - Partition function formulas
    - Recurrence formula
    - Approximation formulas
    - Restricted partitions
    - Conjugate and self-conjugate partitions
    - Odd parts and distinct parts
    - Restricted part size or number of parts
    - Asymptotics
    - Partitions in a rectangle and Gaussian binomial coefficients
    - Rank and Durfee square
    - "Young's lattice"
    - Algorithm
    - Notes
categories:
    - Combinatorics
---
In number theory and combinatorics, a partition of a positive integer n, also called an integer partition, is a way of writing n as a sum of positive integers. Two sums that differ only in the order of their summands are considered the same partition. (If order matters, the sum becomes a composition.) For example, 4 can be partitioned in five distinct ways:
