---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tin
offline_file: ""
offline_thumbnail: ""
uuid: 713262f1-f444-43ef-a212-88cafea5e9fc
updated: 1484310212
title: tin
categories:
    - Dictionary
---
tin
     n 1: a silvery malleable metallic element that resists corrosion;
          used in many alloys and to coat other metals to prevent
          corrosion; obtained chiefly from cassiterite where it
          occurs as tin oxide [syn: {Sn}, {atomic number 50}]
     2: metal container for storing dry foods such as tea or flour
        [syn: {canister}, {cannister}]
     3: airtight sealed metal container for food or drink or paint
        etc. [syn: {can}, {tin can}]
     v 1: plate with tin
     2: preserve in a can or tin; "tinned foods are not very tasty"
        [syn: {can}, {put up}]
     3: prepare (a metal) for soldering or brazing by applying a
        thin layer of solder to ...
