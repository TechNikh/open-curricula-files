---
version: 1
type: video
provider: YouTube
id: rNCPJFgN5Bc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c48c6fee-bebf-4ce5-a100-1c50b5b879f2
updated: 1486069605
title: Adding three digit numbers with regrouping
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/rNCPJFgN5Bc/default.jpg
    - https://i3.ytimg.com/vi/rNCPJFgN5Bc/1.jpg
    - https://i3.ytimg.com/vi/rNCPJFgN5Bc/2.jpg
    - https://i3.ytimg.com/vi/rNCPJFgN5Bc/3.jpg
---
