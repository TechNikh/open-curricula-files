---
version: 1
type: video
provider: YouTube
id: ccrYD6iX_SY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Probability%20and%20combinatorics/Example-%20Combinatorics%20and%20probability.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9d6e0be0-bf2c-4a0b-b4ad-56bf54421253
updated: 1486069563
title: 'Example- Combinatorics and probability'
categories:
    - Probability and combinatorics
thumbnail_urls:
    - https://i3.ytimg.com/vi/ccrYD6iX_SY/default.jpg
    - https://i3.ytimg.com/vi/ccrYD6iX_SY/1.jpg
    - https://i3.ytimg.com/vi/ccrYD6iX_SY/2.jpg
    - https://i3.ytimg.com/vi/ccrYD6iX_SY/3.jpg
---
