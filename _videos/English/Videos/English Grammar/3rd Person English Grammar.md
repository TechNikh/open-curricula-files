---
version: 1
type: video
provider: YouTube
id: -H6xl0LlGr0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/English%20Grammar/3rd%20Person%20English%20Grammar.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a28096cf-222c-44ec-a4a7-384734952c09
updated: 1486069631
title: 3rd Person English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/-H6xl0LlGr0/default.jpg
    - https://i3.ytimg.com/vi/-H6xl0LlGr0/1.jpg
    - https://i3.ytimg.com/vi/-H6xl0LlGr0/2.jpg
    - https://i3.ytimg.com/vi/-H6xl0LlGr0/3.jpg
---
