---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Voronoi_diagram
offline_file: ""
offline_thumbnail: ""
uuid: 3836022e-5376-413d-95f5-c72c8a55e766
updated: 1484308506
title: Voronoi diagram
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Euclidean_Voronoi_diagram.svg.png
tags:
    - The simplest case
    - Formal definition
    - Illustration
    - Properties
    - History and research
    - Examples
    - Higher-order Voronoi diagrams
    - Farthest-point Voronoi diagram
    - Generalizations and variations
    - Applications
    - Natural sciences
    - Health
    - Engineering
    - Geometry
    - Informatics
    - Algorithms
    - Notes
categories:
    - Discrete geometry
---
In mathematics, a Voronoi diagram is a partitioning of a plane into regions based on distance to points in a specific subset of the plane. That set of points (called seeds, sites, or generators) is specified beforehand, and for each seed there is a corresponding region consisting of all points closer to that seed than to any other. These regions are called Voronoi cells. The Voronoi diagram of a set of points is dual to its Delaunay triangulation.
