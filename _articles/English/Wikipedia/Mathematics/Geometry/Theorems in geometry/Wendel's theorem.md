---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Wendel%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 083898c4-2f5a-4483-9511-ff9d7eca67ba
updated: 1484309127
title: "Wendel's theorem"
categories:
    - Theorems in geometry
---
In geometric probability theory, Wendel's theorem, named after James G. Wendel, gives the probability that N points distributed uniformly at random on an n-dimensional hypersphere all lie on the same "half" of the hypersphere. In other words, one seeks the probability that there is some hyperplane intersecting the center of the hypersphere such that all the points lie on the same side of the hyperplane. Wendel's theorem says that the probability is[1]
