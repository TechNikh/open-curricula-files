---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93Nagy_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: e48c6ba7-ee36-421b-af2c-033595c96732
updated: 1484309359
title: Erdős–Nagy theorem
categories:
    - Geometry stubs
---
The Erdős–Nagy theorem is a result in discrete geometry stating that a non-convex simple polygon can be made into a convex polygon by a finite sequence of flips. The flips are defined by taking a convex hull of a polygon and reflecting a pocket with respect to the boundary edge. The theorem is named after mathematicians Paul Erdős and Béla Szőkefalvi-Nagy.
