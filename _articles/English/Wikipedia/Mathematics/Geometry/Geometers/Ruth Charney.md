---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ruth_Charney
offline_file: ""
offline_thumbnail: ""
uuid: 57b55a1b-20a3-417a-ad3c-c1e8c9e1e6ae
updated: 1484309077
title: Ruth Charney
tags:
    - life
    - Work
    - Selected publications
categories:
    - Geometers
---
Ruth Michele Charney is an American mathematician known for her work in geometric group theory and Artin groups. She became president of the Association for Women in Mathematics in 2013. She was in the first group of mathematicians named Fellows of the American Mathematical Society.[1]
