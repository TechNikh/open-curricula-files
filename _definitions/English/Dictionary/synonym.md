---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/synonym
offline_file: ""
offline_thumbnail: ""
uuid: c6df2d56-1fdd-4ea5-8ca0-3747b7e76a38
updated: 1484310148
title: synonym
categories:
    - Dictionary
---
synonym
     n : two words that can be interchanged in a context are said to
         be synonymous relative to that context [syn: {equivalent
         word}]
