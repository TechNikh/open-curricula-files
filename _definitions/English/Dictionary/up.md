---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/up
offline_file: ""
offline_thumbnail: ""
uuid: b74b6ef1-0233-4970-93c4-8458098b6a32
updated: 1484310357
title: up
categories:
    - Dictionary
---
up
     adj 1: being or moving higher in position or greater in some value;
            being above a former position or level; "the anchor is
            up"; "the sun is up"; "he lay face up"; "he is up by a
            pawn"; "the market is up"; "the corn is up" [ant: {down}]
     2: getting higher or more vigorous; "its an up market"; "an
        improving economy" [syn: {improving}]
     3: extending or moving toward a higher place; "the up
        staircase"; "a general upward movement of fish" [syn: {up(a)},
         {upward(a)}]
     4: (usually followed by `on' or `for') in readiness; "he was up
        on his homework"; "had to be up for the game" [syn: {up(p)}]
     5: open; "the ...
