---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/length
offline_file: ""
offline_thumbnail: ""
uuid: 60e36943-a8e0-4c83-932d-4865151901f5
updated: 1484310323
title: length
categories:
    - Dictionary
---
length
     n 1: the linear extent in space from one end to the other; the
          longest horizontal dimension of something that is fixed
          in place; "the length of the table was 5 feet"
     2: continuance in time; "the ceremony was of short duration";
        "he complained about the length of time required" [syn: {duration}]
     3: the property of being the extent of something from beginning
        to end; "the editor limited the length of my article to
        500 words"
     4: size of the gap between two places; "the distance from New
        York to Chicago"; "he determined the length of the
        shortest line segment joining the two points" [syn: {distance}]
     5: ...
