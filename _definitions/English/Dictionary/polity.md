---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/polity
offline_file: ""
offline_thumbnail: ""
uuid: 13edf991-0196-4f26-bfb4-f9adfbab4741
updated: 1484310595
title: polity
categories:
    - Dictionary
---
polity
     n 1: the form of government of a social organization [syn: {civil
          order}]
     2: a politically organized unit
     3: shrewd or crafty management of public affairs; "we was
        innocent of stratagems and polity"
