---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/yeast
offline_file: ""
offline_thumbnail: ""
uuid: 5d1bed42-c856-4cdc-9cae-6bd11c5fc6fa
updated: 1484310377
title: yeast
categories:
    - Dictionary
---
yeast
     n 1: a commercial leavening agent containing yeast cells; used to
          raise the dough in making bread and for fermenting beer
          or whiskey [syn: {barm}]
     2: any of various single-celled fungi that reproduce asexually
        by budding or division
