---
version: 1
type: video
provider: YouTube
id: sLbT5P_EP5w
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 02d0de61-b439-46ed-afa5-f645478998ab
updated: 1486069617
title: Understanding multiplication through area models
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/sLbT5P_EP5w/default.jpg
    - https://i3.ytimg.com/vi/sLbT5P_EP5w/1.jpg
    - https://i3.ytimg.com/vi/sLbT5P_EP5w/2.jpg
    - https://i3.ytimg.com/vi/sLbT5P_EP5w/3.jpg
---
