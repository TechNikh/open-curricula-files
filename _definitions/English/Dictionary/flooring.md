---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/flooring
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484572981
title: flooring
categories:
    - Dictionary
---
flooring
     n 1: the inside lower horizontal surface (as of a room or
          hallway); "they needed rugs to cover the bare floors"
          [syn: {floor}]
     2: building material used in laying floors
