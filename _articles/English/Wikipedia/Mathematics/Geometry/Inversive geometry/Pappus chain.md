---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pappus_chain
offline_file: ""
offline_thumbnail: ""
uuid: 04610c60-7ac2-42c6-868d-e7dd66f5bccb
updated: 1484309258
title: Pappus chain
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Pappus_Chain_Full.svg.png
tags:
    - Construction
    - Properties
    - Centers of the circles
    - Ellipse
    - Coordinates
    - Radii of the circles
    - Circle inversion
    - Steiner chain
    - Bibliography
categories:
    - Inversive geometry
---
