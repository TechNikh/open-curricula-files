---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/riboflavin
offline_file: ""
offline_thumbnail: ""
uuid: 3300a836-ceab-41d2-8012-a4b16524665a
updated: 1484310162
title: riboflavin
categories:
    - Dictionary
---
riboflavin
     n : a B vitamin that prevents skin lesions and weight loss [syn:
          {vitamin B2}, {vitamin G}, {lactoflavin}, {ovoflavin}, {hepatoflavin}]
