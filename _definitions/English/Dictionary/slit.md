---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/slit
offline_file: ""
offline_thumbnail: ""
uuid: 2229ddb1-50b3-4ad1-9384-14aecc221000
updated: 1484310196
title: slit
categories:
    - Dictionary
---
slit
     n 1: a long narrow opening
     2: obscene terms for female genitals [syn: {cunt}, {puss}, {pussy},
         {snatch}, {twat}]
     3: a depression scratched or carved into a surface [syn: {incision},
         {scratch}, {prick}, {dent}]
     4: a narrow fissure
     v 1: make a clean cut through; "slit her throat" [syn: {slice}]
     2: cut a slit into; "slit the throat of the victim"
     [also: {slitting}]
