---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bodily
offline_file: ""
offline_thumbnail: ""
uuid: 7f35aa7d-f457-4b38-91ed-b37bea9e7cca
updated: 1484310290
title: bodily
categories:
    - Dictionary
---
bodily
     adj 1: of or relating to or belonging to the body; "a bodily
            organ"; "bodily functions"; "carnal remains" [syn: {carnal}]
     2: affecting or characteristic of the body as opposed to the
        mind or spirit; "bodily needs"; "a corporal defect";
        "corporeal suffering"; "a somatic symptom or somatic
        illness" [syn: {corporal}, {corporeal}, {somatic}]
     3: having or relating to a physical material body; "bodily
        existence"
     adv : in bodily form; "he was translated bodily to heaven"
