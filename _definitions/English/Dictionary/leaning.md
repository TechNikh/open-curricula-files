---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/leaning
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484378941
title: leaning
categories:
    - Dictionary
---
leaning
     adj 1: departing or being caused to depart from the true vertical
            or horizontal; "the leaning tower of Pisa"; "the
            headstones were tilted" [syn: {atilt}, {canted}, {tilted},
             {tipped}]
     2: resting against a support
     n 1: an inclination to do something; "he felt leanings toward
          frivolity" [syn: {propensity}, {tendency}]
     2: a natural inclination; "he has a proclivity for
        exaggeration" [syn: {proclivity}, {propensity}]
     3: the property possessed by a line or surface that departs
        from the vertical; "the tower had a pronounced tilt"; "the
        ship developed a list to starboard"; "he walked with a
     ...
