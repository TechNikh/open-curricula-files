---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/enormous
offline_file: ""
offline_thumbnail: ""
uuid: 3dd5d34d-9261-4ca4-a6c9-81deb939e24f
updated: 1484310373
title: enormous
categories:
    - Dictionary
---
enormous
     adj : extraordinarily large in size or extent or amount or power
           or degree; "an enormous boulder"; "enormous expenses";
           "tremendous sweeping plains"; "a tremendous fact in
           human experience; that a whole civilization should be
           dependent on technology"- Walter Lippman; "a plane took
           off with a tremendous noise" [syn: {tremendous}]
