---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Pakistani_Telecom_Spectrum_Auction
offline_file: ""
offline_thumbnail: ""
uuid: 854bca1d-1947-4716-870f-a5faa34c8c59
updated: 1484309017
title: Pakistani Telecom Spectrum Auction
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/800px-Flag_of_Pakistan.svg.png
categories:
    - Auctions
---
On 14 April 2014, four companies submitted their bids to PTA, includes Zong, Ufone, Telenor and Mobilink, while Warid Telecom did not submit.[2] Reuters confirmed that Mobilink and Zong bid for 10 MHz while Telenor and Ufone bid for 5 MHz.[3]
