---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/K%C3%A1roly_Bezdek'
offline_file: ""
offline_thumbnail: ""
uuid: d33672a5-c7d7-4584-87aa-5cb29041966f
updated: 1484309069
title: Károly Bezdek
tags:
    - Early life and family
    - Professional accomplishments
    - Research interests and notable results
    - Books
    - Academic honors
categories:
    - Geometers
---
Károly Bezdek (born May 28, 1955 in Budapest, Hungary), is a Hungarian-Canadian mathematician with main interests in geometry and specializing in combinatorial, computational, convex, and discrete geometry. Bezdek is a professor as well as a Canada Research Chair (Tier 1) of mathematics at the University of Calgary in Calgary, Canada, and the director of the Center for Computational and Discrete Geometry (CCDG). He is also a professor of mathematics at the University of Pannonia in Hungary. He is the author of 2 books (research monographs) and more than 110 research papers. He is an Editor-in-Chief of the e-journal Contributions to Discrete Mathematics (CDM).
