---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Resonance
offline_file: ""
offline_thumbnail: ""
uuid: fde4c42f-4ea8-4ac2-9d7e-7b6cb3464c4b
updated: 1484308894
title: Resonance
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/460px-Resonance.PNG
tags:
    - Examples
    - Theory
    - Resonators
    - Q factor
    - Types of resonance
    - Mechanical and acoustic resonance
    - Electrical resonance
    - Optical resonance
    - Orbital resonance
    - Atomic, particle, and molecular resonance
    - Tacoma Narrows Bridge
    - International Space Station
categories:
    - Waves
---
In physics, resonance is a phenomenon in which a vibrating system or external force drives another system to oscillate with greater amplitude at a specific preferential frequency.
