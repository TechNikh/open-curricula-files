---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disassociate
offline_file: ""
offline_thumbnail: ""
uuid: 0ce55fe5-06d2-4ef7-ab3b-8baadbd1d06d
updated: 1484310180
title: disassociate
categories:
    - Dictionary
---
disassociate
     v : part; cease or break association with; "She disassociated
         herself from the organization when she found out the
         identity of the president" [syn: {dissociate}, {divorce},
          {disunite}, {disjoint}]
