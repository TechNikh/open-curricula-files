---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gates
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484324461
title: gates
categories:
    - Dictionary
---
Gates
     n : United States computer entrepreneur whose software company
         made him the youngest multi-billionaire in the history of
         the United States (born in 1955) [syn: {Bill Gates}, {William
         Henry Gates}]
