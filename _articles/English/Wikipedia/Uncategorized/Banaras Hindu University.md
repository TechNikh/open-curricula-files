---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bhu
offline_file: ""
offline_thumbnail: ""
uuid: 93f7b4ef-9a8d-4fbc-a8cc-a59b9828d142
updated: 1484308412
title: Banaras Hindu University
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Madan_Mohan_Malaviya_01.JPG
tags:
    - History
    - Campus
    - Main campus
    - South campus
    - Rajiv Gandhi South Campus
    - Academics
    - Institutes
    - Indian Institute of Technology (BHU) Varanasi
    - Institute of Science
    - Institute of Agricultural Sciences
    - Institute of Medical Sciences
    - 'Institute of Environment & Sustainable Development'
    - Institute of Management Studies
    - Faculties
    - Inter-disciplinary schools
    - School of Biotechnology
    - DBT-BHU Interdisciplinary School of Life Sciences
    - DST Centre for Interdisciplinary Mathematical Sciences
    - 'Centre of Food Science & Technology'
    - Research centres
    - Affiliated schools and colleges
    - Colleges
    - schools
    - Library system
    - ICT Infrastructure
    - Admissions
    - Halls of Residence
    - Bharat Kala Bhawan
    - Festivals
    - Rankings
    - Awards and Medals
    - Notable alumni, faculty and staff
categories:
    - Uncategorized
---
Banaras Hindu University (Hindi: काशी हिन्दू विश्वविद्यालय ([kaʃi hind̪u viʃvəvid̪yaləy]); commonly referred to as BHU; formerly known as Central Hindu College) is a public central university located in Varanasi, Uttar Pradesh. Established in 1916 by Pandit Madan Mohan Malaviya,[1] BHU is one of the largest residential universities in Asia, with over 20,000 students.[4][5] The university comprises all castes, creeds, religions and genders, and is on the list of Institutes of National Importance.
