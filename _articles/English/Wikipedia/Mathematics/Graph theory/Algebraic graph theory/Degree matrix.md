---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Degree_matrix
offline_file: ""
offline_thumbnail: ""
uuid: ed83efd9-42ba-48be-b347-d70f680bf510
updated: 1484309371
title: Degree matrix
tags:
    - Definition
    - Example
    - Properties
categories:
    - Algebraic graph theory
---
In the mathematical field of graph theory the degree matrix is a diagonal matrix which contains information about the degree of each vertex—that is, the number of edges attached to each vertex.[1] It is used together with the adjacency matrix to construct the Laplacian matrix of a graph.[2]
