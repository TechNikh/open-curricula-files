---
version: 1
type: video
provider: YouTube
id: 3DGKY3eM9PY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Grammar/Syntax/Simple%20and%20compound%20sentences.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 706cf266-2650-4f8b-abe0-6dcfd11858fb
updated: 1486069603
title: Simple and compound sentences
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Syntax
thumbnail_urls:
    - https://i3.ytimg.com/vi/3DGKY3eM9PY/default.jpg
    - https://i3.ytimg.com/vi/3DGKY3eM9PY/1.jpg
    - https://i3.ytimg.com/vi/3DGKY3eM9PY/2.jpg
    - https://i3.ytimg.com/vi/3DGKY3eM9PY/3.jpg
---
A simple sentence contains one independent clause.  A compound sentence contains more than one!  Put another way: a simple sentence contains a subject and a predicate, but a compound sentence contains more than one subject and more than one predicate.


Practice this yourself on Khan Academy right now: https://www.khanacademy.org/syntax/e/simple-and-compound-sentences 

Watch the next lesson: https://www.khanacademy.org/humanities/grammar/syntax/v/complex-sentences-syntax-khan-academy

Missed the previous lesson? Watch here: https://www.khanacademy.org/humanities/grammar/syntax/v/the-exclamation-syntax-khan-academy 

Syntax on Khan Academy: Syntax is the ordering of language; it’s the study of how sentences work. In this section, we’ll scratch the surface of syntax as it applies to English grammar.  Much more can be said about this subject, but we’ll save that for KA Linguistics.


About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to KhanAcademy: https://www.youtube.com/subscription_center?add_user=khanacademy
