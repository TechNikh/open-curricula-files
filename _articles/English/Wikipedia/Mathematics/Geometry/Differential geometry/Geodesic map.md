---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Geodesic_map
offline_file: ""
offline_thumbnail: ""
uuid: 874c6ca9-ba11-45bf-9a93-37aacd2e7a60
updated: 1484309221
title: Geodesic map
categories:
    - Differential geometry
---
In mathematics—specifically, in differential geometry—a geodesic map (or geodesic mapping or geodesic diffeomorphism) is a function that "preserves geodesics". More precisely, given two (pseudo-)Riemannian manifolds (M, g) and (N, h), a function φ : M → N is said to be a geodesic map if
