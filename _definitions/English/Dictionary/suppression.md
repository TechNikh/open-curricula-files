---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suppression
offline_file: ""
offline_thumbnail: ""
uuid: 123d4a50-56b0-4b62-a8f8-b093dc651736
updated: 1484310581
title: suppression
categories:
    - Dictionary
---
suppression
     n 1: (botany) the failure to develop of some part or organ of a
          plant
     2: the act of withholding or withdrawing some book or writing
        from publication or circulation; "a suppression of the
        newspaper" [syn: {curtailment}]
     3: forceful prevention; putting down by power or authority;
        "the suppression of heresy"; "the quelling of the
        rebellion"; "the stifling of all dissent" [syn: {crushing},
         {quelling}, {stifling}]
     4: (psychology) the conscious exclusion of unacceptable
        thoughts or desires [syn: {inhibition}]
