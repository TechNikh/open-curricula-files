---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/irrational
offline_file: ""
offline_thumbnail: ""
uuid: 979fb60c-bc11-4d8a-8ae8-469929ae3099
updated: 1484310156
title: irrational
categories:
    - Dictionary
---
irrational
     adj 1: not consistent with or using reason; "irrational fears";
            "irrational animals" [ant: {rational}]
     2: real but not expressible as the quotient of two integers;
        "irrational numbers" [ant: {rational}]
