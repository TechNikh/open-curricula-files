---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/violation
offline_file: ""
offline_thumbnail: ""
uuid: 01c9b91e-97fe-44cc-b65a-46666e426f7f
updated: 1484310188
title: violation
categories:
    - Dictionary
---
violation
     n 1: a crime less serious than a felony [syn: {misdemeanor}, {misdemeanour},
           {infraction}, {offence}, {offense}, {infringement}]
     2: an act that disregards an agreement or a right; "he claimed
        a violation of his rights under the Fifth Amendment" [syn:
         {infringement}]
     3: entry to another's property without right or permission
        [syn: {trespass}, {encroachment}, {intrusion}, {usurpation}]
     4: a disrespectful act [syn: {irreverence}]
     5: the crime of forcing a woman to submit to sexual intercourse
        against her will [syn: {rape}, {assault}, {ravishment}]
