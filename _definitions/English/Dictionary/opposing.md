---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/opposing
offline_file: ""
offline_thumbnail: ""
uuid: 17543181-0e6e-40cc-975e-fef221de0958
updated: 1484310531
title: opposing
categories:
    - Dictionary
---
opposing
     adj 1: characterized by active hostility; "opponent (or opposing)
            armies" [syn: {opponent}]
     2: acting in opposition to; "the opposing sector of the same
        muscle group"
     3: in opposition to (a policy or attitude etc.); "an opposing
        vote" [syn: {opposed}]
