---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Flammability
offline_file: ""
offline_thumbnail: ""
uuid: ac728438-8a80-45df-8eeb-32a03206a26a
updated: 1484308842
title: Flammability
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Tu_braunschweig_b1_brandschacht.jpg
tags:
    - Definitions
    - Testing
    - Categorization of building materials
    - Important characteristics
    - Flash point
    - Vapor pressure
    - Furniture flammability
    - Examples of flammable substances
    - Examples of nonflammable liquids
    - Classification of flammability
    - Codes
    - Flammability
categories:
    - Thermodynamics
---
Flammability is the ability of a substance to burn or ignite, causing fire or combustion. The degree of difficulty required to cause the combustion of a substance is quantified through fire testing. Internationally, a variety of test protocols exist to quantify flammability. The ratings achieved are used in building codes, insurance requirements, fire codes and other regulations governing the use of building materials as well as the storage and handling of highly flammable substances inside and outside of structures and in surface and air transportation. For instance, changing an occupancy by altering the flammability of the contents requires the owner of a building to apply for a building ...
