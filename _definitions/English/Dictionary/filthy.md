---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/filthy
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484597221
title: filthy
categories:
    - Dictionary
---
filthy
     adj 1: disgustingly dirty; filled or smeared with offensive matter;
            "as filthy as a pigsty"; "a foul pond"; "a nasty
            pigsty of a room" [syn: {foul}, {nasty}]
     2: vile; despicable; "a dirty (or lousy) trick"; "a filthy
        traitor" [syn: {dirty}, {lousy}]
     3: thoroughly unpleasant; "filthy (or foul or nasty or vile)
        weather we're having" [syn: {foul}, {nasty}, {vile}]
     4: characterized by obscenity; "had a filthy mouth"; "foul
        language"; "smutty jokes" [syn: {foul}, {nasty}, {smutty}]
     [also: {filthiest}, {filthier}]
