---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Electrical_drawing
offline_file: ""
offline_thumbnail: ""
uuid: 5d7a9977-bd5f-4b3a-aafe-7b25fef7ef4d
updated: 1484309324
title: Electrical drawing
categories:
    - Technical drawing
---
An electrical drawing, is a type of technical drawing that shows information about power, lighting, and communication for an engineering or architectural project. Any electrical working drawing consists of "lines, symbols, dimensions, and notations to accurately convey an engineering's design to the workers, who install the electrical system on the job".[1]
