---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/public
offline_file: ""
offline_thumbnail: ""
uuid: e7cb8843-e78b-48ef-b26d-2302f4c65e10
updated: 1484310244
title: public
categories:
    - Dictionary
---
public
     adj 1: not private; open to or concerning the people as a whole;
            "the public good"; "public libraries"; "public funds";
            "public parks"; "a public scandal"; "public gardens";
            "performers and members of royal families are public
            figures" [ant: {private}]
     2: affecting the people or community as a whole; "community
        leaders"; "community interests"; "the public welfare"
     n 1: people in general considered as a whole; "he is a hero in
          the eyes of the public" [syn: {populace}, {world}]
     2: a body of people sharing some common interest; "the reading
        public"
