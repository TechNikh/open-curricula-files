---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Yair_Tauman
offline_file: ""
offline_thumbnail: ""
uuid: 2ff7b590-eb5a-40f1-9723-9c9332d2b9fe
updated: 1484309026
title: Yair Tauman
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Yair_Tauman.jpg
tags:
    - Biography (Business)
    - Biography (Film)
    - Selected publications
    - Notes
    - External References
categories:
    - Game theorists
---
Yair Tauman (born January 20, 1948) is a Professor of Economics at State University of New York, Stony Brook and the Director of the Stony Brook Center for Game Theory. He studied at the Hebrew University of Jerusalem where he obtained his B.Sc. in Mathematics and Statistics and M.Sc. and Ph.D. in Mathematics, the latter two under the supervision of Robert Aumann. His areas of research interests are game theory and industrial organization. He has published, among others, in Econometrica, Games and Economic Behavior, Journal of Economic Theory, Quarterly Journal of Economics and RAND Journal of Economics.
