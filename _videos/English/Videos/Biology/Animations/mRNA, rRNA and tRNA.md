---
version: 1
type: video
provider: YouTube
id: dcvtHSFzKOQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/mRNA%2C%20rRNA%20and%20tRNA.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 7968fd68-8bf1-422b-9ed8-4d2915820db9
updated: 1486069672
title: mRNA, rRNA and tRNA
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/dcvtHSFzKOQ/default.jpg
    - https://i3.ytimg.com/vi/dcvtHSFzKOQ/1.jpg
    - https://i3.ytimg.com/vi/dcvtHSFzKOQ/2.jpg
    - https://i3.ytimg.com/vi/dcvtHSFzKOQ/3.jpg
---
