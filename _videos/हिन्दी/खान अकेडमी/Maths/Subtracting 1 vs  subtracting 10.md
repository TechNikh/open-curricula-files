---
version: 1
type: video
provider: YouTube
id: A0iJyqM2Sv4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 9ff22ac0-5447-4c95-aa86-09bf15d78d1d
updated: 1486069605
title: 'Subtracting 1 vs  subtracting 10'
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/A0iJyqM2Sv4/default.jpg
    - https://i3.ytimg.com/vi/A0iJyqM2Sv4/1.jpg
    - https://i3.ytimg.com/vi/A0iJyqM2Sv4/2.jpg
    - https://i3.ytimg.com/vi/A0iJyqM2Sv4/3.jpg
---
