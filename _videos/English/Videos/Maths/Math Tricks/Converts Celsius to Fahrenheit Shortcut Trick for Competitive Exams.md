---
version: 1
type: video
provider: YouTube
id: MZJkXhn5NFE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dd47ff8f-c836-4dc1-93f7-ddc8f653439b
updated: 1486069621
title: >
    Converts Celsius to Fahrenheit Shortcut Trick for
    Competitive Exams
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/MZJkXhn5NFE/default.jpg
    - https://i3.ytimg.com/vi/MZJkXhn5NFE/1.jpg
    - https://i3.ytimg.com/vi/MZJkXhn5NFE/2.jpg
    - https://i3.ytimg.com/vi/MZJkXhn5NFE/3.jpg
---
