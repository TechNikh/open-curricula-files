---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/confine
offline_file: ""
offline_thumbnail: ""
uuid: 8f3f5c99-8340-4b91-bbd1-245e332fdbd8
updated: 1484310563
title: confine
categories:
    - Dictionary
---
confine
     v 1: restrict or confine, "I limit you to two visits to the pub a
          day" [syn: {limit}, {circumscribe}]
     2: place limits on (extent or access); "restrict the use of
        this parking lot"; "limit the time you can spend with your
        friends" [syn: {restrict}, {restrain}, {trammel}, {limit},
         {bound}, {throttle}]
     3: prevent from leaving or from being removed
     4: close in or confine [syn: {enclose}, {hold in}]
     5: deprive of freedom; take into confinement [syn: {detain}]
        [ant: {free}]
     6: to close within bounds, limit or hold back from movement;
        "This holds the local until the express passengers change
        trains"; ...
