---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chair
offline_file: ""
offline_thumbnail: ""
uuid: d41ce9f1-9c60-46b9-8881-5caaca5c2168
updated: 1484310597
title: chair
categories:
    - Dictionary
---
chair
     n 1: a seat for one person, with a support for the back; "he put
          his coat over the back of the chair and sat down"
     2: the position of professor; "he was awarded an endowed chair
        in economics" [syn: {professorship}]
     3: the officer who presides at the meetings of an organization;
        "address your remarks to the chairperson" [syn: {president},
         {chairman}, {chairwoman}, {chairperson}]
     4: an instrument of execution by electrocution; resembles a
        chair; "the murderer was sentenced to die in the chair"
        [syn: {electric chair}, {death chair}, {hot seat}]
     v 1: act or preside as chair, as of an academic department in a
      ...
