---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parked
offline_file: ""
offline_thumbnail: ""
uuid: 1e67826f-464f-4f2e-af84-9e6403f35932
updated: 1484310484
title: parked
categories:
    - Dictionary
---
parked
     adj : that have been left; "there were four parked cars across the
           street"
