---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cultivate
offline_file: ""
offline_thumbnail: ""
uuid: 242ccadb-10fc-4a95-87e9-1aae28c03905
updated: 1484310234
title: cultivate
categories:
    - Dictionary
---
cultivate
     v 1: foster the growth of
     2: prepare for crops; "Work the soil"; "cultivate the land"
        [syn: {crop}, {work}]
     3: train to be discriminative in taste or judgment; "Cultivate
        your musical taste"; "Train your tastebuds"; "She is well
        schooled in poetry" [syn: {educate}, {school}, {train}, {civilize},
         {civilise}]
     4: adapt (a wild plant or unclaimed land) to the environment;
        "domesticate oats"; "tame the soil" [syn: {domesticate}, {naturalize},
         {naturalise}, {tame}]
