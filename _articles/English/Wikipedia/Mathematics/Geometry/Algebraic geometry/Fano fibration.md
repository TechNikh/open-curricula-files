---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fano_fibration
offline_file: ""
offline_thumbnail: ""
uuid: f6f73ebc-45fa-4d8d-a78f-241c9bb4ae07
updated: 1484309147
title: Fano fibration
categories:
    - Algebraic geometry
---
In algebraic geometry, a Fano fibration or Fano fiber space, named after Gino Fano, is a morphism of varieties whose general fiber is a Fano variety (in other words has ample anticanonical bundle) of positive dimension. The ones arising from extremal contractions in the minimal model program are called Mori fibrations or Mori fiber spaces (for Shigefumi Mori). They appear as standard forms for varieties without a minimal model.
