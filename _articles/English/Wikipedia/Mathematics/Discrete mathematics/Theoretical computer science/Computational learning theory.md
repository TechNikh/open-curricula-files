---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Computational_learning_theory
offline_file: ""
offline_thumbnail: ""
uuid: 2f579652-547c-497b-9cb4-3b1492913137
updated: 1484308678
title: Computational learning theory
tags:
    - Overview
    - Surveys
    - VC dimension
    - Feature selection
    - Inductive inference
    - Optimal O notation learning
    - Negative results
    - Boosting (machine learning)
    - Occam Learning
    - Probably approximately correct learning
    - Error tolerance
    - Equivalence
    - Distribution Learning Theory
categories:
    - Theoretical computer science
---
In computer science, computational learning theory (or just learning theory) is a subfield of Artificial Intelligence devoted to studying the design and analysis of machine learning algorithms.[1]
