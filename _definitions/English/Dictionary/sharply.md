---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sharply
offline_file: ""
offline_thumbnail: ""
uuid: 861096e3-13f0-4235-9302-fd66e3627043
updated: 1484310611
title: sharply
categories:
    - Dictionary
---
sharply
     adv 1: in an aggressive manner; "she was being sharply questioned"
            [syn: {aggressively}]
     2: in a well delineated manner; "the new style of Minoan
        pottery was sharply defined" [syn: {crisply}]
     3: changing suddenly in direction and degree; "the road twists
        sharply after the light"; "turn sharp left here" [syn: {sharp}]
     4: very suddenly and to a great degree; "conditions that
        precipitously increase the birthrate"; "prices rose
        sharply" [syn: {precipitously}]
