---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tellurium
offline_file: ""
offline_thumbnail: ""
uuid: b20d535c-971b-4650-b792-c24b46a3c9b6
updated: 1484310397
title: tellurium
categories:
    - Dictionary
---
tellurium
     n : a brittle silver-white metalloid element that is related to
         selenium and sulfur; it is used in alloys and as a
         semiconductor; occurs mainly as tellurides in ores of
         copper and nickel and silver and gold [syn: {Te}, {atomic
         number 52}]
