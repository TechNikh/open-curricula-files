---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Analyst%27s_traveling_salesman_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 9ce8ccb8-82ab-4580-8368-59003e76dee1
updated: 1484308661
title: "Analyst's traveling salesman theorem"
tags:
    - β-numbers
    - "Jones' traveling salesman theorem in R2"
    - Generalizations and Menger curvature
    - Euclidean space and Hilbert space
    - Menger curvature and metric spaces
    - Denjoy–Riesz theorem
categories:
    - Theorems in discrete mathematics
---
The analyst's traveling salesman problem is an analog of the traveling salesman problem in combinatorial optimization. In its simplest and original form, it asks under what conditions may a set E in two-dimensional Euclidean space 
  
    
      
        
          
            R
          
          
            2
          
        
      
    
    {\displaystyle \mathbb {R} ^{2}}
  
 be contained inside a rectifiable curve of finite length. So while in the original traveling salesman problem, one asks for the shortest way to visit every vertex in a graph with a discrete path, this analytical version requires the curve to visit perhaps infinitely many points.
