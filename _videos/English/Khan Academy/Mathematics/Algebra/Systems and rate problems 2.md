---
version: 1
type: video
provider: YouTube
id: H5w55UbIZTw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Systems%20and%20rate%20problems%202.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bc04b98b-2e84-4f30-9b9c-59a79b6bcfa3
updated: 1486069563
title: Systems and rate problems 2
categories:
    - Algebra
thumbnail_urls:
    - https://i3.ytimg.com/vi/H5w55UbIZTw/default.jpg
    - https://i3.ytimg.com/vi/H5w55UbIZTw/1.jpg
    - https://i3.ytimg.com/vi/H5w55UbIZTw/2.jpg
    - https://i3.ytimg.com/vi/H5w55UbIZTw/3.jpg
---
