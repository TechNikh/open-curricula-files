---
version: 1
type: video
provider: YouTube
id: Wp3ScRXaXVE
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 95273d2b-548a-485e-9282-c951453c42b3
updated: 1486069636
title: 'Optical fibers - physics experiment'
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/Wp3ScRXaXVE/default.jpg
    - https://i3.ytimg.com/vi/Wp3ScRXaXVE/1.jpg
    - https://i3.ytimg.com/vi/Wp3ScRXaXVE/2.jpg
    - https://i3.ytimg.com/vi/Wp3ScRXaXVE/3.jpg
---
