---
version: 1
type: video
provider: YouTube
id: Yaeze9u6Cv8
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Polynomial%20and%20rational%20functions/Solving%20rational%20equations%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: a7c3fac5-ebc0-4ebe-a3fa-4ed9d145f953
updated: 1486069561
title: Solving rational equations 1
categories:
    - Polynomial and rational functions
thumbnail_urls:
    - https://i3.ytimg.com/vi/Yaeze9u6Cv8/default.jpg
    - https://i3.ytimg.com/vi/Yaeze9u6Cv8/1.jpg
    - https://i3.ytimg.com/vi/Yaeze9u6Cv8/2.jpg
    - https://i3.ytimg.com/vi/Yaeze9u6Cv8/3.jpg
---
