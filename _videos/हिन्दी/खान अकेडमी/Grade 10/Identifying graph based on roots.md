---
version: 1
type: video
provider: YouTube
id: yjSs2_Vpe5U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 18fc2968-c4f2-4a36-913d-2fbdb6e6277a
updated: 1486069621
title: Identifying graph based on roots
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/yjSs2_Vpe5U/default.jpg
    - https://i3.ytimg.com/vi/yjSs2_Vpe5U/1.jpg
    - https://i3.ytimg.com/vi/yjSs2_Vpe5U/2.jpg
    - https://i3.ytimg.com/vi/yjSs2_Vpe5U/3.jpg
---
