---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/quantified
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484382721
title: quantified
categories:
    - Dictionary
---
quantify
     v 1: use as a quantifier
     2: express as a number or measure or quantity; "Can you
        quantify your results?" [syn: {measure}]
     [also: {quantified}]
