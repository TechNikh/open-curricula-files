---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Triconex
offline_file: ""
offline_thumbnail: ""
uuid: 47e34ffd-11af-41c0-8b53-acd4e562a5fb
updated: 1484308742
title: Triconex
tags:
    - system
    - Operating theory
    - Hardware
    - Software
    - References and notes
categories:
    - Control engineering
---
Triconex is both the name of a Schneider Electric brand that supplies products, systems and services for safety, critical control and turbomachinery applications and the name of its hardware devices that utilize its TriStation application software. Triconex products are based on patented Triple modular redundancy (TMR) industrial safety-shutdown technology. Today, Triconex TMR products operate globally in more than 11,500 installations, making Triconex the largest TMR supplier in the world.
