---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/driver
offline_file: ""
offline_thumbnail: ""
uuid: aa231c6c-ad0a-4610-86b3-41c689b6a411
updated: 1484310424
title: driver
categories:
    - Dictionary
---
driver
     n 1: the operator of a motor vehicle [ant: {nondriver}]
     2: someone who drives animals that pull a vehicle
     3: a golfer who hits the golf ball with a driver
     4: (computer science) a program that determines how a computer
        will communicate with a peripheral device [syn: {device
        driver}]
     5: a golf club (a wood) with a near vertical face that is used
        for hitting long shots from the tee [syn: {number one wood}]
