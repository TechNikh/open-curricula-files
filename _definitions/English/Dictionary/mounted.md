---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mounted
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484365081
title: mounted
categories:
    - Dictionary
---
mounted
     adj 1: assembled for use; especially by being attached to a support
     2: decorated with applied ornamentation; often used in
        combination; "the trim brass-mounted carbine of the
        ranger"- F.V.W.Mason
