---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nightingale
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484606341
title: nightingale
categories:
    - Dictionary
---
nightingale
     n 1: European songbird noted for its melodious nocturnal song
          [syn: {Luscinia megarhynchos}]
     2: English nurse remembered for her work during the Crimean War
        (1820-1910) [syn: {Florence Nightingale}, {the Lady with
        the Lamp}]
