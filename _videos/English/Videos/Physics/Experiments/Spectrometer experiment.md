---
version: 1
type: video
provider: YouTube
id: -hugZlbn2iY
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 03e8d973-4ba6-4053-9665-4a604b97a945
updated: 1486069635
title: Spectrometer experiment
categories:
    - Experiments
thumbnail_urls:
    - https://i3.ytimg.com/vi/-hugZlbn2iY/default.jpg
    - https://i3.ytimg.com/vi/-hugZlbn2iY/1.jpg
    - https://i3.ytimg.com/vi/-hugZlbn2iY/2.jpg
    - https://i3.ytimg.com/vi/-hugZlbn2iY/3.jpg
---
