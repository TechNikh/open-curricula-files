---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Self-sacrifice
offline_file: ""
offline_thumbnail: ""
uuid: 1eef41cc-e0df-4b86-ae68-47f2ab64d684
updated: 1484308482
title: Self-denial
tags:
    - Religion and self-denial
    - Self-denial based on sex
    - Negative effects
categories:
    - Uncategorized
---
Self-denial (also called self-abnegation[1] and self-sacrifice) refers to altruistic abstinence – the willingness to forgo personal pleasures or undergo personal trials in the pursuit of the increased good of another.[2] Various religions and cultures take differing views of self-denial, some considering it a positive trait and others considering it a negative one. According to some Christians, self-denial is considered a superhuman virtue only obtainable through Jesus.[3] Some critics of self-denial suggest that self-denial can lead to self-hatred and claim that the self-denial practiced in Judaism has created self-hating Jews.[4]
