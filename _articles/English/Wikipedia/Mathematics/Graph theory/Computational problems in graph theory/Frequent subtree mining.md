---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Frequent_subtree_mining
offline_file: ""
offline_thumbnail: ""
uuid: edcee3b8-8ff7-4799-9016-50e81be10f19
updated: 1484309392
title: Frequent subtree mining
tags:
    - Definition
    - Formal definition
    - Algorithms
    - Applications
    - Challenges
categories:
    - Computational problems in graph theory
---
In computer science, frequent subtree mining is the problem of finding all patterns in a given database whose support (a metric related to its number of occurrences in other subtrees) is over a given threshold.[1] It is a more general form of the maximum agreement subtree problem.[2]
