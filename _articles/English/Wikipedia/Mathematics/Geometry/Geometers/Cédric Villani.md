---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/C%C3%A9dric_Villani'
offline_file: ""
offline_thumbnail: ""
uuid: 79bc4013-c2a0-42ed-b98a-23b9ded81d03
updated: 1484309105
title: Cédric Villani
tags:
    - Biography
    - Work
    - Awards and honors
    - Diplomas, titles and awards
    - Extra-academic distinctions
    - Selected writings
categories:
    - Geometers
---
Cédric Patrice Thierry Villani (born 5 October 1973) is a French mathematician working primarily on partial differential equations, Riemannian geometry and mathematical physics. He was awarded the Fields Medal in 2010.
