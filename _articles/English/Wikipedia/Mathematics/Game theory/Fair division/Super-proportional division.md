---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Super-proportional_division
offline_file: ""
offline_thumbnail: ""
uuid: 735422fa-4021-4c52-917c-f839fd159dbf
updated: 1484308518
title: Super-proportional division
tags:
    - Conjecture
    - Existence proof
    - Protocol
    - Single piece of disagreement
    - Two pieces of disagreement
    - Super-proportional division for two partners
    - Super-proportional division for n partners
categories:
    - Fair division
---
In the context of fair cake-cutting, a super-proportional division is a division in which each partner receives strictly more than 1/n of the resource by their own subjective valuation (
  
    
      
        V
        >
        1
        
          /
        
        n
      
    
    {\displaystyle V>1/n}
  
).
