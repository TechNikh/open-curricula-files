---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/symmetry
offline_file: ""
offline_thumbnail: ""
uuid: daa12f3b-7a5b-41c1-91f1-cc3fceca5040
updated: 1484310208
title: symmetry
categories:
    - Dictionary
---
symmetry
     n 1: (mathematics) an attribute of a shape or relation; exact
          correspondence of form on opposite sides of a dividing
          line or plane [syn: {symmetricalness}, {correspondence},
           {balance}] [ant: {asymmetry}]
     2: balance among the parts of something [syn: {proportion}]
        [ant: {disproportion}]
     3: (physics) the property of being isotropic; having the same
        value when measured in different directions [syn: {isotropy}]
        [ant: {anisotropy}]
