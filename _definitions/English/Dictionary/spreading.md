---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spreading
offline_file: ""
offline_thumbnail: ""
uuid: 3450bc2c-6d49-49f8-b148-206e0d8ae24b
updated: 1484310387
title: spreading
categories:
    - Dictionary
---
spreading
     adj 1: spreading over a wide area; "under the spreading chestnut
            tree"; "the spreading circle of lamplight";
            "wide-spreading branches" [syn: {wide-spreading}]
     2: spreading by diffusion [syn: {diffusing(a)}, {diffusive}, {dispersive},
         {disseminative}, {disseminating}, {scattering}]
     n 1: process or result of distributing or extending over a wide
          expanse of space [syn: {spread}]
     2: the opening of a subject to widespread discussion and debate
        [syn: {dissemination}, {airing}, {public exposure}]
     3: act of extending over a wider scope or expanse of space or
        time [syn: {spread}]
