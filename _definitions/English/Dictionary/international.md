---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/international
offline_file: ""
offline_thumbnail: ""
uuid: 48f276ba-8bd7-4f8e-b4e6-e0f715a47a33
updated: 1484310254
title: international
categories:
    - Dictionary
---
international
     adj 1: concerning or belonging to all or at least two or more
            nations; "international affairs"; "an international
            agreement"; "international waters" [ant: {national}]
     2: from or between other countries; "external commerce";
        "international trade"; "developing nations need outside
        help" [syn: {external}, {outside(a)}]
     n : any of several international socialist organizations
