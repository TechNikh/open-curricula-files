---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ponderomotive_force
offline_file: ""
offline_thumbnail: ""
uuid: 6640fabc-f104-4122-861f-e5cf2180e9f1
updated: 1484308764
title: Ponderomotive force
tags:
    - Derivation
    - Time averaged Density
    - Generalized Ponderomotive Force
    - Applications
    - Journals
categories:
    - Electrodynamics
---
The ponderomotive force Fp is expressed by
