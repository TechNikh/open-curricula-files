---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/student
offline_file: ""
offline_thumbnail: ""
uuid: 22010364-68f5-4c7f-8b92-3d6c6f562a8c
updated: 1484310279
title: student
categories:
    - Dictionary
---
student
     n 1: a learner who is enrolled in an educational institution
          [syn: {pupil}, {educatee}]
     2: a learned person (especially in the humanities); someone who
        by long study has gained mastery in one or more
        disciplines [syn: {scholar}, {scholarly person}]
