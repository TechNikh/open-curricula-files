---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dab
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484345521
title: dab
categories:
    - Dictionary
---
dab
     n 1: a light touch or stroke [syn: {tap}, {pat}]
     2: a small quantity of something moist or soft; "a dab of
        paint"; "a splatter of mud" [syn: {splash}, {splatter}]
     v 1: apply (usually a liquid) to a surface; "dab the wall with
          paint" [syn: {swab}, {swob}]
     2: hit lightly; "pat him on the shoulder" [syn: {pat}]
     [also: {dabbing}, {dabbed}]
