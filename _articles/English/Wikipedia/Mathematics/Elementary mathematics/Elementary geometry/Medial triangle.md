---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Medial_triangle
offline_file: ""
offline_thumbnail: ""
uuid: addb1bc7-7603-40df-a920-a9673428d0c4
updated: 1484308930
title: Medial triangle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/55px-Icon_Camera.svg_0.png
tags:
    - Properties
    - Coordinates
    - Anticomplementary triangle
categories:
    - Elementary geometry
---
The medial triangle or midpoint triangle of a triangle ABC is the triangle with vertices at the midpoints of the triangle's sides AB, AC and BC. It is the n=3 case of the midpoint polygon of a polygon with n sides. The medial triangle is not the same thing as the median triangle, which is the triangle whose sides have the same lengths as the medians of ABC.
