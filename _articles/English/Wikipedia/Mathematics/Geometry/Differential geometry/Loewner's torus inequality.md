---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Loewner%27s_torus_inequality'
offline_file: ""
offline_thumbnail: ""
uuid: 6b1146e8-5125-4090-aceb-a9a89d230546
updated: 1484309228
title: "Loewner's torus inequality"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Loewner63.jpg
tags:
    - Statement
    - Case of equality
    - Alternative formulation
    - "Proof of Loewner's torus inequality"
    - Higher genus
categories:
    - Differential geometry
---
In differential geometry, Loewner's torus inequality is an inequality due to Charles Loewner. It relates the systole and the area of an arbitrary Riemannian metric on the 2-torus.
