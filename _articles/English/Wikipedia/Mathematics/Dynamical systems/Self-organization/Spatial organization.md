---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Spatial_organization
offline_file: ""
offline_thumbnail: ""
uuid: 2d6fb485-44d9-4d4c-8429-ae93f5910186
updated: 1484308832
title: Spatial organization
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/175px-Paint_marked_ants.JPG
tags:
    - Spatial organization in eusocial insects
    - Spatial organization in the nest
    - "Foraging-for-work"
    - Dominance hierarchy
    - Spatial organization outside the nest
    - >
        Spatial organization as an emergent property of a
        self-organized system
categories:
    - Self-organization
---
Spatial organization can be observed when components of an abiotic or biological group are arranged non-randomly in space. Abiotic patterns, such as the ripple formations in sand dunes or the oscillating wave patterns of the Belousov-Zhabotinsky reaction[1] emerge after thousands of particles interact millions of times. On the other hand, individuals in biological groups may be arranged non-randomly due to selfish behavior, dominance interactions, or cooperative behavior. W. D. Hamilton (1971) proposed that in a non-related "herd" of animals, spatial organization is likely a result of the selfish interests of individuals trying to acquire food or avoid predation.[2] On the other hand, ...
