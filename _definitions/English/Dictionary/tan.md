---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tan
offline_file: ""
offline_thumbnail: ""
uuid: 07b9096f-6bb3-4a6b-ab50-18266044ce28
updated: 1484310216
title: tan
categories:
    - Dictionary
---
tan
     adj : of a light yellowish-brown color
     n 1: a browning of the skin resulting from exposure to the rays
          of the sun [syn: {suntan}, {sunburn}, {burn}]
     2: a light brown [syn: {topaz}]
     3: ratio of the opposite to the adjacent side of a right-angled
        triangle [syn: {tangent}]
     v 1: treat skins and hides with tannic acid so as to convert them
          into leather
     2: get a tan, from wind or sun [syn: {bronze}]
     [also: {tanning}, {tanned}, {tannest}, {tanner}]
