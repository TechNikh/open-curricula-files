---
version: 1
type: video
provider: YouTube
id: jVD67pMdv9k
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fccae2fd-374c-4660-bb4c-cb28d7536a86
updated: 1486069643
title: Electric Sheep in HD (2-hour 1080p Fractal Animation)
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/jVD67pMdv9k/default.jpg
    - https://i3.ytimg.com/vi/jVD67pMdv9k/1.jpg
    - https://i3.ytimg.com/vi/jVD67pMdv9k/2.jpg
    - https://i3.ytimg.com/vi/jVD67pMdv9k/3.jpg
---
