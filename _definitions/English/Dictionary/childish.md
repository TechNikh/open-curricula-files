---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/childish
offline_file: ""
offline_thumbnail: ""
uuid: e18d910d-679c-48b7-884f-f60d7e1e8835
updated: 1484310152
title: childish
categories:
    - Dictionary
---
childish
     adj : indicating a lack of maturity; "childish tantrums";
           "infantile behavior" [syn: {infantile}]
