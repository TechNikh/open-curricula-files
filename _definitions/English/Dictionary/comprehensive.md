---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/comprehensive
offline_file: ""
offline_thumbnail: ""
uuid: 2268039b-fee2-478b-a94c-626cef02521d
updated: 1484310581
title: comprehensive
categories:
    - Dictionary
---
comprehensive
     adj 1: including all or everything; "comprehensive coverage"; "a
            comprehensive history of the revolution"; "a
            comprehensive survey"; "a comprehensive education"
            [ant: {noncomprehensive}]
     2: broad in scope; "a comprehensive survey of world affairs"
     3: being the most comprehensive of its class; "an unabridged
        dictionary"
