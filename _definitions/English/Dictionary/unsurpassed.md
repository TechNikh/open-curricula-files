---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unsurpassed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484559541
title: unsurpassed
categories:
    - Dictionary
---
unsurpassed
     adj : not capable of being improved on [syn: {unexcelled}, {unexceeded}]
