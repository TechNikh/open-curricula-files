---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/printing
offline_file: ""
offline_thumbnail: ""
uuid: dd7a6e27-1f92-49ce-8959-5cd94e4864b3
updated: 1484310527
title: printing
categories:
    - Dictionary
---
printing
     n 1: text handwritten in the style of printed matter
     2: the business of printing
     3: reproduction by applying ink to paper as for publication
        [syn: {printing process}]
     4: all the copies of a work printed at one time; "they ran off
        an initial printing of 2000 copies" [syn: {impression}]
