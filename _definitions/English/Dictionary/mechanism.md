---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mechanism
offline_file: ""
offline_thumbnail: ""
uuid: f41a7579-6358-4ca6-8fca-d0ac146ffd6d
updated: 1484310335
title: mechanism
categories:
    - Dictionary
---
mechanism
     n 1: the atomic process that occurs during a chemical reaction;
          "he determined unique mechanisms for the photochemical
          reactions" [syn: {chemical mechanism}]
     2: the technical aspects of doing something; "a mechanism of
        social control"; "mechanisms of communication"; "the
        mechanics of prose style" [syn: {mechanics}]
     3: a natural object resembling a machine in structure or
        function; "the mechanism of the ear"; "the mechanism of
        infection"
     4: (philosophy) the philosophical theory that all phenomena can
        be explained in terms of physical or biological causes
     5: device consisting of a piece of ...
