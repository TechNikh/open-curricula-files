---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sphere
offline_file: ""
offline_thumbnail: ""
uuid: 484202eb-4903-4bc7-ad18-495d672229a1
updated: 1484308941
title: Sphere
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/440px-Sphere_wireframe_10deg_6r.svg_.png
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/440px-Sphere_and_Ball.png
tags:
    - Surface area
    - Enclosed volume
    - Equations in three-dimensional space
    - Terminology
    - Hemisphere
    - Generalizations
    - Dimensionality
    - Metric spaces
    - Topology
    - Spherical geometry
    - Eleven properties of the sphere
    - Cubes in relation to spheres
categories:
    - Elementary geometry
---
A sphere (from Greek σφαῖρα — sphaira, "globe, ball"[1]) is a perfectly round geometrical object in three-dimensional space that is the surface of a completely round ball, (viz., analogous to a circular object in two dimensions).[2]
