---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bottle_gourd
offline_file: ""
offline_thumbnail: ""
uuid: 5a5c1577-4c48-423b-ad65-adc35fd5858e
updated: 1484308365
title: Calabash
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/180px-Refreshing_palm_wine.jpg
tags:
    - Origin and dispersal
    - Cultivation
    - Occasional toxicity
    - Culinary uses
    - Central America
    - East Asia
    - China
    - Japan
    - Korea
    - Ingredients
    - Dishes
    - Europe
    - Middle East
    - South Asia
    - Bangladesh
    - India
    - Nepal
    - Pakistan
    - Sri Lanka
    - Southeast Asia
    - Burma
    - Philippines
    - Vietnam
    - Cultural uses
    - Africa
    - Caribbean
    - China
    - Costa Rica
    - Hawaii
    - India
    - Mexico
    - South America
    - Venezuela
    - Other uses
categories:
    - Uncategorized
---
The calabash, bottle gourd,[1] or white-flowered gourd,[2] Lagenaria siceraria (synonym Lagenaria vulgaris Ser.), also known by many other names that include: opo squash (from Tagalog: upo), long melon, suzza melon, New Guinea bean and Tasmania bean[3] is a vine grown for its fruit, which can either be harvested young and used as a vegetable, or harvested mature, dried, and used as a bottle, utensil, or pipe. The fresh fruit has a light-green smooth skin and a white flesh. Rounder varieties are called calabash gourds. They grow in a variety of shapes: they can be huge and rounded, small and bottle shaped, or slim and serpentine, more than a metre long. Because bottle gourds are also called ...
