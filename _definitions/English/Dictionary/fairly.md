---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fairly
offline_file: ""
offline_thumbnail: ""
uuid: 83f562c8-b827-499c-a1b7-aeb6a173ad3e
updated: 1484310535
title: fairly
categories:
    - Dictionary
---
fairly
     adv 1: to a moderately sufficient extent or degree; "the shoes are
            priced reasonably"; "he is fairly clever with
            computers"; "they lived comfortably within reason"
            [syn: {reasonably}, {moderately}, {within reason}, {somewhat},
             {middling}, {passably}] [ant: {unreasonably}, {unreasonably}]
     2: in a fair evenhanded manner; "deal fairly with one another"
        [syn: {fair}, {without favoring one party}, {without
        favouring one party}, {evenhandedly}]
     3: in conformity with the rules or laws and without fraud or
        cheating; "they played fairly" [syn: {fair}, {clean}]
        [ant: {unfairly}]
