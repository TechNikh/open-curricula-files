---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/optional
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484497921
title: optional
categories:
    - Dictionary
---
optional
     adj : possible but not necessary; left to personal choice [ant: {obligatory}]
