---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/madam
offline_file: ""
offline_thumbnail: ""
uuid: 37097f93-a2f3-4617-aa57-0ecf2e4900b1
updated: 1484310138
title: madam
categories:
    - Dictionary
---
madam
     n 1: a woman of refinement; "a chauffeur opened the door of the
          limousine for the grand lady" [syn: {dame}, {ma'am}, {lady},
           {gentlewoman}]
     2: a woman who runs a house of prostitution [syn: {brothel
        keeper}]
