---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/john
offline_file: ""
offline_thumbnail: ""
uuid: 9cdc1a16-9853-402b-9e28-6422eb254826
updated: 1484310397
title: john
categories:
    - Dictionary
---
john
     n 1: a room equipped with toilet facilities [syn: {toilet}, {lavatory},
           {lav}, {can}, {privy}, {bathroom}]
     2: youngest son of Henry II; King of England from 1199 to 1216;
        succeeded to the throne on the death of his brother
        Richard I; lost his French possessions; in 1215 John was
        compelled by the barons to sign the Magna Carta
        (1167-1216) [syn: {King John}, {John Lackland}]
     3: (New Testament) disciple of Jesus; traditionally said to be
        the author of the 4th Gospel and three epistles and the
        book of Revelation [syn: {Saint John}, {St. John}, {Saint
        John the Apostle}, {St. John the Apostle}, {John the
       ...
