---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/extrovert
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484415901
title: extrovert
categories:
    - Dictionary
---
extrovert
     adj : characterized by extroversion [syn: {extravert}, {extroverted},
            {extraverted}, {extrovertive}, {extravertive}]
     n : (psychology) a person concerned more with practical
         realities than with inner thoughts and feelings [syn: {extravert}]
         [ant: {introvert}]
