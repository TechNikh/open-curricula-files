---
version: 1
type: video
provider: YouTube
id: 2PM1yc_z4Bk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Chemistry/Chemical%20equilibrium/Introduction%20to%20reaction%20quotient%20Qc.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4d649cd3-c90a-4aab-97bc-8a9cbbd86dfd
updated: 1486069587
title: Introduction to reaction quotient Qc
tags:
    - Education
    - online learning
    - learning
    - lessons
categories:
    - Chemical equilibrium
thumbnail_urls:
    - https://i3.ytimg.com/vi/2PM1yc_z4Bk/default.jpg
    - https://i3.ytimg.com/vi/2PM1yc_z4Bk/1.jpg
    - https://i3.ytimg.com/vi/2PM1yc_z4Bk/2.jpg
    - https://i3.ytimg.com/vi/2PM1yc_z4Bk/3.jpg
---
Introduction to the reaction quotient Qc, and how to use the reaction quotient to predict how the reaction concentrations will change. 

Chemistry on Khan Academy: Did you know that everything is made out of chemicals? Chemistry is the study of matter: its composition, properties, and reactivity. This material roughly covers a first-year high school or college course, and a good understanding of algebra is helpful.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan AcademyÃ¢ÂÂs Chemistry channel: https://www.youtube.com/channel/UCyEot66LrwWFEMONvrIBh3A?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
