---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Quasi-isometry
offline_file: ""
offline_thumbnail: ""
uuid: 06a5d237-d3e4-487a-a057-2412914da518
updated: 1484309272
title: Quasi-isometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Equilateral_Triangle_Lattice.svg.png
tags:
    - Definition
    - Examples
    - Equivalence relation
    - Use in geometric group theory
    - Quasigeodesics and the Morse lemma
    - Examples of quasi-isometry invariants of groups
    - Hyperbolicity
    - growth
    - Ends
    - Amenability
    - Asymptotic cone
categories:
    - Metric geometry
---
In mathematics, quasi-isometry is an equivalence relation on metric spaces that ignores their small-scale details in favor of their coarse structure. The concept is especially important in geometric group theory following the work of Gromov.[1]
