---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Euler%27s_rotation_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 3d3e35ff-2a99-4890-9583-719d843c4311
updated: 1484309114
title: "Euler's rotation theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Euler_AxisAngle.png
tags:
    - "Euler's theorem (1776)"
    - Proof
    - previous analysis
    - construction of the best candidate point
    - proof of its invariance under the transformation
    - final notes about the construction
    - Matrix proof
    - Equivalence of an orthogonal matrix to a rotation matrix
    - Excursion into matrix theory
    - Equivalence classes
    - Applications
    - Generators of rotations
    - Quaternions
    - Generalizations
    - Notes
categories:
    - Theorems in geometry
---
In geometry, Euler's rotation theorem states that, in three-dimensional space, any displacement of a rigid body such that a point on the rigid body remains fixed, is equivalent to a single rotation about some axis that runs through the fixed point. It also means that the composition of two rotations is also a rotation. Therefore the set of rotations has a group structure, known as a rotation group.
