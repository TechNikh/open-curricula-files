---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Star_product
offline_file: ""
offline_thumbnail: ""
uuid: 69c6f2fb-f995-453e-8e91-eade6439a8ce
updated: 1484308590
title: Star product
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/He1523a.jpg
tags:
    - Definition
    - Example
    - Properties
categories:
    - Combinatorics
---
In mathematics, the star product is a method of combining graded posets with unique minimal and maximal elements, preserving the property that the posets are Eulerian.
