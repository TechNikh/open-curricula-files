---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/peeled
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484647561
title: peeled
categories:
    - Dictionary
---
peeled
     adj : (used informally) completely unclothed [syn: {bare-assed}, {bare-ass},
            {in the altogether}, {in the buff}, {in the raw}, {raw},
            {naked as a jaybird}, {stark naked}]
