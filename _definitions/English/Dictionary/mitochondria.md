---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mitochondria
offline_file: ""
offline_thumbnail: ""
uuid: b4a4163f-afaf-407b-9ddf-7cd657bba588
updated: 1484310162
title: mitochondria
categories:
    - Dictionary
---
mitochondrion
     n : an organelle containing enzymes responsible for producing
         energy [syn: {chondriosome}]
     [also: {mitochondria} (pl)]
