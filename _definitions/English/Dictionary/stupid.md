---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/stupid
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484423581
title: stupid
categories:
    - Dictionary
---
stupid
     adj 1: lacking or marked by lack of intellectual acuity [ant: {smart}]
     2: in a state of mental numbness especially as resulting from
        shock; "he had a dazed expression on his face"; "lay
        semiconscious, stunned (or stupefied) by the blow"; "was
        stupid from fatigue" [syn: {dazed}, {stunned}, {stupefied},
         {stupid(p)}]
     3: without much intelligence; "a dull job with lazy and
        unintelligent co-workers" [syn: {unintelligent}] [ant: {intelligent}]
     n : a person who is not very bright; "The economy, stupid!"
         [syn: {stupid person}, {dullard}, {dolt}, {pudding head},
          {pudden-head}, {poor fish}, {pillock}]
