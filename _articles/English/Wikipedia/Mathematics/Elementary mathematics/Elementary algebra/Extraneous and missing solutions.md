---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Extraneous_and_missing_solutions
offline_file: ""
offline_thumbnail: ""
uuid: ccac61d8-238f-4e40-8670-2954a2f8b58c
updated: 1484308909
title: Extraneous and missing solutions
tags:
    - 'Extraneous solutions: multiplication'
    - 'Extraneous solutions: rational'
    - 'Missing solutions: division'
    - Other operations
categories:
    - Elementary algebra
---
In mathematics, an extraneous solution (or spurious solution) is a solution, such as that to an equation, that emerges from the process of solving the problem but is not a valid solution to the original problem. A missing solution is a solution that was a valid solution to the original problem, but disappeared during the process of solving the problem. Both are frequently the consequence of performing operations that are not invertible for some or all values of the variables, which prevents the chain of logical implications in the proof from being bidirectional.
