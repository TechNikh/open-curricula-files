---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/taking
offline_file: ""
offline_thumbnail: ""
uuid: 96efb8ae-f8d9-4746-98ac-469548a7f9cb
updated: 1484310311
title: taking
categories:
    - Dictionary
---
taking
     adj : very attractive; capturing interest; "a fetching new
           hairstyle"; "something inexpressibly taking in his
           manner"; "a winning personality" [syn: {fetching}, {winning}]
     n : the act of someone who picks up or takes something; "the
         pickings were easy"; "clothing could be had for the
         taking" [syn: {pickings}]
