---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/substantial
offline_file: ""
offline_thumbnail: ""
uuid: bcaace21-bf4b-4d13-892d-3a5bb97f2e98
updated: 1484310455
title: substantial
categories:
    - Dictionary
---
substantial
     adj 1: fairly large; "won by a substantial margin" [syn: {significant}]
     2: being the essence or essential element of a thing;
        "substantial equivalents"; "substantive information" [syn:
         {substantive}, {in essence}]
     3: having substance or capable of being treated as fact; not
        imaginary; "the substantial world"; "a mere dream, neither
        substantial nor practical"; "most ponderous and
        substantial things"- Shakespeare [syn: {real}, {material}]
        [ant: {insubstantial}]
     4: providing abundant nourishment; "a hearty meal"; "good solid
        food"; "ate a substantial breakfast" [syn: {hearty}, {satisfying},
         ...
