---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Non-equilibrium_thermodynamics
offline_file: ""
offline_thumbnail: ""
uuid: e47b3bcc-e9dd-4065-903a-9b08fae17da7
updated: 1484308808
title: Non-equilibrium thermodynamics
tags:
    - Scope of non-equilibrium thermodynamics
    - >
        Difference between equilibrium and non-equilibrium
        thermodynamics
    - Non-equilibrium state variables
    - Overview
    - >
        Quasi-radiationless non-equilibrium thermodynamics of matter
        in laboratory conditions
    - Local equilibrium thermodynamics
    - >
        Local equilibrium thermodynamics with materials with
        "memory"
    - Extended irreversible thermodynamics
    - Basic concepts
    - Stationary states, fluctuations, and stability
    - Local thermodynamic equilibrium
    - Local thermodynamic equilibrium of ponderable matter
    - "Milne's 1928 definition of local thermodynamic equilibrium in terms of radiative equilibrium"
    - Entropy in evolving systems
    - Flows and forces
    - The Onsager relations
    - Speculated extremal principles for non-equilibrium processes
    - Applications of non-equilibrium thermodynamics
    - Bibliography of cited references
categories:
    - Non-equilibrium thermodynamics
---
Non-equilibrium thermodynamics is a branch of thermodynamics that deals with physical systems that are not in thermodynamic equilibrium but can be adequately described in terms of variables (non-equilibrium state variables) that represent an extrapolation of the variables used to specify the system in thermodynamic equilibrium. Non-equilibrium thermodynamics is concerned with transport processes and with the rates of chemical reactions. It relies on what may be thought of as more or less nearness to thermodynamic equilibrium. Non-equilibrium thermodynamics is a work in progress, not an established edifice. This article will try to sketch some approaches to it and some concepts important for ...
