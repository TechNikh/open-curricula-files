---
version: 1
type: video
provider: YouTube
id: F2psxMnGdUw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Solid%20of%20Revolution%20%28part%206%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: f5f24975-bf23-429e-93b7-a18746974a6c
updated: 1486069694
title: Solid of Revolution (part 6)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/F2psxMnGdUw/default.jpg
    - https://i3.ytimg.com/vi/F2psxMnGdUw/1.jpg
    - https://i3.ytimg.com/vi/F2psxMnGdUw/2.jpg
    - https://i3.ytimg.com/vi/F2psxMnGdUw/3.jpg
---
