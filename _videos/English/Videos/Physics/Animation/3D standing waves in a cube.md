---
version: 1
type: video
provider: YouTube
id: cAp3V6FWQ60
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 553065cc-8466-42f4-8e5e-514883c6b465
updated: 1486069651
title: 3D standing waves in a cube
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/cAp3V6FWQ60/default.jpg
    - https://i3.ytimg.com/vi/cAp3V6FWQ60/1.jpg
    - https://i3.ytimg.com/vi/cAp3V6FWQ60/2.jpg
    - https://i3.ytimg.com/vi/cAp3V6FWQ60/3.jpg
---
