---
version: 1
type: video
provider: YouTube
id: Ktv-CaOt6UQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c9d00c96-db6c-466b-9bfa-b5f3b6f10aa7
updated: 1486069681
title: 'Muscles, part 1 - Muscle Cells- Crash Course A&P #21'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/Ktv-CaOt6UQ/default.jpg
    - https://i3.ytimg.com/vi/Ktv-CaOt6UQ/1.jpg
    - https://i3.ytimg.com/vi/Ktv-CaOt6UQ/2.jpg
    - https://i3.ytimg.com/vi/Ktv-CaOt6UQ/3.jpg
---
