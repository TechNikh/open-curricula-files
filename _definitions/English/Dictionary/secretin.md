---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/secretin
offline_file: ""
offline_thumbnail: ""
uuid: 82367fa7-9a5d-4d05-9a2b-0f3f354529b1
updated: 1483328796
title: secretin
categories:
    - Dictionary
---
secretin
     n : peptic hormone produced by the mucous lining of the small
         intestine; can stimulate secretion by the pancreas and
         liver
