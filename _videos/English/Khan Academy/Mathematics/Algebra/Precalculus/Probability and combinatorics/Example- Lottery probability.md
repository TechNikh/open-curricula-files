---
version: 1
type: video
provider: YouTube
id: DIjlllgq3dc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Precalculus/Probability%20and%20combinatorics/Example-%20Lottery%20probability.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 310dc7bb-7f42-46d5-8051-6d1080e05ee2
updated: 1486069561
title: 'Example- Lottery probability'
categories:
    - Probability and combinatorics
thumbnail_urls:
    - https://i3.ytimg.com/vi/DIjlllgq3dc/default.jpg
    - https://i3.ytimg.com/vi/DIjlllgq3dc/1.jpg
    - https://i3.ytimg.com/vi/DIjlllgq3dc/2.jpg
    - https://i3.ytimg.com/vi/DIjlllgq3dc/3.jpg
---
