---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/feminine
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484557921
title: feminine
categories:
    - Dictionary
---
feminine
     adj 1: associated with women and not with men; "feminine intuition"
            [ant: {masculine}]
     2: of grammatical gender [ant: {neuter}, {masculine}]
     3: befitting or characteristic of a woman especially a mature
        woman; "womanly virtues of gentleness and compassion"
        [syn: {womanly}] [ant: {unwomanly}]
     4: (music or poetry) ending on an unaccented beat or syllable;
        "a feminine ending"
     n : a gender that refers chiefly (but not exclusively) to
         females or to objects classified as female
