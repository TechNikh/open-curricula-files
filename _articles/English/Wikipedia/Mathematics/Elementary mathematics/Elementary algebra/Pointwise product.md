---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pointwise_product
offline_file: ""
offline_thumbnail: ""
uuid: 075dd83f-51c5-4062-bc93-5562acd5e1a5
updated: 1484308912
title: Pointwise product
tags:
    - Formal definition
    - Examples
    - Algebraic application of pointwise products
    - Generalization
categories:
    - Elementary algebra
---
The pointwise product of two functions is another function, obtained by multiplying the image of the two functions at each value in the domain. If f and g are both functions with domain X and codomain Y, and elements of Y can be multiplied (for instance, Y could be some set of numbers), then the pointwise product of f and g is another function from X to Y which maps x ∈ X to f(x)g(x).
