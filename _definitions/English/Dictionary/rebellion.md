---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rebellion
offline_file: ""
offline_thumbnail: ""
uuid: 9415e3e9-4d13-4a53-82bf-1e87c6cae127
updated: 1484310575
title: rebellion
categories:
    - Dictionary
---
rebellion
     n 1: refusal to accept some authority or code or convention;
          "each generation must have its own rebellion"; "his body
          was in rebellion against fatigue"
     2: organized opposition to authority; a conflict in which one
        faction tries to wrest control from another [syn: {insurrection},
         {revolt}, {rising}, {uprising}]
