---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Adaptive_system
offline_file: ""
offline_thumbnail: ""
uuid: 604aee0a-7b97-429b-bb0a-2a6571dc2213
updated: 1484308722
title: Adaptive system
tags:
    - The law of adaptation
    - Benefit of self-adjusting systems
    - 'Practopoiesis: Adaptation across different levels of organization'
    - Notes
categories:
    - Control engineering
---
The term adaptation is used in biology in relation to how living beings adapt to their environments, but with two different meanings. First, the continuous adaptation of an organism to its environment, so as to maintain itself in a viable state, through sensory feedback mechanisms. Second, the development (through evolutionary steps) of an adaptation (an anatomic structure, physiological process or behavior characteristic) that increases the probability of an organism reproducing itself (although sometimes not directly).[citation needed]
