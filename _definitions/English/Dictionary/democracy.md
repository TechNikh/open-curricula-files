---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/democracy
offline_file: ""
offline_thumbnail: ""
uuid: d91d67b5-68e0-4ba2-874d-318b92ac1bfa
updated: 1484310471
title: democracy
categories:
    - Dictionary
---
democracy
     n 1: the political orientation of those who favor government by
          the people or by their elected representatives
     2: a political system in which the supreme power lies in a body
        of citizens who can elect people to represent them [syn: {republic},
         {commonwealth}] [ant: {autocracy}]
     3: the doctrine that the numerical majority of an organized
        group can make decisions binding on the whole group [syn:
        {majority rule}]
