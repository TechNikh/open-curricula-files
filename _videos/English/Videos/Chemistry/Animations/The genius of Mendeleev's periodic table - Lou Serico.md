---
version: 1
type: video
provider: YouTube
id: fPnwBITSmgU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/The%20genius%20of%20Mendeleev%27s%20periodic%20table%20-%20Lou%20Serico.webm"
offline_file: ""
offline_thumbnail: ""
uuid: c73a065a-08d1-4e2f-9fd2-38c6f68e26ff
updated: 1486069662
title: "The genius of Mendeleev's periodic table - Lou Serico"
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/fPnwBITSmgU/default.jpg
    - https://i3.ytimg.com/vi/fPnwBITSmgU/1.jpg
    - https://i3.ytimg.com/vi/fPnwBITSmgU/2.jpg
    - https://i3.ytimg.com/vi/fPnwBITSmgU/3.jpg
---
