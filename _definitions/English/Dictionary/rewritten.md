---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rewritten
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484321041
title: rewritten
categories:
    - Dictionary
---
rewrite
     n : something that has been written again; "the rewrite was much
         better" [syn: {revision}, {rescript}]
     v 1: write differently; alter the writing of; "The student
          rewrote his thesis"
     2: rewrite so as to make fit to suit a new or different
        purpose; "re-write a play for use in schools"
     [also: {rewrote}, {rewritten}]
