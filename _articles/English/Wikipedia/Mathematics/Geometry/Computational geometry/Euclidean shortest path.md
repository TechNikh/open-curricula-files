---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Euclidean_shortest_path
offline_file: ""
offline_thumbnail: ""
uuid: e23d0e06-59d6-4cd1-b411-fb75cc3138dc
updated: 1484309175
title: Euclidean shortest path
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Euclidean_Shortest_Path_KernelCAD_Screenshot.jpg
tags:
    - Notes
categories:
    - Computational geometry
---
The Euclidean shortest path problem is a problem in computational geometry: given a set of polyhedral obstacles in a Euclidean space, and two points, find the shortest path between the points that does not intersect any of the obstacles.
