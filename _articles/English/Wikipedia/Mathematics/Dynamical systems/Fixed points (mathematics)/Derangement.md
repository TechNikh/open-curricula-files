---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Derangement
offline_file: ""
offline_thumbnail: ""
uuid: c707c655-d5ec-4578-bd64-2efa0d3e8de5
updated: 1484308782
title: Derangement
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/300px-N%2521_v_%2521n.svg.png'
tags:
    - Example
    - Counting derangements
    - >
        Limit of ratio of derangement to permutation as n approaches
        ∞
    - Generalizations
    - Computational complexity
categories:
    - Fixed points (mathematics)
---
The number of derangements of a set of size n, usually written Dn, dn, or !n, is called the "derangement number" or "de Montmort number". (These numbers are generalized to rencontres numbers.) The subfactorial function (not to be confused with the factorial n!) maps n to !n.[1] No standard notation for subfactorials is agreed upon; n¡ is sometimes used instead of !n.[2]
