---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mail
offline_file: ""
offline_thumbnail: ""
uuid: ecc80660-37fc-4f85-a3b4-1f7a633b24e1
updated: 1484308460
title: Mail
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Postal_sorting_office_at_Sion.JPG
tags:
    - Etymology
    - History
    - Persia
    - India
    - China
    - Rome
    - Mongol Empire
    - Other systems
    - Postal reforms
    - Modern transport and technology
categories:
    - Uncategorized
---
The mail or post is a system for physically transporting documents and other small packages; or, the postcards, letters, and parcels themselves.[1] A postal service can be private or public, though many governments place restrictions on private systems. Since the mid-19th century national postal systems have generally been established as government monopolies with a fee on the article prepaid. Proof of payment is often in the form of adhesive postage stamps, but postage meters are also used for bulk mailing. Modern private postal systems are typically distinguished from national postal agencies by the names "courier" or "delivery service".
