---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/depressing
offline_file: ""
offline_thumbnail: ""
uuid: 70036829-5e1e-4770-83c8-54da439ec16d
updated: 1484310313
title: depressing
categories:
    - Dictionary
---
depressing
     adj 1: causing dejection; "a blue day"; "the dark days of the war";
            "a week of rainy depressing weather"; "a disconsolate
            winter landscape"; "the first dismal dispiriting days
            of November"; "a dark gloomy day"; "grim rainy
            weather" [syn: {blue}, {dark}, {disconsolate}, {dismal},
             {dispiriting}, {gloomy}, {grim}]
     2: causing or suggestive of sorrow or gloom; "a gloomy
        outlook"; "gloomy news" [syn: {depressive}, {gloomy}, {saddening}]
