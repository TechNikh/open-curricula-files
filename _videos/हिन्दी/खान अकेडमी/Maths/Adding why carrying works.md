---
version: 1
type: video
provider: YouTube
id: gAt5zYkH2Jw
offline_file: ""
offline_thumbnail: ""
uuid: 98c5c307-949b-4faf-857e-da1cbba1e660
updated: 1484040352
title: Adding why carrying works
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/gAt5zYkH2Jw/default.jpg
    - https://i3.ytimg.com/vi/gAt5zYkH2Jw/1.jpg
    - https://i3.ytimg.com/vi/gAt5zYkH2Jw/2.jpg
    - https://i3.ytimg.com/vi/gAt5zYkH2Jw/3.jpg
---
