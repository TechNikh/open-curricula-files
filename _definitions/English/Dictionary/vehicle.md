---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vehicle
offline_file: ""
offline_thumbnail: ""
uuid: d168f266-39f1-4746-ab3b-4af87d8aa7d2
updated: 1484310484
title: vehicle
categories:
    - Dictionary
---
vehicle
     n 1: a conveyance that transports people or objects
     2: a medium for the expression or achievement of something;
        "his editorials provided a vehicle for his political
        views"; "a congregation is a vehicle of group identity"
     3: any inanimate object (as a towel or money or clothing or
        dishes or books or toys etc.) that can transmit infectious
        agents from one person to another [syn: {fomite}]
