---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wildly
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484482321
title: wildly
categories:
    - Dictionary
---
wildly
     adv 1: to an extreme or greatly exaggerated degree; "the storyline
            is wildly unrealistic"
     2: in an uncontrolled or unrestrained manner; "He gesticulated
        wildly"
     3: with violent and uncontrollable passion; "attacked wildly,
        slashing and stabbing over and over"
