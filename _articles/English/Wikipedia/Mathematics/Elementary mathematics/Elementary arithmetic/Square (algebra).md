---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Square_(algebra)
offline_file: ""
offline_thumbnail: ""
uuid: c3a7c473-abac-4f49-bcd3-08d480df643b
updated: 1484308924
title: Square (algebra)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/168px-Five_Squared.svg.png
tags:
    - In real numbers
    - In geometry
    - In abstract algebra and number theory
    - In complex numbers and related algebras over the reals
    - Other uses
    - Related identities
    - Related physical quantities
    - Footnotes
categories:
    - Elementary arithmetic
---
In mathematics, a square is the result of multiplying a number by itself. The verb "to square" is used to denote this operation. Squaring is the same as raising to the power 2, and is denoted by a superscript 2; for instance, the square of 3 may be written as 32, which is the number 9. In some cases when superscripts are not available, as for instance in programming languages or plain text files, the notations x^2 or x**2 may be used in place of x2.
