---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vicinity
offline_file: ""
offline_thumbnail: ""
uuid: 2747cb92-150d-4468-bef9-c801b2fb016d
updated: 1484310226
title: vicinity
categories:
    - Dictionary
---
vicinity
     n : a surrounding or nearby region; "the plane crashed in the
         vicinity of Asheville"; "it is a rugged locality"; "he
         always blames someone else in the immediate
         neighborhood"; "I will drop in on you the next time I am
         in this neck of the woods" [syn: {locality}, {neighborhood},
          {neighbourhood}, {neck of the woods}]
