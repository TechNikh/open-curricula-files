---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/continental
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484631301
title: continental
categories:
    - Dictionary
---
continental
     adj 1: of or pertaining to or typical of Europe; "a Continental
            breakfast"
     2: of or relating to or concerning the American colonies during
        and immediately after the Revolutionary War; "the
        Continental Army"; "the Continental Congress"
     3: of or relating to or characteristic of a continent; "the
        continental divide"; "continental drift"
     4: being or concerning or limited to a continent especially the
        continents of North America or Europe; "the continental
        United States"; "continental Europe"; "continental waters"
        [ant: {intercontinental}]
