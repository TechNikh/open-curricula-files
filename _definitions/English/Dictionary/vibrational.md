---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vibrational
offline_file: ""
offline_thumbnail: ""
uuid: 51f62456-7847-4747-b05c-67df3a90184b
updated: 1484310232
title: vibrational
categories:
    - Dictionary
---
vibrational
     adj : of or relating to or characterized by vibration
