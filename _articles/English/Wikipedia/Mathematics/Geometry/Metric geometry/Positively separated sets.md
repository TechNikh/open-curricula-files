---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Positively_separated_sets
offline_file: ""
offline_thumbnail: ""
uuid: 68259c90-83da-4a50-941a-ae299a28862f
updated: 1484309272
title: Positively separated sets
categories:
    - Metric geometry
---
is strictly positive. (Some authors also specify that A and B should be disjoint sets; however, this adds nothing to the definition, since if A and B have some common point p, then d(p, p) = 0, and so the infimum above is clearly 0 in that case.)
