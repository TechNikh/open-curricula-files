---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hassler_Whitney
offline_file: ""
offline_thumbnail: ""
uuid: 6e077c9a-2b26-4225-ab4b-db502c0556fa
updated: 1484309107
title: Hassler Whitney
tags:
    - Biography
    - life
    - Death
    - Academic career
    - Honors
    - Work
    - Research
    - Teaching activity
    - Teaching the youth
    - Selected publications
    - Notes
    - Biographical and general references
    - Scientific references
categories:
    - Geometers
---
Hassler Whitney (March 23, 1907 – May 10, 1989) was an American mathematician. He was one of the founders of singularity theory, and did foundational work in manifolds, embeddings, immersions, characteristic classes, and geometric integration theory.
