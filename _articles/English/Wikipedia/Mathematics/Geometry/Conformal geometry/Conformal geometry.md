---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conformal_geometry
offline_file: ""
offline_thumbnail: ""
uuid: d9d4b82d-c4b3-43f9-bc72-ee8463a4fc70
updated: 1484309187
title: Conformal geometry
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Conformal_grid_before_M%25C3%25B6bius_transformation.svg.png'
tags:
    - Conformal manifolds
    - Möbius geometry
    - Two dimensions
    - Minkowski plane
    - Euclidean space
    - Higher dimensions
    - The inversive model
    - The projective model
    - The Euclidean sphere
    - Representative metrics
    - Ambient metric model
    - The Kleinian model
    - The conformal Lie algebras
    - Notes
categories:
    - Conformal geometry
---
In mathematics, conformal geometry is the study of the set of angle-preserving (conformal) transformations on a space. In two real dimensions, conformal geometry is precisely the geometry of Riemann surfaces. In more than two dimensions, conformal geometry may refer either to the study of conformal transformations of "flat" spaces (such as Euclidean spaces or spheres), or, more commonly, to the study of conformal manifolds, which are Riemannian or pseudo-Riemannian manifolds with a class of metrics that are defined up to scale. Study of the flat structures is sometimes termed Möbius geometry, and is a type of Klein geometry.
