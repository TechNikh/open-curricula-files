---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/permit
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484628721
title: permit
categories:
    - Dictionary
---
permit
     n 1: a legal document giving official permission to do something
          [syn: {license}, {licence}]
     2: the act of giving a formal (usually written) authorization
        [syn: {license}, {permission}]
     3: large game fish; found in waters of the West Indies [syn: {Trachinotus
        falcatus}]
     v 1: consent to, give permission; "She permitted her son to visit
          her estranged husband"; "I won't let the police search
          her basement"; "I cannot allow you to see your exam"
          [syn: {allow}, {let}, {countenance}] [ant: {forbid}, {forbid}]
     2: make it possible through a specific action or lack of action
        for something to happen; "This ...
