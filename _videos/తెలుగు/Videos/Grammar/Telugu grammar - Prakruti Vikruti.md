---
version: 1
type: video
provider: YouTube
id: 9zrs2tcGq9c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 409b1f45-e1c5-4330-9bf2-2f9b749ee932
updated: 1486069629
title: 'Telugu grammar - Prakruti Vikruti'
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/9zrs2tcGq9c/default.jpg
    - https://i3.ytimg.com/vi/9zrs2tcGq9c/1.jpg
    - https://i3.ytimg.com/vi/9zrs2tcGq9c/2.jpg
    - https://i3.ytimg.com/vi/9zrs2tcGq9c/3.jpg
---
