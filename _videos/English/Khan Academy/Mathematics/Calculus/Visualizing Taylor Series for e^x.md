---
version: 1
type: video
provider: YouTube
id: AFMXixBVP-0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Visualizing%20Taylor%20Series%20for%20e%5Ex.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 13c55cc7-3c82-4ed3-9c4a-f94a80e3bc01
updated: 1486069693
title: Visualizing Taylor Series for e^x
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/AFMXixBVP-0/default.jpg
    - https://i3.ytimg.com/vi/AFMXixBVP-0/1.jpg
    - https://i3.ytimg.com/vi/AFMXixBVP-0/2.jpg
    - https://i3.ytimg.com/vi/AFMXixBVP-0/3.jpg
---
