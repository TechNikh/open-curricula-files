---
version: 1
type: video
provider: YouTube
id: b-HZ1SZPaQw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: be39e029-02b0-4471-bacc-3be31e43c429
updated: 1486069682
title: 'Torque- Crash Course Physics #12'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/b-HZ1SZPaQw/default.jpg
    - https://i3.ytimg.com/vi/b-HZ1SZPaQw/1.jpg
    - https://i3.ytimg.com/vi/b-HZ1SZPaQw/2.jpg
    - https://i3.ytimg.com/vi/b-HZ1SZPaQw/3.jpg
---
