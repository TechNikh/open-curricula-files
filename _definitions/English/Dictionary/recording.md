---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/recording
offline_file: ""
offline_thumbnail: ""
uuid: 385ef85a-61b4-4d31-8f7b-95741404d540
updated: 1484310469
title: recording
categories:
    - Dictionary
---
recording
     n 1: signal encoding something (e.g., picture or sound) that has
          been recorded
     2: the act of making a record (especially an audio record);
        "she watched the recording from a sound-proof booth" [syn:
         {transcription}]
     3: a storage device on which information (sounds or images)
        have been recorded
