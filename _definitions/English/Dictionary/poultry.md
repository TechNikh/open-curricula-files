---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/poultry
offline_file: ""
offline_thumbnail: ""
uuid: 31323fa2-d3c1-4369-b2c6-77908888ab27
updated: 1484310535
title: poultry
categories:
    - Dictionary
---
poultry
     n 1: a domesticated gallinaceous bird though to be descended from
          the red jungle fowl [syn: {domestic fowl}, {fowl}]
     2: flesh of chickens or turkeys or ducks or geese raised for
        food
