---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sworn
offline_file: ""
offline_thumbnail: ""
uuid: e6e731e0-a02e-4049-8e8b-93ccd9ff7953
updated: 1484310192
title: sworn
categories:
    - Dictionary
---
swear
     v 1: utter obscenities or profanities; "The drunken men were
          cursing loudly in the street" [syn: {curse}, {cuss}, {blaspheme},
           {imprecate}]
     2: to declare or affirm solemnly and formally as true; "Before
        God I swear I am innocent" [syn: {affirm}, {verify}, {assert},
         {avow}, {aver}, {swan}]
     3: promise solemnly; take an oath
     4: make a deposition; declare under oath [syn: {depose}, {depone}]
     5: have confidence or faith in; "We can trust in God"; "Rely on
        your friends"; "bank on your good education"; "I swear by
        my grandmother's recipes" [syn: {trust}, {rely}, {bank}]
        [ant: {distrust}, {distrust}]
     ...
