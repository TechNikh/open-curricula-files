---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/placed
offline_file: ""
offline_thumbnail: ""
uuid: f4a8805c-7acf-4ced-aa43-03e9df5f4727
updated: 1484310343
title: placed
categories:
    - Dictionary
---
placed
     adj 1: situated in a particular spot or position; "valuable
            centrally located urban land"; "strategically placed
            artillery"; "a house set on a hilltop"; "nicely
            situated on a quiet riverbank" [syn: {located}, {set},
             {situated}]
     2: put in position in relation to other things; "end tables
        placed conveniently"
