---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/embrace
offline_file: ""
offline_thumbnail: ""
uuid: 52b3d7df-0285-4930-912c-6c7b810bf1ba
updated: 1484310545
title: embrace
categories:
    - Dictionary
---
embrace
     n 1: the act of clasping another person in the arms (as in
          greeting or affection) [syn: {embracing}]
     2: the state of taking in or encircling; "an island in the
        embrace of the sea"
     3: a close affectionate and protective acceptance; "his willing
        embrace of new ideas"; "in the bosom of the family" [syn:
        {bosom}]
     v 1: include in scope; include as part of something broader; have
          as one's sphere or territory; "This group encompasses a
          wide range of people from different backgrounds"; "this
          should cover everyone in the group" [syn: {encompass}, {comprehend},
           {cover}]
     2: hug, usually with ...
