---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Screening_game
offline_file: ""
offline_thumbnail: ""
uuid: 8479c24a-0129-4de1-abea-bbf8be1f6f27
updated: 1484309038
title: Screening game
categories:
    - Game theory game classes
---
A screening game is a two-player principal–agent type game used in economic and game theoretical modeling. Principal–agent problems are situations where there are two players whose interests are not necessarily at ends, but where complete honesty is not optimal for one player. This will lead to strategies where the players exchange information based in their actions which is to some degree noisy. This ambiguity prevents the other player from taking advantage of the first. The game is closely related to signaling games, but there is a difference in how information is exchanged.
