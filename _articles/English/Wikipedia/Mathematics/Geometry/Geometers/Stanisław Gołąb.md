---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Stanis%C5%82aw_Go%C5%82%C4%85b'
offline_file: ""
offline_thumbnail: ""
uuid: 19cb784e-9566-4db3-83c3-35bb7236a6f8
updated: 1484309080
title: Stanisław Gołąb
categories:
    - Geometers
---
In 1932, he proved that the perimeter of the unit disc respect to a given metric can take any value in between 6 and 8, and that these extremal values are obtained if and only if the unit disc is an affine regular hexagon resp. a parallelogram.[1]
