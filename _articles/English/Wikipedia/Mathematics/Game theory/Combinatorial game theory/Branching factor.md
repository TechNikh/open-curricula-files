---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Branching_factor
offline_file: ""
offline_thumbnail: ""
uuid: 2851f4cd-6cd7-477f-aaff-a315c09f6d90
updated: 1484309020
title: Branching factor
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Red-black_tree_example.svg.png
categories:
    - Combinatorial game theory
---
In computing, tree data structures, and game theory, the branching factor is the number of children at each node, the outdegree. If this value is not uniform, an average branching factor can be calculated.
