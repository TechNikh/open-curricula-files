---
version: 1
type: video
provider: YouTube
id: 52g-Ec4g18c
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 468dde6c-eff0-4032-830c-4e2188082ee8
updated: 1486069623
title: 'I Will Read Your Mind - Math Magic Trick'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/52g-Ec4g18c/default.jpg
    - https://i3.ytimg.com/vi/52g-Ec4g18c/1.jpg
    - https://i3.ytimg.com/vi/52g-Ec4g18c/2.jpg
    - https://i3.ytimg.com/vi/52g-Ec4g18c/3.jpg
---
