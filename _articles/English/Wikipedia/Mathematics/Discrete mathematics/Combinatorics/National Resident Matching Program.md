---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/National_Resident_Matching_Program
offline_file: ""
offline_thumbnail: ""
uuid: f4f1ff2d-b9e0-49f5-8143-a36e570136b0
updated: 1484308582
title: National Resident Matching Program
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-2007ACGME-2.PNG
tags:
    - History
    - Matching algorithm
    - Inputs
    - Simple case
    - Couples
    - Failure to match
    - International medical graduates
    - Controversy and lawsuits
    - Implementations in software packages
categories:
    - Combinatorics
---
The National Resident Matching Program (NRMP), also called the Match,[1] is a United States-based private non-profit non-governmental organization created in 1952 to help match medical school students with residency programs. The NRMP is sponsored by the American Board of Medical Specialties (ABMS), the American Medical Association (AMA), the Association of American Medical Colleges (AAMC), the American Hospital Association (AHA), and the Council of Medical Specialty Societies (CMSS).
