---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shore
offline_file: ""
offline_thumbnail: ""
uuid: 0ff921e0-1745-44d9-8a54-2bcffc931b40
updated: 1484310284
title: shore
categories:
    - Dictionary
---
shore
     n 1: the land along the edge of a body of water
     2: a beam or timber that is propped against a structure to
        provide support [syn: {shoring}]
     v 1: serve as a shore to; "The river was shored by trees"
     2: arrive on shore; "The ship landed in Pearl Harbor" [syn: {land},
         {set ashore}]
     3: support by placing against something solid or rigid; "shore
        and buttress an old building" [syn: {prop up}, {prop}, {shore
        up}]
