---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/convincingly
offline_file: ""
offline_thumbnail: ""
uuid: 03559033-4f35-4788-a3d7-cbb7d6f7c63d
updated: 1484310607
title: convincingly
categories:
    - Dictionary
---
convincingly
     adv : in a convincing manner; "he argued convincingly" [ant: {unconvincingly}]
