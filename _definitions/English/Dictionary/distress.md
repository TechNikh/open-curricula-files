---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/distress
offline_file: ""
offline_thumbnail: ""
uuid: 4557f525-da4f-4959-8677-c8110d4b9d4d
updated: 1484310486
title: distress
categories:
    - Dictionary
---
distress
     n 1: psychological suffering; "the death of his wife caused him
          great distress" [syn: {hurt}, {suffering}]
     2: a state of adversity (danger or affliction or need); "a ship
        in distress"; "she was the classic maiden in distress"
     3: extreme physical pain; "the patient appeared to be in
        distress"
     4: the seizure and holding of property as security for payment
        of a debt or satisfaction of a claim; "Originally distress
        was a landloard's remedy against a tenant for unpaid rents
        or property damage but now the landlord is given a
        landlord's lien" [syn: {distraint}]
     v : cause mental pain to; "The news of her ...
