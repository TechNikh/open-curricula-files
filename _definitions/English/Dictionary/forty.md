---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forty
offline_file: ""
offline_thumbnail: ""
uuid: 172a375a-0e99-4509-8f58-345ab45cf16a
updated: 1484310525
title: forty
categories:
    - Dictionary
---
forty
     adj : being ten more than thirty [syn: {40}, {xl}, {twoscore}]
     n : the cardinal number that is the product of ten and four
         [syn: {40}, {XL}]
