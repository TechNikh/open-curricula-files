---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Juggi
offline_file: ""
offline_thumbnail: ""
uuid: 5d8bd19d-dfd2-48ba-8327-b5fc8f377623
updated: 1484308450
title: Juggi
categories:
    - Uncategorized
---
The Juggis, or Jughis, are members of an ethnic group of itinerant travelers found in north-central Afghanistan. They are looked down on by other local groups, and considered "as blots on the ethnic landscape." Many Juggis can be found living in makeshift camps and tents in the outskirts of Mazar-i-Sharif. Most Juggis claim to descend from travelers from Uzbekistan.[1]
