---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aswan
offline_file: ""
offline_thumbnail: ""
uuid: 51a5b1f9-c758-4dbf-ae86-f5aaaa26cf3b
updated: 1484310178
title: aswan
categories:
    - Dictionary
---
Aswan
     n : an ancient city on the Nile in Egypt; site of the Aswan High
         Dam [syn: {Assuan}, {Assouan}]
