---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/british
offline_file: ""
offline_thumbnail: ""
uuid: 08063740-e454-43fd-b4ee-f50a50869731
updated: 1484310273
title: british
categories:
    - Dictionary
---
British
     adj : of or relating to or characteristic of Great Britain or its
           people or culture; "his wife is British"
     n : the people of Great Britain [syn: {British people}, {the
         British}, {Brits}]
