---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/operating
offline_file: ""
offline_thumbnail: ""
uuid: acc57d9b-fc7a-48ef-9401-ad5cb394eeee
updated: 1484310144
title: operating
categories:
    - Dictionary
---
operating
     adj 1: involved in a kind of operation; "the operating conditions
            of the oxidation pond"
     2: being in effect or operation; "de facto apartheid is still
        operational even in the `new' African nations"- Leslie
        Marmon Silko; "bus service is in operation during the
        emergency"; "the company had several operating divisions"
        [syn: {operational}, {in operation(p)}, {operating(a)}]
