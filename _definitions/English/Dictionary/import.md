---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/import
offline_file: ""
offline_thumbnail: ""
uuid: d5dbd65b-0146-47b1-b150-d688194a92aa
updated: 1484310529
title: import
categories:
    - Dictionary
---
import
     n 1: commodities (goods or services) bought from a foreign
          country [syn: {importation}] [ant: {export}]
     2: an imported person brought from a foreign country; "the lead
        role was played by an import from Sweden"; "they are
        descendants of indentured importees" [syn: {importee}]
     3: the message that is intended or expressed or signified;
        "what is the meaning of this sentence"; "the significance
        of a red traffic light"; "the signification of Chinese
        characters"; "the import of his announcement was
        ambigtuous" [syn: {meaning}, {significance}, {signification}]
     4: a meaning that is not expressly stated but can be ...
