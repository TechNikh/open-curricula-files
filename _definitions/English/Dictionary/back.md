---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/back
offline_file: ""
offline_thumbnail: ""
uuid: 91f045e5-b0ee-4b61-b6e8-d46c1ec2544b
updated: 1484310327
title: back
categories:
    - Dictionary
---
back
     adj 1: related to or located at the back; "the back yard"; "the
            back entrance" [syn: {back(a)}] [ant: {front(a)}]
     2: located at or near the back of an animal; "back (or hind)
        legs"; "the hinder part of a carcass" [syn: {back(a)}, {hind(a)},
         {hinder(a)}]
     3: of an earlier date; "back issues of the magazine" [syn: {back(a)}]
     n 1: the posterior part of a human (or animal) body from the neck
          to the end of the spine; "his back was nicely tanned"
          [syn: {dorsum}]
     2: the side that goes last or is not normally seen; "he wrote
        the date on the back of the photograph" [syn: {rear}]
        [ant: {front}]
     3: the ...
