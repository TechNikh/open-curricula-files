---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dilly-dally
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484493781
title: dilly-dally
categories:
    - Dictionary
---
dilly-dally
     v : postpone doing what one should be doing; "He did not want to
         write the letter and procrastinated for days" [syn: {procrastinate},
          {stall}, {drag one's feet}, {drag one's heels}, {shillyshally},
          {dillydally}]
     [also: {dilly-dallied}]
