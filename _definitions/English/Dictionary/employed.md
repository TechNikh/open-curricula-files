---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/employed
offline_file: ""
offline_thumbnail: ""
uuid: dfd5a1a9-5b07-4ebe-8a09-7a14af2d9179
updated: 1484310448
title: employed
categories:
    - Dictionary
---
employed
     adj 1: having your services engaged for; or having a job especially
            one that pays wages or a salary; "most of our
            graduates are employed" [ant: {unemployed}]
     2: put to use [syn: {made use of(p)}]
