---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/coiled
offline_file: ""
offline_thumbnail: ""
uuid: 6c4c49d2-e422-498e-add1-a7558e297587
updated: 1484310323
title: coiled
categories:
    - Dictionary
---
coiled
     adj : curled or wound (especially in concentric rings or spirals);
           "a coiled snake ready to strike"; "the rope lay coiled
           on the deck" [ant: {uncoiled}]
