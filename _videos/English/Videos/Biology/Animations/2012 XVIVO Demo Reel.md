---
version: 1
type: video
provider: YouTube
id: TZuxuVJA36Q
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2c1774e3-17ce-47e2-9899-3ed9cf6c4bb1
updated: 1486069666
title: 2012 XVIVO Demo Reel
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/TZuxuVJA36Q/default.jpg
    - https://i3.ytimg.com/vi/TZuxuVJA36Q/1.jpg
    - https://i3.ytimg.com/vi/TZuxuVJA36Q/2.jpg
    - https://i3.ytimg.com/vi/TZuxuVJA36Q/3.jpg
---
