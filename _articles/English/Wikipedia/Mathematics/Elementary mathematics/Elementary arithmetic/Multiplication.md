---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Multiplication
offline_file: ""
offline_thumbnail: ""
uuid: ae74310b-39d4-4dae-af48-8b044e5b174e
updated: 1484308921
title: Multiplication
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Multiply_4_bags_3_marbles.svg.png
tags:
    - Notation and terminology
    - Computation
    - Historical algorithms
    - Egyptians
    - Babylonians
    - Chinese
    - Modern methods
    - Grid Method
    - Computer algorithms
    - Products of measurements
    - Products of sequences
    - Capital Pi notation
    - Infinite products
    - Properties
    - Axioms
    - Multiplication with set theory
    - Multiplication in group theory
    - Multiplication of different kinds of numbers
    - Exponentiation
    - Notes
categories:
    - Elementary arithmetic
---
Multiplication (often denoted by the cross symbol "×", by a point "·", by juxtaposition, or, on computers, by an asterisk "∗") is one of the four elementary, mathematical operations of arithmetic; with the others being addition, subtraction and division.
