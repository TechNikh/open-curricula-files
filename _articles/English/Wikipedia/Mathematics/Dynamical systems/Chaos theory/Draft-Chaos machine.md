---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Draft:Chaos_machine
offline_file: ""
offline_thumbnail: ""
uuid: 4fd93a5f-be77-4e82-b220-68356880abb3
updated: 1484308702
title: Draft:Chaos machine
categories:
    - Chaos theory
---
In mathematics, a chaos machine is a class of algorithms constructed on the base of chaos theory (mainly deterministic chaos) to produce pseudo-random oracle. It presents the idea to create a universal scheme with modular design and customizable parameters, that can be applied where randomness and sensitiveness is needed.
