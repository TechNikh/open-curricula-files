---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nevertheless
offline_file: ""
offline_thumbnail: ""
uuid: 67514936-dcbf-44e2-bf32-a2f7c4d683cb
updated: 1484310183
title: nevertheless
categories:
    - Dictionary
---
nevertheless
     adv : despite anything to the contrary (usually following a
           concession); "although I'm a little afraid, however I'd
           like to try it"; "while we disliked each other,
           nevertheless we agreed"; "he was a stern yet fair
           master"; "granted that it is dangerous, all the same I
           still want to go" [syn: {however}, {withal}, {still}, {yet},
            {all the same}, {even so}, {nonetheless}, {notwithstanding}]
