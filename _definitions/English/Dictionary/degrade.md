---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/degrade
offline_file: ""
offline_thumbnail: ""
uuid: a5cb908f-a5bf-45d7-b4c5-130df5af09ea
updated: 1484310480
title: degrade
categories:
    - Dictionary
---
degrade
     v 1: reduce the level of land, as by erosion [ant: {aggrade}]
     2: reduce in worth or character, usually verbally; "She tends
        to put down younger women colleagues"; "His critics took
        him down after the lecture" [syn: {take down}, {disgrace},
         {demean}, {put down}]
     3: lower the grade of something; reduce its worth [syn: {cheapen}]
