---
version: 1
type: video
provider: YouTube
id: scCfeJhJ2TA
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ed320e6b-8b00-420a-8ece-c0d9923eadea
updated: 1486069623
title: Multiply Numbers By Lines Rotation Trick
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/scCfeJhJ2TA/default.jpg
    - https://i3.ytimg.com/vi/scCfeJhJ2TA/1.jpg
    - https://i3.ytimg.com/vi/scCfeJhJ2TA/2.jpg
    - https://i3.ytimg.com/vi/scCfeJhJ2TA/3.jpg
---
