---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/detention
offline_file: ""
offline_thumbnail: ""
uuid: 97bd5bc9-d8a8-4527-a7c6-cc624daae070
updated: 1484310611
title: detention
categories:
    - Dictionary
---
detention
     n 1: a state of being confined (usually for a short time); "his
          detention was politically motivated"; "the prisoner is
          on hold"; "he is in the custody of police" [syn: {hold},
           {custody}]
     2: a punishment in which a student must stay at school after
        others have gone home; "the detention of tardy pupils"
