---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hypot
offline_file: ""
offline_thumbnail: ""
uuid: 577ef06d-08b0-4ff2-ad18-32b3ad71b1a9
updated: 1484309343
title: Hypot
tags:
    - Motivation and usage
    - Implementation
    - Programming language support
categories:
    - Trigonometry
---
Hypot is a mathematical function defined to calculate the length of the hypotenuse of a right-angle triangle. It was designed to avoid errors arising due to limited-precision calculations performed on computers.
