---
version: 1
type: video
provider: YouTube
id: eSInI1xHvh4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 60f28313-29e2-4cd2-b0c2-fdb3a785b18c
updated: 1486069662
title: >
    Collision Theory and Reactions Part 1 | The Chemistry
    Journey | The Fuse School
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/eSInI1xHvh4/default.jpg
    - https://i3.ytimg.com/vi/eSInI1xHvh4/1.jpg
    - https://i3.ytimg.com/vi/eSInI1xHvh4/2.jpg
    - https://i3.ytimg.com/vi/eSInI1xHvh4/3.jpg
---
