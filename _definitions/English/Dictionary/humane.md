---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/humane
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484520961
title: humane
categories:
    - Dictionary
---
humane
     adj 1: pertaining to or concerned with the humanities; "humanistic
            studies"; "a humane education" [syn: {humanist}, {humanistic}]
     2: marked or motivated by concern with the alleviation of
        suffering [ant: {inhumane}]
     3: showing evidence of moral and intellectual advancement
