---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vaporized
offline_file: ""
offline_thumbnail: ""
uuid: 6fca3659-2073-4a75-b359-97fe9d5827ef
updated: 1484310414
title: vaporized
categories:
    - Dictionary
---
vaporized
     adj : converted into a gas or vapor [syn: {gasified}, {vaporised},
            {volatilized}, {volatilised}]
