---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laser_schlieren_deflectometry
offline_file: ""
offline_thumbnail: ""
uuid: 2b3540fa-ce7a-4f02-a78f-ed1ab026d1cf
updated: 1484308846
title: Laser schlieren deflectometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/356px-Fig-LSD.jpeg
categories:
    - Thermodynamics
---
Laser schlieren deflectometry (LSD) is a method for a high-speed measurement of the gas temperature in microscopic dimensions, in particular for temperature peaks under dynamic conditions at atmospheric pressure. The principle of LSD is derived from schlieren photography: a narrow laser beam is used to scan an area in a gas where changes in properties are associated with characteristic changes of refractive index. Laser schlieren deflectometry is claimed to overcome limitations of other methods regarding temporal and spatial resolution.[1]
