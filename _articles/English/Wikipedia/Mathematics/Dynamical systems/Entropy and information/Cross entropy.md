---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cross_entropy
offline_file: ""
offline_thumbnail: ""
uuid: c3e9511e-a2db-4c49-b67c-4ce6c6561934
updated: 1484308767
title: Cross entropy
tags:
    - Motivation
    - Estimation
    - Cross-entropy minimization
    - Cross-entropy error function and logistic regression
categories:
    - Entropy and information
---
In information theory, the cross entropy between two probability distributions 
  
    
      
        p
      
    
    {\displaystyle p}
  
 and 
  
    
      
        q
      
    
    {\displaystyle q}
  
 over the same underlying set of events measures the average number of bits needed to identify an event drawn from the set, if a coding scheme is used that is optimized for an "unnatural" probability distribution 
  
    
      
        q
      
    
    {\displaystyle q}
  
, rather than the "true" distribution 
  
    
      
        p
      
    
    {\displaystyle p}
  
.
