---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/painted
offline_file: ""
offline_thumbnail: ""
uuid: 4c6e6207-2916-4c53-9584-ab1c28775b32
updated: 1484310210
title: painted
categories:
    - Dictionary
---
painted
     adj 1: represented in a painting; "as idle as a painted ship upon a
            painted ocean"
     2: coated with paint; "freshly painted lawn furniture" [ant: {unpainted}]
     3: lacking substance or vitality as if produced by painting;
        "in public he wore a painted smile"
     4: having makeup applied; "brazen painted faces" [ant: {unpainted}]
     5: having sections or patches colored differently and usually
        brightly; "a jester dressed in motley"; "the painted
        desert"; "a particolored dress"; "a piebald horse"; "pied
        daisies" [syn: {motley}, {calico}, {multicolor}, {multicolour},
         {multicolored}, {multicoloured}, {particolored}, ...
