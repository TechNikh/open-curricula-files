---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/brass
offline_file: ""
offline_thumbnail: ""
uuid: 9fa69e84-2234-4079-9cf0-5874e3bd332e
updated: 1484310230
title: brass
categories:
    - Dictionary
---
brass
     n 1: an alloy of copper and zinc
     2: a wind instrument that consists of a brass tube (usually of
        variable length) blown by means of a cup-shaped or
        funnel-shaped mouthpiece
     3: the persons (or committees or departments etc.) who make up
        a body for the purpose of administering something; "he
        claims that the present administration is corrupt"; "the
        governance of an association is responsible to its
        members"; "he quickly became recognized as a member of the
        establishment" [syn: {administration}, {governance}, {governing
        body}, {establishment}, {organization}, {organisation}]
     4: impudent aggressiveness; "I ...
