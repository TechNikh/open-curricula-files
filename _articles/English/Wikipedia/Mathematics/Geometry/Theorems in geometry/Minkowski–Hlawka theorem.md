---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Minkowski%E2%80%93Hlawka_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 9bb3c1e0-ec56-4ece-bdfe-7576ada6a6d8
updated: 1484309117
title: Minkowski–Hlawka theorem
categories:
    - Theorems in geometry
---
In mathematics, the Minkowski–Hlawka theorem is a result on the lattice packing of hyperspheres in dimension n > 1. It states that there is a lattice in Euclidean space of dimension n, such that the corresponding best packing of hyperspheres with centres at the lattice points has density Δ satisfying
