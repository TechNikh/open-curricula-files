---
version: 1
type: video
provider: YouTube
id: _XU-26UY9Xs
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 425371c4-1170-4da4-866c-a2b054fcd98f
updated: 1486069619
title: Order of steps exercise example
categories:
    - Grade 10
thumbnail_urls:
    - https://i3.ytimg.com/vi/_XU-26UY9Xs/default.jpg
    - https://i3.ytimg.com/vi/_XU-26UY9Xs/1.jpg
    - https://i3.ytimg.com/vi/_XU-26UY9Xs/2.jpg
    - https://i3.ytimg.com/vi/_XU-26UY9Xs/3.jpg
---
