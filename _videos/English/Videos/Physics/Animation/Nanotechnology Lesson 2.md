---
version: 1
type: video
provider: YouTube
id: 5way8Jvx29s
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: b9e80374-ac5d-44bc-8263-a13f897f2cae
updated: 1486069653
title: Nanotechnology Lesson 2
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/5way8Jvx29s/default.jpg
    - https://i3.ytimg.com/vi/5way8Jvx29s/1.jpg
    - https://i3.ytimg.com/vi/5way8Jvx29s/2.jpg
    - https://i3.ytimg.com/vi/5way8Jvx29s/3.jpg
---
