---
version: 1
type: video
provider: YouTube
id: 3gUE_P9T-Wk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/First%20living%20things%20on%20land%20clarification.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 0fcceaeb-8950-4c53-81ce-98db1b5e57da
updated: 1486069686
title: First living things on land clarification
categories:
    - 'Cosmology & Astronomy'
thumbnail_urls:
    - https://i3.ytimg.com/vi/3gUE_P9T-Wk/default.jpg
    - https://i3.ytimg.com/vi/3gUE_P9T-Wk/1.jpg
    - https://i3.ytimg.com/vi/3gUE_P9T-Wk/2.jpg
    - https://i3.ytimg.com/vi/3gUE_P9T-Wk/3.jpg
---
