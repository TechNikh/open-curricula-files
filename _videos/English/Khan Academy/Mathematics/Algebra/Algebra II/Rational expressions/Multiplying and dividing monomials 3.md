---
version: 1
type: video
provider: YouTube
id: abuhkDhowyc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20II/Rational%20expressions/Multiplying%20and%20dividing%20monomials%203.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cf4bd465-47f3-44f2-a5d8-53fffa094e16
updated: 1486069565
title: Multiplying and dividing monomials 3
categories:
    - Rational expressions
thumbnail_urls:
    - https://i3.ytimg.com/vi/abuhkDhowyc/default.jpg
    - https://i3.ytimg.com/vi/abuhkDhowyc/1.jpg
    - https://i3.ytimg.com/vi/abuhkDhowyc/2.jpg
    - https://i3.ytimg.com/vi/abuhkDhowyc/3.jpg
---
