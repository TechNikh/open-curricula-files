---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Mathematical_chemistry
offline_file: ""
offline_thumbnail: ""
uuid: 0c96581b-a430-487e-aa18-e1d9f3449fe1
updated: 1484309429
title: Mathematical chemistry
tags:
    - Bibliography
    - Notes
categories:
    - Mathematical chemistry
---
Mathematical chemistry[1] is the area of research engaged in novel applications of mathematics to chemistry; it concerns itself principally with the mathematical modeling of chemical phenomena.[2] Mathematical chemistry has also sometimes been called computer chemistry, but should not be confused with computational chemistry.
