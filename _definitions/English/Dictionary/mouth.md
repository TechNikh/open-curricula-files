---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mouth
offline_file: ""
offline_thumbnail: ""
uuid: 8b466daf-2dd9-4b81-b1df-2bbe5b29d0be
updated: 1484310361
title: mouth
categories:
    - Dictionary
---
mouth
     n 1: the opening through which food is taken in and vocalizations
          emerge; "he stuffed his mouth with candy" [syn: {oral
          cavity}, {oral fissure}, {rima oris}]
     2: the externally visible part of the oral cavity on the face
        and the system of organs surrounding the opening; "she
        wiped lipstick from her mouth"
     3: an opening that resembles a mouth (as of a cave or a gorge);
        "he rode into the mouth of the canyon"; "they built a fire
        at the mouth of the cave"
     4: the point where a stream issues into a larger body of water;
        "New York is at the mouth of the Hudson"
     5: a person conceived as a consumer of food; "he ...
