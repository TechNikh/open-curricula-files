---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Dirac_bracket
offline_file: ""
offline_thumbnail: ""
uuid: f4c59387-60de-43bf-af3d-b2a8e75864ac
updated: 1484308787
title: Dirac bracket
tags:
    - Inadequacy of the standard Hamiltonian procedure
    - Example of a Lagrangian linear in velocity
    - Generalized Hamiltonian procedure
    - Generalizing the Hamiltonian
    - Consistency conditions
    - Determination of the uk
    - The total Hamiltonian
    - The Dirac bracket
    - Illustration on the example provided
    - Further Illustration for a hypersphere
categories:
    - Hamiltonian mechanics
---
The Dirac bracket is a generalization of the Poisson bracket developed by Paul Dirac[1] to treat classical systems with second class constraints in Hamiltonian mechanics, and to thus allow them to undergo canonical quantization. It is an important part of Dirac's development of Hamiltonian mechanics to elegantly handle more general Lagrangians, when constraints and thus more apparent than dynamical variables are at hand.[2] More abstractly, the two-form implied from the Dirac bracket is the restriction of the symplectic form to the constraint surface in phase space.[3]
