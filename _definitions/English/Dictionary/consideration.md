---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consideration
offline_file: ""
offline_thumbnail: ""
uuid: e1eda077-f2da-42f9-8b5b-32eb519b90a4
updated: 1484310299
title: consideration
categories:
    - Dictionary
---
consideration
     n 1: the process of giving careful thought to something
     2: information that should be kept in mind when making a
        decision; "another consideration is the time it would
        take" [syn: {circumstance}, {condition}]
     3: a discussion of a topic (as in a meeting); "consideration of
        the traffic problem took more than an hour"
     4: kind and considerate regard for others; "he showed no
        consideration for her feelings" [syn: {considerateness}, {thoughtfulness}]
        [ant: {inconsideration}, {inconsideration}]
     5: a fee charged in advance to retain the services of someone
        [syn: {retainer}]
     6: a considerate and thoughtful act ...
