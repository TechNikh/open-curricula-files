---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/down
offline_file: ""
offline_thumbnail: ""
uuid: d3d91ed3-b7f4-4a46-8ded-eef3103175c2
updated: 1484310361
title: down
categories:
    - Dictionary
---
down
     adj 1: being or moving lower in position or less in some value;
            "lay face down"; "the moon is down"; "our team is down
            by a run"; "down by a pawn"; "the stock market is down
            today" [ant: {up}]
     2: becoming progressively lower; "the down trend in the real
        estate market" [syn: {down(a)}]
     3: understood perfectly; "had his algebra problems down" [syn:
        {down pat(p)}, {mastered}]
     4: extending or moving from a higher to a lower place; "the
        down staircase"; "the downward course of the stream" [syn:
         {down(a)}, {downward(a)}]
     5: out; "two down in the last of the ninth" [syn: {down(p)}]
     6: lower than ...
