---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nonpolar
offline_file: ""
offline_thumbnail: ""
uuid: 46919f4b-d02a-42d5-acd9-acc4d96a67a0
updated: 1484308386
title: Chemical polarity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Water-elpot-transparent-3D-balls_0.png
tags:
    - Polarity of bonds
    - Classification
    - Polarity of molecules
    - Polar molecules
    - Nonpolar molecules
    - Amphiphilic molecules
    - Predicting molecule polarity
categories:
    - Uncategorized
---
Polar molecules must contain polar bonds due to a difference in electronegativity between the bonded atoms. A polar molecule with two or more polar bonds must have an asymmetric geometry so that the bond dipoles do not cancel each other.
