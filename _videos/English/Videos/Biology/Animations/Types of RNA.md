---
version: 1
type: video
provider: YouTube
id: fC04zNwgY4Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Types%20of%20RNA.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2a944d6b-90d0-4d4c-ada3-b44b70080179
updated: 1486069673
title: Types of RNA
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/fC04zNwgY4Q/default.jpg
    - https://i3.ytimg.com/vi/fC04zNwgY4Q/1.jpg
    - https://i3.ytimg.com/vi/fC04zNwgY4Q/2.jpg
    - https://i3.ytimg.com/vi/fC04zNwgY4Q/3.jpg
---
