---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/negativity
offline_file: ""
offline_thumbnail: ""
uuid: aa9657fd-d7e1-4e19-a612-ff2d40c0462d
updated: 1484310156
title: negativity
categories:
    - Dictionary
---
negativity
     n 1: the disagreeable quality of one who dissents [syn: {negativeness}]
          [ant: {positiveness}]
     2: characterized by habitual skepticism and a tendency to deny
        or oppose or resist suggestions or commands [syn: {negativism}]
        [ant: {positivism}]
     3: (chemistry) the tendency of an atom or radical to attract
        electrons in the formation of an ionic bond [syn: {electronegativity}]
