---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Analytic_geometry
offline_file: ""
offline_thumbnail: ""
uuid: 9e879fca-c095-44d4-be17-f2baf3a8dba1
updated: 1484309159
title: Analytic geometry
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Punktkoordinaten.svg.png
tags:
    - History
    - Ancient Greece
    - Persia
    - Western Europe
    - Coordinates
    - Cartesian coordinates (in a plane or space)
    - Polar coordinates (in a plane)
    - Cylindrical coordinates (in a space)
    - Spherical coordinates (in a space)
    - Equations and curves
    - Lines and planes
    - Conic sections
    - Quadric surfaces
    - Distance and angle
    - Transformations
    - Finding intersections of geometric objects
    - Finding intercepts
    - Tangents and normals
    - Tangent lines and planes
    - Normal line and vector
    - Notes
    - Books
    - Articles
categories:
    - Analytic geometry
---
In classical mathematics, analytic geometry, also known as coordinate geometry, or Cartesian geometry, is the study of geometry using a coordinate system. This contrasts with synthetic geometry.
