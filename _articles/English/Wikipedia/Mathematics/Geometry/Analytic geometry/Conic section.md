---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Conic_section
offline_file: ""
offline_thumbnail: ""
uuid: d9dab0c0-8813-437d-923b-498eca221690
updated: 1484309159
title: Conic section
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Conic_sections_with_plane.svg.png
tags:
    - Euclidean geometry
    - Definition
    - Eccentricity, focus and directrix
    - Conic parameters
    - Standard forms in Cartesian coordinates
    - General Cartesian form
    - Matrix notation
    - Discriminant
    - Invariants
    - Eccentricity in terms of coefficients
    - Conversion to canonical form
    - Polar coordinates
    - Properties
    - History
    - Menaechmus and early works
    - Apollonius of Perga
    - Al-Kuhi
    - Omar Khayyám
    - Europe
    - Applications
    - In the real projective plane
    - Intersection at infinity
    - Homogeneous coordinates
    - Projective definition of a circle
    - "Steiner's projective conic definition"
    - Line conics
    - "Von Staudt's definition"
    - Constructions
    - In the complex projective plane
    - Degenerate cases
    - Pencil of conics
    - Intersecting two conics
    - Generalizations
    - In other areas of mathematics
    - Notes
categories:
    - Analytic geometry
---
In mathematics, a conic section (or simply conic) is a curve obtained as the intersection of the surface of a cone with a plane. The three types of conic section are the hyperbola, the parabola, and the ellipse. The circle is a special case of the ellipse, and is of sufficient interest in its own right that it was sometimes called a fourth type of conic section. The conic sections have been studied by the ancient Greek mathematicians with this work culminating around 200 BC, when Apollonius of Perga undertook a systematic study of their properties.
