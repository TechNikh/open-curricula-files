---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/suitability
offline_file: ""
offline_thumbnail: ""
uuid: 9bfe5e81-f2c5-4088-b3e8-35a125885b41
updated: 1484310549
title: suitability
categories:
    - Dictionary
---
suitability
     n : the quality of having the properties that are right for a
         specific purpose; "an important requirement is
         suitability for long trips" [syn: {suitableness}] [ant: {unsuitability},
          {unsuitability}]
