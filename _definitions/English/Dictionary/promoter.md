---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/promoter
offline_file: ""
offline_thumbnail: ""
uuid: 2e37fb9f-6ef5-4760-bf48-9e23b07e5d47
updated: 1484310389
title: promoter
categories:
    - Dictionary
---
promoter
     n 1: someone who is an active supporter and advocate [syn: {booster},
           {plugger}]
     2: a sponsor who books and stages public entertainments [syn: {showman},
         {impresario}]
