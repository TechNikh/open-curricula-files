---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hockey-stick_identity
offline_file: ""
offline_thumbnail: ""
uuid: d5746b3b-52d8-4386-8148-18edd383cb0b
updated: 1484308572
title: Hockey-stick identity
tags:
    - Proofs
    - Inductive proof
    - Algebraic proof
    - A combinatorial proof
    - Another combinatorial proof
categories:
    - Combinatorics
---
is known as the hockey-stick or Christmas stocking identity. That name stems from the graphical representation of the identity on Pascal's triangle: when the addends represented in the summation and the sum itself are highlighted, the shape revealed is vaguely reminiscent of those objects.
