---
version: 1
type: article
id: https://en.wikipedia.org/wiki/PA_degree
offline_file: ""
offline_thumbnail: ""
uuid: da2bf6ae-7fa5-4237-a5ec-c3cf0836f84a
updated: 1484309489
title: PA degree
tags:
    - Background
    - Completions of Peano arithmetic
    - Properties
categories:
    - Computability theory
---
In recursion theory, a mathematical discipline, a PA degree is a Turing degree that computes a complete extension of Peano arithmetic (Jockusch 1987). These degrees are closely related to fixed-point-free (DNR) functions, and have been thoroughly investigated in recursion theory.
