---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/tween
offline_file: ""
offline_thumbnail: ""
uuid: c9a63e0d-0bb9-4704-8a5a-cc3b44de31b4
updated: 1484310174
title: tween
categories:
    - Dictionary
---
'tween
     adv : in between; "two houses with a tree between" [syn: {between}]
