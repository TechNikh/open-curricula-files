---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/disheartenment
offline_file: ""
offline_thumbnail: ""
uuid: 248c68cb-0cfb-4aa1-93cf-42b5c141c90a
updated: 1484310609
title: disheartenment
categories:
    - Dictionary
---
disheartenment
     n 1: the feeling of despair in the face of obstacles [syn: {discouragement},
           {dismay}]
     2: a communication that leaves you disheartened or daunted
