---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cerebral_hemisphere
offline_file: ""
offline_thumbnail: ""
uuid: 146640cd-f456-445e-98a5-2a99c68e013c
updated: 1484308371
title: Cerebral hemisphere
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Blausen_0215_CerebralHemispheres.png
tags:
    - Structure
    - Composition
    - Development
    - Function
    - Hemisphere lateralization
    - Clinical significance
categories:
    - Uncategorized
---
The vertebrate cerebrum (brain) is formed by two cerebral hemispheres that are separated by a groove, the medial longitudinal fissure. The brain can thus be described as being divided into left and right cerebral hemispheres. Each of these hemispheres has an outer layer of grey matter, the cerebral cortex, that is supported by an inner layer of white matter. In eutherian (placental) mammals, the hemispheres are linked by the corpus callosum, a very large bundle of nerve fibers. Smaller commissures, including the anterior commissure, the posterior commissure and the fornix, also join the hemispheres and these are also present in other vertebrates. These commissures transfer information ...
