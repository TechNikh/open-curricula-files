---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/reflection
offline_file: ""
offline_thumbnail: ""
uuid: 394450b4-ce71-4695-92db-31719eb38b39
updated: 1484310221
title: reflection
categories:
    - Dictionary
---
reflection
     n 1: a calm lengthy intent consideration [syn: {contemplation}, {reflexion},
           {rumination}, {musing}, {thoughtfulness}]
     2: the phenomenon of a propagating wave (light or sound) being
        thrown back from a surface [syn: {reflexion}]
     3: expression without words; "tears are an expression of
        grief"; "the pulse is a reflection of the heart's
        condition" [syn: {expression}, {manifestation}, {reflexion}]
     4: the image of something as reflected by a mirror (or other
        reflective material); "he studied his reflection in the
        mirror" [syn: {reflexion}]
     5: a likeness in which left and right are reversed [syn: {mirror
        ...
