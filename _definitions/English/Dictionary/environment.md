---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/environment
offline_file: ""
offline_thumbnail: ""
uuid: 165b37d6-cbec-4c1e-88d8-0ca291498bce
updated: 1484310293
title: environment
categories:
    - Dictionary
---
environment
     n 1: the totality of surrounding conditions; "he longed for the
          comfortable environment of his livingroom"
     2: the area in which something exists or lives; "the
        country--the flat agricultural surround" [syn: {environs},
         {surroundings}, {surround}]
