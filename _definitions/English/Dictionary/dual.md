---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dual
offline_file: ""
offline_thumbnail: ""
uuid: ad92c638-5bdd-4ddf-bd67-c858046727c1
updated: 1484310597
title: dual
categories:
    - Dictionary
---
dual
     adj 1: consisting of or involving two parts or components usually
            in pairs; "an egg with a double yolk"; "a double
            (binary) star"; "double doors"; "dual controls for
            pilot and copilot"; "duple (or double) time consists
            of two (or a multiple of two) beats to a measure"
            [syn: {double}, {duple}]
     2: having more than one decidedly dissimilar aspects or
        qualities; "a double (or dual) role for an actor"; "the
        office of a clergyman is twofold; public preaching and
        private influence"- R.W.Emerson; "every episode has its
        double and treble meaning"-Frederick Harrison [syn: {double},
         ...
