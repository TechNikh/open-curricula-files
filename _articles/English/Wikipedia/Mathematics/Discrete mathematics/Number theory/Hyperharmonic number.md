---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hyperharmonic_number
offline_file: ""
offline_thumbnail: ""
uuid: f4b9705d-291e-466f-a8cd-88684d8f3e72
updated: 1484308645
title: Hyperharmonic number
tags:
    - Identities involving hyperharmonic numbers
    - Asymptotics
    - Generating function and infinite series
    - An open conjecture
    - Notes
categories:
    - Number theory
---
In mathematics, the n-th hyperharmonic number of order r, denoted by 
  
    
      
        
          H
          
            n
          
          
            (
            r
            )
          
        
      
    
    {\displaystyle H_{n}^{(r)}}
  
, is recursively defined by the relations:
