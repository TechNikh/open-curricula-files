---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Georg_Feigl
offline_file: ""
offline_thumbnail: ""
uuid: 91250c75-6309-4cca-a116-d52f175c2b5e
updated: 1484309078
title: Georg Feigl
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Wechselburg-Barockschloss.jpg
categories:
    - Geometers
---
Georg Feigl started studying mathematics and physics at the University of Jena in 1909. In 1918, he obtained his doctorate under Paul Koebe. From 1928 he was editor of the Jahrbuch über die Fortschritte der Mathematik ("Yearbook on the progress of mathematics"). In 1935 he became a full professor at the University of Breslau. In 1937—1941, he was an editor of the journal Deutsche Mathematik.
