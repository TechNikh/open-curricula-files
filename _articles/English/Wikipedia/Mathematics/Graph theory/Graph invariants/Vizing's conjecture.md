---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Vizing%27s_conjecture'
offline_file: ""
offline_thumbnail: ""
uuid: 1ed1ce35-6a0d-423e-8560-feeb638afe00
updated: 1484309426
title: "Vizing's conjecture"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/240px-Star_product_domination.svg.png
tags:
    - Examples
    - Partial results
    - Upper bounds
    - Notes
categories:
    - Graph invariants
---
In graph theory, Vizing's conjecture concerns a relation between the domination number and the cartesian product of graphs. This conjecture was first stated by Vadim G. Vizing (1968), and states that, if γ(G) denotes the minimum number of vertices in a dominating set for G, then
