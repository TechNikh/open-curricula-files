---
version: 1
type: video
provider: YouTube
id: drper23pGk8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 2cf27a58-c318-448d-a1e6-6b96062d8e2e
updated: 1486069649
title: "Fibonacci's Temple - Mandelbulb 3D Animation"
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/drper23pGk8/default.jpg
    - https://i3.ytimg.com/vi/drper23pGk8/1.jpg
    - https://i3.ytimg.com/vi/drper23pGk8/2.jpg
    - https://i3.ytimg.com/vi/drper23pGk8/3.jpg
---
