---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/perforated
offline_file: ""
offline_thumbnail: ""
uuid: 33da0cf8-fae9-46fb-be96-ebe4993a7d12
updated: 1484310316
title: perforated
categories:
    - Dictionary
---
perforated
     adj 1: having a hole cut through; "pierced ears"; "a perforated
            eardrum"; "a punctured balloon" [syn: {pierced}, {perforate},
             {punctured}]
     2: having a number or series of holes; "a perforated steel
        plate"; "perforated cancellation"; "perforated stamp"
