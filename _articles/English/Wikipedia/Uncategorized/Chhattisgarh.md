---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chattisgarh
offline_file: ""
offline_thumbnail: ""
uuid: 85d629c5-2cc9-49e8-9623-94ed2ecccdbb
updated: 1484308464
title: Chhattisgarh
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Shorea_robusta_in_Chhattisgarh_0.jpg
tags:
    - Administration
    - Districts
    - Etymology
    - geography
    - Climate
    - Temperature
    - Transport
    - Roads
    - Rail network
    - 'Rail Network Expansion [14]'
categories:
    - Uncategorized
---
Chhattisgarh (Chatīsgaṛh, literally 'Thirty-Six Forts') is a state in central India. It is the 10th largest state in India, with an area of 135,194 km2 (52,199 sq mi). With a population of 28 million, Chhattisgarh is the 17th most-populated state of the nation. It is a source of electricity and steel for India, accounting for 15% of the total steel produced in the country.[1] Chhattisgarh is one of the fastest-developing states in India.[2]
