---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gift
offline_file: ""
offline_thumbnail: ""
uuid: bab38ce3-2604-4dde-a1e7-fed645a0f745
updated: 1484310561
title: gift
categories:
    - Dictionary
---
gift
     n 1: something acquired without compensation
     2: natural qualities or talents [syn: {endowment}, {talent}, {natural
        endowment}]
     3: the act of giving [syn: {giving}]
     v 1: give qualities or abilities to [syn: {endow}, {indue}, {empower},
           {invest}, {endue}]
     2: give as a present; make a gift of; "What will you give her
        for her birthday?" [syn: {give}, {present}]
