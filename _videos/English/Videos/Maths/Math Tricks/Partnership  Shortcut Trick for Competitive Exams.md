---
version: 1
type: video
provider: YouTube
id: 6UdGIyyYd2U
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: dfee52a1-37d4-47d9-8567-92d5537a63ff
updated: 1486069622
title: 'Partnership  Shortcut Trick for Competitive Exams'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/6UdGIyyYd2U/default.jpg
    - https://i3.ytimg.com/vi/6UdGIyyYd2U/1.jpg
    - https://i3.ytimg.com/vi/6UdGIyyYd2U/2.jpg
    - https://i3.ytimg.com/vi/6UdGIyyYd2U/3.jpg
---
