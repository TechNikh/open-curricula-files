---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/J%C3%A1nos_Bolyai'
offline_file: ""
offline_thumbnail: ""
uuid: 24194f06-884d-483b-bf64-dbeb077025bc
updated: 1484309071
title: János Bolyai
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Bolyai-arck%25C3%25A9p.JPG'
tags:
    - Early life
    - Career
    - Personal life
    - Legacy
    - Works
    - Sources
categories:
    - Geometers
---
János Bolyai (Hungarian: [ˈjaːnoʃ ˈboːjɒi]; 15 December 1802 – 27 January 1860) or Johann Bolyai,[2] was a Hungarian mathematician, one of the founders of non-Euclidean geometry — a geometry that differs from Euclidean geometry in its definition of parallel lines. The discovery of a consistent alternative geometry that might correspond to the structure of the universe helped to free mathematicians to study abstract concepts irrespective of any possible connection with the physical world.[3]
