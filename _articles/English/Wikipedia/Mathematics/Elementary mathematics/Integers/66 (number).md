---
version: 1
type: article
id: https://en.wikipedia.org/wiki/66_(number)
offline_file: ""
offline_thumbnail: ""
uuid: 0fa98dfe-ea1a-4aef-a20d-fc40420ffb72
updated: 1484308967
title: 66 (number)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Route66_sign.jpg
tags:
    - In mathematics
    - In science
    - Astronomy
    - Physics
    - In computing
    - In motor vehicle transportation
    - In religion
    - In sports
    - In entertainment
    - Cinema
    - Television
    - Video games
    - In other fields
categories:
    - Integers
---
Usages of this number include:
