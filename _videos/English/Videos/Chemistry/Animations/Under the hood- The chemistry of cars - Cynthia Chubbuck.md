---
version: 1
type: video
provider: YouTube
id: e9vfMrjitXw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Under%20the%20hood-%20The%20chemistry%20of%20cars%20-%20Cynthia%20Chubbuck.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 2db1f290-96a5-4226-9790-84a136e0af1b
updated: 1486069664
title: 'Under the hood- The chemistry of cars - Cynthia Chubbuck'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/e9vfMrjitXw/default.jpg
    - https://i3.ytimg.com/vi/e9vfMrjitXw/1.jpg
    - https://i3.ytimg.com/vi/e9vfMrjitXw/2.jpg
    - https://i3.ytimg.com/vi/e9vfMrjitXw/3.jpg
---
