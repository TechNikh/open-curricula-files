---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Daniel_P._Sanders
offline_file: ""
offline_thumbnail: ""
uuid: 7f6faef8-3135-4096-b9d2-346d750da183
updated: 1484309476
title: Daniel P. Sanders
categories:
    - Graph theorists
---
Daniel P. Sanders is an American mathematician. He known for his 1996 efficient proof (algorithm) of proving the Four color theorem (with Neil Robertson, Paul Seymour, and Robin Thomas). He used to be a guest professor of the department of computer science at Columbia University.
