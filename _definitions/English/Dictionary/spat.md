---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/spat
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484462221
title: spat
categories:
    - Dictionary
---
spat
     n 1: a quarrel about petty points [syn: {bicker}, {bickering}, {tiff},
           {squabble}, {pettifoggery}, {fuss}]
     2: a cloth covering (a legging) that provides covering for the
        instep and ankles [syn: {spats}, {gaiter}]
     3: a young oyster or other bivalve
     v 1: come down like raindrops; "Bullets were spatting down on us"
     2: become permanently attached; "mollusks or oysters spat"
     3: strike with a sound like that of falling rain; "Bullets were
        spatting the leaves"
     4: clap one's hands or shout after performances to indicate
        approval [syn: {applaud}, {clap}, {acclaim}] [ant: {boo}]
     5: engage in a brief and petty quarrel
     ...
