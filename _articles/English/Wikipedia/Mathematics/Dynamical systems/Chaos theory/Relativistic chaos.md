---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Relativistic_chaos
offline_file: ""
offline_thumbnail: ""
uuid: 40d6d1e1-0f9a-4349-b720-cbee1ce314fc
updated: 1484308709
title: Relativistic chaos
categories:
    - Chaos theory
---
In physics, relativistic chaos is the application of chaos theory to dynamical systems described primarily by general relativity, and also special relativity.
