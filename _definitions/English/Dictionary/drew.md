---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/drew
offline_file: ""
offline_thumbnail: ""
uuid: cab48421-4cdf-4cc5-b242-a249aad033ec
updated: 1484310477
title: drew
categories:
    - Dictionary
---
draw
     n 1: a gully that is shallower than a ravine
     2: an entertainer who attracts large audiences; "he was the
        biggest drawing card they had" [syn: {drawing card}, {attraction},
         {attractor}, {attracter}]
     3: the finish of a contest in which the score is tied and the
        winner is undecided; "the game ended in a draw"; "their
        record was 3 wins, 6 losses and a tie" [syn: {standoff}, {tie}]
     4: anything (straws or pebbles etc.) taken or chosen at random;
        "the luck of the draw"; "they drew lots for it" [syn: {lot}]
     5: a playing card or cards dealt or taken from the pack; "he
        got a pair of kings in the draw"
     6: a golf shot ...
