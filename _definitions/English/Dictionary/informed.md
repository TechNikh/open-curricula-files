---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/informed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484463001
title: informed
categories:
    - Dictionary
---
informed
     adj : having much knowledge or education; "an informed public";
           "informed opinion"; "the informed customer" [ant: {uninformed}]
