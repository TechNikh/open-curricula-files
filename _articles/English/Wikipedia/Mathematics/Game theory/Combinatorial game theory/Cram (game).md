---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cram_(game)
offline_file: ""
offline_thumbnail: ""
uuid: 0198984f-f12e-46a3-8a4c-27a3bf9f3347
updated: 1484308535
title: Cram (game)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Cram_Game_Example_1.svg.png
tags:
    - Rules
    - Symmetry play
    - Normal version
    - Grundy value
    - Known values
    - Misère version
    - Misère Grundy-value
    - Known values
categories:
    - Combinatorial game theory
---
Cram is a mathematical game played on a sheet of graph paper. It is the impartial version of Domineering and the only difference in the rules is that each player may place their dominoes in either orientation, but it results in a very different game. It has been called by many names, including "plugg" by Geoffrey Mott-Smith, and "dots-and-pairs." Cram was popularized by Martin Gardner in Scientific American.[1]
