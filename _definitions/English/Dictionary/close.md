---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/close
offline_file: ""
offline_thumbnail: ""
uuid: c7d38e93-e564-481b-b51b-b2015b625b1a
updated: 1484310353
title: close
categories:
    - Dictionary
---
close
     adj 1: at or within a short distance in space or time or having
            elements near each other; "close to noon"; "how close
            are we to town?"; "a close formation of ships" [ant: {distant}]
     2: close in relevance or relationship; "a close family"; "we
        are all...in close sympathy with..."; "close kin"; "a
        close resemblance" [ant: {distant}]
     3: not far distant in time or space or degree or circumstances;
        "near neighbors"; "in the near future"; "they are near
        equals"; "his nearest approach to success"; "a very near
        thing"; "a near hit by the bomb"; "she was near tears";
        "she was close to tears"; "had a close ...
