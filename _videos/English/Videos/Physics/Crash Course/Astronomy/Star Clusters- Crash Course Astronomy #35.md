---
version: 1
type: video
provider: YouTube
id: an4rgJ3O21A
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: aaea82ac-2c1c-4442-b8a8-921f0a76717b
updated: 1486069684
title: 'Star Clusters- Crash Course Astronomy #35'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/an4rgJ3O21A/default.jpg
    - https://i3.ytimg.com/vi/an4rgJ3O21A/1.jpg
    - https://i3.ytimg.com/vi/an4rgJ3O21A/2.jpg
    - https://i3.ytimg.com/vi/an4rgJ3O21A/3.jpg
---
