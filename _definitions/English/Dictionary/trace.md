---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/trace
offline_file: ""
offline_thumbnail: ""
uuid: c616dd9d-2ec4-428b-b18f-2168533db130
updated: 1484310433
title: trace
categories:
    - Dictionary
---
trace
     n 1: a just detectable amount; "he speaks French with a trace of
          an accent" [syn: {hint}, {suggestion}]
     2: an indication that something has been present; "there wasn't
        a trace of evidence for the claim"; "a tincture of
        condescension" [syn: {vestige}, {tincture}, {shadow}]
     3: a suggestion of some quality; "there was a touch of sarcasm
        in his tone"; "he detected a ghost of a smile on her face"
        [syn: {touch}, {ghost}]
     4: drawing created by tracing [syn: {tracing}]
     5: either of two lines that connect a horse's harness to a
        wagon or other vehicle or to a whiffletree
     6: a visible mark (as a footprint) left by ...
