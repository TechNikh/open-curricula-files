---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Ordered_graph
offline_file: ""
offline_thumbnail: ""
uuid: bab75d13-8f60-4004-a391-d35d671745e3
updated: 1484309406
title: Ordered graph
categories:
    - Extensions and generalizations of graphs
---
In an ordered graph, the parents of a node are the nodes that are joined to it and precede it in the ordering. More precisely, 
  
    
      
        n
      
    
    {\displaystyle n}
  
 is a parent of 
  
    
      
        m
      
    
    {\displaystyle m}
  
 in the ordered graph 
  
    
      
        ⟨
        N
        ,
        E
        ,
        <
        ⟩
      
    
    {\displaystyle \langle N,E,<\rangle }
  
 if 
  
    
      
        (
        n
        ,
        m
        )
        ∈
        E
      
    
    {\displaystyle (n,m)\in E}
  
 and 
  
    
      
        n
        <
        m
      
    
    {\displaystyle n<m}
  
. The width of a node is the ...
