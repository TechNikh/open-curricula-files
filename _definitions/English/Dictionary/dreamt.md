---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dreamt
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484434981
title: dreamt
categories:
    - Dictionary
---
dream
     n 1: a series of mental images and emotions occurring during
          sleep; "I had a dream about you last night" [syn: {dreaming}]
     2: a cherished desire; "his ambition is to own his own
        business" [syn: {ambition}, {aspiration}]
     3: imaginative thoughts indulged in while awake; "he lives in a
        dream that has nothing to do with reality" [syn: {dreaming}]
     4: a fantastic but vain hope (from fantasies induced by the
        opium pipe); "I have this pipe dream about being emperor
        of the universe" [syn: {pipe dream}]
     5: a state of mind characterized by abstraction and release
        from reality; "he went about his work as if in a dream"
    ...
