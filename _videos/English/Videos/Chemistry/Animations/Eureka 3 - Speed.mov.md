---
version: 1
type: video
provider: YouTube
id: LMsyYyrS3Bc
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Eureka%203%20-%20Speed.mov.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 06cde2a1-cf25-42a0-bd4c-74493ac6a6b1
updated: 1486069664
title: 'Eureka 3 - Speed.mov'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/LMsyYyrS3Bc/default.jpg
    - https://i3.ytimg.com/vi/LMsyYyrS3Bc/1.jpg
    - https://i3.ytimg.com/vi/LMsyYyrS3Bc/2.jpg
    - https://i3.ytimg.com/vi/LMsyYyrS3Bc/3.jpg
---
