---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Homo
offline_file: ""
offline_thumbnail: ""
uuid: 872c9406-8210-40c4-8303-1c6f5ea4a822
updated: 1484308342
title: Homo
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Homininae.svg.png
tags:
    - Names and taxonomy
    - Evolution
    - Migration
    - List of species
categories:
    - Uncategorized
---
Homo is the genus that comprises the species Homo sapiens, which includes modern humans, as well as several extinct species classified as ancestral to or closely related to modern humans—as for examples Homo habilis and Homo neanderthalensis. The genus is about 2.8 million years old;[1][2][3][4][5] it first appeared as its earliest species Homo habilis, which emerged from the genus Australopithecus, which itself had previously split from the lineage of Pan, the chimpanzees.[6][7] Taxonomically, Homo is the only genus assigned to the subtribe Hominina which, with the subtribes Australopithecina and Panina, comprise the tribe Hominini (see evolutionary tree below). All species of the genus ...
