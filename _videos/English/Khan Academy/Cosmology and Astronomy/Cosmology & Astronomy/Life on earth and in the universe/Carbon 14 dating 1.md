---
version: 1
type: video
provider: YouTube
id: 8wYvKeSK1IY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Cosmology%20and%20Astronomy/Cosmology%20%26%20Astronomy/Life%20on%20earth%20and%20in%20the%20universe/Carbon%2014%20dating%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: aaf943d7-d176-4920-ac62-ba99af647ad3
updated: 1486069685
title: Carbon 14 dating 1
categories:
    - Life on earth and in the universe
thumbnail_urls:
    - https://i3.ytimg.com/vi/8wYvKeSK1IY/default.jpg
    - https://i3.ytimg.com/vi/8wYvKeSK1IY/1.jpg
    - https://i3.ytimg.com/vi/8wYvKeSK1IY/2.jpg
    - https://i3.ytimg.com/vi/8wYvKeSK1IY/3.jpg
---
