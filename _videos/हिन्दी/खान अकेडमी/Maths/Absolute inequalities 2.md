---
version: 1
type: video
provider: YouTube
id: 6Ryg6Os1CPM
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e00885e8-d672-42fc-aa20-3948c7a09367
updated: 1486069607
title: Absolute inequalities 2
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/6Ryg6Os1CPM/default.jpg
    - https://i3.ytimg.com/vi/6Ryg6Os1CPM/1.jpg
    - https://i3.ytimg.com/vi/6Ryg6Os1CPM/2.jpg
    - https://i3.ytimg.com/vi/6Ryg6Os1CPM/3.jpg
---
