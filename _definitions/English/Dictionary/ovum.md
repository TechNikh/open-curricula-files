---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ovum
offline_file: ""
offline_thumbnail: ""
uuid: 2771d350-0777-4f43-90f5-349f0e421399
updated: 1484310295
title: ovum
categories:
    - Dictionary
---
ovum
     n : the female reproductive cell; the female gamete [syn: {egg
         cell}]
     [also: {ova} (pl)]
