---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Recursively_enumerable_set
offline_file: ""
offline_thumbnail: ""
uuid: 38fe6bf0-a68f-4dae-99a2-42cde63b9e90
updated: 1484309489
title: Recursively enumerable set
tags:
    - Formal definition
    - Equivalent formulations
    - Examples
    - Properties
    - Remarks
categories:
    - Computability theory
---
In computability theory, traditionally called recursion theory, a set S of natural numbers is called recursively enumerable, computably enumerable, semidecidable, provable or Turing-recognizable if:
