---
version: 1
type: video
provider: YouTube
id: lTyfaumcrhQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 46e4bb17-4d6d-4e92-945e-35eccc53a707
updated: 1486069622
title: 'Addition of mix numbers - Shortcut Trick for Competitive Exams'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/lTyfaumcrhQ/default.jpg
    - https://i3.ytimg.com/vi/lTyfaumcrhQ/1.jpg
    - https://i3.ytimg.com/vi/lTyfaumcrhQ/2.jpg
    - https://i3.ytimg.com/vi/lTyfaumcrhQ/3.jpg
---
