---
version: 1
type: video
provider: YouTube
id: vfRgGtK-Gl8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: d54ff3fc-4214-4f4b-a709-67fff2b55a86
updated: 1486069631
title: Shapes of Objects
categories:
    - Class 3
thumbnail_urls:
    - https://i3.ytimg.com/vi/vfRgGtK-Gl8/default.jpg
    - https://i3.ytimg.com/vi/vfRgGtK-Gl8/1.jpg
    - https://i3.ytimg.com/vi/vfRgGtK-Gl8/2.jpg
    - https://i3.ytimg.com/vi/vfRgGtK-Gl8/3.jpg
---
