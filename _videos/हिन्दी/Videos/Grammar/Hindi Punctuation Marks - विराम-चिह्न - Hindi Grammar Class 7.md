---
version: 1
type: video
provider: YouTube
id: zyInhGi6mrw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 33986d43-9b00-4371-91ed-3385a630e5ed
updated: 1486069629
title: >
    Hindi Punctuation Marks / विराम-चिह्न |
    Hindi Grammar Class 7
categories:
    - Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/zyInhGi6mrw/default.jpg
    - https://i3.ytimg.com/vi/zyInhGi6mrw/1.jpg
    - https://i3.ytimg.com/vi/zyInhGi6mrw/2.jpg
    - https://i3.ytimg.com/vi/zyInhGi6mrw/3.jpg
---
