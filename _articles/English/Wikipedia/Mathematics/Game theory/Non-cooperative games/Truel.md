---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Truel
offline_file: ""
offline_thumbnail: ""
uuid: d832187e-6ffc-4456-a6d9-54e28ae806e3
updated: 1484309057
title: Truel
tags:
    - Game theory overview
    - History
    - Truels in popular culture
categories:
    - Non-cooperative games
---
A truel is a neologism for a duel among three opponents, in which players can fire at one another in an attempt to eliminate them while surviving themselves.[1]
