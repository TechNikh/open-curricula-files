---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adopted
offline_file: ""
offline_thumbnail: ""
uuid: 211af2a1-dce2-4a90-9afc-9785e7130d5f
updated: 1484310409
title: adopted
categories:
    - Dictionary
---
adopted
     adj 1: acquired as your own by free choice; "my adopted state"; "an
            adoptive country" [syn: {adoptive}] [ant: {native}]
     2: having been taken into a specific relationship; "an adopted
        child"
