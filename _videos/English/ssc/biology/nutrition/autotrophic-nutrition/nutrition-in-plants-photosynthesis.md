---
title: Nutrition in Plants Photosynthesis
youtube_id: Dm6iX6BOaPM
tags:
    - SSC
    - Nutrition
    - Photosynthesis
    - Autotrophic
categories:
    - autotrophic-nutrition
---
