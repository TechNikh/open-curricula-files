---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/bickering
offline_file: ""
offline_thumbnail: ""
uuid: 5cf67483-4ba3-4a18-ae1f-08142bf917bb
updated: 1484310152
title: bickering
categories:
    - Dictionary
---
bickering
     n : a quarrel about petty points [syn: {bicker}, {spat}, {tiff},
          {squabble}, {pettifoggery}, {fuss}]
