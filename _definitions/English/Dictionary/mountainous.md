---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mountainous
offline_file: ""
offline_thumbnail: ""
uuid: 052f8a9d-1ef6-407d-b953-d7034b95723d
updated: 1484310545
title: mountainous
categories:
    - Dictionary
---
mountainous
     adj 1: having hills and crags; "hilly terrain" [syn: {cragged}, {craggy},
             {hilly}]
     2: like a mountain in size and impressiveness; "mountainous
        waves"; "a mountainous dark man"
     3: containing many mountains
