---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/straw
offline_file: ""
offline_thumbnail: ""
uuid: d61e7af6-c07f-470f-b54d-43d0e1b65587
updated: 1484310210
title: straw
categories:
    - Dictionary
---
straw
     adj : of a pale yellow color like straw; straw colored
     n 1: plant fiber used e.g. for making baskets and hats or as
          fodder
     2: material consisting of seed coverings and small pieces of
        stem or leaves that have been separated from the seeds
        [syn: {chaff}, {husk}, {shuck}, {stalk}, {stubble}]
     3: a yellow tint; yellow diluted with white [syn: {pale yellow}]
     4: a thin paper or plastic tube used to such liquids into the
        mouth [syn: {drinking straw}]
     v 1: cover or provide with or as if with straw; "cows were
          strawed to weather the snowstorm"
     2: spread by scattering ("straw" is archaic); "strew toys all
        ...
