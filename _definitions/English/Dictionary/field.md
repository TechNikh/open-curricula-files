---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/field
offline_file: ""
offline_thumbnail: ""
uuid: 7da43900-8f50-4c7d-a90c-5fe7c7f51e37
updated: 1484310256
title: field
categories:
    - Dictionary
---
field
     n 1: a piece of land cleared of trees and usually enclosed; "he
          planted a field of wheat"
     2: a region where a battle is being (or has been) fought; "they
        made a tour of Civil War battlefields" [syn: {battlefield},
         {battleground}, {field of battle}, {field of honor}]
     3: somewhere (away from a studio or office or library or
        laboratory) where practical work is done or data is
        collected; "anthropologists do much of their work in the
        field"
     4: a branch of knowledge; "in what discipline is his
        doctorate?"; "teachers should be well trained in their
        subject"; "anthropology is the study of human beings"
     ...
