---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gordan%27s_lemma'
offline_file: ""
offline_thumbnail: ""
uuid: 926d654a-e5f4-43f5-ac12-951bec607b60
updated: 1484309149
title: "Gordan's lemma"
tags:
    - Proof
    - Topological proof
    - Algebraic proof
categories:
    - Algebraic geometry
---
In convex geometry, Gordan's lemma states that the semigroup of integral points in the dual cone of a rational convex polyhedral cone is finitely generated.[1] In algebraic geometry, the prime spectrum of the semigroup algebra of such a semigroup is, by definition, an affine toric variety; thus, the lemma says an affine toric variety is indeed an algebraic variety. The lemma is named after the German mathematician Paul Gordan (1837–1912).
