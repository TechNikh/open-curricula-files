---
version: 1
type: video
provider: YouTube
id: Y663Q4eBqXw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: fc959e50-80ab-4a16-ad0b-caeb1e998182
updated: 1486069607
title: Comparison word problems
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/Y663Q4eBqXw/default.jpg
    - https://i3.ytimg.com/vi/Y663Q4eBqXw/1.jpg
    - https://i3.ytimg.com/vi/Y663Q4eBqXw/2.jpg
    - https://i3.ytimg.com/vi/Y663Q4eBqXw/3.jpg
---
