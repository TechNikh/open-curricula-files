---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/jaundice
offline_file: ""
offline_thumbnail: ""
uuid: b7981cf2-e82b-4969-96a2-fa85403fed1d
updated: 1484310163
title: jaundice
categories:
    - Dictionary
---
jaundice
     n 1: yellowing of the skin and the whites of the eyes caused by
          an accumulation of bile pigment (bilirubin) in the
          blood; can be a symptom of gallstones or liver infection
          or anemia [syn: {icterus}]
     2: a sharp and bitter manner [syn: {bitterness}, {acrimony}, {acerbity}]
     v 1: distort adversely; "Jealousy had jaundiced his judgment"
     2: affect with, or as if with, jaundice
