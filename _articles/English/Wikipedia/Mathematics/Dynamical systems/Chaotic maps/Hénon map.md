---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/H%C3%A9non_map'
offline_file: ""
offline_thumbnail: ""
uuid: 6cd0572d-d6db-4907-abb4-6e142f598d83
updated: 1484308722
title: Hénon map
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-HenonMap.svg.png
tags:
    - Attractor
    - Decomposition
    - Notes
categories:
    - Chaotic maps
---
The Hénon map is a discrete-time dynamical system. It is one of the most studied examples of dynamical systems that exhibit chaotic behavior. The Hénon map takes a point (xn, yn) in the plane and maps it to a new point
