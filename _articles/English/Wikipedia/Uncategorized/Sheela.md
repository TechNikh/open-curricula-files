---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Sheela
offline_file: ""
offline_thumbnail: ""
uuid: 4ab5f546-16b2-4d2d-882a-063cdea69afa
updated: 1484308351
title: Sheela
tags:
    - Early life and family
    - Film career
    - Awards
    - Filmography
    - Actress
    - Malayalam
    - Tamil
    - telugu
    - Story, screenplay, and direction
    - Television
    - Malayalam
    - Tamil
categories:
    - Uncategorized
---
Sheela (born 24 March 1945) is an Indian film actress who works predominantly in Malayalam cinema.She is the real queen in Malayalam film industry . Alongside Prem Nazir, she holds the Guinness World Record for acting in the largest number of films (107) together as heroine and hero.[2][3] In 2005 she won the National Film Award for Best Supporting Actress for her role in the Malayalam film Akale.
