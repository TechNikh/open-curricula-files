---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Lyapunov_stability
offline_file: ""
offline_thumbnail: ""
uuid: c543b299-cbe4-471e-a2ae-68b5bec2da16
updated: 1484308837
title: Lyapunov stability
tags:
    - History
    - Definition for continuous-time systems
    - "Lyapunov's second method for stability"
    - Definition for discrete-time systems
    - Stability for linear state space models
    - Stability for systems with inputs
    - Example
    - "Barbalat's lemma and stability of time-varying systems"
categories:
    - Stability theory
---
Various types of stability may be discussed for the solutions of differential equations or difference equations describing dynamical systems. The most important type is that concerning the stability of solutions near to a point of equilibrium. This may be discussed by the theory of Lyapunov. In simple terms, if the solutions that start out near an equilibrium point 
  
    
      
        
          x
          
            e
          
        
      
    
    {\displaystyle x_{e}}
  
 stay near 
  
    
      
        
          x
          
            e
          
        
      
    
    {\displaystyle x_{e}}
  
 forever, then 
  
    
      
        
          x
          
            ...
