---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generalized_flag_variety
offline_file: ""
offline_thumbnail: ""
uuid: 55c276d7-865a-4701-9553-1e82db8cec44
updated: 1484309219
title: Generalized flag variety
tags:
    - Flags in a vector space
    - 'Prototype: the complete flag variety'
    - Partial flag varieties
    - Generalization to semisimple groups
    - Cohomology
    - Highest weight orbits and homogeneous projective varieties
    - Symmetric spaces
categories:
    - Differential geometry
---
In mathematics, a generalized flag variety (or simply flag variety) is a homogeneous space whose points are flags in a finite-dimensional vector space V over a field F. When F is the real or complex numbers, a generalized flag variety is a smooth or complex manifold, called a real or complex flag manifold. Flag varieties are naturally projective varieties.
