---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/plaster
offline_file: ""
offline_thumbnail: ""
uuid: 8bdf16ee-d4c6-4e85-ae18-36e49a3f18dc
updated: 1484310387
title: plaster
categories:
    - Dictionary
---
plaster
     n 1: a mixture of lime or gypsum with sand and water; hardens
          into a smooth solid; used to cover walls and ceilings
     2: any of several gypsum cements; a white powder (a form of
        calcium sulphate) that forms a paste when mixed with water
        and hardens into a solid; used in making molds and
        sculptures and casts for broken limbs [syn: {plaster of
        Paris}]
     3: a medical dressing consisting of a soft heated mass of meal
        or clay that is spread on a cloth and applied to the skin
        to treat inflamed areas or improve circulation etc. [syn:
        {poultice}, {cataplasm}]
     4: a hardened surface of plaster (as on a wall or ...
