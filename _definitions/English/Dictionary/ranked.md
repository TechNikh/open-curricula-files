---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ranked
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484506681
title: ranked
categories:
    - Dictionary
---
ranked
     adj : arranged in a sequence of grades or ranks; "stratified areas
           of the distribution" [syn: {graded}, {stratified}]
