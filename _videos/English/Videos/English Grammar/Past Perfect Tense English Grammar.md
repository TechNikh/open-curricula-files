---
version: 1
type: video
provider: YouTube
id: XjS8xPRtjlc
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 1d816c3c-0280-4934-832c-5587b6c144a8
updated: 1486069632
title: Past Perfect Tense English Grammar
categories:
    - English Grammar
thumbnail_urls:
    - https://i3.ytimg.com/vi/XjS8xPRtjlc/default.jpg
    - https://i3.ytimg.com/vi/XjS8xPRtjlc/1.jpg
    - https://i3.ytimg.com/vi/XjS8xPRtjlc/2.jpg
    - https://i3.ytimg.com/vi/XjS8xPRtjlc/3.jpg
---
