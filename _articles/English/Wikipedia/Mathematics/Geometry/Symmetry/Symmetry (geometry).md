---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Symmetry_(geometry)
offline_file: ""
offline_thumbnail: ""
uuid: 4dda1d17-127b-42ab-8ed8-a3277319c0a5
updated: 1484309317
title: Symmetry (geometry)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Simetria-bilateria.svg.png
tags:
    - Symmetries in general
    - Reflectional symmetry
    - Point reflection and other involutive isometries
    - Rotational symmetry
    - Translational symmetry
    - Glide reflection symmetry
    - Rotoreflection symmetry
    - Helical symmetry
    - Double rotation symmetry
    - Non-isometric symmetries
    - Scale symmetry and fractals
    - Abstract symmetry
    - "Klein's view"
    - "Thurston's view"
categories:
    - Symmetry
---
A geometric object has symmetry if there is an "operation" or "transformation" (technically, an isometry or affine map) that maps the figure/object onto itself; i.e., it is said that the object has an invariance under the transform.[1] For instance, a circle rotated about its center will have the same shape and size as the original circle—all points before and after the transform would be indistinguishable. A circle is said to be symmetric under rotation or to have rotational symmetry. If the isometry is the reflection of a plane figure, the figure is said to have reflectional symmetry or line symmetry; moreover, it is possible for a figure/object to have more than one line of symmetry.[2]
