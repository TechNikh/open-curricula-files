---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/subsequently
offline_file: ""
offline_thumbnail: ""
uuid: 955772a2-d018-4534-bfa0-394cf9d9f8b2
updated: 1484310601
title: subsequently
categories:
    - Dictionary
---
subsequently
     adv : happening at a time subsequent to a reference time; "he
           apologized subsequently"; "he's going to the store but
           he'll be back here later"; "it didn't happen until
           afterward"; "two hours after that" [syn: {later}, {afterwards},
            {afterward}, {after}, {later on}]
