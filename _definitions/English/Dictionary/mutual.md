---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mutual
offline_file: ""
offline_thumbnail: ""
uuid: b29fe7a4-047e-4537-bc04-bf3741b6b3b3
updated: 1484310405
title: mutual
categories:
    - Dictionary
---
mutual
     adj 1: common to or shared by two or more parties; "a common
            friend"; "the mutual interests of management and
            labor" [syn: {common}]
     2: concerning each of two or more persons or things; especially
        given or done in return; "reciprocal aid"; "reciprocal
        trade"; "mutual respect"; "reciprocal privileges at other
        clubs" [syn: {reciprocal}] [ant: {nonreciprocal}]
