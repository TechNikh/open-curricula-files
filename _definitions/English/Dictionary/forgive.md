---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/forgive
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484588761
title: forgive
categories:
    - Dictionary
---
forgive
     v 1: stop blaming or grant forgiveness; "I forgave him his
          infidelity"; "She cannot forgive him for forgetting her
          birthday"
     2: absolve from payment; "I forgive you your debt"
     [also: {forgiven}, {forgave}]
