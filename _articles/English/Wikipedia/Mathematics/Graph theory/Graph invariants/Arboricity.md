---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Arboricity
offline_file: ""
offline_thumbnail: ""
uuid: ddf876bf-be89-461b-963a-8949496b3593
updated: 1484309417
title: Arboricity
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-K44_arboricity.svg.png
tags:
    - Example
    - Arboricity as a measure of density
    - Algorithms
    - Related concepts
categories:
    - Graph invariants
---
The arboricity of an undirected graph is the minimum number of forests into which its edges can be partitioned. Equivalently it is the minimum number of spanning forests needed to cover all the edges of the graph.
