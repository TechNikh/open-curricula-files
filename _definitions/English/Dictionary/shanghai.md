---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shanghai
offline_file: ""
offline_thumbnail: ""
uuid: d8659a40-2581-4676-a681-1108e953348a
updated: 1484310569
title: shanghai
categories:
    - Dictionary
---
Shanghai
     n : the largest city of China; located in the east on the
         Pacific; one of the largest ports in the world
     v : take (someone) against his will for compulsory service,
         especially on board a ship; "The men were shanghaied
         after being drugged" [syn: {impress}]
