---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dismantle
offline_file: ""
offline_thumbnail: ""
uuid: f567ea1a-e420-4057-bfbe-fb34cdefa508
updated: 1484310565
title: dismantle
categories:
    - Dictionary
---
dismantle
     v 1: tear down so as to make flat with the ground; "The building
          was levelled" [syn: {level}, {raze}, {rase}, {tear down},
           {take down}, {pull down}] [ant: {raise}]
     2: take apart into its constituent pieces [syn: {disassemble},
        {take apart}, {break up}, {break apart}] [ant: {assemble}]
     3: take off or remove; "strip a wall of its wallpaper" [syn: {strip}]
