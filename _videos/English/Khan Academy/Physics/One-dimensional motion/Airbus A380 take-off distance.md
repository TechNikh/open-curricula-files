---
version: 1
type: video
provider: YouTube
id: fQt69_Q2CTw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/One-dimensional%20motion/Airbus%20A380%20take-off%20distance.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5154f2d0-692e-4391-abe5-f70878ca5314
updated: 1486069595
title: Airbus A380 take-off distance
categories:
    - One-dimensional motion
thumbnail_urls:
    - https://i3.ytimg.com/vi/fQt69_Q2CTw/default.jpg
    - https://i3.ytimg.com/vi/fQt69_Q2CTw/1.jpg
    - https://i3.ytimg.com/vi/fQt69_Q2CTw/2.jpg
    - https://i3.ytimg.com/vi/fQt69_Q2CTw/3.jpg
---
How long of a runway does an A380 need? Created by Sal Khan.

Watch the next lesson: https://www.khanacademy.org/science/physics/one-dimensional-motion/acceleration-tutorial/v/why-distance-is-area-under-velocity-time-line?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/one-dimensional-motion/acceleration-tutorial/v/airbus-a380-take-off-time?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
