---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mish-mash
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495881
title: mish-mash
categories:
    - Dictionary
---
mishmash
     n : a motley assortment of things [syn: {odds and ends}, {oddments},
          {melange}, {farrago}, {ragbag}, {hodgepodge}, {mingle-mangle},
          {hotchpotch}, {omnium-gatherum}]
