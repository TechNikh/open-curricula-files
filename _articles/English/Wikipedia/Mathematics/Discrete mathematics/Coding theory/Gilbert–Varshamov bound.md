---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Gilbert%E2%80%93Varshamov_bound'
offline_file: ""
offline_thumbnail: ""
uuid: 8ca548f2-7ba7-4032-a725-67a78206704f
updated: 1484308504
title: Gilbert–Varshamov bound
tags:
    - Statement of the bound
    - Proof
    - An improvement in the prime power case
categories:
    - Coding theory
---
In coding theory, the Gilbert–Varshamov bound (due to Edgar Gilbert[1] and independently Rom Varshamov[2]) is a limit on the parameters of a (not necessarily linear) code. It is occasionally known as the Gilbert–Shannon–Varshamov bound (or the GSV bound), but the name "Gilbert–Varshamov bound" is by far the most popular. Varshamov proved this bound by using the probabilistic method for linear codes. For more about that proof, see: GV-linear-code.
