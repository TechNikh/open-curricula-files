---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/basically
offline_file: ""
offline_thumbnail: ""
uuid: ca6d809d-cda9-4898-995c-a5e214d52694
updated: 1484310375
title: basically
categories:
    - Dictionary
---
basically
     adv : at bottom or by one's (or its) very nature; "He is basically
           dishonest"; "the argument was essentially a technical
           one"; "for all his bluster he is in essence a shy
           person" [syn: {fundamentally}, {essentially}, {in
           essence}, {au fond}]
