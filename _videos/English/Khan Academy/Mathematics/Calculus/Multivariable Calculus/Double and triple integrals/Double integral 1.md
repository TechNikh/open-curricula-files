---
version: 1
type: video
provider: YouTube
id: 85zGYB-34jQ
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Multivariable%20Calculus/Double%20and%20triple%20integrals/Double%20integral%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9c716c7f-a57d-4c28-b438-19f8e74ebc81
updated: 1486069689
title: Double integral 1
categories:
    - Double and triple integrals
thumbnail_urls:
    - https://i3.ytimg.com/vi/85zGYB-34jQ/default.jpg
    - https://i3.ytimg.com/vi/85zGYB-34jQ/1.jpg
    - https://i3.ytimg.com/vi/85zGYB-34jQ/2.jpg
    - https://i3.ytimg.com/vi/85zGYB-34jQ/3.jpg
---
