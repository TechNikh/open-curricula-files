---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/civilian
offline_file: ""
offline_thumbnail: ""
uuid: 010037d9-5ec2-4152-ac56-0f6a9097ca37
updated: 1484310579
title: civilian
categories:
    - Dictionary
---
civilian
     adj : associated with or performed by civilians as contrasted with
           the military; "civilian clothing"; "civilian life"
           [ant: {military}]
     n : a nonmilitary citizen [ant: {serviceman}]
