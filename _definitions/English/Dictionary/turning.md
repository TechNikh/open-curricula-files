---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/turning
offline_file: ""
offline_thumbnail: ""
uuid: 6e06af45-d4bc-427b-a1c4-5d6c199b1b1f
updated: 1484310459
title: turning
categories:
    - Dictionary
---
turning
     n 1: the act of changing or reversing the direction of the
          course; "he took a turn to the right" [syn: {turn}]
     2: act of changing in practice or custom; "the law took many
        turnings over the years"
     3: a movement in a new direction; "the turning of the wind"
        [syn: {turn}]
