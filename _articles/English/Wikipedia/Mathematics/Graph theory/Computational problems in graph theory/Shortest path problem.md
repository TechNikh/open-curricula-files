---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shortest_path_problem
offline_file: ""
offline_thumbnail: ""
uuid: e1f100ed-c68c-4f32-bd02-f1760de11c18
updated: 1484309398
title: Shortest path problem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-6n-graf.svg_1.png
tags:
    - Definition
    - Algorithms
    - Single-source shortest paths
    - Undirected graphs
    - Unweighted graphs
    - Directed acyclic graphs
    - Directed graphs with nonnegative weights
    - Planar directed graphs with nonnegative weights
    - >
        Directed graphs with arbitrary weights without negative
        cycles
    - Planar directed graphs with arbitrary weights
    - All-pairs shortest paths
    - Undirected graph
    - Directed graph
    - Applications
    - Road networks
    - Related problems
    - Strategic shortest-paths
    - Linear programming formulation
    - 'General algebraic framework on semirings: the algebraic path problem'
    - Shortest path in stochastic time-dependent networks
    - Notes
    - Bibliography
    - Missing references
categories:
    - Computational problems in graph theory
---
In graph theory, the shortest path problem is the problem of finding a path between two vertices (or nodes) in a graph such that the sum of the weights of its constituent edges is minimized.
