---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Combinatorial_class
offline_file: ""
offline_thumbnail: ""
uuid: 2794f4de-0ed5-4339-8de9-0e62265d9c5b
updated: 1484308563
title: Combinatorial class
categories:
    - Combinatorics
---
In mathematics, a combinatorial class is a countable set of mathematical objects, together with a size function mapping each object to a non-negative integer, such that there are finitely many objects of each size.[1][2]
