---
version: 1
type: video
provider: YouTube
id: wFpeM3fxJoQ
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 25cb45b4-b34a-409c-9d4b-7cf15cfbcebe
updated: 1486069641
title: VLT (Very Large Telescope) HD Timelapse Footage
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/wFpeM3fxJoQ/default.jpg
    - https://i3.ytimg.com/vi/wFpeM3fxJoQ/1.jpg
    - https://i3.ytimg.com/vi/wFpeM3fxJoQ/2.jpg
    - https://i3.ytimg.com/vi/wFpeM3fxJoQ/3.jpg
---
