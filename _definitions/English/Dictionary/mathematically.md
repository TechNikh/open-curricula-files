---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mathematically
offline_file: ""
offline_thumbnail: ""
uuid: e5e926c7-90c3-4051-8237-b75dc0d7657c
updated: 1484310200
title: mathematically
categories:
    - Dictionary
---
mathematically
     adv : with respect to mathematics; "mathematically impossible"
