---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vertex_cover
offline_file: ""
offline_thumbnail: ""
uuid: 042e6401-ccf5-4aee-ba8a-0ad796f97a92
updated: 1484309396
title: Vertex cover
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Couverture_de_sommets.svg.png
tags:
    - Definition
    - Examples
    - Properties
    - Computational problem
    - ILP formulation
    - Exact evaluation
    - Fixed-parameter tractability
    - Approximate evaluation
    - Inapproximability
    - Pseudo code
    - Vertex cover in hypergraphs
    - Fixed-parameter tractability
    - Hitting set and set cover
    - Applications
    - Notes
categories:
    - Computational problems in graph theory
---
In the mathematical discipline of graph theory, a vertex cover (sometimes node cover) of a graph is a set of vertices such that each edge of the graph is incident to at least one vertex of the set. The problem of finding a minimum vertex cover is a classical optimization problem in computer science and is a typical example of an NP-hard optimization problem that has an approximation algorithm. Its decision version, the vertex cover problem, was one of Karp's 21 NP-complete problems and is therefore a classical NP-complete problem in computational complexity theory. Furthermore, the vertex cover problem is fixed-parameter tractable and a central problem in parameterized complexity theory.
