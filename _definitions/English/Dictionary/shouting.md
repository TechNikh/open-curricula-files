---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shouting
offline_file: ""
offline_thumbnail: ""
uuid: 634cf1c3-7c37-419b-845a-af0132701b44
updated: 1484310144
title: shouting
categories:
    - Dictionary
---
shouting
     adj : noisy with or as if with loud cries and shouts; "a crying
           mass of rioters"; "a howling wind"; "shouting fans";
           "the yelling fiend" [syn: {crying}, {howling}, {yelling}]
     n 1: encouragement in the form of cheers from spectators; "it's
          all over but the shouting" [syn: {cheering}]
     2: uttering a loud inarticulate cry as of pain or excitement
        [syn: {yelling}]
