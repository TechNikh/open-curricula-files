---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/digit
offline_file: ""
offline_thumbnail: ""
uuid: bb518bd2-e131-41d6-9046-41e59a799af4
updated: 1484310156
title: digit
categories:
    - Dictionary
---
digit
     n 1: one of the elements that collectively form a system of
          numbers; "0 and 1 are digits" [syn: {figure}]
     2: the length of breadth of a finger used as a linear measure
        [syn: {finger}, {fingerbreadth}, {finger's breadth}]
     3: a finger or toe in human beings or corresponding part in
        other vertebrates [syn: {dactyl}]
