---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Central_angle
offline_file: ""
offline_thumbnail: ""
uuid: 8c0b86e9-d741-4826-89ec-868c4108af47
updated: 1484308925
title: Central angle
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Sector_central_angle_arc.svg.png
tags:
    - Formulas
    - Central angle of a regular polygon
categories:
    - Elementary geometry
---
A central angle is an angle whose apex (vertex) is the center O of a circle and whose legs (sides) are radii intersecting the circle in two distinct points A and B. The central angle is subtended by an arc between those two points, and the arc length is the central angle (measured in radians) times the radius.[1] The central angle is also known as the arc's angular distance.
