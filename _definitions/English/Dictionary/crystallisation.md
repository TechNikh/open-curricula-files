---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/crystallisation
offline_file: ""
offline_thumbnail: ""
uuid: 2d9958aa-8d9a-43be-9b5a-548400edeec8
updated: 1484310387
title: crystallisation
categories:
    - Dictionary
---
crystallisation
     n : the formation of crystals [syn: {crystallization}, {crystallizing}]
