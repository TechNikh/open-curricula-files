---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Generator_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 2b5dd2ac-0e80-4fb6-9b38-7f19f47356e7
updated: 1484308539
title: Generator matrix
tags:
    - Terminology
    - Equivalent Codes
    - Notes
categories:
    - Coding theory
---
In coding theory, a generator matrix is a matrix whose rows form a basis for a linear code. The codewords are all of the linear combinations of the rows of this matrix, that is, the linear code is the row space of its generator matrix.
