---
version: 1
type: video
provider: YouTube
id: 4F1M6PrJWlI
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: ce10b9ff-e690-489d-8c8b-d646a7700c03
updated: 1486069647
title: Mandelbulb 3D Tutorial + New Style?
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/4F1M6PrJWlI/default.jpg
    - https://i3.ytimg.com/vi/4F1M6PrJWlI/1.jpg
    - https://i3.ytimg.com/vi/4F1M6PrJWlI/2.jpg
    - https://i3.ytimg.com/vi/4F1M6PrJWlI/3.jpg
---
