---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/high
offline_file: ""
offline_thumbnail: ""
uuid: 3f60db14-262f-4e72-afe8-d50571961d62
updated: 1484310271
title: high
categories:
    - Dictionary
---
high
     adj 1: greater than normal in degree or intensity or amount; "a
            high temperature"; "a high price"; "the high point of
            his career"; "high risks"; "has high hopes"; "the
            river is high"; "he has a high opinion of himself"
            [ant: {low}]
     2: (literal meanings) being at or having a relatively great or
        specific elevation or upward extension (sometimes used in
        combinations like `knee-high'); "a high mountain"; "high
        ceilings"; "high buildings"; "a high forehead"; "a high
        incline"; "a foot high" [ant: {low}]
     3: standing above others in quality or position; "people in
        high places"; "the high ...
