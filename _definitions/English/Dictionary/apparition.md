---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/apparition
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484551501
title: apparition
categories:
    - Dictionary
---
apparition
     n 1: a ghostly appearing figure; "we were unprepared for the
          apparition that confronted us" [syn: {phantom}, {phantasm},
           {phantasma}, {specter}, {spectre}]
     2: the appearance of a ghostlike figure; "I was recalled to the
        present by the apparition of a frightening specter"
     3: something existing in perception only; "a ghostly apparition
        at midnight" [syn: {phantom}, {phantasm}, {phantasma}, {shadow}]
     4: an act of appearing or becoming visible unexpectedly;
        "natives were amazed at the apparition of this white
        stanger"
