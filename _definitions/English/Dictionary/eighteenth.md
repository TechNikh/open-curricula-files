---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eighteenth
offline_file: ""
offline_thumbnail: ""
uuid: 3278f385-62b4-499f-a1aa-bf2ce0a0f3d3
updated: 1484310395
title: eighteenth
categories:
    - Dictionary
---
eighteenth
     adj : coming next after the seventeenth in position [syn: {18th}]
     n : position 18 in a countable series of things
