---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Spanier%E2%80%93Whitehead_duality'
offline_file: ""
offline_thumbnail: ""
uuid: 3fc801d4-26f6-470b-9358-bb6c463d4bd6
updated: 1484309237
title: Spanier–Whitehead duality
categories:
    - Duality theories
---
In mathematics, Spanier–Whitehead duality is a duality theory in homotopy theory, based on a geometrical idea that a topological space X may be considered as dual to its complement in the n-sphere, where n is large enough. Its origins lie in the Alexander duality theory, in homology theory, concerning complements in manifolds. The theory is also referred to as S-duality, but this can now cause possible confusion with the S-duality of string theory. It is named for Edwin Spanier and J. H. C. Whitehead, who developed it in papers from 1955.
