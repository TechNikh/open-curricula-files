---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Equilibrium_point
offline_file: ""
offline_thumbnail: ""
uuid: ad20ea2e-b3fd-4a1f-96aa-f6e06a0da9ff
updated: 1484308825
title: Equilibrium point
tags:
    - Formal definition
    - Classification
categories:
    - Stability theory
---
