---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/mystery
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484598781
title: mystery
categories:
    - Dictionary
---
mystery
     n 1: something that baffles understanding and cannot be
          explained; "how it got out is a mystery"; "it remains
          one of nature's secrets" [syn: {enigma}, {secret}, {closed
          book}]
     2: a story about a crime (usually murder) presented as a novel
        or play or movie [syn: {mystery story}, {whodunit}]
