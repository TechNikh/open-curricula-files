---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cocoloring
offline_file: ""
offline_thumbnail: ""
uuid: 0031fdb7-c392-4555-9c9e-171ba50b6560
updated: 1484309376
title: Cocoloring
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Cocoloring.svg.png
categories:
    - Graph coloring
---
In graph theory, a cocoloring of a graph G is an assignment of colors to the vertices such that each color class forms an independent set in G or in the complement of G. The cochromatic number z(G) of G is the least number of colors needed in any cocolorings of G. The graphs with cochromatic number 2 are exactly the bipartite graphs, complements of bipartite graphs, and split graphs.
