---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/nitric
offline_file: ""
offline_thumbnail: ""
uuid: 3499ca45-0b8d-4811-ba7d-a87c05b9b216
updated: 1484310377
title: nitric
categories:
    - Dictionary
---
nitric
     adj : of or containing nitrogen; "nitric acid" [syn: {azotic}, {nitrous}]
