---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Sharkovskii%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 055268e5-ca99-4b44-a548-7df010bc7c39
updated: 1484308692
title: "Sharkovskii's theorem"
tags:
    - The theorem
    - Generalizations
categories:
    - Theorems in dynamical systems
---
In mathematics, Sharkovskii's theorem, named after Oleksandr Mykolaiovych Sharkovskii who published it in 1964, is a result about discrete dynamical systems.[1] One of the implications of the theorem is that if a discrete dynamical system on the real line has a periodic point of period 3, then it must have periodic points of every other period.
