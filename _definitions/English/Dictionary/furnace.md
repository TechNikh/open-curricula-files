---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/furnace
offline_file: ""
offline_thumbnail: ""
uuid: 0af3f085-300a-4c27-86a2-c272eef8c012
updated: 1484310411
title: furnace
categories:
    - Dictionary
---
furnace
     n : an enclosed chamber in which heat is produced to heat
         buildings, destroy refuse, smelt or refine ores, etc.
