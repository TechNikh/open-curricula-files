---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/complacent
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484448961
title: complacent
categories:
    - Dictionary
---
complacent
     adj : contented to a fault; "he had become complacent after years
           of success"; "his self-satisfied dignity" [syn: {self-satisfied}]
