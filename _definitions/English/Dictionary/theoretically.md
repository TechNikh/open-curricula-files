---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/theoretically
offline_file: ""
offline_thumbnail: ""
uuid: 3eca7835-91bd-4274-a2b5-8073a7f0a728
updated: 1484310212
title: theoretically
categories:
    - Dictionary
---
theoretically
     adv 1: according to the assumed facts; "on paper the candidate
            seems promising" [syn: {on paper}, {in theory}]
     2: in a theoretical manner; "he worked the problem out
        theoretically" [ant: {empirically}]
