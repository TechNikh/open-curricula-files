---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/female
offline_file: ""
offline_thumbnail: ""
uuid: f9dbb6d7-a2d9-4fd5-aa2f-115373ebd0d6
updated: 1484310297
title: female
categories:
    - Dictionary
---
female
     adj 1: being the sex (of plant or animal) that produces
            fertilizable gametes (ova) from which offspring
            develop; "a female heir"; "female holly trees bear the
            berries" [ant: {androgynous}, {male}]
     2: characteristic of or peculiar to a woman; "female
        sensitiveness"; "female suffrage" [syn: {distaff}]
     3: for or composed of women or girls; "the female lead in the
        play"; "a female chorus"
     n 1: an animal that produces gametes (ova) that can be fertilized
          by male gametes (spermatozoa) [ant: {male}]
     2: a person who belongs to the sex that can have babies [syn: {female
        person}] [ant: {male}]
