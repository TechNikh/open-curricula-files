---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grade
offline_file: ""
offline_thumbnail: ""
uuid: b3c716aa-6a7c-4a6b-af41-8fb0ba41e4cc
updated: 1484310421
title: grade
categories:
    - Dictionary
---
grade
     n 1: a body of students who are taught together; "early morning
          classes are always sleepy" [syn: {class}, {form}]
     2: a relative position or degree of value in a graded group;
        "lumber of the highest grade" [syn: {level}, {tier}]
     3: the gradient of a slope or road or other surface; "the road
        had a steep grade"
     4: one-hundredth of a right angle [syn: {grad}]
     5: a degree of ablaut [syn: {gradation}]
     6: a number or letter indicating quality (especially of a
        student's performance); "she made good marks in algebra";
        "grade A milk"; "what was your score on your homework?"
        [syn: {mark}, {score}]
     7: the height ...
