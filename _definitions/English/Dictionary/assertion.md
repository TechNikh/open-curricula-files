---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assertion
offline_file: ""
offline_thumbnail: ""
uuid: b1f751e1-6b79-40e9-b99b-f274aacf3c85
updated: 1484310204
title: assertion
categories:
    - Dictionary
---
assertion
     n 1: a declaration that is made emphatically (as if no supporting
          evidence were necessary) [syn: {averment}, {asseveration}]
     2: the act of affirming or asserting or stating something [syn:
         {affirmation}, {statement}]
