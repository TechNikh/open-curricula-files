---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/asylum
offline_file: ""
offline_thumbnail: ""
uuid: 7f19c17c-2501-4981-8d9a-eda22117355d
updated: 1484310176
title: asylum
categories:
    - Dictionary
---
asylum
     n 1: a shelter from danger or hardship [syn: {refuge}, {sanctuary}]
     2: a hospital for mentally incompetent or unbalanced person
        [syn: {mental hospital}, {psychiatric hospital}, {mental
        institution}, {institution}, {mental home}, {insane asylum}]
