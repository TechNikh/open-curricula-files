---
version: 1
type: video
provider: YouTube
id: GnYnBfOQdV8
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 463059e6-a454-465d-bc2c-bdc4e206b781
updated: 1486069617
title: Comparing exponential and logarithmic functions
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/GnYnBfOQdV8/default.jpg
    - https://i3.ytimg.com/vi/GnYnBfOQdV8/1.jpg
    - https://i3.ytimg.com/vi/GnYnBfOQdV8/2.jpg
    - https://i3.ytimg.com/vi/GnYnBfOQdV8/3.jpg
---
