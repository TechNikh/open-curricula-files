---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/accordingly
offline_file: ""
offline_thumbnail: ""
uuid: ac7fecc6-a604-492b-b5bf-19eda4441401
updated: 1484310521
title: accordingly
categories:
    - Dictionary
---
accordingly
     adv 1: (sentence connectors) because of the reason given;
            "consequently, he didn't do it"; "continued to have
            severe headaches and accordingly returned to the
            doctor" [syn: {consequently}]
     2: in accordance with; "she acted accordingly"
