---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Bianconi%E2%80%93Barab%C3%A1si_model'
offline_file: ""
offline_thumbnail: ""
uuid: c777c246-357f-4362-b216-87f9c02c3242
updated: 1484309448
title: Bianconi–Barabási model
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/350px-Bec5555_0.jpg
tags:
    - concepts
    - Algorithm
    - growth
    - Preferential attachment
    - Properties
    - Equal fitnesses
    - Degree distribution
    - History
categories:
    - Random graphs
---
The Bianconi–Barabási model is a model in network science that explains the growth of complex evolving networks. This model can explain that nodes with different characteristics acquire links at different rates. It predicts that a node's growth depends on its fitness and can calculate the degree distribution. The Bianconi–Barabási model is named after its inventors Ginestra Bianconi and Albert-László Barabási. This model is a variant of the Barabási–Albert model.
