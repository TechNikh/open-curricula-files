---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Maikal
offline_file: ""
offline_thumbnail: ""
uuid: 2b2aab31-3806-42ba-8008-67896b4d70c1
updated: 1484308420
title: Maikal Hills
tags:
    - geography
    - Natural Reserves
    - Kanha National Park
    - Geology
categories:
    - Uncategorized
---
The Maikal Hills are range of hills in the state of Chhattisgarh India. The Maikal Hills are an eastern part of the Satpuras in Kawardha District of Chhattisgarh, overlooking the scenic town of Kawardha. they have an altitude ranging from 340 m to 941 m above sea level. It is a picturesque spot in the state with its serene and peaceful atmosphere. This densely forested and thinly populated range gives rise to several streams and rivers including the tributaries of Narmada and Wainganga rivers. The hills are inhabited by two tribal peoples, the Baigas and the Gonds. The hill range is very rich in flora and fauna wealth.[1]
