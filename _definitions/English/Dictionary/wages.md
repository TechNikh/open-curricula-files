---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wages
offline_file: ""
offline_thumbnail: ""
uuid: f2716ee8-8492-48cc-a263-810d35d1aacc
updated: 1484310441
title: wages
categories:
    - Dictionary
---
wages
     n : a recompense for worthy acts or retribution for wrongdoing;
         "the wages of sin is death"; "virtue is its own reward"
         [syn: {reward}, {payoff}]
