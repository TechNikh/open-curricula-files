---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Cartesian_coordinate_system
offline_file: ""
offline_thumbnail: ""
uuid: ce7e5836-d5c5-4ade-ae15-65c0c0d41b81
updated: 1484309160
title: Cartesian coordinate system
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Cartesian-coordinate-system.svg.png
tags:
    - History
    - Description
    - One dimension
    - Two dimensions
    - Three dimensions
    - Higher dimensions
    - Generalizations
    - Notations and conventions
    - Quadrants and octants
    - Cartesian formulae for the plane
    - Distance between two points
    - Euclidean transformations
    - Translation
    - Rotation
    - Reflection
    - Glide reflection
    - General matrix form of the transformations
    - Affine transformation
    - Scaling
    - Shearing
    - Orientation and handedness
    - In two dimensions
    - In three dimensions
    - Representing a vector in the standard basis
    - Applications
    - Notes
    - Sources
categories:
    - Analytic geometry
---
A Cartesian coordinate system is a coordinate system that specifies each point uniquely in a plane by a pair of numerical coordinates, which are the signed distances to the point from two fixed perpendicular directed lines, measured in the same unit of length. Each reference line is called a coordinate axis or just axis of the system, and the point where they meet is its origin, usually at ordered pair (0, 0). The coordinates can also be defined as the positions of the perpendicular projections of the point onto the two axes, expressed as signed distances from the origin.
