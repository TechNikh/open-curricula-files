---
version: 1
type: video
provider: YouTube
id: IAbSeAk5RJU
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Another%20u-subsitution%20example.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 4e9b6af1-0cce-47ed-a6da-936a356be6ec
updated: 1486069694
title: Another u-subsitution example
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/IAbSeAk5RJU/default.jpg
    - https://i3.ytimg.com/vi/IAbSeAk5RJU/1.jpg
    - https://i3.ytimg.com/vi/IAbSeAk5RJU/2.jpg
    - https://i3.ytimg.com/vi/IAbSeAk5RJU/3.jpg
---
