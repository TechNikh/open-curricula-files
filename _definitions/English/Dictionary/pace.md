---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pace
offline_file: ""
offline_thumbnail: ""
uuid: 3453d6c8-2bef-4038-b753-5ff37104bacb
updated: 1484310252
title: pace
categories:
    - Dictionary
---
pace
     n 1: the rate of moving (especially walking or running) [syn: {gait}]
     2: the distance covered by a step; "he stepped off ten paces
        from the old tree and began to dig" [syn: {footstep}, {step},
         {stride}]
     3: the relative speed of progress or change; "he lived at a
        fast pace"; "he works at a great rate"; "the pace of
        events accelerated" [syn: {rate}]
     4: a step in walking or running [syn: {stride}, {tread}]
     5: the rate of some repeating event [syn: {tempo}]
     6: a unit of length equal to 3 feet; defined as 91.44
        centimeters; originally taken to be the average length of
        a stride [syn: {yard}]
     v 1: walk with ...
