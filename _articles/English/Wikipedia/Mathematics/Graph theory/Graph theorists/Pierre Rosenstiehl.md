---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pierre_Rosenstiehl
offline_file: ""
offline_thumbnail: ""
uuid: f6b2bc5c-5d1d-4418-b790-6665289b2cd8
updated: 1484309472
title: Pierre Rosenstiehl
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Pierre_Rosenstiehl.jpg
categories:
    - Graph theorists
---
The Fraysseix-Rosenstiehl's planarity criterion is at the origin of the left-right planarity algorithm implemented in Pigale software, which is considered as the fastest implemented planarity testing algorithm.[1]
