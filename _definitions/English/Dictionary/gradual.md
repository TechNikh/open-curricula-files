---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/gradual
offline_file: ""
offline_thumbnail: ""
uuid: f0100163-8930-48bd-ba6b-b038b2f8592e
updated: 1484310273
title: gradual
categories:
    - Dictionary
---
gradual
     adj 1: proceeding in small stages; "a gradual increase in prices"
            [ant: {sudden}]
     2: of a topographical gradient; not steep or abrupt; "a gradual
        slope" [ant: {steep}]
     n : (Roman Catholic Church) an antiphon (usually from the Book
         of Psalms) immediately after the epistle at Mass
