---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/diagonally
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484333641
title: diagonally
categories:
    - Dictionary
---
diagonally
     adv : in a diagonal manner; "she lives diagonally across the
           street from us"
