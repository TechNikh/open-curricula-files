---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/interesting
offline_file: ""
offline_thumbnail: ""
uuid: 22c53028-e154-4539-8ffb-6372fbf406a7
updated: 1484310214
title: interesting
categories:
    - Dictionary
---
interesting
     adj : arousing or holding the attention [ant: {uninteresting}]
