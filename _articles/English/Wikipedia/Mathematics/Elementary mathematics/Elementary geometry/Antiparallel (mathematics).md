---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Antiparallel_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 5ffc519d-53fa-4645-8333-d07dbc1cfc77
updated: 1484308925
title: Antiparallel (mathematics)
tags:
    - Definitions
    - Antiparallel vectors
    - Relations
categories:
    - Elementary geometry
---
