---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/wide
offline_file: ""
offline_thumbnail: ""
uuid: 3b256407-c0cb-4d73-b84e-eb18d6fa2b9b
updated: 1484310391
title: wide
categories:
    - Dictionary
---
wide
     adj 1: having great (or a certain) extent from one side to the
            other; "wide roads"; "a wide necktie"; "wide margins";
            "three feet wide"; "a river two miles broad"; "broad
            shoulders"; "a broad river" [syn: {broad}] [ant: {narrow}]
     2: broad in scope or content; "across-the-board pay increases";
        "an all-embracing definition"; "blanket sanctions against
        human-rights violators"; "an invention with broad
        applications"; "a panoptic study of Soviet nationality"-
        T.G.Winner; "granted him wide powers" [syn: {across-the-board},
         {all-embracing}, {all-encompassing}, {all-inclusive}, {blanket(a)},
         ...
