---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pons_asinorum
offline_file: ""
offline_thumbnail: ""
uuid: 7048f4c0-9537-45c1-899d-624d1cd9f806
updated: 1484308935
title: Pons asinorum
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/150px-Byrne_Preface-15.png
tags:
    - Proofs
    - Euclid and Proclus
    - Pappus
    - Others
    - In inner product spaces
    - Etymology and related terms
    - Metaphorical usage
categories:
    - Elementary geometry
---
In geometry, the statement that the angles opposite the equal sides of an isosceles triangle are themselves equal is known as the pons asinorum (Latin pronunciation: [ˈpons asiˈnoːrʊm]; English /ˈpɒnz ˌæsᵻˈnɔərəm/ PONZ-ass-i-NOR-(r)əm), Latin for "bridge of donkeys". This statement is Proposition 5 of Book 1 in Euclid's Elements, and is also known as the isosceles triangle theorem. Its converse is also true: if two angles of a triangle are equal, then the sides opposite them are also equal.
