---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Edward_Kofler
offline_file: ""
offline_thumbnail: ""
uuid: 9055a31c-f2a9-406e-979c-535a6d8e6ae9
updated: 1484308512
title: Edward Kofler
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Kofbook89.jpg
tags:
    - Biography
    - Bibliography
categories:
    - Game theorists
---
Edward Kofler (November 16, 1911 – April 22, 2007) was a mathematician who made important contributions to game theory and fuzzy logic by working out the theory of linear partial information.
