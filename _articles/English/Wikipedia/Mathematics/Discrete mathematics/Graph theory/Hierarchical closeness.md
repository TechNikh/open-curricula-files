---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Hierarchical_closeness
offline_file: ""
offline_thumbnail: ""
uuid: 8746bba3-4e96-4d9c-b155-7b85a901c9d7
updated: 1484308620
title: Hierarchical closeness
categories:
    - Graph theory
---
Hierarchical closeness (HC) is a structural centrality measure used in network theory or graph theory. It is extended from closeness centrality to rank how centrally located a node is in a directed network. While the original closeness centrality of a directed network considers the most important node to be that with the least total distance from all other nodes, hierarchical closeness evaluates the most important node as the one which reaches the most nodes by the shortest paths. The hierarchical closeness explicitly includes information about the range of other nodes that can be affected by the given node. In a directed network 
  
    
      
        G
        (
        V
        ,
      ...
