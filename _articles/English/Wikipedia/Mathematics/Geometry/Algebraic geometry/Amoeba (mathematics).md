---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Amoeba_(mathematics)
offline_file: ""
offline_thumbnail: ""
uuid: 53e72340-7af3-4a15-a4ec-e043e40a35cd
updated: 1484309134
title: Amoeba (mathematics)
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Amoeba_of_p%253Dw-2z-1.svg.png'
tags:
    - Definition
    - Properties
    - Ronkin function
categories:
    - Algebraic geometry
---
In complex analysis, a branch of mathematics, an amoeba is a set associated with a polynomial in one or more complex variables. Amoebas have applications in algebraic geometry, especially tropical geometry.
