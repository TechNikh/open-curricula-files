---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/squeak
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484597641
title: squeak
categories:
    - Dictionary
---
squeak
     n 1: a short high-pitched noise; "the squeak of of shoes on
          powdery snow"
     2: something achieved (or escaped) by a narrow margin [syn: {close
        call}, {close shave}, {squeaker}, {narrow escape}]
     v : make a high-pitched, screeching noise; "The door creaked
         when I opened it slowly" [syn: {screech}, {creak}, {screak},
          {skreak}]
