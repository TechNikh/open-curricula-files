---
version: 1
type: video
provider: YouTube
id: s94suB5uLWw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: bb99d2e6-fa2d-4bf4-8f00-21e291be0df5
updated: 1486069685
title: 'Magnetism- Crash Course Physics #32'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/s94suB5uLWw/default.jpg
    - https://i3.ytimg.com/vi/s94suB5uLWw/1.jpg
    - https://i3.ytimg.com/vi/s94suB5uLWw/2.jpg
    - https://i3.ytimg.com/vi/s94suB5uLWw/3.jpg
---
