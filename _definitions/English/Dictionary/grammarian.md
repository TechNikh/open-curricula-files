---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grammarian
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484441101
title: grammarian
categories:
    - Dictionary
---
grammarian
     n : a linguist who specializes in the study of grammar and
         syntax [syn: {syntactician}]
