---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/responsibility
offline_file: ""
offline_thumbnail: ""
uuid: 34360fe7-1a18-490b-b623-0253033b3064
updated: 1484310555
title: responsibility
categories:
    - Dictionary
---
responsibility
     n 1: the social force that binds you to your obligations and the
          courses of action demanded by that force; "we must
          instill a sense of duty in our children"; "every right
          implies a responsibility; every opportunity, an
          obligation; every possession, a duty"- John
          D.Rockefeller Jr [syn: {duty}, {obligation}]
     2: the proper sphere or extent of your activities; "it was his
        province to take care of himself" [syn: {province}]
     3: a form of trustworthiness; the trait of being answerable to
        someone for something or being responsible for one's
        conduct; "he holds a position of great responsibility"
  ...
