---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Honda
offline_file: ""
offline_thumbnail: ""
uuid: c50ed3bc-35b0-4e0b-b5ae-f9cbb3c3f82f
updated: 1484308460
title: Honda
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Honda_aoyama.JPG
tags:
    - History
    - Corporate profile and divisions
    - Leadership
    - Products
    - Automobiles
    - Motorcycles
    - Power equipment
    - Engines
    - Robots
    - Aircraft
categories:
    - Uncategorized
---
Honda Motor Co., Ltd. (本田技研工業株式会社, Honda Giken Kōgyō KK?, IPA: [hoɴda] ( listen); /ˈhɒndə/) is a Japanese public multinational conglomerate corporation primarily known as a manufacturer of automobiles, aircraft, motorcycles, and power equipment.
