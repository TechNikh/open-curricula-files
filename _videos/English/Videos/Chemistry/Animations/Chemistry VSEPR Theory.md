---
version: 1
type: video
provider: YouTube
id: i3FCHVlSZc4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 435a9ded-6900-4459-95fc-fe3a1f9ddfcc
updated: 1486069662
title: Chemistry VSEPR Theory
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/i3FCHVlSZc4/default.jpg
    - https://i3.ytimg.com/vi/i3FCHVlSZc4/1.jpg
    - https://i3.ytimg.com/vi/i3FCHVlSZc4/2.jpg
    - https://i3.ytimg.com/vi/i3FCHVlSZc4/3.jpg
---
