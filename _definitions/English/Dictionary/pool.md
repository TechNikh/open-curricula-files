---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pool
offline_file: ""
offline_thumbnail: ""
uuid: 08941059-085a-451b-b223-25a15158348f
updated: 1484310206
title: pool
categories:
    - Dictionary
---
pool
     n 1: an excavation that is (usually) filled with water
     2: a small lake; "the pond was too small for sailing" [syn: {pond}]
     3: an organization of people or resources that can be shared;
        "a car pool"; "a secretarial pool"; "when he was first
        hired he was assigned to the pool"
     4: an association of companies for some definite purpose [syn:
        {consortium}, {syndicate}]
     5: any communal combination of funds; "everyone contributed to
        the pool"
     6: a small body of standing water (rainwater) or other liquid;
        "there were puddles of muddy water in the road after the
        rain"; "the body lay in a pool of blood" [syn: {puddle}]
  ...
