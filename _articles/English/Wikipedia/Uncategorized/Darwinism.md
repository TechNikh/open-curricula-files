---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Darwinism
offline_file: ""
offline_thumbnail: ""
uuid: 836d68f4-609c-4b69-8980-1964904b9f88
updated: 1484308345
title: Darwinism
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Charles_Darwin_by_Julia_Margaret_Cameron_2.jpg
tags:
    - Conceptions of Darwinism
    - 19th-century usage
    - Other uses
    - Notes
categories:
    - Uncategorized
---
Darwinism is a theory of biological evolution developed by the English naturalist Charles Darwin (1809-1882) and others, stating that all species of organisms arise and develop through the natural selection of small, inherited variations that increase the individual's ability to compete, survive, and reproduce. Also called Darwinian theory, it originally included the broad concepts of transmutation of species or of evolution which gained general scientific acceptance after Darwin published On the Origin of Species in 1859, including concepts which predated Darwin's theories, but subsequently referred to specific concepts of natural selection, of the Weismann barrier or in genetics of the ...
