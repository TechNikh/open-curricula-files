---
version: 1
type: video
provider: YouTube
id: 6uCY4x_3v0I
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 7b7f0378-1598-4cd6-88b2-f94b42819bef
updated: 1486069668
title: 'Travel of Air Through Respiratory System - Gas Exchange in the Lungs - Nose to Alveoli Pathway'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/6uCY4x_3v0I/default.jpg
    - https://i3.ytimg.com/vi/6uCY4x_3v0I/1.jpg
    - https://i3.ytimg.com/vi/6uCY4x_3v0I/2.jpg
    - https://i3.ytimg.com/vi/6uCY4x_3v0I/3.jpg
---
