---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Borel_determinacy_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 31147837-1e4d-4f38-a146-5b49ad22e50e
updated: 1484308997
title: Borel determinacy theorem
tags:
    - Background
    - Gale–Stewart games
    - Winning strategies
    - Topology
    - Previous results
    - Borel determinacy
    - Set-theoretic aspects
    - Stronger forms of determinacy
categories:
    - Theorems in the foundations of mathematics
---
In descriptive set theory, the Borel determinacy theorem states that any Gale-Stewart game whose payoff set is a Borel set is determined, meaning that one of the two players will have a winning strategy for the game. It was proved by Donald A. Martin in 1975. The theorem is applied in descriptive set theory to show that Borel sets in Polish spaces have regularity properties such as the perfect set property and the property of Baire.
