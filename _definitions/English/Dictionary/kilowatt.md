---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/kilowatt
offline_file: ""
offline_thumbnail: ""
uuid: 630210f1-c2a4-441f-b570-1b82eb27d127
updated: 1484310196
title: kilowatt
categories:
    - Dictionary
---
kilowatt
     n : a unit of power equal to 1000 watts [syn: {kW}]
