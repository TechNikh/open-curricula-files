---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Bisphenol
offline_file: ""
offline_thumbnail: ""
uuid: 2288d5e5-dcaf-4eed-924c-d0678da4fbc7
updated: 1484308391
title: Bisphenol
tags:
    - List
    - Health effects
    - Prevention measures
categories:
    - Uncategorized
---
The bisphenols (pronounced /ˈbɪs.fᵻ.nɒl/) are a group of chemical compounds with two hydroxyphenyl functionalities. Most of them are based on diphenylmethane. The exceptions are bisphenol S, P, and M. "Bisphenol" is a common name; the letter following refers to one of the reactants. Bisphenol A is the most popular representative of this group, often simply called "bisphenol."
