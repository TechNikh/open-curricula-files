---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Gramian_matrix
offline_file: ""
offline_thumbnail: ""
uuid: 87a5d8d5-d839-4f76-8fb1-9908956cf5ea
updated: 1484309162
title: Gramian matrix
tags:
    - Examples
    - Applications
    - Properties
    - Positive semidefinite
    - Change of basis
    - Gram determinant
categories:
    - Analytic geometry
---
In linear algebra, the Gram matrix (Gramian matrix or Gramian) of a set of vectors 
  
    
      
        
          v
          
            1
          
        
        ,
        …
        ,
        
          v
          
            n
          
        
      
    
    {\displaystyle v_{1},\dots ,v_{n}}
  
 in an inner product space is the Hermitian matrix of inner products, whose entries are given by 
  
    
      
        
          G
          
            i
            j
          
        
        =
        ⟨
        
          v
          
            i
          
        
        ,
        
          v
          
            j
          
        
        ⟩
      
    
  ...
