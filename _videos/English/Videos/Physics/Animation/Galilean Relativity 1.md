---
version: 1
type: video
provider: YouTube
id: VnEPZ_xZvP4
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Animation/Galilean%20Relativity%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: cd8f3fa6-29d3-4d12-9e77-26db5f6cd36e
updated: 1486069653
title: Galilean Relativity 1
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/VnEPZ_xZvP4/default.jpg
    - https://i3.ytimg.com/vi/VnEPZ_xZvP4/1.jpg
    - https://i3.ytimg.com/vi/VnEPZ_xZvP4/2.jpg
    - https://i3.ytimg.com/vi/VnEPZ_xZvP4/3.jpg
---
