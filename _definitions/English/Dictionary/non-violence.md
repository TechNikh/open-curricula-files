---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/non-violence
offline_file: ""
offline_thumbnail: ""
uuid: 18f3bfc3-ec4b-4573-bbda-89acab66a86a
updated: 1484310176
title: non-violence
categories:
    - Dictionary
---
nonviolence
     n : peaceful resistance to a government by fasting or refusing
         to cooperate [syn: {passive resistance}, {nonviolent
         resistance}]
