---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discussion
offline_file: ""
offline_thumbnail: ""
uuid: 4d3930ef-8298-4756-b1d9-a892e47654f0
updated: 1484310230
title: discussion
categories:
    - Dictionary
---
discussion
     n 1: an extended communication (often interactive) dealing with
          some particular topic; "the book contains an excellent
          discussion of modal logic"; "his treatment of the race
          question is badly biased" [syn: {treatment}, {discourse}]
     2: an exchange of views on some topic; "we had a good
        discussion"; "we had a word or two about it" [syn: {give-and-take},
         {word}]
