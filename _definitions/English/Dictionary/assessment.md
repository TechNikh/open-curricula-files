---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/assessment
offline_file: ""
offline_thumbnail: ""
uuid: 13fa1c08-25af-4807-b8d5-8e3fcf54cbb4
updated: 1484310277
title: assessment
categories:
    - Dictionary
---
assessment
     n 1: the classification of someone or something with respect to
          its worth [syn: {appraisal}]
     2: an amount determined as payable; "the assessment for repairs
        outraged the club's membership"
     3: the market value set on assets
     4: the act of judging or assessing a person or situation or
        event; "they criticized my judgment of the contestants"
        [syn: {judgment}, {judgement}]
