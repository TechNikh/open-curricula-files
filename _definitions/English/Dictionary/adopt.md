---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/adopt
offline_file: ""
offline_thumbnail: ""
uuid: e8757950-2202-4126-8260-1d675a5d8a59
updated: 1484310462
title: adopt
categories:
    - Dictionary
---
adopt
     v 1: choose and follow; as of theories, ideas, policies,
          strategies or plans; "She followed the feminist
          movement"; "The candidate espouses Republican ideals"
          [syn: {follow}, {espouse}]
     2: take up and practice as one's own [syn: {borrow}, {take over},
         {take up}]
     3: take on titles, offices, duties, responsibilities; "When
        will the new President assume office?" [syn: {assume}, {take
        on}, {take over}]
     4: take on a certain form, attribute, or aspect; "His voice
        took on a sad tone"; "The story took a new turn"; "he
        adopted an air of superiority"; "She assumed strange
        manners"; "The gods ...
