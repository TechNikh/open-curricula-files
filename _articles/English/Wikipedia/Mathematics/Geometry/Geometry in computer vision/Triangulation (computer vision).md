---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Triangulation_(computer_vision)
offline_file: ""
offline_thumbnail: ""
uuid: c99562c8-9af2-4d42-a33d-a1c0d1c02cbe
updated: 1484309181
title: Triangulation (computer vision)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/380px-TriangulationReal.svg.png
tags:
    - Introduction
    - Properties of triangulation methods
    - Singularities
    - Invariance
    - Computational complexity
    - Some triangulation methods found in the literature
    - Mid-point method
    - Direct linear transformation
    - Via the essential matrix
    - Optimal triangulation
categories:
    - Geometry in computer vision
---
In computer vision triangulation refers to the process of determining a point in 3D space given its projections onto two, or more, images. In order to solve this problem it is necessary to know the parameters of the camera projection function from 3D to 2D for the cameras involved, in the simplest case represented by the camera matrices. Triangulation is sometimes also referred to as reconstruction.
