---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Technical_drawing
offline_file: ""
offline_thumbnail: ""
uuid: e8cebfe7-8a7a-4f84-a6d5-343e39a4df96
updated: 1484309322
title: Technical drawing
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/210px-Drafter_at_work.jpg
tags:
    - Methods
    - Manual or by instrument
    - Computer aided design
    - Applications for technical drawing
    - Architecture
    - Engineering
    - Related fields
    - Technical illustration
    - Cutaway drawing
    - Technical drawings
    - Types of technical drawings
    - Two-dimensional representation
    - Three-dimensional representation
    - Views
    - Multiview
    - Section
    - Auxiliary
    - Pattern
    - Exploded
    - Standards and conventions
    - Basic drafting paper sizes
    - Patent drawing
    - Sets of technical drawings
    - Working drawings for production
    - Assembly drawings
    - As-fitted drawings
categories:
    - Technical drawing
---
Technical drawing, drafting or draughting, is the act and discipline of composing drawings that visually communicate how something functions or is to be constructed.
