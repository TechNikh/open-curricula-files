---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Nilgiris
offline_file: ""
offline_thumbnail: ""
uuid: 731a442e-0611-4321-ae10-1474fec058ac
updated: 1484308420
title: Nilgiri mountains
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-Nilgiris_Biosphere_Reserve.jpg
tags:
    - Location
    - Conservation
    - History
    - Peaks in the Nilgiris
    - Waterfalls
    - Flora and Fauna
categories:
    - Uncategorized
---
The Nilgiri (Blue Mountains), form part of the Western Ghats in western Tamil Nadu, Karnataka and Kerala states in Southern India. At least 24 of the Nilgiri mountains' peaks are above 2,000 metres (6,600 ft), the highest peak being Doddabetta, at 2,637 metres (8,652 ft).
