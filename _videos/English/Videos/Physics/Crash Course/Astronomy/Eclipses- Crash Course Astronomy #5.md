---
version: 1
type: video
provider: YouTube
id: PRgua7xceDA
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Physics/Crash%20Course/Astronomy/Eclipses-%20Crash%20Course%20Astronomy%20%235.webm"
offline_file: ""
offline_thumbnail: ""
uuid: aff405f0-e864-4b6e-a87e-2b2e33b05f3f
updated: 1486069684
title: 'Eclipses- Crash Course Astronomy #5'
categories:
    - Astronomy
thumbnail_urls:
    - https://i3.ytimg.com/vi/PRgua7xceDA/default.jpg
    - https://i3.ytimg.com/vi/PRgua7xceDA/1.jpg
    - https://i3.ytimg.com/vi/PRgua7xceDA/2.jpg
    - https://i3.ytimg.com/vi/PRgua7xceDA/3.jpg
---
