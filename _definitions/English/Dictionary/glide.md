---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/glide
offline_file: ""
offline_thumbnail: ""
uuid: 24cac096-cfb1-4808-9259-1f40620e9377
updated: 1484310146
title: glide
categories:
    - Dictionary
---
glide
     n 1: a vowel-like sound that serves as a consonant [syn: {semivowel}]
     2: the act of moving smoothly along a surface while remaining
        in contact with it; "his slide didn't stop until the
        bottom of the hill"; "the children lined up for a coast
        down the snowy slope" [syn: {slide}, {coast}]
     3: the activity of flying a glider [syn: {gliding}, {sailplaning},
         {soaring}, {sailing}]
     v 1: move smoothly and effortlessly
     2: fly in or as if in a glider plane
     3: cause to move or pass silently, smoothly, or imperceptibly
