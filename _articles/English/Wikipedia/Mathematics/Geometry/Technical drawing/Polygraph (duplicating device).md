---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Polygraph_(duplicating_device)
offline_file: ""
offline_thumbnail: ""
uuid: 5ebce688-c9e6-4ddb-8643-e459e0918261
updated: 1484309332
title: Polygraph (duplicating device)
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/200px-JeffersonPolygraphReproductionFrontView.jpg
tags:
    - Description of the device
    - Stationary parts
    - Platform
    - Bridge
    - Inkwells
    - Moving parts
    - Planar pantograph
    - Descending pantograph
    - Pen lift transfer
    - Viewing in museums
    - Patent
    - A modern version
categories:
    - Technical drawing
---
Patented by John Isaac Hawkins on May 17, 1803,[1] it was most famously used by the third U.S. president, Thomas Jefferson, who acquired his first polygraph in 1804 and later suggested improvements to Charles Willson Peale, owner of the American rights. Because Jefferson was a prolific letter writer, the preservation of his copies have offered historians extensive insights into Jefferson's viewpoints and actions.[2] Jefferson called the polygraph "the finest invention of the present age".[3][4] A description of Jefferson's office routine in his own words may be read here.[5]
