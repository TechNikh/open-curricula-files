---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deterioration
offline_file: ""
offline_thumbnail: ""
uuid: 41b3067c-b1c1-4ac7-bbbf-2c1e46303d3e
updated: 1484310377
title: deterioration
categories:
    - Dictionary
---
deterioration
     n 1: a symptom of reduced quality or strength [syn: {impairment}]
     2: process of changing to an inferior state [syn: {decline in
        quality}, {declension}, {worsening}]
