---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/transplanting
offline_file: ""
offline_thumbnail: ""
uuid: 63274488-96c2-433f-9bbe-b7f97f9f8e91
updated: 1484310521
title: transplanting
categories:
    - Dictionary
---
transplanting
     n : the act of uprooting and moving a plant to a new location;
         "the transplant was successful"; "too frequent
         transplanting is not good for plants" [syn: {transplant},
          {transplantation}]
