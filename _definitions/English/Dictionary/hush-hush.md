---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hush-hush
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484495581
title: hush-hush
categories:
    - Dictionary
---
hush-hush
     adj : conducted with or marked by hidden aims or methods;
           "clandestine intelligence operations";
           "cloak-and-dagger activities behind enemy lines";
           "hole-and-corner intrigue"; "secret missions"; "a
           secret agent"; "secret sales of arms"; "surreptitious
           mobilization of troops"; "an undercover investigation";
           "underground resistance" [syn: {clandestine}, {cloak-and-dagger},
            {hole-and-corner(a)}, {hugger-mugger}, {on the
           quiet(p)}, {secret}, {surreptitious}, {undercover}, {underground}]
