---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/cleave
offline_file: ""
offline_thumbnail: ""
uuid: 1a35f3e4-ad87-4f7b-84e5-e99402e474e9
updated: 1484310414
title: cleave
categories:
    - Dictionary
---
cleave
     v 1: separate or cut with a tool, such as a sharp instrument;
          "cleave the bone" [syn: {split}, {rive}]
     2: make by cutting into; "The water is going to cleave a
        channel into the rock"
     3: come or be in close contact with; stick or hold together and
        resist separation; "The dress clings to her body"; "The
        label stuck to the box"; "The sushi rice grains cohere"
        [syn: {cling}, {adhere}, {stick}, {cohere}]
     [also: {cloven}, {clove}, {cleft}]
