---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Centroidal_Voronoi_tessellation
offline_file: ""
offline_thumbnail: ""
uuid: f3a8ef04-f3b6-4db7-974d-da518f26025e
updated: 1484308608
title: Centroidal Voronoi tessellation
categories:
    - Discrete geometry
---
In geometry, a centroidal Voronoi tessellation (CVT) is a special type of Voronoi tessellation or Voronoi diagrams. A Voronoi tessellation is called centroidal when the generating point of each Voronoi cell is also its mean (center of mass). It can be viewed as an optimal partition corresponding to an optimal distribution of generators. A number of algorithms can be used to generate centroidal Voronoi tessellations, including Lloyd's algorithm for K-means clustering.
