---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/guidance
offline_file: ""
offline_thumbnail: ""
uuid: 3f9c7a60-40c1-4c2b-89c5-16ab6c9ca166
updated: 1484310256
title: guidance
categories:
    - Dictionary
---
guidance
     n 1: something that provides direction or advice as to a decision
          or course of action [syn: {counsel}, {counseling}, {counselling},
           {direction}]
     2: the act of guiding or showing the way [syn: {steering}]
     3: the act of setting and holding a course; "a new council was
        installed under the direction of the king" [syn: {steering},
         {direction}]
