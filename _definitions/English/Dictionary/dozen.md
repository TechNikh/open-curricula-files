---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/dozen
offline_file: ""
offline_thumbnail: ""
uuid: 477477ba-1037-43fb-83b4-9cd14fe42c95
updated: 1484310555
title: dozen
categories:
    - Dictionary
---
dozen
     adj : denoting a quantity consisting of 12 items or units [syn: {twelve},
            {12}, {xii}]
     n : the cardinal number that is the sum of eleven and one [syn:
         {twelve}, {12}, {XII}]
