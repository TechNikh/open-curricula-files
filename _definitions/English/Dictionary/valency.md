---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/valency
offline_file: ""
offline_thumbnail: ""
uuid: fbe12d80-8437-40f0-8472-e6f900457bfb
updated: 1484310397
title: valency
categories:
    - Dictionary
---
valency
     n 1: the phenomenon of forming chemical bonds
     2: (biology) a relative capacity to unite or react or interact
        as with antigens or a biological substrate [syn: {valence}]
     3: (chemistry) a property of atoms or radicals; their combining
        power given in terms of the number of hydrogen atoms (or
        the equivalent) [syn: {valence}]
