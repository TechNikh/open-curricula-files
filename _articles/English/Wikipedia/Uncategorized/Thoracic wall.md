---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Chest_wall
offline_file: ""
offline_thumbnail: ""
uuid: d1a8e25f-7f6a-4028-b700-3736d34a1169
updated: 1484308377
title: Thoracic wall
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/250px-Scheme_body_cavities-en.svg.png
tags:
    - Structure
    - Function
    - Diving reflex
    - Clinical significance
    - Necrosis
categories:
    - Uncategorized
---
