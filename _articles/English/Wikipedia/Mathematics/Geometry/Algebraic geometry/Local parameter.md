---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Local_parameter
offline_file: ""
offline_thumbnail: ""
uuid: b17965a1-5ef8-4f65-8d0a-899fd5faec22
updated: 1484309156
title: Local parameter
tags:
    - Introduction
    - Definition
categories:
    - Algebraic geometry
---
In the geometry of complex algebraic curves, a local parameter for a curve C at a smooth point P is just a meromorphic function on C that has a simple zero at P. This concept can be generalized to curves defined over fields other than 
  
    
      
        
          C
        
      
    
    {\displaystyle \mathbb {C} }
  
 (or even schemes), because the local ring at a smooth point P of an algebraic curve C (defined over an algebraically closed field) is always a discrete valuation ring.[1] This valuation will endow us with a way to count the order (at the point P) of rational functions (which are natural generalizations for meromorphic functions in the non-complex realm) having a zero ...
