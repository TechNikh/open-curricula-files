---
version: 1
type: video
provider: YouTube
id: FU6y1XIADdg
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 714d9f05-796c-42b3-81ad-0404b57dcfab
updated: 1486069680
title: 'Nuclear Chemistry Part 2- Fusion and Fission - Crash Course Chemistry #39'
categories:
    - Crash Course
thumbnail_urls:
    - https://i3.ytimg.com/vi/FU6y1XIADdg/default.jpg
    - https://i3.ytimg.com/vi/FU6y1XIADdg/1.jpg
    - https://i3.ytimg.com/vi/FU6y1XIADdg/2.jpg
    - https://i3.ytimg.com/vi/FU6y1XIADdg/3.jpg
---
