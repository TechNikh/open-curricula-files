---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Laplacian_smoothing
offline_file: ""
offline_thumbnail: ""
uuid: e87a5932-e82b-4092-80c9-9c25e2c54bb7
updated: 1484309365
title: Laplacian smoothing
categories:
    - Geometry stubs
---
Laplacian smoothing is an algorithm to smooth a polygonal mesh.[1][2] For each vertex in a mesh, a new position is chosen based on local information (such as the position of neighbors) and the vertex is moved there. In the case that a mesh is topologically a rectangular grid (that is, each internal vertex is connected to four neighbors) then this operation produces the Laplacian of the mesh.
