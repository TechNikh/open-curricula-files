---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/authentic
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484632801
title: authentic
categories:
    - Dictionary
---
authentic
     adj 1: conforming to fact and therefore worthy of belief; "an
            authentic account by an eyewitness"; "reliable
            information" [syn: {reliable}]
     2: not counterfeit or copied; "an authentic signature"; "a bona
        fide manuscript"; "an unquestionable antique";
        "photographs taken in a veritable bull ring" [syn: {bona
        fide}, {unquestionable}, {veritable}]
