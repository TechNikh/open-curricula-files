---
version: 1
type: video
provider: YouTube
id: omUfrXtHtN0
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Arithmetic/Level%204%20Subtraction.webm"
offline_file: ""
offline_thumbnail: ""
uuid: b408737e-e33a-424e-8532-94e547b790c9
updated: 1486069567
title: Level 4 Subtraction
categories:
    - Arithmetic
thumbnail_urls:
    - https://i3.ytimg.com/vi/omUfrXtHtN0/default.jpg
    - https://i3.ytimg.com/vi/omUfrXtHtN0/1.jpg
    - https://i3.ytimg.com/vi/omUfrXtHtN0/2.jpg
    - https://i3.ytimg.com/vi/omUfrXtHtN0/3.jpg
---
Subtraction of multi-digit numbers involving borrowing
