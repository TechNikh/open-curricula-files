---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Natural_density
offline_file: ""
offline_thumbnail: ""
uuid: 794dc439-f14b-48f5-9848-b7bcac6768aa
updated: 1484308592
title: Natural density
tags:
    - Definition
    - Upper and lower asymptotic density
    - Remark
    - Properties and examples
    - Other density functions
    - Notes
categories:
    - Combinatorics
---
In number theory, natural density (or asymptotic density or arithmetic density) is one of the possibilities to measure how large a subset of the set of natural numbers is.
