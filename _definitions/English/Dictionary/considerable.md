---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/considerable
offline_file: ""
offline_thumbnail: ""
uuid: 8c2d334c-7b12-4b13-b555-7ad421d3d0c6
updated: 1484310271
title: considerable
categories:
    - Dictionary
---
considerable
     adj : large or relatively large in number or amount or extent or
           degree; "a considerable quantity"; "the economy was a
           considerable issue in the campaign"; "went to
           considerable trouble for us"; "spent a considerable
           amount of time on the problem" [ant: {inconsiderable}]
