---
version: 1
type: video
provider: YouTube
id: QoFhPG_ks3M
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: c410f7d2-5213-48ef-b943-3f599c5b5dfe
updated: 1486069621
title: 'Time & distance Aptitude Shortcut Method'
categories:
    - Math Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/QoFhPG_ks3M/default.jpg
    - https://i3.ytimg.com/vi/QoFhPG_ks3M/1.jpg
    - https://i3.ytimg.com/vi/QoFhPG_ks3M/2.jpg
    - https://i3.ytimg.com/vi/QoFhPG_ks3M/3.jpg
---
