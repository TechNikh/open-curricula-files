---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magic
offline_file: ""
offline_thumbnail: ""
uuid: 45c1c8cd-b64e-41f5-b683-bab0204b5d9a
updated: 1484310405
title: magic
categories:
    - Dictionary
---
magic
     adj : possessing or using or characteristic of or appropriate to
           supernatural powers; "charming incantations"; "magic
           signs that protect against adverse influence"; "a
           magical spell"; "'tis now the very witching time of
           night"- Shakespeare; "wizard wands"; "wizardly powers"
           [syn: {charming}, {magical}, {sorcerous}, {witching(a)},
            {wizard(a)}, {wizardly}]
     n 1: any art that invokes supernatural powers
     2: an illusory feat; considered magical by naive observers
        [syn: {magic trick}, {conjuring trick}, {trick}, {legerdemain},
         {conjuration}, {illusion}, {deception}]
