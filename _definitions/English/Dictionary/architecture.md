---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/architecture
offline_file: ""
offline_thumbnail: ""
uuid: 89ccc63c-067a-49d9-a45a-2bb0550c6865
updated: 1484310609
title: architecture
categories:
    - Dictionary
---
architecture
     n 1: an architectural product or work
     2: the discipline dealing with the principles of design and
        construction and ornamentation of fine buildings;
        "architecture and eloquence are mixed arts whose end is
        sometimes beauty and sometimes use"
     3: the profession of designing buildings and environments with
        consideration for their esthetic effect
     4: (computer science) the structure and organization of a
        computer's hardware or system software; "the architecture
        of a computer's system software" [syn: {computer
        architecture}]
