---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/rear
offline_file: ""
offline_thumbnail: ""
uuid: ecf367a0-d6af-4422-a592-54719873c768
updated: 1484310216
title: rear
categories:
    - Dictionary
---
rear
     adj : located in or toward the back or rear; "the chair's rear
           legs"; "the rear door of the plane"; "on the rearward
           side" [syn: {rear(a)}, {rearward(a)}]
     n 1: the back of a military formation or procession; "infantrymen
          were in the rear" [ant: {head}]
     2: the side of an object that is opposite its front; "his room
        was toward the rear of the hotel" [syn: {backside}, {back
        end}] [ant: {front}]
     3: the part of something that is furthest from the normal
        viewer; "he stood at the back of the stage"; "it was
        hidden in the rear of the store" [syn: {back}] [ant: {front}]
     4: the fleshy part of the human body ...
