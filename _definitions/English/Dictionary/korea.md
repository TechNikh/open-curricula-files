---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/korea
offline_file: ""
offline_thumbnail: ""
uuid: b14db2a2-b29d-4569-b36c-1e22c4bd2822
updated: 1484310515
title: korea
categories:
    - Dictionary
---
Korea
     n : an Asian peninsula (off Manchuria) separating the Yellow Sea
         and the Sea of Japan; the Korean name is Choson [syn: {Korean
         Peninsula}, {Choson}]
