---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/insensitivity
offline_file: ""
offline_thumbnail: ""
uuid: 9dfa1886-b256-4b69-a478-d7fce0944c9c
updated: 1484310585
title: insensitivity
categories:
    - Dictionary
---
insensitivity
     n : the inability to respond to affective changes in your
         interpersonal environment [syn: {insensitiveness}] [ant:
         {sensitivity}, {sensitivity}]
