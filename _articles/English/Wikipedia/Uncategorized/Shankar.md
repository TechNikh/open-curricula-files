---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shankar
offline_file: ""
offline_thumbnail: ""
uuid: d7456551-eba2-4bf1-b4df-84471069910c
updated: 1484308335
title: Shankar
categories:
    - Uncategorized
---
Shankar is a Sanskrit word meaning "Beneficent" or "Giver of Bliss". Shankar is the Hindi/North Indian version of the name Shankara also written sometimes as Sankara.
