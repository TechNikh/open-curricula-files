---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Vortex_ring_toy
offline_file: ""
offline_thumbnail: ""
uuid: f3bc2ec1-10e6-426c-86bd-a5ee2191e5b1
updated: 1484308883
title: Vortex ring toy
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Giant_smoke_ring.jpg
categories:
    - Vortices
---
A vortex ring toy is any of several toys that generate vortex rings – rolling torus-shapes of fluid, similar in structure to smoke rings – that move through the fluid (most often air, and sometimes water).
