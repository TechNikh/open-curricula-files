---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/unpaired
offline_file: ""
offline_thumbnail: ""
uuid: 15ec1555-4548-4a1f-a130-e40e54134d69
updated: 1484310395
title: unpaired
categories:
    - Dictionary
---
unpaired
     adj : of the remaining member of a pair, of socks e.g. [syn: {odd},
            {unmatched}, {unmated}]
