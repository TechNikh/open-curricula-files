---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Shivaji
offline_file: ""
offline_thumbnail: ""
uuid: 754fb677-361e-4965-9463-aef8b5059f9d
updated: 1484308450
title: Shivaji
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Shivneri_Shiv_Mandir.JPG
tags:
    - Early life
    - Upbringing and concept of Hindavi Swarajya
    - Conflict with Adilshahi sultanate
    - Combat with Afzal Khan
    - Battle of Pratapgarh
    - Battle of Kolhapur
    - Siege of Panhala and Battle of Pavan Khind
    - Clash with the Mughals
    - Attack on Shaista Khan
    - Treaty of Purandar
    - Arrest in Agra and escape
    - Reconquest
    - Dealings with the English
    - Battle of Nesari
    - Coronation
    - Conquest in Southern India
    - Death and succession
    - The Marathas after Shivaji
    - Governance
    - Promotion of Marathi and Sanskrit
    - Religious policy
    - Islam
    - Military
    - Forts
    - Navy
    - Legacy
    - Historiography
    - Legacy
    - Commemorations
    - Statues
    - Armed forces
    - government
    - Airports and railway stations
    - Educational institutes
    - Depiction in popular culture
    - Films
    - Literature
    - Poetry and music
    - Theatre
    - Television
categories:
    - Uncategorized
---
Shivaji Bhonsle (Marathi [ʃiʋaˑɟiˑ bʱoˑs(ə)leˑ]; c. 1627/1630[1] – 3 April 1680), also known as Chhatrapati Shivaji Maharaj, was an Indian warrior king and a member of the Bhonsle Maratha clan. Shivaji carved out an enclave from the declining Adilshahi sultanate of Bijapur that formed the genesis of the Maratha Empire. In 1674, he was formally crowned as the Chhatrapati (Monarch) of his realm at Raigad.
