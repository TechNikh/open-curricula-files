---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/overdue
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484451601
title: overdue
categories:
    - Dictionary
---
overdue
     adj : past due; not paid at the scheduled time; "an overdue
           installment"; "a delinquent account" [syn: {delinquent}]
