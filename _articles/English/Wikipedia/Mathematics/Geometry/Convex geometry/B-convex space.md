---
version: 1
type: article
id: https://en.wikipedia.org/wiki/B-convex_space
offline_file: ""
offline_thumbnail: ""
uuid: 03ab803d-59fc-42a8-9495-741f1911dbcf
updated: 1484309192
title: B-convex space
categories:
    - Convex geometry
---
In functional analysis, the class of B-convex spaces is a class of Banach space. The concept of B-convexity was defined and used to characterize Banach spaces that have the strong law of large numbers by Anatole Beck in 1962; accordingly, "B-convexity" is understood as an abbreviation of Beck convexity. Beck proved the following theorem: A Banach space is B-convex if and only if every sequence of independent, symmetric, uniformly bounded and Radon random variables in that space satisfies the strong law of large numbers.
