---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/absent
offline_file: ""
offline_thumbnail: ""
uuid: dd0765f4-eecf-4647-b771-26bd8b98b408
updated: 1484310416
title: absent
categories:
    - Dictionary
---
absent
     adj 1: not in a specified place physically or mentally [ant: {present}]
     2: lost in thought; showing preoccupation; "an absent stare";
        "an absentminded professer"; "the scatty glancing quality
        of a hyperactive but unfocused intelligence" [syn: {absentminded},
         {abstracted}, {scatty}]
     v : go away or leave; "He absented himself" [syn: {remove}]
