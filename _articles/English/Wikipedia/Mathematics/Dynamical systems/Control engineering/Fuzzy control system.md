---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fuzzy_control_system
offline_file: ""
offline_thumbnail: ""
uuid: 93f0c8ef-e0cf-4240-bb48-595e1937f1e0
updated: 1484308738
title: Fuzzy control system
tags:
    - Overview
    - History and applications
    - Fuzzy sets
    - Fuzzy control in detail
    - Building a fuzzy controller
    - Antilock brakes
    - Logical interpretation of fuzzy control
categories:
    - Control engineering
---
A fuzzy control system is a control system based on fuzzy logic—a mathematical system that analyzes analog input values in terms of logical variables that take on continuous values between 0 and 1, in contrast to classical or digital logic, which operates on discrete values of either 1 or 0 (true or false, respectively).[1][2]
