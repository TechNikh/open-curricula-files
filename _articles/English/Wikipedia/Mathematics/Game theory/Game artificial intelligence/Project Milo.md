---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Project_Milo
offline_file: ""
offline_thumbnail: ""
uuid: 6a1247f1-8d92-4fc2-a856-d5f2f9a6bf32
updated: 1484309006
title: Project Milo
categories:
    - Game artificial intelligence
---
Project Milo (also referred to as Milo and Kate) was a project in development by Lionhead Studios for the Xbox 360 video game console. Formerly a secretive project under the early code name "Dimitri",[1] Project Milo was unveiled at the 2009 Electronic Entertainment Expo (E3) in a demonstration for Kinect, as a "controller-free" entertainment initiative for the Xbox 360 based on depth-sensing and pattern recognition technologies.[2] The project was a tech demo to showcase the capabilities of Kinect and has not been released,[3] despite conflicting reports that the project was an actual game.
