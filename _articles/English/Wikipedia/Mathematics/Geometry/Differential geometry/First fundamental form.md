---
version: 1
type: article
id: https://en.wikipedia.org/wiki/First_fundamental_form
offline_file: ""
offline_thumbnail: ""
uuid: 556a461e-2700-48a4-887f-d32a2a65da9a
updated: 1484309219
title: First fundamental form
tags:
    - Further notation
    - Calculating lengths and areas
    - Example
    - Length of a curve on the sphere
    - Area of a region on the sphere
    - Gaussian curvature
categories:
    - Differential geometry
---
In differential geometry, the first fundamental form is the inner product on the tangent space of a surface in three-dimensional Euclidean space which is induced canonically from the dot product of R3. It permits the calculation of curvature and metric properties of a surface such as length and area in a manner consistent with the ambient space. The first fundamental form is denoted by the Roman numeral I,
