---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/parcel
offline_file: ""
offline_thumbnail: ""
uuid: b2f6ee50-8bff-4858-82f5-00c1a84b8f38
updated: 1484310585
title: parcel
categories:
    - Dictionary
---
parcel
     n 1: a wrapped container [syn: {package}]
     2: the result of parcelling out or sharing; "death gets more
        than its share of attention from theologicans" [syn: {portion},
         {share}]
     3: an extended area of land [syn: {tract}, {piece of land}, {piece
        of ground}, {parcel of land}]
     4: a collection of things wrapped or boxed together [syn: {package},
         {bundle}, {packet}]
     v 1: divide into parts; "The developers parceled the land"
     2: cover with strips of canvas; "parcel rope"
     3: make into a wrapped container
     [also: {parcelling}, {parcelled}]
