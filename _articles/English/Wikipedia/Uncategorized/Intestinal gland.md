---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Succus_entericus
offline_file: ""
offline_thumbnail: ""
uuid: e7fad2c3-613c-4da0-84cb-d36723193214
updated: 1484308377
title: Intestinal gland
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/159px-Small_intestine_low_mag.jpg
tags:
    - Structure
    - Function
    - Intestinal juice
    - Colonic crypts
    - Clinical significance
    - History
categories:
    - Uncategorized
---
In histology, an intestinal gland (also crypt of Lieberkühn and intestinal crypt) is a gland found in the intestinal epithelium lining of the small intestine and large intestine (colon). The glands and intestinal villi are covered by epithelium which contains multiple types of cells: enterocytes (absorbing water and electrolytes), goblet cells (secreting mucus), enteroendocrine cells (secreting hormones), cup cells, tuft cells and, at the base of the gland, Paneth cells (secreting anti-microbial peptides) and stem cells. These cells are not all present in the colon.
