---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lottery
offline_file: ""
offline_thumbnail: ""
uuid: ae1643a9-cb42-4e1f-b9ba-7f15acfbf93c
updated: 1484310445
title: lottery
categories:
    - Dictionary
---
lottery
     n 1: something that is regarded as a chance event; "the election
          was just a lottery to them"
     2: players buy (or are given) chances and prizes are
        distributed according to the drawing of lots [syn: {drawing}]
