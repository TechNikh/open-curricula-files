---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/retail
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484400661
title: retail
categories:
    - Dictionary
---
retail
     adj : selling or related to selling direct to the consumer;
           "retail trade"; "retail business"; "retail price" [ant:
            {wholesale}]
     n : the selling of goods to consumers; usually in small
         quantities and not for resale [ant: {wholesale}]
     adv : at a retail price; "I'll sell it to you retail only" [ant: {wholesale}]
     v 1: be sold at the retail level; "These gems retail at thousands
          of dollars each"
     2: sell on the retail market [ant: {wholesale}]
