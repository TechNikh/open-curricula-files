---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/newspaper
offline_file: ""
offline_thumbnail: ""
uuid: cdf47bb7-6001-4a08-a3c5-25e4f98f9467
updated: 1484310443
title: newspaper
categories:
    - Dictionary
---
newspaper
     n 1: a daily or weekly publication on folded sheets; contains
          news and articles and advertisements; "he read his
          newspaper at breakfast" [syn: {paper}]
     2: a business firm that publishes newspapers; "Murdoch owns
        many newspapers" [syn: {paper}, {newspaper publisher}]
     3: a newspaper as a physical object; "when it began to rain he
        covered his head with a newspaper" [syn: {paper}]
     4: cheap paper made from wood pulp and used for printing
        newspapers; "they used bales of newspaper every day" [syn:
         {newsprint}]
