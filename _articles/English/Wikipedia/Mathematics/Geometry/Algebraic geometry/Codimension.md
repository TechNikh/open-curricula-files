---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Codimension
offline_file: ""
offline_thumbnail: ""
uuid: 3fe1b15a-d271-4c2b-8a04-de4616b08a52
updated: 1484309142
title: Codimension
tags:
    - Definition
    - Additivity of codimension and dimension counting
    - Dual interpretation
    - In geometric topology
categories:
    - Algebraic geometry
---
In mathematics, codimension is a basic geometric idea that applies to subspaces in vector spaces, to submanifolds in manifolds, and suitable subsets of algebraic varieties.
