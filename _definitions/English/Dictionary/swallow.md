---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swallow
offline_file: ""
offline_thumbnail: ""
uuid: 4515838d-d26e-48a6-842d-ec077e10db15
updated: 1484310337
title: swallow
categories:
    - Dictionary
---
swallow
     n 1: a small amount of liquid food; "a sup of ale" [syn: {sup}]
     2: the act of swallowing; "one swallow of the liquid was
        enough"; "he took a drink of his beer and smacked his
        lips" [syn: {drink}, {deglutition}]
     3: small long-winged songbird noted for swift graceful flight
        and the regularity of its migrations
     v 1: pass through the esophagus as part of eating or drinking;
          "Swallow the raw fish--it won't kill you!" [syn: {get
          down}]
     2: engulf and destroy; "The Nazis swallowed the Baltic
        countries"
     3: enclose or envelop completely, as if by swallowing; "The
        huge waves swallowed the small boat and ...
