---
version: 1
type: video
provider: YouTube
id: VrflZifKIuw
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Physics/Impacts%20and%20linear%20momentum/Center%20of%20mass.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 15d9f146-47d8-4472-b346-5a8feba512f3
updated: 1486069595
title: Center of mass
categories:
    - Impacts and linear momentum
thumbnail_urls:
    - https://i3.ytimg.com/vi/VrflZifKIuw/default.jpg
    - https://i3.ytimg.com/vi/VrflZifKIuw/1.jpg
    - https://i3.ytimg.com/vi/VrflZifKIuw/2.jpg
    - https://i3.ytimg.com/vi/VrflZifKIuw/3.jpg
---
In this video, David solves an example elastic collision problem to find the final velocities using the easier/shortcut approach.

Watch the next lesson: https://www.khanacademy.org/science/physics/torque-angular-momentum/rotational-kinematics/v/angular-motion-variables?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Missed the previous lesson? https://www.khanacademy.org/science/physics/linear-momentum/elastic-and-inelastic-collisions/v/deriving-the-shortcut-to-solve-elastic-collision-problems?utm_source=YT&utm_medium=Desc&utm_campaign=physics

Physics on Khan Academy: Physics is the study of the basic principles that govern the physical world around us. We'll start by looking at motion itself. Then, we'll learn about forces, momentum, energy, and other concepts in lots of different physical situations. To get the most out of physics, you'll need a solid understanding of algebra and a basic understanding of trigonometry.

About Khan Academy: Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.

For free. For everyone. Forever. #YouCanLearnAnything

Subscribe to Khan Academy’s Physics channel: https://www.youtube.com/channel/UC0oGarQW2lE5PxhGoQAKV7Q?sub_confirmation=1
Subscribe to Khan Academy: https://www.youtube.com/subscription_center?add_user=khanacademy
