---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Block_code
offline_file: ""
offline_thumbnail: ""
uuid: 667395b9-a8ae-4002-8ca2-bba5147b3515
updated: 1484308544
title: Block code
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/720px-HammingLimit.png
tags:
    - The block code and its parameters
    - The alphabet Σ
    - The message length k
    - The block length n
    - The rate R
    - The distance d
    - Popular notation
    - Examples
    - Error detection and correction properties
    - Lower and upper bounds of block codes
    - Family of codes
    - Hamming bound
    - Singleton bound
    - Plotkin bound
    - Gilbert–Varshamov bound
    - Johnson bound
    - Elias–Bassalygo bound
    - Sphere packings and lattices
    - Notes
categories:
    - Coding theory
---
In coding theory, a block code is any member of the large and important family of error-correcting codes that encode data in blocks. There is a vast number of examples for block codes, many of which have a wide range of practical applications. Block codes are conceptually useful because they allow coding theorists, mathematicians, and computer scientists to study the limitations of all block codes in a unified way. Such limitations often take the form of bounds that relate different parameters of the block code to each other, such as its rate and its ability to detect and correct errors.
