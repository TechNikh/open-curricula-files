---
version: 1
type: video
provider: YouTube
id: YZBStgZGyDY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Algebra/Algebra%20I/Linear%20equations/Solving%20equations%20with%20the%20distributive%20property.webm"
offline_file: ""
offline_thumbnail: ""
uuid: bd4338a5-ded1-4f1a-b35c-34e38886f9a4
updated: 1486069563
title: Solving equations with the distributive property
categories:
    - Linear equations
thumbnail_urls:
    - https://i3.ytimg.com/vi/YZBStgZGyDY/default.jpg
    - https://i3.ytimg.com/vi/YZBStgZGyDY/1.jpg
    - https://i3.ytimg.com/vi/YZBStgZGyDY/2.jpg
    - https://i3.ytimg.com/vi/YZBStgZGyDY/3.jpg
---
