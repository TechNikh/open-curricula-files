---
version: 1
type: video
provider: YouTube
id: D3a8NnpQ2vU
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 44a84dda-6a80-4f7e-8492-23cdbc11130f
updated: 1486069607
title: Factoring simple quadratic expression
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/D3a8NnpQ2vU/default.jpg
    - https://i3.ytimg.com/vi/D3a8NnpQ2vU/1.jpg
    - https://i3.ytimg.com/vi/D3a8NnpQ2vU/2.jpg
    - https://i3.ytimg.com/vi/D3a8NnpQ2vU/3.jpg
---
