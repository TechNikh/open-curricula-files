---
version: 1
type: video
provider: YouTube
id: BiGKR4eAVQw
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 061f0929-a357-4919-b74f-7f7aabbd373f
updated: 1486069648
title: Fractal Enlightenment 2
categories:
    - Animation
thumbnail_urls:
    - https://i3.ytimg.com/vi/BiGKR4eAVQw/default.jpg
    - https://i3.ytimg.com/vi/BiGKR4eAVQw/1.jpg
    - https://i3.ytimg.com/vi/BiGKR4eAVQw/2.jpg
    - https://i3.ytimg.com/vi/BiGKR4eAVQw/3.jpg
---
