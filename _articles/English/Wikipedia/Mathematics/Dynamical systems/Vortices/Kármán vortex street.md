---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/K%C3%A1rm%C3%A1n_vortex_street'
offline_file: ""
offline_thumbnail: ""
uuid: edb29c13-f397-45af-a99f-92bf8a7a0f0a
updated: 1484308879
title: Kármán vortex street
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/400px-Karmansche_Wirbelstr_kleine_Re.JPG
tags:
    - Analysis
    - In meteorology
    - Engineering problems
    - Formula
    - History
categories:
    - Vortices
---
In fluid dynamics, a Kármán vortex street (or a von Kármán vortex sheet) is a repeating pattern of swirling vortices caused by the unsteady separation of flow of a fluid around blunt bodies. It is named after the engineer and fluid dynamicist Theodore von Kármán,[1] and is responsible for such phenomena as the "singing" of suspended telephone or power lines, and the vibration of a car antenna at certain speeds.
