---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Transitive_reduction
offline_file: ""
offline_thumbnail: ""
uuid: 771d5181-c4ba-49b0-b1c5-982fb2b77e5d
updated: 1484308630
title: Transitive reduction
tags:
    - In directed acyclic graphs
    - In graphs with cycles
    - Computational complexity
    - Computing the reduction using the closure
    - Computing the closure using the reduction
    - Computing the reduction in sparse graphs
    - Notes
categories:
    - Graph theory
---
In mathematics, a transitive reduction of a directed graph is a graph with as few edges as possible that has the same reachability relation as the given graph. Equivalently, the given graph and its transitive reduction should have the same transitive closure as each other, and its transitive reduction should have as few edges as possible among all graphs with this property. Transitive reductions were introduced by Aho, Garey & Ullman (1972), who provided tight bounds on the computational complexity of constructing them.
