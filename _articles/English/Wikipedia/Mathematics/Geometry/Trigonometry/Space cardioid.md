---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Space_cardioid
offline_file: ""
offline_thumbnail: ""
uuid: bcf8578e-c0a7-413a-9911-bb9b9d5b3ab6
updated: 1484309350
title: Space cardioid
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/750px-Earth-moon_4.jpg
categories:
    - Trigonometry
---
The general form of the equation is most easily understood in parametric form, as follows:
