---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/emotionally
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484618281
title: emotionally
categories:
    - Dictionary
---
emotionally
     adv 1: in an emotional manner; "at the funeral he spoke
            emotionally" [syn: {showing emotion}] [ant: {unemotionally}]
     2: with regard to emotions; "emotionally secure"
