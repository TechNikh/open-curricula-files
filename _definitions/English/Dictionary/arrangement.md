---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/arrangement
offline_file: ""
offline_thumbnail: ""
uuid: 58d8bcab-bb90-4fb6-911a-90ac25acf5fd
updated: 1484310337
title: arrangement
categories:
    - Dictionary
---
arrangement
     n 1: the thing arranged or agreed to; "they made arrangements to
          meet in Chicago" [syn: {agreement}]
     2: an orderly grouping (of things or persons) considered as a
        unit; the result of arranging; "a flower arrangement"
     3: an organized structure for arranging or classifying; "he
        changed the arrangement of the topics"; "the facts were
        familiar but it was in the organization of them that he
        was original"; "he tried to understand their system of
        classification" [syn: {organization}, {organisation}, {system}]
     4: the spatial property of the way in which something is
        placed; "the arrangement of the furniture"; ...
