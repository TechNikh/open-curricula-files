---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/swear
offline_file: ""
offline_thumbnail: ""
uuid: 3df2828a-4784-4f87-97d2-77474527cd02
updated: 1484310563
title: swear
categories:
    - Dictionary
---
swear
     v 1: utter obscenities or profanities; "The drunken men were
          cursing loudly in the street" [syn: {curse}, {cuss}, {blaspheme},
           {imprecate}]
     2: to declare or affirm solemnly and formally as true; "Before
        God I swear I am innocent" [syn: {affirm}, {verify}, {assert},
         {avow}, {aver}, {swan}]
     3: promise solemnly; take an oath
     4: make a deposition; declare under oath [syn: {depose}, {depone}]
     5: have confidence or faith in; "We can trust in God"; "Rely on
        your friends"; "bank on your good education"; "I swear by
        my grandmother's recipes" [syn: {trust}, {rely}, {bank}]
        [ant: {distrust}, {distrust}]
     ...
