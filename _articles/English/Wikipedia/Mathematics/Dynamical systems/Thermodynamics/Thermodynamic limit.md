---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermodynamic_limit
offline_file: ""
offline_thumbnail: ""
uuid: 333c49c0-5fb1-42ba-bfd3-ed5ba72e24eb
updated: 1484308866
title: Thermodynamic limit
categories:
    - Thermodynamics
---
The thermodynamic limit, or macroscopic limit,[1] of a system in statistical mechanics is the limit for a large number N of particles (e.g., atoms or molecules) where the volume is taken to grow in proportion with the number of particles.[2] The thermodynamic limit is defined as the limit of a system with a large volume, with the particle density held fixed.[3]
