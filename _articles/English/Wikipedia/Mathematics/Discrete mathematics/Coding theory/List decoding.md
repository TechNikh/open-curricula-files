---
version: 1
type: article
id: https://en.wikipedia.org/wiki/List_decoding
offline_file: ""
offline_thumbnail: ""
uuid: c762cefd-a242-4520-86fc-536144614e20
updated: 1484308562
title: List decoding
tags:
    - Mathematical formulation
    - Motivation for list decoding
    - List-decoding potential
    - (p, L)-list-decodability
    - Combinatorics of list decoding
    - List-decoding capacity
    - Sketch of proof
    - List-decoding algorithms
    - Applications in complexity theory and cryptography
categories:
    - Coding theory
---
In computer science, particularly in coding theory, list decoding is an alternative to unique decoding of error-correcting codes for large error rates. The notion was proposed by Elias in the 1950s. The main idea behind list decoding is that the decoding algorithm instead of outputting a single possible message outputs a list of possibilities one of which is correct. This allows for handling a greater number of errors than that allowed by unique decoding.
