---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/chi
offline_file: ""
offline_thumbnail: ""
uuid: f0512959-ba41-490f-96bd-e77b95146fb7
updated: 1484310577
title: chi
categories:
    - Dictionary
---
ch'i
     n : the circulating life energy that in Chinese philosophy is
         thought to be inherent in all things; in traditional
         Chinese medicine the balance of negative and positive
         forms in the body is believed to be essential for good
         health [syn: {qi}, {chi}, {ki}]
