---
version: 1
type: article
id: >
    https://en.wikipedia.org/wiki/Special_conformal_transformation
offline_file: ""
offline_thumbnail: ""
uuid: 1ee25863-d52c-4854-b07a-1282cfb1eb5c
updated: 1484309319
title: Special conformal transformation
thumbnail_urls:
    - 'http://eschool2go.org/sites/default/files/articles/thumbnails/288px-Conformal_grid_before_M%25C3%25B6bius_transformation.svg_0.png'
categories:
    - Symmetry
---
Special conformal transformations arise from translation of spacetime and inversion. The inversion can be taken[1] to be multiplicative inversion of biquaternions B. The complex algebra B can be extended to P(B) through the projective line over a ring. Homographies on P(B) include translations:
