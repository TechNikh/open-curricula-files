---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hospitality
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484628541
title: hospitality
categories:
    - Dictionary
---
hospitality
     n : kindness in welcoming guests or strangers [syn: {cordial
         reception}] [ant: {inhospitality}]
