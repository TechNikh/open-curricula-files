---
version: 1
type: video
provider: YouTube
id: Rm-i1c7zr6Q
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Chemistry/Animations/Lattice%20Structures%20Part%201.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 31510269-c881-4035-be1b-a056e7c712b5
updated: 1486069664
title: Lattice Structures Part 1
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/Rm-i1c7zr6Q/default.jpg
    - https://i3.ytimg.com/vi/Rm-i1c7zr6Q/1.jpg
    - https://i3.ytimg.com/vi/Rm-i1c7zr6Q/2.jpg
    - https://i3.ytimg.com/vi/Rm-i1c7zr6Q/3.jpg
---
