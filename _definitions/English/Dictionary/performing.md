---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/performing
offline_file: ""
offline_thumbnail: ""
uuid: 1aad8940-18ad-4a0f-bbe7-2e2d303c2769
updated: 1484310158
title: performing
categories:
    - Dictionary
---
performing
     n : the performance of a part or role in a drama [syn: {acting},
          {playing}, {playacting}]
