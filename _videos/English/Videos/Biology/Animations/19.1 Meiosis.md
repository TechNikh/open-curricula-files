---
version: 1
type: video
provider: YouTube
id: 9zemQmT0h-k
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/19.1%20Meiosis.webm"
offline_file: ""
offline_thumbnail: ""
uuid: ed731980-f552-4e8d-aa77-77755c5af0ed
updated: 1486069676
title: 19.1 Meiosis
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/9zemQmT0h-k/default.jpg
    - https://i3.ytimg.com/vi/9zemQmT0h-k/1.jpg
    - https://i3.ytimg.com/vi/9zemQmT0h-k/2.jpg
    - https://i3.ytimg.com/vi/9zemQmT0h-k/3.jpg
---
