---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Strongly_connected_component
offline_file: ""
offline_thumbnail: ""
uuid: 0142601b-beb3-40b5-a3ec-c3dd0d337abc
updated: 1484309404
title: Strongly connected component
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Scc.png
tags:
    - Definitions
    - Algorithms
    - Applications
    - Related results
categories:
    - Graph connectivity
---
In the mathematical theory of directed graphs, a graph is said to be strongly connected if every vertex is reachable from every other vertex. The strongly connected components of an arbitrary directed graph form a partition into subgraphs that are themselves strongly connected. It is possible to test the strong connectivity of a graph, or to find its strongly connected components, in linear time.
