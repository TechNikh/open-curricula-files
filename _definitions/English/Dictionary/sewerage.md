---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sewerage
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484613541
title: sewerage
categories:
    - Dictionary
---
sewerage
     n 1: waste matter carried away in sewers or drains [syn: {sewage}]
     2: a waste pipe that carries away sewage or surface water [syn:
         {sewer}, {cloaca}]
