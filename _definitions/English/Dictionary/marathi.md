---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/marathi
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484582761
title: marathi
categories:
    - Dictionary
---
Marathi
     n : an Indic language; the state language of Maharashtra in west
         central India; written in the Devanagari script [syn: {Mahratti}]
