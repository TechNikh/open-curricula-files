---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/vapour
offline_file: ""
offline_thumbnail: ""
uuid: a52b2084-e605-4f2f-ab63-9932d2267cba
updated: 1484310226
title: vapour
categories:
    - Dictionary
---
vapour
     n 1: a visible suspension in the air of particles of some
          substance [syn: {vapor}]
     2: the process of becoming a vapor [syn: {vaporization}, {vaporisation},
         {vapor}, {evaporation}]
