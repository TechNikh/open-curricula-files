---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/staff
offline_file: ""
offline_thumbnail: ""
uuid: a8e809c7-4c6b-4f7e-ab8b-fae5f35a097b
updated: 1484310479
title: staff
categories:
    - Dictionary
---
staff
     n 1: personnel who assist their superior in carrying out an
          assigned task; "the hospital has an excellent nursing
          staff"; "the general relied on his staff to make routine
          decisions"
     2: the body of teachers and administrators at a school; "the
        dean addressed the letter to the entire staff of the
        university" [syn: {faculty}]
     3: a strong rod or stick with a specialized utilitarian
        purpose; "he walked with the help of a wooden staff"
     4: building material consisting of plaster and hair; used to
        cover external surfaces of temporary structure (as at an
        exposition) or for decoration
     5: a rod carried ...
