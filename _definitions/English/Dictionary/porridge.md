---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/porridge
offline_file: ""
offline_thumbnail: ""
uuid: 29990ea3-36aa-416d-89ff-1e33abd44b9f
updated: 1484310328
title: porridge
categories:
    - Dictionary
---
porridge
     n : soft food made by boiling oatmeal or other meal or legumes
         in water or milk until thick
