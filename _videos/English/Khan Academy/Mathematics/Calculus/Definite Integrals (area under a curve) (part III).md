---
version: 1
type: video
provider: YouTube
id: 7wUHJ7JQ-gs
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Khan%20Academy/Mathematics/Calculus/Definite%20Integrals%20%28area%20under%20a%20curve%29%20%28part%20III%29.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 3a0be7a6-3648-404f-8634-b104b2fce0f1
updated: 1486069696
title: Definite Integrals (area under a curve) (part III)
categories:
    - Calculus
thumbnail_urls:
    - https://i3.ytimg.com/vi/7wUHJ7JQ-gs/default.jpg
    - https://i3.ytimg.com/vi/7wUHJ7JQ-gs/1.jpg
    - https://i3.ytimg.com/vi/7wUHJ7JQ-gs/2.jpg
    - https://i3.ytimg.com/vi/7wUHJ7JQ-gs/3.jpg
---
