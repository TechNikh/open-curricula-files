---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thomas_W._Tucker
offline_file: ""
offline_thumbnail: ""
uuid: 6390b738-7010-4466-84f8-c6ee841e12f4
updated: 1484309475
title: Thomas W. Tucker
categories:
    - Graph theorists
---
Thomas William Tucker (born July 15, 1945) is an American mathematician, the Charles Hetherington Professor of Mathematics at Colgate University,[1] and an expert in the area of topological graph theory.[2][3]
