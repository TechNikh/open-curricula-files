---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/map
offline_file: ""
offline_thumbnail: ""
uuid: 2506da38-36cd-48d9-befd-345ec69c2242
updated: 1484310431
title: map
categories:
    - Dictionary
---
map
     n 1: a diagrammatic representation of the earth's surface (or
          part of it)
     2: a function such that for every element of one set there is a
        unique element of another set [syn: {mapping}, {correspondence}]
     v 1: make a map of; show or establish the features of details of;
          "map the surface of Venus"
     2: explore or survey for the purpose of making a map; "We
        haven't even begun to map the many galaxies that we know
        exist"
     3: locate within a specific region of a chromosome in relation
        to known DNA or gene sequences; "map the genes"
     4: plan, delineate, or arrange in detail; "map one's future"
        [syn: {map ...
