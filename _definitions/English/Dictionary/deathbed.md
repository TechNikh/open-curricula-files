---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/deathbed
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484522581
title: deathbed
categories:
    - Dictionary
---
deathbed
     n 1: the last few hours before death
     2: the bed on which a person dies
