---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Pseudo-modal_energies
offline_file: ""
offline_thumbnail: ""
uuid: bffdf98b-9a56-479e-945a-7f1154d2655b
updated: 1484308819
title: Pseudo-modal energies
categories:
    - Oscillation
---
Pseudo-modal energies are used for estimating the energy content of a mechanical system near its resonance frequencies. They are defined as the integral of the frequency response function within a certain bandwidth around a resonance.[1]
