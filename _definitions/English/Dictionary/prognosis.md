---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/prognosis
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484530021
title: prognosis
categories:
    - Dictionary
---
prognosis
     n 1: a prediction about how something (as the weather) will
          develop [syn: {forecast}]
     2: a prediction of the course of a disease [syn: {prospect}, {medical
        prognosis}]
     [also: {prognoses} (pl)]
