---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/shell
offline_file: ""
offline_thumbnail: ""
uuid: 9a9eeebb-f715-476d-94db-b278ff7f237e
updated: 1484310206
title: shell
categories:
    - Dictionary
---
shell
     n 1: ammunition consisting of a cylindrical metal casing
          containing an explosive charge and a projectile; fired
          from a large gun
     2: the material that forms the hard outer covering of many
        animals
     3: hard outer covering or case of certain organisms such as
        arthropods and turtles [syn: {carapace}, {cuticle}]
     4: the hard usually fibrous outer layer of some fruits
        especially nuts
     5: the exterior covering of a bird's egg [syn: {eggshell}]
     6: a rigid covering that envelops an object; "the satellite is
        covered with a smooth shell of ice"
     7: a very light narrow racing boat [syn: {racing shell}]
     8: the ...
