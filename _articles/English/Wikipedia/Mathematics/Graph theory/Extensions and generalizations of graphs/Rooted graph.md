---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Rooted_graph
offline_file: ""
offline_thumbnail: ""
uuid: 97938c2e-801f-4551-ae4b-5b53822e4f7e
updated: 1484309408
title: Rooted graph
tags:
    - Variations
    - Applications
    - Flow graphs
    - Set theory
    - Combinatorial enumeration
    - Related concepts
categories:
    - Extensions and generalizations of graphs
---
In mathematics, and, in particular, in graph theory, a rooted graph is a graph in which one vertex has been distinguished as the root.[1][2] Both directed and undirected versions of rooted graphs have been studied, and there are also variant definitions that allow multiple roots.
