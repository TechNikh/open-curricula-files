---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Main_stem
offline_file: ""
offline_thumbnail: ""
uuid: f80a60cd-9b3f-4240-aea7-3927e42e8ca3
updated: 1484308377
title: Main stem
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/300px-Mississippirivermapnew.jpg
categories:
    - Uncategorized
---
In hydrology, a main stem is "the primary downstream segment of a river, as contrasted to its tributaries".[1] Another common term for the main stem, the final large channel of a riverine system, is the trunk.[2] Water enters the main stem from the river's drainage basin, the land area through which the main stem and its tributaries flow.[3] A drainage basin may also be referred to as a watershed or catchment.[3]
