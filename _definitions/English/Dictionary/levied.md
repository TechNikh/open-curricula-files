---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/levied
offline_file: ""
offline_thumbnail: ""
uuid: 0f8fefba-c415-47b0-9d39-7fe4ea8e296b
updated: 1484310387
title: levied
categories:
    - Dictionary
---
levy
     n 1: a charge imposed and collected
     2: the act of drafting into military service [syn: {levy en
        masse}]
     v 1: impose and collect; "levy a fine" [syn: {impose}]
     2: cause to assemble or enlist in the military; "raise an
        army"; "recruit new soldiers" [syn: {recruit}, {raise}]
     [also: {levied}]
