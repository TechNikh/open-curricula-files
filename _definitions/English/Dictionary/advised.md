---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/advised
offline_file: ""
offline_thumbnail: ""
uuid: 855f998d-9269-44bf-92db-bff3e7fe552e
updated: 1484310330
title: advised
categories:
    - Dictionary
---
advised
     adj 1: having the benefit of careful prior consideration or
            counsel; "a well-advised delay in carrying out the
            plan" [syn: {well-advised}] [ant: {ill-advised}]
     2: having received information; "be kept advised"
