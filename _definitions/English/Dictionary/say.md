---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/say
offline_file: ""
offline_thumbnail: ""
uuid: 82199d03-20af-4223-972e-50f5619346ad
updated: 1484310319
title: say
categories:
    - Dictionary
---
say
     n : the chance to speak; "let him have his say"
     v 1: express in words; "He said that he wanted to marry her";
          "tell me what is bothering you"; "state your opinion";
          "state your name" [syn: {state}, {tell}]
     2: report or maintain; "He alleged that he was the victim of a
        crime"; "He said it was too late to intervene in the war";
        "The registrar says that I owe the school money" [syn: {allege},
         {aver}]
     3: express a supposition; "Let us say that he did not tell the
        truth"; "Let's say you had a lot of money--what would you
        do?" [syn: {suppose}]
     4: have or contain a certain wording or form; "The passage
       ...
