---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Control_of_chaos
offline_file: ""
offline_thumbnail: ""
uuid: 0abe5c2f-e84c-47e4-a3e4-42e229533bdf
updated: 1484308707
title: Control of chaos
tags:
    - OGY method
    - Pyragas method
    - Applications
categories:
    - Chaos theory
---
In lab experiments that study chaos theory, approaches designed to control chaos are based on certain observed system behaviors. Any chaotic attractor contains an infinite number of unstable, periodic orbits. Chaotic dynamics, then, consists of a motion where the system state moves in the neighborhood of one of these orbits for a while, then falls close to a different unstable, periodic orbit where it remains for a limited time, and so forth. This results in a complicated and unpredictable wandering over longer periods of time.[1]
