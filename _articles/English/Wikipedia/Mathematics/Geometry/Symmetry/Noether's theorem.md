---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Noether%27s_theorem'
offline_file: ""
offline_thumbnail: ""
uuid: 0d38870c-4782-4c9d-ad18-bb0b6f2613d5
updated: 1484309313
title: "Noether's theorem"
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Noether.jpg
tags:
    - Basic illustrations and background
    - Informal statement of the theorem
    - Historical context
    - Mathematical expression
    - Simple form using perturbations
    - Examples
    - Field theory version
    - Derivations
    - One independent variable
    - Field-theoretic derivation
    - Manifold/fiber bundle derivation
    - Comments
    - Generalization to Lie algebras
    - Generalization of the proof
    - Examples
    - 'Example 1: Conservation of energy'
    - 'Example 2: Conservation of center of momentum'
    - 'Example 3: Conformal transformation'
    - Applications
    - Notes
categories:
    - Symmetry
---
Noether's (first)[1] theorem states that every differentiable symmetry of the action of a physical system has a corresponding conservation law. The theorem was proven by mathematician Emmy Noether in 1915 and published in 1918.[2] The action of a physical system is the integral over time of a Lagrangian function (which may or may not be an integral over space of a Lagrangian density function), from which the system's behavior can be determined by the principle of least action.
