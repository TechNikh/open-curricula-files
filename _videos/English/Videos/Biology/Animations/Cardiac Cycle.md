---
version: 1
type: video
provider: YouTube
id: rguztY8aqpk
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/Cardiac%20Cycle.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 9e771ed8-97b4-4690-998a-95f0b0ffb85f
updated: 1486069670
title: Cardiac Cycle
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/rguztY8aqpk/default.jpg
    - https://i3.ytimg.com/vi/rguztY8aqpk/1.jpg
    - https://i3.ytimg.com/vi/rguztY8aqpk/2.jpg
    - https://i3.ytimg.com/vi/rguztY8aqpk/3.jpg
---
