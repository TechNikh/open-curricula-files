---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/refrigerator
offline_file: ""
offline_thumbnail: ""
uuid: 3ac7aad2-4d3f-4a55-96a9-81e768ec236b
updated: 1484310232
title: refrigerator
categories:
    - Dictionary
---
refrigerator
     n : white goods in which food can be stored at low temperatures
         [syn: {icebox}]
