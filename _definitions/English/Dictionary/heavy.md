---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/heavy
offline_file: ""
offline_thumbnail: ""
uuid: 9a6af666-3212-4a75-b5d9-8b7e55b749af
updated: 1484310479
title: heavy
categories:
    - Dictionary
---
heavy
     adj 1: of comparatively great physical weight or density; "a heavy
            load"; "lead is a heavy metal"; "heavy mahogony
            furniture" [ant: {light}]
     2: unusually great in degree or quantity or number; "heavy
        taxes"; "a heavy fine"; "heavy casualties"; "heavy
        losses"; "heavy rain"; "heavy traffic" [ant: {light}]
     3: of the military or industry; using (or being) the heaviest
        and most powerful armaments or weapons or equipment;
        "heavy artillery"; "heavy infantry"; "a heavy cruiser";
        "heavy guns"; "heavy industry involves large-scale
        production of basic products (such as steel) used by other
        industries" ...
