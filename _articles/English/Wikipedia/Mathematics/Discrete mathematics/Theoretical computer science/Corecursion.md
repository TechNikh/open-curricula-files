---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Corecursion
offline_file: ""
offline_thumbnail: ""
uuid: e7c4076f-44eb-45c8-b481-2e6828ab8cfc
updated: 1484308675
title: Corecursion
tags:
    - Examples
    - Factorial
    - Fibonacci sequence
    - Tree traversal
    - Definition
    - Discussion
    - History
    - Notes
categories:
    - Theoretical computer science
---
In computer science, corecursion is a type of operation that is dual to recursion. Whereas recursion works analytically, starting on data further from a base case and breaking it down into smaller data and repeating until one reaches a base case, corecursion works synthetically, starting from a base case and building it up, iteratively producing data further removed from a base case. Put simply, corecursive algorithms use the data that they themselves produce, bit by bit, as they become available, and needed, to produce further bits of data. A similar but distinct concept is generative recursion which may lack a definite "direction" inherent in corecursion and recursion.
