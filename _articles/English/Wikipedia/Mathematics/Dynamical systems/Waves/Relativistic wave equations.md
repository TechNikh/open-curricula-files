---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Relativistic_wave_equations
offline_file: ""
offline_thumbnail: ""
uuid: 9f746902-bba3-43e7-8971-3ab86c8be5e4
updated: 1484308903
title: Relativistic wave equations
tags:
    - History
    - 'Early 1920s: Classical and quantum mechanics'
    - 'Late 1920s: Relativistic quantum mechanics of spin-0 and spin-1/2 particles'
    - '1930s–1960s: Relativistic quantum mechanics of higher-spin particles'
    - 1960s–Present
    - Linear equations
    - >
        Derivation of basic relativistic quantum wave equations
        using 4-vectors
    - Gauge fields
    - Non-linear equations
    - Gauge fields
    - Spin 2
    - Notes
categories:
    - Waves
---
In physics, specifically relativistic quantum mechanics (RQM) and its applications to particle physics, relativistic wave equations predict the behavior of particles at high energies and velocities comparable to the speed of light. In the context of quantum field theory (QFT), the equations determine the dynamics of quantum fields.
