---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/officer
offline_file: ""
offline_thumbnail: ""
uuid: 9defbbfa-6a45-48ff-9e33-a759ce8df6f4
updated: 1484310424
title: officer
categories:
    - Dictionary
---
officer
     n 1: any person in the armed services who holds a position of
          authority or command; "an officer is responsible for the
          lives of his men" [syn: {military officer}]
     2: someone who is appointed or elected to an office and who
        holds a position of trust; "he is an officer of the
        court"; "the club elected its officers for the coming
        year" [syn: {officeholder}]
     3: a member of a police force; "it was an accident, officer"
        [syn: {policeman}, {police officer}]
     4: a person authorized to serve in a position of authority on a
        vessel; "he is the officer in charge of the ship's
        engines" [syn: {ship's officer}]
 ...
