---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/ultraviolet
offline_file: ""
offline_thumbnail: ""
uuid: 7a1c45cf-16ee-440e-9db3-854f26bb424e
updated: 1484310389
title: ultraviolet
categories:
    - Dictionary
---
ultraviolet
     adj : having or employing wavelengths shorter than light but
           longer than X-rays; lying outside the visible spectrum
           at its violet end; "ultraviolet radiation"; "an
           ultraviolet lamp"
     n : radiation lying in the ultraviolet range; wave lengths
         shorter than light but longer than X rays [syn: {ultraviolet
         radiation}, {ultraviolet light}, {ultraviolet
         illumination}, {UV}]
