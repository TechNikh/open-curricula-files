---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/aware
offline_file: ""
offline_thumbnail: ""
uuid: 53b264d9-c8de-413d-a401-1a62468b12e3
updated: 1484310246
title: aware
categories:
    - Dictionary
---
aware
     adj 1: (sometimes followed by `of') having or showing realization
            or perception; "was aware of his opponent's
            hostility"; "became aware of her surroundings"; "aware
            that he had exceeded the speed limit" [syn: {aware(p)}]
            [ant: {unaware}]
     2: bearing in mind; attentive to; "ever mindful of her health";
        "mindful of his responsibilities"; "mindful of these
        criticisms, I shall attempt to justify my action" [syn: {mindful}]
        [ant: {unmindful}]
     3: aware or knowing; "a witting tool of the Communists" [syn: {witting}]
        [ant: {unwitting}]
     4: (usually followed by `of') having knowledge or
        ...
