---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pursue
offline_file: ""
offline_thumbnail: ""
uuid: 9729e214-ebbf-4a4f-8b4d-601da951d1b1
updated: 1484310441
title: pursue
categories:
    - Dictionary
---
pursue
     v 1: carry out or participate in an activity; be involved in;
          "She pursued many activities"; "They engaged in a
          discussion" [syn: {prosecute}, {engage}]
     2: follow in or as if in pursuit; "The police car pursued the
        suspected attacker"; "Her bad deed followed her and
        haunted her dreams all her life" [syn: {follow}]
     3: go in search of or hunt for; "pursue a hobby" [syn: {quest
        for}, {go after}, {quest after}]
     4: carry further or advance; "Can you act on this matter soon?"
        [syn: {follow up on}, {act on}]
