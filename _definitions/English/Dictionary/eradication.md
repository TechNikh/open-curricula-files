---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/eradication
offline_file: ""
offline_thumbnail: ""
uuid: c7e84ee5-eee2-46a3-8f1d-006f1d285f53
updated: 1484310609
title: eradication
categories:
    - Dictionary
---
eradication
     n : the complete destruction of every trace of something [syn: {obliteration}]
