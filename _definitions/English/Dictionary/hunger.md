---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hunger
offline_file: ""
offline_thumbnail: ""
uuid: 018c47f2-9959-43b2-817d-266843f49f8a
updated: 1484310351
title: hunger
categories:
    - Dictionary
---
hunger
     n 1: a physiological need for food; the consequence of foood
          deprivation [syn: {hungriness}]
     2: strong desire for something (not food or drink); "a thirst
        for knowledge"; "hunger for affection" [syn: {thirst}]
     v 1: feel the need to eat
     2: have a craving, appetite, or great desire for [syn: {crave},
         {thirst}, {starve}, {lust}]
     3: be hungry; go without food; "Let's eat--I'm starving!" [syn:
         {starve}, {famish}] [ant: {be full}]
