---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/lending
offline_file: ""
offline_thumbnail: ""
uuid: 5b63f300-bfb3-4230-a9ed-85db3167270b
updated: 1484310521
title: lending
categories:
    - Dictionary
---
lending
     n : disposing of money or property with the expectation that the
         same thing (or an equivalent) will be returned [syn: {loaning}]
