---
version: 1
type: article
id: https://en.wikipedia.org/wiki/All_Students_Take_Calculus
offline_file: ""
offline_thumbnail: ""
uuid: 7f519ed6-678a-4cfe-8e64-7828c61985ac
updated: 1484309338
title: All Students Take Calculus
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-All_Students_Take_Calculus.svg.png
tags:
    - Other variants
categories:
    - Trigonometry
---
All Students Take Calculus is a mnemonic in mathematics that is used to help people memorize the sign values of all the trigonometric functions in the 2-dimensional Cartesian coordinate system. The letters ASTC signify which of the trigonometric functions are positive, in the order of the quadrants - starting in the top right quadrant, and moving counterclockwise.
