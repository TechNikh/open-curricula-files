---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/grandstand
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484470801
title: grandstand
categories:
    - Dictionary
---
grandstand
     n 1: the audience at a stadium or racetrack
     2: a stand at a racecourse or stadium consisting of tiers with
        rows of individual seats that are under a protective roof
        [syn: {covered stand}]
     v : perform ostentatiously in order to impress the audience and
         with an eye to the applause; "She never misses a chance
         to grandstand"
