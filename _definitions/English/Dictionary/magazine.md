---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/magazine
offline_file: ""
offline_thumbnail: ""
uuid: 5b61e414-a5c2-4bd8-9c9f-ad86887ed318
updated: 1484310521
title: magazine
categories:
    - Dictionary
---
magazine
     n 1: a periodic paperback publication; "it takes several years
          before a magazine starts to break even or make money"
          [syn: {mag}]
     2: product consisting of a paperback periodic publication as a
        physical object; "tripped over a pile of magazines"
     3: a business firm that publishes magazines; "he works for a
        magazine" [syn: {magazine publisher}]
     4: a light-tight supply chamber holding the film and supplying
        it for exposure as required [syn: {cartridge}]
     5: a storehouse (as a compartment on a warship) where weapons
        and ammunition are stored [syn: {powder store}, {powder
        magazine}]
     6: a metal frame ...
