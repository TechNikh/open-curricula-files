---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/consensual
offline_file: ""
offline_thumbnail: ""
uuid: 30c8015b-0244-4733-92d4-866e8e52d0f8
updated: 1484310603
title: consensual
categories:
    - Dictionary
---
consensual
     adj : existing by consent; "a consensual contract"
