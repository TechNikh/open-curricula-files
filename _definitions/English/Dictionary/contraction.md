---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/contraction
offline_file: ""
offline_thumbnail: ""
uuid: 2c740be2-56b7-40d5-b597-c6270f1caf76
updated: 1484310330
title: contraction
categories:
    - Dictionary
---
contraction
     n 1: (physiology) a shortening or tensing of a part or organ
          (especially of a muscle or muscle fiber) [syn: {muscular
          contraction}, {muscle contraction}]
     2: the process or result of becoming smaller or pressed
        together; "the contraction of a gas on cooling" [syn: {compression},
         {condensation}]
     3: a word formed from two or more words by omitting or
        combining some sounds; "`won't' is a contraction of `will
        not'"; "`o'clock' is a contraction of `of the clock'"
     4: the act of decreasing (something) in size or volume or
        quantity or scope [ant: {expansion}]
