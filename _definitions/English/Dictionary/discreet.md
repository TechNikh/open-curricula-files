---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/discreet
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484518861
title: discreet
categories:
    - Dictionary
---
discreet
     adj 1: marked by prudence or modesty and wise self-restraint; "his
            trusted discreet aide"; "a discreet, finely wrought
            gold necklace" [ant: {indiscreet}]
     2: unobtrusively perceptive and sympathetic; "a discerning
        editor"; "a discreet silence" [syn: {discerning}]
     3: heedful of potential consequences; "circumspect actions";
        "physicians are now more circumspect about recommending
        its use"; "a discreet investor" [syn: {circumspect}]
