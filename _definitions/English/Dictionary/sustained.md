---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/sustained
offline_file: ""
offline_thumbnail: ""
uuid: 646f4de1-04ea-4ccd-8abf-2d12bc41ff7d
updated: 1484310565
title: sustained
categories:
    - Dictionary
---
sustained
     adj 1: maintained at length without interruption or weakening;
            "sustained flight"
     2: (of an electric arc) continuous; "heat transfer to the anode
        in free burning arcs" [syn: {free burning}]
