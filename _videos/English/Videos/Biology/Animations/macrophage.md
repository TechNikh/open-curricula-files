---
version: 1
type: video
provider: YouTube
id: spQnopJVUXo
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/macrophage.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5be046fe-e4ab-40f4-9178-8fe65a29479a
updated: 1486069670
title: macrophage
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/spQnopJVUXo/default.jpg
    - https://i3.ytimg.com/vi/spQnopJVUXo/1.jpg
    - https://i3.ytimg.com/vi/spQnopJVUXo/2.jpg
    - https://i3.ytimg.com/vi/spQnopJVUXo/3.jpg
---
