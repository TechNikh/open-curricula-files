---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Human_beings
offline_file: ""
offline_thumbnail: ""
uuid: e151d47c-0fde-4045-bd48-e36b24df924c
updated: 1484308366
title: Human
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Map-of-human-migrations.jpg
tags:
    - Etymology and definition
    - History
    - Evolution and range
    - Evidence from molecular biology
    - Evidence from the fossil record
    - Anatomical adaptations
    - Rise of Homo sapiens
    - Transition to civilization
    - Habitat and population
    - Biology
    - Anatomy and physiology
    - Genetics
    - Life cycle
    - Diet
    - Biological variation
    - Structure of variation
    - Psychology
    - Sleep and dreaming
    - Consciousness and thought
    - Motivation and emotion
    - Sexuality and love
    - Behavior
    - language
    - Gender roles
    - Kinship
    - Ethnicity
    - Society, government, and politics
    - Trade and economics
    - War
    - Material culture and technology
    - Body culture
    - Philosophy and self-reflection
    - Religion and spirituality
    - Art, music, and literature
    - Science
categories:
    - Uncategorized
---
Modern humans (Homo sapiens, primarily ssp. Homo sapiens sapiens) are the only extant members of Hominina clade (or human clade), a branch of the taxonomical tribe Hominini belonging to the family of great apes. They are characterized by erect posture and bipedal locomotion; manual dexterity and increased tool use, compared to other animals; and a general trend toward larger, more complex brains and societies.[3][4]
