---
version: 1
type: video
provider: YouTube
id: 00t9h3EnNSk
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: e3cf09ea-d4fb-4630-88dc-a1d6ab9567eb
updated: 1486069657
title: Voltage Sources, Part 2, Current Sources-A
categories:
    - Tricks
thumbnail_urls:
    - https://i3.ytimg.com/vi/00t9h3EnNSk/default.jpg
    - https://i3.ytimg.com/vi/00t9h3EnNSk/1.jpg
    - https://i3.ytimg.com/vi/00t9h3EnNSk/2.jpg
    - https://i3.ytimg.com/vi/00t9h3EnNSk/3.jpg
---
