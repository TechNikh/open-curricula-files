---
version: 1
type: video
provider: YouTube
id: WEvt5I-PRf4
download_url: ""
offline_file: ""
offline_thumbnail: ""
uuid: 521cdf05-3476-42cb-b4f8-51f116a0a5a8
updated: 1486069614
title: Example of plotting a simple inequality on a number line
categories:
    - Maths
thumbnail_urls:
    - https://i3.ytimg.com/vi/WEvt5I-PRf4/default.jpg
    - https://i3.ytimg.com/vi/WEvt5I-PRf4/1.jpg
    - https://i3.ytimg.com/vi/WEvt5I-PRf4/2.jpg
    - https://i3.ytimg.com/vi/WEvt5I-PRf4/3.jpg
---
