---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Thermos_flask
offline_file: ""
offline_thumbnail: ""
uuid: fad46f04-cb54-47cc-8c46-d07b1021134a
updated: 1484308375
title: Vacuum flask
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/170px-Thermos.JPG
tags:
    - History
    - Design
    - Research and industry
    - Safety
    - Thermodynamics
categories:
    - Uncategorized
---
A vacuum flask (also known as a Dewar flask, Dewar bottle or thermos) is an insulating storage vessel that greatly lengthens the time over which its contents remain hotter or cooler than the flask's surroundings. Invented by Sir James Dewar in 1892, the vacuum flask consists of two flasks, placed one within the other and joined at the neck. The gap between the two flasks is partially evacuated of air, creating a near-vacuum which significantly reduces heat transfer by conduction or convection.
