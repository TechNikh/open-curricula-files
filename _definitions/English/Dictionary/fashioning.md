---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/fashioning
offline_file: ""
offline_thumbnail: ""
uuid: 565d32a2-6031-4b0a-b9db-028f50aa6bb2
updated: 1484310599
title: fashioning
categories:
    - Dictionary
---
fashioning
     n : the act that results in something coming to be; "the
         devising of plans"; "the fashioning of pots and pans";
         "the making of measurements"; "it was already in the
         making" [syn: {devising}, {making}]
