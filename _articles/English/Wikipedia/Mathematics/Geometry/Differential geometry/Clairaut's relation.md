---
version: 1
type: article
id: 'https://en.wikipedia.org/wiki/Clairaut%27s_relation'
offline_file: ""
offline_thumbnail: ""
uuid: 8698a6c5-417e-4b02-a979-e4ca8e51940f
updated: 1484309207
title: "Clairaut's relation"
categories:
    - Differential geometry
---
Clairaut's relation, named after Alexis Claude de Clairaut, is a formula in classical differential geometry. The formula relates the distance r(t) from a point on a great circle of the unit sphere to the z-axis, and the angle θ(t) between the tangent vector and the latitudinal circle:
