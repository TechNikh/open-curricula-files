---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/pitched
offline_file: ""
offline_thumbnail: ""
uuid: f601a6db-6446-40b7-a6aa-161eb976e401
updated: 1484310607
title: pitched
categories:
    - Dictionary
---
pitched
     adj 1: (of sound) set to a certain pitch or key; usually used as a
            combining form; "high-pitched"
     2: set at a slant; "a pitched rather than a flat roof"
