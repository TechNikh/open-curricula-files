---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/hydrolysis
offline_file: ""
offline_thumbnail: ""
uuid: e765d13a-a270-42d6-b6c1-5569e5aa1664
updated: 1484310426
title: hydrolysis
categories:
    - Dictionary
---
hydrolysis
     n : a chemical reaction in which water reacts with a compound to
         produce other compounds; involves the splitting of a bond
         and the addition of the hydrogen cation and the hydroxide
         anion from the water
