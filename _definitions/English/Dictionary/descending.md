---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/descending
offline_file: ""
offline_thumbnail: ""
uuid: 50994dde-e50b-4923-8142-ebb502670e3c
updated: 1484310409
title: descending
categories:
    - Dictionary
---
descending
     adj : coming down or downward [syn: {descending(a)}] [ant: {ascending(a)}]
