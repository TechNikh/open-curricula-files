---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Deduction_theorem
offline_file: ""
offline_thumbnail: ""
uuid: 3598511e-a006-4304-940a-18cc6b3a3ef3
updated: 1484308997
title: Deduction theorem
tags:
    - Examples of deduction
    - Virtual rules of inference
    - >
        Conversion from proof using the deduction meta-theorem to
        axiomatic proof
    - The deduction theorem in predicate logic
    - Example of conversion
    - Paraconsistent deduction theorem
    - The resolution theorem
    - Notes
categories:
    - Theorems in the foundations of mathematics
---
In mathematical logic, the deduction theorem is a metatheorem of first-order logic.[1] It is a formalization of the common proof technique in which an implication A → B is proved by assuming A and then deriving B from this assumption conjoined with known results. The deduction theorem explains why proofs of conditional sentences in mathematics are logically correct. Though it has seemed "obvious" to mathematicians literally for centuries that proving B from A conjoined with a set of theorems is sufficient to proving the implication A → B based on those theorems alone, it was left to Herbrand and Tarski to show (independently) this was logically correct in the general case.
