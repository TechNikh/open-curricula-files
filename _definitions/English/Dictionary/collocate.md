---
version: 1
type: definition
id: https://en.wiktionary.org/wiki/collocate
offline_file: ""
offline_thumbnail: ""
uuid: ""
updated: 1484525221
title: collocate
categories:
    - Dictionary
---
collocate
     v 1: have a strong tendency to occur side by side; "The words
          'new' and 'world' collocate"
     2: group or chunk together in a certain order or place side by
        side [syn: {lump}, {chunk}]
