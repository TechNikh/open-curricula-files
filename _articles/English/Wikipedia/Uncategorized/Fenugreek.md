---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Fenugreek
offline_file: ""
offline_thumbnail: ""
uuid: 524e88f6-7105-42f8-b5f1-6ce2ddb042bf
updated: 1484308378
title: Fenugreek
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Aesthetic_bunch_of_fenugreek_greens.jpg
tags:
    - History
    - Etymology
    - Production
    - use
    - Cooking
    - Nutritional profile
    - Safety
    - Research
categories:
    - Uncategorized
---
Fenugreek (/ˈfɛnjᵿɡriːk/; Trigonella foenum-graecum) is an annual plant in the family Fabaceae, with leaves consisting of three small obovate to oblong leaflets. It is cultivated worldwide as a semiarid crop, and its seeds are a common ingredient in dishes from the Indian subcontinent.
