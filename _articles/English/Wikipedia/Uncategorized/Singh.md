---
version: 1
type: article
id: https://en.wikipedia.org/wiki/Singh
offline_file: ""
offline_thumbnail: ""
uuid: 0533b875-6c6d-4686-aadc-bc1ff41f7b0e
updated: 1484308444
title: Singh
thumbnail_urls:
    - >
        http://eschool2go.org/sites/default/files/articles/thumbnails/220px-Rana_Sanga.jpg
tags:
    - Etymology and variations
    - History
    - Usage
    - 'Immigration issues: common surname'
categories:
    - Uncategorized
---
Singh /ˈsɪŋ/ is a title, middle name or surname, which originated in India. Derived from the Sanskrit word for lion, it was adopted as a title by certain warrior castes in India.[1] It was later adopted by several castes and communities, including the Sikhs, and was mandated by Guru Gobind Singh for all Sikhs. As a surname or a middle name, it is now found throughout the Indian subcontinent and among the Indian diaspora, cutting across communities and religious groups, becoming more of a title than a surname.[2]
