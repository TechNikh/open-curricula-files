---
version: 1
type: video
provider: YouTube
id: XbI8eY-BeXY
download_url: "https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/The%20Kidney%20%26%20the%20Counter%20Current%20Multiplier-%20Pharmacology%20teaching%20at%20Aston%20University.webm"
offline_file: ""
offline_thumbnail: ""
uuid: 5a34dc0d-3fd4-45db-ac2f-d4257074f79a
updated: 1486069670
title: 'The Kidney & the Counter Current Multiplier- Pharmacology teaching at Aston University'
categories:
    - Animations
thumbnail_urls:
    - https://i3.ytimg.com/vi/XbI8eY-BeXY/default.jpg
    - https://i3.ytimg.com/vi/XbI8eY-BeXY/1.jpg
    - https://i3.ytimg.com/vi/XbI8eY-BeXY/2.jpg
    - https://i3.ytimg.com/vi/XbI8eY-BeXY/3.jpg
---
